    const general = require('custom/general');
    const custom_socket = require('custom/custom_socket');
    const chat_message = require('custom/chat_message');
    const app = general.app;
    const http = general.http;
    const io = general.io;
    const connection = general.connection;
    var users = [];

    chat_message.init_chat(app, connection ,users);

    io.on('connection', function (socket) {
        custom_socket.init_socket(users, socket, connection);
    });
   