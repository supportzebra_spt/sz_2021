    var app = require('express')();
    var http = require('http').Server(app);
    var io = require('socket.io')(http);

    //    app.get('/', function (req, res) {
    //        res.sendFile(__dirname + '/index.html');
    //    });

    http.listen(2516, function() {
        console.log('listening on *:2516');
    });

    var users = [];

    io.on('connection', function(socket) {
        var socketId = socket.id;
        console.log(socketId + " is connected.");
        socket.on('disconnect', function() {
            console.log(socketId + " is disconnected.");
            for (var x = 0; x < users.length; x++) {
                if (socketId === users[x].socketid) {
                    users.splice(x, 1);
                }
            }
        });
        //page clock
        socket.on('requestTime', function() {
            socket.emit('responseTime', new Date());
        });
	
        //every tab
        socket.on('new user', function(data) {
            var userdata = {
                socketid: socketId,
                uid: data.uid,
                emp_id: data.emp_id
            };
            users.push(userdata);
        });

        //every request notifications
        socket.on('new notifications', function(details) {
            for (var x = 0; x < users.length; x++) {
                if (users[x].uid + "" === details.uid + "") {
					console.log(users[x].uid + " === "+ details.uid + "")
                     socket.to(users[x].socketid).emit('new notification', {
                        uid: details.uid,
                        notification_ID: details.notification_ID,
                        link: details.link,
                        notificationRecipient_ID: details.notificationRecipient_ID,
                        status: details.status,
                        pic: details.pic,
                        message: details.message,
                        sender: details.sender,
                        notification: details.notification,
                        timeElapsed: details.timeElapsed,
                        createdOn: details.createdOn,
                        unreadCount: details.unreadCount
                    });
                }
            }

        });
        //every system notifications
        socket.on('new system_notification', function(details) {
            for (var x = 0; x < users.length; x++) {
                if (users[x].uid + "" === details.uid + "") {
                    // console.log("nakasulod ko!");
                    socket.to(users[x].socketid).emit('new system_notification', {
                        uid: details.uid,
                        systemNotification_ID: details.systemNotification_ID,
                        link: details.link,
                        systemNotificationRecipient_ID: details.systemNotificationRecipient_ID,
                        status: details.status,
                        pic: details.pic,
                        message: details.message,
                        notification: details.notification,
                        timeElapsed: details.timeElapsed,
                        createdOn: details.createdOn,
                        unreadCount: details.unreadCount
                    });
                }
            }

        });

        //every request notifications
        socket.on('dtr violation', function(details) {

            for (var x = 0; x < users.length; x++) {
                // console.log(users[x].emp_id+" ***  "+details.supervisor);
                if (users[x].emp_id === details.supervisor) {
                    socket.to(users[x].socketid).emit('dtr violation2', {
                        uid: details.uid,
                    });
                }
            }

        });
		socket.on('live', function(details) {

            for (var x = 0; x < users.length; x++) {
                if (users[x].emp_id+"" === details.sup+"") {
					console.log(users[x].emp_id+" === "+details.sup+"");
					socket.to(users[x].socketid).emit('log',{
                        emp_id: details.emp_id,
                        uid: details.uid,
                        sup: details.sup,
                        fname: details.fname,
                        lname: details.lname,
                        pic: details.pic,
						position: details.position,
						account: details.account,
						dtr: details.dtr,

                    });
                }
            }

        });
    });