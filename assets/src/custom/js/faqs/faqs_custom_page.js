var offenseDatatable = function() {
    var offense = function(searchVal = "") {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl+"/Faqs/faqs_custom_group_data",
                        params: {
                            query: {
                                searchField: searchVal,
                            },
                        },
                        map: function(raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', 
                class: '', 
                scroll: true, 
                height: 550, 
                footer: false 
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50] 
                    },
                }
            },
            search: {
                input: $('#searchField'),
            },
            columns: 
            [{
                field: "distro_id",
                title: "#",
                width: 50,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    index = index + 1;
                    return index;
                },
            }, 
            {
                field: 'distro_name',
                width: 150,
                title: 'Distro Name',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
            },
            {
                field: 'emp_id',
                width: 150,
                title: 'Created by',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
                template: function(row, index, datatable) 
                {
                    var updatedBy = row.lname+', '+row.fname+' '+row.mname;
                    return updatedBy;
                },
            },
            {
                field: 'timeUpdate',
                width: 120,
                title: 'Date Created',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
                template: function(row, index, datatable) 
                {
                    var updatedTime = moment(row.timeUpdate).format('MMMM D, YYYY hh:mm:ss A');
                    return updatedTime;
                },
            },
            {
                field: "Actions",
                width: 108,
                title: "Actions",
                sortable: false,
                textAlign: 'center',
                overflow: 'visible',
                template: function(row, index, datatable) {
                    var edit,remove;
                    var session_emp_id = pass_emp_id;
                    var display_action = '';
                    edit = '<span data-skin="dark" data-container="body" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="Update Employee List">\
                                    <a class="custom_group_edit m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-list_emp_id="' + row.list_emp_id + '" data-list_group_emp_id="' + row.list_group_emp_id + '" data-distro_id="' + row.distro_id + '" data-distro_name="' + row.distro_name + '">\
                                        <i class="la la-edit"></i>\
                                    </a>\
                                </span>';
                    remove = '<span data-skin="dark" data-container="body" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="Remove Employee List">\
                                    <a class="custom_group_delete m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-distro_id="' + row.distro_id + '" >\
                                        <i class="la la-trash"></i>\
                                    </a>\
                                </span>';
                    if(row.emp_id == session_emp_id){
                        display_action = "\
                        "+edit+"\
                        "+remove+"\
                        ";
                    }
                    return '\
                    <span data-skin="dark" data-container="body" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="View Employee List">\
                        <a class="custom_group_view m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-distro_id="' + row.distro_id + '" data-list_emp_id="' + row.list_emp_id + '" data-list_group_emp_id="' + row.list_group_emp_id + '">\
                            <i class="la la-eye"></i>\
                        </a>\
                    </span>\
                    '+display_action+'\
                    ';
                }
            }
            ],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
        init: function(searchVal) {
            offense(searchVal);
        }
    };
}();

function initOffenses() {
    $('#offenseDatatable').mDatatable('destroy');
    offenseDatatable.init();
}

function capitalizeTheFirstLetterOfEachWord(words) {
   var separateWord = words.toLowerCase().split(' ');
   for (var i = 0; i < separateWord.length; i++) {
      separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
      separateWord[i].substring(1);
  }
  return separateWord.join(' ');
}

function filterItems(needle, heystack) {
  var query = needle.toLowerCase();
  return heystack.filter(function(item) {
      return item.toLowerCase().indexOf(query) >= 0;
  })
}

$(function() {
    initOffenses();
    $('.m_selectpicker').selectpicker();
    $('#custom_name_add').maxlength({
        threshold: 3,
        warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
        limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
        appendToParent: true,
        separator: ' of ',
        preText: 'You have ',
        postText: ' chars remaining.',
        validate: true
    });
})

function submitDatav2(list_emp_id) {
    var distro_id = $('#distro_id_edit').val();
    var distro_name = $('#distro_name_edit').val();
    $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_custom_group_update",
        dataType: "JSON",
        data: {
            distro_id,
            distro_name,
            list_emp_id: list_emp_id.toString(),
        },
        success: function(data) {
            $('#category_id_edit').val("");
            $('#category_name_edit').val("");
            $('#list_emp_id_edit').selectpicker("val", "");
            $('#list_acc_id_edit').selectpicker("val", "");
            $('#modal_update').modal('hide');
            swal("Success!", "Your file has been updated!", "success");
            $('#offenseDatatable').mDatatable('reload');
        },
        error: function(data) {
            swal('Error!', 'Custom Group Name already exist!', 'error');
        }
    });
    return false;
}

$('#addDistro').on('submit', function(e) {
    e.preventDefault();
    let distro_name = $('#custom_name_add').val();
    let this_list_emp_id = $('#list_emp_id_add').val();
    let list_emp_id = filterItems('emp_', this_list_emp_id);
    let list_group_emp_id = filterItems('dis_', this_list_emp_id);
    let comma_list_emp_id = ','+list_emp_id.toString();
    let comma_list_group_emp_id = ','+list_group_emp_id.toString();
    let final_list_emp_id = comma_list_emp_id.split(',emp_');
    let final_list_group_emp_id = comma_list_group_emp_id.split(',dis_');
    final_list_emp_id.shift();
    final_list_group_emp_id.shift();
     $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_custom_group_add",
        dataType: "JSON",
        data: {
            distro_name,
            list_emp_id: final_list_emp_id.toString(),
            list_group_emp_id: final_list_group_emp_id.toString(),
        },
        success: function(data) {
            $('#custom_name_add').val("");
            $('#list_emp_id_add').selectpicker("val", "");
            $('#modal_add').modal('hide');
            swal('Success!', 'Your file has been saved.', 'success');
            $('#offenseDatatable').mDatatable('reload');
        },
        error: function(data) {
            swal('Error!', 'Custom Group Name already exist!', 'error');
        }
    });
});

$('.page_here').on('click', '.custom_group_edit', function() {
    $('#modal_update').modal('show');
    var distro_id = $(this).data('distro_id');
    var distro_name = $(this).data('distro_name');
    var list_emp_id = $(this).data('list_emp_id')+'';
    var list_group_emp_id = $(this).data('list_group_emp_id')+'';
    $('#distro_id_edit').val(distro_id);
    $('#distro_name_edit').val(distro_name);
    $('#list_acc_id_edit').selectpicker('val', list_emp_id.split(","));
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_employee',
        async: false,
        data: {
            list_emp_id: list_emp_id.toString(),
            distro_id: list_group_emp_id.toString(),
        },
        dataType: 'json',
        success: function(data) {
            let html = '';
            let html2 = '';
            let checked = '';
            let final_html = '';
            data.emp.forEach(function(value){
                checked = (value.checked == 1) ? 'selected' : '';
                html += '<option '+checked+' value="emp_'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span>">'+value.lname+', '+value.fname+' '+value.mname+'</option>'
            })
            data.dis.forEach(function(value){
                if(distro_id != value.distro_id){
                    checked_dis = (value.checked == 1) ? 'selected' : '';
                    // let list_id = (value.list_emp_id) ? value.list_emp_id.split(',') : '';
                    html2 += '<option '+checked_dis+' value="dis_'+value.distro_id+'">'+value.distro_name+'</option>';
                }
            })
            final_html += '<optgroup label="Distro Group">'+html2+'</optgroup>'+
                          // '<optgroup label="Account List">'+html3+'</optgroup>'+
                          '<optgroup label="Employee">'+html+'</optgroup';
            $('#list_acc_id_edit').html(final_html);
            $('#list_acc_id_edit').selectpicker('refresh');
        }
    });
});

$('.page_here').on('click', '.custom_group_view', function() {
    $('#modal_view').modal('show');
    var distro_id = $(this).data('distro_id');
    var list_emp_id = $(this).data('list_emp_id')+'';
    var list_group_emp_id = $(this).data('list_group_emp_id')+'';
    if (list_emp_id != null || list_group_emp_id != null) {
        $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_employee',
        async: false,
        data: {
            list_emp_id: list_emp_id.toString(),
            distro_id: list_group_emp_id.toString(),
        },
        dataType: 'json',
        success: function(data) {
            let html = '';
            let html2 = '';
            let checked = '';
            let final_html = '';
            data.emp.forEach(function(value){
                // checked = (value.checked == 1) ? 'selected' : '';
                if(value.checked == 1) {
                    html += 
                    '<li style="margin: 10px 0;" class="faqs">\
                    <div class="m--align-center">\
                    <div class="m-card-user m-card-user--skin-dark" style="padding: 5px;">\
                    <div class="m-card-user__pic">\
                    <img src="'+baseUrl+'/'+value.pic+'" onerror="noImage('+value.uid+')" class="m--img-rounded m--marginless header-userpic'+value.uid+'" style="border: 1px solid #afabab;" />\
                    </div>\
                    <div class="m-card-user__details">\
                    <span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit; color: #333!important; font-weight: bold;">'+value.lname+', '+value.fname+' '+value.mname+'</span>\
                    <span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: smaller;">'+value.acc_name+'</span>\
                    </div>\
                    </div>\
                    </div>\
                    </li>';
                }
            })
            data.dis.forEach(function(value){
                if(distro_id != value.distro_id){
                    // checked_dis = (value.checked == 1) ? 'selected' : '';
                    // let list_id = (value.list_emp_id) ? value.list_emp_id.split(',') : '';
                    if(value.checked == 1) {
                        html += 
                        '<li style="margin: 10px 0;" class="faqs">\
                        <div class="m--align-center">\
                        <div class="m-card-user m-card-user--skin-dark" style="padding: 5px;">\
                        <div class="m-card-user__pic">\
                        <img src="'+baseUrl+'/'+value.pic+'" onerror="noImage('+value.uid+')" class="m--img-rounded m--marginless header-userpic'+value.uid+'" style="border: 1px solid #afabab;" />\
                        </div>\
                        <div class="m-card-user__details">\
                        <span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit; color: #333!important; font-weight: bold;">'+value.distro_name+'</span>\
                        <span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: smaller;">Distro Group</span>\
                        </div>\
                        </div>\
                        </div>\
                        </li>';
                    }
                }
            })
            // $('#number_of_employee').html('<small class="text-muted m--font-primary">Showing '+count+' records</small>');
            $('#modalBody1').html('<ul class="view_custom" style="list-style-type: none; padding: 0;">'+html+'</ul>');
        }
    });
    } else {
        $('#modalBody1').html("No Data Available");
    }
});

$('.addNewButton').click(function() {
    $('#modal_add').modal('show');
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_employee',
        async: false,
        dataType: 'json',
        success: function(data) {
            let html = '';
            let html2 = '';
            let final_html = '';
            data.emp.forEach(function(value){
                let lname = (value.lname) ? capitalizeTheFirstLetterOfEachWord(value.lname) : '';
                let fname = (value.fname) ? capitalizeTheFirstLetterOfEachWord(value.fname) : '';
                let mname = (value.mname) ? capitalizeTheFirstLetterOfEachWord(value.mname) : '';
                html += '<option value="emp_'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span>">'+lname+', '+fname+' '+mname+'</option>'
            })
            data.dis.forEach(function(value){
                checked_dis = (value.checked == 1) ? 'selected' : '';
                // let list_id = (value.list_emp_id) ? value.list_emp_id.split(',') : '';
                html2 += '<option '+checked_dis+' value="dis_'+value.distro_id+'">'+value.distro_name+'</option>';
            })
            final_html += '<optgroup label="Distro Group">'+html2+'</optgroup>'+
                          // '<optgroup label="Account List">'+html3+'</optgroup>'+
                          '<optgroup label="Employee">'+html+'</optgroup';
            $('#list_emp_id_add').html(final_html);
            $('#list_emp_id_add').selectpicker('refresh');
        }
    });
});

$('#updateDistro').on('submit', function(e) {
    e.preventDefault();
    var distro_id = $('#distro_id_edit').val();
    var distro_name = $('#distro_name_edit').val();
    var this_list_emp_id = $('#list_acc_id_edit').val();
    let list_emp_id = filterItems('emp_', this_list_emp_id);
    let list_group_emp_id = filterItems('dis_', this_list_emp_id);
    let comma_list_emp_id = ','+list_emp_id.toString();
    let comma_list_group_emp_id = ','+list_group_emp_id.toString();
    let final_list_emp_id = comma_list_emp_id.split(',emp_');
    let final_list_group_emp_id = comma_list_group_emp_id.split(',dis_');
    final_list_emp_id.shift();
    final_list_group_emp_id.shift();
    $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_custom_group_update",
        dataType: "JSON",
        data: {
            distro_id,
            distro_name,
            list_emp_id: final_list_emp_id.toString(),
            list_group_emp_id: final_list_group_emp_id.toString(),

        },
        success: function(data) {
            $('#distro_id_edit').val("");
            $('#distro_name_edit').val("");
            $('#list_acc_id_edit').selectpicker("val", "");
            $('#modal_update').modal('hide');
            swal("Success!", "Your file has been updated!", "success");
            $('#offenseDatatable').mDatatable('reload');
        },
        error: function(data) {
            swal('Error!', 'Custom Group Name already exist!', 'error');
        }
    });
    return false;
});

$('.page_here').on('click', '.custom_group_delete', function(e) {
    var distro_id = $(this).data('distro_id');
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        input: 'textarea',
        allowEscapeKey: 'false',
        allowOutsideClick: 'false',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        inputValidator: (value) => {
            return new Promise((resolve) => {
                if (value.trim() !== '') {
                    resolve()
                } else {
                    resolve('Oops! Reason is Required')
                }
            })
        }
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: baseUrl+"/Faqs/faqs_custom_group_delete",
                dataType: "JSON",
                data: {
                    distro_id: distro_id,
                    deleteReason: result.value
                },
                success: function(data) {
                    $('#offenseDatatable').mDatatable('reload');
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
            return false;
        }
    });
});

function search() {
    let input = document.getElementById('searchbar').value
    input = input.toLowerCase();
    let x = document.getElementsByClassName('faqs');
    for (i = 0; i < x.length; i++) {
        if (!x[i].innerHTML.toLowerCase().includes(input)) {
            x[i].style.display = "none";
        } else {
            x[i].style.display = "list-item";
        }
    }
}