$(function(){
	faqsView();
	filter_category();
	access();
	$('.m_selectpicker').selectpicker();
})

function access() {
	$.ajax({
		type: 'post',
		url: baseUrl+'/Faqs/access',
		async: false,
		dataType: 'json',
		success: function(data) {
			console.log(data);
			let html = '';
			if(data){
				html = '<a href="'+baseUrl+'/Faqs/faqs_create_page" class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">Add New <i class="fa fa-plus"></i></a>';
			}
			$('#access').html(html);
		}
	})
}

function filter_category(){
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_category',
        async: false,
        dataType: 'json',
        data: {selected:'cat_only'},
        success: function(data) {
            let html = '<option selected value="">All Category</option>';
            data.forEach(function(value){
                let subtext = '';
                subtext = (value.relationship == 0) ? 'Category' : 'Sub-Category';
                html += '<option data-subtext="'+subtext+'" value="'+value.faqs_category_id+'">'+value.faqs_category_name+'</option>'
            })
            $('#category').html(html);
        }
    });
}

function faqsView() {
	var searchText = $('#searchField').val();
	var category = $('#category').val();
	var visible = $('#visible').val();
	$.ajax({
		type  : 'POST',
		data:{searchText,category,visible},
		url   : baseUrl+'/Faqs/faqs_v2_view',
		success : function(data){
			var html = '';
			var faqsList = '';
			var count = '';
			var test = JSON.parse(data);
			test.forEach(function(value){
				value.faqs.forEach(function(faqs){
					var iconHere = '';
					if(faqs.isDisplay == 0){
						iconHere = 'fa fa-eye'
					} else {
						iconHere = 'fa fa-globe'
					}
					count ++;
					if(count < 6) {
						faqsList += 
						'<li style="width: 400px; nth-of-type(1n+3)">'+
						'<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">'+
						'<i style="color: #183247" class="'+iconHere+'"></i>&nbsp&nbsp<a href="'+baseUrl+'/Faqs/faqs_details/'+faqs.faqs_id+'" style="color: #45A0FD; text-decoration: none;">'+faqs.title+
						'</div>'+
						'</li>';
					}
				})
				if(count > 5){
					faqsList += '<a href="#" data-faqs_category_id="'+value.faqs_category_id+'" data-faqs_category_name="'+value.faqs_category_name+'" class="seemore">See More...</a>';
				}
				if(value.faqs.length ===0){
					html += '';
				} else {
					html += 
					'<div class="col-xl-6">'+
					'<div class="text-truncate" style="margin: 10px 0;">'+
					'<a style="font-size: 16px; font-weight: 600; letter-spacing: 0.2px; color: #183247; width: 10em; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">'+value.faqs_category_name+
					'</a>'+
					'<span style="font-size: 16px; color: #9aa1a6; font-weight: normal;">('+count+')</span>'+
					'</div>'+
					'<ul style="padding: 0; margin: 5px 0 5px 22px; list-style-type: none;">'+
					faqsList+
					'</ul>'+
					'</div>';
				}
				faqsList = '';
				count = '';
			})
			var final_html = '';
			if(html == ''){
				final_html = 'No Data Available';
			} else {
				final_html =
				'<div class="row">'+
				html+
				'</div>';
			}
			$('#faqsView').html(final_html);
		}
	});
}

$('#searchField').keyup(function(){
	var search = $(this).val();
	faqsView();
});

$('#category,#visible').change(function(){
	var id = $(this).val();
	faqsView();
});

$('#faqsView').on('click','.seemore',function(){
	$('#faqsModal1').modal('show');
	var faqs_category_id = $(this).data('faqs_category_id');
	$("#faqs_category_name").text($(this).data('faqs_category_name'));
	$.ajax({
		type : "POST",
		url  : baseUrl+"/Faqs/faqs_get_list",
		dataType : "JSON",
		data : {faqs_category_id:faqs_category_id},
		success: function(data){
			var html = '';
			data.forEach(function(value){
				var iconHere = '';
				if(value.isDisplay == 0){
					iconHere = 'fa fa-eye'
				} else {
					iconHere = 'fa fa-globe'
				}
				html += 
				'<ul style="padding: 0; margin: 5px 0 5px 22px; list-style-type: none;">'+
				'<li class="faqs">'+'&nbsp;'+
				'<i style="color: #183247" class="'+iconHere+'"></i>&nbsp&nbsp<a href="'+baseUrl+'/Faqs/faqs_details/'+value.faqs_id+'" style="color: #45A0FD; text-decoration: none; ">'+value.title+'</a>'+
				'</li>'+
				'</ul>';
			})

			$('#faqsView1').html(html);
		}
	});
});

function search() {
	let input = document.getElementById('searchbar').value
	input=input.toLowerCase();
	let x = document.getElementsByClassName('faqs');

	for (i = 0; i < x.length; i++) { 
		if (!x[i].innerHTML.toLowerCase().includes(input)) {
			x[i].style.display="none";
		}
		else {
			x[i].style.display="list-item";                 
		}
	}
}