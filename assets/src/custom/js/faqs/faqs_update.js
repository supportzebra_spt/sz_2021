$(function(){
  $('.m_selectpicker').selectpicker();
  $('#details').summernote({
    toolbar: [
    ['style', ['style']],
    ['font', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'hr']],
      ['view', ['fullscreen', /*'codeview'*/]],   // remove codeview button 
      ],
      tabsize: 2,
      height: 500,
      callbacks: {
        onImageUpload: function(image) {
          uploadImageGeneral(image[0]);
        },
        onMediaDelete : function(target) {
          deleteImageGeneral(target[0].src);
        },
        onChange: function(e) {
          setTimeout(function(){
            details_update();
          },200);
        }
      }
    });

  function uploadImageGeneral(image) {
    var data = new FormData();
    data.append("image", image);
    $.ajax({
      url: baseUrl+"/Faqs/general_upload_photo",
      cache: false,
      contentType: false,
      processData: false,
      data: data,
      type: "POST",
      beforeSend: function(){
       mApp.block('.m-content', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'success',
          message: 'Please wait...'
      });
    },
      success: function(url) {
        // var image = $('<a target="_blank" href="'+url+'"><img src="'+url+'"/></a>');
        var image = $('<img class="myImg" src="'+url+'" />');
        $('#details').summernote("insertNode", image[0]);
        details_update();
        // $('#details').summernote("insertImage", url);

      mApp.unblock('.m-content');
      },
      error: function(data) {

      mApp.unblock('.m-content');
        console.log(data);
      }
    });
  }

  function deleteImageGeneral(src) {
    $.ajax({
      data: {src : src},
      type: "POST",
      url: baseUrl+"/Faqs/delete_image",
      cache: false,
      success: function(response) {
        console.log(response);
        details_update();
      }
    });
  }
  faqsGeneralView();
});

function capitalizeTheFirstLetterOfEachWord(words) {
  var separateWord = words.toLowerCase().split(' ');
  for (var i = 0; i < separateWord.length; i++) {
    separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
    separateWord[i].substring(1);
  }
  return separateWord.join(' ');
}

function faqsGeneralView() {
  $.ajax({
    type : "POST",
    url  : baseUrl+"/Faqs/get_faqs",
    dataType : "JSON",
    data : {faqs_id},
    success: function(data){
      $("#edit_faqs_id").val(data.faqs_id);
      $("#edit_title").val(data.title);
      $("#details").summernote('code', data.details);
      let check = (data.isDisplay == 1) ? 'checked' : '';
      var visible = 
      '<span class="m-switch m-switch--outline m-switch--icon m-switch--accent">'+
      '<label>'+
      '<input data-faqs_id="'+data.faqs_id+'" class="get_value" '+check+' type="checkbox" name="">'+
      '<span></span>'+
      '</label>'+
      '</span>';
      $("#isVisible").html(visible);
      select_category(data.faqs_category_id);
      select_list_access(data.list_access);
      select_list_emp_id(data.list_emp_id);
      $('.get_value').click(function() {
        let isDisplay =  ($('.get_value').is(":checked")) ? 1 : 0;
        let faqs_id = $(this).data('faqs_id');
        is_publish(isDisplay, faqs_id);
      });
    }
  });
}

function select_category(category_id) {
  $.ajax({
    type: 'post',
    url: baseUrl+'/Faqs/list_of_category',
    async: false,
    dataType: 'json',
    data:{category_id, selected:'cat_only'},
    success: function(data) {
      let html = '';
      data.forEach(function(value){
        let subtext = (value.relationship == 0) ? 'Category' : 'Sub-Category';
        let selected = (value.selected == 1) ? 'selected' : '';
        html += '<option '+selected+' data-subtext="'+subtext+'" value="'+value.faqs_category_id+'">'+value.faqs_category_name+'</option>';
      })
      $('#edit_category_id').html(html);
      $('#edit_category_id').selectpicker('refresh');
    }
  }); 
}

function select_list_access(list_access) {
  $.ajax({
    type: 'post',
    url: baseUrl+'/Faqs/list_of_employee',
    async: false,
    dataType: 'json',
    data:{list_emp_id:list_access},
    success: function(data) {
      let html = '';
      let dis_name = '';
      let checked = '';
      data.emp.forEach(function(value){
        checked = (value.checked == 1) ? 'selected' : '';
        dis_name = (value.distro_name) ? value.distro_name : '';
        let lname = (value.lname) ? capitalizeTheFirstLetterOfEachWord(value.lname) : '';
        let fname = (value.fname) ? capitalizeTheFirstLetterOfEachWord(value.fname) : '';
        let mname = (value.mname) ? capitalizeTheFirstLetterOfEachWord(value.mname) : '';
        html += '<option '+checked+' value="'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span> <br><span class=m--font-danger>'+dis_name+'</span>">'+lname+', '+fname+' '+mname+'</option>'
      })
      $('#edit_list_access').html(html);
      $('#edit_list_access').selectpicker('refresh');
    }
  });     
}

function select_list_emp_id(list_emp_id) {
  $.ajax({
    type: 'post',
    url: baseUrl+'/Faqs/list_of_employee',
    async: false,
    dataType: 'json',
    data:{list_emp_id},
    success: function(data) {
      let html = '';
      let dis_name = '';
      let checked = '';
      data.emp.forEach(function(value){
        checked = (value.checked == 1) ? 'selected' : '';
        dis_name = (value.distro_name) ? value.distro_name : '';
        let lname = (value.lname) ? capitalizeTheFirstLetterOfEachWord(value.lname) : '';
        let fname = (value.fname) ? capitalizeTheFirstLetterOfEachWord(value.fname) : '';
        let mname = (value.mname) ? capitalizeTheFirstLetterOfEachWord(value.mname) : '';
        html += '<option '+checked+' value="'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span> <br><span class=m--font-danger>'+dis_name+'</span>">'+lname+', '+fname+' '+mname+'</option>'
      })
      $('#edit_list_emp_id').html(html);
      $('#edit_list_emp_id').selectpicker('refresh');
    }
  });     
}

$('.view_account').click(function(){
  let faqs_id = $(this).data('faqs_id');
  $.ajax({
    type  : 'POST',
    url   : baseUrl+'/Faqs/isEditor',
    async : false,
    dataType : 'json',
    data : {faqs_id},
    success : function(data){
      let html = '';
      let who_can_view = '';
      let list_emp_id = [];
      let list_access = [];
      let list_group_access = '';
      let list_group_emp_id = '';
      data.forEach(function(value){
        list_group_access = value.list_group_access;
        list_group_emp_id = value.list_group_emp_id;
        let faqs_list_access = (value.faqs_list_access) ? value.faqs_list_access.split(",") : '';
        let faqs_list_emp_id = (value.faqs_list_emp_id) ? value.faqs_list_emp_id.split(",") : '';
        let sub_list_access = (value.sub_list_access) ? value.sub_list_access.split(",") : '';
        let sub_list_emp_id = (value.sub_list_emp_id) ? value.sub_list_emp_id.split(",") : '';
        let cat_list_access = (value.cat_list_access) ? value.cat_list_access.split(",") : '';
        let cat_list_emp_id = (value.cat_list_emp_id) ? value.cat_list_emp_id.split(",") : '';
        list_access.push(value.faqs_emp_id);
        if(!faqs_list_emp_id.includes(value.sub_emp_id)){
          list_access.push(value.sub_emp_id);
        }
        if(!faqs_list_emp_id.includes(value.cat_emp_id) && !sub_list_emp_id.includes(value.cat_emp_id)){
          list_access.push(value.cat_emp_id);
        }
        for (let i = 0; i<faqs_list_access.length; i++){
          list_access.push(faqs_list_access[i]);
        }
        for (let i = 0; i<faqs_list_emp_id.length; i++){
          list_emp_id.push(faqs_list_emp_id[i]);
        }
        for (let i = 0; i<sub_list_access.length; i++){
          if(faqs_list_emp_id.includes(sub_list_access[i])){
            list_emp_id.push(sub_list_access[i]);
          } else{
            list_access.push(sub_list_access[i]);
          }
        }
        for (let i = 0; i<sub_list_emp_id.length; i++){
          if(!faqs_list_access.includes(sub_list_emp_id[i])){
            list_emp_id.push(sub_list_emp_id[i]);
          }
        }
        for (let i = 0; i<cat_list_emp_id.length; i++){
          if(!sub_list_access.includes(cat_list_emp_id[i]) || !faqs_list_access.includes(cat_list_emp_id[i])){
            list_emp_id.push(cat_list_emp_id[i]);
          }
        }
        for (let i = 0; i<cat_list_access.length; i++){
          if(sub_list_emp_id.includes(cat_list_access[i]) || faqs_list_emp_id.includes(cat_list_access[i])){
            list_emp_id.push(cat_list_access[i]);
          } else {
            list_access.push(cat_list_access[i]);
          }
        }
      })
      modalView(removeDuplicates(list_emp_id),removeDuplicates(list_access),list_group_access,list_group_emp_id);
    }
  })
});

function removeDuplicates(data){
  return data.filter((value, index) => data.indexOf(value) === index);
}

function modalView(list_emp_id,list_access,list_group_access,list_group_emp_id){

  $('#faqsModal1').modal('show');
  var list_emp_id = list_emp_id+'';
  var list_access = list_access+'';
  list_emp_id = list_emp_id.split(',');
  list_access = list_access.split(',');
  var list_group_emp_id = list_group_emp_id+'';
    var list_group_access = list_group_access+'';
    list_group_emp_id = list_group_emp_id.split(',');
    list_group_access = list_group_access.split(',');
  $.ajax({
    type  : 'POST',
    url   : baseUrl+'/Faqs/checkEmployee',
    async : false,
    dataType : 'json',
    beforeSend: function(){
       mApp.block('#faqsModal1 .modal-body', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'success',
          message: 'Please wait...'
      });
    },
    success : function(data){
      var html = '';
      var account = '';
      var gicheck = '';
      var str = '';
      var html2 = '';
      var count = 0;
      let set_pic = '';
      data.dis.forEach(function(value) {
                var n = list_group_emp_id.includes(value.distro_id);
                var m = list_group_access.includes(value.distro_id);
                let stats = (m) ? 'Editor' : 'Viewer';
                let color = (m) ? 'm-badge--success' : 'm-badge--info';
                if(n || m){
                    html2 += '\
                    <li style="margin: 10px 0;" class="faqs">\
                    <div class="m--align-center">\
                    <div class="m-card-user m-card-user--skin-dark";padding: 5px;">\
                    <div class="m-card-user__pic">\
                    <img src="'+baseUrl+'/'+value.pic+'" onerror="noImage('+value.uid+')" class="m--img-rounded m--marginless header-userpic'+value.uid+'" style="border: 1px solid #afabab;" />\
                    </div>\
                    <div class="m-card-user__details">\
                    <span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit; color: #333!important; font-weight: bold;">'+value.distro_name+' <span class="m-badge '+color+' m-badge--wide " style="font-weight: normal;">'+stats+'</span></span>\
                    <span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: smaller;">Distro Group</span>\
                    </div>\
                    </div>\
                    </div>\
                    </li>';
                }
            })
      data.emp.forEach(function(value) {
        if(value.emp_id == 184){
                    set_pic = value.pic;
                }
        var n = list_emp_id.includes(value.emp_id);
        var m = list_access.includes(value.emp_id);
        let stats = (m) ? 'Editor' : 'Viewer';
        let color = (m) ? 'm-badge--success' : 'm-badge--info';
        if(n || m){
          count = count + 1;
          html2 += '\
          <li style="margin: 10px 0;" class="faqs">\
          <div class="m--align-center">\
          <div class="m-card-user m-card-user--skin-dark";padding: 5px;">\
          <div class="m-card-user__pic">\
          <img src="'+baseUrl+'/'+value.pic+'" onerror="noImage('+value.uid+')" class="m--img-rounded m--marginless header-userpic'+value.uid+'" style="border: 1px solid #afabab;" />\
          </div>\
          <div class="m-card-user__details">\
          <span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit; color: #333!important; font-weight: bold;">'+value.lname+', '+value.fname+' '+value.mname+' <span class="m-badge '+color+' m-badge--wide " style="font-weight: normal;">'+stats+'</span></span>\
          <span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: smaller;">'+value.acc_name+'</span>\
          </div>\
          </div>\
          </div>\
          </li>';
        }
      })
      
      $('#number_of_employee').html('<small class="text-muted m--font-primary">Show '+count+' records</small>');
      $('#modalBody1').html('<ul class="view_category" style="list-style-type: none; padding: 0">'+html2+'</ul>');
      mApp.unblock('#faqsModal1 .modal-body');
    },
    error:function(err){
      console.log("ERROR:",err);
      mApp.unblock('#faqsModal1 .modal-body');
    }
  });
}

function details_update() {
  let faqs_id = $('#edit_faqs_id').val();
  let details = $('#details').val();
  $.ajax({
    type: "POST",
    url: baseUrl+"/Faqs/faqs_update",
    dataType: "JSON",
    data: {
      faqs_id,
      details,
      update:'details',
    },
    success: function(data) {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };
      toastr.success("Details updated!");
    },
    error:function(error){
      console.log("ERRORR:,",error);
    }
  });
  return false;
}

function is_publish(data,faqs_id) {
  $.ajax({
    type: "POST",
    url: baseUrl+"/Faqs/faqs_update",
    dataType: "JSON",
    data: {
      faqs_id,
      data,
      update:'is_publish',
    },
    success: function(data) {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };
      toastr.success("Status updated!");
    }
  });
  return false;
}
var typingTimer;
var doneTypingInterval = 2000;

function doneTyping () {
  let faqs_id = $('#edit_faqs_id').val();
  let title = $('#edit_title').val();
  if(title != ''){
    $.ajax({
      type: "POST",
      url: baseUrl+"/Faqs/faqs_update",
      dataType: "JSON",
      data: {
        faqs_id,
        title,
        update:'title',
      },
      success: function(data) {
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": true,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        };
        toastr.success("Title updated!");
      }
    });
    return false;
  } else {
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };
    toastr.error("The title must be filled!");
  }
}
$('#updateSubmitAccount').on('keydown','#edit_title',function(){
clearTimeout(typingTimer);
});
$('#updateSubmitAccount').on('keyup','#edit_title',function(){
  if (typingTimer) clearTimeout(typingTimer); 
  typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

$('#updateSubmitAccount').on('change', '#edit_category_id', function(){
  let faqs_id = $('#edit_faqs_id').val();
  let category = $(this).val();
  console.log(category);
  $.ajax({
    type: "POST",
    url: baseUrl+"/Faqs/faqs_update",
    dataType: "JSON",
    data: {
      faqs_id,
      category,
      update:'category',
    },
    success: function(data) {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };
      toastr.success("Category updated!");
    }
  });
  return false;
})

$('#updateSubmitAccount').on('change', '#edit_list_access', function(){
  let faqs_id = $('#edit_faqs_id').val();
  let list_access = $(this).val();
  list_access = (list_access != null) ? list_access.toString() : '';
  $.ajax({
    type: "POST",
    url: baseUrl+"/Faqs/faqs_update",
    dataType: "JSON",
    data: {
      faqs_id,
      list_access,
      update:'list_access',
    },
    success: function(data) {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };
      toastr.success("Editor updated!");
    }
  });
  return false;
})

$('#updateSubmitAccount').on('change', '#edit_list_emp_id', function(){
  let faqs_id = $('#edit_faqs_id').val();
  let list_emp_id = $(this).val();
  list_emp_id = (list_emp_id != null) ? list_emp_id.toString() : '';
    $.ajax({
      type: "POST",
      url: baseUrl+"/Faqs/faqs_update",
      dataType: "JSON",
      data: {
        faqs_id,
        list_emp_id,
        update:'list_emp_id',
      },
      success: function(data) {
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": true,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        };
        toastr.success("Editor updated!");
      }

    });
    return false;
})

$('#updateSubmitAccount').on('keyup','#details',function(){
  let faqs_id = $('#edit_faqs_id').val();
  let details = $('#details').val();
  console.log(details);
  // $.ajax({
  //   type: "POST",
  //   url: baseUrl+"/Faqs/faqs_update",
  //   dataType: "JSON",
  //   data: {
  //     faqs_id,
  //     details,
  //     update:'details',
  //   },
  //   success: function(data) {
  //     toastr.options = {
  //       "closeButton": true,
  //       "debug": false,
  //       "newestOnTop": true,
  //       "progressBar": false,
  //       "positionClass": "toast-top-right",
  //       "preventDuplicates": true,
  //       "onclick": null,
  //       "showDuration": "300",
  //       "hideDuration": "1000",
  //       "timeOut": "5000",
  //       "extendedTimeOut": "1000",
  //       "showEasing": "swing",
  //       "hideEasing": "linear",
  //       "showMethod": "fadeIn",
  //       "hideMethod": "fadeOut"
  //     };
  //     toastr.success("Details updated!");
  //   }
  // });
  // return false;
});

function submitData(){
  let isDisplay =  ($('.get_value').is(":checked")) ? 1 : 0;
  let title = $('#edit_title').val();
  let faqs_category_id = $('#edit_category_id').val();
  let details = $('#details').val();
  let list_access = $('#edit_list_access').val();
  let list_emp_id = $('#edit_list_emp_id').val();
  $.ajax({
    type : "POST",
    url  : baseUrl+"/Faqs/faqs_update",
    dataType : "JSON",
    data : {
      faqs_id,
      title,
      faqs_category_id,
      details,
      isDisplay,
      list_emp_id:list_emp_id.toString(),
      list_access:list_access.toString(),
    },
    success: function(data){
      window.location.href = baseUrl+"/Faqs/faqs_details_server_page/"+faqs_id;
    }
  }); 
  return false;
}


$('.group_editor').click(function() {
  $('#faqsModal1').modal('show');
  let list_access = $('#edit_list_access').val();
  console.log(list_access);
})

$('.group_viewer').click(function() {
  $('#faqsModal2').modal('show');
})

function capitalizeTheFirstLetterOfEachWord(words) {
 var separateWord = words.toLowerCase().split(' ');
 for (var i = 0; i < separateWord.length; i++) {
  separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
  separateWord[i].substring(1);
}
return separateWord.join(' ');
}

function search() {
  let input = document.getElementById('searchbar').value
  input=input.toLowerCase();
  let x = document.getElementsByClassName('faqs');
  
  for (i = 0; i < x.length; i++) { 
    if (!x[i].innerHTML.toLowerCase().includes(input)) {
      x[i].style.display="none";
    }
    else {
      x[i].style.display="list-item";                 
    }
  }
}

