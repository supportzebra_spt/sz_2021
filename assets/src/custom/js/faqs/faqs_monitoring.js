var history_logs = function() {
    var offense = function(searchVal = "", start_date, end_date) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl+"/Faqs/faqs_monitoring_data",
                        params: {
                            query: {
                                searchField: searchVal,
                                searchType: $('#searchType').val(),
                                start: start_date,
                                end: end_date,
                            },
                        },
                        map: function(raw) {
                            console.log(raw);
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '', 
                scroll: true, 
                height: 550, 
                footer: false 
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50] 
                    },
                }
            },
            search: {
                input: $('#searchField'),
            },
            columns: 
            [{
                field: "faqs_id",
                title: "#",
                width: 70,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) 
                {
                    index = index + 1;
                    return index;
                },
            }, 
            {
                field: 'title',
                title: 'Title',
                width: 300,
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
            },
            {
                field: 'faqs_category_name',
                title: 'Category',
                width: 120,
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
            },
            {
                field: 'isDisplay',
                title: 'Status',
                width: 70,
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row) {
                    var status = {
                        0: {'title': 'Draft', 'class': 'm-badge--brand'},
                        1: {'title': 'Publish', 'class': ' m-badge--info'},
                        2: {'title': 'Archive', 'class': ' m-badge--metal'}
                    };
                    return '<span class="m-badge ' + status[row.isDisplay].class + ' m-badge--wide">' + status[row.isDisplay].title + '</span>';
                }
            },
            {
                field: 'timeUpdate',
                title: 'Date Created',
                width: 100,
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function(row, index, datatable) 
                {
                    var updatedTime = moment(row.timeUpdate).format('LL');
                    return updatedTime;
                },
            },
            {
                field: "Actions",
                width: 70,
                title: "Actions",
                sortable: false,
                textAlign: 'center',
                overflow: 'visible',
                template: function(row, index, datatable) 
                {
                    return '\
                    <span data-skin="dark" data-container="body" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="View Data">\
                        <a target="_blank" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" href="'+baseUrl+'/Faqs/faqs_details/'+row.faqs_id+'">\
                            <i class="la la-eye"></i>\
                        </a>\
                    </span>';
                }
            }],
        };
            var datatable = $('#monitoring').mDatatable(options);
    };
    return {
        init: function(searchVal, start_date, end_date) {
            offense(searchVal, start_date, end_date);
        }
    };
}();

function initOffenses() {
    let search_date_val = $('#searchDate').val().split(' - ');
    $('#monitoring').mDatatable('destroy');
    history_logs.init('',  moment(search_date_val[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(search_date_val[1], 'MM/DD/YYYY').format('Y-MM-DD'));
}

$(function() {
    initOffenses();
    $('.m_selectpicker').selectpicker();
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end =  moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#searchDate').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function(start, end, label) {
            $('#searchDate').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initOffenses();
        });
    });
    
});
$('#searchType,#searchDate').change(function(){
    initOffenses();
});
