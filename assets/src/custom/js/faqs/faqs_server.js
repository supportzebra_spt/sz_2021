var offenseDatatable = function () {
    var offense = function (searchVal = "") {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl+"/Faqs/faqs_server_data",
                        params: {
                            query: {
                                searchField: searchVal,
                                searchCategory: $('#searchCategory').val(),
                                searchStatus: $('#searchStatus').val(),
                            },
                        },
                        map: function (raw) {
                            console.log(raw);
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '', 
                scroll: true, 
                height: 550, 
                footer: false 
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#searchField'),
            },
            columns: 
            [{
                field: "faqs_id",
                title: "#",
                width: 50,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    index = index+1;
                    return index;
                },
            }, 
            {
                field: 'faqs_category_name',
                title: 'Category Name',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
            },  
            {
                field: 'title',
                width: 320,
                title: 'Title',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
            },  
            {
                field: 'isDisplay',
                width: 70,
                title: 'Status',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row) {
                    var status = {
                        0: {'title': 'Draft', 'class': 'm-badge--brand'},
                        1: {'title': 'Publish', 'class': ' m-badge--info'},
                    };
                    return '<span class="m-badge ' + status[row.isDisplay].class + ' m-badge--wide">' + status[row.isDisplay].title + '</span>';
                }
            },  
            {
                field: "Actions",
                width: 145,
                title: "Actions",
                textAlign: 'center',
                sortable: false,
                overflow: 'visible',
                template: function (row, index, datatable) {
                    return '\
                    <span data-skin="dark" data-container="body" title="" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="View FAQ">\
                    <a target="_blank" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" href="'+baseUrl+'/Faqs/faqs_details/'+row.faqs_id+'" >\
                    <i class="la la-eye"></i>\
                    </a>\
                    </span>\
                    <span data-skin="dark" data-container="body" title="" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="List of Access">\
                    <a class="faqs_modal m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-faqs_id="'+row.faqs_id+'" data-list_emp_id="'+row.list_emp_id+'" data-list_access="'+row.list_access+'">\
                    <i class="la la-group"></i>\
                    </a>\
                    </span>\
                    <span data-skin="dark" data-container="body" title="" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="Update FAQ">\
                    <a class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" href="'+baseUrl+'/Faqs/faqs_edit_page/'+row.faqs_id+'">\
                    <i class="la la-edit"></i>\
                    </a>\
                    </span>\
                    <span data-skin="dark" data-container="body" title="" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="Remove FAQ">\
                    <a class="faqs_delete m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-faqs_id="'+row.faqs_id+'">\
                    <i class="la la-trash"></i>\
                    </a>\
                    </span>\
                    ';
                }
            }],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
        init: function (searchVal, ) {
            offense(searchVal);
        }
    };
}();

function initOffenses(){
    $('#offenseDatatable').mDatatable('destroy');
    offenseDatatable.init('');
}

function filter_category(){
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_category',
        async: false,
        dataType: 'json',
        data: {selected:'filter'},
        success: function(data) {
            let html = '<option selected value="">All Category</option>';
            data.forEach(function(value){
                let subtext = '';
                subtext = (value.relationship == 0) ? 'Category' : 'Sub-Category';
                html += '<option data-subtext="'+subtext+'" value="'+value.faqs_category_id+'">'+value.faqs_category_name+'</option>'
            })
            $('#searchCategory').html(html);
        }
    });
}

$(function(){
    initOffenses();
    filter_category();
    $('.m_selectpicker').selectpicker();
})

$('#searchCategory,#searchStatus').change(function(){
    initOffenses();
});

$('.page_here').on('click','.faqs_modal',function(){
    let faqs_id = $(this).data('faqs_id');
    $.ajax({
        type  : 'POST',
        url   : baseUrl+'/Faqs/isEditor',
        async : false,
        dataType : 'json',
        data : {faqs_id},
        success : function(data){
            let html = '';
            let who_can_view = '';
            let list_emp_id = [];
            let list_access = [];
            let list_group_access = '';
            let list_group_emp_id = '';
            data.forEach(function(value){
                list_group_access = value.list_group_access;
                list_group_emp_id = value.list_group_emp_id;
                let faqs_list_access = (value.faqs_list_access) ? value.faqs_list_access.split(",") : '';
                let faqs_list_emp_id = (value.faqs_list_emp_id) ? value.faqs_list_emp_id.split(",") : '';
                let sub_list_access = (value.sub_list_access) ? value.sub_list_access.split(",") : '';
                let sub_list_emp_id = (value.sub_list_emp_id) ? value.sub_list_emp_id.split(",") : '';
                let cat_list_access = (value.cat_list_access) ? value.cat_list_access.split(",") : '';
                let cat_list_emp_id = (value.cat_list_emp_id) ? value.cat_list_emp_id.split(",") : '';
                list_access.push(value.faqs_emp_id);
                if(!faqs_list_emp_id.includes(value.sub_emp_id)){
                    list_access.push(value.sub_emp_id);
                }
                if(!faqs_list_emp_id.includes(value.cat_emp_id) && !sub_list_emp_id.includes(value.cat_emp_id)){
                    list_access.push(value.cat_emp_id);
                }
                for (let i = 0; i<faqs_list_access.length; i++){
                    list_access.push(faqs_list_access[i]);
                }
                for (let i = 0; i<faqs_list_emp_id.length; i++){
                    list_emp_id.push(faqs_list_emp_id[i]);
                }
                for (let i = 0; i<sub_list_access.length; i++){
                    if(faqs_list_emp_id.includes(sub_list_access[i])){
                        list_emp_id.push(sub_list_access[i]);
                    } else{
                        list_access.push(sub_list_access[i]);
                    }
                }
                for (let i = 0; i<sub_list_emp_id.length; i++){
                    if(!faqs_list_access.includes(sub_list_emp_id[i])){
                        list_emp_id.push(sub_list_emp_id[i]);
                    }
                }
                for (let i = 0; i<cat_list_emp_id.length; i++){
                    if(!sub_list_access.includes(cat_list_emp_id[i]) || !faqs_list_access.includes(cat_list_emp_id[i])){
                        list_emp_id.push(cat_list_emp_id[i]);
                    }
                }
                for (let i = 0; i<cat_list_access.length; i++){
                    if(sub_list_emp_id.includes(cat_list_access[i]) || faqs_list_emp_id.includes(cat_list_access[i])){
                        list_emp_id.push(cat_list_access[i]);
                    } else {
                        list_access.push(cat_list_access[i]);
                    }
                }
            })
            modalView(removeDuplicates(list_emp_id),removeDuplicates(list_access),list_group_access,list_group_emp_id);
        }
    })
});

function removeDuplicates(data){
    return data.filter((value, index) => data.indexOf(value) === index);
}

function modalView(list_emp_id,list_access,list_group_access,list_group_emp_id){
    $('#faqsModal1').modal('show');
    var list_emp_id = list_emp_id+'';
    var list_access = list_access+'';
    list_emp_id = list_emp_id.split(',');
    list_access = list_access.split(',');
    var list_group_emp_id = list_group_emp_id+'';
    var list_group_access = list_group_access+'';
    list_group_emp_id = list_group_emp_id.split(',');
    list_group_access = list_group_access.split(',');
    $.ajax({
        type  : 'POST',
        url   : baseUrl+'/Faqs/checkEmployee',
        async : false,
        dataType : 'json',
        success : function(data){
            var html = '';
            var account = '';
            var gicheck = '';
            var str = '';
            var html2 = '';
            var html3 = '';
            var count = 0;
            let set_pic = '';
            data.emp.forEach(function(value) {
                if(value.emp_id == 184){
                    set_pic = value.pic;
                }
                var n = list_emp_id.includes(value.emp_id);
                var m = list_access.includes(value.emp_id);
                let stats = (m) ? 'Editor' : 'Viewer';
                let color = (m) ? 'm-badge--success' : 'm-badge--info';
                if(n || m){
                    html2 += '\
                    <li style="margin: 10px 0;" class="faqs">\
                    <div class="m--align-center">\
                    <div class="m-card-user m-card-user--skin-dark";padding: 5px;">\
                    <div class="m-card-user__pic">\
                    <img src="'+baseUrl+'/'+value.pic+'" onerror="noImage('+value.uid+')" class="m--img-rounded m--marginless header-userpic'+value.uid+'" style="border: 1px solid #afabab;" />\
                    </div>\
                    <div class="m-card-user__details">\
                    <span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit; color: #333!important; font-weight: bold;">'+value.lname+', '+value.fname+' '+value.mname+' <span class="m-badge '+color+' m-badge--wide " style="font-weight: normal;">'+stats+'</span></span>\
                    <span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: smaller;">'+value.acc_name+'</span>\
                    </div>\
                    </div>\
                    </div>\
                    </li>';
                }
            })
            data.dis.forEach(function(value) {
                var n = list_group_emp_id.includes(value.distro_id);
                var m = list_group_access.includes(value.distro_id);
                let stats = (m) ? 'Editor' : 'Viewer';
                let color = (m) ? 'm-badge--success' : 'm-badge--info';
                if(n || m){
                    html2 += '\
                    <li style="margin: 10px 0;" class="faqs">\
                    <div class="m--align-center">\
                    <div class="m-card-user m-card-user--skin-dark";padding: 5px;">\
                    <div class="m-card-user__pic">\
                    <img src="'+baseUrl+'/'+value.pic+'" onerror="noImage('+value.uid+')" class="m--img-rounded m--marginless header-userpic'+value.uid+'" style="border: 1px solid #afabab;" />\
                    </div>\
                    <div class="m-card-user__details">\
                    <span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit; color: #333!important; font-weight: bold;">'+value.distro_name+' <span class="m-badge '+color+' m-badge--wide " style="font-weight: normal;">'+stats+'</span></span>\
                    <span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: smaller;">Distro Group</span>\
                    </div>\
                    </div>\
                    </div>\
                    </li>';
                }
            })

            // $('#number_of_employee').html('<small class="text-muted m--font-primary">Show '+count+' records</small>');
            $('#modalBody1').html('<ul class="view_category" style="list-style-type: none; padding: 0">'+html2+'</ul>');
        }
    });
}

$('.page_here').on('click','.faqs_delete',function(e){
    var faqs_id = $(this).data('faqs_id');
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        input: 'textarea',
        allowEscapeKey: 'false',
        allowOutsideClick: 'false',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        inputValidator: (value) => {
            return new Promise((resolve) => {
                if (value.trim() !== '') {
                    resolve()
                } else {
                    resolve('Oops! Reason is Required')
                }
            })
        }
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                type : "POST",
                url  : baseUrl+"/Faqs/faqs_delete",
                dataType : "JSON",
                data : {faqs_id:faqs_id,
                    deleteReason:result.value,
                },
                success: function(data){
                    $('#offenseDatatable').mDatatable('reload');
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                        )
                }
            });
            return false;
            console.log(result.value);
        }
    });
});

function search() {
    let input = document.getElementById('searchbar').value
    input = input.toLowerCase();
    let x = document.getElementsByClassName('faqs');
    for (i = 0; i < x.length; i++) {
        if (!x[i].innerHTML.toLowerCase().includes(input)) {
            x[i].style.display = "none";
        } else {
            x[i].style.display = "list-item";
        }
    }
}