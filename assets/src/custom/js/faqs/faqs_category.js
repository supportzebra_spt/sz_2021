var offenseDatatable = function() {
    var offense = function(searchVal = "") {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl+"/Faqs/faqs_category_data",
                        params: {
                            query: {
                                searchField: searchVal,
                                // searchType: $('#searchType').val(),
                                // searchCategory: $('#searchCategory').val(),
                            },
                        },
                        map: function(raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', 
                class: '', 
                scroll: true, 
                height: 550, 
                footer: false 
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50] 
                    },
                }
            },
            search: {
                input: $('#searchField'),
            },
            columns: 
            [{
                field: "faqs_category_id",
                title: "#",
                width: 70,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    index = index + 1;
                    return index;
                },
            }, 
            {
                field: 'faqs_category_name',
                width: 500,
                title: 'Category Name',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
            },
            {
                field: 'sub_name',
                width: 150,
                title: 'Sub-Category',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    let color = ["brand", "metal", "primary", "success", "info", "warning", "danger", "focus", "accent"];
                    let subname = (row.sub_name) ? row.sub_name.split(",") : '';
                    let badges = '';
                    let count = 0;
                    let color_display = '';
                    for (let i=0; i<subname.length; i++){
                        count ++;
                        let random = Math.floor((Math.random() * color.length));
                        color_display = (random, color[random]);
                        if(count < 4){
                            let name = '';
                            if(subname[i].length > 6){
                                name = subname[i].substring(0, 5)+'...';
                            } else {
                                name = subname[i];
                            }
                            badges += '<span style="margin: 2px" class="m-badge m-badge--'+color_display+' m-badge--wide">'+name+'</span> ';
                        } else {
                            badges = '<span style="margin: 2px" class="m-badge m-badge--'+color_display+' m-badge--wide">See More...</span>';
                        }
                    }
                    let html = '<a style="text-decoration: none;" href="#" class="view_subcategory" data-faqs_category_id="' + row.faqs_category_id + '" data-faqs_category_name="' + row.faqs_category_name + '" data-list_emp_id="' + row.list_emp_id + '" data-list_access="' + row.list_access + '" data-list_group_emp_id="' + row.list_group_emp_id + '" data-list_group_access="' + row.list_group_access + '">\
                    '+badges+'\
                    </a>'
                    return html;
                },
            },
            {
                field: "Actions",
                width: 110,
                title: "Actions",
                sortable: false,
                textAlign: 'center',
                overflow: 'visible',
                template: function(row, index, datatable) {
                    let access = '';
                    let checked = (row.list_access != null) ? row.list_access.includes(emp_id) : 0;
                    if(emp_id == row.emp_id || checked){
                        access = '<span data-skin="dark" data-container="body" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="Add Sub-Category"><a class="add_subcategory m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-faqs_category_id="' + row.faqs_category_id + '" data-faqs_category_name="' + row.faqs_category_name + '" data-list_emp_id="' + row.list_emp_id + '" data-list_access="' + row.list_access + '"><i class="la la-plus-circle"></i></a></span>\
                        <span data-skin="dark" data-container="body" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="FAQ Category"><a class="faqs_edit m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-list_emp_id="' + row.list_emp_id + '" data-faqs_category_id="' + row.faqs_category_id + '" data-faqs_category_name="' + row.faqs_category_name + '" data-relationship="' + row.relationship + '" data-list_access="' + row.list_access + '" data-list_emp_id="' + row.list_emp_id + '" data-list_group_emp_id="' + row.list_group_emp_id + '" data-list_group_access="' + row.list_group_access + '"><i class="la la-file"></i></a></span>\
                        <span data-skin="dark" data-container="body" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="Remove FAQ Category"><a class="deleteCategory m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-faqs_category_id="' + row.faqs_category_id + '" ><i class="la la-trash"></i></a></span>';
                    }
                    return access;
                }
            }],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
        init: function(searchVal) {
            offense(searchVal);
        }
    };
}();

function initOffenses() {
    $('#offenseDatatable').mDatatable('destroy');
    offenseDatatable.init();
}

function capitalizeTheFirstLetterOfEachWord(words) {
   var separateWord = words.toLowerCase().split(' ');
   for (var i = 0; i < separateWord.length; i++) {
      separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
      separateWord[i].substring(1);
  }
  return separateWord.join(' ');
}

function filter_category(){
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_category',
        async: false,
        dataType: 'json',
        success: function(data) {
            let html = '<option selected value="">All Category</option>';
            data.forEach(function(value){
                if(value.relationship == 0){
                    html += '<option value="'+value.faqs_category_id+'">'+value.faqs_category_name+'</option>'
                }
            })
            $('#searchCategory').html(html);
        }
    });
}

function this_list_access(to_display, list_emp_id, distro_id) {
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_employee',
        async: false,
        dataType: 'json',
        data: {
            list_emp_id,distro_id
        },
        success: function(data) {
            let html = '';
            let dis_name = '';
            let checked_emp = '';
            let checked_dis = '';
            let html1 = '';
            let html2 = '';
            let html3 = '';
            let final_html = '';
            data.emp.forEach(function(value){
                if(emp_id != value.emp_id){
                    checked_emp = (value.checked == 1) ? 'selected' : '';
                    // dis_name = (value.distro_name) ? value.distro_name : '';
                    let lname = (value.lname) ? capitalizeTheFirstLetterOfEachWord(value.lname) : '';
                    let fname = (value.fname) ? capitalizeTheFirstLetterOfEachWord(value.fname) : '';
                    let mname = (value.mname) ? capitalizeTheFirstLetterOfEachWord(value.mname) : '';
                    // html += '<option '+checked+' value="'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span> <br><span class=m--font-danger>'+dis_name+'</span>">'+lname+', '+fname+' '+mname+'</option>'
                    html1 += '<option '+checked_emp+' value="emp_'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span>">'+lname+', '+fname+' '+mname+'</option>'

                }
            })
            data.dis.forEach(function(value){
                checked_dis = (value.checked == 1) ? 'selected' : '';
                // let list_id = (value.list_emp_id) ? value.list_emp_id.split(',') : '';
                html2 += '<option '+checked_dis+' value="dis_'+value.distro_id+'">'+value.distro_name+'</option>';
            })
            // data.acc.forEach(function(value){
            //     let list_id = (value.list_emp_id) ? value.list_emp_id.split(',') : '';
            //     html3 += '<option value="'+list_id+'">'+value.acc_name+'</option>';
            // })
            
            final_html += '<optgroup label="Distro Group">'+html2+'</optgroup>'+
                          // '<optgroup label="Account List">'+html3+'</optgroup>'+
                          '<optgroup label="Employee">'+html1+'</optgroup';
            $('#'+to_display+'').html(final_html);
            $('#'+to_display+'').selectpicker('refresh');
        }
    });
}

$(document).ajaxComplete(function() {
    $('[data-toggle="tooltip"]').tooltip({
        "html": true,
    });
});

$(function() {
    initOffenses();
    visible_to();
    filter_category();
    $('.m_selectpicker').selectpicker({});
    $('#category_name_add').maxlength({
        alwaysShow: true,
        threshold: 3,
        warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
        limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
        appendToParent: true,
        separator: ' of ',
        preText: 'You have ',
        postText: ' chars remaining.',
        validate: true
    });
    $('#faqsModal').on('shown.bs.modal', function () {
        $('#m_select2_1_modal').select2({
            placeholder: "Select a state"
        });
    });
    
})

// $('#searchType,#searchCategory').change(function(){
//     initOffenses();
// });

// $('#m_modal_1').on('change', '#relationship_add', function(){
//     let relationship_add = $('#relationship_add').val();
//     let split_relationship_add = relationship_add.split('|');
//     let category_id = split_relationship_add.slice(0,1);
//     let list_emp_id = split_relationship_add.slice(1,2)+'';
//     let list_acc_id = [];
//     let employee_emp_id = [];
//     let array_list_emp_id = [];
//     $('#list_emp_id_add').selectpicker();
//     $.ajax({
//         type: 'post',
//         url: baseUrl+'/Faqs/create_category_list_employee',
//         async: false,
//         data: {
//             category_id: category_id.toString()
//         },
//         dataType: 'json',
//         success: function(data) {
//             let html = '';
//             let dis_name = '';
//             data.forEach(function(value){
//                 dis_name = (value.distro_name != null) ? value.distro_name : "";
//                 html += '<option value="'+value.emp_id+'" data-subtext="<span class=m--font-success>'+value.acc_name+'</span> <span class=m--font-danger>'+dis_name+'</span>">'+value.lname+', '+value.fname+' '+value.mname+'</option>'
//             })
//             $('#list_emp_id_add').html(html);
//             $('#list_emp_id_add').selectpicker('refresh');
//             $('#list_access_add').html(html);
//             $('#list_access_add').selectpicker('refresh');
//         }
//     });
// });

// $('#faqsModal').on('change', '#relationship_edit', function(){
//     let relationship_edit = $('#relationship_edit').val();
//     let category_id_edit = $('#category_id_edit').val();
//     let list_emp_id = $('#visible_edit').val();
//     let list_acc_id = [];
//     let employee_emp_id = [];
//     let array_list_emp_id = [];
//     $('#visible_edit').selectpicker();
//     $.ajax({
//         type: 'post',
//         url: baseUrl+'/Faqs/update_category_list_employee',
//         async: false,
//         data: {
//             category_id: category_id_edit,
//             relationship: relationship_edit,
//         },
//         dataType: 'json',
//         success: function(data) {
//             let html = '';
//             let dis_name = '';
//             let check = '';
//             data.forEach(function(value){
//                 if(value.checked == 1){
//                     check = 'selected';
//                 } else {
//                     check = '';
//                 }
//                 dis_name = (value.distro_name != null) ? value.distro_name : "";
//                 html += '<option '+check+' value="'+value.emp_id+'" data-subtext="<span class=m--font-success>'+value.acc_name+'</span> <span class=m--font-danger>'+dis_name+'</span>">'+value.lname+', '+value.fname+' '+value.mname+'</option>'
//             })
//             $('#visible_edit').html(html);
//             $('#visible_edit').selectpicker('refresh');
//         }
//     });
// });
function filterItems(needle, heystack) {
  var query = needle.toLowerCase();
  return heystack.filter(function(item) {
      return item.toLowerCase().indexOf(query) >= 0;
  })
}

$('#addCategory').on('submit', function(e) {
    e.preventDefault();
    let faqs_category_name = $('#category_name_add').val();
    let relation = 0;
    let this_list_access = $('#list_access_add').val();
    let this_list_emp_id = $('#list_emp_id_add').val();
    let list_access = filterItems('emp_', this_list_access);
    let list_group_access = filterItems('dis_', this_list_access);
    let list_emp_id = filterItems('emp_', this_list_emp_id);
    let list_group_emp_id = filterItems('dis_', this_list_emp_id);
    let comma_list_access = ','+list_access.toString();
    let comma_list_group_access = ','+list_group_access.toString();
    let comma_list_emp_id = ','+list_emp_id.toString();
    let comma_list_group_emp_id = ','+list_group_emp_id.toString();
    let final_list_access = comma_list_access.split(',emp_');
    let final_list_group_access = comma_list_group_access.split(',dis_');
    let final_list_emp_id = comma_list_emp_id.split(',emp_');
    let final_list_group_emp_id = comma_list_group_emp_id.split(',dis_');
    final_list_emp_id.shift();
    final_list_group_emp_id.shift();
    final_list_access.shift();
    final_list_group_access.shift();
    $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_category_add",
        dataType: "JSON",
        data: {
            faqs_category_name,
            relation,
            list_emp_id: final_list_emp_id.toString(),
            list_access: final_list_access.toString(),
            list_group_emp_id: final_list_group_emp_id.toString(),
            list_group_access: final_list_group_access.toString(),
        },
        success: function(data) {
            $('#category_name_add').val("");
            $('#list_access_add').selectpicker('val', '');
            $('#relationship_add').selectpicker('val', '');
            $('#list_emp_id_add').selectpicker('val', '');
            $('#m_modal_1').modal('hide');
            swal('Success!', 'Your file has been saved.', 'success');
            $('#offenseDatatable').mDatatable('reload');
        },
        error: function(data) {
            swal('Error!', 'Category name already exist,', 'error');
        }
    });
    return false;
});

$('#addSubcategory').on('submit', function(e) {
    e.preventDefault();
    let faqs_category_name = $('#sub_category_name_add').val();
    let this_list_access = $('#sub_list_access_add').val();
    let relation = $('#sub_category_id_add').val();
    let this_list_emp_id = $('#sub_list_emp_id_add').val();
    let list_access = filterItems('emp_', this_list_access);
    let list_group_access = filterItems('dis_', this_list_access);
    let list_emp_id = filterItems('emp_', this_list_emp_id);
    let list_group_emp_id = filterItems('dis_', this_list_emp_id);
    let comma_list_access = ','+list_access.toString();
    let comma_list_group_access = ','+list_group_access.toString();
    let comma_list_emp_id = ','+list_emp_id.toString();
    let comma_list_group_emp_id = ','+list_group_emp_id.toString();
    let final_list_access = comma_list_access.split(',emp_');
    let final_list_group_access = comma_list_group_access.split(',dis_');
    let final_list_emp_id = comma_list_emp_id.split(',emp_');
    let final_list_group_emp_id = comma_list_group_emp_id.split(',dis_');
    final_list_emp_id.shift();
    final_list_group_emp_id.shift();
    final_list_access.shift();
    final_list_group_access.shift();
    $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_category_add",
        dataType: "JSON",
        data: {
            faqs_category_name,
            relation,
            list_emp_id: final_list_emp_id.toString(),
            list_access: final_list_access.toString(),
            list_group_emp_id: final_list_group_emp_id.toString(),
            list_group_access: final_list_group_access.toString(),
        },
        success: function(data) {
            $('#sub_category_name_add').val("");
            $('#sub_list_access_add').selectpicker('val', '');
            $('#sub_category_id_add').selectpicker('val', '');
            $('#list_emp_id_add').selectpicker('val', '');
            $('#faqsModal3').modal('hide');
            swal('Success!', 'Your file has been saved.', 'success');
            $('#offenseDatatable').mDatatable('reload');
        },
        error: function(data) {
            swal('Error!', 'Category name already exist,', 'error');
        }
    });
    return false;
});

$('.page_here').on('click', '.faqs_edit', function() {
    $('#faqsModal').modal('show');
    let category_id = $(this).data('faqs_category_id');
    let category_name = $(this).data('faqs_category_name');
    let list_access = $(this).data('list_access');
    let relationship = $(this).data('relationship');
    let list_emp_id = $(this).data('list_emp_id');
    let list_group_access = $(this).data('list_group_access');
    let list_group_emp_id = $(this).data('list_group_emp_id');
    $('#category_name_edit').val(category_name);
    $('#category_id_edit').val(category_id);
    this_list_access('list_access_edit', list_access, list_group_access);
    this_list_access('visible_edit', list_emp_id, list_group_emp_id);
});

$('.page_here').on('click', '.add_subcategory', function() {
    let name = $(this).data("faqs_category_name");
    let id = $(this).data("faqs_category_id");
    $("#sub_name").text("Create Sub-Category ("+name+")");
    $("#sub_category_id_add").val(id);
    $('#faqsModal3').modal('show');
})

// $('.page_here').on('click', '.view_account', function() {
//     var faqs_category_name = $(this).data('faqs_category_name');
//     var faqs_category_id = $(this).data('faqs_category_id');
//     var list_emp_id = $(this).data('list_emp_id')+'';
//     var list_access = $(this).data('list_access')+'';
//     $('#upcategory_id').val(faqs_category_id);
//     $('#upcategory_name').val(faqs_category_name);
//     $('#tab_subcategory').html('success');
//     list_emp_id = list_emp_id.split(",");
//     list_access = list_access.split(",");
//     $.ajax({
//         type: 'post',
//         url: baseUrl+'/Faqs/checkEmployee',
//         async: false,
//         dataType: 'json',
//         success: function(data) {
//             var html = '';
//             var account = '';
//             var gicheck = '';
//             var str = '';
//             var html2 = '';
//             var count = 0;
//             data.forEach(function(value) {
//                 var n = list_emp_id.includes(value.emp_id);
//                 var m = list_access.includes(value.emp_id);
//                 let stats = (m) ? 'Editor' : 'Viewer';
//                 let color = (m) ? 'm-badge--success' : 'm-badge--info';
//                 if(n || m){
//                     count = count + 1;
//                     html2 += '\
//                     <li style="margin: 10px 0;" class="faqs">\
//                     <div class="m--align-center">\
//                     <div class="m-card-user m-card-user--skin-dark";padding: 5px;">\
//                     <div class="m-card-user__pic">\
//                     <img src="'+baseUrl+'/'+value.pic+'" onerror="noImage('+value.uid+')" class="m--img-rounded m--marginless header-userpic'+value.uid+'" style="border: 1px solid #afabab;" />\
//                     </div>\
//                     <div class="m-card-user__details">\
//                     <span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit; color: #333!important; font-weight: bold;">'+value.lname+', '+value.fname+' '+value.mname+' <span class="m-badge '+color+' m-badge--wide " style="font-weight: normal;">'+stats+'</span></span>\
//                     <span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: spaner;">'+value.acc_name+'</span>\
//                     </div>\
//                     </div>\
//                     </div>\
//                     </li>';
//                 }
//             })
//             $('#number_of_employee').html('<span class="text-muted m--font-primary">Show '+count+' records</span>');
//             $('#modalBody1').html('<ul class="view_category" style="list-style-type: none; padding: 0">'+html2+'</ul>');
//         }
//     });
//     $('#faqsModal1').modal('show');
// });

$('.page_here').on('click', '.view_subcategory', function() {
   
    var faqs_category_name = $(this).data('faqs_category_name');
    var faqs_category_id = $(this).data('faqs_category_id');
    var list_emp_id = $(this).data('list_emp_id')+'';
    var list_access = $(this).data('list_access')+'';
    var list_group_emp_id = $(this).data('list_group_emp_id')+'';
    var list_group_access = $(this).data('list_group_access')+'';
    list_access = list_access.split(",");
    list_emp_id = list_emp_id.split(",");
    list_group_access = list_group_access.split(",");
    list_group_emp_id = list_group_emp_id.split(",");
    $('#head_sub_category').text(faqs_category_name);
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/get_faqs_sub_category',
        async: false,
        dataType: 'json',
        data: {faqs_category_id,list_access,list_emp_id,list_group_access,list_group_emp_id},
        beforeSend: function(){
            mApp.block('#faqsModal2 .table', {
        overlayColor: '#000000',
        type: 'loader',
        state: 'success',
        message: 'Please wait...'
    });
        },
        success: function(data) {
            let html = '';
            let editor = '';
            let viewer = '';
            let editor_group = '';
            let viewer_group = '';
            let check_editor = '';
            let check_viewer = '';
            let value_editor = '';
            let check_group_viewer = '';
            let value_group_editor = '';
            let final_list_emp_id = '';
            let final_list_access = '';
            data.subfolder.forEach(function(value) {
                value.faqs.forEach(function(faqs) {
                    if(emp_id != faqs.emp_id){
                        let lname = (faqs.lname) ? capitalizeTheFirstLetterOfEachWord(faqs.lname) : '';
                        let fname = (faqs.fname) ? capitalizeTheFirstLetterOfEachWord(faqs.fname) : '';
                        let mname = (faqs.mname) ? capitalizeTheFirstLetterOfEachWord(faqs.mname) : '';
                        // let dis_name = (faqs.distro_name != null) ? faqs.distro_name : "";
                        check_editor = (faqs.checked_editor == 1) ? 'selected' : '';
                        editor += '<option '+check_editor+' value="emp_'+faqs.emp_id+'" data-subtext="<br><span class=m--font-success>'+faqs.acc_name+'</span>"> '+lname+', '+fname+' '+mname+' </option>';
                        check_viewer = (faqs.checked_viewer == 1) ? 'selected' : '';
                        viewer += '<option '+check_viewer+' value="emp_'+faqs.emp_id+'" data-subtext="<br><span class=m--font-success>'+faqs.acc_name+'</span>"> '+lname+', '+fname+' '+mname+' </option>';
                    }
                })
                value.faqs_group.forEach(function(faqs) {
                    check_group_viewer = (faqs.checked_group_editor == 1) ? 'selected' : '';
                    editor_group += '<option '+check_group_viewer+' value="dis_'+faqs.distro_id+'">'+faqs.distro_name+' </option>';
                    value_group_editor = (faqs.checked_group_viewer == 1) ? 'selected' : '';
                    viewer_group += '<option '+value_group_editor+' value="dis_'+faqs.distro_id+'">'+faqs.distro_name+' </option>';
                })
                final_list_group_access = '<optgroup label="Distro Group">'+editor_group+'</optgroup>'+
                                          '<optgroup label="Employee">'+editor+'</optgroup';
                final_list_group_emp_id = '<optgroup label="Distro Group">'+viewer_group+'</optgroup>'+
                                          '<optgroup label="Employee">'+viewer+'</optgroup';
                html += 
                '<tr style="height:10px !important;">'+
                '<td>'+
                '<input data-faqs_category_id="'+value.faqs_category_id+'" type="text" maxlength="25" class="form-control m-input sub_category_name" value="'+value.faqs_category_name+'" required></td>'+
                '<td><select name="no_null" data-faqs_category_id="'+value.faqs_category_id+'" class="form-control m-bootstrap-select m_selectpicker display_editor " multiple data-live-search="true" data-actions-box="false" data-selected-text-format="count" data-count-selected-text="Selected Employee ({0})">'+final_list_group_access+'</select></td>'+
                '<td><select name="no_null" data-faqs_category_id="'+value.faqs_category_id+'" class="form-control m-bootstrap-select m_selectpicker display_viewer" multiple data-live-search="true" data-selected-text-format="count" data-actions-box="true" data-count-selected-text="Selected Employee ({0})">'+final_list_group_emp_id+'</select></td>'+
                '<td><span data-skin="dark" data-container="body" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="Remove FAQ Category"><a class="deletesubCategory m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-faqs_category_id="' +value.faqs_category_id+ '" ><i class="la la-trash"></i></a></span></td>'+
                '</tr>';
                editor = '';
                viewer = '';
                editor_group = '';
                viewer_group = '';
            })
            $('#display_here').html(html);
            $('.display_editor').selectpicker('refresh');
            $('.display_viewer').selectpicker('refresh');
            mApp.unblock('#faqsModal2 .table');
        },error:function(){
            mApp.unblock('#faqsModal2 .table');
        }
    })
    $('#faqsModal2').modal('show');
});

$('#faqsModal2').on('keyup', '.sub_category_name', function(){
    let faqs_category_id = $(this).data('faqs_category_id');
    let faqs_category_name = $(this).val();
    $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_category_update",
        dataType: "JSON",
        data: {
            faqs_category_id,
            faqs_category_name,
            update:'name',
        },
        success: function(data) {
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": true,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": true,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          };
          $('#offenseDatatable').mDatatable('reload');
          toastr.success("File updated!");
      }
  });
    return false;
});

$('#faqsModal').on('keyup', '#category_name_edit', function(){
    let faqs_category_id = $('#category_id_edit').val();
    let faqs_category_name = $(this).val();
    $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_category_update",
        dataType: "JSON",
        data: {
            faqs_category_id,
            faqs_category_name,
            update:'name',
        },
        success: function(data) {
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": true,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": true,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          };
          $('#offenseDatatable').mDatatable('reload');
          toastr.success("File updated!");
      }
  });
    return false;
});

$('#faqsModal2').on('change', '.display_editor', function(){
    let faqs_category_id = $(this).data('faqs_category_id');
    let this_list_access = $(this).val();
    // list_access = (list_access != null) ? list_access.toString() : '';
    let list_access = filterItems('emp_', this_list_access);
    let list_group_access = filterItems('dis_', this_list_access);
    let comma_list_access = ','+list_access.toString();
    let comma_list_group_access = ','+list_group_access.toString();
    let final_list_access = comma_list_access.split(',emp_');
    let final_list_group_access = comma_list_group_access.split(',dis_');
    final_list_access.shift();
    final_list_group_access.shift();
    $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_category_update",
        dataType: "JSON",
        data: {
            faqs_category_id,
            list_access: final_list_access.toString(),
            list_group_access: final_list_group_access.toString(),
            update:'editor',
        },
        success: function(data) {
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": true,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": true,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          };
          $('#offenseDatatable').mDatatable('reload');
          toastr.success("File updated!");
      }
  });
    return false;
})



$('#faqsModal2').on('change', '.display_viewer', function(){
    let faqs_category_id = $(this).data('faqs_category_id');
    let this_list_emp_id = $(this).val();
    let list_emp_id = filterItems('emp_', this_list_emp_id);
    let list_group_emp_id = filterItems('dis_', this_list_emp_id);
    let comma_list_emp_id = ','+list_emp_id.toString();
    let comma_list_group_emp_id = ','+list_group_emp_id.toString();
    let final_list_emp_id = comma_list_emp_id.split(',emp_');
    let final_list_group_emp_id = comma_list_group_emp_id.split(',dis_');
    final_list_emp_id.shift();
    final_list_group_emp_id.shift();
    $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_category_update",
        dataType: "JSON",
        data: {
            faqs_category_id,
            list_emp_id: final_list_emp_id.toString(),
            list_group_emp_id: final_list_group_emp_id.toString(),
            update:'viewer',
        },
        success: function(data) {
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": true,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": true,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          };
          $('#offenseDatatable').mDatatable('reload');
          toastr.success("File updated!");
      }
  });
    return false;
})

$('#faqsModal').on('change', '#list_access_edit', function(){
    let faqs_category_id = $('#category_id_edit').val();
    let this_list_access = $(this).val();
    // list_access = (list_access != null) ? list_access.toString() : '';
    let list_access = filterItems('emp_', this_list_access);
    let list_group_access = filterItems('dis_', this_list_access);
    let comma_list_access = ','+list_access.toString();
    let comma_list_group_access = ','+list_group_access.toString();
    let final_list_access = comma_list_access.split(',emp_');
    let final_list_group_access = comma_list_group_access.split(',dis_');
    final_list_access.shift();
    final_list_group_access.shift();
    console.log(final_list_group_access);
    $.ajax({
        type: "POST",
        url: baseUrl+"/Faqs/faqs_category_update",
        dataType: "JSON",
        data: {
            faqs_category_id,
            list_access: final_list_access.toString(),
            list_group_access: final_list_group_access.toString(),
            update:'editor',
        },
        success: function(data) {
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": true,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": true,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          };
          $('#offenseDatatable').mDatatable('reload');
          toastr.success("File updated!");
      }
  });
    return false;
})

$('#faqsModal').on('change', '#visible_edit', function(){
    let faqs_category_id = $('#category_id_edit').val();
    let this_list_emp_id = $(this).val();
    // list_emp_id = (list_emp_id != null) ? list_emp_id.toString() : '';
    let list_emp_id = filterItems('emp_', this_list_emp_id);
    let list_group_emp_id = filterItems('dis_', this_list_emp_id);
    let comma_list_emp_id = ','+list_emp_id.toString();
    let comma_list_group_emp_id = ','+list_group_emp_id.toString();
    let final_list_emp_id = comma_list_emp_id.split(',emp_');
    let final_list_group_emp_id = comma_list_group_emp_id.split(',dis_');
    final_list_emp_id.shift();
    final_list_group_emp_id.shift();
    if(this_list_emp_id != ''){
        $.ajax({
            type: "POST",
            url: baseUrl+"/Faqs/faqs_category_update",
            dataType: "JSON",
            data: {
                faqs_category_id,
                list_emp_id: final_list_emp_id.toString(),
                list_group_emp_id: final_list_group_emp_id.toString(),
                update:'viewer',
            },
            success: function(data) {
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": true,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": true,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
              };
              $('#offenseDatatable').mDatatable('reload');
              toastr.success("File updated!");
          }
      });
        return false;
    } else {
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": true,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
      };
      toastr.error("Select atleast 1 viewer!");
  }
  
})

// function display_editor(list_access, check_editor) {
//     $.ajax({
//         type: 'post',
//         url: baseUrl+'/Faqs/sub_category_list_of_employee',
//         async: false,
//         data: {
//             list_emp_id: list_access.toString(),
//             check_viewer: check_editor.toString(),
//         },
//         dataType: 'json',
//         success: function(data) {
//             let html = '';
//             let checked = '';
//             data.forEach(function(value){
//                 checked = (value.checked == 1) ? 'selected' : '';
//                 html += '<option '+checked+' value="'+value.emp_id+'" data-subtext="<span class=m--font-success>'+value.acc_name+'</span>">'+value.lname+', '+value.fname+' '+value.mname+'</option>'
//             })
//             console.log(html);
//             $('#display_editor').html(html);
//             $('#display_editor').selectpicker('refresh');
//         }
//     });
// }


$('.addNewButton').click(function() {
    $('#m_modal_1').modal('show');
    // list_access('list_access_add', '');
});

// $('#updateCategory').on('submit', function() {
//     var faqs_category_id = $('#category_id_edit').val();
//     var faqs_category_name = $('#category_name_edit').val();
//     var list_access = $('#list_access_edit').val();
//     var relationship = $('#relationship_edit').val();
//     var list_emp_id = $('#visible_edit').val();
//     $.ajax({
//         type: "POST",
//         url: baseUrl+"/Faqs/faqs_category_update",
//         dataType: "JSON",
//         data: {
//             faqs_category_id,
//             faqs_category_name,
//             list_access:list_access.toString(),
//             relationship,
//             list_emp_id:list_emp_id.toString()
//         },
//         success: function(data) {
//             swal("Success!", "Your file has been updated!", "success");
//             $('#category_id_edit').val("");
//             $('#category_name_edit').val("");
//             $('#list_access_edit').selectpicker("val", "");
//             $('#relationship_edit').selectpicker("val", "");
//             $('#visible_edit').selectpicker("val", "");
//             $('#faqsModal').modal('hide');
//             $('#offenseDatatable').mDatatable('reload');
//         }
//     });
//     return false;
// });

$('#offenseDatatable').on('click', '.deleteCategory', function(e) {
    var faqs_category_id = $(this).data('faqs_category_id');
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        input: 'textarea',
        allowEscapeKey: 'false',
        allowOutsideClick: 'false',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        inputValidator: (value) => {
            return new Promise((resolve) => {
                if (value.trim() !== '') {
                    resolve()
                } else {
                    resolve('Oops! Reason is Required')
                }
            })
        }
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: baseUrl+"/Faqs/faqs_category_delete",
                dataType: "JSON",
                data: {
                    faqs_category_id: faqs_category_id,
                    deleteReason: result.value
                },
                success: function(data) {
                    $('#offenseDatatable').mDatatable('reload');
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                        )
                }
            });
            return false;
        }
    });
});

$('#faqsModal2').on('click', '.deletesubCategory', function(e) {
    $('#faqsModal2').modal('hide');
    var faqs_category_id = $(this).data('faqs_category_id');
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        input: 'textarea',
        allowEscapeKey: 'false',
        allowOutsideClick: 'false',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        inputValidator: (value) => {
            return new Promise((resolve) => {
                if (value.trim() !== '') {
                    resolve()
                } else {
                    resolve('Oops! Reason is Required')
                }
            })
        }
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: baseUrl+"/Faqs/faqs_category_delete",
                dataType: "JSON",
                data: {
                    faqs_category_id: faqs_category_id,
                    deleteReason: result.value
                },
                success: function(data) {
                    $('#offenseDatatable').mDatatable('reload');
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                        )
                }
            });
            return false;
        } else {
            $('#faqsModal2').modal('show');
        }
    });
});

function search() {
    let input = document.getElementById('searchbar').value
    input = input.toLowerCase();
    let x = document.getElementsByClassName('faqs');
    for (i = 0; i < x.length; i++) {
        if (!x[i].innerHTML.toLowerCase().includes(input)) {
            x[i].style.display = "none";
        } else {
            x[i].style.display = "list-item";
        }
    }
}

$('#m_modal_1').on('change', '#list_access_add', function(){
    let check_editor = $(this).val();
    console.log(check_editor);
})

function visible_to() {
    
    var category_id = 0;
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/create_category_list_employee',
        async: false,
        dataType: 'json',
        success: function(data) {
            let html1 = '';
            let html2 = '';
            let final_html = '';
            let dis_name = '';
            let distro_id = [];
            let list_id = [];
            data.emp.forEach(function(value){
                if(emp_id != value.emp_id){
                    list_id.push(value.emp_id);
                    // dis_name = (value.distro_name != null) ? value.distro_name : "";
                    let lname = (value.lname) ? capitalizeTheFirstLetterOfEachWord(value.lname) : '';
                    let fname = (value.fname) ? capitalizeTheFirstLetterOfEachWord(value.fname) : '';
                    let mname = (value.mname) ? capitalizeTheFirstLetterOfEachWord(value.mname) : '';
                    // html1 += '<option value="'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span> <br><span class=m--font-danger>'+dis_name+'</span>">'+lname+', '+fname+' '+mname+'</option>'
                    html1 += '<option value="emp_'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span>">'+lname+', '+fname+' '+mname+'</option>'
                }
            })
            data.dis.forEach(function(value){
                html2 += '<option value="dis_'+value.distro_id+'">'+value.distro_name+'</option>';
            })
            
            final_html += '<optgroup label="Distro Group">'+html2+'</optgroup>'+
                          '<optgroup label="Employee">'+html1+'</optgroup';
            $('#list_access_add').html(final_html);
            $('#list_access_add').selectpicker('refresh');
            $('#list_emp_id_add').html(final_html);
            $('#list_emp_id_add').selectpicker('refresh');
            $('#sub_list_access_add').html(final_html);
            $('#sub_list_access_add').selectpicker('refresh');
            $('#sub_list_emp_id_add').html(final_html);
            $('#sub_list_emp_id_add').selectpicker('refresh');

        }
    });
}