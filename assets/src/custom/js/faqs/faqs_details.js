$(function(){
	faqsDetails();
	isEditor();
});

function isEditor() {
	$.ajax({
		type  : 'POST',
		url   : baseUrl+'/Faqs/isEditor',
		async : false,
		dataType : 'json',
		data : {faqs_id},
		success : function(data){
			let html = '';
			let who_can_view = '';
			let list_emp_id = [];
			let list_access = [];
			let list_group_access = '';
            let list_group_emp_id = '';
			data.forEach(function(value){
				list_group_access = value.list_group_access;
                list_group_emp_id = value.list_group_emp_id;
				let faqs_list_access = (value.faqs_list_access) ? value.faqs_list_access.split(",") : '';
				let faqs_list_emp_id = (value.faqs_list_emp_id) ? value.faqs_list_emp_id.split(",") : '';
				let sub_list_access = (value.sub_list_access) ? value.sub_list_access.split(",") : '';
				let sub_list_emp_id = (value.sub_list_emp_id) ? value.sub_list_emp_id.split(",") : '';
				let cat_list_access = (value.cat_list_access) ? value.cat_list_access.split(",") : '';
				let cat_list_emp_id = (value.cat_list_emp_id) ? value.cat_list_emp_id.split(",") : '';
				list_access.push(value.faqs_emp_id);
				if(!faqs_list_emp_id.includes(value.sub_emp_id)){
					list_access.push(value.sub_emp_id);
				}
				if(!faqs_list_emp_id.includes(value.cat_emp_id) && !sub_list_emp_id.includes(value.cat_emp_id)){
					list_access.push(value.cat_emp_id);
				}
				for (let i = 0; i<faqs_list_access.length; i++){
					list_access.push(faqs_list_access[i]);
				}
				for (let i = 0; i<faqs_list_emp_id.length; i++){
					list_emp_id.push(faqs_list_emp_id[i]);
				}
				for (let i = 0; i<sub_list_access.length; i++){
					if(faqs_list_emp_id.includes(sub_list_access[i])){
						list_emp_id.push(sub_list_access[i]);
					} else{
						list_access.push(sub_list_access[i]);
					}
				}
				for (let i = 0; i<sub_list_emp_id.length; i++){
					if(!faqs_list_access.includes(sub_list_emp_id[i])){
						list_emp_id.push(sub_list_emp_id[i]);
					}
				}
				for (let i = 0; i<cat_list_emp_id.length; i++){
					if(!sub_list_access.includes(cat_list_emp_id[i]) || !faqs_list_access.includes(cat_list_emp_id[i])){
						list_emp_id.push(cat_list_emp_id[i]);
					}
				}
				for (let i = 0; i<cat_list_access.length; i++){
					if(sub_list_emp_id.includes(cat_list_access[i]) || faqs_list_emp_id.includes(cat_list_access[i])){
						list_emp_id.push(cat_list_access[i]);
					} else {
						list_access.push(cat_list_access[i]);
					}
				}

				console.log(removeDuplicates(list_access));
				console.log(removeDuplicates(list_emp_id));
				if(value.faqs_emp_id == emp_id || value.sub_emp_id == emp_id || value.cat_emp_id == emp_id || faqs_list_access.includes(emp_id) || (sub_list_access.includes(emp_id) && (!faqs_list_emp_id.includes(emp_id))) || (cat_list_access.includes(emp_id) && (!faqs_list_emp_id.includes(emp_id)) && (!sub_list_emp_id.includes(emp_id)))){
					let checkTitle = (value.isDisplay==1) ? 'Published' : 'Draft';
					let check = (value.isDisplay==1) ? 'checked' : '';
					let updateButton = 
					'<a href="'+baseUrl+'/Faqs/faqs_edit_page/'+value.faqs_id+'" class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-info addNewButton">Update <i class="fa fa-pencil"></i></a>'
					let isVisible = 
					'<div class="row">'+
					'<div class="col-5"><label class="col-form-label">'+checkTitle+' </label></div>  '+
					'<div class="col-5">'+
					'<span class="m-switch m-switch--outline m-switch--icon m-switch--accent">'+
					'<label>'+
					'<input value="'+value.isDisplay+'" class="get_value" '+check+' type="checkbox" name="">'+
					'<span></span>'+
					'</label>'+
					'</span>'+
					'</div>'+
					'</div>'
					let isArchive =
					'<div class="row">'+
					'<div class="col-5"><label class="col-form-label">Archive</label></div>'+
					'<div class="col-5">'+
					'<span class="m-switch m-switch--outline m-switch--icon m-switch--accent">'+
					'<label>'+
					'<input class="get_archive" type="checkbox" name="">'+
					'<span></span>'+
					'</label>'+
					'</span>'+
					'</div>'+
					'</div>';
					html = 
					'<div class="row">'+
					'<div class="col-4">'+updateButton+'</div>'+
					'<div class="col-4" id="isVisible">'+isVisible+'</div>'+
					'<div class="col-4" id="isArchive">'+isArchive+'</div>'+
					'</div>';
					who_can_view = '<a class="view_account" style="cursor: pointer; color: #45A0FD;">Who can view <i style="color: #575962;" class="fa fa-search"></i></a>';
				} 
				
			})
			$('#isEditor').html(html);
			$('#who_can_view').html(who_can_view);
			$('.view_account').click(function(){
				// var list_emp_id = $(this).data('list_emp_id');
				// var list_access = $(this).data('list_access');
				modalView(removeDuplicates(list_emp_id),removeDuplicates(list_access),list_group_access,list_group_emp_id);
			});
		}
	})
}

function removeDuplicates(data){
	return data.filter((value, index) => data.indexOf(value) === index);
}

function faqsDetails() {
	$.ajax({
		type  : 'POST',
		url   : baseUrl+'/Faqs/faqs_details_data',
		async : false,
		dataType : 'json',
		data : {id:faqs_id},
		success : function(data){
			let m = moment();
			var html = '';
			var visible = '';
			var archive = '';
			
			data.forEach(function(value){
				var check = '';
				var checkTitle = 'Draft';
				var hide = '';
				if(value.isDisplay == 1){
					check = 'checked';
					checkTitle = 'Publish';
				} else if(value.isDisplay == 2){
					hide = 'checked';
				}
				if(emp_id==value.emp_id || value.faqs_list_access+''.includes(emp_id) || (value.cat_list_access+''.includes(emp_id) && !value.faqs_list_emp_id+''.includes(emp_id))){
					who_can_view = '<a class="view_account" style="cursor:pointer; color: #45A0FD;" data-list_emp_id="'+value.list_emp_id+'" data-list_access="'+value.list_access+'">Who can view <i style="color: #575962;" class="fa fa-search"></i></a>';
				}
				visible = 
				'<div class="row">'+
				'<div class="col-4"><label class="col-form-label">'+checkTitle+'</label></div>'+
				'<div class="col-5">'+
				'<span class="m-switch m-switch--outline m-switch--icon m-switch--accent">'+
				'<label>'+
				'<input value="'+value.isDisplay+'" class="get_value" '+check+' type="checkbox" name="">'+
				'<span></span>'+
				'</label>'+
				'</span>'+
				'</div>'+
				'</div>';

				archive =
				'<div class="row">'+
				'<div class="col-5"><label class="col-form-label">Archive</label></div>'+
				'<div class="col-5">'+
				'<span class="m-switch m-switch--outline m-switch--icon m-switch--accent">'+
				'<label>'+
				'<input class="get_archive" type="checkbox" '+hide+' name="">'+
				'<span></span>'+
				'</label>'+
				'</span>'+
				'</div>'+
				'</div>';
				var time = moment(value.timeUpdate).fromNow();
				var time2 = moment(value.timeUpdate).format('LLLL');
				
				
				html += '<div class="m-portlet m-portlet--space">'+
				'<div class="m-portlet__head">'+
				'<div class="m-portlet__head-caption">'+
				'<div class="m-portlet__head-title">'+                        
				'<h2 style="color: #183247; line-height: 26px; font-family: roboto;">'+value.title+'</h2>'+
				'<div style="line-height: 21.875px; font-family: roboto; font-size: 14px;"><br>'+
				'<div> Created by: '+value.lname+', '+value.fname+' '+value.mname+'</div>'+
				'Modified on: '+
				'<span data-skin="dark" data-container="body" title="" data-toggle="m-tooltip" data-html="true" data-placement="right" data-original-title="'+time2+'">'+time+'</span>'+
				'<br>'+
				'<div id="who_can_view"></div>'+
				'</div>'+
				'</div>'+  
				'</div>'+
				'</div>'+
				'<div class="m-portlet__body">'+
				value.details+
				'</div>'+
				'</div>';
			});
			// $('#isVisible').html(visible);
			// $('#isArchive').html(archive);
			$('#faqsView').html(html);
			
		}
	})
}

$('.m-subheader').on('click', '.get_value',function(){
	var id = faqs_id;
	var checkBox = '';
	var title = '';
	var statement = '';
	if($(this).is(":checked")){
		checkBox = '1';
		title = 'Successfully set to published';
		statement = 'This article was successfully published!';
	} else {
		checkBox = '0';
		title = 'Successfully set to draft';
		statement = 'This article is only viewable by you!';
	}
	$.ajax({
		type : "POST",
		url  : baseUrl+"/Faqs/checkId",
		dataType : "JSON",
		data : {
			isDisplay:checkBox,id:id
		},
		success: function(data){
			swal({
				title: title,
				text: statement,
				type: 'success',
				confirmButtonText: 'Ok',
				allowEscapeKey: 'false',
				allowOutsideClick: 'false',
			}).then(function(result) {
				if (result.value) {
					location.reload();
				}
			});
		}
	});
});

$('.m-subheader').on('click', '.get_archive',function(e){
	e.preventDefault();
	swal({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type: 'warning',
		input: 'textarea',
		allowEscapeKey: 'false',
		allowOutsideClick: 'false',
		showCancelButton: true,
		confirmButtonText: 'Yes, delete it!',
		inputValidator: (value) => {
			return new Promise((resolve) => {
				if (value.trim() !== '') {
					resolve()
				} else {
					resolve('Oops! Reason is Required')
				}
			})
		}
	}).then(function(result) {
		if(result.value) {
			$.ajax({
				type : "POST",
				url  : baseUrl+"/Faqs/faqs_delete",
				dataType : "JSON",
				data : {faqs_id,
					deleteReason:result.value,
				},
				success: function(data){
					swal(
						'Deleted!',
						'Your file has been deleted.',
						'success'
						)
				}
			});
			location.reload();
		}else{
			var archive =
			'<div class="row">'+
			'<div class="col-5"><label class="col-form-label">Archive</label></div>'+
			'<div class="col-5">'+
			'<span class="m-switch m-switch--outline m-switch--icon m-switch--accent">'+
			'<label>'+
			'<input class="get_archive" type="checkbox" name="">'+
			'<span></span>'+
			'</label>'+
			'</span>'+
			'</div>'+
			'</div>';
			location.reload();
		}
	});
});



function modalView(list_emp_id,list_access,list_group_access,list_group_emp_id){
	$('#faqsModal1').modal('show');
	var list_emp_id = list_emp_id+'';
	var list_access = list_access+'';
	list_emp_id = list_emp_id.split(',');
	list_access = list_access.split(',');
	var list_group_emp_id = list_group_emp_id+'';
    var list_group_access = list_group_access+'';
    list_group_emp_id = list_group_emp_id.split(',');
    list_group_access = list_group_access.split(',');
	$.ajax({
		type  : 'POST',
		url   : baseUrl+'/Faqs/checkEmployee',
		async : false,
		dataType : 'json',
		success : function(data){
			var html = '';
			var account = '';
			var gicheck = '';
			var str = '';
			var html2 = '';
			var count = 0;
			let set_pic = '';
			data.emp.forEach(function(value) {
				if(value.emp_id == 184){
                    set_pic = value.pic;
                }
				var n = list_emp_id.includes(value.emp_id);
				var m = list_access.includes(value.emp_id);
				let stats = (m) ? 'Editor' : 'Viewer';
				let color = (m) ? 'm-badge--success' : 'm-badge--info';
				if(n || m){
					count = count + 1;
					html2 += '\
					<li style="margin: 10px 0;" class="faqs">\
					<div class="m--align-center">\
					<div class="m-card-user m-card-user--skin-dark";padding: 5px;">\
					<div class="m-card-user__pic">\
					<img src="'+baseUrl+'/'+value.pic+'" onerror="noImage('+value.uid+')" class="m--img-rounded m--marginless header-userpic'+value.uid+'" style="border: 1px solid #afabab;" />\
					</div>\
					<div class="m-card-user__details">\
					<span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit; color: #333!important; font-weight: bold;">'+value.lname+', '+value.fname+' '+value.mname+' <span class="m-badge '+color+' m-badge--wide " style="font-weight: normal;">'+stats+'</span></span>\
					<span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: smaller;">'+value.acc_name+'</span>\
					</div>\
					</div>\
					</div>\
					</li>';
				}
			})
			data.dis.forEach(function(value) {
                var n = list_group_emp_id.includes(value.distro_id);
                var m = list_group_access.includes(value.distro_id);
                let stats = (m) ? 'Editor' : 'Viewer';
                let color = (m) ? 'm-badge--success' : 'm-badge--info';
                if(n || m){
                    html2 += '\
                    <li style="margin: 10px 0;" class="faqs">\
                    <div class="m--align-center">\
                    <div class="m-card-user m-card-user--skin-dark";padding: 5px;">\
                    <div class="m-card-user__pic">\
                    <img src="'+baseUrl+'/'+value.pic+'" onerror="noImage('+value.uid+')" class="m--img-rounded m--marginless header-userpic'+value.uid+'" style="border: 1px solid #afabab;" />\
                    </div>\
                    <div class="m-card-user__details">\
                    <span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit; color: #333!important; font-weight: bold;">'+value.distro_name+' <span class="m-badge '+color+' m-badge--wide " style="font-weight: normal;">'+stats+'</span></span>\
                    <span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: smaller;">Distro Group</span>\
                    </div>\
                    </div>\
                    </div>\
                    </li>';
                }
            })
			$('#number_of_employee').html('<small class="text-muted m--font-primary">Show '+count+' records</small>');
			$('#modalBody1').html('<ul class="view_category" style="list-style-type: none; padding: 0">'+html2+'</ul>');
		}
	});
}
function search() {
	let input = document.getElementById('searchbar').value
	input=input.toLowerCase();
	let x = document.getElementsByClassName('faqs');
	
	for (i = 0; i < x.length; i++) { 
		if (!x[i].innerHTML.toLowerCase().includes(input)) {
			x[i].style.display="none";
		}
		else {
			x[i].style.display="list-item";                 
		}
	}
}

$("#faqsView").on('click','.myImg',function(elem){
	// alert('clicked');
	console.log($(this).attr('src'));
	$("#img01").attr('src',$(this).attr('src'));
	$("#myModal").modal('show');
})
// var modal = $("myModal");

// // Get the image and insert it inside the modal - use its "alt" text as a caption
// var img = $("myImg");
// var modalImg = $("img01");
// img.onclick = function(){
//   modal.style.display = "block";
//   modalImg.src = this.src;
//   captionText.innerHTML = this.alt;
// }

// // Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close")[0];

// // When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//   modal.style.display = "none";
// }