$(function(){
    $('.m_selectpicker').selectpicker();
    $('#details').summernote({
        toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen'/*, 'codeview' */]],   // remove codeview button 
      ],
        tabsize: 2,
        height: 500,
        callbacks: {
            onImageUpload: function(image) {
                uploadImageGeneral(image[0]);
            },
            onMediaDelete : function(target) {
                deleteImageGeneral(target[0].src);
            }
        }
    });

    function uploadImageGeneral(image) {
        var data = new FormData();
        data.append("image", image);
        $.ajax({
            url: baseUrl+"/Faqs/general_upload_photo",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "POST",
           beforeSend: function(){
               mApp.block('.m-content', {
                  overlayColor: '#000000',
                  type: 'loader',
                  state: 'success',
                  message: 'Please wait...'
              });
            },
            success: function(url) {
                // $('#details').summernote("insertImage", url);
                var image = $('<img class="myImg" src="'+url+'" />');
                $('#details').summernote("insertNode", image[0]);
                mApp.unblock('.m-content');
            },
            error: function(data) {
                mApp.unblock('.m-content');
                console.log(data);
            }
        });
    }

    function deleteImageGeneral(src) {
        $.ajax({
            data: {src : src},
            type: "POST",
            url: baseUrl+"/Faqs/delete_image",
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }
    select_category();
    select_list_access();
    select_list_emp_id();
    faqsView();
    ClipboardDemo.init();
});
$('.modal_pop_up').click(function() {
    $('#faqsModal1').modal('show');
});

var ClipboardDemo = function () {
    var demos = function () {
        new Clipboard('[data-clipboard=true]').on('success', function(e) {
            e.clearSelection();
            alert('Copied!');
        });
    }
    return {
        init: function() {
            demos(); 
        }
    };
}();

function select_category() {
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_category',
        async: false,
        dataType: 'json',
        data: {
            selected:'cat_only',
        },
        success: function(data) {
            let html = '';
            data.forEach(function(value){
                let subtext = (value.relationship == 0) ? 'Category' : 'Sub-Category';
                let selected = (value.selected == 1) ? 'selected' : '';
                html += '<option '+selected+'value="'+value.faqs_category_id+'" data-subtext="'+subtext+'">'+value.faqs_category_name+'</option>';
            })
            $('#category_id').html(html);
            $('#category_id').selectpicker('refresh');
        }
    }); 
}

function select_list_access() {
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_employee',
        async: false,
        dataType: 'json',
        success: function(data) {
            let html = '';
            let dis_name = '';
            let checked = '';
            data.emp.forEach(function(value){
                // checked = (value.checked == 1) ? 'selected' : '';
                dis_name = (value.distro_name) ? value.distro_name : '';
                let lname = (value.lname) ? capitalizeTheFirstLetterOfEachWord(value.lname) : '';
                let fname = (value.fname) ? capitalizeTheFirstLetterOfEachWord(value.fname) : '';
                let mname = (value.mname) ? capitalizeTheFirstLetterOfEachWord(value.mname) : '';
                html += '<option '+checked+' value="'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span> <span class=m--font-danger>'+dis_name+'</span>">'+lname+', '+fname+' '+mname+'</option>'
            })
            $('#list_access').html(html);
            $('#list_access').selectpicker('refresh');
        }
    });     
}

function select_list_emp_id() {
    $.ajax({
        type: 'post',
        url: baseUrl+'/Faqs/list_of_employee',
        async: false,
        dataType: 'json',
        success: function(data) {
            let html = '';
            let dis_name = '';
            let checked = '';
            data.emp.forEach(function(value){
                checked = (value.checked == 1) ? 'selected' : '';
                dis_name = (value.distro_name) ? value.distro_name : '';
                let lname = (value.lname) ? capitalizeTheFirstLetterOfEachWord(value.lname) : '';
                let fname = (value.fname) ? capitalizeTheFirstLetterOfEachWord(value.fname) : '';
                let mname = (value.mname) ? capitalizeTheFirstLetterOfEachWord(value.mname) : '';
                html += '<option '+checked+' value="'+value.emp_id+'" data-subtext="<br><span class=m--font-success>'+value.acc_name+'</span> <span class=m--font-danger>'+dis_name+'</span>">'+lname+', '+fname+' '+mname+'</option>'
            })
            $('#list_emp_id').html(html);
            $('#list_emp_id').selectpicker('refresh');
        }
    });     
}

function faqsView() {
    var searchText = "";
    var category = 0;
    var visible = 2;
    $.ajax({
        type  : 'POST',
        data:{searchText,category,visible},
        url   : baseUrl+'/Faqs/faqs_v2_view',
        async: false,
        dataType: 'json',
        success : function(data){
            var html = '';
            var faqsList = '';
            var count = '';
            data.forEach(function(value){
                value.faqs.forEach(function(faqs){
                    faqsList += '<option value="'+baseUrl+'/Faqs/faqs_details/'+faqs.faqs_id+'">'+faqs.title+'</option>';
                })
                if(value.faqs.length ===0){
                    html += '';
                } else {
                    html += 
                    '<optgroup label="'+value.faqs_category_name+'">'+
                    faqsList+
                    '</optgroup>';
                }
                faqsList = '';
            })
            $('#faqs_list').html(html);
            $('#faqs_list').selectpicker('refresh');
        }
    });
}

$('#faqsModal1').on('change', '#faqs_list', function(){
    let faqs_list = $('#faqs_list').val();
    $('#faqs_link').val(faqs_list);
})

function submitData(){
    let isDisplay =  ($('.get_value').is(":checked")) ? 1 : 0;
    let title = $('#title').val();
    let faqs_category_id = $('#category_id').val();
    let details = $('#details').val();
    let list_access = $('#list_access').val();
    let list_emp_id = $('#list_emp_id').val();
    $.ajax({
        type : "POST",
        url  : baseUrl+"/Faqs/faqs_create_add",
        dataType : "JSON",
        data : {
            faqs_category_id,
            title,
            details,
            isDisplay,
            list_emp_id:list_emp_id.toString(),
            list_access:list_access.toString(),
        },
        success: function(data){
            $('#category_id').val("");
            $('#title').val("");
            $("#details").summernote("code", "");
            $('#isDisplay').val("");
            $('.m_selectpicker').selectpicker('val', '');
            swal('Success!', 'Your file has been saved.', 'success');
            window.location.href = baseUrl+"/Faqs/faqs_edit_page/"+data;
        },
        error: function(data) {
            swal('Error!', 'FAQ title already exist,', 'error');
        }
    });
    return false;
}

$('#m_form_1').formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        title: {
            validators: {
                notEmpty: {
                    message: 'Oops! Title label is required'
                },
            }
        },
        category: {
            validators: {
                notEmpty: {
                    message: 'Oops! Category label is required'
                },
            }
        },
    }
}).on('success.form.fv', function (e, data) {
    e.preventDefault();
    submitData();
});
function capitalizeTheFirstLetterOfEachWord(words) {
 var separateWord = words.toLowerCase().split(' ');
 for (var i = 0; i < separateWord.length; i++) {
  separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
  separateWord[i].substring(1);
}
return separateWord.join(' ');
}