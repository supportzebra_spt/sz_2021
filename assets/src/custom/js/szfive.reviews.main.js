var imagePath = baseUrl + '/assets/images/';

var ReviewersDatatable = function () {

    var reviewersSelector = function (data) {

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/szfive/get_survey_reviewers',
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
                        params: {
                            query: {
                                startDate: data.startDate,
                                endDate: data.endDate,
                                depId: data.depId
                            },
                        },
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                scroll: false,
                footer: false,
                header: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50],
                    },
                },
            },
            search: {
                input: $('#reviewerDetailsSearch'),
            },
            translate: {
                records: {
                    processing: 'Please wait...',
                    noRecords: '<i class="flaticon-users" style="font-size: 24px;margin: 0 5px;"></i><br> No reviewers found.',
                },
            },
            columns: [{
                width: 330,
                field: 'reviewId',
                sortable: false,
                title: 'Reviewer Details',
                template: function (data) {

                    if (data.total_reviewed != 0) {
                        var review_badge = '<span class="m-badge m-badge--success">' + data.total_reviewed + '</span>';
                    } else {
                        var review_badge = '';
                    }
                    var unreviewed = data.total_reviews - data.total_reviewed;

                    if (unreviewed != 0) {

                        var unreview_badge = '<span class="m-badge m-badge--danger">' + unreviewed + '</span>';
                    } else {
                        var unreview_badge = '';
                    }
                    str = '<div class="m-widget5__item" style="margin-bottom: 0;">';
                    str += '    <div class="m-widget5__pic">';
                    str += '        <img class="m-widget7__img reviewer-img' + data.reviewerId + '" src="' + data.pic + '"  onerror="noImage(' + data.reviewerId + ')" alt="" style="border-radius: 50%; width: 5rem;border: 1px solid #eee;">';
                    str += '    </div>';
                    str += '    <div class="m-widget5__content">';
                    str += '        <h5 class="m-widget5__title" style="margin-bottom: 0;">' + data.reviewer + '</h5>';
                    str += '        <span class="m-widget5__desc">' + data.position + '</span><br><span class="m-widget5__desc">' + data.dept_abbv + ' | ' + data.account + '</span>';
                    str += '<div class="m-btn-group m-btn-group--pill btn-group mr-2" role="group" aria-label="First group" style="margin-top: 10px;">';
                    str += '    <button type="button" class="m-btn btn btn-outline-secondary" onClick="reviewedAnswer(' + data.reviewerId + ')">';
                    str += '        <i class="fa fa-eye"></i> Reviewed ' + review_badge;
                    str += '    </button>';
                    str += '    <button type="button" class="m-btn btn btn-outline-secondary" onClick="unreviewedAnswer(' + data.reviewerId + ')">';
                    str += '        <i class="fa fa-eye-slash"></i> Unreviewed ' + unreview_badge;
                    str += '    </button>';
                    str += '</div>';
                    str += '    </div>';
                    str += '</div>';
                    return str;
                },
            }, {
                width: 80,
                field: 'total_reviews',
                sortable: false,
                title: 'Reviewer Details',
                template: function (data) {
                    str = '    <div class="m-widget5__stats1">';
                    str += '        <span class="m-widget5__number">' + data.total_reviews + '</span><br>';
                    str += '        <span class="m-widget5__sales">total answers</span>';
                    str += '    </div>';
                    return str;
                }
            }, {
                width: 80,
                field: 'total_reviewed',
                sortable: false,
                title: 'Reviewer Details',
                template: function (data) {
                    str = '    <div class="m-widget5__stats2">';
                    str += '        <span class="m-widget5__number">' + data.total_reviewed + '</span><br>';
                    str += '        <span class="m-widget5__sales">reviewed answers</span>';
                    str += '    </div>';
                    return str;
                }
            }, {
                width: 70,
                field: 'reviewerId',
                sortable: false,
                title: 'Reviewer Details',
                template: function (data) {
                    // var percent = (data.total_reviewed / data.total_reviews) * 100;
                    // percent = percent.toFixed(2) + '%';
                    str = '    <div class="m-widget5__stats2">';
                    str += '        <span class="m-widget5__number">' + data.percent + '</span><br>';
                    str += '        <span class="m-widget5__sales">complete</span>';
                    str += '    </div>';
                    return str;
                }
            }]
        };

        var datatable = $('.m_datatable_reviewers').mDatatable(options);
    };

    return {
        init: function (data) {
            reviewersSelector(data);
        },
    };
}();

// Modal for List of Reviews
var ReviewListsDatatable = function () {

    var revieweredAnswerSelector = function (data) {

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/szfive/get_reviewed_survey_answers',
                        params: {
                            query: {
                                reviewerId: data.reviewerId,
                                isReviewed: data.isReviewed,
                                startDate: data.startDate,
                                endDate: data.endDate
                            },
                        },
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                scroll: false,
                footer: false,
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#reviewersListSearch'),
            },
            translate: {
                records: {
                    processing: 'Please wait...',
                    noRecords: 'No answers found.',
                },
            },
            columns: [{
                field: 'reviewId',
                sortable: false,
                width: 250,
                title: 'Name',
                template: function (data) {

                    var pic = data.pic ? data.pic : "images/img/sz.png";

                    output = '<div class="m-card-user m-card-user--sm">\
                                   <div class ="m-card-user__pic"> \
                                        <a class ="m-card-user__email m-link" style="cursor:pointer;" onClick="viewSurveyAnswer(' + data.answerId + ')"><img src="' + imagePath + pic + '" onerror="noImage2(' + data.empId + ')" style="height:40px;" class = "m--img -rounded m--marginless answer-pic-dt' + data.empId + '" alt = "photo "></a> \
                                    </div>\
                                    <div class="m-card-user__details">\
                                        <a class ="m-card-user__email m-link" style="cursor:pointer;" onClick="viewSurveyAnswer(' + data.answerId + ')"><span class="m-card-user__name"><strong>' + data.fname + ' ' + data.lname + '</strong></span></a>\
                                        <p> ' + data.position + ' </span>\
                                    </div>\
                                </div>';
                    return output;
                },
            }, {
                field: 'supervisor_name',
                sortable: false,
                width: 160,
                title: 'Reviewer',
            }, {
                field: 'dateCreated',
                width: 100,
                title: 'Date Answered',
            }, {
                field: 'dateUpdated',
                width: 100,
                title: 'Date Reviewed',
                template: function (data) {

                    if (data.isReviewed == 1) {
                        str = '<span>' + data.dateUpdated + '</span>';
                    } else {
                        str = '<span>No date yet.</span>';
                    }
                    return str;
                }
            }],
        };

        var datatable = $('.m_datatable_reviewed_survey_answers').mDatatable(options);
    };

    return {
        init: function (data) {
            revieweredAnswerSelector(data);
        },
    };
}();

var DatePickers = function () {

    var reviewsMain = function () {

        if ($('#survey_reviews_daterange').length == 0) {
            return;
        }

        getCurrentDateTime(function (date) {
            var picker = $('#survey_reviews_daterange');
            var start = moment(date, 'Y-MM-DD').startOf('isoWeek');
            var end = moment(date, 'Y-MM-DD').endOf('isoWeek');

            function cb(start, end, label) {

                var title = '';
                var range = '';

                if (start.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').startOf('isoWeek').format('YYYY-MM-DD') && end.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').endOf('isoWeek').format('YYYY-MM-DD')) {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Today') {
                    title = 'Today:';
                    range = start.format('MMM D, Y');
                } else if (label == 'Yesterday') {
                    title = 'Yesterday:';
                    range = start.format('MMM D, Y');
                } else if (label == 'Last 7 Days') {
                    title = 'Last 7 Days:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'This Week') {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Week') {
                    title = 'Last Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last 30 Days') {
                    title = 'Last 30 Days:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'This Month') {
                    title = 'This Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Month') {
                    title = 'Last Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else {
                    title = 'Custom:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                }

                picker.find('.m-subheader__daterange-date-reviewers').html(range);
                picker.find('.m-subheader__daterange-title-reviewers').html(title);

                $('input[name=start_date_reviewer]').val(start.format('YYYY-MM-DD'));
                $('input[name=end_date_reviewer]').val(end.format('YYYY-MM-DD'));

                if ($('input[name=start_date_reviewer]').val() != null && $('input[name=end_date_reviewer]').val() != null) {

                    var params = {
                        startDate: $('input[name=start_date_reviewer]').val(),
                        endDate: $('input[name=end_date_reviewer]').val(),
                        depId: $('#select_reviews_by_department').select2("val")
                    };
                    getSurveyReviewers(params);
                    showMainReviewsChart(params);
                }
            }

            picker.daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: {
                    'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                    'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                    'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                    'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                    'This Week': [moment(date, 'Y-MM-DD').startOf('isoWeek'), moment(date, 'Y-MM-DD').endOf('isoWeek')],
                    'Last Week': [moment(date, 'Y-MM-DD').subtract(1, 'week').startOf('isoWeek'), moment(date, 'Y-MM-DD').subtract(1, 'week').endOf('isoWeek')],
                    'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                    'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end, '');
        });

    }

    var reviewsList = function () {

        getCurrentDateTime(function (date) {
            var start = moment(date, 'Y-MM-DD');
            var end = moment(date, 'Y-MM-DD');

            $('#reviews_team_list_daterange').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',

                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                    'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                    'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                    'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                    'This Week': [moment(date, 'Y-MM-DD').startOf('week'), moment(date, 'Y-MM-DD').endOf('week')],
                    'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                    'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
                }
            }, function (start, end, label) {
                $('#reviews_team_list_daterange .form-control').val(start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y'));
                var params = {
                    startDate: start.format('YYYY-MM-DD'),
                    endDate: end.format('YYYY-MM-DD'),
                    reviewerId: $('#survey_reviews_modals').data('id'),
                    isReviewed: $('#view_ans_review_title').data('id'),
                };
                $('.m_datatable_reviewed_survey_answers').mDatatable('destroy');
                ReviewListsDatatable.init(params);
            });
        });
    }

    return {
        init: function () {
            reviewsMain();
            reviewsList();
        },
    };
}();

var FormValidation = function () {

    var reviewForm = function () {
        $('#reviewAnswerForm').validate({
            rules: {
                reviewMessage: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                $('#submitReview').removeClass('m-loader m-loader--light m-loader--right');

                swal({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {

                var message = $('#reviewAnswerForm').find('textarea#reviewAnswerTextarea').val();
                var answerId = $('#reviewAnswerForm').find('input[name=answerId]').val();
                message = message.replace(/ +(?= )/g, '');

                var options = {
                    url: baseUrl + '/szfive/submit_review_answer',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        message: message,
                        answerId: answerId
                    },
                    success: function (response) {

                        if (response.status) {
                            var data = response.data;

                            $('#reviewAnswerForm').find('textarea#reviewAnswerTextarea').val("");
                            setTimeout(function () {
                                $('#submitReview').removeClass(
                                    'm-loader m-loader--light m-loader--right'
                                );
                            }, 1000);

                            var dateReviewed = moment(data.dateReviewed).format('MMMM D, YYYY hh:mm:ss A');
                            var message = data.message ? data.message : 'No remarks';

                            str = '<span class="m-badge m-badge--success m-badge--wide">Reviewed by</span><span style="color: #649e99;"> <b style="font-weight: 400;">' + data.reviewer_name + '</b> on <b style="font-weight: 400;">' + dateReviewed + '</b></span>';
                            str += '<p style="border: 4px solid #f7f7fa;padding: 8px;margin-top: 10px;"><b style="color: #000000b5;font-weight: 600;">Remarks:</b> &nbsp;&nbsp;' + message + '</p>';
                            $('#reviewTextarea').hide();
                            $('#mark_as_review_wrapper').html('');
                            $('#alreadyReviewed').html(str);
                            $('.m_datatable_reviewers').mDatatable('reload');
                            $('.m_datatable_reviewed_survey_answers').mDatatable('reload');

                            var params = {
                                startDate: $('input[name=start_date_reviewer]').val(),
                                endDate: $('input[name=end_date_reviewer]').val(),
                                depId: $('#select_reviews_by_department').select2("val")
                            };
                            showMainReviewsChart(params);

                            swal({
                                "title": "",
                                "text": "You have successfully reviewed this answer.",
                                "type": "success",
                                "timer": 1500,
                                "showConfirmButton": false

                            });
                        }
                    },
                    error: function () {
                        alert('error');
                    },
                };

                $(form).ajaxSubmit(options);
            }
        })
    }

    return {
        init: function () {
            reviewForm();
        }
    };
}();

var ReviewChartMain = function () {
    var selec2tChartMain = function () {
        $('#select_reviews_by_department, #select_reviews_by_department_validate').select2({
            placeholder: "Filter by team...",
        });
    }

    return {
        init: function () {
            selec2tChartMain();
        }
    };
}();


jQuery(document).ready(function () {

    ReviewChartMain.init();
    DatePickers.init();
    FormValidation.init();

    $('#cancelReview').on('click', function () {
        $('#reviewTextarea').slideUp();
        $('#submitReview').removeClass('m-loader m-loader--light m-loader--right');
    });

    $('#submitReview').on('click', function () {
        $(this).addClass('m-loader m-loader--light m-loader--right');
    });

    $("#select_reviews_by_department").on('key change', function () {
        var params = {
            startDate: $('input[name=start_date_reviewer]').val(),
            endDate: $('input[name=end_date_reviewer]').val(),
            depId: $('#select_reviews_by_department').select2("val")
        };
        getSurveyReviewers(params);
        showMainReviewsChart(params);
    });

    $('#view_specific_ans_reviews_modals').on('hidden.bs.modal', function () {
        $(this).find('#reviewAnswerForm').trigger("reset");
    });

});

function reviewChart(data) {
    Highcharts.chart('dept_reviewers_chart', {
        chart: {
            type: 'column',
            height: 350,
            events: {
                drilldown: function (e) {
                    var chart = this;

                    if (!e.seriesOptions) {

                        var params = {
                            startDate: $('input[name=start_date_reviewer]').val(),
                            endDate: $('input[name=end_date_reviewer]').val(),
                            depId: $('#select_reviews_by_department').select2("val"),
                            depName: e.point.name
                        };

                        getDeptReviewers(params, function (drilldown) {
                            var series = drilldown[e.point.name];
                            chart.showLoading('Please wait...');

                            setTimeout(function () {
                                chart.hideLoading();
                                chart.setTitle({
                                    text: "Team Supervisors Total Reviewed Answers"
                                });
                                chart.setSubtitle({
                                    text: "Reviewed Answers by SZ Team Supervisors"
                                });
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 500);
                        });
                    }
                },
                drillup: function (e) {
                    var chart = this;
                    chart.setTitle({
                        text: "Reviewed Surveys of the SupportZebra Teams"
                    });
                    chart.setSubtitle({
                        text: "Survey Reviews by Team or Departments"
                    });
                }
            }
        },
        title: {
            text: 'SupportZebra Team Survey Reviews'
        },
        subtitle: {
            text: 'Survey Reviews by Team or Departments'
        },
        colors: ['#ff5722', '#ff9800', '#ffc107', '#f3da03', '#cddc39', '#8bc34a', '#4caf50', '#009688', '#00bcd4', '#03a9f4', '#2196f3', '#3f51b5', '#673ab7'],

        xAxis: {
            type: 'category',
        },
        yAxis: {
            title: {
                text: ''
            },
            labels: {
                enabled: false
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.2f} %'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}"><b>{point.name}</b></span>: Reviewed <b>{point.reviewed}</b> out of {point.total} answers<br/>'
        },
        series: [{
            name: 'SZ Teams',
            colorByPoint: true,
            data: data.dept_reviews
        }],
        drilldown: {
            series: []
        }
    });
}

// Functions
function reviewedAnswer(reviewerId) {
    var params = {
        reviewerId: reviewerId,
        isReviewed: 1,
        startDate: 0,
        endDate: 0
    };
    $('#reviews_team_list_daterange .form-control').val("");
    $('#survey_reviews_modals').attr('data-id', params.reviewerId);
    $('#view_ans_review_title').attr('data-id', params.isReviewed);
    $('#view_ans_review_title').html('List of Reviewed Answers');
    $('.m_datatable_reviewed_survey_answers').mDatatable('destroy');

    ReviewListsDatatable.init(params);

    setTimeout(function () {
        $('#reviewed_survey_answers_modal').modal('show');
    }, 500);
}

function unreviewedAnswer(reviewerId) {
    var params = {
        reviewerId: reviewerId,
        isReviewed: 0,
        startDate: 0,
        endDate: 0
    };
    $('#reviews_team_list_daterange .form-control').val("");
    $('#survey_reviews_modals').attr('data-id', params.reviewerId);
    $('#view_ans_review_title').attr('data-id', params.isReviewed);
    $('#view_ans_review_title').html('List of Unreviewed Answers');
    $('.m_datatable_reviewed_survey_answers').mDatatable('destroy');

    ReviewListsDatatable.init(params);

    setTimeout(function () {
        $('#reviewed_survey_answers_modal').modal('show');
    }, 500);
}

function noImage2(id) {
    $('.answer-pic-dt' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function viewSurveyAnswer(answerId) {
    $('#alreadyReviewed').html('');

    $.when(fetchPostData({
            answerId: answerId
        }, '/szfive/get_reviews_specific_answer'))
        .then(function (data) {

            var data = jQuery.parseJSON(data);

            header = '<div class="m-widget3__header" style="display: table;padding: 0px 10px;padding-top: 8px; ">';
            header += '        <div class="m-widget3__user-img" style="margin-bottom: .7rem;">';
            header += '            <img class="m-widget3__img answer-pic-dt' + data.uid + '" src="' + data.pic + '" onerror="noImage2(' + data.uid + ')" alt="" style="width: 3.2rem;border-radius: 50%;">';
            header += '        </div>';
            header += '        <div class="m-widget3__info" style="display:table-cell;width:100%; padding-left:1rem;font-size:1rem;vertical-align:middle;">';
            header += '            <span class="m-widget3__username" style="font-size: 15px;color: #36817a;">';
            header += '                ' + data.fullname + ' answered ';
            header += '                <u style=" font-size: 17px;">' + data.answer + '</u> out of';
            header += '                <u>' + data.numberOfChoices + '</u> for the daily survey on ' + data.survey_date + '.';
            header += '            </span>';
            header += '            <br>';
            header += '            <span class="m-widget3__time" style="font-size: .85rem;">' + data.answer_date + '</span>';
            header += '        </div>';
            header += '    </div>';

            $('#view_answer_review_header').html(header);

            str = '<div class="m-widget3__item">';
            str += '    <div class="m-widget3__body" id="review_userAnswerId" data-id="' + answerId + '" style=" padding: 0px 10px; margin-bottom: 25px; ">';
            str += '        <blockquote class="blockquote">';
            str += '            <p class="mb-0" style=" font-weight: 400;padding: 0 0 10px 0;">' + data.detailedAnswer + '</p>';
            str += '            <footer class="blockquote-footer">Feeling';
            str += '                <img src="' + imagePath + data.choice_img + '" style=" width: 20px; ">';
            str += '                <cite title="Source Title">';
            str += '                    ' + data.choice_label + ' </cite>';
            str += '            </footer>';
            str += '        </blockquote>';
            str += '     </div>';
            str += '    </div>'

            $('.view_reviews_answer_content').html(str);

            var str_review = '';

            if (parseInt(data.isReviewed) == 1) {

                var dateReviewed = moment(data.dateReviewed).format('MMMM D, YYYY hh:mm:ss A');
                var message = data.message ? data.message : 'No remarks';

                str_review += '<span class="m-badge m-badge--success m-badge--wide">Reviewed by</span><span style="color: #649e99;"> <b style="font-weight: 400;">' + data.supervisorFname + ' ' + data.supervisorLname + '</b> on <b style="font-weight: 400;">' + dateReviewed + '</b></span>';
                str_review += '<p style="border: 4px solid #f7f7fa;padding: 8px;margin-top: 10px;"><b style="color: #000000b5;font-weight: 600;">Remarks:</b> &nbsp;&nbsp;' + message + '</p>';
                str_review += '<small>Reviewed <b><i>' + data.time_diff + '</i></b> after the team member answered the survey.</small>';
                $('#reviewTextarea').hide();
            } else {
                if (data.reviewerId == data.session_id) {
                    $('#reviewTextarea').hide();
                    $('#reviewAnswerForm').find('input[name=answerId]').val(answerId);

                    str_review += '<button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-info m-btn--gradient-to-accent btn-sm" id="btn-reviewAnswer" onClick="markAsReviewed(' + data.answer + ',' + data.numberOfChoices + ')">Mark as Reviewed</button>';
                } else {
                    str_review += '<span class="m-badge m-badge--danger m-badge--wide">Unreviewed</span>';
                }
            }

            $('#mark_as_review_wrapper').html(str_review);
        });

    setTimeout(function () {
        $('#view_specific_ans_reviews_modals').modal('show');
    }, 500);
}

function getSurveyReviewers(params) {

    $('.m_datatable_reviewers').mDatatable('destroy');
    ReviewersDatatable.init(params);
}

function markAsReviewed(answer, numberOfChoices) {

    var numbers = [1];
    numbers.push(numberOfChoices);

    var median = 0;
    var numsLen = numbers.length;
    numbers.sort();

    if (numsLen % 2 === 0) { // is even
        // average of two middle numbers
        median = (numbers[numsLen / 2 - 1] + numbers[numsLen / 2]) / 2;
    } else { // is odd
        // middle number only
        median = numbers[(numsLen - 1) / 2];
    }

    if (answer <= median) {
        $('#reviewTextarea').slideDown();
    } else {
        $('#reviewTextarea').css("display", "none");
        $.when(fetchPostData({
                message: '',
                answerId: $('#review_userAnswerId').data('id')
            }, '/szfive/submit_review_answer'))
            .then(function (response) {
                var resp = jQuery.parseJSON(response);

                if (resp.status == true) {

                    var data = resp.data;
                    swal({
                        "title": "",
                        "text": "You have successfully reviewed this answer.",
                        "type": "success",
                        "timer": 1500,
                        "showConfirmButton": false
                    });

                    var dateReviewed = moment(data.dateReviewed).format('MMMM D, YYYY hh:mm:ss A');
                    var message = data.message ? data.message : 'No remarks';

                    str = '<span class="m-badge m-badge--success m-badge--wide">Reviewed by</span><span style="color: #649e99;"> <b style="font-weight: 400;">' + data.reviewer_name + '</b> on <b style="font-weight: 400;">' + dateReviewed + '</b></span>';
                    str += '<p style="border: 4px solid #f7f7fa;padding: 8px;margin-top: 10px;"><b style="color: #000000b5;font-weight: 600;">Remarks:</b> &nbsp;&nbsp;' + message + '</p>';

                    $('#mark_as_review_wrapper').html('');
                    $('#alreadyReviewed').html(str);
                    $('.m_datatable_reviewers').mDatatable('reload');
                    $('.m_datatable_reviewed_survey_answers').mDatatable('reload');

                    var params = {
                        startDate: $('input[name=start_date_reviewer]').val(),
                        endDate: $('input[name=end_date_reviewer]').val(),
                        depId: $('#select_reviews_by_department').select2("val")
                    };
                    showMainReviewsChart(params);
                } else {
                    swal({
                        "title": "Error",
                        "text": "There are some errors in your submission. Please correct them.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                }
            });
    }
}

function hideChart() {
    showSpinner();
    setTimeout(function () {
        $('#display-mainchart').show();
    }, 600);
    $('#dept_reviews_chart').hide();
}

function showChart() {
    $('#display-mainchart').hide();
    $('#dept_reviews_chart').show();
}

function showMainReviewsChart(params) {
    $.when(fetchPostData({
            params
        }, '/szfive/get_by_departments_reviews'))
        .then(function (response) {
            console.log(response);
            response = jQuery.parseJSON(response);
            if (response.hasData == false) {
                hideChart();
            } else {
                reviewChart(response);
                showChart();
            }
        });
}

function getDeptReviewers(params, callback) {
    $.when(fetchPostData({
            params
        }, '/szfive/get_by_department_reviewers'))
        .then(function (data) {
            drilldown = jQuery.parseJSON(data);
            callback(drilldown);
        });
}

function showSpinner() {
    mApp.block('#m_blockui_2_portlet', {
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Please wait...'
    });

    setTimeout(function () {
        mApp.unblock('#m_blockui_2_portlet');
    }, 500);
}

function noImage(id) {
    $('.reviewer-img' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}