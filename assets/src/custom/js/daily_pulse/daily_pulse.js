function updateEscalation(element, callback){
    $.when(fetchPostData({
        escalationId: $('#' + $(element).data('columnid')).data('escalationid'),
        columnAttribute: $('#' + $(element).data('columnid')).data('tableattrib'),
        value: $.trim($(element).val())
    }, '/dailypulse/update_escalation')).then(function (update) {
        var updateStat = $.parseJSON(update.trim());
        callback(updateStat);
    })
}

function updateTextAreaChange(element){
    // console.log($('#textAreaField').val());
    // if ($.trim($('#textAreaField').val()) == "") {
    //         console.log('if');
    //     $('.popoverErr').text($(element).data('column')+" should not be empty");
    //     $('.submitBtn').addClass('disabled');
    // } else {
    //         console.log('else');
        if ($.trim($('#textAreaField').val()) == $.trim($(element).data('origval'))) {
            $('.popoverErr').text("No changes made for "+$(element).data('column'));
            $('.submitBtn').prop('disabled', true);
            $('.submitBtn').addClass('disabled');
        } else {
            $(element).val($.trim($('#textAreaField').val()));
            updateEscalation(element, function (updateStat) {
                if (parseInt(updateStat)) {
                    if ($.trim($('#textAreaField').val()) == "") {
                        $('#' + $(element).data('columnid')).text("-");

                    }else{
                        $('#' + $(element).data('columnid')).text($.trim($('#textAreaField').val()));
                    }
                    $('#' + $(element).data('columnid')).data('contentvalue', $.trim($('#textAreaField').val()));
                    toastr.success('Successfully updated value of "' + $(element).data('column') + '"');
                } else {
                    toastr.error('Something went wrong while updating the "' + $(element).data('column') + '". Please inform your system administrator');
                }
                (($('[data-toggle="popover"],[data-original-title]').popover('dispose').data('bs.popover') || {}).inState || {}).click = false
            });
        }
    // }
}

function textAreaChange(element){// will just hide change the save button accessibility
    // console.log(origValue);
    // if ($.trim($(element).val()) == "") {
    //     $('.submitBtn').prop('disabled', true);
    //     $('.submitBtn').addClass('disabled');
    // }else{
        // console.log($.trim($(element).val()));
        // console.log(origValue);
        if ($.trim($(element).val()) == $.trim($(element).data('origval'))) {
            $('.submitBtn').prop('disabled', true);
            $('.submitBtn').addClass('disabled');
        }else{
            $('.submitBtn').prop('disabled', false);
            $('.submitBtn').removeClass('disabled');
        }
    // }
}

function selectChange(element){//Automatically update in every change
    // data-column="Follow Up Question" id="question' + value.escalation_ID + '" data-escalationid="' + value.escalation_ID + '" data-tableattrib="followUpQuestion" data-contentvalue="' + value.followUpQuestion + '"
    if ($(element).val() == $(element).data('origval')) {
        toastr.info('No Changes made for "' + $(element).data('columnid') + '"');
    } else {
        updateEscalation(element, function(updateStat){
            if(parseInt(updateStat)){
                $('#' + $(element).data('columnid')).text($(element).find('option:selected').text());
                $('#' + $(element).data('columnid')).data('contentvalue', $(element).val());
                toastr.success('Successfully updated value of "' + $(element).data('column') + '"');
            }else{
                toastr.error('Something went wrong while updating the "' + $(element).data('column') + '". Please inform your system administrator');
            }
        });
        (($('[data-toggle="popover"],[data-original-title]').popover('dispose').data('bs.popover') || {}).inState || {}).click = false
    }
}
function textAreaFieldPopup(element){
    var text = "";
    var popoverBtn = '<button type="button" class="submitBtn btn btn-success m-btn m-btn--icon m-btn--icon-only mr-1 disabled"  disabled data-column="' + $(element).data('column') + '" data-columnid="' + $(element).attr('id') + '" data-origval="' + $(element).data('contentvalue') + '" onclick="updateTextAreaChange(this)">' +
                                '<i class="fa fa-check"></i>'+
                            '</button>'+
                            '<button type="button" class="cancelBtn btn btn-danger m-btn m-btn--icon m-btn--icon-only" onclick="closePoper()">' +
                                '<i class="fa fa-close"></i>' +
                            '</button>';
    var oneFieldPopoverHead = '<div class="col-md-12">' +
                                    '<div class="form-group m-form__group row" style="margin-bottom: -7px;">' +
                                        '<div class="col-lg-12 mb-1 popoverErr" style="color:#ff7d7d; font-size: 11px; display:none"></div>' +
                                        '<div class="col-12 pt-0 pl-1 pr-1 mb-3">';
    var oneFieldPopoverFoot =  '</div>'+
                                    '<div class="col p-0">' +
                                        '<p class="pull-right">' +
                                            popoverBtn +
                                        '</p>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'; 
    if ($(element).data('contentvalue') != null){
        text = $(element).data('contentvalue');
    }
    var oneTimeField = '<textarea class="form-control m-input m-input--air" name="textAreaField" data-column="' + $(element).data('column') + '" data-columnid="' + $(element).attr('id') + '" data-origval="' + $(element).data('contentvalue') + '" id="textAreaField" onkeyup="textAreaChange(this)">' + text + '</textarea>';
    var oneTimeFieldPopoverBody = oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot;
    $(element).popover({
         title: $(element).data('column'),
         html: true,
         container: 'body',
         animation: true,
         content: oneTimeFieldPopoverBody,
         trigger: 'click',
    }).click(function (event) {
        $(element).popover('show');
    });
}

function selectPopup(element){
    var optionNo = "";
    var optionYes = "";
    if ($(element).data('contentvalue') == 1){
        optionYes = "selected";
    }else{
        optionNo = "selected";
    }

    var oneFieldPopoverHead = '<div class="col-md-12">' +
                                    '<div class="form-group m-form__group row" style="margin-bottom: -7px;">' +
                                        '<div class="col-lg-12 mb-1 popoverErr" style="color:#ff7d7d; font-size: 11px; display:none"></div>' ;
    var oneTimeField = '<div class="col-lg-12 mb-3 pl-1 pr-1">' +
                            '<select class="custom-select form-control" name="selectField" id="selectField" data-column="' + $(element).data('column') + '" data-columnid="' + $(element).attr('id') + '" data-origval="' + $(element).data('contentvalue') + '" onchange="selectChange(this)">' +
                                '<option value="1" '+optionYes+'>Yes</option>'+
                                '<option value="0" '+optionNo+'>No</option>'+
                            '</select>'+
                        '</div>'+
                        '</div></div>';
    var oneTimeFieldPopoverBody =oneFieldPopoverHead + oneTimeField ;
    $(element).popover({
         title: $(element).data('column'),
         html: true,
         container: 'body',
         animation: true,
         content: oneTimeFieldPopoverBody,
         trigger: 'click',
    }).click(function (event) {
        $(element).popover('show');
    });
}

function selectPopupIntervention(element){
    var optionNone = "";
    var level1 = "";
    var level2 = "";
    var level3 = "";
    var level4 = "";
    var level5 = "";
    if (parseInt($(element).data('contentvalue')) == 0){
        optionNone = "selected";
    } else if (parseInt($(element).data('contentvalue')) == 1) {
        level1 = "selected";
    } else if (parseInt($(element).data('contentvalue')) == 2) {
        level2 = "selected";
    } else if (parseInt($(element).data('contentvalue')) == 3) {
        level3 = "selected";
    } else if (parseInt($(element).data('contentvalue')) == 4) {
        level4 = "selected";
    } else if (parseInt($(element).data('contentvalue')) == 5) {
        level5 = "selected";
    }
    var oneFieldPopoverHead = '<div class="col-md-12">' +
                                    '<div class="form-group m-form__group row" style="margin-bottom: -7px;">' +
                                        '<div class="col-lg-12 mb-1 popoverErr" style="color:#ff7d7d; font-size: 11px; display:none"></div>' +
                                        '<div class="col-12 pt-0 pl-1 pr-1 mb-3">';
    var oneFieldPopoverFoot =  '</div>'+
                                '</div>'+
                            '</div>'; 
    var oneTimeField = '<select class="custom-select form-control" id="selectField" name="selectField" data-column="' + $(element).data('column') + '" data-columnid="' + $(element).attr('id') + '" id="selectField" data-origval="' + $(element).data('contentvalue') + '" onchange="selectChange(this)">' +
                            '<option value="0" '+optionNone+'>None</option>'+
                            '<option value="1" ' + level1 + '>Supervisor Level 1</option>' +
                            '<option value="2" ' + level2 + '>Supervisor Level 2</option>' +
                            '<option value="3" ' + level3 + '>Supervisor Level 3</option>' +
                            '<option value="4" ' + level4 + '>HR Manager</option>' +
                            '<option value="5" ' + level5 + '>Managing Director</option>' +
                        '</select>';
    var oneTimeFieldPopoverBody =oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot;
    $(element).popover({
         title: $(element).data('column'),
         html: true,
         container: 'body',
         animation: true,
         content: oneTimeFieldPopoverBody,
         trigger: 'click',
    }).click(function (event) {
        $(element).popover('show');
    });
}

function initEscalationTable(callback){
    console.log($('#choiceSelection').val());
    $.when(fetchPostData({
        choiceId: $('#choiceSelection').val(),
    }, '/dailypulse/get_choice_escalation_table')).then(function (escalation) {
        var escalationObj = $.parseJSON(escalation.trim());
        var tableRow = "";
        if (parseInt(escalationObj.exist)){
            $.each(escalationObj.record, function(index, value){
                var question = "-";
                var message = "-";
                var intervention = "None";
                var confetti = "No";
                var icon = "No";
                if(value.followUpQuestion != null){
                    question = value.followUpQuestion;
                }
                if (value.message != null){
                    message = value.message;
                }
                if (parseInt(value.intervention) != 0){
                    intervention = value.intervention;
                    if (parseInt(value.intervention) < 4){
                        intervention = "Supervisor Level "+value.intervention;
                    } else if (parseInt(value.intervention) == 4){
                        intervention = "HR Manager";
                    } else if (parseInt(value.intervention) == 5) {
                        intervention = "Managing Director";
                    }
                }
                if (parseInt(value.isConfetti) != 0){
                    confetti = "Yes";
                }
                if (parseInt(value.isIcon) != 0){
                    icon = "Yes";
                }
                tableRow += '<tr id="row' + value.escalation_ID + '">' +
                    '<th class="text-capitalize text-center first-col col">'+value.occurence+
                        '<button type="button" class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air pull-right" style="width: 22px !important;height: 22px !important; " data-escalationid="' + value.escalation_ID + '" data-occurencenum="' + value.occurence + '" onclick="confirmRemove(this)"><i class="fa fa-times" style="color: #bbbaba;"></i></button>' +
                    '</th>'+
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="Follow Up Question" id="question' + value.escalation_ID + '" data-escalationid="' + value.escalation_ID + '" data-tableattrib="followUpQuestion" data-contentvalue="' + value.followUpQuestion + '" onmouseover="textAreaFieldPopup(this)">' + question + '</span></td>' +
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="Message" id="message' + value.escalation_ID + '" data-escalationid="' + value.escalation_ID + '" data-tableattrib="message" data-contentvalue="' + value.message + '" onmouseover="textAreaFieldPopup(this)">' + message + '</span></td>' +
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="Intervention" id="intervention' + value.escalation_ID + '" data-escalationid="' + value.escalation_ID + '" data-tableattrib="intervention" data-contentvalue="' + value.intervention + '" onmouseover="selectPopupIntervention(this)">' + intervention + '</span></td>' +
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="With Confetti?" id="confetti' + value.escalation_ID + '" data-escalationid="' + value.escalation_ID + '" data-tableattrib="isConfetti" data-contentvalue="' + value.isConfetti + '" onmouseover="selectPopup(this)">' + confetti + '</span></td>' +
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="With Icon?" id="icon' + value.escalation_ID + '" data-escalationid="' + value.escalation_ID + '" data-tableattrib="isIcon" data-contentvalue="' + value.isIcon + '" onmouseover="selectPopup(this)">' + icon + '</span></td>' +
                    // '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px">2 months</td>'+
                    // '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px">2 months</td>'+
                    // '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px">2 months</td>'+
                '</tr>';
            })
            $('#ecalationTableBody').html(tableRow);
            $('#noEscalationSettings').hide();
            $('#escalationTable').fadeIn();
        }else{
            $('#noEscalationSettings').fadeIn();
            $('#escalationTable').hide();
        }
        callback(escalationObj);
        init_resizable();
    })
}

function snapszChoices(callback){
    $.when(fetchGetData('/dailypulse/fetch_snapsz_choices')).then(function (choices) {
        var choicesObj = $.parseJSON(choices.trim());
        var optionAsText = "";
        if (parseInt(choicesObj.exist)){
            $.each(choicesObj.record, function(index, value){
                var selectedText = "";
                if (index == 0){
                    selectedText = "selected";
                }
                optionAsText += "<option value='" + value.choiceId + "' " + selectedText + ">" + value.label + "</option>";
            })
        }
        $('#choiceSelection').html(optionAsText);
        callback(choicesObj);
    });
}

function addEscalation(callback) {
    $.when(fetchPostData({
        choiceId: $('#choiceSelection').val(),
    }, '/dailypulse/new_escalation')).then(function (newEscalation) {
        var newEscalationObj = $.parseJSON(newEscalation.trim());
        if (parseInt(newEscalationObj.add_stat) != 0){
            tableRow = '<tr id="row' + newEscalationObj.add_stat + '">' +
                    '<th class="text-capitalize text-center first-col col">' + newEscalationObj.occurence_num +
                    '<button type="button" class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air pull-right" style="width: 22px !important;height: 22px !important;" data-escalationid="' + newEscalationObj.add_stat + '" data-occurencenum="' + newEscalationObj.occurence_num + '" onclick="confirmRemove(this)"><i class="fa fa-times" style="color: #bbbaba;"></i></button>' +
                    '</th>' +
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="Follow Up Question" id="question' + newEscalationObj.add_stat + '" data-escalationid="' + newEscalationObj.add_stat + '" data-tableattrib="followUpQuestion" data-contentvalue="' + null + '" onmouseover="textAreaFieldPopup(this)">-</span></td>' +
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="Message" id="message' + newEscalationObj.add_stat + '" data-escalationid="' + newEscalationObj.add_stat + '" data-tableattrib="message" data-contentvalue="' + null + '" onmouseover="textAreaFieldPopup(this)">-</span></td>' +
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="Intervention" id="intervention' + newEscalationObj.add_stat + '" data-escalationid="' + newEscalationObj.add_stat + '" data-tableattrib="intervention" data-contentvalue="' + 0 + '" onmouseover="selectPopupIntervention(this)">None</span></td>' +
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="With Confetti?" id="confetti' + newEscalationObj.add_stat + '" data-escalationid="' + newEscalationObj.add_stat + '" data-tableattrib="isConfetti" data-contentvalue="' + 0 + '" onmouseover="selectPopup(this)">No</span></td>' +
                    '<td class="text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><span class="editableStyle" data-column="With Icon?" id="icon' + newEscalationObj.add_stat + '" data-escalationid="' + newEscalationObj.add_stat + '" data-tableattrib="isIcon" data-contentvalue="' + 0 + '" onmouseover="selectPopup(this)">No</span></td>' +
                '</tr>';
            $('#ecalationTableBody').append(tableRow);
        }else{
            toastr.error('Something went wrong while adding a new occurence. Please inform your system administrator');
        }
    })
}

function deleteEscalationOccurence(element){
    $.when(fetchPostData({
        escalationId: $(element).data('escalationid'),
        occurenceNum: $(element).data('occurencenum'),
        choiceId: $('#choiceSelection').val(),
    }, '/dailypulse/remove_specific_escalation')).then(function (remove) {
        var removeStat = $.parseJSON(remove.trim());
        if (parseInt(removeStat.delete_record_exist)){
            if (parseInt(removeStat.delete_escalation)){
                toastr.success('Successfully Removed Occurence Settings');
            }else{
                toastr.error('Something went wrong while deleting the record. Please inform your system administrator');
            }
        }else{
            toastr.error('The record you are about to remove does not exist. Please inform your system administrator');
        }
        initEscalationTable(function (escalationObj) {});
    });
}

function confirmRemove(element){
    console.log($(element).data('escalationid'));
    console.log($('#choiceSelection').find('option:selected').text());
    $.when(fetchPostData({
        escalationId: $(element).data('escalationid'),
    }, '/dailypulse/check_if_occurence_exist')).then(function (exist) {
        var existStat = $.parseJSON(exist.trim());
        if (parseInt(existStat.exist)) {
            swal({
                title: 'Are you sure you want to remove Occurence No. ' + $(element).data('occurencenum') + ' settings of choice ' + $('#choiceSelection').find('option:selected').text() + '?',
                text: "You will not be able to revert back this occurence settings after removal.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete it!',
                cancelButtonText: 'No, Don\'t Delete',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    deleteEscalationOccurence(element);
                } else if (result.dismiss === 'cancel') {
                }
            });
        } else {
            toastr.error('The record you are about to remove does not exist. Please inform your system administrator');
            initEscalationTable(function (escalationObj) {});
        }
    });
}

function init_resizable() {
    $(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
    var thHeight = $("table#escalationTable th:first").height();
    $(".resizable-header").resizable({
        handles: "e",
        minHeight: thHeight,
        maxHeight: thHeight,
        minWidth: 5,
        resize: function (event, ui) {
            var sizerID = "#" + $(event.target).find('div').attr('id');
            $(sizerID).width(ui.size.width);
        }
    });
}

var BootstrapSelect = function () {
    var options = function (elementId, titleVal) {
        $(elementId).selectpicker({
            title: titleVal,
            size: 'auto'
        });
    }
    return {
        init: function (elementId, titleVal) {
            options(elementId, titleVal);
        }
    };
}();

function closePoper() {
    $('[data-toggle="popover"],[data-original-title]').popover('hide')
}

$(document).on('click', function (e) {
    $('[data-toggle="popover"],[data-original-title]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            (($(this).popover('dispose').data('bs.popover') || {}).inState || {}).click = false // fix for BS 3.3.6
        }
    });
});

$('[data-toggle="popover"],[data-original-title]').on('hidden.bs.popover', function () {
    (($(this).popover('dispose').data('bs.popover') || {}).inState || {}).click = false;
})

$('#choiceSelection').on('change', function(){
    initEscalationTable(function (escalationObj) {});
})

$(function () {
    snapszChoices(function(choicesObj){
        if (parseInt(choicesObj.exist)) {
            BootstrapSelect.init('#choiceSelection', 'Checklist Status');
            initEscalationTable(function(escalationObj){});
        }
    });
})