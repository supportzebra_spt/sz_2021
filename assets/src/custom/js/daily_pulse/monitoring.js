function humanTimeFormat(hours, callback) {
    var totalHours = parseInt(hours);
    var totalMinutes = Math.round(((hours - totalHours) * 60).toFixed(2));
    var minInt = parseInt(((hours - totalHours) * 60).toFixed(2));
    var minRaw = ((hours - totalHours) * 60).toFixed(2);
    var totalSeconds = Math.round(((minRaw - minInt) * 60).toFixed(2));
    totalMinutes = minInt;

    // console.log(moment.duration(hours, "hours").humanize(false))

    // console.log("raw: " + minRaw + " | int: " + minInt);
    // console.log(totalSeconds);
    

    var totalTime = '';
    if (totalMinutes == 60) {
        totalMinutes = 0;
        totalHours++;
    }
     if (totalSeconds == 60) {
         totalSeconds = 0;
         totalMinutes++;
     }
    var totalTimeArr = [];
    var totalTimeCount = 0;

    if(totalHours > 0){
        if (totalHours == 1){
            totalTimeArr[totalTimeCount] = totalHours + " hr";
        }else{
            totalTimeArr[totalTimeCount] = totalHours + " hrs";
        }
        totalTimeCount++;
    }
    if (totalMinutes > 0){
         if (totalMinutes == 1) {
             totalTimeArr[totalTimeCount] = totalMinutes + " min";
         } else {
             totalTimeArr[totalTimeCount] = totalMinutes + " mins";
         }
         totalTimeCount++
    }
    if (totalSeconds > 0) {
         if (totalSeconds == 1) {
             totalTimeArr[totalTimeCount] = totalSeconds + " sec";
         } else {
             totalTimeArr[totalTimeCount] = totalSeconds + " secs";
         }
         totalTimeCount++;
    }
    totalTime = totalTimeArr.join(", ");
    callback(totalTime);
}

var dailyPulseDatatable = function () {
    var dailyPulse = function (start, end) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/dailypulse/get_daily_pulse_record_datatable",
                        params: {
                            query: {
                                startDate: start,
                                endDate: end,
                                statusId: $("#pulseRecordStatus").val(),
                                empId: $("#empSelectPulseRecord").val(),
                                pulseRecordSearch: $("#pulseRecordSearch").val(),
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#pulseRecordSearch'),
            },
            // columns definition
            columns: [
                {
                    field: "answerId",
                    title: "Pulse ID",
                    width: 80,
                    selector: false,
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return row.answerId.padLeft(8);
                    }
                }, {
                    field: 'sched_date',
                    width: 80,
                    title: 'Date',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                        return html;
                    },
                }, {
                    field: 'empEmp_id',
                    width: 180,
                    title: 'Respondent',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        // var picName = row.appEmpPic.substr(0, row.appEmpPic.indexOf(".jpg"))
                        // var pic = imagePath + "" + picName + "_thumbnail.jpg";
                        // var elementId = "#pulseRecord" + row.answerId + "";
                        // var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.empFname + ' ' + row.empLname + '</div>';
                        // return html;
                        return row.empFname + ' ' + row.empLname;
                    },
                }, {
                    field: 'interveneLevel',
                    width: 60,
                    title: 'Level',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',

                }, {
                    field: 'interveneEmp_id',
                    width: 180,
                    title: 'Intervenor',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        // var picName = row.picIntervene.substr(0, row.picIntervene.indexOf(".jpg"))
                        // var pic = imagePath + "" + picName + "_thumbnail.jpg";
                        // var elementId = "#pulseRecordIntervenor" + row.answerId;
                        // var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.interveneFname + ' ' + row.interveneLname + '</div>';
                        // return html;
                        return row.interveneFname + ' ' + row.interveneLname;
                    },
                }, {
                    field: 'deadline',
                    width: 90,
                    title: 'Intervention',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var interventionDate = row.interventionDate;
                        var dateIntervened = row.dateIntervened;
                        var interventionTime = "--";
                        if (parseInt(row.status_ID) == 3){
                            interTime = (moment(dateIntervened, "YYYY-MM-DD HH:mm:ss").valueOf() - moment(interventionDate, "YYYY-MM-DD HH:mm:ss").valueOf()) / 3600000;
                            humanTimeFormat(interTime, function (humanTimeFormat) {
                              interventionTime =  humanTimeFormat;
                            });
                        }
                        return interventionTime;
                    },
                }, {
                    field: 'status_ID',
                    width: 80,
                    title: 'Status',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        if (parseInt(row.status_ID) == 12) {
                            var badgeColor = 'btn-warning';
                        } else if (parseInt(row.status_ID) == 3) {
                            var badgeColor = 'btn-success';
                            var dotAlert = "";
                        }
                        var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + "' onclick='approverInfo(" + row.additionalHourRequestId + ")' style='font-size: 11px;'>" + row.description + "</span>";
                        return html;
                    },
                }
            ],
        };
        var datatable = $('#recordsPulseDatatable').mDatatable(options);
    };
    return {
        init: function (start, end) {
            dailyPulse(start, end);
        }
    };
}();

function init_select2(obj, elementId, label, callback) {
    var optionsAsString = "<option value='0'>ALL</option>";
    if (parseInt(obj.exist)) {
        $.each(obj.record, function (index, value) {
            console.log(value.emp_id);
            optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.lname + ", " + value.fname) + "</option>";
        });
    }
    console.log
    $(elementId).html(optionsAsString);
    $(elementId).select2({
        width: '100%'
    }).trigger('change');

    var selectId = elementId.replace('#', '');
    $('#select2-' + selectId + '-container').parents('.select2-container').tooltip({
        title: label,
        placement: "top"
    });
    callback(obj);
}

function initEmployeeRecorName(start, end, callback) {
    $.when(fetchPostData({
        startDate: start,
        endDate: end,
        statusId: $("#pulseRecordStatus").val()
    }, '/dailypulse/get_record_emp_name')).then(function (empName) {
        var empNameObj = $.parseJSON(empName.trim());
        callback(empNameObj);
    })
}

function initEmployeeRecorNameField(start, end, callback) {
    initEmployeeRecorName(start, end, function (empNameObj) {
        init_select2(empNameObj, "#empSelectPulseRecord", "Select a Respondent", function(obj){
            callback(obj);
        });
    });
}

function initPulseRecordDatatable(start, end) {
    $('#recordsPulseDatatable').mDatatable('destroy');
    dailyPulseDatatable.init(start, end);
}

function initPulseRecordDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#pulseRecordDateRange .form-control').val(start + ' - ' + end);
        $('#pulseRecordDateRange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#pulseRecordDateRange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initEmployeeRecorNameField(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function(obj){
                initPulseRecordDatatable(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'));
            });
            // initIrAbleDtrVioRecord(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
        });
        var dateRanges = {
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        };
        callback(dateRanges);
    });
}

function initRecordsTab(){
    initPulseRecordDateRangePicker(function (dateRanges) {
        initEmployeeRecorNameField(dateRanges.start, dateRanges.end, function (obj) {
            initPulseRecordDatatable(dateRanges.start, dateRanges.end);
        });
    });
}

$('#pulseRecordStatus, #empSelectPulseRecord, #pulseRecordSearch').on('change', function () {
    var dateRange = $('#pulseRecordDateRange').val().split('-');
    initPulseRecordDatatable(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
})



$(function () {
    initRecordsTab();
})