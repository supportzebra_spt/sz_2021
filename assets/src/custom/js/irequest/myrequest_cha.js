// ON LOAD 
$(function() {
    var btn_formanpower="";
    btn_formanpower += '<button id="manpower_Btnid" class="btn btn-primary m-btn m-btn--icon manpowerbtnid" data-toggle="modal"data-target="#form_irequest">';
    btn_formanpower += '<span> <i class="fa fa-plus"></i><span>Manpower Request</span></span></button>';
    $("#manpower_btndiv").html(btn_formanpower);
    $("#label_req").text("My Requests");

    initRecordDateRangePicker_mypersonal_ireq(function (dateRanges) {
        initpending_irequest(dateRanges.start, dateRanges.end,1);
        filters_myreq_pending(1);
    });
    $('#my_pendingposition,#my_pendingdepartment, #my_manreqposition, #my_manreqdepartment, #my_manreqstatus, #my_pending_stats').select2({
        placeholder: "-Select-",
        width: '100%'
    });
    // cronjob_missed();
})
function cronjob_missed(){
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/test1",
        cache: false,
        success: function(res) {
            // console.log("success");
            var result = JSON.parse(res);
            console.log(result);
            console.log("SEE");
        }
    });
}
// ON EVENT 
$("#irequest_pending_col").on('click','.view_pendingreq', function(){
    var id_ireq = $(this).data('id');
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/manpowerdetails",
        data: {
            ireq: id_ireq,
            stat: 1,
            requestor: 1,
            approver: 0,
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var stringx_attachment="";
            var btn_appdisapprove="";
            var stringx_approver="";
            stringx_approver += '<div class="row">';
            stringx_approver += '<div class="col-12">';
            stringx_approver += '<button data-id="'+id_ireq+'" type="button" class="btn btn-link view_approvers_irqdisplay">Click to View Approver Details</button>';
            stringx_approver += '</div>';
            stringx_approver += '</div>';

            btn_appdisapprove += '<button type="button" class="btn btn-secondary" data-dismiss="modal">';
            btn_appdisapprove += '<span><i class="fa fa-close"></i><span> Cancel</span></span>';
            btn_appdisapprove += '</button>';

            $("#irequest_displaydetails").modal("show");
            $("#approvdis_btn").html(btn_appdisapprove);
            $("#ireq_id_display").text("00"+result[0].irequest_ID);
            $("#ireq_pos_display").text(result[0].pos_details);
            $("#ireq_reqdept_display").text(result[0].acc_name);
            $("#ireq_manpower_display").text(result[0].manpower_requested);
            $("#ireq_edreq_display").text(result[0].educationalAttainment);
            $("#ireq_gender_display").text(result[0].gender);
            $("#ireq_bdesc_display").text(result[0].description);
            $("#ireq_prefqual_display").text(result[0].qualifications);
            $("#ireq_hpurpose_display").text(result[0].hpurpose);
            $("#ireq_tHiring_display").text(result[0].hiringtype_desc);

            // $("#ireq_dateduration_display").text(S_date + " - " + E_date);
            $("#ireq_dateduration_display").text(result[0].days_needed);
            $("#moredetails_irq").html(stringx_approver);

            if(result[0].status==2){
                $("#ireq_status_display").text("PENDING FOR APPROVAL");
                $("#color_change_dis").addClass("pending_color");
                $("#color_change_dis").removeClass("disapproved_color");
                $("#color_change_dis").removeClass("approved_color");
                $("#color_change_dis").removeClass("missed_color");
            }else{
                $("#ireq_status_display").text("MISSED REQUEST");
                $("#color_change_dis").addClass("missed_color");
                $("#color_change_dis").removeClass("pending_color");
                $("#color_change_dis").removeClass("approved_color");
                $("#color_change_dis").removeClass("disapproved_color");
            }

            if(result[0].descriptionAttachment!=null){
                var atf=result[0].descriptionAttachment;
                var atf2 = atf.replace("uploads/irequest/job_description/", " ");
                stringx_attachment+='<div class="col-xl-4 col-sm-12"><label class="w_color_label">Attached Job Description </label></div>';
                stringx_attachment+='<div class="col-xl-7 col-sm-12" style="padding-left:2em">';
                stringx_attachment+='<div class="row">';
                stringx_attachment+='<div class="col-12" style="color:darkcyan;font-weight:500"><span>'+atf2+'</span></div>';
                stringx_attachment+='<div class="col-12" style="">';
                stringx_attachment+="<a href='"+baseUrl +"/"+atf+"' class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' download='' style='background: lavender;'><span><i class='fa fa-download'></i>&nbsp;Download Attached File</span></a></div>";
                stringx_attachment+='</div>';
                stringx_attachment+='</div>';  
                $("#ireq_attachedfile_display").html(stringx_attachment);
            }
        }
    });
});
//REMOVE
$("#irequest_pending_col").on('click','.remove_irequest', function(){
    let id_ireq=$(this).data('id');
    let stat=$(this).data('stat');
    swal({
        title: 'Are you sure you want to cancel this Manpower Request?',
        text: 'Please review this pending request. It will not be retrieved once cancelled.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Cancel Request'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: baseUrl + "/irequest/cancel_manpowerrequest",
                data: {
                    ireq: id_ireq,
                    status: stat
                },
                cache: false,
                success: function(res) {
                    var result = JSON.parse(res);
                    if(result==1){
                        swal(
                            'Deleted!',
                            'Pending Manpower Request has been cancelled.',
                            'success'
                        )
                        initRecordDateRangePicker_mypersonal_ireq(function (dateRanges) {
                            initpending_irequest(dateRanges.start, dateRanges.end,1);
                            filters_myreq_pending(1);
                        });
                    }else{
                        swal(
                            'Cancelled',
                            'You cannot remove this Pending Manpower Request because it was already approved by other approver/s.',
                            'error'
                        )
                    }
                },
                error: function(res) {
                    swal(
                        'Oops!',
                        'Something is wrong with your code!',
                        'error'
                    )
                }
            });
        } else if (result.dismiss === 'cancel') {
            swal(
                'Cancelled',
                'Removing of Pending Manpower Request has been cancelled.',
                'error'
            )
        }
    });
});
function disp_approvers(ireq_ID){
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/fetch_approversdisplay",
        data: {
            ireq: ireq_ID,
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var stringxx="";
            var stat="";
            var backcolor ="";
            var statlabel ="";
            var iconz ="";
            var colorz ="";
            var remarks ="No remarks yet.";
           
            $.each(result, function(key, item) {
                var sincedate = moment(item.dated_status, 'Y-MM-DD').format('MMM DD, Y');

                // if(item.remarks!=null||item.remarks!=""){
                //     remarks=item.remarks;
                // }else{
                //     remarks="No remarks";
                // }
                if(item.status==4){
                    stat ="Current Approver since "+sincedate;
                    backcolor = "background: #b5e7ec";
                    statlabel = "Current";
                    iconz = "fa-spinner";
                    colorz = "btn-info";  
                }else if(item.status == 2){
                    stat ="Pending since " + sincedate;
                    backcolor = "background: #f9db9d";
                    statlabel = "Pending";
                    iconz = "fa-spinner";
                    colorz = "btn-warning";
                    remarks="No Remarks";
                }else if(item.status == 5){
                    stat = "Approved last " + sincedate;
                    backcolor = "background: darkseagreen";
                    statlabel = "Approved";
                    iconz = "fa-thumbs-o-up";
                    colorz = "btn-success";
                    remarks = item.remarks;
                }else if(item.status == 6){
                    stat = "Disapproved last " +sincedate;
                    backcolor = "background: #ffb9c5";
                    statlabel = "Disapproved";
                    iconz = "fa-thumbs-o-down";
                    colorz = "btn-danger";
                    remarks = item.remarks;                    
                }else if(item.status == 12){
                    stat = "Missed last " +sincedate;
                    backcolor = "background: #ffb9c5";
                    statlabel = "Missed";
                    iconz = "fa-exclamation-circle";
                    colorz = "btn-warning";
                    remarks = item.remarks; 
                }else{
                    stat = "Cancelled last " +sincedate;
                    backcolor = "background: #ffb9c5";
                    statlabel = "Cancelled";
                    iconz = "fa-remove";
                    colorz = "btn-danger";
                    remarks = item.remarks; 
                }
                stringxx +='<div class="m-portlet bg-secondary">';
                stringxx +='<div class="col-md-12 pt-2 pb-2" style="background:#c5d2d4"> <div class="row">';
                stringxx +='<div class="col-7 col-sm-7 col-md-7 pt-2"><strong>Recommender '+item.level+'</strong></div>';
                stringxx +='<div class="col-5 col-sm-5 col-md-5">';
                stringxx +=' <div class="btn pull-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;background: #505a6b;">';
                stringxx +='<div class="btn '+colorz+' m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air diringapart" style="width: 25px !important;height: 25px !important;">';
                stringxx +='<i class="fa '+iconz+'" style="font-size: 17 px;"></i></div>';
                stringxx +='<span class="button-content text-light text-capitalize pl-2">'+statlabel+'</span>';
                stringxx +='</div></div></div></div>';
                stringxx +='<div class="m-portlet__body pt-3 pl-4 pb-3 pr-4">';
                stringxx +='<div class="row">';
                stringxx +='<div class="col-2 col-sm-2 col-md-2 d-none d-sm-block">';
                stringxx +='<img src="http://localhost/sz/assets/images/img/sz.png" onerror="noImageFound(this)" width="58" alt="" class="mx-auto rounded-circle">';
                stringxx +='</div>';
                stringxx +='<div class="col-12 col-sm-10 col-md-10 pl-1">';
                stringxx +='<div class="col-md-12 font-weight-bold" style="font-size: 15px;">';
                stringxx +=''+item.fname+' '+item.lname+'';
                stringxx +='</div>';
                stringxx +='<div class="col-md-12" style="font-size: 11px;margin-top: -3px;font-weight: bolder; color: #179e9e;">'
                stringxx +=' '+stat+'</div>';
                stringxx +=' <div class="col-md-12 mt-3" style="font-size: 12px;"><i> - '+remarks+'.</i></div>';
                stringxx +='</div>';
                stringxx +='<div class="col-md-12 pt-3"></div>';
                stringxx +='</div></div> </div>';
            });
            $("#approverlist_disp").html(stringxx);
        }
    });
}

// PENDING TAB 
$("#tab_mypendingireq").on('click', function(){
    initRecordDateRangePicker_mypersonal_ireq(function (dateRanges) {
        initpending_irequest(dateRanges.start, dateRanges.end, 1);
        filters_myreq_pending(1);
    });
});


$("#my_pendingposition,#my_pendingdepartment, #my_pending_id, #my_pending_stats").on('change', function(){
    var dateRange = $('#recordsall-date_mypendingireq').val().split('-');
    initpending_irequest(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'),1);
});

$('#form_manpower')
    .find('[name="irequest_purp_ofhiring"]')
    .change(function (e) {
        $('#form_manpower').formValidation('revalidateField', 'irequest_purp_ofhiring');
    })
    .end()
.formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    // live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        irequest_educationalreq: {
            validators: {
                // stringLength: {
                //     max: 45,
                //     message: 'The folder name must be less than 45 characters'
                // },
                notEmpty: {
                    message: 'Educational Requirement field is required!'
                },
            }
        },
        irequest_req_dept: {
            validators: {
                notEmpty: {
                    message: 'Please select requesting department!'
                },
            }
        },
        irequest_pos_title: {
            validators: {
                notEmpty: {
                    message: 'Please select position!'
                },
            }
        },
        irequest_daysneeded: {
            validators: {
                notEmpty: {
                    message: 'Please input days needed!'
                },
            },
            number: {
                number: true,
                message: 'Oops! Only digits are allowed.'
            }
        },
        irequest_manpower: {
            validators: {
                notEmpty: {
                    message: 'Please input manpower needed!'
                },
            },
            number: {
                number: true,
                message: 'Oops! Only digits are allowed.'
            }
        },
        irequest_descriptionduties: {
            validators: {
                notEmpty: {
                    message: 'Description is required!'
                },
            }
        },
        irequest_qualifications: {
            validators: {
                notEmpty: {
                    message: 'Preferred qualifications description is required!'
                },
            }
        },
        irequest_purp_ofhiring: {
            validators: {
                notEmpty: {
                    message: 'Please select purpose for hiring!'
                },
            }
        },
        hiringType: {
            validators: {
                notEmpty: {
                    message: 'Please check type of hiring!'
                },
            }
        },
        irequest_gender: {
            validators: {
                notEmpty: {
                    message: 'Please select gender!'
                },
            }
        }
    }
}).on('success.form.fv', function(e, data) {
    e.preventDefault();
    $('#form_manpower').formValidation('disableSubmitButtons', true);
    var frm = new FormData(this);  
    var typeofHiring_id = $('input[name="hiringType"]:checked').map(function() { return $(this).val().toString(); } ).get().join(",");
    var typeofHiring_desc = $('input[name="hiringType"]:checked').map(function() { return $(this).data("desc").toString(); } ).get().join("/");
    swal({
        title: 'Are you sure you want to submit this Manpower Request?',
        text: 'Please review all details. It will not be modified once submitted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Submit Request'
    }).then(function (result) {
            let pos_ID = $("#irequest_pos_title").val();
        if (result.value) {
            frm.append("req_department", $("#irequest_req_dept").val());
            frm.append("position_site", $("#Pos"+pos_ID).data("site"));
            frm.append("position", $("#irequest_pos_title").val());
            // frm.append("dateduration", $("#irequest_dateduration").val());
            frm.append("daysneeded", $("#irequest_daysneeded").val());
            frm.append("purpose_hiring", $("input[name='irequest_purp_ofhiring']:checked").val());
            frm.append("type_hiring", typeofHiring_id);
            frm.append("type_hiring_desc", typeofHiring_desc);
            frm.append("manpower", $("#irequest_manpower").val());
            frm.append("edrequirement", $("#irequest_educationalreq").val());
            frm.append("gender", $("input[name='irequest_gender']:checked").val());
            frm.append("brief_description", $("#irequest_descriptionduties").val());
            frm.append("qualifications", $("#irequest_qualifications").val());
            // frm.append("aftapproved", $("#irequest_aftapproved").val());
            frm.append("otherpurpose", $("#irequest_otherspurpose").val());
            frm.append("aftapproved", $("input[name='irequest_aftapproved']:checked").val());
            $.ajax({
                type: "POST",
                url: baseUrl + "/irequest/save_manpowerrequest",
                data: frm,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    var resObj = $.parseJSON(res.trim());
                    systemNotification(resObj.notifid.notif_id);
                    $('#form_irequest').modal('hide');
                    swal({
                            position: 'center',
                            type: 'success',
                            title: 'Successfully Submitted Manpower Request!',
                            showConfirmButton: false,
                            timer: 1500
                    });
                    initRecordDateRangePicker_mypersonal_ireq(function (dateRanges) {
                        initpending_irequest(dateRanges.start, dateRanges.end,1);
                        filters_myreq_pending(1);
                    });
                }

            });
        }
    })
});

$('#irequest_req_dept,#irequest_pos_title').select2({
    placeholder: "Select",
    width: '100%'
});
$('#irequest_pos_title').select2({
    placeholder: "-Select Requesting Team First-",
    width: '100%'
});

$("#manpower_btndiv").on("click",".manpowerbtnid", function(){
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/fetch_account_position_det",
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var stringx_acc = "<option disabled selected=''> - Select Team - </option>";
            var stringx_pos="<option disabled selected=''> - Select Position - </option>";
            var stringx_pur="";
            var stringx_type="";
            $.each(result.accounts, function (key, data) {
                stringx_acc += '<option value="'+data.acc_id+'">'+data.acc_name+'</option>';
            });
            $.each(result.positions, function (key, data) {
                stringx_pos += '<option value="'+data.pos_id+'">'+data.pos_details+'</option>';
            });
            $.each(result.purposes, function (key, data) {
                var cl_anotherfield="existing_optionpurp";
                var id_puthere="";
                if(data.hiringPurpose_ID==10){
                    cl_anotherfield="otherin_field";
                }else if(data.hiringPurpose_ID==1){
                    id_puthere='id="radio_defselect"';
                }
                stringx_pur += ' <label class="m-radio m-radio--state-success '+cl_anotherfield+'"><input '+id_puthere+' type="radio" name="irequest_purp_ofhiring" value="'+data.hiringPurpose_ID+'">'+data.purpose+'<span></span></label>';
            });
            $.each(result.hiring_type, function (key, data) {
                let type_Checked = "";
                stringx_type += '<label class="m-checkbox m-checkbox--state-success">';
                
                if(data.hiringType_ID ==1){
                    type_Checked = 'checked="checked"';
                }
                stringx_type += '<input '+type_Checked+' data-desc="'+data.description+'" name="hiringType" value="'+data.hiringType_ID+'" type="checkbox">'+data.description;
                stringx_type += '<span></span>';
                stringx_type += '</label>';
            });
            $("#irequest_req_dept").html(stringx_acc);
            $("#irequest_type_ofhiring").html(stringx_type);

            // $("#irequest_pos_title").html(stringx_pos);

            $("#irequest_purp_ofhiring").html(stringx_pur);
            $("#radio_defselect").prop("checked", true);
        }
    });
});
$("#irequest_dateduration").on("mouseover",function(){
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#irequest_dateduration').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        });
    });
});

$('#irequest_purp_ofhiring').on('click', '.otherin_field', function() {
    var strx="";
    strx+='<div class="form-group m-form__group" id="removethis_purp">';
    strx+='<input type="text" style="background:bisque;font-style:italic" class="form-control m-input" name="irequest_otherspurpose" id="irequest_otherspurpose" placeholder="Type other purpose of hiring here..."></div>';         
    $("#additional_field").html(strx);
});
$('#irequest_purp_ofhiring').on('click', '.existing_optionpurp', function() {
    $("#removethis_purp").remove();
});

$("#irequest_req_dept").on('change', function(){
    console.log($(this).val());
    let stringx_pos ="";
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/getpos_perdepartment",
        data: {
            department_id: $(this).val()
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            $.each(result, function (key, data) {
                stringx_pos += '<option id="Pos'+data.pos_id+'" data-site="'+data.site_ID+'" value="'+data.pos_id+'">'+data.pos_details+'</option>';
             });
             $("#irequest_pos_title").html(stringx_pos);
        }
    });
});

// FUNCTION CALLS 
function add_manpowerrequest(){
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/save_manpowerrequest",
        data: {
            req_department: $("#irequest_req_dept").val(),
            position: $("#irequest_pos_title").val(),
            // dateduration: $("#irequest_dateduration").val(),
            daysneeded: $("#irequest_daysneeded").val(),
            purpose_hiring: $("input[name='irequest_purp_ofhiring']:checked").val(),
            manpower: $("#irequest_manpower").val(),
            edrequirement: $("#irequest_educationalreq").val(),
            gender:  $("input[name='irequest_gender']:checked").val(),
            brief_description: $("#irequest_descriptionduties").val(),
            qualifications: $("#irequest_qualifications").val(),
            aftapproved: $("#irequest_aftapproved").val(),
            otherpurpose: $("#irequest_otherspurpose").val(),
            aftapproved: $("input[name='irequest_aftapproved']:checked").val()
        },
        cache: false,
        success: function(res) {
            $('#form_irequest').modal('hide');
            swal({
                    position: 'center',
                    type: 'success',
                    title: 'Successfully Submitted Manpower Request!',
                    showConfirmButton: false,
                    timer: 1500
            });
        }
    });
}
$('#form_irequest').on('hidden.bs.modal', function () {
    formReset('#form_manpower');
    clear_mrf_fields();
});
function clear_mrf_fields(){
    $("#irequest_otherspurpose").val("");
    $("#irequest_otherspurpose").slideUp();
    $("input[name='irequest_gender']").prop("checked", false);
    $("#irequest_aftapproved").prop("checked", false);
    $("#upload_attachment_jobdesc").val("");
    // $("#files_select_label_fileir").text("");
    $("#fileList_jobdesc").empty();
    $("#irequest_pos_title").empty();
    $("#display_div_attachment_jobdesc").hide();
}

function getattachment_forIrequest() {
    var input = document.getElementById("upload_attachment_jobdesc");
    var ul = document.getElementById("fileList_jobdesc");
    var maxsize = 5000000;
    var exceed = 0;
    $("#label_exceeded_jd").text("");
    $("#display_div_attachment_jobdesc").show();
    while (ul.hasChildNodes()) {
        ul.removeChild(ul.firstChild);
    }
    for (var i = 0; i < input.files.length; i++) {
        var li = document.createElement("li");
        li.innerHTML = input.files[i].name;
        var file_size = input.files[i].size;
        var g = input.files[i].name;
        var file_ext = g.split('.').pop();

        if (file_ext == "doc" || file_ext == "DOC" || file_ext == "docx" || file_ext == "pdf" || file_ext == "txt" || file_ext ==
            "xml" || file_ext == "xlsx" || file_ext == "xls" || file_ext == "xlsm" || file_ext == "ppt" || file_ext == "pptx" || file_ext == "csv" || file_ext == "xlxx") {

            if (file_size <= maxsize) {
                ul.appendChild(li);
                $("#fileList_jobdesc li").removeClass("included");
            } else {
                ul.appendChild(li);
                exceed = 1;
                $("#fileList_jobdesc li").addClass("not_included");
                $("#label_exceeded_jd").text("File/s exceeded to 5mb. Please upload another.");
            }
        } else {
            ul.appendChild(li);
            exceed = 1;
            $("#fileList_jobdesc li").addClass("not_included");
            $("#label_exceeded_jd").text("Invalid file type. Please reupload allowed file types.");
        }
    }
    if (!ul.hasChildNodes()) {
        var li = document.createElement("li");
        li.innerHTML = 'No Files Selected';
        ul.appendChild(li);
        $("#label_exceeded_jd").text("");
    }
    if (exceed) {
        $('#form_manpower').formValidation('disableSubmitButtons', true);
    } else {
        $('#form_manpower').formValidation('disableSubmitButtons', false);
    }
}
function filters_myreq_pending(fn){
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/filters_myreq_pending",
        data: {
            filter_no: fn,
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var stringx_position="";
            var stringx_account="";
            stringx_position += "<option disabled selected=''>-Select Position-</option>";
            stringx_position += "<option value='0'>All</option>";
            stringx_account += "<option disabled selected=''>-Select Requesting Department-</option>";;
            stringx_account += "<option value='0'>All</option>";
          
            
            $.each(result.position,function(key,data){
                stringx_position+="<option value=" + data.position_ID + ">" + data.pos_details + "</option>";
            });

            $.each(result.account,function(key,data){
                stringx_account+="<option value=" + data.requesting_dept + ">" + data.acc_name + "</option>";
            });

            if(fn==1){
                $("#my_pendingposition").html(stringx_position);
                $("#my_pendingdepartment").html(stringx_account);
            }else if(fn==2){
                $("#my_manreqposition").html(stringx_position);
                $("#my_manreqdepartment").html(stringx_account);
            }else if(fn==10){
                $("#manpowerRec_my_manreqposition").html(stringx_position);
                $("#manpowerRec_my_manreqdepartment").html(stringx_account);
            }      
        }
    });
}


//DATATABLE FOR PENDING REQUESTS

// DATE 

function initRecordDateRangePicker_mypersonal_ireq(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#recordsall-date_mypendingireq .form-control').val(start + ' - ' + end);
        $('#recordsall-date_mypendingireq').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#recordsall-date_mypendingireq .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initpending_irequest(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'),1);
        });
        var dateRanges = {
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        };
        callback(dateRanges);
    });
}
   
function initpending_irequest(startDate, endDate,dt) {
    $('#irequest_mypendingDatatable').mDatatable('destroy');
    records_mypendingirequestDt.init(startDate, endDate,dt);
}

// My PENDING REQUEST
var records_mypendingirequestDt = function () {
    var irequestrecord = function (start, end,dt) {
        // var func_ir = (ireq === "1") ? "/irequest/list_irequest_mypending_datatable" : (ireq === "2") ? "/discipline/list_irall_datatable" : (ireq === "3") ? "" : " ";
        var func_ir ="";
        func_ir = "/irequest/list_irequest_mypending_datatable";
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + func_ir,
                        params: {
                            query: {
                                position: $('#my_pendingposition').val(),
                                req_department: $('#my_pendingdepartment').val(),
                                search_irequestid: $('#my_pending_id').val(),
                                status: $('#my_pending_stats').val(),
                                page_type: 'myreq',
                                cont_access: dt,
                                startDate: start,
                                endDate: end
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "irequest_ID",
                title: "iRequest ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.irequest_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "position_ID",
                title: "Position",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.pos_details;
                    return html;
                },
            }, {
                field: "requesting_dept",
                title: "Requesting Department",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.acc_name;
                    return html;
                },
            },
            {
                field: "dateRequested",
                title: "Date Requested",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.dateRequested, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            },
            {
                field: "statdesc",
                title: "Status",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    var af="";
                    var desc="";
                    if(row.status==2){
                        af="m-badge--info";
                        desc="Pending";
                    }else{
                        af="m-badge--warning";
                        desc="Missed";
                    }
                    html+='<span style="width: 120px;"><span class="m-menu__link-badge"><span class="m-badge m-badge--wide '+af+'">'+desc+'</span></span></span>';
                    // html = row.statdesc;
                    return html;
                },
            },
            {
                field: "",
                title: "Action",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html +='<button type="button" data-id="'+row.irequest_ID+'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only view_pendingreq"><i class="fa fa-eye"></i></button>';
                    html +='&nbsp;&nbsp;<button type="button" data-stat="'+row.status+'" data-id="'+row.irequest_ID+'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only remove_irequest"><i class="fa fa-remove"></i></button>';
                    return html;
                },
            },
            ],
        };
        var datatable;
        datatable = $('#irequest_mypendingDatatable').mDatatable(options);
    };
    return {
        init: function (start, end,dt) {
            irequestrecord(start, end,dt);
        }
    };
}();

//MANPOWER REQUEST
//MANPOWER RECRUITMENT

