$(function(){
    // customPopoverDatePicker("#popovershowthis");  
    // initRecordDateRangePicker_irecruit_ireq(function (dateRanges) {
    //     initpending_recruitment(dateRanges.start, dateRanges.end,1);
    //     filters_myreq_pending(8);
    // });
    $('#tat_manreqposition,#tat_manreqdepartment').select2({
        placeholder: "-Select-",
        width: '100%'
    });
    $('#applicant_nAmes').selectpicker({
        placeholder: "Select Names",
        width: '100%',
        allowClear: true
    });
});

$("#tattab_pendingrecruitment").on("click", function(){   
    initRecordDateRangePicker_irecruit_ireq(function (dateRanges) {
        initpending_recruitment(dateRanges.start, dateRanges.end,1);
        filters_myreq_pending(8);
    });
    $("#recordsall-date_tat").hide("");
    $('#tat_manreqposition,#tat_manreqdepartment').select2({
        placeholder: "-Select-",
        width: '100%'
    });
});

$("#tat_manreqposition,#tat_manreqdepartment,#tat_recruitment_id").on('change', function(){
    var dateRange = $('#recordsall-date_tat').val().split('-');
    initpending_recruitment(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'),1);
});

function initRecordDateRangePicker_irecruit_ireq(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#recordsall-date_tat .form-control').val(start + ' - ' + end);
        $('#recordsall-date_tat').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#recordsall-date_tat .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initpending_recruitment(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'),1);
        });
        var dateRanges = {
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        };
        callback(dateRanges);
    });
}

function initpending_recruitment(startDate, endDate,dt) {
    $('#irequest_pendingRecruitmentDatatable').mDatatable('destroy');
    records_mypendingRecruitment.init(startDate, endDate,dt);
}

// My PENDING REQUEST
var records_mypendingRecruitment = function () {
    var irequestrecord = function (start, end,dt) {
        var func_ir ="";
        func_ir = "/irequest/list_irequest_recruitment_datatable";
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + func_ir,
                        params: {
                            query: {
                                position: $('#tat_manreqposition').val(),
                                req_department: $('#tat_manreqdepartment').val(),
                                search_irequestid: $('#tat_recruitment_id').val(),
                                // page_type: 'myreq',
                                // cont_access: dt,
                                startDate: start,
                                endDate: end,
                                type: 1
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { 
                },
            },
            // columns definition
            columns: [{
                field: "irequestRecruitment_ID",
                title: "Recruitment ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.irequestRecruitment_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "irequest_ID",
                title: "iRequest ID",
                width: 80,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.irequest_ID.padLeft(8);
                    return html;
                },
            }, {
                field: "position_ID",
                title: "Position",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.pos_details;
                    return html;
                },
            },
            {
                field: "requesting_dept",
                title: "Requesting Team",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.acc_name;
                    return html;
                },
            },
            {
                field: "manpower_requested",
                title: "Manpower",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.manpower_supplied +" / "+ row.manpower_requested;
                    return html;
                },
            },
            {
                field: "status_ID",
                title: "Status",
                width: 90,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    var af="";
                    var desc="";
                    if(row.status_ID==2){
                        af="m-badge--info";
                        desc="Pending";
                    }else if(row.status_ID==16){
                        af="m-badge--danger";
                        desc="Overdue";
                    }
                    html+='<span style="width: 120px;"><span class="m-menu__link-badge"><span class="m-badge m-badge--wide '+af+'">'+desc+'</span></span></span>';
                    // html = row.statdesc;
                    return html;
                },
            },
            {
                field: "",
                title: "Action",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html +='<button type="button" data-position="'+row.position_ID+'" data-recid="'+row.irequestRecruitment_ID+'" data-id="'+row.irequest_ID+'" data-display="pending" class="btn btn-outline-success m-btn m-btn--icon m-btn--icon-only view_recruitmentdet"><i class="fa fa-pencil"></i></button>';
                    html +='&nbsp;&nbsp;<button type="button" data-recid="'+row.irequestRecruitment_ID+'" data-id="'+row.irequest_ID+'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only cancel_recruitment"><i class="fa fa-remove"></i></button>';
                    return html;
                },
            },
            ],
        };
        var datatable;
        datatable = $('#irequest_pendingRecruitmentDatatable').mDatatable(options);
    };
    return {
        init: function (start, end,dt) {
            irequestrecord(start, end,dt);
        }
    };
}();

// ON EVENT 
$("#irequest_recruitment_col").on('click','.view_recruitmentdet', function(){
    var id_ireq = $(this).data('id');
    let pos = $(this).data('position');
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/manpowerdetails2",
        data: {
            ireq: id_ireq,
            stat: 4,
            requestor: 1,
            approver: 0,
            position: pos
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            // var d_start = result[0].dateStartRequired;
            // var d_end = result[0].dateEndRequired;
            // var m1 = moment(d_start);
            // var m2 = moment(d_end);
            // var S_date = m1.format('MMM D '+', '+'YYYY');
            // var E_date = m2.format('MMM D '+', '+'YYYY');
            var stringx_attachment="";
            var btn_appdisapprove="";
            var stringx_approver="";
            var stringx_app="";
            var stringxx_hired="";
            var btn_ned="";
            var btn_addNew="";
            var btn_addSelected="";
            var btn_sup="";
            var btn_comp="";
            let a = parseInt(result.details[0].manpower_requested);
            let b = parseInt(result.details[0].manpower_supplied);
            let rem = a-b;

            btn_comp += '<button data-status="3" type="button" data-id="'+id_ireq+'" class="btn btn-success autocomplete"><span><i class="fa fa-check"></i>';
            btn_comp += '<span> Set to Completed</span></span></button>';

            stringx_approver += '<div class="row">';
            stringx_approver += '<div class="col-12">';
            stringx_approver += '<button data-id="'+id_ireq+'" id="" type="button" class="btn btn-link view_approvers_irqdisplay">Click to View Approver Details</button>';
            stringx_approver += '</div>';
            stringx_approver += '</div>';

            btn_appdisapprove += '<button type="button" class="btn btn-secondary" data-dismiss="modal">';
            btn_appdisapprove += '<span><i class="fa fa-close"></i><span> Cancel</span></span>';
            btn_appdisapprove += '</button>';

            btn_ned += '<button type="button" data-id="'+id_ireq+'" data-change="ned" class="btn btn-success m-btn m-btn--icon m-btn--icon-only manpowerchange">';
            btn_ned += '<i class="fa fa-check"></i></button>';

            btn_addSelected += '<button type="button" data-ireq="'+id_ireq+'" data-site="'+result.details[0].site_ID+'" data-position="'+result.details[0].position_ID+'" data-id="'+result.details[0].irequestRecruitment_ID+'" class="btn btn-success m-btn m-btn--icon app_selectedBtn">';
            btn_addSelected += '<span><i class="fa fa-check"></i><span> Save Selected Applicant(s)</span></span>';
            btn_addSelected += '</button>';

            // btn_addNew += '<span class="mr-2"> OR </span><button type="button" data-ireq="'+id_ireq+'" data-site="'+result.details[0].site_ID+'" data-position="'+result.details[0].position_ID+'" data-id="'+result.details[0].irequestRecruitment_ID+'" class="btn btn-success m-btn m-btn--icon app_addNewBtn">';
            // btn_addNew += '<span><i class="fa fa-plus"></i><span> Add New Applicant</span></span>';
            btn_addNew += '</button>';

            btn_sup += '<button type="button" data-id="'+id_ireq+'" data-change="sup" class="btn btn-success m-btn m-btn--icon m-btn--icon-only manpowerchange">';
            btn_sup += '<i class="fa fa-check"></i></button>';
       
            $("#recruitment_displaydetails").modal("show");
            $("#approvdis_btn").html(btn_appdisapprove);
            $("#btnfor_completed").html(btn_comp);
            $("#ireq_id_display").text("00"+result.details[0].irequest_ID);
            $("#ireq_pos_display").text(result.details[0].pos_details);
            $("#ireq_reqdept_display").text(result.details[0].acc_name);
            $("#ireq_manpower_display").text(result.details[0].manpower_requested);
            $("#ireq_edreq_display").text(result.details[0].educationalAttainment);
            $("#ireq_gender_display").text(result.details[0].gender);
            $("#ireq_bdesc_display").text(result.details[0].description);
            $("#ireq_prefqual_display").text(result.details[0].qualifications);
            $("#ireq_hpurpose_display").text(result.details[0].hpurpose);
            $("#ireq_dateduration_display").text(result.details[0].days_needed);
            // $("#ireq_dateduration_display").text(S_date + " - " + E_date);
            $("#moredetails_irq").html(stringx_approver);
            $("#update_req").val(result.details[0].manpower_requested);
            $("#update_sup").val(result.details[0].manpower_supplied);
            $("#ireq_manpowersupplied_display").text(result.details[0].manpower_supplied);
            $("#btn-check_ned").html(btn_ned);
            $("#btn-check_sup").html(btn_sup);
            $("#update_manpowerdes").show("");
            $("#moredetails_irq_recc").html(stringx_approver);
            $("#ireq_manpowerRemaining_display").text(rem);
            $("#sel_ApplicantDIV").show("");
            $("#ireq_tHiring_display_recc").text(result.details[0].hiringtype_desc);

            $("#buttonToSaveApplicants").html(btn_addSelected);
            $("#addNewApplicant").html(btn_addNew);

            $("#ireq_status_display").text("PENDING RECRUITMENT");
            $("#color_change_dis").addClass("pending_recruitment_css");
            $("#color_change_dis").removeClass("completed_recruitment_css");
            $("#color_change_dis").removeClass("cancelled_recruitment_css");
            $("#NumDaysFilled").hide("");

            if(result.details[0].descriptionAttachment!=null){
                var atf=result.details[0].descriptionAttachment;
                var atf2 = atf.replace("uploads/irequest/job_description/", " ");
                stringx_attachment+='<div class="col-xl-4 col-sm-12"><label class="w_color_label">Attached Job Description </label></div>';
                stringx_attachment+='<div class="col-xl-7 col-sm-12" style="padding-left:2em">';
                stringx_attachment+='<div class="row">';
                stringx_attachment+='<div class="col-12" style="color:darkcyan;font-weight:500"><span>'+atf2+'</span></div>';
                stringx_attachment+='<div class="col-12" style="">';
                stringx_attachment+="<a href='"+baseUrl +"/"+atf+"' class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' download='' style='background: lavender;'><span><i class='fa fa-download'></i>&nbsp;Download Attached File</span></a></div>";
                stringx_attachment+='</div>';
                stringx_attachment+='</div>';           
                $("#ireq_attachedfile_display").html(stringx_attachment);
            }

            $.each(result.applicants,function(key,data){
                stringx_app+="<option value=" + data.apid + ">" + data.lname +" ,"+ data.fname +" "+ data.mname + "</option>";
            });
           
            if(result.hiredEx==1){
                $.each(result.hired,function(key,data){
                    var d_hired = data.date_hired;
                    var m1 = moment(d_hired);
                    var hired_Date = m1.format('MMM D '+', '+'YYYY');
                    stringxx_hired += '<li data-id="'+data.apid+'" data-application="'+data.application_ID+'" class="list-group-item d-flex justify-content-between align-items-center">'+data.lname+', '+data.fname+'  -  (Date Hired: '+hired_Date+')'+' <span><a href="#" data-ireq="'+id_ireq+'" data-pos="'+data.position_ID+'" data-id="'+data.apid+'" data-application="'+data.application_ID+'" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air removeHiredApplicant"><i class="la la-remove"></i></a></span></li>';
                });
            }else{
                stringxx_hired += '<li class="list-group-item d-flex justify-content-between align-items-center" style="text-align: center;color: #e36891;">No Hired Applicants Yet! <i class="la la-exclamation"></i></li>';
            }           
            $("#applicant_nAmes").html(stringx_app);
            $("#applicant_hiredNames").html(stringxx_hired);
            $('#applicant_nAmes').selectpicker('refresh');
            $('#manpowerToSupplyDIV').show("");
        }
    });
});


$("#buttonToSaveApplicants").on("click", ".app_selectedBtn", function(){
    let ireq_recID = $(this).data("id");
    let site = $(this).data("site");
    let position = $(this).data("position");
    let ireq_id = $(this).data("ireq");
    let applicant = $("#applicant_nAmes").val();
    if (applicant==null || applicant==""){
        swal("Oops! You have not selected any applicants.", "Please select at least one applicant to save.", "error");
    }else{   
    let applicant_sNames = applicant.toString();
        swal({
            title: 'Are you sure you want to add these selected applicant(s) as hired Employee(s) for this position?',
            text: 'Please review the selected names.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Add as Hired'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: baseUrl + "/irequest/add_Applicants",
                    data: {
                        irec_req: ireq_recID,
                        applicant: applicant_sNames,
                        site: site,
                        position: position,
                        ireq: ireq_id,
                        num_Applicant: applicant.length
                    },
                    cache: false,
                    success: function(res) {
                        var result = JSON.parse(res);
                        var stringxx = "";
                        var stringxx_app = "";
                        if(result.morethan == 1){
                            let rem = result.remaining;
                            swal(
                                'Oops!',
                                'Number of applicant(s) to supply is more than the Manpower Needed. There is/are only <strong>'+rem+'</strong> manpower slots to fill. Please check.',
                                'error'
                            )
                        }else{    
                            //JUst enough number                     
                            initRecordDateRangePicker_irecruit_ireq(function (dateRanges) {
                                initpending_recruitment(dateRanges.start, dateRanges.end,1);
                                filters_myreq_pending(8);
                            });
                            if(result.completed==1){   
                                $("#recruitment_displaydetails").modal("hide");                          
                                swal(
                                    'Completed!',
                                    'You have successfully added applicant(s) as hired Employee(s) for this position and completed the Manpower Needed.',
                                    'success'
                                )
                                // close Modal 
                            }else{
                                //Append
                               
                                $.each(result.HiredApplicants,function(key,data){
                                    var d_hired = data.date_hired;
                                    var m1 = moment(d_hired);
                                    var hired_Date = m1.format('MMM D '+', '+'YYYY');
                                    stringxx += '<li data-id="'+data.apid+'" data-application="'+data.application_ID+'" class="list-group-item d-flex justify-content-between align-items-center">'+data.lname+', '+data.fname+'  -  (Date Hired: '+hired_Date+')'+'<span><a href="#" data-ireq="'+ireq_id+'" data-pos="'+data.position_ID+'" data-id="'+data.apid+'" data-application="'+data.application_ID+'" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air removeHiredApplicant" ><i class="la la-remove"></i></a></span></li>';
                                });                        
                                DestroyApplicant_listSelect(); 
                                $.each(result.applicants,function(key,data){
                                    stringxx_app+="<option value=" + data.apid + ">" + data.lname +" ,"+ data.fname +" "+ data.mname + "</option>";
                                });
                                $("#applicant_nAmes").html(stringxx_app);                           
                                $('#applicant_nAmes').selectpicker('refresh');                           
                                $("#applicant_hiredNames").html(stringxx);      
                                swal(
                                    'Success!',
                                    'You have successfully added applicant(s) as hired Employee(s) for this position.',
                                    'success'
                                )
                                $("#ireq_manpowersupplied_display").text(result.supply);
                                $("#ireq_manpowerRemaining_display").text(result.tosupply);
                            }               
                        }
                    },
                    error: function(res) {
                        swal(
                            'Oops!',
                            'Something is wrong with your code!',
                            'error'
                        )
                    }
                });
            } else if (result.dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Adding of Applicant(s) has been cancelled.',
                    'error'
                )
            }
        });
    }
});

$("#irequest_recruitment_col").on('click','.cancel_recruitment', function(){
    let id_ireq=$(this).data('recid');
    let id_req=$(this).data('id');
    swal({
        title: 'Are you sure you want to cancel this Manpower for Recruitment?',
        text: 'Please review this Manpower. It will not be retrieved once cancelled.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Cancel Recruitment'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: baseUrl + "/irequest/cancel_manpowerRecruitment",
                data: {
                    irecruitment: id_ireq,
                    ireqid: id_req
                },
                cache: false,
                success: function(res) {
                    swal(
                        'Cancelled!',
                        'Pending Manpower for Recruitment has been cancelled.',
                        'success'
                    )
                    initRecordDateRangePicker_irecruit_ireq(function (dateRanges) {
                        initpending_recruitment(dateRanges.start, dateRanges.end,1);
                        filters_myreq_pending(8);
                    });
                },
                error: function(res) {
                    swal(
                        'Oops!',
                        'Something is wrong with your code!',
                        'error'
                    )
                }
            });
        } else if (result.dismiss === 'cancel') {
            swal(
                'Cancelled',
                'Cancelling of Pending Manpower for Recruitment has been cancelled.',
                'error'
            )
        }
    });
});

$("#update_manpowerdes").on('click' ,'.manpowerchange', function(){
    var ch=$(this).data('change');
    var upnum = 0;
    if(ch=="ned"){
        upnum = $("#update_req").val();
    }else{
        upnum = $("#update_sup").val();
    }
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/manpower_reqsup",
        data: {
            ireq: $(this).data('id'),
            type_change: $(this).data('change'),
            update_num: upnum
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var message = "";

            if(result.notif==1){
                if(ch=="ned"){
                    $("#ireq_manpower_display").text(upnum);
                }else{
                    $("#ireq_manpowersupplied_display").text(upnum);
                }
                if(result.exit==1){
                    $("#recruitment_displaydetails").modal("hide");
                    swal(
                        'Success!',
                         'Completed Manpower Supply',
                        'success'
                    )
                }else{
                    swal(
                        'Success!',
                         'Changed Manpower Needed/Supplied.',
                        'success'
                    )
                }
               
                initRecordDateRangePicker_irecruit_ireq(function (dateRanges) {
                    initpending_recruitment(dateRanges.start, dateRanges.end,1);
                });
            }else{         
                swal(
                    'Oops!',
                    'Invalid Manpower required/supply number! Be sure that for Manpower required, it should be lesser than or equal to Manpower supply and Manpower supply should be lesser than or equal to Manpower required.',
                    'error'
                )
            }
        }
    });
});

function filters_myreq_pending(fn){
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/filters_myreq_pending",
        data: {
            filter_no: fn,
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var stringx_position="";
            var stringx_account="";
            stringx_position += "<option disabled selected=''>-Select Position-</option>";
            stringx_position += "<option value='0'>All</option>";
            stringx_account += "<option disabled selected=''>-Select Requesting Team-</option>";;
            stringx_account += "<option value='0'>All</option>";
              
            $.each(result.position,function(key,data){
                stringx_position+="<option value=" + data.position_ID + ">" + data.pos_details + "</option>";
            });

            $.each(result.account,function(key,data){
                stringx_account+="<option value=" + data.requesting_dept + ">" + data.acc_name + "</option>";
            });
            if(fn==8){
                $("#tat_manreqposition").html(stringx_position);
                $("#tat_manreqdepartment").html(stringx_account);
            }else if(fn==9){
                $("#tat_manreqposition_c").html(stringx_position);
                $("#tat_manreqdepartment_c").html(stringx_account);
            }           
        }
    });
}

$("#moredetails_irq_recc").on('click','.view_approvers_irqdisplay', function(){
    $("#irequest_approverdetails").modal("show");
    $("#recruitment_displaydetails").modal("hide");
    $("#dis_appr_bttn").data("id",2);
    disp_approvers($(this).data("id"));
});

$("#dis_appr_bttn").on('click', function(){
    let i_d=$(this).data("id");
    $("#recruitment_displaydetails").modal("show");
});

function disp_approvers(ireq_ID){
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/fetch_approversdisplay",
        data: {
            ireq: ireq_ID,
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var stringxx="";
            var stat="";
            var backcolor ="";
            var statlabel ="";
            var iconz ="";
            var colorz ="";
            var remarks ="No remarks yet.";
           
            $.each(result, function(key, item) {
                var sincedate = moment(item.dated_status, 'Y-MM-DD').format('MMM DD, Y');

                if(item.remarks!=null || item.remarks!=" " || item.remarks!=NULL){
                    remarks=item.remarks;
                }else{
                    remarks="No remarks";
                }
                if(item.status==4){
                    stat ="Current Approver since "+sincedate;
                    backcolor = "background: #b5e7ec";
                    statlabel = "Current";
                    iconz = "fa-spinner";
                    colorz = "btn-info";  
                }else if(item.status == 2){
                    stat ="Pending since " + sincedate;
                    backcolor = "background: #f9db9d";
                    statlabel = "Pending";
                    iconz = "fa-spinner";
                    colorz = "btn-warning";
                    remarks="No Remarks";
                }else if(item.status == 5){
                    stat = "Approved last " + sincedate;
                    backcolor = "background: darkseagreen";
                    statlabel = "Approved";
                    iconz = "fa-thumbs-o-up";
                    colorz = "btn-success";
                    remarks = item.remarks;
                }else if(item.status == 6){
                    stat = "Disapproved last " +sincedate;
                    backcolor = "background: #ffb9c5";
                    statlabel = "Disapproved";
                    iconz = "fa-thumbs-o-down";
                    colorz = "btn-danger";
                    remarks = item.remarks;                    
                }else if(item.status == 12){
                    stat = "Missed last " +sincedate;
                    backcolor = "background: #ffb9c5";
                    statlabel = "Missed";
                    iconz = "fa-exclamation-circle";
                    colorz = "btn-warning";
                    remarks = item.remarks; 
                }else{
                    stat = "Cancelled last " +sincedate;
                    backcolor = "background: #ffb9c5";
                    statlabel = "Cancelled";
                    iconz = "fa-remove";
                    colorz = "btn-danger";
                    remarks = item.remarks; 
                }

                stringxx +='<div class="m-portlet bg-secondary">';
                stringxx +='<div class="col-md-12 pt-2 pb-2" style="background:#c5d2d4"> <div class="row">';
                stringxx +='<div class="col-7 col-sm-7 col-md-7 pt-2"><strong>Recommender '+item.level+'</strong></div>';
                stringxx +='<div class="col-5 col-sm-5 col-md-5">';
                stringxx +=' <div class="btn pull-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;background: #505a6b;">';
                stringxx +='<div class="btn '+colorz+' m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="width: 25px !important;height: 25px !important;">';
                stringxx +='<i class="fa '+iconz+'" style="font-size: 17 px;"></i></div>';
                stringxx +='<span class="button-content text-light text-capitalize pl-2">'+statlabel+'</span>';
                stringxx +='</div></div></div></div>';
                stringxx +='<div class="m-portlet__body pt-3 pl-4 pb-3 pr-4">';
                stringxx +='<div class="row">';
                stringxx +='<div class="col-2 col-sm-2 col-md-2 d-none d-sm-block">';
                stringxx +='<img src="http://localhost/sz/assets/images/img/sz.png" onerror="noImageFound(this)" width="58" alt="" class="mx-auto rounded-circle">';
                stringxx +='</div>';
                stringxx +='<div class="col-12 col-sm-10 col-md-10 pl-1">';
                stringxx +='<div class="col-md-12 font-weight-bold" style="font-size: 15px;">';
                stringxx +=''+item.fname+' '+item.lname+'';
                stringxx +='</div>';
                stringxx +='<div class="col-md-12" style="font-size: 11px;margin-top: -3px;font-weight: bolder; color: #179e9e;">'
                stringxx +=' '+stat+'</div>';
                stringxx +=' <div class="col-md-12 mt-3" style="font-size: 12px;"><i> - '+remarks+'.</i></div>';
                stringxx +='</div>';
                stringxx +='<div class="col-md-12 pt-3"></div>';
                stringxx +='</div></div> </div>';
            });
            $("#approverlist_disp").html(stringxx);
        }
    });
}

$("#applicant_hiredNames").on("click",".removeHiredApplicant", function(){
    let apid =$(this).data("application");
    let applicant_id = $(this).data("id");
    let position = $(this).data("pos");
    let ireq = $(this).data("ireq");

    swal({
        title: 'Are you sure you want to remove this Applicant as hired for this position?',
        text: 'Please review.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Remove Applicant.'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: baseUrl + "/irequest/remove_Applicant",
                data: {
                    application_id: apid,
                    applicant_id: applicant_id,
                    position: position,
                    ireq: ireq
                },
                cache: false,
                success: function(res) {
                    var result = JSON.parse(res);
                    var stringxx = "";
                    var stringxx_app = "";
                   
                    swal(
                        'Cancelled!',
                        'Removal of applicant has been cancelled.',
                        'success'
                    )
                    initRecordDateRangePicker_irecruit_ireq(function (dateRanges) {
                        initpending_recruitment(dateRanges.start, dateRanges.end,1);
                        filters_myreq_pending(8);
                    });
                   
                    if(result.hired!=0){
                        $.each(result.hired,function(key,data){
                            var d_hired = data.date_hired;
                            var m1 = moment(d_hired);
                            var hired_Date = m1.format('MMM D '+', '+'YYYY');
                            stringxx += '<li data-id="'+data.apid+'" data-application="'+data.application_ID+'" class="list-group-item d-flex justify-content-between align-items-center">'+data.lname+', '+data.fname+'  -  (Date Hired: '+hired_Date+')'+'<span><a href="#" data-ireq="'+ireq+'" data-pos="'+data.position_ID+'" data-id="'+data.apid+'" data-application="'+data.application_ID+'" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air removeHiredApplicant" ><i class="la la-remove"></i></a></span></li>';
                        });
                    }else{
                        stringxx = '<li class="list-group-item d-flex justify-content-between align-items-center" style="text-align: center;color: #e36891;">No Hired Applicants Yet! <i class="la la-exclamation"></i></li>';                
                    }
                     
                    DestroyApplicant_listSelect();
                    $.each(result.applicants,function(key,data){
                        stringxx_app+="<option value=" + data.apid + ">" + data.lname +" ,"+ data.fname +" "+ data.mname + "</option>";
                    });
                    $("#applicant_nAmes").html(stringxx_app);
                    $('#applicant_nAmes').selectpicker('refresh');
                    $("#applicant_hiredNames").html(stringxx);
                    $("#ireq_manpowersupplied_display").text(result.supply);  
                    $("#ireq_manpowerRemaining_display").text(result.tosupply);
                },
                error: function(res) {
                    swal(
                        'Oops!',
                        'Something is wrong with your code!',
                        'error'
                    )
                }
            });
        } else if (result.dismiss === 'cancel') {
            swal(
                'Cancelled',
                'Removal of applicant has been cancelled.',
                'error'
            )
        }
    });
});

$("#btnfor_completed").on("click", ".autocomplete", function(){
    let ireqid = $(this).data('id');
    let status = $(this).data('status');
    let mssg = "";
    let btnms = "";
    let mssg2 = "";
    if(status==3){
        mssg = "Are you sure you want to set this Manpower for Recruitment to COMPLETED?";
        btnms = "Yes, Set to Completed!";
        mssg2 = "Set Manpower for Recruitment to Completed";
    }else{
        mssg = "Are you sure you want to OPEN again this Manpower for Recruitment?"
        btnms = "Yes, Set to Pending!";
        mssg2 = "Set Manpower for Recruitment to Pending";
    }
    swal({
        title: mssg,
        text: 'Regardless of the number of Manpower Supplied?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: btnms
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: baseUrl + "/irequest/manpowerto_completed",
                data: {
                    ireq: ireqid,
                    stat: status,
                },
                cache: false,
                success: function(res) {
                    var result = JSON.parse(res);
                    $("#recruitment_displaydetails").modal("hide");
                    if(status==3){
                        initRecordDateRangePicker_irecruit_ireq(function (dateRanges) {
                            initpending_recruitment(dateRanges.start, dateRanges.end,1);
                            filters_myreq_pending(8);
                        });
                    }else{
                        initRecordDateRangePicker_irecruit_ireq_completed(function (dateRanges) {
                            initcompleted_recruitment(dateRanges.start, dateRanges.end,1);
                            filters_myreq_pending(9);
                        });
                    }

                    if(result==1){
                        swal(
                            'Success!',
                            mssg2,
                            'success'
                        )
                    }else{
                        swal(
                            'Oops!',
                            'You cannot revert this Manpower Recruitment to PENDING. It has recorded that the number of Manpower supplied is COMPLETED.',
                            'error'
                        )
                    }
                },
                error: function(res) {
                    swal(
                        'Oops!',
                        'Something is wrong with your code!',
                        'error'
                    )
                }
            });
        } else if (result.dismiss === 'cancel') {
            swal(
                'Cancelled',
                'Changing of Manpower for Recruitment status has been cancelled.',
                'error'
            )
        }
    });
});

$("#applicant_nAmes").on("click", function(){
    console.log("UPDATE");
});

function DestroyApplicant_listSelect(){
    $("#applicant_nAmes").selectpicker("destroy");
    $("#applicant_nAmes").empty();
    $('#applicant_nAmes').selectpicker({
        placeholder: "Select Names",
        width: '100%',
        allowClear: true
    });
}
function refresh_ApplicantList(ireq){
    // $("#applicant_nAmes").selectpicker("destroy");
    $("#applicant_nAmes").selectpicker("destroy");
    $("#applicant_nAmes").empty();
    $('#applicant_nAmes').selectpicker({
        placeholder: "Select Names",
        width: '100%',
        allowClear: true
    });
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/get_ApplicantList",
        data: {
            ireq: ireq
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var stringxx_app="";
            $.each(result,function(key,data){
                stringxx_app+="<option value=" + data.apid + ">" + data.lname +" ,"+ data.fname +" "+ data.mname + "</option>";
            });
            $("#applicant_nAmes").html(stringxx_app);
            $('#applicant_nAmes').selectpicker('refresh');
            console.log(stringxx_app);
        }
    });
}

