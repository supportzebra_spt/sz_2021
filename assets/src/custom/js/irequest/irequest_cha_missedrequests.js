$("#tab_missedrequests").on('click', function(){
    initRecordDateRangePicker_missed_ireq(function (dateRanges) {
        initmissed_irequest(dateRanges.start, dateRanges.end,1);
        filters_myreq_pending(7);
    });
})

function initRecordDateRangePicker_missed_ireq(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#recordsall-date_missedireq .form-control').val(start + ' - ' + end);
        $('#recordsall-date_missedireq').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#recordsall-date_missedireq .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initmissed_irequest(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'),1);
        });
        var dateRanges = {
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        };
        callback(dateRanges);
    });
}

function initmissed_irequest(startDate, endDate,dt) {
    $('#irequest_missedDatatable').mDatatable('destroy');
    records_missedirequestDt.init(startDate, endDate,dt);
}

var records_missedirequestDt = function () {
    var irequestrecord = function (start, end,dt) {
        // var func_ir = (ireq === "1") ? "/irequest/list_irequest_mypending_datatable" : (ireq === "2") ? "/discipline/list_irall_datatable" : (ireq === "3") ? "" : " ";
        var func_ir ="";
        func_ir = "/irequest/list_irequest_mypending_datatable";
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + func_ir,
                        params: {
                            query: {
                                position: $('#missed_position').val(),
                                req_department: $('#missed_department').val(),
                                search_irequestid: $('#missed_search').val(),
                                page_type: 'missedReq',
                                cont_access: 4,
                                startDate: start,
                                endDate: end
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "irequest_ID",
                title: "iRequest ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.irequest_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "position_ID",
                title: "Position",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.pos_details;
                    return html;
                },
            }, {
                field: "requesting_dept",
                title: "Requesting Department",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.acc_name;
                    return html;
                },
            },
            {
                field: "dateRequested",
                title: "Date Requested",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.dateRequested, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            },
            {
                field: "",
                title: "Action",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html +='<button type="button" data-id="'+row.irequest_ID+'" class="btn btn-outline-success m-btn m-btn--icon m-btn--icon-only view_missedreq"><i class="fa fa-pencil"></i></button>';
                    // html +='&nbsp;&nbsp;<button type="button" data-id="'+row.irequest_ID+'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only remove_irequest"><i class="fa fa-remove"></i></button>';
                    return html;
                },
            },
            ],
        };
        var datatable;
        datatable = $('#irequest_missedDatatable').mDatatable(options);
    };
    return {
        init: function (start, end,dt) {
            irequestrecord(start, end,dt);
        }
    };
}();

$("#missed_position,#missed_department, #missed_search").on('change', function(){
    var dateRange = $('#recordsall-date_missedireq').val().split('-');
    initmissed_irequest(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'),1);
});

// VIEW MORE 
$("#irequest_missed_col").on('click','.view_missedreq', function(){
    var id_ireq = $(this).data('id');
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/manpowerdetails",
        data: {
            ireq: id_ireq,
            stat: 1,
            requestor: 3,
            approver: 1,
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            // var d_start = result[0].dateStartRequired;
            // var d_end = result[0].dateEndRequired;
            // var m1 = moment(d_start);
            // var m2 = moment(d_end);
            // var S_date = m1.format('MMM D '+', '+'YYYY');
            // var E_date = m2.format('MMM D '+', '+'YYYY');

            var stringx_attachment="";
            var btn_appdisapprove="";
            var stringx_approver="";
            stringx_approver += '<div class="row">';
            stringx_approver += '<div class="col-12">';
            stringx_approver += '<button data-id="'+id_ireq+'" type="button" class="btn btn-link view_approvers_irqdisplay">Click to View Approver Details</button>';
            stringx_approver += '</div>';
            stringx_approver += '</div>';

            btn_appdisapprove += '<button type="button" data-dec="6" data-reqid="'+id_ireq+'" class="btn btn-danger mrf_disapprove" >';
            btn_appdisapprove += '<span><i class="fa fa-thumbs-o-down"></i><span> Disapprove</span></span>';
            btn_appdisapprove += '</button>';
            btn_appdisapprove += '<button type="button" data-dec="5" data-reqid="'+id_ireq+'" class="btn btn-success mrf_approve" >';
            btn_appdisapprove += '<span><i class="fa fa-thumbs-o-up"></i><span> Approve</span></span>';
            btn_appdisapprove += '</button>';

            $("#irequest_displaydetails").modal("show");
            $("#approvdis_btn").html(btn_appdisapprove);
            $("#ireq_id_display").text("00"+result[0].irequest_ID);
            $("#ireq_pos_display").text(result[0].pos_details);
            $("#ireq_reqdept_display").text(result[0].acc_name);
            $("#ireq_manpower_display").text(result[0].manpower_requested);
            $("#ireq_edreq_display").text(result[0].educationalAttainment);
            $("#ireq_gender_display").text(result[0].gender);
            $("#ireq_bdesc_display").text(result[0].description);
            $("#ireq_prefqual_display").text(result[0].qualifications);
            $("#ireq_hpurpose_display").text(result[0].hpurpose);
            $("#ireq_tHiring_display").text(result[0].hiringtype_desc);

            $("#ireq_dateduration_display").text(result[0].days_needed);
            // $("#ireq_dateduration_display").text(S_date + " - " + E_date);
            $("#moredetails_irq").html(stringx_approver);
            $("#ireq_status_display").text("MISSED REQUEST");
            $("#color_change_dis").addClass("missed_color");
            $("#color_change_dis").removeClass("approved_color");
            $("#color_change_dis").removeClass("disapproved_color");
            $("#color_change_dis").removeClass("pending_color");

            if(result[0].descriptionAttachment!=null){
                var atf=result[0].descriptionAttachment;
                var atf2 = atf.replace("uploads/irequest/job_description/", " ");
                stringx_attachment+='<div class="col-xl-4 col-sm-12"><label class="w_color_label">Attached Job Description </label></div>';
                stringx_attachment+='<div class="col-xl-7 col-sm-12" style="padding-left:2em">';
                stringx_attachment+='<div class="row">';
                stringx_attachment+='<div class="col-12" style="color:darkcyan;font-weight:500"><span>'+atf2+'</span></div>';
                stringx_attachment+='<div class="col-12" style="">';
                stringx_attachment+="<a href='"+baseUrl +"/"+atf+"' class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' download='' style='background: lavender;'><span><i class='fa fa-download'></i>&nbsp;Download Attached File</span></a></div>";
                stringx_attachment+='</div>';
                stringx_attachment+='</div>';  
                $("#ireq_attachedfile_display").html(stringx_attachment);
            }
        }
    });
});

$("#approvdis_btn").on("click", ".mrf_approve", function(){
    var id_ireq=$(this).data('reqid');
    var d= $(this).data('dec');
    var stringxx="";
    $("#mess_apdis").text("Are you sure you want to Approve Manpower Request with iReq# 0000"+id_ireq+" ?");
    $('#irequest_displaydetails').modal('hide');
    $('#approval-disapproval_modal').modal("show");
    $('#apdis_btn').data('id',id_ireq);
    $("#approval_comment").val("");

    stringxx +='<button  type="button" data-id="'+id_ireq+'" data-ap="1" data-dec="'+d+'" class="btn btn-success m-btn m-btn--icon m-btn--wide app_dis_submit">';
    stringxx +='<span><i class="fa fa-thumbs-up"></i><span>Yes, Approve!</span></span></button>';   
    $("#apdis_btn").html(stringxx);
});

$("#approvdis_btn").on("click", ".mrf_disapprove", function(){
    var id_ireq=$(this).data('reqid');
    var d= $(this).data('dec');
    var stringxx="";
    $("#mess_apdis").text("Are you sure you want to Disapprove Manpower Request with iReq# 0000"+id_ireq+" ?");
    $('#irequest_displaydetails').modal('hide');
    $('#approval-disapproval_modal').modal("show");
    $('#apdis_btn').data('id',id_ireq);
    $("#approval_comment").val("");

    stringxx +='<button  type="button" data-id="'+id_ireq+'" data-ap="0" data-dec="'+d+'" class="btn btn-warning m-btn m-btn--icon m-btn--wide app_dis_submit">';
    stringxx +='<span><i class="fa fa-thumbs-down"></i><span>Yes, Disapprove. </span></span></button>';   
    $("#apdis_btn").html(stringxx);
});

$("#apdis_btn").on('click', '.app_dis_submit', function(){
    var ap = $(this).data('ap');
    var apmes="";
    var apmes2="";
    if(ap==1){
        apmes="APPROVED!";
        apmes2="Pending Manpower Request has been APPROVED!";     
    }else{
        apmes="DISAPPROVED!";
        apmes2="Pending Manpower Request has been DISAPPROVED!";
    }
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/approve_disapprove_manpowerrequest",
        data: {
            ireq: $(this).data('id'),
            dec: $(this).data('dec'),
            remarks: $("#approval_comment").val(),
            missed: 1
        },
        cache: false,
        success: function(res) {
            var resObj = $.parseJSON(res.trim());
            systemNotification(resObj.notifid.notif_id);
            $('#irequest_displaydetails').modal('hide');
            $('#approval-disapproval_modal').modal('hide');
            swal(
                apmes,
                apmes2,
                'success'
            )
            initRecordDateRangePicker_missed_ireq(function (dateRanges) {
                initmissed_irequest(dateRanges.start, dateRanges.end,1);
                filters_myreq_pending(7);
            });
        },
        error: function(res) {
            swal(
                'Oops!',
                'Something is wrong with your code!',
                'error'
            )
        }
    });
});