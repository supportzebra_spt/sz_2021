$(function(){
    $('#tat_manreqposition_hired, #tat_manreq_position').select2({
        placeholder: "-Select-",
        width: '100%'
    });
});

$("#tat_manreqposition_hired, #tat_manreq_position, #tat_recruitment_id_hired").on('change', function(){
    var dateRange = $('#recordsall-date_tat_hired').val().split('-');
    initrec_hired(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'),1);
});

$("#tattab_hiredApplicants").on('click', function(){
    initRecordDateRangePicker_irec_hired(function (dateRanges) {
        initrec_hired(dateRanges.start, dateRanges.end,2);
        filters_hired_applicant();
    });
});

function filters_hired_applicant(){
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/filters_hired_applicant",
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var stringx_app = "";
            var stringxx_pos = "";
            stringx_app += "<option disabled selected=''> - Select Hired Applicant - </option>";
            stringx_app += "<option value='0'>All</option>";
            stringxx_pos += "<option disabled selected=''> - Select Position - </option>";
            stringxx_pos += "<option value='0'>All</option>";
            $.each(result.applicant, function (key, data) {
                stringx_app += '<option value="'+data.apid+'">'+data.lname+' , '+data.fname+' '+data.mname+ '</option>';
            });
            $.each(result.position, function (key, data) {
                stringxx_pos += '<option value="'+data.pos_id+'">'+data.pos_details+ '</option>';
            });
            $("#tat_manreqposition_hired").html(stringx_app);
            $("#tat_manreq_position").html(stringxx_pos);
        }
    });
}
    // DATE 
    function initRecordDateRangePicker_irec_hired(callback) {
        getCurrentDateTime(function (date) {
            start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
            end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
            $('#recordsall-date_tat_hired .form-control').val(start + ' - ' + end);
            $('#recordsall-date_tat_hired').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                startDate: start,
                endDate: end,
                opens: "left",
                ranges: {
                    'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                    'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                    'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                    'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                    'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                    'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
                }
            }, function (start, end, label) {
                $('#recordsall-date_tat_hired .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
                initrec_hired(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'),2);
            });
            var dateRanges = {
                'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
                'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
            };
            callback(dateRanges);
        });
    }
    function initrec_hired(startDate, endDate,dt) {
        $('#irequest_tathiredApplicantsDatatable').mDatatable('destroy');
        records_Recruitment_hired.init(startDate, endDate,dt);
    }

    var records_Recruitment_hired = function () {
        var irequestrecord = function (start, end,dt) {
            // var func_ir = (ireq === "1") ? "/irequest/list_irequest_mypending_datatable" : (ireq === "2") ? "/discipline/list_irall_datatable" : (ireq === "3") ? "" : " ";
            var func_ir ="";
            func_ir = "/irequest/list_irequest_hired_datatable";
            var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            method: 'POST',
                            url: baseUrl + func_ir,
                            params: {
                                query: {
                                    position: $('#tat_manreq_position').val(),
                                    hired: $('#tat_manreqposition_hired').val(),
                                    search_irequestid: $('#tat_recruitment_id_hired').val(),
                                    // page_type: 'myreq',
                                    // cont_access: dt,
                                    startDate: start,
                                    endDate: end
                                    // type: 1
                                },
                            },
                        }
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },
                layout: {
                    theme: 'default', // datatable theme
                    class: '', // custom wrapper class
                    scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                    height: 550, // datatable's body's fixed height
                    footer: false // display/hide footer
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    // toolbar placement can be at top or bottom or both top and bottom repeated
                    placement: ['bottom'],
    
                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                        },
                    }
                },
                // search: {
                //     input: $('#reportSearch'),
                // },
                rows: {
                    afterTemplate: function (row, data, index) { },
                },
                // columns definition
                columns: [{
                    field: "apid",
                    title: "Applicant ID",
                    width: 120,
                    selector: false,
                    sortable: 'desc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = row.apid.padLeft(8);
                        return html;
                    },
                },
                {
                    field: "lname",
                    title: "Name",
                    width: 120,
                    selector: false,
                    // sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = row.lname+" ,"+row.fname+" "+ row.mname;
                        return html;
                    },
                }, {
                    field: "irequest_ID",
                    title: "iRequest ID",
                    width: 115,
                    selector: false,
                    // sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = row.irequest_ID.padLeft(8);
                        return html;
                    },
                },
                {
                    field: "pos_id",
                    title: "Position",
                    width: 115,
                    selector: false,
                    // sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = row.pos_details;
                        return html;
                    },
                },
                {
                    field: "date_hired",
                    title: "Date Hired",
                    width: 115,
                    selector: false,
                    // sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var d_start = row.date_hired;
                        var m1 = moment(d_start);
                        var html = m1.format('MMM D '+', '+'YYYY');
                        return html;
                    },
                },
                // {
                //     field: "",
                //     title: "Action",
                //     width: 120,
                //     selector: false,
                //     // sortable: 'asc',
                //     textAlign: 'center',
                //     template: function (row, index, datatable) {
                //         var html = "";
                //         html +='<button type="button" data-position="'+row.position_ID+'" data-recid="'+row.irequestRecruitment_ID+'" data-id="'+row.irequest_ID+'" data-display="pending" class="btn btn-outline-success m-btn m-btn--icon m-btn--icon-only view_recruitmentdet"><i class="fa fa-pencil"></i></button>';
                //         html +='&nbsp;&nbsp;<button type="button" data-recid="'+row.irequestRecruitment_ID+'" data-id="'+row.irequest_ID+'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only cancel_recruitment"><i class="fa fa-remove"></i></button>';
                //         return html;
                //     },
                // },
                ],
            };
            var datatable;
            datatable = $('#irequest_tathiredApplicantsDatatable').mDatatable(options);
        };
        return {
            init: function (start, end,dt) {
                irequestrecord(start, end,dt);
            }
        };
    }();