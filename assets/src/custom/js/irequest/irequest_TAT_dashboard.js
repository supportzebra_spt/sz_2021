$(function(){
    dateRAnge(function(dateRanges){
        getRecReqRecords_Number(dateRanges.start, dateRanges.end);
    });
});
$("#dashBoardTab").on("click", function(){
    dateRAnge(function(dateRanges){
        getRecReqRecords_Number(dateRanges.start, dateRanges.end);
    });
});
$("#irequest_dateduration").on("change", function(){
    // console.log("trigger 3");
    var dateRange = $('#irequest_dateduration').val().split('-');
    getRecReqRecords_Number(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
});

function getRecReqRecords_Number(startDate, endDate){
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/getRecReqRecords_Number",
        data: {
            s: startDate,
            e: endDate
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            console.log(result);
            let vac=0,hire=0,exe=0,cancelled=0,rec=0;
            var btn1 = "";
            var btn2 = "";

            btn1 += '<button id="export_requestRecords_btn" type="button"class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent">EXPORT';
            btn1 += '<span><i class="fa fa-download"></i></span>';
            btn1 += '</button>';

            btn2 += '<button id="export_recruitRecords_btn" type="button"class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent">EXPORT';
            btn2 += '<span><i class="fa fa-download"></i></span>';
            btn2 += '</button>';

            if(result.pending_req[0].pending!=null || result.pending_req[0].pending!=0 || result.approved_req[0].approved!=null || result.approved_req[0].approved!=0  || result.disapproved_req[0].disapproved!=null || result.disapproved_req[0].disapproved!=0  || result.missed_req[0].missed!=null || result.missed_req[0].missed!=0 ){
                $("#irequest_PendingNum").text(result.pending_req[0].pending);
                $("#irequest_ApprovedNum").text(result.approved_req[0].approved);
                $("#irequest_MissedNum").text(result.missed_req[0].missed);
                $("#irequest_DisapprovedNum").text(result.disapproved_req[0].disapproved);
           
                rec = result.all_req[0].all_req;
            }
           if(result.recruitment.error==0)
            {
                vac = result.recruitment.total_count.vacant;
                hire = result.recruitment.total_count.hired;
                exe = result.recruitment.total_count.exceed_30;
                cancelled = result.recruitment.total_count.cancelled;
                $("#addBtn_request2").html(btn2);
            }else{
                $("#export_recruitRecords_btn").remove("");
            }
            $("#irecruit_vacantNum").text(vac);
            $("#irecruit_hiredNum").text(hire);
            $("#irecruit_ExceededNum").text(exe);
            $("#irecruit_CancelledNum").text(cancelled);
            $("#tot_irequest").text(rec);
            if(rec!=0){
                $("#addBtn_request").html(btn1);
            }else{
                $("#export_requestRecords_btn").remove("");
            }
        }
    });
}
function dateRAnge(callback){
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#irequest_dateduration .form-control').val(start + ' - ' + end);
        $('#irequest_dateduration').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end) {
            $('#irequest_dateduration .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            // getRecReqRecords_Number(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'));
        });
        var dateRanges = {
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        };
        callback(dateRanges);
    });
}
$("#addBtn_request").on("click", function(){
    var st_date = $('#irequest_dateduration').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var en_date = $('#irequest_dateduration').data('daterangepicker').endDate.format('YYYY-MM-DD');
    $.ajax({
        url: baseUrl + "/irequest/irequest_excel_report",
        method: 'POST',
        data: {
            start: st_date,
            end: en_date,
        },
        beforeSend: function () {
            mApp.block('body', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg',
                message: 'Downloading...'
            });
        },
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
            console.log(data);
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'iRequest_Report_' + st_date + '_to_' + en_date + '.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
            mApp.unblock('body');
        },
        error: function (data) {
            $.notify({
                message: 'Error in exporting excel'
            }, {
                type: 'danger',
                timer: 1000
            });
            mApp.unblock('body');
        }
    });
});

$("#addBtn_request2").on("click", function(){
    var st_date = $('#irequest_dateduration').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var en_date = $('#irequest_dateduration').data('daterangepicker').endDate.format('YYYY-MM-DD');
    $.ajax({
        url: baseUrl + "/irequest/irecruit_excel_report",
        method: 'POST',
        data: {
            start: st_date,
            end: en_date,
        },
        beforeSend: function () {
            mApp.block('body', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg',
                message: 'Downloading...'
            });
        },
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
            console.log(data);
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'Recruitment_Report_' + st_date + '_to_' + en_date + '.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
            mApp.unblock('body');
        },
        error: function (data) {
            $.notify({
                message: 'Error in exporting excel'
            }, {
                type: 'danger',
                timer: 1000
            });
            mApp.unblock('body');
        }
    });
});

$(".backg_request").on("mouseover", function(){
    $(this).addClass("ShadowClass");
});
$(".backg_request").on("mouseleave", function(){
    $(this).removeClass("ShadowClass");
});

$(".backg_recruit").on("mouseover", function(){
    $(this).addClass("ShadowClass2");
});
$(".backg_recruit").on("mouseleave", function(){
    $(this).removeClass("ShadowClass2");
});


$(".backg_request").on("click", function(){
    var st_date = $('#irequest_dateduration').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var en_date = $('#irequest_dateduration').data('daterangepicker').endDate.format('YYYY-MM-DD');
    initManpowerDetails(st_date,en_date,$(this).data("stat"));
    $("#details_ForCountNumber").modal("show");
    if($(this).data("stat")==2){
        $("#records_laBel_mp").text("Pending");
    }else if($(this).data("stat")==5){
        $("#records_laBel_mp").text("Approved");
    }else if($(this).data("stat")==12){
        $("#records_laBel_mp").text("Missed");
    }else if($(this).data("stat")==6){
        $("#records_laBel_mp").text("Disapproved");
    }
});

$(".backg_recruit").on("click", function(){
    var st_date = $('#irequest_dateduration').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var en_date = $('#irequest_dateduration').data('daterangepicker').endDate.format('YYYY-MM-DD');
    recruitDetails(st_date,en_date,$(this).data("stat"));
    $("#details_ForCountNumber_recruit").modal("show");
});

function recruitDetails(s,e,stat){
    $(".otherVacanttable").hide("");
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/getRequestDetails",
        data: {
            start: s,
            end: e,
            status: stat 
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var td_Stringxx = "";
            var td_Stringxx2 = "";
            var head = "";
            var head2 = "";
            var memdate = "";
            var label = "";
            var label2 = "";    
            
            if(stat=="vacant"){
                head += "<tr>";
                head += "<th>Position</th>";
                head += "<th>Requested Manpower</th>";
                head += "<th>Hired Manpower</th>";
                head += "<th>Manpower to Fill</th>";
                head += "<th>Date Approved</th>";
                head += "</tr>";

                $.each(result.vacant_details,function(key,data){
                    memdate = moment(data.position.datedStatus, 'Y-MM-DD').format('MMM DD, Y');
                    td_Stringxx +="<tr>";
                    td_Stringxx +="<td>"+data.position.pos_details+"</td>";
                    td_Stringxx +="<td>"+data.position.requested+"</td>";
                    td_Stringxx +="<td>"+data.position.hired+"</td>";;
                    td_Stringxx +="<td>"+data.position.vacant+"</td>";
                    td_Stringxx +="<td>"+memdate+"</td>";
                    td_Stringxx +="</tr>";
                });
                label = "Vacant Position(s)";
                if(result.vacant_details==null){
                    $(".allNorecords").show("");
                }else{
                    $(".allNorecords").hide("");
                }
            }else if(stat=="exceeded"){
                head += "<tr>";
                head += "<th>iRequest ID</th>";
                head += "<th>Applicant Name</th>";
                head += "<th>Position</th>";
                head += "<th>Date Hired</th>";
                head += "<th>Ave. Days Filled</th>";
                head += "</tr>";

                head2 += "<tr>";
                head2 += "<th>iRequest ID</th>";
                head2 += "<th>Position</th>";
                head2 += "<th>Manpower Requested</th>";
                head2 += "</tr>";

                $.each(result.exceed_30_applicant,function(key,data){
                    memdate = moment(data.dated_status, 'Y-MM-DD').format('MMM DD, Y');
                    td_Stringxx +="<tr>";
                    td_Stringxx +="<td>"+data.irequest_ID.padLeft(8)+"</td>";
                    td_Stringxx +="<td>"+data.lname+", "+data.fname+"</td>";
                    td_Stringxx +="<td>"+data.pos_details+"</td>";;
                    td_Stringxx +="<td>"+memdate+"</td>";
                    td_Stringxx +="<td>"+data.days_fill_ave+"</td>";
                    td_Stringxx +="</tr>";               
                });
                $.each(result.exceed_30_vacant,function(key,data){
                    td_Stringxx2 +="<tr>";
                    td_Stringxx2 +="<td>"+data.details.irequest_ID.padLeft(8)+"</td>";
                    td_Stringxx2 +="<td>"+data.details.pos_details+"</td>";;
                    td_Stringxx2 +="<td>"+data.details.manpower_requested+"</td>";
                    td_Stringxx2 +="</tr>";               
                });
                $("#detailsForRequest_head_vacant").html(head2);
                $("#detailsForRequest_body_vacant").html(td_Stringxx2);
                $(".otherVacanttable").show("");

                label = "Hired Applicants (Exceeded 30 Days)";
                label2 = "Position(s) Vacant (Exceeded 30 Days)";

                if(result.exceed_30_applicant==0){
                    $(".allNorecords").show("");
                }else{
                    $(".allNorecords").hide("");
                }

                if(result.exceed_30_vacant==0){
                    $(".vacantNorecords").show("");
                }else{
                    $(".vacantNorecords").hide("");
                }      
                
            }else if(stat=="cancelled"){
                head += "<tr>";
                head += "<th>iRequest ID</th>";
                head += "<th>Position</th>";
                head += "<th>Manpower Requested</th>";
                head += "</tr>";

                $.each(result.cancelled_details,function(key,data){
                    td_Stringxx +="<tr>";
                    td_Stringxx +="<td>"+data.details.irequest_ID.padLeft(8)+"</td>";
                    td_Stringxx +="<td>"+data.details.pos_details+"</td>";;
                    td_Stringxx +="<td>"+data.details.manpower_requested+"</td>";
                    td_Stringxx +="</tr>";               
                });
                label = "Cancelled Position";

                if(result.cancelled_details==0){
                    $(".allNorecords").show("");
                }else{
                    $(".allNorecords").hide("");
                }

            }else if(stat=="hired"){
                head += "<tr>";
                head += "<th>iRequest ID</th>";
                head += "<th>Position</th>";
                head += "<th>Applicant Name</th>";
                head += "<th>Ave. Days Filled</th>";
                head += "<th>Date Hired</th>";
                head += "</tr>";

                $.each(result.recruitment_detailed_applicants,function(key,data){
                    memdate = moment(data.datedStatus, 'Y-MM-DD').format('MMM DD, Y');
                    td_Stringxx +="<tr>";
                    td_Stringxx +="<td>"+data.irequest_ID.padLeft(8)+"</td>";
                    td_Stringxx +="<td>"+data.pos_details+"</td>";
                    td_Stringxx +="<td>"+data.lname+", "+data.fname+"</td>";
                    td_Stringxx +="<td>"+data.days_fill_ave+"</td>";
                    td_Stringxx +="<td>"+memdate+"</td>";
                    td_Stringxx +="</tr>";
                });
                label = "Hired Applicants";
                
                if(result.error==1){
                    $(".allNorecords").show();
                }else{
                    $(".allNorecords").hide();;
                }
            }
            $("#records_laBel").text(label);
            $("#records_laBel2").text(label2);

            $("#detailsForRequest_head_vacant").html(head2);
            $("#detailsForRequest_body_vacant").html(td_Stringxx2);
            $("#detailsForRequest_head").html(head);
            $("#detailsForRequest").html(td_Stringxx);

            if(result.error == 1){
                $(".allNorecords").show("");
                $(".vacantNorecords").show("");
            }

            // if(result.error == 1){
            //     $(".allNorecords").show("");
            // }else{
            //     $(".allNorecords").hide("");
            // }        
        }
    });
}

var records_manpowerDetails = function () {
    var irequestrecord = function (start, end,stat) {
        var func_ir ="";
        func_ir = "/irequest/list_get_manpowerDetails";
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + func_ir,
                        params: {
                            query: {
                                startDate: start,
                                endDate: end,
                                status: stat
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [
            {
                field: "irequest_ID",
                title: "iRequest ID",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.irequest_ID.padLeft(8);
                    return html;
                },
            }, {
                field: "position_ID",
                title: "Position",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.pos_details;
                    return html;
                },
            },
            {
                field: "requesting_dept",
                title: "Requesting Team",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.acc_name;
                    return html;
                },
            },
            {
                field: "manpower_requested",
                title: "Manpower Needed",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.manpower_requested;
                    return html;
                },
            },
            {
                field: "",
                title: "Date Requested",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.dateRequested, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            },
            ],
        };
        var datatable;
        datatable = $('#manpowerDetails_Datatable').mDatatable(options);
    };
    return {
        init: function (start, end,stat) {
            irequestrecord(start, end,stat);
        }
    };
}();

function initManpowerDetails(startDate, endDate,stat) {
    $('#manpowerDetails_Datatable').mDatatable('destroy');
    records_manpowerDetails.init(startDate, endDate,stat);
}
function initRecruitDetails(startDate, endDate,stat) {
    $('#manpowerDetails_rec_Datatable').mDatatable('destroy');
    records_RecruitDetails.init(startDate, endDate,stat);
}



