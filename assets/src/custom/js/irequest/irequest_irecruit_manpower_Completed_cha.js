$("#tattab_completedrecruitment").on("click", function(){   
    initRecordDateRangePicker_irecruit_ireq_completed(function (dateRanges) {
        initcompleted_recruitment(dateRanges.start, dateRanges.end,1);
        filters_myreq_pending(9);
    });
    $('#tat_manreqposition_c,#tat_manreqdepartment_c').select2({
        placeholder: "-Select-",
        width: '100%'
    });
});

$("#tat_manreqposition_c,#tat_manreqdepartment_c,#tat_recruitment_id_c,#tat_manreq_stat").on('change', function(){
    var dateRange = $('#recordsall-date_tat_c').val().split('-');
    initcompleted_recruitment(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'),1);
});

function initRecordDateRangePicker_irecruit_ireq_completed(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#recordsall-date_tat_c .form-control').val(start + ' - ' + end);
        $('#recordsall-date_tat_c').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#recordsall-date_tat_c .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initcompleted_recruitment(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'),1);
        });
        var dateRanges = {
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        };
        callback(dateRanges);
    });
}

function initcompleted_recruitment(startDate, endDate,dt) {
    $('#irequest_completedRecruitmentDatatable').mDatatable('destroy');
    records_mycompletedRecruitment.init(startDate, endDate,dt);
}

// My PENDING REQUEST
var records_mycompletedRecruitment = function () {
    var irequestrecord = function (start, end,dt) {
        // var func_ir = (ireq === "1") ? "/irequest/list_irequest_mypending_datatable" : (ireq === "2") ? "/discipline/list_irall_datatable" : (ireq === "3") ? "" : " ";
        var func_ir ="";
        func_ir = "/irequest/list_irequest_recruitment_datatable";
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + func_ir,
                        params: {
                            query: {
                                position: $('#tat_manreqposition_c').val(),
                                req_department: $('#tat_manreqdepartment_c').val(),
                                search_irequestid: $('#tat_recruitment_id_c').val(),
                                status: $("#tat_manreq_stat").val(),
                                // page_type: 'myreq',
                                // cont_access: dt,
                                startDate: start,
                                endDate: end,
                                type: 2
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "irequestRecruitment_ID",
                title: "iRequest-Recruitment ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.irequestRecruitment_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "irequest_ID",
                title: "iRequest ID",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.irequest_ID.padLeft(8);
                    return html;
                },
            }, {
                field: "position_ID",
                title: "Position",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.pos_details;
                    return html;
                },
            },
            {
                field: "requesting_dept",
                title: "Requesting Team",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.acc_name;
                    return html;
                },
            },
            {
                field: "manpower_requested",
                title: "Manpower",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.manpower_supplied +" / "+ row.manpower_requested;
                    return html;
                },
            },
            {
                field: "reqstatus",
                title: "Status",
                width: 90,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    var af="";
                    var desc="";
                    if(row.reqstatus==2){
                        af="m-badge--info";
                        desc="Pending";
                    }else if(row.reqstatus==3){
                        af="m-badge--success";
                        desc="Completed";
                    }else{
                        af="m-badge--danger";
                        desc="Cancelled";
                    }
                    html+='<span style="width: 120px;"><span class="m-menu__link-badge"><span class="m-badge m-badge--wide '+af+'">'+desc+'</span></span></span>';
                    // html = row.statdesc;
                    return html;
                },
            },
            {
                field: "",
                title: "Action",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html +='<button type="button" data-position="'+row.position_ID+'" data-recid="'+row.irequestRecruitment_ID+'" data-id="'+row.irequest_ID+'" data-display="completed" class="btn btn-outline-success m-btn m-btn--icon m-btn--icon-only view_recruitmentdet"><i class="fa fa-pencil"></i></button>';
                    // html +='&nbsp;&nbsp;<button type="button" data-recid="'+row.irequestRecruitment_ID+'" data-id="'+row.irequest_ID+'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only cancel_recruitment"><i class="fa fa-remove"></i></button>';
                    return html;
                },
            },
            ],
        };
        var datatable;
        datatable = $('#irequest_completedRecruitmentDatatable').mDatatable(options);
    };
    return {
        init: function (start, end,dt) {
            irequestrecord(start, end,dt);
        }
    };
}();

$("#irequest_recruitment_completed_col").on('click','.view_recruitmentdet', function(){
    var id_ireq = $(this).data('id');
    let pos = $(this).data('position');
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/manpowerdetails2",
        data: {
            ireq: id_ireq,
            stat: 4,
            requestor: 1,
            approver: 0,
            position: pos
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            // var d_start = result[0].dateStartRequired;
            // var d_end = result[0].dateEndRequired;
            // var m1 = moment(d_start);
            // var m2 = moment(d_end);
            // var S_date = m1.format('MMM D '+', '+'YYYY');
            // var E_date = m2.format('MMM D '+', '+'YYYY');
            var stringx_attachment="";
            var btn_appdisapprove="";
            var stringx_approver="";
            var btn_ned="";
            var btn_sup="";
            var btn_comp="";
            var stringxx_hired="";

            btn_comp += '<button data-status="2" type="button" data-id="'+id_ireq+'" class="btn btn-info autocomplete"><span><i class="fa fa-repeat"></i>';
            btn_comp += '<span> Set Pending for Recruitment</span></span></button>';

            stringx_approver += '<div class="row">';
            stringx_approver += '<div class="col-12">';
            stringx_approver += '<button data-id="'+id_ireq+'" type="button" class="btn btn-link view_approvers_irqdisplay">Click to View Approver Details</button>';
            stringx_approver += '</div>';
            stringx_approver += '</div>';

            btn_appdisapprove += '<button type="button" class="btn btn-secondary" data-dismiss="modal">';
            btn_appdisapprove += '<span><i class="fa fa-close"></i><span> Cancel</span></span>';
            btn_appdisapprove += '</button>';

            btn_ned += '<button type="button" data-id="'+id_ireq+'" data-change="ned" class="btn btn-success m-btn m-btn--icon m-btn--icon-only manpowerchange">';
            btn_ned += '<i class="fa fa-check"></i></button>';

            btn_sup += '<button type="button" data-id="'+id_ireq+'" data-change="sup" class="btn btn-success m-btn m-btn--icon m-btn--icon-only manpowerchange">';
            btn_sup += '<i class="fa fa-check"></i></button>';

           
            $("#recruitment_displaydetails").modal("show");
            $("#approvdis_btn").html(btn_appdisapprove);
            $("#btnfor_completed").html(btn_comp);
            $("#ireq_id_display").text("00"+result.details[0].irequest_ID);
            $("#ireq_pos_display").text(result.details[0].pos_details);
            $("#ireq_reqdept_display").text(result.details[0].acc_name);
            $("#ireq_manpower_display").text(result.details[0].manpower_requested);
            $("#ireq_edreq_display").text(result.details[0].educationalAttainment);
            $("#ireq_gender_display").text(result.details[0].gender);
            $("#ireq_bdesc_display").text(result.details[0].description);
            $("#ireq_prefqual_display").text(result.details[0].qualifications);
            $("#ireq_hpurpose_display").text(result.details[0].hpurpose);
            $("#ireq_dateduration_display").text(result.details[0].days_needed);
            $("#ireq_tHiring_display_recc").text(result.details[0].hiringtype_desc);
            // $("#ireq_dateduration_display").text(S_date + " - " + E_date);
            $("#moredetails_irq").html(stringx_approver);
            $("#update_req").val(result.details[0].manpower_requested);
            $("#update_sup").val(result.details[0].manpower_supplied);
            $("#ireq_manpowersupplied_display").text(result.details[0].manpower_supplied);
            $("#update_manpowerdes").hide("");
            $("#moredetails_irq_recc").html(stringx_approver);
            $("#sel_ApplicantDIV").hide("");
            $("#ireq_manpowerfilled_display").text(result.days);
            // $("#btn-check_ned").html(btn_ned);
            // $("#btn-check_sup").html(btn_sup);

            if(result.details[0].recstatus==2){
                $("#ireq_status_display").text("PENDING RECRUITMENT");
                $("#color_change_dis").addClass("pending_recruitment_css");
                $("#color_change_dis").removeClass("completed_recruitment_css");
                $("#color_change_dis").removeClass("cancelled_recruitment_css");
                $("#NumDaysFilled").hide("");
            }else if(result.details[0].recstatus==3){
                $("#ireq_status_display").text("COMPLETED RECRUITMENT");
                $("#color_change_dis").addClass("completed_recruitment_css");
                $("#color_change_dis").removeClass("cancelled_recruitment_css");
                $("#color_change_dis").removeClass("pending_recruitment_css");
                $("#NumDaysFilled").show("");
            }else{
                $("#ireq_status_display").text("CANCELLED RECORD");
                $("#color_change_dis").addClass("cancelled_recruitment_css");
                $("#color_change_dis").removeClass("completed_recruitment_css");
                $("#color_change_dis").removeClass("pending_recruitment_css");
                $("#NumDaysFilled").hide("");
            }
            if(result.details[0].descriptionAttachment!=null){
                var atf=result.details[0].descriptionAttachment;
                var atf2 = atf.replace("uploads/irequest/job_description/", " ");
                stringx_attachment+='<div class="col-xl-4 col-sm-12"><label class="w_color_label">Attached Job Description </label></div>';
                stringx_attachment+='<div class="col-xl-7 col-sm-12" style="padding-left:2em">';
                stringx_attachment+='<div class="row">';
                stringx_attachment+='<div class="col-12" style="color:darkcyan;font-weight:500"><span>'+atf2+'</span></div>';
                stringx_attachment+='<div class="col-12" style="">';
                stringx_attachment+="<a href='"+baseUrl +"/"+atf+"' class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' download='' style='background: lavender;'><span><i class='fa fa-download'></i>&nbsp;Download Attached File</span></a></div>";
                stringx_attachment+='</div>';
                stringx_attachment+='</div>';           
                $("#ireq_attachedfile_display").html(stringx_attachment);
            }

            if(result.hiredEx==1){
                $.each(result.hired,function(key,data){
                    var d_hired = data.date_hired;
                    var m1 = moment(d_hired);
                    var hired_Date = m1.format('MMM D '+', '+'YYYY');
                    stringxx_hired += '<li data-id="'+data.apid+'" data-application="'+data.application_ID+'" class="list-group-item d-flex justify-content-between align-items-center">'+data.fname+' '+data.mname+' '+data.lname+'<span class="hiredClass"> Date Hired: '+hired_Date+'</span></li>';
                });
            }else{
                stringxx_hired += '<li class="list-group-item d-flex justify-content-between align-items-center" style="text-align: center;color: #e36891;">No Hired Applicants Yet! <i class="la la-exclamation"></i></li>';
            }
            $("#applicant_hiredNames").html(stringxx_hired);
            $('#manpowerToSupplyDIV').hide("");
        }
    });
});

$("#view_btnTRY").on('click', function(){
    // $("#irequest_approverdetails").modal("show");
    // $("#recruitment_displaydetails").modal("hide");
    // $("#dis_appr_bttn").data("id",1);
    // disp_approvers($(this).data("id"));
    console.log("err");
});