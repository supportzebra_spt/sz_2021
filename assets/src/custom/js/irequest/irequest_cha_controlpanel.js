$(function(){
    initapprovers();
    approvaldeadline();
    customPopoverDatePicker("#popover_ireq_deadline");  
    // Validation 
    $("#cp_employees, #ir_addlevel").select2({
      placeholder: "-Select 'Approval For' first-",
      width: '100%'
    });
    
    $('#form_addir_approver').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        ir_addlevel: {
              validators: {
                  notEmpty: {
                      message: 'Level of Approval is required!'
                  },
              }
          },
          ir_addapptype: {
            validators: {
                notEmpty: {
                    message: 'Appoval Type is required!'
                },
            }
        },
          cp_employees: {
              validators: {
                  notEmpty: {
                      message: 'Employee is required!.'
                  },
              }
          },
      }
  }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_addir_approver').formValidation('disableSubmitButtons', true);
      add_irequestapproval();   
  });
});

// UPDATE APPROVER DETAILS
$("#btn_approvalmodalfooter").on('click','.up_ap_details', function(){
    var id_approverid = $(this).data('id');
    var name = $(this).data('name');
    let etype = $(this).data('etype');
    let olevel = $(this).data('level');
    swal({
      title: 'Are you sure you want to update approver details of '+name+'?',
      // text: 'Please check your update details.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Update Details'
  }).then(function (result) {
      if (result.value) {
          $.ajax({
              type: "POST",
              url: baseUrl + "/irequest/update_approver_details",
              data: {
                  ireq_approver: id_approverid,
                  level: $("#ir_addlevel_update").val(),
                  etype: etype,
                  olevel: olevel
                  // approval: $("#ir_addapptype_update").val()
              },
              cache: false,
              success: function(res) {
                $('#irequest_updateapprover').modal('hide');
                swal({
                  position: 'center',
                  type: 'success',
                  title: 'Successfully Updated Approver Details!',
                  showConfirmButton: false,
                  timer: 1500
              });
                  initapprovers();
              },
              error: function(res) {
                  swal(
                      'Oops!',
                      'Something is wrong with your code!',
                      'error'
                  )
              }
          });
      } else if (result.dismiss === 'cancel') {
          swal(
              'Cancelled',
              'Updating of Approver details has been cancelled.',
              'error'
          )
      }
  });
});

$("#irequest_addapprover").on('hide.bs.modal', function() {
  formReset('#form_addir_approver');
  $("#ir_addlevel").prop("selectedIndex", 0);
  $("#ir_addapptype").prop("selectedIndex", 0);
  $("#cp_employees").empty();
  $("#ir_addlevel").empty();
});

$("#popover_ireq_deadline").on('click', function(){
  // customPopoverDatePicker("#popover_ireq_deadline");
  var val_a=$("#popover_ireq_deadline").text();
  $("#deadline_ireq_val").val(val_a);
  approvaldeadline();
});

$("#add_approver_irq").on('click', function(){
    $("#irequest_addapprover").modal("show");
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/emp_details_ir_approvers",
        cache: false,
        success: function(res) {
            var result = JSON.parse(res);
            var stringx="<option disabled selected=''> - Select Employee - </option>";
            // $.each(result, function (key, data) {
            //     stringx += '<option value="'+data.emp_ID+'">'+data.lname+" , "+data.fname+" "+data.mname+'</option>';
            // });
            // $("#cp_employees").html(stringx);
        }
    })
});

$("#ireqapprover_col").on('click','.remove_ireq_approver', function(){
  var approver_id= $(this).data('id');
  var name=$(this).data('name');
  let apptype=$(this).data('type');
  let level=$(this).data('level');
    swal({
      title: 'Are you sure you want to remove '+name+' as an approver?',
      text: 'This record will not be retrieved once removed but can add this name again with new record ID.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Remove Approver'
  }).then(function (result) {
      if (result.value) {
          $.ajax({
              type: "POST",
              url: baseUrl + "/irequest/remove_approver",
              data: {
                  ireq_approver: approver_id,
                  apptype: apptype,
                  level: level
              },
              cache: false,
              success: function(res) {
                swal({
                  position: 'center',
                  type: 'success',
                  title: 'Successfully Removed Approver!',
                  showConfirmButton: false,
                  timer: 1500
              });
                  initapprovers();
              },
              error: function(res) {
                  swal(
                      'Oops!',
                      'Something is wrong with your code!',
                      'error'
                  )
              }
          });
      } else if (result.dismiss === 'cancel') {
          swal(
              'Cancelled',
              'Removing of Approver has been cancelled.',
              'error'
          )
      }
  });
});

$("#ireqapprover_col").on('click', '.update_ireq_approver', function(){
  $("#irequest_updateapprover").modal("show");
  $approver_ID= $(this).data('id');
    $.ajax({
      type: "POST",
      url: baseUrl + "/irequest/get_ir_approversdetails",
      data: {
        approver: $approver_ID,
        emptype: $(this).data('type')
      },
      cache: false,
      success: function(res) {
          var result = JSON.parse(res);
          var a_level="";
          var b_level="";
          var c_level="";
          var a_approval="";
          var b_approval="";
          var c_approval="";
          var level_stringx="";
          var approval_stringx="";
          var btn_saveupdate="";
          $(".approver_det_name").text(result.info.fname+" "+result.info.lname);
          // if(result.info.level==1){ a_level ="selected";}else if(result.info.level==2){b_level="selected";}else{c_level="selected";}
          if(result.info.emp_type=="admin"){a_approval="selected";}else if(result.info.emp_type=="agent"){b_approval="selected";}else{c_approval="selected";}   
          
          // Should Change this 
          for(i=1;i<=result.highest_level;i++){
             if(i==result.info.level){
              a_level ="selected";
             }
             level_stringx+='<option value="'+i+'" '+a_level+'>'+i+'</option>';
          }
          // approval_stringx+='<option value="admin" '+a_approval+'>Admin</option>';
          // approval_stringx+='<option value="agent" '+b_approval+'>Agent</option>';

          btn_saveupdate+='<button type="button" data-level="'+result.info.level+'" data-etype="'+result.info.emp_type+'" data-name="'+result.info.fname+" "+result.info.lname+'" data-id="'+$approver_ID+'" class="btn btn-success up_ap_details">Save Changes</button>';
          $("#ir_addlevel_update").html(level_stringx);
          // $("#ir_addapptype_update").html(approval_stringx);
          $("#btn_approvalmodalfooter").html(btn_saveupdate);
      }
    })
});
function initapprovers() {
  $('#irequest_approversdatatable').mDatatable('destroy');
  records_approversDt.init();
}

$("#filter_emptype").on('change', function(){
  initapprovers();
});

var records_approversDt = function () {
    var irequestrecord = function () {
        var func_ir ="";
        func_ir = "/irequest/list_irequest_approver_datatable";
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + func_ir,
                        params: {
                            query: {
                                app_type: $("#filter_emptype").val(),
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 600, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: false,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "approver_ID",
                title: "Approver ID",
                width: 80,
                selector: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.approver_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "lname",
                title: "Approver",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.fname+" "+row.lname;
                    return html;
                },
            },{
                field: "level",
                title: "Level",
                width: 50,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.level;
                    return html;
                },
            },
              {
                field: "emp_type",
                title: "For",
                width: 60,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.emp_type;
                    return html;
                },
              },
            {
                field: "",
                title: "Action",
                width: 80,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html +='<button type="button" data-type="'+row.emp_type+'" data-id="'+row.approver_ID+'" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill update_ireq_approver"><i class="fa fa-edit"></i></button>';
                    html +='&nbsp;&nbsp;<button type="button" data-level="'+row.level+'" data-type="'+row.emp_type+'" data-name="'+row.fname+" "+row.lname+'" data-id="'+row.approver_ID+'" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill remove_ireq_approver"><i class="fa fa-remove"></i></button>';
                    return html;
                },
            },
            ],
        };
        var datatable;
        datatable = $('#irequest_approversdatatable').mDatatable(options);
    };
    return {
        init: function () {
            irequestrecord();
        }
    };
}();

// POP OVER 

var customPopoverDatePicker = function(elemx) {
    var elem = $(elemx);
    var id = elem.data('id');
    var value = elem.data("value");
    if (value === null) {
      value = '';
    }
    var buttons = '<button title="Save Changes" class="btn btn-outline-info btn-sm py-1 px-1 popover-approve" type="button"><i class="fa fa-check" style="width:20px;font-size:15px;line-height: 1;"></i></button> <button title="Cancel Changes" class="btn btn-outline-danger btn-sm  py-1 px-1 popover-reject" type="button"><i class="fa  fa-times"  style="width:20px;font-size:15px;line-height: 1;"></i></button>';
    var content = '<input id="deadline_ireq_val" type="number"  name="" class="form-control form-control-sm">';
    var clearfield = '<button title="Clear Input" class="btn btn-outline-warning btn-sm py-1 px-1 popover-clearfield" type="button" style="margin-right:3px"><i class="fa fa-repeat" style="width:20px;font-size:15px;line-height: 1;"></i></button>';
    if (value === "" || value === null) {
    //   elem.html('0');
      elem.addClass('m--font-danger font-italic');
    }
    var title = "Set Deadline";
    var checklistOptions = {
      orientation: "bottom left",
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
      },
    };  
    elem.popover({
      trigger: 'click',
      placement: 'bottom',
      title: "<span style='color:white'>" + title + "</span>",
      html: true,
      template: '<div class="m-popover popover" role="tooltip" style="z-index:100!important">\
                    <div class="arrow"></div>\
                    <h3 class="popover-header"></h3>\
                    <div class="popover-body p-2"></div>\
                    </div>',
      content: '<div class="row no-gutters" data-id="" data-type=""><div class="col-md-7 col-xs-6">' + content + '</div><div class="col-md-5 col-xs-6 text-right">' + clearfield + buttons + '</div></div>'
    }).on('click', function(e) {
      $('[data-original-title]').not(elem).popover('hide');
      e.preventDefault();
    }).on('inserted.bs.popover', function() {
      var popover_elem = $(this).data("bs.popover").tip;
    }).on("show.bs.popover", function() {
      var el = $(this).data("bs.popover").tip;
      $(el).css("max-width", "500px");
    });
};

  $("html").on('click', '.popover-reject', function() {
    $(this).closest('.popover').popover('hide');
  });
  $("html").on('click', '.popover-clearfield', function() {
    $(this).closest('.popover').find('.form-control').val('');
  });
  $("html").on('click', '.popover-approve', function() {
    $.ajax({
        type: "POST",
        url: baseUrl + "/irequest/save_deadlineApproval",
        data: {
          deadline: $("#deadline_ireq_val").val()
        },
        cache: false,
        success: function(res) {
          $('.popover').popover('hide');
          approvaldeadline();
          swal({
            position: 'center',
            type: 'success',
            title: 'Successfully Set Deadline!',
            showConfirmButton: false,
            timer: 1500
        });
        }
      });
  });

//Function
function add_irequestapproval(){
  $.ajax({
      type: "POST",
      url: baseUrl + "/irequest/insert_irequest_approver",
      data: {
        emp: $("#cp_employees").val(),
        level: $("#ir_addlevel").val(),
        approval: $("#ir_addapptype").val()
      },
      cache: false,
      success: function(res) {
        initapprovers();
        $('#irequest_addapprover').modal('hide');
        swal({
                position: 'center',
                type: 'success',
                title: 'Successfully Added Approver!',
                showConfirmButton: false,
                timer: 1500
        });
      }
  });
}
//Fetch approval deadline
function approvaldeadline(){
  $.ajax({
    type: "POST",
    url: baseUrl + "/irequest/fetch_approvaldeadline",
    cache: false,
    success: function(res) {
      var result = JSON.parse(res);
      $("#popover_ireq_deadline").text(result[0].days);
      $("#deadline_ireq_val").val(result[0].days);
    }
  });
}

$("#ir_addapptype").on("change", function(){
    $.ajax({
      type: "POST",
      url: baseUrl + "/irequest/queryApproval_employees_level",
      data: {
        approval: $("#ir_addapptype").val()
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res);
        let stringx="<option disabled selected=''> - Select Employee - </option>";
        let stringxL="<option disabled selected=''> - Select Level - </option>";
        // initapprovers();
        // $('#irequest_addapprover').modal('hide');
        // swal({
        //         position: 'center',
        //         type: 'success',
        //         title: 'Successfully Added Approver!',
        //         showConfirmButton: false,
        //         timer: 1500
        // });
        $.each(result.employees, function (key, data) {
            stringx += '<option value="'+data.emp_ID+'">'+data.lname+" , "+data.fname+" "+data.mname+'</option>';
        });
        if(result.level==0){
          // no existing approvers 
          stringxL += '<option value="1">1</option>';
        }else{
            // display according to highest number to possibly add 
            for(i = 1;i<=result.highest_num;i++){
              stringxL += '<option value="'+i+'">'+i+'</option>';
            }
        }
        $("#cp_employees").html(stringx);
        $("#ir_addlevel").html(stringxL);
      }
  });
});
