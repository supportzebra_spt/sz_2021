var shiftStartToShow = '';
var shiftEndToShow = '';
var shiftStartToCompute = '';
var shiftEndToCompute = '';

function getPersonalAhrCount(callback) {
    $.when(fetchGetData('/additional_hour/get_ahr_personal_count')).then(function (statCount) {
        statCountObj = $.parseJSON(statCount.trim());
        $('#requestCount').text(statCountObj.request);
        $('#pendingCount').text(statCountObj.pending);
        $('.dueCount').text(statCountObj.missed);
        if (parseInt(statCountObj.request) > 0) {
            $('#pendingPersonalAhrRequest').find('.request').addClass('m-animate-shake');
        } else {
            $('#pendingPersonalAhrRequest').find('.request').removeClass('m-animate-shake');
        }
        if (parseInt(statCountObj.pending) > 0) {
            $('#pendingPersonalAhrRequest').find('.pending').addClass('fa-spin');
        } else {
            $('#pendingPersonalAhrRequest').find('.pending').removeClass('fa-spin');
        }
        if (parseInt(statCountObj.missed) > 0) {
            $('#pendingPersonalAhrRequest').find('.missed').addClass('m-animate-blink');
            $('#dueNotif').removeClass('d-none');
            $('#dueNotif')[0].removeAttribute('style');
            if (parseInt(statCountObj.missed) > 1) {
                $('#requestForm').text('Requests');
            } else {
                $('#requestForm').text('Request');
            }
        } else {
            $('#dueNotif').addClass('d-none');
            $('#pendingPersonalAhrRequest').find('.missed').removeClass('m-animate-blink');
        }
        callback(statCountObj);
    });
}

// DATATABLE ----------

var personalPendingAhrDatatable = function () {
    var personalPendingAhr = function (requestStat, searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour/get_personal_pending_ahr_request",
                        params: {
                            query: {
                                requestStat: requestStat,
                                personalPendingAhrSearch: searchVal
                            },
                        },
                        map: function (raw) {
                            console.log('data:');
                            console.log(raw);
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            // $('#pendingCount').text(dataSet.length);
                       
                            getPersonalAhrCount(function (stat) {
                            })
                            return dataSet;
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 540, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },

            search: {
                input: $('#personalPendingAhrSearch'),
            },
            // columns definition
            columns: [{
                    field: "additionalHourRequestId",
                    title: "AHR ID",
                    width: 80,
                    selector: false,
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        // console.log(row.additionalHourRequestId.padLeft(8));
                        var ahrId = row.additionalHourRequestId.padLeft(8);
                        var html = "<td'>" + ahrId + "</td>";
                        return html;
                    }
                }, {
                    field: 'sched_date',
                    width: 100,
                    title: 'Schedule',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                        return html;
                    },
                }, {
                    field: 'dateTimeFiled',
                    width: 100,
                    title: 'Filed On',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = "<td>" + moment(row.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</td>";
                        return html;
                    },
                }, {
                    field: 'typesDescription',
                    width: 60,
                    title: 'TYPE',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center',
                    // template: function(row, index, datatable) {
                    //     var html = "<td class='text-center'>" + row.typesDescription + "</td>";
                    //     return html;
                    // },
                }
                // , {
                //     field: 'accomplished',
                //     width: 200,
                //     title: 'Date and Time Accomplished',
                //     overflow: 'visible',
                //     sortable: false,
                //     textAlign: 'center',
                //     template: function(row, index, datatable) {
                //         var html = "<td class = 'col-md-4'><p>" + row.otStartDate + " " + row.otStartTime + "</p><p>" + row.otEndDate + " " + row.otEndTime + "</p></td>";
                //         return html;
                //     },
                // }
                , {
                    field: 'totalOtHours',
                    width: 100,
                    title: 'Time',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function(row, index, datatable) {
                        var html ="";
                        humanTimeFormat(row.totalOtHours, function (formattedHours) {
                            // $('#additionalVal').text(formattedBct);
                            html = "<span class='text-center'>" + formattedHours + "</span>";
                        });
                        return html;
                    },
                }, {
                    field: 'statusDescription',
                    width: 80,
                    title: 'Status',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        if (parseInt(row.approvalStatus_ID) == 12) {
                            var badgeColor = 'btn-warning';
                            var dotAlert = "<span class='m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink mr-1' style='min-height: 7px;min-width: 7px;'></span>";
                        } else if (parseInt(row.approvalStatus_ID) == 2) {
                            var badgeColor = 'btn-info';
                            var dotAlert = "";
                        }
                        var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + "' onclick='approverInfo(" + row.additionalHourRequestId + ")'>"+dotAlert+" " + row.statusDescription + "</span>";
                        return html;
                    },

                }, {
                    field: 'action',
                    width: 70,
                    title: 'Action',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = '<span><a id="remove' + row.additionalHourRequestId + '" class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" data-toggle="m-tooltip" title="Remove Request" onclick="removeAhr(' + row.additionalHourRequestId + ')"><i class="fa fa-remove"></i></a>&nbsp;&nbsp;<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View details" onclick="initRecordInfo(' + row.additionalHourRequestId + ', ' + row.additionalHourTypeId + ', \'' + row.requestType + '\')"><i class="fa fa-search"></i></a><span>';
                        return html;
                    }
                }
            ],
        };
        options.search = {
            input: $('#personalPendingAhrSearch'),
        };
        var datatable = $('#personalAhrPendingDatatable').mDatatable(options);
    };
    return {
        init: function (requestStat, searchVal) {
            personalPendingAhr(requestStat, searchVal);
        }
    };

}();

var personalRecordAhrDatatable = function () {
    var personalRecordAhr = function (statusVal, searchVal, dateFrom, dateTo) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour/get_personal_record_ahr_request",
                        params: {
                            query: {
                                personalAhrRecordSearch: searchVal,
                                requestStat: statusVal,
                                rangeStart: dateFrom,
                                rangeEnd: dateTo
                            },
                        },
                        map: function (raw) {
                                // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            // $('#pendingCount').text(dataSet.length);
                            return dataSet;
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },

            search: {
                input: $('#personalAhrRecordSearch'),
            },
            // columns definition
            columns: [{
                    field: "additionalHourRequestId",
                    title: "AHR ID",
                    width: 80,
                    selector: false,
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var ahrId = row.additionalHourRequestId.padLeft(8);
                        var html = "<td'>" + ahrId + "</td>";
                        return html;
                    }
                }, {
                    field: 'sched_date',
                    width: 100,
                    title: 'Schedule',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                        return html;
                    },
                }, {
                    field: 'dateTimeFiled',
                    width: 150,
                    title: 'Filed On',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = "<td>" + moment(row.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</td>";
                        return html;
                    },
                }, {
                    field: 'typesDescription',
                    width: 80,
                    title: 'TYPE',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center'
                }, {
                    field: 'totalOtHours',
                    width: 100,
                    title: 'Time',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = "";
                        humanTimeFormat(row.totalOtHours, function (formattedHours) {
                            // $('#additionalVal').text(formattedBct);
                            html = "<span class='text-center'>" + formattedHours + "</span>";
                        });
                        return html;
                    },
                }, {
                    field: 'statusDescription',
                    width: 80,
                    title: 'Status',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        if (parseInt(row.approvalStatus_ID) == 12) {
                            var badgeColor = 'btn-warning';
                        } else if (parseInt(row.approvalStatus_ID) == 2) {
                            var badgeColor = 'btn-info';
                        } else if (parseInt(row.approvalStatus_ID) == 5){
                            var badgeColor = 'btn-success';
                        } else if (parseInt(row.approvalStatus_ID) == 6) {
                            var badgeColor = 'btn-danger';
                        }
                        var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide' onclick='approverInfo(" + row.additionalHourRequestId + ")'>" + row.statusDescription + "</span>";
                        return html;
                    },
                }, {
                    field: 'action',
                    width: 60,
                    title: 'Action',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        console.log(row.additionalHourTypeId);
                        console.log(row.requestType);
                        var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View details" onclick="initRecordInfo(' + row.additionalHourRequestId + ',' + row.additionalHourTypeId + ', \'' + row.requestType + '\')"><i class="fa fa-search"></i></a>';
                        return html;
                    }
                }
            ],
        };
        options.search = {
            input: $('#personalAhrRecordSearch'),
        };
        var datatable = $('#personalAhrRecordDatatable').mDatatable(options);
    };
    return {
        init: function (statusVal, searchVal, dateFrom, dateTo) {
            personalRecordAhr(statusVal, searchVal, dateFrom, dateTo);
        }
    };

}();


function initDisabled() {
    $('#startDate').prop('disabled', true);
    $('#startTime').prop('disabled', true);
    $('#endDate').prop('disabled', false);
    $('#endTime').prop('disabled', false);
    $('#additional').hide();
}

function resetManualForm() {
    $('#unrenderedOt').text('00 hr & 00 min');
    $('#unrenderedOt').data('otHours', 0);
    $('#schedDate').val('');
    $('select[name="shift"]').html('');
    $('#startDate').val('');
    $('#startTime').val('').timepicker('setDefaultTime', 'current');
    $('#endDate').val('');
    $('#endTime').val('').timepicker('setDefaultTime', 'current');
    $('#reasonManual').val('');
    $('#errNotifyUnrenderedAhr').hide();
    $('.character-remaining').text('');
    $('#requestManualAhrBtn').prop('disabled', false);
}

function getAccountShifts(callback) {
    $.when(fetchPostData({}, '/additional_hour/get_account_shifts')).then(function (accountShifts) {
        var accShifts = $.parseJSON(accountShifts.trim());
        callback(accShifts);
    });
}

function datedShift(bct, breaks, callback) {
    var dateSched = moment($('#schedDate').val(), 'MM/DD/YYYY').format('YYYY-MM-DD');
    var time = $('#shiftunrendered').find('option:selected').text().split('-');
    var rawShiftStart = moment(time[0], 'hh:mm:ss A').format('HH:mm');
    var shiftStart = moment(dateSched + " " + time[0], "YYYY-MM-DD 'hh:mm:ss A").valueOf();
    var shiftEnd = moment(dateSched + " " + time[1], "YYYY-MM-DD 'hh:mm:ss A").valueOf();

    if (rawShiftStart == "00:00") {
        shiftEnd = moment(shiftEnd).add(1, 'days').valueOf();
        shiftStart = moment(shiftStart).add(1, 'days').valueOf();
    }

    shiftStart = moment(shiftStart).subtract(bct, 'minutes').valueOf();

    if (shiftStart > shiftEnd) {
        shiftEnd = moment(shiftEnd).add(1, 'days').valueOf();
    }

    if (moment(shiftStart).format('HH:mm') == '00:00') {
        shiftStartToShow = moment(moment(shiftStart).subtract(1, 'days').valueOf()).format('MM/DD/YYYY hh:mm A');
    } else {
        shiftStartToShow = moment(shiftStart).format('MM/DD/YYYY hh:mm A');
    }
    if (moment(shiftEnd).format('HH:mm') == '00:00') {
        shiftEndToShow = moment(moment(shiftEnd).subtract(1, 'days').valueOf()).format('MM/DD/YYYY hh:mm A');
    } else {
        shiftEndToShow = moment(shiftEnd).format('MM/DD/YYYY hh:mm A');
    }
    console.log(rawShiftStart);
    shiftStartToCompute = moment(shiftStart).format('MM/DD/YYYY hh:mm A');
    shiftEndToCompute = moment(shiftEnd).format('MM/DD/YYYY hh:mm A');
    console.log("SHIFT START: " + moment(shiftStart).format('MM/DD/YYYY hh:mm A'));
    console.log("SHIFT END: " + moment(shiftEnd).format('MM/DD/YYYY hh:mm A'));
    console.log("shift start to show:" + shiftStartToShow);
    console.log("shift end to show:" + shiftEndToShow);
    callback();
    $('#additionalVal').text('');
    if ($('#ahrTypeUnrendered').val() == 1) {
        $('#startDate').val(moment(shiftEndToShow, 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY'));
        $('#startTime').val(moment(shiftEndToShow, 'MM/DD/YYYY hh:mm A').format('hh:mm A'));
        $('#endDate').val('');
        $('#endTime').timepicker('setDefaultTime', 'current');
    } else if ($('#ahrTypeUnrendered').val() == 2) {
        $('#startDate').val('');
        $('#startTime').timepicker('setDefaultTime', 'current');
        $('#endDate').val(moment(shiftStartToShow, 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY'));
        $('#endTime').val(moment(shiftStartToShow, 'MM/DD/YYYY hh:mm A').format('hh:mm A'));
        var bctMin = (bct) / 60;
        $('#additionalVal').data('bct', bct)
        humanTimeFormat(bctMin, function (formattedBct) {
            $('#additionalVal').text(formattedBct);
        })
    }
}

function validateUnrenderedAhrForm(callback) {
    console.log($('#ahrTypeUnrendered').val());
    console.log($('#schedDate').val());
    console.log($('#shiftunrendered').val());
    console.log($('#startDate').val());
    console.log($('#startTime').val());
    console.log($('#endDate').val());
    console.log($('#endTime').val());

    var fieldError = 0;
    var errorMssg = "Fields are required: ";
    if ($('#ahrTypeUnrendered').val() == '') {
        errorMssg += 'AHR TYPE';
        fieldError = 1;
    } else {
        if ($('#ahrTypeUnrendered').val() != 3) {
            if ($('#shiftunrendered').val() == null || $('#shiftunrendered').val() == 0) {
                errorMssg += ' Shift,';
                fieldError = 1;
            }
        }
    }

    if ($('#schedDate').val() == '') {
        errorMssg += ' Date Schedule,';
        fieldError = 1;
    }

    if ($('#startDate').val() == '') {
        errorMssg += ' Start Date,';
        fieldError = 1;
    }

    if ($('#startTime').val() == '') {
        errorMssg += ' Start Time,';
        fieldError = 1;
    }
    if ($('#endDate').val() == '') {
        errorMssg += ' End Date,';
        fieldError = 1;
    }
    if ($('#endTime').val() == '') {
        errorMssg += ' End Time,';
        fieldError = 1;
    }

    if (!$.trim($("#reasonManual").val())) {
        errorMssg += ' Reason,';
        fieldError = 1;
    }
    if ($('#unrenderedOt').data('otHours') == undefined || $('#unrenderedOt').data('otHours') == 0) {
        fieldError = 1;
    }

    if (fieldError) {
        $('#errNotifyUnrenderedAhrMssg').html(errorMssg);
        $('#errNotifyUnrenderedAhr').show();
        // $('#requestManualAhrBtn').prop('disabled', true);
    } else {
        $('#errNotifyUnrenderedAhr').hide();
        $('#requestManualAhrBtn').prop('disabled', false);
    }
    console.log(errorMssg);
    callback(fieldError);
}

function getBctAndBreakNoSched(callback) {
    $.when(fetchPostData({
        schedId: $('#shiftunrendered').find('option:selected').val()
    }, '/additional_hour/get_breaks_for_unrendered_filing')).then(function (res) {
        var bctBreak = jQuery.parseJSON(res.trim());
        datedShift(bctBreak.bct, bctBreak.totalBreak.minutes, function () {});
        // $('#unrenderedAhrForm').formValidation('validateField', 'shiftunrendered');
    });
    callback();
}

function getBctAndBreak(callback) {
    var bctBreakObj = '';
    $.when(fetchPostData({
        schedId: $('#shiftunrendered').find('option:selected').val()
    }, '/additional_hour/get_breaks_for_unrendered_filing')).then(function (res) {
        var bctBreak = jQuery.parseJSON(res.trim());
        datedShift(bctBreak.bct, bctBreak.totalBreak.minutes, function () {});
    });
    callback();
}

function updateFormLayout() {
    $('#reasonManual').prop('disabled', false);
    if ($('#ahrTypeUnrendered').val() == 1) {
        $('#shiftField').show();
        $('#shiftWord').hide();
        // $('#endDate').val('');
        $('#endTime').timepicker('setDefaultTime', 'current');
        // $('#unrenderedAhrForm').formValidation('resetField', 'endTime', true);
        $('#unrenderedAhrForm').formValidation('resetField', 'endDate', true);
        $('#startDate').prop('disabled', true);
        $('#startTime').prop('disabled', true);
        $('#endDate').prop('disabled', false);
        $('#endTime').prop('disabled', false);
        $('#shiftunrendered').show()
        $('#additional').hide();
    } else if ($('#ahrTypeUnrendered').val() == 2) {
        $('#shiftField').show();
        $('#shiftWord').hide();
        // $('#startDate').val('');
        $('#startTime').timepicker('setDefaultTime', 'current');
        // $('#unrenderedAhrForm').formValidation('resetField', 'startTime', true);
        $('#unrenderedAhrForm').formValidation('resetField', 'startDate', true);
        $('#endDate').prop('disabled', true);
        $('#endTime').prop('disabled', true);
        $('#startDate').prop('disabled', false);
        $('#startTime').prop('disabled', false);
        $('#shiftunrendered').show()
        $('#additionalLabel').text('BCT');
        $('#additional').show();
    } else {
        $('#shiftField').hide();
        getAccountShifts(function (accShifts) {
            var optionsAsString = "";
            $.each(accShifts, function (index, value) {
                optionsAsString += "<option value='" + value.acc_time_id + "'>" + value.time_start + " - " + value.time_end + "</option>";
            });
            $('#shiftWordUnrendered').html(optionsAsString);
            // $("#shiftWordUnrendered").select2("destroy").select2();
            $('#shiftWordUnrendered').select2({
                placeholder: "Select a shift",
                width: '100%'
            });
            $('#shiftWord').show();
        })
        $('#endDate').prop('disabled', true);
        $('#endTime').prop('disabled', true);
        $('#startDate').prop('disabled', true);
        $('#startTime').prop('disabled', true);
        // $('#shiftunrendered').hide()
        $('#additionalLabel').text('TOTAL GIVEN BREAK');
        $('#additional').show();

    }
}

function assignTimeDate(dateSched, shift) {
    var shiftVal = shift.split('-');
    console.log(shift);
    var shiftStart = moment(dateSched + " " + shiftVal[0], "MM/DD/YYYY hh:mm:ss A").valueOf();
    var shiftEnd = moment(dateSched + " " + shiftVal[1], "MM/DD/YYYY hh:mm:ss A").valueOf();
    console.log(moment(shiftStart).format('Y-MM-DD hh:mm A'));
    console.log(moment(shiftEnd).format('Y-MM-DD hh:mm A'));
    if (shiftStart > shiftEnd) {
        shiftEnd = moment(shiftEnd).add(1, 'days').valueOf();
    }
    shiftStartToCompute = moment(shiftStart).format('MM/DD/YYYY h:mm A');
    shiftEndToCompute = moment(shiftEnd).format('MM/DD/YYYY h:mm A');
    console.log('shift end:' + moment(shiftEnd).format('Y-MM-DD hh:mm A'));
    if (moment(shiftStart).format('HH:mm') == '00:00') {
        console.log('shift start is 12 am');
        var endDateTime = moment(shiftEnd).add(1, 'days').valueOf();
    } else if (moment(shiftEnd).format('HH:mm') == '00:00') {
        console.log('shift end is 12 am');
        var endDateTime = moment(shiftEnd).subtract(1, 'days').valueOf();
    } else {
        var endDateTime = shiftEnd;
    }
    console.log(moment(endDateTime).format('MM/DD/YYYY hh:mm A'));
    $('#startDate').val(moment(shiftStart).format('MM/DD/YYYY'));
    $('#startTime').val(moment(shiftStart).format('h:mm A'));
    $('#endDate').val(moment(endDateTime).format('MM/DD/YYYY'));
    $('#endTime').val(moment(endDateTime).format('h:mm A'))

    computeManualAhrTime(function (renderedOt) {
        console.log(renderedOt);
        $('#unrenderedOt').text(renderedOt.formatted);
        $('#unrenderedOt').data('otHours', renderedOt.otHours);
    })

}

function checkIfAlreadyFiledNoSched(ahrTypeVal, schedDate, callback) {
    $.when(fetchPostData({
        ahrType: ahrTypeVal,
        dateSched: schedDate
    }, '/additional_hour/check_if_filed_no_sched')).then(function (checkIfFiled) {
        var checkIfFiledBool = $.parseJSON(checkIfFiled.trim());
        // console.log(existValue);
        callback(checkIfFiledBool);
    });
}

function getShiftUnrendered(date, callback) {
    $.when(fetchPostData({
        scheduleDate: date
    }, '/additional_hour/get_shift')).then(function (shift) {
        var shift = jQuery.parseJSON(shift.trim());
        var ahrTypeVal = $('#ahrTypeUnrendered').val();
        var optionsAsString = "";
        console.log(shift);
        if (!shift) {
            var exist = 0;
                $('#shiftunrendered').html(optionsAsString);
        } else {
            var exist = 1;
             $('#unrenderedAhrForm').formValidation('updateStatus', 'shiftunrendered', 'VALID');
            if (shift.length > 1) {
                optionsAsString += "<option value ='0' hidden>Select a Shift</option>";
               
            }
            $.each(shift, function (index, value) {
                optionsAsString += "<option value='" + value.schedId + "' shiftType='" + value.type + "'>" + value.time_start + " - " + value.time_end + "</option>";
                $('#shiftunrendered').data('empid', value.empId);
                $('#shiftunrendered').data('accountid', value.accountId);
            });
             $('#shiftunrendered').html(optionsAsString);
             $('#unrenderedAhrForm').formValidation('validateField', 'shiftunrendered');
            if (shift.length == 1) {
                var schedIdVal = $('#shiftunrendered').val();
                console.log("sched ID: "+schedIdVal);
                checkIfAlreadyFiled(ahrTypeVal, schedIdVal, function (checkIfFiledBool) {
                    // $('#shiftunrendered').trigger('change');
                    if (parseInt(checkIfFiledBool.exist)) {
                        $('#errNotifyUnrenderedAhrMssg').html('<b>Request Exist: </b>You have already filed a request for this schedule.');
                        $('#errNotifyUnrenderedAhr').show();
                        $('#startDate, #startTime, #endDate, #endTime, #reasonManual, #requestManualAhrBtn').prop('disabled', true);
                    } else {
                        $('#errNotifyUnrenderedAhr').hide();
                        $('#shiftunrendered').trigger('change');
                        $('#startDate, #startTime, #endDate, #endTime, #reasonManual, #requestManualAhrBtn').prop('disabled', false);
                        $('#unrenderedAhrForm').formValidation('updateStatus', 'shiftunrendered', 'VALID');
                        updateFormLayout();
                        getBctAndBreak(function () {});
                    }
                })
            }
        }
        callback(exist)
    });
}

function checkAhrTypeForShiftChecking() {
    if ($('#ahrTypeUnrendered').val() != 3) {
        getShiftUnrendered($('#schedDate').val(), function (exist) {
            if (exist) {
                checkIfAlreadyFiled($('#ahrTypeUnrendered').val(), $('#shiftunrendered').val(), function (checkIfFiledBool) {
                    if (parseInt(checkIfFiledBool.exist)) {
                        $('#errNotifyUnrenderedAhrMssg').html('<b>Request Exist: </b>You have already filed a request for this schedule.');
                        $('#errNotifyUnrenderedAhr').show();
                        $('#startDate, #startTime, #endDate, #endTime, #reasonManual, #requestManualAhrBtn').prop('disabled', true);
                    } else {
                        $('#errNotifyUnrenderedAhr').hide();
                        $('#shiftunrendered, #startDate, #startTime, #endDate, #endTime, #reasonManual, #requestManualAhrBtn').prop('disabled', false);
                        updateFormLayout();
                        getBctAndBreak(function () {});
                    }
                })
            } else {
                $('#errNotifyUnrenderedAhrMssg').html('<b>No Schedule found!</b> Please inform your Supervisor');
                $('#errNotifyUnrenderedAhr').show();
                $('#shiftunrendered, #startDate, #startTime, #endDate, #endTime, #reasonManual, #requestManualAhrBtn').prop('disabled', true);
                $('#shiftField').show();
            }
        });
    } else {
        $.when(fetchPostData({
            scheduleDate: moment($('#schedDate').val(), 'MM/DD/Y').format('Y-MM-DD'),
        }, '/additional_hour/check_if_personal_sched_rest_day')).then(function (schedule) {
            var sched = jQuery.parseJSON(schedule.trim());
            console.log('schedbool ' + sched.bool)
            if (parseInt(sched.bool)) {
                $('#shiftWordUnrendered').data('schedid', sched.sched_id);
                checkIfAlreadyFiledNoSched($('#ahrTypeUnrendered').val(), moment($('#schedDate').val(), 'MM/DD/YYYY').format('Y-MM-DD'), function (checkIfFiledBool) {
                    if (parseInt(checkIfFiledBool.exist)) {
                        $('#errNotifyUnrenderedAhrMssg').html('<b>Request Exist: </b>You have already filed a request for this schedule.');
                        $('#errNotifyUnrenderedAhr').show();
                        $('#startDate, #startTime, #endDate, #endTime, #reasonManual, #requestManualAhrBtn').prop('disabled', true);                
                    } else {
                        $('#errNotifyUnrenderedAhr').hide();
                        $('#shiftField').hide();
                        $('#startDate, #startTime, #endDate, #endTime, #reasonManual, #requestManualAhrBtn').prop('disabled', false);
                        updateFormLayout();                
                    }
                })
                $('#shiftWordUnrendered').prop('disabled', false);
            } else {
                $('#errNotifyUnrenderedAhrMssg').html('Sorry you cannot file Word. This date is either not assigned with a schedule or not set as Rest day.');
                $('#shiftWordUnrendered, #startDate, #startTime, #endDate, #endTime, #reasonManual, #requestManualAhrBtn').prop('disabled', true);
                $('#errNotifyUnrenderedAhr').show();
            }
        });
    }
}

function computeManualAhrTime(callback) {
    var startDateVal = $('#startDate').val();
    var endDateVal = $('#endDate').val();
    if (moment($('#startTime').val(), 'h:mm A').format('HH:mm') == '00:00'){
        startDateVal = moment(moment(startDateVal, 'MM/DD/YYYY').add(1, 'days').valueOf()).format('MM/DD/YYYY');
    }
    if (moment($('#endTime').val(), 'h:mm A').format('HH:mm') == '00:00') {
        endDateVal = moment(moment(endDateVal, 'MM/DD/YYYY').add(1, 'days').valueOf()).format('MM/DD/YYYY');
    }
    console.log("THIS IS THE VALUE---------------------------------------------");
    console.log(startDateVal);
    console.log(endDateVal);
    var start = moment(startDateVal + " " + $('#startTime').val(), 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY hh:mm A');
    var end = moment(endDateVal + " " + $('#endTime').val(), 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY hh:mm A');
    console.log(start);
    console.log(end);
    if ($('#ahrTypeUnrendered').val() == 1) {
        console.log("shift start:" + shiftEndToCompute);
        console.log("input time end" + end);
        startStr = moment(start, 'MM/DD/YYYY hh:mm A').valueOf();
        endStr = moment(end, 'MM/DD/YYYY hh:mm A').valueOf();
    } else if ($('#ahrTypeUnrendered').val() == 2) {
        startStr = moment(start, 'MM/DD/YYYY hh:mm A').valueOf();
        endStr = moment(end, 'MM/DD/YYYY hh:mm A').valueOf();
    } else {
        startStr = moment(start, 'MM/DD/YYYY h:mm A').valueOf();
        endStr = moment(end, 'MM/DD/YYYY h:mm A').valueOf();
    }
    var hours = (endStr - startStr) / 3600000;


    if (hours <= 0) {
        hours = 0;
    } else {
        // hours = hours;
        // if (parseInt($('#ahrTypeUnrendered').val()) == 2) {
        //     var hours = hours - (parseFloat($('#additionalVal').data('bct')) / 60);
        // } else 
        if (parseInt($('#ahrTypeUnrendered').val()) == 3) {
            console.log('break ' + $('#additionalVal').data('break'));
            var breaks = parseFloat($('#additionalVal').data('break')) / 60;
            var hours = hours - breaks;
        } else {
            var hours = hours;
        }
    }
    humanTimeFormat(hours, function (humanTimeFormat) {
        var renderedOt = {
            otHours: hours.toFixed(2),
            formatted: humanTimeFormat
        };
        callback(renderedOt);
        // console.log(renderedOt);
    });
    // console.log("manual ot time: " + seconds)
}

function checkIfRenderedEnable() {
    $.when(fetchGetData('/additional_hour/check_if_rendered_enabled')).then(function (enableBool) {
        var enable = jQuery.parseJSON(enableBool.trim());
        if (parseInt(enable)) {
            console.log("WITH RENDERED");
            $('#unrenderedOnly').hide();
            $('#dropdownMenuButton').removeClass('d-none');
            $('#dropdownMenuButton')[0].removeAttribute('style');
        } else {
            console.log("WITHOUT RENDERED");
            $('#dropdownMenuButton').addClass('d-none');
            $('#unrenderedOnly').show();
        }
    });
}

function queryPersonalAhrRecord(from, to) {
    $('#personalAhrRecordDatatable').mDatatable('destroy');
    personalRecordAhrDatatable.init($('input[name=approvalStat]:checked').val(), $('#personalAhrRecordSearch').val(), from, to);
}

function deleteAhr(ahr_id){
    $.when(fetchPostData({
        ahrId: ahr_id,
    }, '/additional_hour/remove_request')).then(function (remove) {
        var removeObj = $.parseJSON(remove.trim());
        console.log(removeObj);
        if (parseInt(removeObj.approval_ongoing)) {
            swal("Cancel Restricted!", "The approval is ongoing, you are not allowed to Cancel!", "error")
        } else {
            if(parseInt(removeObj.delete_ahr_stat)){
                swal({
                    title: 'Request Cancelled',
                    text: "Your request is cancelled and removed!",
                    type: 'success',
                    showCloseButton: true,
                    timer: 5000
                })
                notification(removeObj.notify_approvers.notif_id);
            }else{
                swal({
                    title: 'Error in Cancelling',
                    text: "Something went wrong while cancelling your request. Please immediately inform your supervisor or the system administrator about the issue.!",
                    type: 'error',
                    showCloseButton: true,
                })
            }
            $('#personalAhrPendingDatatable').mDatatable('reload');
        }
    });
}

function removeAhr(ahr_id){
    $.when(fetchPostData({
        ahrId: ahr_id
    }, '/additional_hour/check_approval_stat')).then(function (approvalStat) {
        var approvalStatVal = $.parseJSON(approvalStat.trim());
        if(parseInt(approvalStatVal)){
            swal("Delete Restricted!", "The approval is ongoing, you are not allowed to delete!", "error")
        }else{
            swal({
                title: 'Are you sure you want to Cancel this Request?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Cancel it!',
                cancelButtonText: 'No, Do not Cancel it!',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    deleteAhr(ahr_id);
                } else if (result.dismiss === 'cancel') {
                    swal({
                        title: 'Not Cancelled',
                        text: "Your request was not cancelled!",
                        type: 'info',
                        showCloseButton: true,
                        timer: 5000
                    })
                }
            });
            $('#personalAhrPendingDatatable').mDatatable('reload');
        }
    });
}

function schedDateChange() {
    if ($('#ahrTypeUnrendered').val() == 3) {
        getShiftBreak($('#shiftWordUnrendered').val(), function (breaks) {
            var breakHr = (breaks.minutes) / 60;
            $('#additionalVal').data('break', breaks.minutes)
            humanTimeFormat(breakHr, function (formattedBreak) {
                $('#additionalVal').text(formattedBreak);
            })
            assignTimeDate($('#schedDate').val(), $('#shiftWordUnrendered').find('option:selected').text());
        })
    }
}

function requestUnrenderedAhr(){
    if ($('#ahrTypeUnrendered').val() != 3) {
        var shiftId = $('#shiftunrendered').val();
        var acc_time_id = 0;
    } else {
        var shiftId = $('#shiftWordUnrendered').data('schedid');
        var acc_time_id = $('#shiftWordUnrendered').val()
    }
    requestAhr($('#ahrTypeUnrendered').val(), shiftId, moment($('#startDate').val(), "MM/DD/YYYY").format('YYYY-MM-DD'), moment($('#startTime').val(), "hh:mm A").format('HH:mm'), moment($('#endDate').val(), "MM/DD/YYYY").format('YYYY-MM-DD'), moment($('#endTime').val(), "hh:mm A").format('HH:mm'), $('#unrenderedOt').data('otHours'), $('#reasonManual').val(), 2, acc_time_id, function (insertStat) {
        var addStat = jQuery.parseJSON(insertStat.trim());
        if (parseInt(addStat.request_stat) != 0) {
            // var notif_id[0] = parseInt(addStat.set_notif_details.notif_id);
            notification(addStat.set_notif_details.notif_id);
            swal("Successfully Filed Request!", "Your request is now directed to your Immediate Supervisor for approval.", "success");
        } else {
            swal("File Request Error !", "Something went wrong while filing your request. Please report the issue immediately to your supervisor or to the system administrator", "error");
        }
        $('#personalAhrPendingDatatable').mDatatable('reload');
        $('#unrenderedAhrFilingModal').modal('hide');
    });
}

function verifyIfUnrenderedFinal() {
    swal({
        title: 'Is this AHR request Final?',
        text: "You will not be able to edit this request. Please review first before",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, This is final!',
        cancelButtonText: 'Not Yet, I will review',
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            requestUnrenderedAhr();
            console.log("final");
        } else if (result.dismiss === 'cancel') {
            $('#unrenderedAhrForm').formValidation('disableSubmitButtons', false);
        }
    });
}

function validateUnrenderedForm() {
    $('#unrenderedAhrForm').formValidation('destroy');
    if ($('#ahrTypeUnrendered').val() != 3) {
        $('#unrenderedAhrForm')
            .find('[name="schedDate"]')
            .change(function (e) {
                schedDateChange();
                $('#unrenderedAhrForm').formValidation('revalidateField', 'schedDate');
            })
            .end()
            .formValidation({
                message: 'This value is not valid',
                live: 'enabled',
                excluded: ':disabled',
                feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-remove',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    schedDate: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! Shift is required'
                            }
                        }
                    },
                    shiftunrendered: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! Date Rendered is required'
                            }
                        }
                    },
                    startDate: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! Start Date is required'
                            }
                        }
                    },
                    startTime: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! Start Time is required'
                            }
                        }
                    },
                    endDate: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! End Date is required'
                            }
                        }
                    },
                    endTime: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! End Time is required'
                            }
                        }
                    },
                    reasonManual: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! Reason is required'
                            },
                            stringLength: {
                                max: 100,
                                message: 'Reason must not exceed beyond 100 characters'
                            }
                        }
                    },
                }
            }).on('success.form.fv', function (e, data) {
                e.preventDefault();
                if (($('#startDate').val() != '') && ($('#startTime').val() != '') && ($('#endDate').val() != '') && ($('#endTime').val() != '')) {
                    computeManualAhrTime(function (renderedOt) {
                        console.log(renderedOt);
                        console.log(renderedOt.formatted);
                        console.log(renderedOt.otHours);
                        $('#unrenderedOt').text(renderedOt.formatted);
                        $('#unrenderedOt').data('otHours', renderedOt.otHours);
                        $('#unrenderedOt').removeClass('text-danger');
                        if (renderedOt.otHours == 0.00) {
                            $('#errNotifyUnrenderedAhrMssg').html('Sorry, You don\'t have enough time to file an Additional Hour Request.');
                            $('#errNotifyUnrenderedAhr').show();
                            $('#requestManualAhrBtn').prop('disabled', true);
                        } else if (renderedOt.otHours > 12.0) {
                            $('#unrenderedOt').addClass('text-danger');
                            $('#errNotifyUnrenderedAhrMssg').html('<b>Maximum Limit Reached! </b>You have exceeded maximum 12 hours of overtime.');
                            $('#errNotifyUnrenderedAhr').show();
                            $('#reasonManual, #requestManualAhrBtn').prop('disabled', true);
                            // $('#reasonManual, #requestManualAhrBtn').prop('disabled', true);
                            // $('#unrenderedAhrForm').formValidation('disableSubmitButtons', true);
                        } else {
                            $('#errNotifyUnrenderedAhr').hide();
                            $('#reasonManual, #requestManualAhrBtn').prop('disabled', false);
                            verifyIfUnrenderedFinal();
                        }
                    });
                }
            });
    } else {
         $('#unrenderedAhrForm')
             .find('[name="schedDate"]')
             .change(function (e) {
                 schedDateChange();
                 $('#unrenderedAhrForm').formValidation('revalidateField', 'schedDate');
             })
             .end()
             .formValidation({
                 message: 'This value is not valid',
                 live: 'enabled',
                 excluded: ':disabled',
                 feedbackIcons: {
                     valid: 'fa fa-check',
                     invalid: 'fa fa-remove',
                     validating: 'fa fa-refresh'
                 },
                 fields: {
                     schedDate: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Shift is required'
                             }
                         }
                     },
                     shiftWord: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Date Rendered is required'
                             }
                         }
                     },
                     startDate: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Start Date is required'
                             }
                         }
                     },
                     startTime: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Start Time is required'
                             }
                         }
                     },
                     endDate: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! End Date is required'
                             }
                         }
                     },
                     endTime: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! End Time is required'
                             }
                         }
                     },
                     reasonManual: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Reason is required'
                             },
                             stringLength: {
                                 max: 100,
                                 message: 'Reason must not exceed beyond 100 characters'
                             }
                         }
                     },
                 }
             }).on('success.form.fv', function (e, data) {
                 e.preventDefault();
                 if (($('#startDate').val() != '') && ($('#startTime').val() != '') && ($('#endDate').val() != '') && ($('#endTime').val() != '')) {
                     computeManualAhrTime(function (renderedOt) {
                         console.log(renderedOt);
                         console.log(renderedOt.formatted);
                         console.log(renderedOt.otHours);
                         $('#unrenderedOt').text(renderedOt.formatted);
                         $('#unrenderedOt').data('otHours', renderedOt.otHours);
                         $('#unrenderedOt').removeClass('text-danger');
                         if (renderedOt.otHours == 0.00) {
                             $('#errNotifyUnrenderedAhrMssg').html('Sorry, You don\'t have enough time to file an Additional Hour Request.');
                             $('#errNotifyUnrenderedAhr').show();
                             $('#requestManualAhrBtn').prop('disabled', true);
                         } else if (renderedOt.otHours > 12.0) {
                             $('#unrenderedOt').addClass('text-danger');
                             $('#errNotifyUnrenderedAhrMssg').html('<b>Maximum Limit Reached! </b>You have exceeded maximum 12 hours of overtime.');
                             $('#errNotifyUnrenderedAhr').show();
                             $('#reasonManual, #requestManualAhrBtn').prop('disabled', true);
                         } else {
                             $('#errNotifyUnrenderedAhr').hide();
                             $('#reasonManual, #requestManualAhrBtn').prop('disabled', false);
                             verifyIfUnrenderedFinal();
                         }
                     });
                 }
             });
    }
}

$('#unrenderedAhrFilingModal').on('hidden.bs.modal', function () {
    formReset('#unrenderedAhrForm');
    resetManualForm();
    updateFormLayout()
})

$('#ahrTypeUnrendered').on('change', function () {
    updateFormLayout();
    validateUnrenderedForm();
    if ($('#schedDate').val() != '') {
        checkAhrTypeForShiftChecking();
    }
})

$('#schedDate').on('change', function () {
    checkAhrTypeForShiftChecking();
})

$('#shiftunrendered').on('change', function () {
    var ahrTypeVal = $('#ahrTypeUnrendered').val();
    var schedIdVal = $('#shiftunrendered').val()
    checkIfAlreadyFiled(ahrTypeVal, schedIdVal, function (checkIfFiledBool) {
        if (parseInt(checkIfFiledBool.exist)) {
            $('#errNotifyUnrenderedAhrMssg').html('<b>Request Exist: </b>You have already filed a request for this schedule.');
            $('#errNotifyUnrenderedAhr').show();
            $('#requestManualAhrBtn').prop('disabled', true);
        } else {
            $('#errNotifyUnrenderedAhr').hide();
            $('#requestManualAhrBtn').prop('disabled', false);
            getBctAndBreak(function () {});
        }
    })

})

// $('#reasonManual').bind('input propertychange', function () {
//     validateUnrenderedAhrForm(function (fieldError) {
//         if (fieldError) {
//             $('#requestManualAhrBtn').prop('disabled', true);
//         } else {
//             $('#requestManualAhrBtn').prop('disabled', false);
//         }
//     })
// })


$('#requestManualAhrBtn').on('click', function () {
    validateUnrenderedForm();
})

$('#startDate, #startTime, #endDate, #endTime').on('change', function () {
    console.log($('#startDate').val());
    console.log($('#startTime').val());
    console.log($('#endDate').val());
    console.log($('#endTime').val());
    if (($('#startDate').val() != '') && ($('#startTime').val() != '') && ($('#endDate').val() != '') && ($('#endTime').val() != '')) {
        if ($('#ahrTypeUnrendered').val() != 3) {
            if (($('#shiftunrendered').val() !== 0) && ($('#shiftunrendered').val() !== '')) {
                computeManualAhrTime(function (renderedOt) {
                    console.log(renderedOt);
                    console.log(renderedOt.formatted);
                    console.log(renderedOt.otHours);
                    $('#unrenderedOt').text(renderedOt.formatted);
                    $('#unrenderedOt').data('otHours', renderedOt.otHours);
                    $('#unrenderedOt').removeClass('text-danger');
                    if (renderedOt.otHours == 0.00) {
                        $('#errNotifyUnrenderedAhrMssg').html('Sorry, You don\'t have enough time to file an Additional Hour Request.');
                        $('#errNotifyUnrenderedAhr').show();
                        $('#requestManualAhrBtn').prop('disabled', true);
                    } else if (renderedOt.otHours > 12.0) {
                        $('#unrenderedOt').addClass('text-danger');
                        $('#errNotifyUnrenderedAhrMssg').html('<b>Maximum Limit Reached! </b>You have exceeded maximum 12 hours of overtime.');
                        $('#errNotifyUnrenderedAhr').show();
                        $('#reasonManual, #requestManualAhrBtn').prop('disabled', true);

                        // $('#reasonManual, #requestManualAhrBtn').prop('disabled', true);
                        // $('#unrenderedAhrForm').formValidation('disableSubmitButtons', true);
                    } else {
                        $('#errNotifyUnrenderedAhr').hide();
                        $('#reasonManual, #requestManualAhrBtn').prop('disabled', false);
                    }
                });
            }
        } else {
            computeManualAhrTime(function (renderedOt) {
                console.log(renderedOt);
                $('#unrenderedOt').text(renderedOt.formatted);
                $('#unrenderedOt').data('otHours', renderedOt.otHours);
                $('#unrenderedOt').removeClass('text-danger');
                if (renderedOt.otHours == 0.00) {
                    $('#errNotifyUnrenderedAhrMssg').html('Sorry, You don\'t have enough time to file an Additional Hour Request.');
                    $('#errNotifyUnrenderedAhr').show();
                    $('#requestManualAhrBtn').prop('disabled', true);
                } else if (renderedOt.otHours > 12.0){
                    $('#unrenderedOt').addClass('text-danger');
                    $('#errNotifyUnrenderedAhrMssg').html('<b>Maximum Limit Reached! </b>You have exceeded maximum 12 hours of overtime.');
                    $('#errNotifyUnrenderedAhr').show();
                    $('#reasonManual, #requestManualAhrBtn').prop('disabled', true);
                } else {
                    $('#errNotifyUnrenderedAhr').hide();
                    $('#reasonManual, #requestManualAhrBtn').prop('disabled', false);
                }
            });
        }
    }
})

// $('#schedDate').on('change', function () {
    
// })
$('#personalAhrRecordSearch').on('change', function(){
    var dateRange = $('#dateRangePersonalRecord .form-control').val().split('-');
    queryPersonalAhrRecord(moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'));
})

$('input[name=approvalStat]').on('change', function () {
    var dateRange = $('#dateRangePersonalRecord .form-control').val().split('-');
    queryPersonalAhrRecord(moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'));
})

//  $('input[name=example-radio]:checked').val();

$('#shiftWordUnrendered').on('change', function () {
    if ($('#schedDate').val() != '') {
        getShiftBreak($(this).val(), function (breaks) {
            var breakHr = (breaks.minutes) / 60;
            $('#additionalVal').data('break', breaks.minutes)
            humanTimeFormat(breakHr, function (formattedBreak) {
                $('#additionalVal').text(formattedBreak);
            })
            assignTimeDate($('#schedDate').val(), $('#shiftWordUnrendered').find('option:selected').text());
        })
    }
});


$('#requestStatus, #personalPendingAhrSearch').on('change', function () {
    // alert($('#personalPendingAhrSearch').val());
    $('#personalAhrPendingDatatable').mDatatable('destroy');
    personalPendingAhrDatatable.init($('#requestStatus').val(), $('#personalPendingAhrSearch').val());
})


$('#endDate').on('change', function(){
    if($(this).val() == ''){
        // alert('empty');
        $('#unrenderedAhrForm').formValidation('updateStatus', 'endDate', 'INVALID');
    }else{
        $('#unrenderedAhrForm').formValidation('updateStatus', 'endDate', 'VALID');
    }
})

$('#startDate').on('change', function () {
    if ($(this).val() == '') {
        // alert('empty');
        $('#unrenderedAhrForm').formValidation('updateStatus', 'startDate', 'INVALID');
    } else {
        $('#unrenderedAhrForm').formValidation('updateStatus', 'startDate', 'VALID');
    }
})

$('#checkPersonalRequest').click(function () {
    $('.nav-tabs').find('a[href="#pendingPersonalAhrRequest"]').trigger('click');
});
var myFunction = function () {
    // alert('Click function');
};

$('#testBtn').on('click', function(){


})

$(function () {
    var start = '';
    var end = '';
    validateUnrenderedForm();
    getCurrentDateTime(function (date) {
        $('#dateRangePersonalRecord .form-control').val(moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY') + ' - ' + moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY'));
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        personalRecordAhrDatatable.init($('input[name=approvalStat]:checked').val(), '', moment(date, 'Y-MM-DD').startOf('month').format('Y-MM-DD'), moment(date, 'Y-MM-DD').endOf('month').format('Y-MM-DD'))
        $('#dateRangePersonalRecord').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#dateRangePersonalRecord .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            queryPersonalAhrRecord(moment(start.format('MM/DD/YYYY'), 'MM/DD/YYYY').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/YYYY').format('Y-MM-DD'));
        });
    });
    initDisabled();
//    checkIfRenderedEnable();
    personalPendingAhrDatatable.init(0, '');

})
