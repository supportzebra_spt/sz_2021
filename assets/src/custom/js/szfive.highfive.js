var imagePath = baseUrl + '/';
var offsetCounter = 0;
var totalHighfives = 0;
var offset = 0;
var offsetCommentCounter = 0;
var offsetComment = 0;

var Select2 = function () {
    var highFives = function () {
        $('#select_highfive_type, #select_highfive_subject, #select_highfive_subject_validate').select2({
            placeholder: "Select an option",
        });
        $('#select_post_type').selectpicker();
    }

    return {
        init: function () {
            highFives();
        }
    };
}();

// High Five Modal
$(function () {

    var selected = $('#select_highfive_subject').find("option:selected").val();
    if (selected == 'company_wide') {
        $('#select_highfive_type').attr('disabled', '');
    }

    $(document)
        .on('focus', 'textarea', function () {
            var savedValue = this.value;
            this.value = '';
            this.baseScrollHeight = this.scrollHeight;
            this.value = savedValue;
        })
        .on('input', 'textarea', function () {
            var minRows = this.getAttribute('data-min-rows') | 0,
                rows;
            this.rows = minRows;
            rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 16);
            this.rows = minRows + rows;
        });

    $('textarea.highfive_mention').mentionsInput({
        onDataRequest: function (mode, query, callback) {
            $.getJSON(baseUrl + '/szfive/user_mention', function (responseData) {
                responseData = _.filter(responseData, function (item) {
                    return item.name.toLowerCase().indexOf(query.toLowerCase()) >
                        -1
                });
                callback.call(this, responseData);
            });
        },
        templates: {
            mentionItemSyntax: _.template('<%= name %>'),
        }
    });

    $('#submitMentions').tooltip('disable');

    $('textarea.highfive_mention').bind('input', function () {
        if ($('textarea.mention').val() != '') {
            $('#submitMentions').tooltip('disable');
            $('#submitMentions').tooltip('hide');
        }
    });

    $('textarea.highfive_mention').on('focus', function () {
        $('#highfive_options').show(300);
        $('#post_type').show(300);
    });

    $('textarea.highfive_mention').on('blur', function () {
        if ($('textarea.highfive_mention').val() == '') {
            $('#highfive_options').hide(300);
            $('#post_type').hide(300);
        }
    });

    $('#select_highfive_subject').on('change', function () {
        var val = $("#select_highfive_subject :selected").val();

        $('#display_highfives').html('');
        offsetCounter = 0;
        offset = 0;
        initHighfives();

        if (val == 'company_wide') {
            $('#select_highfive_type').attr('disabled', '');
        } else {
            $('#select_highfive_type').removeAttr('disabled');
        }
    });

    $('#select_highfive_type').on('change', function () {
        offsetCounter = 0;
        offset = 0;
        $('#display_highfives').html('');
        initHighfives();
    });
});

// Functions
function commentHi5(id) {
    $('#m-scrollable-comment').css('max-height', '250px');
    $('textarea.highfive_post_comment').mentionsInput({
        onDataRequest: function (mode, query, callback) {
            $.getJSON(baseUrl + '/szfive/user_comment_mentions', function (responseData) {
                responseData = _.filter(responseData, function (item) {
                    return item.name.toLowerCase().indexOf(query.toLowerCase()) >
                        -1
                });
                callback.call(this, responseData);
            });
        },
        templates: {
            mentionItemSyntax: _.template('<%= name %>'),
        }
    });
    $.when(fetchPostData({
            id: id,
            offset: 0,
            noOfItems: 10
        }, '/szfive/get_highfive_comments'))
        .then(function (response) {
            var res = jQuery.parseJSON(response);
            var data = res.entry;
            var pic = data.pic ? data.pic : "images/img/sz.png";
            var comments = (data.totalComment.count != 0) ? '<a class="like-info pull-right like-info' + data.hiFiveId + '">' + data.totalComment.count + ' comments</a>' : '';
            var likes_info = (data.likes_info != '') ? '<span style="margin-left: 10px;">' + data.likes_info + '</span>' : '';
            var dateCreated = moment(data.dateCreated).format('MMM D, YYYY hh:mm:ss A');

            header = '<div class="m-widget3__header" style="display: table;">';
            header += '<div class="m-widget3__user-img" style="margin-bottom: .7rem;">';
            header += ' <img class="m-widget3__img user-pic-dt' + data.emp_id + '" src="' + imagePath + pic + '" onerror="noImage(' + data.emp_id + ')"alt="" style="width: 3.2rem;border-radius: 50%;">';
            header += ' </div>';
            header += ' <div class="m-widget3__info" style="display:table-cell;width:100%; padding-left:1rem;font-size:1rem;vertical-align:middle;">';
            header += ' <span class="m-widget3__username" style="font-size: 15px;color: #36817a;">' + data.fullname + ' gave ' + data.header_msg + ' a High Five!</span>';
            header += '<br><span class="m-widget3__time" style="font-size: .85rem;">' + dateCreated + '</span>';
            header += '</div> </div>';
            str = '<div class="m-widget3__item" id="hi5-comment-modal" data-id="' + data.hiFiveId + '">';
            str += '<div class="m-widget3__body">';
            str += '<p class="m-widget3__text">' + data.message + '</p>';
            str += '<div>';
            str += '<span id="all-reaction-comment' + data.hiFiveId + '">' + data.reactions + '</span><a data-html="true"  data-placement="top" data-toggle="tooltip" href="#" data-original-title="'+data.tooltip+'" class="like-info" id="like-info-comment' + data.hiFiveId + '">' + likes_info + '</a>' + comments + '</div>';
            str += '<div class="row" style="text-align: center;margin-left: 0px;margin-top: 12px;font-size: 12px;">';
            if (data.my_reaction == '') {

                str += '<a class="reaction" id="like-comment' + data.hiFiveId + '" rel="like" style="margin-right: 10px;">';
                str += '<i class="likeIconDefault" ></i> Like';
            } else {
                str += '<a class="unLike-comment" id="like-comment' + data.hiFiveId + '" rel="unlike" style="margin-right: 10px;">';
            }
            str += data.my_reaction;
            str += '</a>';
            str += '<a href="#" onClick="commentHi5(' + data.hiFiveId + ')"><i class="fa fa-comment" style="color: #afb4bd;font-size: 1rem;"></i> Comment</a></div></div>';
            str += '</div>';
            str += '</div></div></div>';

            $('#comment-modal-header').html(header);
            $('#comment-parent').html(str);
            $("input[name='hiFiveId']").val(data.hiFiveId);

            options = '';
            $.each(res.sent_to_users, function (key, val) {
                options += '<option value="' + key + '">' + val + '</option>';
            });
            $('#select_user').selectpicker('destroy');
            $('#select_user').html(options);
            $('#select_user').selectpicker();

            var cdata = {
                sessionId: data.session_id,
                postId: data.hiFiveId,
                total: data.totalComment.count
            };

            $('.like-info').tooltip();
            loadComments(res.child_comments, cdata);
        });
    setTimeout(function () {
        $('#comment_hi5_modal').modal('show');
    }, 1000);
}

function loadComments(comments, cdata) {
    str = '';

    $.each(comments, function (key, val) {

        pic = val.pic ? val.pic : "images/img/sz.png";
        str += '<div class="m-widget3__header comment-cwrapper comment-cwrapper' + val.commentId + '">';
        str += '<div class="m-widget3__user-img">';
        str += '<img class="m-widget3__img comment-pic comment-pic-dt' + val.emp_id + '" src="' + imagePath + pic + '" onerror="noCommentImage(' + val.emp_id + ')" alt="">';
        str += '</div>';
        str += '<div class="m-widget3__info comment-info">';
        str += '<span class="m-widget3__username comment-name">' + val.fname + ' ' + val.lname + '</span><span style="font-size: 11px;font-style: italic;margin-left: 5px;">' + val.dateCreated + '</span>';
        str += '<p class="m-widget3__time comment-content" id="comment-content-' + val.commentId + '-">' + val.content + '</p>';
        str += '</div>';
        if (cdata.sessionId == val.uid) {
            str += '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="click" aria-expanded="true">';
            str += '<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn m-btn m-btn--link action-btn">';
            str += ' <i class="la la-ellipsis-h"></i>';
            str += ' </a>';
            str += ' <div class="m-dropdown__wrapper">';
            str += '<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 29.7032px;"></span>';
            str += '<div class="m-dropdown__inner">';
            str += ' <div class="m-dropdown__body">';
            str += '<div class="m-dropdown__content">';
            str += ' <ul class="m-nav">';
            str += '<li class="m-nav__item">';
            str += '<a href="#" onClick="showEditComment(' + val.commentId + ')" class="m-nav__link">';
            str += ' <i class="m-nav__link-icon la la-edit"></i>';
            str += ' <span class="m-nav__link-text">Edit</span>';
            str += '</a>';
            str += '</li>';
            str += ' <li class="m-nav__item">';
            str += ' <a href="#" class="m-nav__link" onClick="deleteHi5Comment(' + val.commentId + ',' + cdata.postId + ')">';
            str += '<i class="m-nav__link-icon la la-trash-o"></i>';
            str += '<span class="m-nav__link-text">Delete</span>';
            str += '</a>';
            str += '</li>';
            str += ' </ul>';
            str += ' </div>';
            str += '</div>';
            str += '</div>';
            str += ' </div>';
            str += '</div>';
        }
        str += '</div>';
    });

    $('#comment-child').append(str);

    offsetCommentCounter = offsetCommentCounter + 1;
    offsetComment = offsetCommentCounter * 10

    if (cdata.total > offsetComment) {
        $('.view-more').show();
    } else {
        $('.view-more').hide();
    }
}

function noImage(id) {
    $('.user-pic2' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    $('.user-pic-dt' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function noImageR(id) {
    $('.receipient-pic-dt-' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function noHi5Img(id) {
    $('.hi5img' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function noSenderImg(id) {
    $('.sender' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function noCommentImage(id) {
    $('.comment-pic-dt' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function showEditComment(id) {
    $('textarea.highfive_edit_comment').mentionsInput({
        onDataRequest: function (mode, query, callback) {
            $.getJSON(baseUrl + '/szfive/user_comment_mentions', function (responseData) {
                responseData = _.filter(responseData, function (item) {
                    return item.name.toLowerCase().indexOf(query.toLowerCase()) >
                        -1
                });
                callback.call(this, responseData);
            });
        },
        templates: {
            mentionItemSyntax: _.template('<%= name %>'),
        }
    });
    $.when(fetchPostData({
            id: id
        }, '/szfive/get_hi5_comment_details'))
        .then(function (response) {

            var data = jQuery.parseJSON(response);

            $('textarea#hi5commentDetail').val(data.content);
            $('input[name="commentId"]').val(data.id);
            $('input[name="isPublic"]').val(data.isPublic);
        });

    setTimeout(function () {
        $('#edit_comment_hi5_modal').modal('show');
    }, 1000);
}

function deleteHi5Comment(id, postId) {
    swal({
        title: '',
        text: 'Are you sure you want to permanently remove this comment?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No, cancel'
    }).then(function (result) {
        if (result.value === true) {

            $.ajax({
                url: baseUrl + "/szfive/delete_hi5_comment",
                type: 'POST',
                dataType: "json",
                data: {
                    id: id
                },
                success: function (data) {
                    showSpinner();
                    setTimeout(function () {
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "2000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };

                        toastr.success("Comment deleted.");
                        $('.comment-cwrapper' + id).remove();
                        var C = parseInt($(".like-info" + postId).html());
                        $(".like-info" + postId).html(C - 1 + ' comments');

                    }, 1000);
                },
                error: function () {
                    swal({
                        "title": "",
                        "text": "Error",
                        "type": "error",
                        "timer": 2000,
                        "showConfirmButton": false
                    });
                }
            });
        }
    });
}

function editHiFivepost(id) {

    $.when(fetchPostData({
            id: id
        }, '/szfive/get_highfive_post'))
        .then(function (data) {
            setTimeout(function () {
                $('#showEditHiFivePost').modal('show');
            }, 1000);
        });
}

function deleteHiFivepost(id) {
    swal({
        title: '',
        text: 'Are you sure you want to delete this High Five?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No, cancel'
    }).then(function (result) {
        if (result.value === true) {

            $.ajax({
                url: baseUrl + "/szfive/delete_hi5_post",
                type: 'POST',
                dataType: "json",
                data: {
                    id: id
                },
                success: function (data) {

                    mApp.blockPage({
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'success',
                        message: 'Please wait...'
                    });

                    setTimeout(function () {
                        mApp.unblockPage();
                        $('#highfive-item' + id).remove();
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "2000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };

                        toastr.success("Successfully removed High Five.");
                    }, 1000);
                },
                error: function () {
                    swal({
                        "title": "",
                        "text": "Error",
                        "type": "error",
                        "timer": 2000,
                        "showConfirmButton": false
                    });
                }
            });
        }
    })
}

function initHighfives() {
    var data = {
        offset: offset,
        noOfItems: 10,
        selectedPerson: $('#select_highfive_subject').find("option:selected").val(),
        selectedType: $('#select_highfive_type').find("option:selected").val()
    };

    mApp.blockPage({
        overlayColor: '#000000',
        state: 'primary'
    });

    loadHighfives(data, function() {
        mApp.unblockPage();
    });
}

function loadHighfives(data, callback) {

    $.when(fetchPostData({
            params: data
        }, '/szfive/get_all_highfive_posts'))
        .then(function (response) {
            var resp = jQuery.parseJSON(response);
            var data = resp.data;
            var str = '';
            totalHighfives = resp.count;

            if (data.length != 0) {
                $.each(data, function (key, value) {
                    console.log(value);
                    var pic = value.pic ? value.pic : "images/img/sz.png";
                    var comments = (value.totalComment.count != 0) ? '<a style="cursor: pointer;" onClick="commentHi5(' + value.hiFiveId + ')" class="like-info pull-right like-info' + value.hiFiveId + '">' +
                        value.totalComment.count + ' comments</a>' : '';
                    var likes_info = (value.likes_info != '') ? '<span style="margin-left: 10px;">' + value.likes_info + '</span>' : '';
                    str += '<li style="margin-bottom: 25px;" id="highfive-item' + value.hiFiveId + '">';
                    str += '<div class="row">';
                    str += '<div class="col-lg-2">';
                    str += '<div style="margin-bottom: .7rem;white-space: nowrap!important;text-align: center;"><img class="m-widget3__img user-pic-dt' + value.emp_id + '"   src="' +
                        imagePath + pic + '" onerror="noImage(' + value.emp_id +
                        ')" alt="" style="width: 3.5rem;border-radius: 50%;display: inline-block;vertical-align: middle;border: 3px solid #fff;height: 3.5rem;">';
                    if (value.moreThanOne == true) {
                        str += ' <span class="symbol" style="background-color: #716aca;">';
                        str += ' <span style="margin-top: 6px;display: block;color: #fff;font-size: 22px;font-weight: 500;">' + value.noOfRecepients + '</span></span>';
                    } else {
                        str += '<img class="m-widget3__img receipient-pic-dt-' + value.hi5recepientId + '" src="' +
                            value.hi5recepient + '" onerror="noImageR(' + value.hi5recepientId + ')" alt="" style="width: 3.7rem;height: 3.7rem;border-radius: 50%;display: inline-block;vertical-align: middle;border: 3px solid #ffffff;margin-left: -12px;">';
                    }
                    str += '</div></div>';
                    str += '<div class="col-lg-10">';
                    str += '<a href="' + baseUrl + '/' + value.link + '" class="m-widget3__username" style="font-size: 13px;font-weight: 500;color: #525252;">' + value.user_name + '<span style="font-weight: 400;margin: 0 2px;"> to </span> <span>' + value.header_msg + '</span></a>';
                    str += '<br><span class="m-widget3__time" style="font-size: .85rem;">' + value.dateAnswered + ', ' + value.dateSubmitted;
                    if (value.isPublic == 1) {
                        str += '&nbsp;<i class="fa fa-globe" style="font-size: 12px;color: #767e84d6;"></i></span>';
                    } else {
                        str += '&nbsp;<i class="fa fa-lock" style="font-size: 12px;color: #767e84d6;"></i></span>';
                    }
                    str += '<div class="m-widget3__body">';
                    str += '<p class="m-widget3__text" style="font-size: 15px; margin-bottom: 0;word-wrap: break-word;">' + value.message + '</p>';
                    str += '</div></div>';
                    str += '</div>';
                    str += '<div class="row">';
                    str += '<div class="col-lg-2"></div>';
                    str += '<div class="col-lg-10">';
                    str += '<div style="margin-top: 15px;">';
                    str += '<span id="all-reaction' + value.hiFiveId + '">' + value.reactions + '</span><a data-html="true"  data-placement="top" data-toggle="tooltip" href="#" data-original-title="'+value.tooltip+'" class="like-info" id="like-info-comment' + value.hiFiveId + '">' + likes_info + '</a>' + comments + '</div>';
                    str += '<div class="row" style="text-align: center;margin-left: 0px;margin-top: 5px;font-size: 12px;">';
                    if (value.my_reaction == '') {
                        str += '<a class="reaction" id="like' + value.hiFiveId + '" rel="like" style="margin-right: 10px;">';
                        str += '<i class="likeIconDefault" ></i> Like';
                    } else {
                        $('#people_reacted').show();
                        str += '<a class="unLike" id="like' + value.hiFiveId + '" rel="unlike" style="margin-right: 10px;">';
                    }
                    str += value.my_reaction;
                    str += '</a>';
                    str += '<a style="cursor: pointer;" onClick="commentHi5(' + value.hiFiveId + ')"><i class="fa fa-comment" style="color: #afb4bd;font-size: 1rem;"></i> Comment</a>';
                    if (value.senderId == value.sessionId) {
                        str += '<a style="margin-left: 10px;cursor: pointer;" onClick="deleteHiFivepost(' + value.hiFiveId + ')"><i class="fa fa-trash-o" style="color: #afb4bd;font-size: 1rem;"></i> Delete</a> ';
                    }
                    str += '</div></div></div>';
                    str += '</li>';
                });

                $('#display_highfives').append(str);

            } else {
                $('#display_highfives').html('<div style="text-align: center;">No High Fives to display.</div>');
            }

            $('.like-info').tooltip();
            $('#loadingMore').hide();

            $(window).on('scroll', function () {
                var scrollHeight = $(document).height();
                var scrollPosition = $(window).height() + $(window).scrollTop();

                if ((scrollHeight - scrollPosition) / scrollHeight === 0) {

                    offsetCounter = offsetCounter + 1;
                    offset = offsetCounter * 10

                    if (totalHighfives > offset) {
                        var data = {
                            offset: offset,
                            noOfItems: 10,
                            selectedPerson: $('#select_highfive_subject').find("option:selected").val(),
                            selectedType: $('#select_highfive_type').find("option:selected").val()
                        };

                        $('#loadingMore').show();
                        loadHighfives(data, function() {
                            $('#loadingMore').hide();
                        });
                    }
                }
            });
            callback();
        });
}

function showSpinner() {
    mApp.block('#comment_hi5_modal .modal-content', {});

    setTimeout(function () {
        mApp.unblock('#comment_hi5_modal .modal-content');
    }, 1000);
}

function addReaction(params) {
    var htmlData = '<i class="' + params.reactionName.toLowerCase() + 'IconSmall likeTypeSmall defaultIcon"></i>' + params.reactionName;

    $.when(fetchPostData({
                highfiveId: params.highfiveId,
                reactionType: params.reactionType
            },
            '/szfive/add_high_five_reaction'))
        .then(function (response) {

            var data = jQuery.parseJSON(response);

            if (data.success == true) {
                var val = data.details;
                $('#like-info' + params.highfiveId).html('<span style="margin-left: 10px;">' + val.likes_info);
                $('#like-info-comment' + params.highfiveId).html('<span style="margin-left: 10px;">' + val.likes_info);
                $('#all-reaction' + params.highfiveId).html(val.reactions);
                $('#all-reaction-comment' + params.highfiveId).html(val.reactions);
                $("#like" + params.highfiveId).html(htmlData).removeClass('reaction').removeClass('tooltipstered').addClass('unLike').attr('rel', 'unlike');
                $("#like-comment" + params.highfiveId).html(htmlData).removeClass('reaction').removeClass('tooltipstered').addClass('unLike-comment').attr('rel', 'unlike');
                $("#" + params.x).hide();

                if (data.system_notif_ids.length != 0) {
                    systemNotification(data.system_notif_ids,'snapsz');
                }
            }
        });
}

function removeReaction(params) {

    var htmlData = '<i class="likeIconDefault" ></i>Like';

    $.when(fetchPostData({
                highfiveId: params.highfiveId,
                reactionType: params.reactionType
            },
            '/szfive/delete_high_five_reaction'))
        .then(function (response) {
            var resp = jQuery.parseJSON(response);

            if (resp.success == 1) {

                if (resp.data.length == 0) {
                    $('#like-info' + params.highfiveId).html('');
                    $('#like-info-comment' + params.highfiveId).html('');
                    $('#all-reaction' + params.highfiveId).html("");
                    $('#all-reaction-comment' + params.highfiveId).html("");

                } else {
                    var val = resp.data;
                    $('#all-reaction' + params.highfiveId).html('<span style="margin-left: 10px;">' + val.reactions);
                    $('#all-reaction-comment' + params.highfiveId).html('<span style="margin-left: 10px;">' + val.reactions);
                    $('#like-info' + params.highfiveId).html(val.likes_info);
                    $('#like-info-comment' + params.highfiveId).html(val.likes_info);
                }
                $("#like" + params.highfiveId).html(htmlData).addClass('reaction').addClass('tooltipstered').removeClass('unLike');
                $("#like-comment" + params.highfiveId).html(htmlData).addClass('reaction').addClass('tooltipstered').removeClass('unLike-comment');
            }
        });
}
/// Form Validations
var Hi5CommentValidation = function () {

    var editComment = function () {

        $("#highfive_edit_comment_form").validate({
            rules: {
                commentEdit: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "Comment must not be empty.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {

                var content = $('#highfive_edit_comment_form').find('input[name="commentEdit"]').val();
                var commentId = $('input[name="commentId"]').val();
                var usermention = 0;
                var hiFiveId = $('#hi5-comment-modal').data('id');
                var isPublic = $('input[name="isPublic"]').val();

                $('textarea.highfive_edit_comment').mentionsInput('val', function (userMention) {
                    if (userMention != '') {
                        content = userMention;
                    }
                });

                $('textarea.highfive_edit_comment').mentionsInput('getMentions', function (data) {
                    if (data.length != 0) {
                        usermention = data;
                    }
                });

                var options = {
                    url: baseUrl + "/szfive/update_hi5_comment",
                    type: 'POST',
                    dataType: "json",
                    data: {
                        hiFiveId: hiFiveId,
                        commentId: commentId,
                        content: content,
                        usermention: usermention,
                        privateToUser: 0,
                        isPublic: isPublic,
                    },
                    success: function (response) {
                        if (response.status) {
                            if (response.system_notif_ids.length != 0) {
                                systemNotification(response.system_notif_ids,'snapsz');
                            }

                            mApp.block('#edit_comment_hi5_modal .modal-content', {
                                overlayColor: '#000000',
                                type: 'loader',
                                state: 'primary',
                                message: 'Processing...'
                            });

                            setTimeout(function () {
                                mApp.unblock(
                                    '#edit_comment_hi5_modal .modal-content'
                                );

                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-center",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "2000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                };

                                $('#comment-content-' + commentId + '-').html(response.comment);
                                $('#edit_comment_hi5_modal').modal('hide');

                                toastr.success("Comment updated.");

                            }, 1000);
                        } else {
                            swal({
                                "title": "Error",
                                "text": "There was ab error updating your comment!",
                                "type": "error",
                                "timer": 1500,
                                "showConfirmButton": false
                            });
                        }

                    },
                    error: function () {
                        alert('error');
                    },
                };
                $(form).ajaxSubmit(options);
            }
        });
    }

    var submitComment = function () {
        $("#highfive_comment_form").validate({
            rules: {
                comment: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "Comment must not be empty.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {

                var comment = $('#highfive_comment_form').find('input[name="comment"]').val();
                var hiFiveId = $("input[name='hiFiveId']").val();
                var privateToUser = $('#select_user').val();
                var isPublic = 1;

                if ($('#checkPrivate').is(':checked')) {
                    isPublic = 0;
                }

                var content = '';
                var usermention = 0;

                $('textarea.highfive_post_comment').mentionsInput('val', function (userMention) {
                    if (userMention != '') {
                        content = userMention;
                    }
                });

                $('textarea.highfive_post_comment').mentionsInput('getMentions', function (data) {
                    if (data.length != 0) {
                        usermention = data;
                    }
                });

                var options = {
                    url: baseUrl + "/szfive/add_highfive_comment",
                    type: 'POST',
                    dataType: "json",
                    data: {
                        hiFiveId: hiFiveId,
                        privateToUser: privateToUser,
                        isPublic: isPublic,
                        content: content,
                        usermention: usermention
                    },
                    success: function (resp) {
                        var val = resp.data;
                        if (resp.system_notif_ids.length != 0) {
                            systemNotification(resp.system_notif_ids,'snapsz');
                        }

                        $('#inputHi5Comment').val("");

                        var userId = $('#highfive-page').data('id');
                        var img = $(".header-userpic" + userId).attr('src')

                        str = '<div class="m-widget3__header comment-cwrapper comment-cwrapper' + val.commentId + '">';
                        str += '<div class="m-widget3__user-img">';
                        str += '<img class="m-widget3__img comment-pic comment-pic-dt' + val.emp_id + '" src="' + img + '" onerror="noCommentImage(' + val.emp_id + ')" alt="">';
                        str += '</div>';
                        str += '<div class="m-widget3__info comment-info">';
                        str += '<span class="m-widget3__username comment-name">' + val.fullname + '</span><span style="font-size: 11px;font-style: italic;margin-left: 5px;">' + val.dateCreated + '</span>';
                        str += '<p class="m-widget3__time comment-content" id="comment-content-' + val.commentId + '-">' + val.content + '</p>';
                        str += '</div>';
                        str += '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="click" aria-expanded="true">';
                        str += '<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn m-btn m-btn--link action-btn">';
                        str += '<i class="la la-ellipsis-h"></i>';
                        str += ' </a>';
                        str += ' <div class="m-dropdown__wrapper">';
                        str += ' <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 29.7032px;"></span>';
                        str += '<div class="m-dropdown__inner">';
                        str += '<div class="m-dropdown__body">';
                        str += ' <div class="m-dropdown__content">';
                        str += '<ul class="m-nav">';
                        str += ' <li class="m-nav__item">';
                        str += '<a href="#" onClick="showEditComment(' + val.commentId + ')" class="m-nav__link">';
                        str += '<i class="m-nav__link-icon la la-edit"></i>';
                        str += '<span class="m-nav__link-text">Edit</span>';
                        str += '</a>';
                        str += '</li>';
                        str += ' <li class="m-nav__item">';
                        str += '<a href="#" class="m-nav__link" onClick="deleteHi5Comment(' + val.commentId + ',' + val.post_id + ')">';
                        str += ' <i class="m-nav__link-icon la la-trash-o"></i>';
                        str += ' <span class="m-nav__link-text">Delete</span>';
                        str += '</a>';
                        str += '</li>';
                        str += ' </ul>';
                        str += ' </div>';
                        str += '</div>';
                        str += '</div>';
                        str += ' </div>';
                        str += '</div>';
                        str += '</div>';

                        showSpinner();
                        setTimeout(function () {
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };

                            toastr.success("Comment successfully sent!");

                            $('#comment-child').append(str);
                            $('.view-more').hide();
                            var C = parseInt($(".like-info" + hiFiveId).html());
                            $(".like-info" + hiFiveId).html(C + 1 + ' comments');


                            var scrollHeight = $('.survey-answer-wrapper').find('#mCSB_2_container')[0].scrollHeight - 250;

                            $('.survey-answer-wrapper').find('#mCSB_2_container').css('top', -scrollHeight);

                            $('.comment-cwrapper' + val.commentId).addClass('highlighted', 1000);
                            setTimeout(function () {
                                $('.comment-cwrapper' + val.commentId).removeClass('highlighted', 1000);
                            }, 500);
                            $('#checkPrivate').prop('checked', false);
                        }, 1000);
                    },
                    error: function () {
                        alert('error');
                    },
                };
                $(form).ajaxSubmit(options);
            }
        });
    }

    var submitHighFive = function () {
        $("#user_mention_form").validate({
            showErrors: function (errorMap, errorList) {
                // Do nothing here
            },
            rules: {
                user_highfive: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                $('#submitMentions').tooltip('enable');
            },
            submitHandler: function (form) {
                $('#submitMentions').tooltip('disable');

                var mention = '';
                var userdata = 0;

                $('textarea.highfive_mention').mentionsInput('val', function (userMention) {
                    if (userMention != '') {
                        mention = userMention;
                        // $(this).val("");
                    }
                });

                $('textarea.highfive_mention').mentionsInput('getMentions', function (data) {
                    if (data.length != 0) {
                        userdata = data;
                    }
                });

                var isPublic = $('#select_post_type').val();

                if (mention != '' && userdata != 0) {

                    mApp.blockPage({
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'success',
                        message: 'Please wait...'
                    });

                    setTimeout(function () {
                        mApp.unblockPage();
                    }, 1000);

                    var options = {
                        url: baseUrl + "/szfive/add_high_five",
                        type: 'POST',
                        dataType: "json",
                        data: {
                            mention: mention,
                            isPublic: isPublic,
                            userdata: userdata
                        },
                        success: function (resp) {
                            if (resp.status === 1) {
                                if (resp.sys_notif_id != 0) {
                                    systemNotification(resp.sys_notif_id,'snapsz');
                                }
                            }
                            setTimeout(function () {
                                $('#highfive_options').hide();
                                $('#post_type').hide(300);
                                $('textarea.highfive_mention').mentionsInput('reset', function () {});
                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-center",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "2000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                };

                                toastr.success("Successfully sent High Five!");
                                location.reload();

                            }, 1500);

                            setTimeout(function () {
                                location.reload();
                            }, 1800);
                        },
                        error: function () {
                            alert('error');
                        },
                    };
                    $(form).ajaxSubmit(options);
                } else {
                    $('#submitMentions').tooltip('enable');
                }
            }
        });
    }
    return {
        init: function () {
            editComment();
            submitComment();
            submitHighFive();
        }
    };
}();

jQuery(document).ready(function () {
    Select2.init();
    initHighfives();
    Hi5CommentValidation.init();

    $(".likeTypeAction").tipsy({
        gravity: 's',
        live: true
    });

    $(".reaction").livequery(function () {
        var reactionsCode = '<span class="likeTypeAction" original-title="Like" data-reaction="1"><i class="likeIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Love" data-reaction="2"><i class="loveIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Haha" data-reaction="3"><i class="hahaIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Wow" data-reaction="4"><i class="wowIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Cool" data-reaction="5"><i class="coolIcon likeType"></i></span>' +
            // '<span class="likeTypeAction" original-title="Confused" data-reaction="6"><i class="confusedIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Sad" data-reaction="7"><i class="sadIcon likeType"></i></span>' +
            '<span class="likeTypeAction last" original-title="Angry" data-reaction="8"><i class="angryIcon likeType"></i></span>';

        $(this).tooltipster({
            contentAsHTML: true,
            interactive: true,
            multiple: true,
            content: $(reactionsCode),
        });
    });

    $('#comment_hi5_modal').on('hidden.bs.modal', function () {
        $('#comment-modal-header').html('');
        $('#checkPrivate').prop('checked', false);
        $('#comment-parent').html('');
        $('#comment-child').html('');
        $('#inputHi5Comment').val('');
        $('textarea.highfive_post_comment').mentionsInput('reset', function () {});

        offsetComment = 0;
        offsetCommentCounter = 0;
    });

    $('#edit_comment_hi5_modal').on('hidden.bs.modal', function () {
        $('textarea.highfive_edit_comment').mentionsInput('reset', function () {});
    });

    /*Reaction*/
    $("body").on("click", ".likeTypeAction", function () {
        var reactionType = $(this).attr("data-reaction");
        var reactionName = $(this).attr("original-title");
        var rel = $(this).parent().parent().attr("rel");
        var x = $(this).parent().parent().attr("id");
        var sid = x.split("reaction");
        var highfiveId = sid[1].replace(/-comment/, '');

        var params = {
            highfiveId: highfiveId,
            reactionType: reactionType,
            reactionName: reactionName,
            x: x,
        };
        addReaction(params);
        return false;
    });

    $("body").on("click", ".unLike", function () {
        var reactionType = '1';
        var x = $(this).attr("id");
        var sid = x.split("like");
        var highfiveId = sid[1];

        var params = {
            highfiveId: highfiveId,
            reactionType: reactionType,
            x: x,
        };
        removeReaction(params);
        return false;
    });

    $("body").on("click", ".unLike-comment", function () {
        var reactionType = '1';
        var x = $(this).attr("id");
        var sid = x.split("like-comment");
        var highfiveId = sid[1];

        var params = {
            highfiveId: highfiveId,
            reactionType: reactionType,
            x: x,
        };
        removeReaction(params);
        return false;
    });

    $('#viewMoreComments').on("click", function () {
        var id = $("input[name='hiFiveId']").val();
        showSpinner();

        $.when(fetchPostData({
                id: id,
                offset: offsetComment,
                noOfItems: 10,
                type: 'sz_highfive'
            }, '/szfive/load_more_comments'))
            .then(function (response) {
                var data = jQuery.parseJSON(response);
                var cdata = {
                    sessionId: data.info.session_id,
                    postId: data.info.hiFiveId,
                    total: data.info.total
                };
                setTimeout(function () {
                    loadComments(data.comments, cdata);
                }, 1000);

            });
    });
});