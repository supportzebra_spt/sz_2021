function get_all_process_select_data(callback){
    $.when(fetchPostData({
        processId: $('#processId').data('processid'),
    }, '/process/post_select_init')).then(function (initSelect) {
    var initSelectObj = $.parseJSON(initSelect.trim());
        var statusOptions = "";
        let confirmationOption = "";
        let confirmationStatusOption = "";
        if(parseInt(initSelectObj.proc_details.processType_ID) == 2){
            confirmationOption = '<option class="font-weight-bold" type="temp" value="chkList_info8" selected>Confirmation Status</option>' ;
        }
        var assigneeOptions = '<optgroup id="singleAssignee" data-max-options="1">'+
                                '<option optiontype="single" value="' + initSelectObj.uid+ '">Me</option>' +
                                '<option optiontype="mult" value="">Everyone</option>'+
                            '</optgroup>';
        var columnsOptions = '<optgroup label="Process Template">'+
                                '<option class="font-weight-bold" type="temp" value="chkList_info2" selected>Date Created</option>'+
                                '<option class="font-weight-bold" type="temp" value="chkList_info3" selected>Due Date</option>'+
                                '<option class="font-weight-bold" type="temp" value="chkList_info4" selected>Checklist Status</option>'+
                                confirmationOption+
                                '<option class="font-weight-bold" type="temp" value="chkList_info5" selected>Date Completed</option>'+
                                '<option class="font-weight-bold" type="temp" value="chkList_info6" selected>Assignees</option>'+
                                '<option class="font-weight-bold" type="temp" value="chkList_info7" selected>Tasks Completed</option>'+
                            '</optgroup><optgroup label="Process Tasks">';
        if (parseInt(initSelectObj.statuses_stat)){
            $.each(initSelectObj.statuses, function(index, value){
                statusOptions +="<option class='text-capitalize' value='"+value.status_id+"'>"+value.description+"</option>";
            })
        }
        if (parseInt(initSelectObj.assignees_stat)){
            assigneeOptions += '<option data-divider="true"></option>';
            $.each(initSelectObj.assignees, function(index, value){
               assigneeOptions += "<option optiontype='single' class='text-capitalize' value='" + value.assignedTo + "'>" + value.fname +" "+value.lname+"</option>";
            })
        }
        if (parseInt(initSelectObj.confirmation_status_stat)) {
            $.each(initSelectObj.confirmation_status, function (index, value) {
                console.log(value.status_ID);
                $confirm_status_desc = "Pending";
                // $confirm_status = null;
                if(value.status_ID == 26){
                    $confirm_status_desc = "Confirmed";
                } else if (value.status_ID == 12){
                    $confirm_status_desc = "Missed";
                }
                confirmationStatusOption += "<option class='text-capitalize' value='" + value.status_ID + "'>" + $confirm_status_desc + "</option>";
            })

        }
        if (parseInt(initSelectObj.columns_stat)) {
            $.each(initSelectObj.columns, function (index, value) {
                columnsOptions += value;
            })

        }
        columnsOptions += "</optgroup>";
        $('#checklistReportStatus').html(statusOptions);
        $('#checklistReportAssignees').html(assigneeOptions);
        $('#checklistReportColumns').html(columnsOptions);
        $('#confirmationStatus').html(confirmationStatusOption);
        callback(initSelectObj);
    })
}

function get_process_checklist_table_details(limiterVal, callback) {
    let confirmationStatVal = 0;
    console.log($('#processId').data('processtype'));
    if(parseInt($('#processId').data('processtype')) == 2){
        confirmationStatVal = $('#confirmationStatus').val().toString();
    }
    $.when(fetchPostData({
        processId: $('#processId').data('processid'),
        checkListName: $('#checklistReportSearch').val(),
        statusId: $('#checklistReportStatus').val().toString(),
        assigneeIds: $('#checklistReportAssignees').val().toString(),
        confirmationStatus: confirmationStatVal,
        limiter: limiterVal
    }, '/process/process_checklist_table_details')).then(function (tableDetails) {
        var tableDetailsObj = $.parseJSON(tableDetails.trim());
        callback(tableDetailsObj);
    })
}

function get_assignee_details(element){
    $.when(fetchPostData({
        processId: $(element).data('processid'),
        checklistId: $(element).data('checklistid'),
    }, '/process/get_checklist_assignee_details')).then(function (assignee) {
        let assigneeObj = $.parseJSON(assignee.trim());
        let assigneeDetails = "";
        let picName = '';
        let pic = '';
        if (parseInt(assigneeObj.exist)){
            $.each(assigneeObj.record, function(index, value){
                picName = value.pic.substr(0, value.pic.indexOf(".jpg"))
                pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                assigneeDetails += '<div class="col-12 py-3 mb-3" style="background: #dadada;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);">'+
                    '<div class="row">'+
                        '<div class="col-sm-12 col-md-4 col-lg-3"><img src="' + pic + '" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block"></div>' +
                            '<div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">'+
                                '<div class="row">'+
                                    '<div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 17px;">' + toTitleCase(value.fname+' '+value.lname)+'</div>'+
                                    '<div class="col-md-12 text-center text-md-left" style="font-size: 11px;margin-top: -3px;font-weight: bolder;color: darkcyan;">'+value.pos_details+'</div>'+
                                    '<div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 10px">'+value.acc_name+'</div>'+
                                    '<div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 10px"> Assigned to ' + value.assignment.toUpperCase() + '</div>' +
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
            })
            $('#assigneeDetails').html(assigneeDetails);
            $('#assigneeDetailsModal').modal('show');
        }
        // console.log(assigneeObj);
        // if()
        // callback(assigneeObj);
    })
} 

function init_process_checklist_table_details() {
    $('#loadingReportSpinner').fadeIn();
    get_process_checklist_table_details(0,function (tableDetailsObj) {
        var tableHeader = "";
        var tableBody = "";
        if (tableDetailsObj.answers.length != 0) {
            let confirmationHeader = "";
            if (parseInt(tableDetailsObj.process_details.processType_ID) == 2){
                confirmationHeader = '<th id="column-header-7" class="text-truncate resizable-header text-center col-3 chkList_info8" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Confirmation Status<div id="column-header-7-sizer"></div></th>';
            }
            tableHeader += '<th id="column-header-9" class="text-truncate resizable-headers text-center first-col" style="background: #5a5a5a;">Checklist Name<div id="column-header-9-sizer"></div></th>' +
                '<th id="column-header-1" class="text-truncate resizable-header text-center col-2 chkList_info2" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Date Created<div id="column-header-1-sizer"></div></th>' +
                '<th id="column-header-2" class="text-truncate resizable-header text-center col-3 chkList_info3" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Due Date<div id="column-header-2-sizer"></div></th>' +
                '<th id="column-header-3" class="text-truncate resizable-header text-center col-3 chkList_info4" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Checklist Status<div id="column-header-3-sizer"></div></th>' +
                confirmationHeader+
                '<th id="column-header-4" class="text-truncate resizable-header text-center col-3 chkList_info5" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Completed<div id="column-header-4-sizer"></div></th>' +
                '<th id="column-header-5" class="text-truncate resizable-header text-center col-3 chkList_info6" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Assignees<div id="column-header-5-sizer"></div></th>' +
                '<th id="column-header-6" class="text-truncate resizable-header text-center col-3 chkList_info7" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Task Completed<div id="column-header-6-sizer"></div></th>';
            $.each(tableDetailsObj.task_columns, function (index, value) {
                tableHeader += value;
            })
            $.each(tableDetailsObj.answers, function (index, value) {
                tableBody += "<tr>" + value + "</tr>";
            })
            $('#processTableHeader').html(tableHeader);
            $('#processTableBody').html(tableBody);
            $('#processTable').fadeIn();
            $('#processTable').data('limit', 0);
            var checkListWord = "Checklist";
            if (tableDetailsObj.total_checklist > 1){
                checkListWord+="s";
            }
            $('#checkListCountInfo').text(tableDetailsObj.answers.length+ " out of " + tableDetailsObj.total_checklist+ " " + checkListWord);
            if ((tableDetailsObj.answers.length) == tableDetailsObj.total_checklist){
                $('#loadMoreBtn').hide();
            }else{
                $('#loadMoreBtn').show();
            }
            $('#loadMoreDiv').fadeIn();
            $('#noChecklistRecord').hide();
        } else {
            $('#processTable').hide();
            $('#noChecklistRecord').fadeIn();
        }
        $('#loadingReportSpinner').fadeOut();
        init_resizable();
        dynamicColumnVisibility();
        // callback(tableDetailsObj);
    });
}

function append_process_checklist_table_details(){
    $('#loadMoreSpinner').fadeIn();
    var limiterVal = $('#processTable').data('limit') + 10;
    get_process_checklist_table_details(limiterVal, function (tableDetailsObj) {
        var tableHeader = "";
        var tableBody = "";
 			 
        if (tableDetailsObj.answers.length != 0) {
            $.each(tableDetailsObj.answers, function (index, value) {
                tableBody += "<tr>" + value + "</tr>";
            })
            $('#processTableBody').append(tableBody);
			$('[data-toggle="tooltip"]').tooltip();
            dynamicColumnVisibility();
            var checkListWord = "Checklist";
            if (tableDetailsObj.total_checklist > 1) {
                checkListWord += "s";
            }
            // console.log(limiterVal);
            $('#checkListCountInfo').text((limiterVal+tableDetailsObj.answers.length) +" out of " + tableDetailsObj.total_checklist + " " + checkListWord);
            if ((limiterVal + tableDetailsObj.answers.length) == tableDetailsObj.total_checklist){
                $('#loadMoreBtn').hide();
            }
        } else {
            
        }
        $('#loadMoreSpinner').fadeOut();
        $('#processTable').data('limit', limiterVal);
    });
}

function dynamicColumnVisibility(){
     // Dynamically Hide and show
    var notSelected = $('#checklistReportColumns option:not(:selected)');
    var selected = $('#checklistReportColumns option:selected');
    // console.log(notSelected.val());
    // console.log(selected.val());
    if (selected.length > 0) {
        var selectedCol = [];
        $.each(selected, function (index, value) {
            selectedCol[index] = "." + $(value).val();
        })
        $("" + selectedCol.join(", ")).fadeIn();
        // console.log("" + selectedCol.join(", "));
    }
    if (notSelected.length > 0) {
        var notSelectCol = [];
        $.each(notSelected, function (index, value) {
            notSelectCol[index] = "." + $(value).val();
        })
        $("" + notSelectCol.join(", ")).hide();
        // console.log(notSelectCol.join(", "));
    }
}



function exportExcelReport(exportType){
    var notSelected = $('#checklistReportColumns option:not(:selected)');
    var notSelectCol = [];
    var notSelectedData = "";
    let confirmationStatVal = 0;
    console.log($('#processId').data('processtype'));
    if (parseInt($('#processId').data('processtype')) == 2) {
        confirmationStatVal = $('#confirmationStatus').val().toString();
    }
    if (notSelected.length > 0) {
        var notSelectCol = [];
        $.each(notSelected, function (index, value) {
            notSelectCol[index] = "."+$(value).val();
        });
        notSelectedData = notSelectCol.toString();
    }
    $.ajax({
        url: baseUrl + "/process/process_checklist_excel_report",
        method: 'POST',
        data: {
            processId: $('#processId').data('processid'),
            checkListName: $('#checklistReportSearch').val(),
            status: $('#checklistReportStatus').val().toString(),
            assignees: $('#checklistReportAssignees').val().toString(),
            confirmationStatus: confirmationStatVal,
            export_type: exportType,
            notSelectedDataVal: notSelectedData
        },
        beforeSend: function () {
            //  $('#loadingExportModal').modal('show');
          },
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
             // $('#loadingExportModal').modal("hide");
           
 			 
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'Process_Manage_report.xlsx';
            a.click();
    
            window.URL.revokeObjectURL(url);
            // mApp.unblock('#card-container');
			
        },
    });
}
// PLUGIN INIT
var BootstrapSelect = function () {
    var options = function (elementId, titleVal, liveSearchBool, liveSearchStyleVal, selectedTextFormatVal, cuntSelectedTextVal) {
        $(elementId).selectpicker({
            title: titleVal,
            liveSearch: liveSearchBool,
            liveSearchStyle: liveSearchStyleVal,
            selectedTextFormat: selectedTextFormatVal,
            countSelectedText: cuntSelectedTextVal,
            // container: ".m-content",
            // dropupAuto: 'true',
            // virtualScroll: 'true',
            size: 'auto'
        });
    }
    return {
        init: function (elementId, titleVal, liveSearchBool, liveSearchStyleVal, selectedTextFormatVal, cuntSelectedTextVal) {
            options(elementId, titleVal, liveSearchBool, liveSearchStyleVal, selectedTextFormatVal, cuntSelectedTextVal);
        }
    };
}();

$('#checklistReportAssignees').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    e.preventDefault();
    var optiontype = $(this).find('option').eq(clickedIndex).attr('optiontype');
    if (optiontype == 'single'){
        $(this).find('option[optiontype="mult"]').prop('selected', false);
        // $(this).find('option[optiontype="all"]').prop('selected', false);
    }
    // else if(optiontype == 'all'){
    //     $(this).find('option[optiontype="mult"]').prop('selected', false);
    //     $(this).find('option[optiontype="single"]').prop('selected', false);
    // }
    else{
        $(this).find('option[optiontype="single"]').prop('selected', false);
    }
    $(this).selectpicker('refresh');
    init_process_checklist_table_details();
});

$('#checklistReportColumns').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    e.preventDefault();
    var optiontype = $(this).find('option').eq(clickedIndex).attr('type');
    var taskid = $(this).find('option').eq(clickedIndex).attr('taskid');
    
    // console.log(notSelected.length);
    if (optiontype == 'task'){
        $(this).find('option[type="subtask"][taskid="' + taskid + '"]').prop('selected', isSelected);
    }
    else{
        if (isSelected == false) {
            $(this).find('option[type="task"][taskid="' + taskid + '"]').prop('selected', false);
        }else{
            if(($(this).find('option[type="subtask"][taskid="' + taskid + '"]:not(:selected)').length) == 0){
                $(this).find('option[type="task"][taskid="' + taskid + '"]').prop('selected', true);
            }
        }
    }
    $(this).selectpicker('refresh');
    dynamicColumnVisibility();
});

$('#checklistReportStatus').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    init_process_checklist_table_details();
});

$('#confirmationStatus').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    init_process_checklist_table_details();
});

$('#checklistReportSearch').on('keyup', function(){
    init_process_checklist_table_details();
});

$('#loadMoreBtn').on('click', function(){
    append_process_checklist_table_details();
})

$('#processTableBody').on('click', '.assigneeInfo', function(){
    get_assignee_details(this);
})

$('#processTableBody').on('mouseover', '.checklistTitle', function () {
    // console.log($(this).data('checklistname'));
    $(this).tooltip({
        title: $(this).data('checklistname'),
        placement: "top",
        html: true
    });
    $(this).tooltip('show')
})

function init_resizable(){
    $(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
    var thHeight = $("table#processTable th:first").height();
    $(".resizable-header").resizable({
        handles: "e",
        minHeight: thHeight,
        maxHeight: thHeight,
        minWidth: 5,
        resize: function (event, ui) {
            var sizerID = "#" + $(event.target).find('div').attr('id');
            // console.log(ui);
            // console.log(sizerID)
            $(sizerID).width(ui.size.width);
            // console.log(ui.size.width)
        }
    });
}


$(function(){
    // Parameter options Value:  elementId, titleVal, liveSearchBool, liveSearchStyleVal, selectedTextFormatVal, cuntSelectedTextVal
    get_all_process_select_data(function (selectObj) {
        BootstrapSelect.init('#checklistReportStatus', 'Checklist Status', false, '', 'count', '{0} Statuses');
        BootstrapSelect.init('#checklistReportAssignees', 'Select Assignees', true, 'contains', 'count', '{0} Assignees');
        BootstrapSelect.init('#checklistReportColumns', 'Select Columns', true, 'contains', 'count', '{0} Columns');
        $('#processReportSelectDiv').fadeIn();
        $('#processId').data('processtype', 1);
        if (parseInt(selectObj.proc_details.processType_ID) == 2){
            $('#processId').data('processtype', 2);
            BootstrapSelect.init('#confirmationStatus', 'Confirmation Status', true, 'contains', 'count', '{0} Statuses');
            $('#coachingLogReportSelectDiv').fadeIn();
        }
        init_process_checklist_table_details();
    });


})