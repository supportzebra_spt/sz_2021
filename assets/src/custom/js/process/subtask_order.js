function change_subtask_sequence(element, order_val){
    $.when(fetchPostData({
        sub_task_id: $(element).data('id'),
        task_id: $(element).data('compid'),
        orderTo: order_val
    }, '/process/reorder_subtask_sequence')).then(function(updateSubtask) {
        // var updateTaskkObj = $.parseJSON(updateTask.trim());
        // console.log(updateTaskkObj);
        // displaytasks(function(task) {});
        display_allcomponents($(element).data('compid'), function(allComp) {});
    });
}
function moveToSubtaskSwal(element){
    $.when(fetchPostData({
        sub_task_id: $(element).data('id'),
        task_id: $(element).data('compid'),
    }, '/process/get_subtask_details')).then(function(subtask) {
        var subtaskObj = $.parseJSON(subtask.trim());
        // console.log(subtaskObj);
        swal({
            title: 'Move to?',
            input: 'number',
            showCancelButton: true,
            inputAttributes: {
                min: 1,
                max: parseInt(subtaskObj.max_sequence),
                step: 1
            },
            inputValue: 1,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value.trim() < 1) {
                        resolve('Invalid input')
                    } else if (value > parseInt(subtaskObj.max_sequence)) {
                        resolve('Must not exceed to ' + parseInt(subtaskObj.max_sequence));
                    } else {
                        change_subtask_sequence(element, value.trim())
                        resolve();
                    }
                })
            }
        })
    });
    // console.log(element);
}
$('#contentdiv').on('click', '.move_subtask', function() {
    // moveToSwal(this);
    moveToSubtaskSwal(this);
    // console.log("move to subtask");
});