function coaching_log_details(checklist_id) {
    console.log(checklist_id);
    $.when(fetchPostData({
        checklistId: checklist_id
    }, '/discipline_mic/get_coaching_log_details')).then(function (checkDetails) {
        let checkDetailsObj = $.parseJSON(checkDetails.trim());
        $('#checklistTitle').text(checkDetailsObj.checklist_details.checklistTitle);
        $('#coachingLogDetails').html(checkDetailsObj.content);
        $('#checklistDates').html(checkDetailsObj.dates);
    });
}

function update_coaching_log(checklist_id) {
    console.log(checklist_id);
    $.when(fetchPostData({
        checklistId: checklist_id
    }, '/discipline_mic/update_coaching_log')).then(function (update) {
        let updateObj = $.parseJSON(update.trim());
        window.location.reload(true);
        // if (parseInt(updateObj.update_stat)) {
        // }
    });
}

function confirmCoachingLog(element){
    $.when(fetchPostData({
        checklistId: $(element).data('checklistid')
    }, '/discipline_mic/confirm_coaching_log')).then(function (confirmation) {
        let confirmationObj = $.parseJSON(confirmation.trim());
        if (parseInt(confirmationObj.confirmed) == 1) {
            swal("Thank you for Confirming!", "You have successfully confirmed the Coaching Log Details", "success");
            window.location.reload(true);
        }else if (parseInt(confirmationObj.confirmed) == 2) {
            swal("Your supervisor just updated the coaching log details!", "You will not be able to confirm yet", "info");
            window.location.reload(true);
        } else {
            swal("Confirmation Error!", "Something went wrong while confirming the Coaching Log Details. Please inform this error immediately to your System Administrator", "error");
        }
    });
}

function confirmConfirmationDetails(element){
    swal({
        title: 'Are you sure you want to Confirm?',
        html: "Please double check the details before confirming.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Confirmed!',
        cancelButtonText: 'No, Not Yet',
        reverseButtons: true,
        // width: '500px'
    }).then(function (result) {
        if (result.value) {
            confirmCoachingLog(element)
        } else if (result.dismiss === 'cancel') {
            // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
        }
    });
}

$('#coachingLogDetails').on('click', '#edit_coaching_log', function () {
    // window.location.href = baseUrl + "/discipline_mic/update_coaching_log/" + $(this).data('procid') + "/" + $(this).data('checklistid');
    update_coaching_log($(this).data('checklistid'));
});

$('#coachingLogDetails').on('click', '#confirm_coaching_log', function () {
    confirmConfirmationDetails(this);
});

$(function(){
    var pathName = window.location.pathname;
    var pathArray = pathName.split('/');
    // console.log(pathArray);
    coaching_log_details(pathArray[4]);
})