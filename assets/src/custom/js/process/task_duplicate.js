function duplicationTask(element){
    let pathName = window.location.pathname;
    let pathArray = pathName.split('/');
    // console.log(pathArray);
    let task_id = $(element).data('id');
    $.when(fetchPostData({
        taskId: $(element).data('id')
    }, '/discipline_mic/task_duplication')).then(function (task) {
        var taskObj = $.parseJSON(task.trim());
        // console.log(taskObj);
        if (parseInt(taskObj.exist)){
            let task_id = taskObj.new_task_id;
            let process_id = taskObj.process_id;
            let task_title = taskObj.task_title;
            let stringx = '';
            let stringcon = '';
            let taskNum = $(".inputtask:last").data('ordernum') + 1;
            let maxnum = 0;
            if (taskObj.max_num == null) {
                maxnum = 0;
            } else {
                maxnum = taskObj.max_num;
            }
            stringx += "<div class='input-group m-form__group mb-1' id='addcheckbox" + task_id+ "' data-subtasks='" + maxnum + "'>";
            stringx += "<div class='input-group-prepend'>";
            stringx += "<span class='input-group-text' style='width: 4em !important;padding: .65rem .5rem!important;'>";
            stringx += " <span>" + taskNum +".</span>";
            stringx += "</span>";
            stringx += "</div> ";
            if (task_title == null || task_title == '') {
                stringx += "<input data-id='" + task_id + "' id='" + task_id + "' data-ordernum='" + taskNum + "' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value=''>";
            } else {
                stringx += "<input data-id='" + task_id + "' id='" + task_id + "' data-ordernum='" + taskNum + "' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask sampletask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value='" + task_title + "'>";
            }
            stringx += "<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='" + process_id + "'>";
            stringx += "<input type='hidden' id='htaskID' name='htaskID' value='" + task_id + "'>";
            stringx += "<div class='input-group-append'>";
            stringx += "<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
            stringx += "<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
            stringx += "<i class='fa fa-delete'></i>";
            stringx += "</a>";
            stringx += "<div class='m-dropdown__wrapper'>";
            stringx += "<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
            stringx += "<div class='m-dropdown__inner'>";
            stringx += "<div class='m-dropdown__body'> ";
            stringx += "<div class='m-dropdown__content'>";
            stringx += "<ul class='m-nav'>";
            stringx += "<li class='m-nav__item'>";
            stringx += "<a data-id='" + task_id + "' onclick='deleteTask(this)' class='m-nav__link'>";
            stringx += "<i class='m-nav__link-icon fa fa-trash'></i>";
            stringx += "<span class='m-nav__link-text'>Delete</span>";
            stringx += "</a>";
            stringx += "</li>";
            stringx += "<li class='m-nav__item'>";
            stringx += "<a data-id='" + task_id + "' class='m-nav__link taskDup'>";
            stringx += "<i class='m-nav__link-icon fa fa-copy'></i>";
            stringx += "<span class='m-nav__link-text'>Duplicate Task</span>";
            stringx += "</a>";
            stringx += "</li>";
            stringx += "</ul>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div> ";
            stringx += "</div>";

            // stringcon += "<div id='formcontent" + task_id + "'class='m-portlet__body formcontent' style='padding: 0rem;'>";
            // stringcon += "<input type='hidden' class='hiddentask' value='" + task_id + "'>"
            stringcon += "<div id='formcontent" + task_id + "'class='m-portlet__body formcontent' style='padding: 0rem;'>";
            stringcon += "<input type='hidden' class='hiddentask' value='" + task_id + "'>"
            if (pathArray[3] == 'checklist_update'){
                   stringcon += "<div class='col-12 pt-2 pb-0' style='box-shadow: 0 1px 15px 1px rgba(44, 44, 45, 0.08);border-bottom: 1px solid gainsboro;'>";
                   stringcon += "<div id='rowdisplay" + task_id + "' class='row'>";
                   stringcon += "<div class='col-7 col-sm-8 col-md-8 col-lg-8 py-3'>";
                   stringcon += "<span id='custom-task-duedate-container-" + task_id + "'></span>"
                   // stringcon += "<div class='input-group m-input-icon m-input-icon--left'> ";
                   // stringcon += "<input readonly='true' type='text' data-id='" + item.task.task_ID + "' onmouseover='duedate(this)' id='taskduedate" + item.task.task_ID + "' class='form-control form-control-sm m-input' placeholder='Task Due Date'>";
                   // stringcon += "<span class='m-input-icon__icon m-input-icon__icon--left'><span><i class='fa fa-calendar-plus-o'></i></span></span> ";
                   // stringcon += "<div class='input-group-append'>";
                   // stringcon += "<button class='btn btn-brand btn-sm m-btn m-btn--custom' data-id='" + item.task.task_ID + "' onclick='savetaskduedate(this)' type='button'>Set</button>";
                   // stringcon += "</div>";
                   // stringcon += "</div>";
                   stringcon += "</div>";

                   stringcon += "<div class='col-5 col-sm-4 col-md-4 col-lg-4 text-center py-3'>";
                   stringcon += "<a href='#' class='btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x' data-tasktitle='" + task_title + "' data-id='" + task_id + "'  data-type='task' onclick='fetch_IDforAssignment(this),display_membernames(),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal' style='font-size: 0.8rem !important;'><i class='la la-user-plus'></i> Assign this task</a>";
                   stringcon += "</div>";
                   stringcon += "</div>";
                   // stringcon += "<div class='col-lg-12' style='background:yellow'>";
                   // stringcon += "Press left-click and drag to rearrange components";
                   // stringcon += "</div>";
                   stringcon += "</div>";
            }
        
             stringcon += "<div class='row'>";
             stringcon += "<div class='col-12'>";
             stringcon += "<form id='formtag" + task_id + "'  style='height:560px;overflow-y:scroll; overflow-x:auto;padding: 1rem 1.5rem 1rem 1.5rem;border-bottom: 1px solid #dedede;'>";
             stringcon += "</form>";
             stringcon += "</div>";
             stringcon += "</div>";
             // stringcon +="<div id='formaddcontent"+item.task_ID+"' class='row droparea_append'>";
             // stringcon +="</div>";
             stringcon += "<div class='row droparea_outside' style='border: 1.4px dashed #36a3f7 !important;'>";
             stringcon += "<div class='col-lg-12 droparea'>";
             stringcon += "<a href='#' data-id='" + task_id + "' id='" + task_id + "' onclick='taskid(this)' class='btn m-btn m-btn--icon m-btn--pill m-btn--air' data-toggle='modal' data-target='#add_content-modal'>";
             stringcon += "<span style='font-size: 1.2 rem;'>";
             stringcon += "<i class='fa fa-plus-circle text-info' style='font-size: 1.6rem!important;'></i>";
             stringcon += "<span class='text-info'>Add Content</span>";
             stringcon += "</span>";
             stringcon += "</a>";
             stringcon += "</div>";
             stringcon += "</div>";
             stringcon += "</div>";
            $("#checkgroup").append(stringx);
            $("#contentdiv").append(stringcon);
            $('#contentdiv').data('activetaskid', task_id);
            $('#checkgroup').find('input[data-id=' + task_id + ']').trigger('click');
            $('#checkgroup').find('input[data-id=' + task_id + ']').focus();
            // $(".inputtask:last").trigger('click');
            // $(".inputtask:last").focus();
            // init_task_after_add()
        }else{
            swal("Record Does Not Exist!", "It seems that the record you are about to remove does not exist. Please report this immediately to the System Administrator", "error");
        }
    })
}

function confirmTaskDuplication(element){
    $.when(fetchPostData({
        taskId: $(element).data('id')
    }, '/discipline_mic/check_task_details')).then(function (checkTask) {
        var checkTaskObj = $.parseJSON(checkTask.trim());
        // console.log(checkTaskObj);
        if (parseInt(checkTaskObj.exist)){
            let task_title = '"'+checkTaskObj.record.taskTitle+'" task';
            if (checkTaskObj.record.taskTitle == '' || checkTaskObj.record.taskTitle == null) {
                task_title = '"this untitled task"';
            }
            swal({
                title: 'Are you sure you want to duplicate  ' + task_title  + '?',
                html: "Please double check the task that you are about to duplicate.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Duplicate!',
                cancelButtonText: 'Don\'t Duplicate',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    duplicationTask(element);
                } else if (result.dismiss === 'cancel') {
                    // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
                }
            });
        }else{
            swal("Record Does Not Exist!", "It seems that the record you are about to remove does not exist. Please report this immediately to the System Administrator", "error");
        }
    })
}

$('#checkgroup').on('click', '.taskDup', function () {
    // console.log("test");
    confirmTaskDuplication(this);
    // console.log($(this).data('id'));
})
