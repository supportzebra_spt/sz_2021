$('#checkrow').on('mouseover', '.dateCreatedDetails', function () {
    let dateCreated = '<p class="mb-0" style="font-size:10px">Created On: ' + moment($(this).data('datecreated'), 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm:ss A') + '</p><p class="mb-0" style="font-size:10px">Created By:&nbsp;' + $(this).data('cname') + '</p>';
    $(this).tooltip({
        title: dateCreated,
        placement: "top",
        html: true
    });
    $(this).tooltip('show')
})

$('#checkrow').on('mouseover', '.checklistNotes', function () {
    $(this).tooltip({
        title: $(this).data('notes'),
        placement: "top",
        html: true
    });
    $(this).tooltip('show');
})

$('#checkrow').on('mouseover', '.checklistTitleInfo', function () {
    $(this).tooltip({
        title: $(this).data('finalchcklisttitle'),
        placement: "top",
        html: true
    });
    $(this).tooltip('show')
})


$('#checkrow').on('mouseover', '.cklistProgressBar', function () {
    console.log($(this).data('title'));
    $(this).tooltip({
        title: $(this).data('title'),
        placement: "top",
        html: true
    });
    $(this).tooltip('show')
})

$('#prorow').on('mouseover', '.protitle, .processCountBadge', function () {
    $(this).tooltip({
        title: $(this).data('title'),
        placement: "top",
        html: true
    });
    $(this).tooltip('show')
})

$('#checkrow').on('mouseover', '.processCountBadge', function () {
    $(this).tooltip({
        title: $(this).data('title'),
        placement: "top",
        html: true
    });
    $(this).tooltip('show')
})