// const { on } = require("gulp");

$('#prorow').on('click', '.duplicate_temp', function() {
    getprocess_dupdetails($(this).data('id'));
});

//PROCESS TEMPLATE DUPLICATE
$('#form_addduptemplate').formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    // live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        processDupTitle: {
            validators: {
                stringLength: {
                    max: 45,
                    message: 'The Process name must be less than 45 characters'
                },
                notEmpty: {
                    message: 'Oops! Process name is required!'
                },
            }
        },
    }
}).on('success.form.fv', function(e, data) {
    e.preventDefault();
    $('#form_addduptemplate').formValidation('disableSubmitButtons', true);
    add_duplicateTemplate();
});

function getprocess_dupdetails(pro_id) {
    $.ajax({
        type: "POST",
        url: baseUrl + "/process/fetchprocess_update",
        data: {
            process_ID: pro_id
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res.trim());
            $("#processDupTitle").val("(Duplicate)"+" "+result.processTitle);
            $("#tempduplicate_modal").data("protype", result.processType_ID);
            $("#tempduplicate_modal").data("proid", pro_id);
        }
    });
}

function add_duplicateTemplate(){
    console.log($("#tempduplicate_modal").data("proid"));
    console.log($("#tempduplicate_modal").data("protype"));
    $.ajax({
        type: "POST",
        url: baseUrl + "/process/add_duplicateprocesstemplate",
        data: {
            processDupTitle: $("#processDupTitle").val(),
            process_ID: $("#tempduplicate_modal").data("proid"),
            processtype_ID: $("#tempduplicate_modal").data("protype")
        },
        cache: false,
        success: function() {
            $("#prorow-perpage").change();
            swal({
                position: 'center',
                type: 'success',
                title: 'Successfully Added Process',
                showConfirmButton: false,
                timer: 1500
            });
            $("#tempduplicate_modal").modal("hide");
        }
    });
}


// $("#date_forchecklist").on('mouseover', function(){
    // $(this).datepicker({
    //     format: 'yyyy-mm-dd',
    //     autoclose: true,
    //     todayBtn: true
    //   });
// });


  