var shiftStartToShow = '';
var shiftEndToShow = '';
var shiftStartToCompute = '';
var shiftEndToCompute = '';

// DATATABLE ----------

var personalPendingAhrDatatable = function () {
    var personalPendingAhr = function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour_request/get_personal_pending_ahr_request",
                        params: {
                            query: {
                                status: 2,
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },

            search: {
                input: $('#"personalAhrRecordSearch'),
            },
            // columns definition
            columns: [{
                field: "additionalHourRequestId",
                title: "AHR ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var ahrId = row.additionalHourRequestId.padLeft(8);
                    var html = "<tr class='row'><td class = 'col-md-1'>" + ahrId + "</td>";
                    return html;
                }
            }, {
                field: 'dateTimeFiled',
                width: 100,
                title: 'Filed On',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-1'>" + row.dateTimeFiled + "</td>";
                    return html;
                },
            }, {
                field: 'typesDescription',
                width: 80,
                title: 'TYPE',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-1'>" + row.typesDescription + "</td>";
                    return html;
                },
            }, {
                field: 'accomplished',
                width: 200,
                title: 'Date and Time Accomplished',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-4'><p>" + row.otStartDate + " " + row.otStartTime + "</p><p>" + row.otEndDate + " " + row.otEndTime + "</p></td>";
                    return html;
                },
            }, {
                field: 'totalOtHours',
                width: 100,
                title: 'Time',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-2'>" + row.totalOtHours + "</td>";
                    return html;
                },
            }, {
                field: 'status',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-2'><p>" + row.statusDescription + "</p></td>";
                    return html;
                },
            }, {
                field: 'action',
                width: 80,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-trash"></i></a>';
                    return html;
                }
            }],
        };
        options.search = {
            input: $('#personalAhrRecordSearch'),
        };
        var datatable = $('#personalAhrRecordDatatable').mDatatable(options);
    };
    return {
        init: function () {
            personalPendingAhr();
        }
    };

}();

var personalRecordAhrDatatable = function () {
    var personalRecordAhr = function (statusVal, searchVal, dateFrom, dateTo) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour_request/get_personal_pending_ahr_request",
                        params: {
                            query: {
                                status: statusVal,
                                approvalRecordSearch: searchVal,
                                rangeStart: dateFrom,
                                rangeEnd: dateTo
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },

            search: {
                input: $('#personalPendingAhrSearch'),
            },
            // columns definition
            columns: [{
                field: "additionalHourRequestId",
                title: "AHR ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var ahrId = row.additionalHourRequestId.padLeft(8);
                    var html = "<tr class='row'><td class = 'col-md-1'>" + ahrId + "</td>";
                    return html;
                }
            }, {
                field: 'dateTimeFiled',
                width: 100,
                title: 'Filed On',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-1'>" + row.dateTimeFiled + "</td>";
                    return html;
                },
            }, {
                field: 'typesDescription',
                width: 80,
                title: 'TYPE',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-1'>" + row.typesDescription + "</td>";
                    return html;
                },
            }, {
                field: 'accomplished',
                width: 200,
                title: 'Date and Time Accomplished',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-4'><p>" + row.otStartDate + " " + row.otStartTime + "</p><p>" + row.otEndDate + " " + row.otEndTime + "</p></td>";
                    return html;
                },
            }, {
                field: 'totalOtHours',
                width: 100,
                title: 'Time',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-2'>" + row.totalOtHours + "</td>";
                    return html;
                },
            }, {
                field: 'status',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td class = 'col-md-2'><p>" + row.statusDescription + "</p></td>";
                    return html;
                },
            }, {
                field: 'action',
                width: 80,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-trash"></i></a>';
                    return html;
                }
            }],
        };
        options.search = {
            input: $('#personalAhrRecordSearch'),
        };
        var datatable = $('#personalAhrRecordDatatable').mDatatable(options);
    };
    return {
        init: function (statusVal, searchVal, dateFrom, dateTo) {
            personalRecordAhr(statusVal, searchVal, dateFrom, dateTo);
        }
    };

}();


function initDisabled() {
    $('#startDate').prop('disabled', true);
    $('#startTime').prop('disabled', true);
    $('#endDate').prop('disabled', false);
    $('#endTime').prop('disabled', false);
    $('#additional').hide();
}

function resetManualForm() {
    $('#unrenderedOt').text('00 hr & 00 min');
    $('#unrenderedOt').data('otHours', 0);
    $('#schedDate').val('');
    $('select[name="shift"]').html('');
    $('#startDate').val('');
    $('#startTime').val('').timepicker('setDefaultTime', 'current');
    $('#endDate').val('');
    $('#endTime').val('').timepicker('setDefaultTime', 'current');
    $('#reasonManual').val('');
    $('#errNotifyUnrenderedAhr').hide();
    $('#requestManualAhrBtn').prop('disabled', false);
}

function getAccountShifts(callback) {
    $.when(fetchPostData({}, '/additional_hour_request/get_account_shifts')).then(function (accountShifts) {
        var accShifts = $.parseJSON(accountShifts.trim());
        callback(accShifts);
    });
}

function datedShift(bct, breaks, callback) {
    var dateSched = moment($('#schedDate').val(), 'MM/DD/YYYY').format('YYYY-MM-DD');
    var time = $('#shiftunrendered').find('option:selected').text().split('-');
    var rawShiftStart = moment(time[0], 'hh:mm:ss A').format('HH:mm');
    var shiftStart = moment(dateSched + " " + time[0], "YYYY-MM-DD 'hh:mm:ss A").valueOf();
    var shiftEnd = moment(dateSched + " " + time[1], "YYYY-MM-DD 'hh:mm:ss A").valueOf();

    if (rawShiftStart == "00:00") {
        shiftEnd = moment(shiftEnd).add(1, 'days').valueOf();
        shiftStart = moment(shiftStart).add(1, 'days').valueOf();
    }

    shiftStart = moment(shiftStart).subtract(bct, 'minutes').valueOf();

    if (shiftStart > shiftEnd) {
        shiftEnd = moment(shiftEnd).add(1, 'days').valueOf();
    }

    if (moment(shiftStart).format('HH:mm') == '00:00') {
        shiftStartToShow = moment(moment(shiftStart).subtract(1, 'days').valueOf()).format('MM/DD/YYYY hh:mm A');
    } else {
        shiftStartToShow = moment(shiftStart).format('MM/DD/YYYY hh:mm A');
    }
    if (moment(shiftEnd).format('HH:mm') == '00:00') {
        shiftEndToShow = moment(moment(shiftEnd).subtract(1, 'days').valueOf()).format('MM/DD/YYYY hh:mm A');
    } else {
        shiftEndToShow = moment(shiftEnd).format('MM/DD/YYYY hh:mm A');
    }
    console.log(rawShiftStart);
    shiftStartToCompute = moment(shiftStart).format('MM/DD/YYYY hh:mm A');
    shiftEndToCompute = moment(shiftEnd).format('MM/DD/YYYY hh:mm A');
    console.log("SHIFT START: " + moment(shiftStart).format('MM/DD/YYYY hh:mm A'));
    console.log("SHIFT END: " + moment(shiftEnd).format('MM/DD/YYYY hh:mm A'));
    console.log("shift start to show:" + shiftStartToShow);
    console.log("shift end to show:" + shiftEndToShow);
    callback();
    $('#additionalVal').text('');
    if ($('#ahrTypeUnrendered').val() == 1) {
        $('#startDate').val(moment(shiftEndToShow, 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY'));
        $('#startTime').val(moment(shiftEndToShow, 'MM/DD/YYYY hh:mm A').format('hh:mm A'));
        $('#endDate').val('');
        $('#endTime').val('');
    } else if ($('#ahrTypeUnrendered').val() == 2) {
        $('#startDate').val('');
        $('#startTime').val('');
        $('#endDate').val(moment(shiftStartToShow, 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY'));
        $('#endTime').val(moment(shiftStartToShow, 'MM/DD/YYYY hh:mm A').format('hh:mm A'));
        var bctMin = (bct) / 60;
        $('#additionalVal').data('bct', bct)
        humanTimeFormat(bctMin, function (formattedBct) {
            $('#additionalVal').text(formattedBct);
        })
    }
}

function validateUnrenderedAhrForm(callback) {
    console.log($('#ahrTypeUnrendered').val());
    console.log($('#schedDate').val());
    console.log($('#shiftunrendered').val());
    console.log($('#startDate').val());
    console.log($('#startTime').val());
    console.log($('#endDate').val());
    console.log($('#endTime').val());

    var fieldError = 0;
    var errorMssg = "Fields are required: ";
    if ($('#ahrTypeUnrendered').val() == '') {
        errorMssg += 'AHR TYPE';
        fieldError = 1;
    } else {
        if ($('#ahrTypeUnrendered').val() != 3) {
            if ($('#shiftunrendered').val() == null || $('#shiftunrendered').val() == 0) {
                errorMssg += ' Shift,';
                fieldError = 1;
            }
        }
    }

    if ($('#schedDate').val() == '') {
        errorMssg += ' Date Schedule,';
        fieldError = 1;
    }

    if ($('#startDate').val() == '') {
        errorMssg += ' Start Date,';
        fieldError = 1;
    }

    if ($('#startTime').val() == '') {
        errorMssg += ' Start Time,';
        fieldError = 1;
    }
    if ($('#endDate').val() == '') {
        errorMssg += ' End Date,';
        fieldError = 1;
    }
    if ($('#endTime').val() == '') {
        errorMssg += ' End Time,';
        fieldError = 1;
    }

    if (!$.trim($("#reasonManual").val())) {
        errorMssg += ' Reason,';
        fieldError = 1;
    }
    if ($('#unrenderedOt').data('otHours') == undefined || $('#unrenderedOt').data('otHours') == 0) {
        fieldError = 1;
    }

    if (fieldError) {
        $('#errNotifyUnrenderedAhrMssg').html(errorMssg);
        $('#errNotifyUnrenderedAhr').show();
        // $('#requestManualAhrBtn').prop('disabled', true);
    } else {
        $('#errNotifyUnrenderedAhr').hide();
        $('#requestManualAhrBtn').prop('disabled', false);
    }
    console.log(errorMssg);
    callback(fieldError);
}

function getBctAndBreakNoSched(callback) {
    $.when(fetchPostData({
        schedId: $('#shiftunrendered').find('option:selected').val()
    }, '/additional_hour_request/get_breaks_for_unrendered_filing')).then(function (res) {
        var bctBreak = jQuery.parseJSON(res.trim());
        datedShift(bctBreak.bct, bctBreak.totalBreak.minutes, function () {});
    });
    callback();
}

function getBctAndBreak(callback) {
    var bctBreakObj = '';
    $.when(fetchPostData({
        schedId: $('#shiftunrendered').find('option:selected').val()
    }, '/additional_hour_request/get_breaks_for_unrendered_filing')).then(function (res) {
        var bctBreak = jQuery.parseJSON(res.trim());
        datedShift(bctBreak.bct, bctBreak.totalBreak.minutes, function () {});
    });
    callback();
}

function updateFormLayout() {
    if ($('#ahrTypeUnrendered').val() == 1) {
        $('#shiftField').show();
        $('#shiftWord').hide();
        $('#endDate').val('');
        $('#endTime').timepicker('setDefaultTime', 'current');
        $('#startDate').prop('disabled', true);
        $('#startTime').prop('disabled', true);
        $('#endDate').prop('disabled', false);
        $('#endTime').prop('disabled', false);
        $('#shiftunrendered').show()
        $('#additional').hide();
    } else if ($('#ahrTypeUnrendered').val() == 2) {
        $('#shiftField').show();
        $('#shiftWord').hide();
        $('#startDate').val('');
        $('#startTime').timepicker('setDefaultTime', 'current');
        $('#endDate').prop('disabled', true);
        $('#endTime').prop('disabled', true);
        $('#startDate').prop('disabled', false);
        $('#startTime').prop('disabled', false);
        $('#shiftunrendered').show()
        $('#additionalLabel').text('BCT');
        $('#additional').show();
    } else {
        $('#shiftField').hide();
        getAccountShifts(function (accShifts) {
            var optionsAsString = "";
            $.each(accShifts, function (index, value) {
                optionsAsString += "<option value='" + value.acc_time_id + "'>" + value.time_start + " - " + value.time_end + "</option>";
            });
            $('#shiftWordUnrendered').html(optionsAsString);
            // $("#shiftWordUnrendered").select2("destroy").select2();
            $('#shiftWordUnrendered').select2({
                placeholder: "Select a shift",
                width: '100%'
            });
            $('#shiftWord').show();
        })
        $('#endDate').prop('disabled', true);
        $('#endTime').prop('disabled', true);
        $('#startDate').prop('disabled', true);
        $('#startTime').prop('disabled', true);
        // $('#shiftunrendered').hide()
        $('#additionalLabel').text('TOTAL GIVEN BREAK');
        $('#additional').show();

    }
}

function assignTimeDate(dateSched, shift) {
    var shiftVal = shift.split('-');
    console.log(shift);
    var shiftStart = moment(dateSched + " " + shiftVal[0], "MM/DD/YYYY hh:mm:ss A").valueOf();
    var shiftEnd = moment(dateSched + " " + shiftVal[1], "MM/DD/YYYY hh:mm:ss A").valueOf();
    console.log(moment(shiftStart).format('Y-MM-DD hh:mm A'));
    console.log(moment(shiftEnd).format('Y-MM-DD hh:mm A'));
    if (shiftStart > shiftEnd) {
        shiftEnd = moment(shiftEnd).add(1, 'days').valueOf();
    }
    shiftStartToCompute = moment(shiftStart).format('MM/DD/YYYY h:mm A');
    shiftEndToCompute = moment(shiftEnd).format('MM/DD/YYYY h:mm A');
    console.log('shift end:' + moment(shiftEnd).format('Y-MM-DD hh:mm A'));
    if (moment(shiftStart).format('HH:mm') == '00:00') {
        console.log('shift start is 12 am');
        var endDateTime = moment(shiftEnd).add(1, 'days').valueOf();
    } else if (moment(shiftEnd).format('HH:mm') == '00:00') {
        console.log('shift end is 12 am');
        var endDateTime = moment(shiftEnd).subtract(1, 'days').valueOf();
    } else {
        var endDateTime = shiftEnd;
    }
    console.log(moment(endDateTime).format('MM/DD/YYYY hh:mm A'));
    $('#startDate').val(moment(shiftStart).format('MM/DD/YYYY'));
    $('#startTime').val(moment(shiftStart).format('h:mm A'));
    $('#endDate').val(moment(endDateTime).format('MM/DD/YYYY'));
    $('#endTime').val(moment(endDateTime).format('h:mm A'))

    computeManualAhrTime(function (renderedOt) {
        console.log(renderedOt);
        $('#unrenderedOt').text(renderedOt.formatted);
        $('#unrenderedOt').data('otHours', renderedOt.otHours);
    })

}

function checkIfAlreadyFiledNoSched(ahrTypeVal, schedDate, callback) {
    $.when(fetchPostData({
        ahrType: ahrTypeVal,
        dateSched: schedDate
    }, '/additional_hour_request/check_if_filed_no_sched')).then(function (checkIfFiled) {
        var checkIfFiledBool = $.parseJSON(checkIfFiled.trim());
        // console.log(existValue);
        callback(checkIfFiledBool);
    });
}

function getShiftUnrendered(date, callback) {
    $.when(fetchPostData({
        scheduleDate: date
    }, '/additional_hour_request/get_shift')).then(function (shift) {
        var shift = jQuery.parseJSON(shift.trim());
        var ahrTypeVal = $('#ahrTypeUnrendered').val();
        var optionsAsString = "";
        if (!shift) {
            var exist = 0;
        } else {
            var exist = 1;
            if (shift.length > 1) {
                optionsAsString += "<option value ='0'></option>";
            }
            $.each(shift, function (index, value) {
                optionsAsString += "<option value='" + value.schedId + "' shiftType='" + value.type + "'>" + value.time_start + " - " + value.time_end + "</option>";
                $('#shiftunrendered').data('empid', value.empId);
                $('#shiftunrendered').data('accountid', value.accountId);
            });
            $('select[name="shift"]').html(optionsAsString);
            if (shift.length == 1) {
                var schedIdVal = $('#shiftunrendered').val()
                checkIfAlreadyFiled(ahrTypeVal, schedIdVal, function (checkIfFiledBool) {
                    if (parseInt(checkIfFiledBool.exist)) {
                        $('#errNotifyUnrenderedAhrMssg').html('You have already filed a request for this schedule.');
                        $('#errNotifyUnrenderedAhr').show();
                        $('#requestManualAhrBtn').prop('disabled', true);
                    } else {
                        $('#errNotifyUnrenderedAhr').hide();
                        $('#requestManualAhrBtn').prop('disabled', false);
                        getBctAndBreak(function () {});
                    }
                })
            }
        }
        callback(exist)
    });
}

function checkAhrTypeForShiftChecking() {
    if ($('#ahrTypeUnrendered').val() != 3) {
        getShiftUnrendered($('#schedDate').val(), function (exist) {
            if (exist) {
                checkIfAlreadyFiled($('#ahrTypeUnrendered').val(), $('#shiftunrendered').val(), function (checkIfFiledBool) {
                    if (parseInt(checkIfFiledBool.exist)) {
                        $('#errNotifyUnrenderedAhrMssg').html('You have already filed a request for this schedule.');
                        $('#errNotifyUnrenderedAhr').show();
                        $('#requestManualAhrBtn').prop('disabled', true);
                    } else {
                        $('#errNotifyUnrenderedAhr').hide();
                        $('#requestManualAhrBtn').prop('disabled', false);
                        getBctAndBreak(function () {});
                    }
                })
            } else {
                $('#errNotifyUnrenderedAhrMssg').html('No Schedule found. Please inform your Supervisor');
                $('#errNotifyUnrenderedAhr').show();
                $('#requestManualAhrBtn').prop('disabled', true);
                $('#shiftField').show();
            }
        });
    } else {
        $.when(fetchPostData({
            scheduleDate: moment($('#schedDate').val(), 'MM/DD/Y').format('Y-MM-DD'),
        }, '/additional_hour_request/check_if_personal_sched_rest_day')).then(function (schedule) {
            var sched = jQuery.parseJSON(schedule.trim());
            console.log('schedbool ' + sched.bool)
            if (parseInt(sched.bool)) {
                $('#shiftWordUnrendered').data('schedid', sched.sched_id);
                checkIfAlreadyFiledNoSched($('#ahrTypeUnrendered').val(), moment($('#schedDate').val(), 'MM/DD/YYYY').format('Y-MM-DD'), function (checkIfFiledBool) {
                    if (parseInt(checkIfFiledBool.exist)) {
                        $('#errNotifyUnrenderedAhrMssg').html('You have already filed a request for this schedule.');
                        $('#errNotifyUnrenderedAhr').show();
                        $('#requestManualAhrBtn').prop('disabled', true);
                    } else {
                        $('#errNotifyUnrenderedAhr').hide();
                        $('#shiftField').hide();
                        $('#requestManualAhrBtn').prop('disabled', false);
                    }
                })

            } else {
                $('#errNotifyUnrenderedAhrMssg').html('Sorry you cannot file Word. This date is either not assigned with a schedule or not set as Rest day.');
                $('#requestManualAhrBtn').prop('disabled', true);
                $('#errNotifyUnrenderedAhr').show();
            }
        });
    }
}

function computeManualAhrTime(callback) {
    var start = moment($('#startDate').val() + " " + $('#startTime').val(), 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY hh:mm A');
    var end = moment($('#endDate').val() + " " + $('#endTime').val(), 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY hh:mm A');

    if ($('#ahrTypeUnrendered').val() == 1) {
        console.log("shift start:" + shiftEndToCompute);
        console.log("input time end" + end);
        startStr = moment(shiftEndToCompute, 'MM/DD/YYYY hh:mm A').valueOf();
        endStr = moment(end, 'MM/DD/YYYY hh:mm A').valueOf();
    } else if ($('#ahrTypeUnrendered').val() == 2) {
        startStr = moment(start, 'MM/DD/YYYY hh:mm A').valueOf();
        endStr = moment(shiftStartToCompute, 'MM/DD/YYYY hh:mm A').valueOf();
    } else {
        startStr = moment(shiftStartToCompute, 'MM/DD/YYYY h:mm A').valueOf();
        endStr = moment(shiftEndToCompute, 'MM/DD/YYYY h:mm A').valueOf();
    }
    var hours = (endStr - startStr) / 3600000;


    if (hours <= 0) {
        hours = 0;
    } else {
        // hours = hours;
        if (parseInt($('#ahrTypeUnrendered').val()) == 2) {
            var hours = hours - (parseFloat($('#additionalVal').data('bct')) / 60);
        } else if (parseInt($('#ahrTypeUnrendered').val()) == 3) {
            console.log('break ' + $('#additionalVal').data('break'));
            var breaks = parseFloat($('#additionalVal').data('break')) / 60;
            var hours = hours - breaks;
        } else {
            var hours = hours;
        }
    }
    humanTimeFormat(hours, function (humanTimeFormat) {
        var renderedOt = {
            otHours: hours.toFixed(2),
            formatted: humanTimeFormat
        };
        callback(renderedOt);
        // console.log(renderedOt);
    });
    // console.log("manual ot time: " + seconds)
}

function checkIfRenderedEnable() {
    alert('rendered check');
    $.when(fetchGetData('/additional_hour_request/check_if_rendered_enabled')).then(function (enableBool) {
        var enable = jQuery.parseJSON(enableBool.trim());
        console.log("ENABLE");
        console.log(enable);
        if (parseInt(enable)) {
            console.log("WITH RENDERED");
            $('#unrenderedOnly').hide();
            $('#dropdownMenuButton').removeClass('d-none');
            $('#dropdownMenuButton')[0].removeAttribute('style');
        } else {
            console.log("WITHOUT RENDERED");
            $('#dropdownMenuButton').addClass('d-none');
            $('#unrenderedOnly').show();
        }
    });
}

$('#unrenderedAhrFilingModal').on('hidden.bs.modal', function () {
    resetManualForm();
})

$('#ahrTypeUnrendered').on('change', function () {
    updateFormLayout();
    if ($('#schedDate').val() != '') {
        checkAhrTypeForShiftChecking();
    }
})

$('#schedDate').on('change', function () {
    checkAhrTypeForShiftChecking();
})

$('#shiftunrendered').on('change', function () {
    var ahrTypeVal = $('#ahrTypeUnrendered').val();
    var schedIdVal = $('#shiftunrendered').val()
    checkIfAlreadyFiled(ahrTypeVal, schedIdVal, function (checkIfFiledBool) {
        if (parseInt(checkIfFiledBool.exist)) {
            $('#errNotifyUnrenderedAhrMssg').html('You have already filed a request for this schedule.');
            $('#errNotifyUnrenderedAhr').show();
            $('#requestManualAhrBtn').prop('disabled', true);
        } else {
            $('#errNotifyUnrenderedAhr').hide();
            $('#requestManualAhrBtn').prop('disabled', false);
            getBctAndBreak(function () {});
        }
    })

})

$('#reasonManual').bind('input propertychange', function () {
    validateUnrenderedAhrForm(function (fieldError) {
        if (fieldError) {
            $('#requestManualAhrBtn').prop('disabled', true);
        } else {
            $('#requestManualAhrBtn').prop('disabled', false);
        }
    })
})

$('#requestManualAhrBtn').on('click', function () {
    validateUnrenderedAhrForm(function (fieldError) {
        if (fieldError) {
            console.log("Incomplete fields")
        } else {
            if ($('#ahrTypeUnrendered').val() != 3) {
                var shiftId = $('#shiftunrendered').val();
                var acc_time_id = 0;
            } else {
                var shiftId = $('#shiftWordUnrendered').data('schedid');
                var acc_time_id = $('#shiftWordUnrendered').val()
            }
            requestAhr($('#ahrTypeUnrendered').val(), shiftId, moment($('#startDate').val(), "MM/DD/YYYY").format('YYYY-MM-DD'), moment($('#startTime').val(), "hh:mm A").format('HH:mm'), moment($('#endDate').val(), "MM/DD/YYYY").format('YYYY-MM-DD'), moment($('#endTime').val(), "hh:mm A").format('HH:mm'), $('#unrenderedOt').data('otHours'), $('#reasonManual').val(), 2, acc_time_id, function (insertStat) {
                var addStat = jQuery.parseJSON(insertStat.trim());
                if (parseInt(addStat.request_stat) != 0) {
                    // var notif_id[0] = parseInt(addStat.set_notif_details.notif_id);
                    notification(addStat.set_notif_details.notif_id);
                    swal("Successfully Filed Request!", "Your request is now directed to your Immediate Supervisor for approval.", "success");
                } else {
                    swal("File Request Error !", "Something went wrong while filing your request. Please report the issue immediately to your supervisor or to the system administrator", "error");
                }
                $('#personalAhrPendingDatatable').mDatatable('reload');
                $('#unrenderedAhrFilingModal').modal('hide');
            });
        }
    })
})

$('#startDate, #startTime, #endDate, #endTime').on('change', function () {
    console.log($('#startDate').val());
    console.log($('#startTime').val());
    console.log($('#endDate').val());
    console.log($('#endTime').val());
    if (($('#startDate').val() != '') && ($('#startTime').val() != '') && ($('#endDate').val() != '') && ($('#endTime').val() != '')) {
        if ($('#ahrTypeUnrendered').val() != 3) {
            if (($('#shiftunrendered').val() !== 0) && ($('#shiftunrendered').val() !== '')) {
                computeManualAhrTime(function (renderedOt) {
                    console.log(renderedOt);
                    console.log(renderedOt.formatted);
                    console.log(renderedOt.otHours);
                    $('#unrenderedOt').text(renderedOt.formatted);
                    $('#unrenderedOt').data('otHours', renderedOt.otHours);
                    if (renderedOt.otHours == 0.00) {
                        $('#errNotifyUnrenderedAhrMssg').html('Sorry, You don\'t have enought time to file an Additional Hour Request.');
                        $('#errNotifyUnrenderedAhr').show();
                        $('#requestManualAhrBtn').prop('disabled', true);
                    } else {
                        $('#errNotifyUnrenderedAhr').hide();
                        $('#requestManualAhrBtn').prop('disabled', false);
                    }
                });
            }
        } else {
            computeManualAhrTime(function (renderedOt) {
                console.log(renderedOt);
                $('#unrenderedOt').text(renderedOt.formatted);
                $('#unrenderedOt').data('otHours', renderedOt.otHours);
                if (renderedOt.otHours == 0.00) {
                    $('#errNotifyUnrenderedAhrMssg').html('Sorry, You don\'t have enought time to file an Additional Hour Request.');
                    $('#errNotifyUnrenderedAhr').show();
                    $('#requestManualAhrBtn').prop('disabled', true);
                } else {
                    $('#errNotifyUnrenderedAhr').hide();
                    $('#requestManualAhrBtn').prop('disabled', false);
                }
            });
        }
    }
})

$('#schedDate').on('change', function () {
    if ($('#ahrTypeUnrendered').val() == 3) {
        getShiftBreak($('#shiftWordUnrendered').val(), function (breaks) {
            var breakHr = (breaks.minutes) / 60;
            $('#additionalVal').data('break', breaks.minutes)
            humanTimeFormat(breakHr, function (formattedBreak) {
                $('#additionalVal').text(formattedBreak);
            })
            assignTimeDate($('#schedDate').val(), $('#shiftWordUnrendered').find('option:selected').text());
        })
    }
})

$('#shiftWordUnrendered').on('change', function () {
    if ($('#schedDate').val() != '') {
        getShiftBreak($(this).val(), function (breaks) {
            var breakHr = (breaks.minutes) / 60;
            $('#additionalVal').data('break', breaks.minutes)
            humanTimeFormat(breakHr, function (formattedBreak) {
                $('#additionalVal').text(formattedBreak);
            })
            assignTimeDate($('#schedDate').val(), $('#shiftWordUnrendered').find('option:selected').text());
        })
    }
})

$(function () {
    initDisabled();
    checkIfRenderedEnable();
    personalPendingAhrDatatable.init();

})