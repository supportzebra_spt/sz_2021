function setRendereAhr(accountId,acc_name) {
    
    $.when(fetchPostData({
        accountId: accountId,
        renderedAhr: $('#renderedSwitch' + accountId + '').prop('checked'),
    }, '/additional_hour/set_rendered_ahr_account')).then(function (update) {
        var updateStat = jQuery.parseJSON(update.trim());
        if (updateStat) {
            if ($('#renderedSwitch' + accountId + '').prop('checked')) {
                toastr.success('Successfully assigned Rendered AHR Filing Feature');
            } else {
                toastr.success('You have disabled Rendered AHR Filing Feature');
            }
        } else {
            toastr.error("Something went wrong while update. Please inform your system administrator");
        }
        $('#assignRenderedAhrTable').mDatatable('reload');
    });
}

var assignRenderedAhrDatatableAdmin = function () {
    var assignAhrAdmin = function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour/get_ahr_rendered_settings_admin",
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#assignRenderedAhrSearchAdmin'),
            },
            // columns definition
            columns: [{
                field: "acc_name",
                title: "ACCOUNT NAME",
                // width: 500,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    console.log(row.acc_id);
                    var html = "<td class = 'col-md-6'>" + row.acc_name + "</td>";
                    return html;
                }
            }, {
                field: 'ahr_rendered',
                // width: 80,
                title: 'Assign Rendered AHR',
                overflow: 'visible',
                sortable: false,
                textAlign: 'right',
                template: function (row, index, datatable) {
                    console.log(row);
                    if (parseInt(row.ahr_rendered) == 1) {
                        var checked = 'checked';
                    } else {
                        var checked = '';
                    }
                    var html = '<td class="col-md-6 pull-right"><span class="m-switch m-switch--icon">' +
                        '<label>' +
                        '<input id="renderedSwitch' + row.acc_id + '" ' + checked + ' onclick="setRendereAhr(' + row.acc_id + ',' + row.ahr_rendered + ')" type="checkbox" name="">' +
                        '<span></span>' +
                        '</label>' +
                        '</span></td>';
                    return html;
                },
            }],
        };
        options.search = {
            input: $('#assignRenderedAhrSearchAdmin'),
        };
        var datatable = $('#assignRenderedAhrTableAdmin').mDatatable(options);
    };
    return {
        init: function () {
            assignAhrAdmin();
        }
    };

}();

var assignRenderedAhrDatatableAgent = function () {
    var assignAhrAgent = function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour/get_ahr_rendered_settings_agent",
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },

            search: {
                input: $('#assignRenderedAhrSearchAgent'),
            },
            // columns definition
            columns: [{
                field: "acc_name",
                title: "ACCOUNT NAME",
                // width: 500,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    console.log(row.acc_id);
                    var html = "<td class = 'col-md-6'>" + row.acc_name + "</td>";
                    return html;
                }
            }, {
                field: 'ahr_rendered',
                // width: 80,
                title: 'Assign Rendered AHR',
                overflow: 'visible',
                sortable: false,
                textAlign: 'right',
                template: function (row, index, datatable) {
                    console.log(row);
                    if (parseInt(row.ahr_rendered) == 1) {
                        var checked = 'checked';
                    } else {
                        var checked = '';
                    }
                    var html = '<td class="col-md-6 pull-right"><span class="m-switch m-switch--icon">' +
                        '<label>' +
                        '<input id="renderedSwitch' + row.acc_id + '" ' + checked + ' onclick="setRendereAhr(' + row.acc_id + ',' + row.ahr_rendered + ')" type="checkbox" name="">' +
                        '<span></span>' +
                        '</label>' +
                        '</span></td>';
                    return html;
                },
            }],
        };
        options.search = {
            input: $('#assignRenderedAhrSearchAgent'),
        };
        var datatable = $('#assignRenderedAhrTableAgent').mDatatable(options);
    };
    return {
        init: function () {
            assignAhrAgent();
        }
    };

}();

// FUNCTIONS ---------------------------------------------------------------------------------------------

function initDaysAvailable() {
    $.when(fetchGetData('/additional_hour/get_days_available_to_file')).then(function (daysAvailable) {
        var days = jQuery.parseJSON(daysAvailable.trim());
        $('#daysAvailableText').text(days.daysAvailable);
        $('#daysAvailable').val(days.daysAvailable);
    });
}

// EVENT TRIGGERS ----------------------------------------------------------------------------------------

$('#daysAvailable').on('change', function () {
    $.when(fetchPostData({
        daysAvailable: $(this).val()
    }, '/additional_hour/set_available_days_to_file')).then(function (updateStat) {
        var stat = $.parseJSON(updateStat.trim());
        if (parseInt(stat)) {
            toastr.success('Successfully set Days Available')
            $('#daysAvailableText').text($('#daysAvailable').val());
        } else {
            toastr.error("Something went wrong while update. Please inform your system administrator");
            $('#daysAvailable').val($('#daysAvailableText').text());
        }
    });
})


$(function () {
    assignRenderedAhrDatatableAdmin.init();
    assignRenderedAhrDatatableAgent.init();
    initDaysAvailable();
    // PLUGING INITIALIZATION -----------------------------------------------------------------------------
    // TOUCHPIN
    $('#daysAvailable').TouchSpin({
        buttondown_class: 'btn btn-secondary',
        buttonup_class: 'btn btn-secondary',
        min: 1,
        max: 60,
        step: 1
    });
    // TOASTR

})