var pathName = window.location.pathname;
var userType = $("input[name='user_type']").val();
var chartData = null;
var answerOffset = 0;
var answerOffsetCnt = 0;
var imagePath = baseUrl + '/assets/images/';
var surveyAverage = 0;
var startDate = moment().startOf('week');
var endDate = moment().endOf('week');

getCurrentDateTime(function (date) {
    startDate = moment(date, 'Y-MM-DD').startOf('week');
    endDate = moment(date, 'Y-MM-DD').endOf('week');
});

var rangeInfo = startDate.format('MMM D') + ' - ' + endDate.format('MMM D');
var numChoiceSelected = 1;
var numChoiceSelectedEdit = 1;
var repeater = $('#m_repeater_3_2').repeater({
    isFirstItemUndeletable: true,
    ready: function () {
        $('.choice-select2_3').select2({
            placeholder: "Select choice",
            allowClear: true,
        });
    },
    show: function () {
        $(this).slideDown();
        initChoiceSelection2();
    },

    hide: function (deleteElement) {
        if (confirm('Are you sure you want to delete this element?')) {
            numChoiceSelectedEdit = numChoiceSelectedEdit - 1;
            $(this).slideUp(deleteElement);
            if (numChoiceSelectedEdit < $("input[name='edit_numberOfChoices']").val()) {
                $('#editNewChoice').show();
            }
        }
    }
});

function initChoiceSelection() {
    var choices = $("select.choice-select2").map(function () {
        return $(this).val();
    }).get();

    $.when(fetchPostData({
            choices
        }, '/szfive/get_all_choices'))
        .then(function (data) {
            data = jQuery.parseJSON(data);
            $.each(data, function (key, val) {
                $('.choice-select2').last().append('<option value="' + val.choiceId + '">' + val.label + '</option>');
            });
        });

    $('.choice-select2').select2({
        placeholder: "Select choice",
        allowClear: true,
    });

    if (numChoiceSelected > $("input[name='add_numberOfChoices']").val() - 1) {
        $('#addNewChoice').hide();

    } else {
        $('#addNewChoice').show();
    }
}

function initChoiceSelection2() {

    $('.choice-select2_3').select2({
        placeholder: "Select choice",
        allowClear: true,
    });
}

var SurveyDatatable = function () {

    var surveySelector = function () {

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/szfive/get_surveys",
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    }
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },

            search: {
                input: $('#surveySearch'),
            },
            columns: [{
                    field: "surveyId",
                    title: "SID",
                    width: 50,
                    selector: false,
                    sortable: 'asc',
                    textAlign: 'center'
                }, {
                    field: "question",
                    title: "Question",
                    sortable: 'asc',
                    width: 150
                }, {
                    field: "numberOfChoices",
                    title: "No. of Choices",
                    width: 80,
                    sortable: 'asc',
                    textAlign: 'center'
                },
                {
                    field: "clabels",
                    title: "Labels",
                    width: 150,
                    sortable: false,
                }, {
                    field: 'Actions',
                    width: 80,
                    title: 'Actions',
                    overflow: 'visible',
                    sortable: false,
                    template: function (row) {
                        var p = row.permissions;
                        var permissions = jQuery.parseJSON(p.settings.trim());
                        var action = '';
                        if ($.inArray('viewSurveys', permissions) != -1) {
                            action += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onClick="viewSurveyModal(' + row.surveyId + ')" title="View">\
                                    <i class="la la-eye"></i></a>';
                        }
                        if ($.inArray('editSurveys', permissions) != -1) {
                            action += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onClick="editSurveyModal(' + row.surveyId + ')" title="Edit survey">\
                            <i class="la la-edit"></i></a>';
                        }
                        return action;
                    },
                }
            ],
        };

        options.search = {
            input: $('#surveySearch'),
        };

        $('.m_datatable_survey').mDatatable(options);
    };

    var questionSelector = function () {

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/szfive/get_questions',
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: true,
                height: 550,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#questionSearch'),
            },
            columns: [{
                field: 'questionId',
                title: 'ID',
                width: 40,
                sortable: 'asc',

            }, {
                field: 'question',
                title: 'Questions',
                width: 220,
            }, {
                field: 'Actions',
                width: 80,
                title: 'Actions',
                sortable: false,
                overflow: 'visible',
                template: function (row) {
                    var p = row.permissions;
                    var permissions = jQuery.parseJSON(p.settings.trim());
                    var action = '';
                    if ($.inArray('viewQuestions', permissions) != -1) {
                        action += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onClick="viewQuestionModal(' + row.questionId + ')" title="View"><i class="la la-eye"></i></a>';
                    }
                    if ($.inArray('editQuestions', permissions) != -1) {
                        action += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onClick="editQuestionModal(' + row.questionId + ')" title="Edit question"><i class="la la-edit"></i></a>';
                    }
                    return action;
                },
            }],
        };
        options.extensions = {
            checkbox: {},
        };
        options.search = {
            input: $('#questionSearch'),
        };

        $('.m_datatable_question').mDatatable(options);
    };

    var choiceSelector = function () {

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/szfive/get_choices',
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: true,
                height: 550,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#choiceSearch'),
            },
            pagination: true,
            columns: [{
                field: 'choiceId',
                title: 'ID',
                width: 40,
                sortable: 'asc',
            }, {
                field: 'label',
                title: 'Label',
            }, {
                field: 'Actions',
                width: 100,
                title: 'Actions',
                sortable: false,
                overflow: 'visible',
                template: function (row) {
                    var p = row.permissions;
                    var permissions = jQuery.parseJSON(p.settings.trim());
                    var action = '';
                    if ($.inArray('viewChoices', permissions) != -1) {
                        action += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onClick="viewChoiceModal(' + row.choiceId + ')" title="View"><i class="la la-eye"></i>\</a>';
                    }
                    if ($.inArray('editChoices', permissions) != -1) {
                        action += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onClick="editChoiceModal(' + row.choiceId + ')" title="Edit choice"><i class="la la-edit"></i></a>';
                    }
                    return action;
                },
            }],
        };
        options.search = {
            input: $('#choiceSearch'),
        };

        $('.m_datatable_choice').mDatatable(options);
    };

    return {
        init: function () {
            surveySelector();
            questionSelector();
            choiceSelector();
        }
    };
}();

//== Select2
var Select2 = function () {
    var selectQuestion = function () {
        $('#select_question_validate, #select_question_editsurvey, #select_question_editsurvey_validate').select2({
            placeholder: "Select a question"
        });
        $('#select_question2, #select_question2_validate').select2({
            placeholder: "Select a question"
        });
        $('#settings_question, #settings_question_validate').select2({
            placeholder: "Select default question"
        });
        $('#m_form_status_survey_ans, #settings_question_validate').select2({
            placeholder: "Select status"
        });
        $('#m_form_type_survey_ans, #settings_question_validate').select2({
            placeholder: "Select account"
        });
    }

    // var selectChoice = function () {
    //     $('#select_choice, #select_choice_validate, #select_choices_editsurvey, #select_choices_editsurvey_validate').select2({
    //         placeholder: "Select choice",
    //         maximumSelectionLength: parseInt($("input[name='numberOfChoices']").val()),
    //     });
    // }

    // var selectChoice = function () {
    //     $('#select_choices_editsurvey, #select_choices_editsurvey_validate').select2({
    //         placeholder: "Select choice",
    //         maximumSelectionLength: parseInt($("input[name='numberOfChoices']").val()),
    //     });
    // }

    var selectSurvey = function () {
        $('#select_survey, #select_survey_validate').select2({
            placeholder: "Select survey",
        });
        $('#settings_survey, #settings_survey_validate').select2({
            placeholder: "Select survey",
        });
    }

    var surveyMainDaterange = function () {

        if ($('#survey_datepicker').length == 0) {
            return;
        }

        getCurrentDateTime(function (date) {
            var picker = $('#survey_datepicker');
            var start = moment(date, 'Y-MM-DD').startOf('isoWeek');
            var end = moment(date, 'Y-MM-DD').endOf('isoWeek');

            function cb(start, end, label) {

                var title = '';
                var range = '';

                if (start.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').startOf('isoWeek').format('YYYY-MM-DD') && end.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').endOf('isoWeek').format('YYYY-MM-DD')) {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Today') {
                    title = 'Today:';
                    range = start.format('MMM D, Y');
                } else if (label == 'Yesterday') {
                    title = 'Yesterday:';
                    range = start.format('MMM D, Y');
                } else if (label == 'This Week') {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Week') {
                    title = 'Last Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'This Month') {
                    title = 'This Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Month') {
                    title = 'Last Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else {
                    title = 'Custom:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                }

                picker.find('.m-subheader__daterange-date-reviewers').html(range);
                picker.find('.m-subheader__daterange-title-reviewers').html(title);

                $('#survey_datepicker .form-control').val(start.format('MMM D') + ' - ' + end.format('MMMM D, YYYY'));
                $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
                $('input[name=end_date]').val(end.format('YYYY-MM-DD'));

                if ($('#select_survey').val() != null) {
                    mApp.block('#main_chart_container', {
                        overlayColor: '#000000',
                        state: 'primary'
                    });  
                
                    getSurveyData(function (callback) {
                        mApp.unblock('#main_chart_container');
                    });
                }

                if (start.format('MMM D, Y') == end.format('MMM D, Y')) {
                    rangeInfo = start.format('MMM D, Y');
                } else {
                    rangeInfo = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                }

                $('#date-heading').html('' + rangeInfo);

                startDate = start;
                endDate = end;
            }

            picker.daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: {
                    'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                    'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                    'This Week': [moment(date, 'Y-MM-DD').startOf('isoWeek'), moment(date, 'Y-MM-DD').endOf('isoWeek')],
                    'Last Week': [moment(date, 'Y-MM-DD').subtract(1, 'week').startOf('isoWeek'), moment(date, 'Y-MM-DD').subtract(1, 'week').endOf('isoWeek')],
                    'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                    'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end, '');

        });

    }

    var surveyReportDaterange = function () {

        $('#daterange_survey_report').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function (start, end, label) {
            $('#daterange_survey_report .form-control').val(start.format('MMM D') + ' - ' + end.format('MMMM D, YYYY'));
            $('input[name=report_start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=report_end_date]').val(end.format('YYYY-MM-DD'));
        });
    }

    var answerFilter = function () {
        $('#m_form_status_survey_ans, #m_form_status_survey_ans_validate').select2({
            placeholder: "Select status",
        });

        $('#m_form_type_survey_ans, #m_form_type_survey_ans_validate').select2({
            placeholder: "Select account",
        });
    }

    return {
        init: function () {
            selectSurvey();
            selectQuestion();
            // selectChoice();
            surveyMainDaterange();
            // surveyReportDaterange();
            answerFilter();
        }
    };
}();

var Select2_modal = function () {
    var modalSelect2 = function () {
        $('#edit_survey_modal').on('shown.bs.modal', function () {
            $('.choice-select2_3').select2({
                placeholder: "Select choice",
                allowClear: true,
            });
        });
    }

    return {
        init: function () {
            modalSelect2();
        }
    };
}();

if ($("input[name='add_numberOfChoices']").val() == '') {
    $('#addNewChoice').hide();
} else {
    $('#addNewChoice').show();
}

if ($("input[name='edit_numberOfChoices']").val() == '') {
    $('#editNewChoice').hide();
} else {
    $('#editNewChoice').show();
}

$('#m_repeater_3').repeater({
    isFirstItemUndeletable: true,
    initEmpty: false,

    show: function () {

        if (numChoiceSelected <= $("input[name='add_numberOfChoices']").val() - 1) {
            numChoiceSelected = numChoiceSelected + 1;
            $(this).slideDown();
            initChoiceSelection();
        }
    },

    hide: function (deleteElement) {
        if (confirm('Are you sure you want to delete this element?')) {
            numChoiceSelected = numChoiceSelected - 1;
            $(this).slideUp(deleteElement);
        }
    }
});

$('#editNewChoice').on('click', function () {
    numChoiceSelectedEdit = numChoiceSelectedEdit + 1;
    if (numChoiceSelectedEdit >= $("input[name='edit_numberOfChoices']").val()) {
        $('#editNewChoice').hide();
    }
});


// Form Validation
var FormValidation = function () {

    var addSurvey = function () {

        $("#add_survey_form").validate({
            rules: {
                questionId: {
                    required: true,
                },
                add_numberOfChoices: {
                    required: true,
                },
                choiceLabels: {
                    required: true,
                    maximumSelectionLength: $("input[name='add_numberOfChoices']").val()
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {

                var question = $("select[name='questionId']").val();
                var numberOfChoices = $("#add_survey_form").find("input[name='add_numberOfChoices']").val();
                var choices = $("select.choice-select2").map(function () {
                    return $(this).val();
                }).get();

                if (numberOfChoices < choices.length) {
                    swal({
                        "title": "",
                        "text": "Choice labels are more than the specified number of choices.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                } else {

                    var options = {
                        url: baseUrl + '/szfive/add_survey',
                        type: 'POST',
                        dataType: "json",
                        data: {
                            question: question,
                            numberOfChoices: numberOfChoices,
                            choiceLabels: choices
                        },
                        success: function (response) {

                            if (response.status === true) {
                                swal({
                                    "title": "",
                                    "text": "Successfully added survey.",
                                    "type": "success",
                                    "timer": 1500,
                                    "showConfirmButton": false
                                });

                                $('.m_datatable_survey').mDatatable('reload');
                                $('#add_survey_modal').modal('hide');
                            }
                        },
                        error: function () {
                            alert('error');
                        },
                    };
                    $(form).ajaxSubmit(options);
                }
            }
        });
    }

    var editSurvey = function () {
        $('#edit_survey_form').validate({
            rules: {
                questionId: {
                    required: true,
                },
                numberOfChoices: {
                    required: true,
                },
                choiceLabels: {
                    required: true,
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {

                var surveyId = $('input[name="edit_surveyId"]').val();
                var question = $('#edit_survey_form').find("#select_question_editsurvey :selected").val();
                var numberOfChoices = $('#edit_survey_form').find("input[name='edit_numberOfChoices']").val();
                var choices = $("select.choice-select2_3").map(function () {
                    return $(this).val();
                }).get();

                if (numberOfChoices < choices.length) {
                    swal({
                        "title": "",
                        "text": "Choice labels are more than the specified number of choices.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                } else {
                    var options = {
                        url: baseUrl + '/szfive/edit_survey',
                        type: 'POST',
                        dataType: "json",
                        data: {
                            question: question,
                            numberOfChoices: numberOfChoices,
                            choiceLabels: choices,
                            surveyId: surveyId
                        },
                        success: function (response) {

                            if (response.status === true) {
                                swal({
                                    "title": "",
                                    "text": "Successfully updated survey.",
                                    "type": "success",
                                    "timer": 1500,
                                    "showConfirmButton": false

                                });
                                $('.m_datatable_survey').mDatatable('reload');
                                $('#edit_survey_modal').modal('hide');
                            } else {
                                swal({
                                    "title": "",
                                    "text": "Question already exists. Please choose another question.",
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                            }
                        },
                        error: function () {
                            alert('error');
                        },
                    };
                    $(form).ajaxSubmit(options);
                }
            }
        })
    }

    var editQuestion = function () {
        $('#edit_question_form').validate({
            rules: {
                question: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {

                var questionId = $('#edit_question_form').find('input[name=questionId]').val();
                var question = $('#edit_question_form').find('#question').val();

                var options = {
                    url: baseUrl + '/szfive/edit_question',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        question: question
                    },
                    success: function (response) {

                        if (response.status === true) {
                            swal({
                                "title": "",
                                "text": "Successfully updated question.",
                                "type": "success",
                                "timer": 1500,
                                "showConfirmButton": false

                            });
                            $('#edit_question_modal').modal('hide');
                            $('.m_datatable_question').mDatatable('reload');
                            $('.m_datatable_survey').mDatatable('reload');

                        }
                    },
                    error: function () {
                        alert('error');
                    },
                };
                $(form).ajaxSubmit(options);
            }
        })
    }

    var editChoice = function () {
        $('#edit_choice_form').validate({
            rules: {
                choice: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {

                var choice = $('#edit_choice_form').find('input[name=choice]').val();
                var label = $('#edit_choice_form').find('#choiceLabel').val();

                var options = {
                    url: baseUrl + '/szfive/edit_choice',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        choice: choice,
                        label: label,
                    },
                    success: function (response) {

                        if (response.status === true) {
                            swal({
                                "title": "",
                                "text": "Successfully updated choice.",
                                "type": "success",
                                "timer": 1500,
                                "showConfirmButton": false
                            });
                            $('#edit_choice_modal').modal('hide');
                            $('.m_datatable_choice').mDatatable('reload');
                            $('.m_datatable_survey').mDatatable('reload');

                        }
                    },
                    error: function () {
                        alert('error');
                    },
                };
                $(form).ajaxSubmit(options);
            }
        })
    }
    return {
        init: function () {
            addSurvey();
            editQuestion();
            editChoice();
            editSurvey();
        }
    };
}();

var AnswerDatatable = function () {

    var answerSelector = function (data) {
        var status = {
            1: {
                'title': 'Teary',
                'color': 'rgb(237, 77, 80)'
            },
            2: {
                'title': 'Sad',
                'color': 'rgb(247, 180, 50)'
            },
            3: {
                'title': 'Average',
                'color': 'rgb(252, 214, 51)'
            },
            4: {
                'title': 'Happy',
                'color': 'rgb(199, 212, 90)'
            },
            5: {
                'title': 'Amazing',
                'color': 'rgb(98, 212, 153)'
            },
        };

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/szfive/get_answers',
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
                        params: {
                            query: {
                                surveyId: data.surveyId,
                                startDate: data.startDate,
                                endDate: data.endDate,
                            },
                        },
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: 'dashed-table',
                scroll: false,
                footer: false,
                header: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#answerSearch'),
            },
            translate: {
                records: {
                    processing: 'Please wait...',
                    noRecords: '<i class="flaticon-edit" style="font-size: 24px;margin: 0 5px;"></i><br> No answers found.',
                },
            },
            columns: [{
                width: 250,
                field: 'employeeId',
                title: 'Name',
                template: function (data) {
                    var pic = data.pic ? data.pic : "images/img/sz.png";
                    output = '<div onClick="showUserAnswer(' + data.answerId + ')" class="m-card-user m-card-user--sm" style="cursor:pointer;    margin-left: 20px;">\
                                   <div class ="m-card-user__pic"> \
                                        <img src="' + imagePath + pic + '" onerror="noImage2(' + data.emp_id + ')" style="height:40px;" class = "m--img -rounded m--marginless answer-pic-dt' + data.emp_id + '" alt = "photo "> \
                                    </div>\
                                    <div class="m-card-user__details">\
                                       <span class="m-card-user__name" style="font-weight: 600;">' + data.user_name + '</span>\
                                        <span class ="m-card-user__email"> ' + data.pos_details + ' </span>\
                                    </div>\
                                </div>';
                    return output;
                },
            }, {
                field: 'emp_stat',
                title: 'Status',
                width: 100,
                template: function (data) {
                    var status = (data.emp_stat ? data.emp_stat : '-');
                    return '<span style="font-size: 12px;">' + status + '</span>';
                },
            }, {
                field: 'choiceLabels',
                title: 'Score',
                width: 50,
                template: function (data) {
                    output = '<div class="m-card-user m-card-user--sm">\
                                    <div class="m-card-user__pic" style="padding-right: 20px;">\
                                        <div class="m-card-user__no-photo" style="color: black;background-color: ' + status[data.answer].color + '"><span>' + data.answer + '</span></div>\
                                    </div>\
                                </div>';
                    return output;
                },
            }, {
                field: 'a.dateCreated',
                title: "Date",
                sortable: 'desc',
                textAlign: 'center',
                width: 130,
                template: function (data) {
                    console.log(data);
                    var dateCreated = moment(data.answeredDate).format('MMM D');
                    return '<span>' + dateCreated + '</span>';
                }
            }, {
                field: "detailedAnswer",
                title: "Comments",
                sortable: false,
                width: 250,
                overflow: 'visible',
                template: function (data) {

                    var length = 30;
                    var detailedAnswer = (data.detailedAnswer ? data.detailedAnswer : '');
                    var trimmedString = detailedAnswer.length > length ? detailedAnswer.substring(0, length - 3) + "..." : detailedAnswer.substring(0, length);

                    output = '<div>\
                                <span>' + trimmedString + '</span>\
                                </a>\
                            </div>\
                            ';

                    return output;
                }
            }],
        };

        var datatable = $('.m_datatable_answers').mDatatable(options);
        var query = datatable.getDataSourceQuery();

        $('#m_form_status_survey_ans').on('change', function () {
            var query = datatable.getDataSourceQuery();
            query.employeeStatus = $(this).val() != 'All' ? $(this).val().toLowerCase() : '';
            query.accountName = $('#m_form_type_survey_ans').val() != 'All' ? $('#m_form_type_survey_ans').val() : '';
            datatable.setDataSourceQuery(query);
            datatable.load();
            hasAnswerData(query);
        }).val(typeof query.employeeStatus !== 'undefined' ? query.employeeStatus : '');

        $('#m_form_type_survey_ans').on('change', function () {
            var query = datatable.getDataSourceQuery();
            query.accountName = $(this).val() != 'All' ? $(this).val().toLowerCase() : '';
            query.employeeStatus = $('#m_form_status_survey_ans').val() != 'All' ? $('#m_form_status_survey_ans').val() : '';
            datatable.setDataSourceQuery(query);
            datatable.load();
            hasAnswerData(query);
        }).val(typeof query.accountName !== 'undefined' ? query.accountName : '');
    };

    return {
        init: function (data) {
            answerSelector(data);
        },
    };
}();

jQuery(document).ready(function () {

    initChoiceSelection();
    initChoiceSelection2();

    Select2.init();
    FormValidation.init();

    if (userType == 1 || userType == 2) {
        SurveyDatatable.init();
    }

    $('#openSurveyModal').on('click', function () {
        $('#survey_modal').modal('show');
    });

    $('#openQuestionModal').on('click', function () {
        $('#survey_question_modal').modal('show');
    });

    $('#openChoiceModal').on('click', function () {
        $('#survey_choice_modal').modal('show');
    });

    $('#getReport').on('click', function () {
        var surveyId = $('#select_survey').val();
        var reportStartDate = $('input[name=start_date]').val();
        var reportEndDate = $('input[name=end_date]').val();

        window.location.href = baseUrl + "/szfive/generate_report_general/" + surveyId + "/" + reportStartDate + "/" + reportEndDate + "/" + surveyAverage;

    });

    $('#getReport_datatable').on('click', function () {
        var surveyId = $('#select_survey').val();
        var reportStartDate = $('input[name=start_date]').val();
        var reportEndDate = $('input[name=end_date]').val();
        var employeeStatus = $("input[name='employeeStatus']").val() != 'all' ? $("input[name='employeeStatus']").val() : 0;
        var accountName = $("input[name='accountName']").val() != 'all' ? $("input[name='accountName']").val() : 0;

        window.location.href = baseUrl + "/szfive/generate_report_specific/" + surveyId + "/" + reportStartDate + "/" + reportEndDate + "/" + surveyAverage + "/" + employeeStatus + "/" + accountName;

    });
});

// End of all Datatables
function surveyDetails(callback) {
    var surveyId = $('#select_survey').val();
    var startDate = $('input[name=start_date]').val();
    var endDate = $('input[name=end_date]').val();

    var answerData = {
        surveyId: surveyId,
        startDate: startDate,
        endDate: endDate
    };
    callback(answerData);
}

$(function () {

    $('#add_survey_modal').on('hidden.bs.modal', function () {
        $(this).find('#add_survey_form').trigger("reset");
        $(this).find('#select_question2').html("");
    });
    $('#edit_survey_modal').on('hidden.bs.modal', function () {
        $('#edit_survey_modal').attr('data-id', '');
        $(this).find('#edit_survey_form').trigger("reset");
        $(this).find('#select_question_editsurvey').prop("selected", false);
        $(this).find('#select_question_editsurvey').removeAttr("selected");
        $(this).find('#select_question_editsurvey').html("");
    });
    $('#add_question_modal').on('hidden.bs.modal', function () {
        $(this).find('#add_question_form')[0].reset();
    });

    $('#edit_question_modal').on('hidden.bs.modal', function () {
        $(this).find('#edit_question_form')[0].reset();
    });

    $('#survey_question_modal').on('shown.bs.modal', function () {
        $(document).off('focusin.modal');
    });

    $('#survey_choice_modal').on('shown.bs.modal', function () {
        $(document).off('focusin.modal');
    });

    $('#addSurvey').on('click', function () {
        $('#add_survey_modal').modal('show');
        $.when(fetchGetData('/szfive/get_survey_list'))
            .then(function (data) {
                data = jQuery.parseJSON(data);
                $.each(data, function (key, val) {
                    // $("#select_choices_editsurvey option[value='" + val + "']").prop("selected", true);
                    $('select#select_question2').append('<option value="' + val.questionId + '">' + val.question + '</option>');
                });
            });
        $('#select_question2, #select_question2_validate').select2({
            placeholder: "Select a question"
        });
    });

    // if ($('#select_survey').val() != null) {
    //     getSurveyData();
    // } else {
    //     removeChart();
    // }

    $('#report-chart').append('<div class="display-chart" style="text-align: center;"></div>');

    $('#addQuestion').on('click', function () {
        swal({
            title: '<strong>Add New Question</strong>',
            html: "<br /><form id='add_question_form' onsubmit='return false'><input autofocus minlength='3' class='form-control' type='text' name='question' placeholder='Enter question *'></form>",
            showCancelButton: true,
            confirmButtonText: 'Submit'
        }).then(function (result) {
            var question = $('#add_question_form').find('input[name=question]').val();
            var val = question.trim();

            if (result.value) {
                if (val == '') {
                    swal(
                        'Error',
                        'Question must not be empty.',
                        'error'
                    )
                } else {
                    $.ajax({
                        url: baseUrl + "/szfive/add_question",
                        type: 'POST',
                        dataType: "json",
                        data: {
                            question: val
                        },
                        success: function (response) {
                            if (response == true) {
                                swal({
                                    "title": "",
                                    "text": "Successfully created question.",
                                    "type": "success",
                                    "timer": 2000,
                                    "showConfirmButton": false
                                });
                                $('.m_datatable_question').mDatatable('reload');
                            } else {
                                swal({
                                    "title": "",
                                    "text": "Question already exists.",
                                    "type": "error",
                                });
                            }
                        },
                        error: function () {
                            swal({
                                "title": "",
                                "text": "Error",
                                "type": "error",
                                "timer": 2000,
                                "showConfirmButton": false
                            });
                        }
                    });
                }
            }
        });
    });

    $('#addChoice').on('click', function () {
        swal({
            title: '<strong>Add New Choice</strong>',
            html: "<br /><form id='add_choice_form' onsubmit='return false'><input autofocus minlength='3' class='form-control' type='text' name='choice' placeholder='Enter choice label *'></form>",
            showCancelButton: true,
            confirmButtonText: 'Submit'
        }).then(function (result) {
            var choice = $('#add_choice_form').find('input[name=choice]').val();
            var val = choice.trim();

            if (result.value) {
                if (val == '') {
                    swal(
                        'Error',
                        'Choice must not be empty.',
                        'error'
                    )
                } else {
                    $.ajax({
                        url: baseUrl + "/szfive/add_choice",
                        type: 'POST',
                        dataType: "json",
                        data: {
                            choice: val
                        },
                        success: function (response) {
                            if (response == true) {
                                swal({
                                    "title": "",
                                    "text": "Successfully created choice.",
                                    "type": "success",
                                    "timer": 2000,
                                    "showConfirmButton": false
                                });
                                $('.m_datatable_choice').mDatatable('reload');
                            } else {
                                swal({
                                    "title": "",
                                    "text": "Choice already exists.",
                                    "type": "error",
                                });
                            }
                        },
                        error: function () {
                            swal({
                                "title": "",
                                "text": "Error",
                                "type": "error",
                                "timer": 2000,
                                "showConfirmButton": false
                            });
                        }
                    });
                }
            }
        });
    });

    $("#viewMoreAnswers").on('click', function () {
        answerOffsetCnt = answerOffsetCnt + 1;
        answerItems = 5;
        data.offset = answerOffsetCnt * answerItems - answerItems;
        data.noOfItems = answerItems;
        $(".spinner-img").show();

        loadAnswers(data, function () {
            $('.spinner-img').hide();
        });
    });
});

function openSurveyModal() {
    $('#survey_modal').modal('show');
}

function openQuestionModal() {
    $('#survey_question_modal').modal('show');
}

function openChoiceModal() {
    $('#survey_choice_modal').modal('show');
}


function editSurveyModal(surveyId) {
    $('#m_repeater_3_2').find('[data-repeater-list]').empty();
    $('#m_repeater_3_2').find('[data-repeater-create]').click();

    $.ajax({
        url: baseUrl + "/szfive/view_survey",
        type: 'POST',
        dataType: "json",
        data: {
            surveyId: surveyId
        },
        success: function (data) {
            Select2_modal.init();
            $('#edit_survey_form').find('input[name=edit_numberOfChoices]').val(data.numberOfChoices);
            $('input[name="edit_surveyId"]').val(data.surveyId);
            numChoiceSelectedEdit = data.c_label_ids.length
            repeater.setList(data.c_label_ids);

            $.when(fetchGetData('/szfive/get_question_list'))
                .then(function (response) {
                    response = jQuery.parseJSON(response);
                    $.each(response, function (key, val) {
                        selected = '';
                        if (data.questionId == val.questionId) {
                            selected = 'selected';
                        }
                        $('#select_question_editsurvey').append('<option value="' + val.questionId + '" ' + selected + '>' + val.question + '</option>');
                    });
                });
            $('#select_question_editsurvey, #select_question_editsurvey_validate').select2({
                placeholder: "Select a question"
            });

            if (numChoiceSelectedEdit >= $("input[name='edit_numberOfChoices']").val()) {
                $('#editNewChoice').hide();
            } else {
                $('#editNewChoice').show();
            }
            $('#edit_survey_modal').modal('show');
        },
        error: function () {
            swal({
                "title": "",
                "text": "Error",
                "type": "error",
                "timer": 2000,
                "showConfirmButton": false
            });
        }
    });
}


function editQuestionModal(questionId) {
    $.ajax({
        url: baseUrl + "/szfive/view_question",
        type: 'POST',
        dataType: "json",
        data: {
            questionId: questionId
        },
        success: function (data) {

            $('#edit_question_form').find('#question').val(data.question);
            $('#edit_question_form').find('input[name=questionId]').val(data.questionId);
            $('#edit_question_modal').modal('show');
        },
        error: function () {
            swal({
                "title": "",
                "text": "Error",
                "type": "error",
                "timer": 2000,
                "showConfirmButton": false
            });
        }
    });
}

function editChoiceModal(choiceId) {
    $.ajax({
        url: baseUrl + "/szfive/view_choice",
        type: 'POST',
        dataType: "json",
        data: {
            choiceId: choiceId
        },
        success: function (data) {

            $('#edit_choice_form').find('input[name=choice]').val(data.choiceId);
            $('#edit_choice_form').find('#choiceLabel').val(data.label);
            $('#edit_choice_modal').modal('show');
        },
        error: function () {
            swal({
                "title": "",
                "text": "Error",
                "type": "error",
                "timer": 2000,
                "showConfirmButton": false
            });
        }
    });
}

function viewSurveyModal(surveyId) {
    $.ajax({
        url: baseUrl + "/szfive/view_survey",
        type: 'POST',
        dataType: "json",
        data: {
            surveyId: surveyId
        },
        success: function (data) {
            $('#view_survey_title').html(data.question);
            $('#blockui_survey_question').html(data.question);
            var str = '';
            $.each(data.clabels, function (key, val) {
                key++;
                str += '<code>' + key + '-' + val + '</code> ';
            });
            $('#blockui_survey_choices').html(str);
            $('#view_survey_modal').modal('show');
        },
        error: function () {
            swal({
                "title": "",
                "text": "Error",
                "type": "error",
                "timer": 2000,
                "showConfirmButton": false
            });
        }
    });
}

function viewQuestionModal(questionId) {
    $.ajax({
        url: baseUrl + "/szfive/view_question",
        type: 'POST',
        dataType: "json",
        data: {
            questionId: questionId
        },
        success: function (data) {
            $('#blockui_question').html(data.question);
            $('#view_question_modal').modal('show');
        },
        error: function () {
            swal({
                "title": "",
                "text": "Error",
                "type": "error",
                "timer": 2000,
                "showConfirmButton": false
            });
        }
    });
}

function viewChoiceModal(choiceId) {
    $.ajax({
        url: baseUrl + "/szfive/view_choice",
        type: 'POST',
        dataType: "json",
        data: {
            choiceId: choiceId
        },
        success: function (data) {
            $('#blockui_choice').html(data.label)
            $('#view_choice_modal').modal('show');
        },
        error: function () {
            swal({
                "title": "",
                "text": "Error",
                "type": "error",
                "timer": 2000,
                "showConfirmButton": false
            });
        }
    });
}

function surveyData(data) {

    getAnswerData();
    surveyAverage = data.average;

    $('#survey-avg').html('AVERAGE: &nbsp;<strong style="font-weight: 600;font-size: 20px;">' + surveyAverage + '</strong>');

    Highcharts.chart('survey_chart_container', {
            chart: {
                type: 'column',
                height: 300,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                events: {
                    drilldown: function (e) {
                        var chart = this;
                        var surveyId = $('#select_survey').find("option:selected").val();
                        if (e.point.answered != 0) {

                            var params = {
                                startDate: startDate.format('YYYY-MM-DD'),
                                endDate: endDate.format('YYYY-MM-DD'),
                                surveyId: surveyId,
                                surveyAnswer: e.point.id
                            };
                            getDrilldownAnswers(params, function (drilldown) {
                                var series = drilldown[e.point.id];

                                chart.showLoading('Please wait...');

                                setTimeout(function () {
                                    chart.hideLoading();
                                    chart.update({
                                        type: 'line'
                                    }, true);
                                    // chart.setTitle({
                                    //     text: "Team Supervisors Total Reviewed Answers"
                                    // });
                                    // chart.setSubtitle({
                                    //     text: "Reviewed Answers by SZ Team Supervisors"
                                    // });
                                    chart.addSeriesAsDrilldown(e.point, series);
                                }, 500);
                            });
                        }
                    },
                    drillup: function (e) {
                        var chart = this;
                        chart.setTitle({
                            text: "Reviewed Surveys of the SupportZebra Teams"
                        });
                        chart.setSubtitle({
                            text: "Survey Reviews by Team or Departments"
                        });
                    }
                }
            },
            title: {
                text: data.question
            },
            subtitle: {
                // text: rangeInfo + '<br><br>Average: ' + surveyAverage
            },
            colors: ['rgb(237, 77, 80)', 'rgb(247, 180, 50)', 'rgb(252, 214, 51)', 'rgb(199, 212, 90)', 'rgb(98, 212, 153)'],
            xAxis: {
                title: {
                    text: ''
                },
                type: 'category',
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    enabled: false
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                shared: true,
                useHTML: true,
                headerFormat: '<span style="color:{point.color}; -webkit-text-stroke: 1px {point.color};"><b>{point.key}</b></span><table>',
                pointFormat: '<tr><td style="color: {series.color}"></td>' +
                    '<td style="text-align: right"><b>{point.answered}</b> out of ' + data.total + ' answers</td></tr>',
                footerFormat: '</table>',
                valueDecimals: 2
            },
            plotOptions: {
                column: {
                    colorByPoint: true,
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        color: 'black',
                        style: {
                            fontSize: '12px',
                            fill: 'black',
                        }
                    },
                },
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} %'
                    }
                }
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: data.question,
                colorByPoint: true,
                data: data.data
            }],
            drilldown: {
                series: []
            }
        },
        function (chart) {

            var str = '';

            $.each(chart.series[0].data, function (j, data) {

                if (data.y == 0) {
                    data.color = '#f2f3f8';
                    style = 'background-color:' + data.color;
                    clickable = '';
                } else {
                    style = 'cursor: pointer;background-color:' + data.color;
                    clickable = 'legend-item-clickable';
                }

                var num = data.category + 1;
                str += '<div class="legend-item ' + clickable + '"><span class="symbol" style="' + style + '"><span style="margin-top: 8px;display: block;">' + num + '</span></span><span class="seriesName" id="">' + data.name + '</span></span></div>';
            });

            $('#customLegend').html(str);

            $('#customLegend .legend-item-clickable').click(function () {
                var inx = $(this).index(),
                    point = chart.series[0].data[inx];
                chartData = point;

                if (chartData.id != 0) {
                    chartData = point;
                    showAnswer();
                }
            });
        });
}

function getSurveyData(callback) {
    var date = $('#survey_datepicker .form-control').val();
    var data = [];

    surveyDetails(function (res) {
        data = res;
    });

    rangeInfo = startDate.format('MMM D') + ' - ' + endDate.format('MMM D');

    $('#date-heading').html('' + rangeInfo);
    $('#detailed-answers').html('<div class="m-widget5"></div>');
    $('#getReport').removeAttr('disabled');
    $('#getReport_datatable').removeAttr('disabled');
    $('#dropdown_report1').attr('data-dropdown-toggle', 'click');
    $('#dropdown_report2').attr('data-dropdown-toggle', 'click');

    $.ajax({
        url: baseUrl + "/szfive/get_survey_data",
        type: 'POST',
        data: {
            surveyId: data.surveyId,
            start_date: data.startDate,
            end_date: data.endDate
        },
        dataType: "json",
        success: function (response) {
            if (response.data != null) {
                $('.no-data').remove();
                $('.legend-item').remove();
                $('#total-answers').html("<strong>" + response.data.total + "</strong> people answered this survey");
                surveyData(response.data);
                $('#survey_mainchart').show(500);
            } else {
                removeChart();
                getAnswerData();
            }
        },
        error: function () {
            swal({
                "title": "",
                "text": "There is an error fetching survey data.",
                "type": "error",
                "timer": 2000,
                "showConfirmButton": false
            });
        }
    });
    callback();
}

function getAnswerData() {
    var answerData = [];
    surveyDetails(function (data) {
        answerData = data;
    });


    $('.m_datatable_answers').mDatatable('destroy');
    AnswerDatatable.init(answerData);
}

function getDrilldownAnswers(params, callback) {
    $.when(fetchPostData({
            params
        }, '/szfive/get_drilldown_survey_answers'))
        .then(function (data) {
            drilldown = jQuery.parseJSON(data);
            callback(drilldown);
        });
}

$("#add_numberOfChoices").on('change', function () {}).bind('input', function () {
    if ($(this).val() == '') {
        $('#addNewChoice').hide();
    } else {
        $('#addNewChoice').show();
    }
});

$("#select_survey").on('change', function () {
    mApp.block('#main_chart_container', {
        overlayColor: '#000000',
        state: 'primary'
    });  

    getSurveyData(function (callback) {
        mApp.unblock('#main_chart_container');
    });
});

$("#edit_numberOfChoices").on('change', function () {}).bind('input', function () {
    if ($(this).val() == '') {
        $('#editNewChoice').hide();
    } else if ($(this).val() <= numChoiceSelectedEdit) {
        $('#editNewChoice').hide();
    } else {
        $('#editNewChoice').show();
    }
});

function removeChart() {
    $('.legend-item').remove();
    $('.display-chart').html("<div class='no-data' style='text-align: center;display: inline-block;'><div class='m-demo-icon__preview'><i style='font-size: 40px;' class='flaticon-notes'></i></div><span class='m-demo-icon__class'>No result found.</span></div>");
    $('#survey-avg').html('AVERAGE: &nbsp;<strong style="font-weight: 600;font-size: 20px;">0</strong>');
    $('#survey_mainchart').hide(500);
    $('#total-answers').html("<strong>0</strong> people answered this survey");
    $('#getReport').attr('disabled', '');
    $('#getReport_datatable').attr('disabled', '');
    $('#dropdown_report1').attr('data-dropdown-toggle', 'disabled');
    $('#dropdown_report2').attr('data-dropdown-toggle', 'disabled');
}

function showAnswer() {
    data = chartData;
    answerOffset = 0;
    answerOffsetCnt = 0;
    data.offset = answerOffset;
    data.noOfItems = 5;
    loadAnswers(data, function () {
        $('#modal_survey_answer').modal('show');
    });
    $('.spec-answer-item').remove();
}

function loadAnswers(data, callback) {
    answerOffsetCnt = answerOffsetCnt + 1;
    var adata = [];
    surveyDetails(function (res) {
        adata = {
            survey_id: res.surveyId,
            start_date: res.startDate,
            end_date: res.endDate,
            answer: data.id,
            offset: data.offset,
            items: data.noOfItems,
        }
    });

    $.when(fetchPostData({
            adata
        }, '/szfive/get_detailed_answers'))
        .then(function (response) {

            var res = JSON.parse(response);
            var str = '';
            var status = {
                1: {
                    'title': 'Teary',
                    'color': '#ed4d5026',
                    'fcolor': 'rgb(237, 77, 80)'
                },
                2: {
                    'title': 'Sad',
                    'color': '#f7b43233',
                    'fcolor': '#f7b432'
                },
                3: {
                    'title': 'Average',
                    'color': '#fcd6332b',
                    'fcolor': '#fcd633'
                },
                4: {
                    'title': 'Happy',
                    'color': 'rgba(199, 212, 90, 0.47)',
                    'fcolor': 'rgba(199, 212, 90)'
                },
                5: {
                    'title': 'Amazing',
                    'color': 'rgba(98, 212, 153, 0.2)',
                    'fcolor': 'rgba(98, 212, 153)'
                },
            };

            $('.survey_ans_label').html('Employees that feel <span class="su-title-answer" style="color: ' + status[res.data[0].answer].fcolor + '"><strong>' + res.data[0].answer_label + '</strong></span>.');

            $.each(res.data, function (key, value) {
                var dateAnswered = moment(value.dateAnswered).format('MMMM D, YYYY');
                var detailedAnswer = (value.detailedAnswer ? value.detailedAnswer : '<i class="unassigned">No detailed answer found.</i>')
                var image = value.pic ? imagePath + value.pic : "img/sz.png";

                str += '<div class="m-widget3__item spec-answer-item">';

                str += '<div class="m-widget3__header comment-dwrapper">';
                str += '<div class="m-widget3__user-img">';
                str += '<img class="m-widget3__img comment-pic user-pic' + value.emp_id + '" src="' + image + '" onerror="noImage(' + value.emp_id + ')" alt="" style="border: 2px solid ' + status[value.answer].color + ';">';
                str += '</div>';
                str += '<div class="m-widget3__info comment-info">';
                str += '  <a href="'+baseUrl+ '/'+ value.link+'"><span class="m-widget3__username comment-dname">' + value.user_name + ', ' + dateAnswered + '</span></a>';
                str += '  <small style="display: block; font-size: 10px;">' + value.dep_name + ' | ' + value.pos_details + '<i></i></small>';
                str += '  <p class="m-widget3__time comment-dcontent"><i class="fa fa-quote-left"></i>' + detailedAnswer + '<i class="fa fa-quote-right"></i></p>';
                str += ' </div>';
                str += '</div>';
                str += '</div>';
            });

            $(".spec-answer-wrapper").append(str);

            var page = data.offset * answerOffsetCnt;

            if (res.count > data.noOfItems && res.count > page) {
                $("#viewMoreAnswers").show(500);
            } else {
                $("#viewMoreAnswers").hide(500);
            }
            callback();
        });
}

function noImage(id) {
    $('.user-pic' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function noImage2(id) {
    $('.answer-pic-dt' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function hasAnswerData(query) {
    employeeStatus = query.employeeStatus ? query.employeeStatus : 0;
    accountName = query.accountName ? query.accountName : 0;

    $("input[name='employeeStatus']").val(employeeStatus);
    $("input[name='accountName']").val(accountName);


    $('.m_datatable_answers').mDatatable().getData().then(function (e, i, n) {
        if (e.data.length != 0) {
            $('#dropdown_report2').attr('data-dropdown-toggle', 'click');
            $('#getReport_datatable').removeAttr('disabled');
        } else {
            $('#getReport_datatable').attr('disabled', '');
            $('#dropdown_report2').attr('data-dropdown-toggle', 'disabled');
        }
    });
}

function showUserAnswer(id) {
    $.when(fetchPostData({
            id
        }, '/szfive/get_answer_details'))
        .then(function (data) {
            data = jQuery.parseJSON(data);
            var dateCreated = moment(data.dateCreated).format('MMMM D, YYYY');
            var surveyDate = moment(data.sched_date).format('MMMM D, YYYY');
            if (data.isReviewed == 0) {
                var status = 'Unreviewed';
                $('tr#dateReviewed').hide();
            } else {
                var status = 'Reviewed';
                var dateReviewed = moment(data.dateUpdated).format('MMMM D, YYYY');
                $('tr#dateReviewed').show();
                $('td#date_reviewed').html(dateReviewed);
            }
            header = '<div class="m-widget3__header" style="display: table;">';
            header += '<div class="m-widget3__user-img">';
            header += ' <img class="m-widget3__img user-pic' + data.emp_id + '" src="' + imagePath + data.pic + '" onerror="noImage(' + data.emp_id + ')"alt="" style="width: 3.2rem;border-radius: 50%;">';
            header += ' </div>';
            header += ' <div class="m-widget3__info" style="display:table-cell;width:100%; padding-left:1rem;font-size:1rem;vertical-align:middle;">';
            header += ' <span class="m-widget3__username" style="font-size: 15px;font-weight: 500;">' + data.fname + ' ' + data.lname + ' feels ' + data.answer_label + '</span>';
            header += '<br><span class="m-widget3__time" style="font-size: .85rem;">' + data.pos_details + '</span>';
            header += '</div> </div>';
            $('#sa_header').html(header);
            $('td#date_answered').html(dateCreated);
            $('td#account').html(data.acc_name);
            $('td#answer').html(data.answer + '/' + data.numberOfChoices);
            $('td#date').html(surveyDate);
            $('td#status').html(status);
            $('td#supervisor').html(data.supervisor);
            $('td#comments').html(data.detailedAnswer);
            $('td#link').html('<a href="' + baseUrl + '/' + data.link + '" target="_blank"><i class="fa fa-eye"></i> View</a>');

            setTimeout(() => {
                $('#survey_user_answer').modal('show');
            }, 500);
        });
}