var reassignmentDatatable = function () {
    var reassignment = function (approverIdVal, changesVal, accountVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/employee/get_custom_approval_datatable",
                        params: {
                            query: {
                                approverId: approverIdVal,
                                changeType: changesVal,
                                accountId: accountVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            rows: {
                afterTemplate: function (row, data, index) {},
            },
            // columns definition
            columns: [{
                field: "approverEmpId",
                title: "Approver",
                width: 150,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.approverFname) + " " + toTitleCase(row.approverLname);
                    return html;
                },
            },{
                field: "acc_name",
                title: "Account",
                width: 120,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.acc_name);
                    return html;
                },
            }, {
                field: "level",
                title: "Level",
                width: 50,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.level;
                    return html;
                },
            }, {
                field: "changeType",
                title: "Type",
                width: 60,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.changeType);
                    return html;
                },
            }, {
                field: "assignEmpId",
                title: "Reassign To",
                width: 150,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = '-';
                    if(row.assignEmpId !== null){
                        html = toTitleCase(row.assignFname) + " " + toTitleCase(row.assignLname);
                    }
                    return html;
                },
            }, {
                field: 'action',
                width: 50,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air removeCustomAppr" style="width: 22px !important;height: 22px !important;" data-fullname="' + toTitleCase(row.approverFname) + " " + toTitleCase(row.approverLname) + '" data-changetype="' + row.changeType + '" data-recid="' + row.customApprovalId + '">' +
                        '<i class="fa fa-remove" style="font-size: 0.95rem !important;"></i>' +
                        '</a>';
                    return html;
                },
            }],
        };
        var datatable = $('#reassignmentDatatable').mDatatable(options);
    };
    return {
        init: function (approverIdVal, changesVal, accountVal) {
            reassignment(approverIdVal, changesVal, accountVal);
        }
    };
}();

function addApprovalChanges(typeVal, empToVal, callback) {
    $.when(fetchPostData({
        approver_emp_id: $('#assignApprover').val(),
        acc_id: $('#assignAccounts').val(),
        level: $('#assignLevel').val(),
        emp_id_to: empToVal,
        type: typeVal
    }, '/employee/add_custom_approval')).then(function (assigned) {
        var assignedObj = $.parseJSON(assigned.trim());
        callback(assignedObj);
        approverSelect();
    });
}


function initReassignmentDatatable(initType) {
    $('#reassignmentDatatable').mDatatable(initType);
    reassignmentDatatable.init($('#empApprovers').val(), $('#changeType').val(), $('#customAccount').val());
}

$('#customApprovalAssignment').on('click', function() {
    initReassignmentDatatable('destroy');
    approverSelect();
    initSelect2Field('#changeType', "Type");
    accountSelect();
})

function alertReassignmentError(reassignType, submitBtn,recordObj){
    $(submitBtn).prop('disabled', true);
    // console.log(recordObj)
    if (recordObj.changeType == reassignType){
        if (recordObj.changeType == "change"){
            $("#errorAlertAssignment").html("Already assigned to " + recordObj.fname + " " + recordObj.lname);
        }else{
            $("#errorAlertAssignment").html("Already added as an exemption");
        }
    }else{
        if (recordObj.changeType == "change") {
            $("#errorAlertAssignment").html("Cannot add exemption because the approval is reassigned to " + recordObj.fname + " " + recordObj.lname);
        }else{
            $("#errorAlertAssignment").html("Cannot reassign because the approver is already exempted in the approval process");
        }
    }

    $("#errorAlertAssignment").fadeIn();
}

function initSelect2Field(element, title){
    $(element).select2({
        placeholder: title,
        width: '100%'
    });
    $(".select2-container").tooltip({
        title: title,
        placement: "top"
    });
    $(".select2-selection span").attr('title', '');
}

function approverSelect(){
    $.when(fetchGetData('/employee/get_approvers_name')).then(function (approver) {
       let approverObj = $.parseJSON(approver.trim());
       let options = "<option value='0'>All</option>"
       console.log(approverObj);
       $.each(approverObj, function(index, value){
           options += "<option value='"+value.emp_id+"'>"+toTitleCase(value.lname+" "+value.fname)+"</option>";
       });
       $('#empApprovers').html(options);
       //  callback(approverObj);
    });
    initSelect2Field("#empApprovers", "Approver Name");
}

function accountSelect(){
    $.when(fetchGetData('/employee/get_all_accounts')).then(function (accounts) {
       let accountsObj = $.parseJSON(accounts.trim());
       let options = "<option value='0'>All</option>"
       console.log(accountsObj.accounts);
       $.each(accountsObj.accounts, function (index, value) {
           options += "<option value='"+value.acc_id+"'>"+toTitleCase(value.acc_name)+"</option>";
       });
       $('#customAccount').html(options);
    });
    initSelect2Field('#customAccount', "Account");
}

function removeCustomApproval(customApprIdVal, typeVal) {
    $.when(fetchPostData({
        customApprovalId: customApprIdVal,
        changeType: typeVal
    }, '/employee/remove_custom_approval')).then(function (removeStat) {
        var removeStatObj = $.parseJSON(removeStat.trim());
        swal("Sucess!", "Successfully added in the exemption list", "success")
        $('#assignModal').modal('hide');
        if (typeVal == 'change'){
            if (parseInt(removeStatObj.custom_approval) == 1 && parseInt(removeStatObj.custom_approval_change) == 1) {
                swal("Sucess!", "Successfully deleted custom approval", "success");
            }else{
                swal("Error!", "Something went wrong while deleting the record. Please report this error immediately to your System Administrator", "error");
            }
        }else{
            if (parseInt(removeStatObj.custom_approval)) {
                swal("Sucess!", "Successfully deleted custom approval", "success");
            } else {
                swal("Error!", "Something went wrong while deleting the record. Please report this error immediately to your System Administrator", "error");
            }
        }
        approverSelect();
        initReassignmentDatatable('reload');
    });
}

function confirmRemoval(customApprIdVal, typeVal) {
    swal({
        title: 'Are you sure you want to remove the custom approval?',
        text: "You will not be able to revert back the current record.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete it!',
        cancelButtonText: 'No, Don\'t Delete',
        reverseButtons: true,
        // width: '500px'
    }).then(function (result) {
        if (result.value) {
            removeCustomApproval(customApprIdVal, typeVal)
        } else if (result.dismiss === 'cancel') {
            // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
        }
    });   
}

$('#assignModal').on('click', '#btn-submitAssignExemption', function () {
    let emptyField = [];
    if ($('#assignApprover').val() == "--" || $('#assignApprover').val() == null) {
        emptyField.push("Approver");
    }
    if ($('#assignAccounts').val() == "--" || $('#assignAccounts').val() == null) {
        emptyField.push("Account");
    }
    if (emptyField.length > 0) {
        $("#errorAlertAssignment").html("<b>Required Field:</b> " + emptyField.join(", "));
        $("#errorAlertAssignment").fadeIn();
    } else {
        $("#errorAlertAssignment").fadeOut();
        addApprovalChanges("exempt", 0, function (assignedObj) {
            if (parseInt(assignedObj.exist)) {
                console.log(assignedObj.record);
                alertReassignmentError('exempt', this ,assignedObj.record);
            } else {
                initReassignmentDatatable('reload');
                swal("Sucess!", "Successfully added in the exemption list", "success")
                $('#assignModal').modal('hide');
            }
        });
    }
});

$('#assignModal').on('click', '#btn-submitAssignChanges', function(){
    let emptyField = [];
    if ($('#assignApprover').val() == "--" || $('#assignApprover').val() == null) {
        emptyField.push("Approver");
    }
    if ($('#assignAccounts').val() == "--" || $('#assignAccounts').val() == null) {
        emptyField.push("Account");
    }
    if ($('#assignEmployee').val() == "--" || $('#assignEmployee').val() == null) {
        emptyField.push("Assign to Employee");
    }
    if(emptyField.length > 0){
        console.log(emptyField);
        $("#errorAlertAssignment").html("<b>Required Field:</b> " + emptyField.join(", "));
        $("#errorAlertAssignment").fadeIn();
    }else{
        $("#errorAlertAssignment").fadeOut();
        console.log("Check if exist");
        addApprovalChanges("change", $('#assignEmployee').val(), function (assignedObj) {
            console.log(assignedObj);
            if (parseInt(assignedObj.exist)){
                alertReassignmentError('change', this ,assignedObj.record);
            }else{
                initReassignmentDatatable('reload');
                $('#assignModal').modal('hide');
                swal("Sucess!", "You have successfully assigned the approval privilege","success");
            }
        });
    }

});

$('#assignApprover, #assignAccounts, #assignLevel, #assignEmployee').on('change', function(){
    $("#errorAlertAssignment").fadeOut();
    $('#btn-submitAssignExemption').prop('disabled', false);
    $('#btn-submitAssignChanges').prop('disabled', false);
});

$('#empApprovers, #changeType, #customAccount').on('change', function(){
    // approverSelect();
    initReassignmentDatatable('destroy');
});

$('#tab-requestApproval').on('click', '.removeCustomAppr', function () {
    confirmRemoval($(this).data('recid'), $(this).data('changetype'))
});

$(function(){
    $("#assignLevel").TouchSpin({
        buttondown_class: 'btn btn-secondary',
        buttonup_class: 'btn btn-secondary',
        min: 1,
        max: 2,
        stepinterval: 50,
        maxboostedstep: 10000000,
        prefix: 'Level'
    });
})