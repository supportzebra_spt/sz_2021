var protocol = window.location.protocol;
var pathName = window.location.pathname;
var host = window.location.host;
var baseUrl = '';
var match = host.match(/\d{1,3}/g);
var ipHost = (match != null ? match : 0);

if (host == 'localhost' || ipHost.length == 4) {
    var pathArray = pathName.split('/');
    baseUrl = protocol + '//' + host + '/' + pathArray[1];
} else {
    baseUrl = protocol + '//' + host;
}

var imagePath = baseUrl + '/';

function errorLog(xhr, textStatus, errorThrown) {
    console.log(xhr);
    console.log(xhr.status);
    console.log(xhr.responseText);
    console.log(textStatus);
    console.log(errorThrown);
}

// GENERAL AJAX FUNCTIONS ---------------------------------------------------------------------------------------------------

// GET AJAX ---------------
var fetchGetData = function (dataURL) {
    var url = baseUrl + dataURL;
    return $.get(url)
        .done(function (res) {})
        .fail(function (xhr, textStatus, errorThrown) {
            errorLog(xhr, textStatus, errorThrown);
        });
}

// POST AJAX ---------------
var fetchPostData = function (query, dataURL) {
    var url = baseUrl + dataURL;
    return $.post(url, query)
        .done(function (res) {})
        .fail(function (xhr, textStatus, errorThrown) {
            errorLog(xhr, textStatus, errorThrown);
        });
}


// SAMPLE FUNCTION

function getShift() {
    $.when(
            // call the general function (fetchPostData) and provide the parameters
            fetchPostData({
                    // DATA
                    scheduleDate: $('.dateRendered').val()
                },
                //  LINK or URL
                'additional_hour_request/get_shift')
        )
        //  then means after the functions inside when are implemented 
        .then(function (shift) {
            // codes inside "THEN " will only be implemented after the ajax call (fetchPostDate) is done
            console.log(shift);
        });
}

// PADLEFT FOR LEADING ZEROS
String.prototype.padLeft = function padLeft(length, leadingChar) {
    if (leadingChar === undefined) leadingChar = "0";
    return this.length < length ? (leadingChar + this).padLeft(length, leadingChar) : this;
};

function setDate(id, date) {
    $(id).datepicker('update', moment(date, 'MM/DD/Y').format('MM/DD/Y'));
}

function getCurrentDateTime(callback) {
    $.when(fetchGetData('/additional_hour/get_server_date_time')).then(function (currentDateTime) {
        var dateTime = $.parseJSON(currentDateTime.trim());
        callback(dateTime.date);
    });
}

function formReset(form) {
    $(form).formValidation('resetForm', true);
}

function noImageFound(element) {
    $(element).attr('src', baseUrl + '/assets/images/img/sz.png');
}
function noImage(element) {
    $(element).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function humanTimeFormat(hours, callback) {
    var totalHours = parseInt(hours);
    var totalMinutes = Math.round(((hours - totalHours) * 60).toFixed(2));
    var totalTime = '';
    if (totalMinutes == 60) {
        totalMinutes = 0;
        totalHours++;
    }
    var totalTime = '';
    if ((totalHours == 0) && (totalMinutes == 1)) {
        totalTime = totalMinutes + " min";
    } else if ((totalHours == 0) && (totalMinutes != 1)) {
        totalTime = totalMinutes + " mins";
    } else if ((totalMinutes == 0) && (totalHours == 1)) {
        totalTime = totalHours + " hr";
    } else if ((totalMinutes == 0) && (totalHours != 1)) {
        totalTime = totalHours + " hrs";
    } else if ((totalHours >= 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " min";
    } else if ((totalHours == 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " mins";
    } else if ((totalHours == 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " min";
    } else if ((totalHours >= 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " mins";
    }
    callback(totalTime);
}