var shiftStartToShow = "";
var shiftStartToCompute = "";
var shiftEndToShow = "";
var shiftEndToCompute = "";
var dtrInToShow = "";
var dtrInToCompute = "";
var dtrOutToShow = "";
var dtrOutToCompute = "";
var actualDtrToShow = "";
var actualDtrToCompute = "";

var dateToComputeAppr = "";
var dateToShowAppr = "";

// DATATABLE
function getStatusCount(callback) {
    var dateRange = $('#dateRangeApprovalRecord .form-control').val().split('-');
     $.when(fetchPostData({
         from: moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'),
         to: moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD')
     }, '/additional_hour/get_approval_status_count')).then(function (countStat) {
         var countStatObj = $.parseJSON(countStat.trim());
         callback(countStatObj);
         $('#recordCount').text(countStatObj.record);
         $('#approvedCount').text(countStatObj.approved);
         $('#disapprovedCount').text(countStatObj.disapproved);
         $('#dueCount').text(countStatObj.missed);
         if (parseInt(countStatObj.missed)>0) {
             $('#approvalAhrRecord').find('.missed').addClass('m-animate-blink');
         }else{
             $('#approvalAhrRecord').find('.missed').removeClass('m-animate-blink');
         }
     });
}
function hide_quick_approval_buttons(){
    $('.quick_approval_btns').fadeOut();
}
// REQUEST DATATABLE
var approvalPendingAhrDatatable = function () {
    var approvalPendingAhr = function (accIdVal, userId, levelVal, searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour/get_approval_pending_ahr_request",
                        params: {
                            query: {
                                acc_id: accIdVal,
                                user: userId,
                                level: levelVal,
                                approvalPendingAhrSearch: searchVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#approvalPendingAhrSearch'),
            },
            rows: {
                beforeTemplate: function (row, data, index) {
                    // hide_quick_approval_buttons();
                },
            },
            // columns definition
            columns: [{
                field: "requestId",
                title: '<label class="m-checkbox"><input id="all_check" type="checkbox"><span style="margin-left: 2px;"></span></label>',
                width: 35,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var ahrId = row.additionalHourRequestId.padLeft(8);
                    var html = '<div style="margin-top:1px"><label class="m-checkbox"><input class="rowcheck" data-ahrid="'+row.additionalHourRequestId+'" data-ahrapprid="'+row.additionalHourRequestApproval_ID+'" type="checkbox"><span></span></label></div>';
                    return html;
                }
            },{
                field: "additionalHourRequestId",
                title: "AHR ID",
                width: 70,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var ahrId = row.additionalHourRequestId.padLeft(8);
                    var html = "<tr class='row'><td class = 'col-md-1'>" + ahrId + "</td>";
                    return html;
                }
            }, {
                field: 'requestor',
                width: 240,
                title: 'Requested By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = imagePath+""+row.pic;
                    var elementId = "#requestApproval" + row.userId +"";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'sched_date',
                width: 80,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'deadline',
                width: 150,
                title: 'Deadline',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.deadline, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</span>";
                    return html;
                },
            }, {
                field: 'ahrTypeDescription',
                width: 60,
                title: 'Type',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
            }, {
                field: 'totalOtHours',
                width: 100,
                title: 'OT Hours',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    humanTimeFormat(row.totalOtHours, function (formattedHours) {
                        // $('#additionalVal').text(formattedBct);
                        html = "<span class='text-center'>" + formattedHours + "</span>";
                    });
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="Approve details"><i class="la la-edit " onclick="initAhrRequest(' + row.additionalHourRequestId + ',' + row.additionalHourRequestApproval_ID + ',' + row.additionalHourTypeId + ',\'' + row.requestType + '\')"></i></a>';
                    // '<a class="btn btn-sm btn-primary text-light m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air mx-1 approvalbtn" title="View Details" data-approval="1" data-ahrid="'+row.additionalHourRequestId +'"><i class="la la-thumbs-up"></i></a>'+
                    // '<a class="btn btn-sm btn-danger text-light m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air approvalbtn" title="View Details" data-approval="0" data-ahrid="'+row.additionalHourRequestId +'"><i class="la la-thumbs-down"></i></a>';                    
                    return html;
                },
            }],
        };
        options.search = {
            input: $('#approvalPendingAhrSearch'),
        };
        var datatable = $('#approvalAhrPendingDatatable').mDatatable(options);
    };
    return {
        init: function (accIdVal, userId, levelVal, searchVal) {
            approvalPendingAhr(accIdVal, userId, levelVal, searchVal);
        }
    };
}();

// RECORDS DATATABLE
var approvalRecordAhrDatatable = function () {
    var approvalRecordAhr = function (statusVal, userId, searchVal, dateFrom, dateTo) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour/get_approval_ahr_records",
                        params: {
                            query: {
                                status: statusVal,
                                user: userId,
                                approvalRecordSearch: searchVal,
                                rangeStart: dateFrom,
                                rangeEnd: dateTo
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            getStatusCount(function (countStat) {});

                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },

            search: {
                input: $('#approvalRecordSearch'),
            },
            // columns definition
            columns: [{
                field: "additionalHourRequestId",
                title: "AHR ID",
                width: 70,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var ahrId = row.additionalHourRequestId.padLeft(8);
                    var html = "<tr class='row'><td class = 'col-md-1'>" + ahrId + "</td>";
                    return html;
                }
            }, {
                field: 'requestor',
                width: 260,
                title: 'Requested By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = imagePath + "" + row.pic;
                    var elementId = "#recordApproval" + row.userId + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'sched_date',
                width: 120,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'ahrTypeDescription',
                width: 60,
                title: 'Type',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
            }, {
                field: 'status',
                width: 80,
                title: 'Decision',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.decisionStat) == 12) {
                        var badgeColor = 'btn-warning';
                        var dotAlert = "<span class='m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink mr-1' style='min-height: 7px;min-width: 7px;'></span>";
                    } else if (parseInt(row.decisionStat) == 5) {
                        var badgeColor = 'btn-success';
                        var dotAlert ="";
                    } else if (parseInt(row.decisionStat) == 6) {
                        var badgeColor = 'btn-danger';
                        var dotAlert ="";
                    }
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + "' onclick='approverInfo(" + row.additionalHourRequestId + ")'>" + dotAlert + " " + row.statusDescription + "</span>";
                     return html;

                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View Details"><i class="fa fa-search" onclick="initRecordInfo(' + row.additionalHourRequestId +',' + row.additionalHourTypeId + ', \'' + row.requestType + '\')"></i></a>';
                    return html;
                },
            }],
        };
        options.search = {
            input: $('#approvalRecordSearch'),
        };
        var datatable = $('#approvalRecordAhrDatatable').mDatatable(options);
    };
    return {
        init: function (statusVal, userId, searchVal, dateFrom, dateTo) {
            approvalRecordAhr(statusVal, userId, searchVal, dateFrom, dateTo);
        }
    };

}();

function getRequestorAccountShifts(empId, callback) {
    $.when(fetchPostData({
        emp_id: empId
    }, '/additional_hour/get_requestor_account_shifts')).then(function (accountShifts) {
        var accShifts = $.parseJSON(accountShifts.trim());
        callback(accShifts);
    });
}

function getRequestorShiftUnrendered(userId, date, callback) {
    $.when(fetchPostData({
        scheduleDate: date,
        uid: userId
    }, '/additional_hour/get_requestor_shift')).then(function (shift) {
        var shift = jQuery.parseJSON(shift.trim());
        var optionsAsString = "";
        var more = 0;
        if (shift.length > 1) {
            more = 1;
            optionsAsString += "<option value ='0'></option>";
        }
        $.each(shift, function (index, value) {
            optionsAsString += "<option value='" + value.schedId + "' shiftType='" + value.type + "'>" + value.time_start + " - " + value.time_end + "</option>";
            $('#shiftunrendered').data('empid', value.empId);
            $('#shiftunrendered').data('accountid', value.accountId);
        });
        $('#shiftunrendered').html(optionsAsString);
        callback(more);
    });
}

function checkIfTimeInRangeAppr(shiftDated, ahrType, bct, actualDateTime, callback) {
    var datedShift = shiftDated; 
    var bctMin = bct * 60;
    var shiftStartStr = moment(datedShift.start, "YYYY-MM-DD kk:mm").subtract(bctMin.toPrecision(2), 'minutes').valueOf();
    var shiftEndStr = moment(datedShift.end, "YYYY-MM-DD kk:mm").valueOf();
    if (moment(datedShift.start, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
        shiftStartStr = moment(datedShift.start, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
    }
    if (moment(datedShift.end, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
        shiftEndStr = moment(datedShift.end, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
    }
    var dtrLogValStr = moment(moment($('#renderedDtrLogVal').text(), 'MMM DD, Y hh:mm A').format("YYYY-MM-DD kk:mm")).valueOf();
    let inputTime = moment(actualDateTime + " " + $('#renderedActualDtrTime').val(), "YYYY-MM-DD hh:mm A").format('YYYY-MM-DD kk:mm');

    var inputTimeStr = moment(inputTime, 'YYYY-MM-DD kk:mm').valueOf();
    if (moment(actualDateTime + " " + $('#renderedActualDtrTime').val(), "YYYY-MM-DD hh:mm A").format('kk:mm') == "24:00") {
        inputTimeStr = moment(inputTime, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
    }

    var exceed = 0;
    if (parseInt(ahrType) == 1) {
        // AFTER
        if (inputTimeStr > dtrLogValStr) {
            exceed = 1;
        } else if ((inputTimeStr == shiftEndStr) && (inputTimeStr < dtrLogValStr)) {
            exceed = 1;
        }
    } else if (parseInt(ahrType) == 2) {
        //PRE
        if ((inputTimeStr > dtrLogValStr) && (inputTimeStr == shiftStartStr)) {
            exceed = 1;
        } else if (inputTimeStr < dtrLogValStr) {
            exceed = 1;
        }
    }
    callback(exceed);
}

function computeAhrTimeRendered(datedShift, wordIn, wordOut, computeDate, actualDtrTime, ahrType, bctVal, breakVal, callback) {
    var shiftStartStr = moment(datedShift.start, "Y-MM-DD kk:mm").valueOf();
    var shiftEndStr = moment(datedShift.end, "Y-MM-DD kk:mm").valueOf();
    if (moment(datedShift.start, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
        shiftStartStr = moment(datedShift.start, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
    }
    if (moment(datedShift.end, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
        shiftEndStr = moment(datedShift.end, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
    }
    if (ahrType != 3) {
        var inputTime = moment($('#renderedActualDtrDate').val() + " " + $('#renderedActualDtrTime').val(), "MM/DD/Y hh:mm A").format('Y-MM-DD kk:mm');
        var inputTimeStr = moment(inputTime, "Y-MM-DD kk:mm").valueOf();
        if (moment(inputTime, "Y-MM-DD kk:mm A").format('kk:mm') == "24:00") {
            inputTimeStr = moment(inputTime, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
        }
        if (parseInt(ahrType) == 1) {
            var start = shiftEndStr;
            var end = inputTimeStr;
        } else if (parseInt(ahrType) == 2) {
            var start = inputTimeStr;
            var end = shiftStartStr;
        }
    } else {
        var dtrInStr = moment(wordIn, "Y-MM-DD kk:mm").valueOf();
        var dtrOutStr = moment(wordOut, "Y-MM-DD kk:mm").valueOf();
        if (moment(wordIn, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
            dtrInStr = moment(wordIn, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
        }
        if (moment(wordOut, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
            dtrOutStr = moment(wordOut, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
        }
        if ((dtrInStr < shiftStartStr) || (dtrInStr == shiftStartStr)) { // word start
            start = shiftStartStr;
        } else {
            start = dtrInStr;
        }
        if ((dtrOutStr > shiftEndStr) || (dtrOutStr == shiftEndStr)) { // word end
            end = shiftEndStr;
        } else {
            end = dtrOutStr;
        }
    }
    var seconds = end - start;
    var hours = seconds / 3600000;
    if (parseInt(ahrType) == 2) {
        if (bctVal == undefined) {
            var hours = hours;
        }else{
            var hours = hours - parseFloat(bctVal);
        }
    } else if (parseInt(ahrType) == 3) {
        if (breakVal == undefined) {
            var hours = hours;
        } else {
            var breaks = parseFloat(breakVal) / 60;
            var hours = hours - breaks;
        }
    } else {
        var hours = hours;
    }
    if (hours > 0) {
        humanTimeFormat(hours, function (humanTimeFormat) {
            var renderedOt = {
                otHours: hours.toFixed(2),
                formatted: humanTimeFormat
            };
            callback(renderedOt);
        })
    } else {
        var hours = 0;
        var renderedOt = {
            otHours: hours,
            formatted: '00 hr, 00 min'
        };
        callback(renderedOt);
    }
}

function computeAhrTimeUnrendered(callback) {
    if (moment($('#startTimeAppr').val(), 'hh:mm A').format('HH:mm') == '00:00'){
        startDateToCompute = moment($('#startDateAppr').val(), 'MM/DD/YYYY').add(1, 'days').format('MM/DD/YYYY');
    }else{
        startDateToCompute = $('#endDateAppr').val();
    }
    if (moment($('#endTimeAppr').val(), 'hh:mm A').format('HH:mm') == '00:00'){
        endDateToCompute = moment($('#endDateAppr').val(), 'MM/DD/YYYY').add(1, 'days').format('MM/DD/YYYY');
    }else{
        endDateToCompute = $('#endDateAppr').val();
    }

    var start = moment(startDateToCompute + " " + $('#startTimeAppr').val(), 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY hh:mm A');
    var end = moment(endDateToCompute + " " + $('#endTimeAppr').val(), 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY hh:mm A');
    if ($('#shiftAppr').data('ahrtype') == 1) {
        startStr = moment(start, 'MM/DD/YYYY hh:mm A').valueOf();
        endStr = moment(end, 'MM/DD/YYYY hh:mm A').valueOf();
    } else if ($('#shiftAppr').data('ahrtype') == 2) {
        startStr = moment(start, 'MM/DD/YYYY hh:mm A').valueOf();
        endStr = moment(end, 'MM/DD/YYYY hh:mm A').valueOf();
    } else {
        startStr = moment(shiftStartToCompute, 'MM/DD/YYYY h:mm A').valueOf();
        endStr = moment(shiftEndToCompute, 'MM/DD/YYYY h:mm A').valueOf();
    }
    var hours = (endStr - startStr) / 3600000;
    if (hours <= 0) {
        hours = 0;
    } else {
        if (parseInt($('#shiftAppr').data('ahrtype')) == 3) {
            var breaks = parseFloat($('#additionalDtrDetailsVal').data('break')) / 60;
            var hours = hours - breaks;
        } else {
            var hours = hours;
        }
    }
    humanTimeFormat(hours, function (humanTimeFormat) {
        var renderedOt = {
            otHours: hours.toFixed(2),
            formatted: humanTimeFormat
        };
        callback(renderedOt);
    });
}

function dateAdjustments(dateTime, callback) {
    var time = moment(dateTime, 'Y-MM-DD HH:mm').format('HH:mm');
    if (time == '00:00') {
        var dateToCompute = moment(dateTime, 'Y-MM-DD HH:mm').format('Y-MM-DD kk:mm');
        var dateTimeToShow = moment(dateTime, 'Y-MM-DD HH:mm').format('Y-MM-DD kk:mm');
    } else {
        var dateTimeToShow = moment(dateTime, 'Y-MM-DD HH:mm').format('Y-MM-DD kk:mm');
        var dateToCompute = moment(dateTime, 'Y-MM-DD HH:mm').format('Y-MM-DD kk:mm');
    }
    var dateAdjustment = {
        show: dateTimeToShow,
        compute: dateToCompute
    }
    callback(dateAdjustment);
}

function assignTimeDateAppr(dateSched, shift, callback) {
    var shiftVal = shift.split('-');
    var shiftStart = moment(dateSched + " " + shiftVal[0], "MM/DD/YYYY hh:mm:ss A").valueOf();
    var shiftEnd = moment(dateSched + " " + shiftVal[1], "MM/DD/YYYY hh:mm:ss A").valueOf();
    if (shiftStart > shiftEnd) {
        shiftEnd = moment(shiftEnd).add(1, 'days').valueOf();
    }
    shiftStartToCompute = moment(shiftStart).format('MM/DD/YYYY h:mm A');
    shiftEndToCompute = moment(shiftEnd).format('MM/DD/YYYY h:mm A');
    if (moment(shiftStart).format('HH:mm') == '00:00') {
        var endDateTime = moment(shiftEnd).add(1, 'days').valueOf();
    } else if (moment(shiftEnd).format('HH:mm') == '00:00') {
        var endDateTime = moment(shiftEnd).subtract(1, 'days').valueOf();
    } else {
        var endDateTime = shiftEnd;
    }
    var datedShift = {
        start: moment(shiftStart).format('Y-MM-DD HH:mm'),
        end: moment(endDateTime).format('Y-MM-DD HH:mm')
    }
    callback(datedShift)
}

function getBctUnrendered(emp_id, callback){
    $.when(fetchPostData({
        empId: emp_id
    }, '/additional_hour/get_bct_unrendered')).then(function (bct) {
        var bctObj = $.parseJSON(bct.trim());
        callback(bctObj);
    });
}

function getOtDetailsApproval(ahrDetails) {
    $.when(fetchPostData({
        ahrType: ahrDetails.additionalHourTypeId,
        scheduleDate: ahrDetails.sched_date,
        schedId: ahrDetails.schedId,
        shiftStart: ahrDetails.time_start,
        shiftEnd: ahrDetails.time_end,
        empId: ahrDetails.emp_id,
        accountId: ahrDetails.acc_id
    }, '/additional_hour/get_ot_details')).then(function (dtrDetails) {
        var dtrDetailsVal = $.parseJSON(dtrDetails.trim());
        $('#shiftAppr').data('dateshift', dtrDetailsVal.datedShifts);
        $('#shiftAppr').data('ahrtype', ahrDetails.additionalHourTypeId);
        var datedShift = dtrDetailsVal.datedShifts;
        var wordIn = '';
        var wordOut = '';
        var computeDate = '';
        var actualDtrTime = '';
        var ahrType = ahrDetails.additionalHourTypeId;
        var bctVal = 0;
        var breakVal = 0;
        let dtrIn = "";
        let dtrOut = "";
        //ADJUSTMENT FOR DTR
        // $.each(dtrDetailsVal.dtrLog, function (index, value) {
        //     dateAdjustments(moment(value.log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
        //         if (value.entry == 'I') {
        //             dtrInToShow = dateAdjustment.show;
        //             dtrInToCompute = dateAdjustment.compute;

        //         } else if (value.entry == 'O') {
        //             dtrOutToShow = dateAdjustment.show;
        //             dtrOutToCompute = dateAdjustment.compute;
        //         }
        //     });
        // })
          $.each(dtrDetailsVal.dtrLog, function (index, value) {
              if (value.entry === "I") {
                  dtrIn = value.log;
                  if (moment(value.log, 'Y-MM-DD HH:mm:ss').format('HH:mm') == '00:00') {
                      dtrIn = moment(value.log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD') + '24:00:00';
                  }
              } else if (value.entry === "O") {
                  dtrOut = value.log;
                  if (moment(value.log, 'Y-MM-DD HH:mm:ss').format('HH:mm') == '00:00') {
                      dtrOut = moment(value.log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD') + ' 24:00:00';
                  }
              }
          })


        //SET DTR DETAILS OF APPROVAL FORM
        if (ahrDetails.requestType == 'rendered') {
            $('#renderedAhrApproval').show();
            $('#unrenderedAhrApproval').hide();
            $('#shiftWord').hide();
            $('#shiftInfo').show();
            if (parseInt(ahrDetails.additionalHourTypeId) != 3) {
                // if (parseInt(typeId) != 3) {
                $('#renderedPreAfterApproval').show();
                $('#renderedWordApproval').hide();
                if (parseInt(ahrDetails.additionalHourTypeId) == 1) {
                    $('#renderBctAppr').hide();
                    // dateAdjustments(moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
                        $('#renderedDtrLogLabel').text('CLOCK OUT');
                        $('#renderedDtrLogVal').text(moment(dtrOut, 'Y-MM-D kk:mm:ss').format('MMM D, Y h:mm A'));
                        // dateToComputeAppr = moment(dtrOutToCompute, 'Y-MM-D kk:mm').format('Y-MM-DD');
                        // dateToShowAppr = moment(dtrOutToCompute, 'Y-MM-D kk:mm').format('Y-MM-DD');
                        $('#renderedActualDtrLabel').text('ACTUAL OUT:');
                        $('#renderedActualDtrDate').val(moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
                        $('#renderedActualDtrDate').data('origrendereddate', moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
                        setDate("#renderedActualDtrDate", moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
                        $('#renderedActualDtrTime').val(moment(ahrDetails.otEndTime, 'kk:mm').format('h:mm A'));
                        $('#renderedActualDtrTime').data('origrenderedtime', moment(ahrDetails.otEndTime, 'kk:mm').format('h:mm A'));
                        initializeActualDtrTimePicker('#renderedActualDtrTime', moment(ahrDetails.otEndTime, 'kk:mm').format('h:mm A'))
                        computeDate = ahrDetails.otEndDate;
                        actualDtrTime = moment(ahrDetails.otEndTime, 'kk:mm').format('kk:mm A');
                    // })
                } else if (parseInt(ahrDetails.additionalHourTypeId) == 2) {
                    // dateAdjustments(moment(ahrDetails.otStartDate + " " + ahrDetails.otStartTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
                        if (parseInt(dtrDetailsVal.account_details.beforeCallingTime) > 0) {
                            $('#renderBctAppr').show();
                            humanTimeFormat(dtrDetailsVal.actual_bct, function (formattedBct) {
                                $('#renderedBctVal').text(formattedBct);
                                $('#renderedBctVal').data('bct', dtrDetailsVal.actual_bct);
                            })
                        } else {
                            $('#renderBctAppr').hide();
                        }
                        $('#renderedDtrLogLabel').text('CLOCK IN');
                        $('#renderedDtrLogVal').text(moment(dtrIn, 'Y-MM-D kk:mm:ss').format('MMM D, Y h:mm A'));
                        // dateToComputeAppr = moment(dtrInToCompute, 'Y-MM-D HH:mm').format('Y-MM-DD');
                        // dateToShowAppr = moment(dtrInToCompute, 'Y-MM-D HH:mm').format('Y-MM-DD');
                        $('#renderedActualDtrLabel').text('ACTUAL IN:');
                        $('#renderedActualDtrDate').val(moment(ahrDetails.otStartDate + " " + ahrDetails.otStartTime, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
                        $('#renderedActualDtrDate').data('origrendereddate', moment(ahrDetails.otStartDate + " " + ahrDetails.otStartTime, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
                        setDate("#renderedActualDtrDate", moment(ahrDetails.otStartDate + " " + ahrDetails.otStartTime, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
                        $('#renderedActualDtrTime').val(moment(ahrDetails.otStartTime, 'kk:mm').format('h:mm A'));
                        $('#renderedActualDtrTime').data('origrenderedtime', moment(ahrDetails.otStartTime, 'kk:mm').format('h:mm A'));
                        initializeActualDtrTimePicker('#renderedActualDtrTime', moment(ahrDetails.otStartTime, 'kk:mm').format('h:mm A'))
                        computeDate = ahrDetails.otStartDate;
                        actualDtrTime = moment(ahrDetails.otStartTime, 'kk:mm').format('kk:mm A');
                        bctVal = dtrDetailsVal.actual_bct;
                    // });
                }
            } else {
                // dateAdjustments(moment(ahrDetails.otStartDate + " " + ahrDetails.otStartTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
                    $('#renderedWordInVal').text(moment(ahrDetails.otStartDate + " " + ahrDetails.otStartTime, 'Y-MM-DD kk:mm').format('MMM D, Y h:mm A'));
                    wordIn = moment(ahrDetails.otStartDate + " " + ahrDetails.otStartTime, 'Y-MM-DD kk:mm').format('Y-MM-DD kk:mm');
                // });
                // dateAdjustments(moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
                    $('#renderedWordOutVal').text(moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD kk:mm').format('MMM D, Y h:mm A'));
                    wordOut = moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD kk:mm').format('Y-MM-DD kk:mm');
                // });

                breakVal = dtrDetailsVal.break.minutes;
                $('#renderedWordBreakVal').data('break', dtrDetailsVal.actual_bct);

                var tard = 0;
                var tardDetails = "";
                if (parseInt(dtrDetailsVal.late) != 0) {
                    var tard1 = 1;
                    tard = 1;
                    humanTimeFormat(dtrDetailsVal.late.hoursTotal, function (formattedLate) {
                        tardDetails += formattedLate + " late";
                    })
                }
                if (parseInt(dtrDetailsVal.ut) != 0) {
                    var tard2 = 1;
                    tard = 1;
                    humanTimeFormat(dtrDetailsVal.ut.hoursTotal, function (formattedUt) {
                        if (tard1 == 1) {
                            tardDetails += ' & ';
                        }
                        tardDetails += formattedUt + " UT";
                    })
                }
                if (tard != 1) {
                    tardDetails = "None";
                }
                $('#renderedWordTardVal').text(tardDetails);
                var breaks
                if (parseInt(dtrDetailsVal.break.minutes) > 0) {
                    var breakHr = dtrDetailsVal.break.minutes / 60;
                    humanTimeFormat(breakHr, function (formattedBreak) {
                        breaks = formattedBreak;
                    });
                } else {
                    breaks = "None";
                }
                $('#renderedWordBreakVal').text(breaks);
                $('#renderedWordApproval').show();
                $('#renderedPreAfterApproval').hide();
            }
            computeAhrTimeRendered(datedShift, wordIn, wordOut, computeDate, actualDtrTime, ahrType, bctVal, breakVal, function (rendereAhr) {
                $('#renderedOt').text(rendereAhr.formatted);
                $('#renderedOt').data('otHours', rendereAhr.otHours);
                $('#approvalAhrModal').modal('show');
            })
        } else {
            // $('#unrenderedAhrApproval').show();
            // $('#renderedAhrApproval').hide();
            // if (parseInt(ahrDetails.additionalHourTypeId) != 3) {
            //     $('#shiftWord').hide(ahrDetails);
            //     $('#shiftInfo').show();
            //     $('#renderedBctVal').data('bct', dtrDetailsVal.actual_bct);
            //     getRequestorShiftUnrendered(ahrDetails.userId, ahrDetails.sched_date, function (more) {
            //         if (more) {
            //             $("#shiftunrendered option").filter(function () {
            //                 return $(this).val() == ahrDetails.schedId;
            //             }).prop('selected', true);
            //         }
            //         $('#shiftField').show();
            //     });
            //     if (parseInt(ahrDetails.additionalHourTypeId) == 1) {
            //         $('#additionalDtrDetails').hide();
            //         $('#startDateAppr').prop('disabled', true);
            //         $('#startTimeAppr').prop('disabled', true);
            //         $('#endDateAppr').prop('disabled', false);
            //         $('#endTimeAppr').prop('disabled', false);
            //         if ($('#approvalAhrModal').data('hr') == 1) {
            //             $('#endDateAppr').prop('disabled', true);
            //             $('#endTimeAppr').prop('disabled', true);
            //         }
            //         // dateAdjustments(moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
            //         //     computeDate = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('Y-MM-DD');
            //         //     actualDtrTime = moment(ahrDetails.otEndTime, 'HH:mm').format('h:mm A');
            //         //     bctVal = dtrDetailsVal.actual_bct;
            //         // });
            //         dateAdjustments(moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
            //             computeDate = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('Y-MM-DD');
            //             actualDtrTime = moment(ahrDetails.otEndTime, 'HH:mm').format('kk:mm A');
            //             // assignTimeDateAppr(moment(ahrDetails.sched_date, 'Y-MM-DD').format('MM/DD/YYYY'), ahrDetails.time_start + "-" + ahrDetails.time_end, function (shiftDated) {
            //             //     var datedShift = shiftDated;
            //             //     $('#shiftAppr').data('dateshift', datedShift);
            //             //     computeAhrTimeRendered(datedShift, wordIn, wordOut, computeDate, actualDtrTime, ahrType, bctVal, breakVal, function (rendereAhr) {
            //             //         $('#renderedOt').text(rendereAhr.formatted);
            //             //         $('#renderedOt').data('otHours', rendereAhr.otHours);
            //             //         $('#approvalAhrModal').modal('show');
            //             //     })
            //             // });
            //         });
                    
            //     } else if (parseInt(ahrDetails.additionalHourTypeId) == 2) {
            //         $('#additionalDtrDetailsLabel').text('BCT');
            //         humanTimeFormat(dtrDetailsVal.actual_bct, function (formattedBct) {
            //             $('#additionalDtrDetailsVal').text(formattedBct);
            //         })
            //         $('#additionalDtrDetails').show();
            //         $('#startDateAppr').prop('disabled', false);
            //         $('#startTimeAppr').prop('disabled', false);
            //         $('#endDateAppr').prop('disabled', true);
            //         $('#endTimeAppr').prop('disabled', true);
            //         if ($('#approvalAhrModal').data('hr') == 1) {
            //             $('#startDateAppr').prop('disabled', true);
            //             $('#startTimeAppr').prop('disabled', true);
            //         }
            //         getBctUnrendered(ahrDetails.emp_id, function (bct) {
            //             $('#additionalDtrDetailsVal').data('bct', bct.bct);
            //         })

            //         bctVal = dtrDetailsVal.actual_bct;
            //         dateAdjustments(moment(ahrDetails.otStartDate + " " + ahrDetails.otStartTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
            //             computeDate = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('Y-MM-DD');
            //             actualDtrTime = moment(ahrDetails.otStartTime, 'HH:mm').format('kk:mm A');
            //             // assignTimeDateAppr(moment(ahrDetails.sched_date, 'Y-MM-DD').format('MM/DD/YYYY'), ahrDetails.time_start + "-" + ahrDetails.time_end, function (shiftDated) {
            //             //     var datedShift = shiftDated;
            //             //     $('#shiftAppr').data('dateshift', datedShift);
            //             //     computeAhrTimeRendered(datedShift, wordIn, wordOut, computeDate, actualDtrTime, ahrType, bctVal, breakVal, function (rendereAhr) {
            //             //         $('#renderedOt').text(rendereAhr.formatted);
            //             //         $('#renderedOt').data('otHours', rendereAhr.otHours);
            //             //         $('#approvalAhrModal').modal('show');
            //             //     })
            //             // });
            //         });
                   
            //     }
            //     assignTimeDateAppr(moment(ahrDetails.sched_date, 'Y-MM-DD').format('MM/DD/YYYY'), ahrDetails.time_start + "-" + ahrDetails.time_end, function (shiftDated) {
            //         var datedShift = shiftDated;
            //         $('#shiftAppr').data('dateshift', datedShift);
            //         getBctUnrendered(ahrDetails.emp_id, function (bct) {
            //             $('#additionalDtrDetailsVal').data('bct', bct.bct);
            //         })
            //         // computeAhrTimeRendered(datedShift, wordIn, wordOut, computeDate, actualDtrTime, ahrType, bctVal, breakVal, function (rendereAhr) {
            //         //     $('#renderedOt').text(rendereAhr.formatted);
            //         //     $('#renderedOt').data('otHours', rendereAhr.otHours);
            //         //     $('#approvalAhrModal').modal('show');
            //         // })
            //         // computeAhrTimeUnrendered(function (rendereAhr) {
            //         //     $('#renderedOt').text(rendereAhr.formatted);
            //         //     $('#renderedOt').data('otHours', rendereAhr.otHours);
            //         //     $('#approvalAhrModal').modal('show');
            //         // })
            //     });               
            // } else {
            //     $('#shiftInfo').hide();
            //     getRequestorAccountShifts(ahrDetails.emp_id, function (accShifts) {
            //         var optionsAsString = "";
            //         $.each(accShifts, function (index, value) {
            //             optionsAsString += "<option value='" + value.acc_time_id + "'>" + value.time_start + " - " + value.time_end + "</option>";
            //         });
            //         $('#shiftWordUnrendered').html(optionsAsString);
            //         $('#shiftWordUnrendered').select2({
            //             placeholder: "Select a shift",
            //             width: '100%'
            //         });
            //         $('#shiftWord').show();
            //     })
            //     $('#additionalDtrDetailsLabel').text('BREAK');
            //     wordRequestDetails(ahrDetails.additionalHourRequestId, function (wordRequestDetails) {

            //         // $('#shiftWordUnrendered option[value=' + wordRequestDetails.accTimeId + ']').prop('selected', 'selected');
            //         // $('#shiftWordUnrendered').val(wordRequestDetails.accTimeId).trigger('change');

            //         // $('#shiftWordUnrendered').val(wordRequestDetails.accTimeId).change();
            //         // $('#shiftWordUnrendered').val(wordRequestDetails.accTimeId).change();

            //         $('#shiftWordUnrendered').data('origacctime', wordRequestDetails.accTimeId);
            //         var breakVal = 0;

            //         getShiftBreak(wordRequestDetails.accTimeId, function (breakShift) {
            //             breakVal = breakShift.minutes;
            //             $('#additionalDtrDetailsVal').data('break', breakShift.minutes);
            //             if (parseInt(breakShift.minutes) > 0) {
            //                 var breakHrUnrenderedWord = breakShift.minutes / 60;

            //                 humanTimeFormat(breakHrUnrenderedWord, function (formattedBreak) {
            //                     breaksUnrenderedWord = formattedBreak;
            //                 });
            //             } else {
            //                 breaksUnrenderedWord = "None";
            //             }
            //             $('#additionalDtrDetailsVal').text(breaksUnrenderedWord);

            //             assignTimeDateAppr(moment(ahrDetails.sched_date, 'Y-MM-DD').format('MM/DD/YYYY'), ahrDetails.time_start + "-" + ahrDetails.time_end, function (shiftDated) {
            //                 datedShift = shiftDated;
            //                 $('#shiftAppr').data('dateshift', datedShift);
            //                 $('#shiftWordUnrendered').val(wordRequestDetails.accTimeId).change();

            //                 $('#additionalDtrDetailsVal').data('break', breakShift.minutes);
                            
            //                 // computeAhrTimeRendered(datedShift, wordIn, wordOut, computeDate, actualDtrTime, ahrType, bctVal, breakVal, function (rendereAhr) {
            //                 // computeAhrTimeUnrendered(function (rendereAhr) {
            //                 //     $('#renderedOt').text(rendereAhr.formatted);
            //                 //     $('#renderedOt').data('otHours', rendereAhr.otHours);
            //                 //     $('#approvalAhrModal').modal('show');
            //                 // })
            //             });
            //         })
            //     });
            //     $('#additionalDtrDetails').show();
            //     $('#startDateAppr').prop('disabled', true);
            //     $('#startTimeAppr').prop('disabled', true);
            //     $('#endDateAppr').prop('disabled', true);
            //     $('#endTimeAppr').prop('disabled', true);
            //     $('#unrenderedWordApproval').show();
            //     $('#unrenderedPreAfterApproval').hide();
            // }
            // dateAdjustments(moment(ahrDetails.otStartDate + " " + ahrDetails.otStartTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
            //     wordIn = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('MMM DD, Y hh:mm A');
            //     $('#startDateAppr').val(moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/YYYY'));
            //     $('#startDateAppr').data('origstartdate', moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/YYYY'));
            //     setDate("#startDateAppr", moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
            //     $('#startTimeAppr').val(moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'));
            //     $('#startTimeAppr').data('origstarttime', moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'));
            //     initializeActualDtrTimePicker('#startTimeAppr', moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'))

            // });
            // dateAdjustments(moment(ahrDetails.otEndDate + " " + ahrDetails.otEndTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
            //     wordOut = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('MMM DD, Y hh:mm A');
            //     $('#endDateAppr').val(moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/YYYY'));
            //     $('#endDateAppr').data('origenddate', moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/YYYY'));
            //     setDate("#endDateAppr", moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
            //     $('#endTimeAppr').val(moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'));
            //     $('#endTimeAppr').data('origendtime', moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'));
            //     initializeActualDtrTimePicker('#endTimeAppr', moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'))
            //     $('#approvalAhrModal').modal('show');
            // });
            
        }
    });
}



function getAhrDetailsWithTime(ahrId) {
    $.when(fetchPostData({
        ahr_id: ahrId
    }, '/additional_hour/get_request_details')).then(function (requestorDetails) {
        var ahrDetails = jQuery.parseJSON(requestorDetails.trim());
        if (parseInt(ahrDetails.additionalHourTypeId) == 1) {
            var ahrType = 'After Shift';
        } else if (parseInt(ahrDetails.additionalHourTypeId) == 2) {
            var ahrType = 'Pre Shift';
        } else {
            var ahrType = 'WORD';
        }
        // var typeId = 3;
        $('#approvalAhrModal').data('ahrid', ahrId);
        //INITIALIZE OT DETAILS
        // var nameExt = "";
        // if ((ahrDetails.nextension != "none") || (ahrDetails.nextension ="")) {
        //     nameExt = ahrDetails.nextension;
        // }
         if (ahrDetails.mname == '') {
             var midName = "";
         } else {
             var midName = ahrDetails.mname.charAt(0) + "."
         }
        var empPic = imagePath + "" + ahrDetails.pic;
        $('#empPicApproval').attr('src', empPic)
        $('#employeeName').text(ahrDetails.fname + " " + midName + " " + ahrDetails.lname + " ");
        $('#employeeAccount').text(ahrDetails.accountDescription);
        $('#employeeJob').text(ahrDetails.positionDescription);
        $('#ahrNo').text(ahrDetails.additionalHourRequestId.padLeft(8));
        $('#dateFiledAppr').text(moment(ahrDetails.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('MMM D, Y h:mm A'));
        $('#filingTypeAppr').text(ahrDetails.requestType);
        $('#filingTypeAppr').data('requesttype', ahrDetails.requestType);
        $('#ahrTypeAppr').text(ahrType);
        $('#dateRenderedAppr').text(moment(ahrDetails.sched_date, 'Y-MM-DD').format('MMM D, Y'));
        $('#shiftAppr').text(ahrDetails.time_start + "-" + ahrDetails.time_end);
        $('#shiftTypeAppr').text(ahrDetails.schedType);
        $('#reasonAppr').text(ahrDetails.reason);
        getOtDetailsApproval(ahrDetails);
    });
}

function getAhrDetailsWithOutTime(ahrId) {
    $.when(fetchPostData({
        ahr_id: ahrId
    }, '/additional_hour/request_details_no_time')).then(function (requestorDetails) {
        var ahrDetails = jQuery.parseJSON(requestorDetails.trim());
        if (parseInt(ahrDetails.additionalHourTypeId) == 1) {
            var ahrType = 'After Shift';
        } else if (parseInt(ahrDetails.additionalHourTypeId) == 2) {
            var ahrType = 'Pre Shift';
        } else {
            var ahrType = 'WORD';
        }
        // var typeId = 3;
        var nameExt = "";
        $('#approvalAhrModal').data('ahrid', ahrId);
        //INITIALIZE OT DETAILS
        // if ((ahrDetails.nextension != "none") || (ahrDetails.nextension = "")) {
        //     nameExt = ahrDetails.nextension;
        // }
        if (ahrDetails.mname == '') {
            var midName = "";
        } else {
            var midName = ahrDetails.mname.charAt(0) + "."
        }
        var empPic = imagePath + "" + ahrDetails.pic;
        $('#empPicApproval').attr('src', empPic)
        $('#employeeName').text(ahrDetails.fname + " " + midName + " " + ahrDetails.lname + " ");
        $('#employeeAccount').text(ahrDetails.accountDescription);
        $('#employeeJob').text(ahrDetails.positionDescription);
        $('#ahrNo').text(ahrDetails.additionalHourRequestId.padLeft(8));
        $('#dateFiledAppr').text(moment(ahrDetails.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('MMM D, Y h:mm A'));
        $('#filingTypeAppr').text(ahrDetails.requestType);
        $('#filingTypeAppr').data('requesttype', ahrDetails.requestType);
        $('#ahrTypeAppr').text(ahrType);
        $('#dateRenderedAppr').text(moment(ahrDetails.sched_date, 'Y-MM-DD').format('MMM D, Y'));
        // $('#shiftAppr').text(ahrDetails.time_start + "-" + ahrDetails.time_end);
        $('#shiftTypeAppr').text(ahrDetails.schedType);
        $('#reasonAppr').text(ahrDetails.reason);
        getOtDetailsApproval(ahrDetails);
    });
}

function initAhrRequest(ahrId, approvalId, ahrTypeId, requestType) {
    $('#approvalAhrModal').data('hr', 0);
    $('#approvalAhrModal').data('approvalid', approvalId);
    if ((requestType == 'unrendered') && (parseInt(ahrTypeId) == 3)) {
        getAhrDetailsWithOutTime(ahrId);
    } else {
        getAhrDetailsWithTime(ahrId);
    }
}

function dateAdjustAppr(actualDtrDate, actualDtrTime, callback) {
    var inputTime = moment(actualDtrDate + " " + actualDtrTime, 'Y-MM-DD HH:mm').format('kk:mm');
    var formattedDateTime = moment(dateToComputeAppr + " " + actualDtrTime, 'Y-MM-DD HH:mm').format('YYYY-MM-DD HH:mm');
    if (inputTime == "24:00") {
        if (($('#shiftAppr').data('added') == undefined) || ($('#shiftAppr').data('added') == 0)) {
            formattedDateTimeStr = moment(formattedDateTime, 'YYYY-MM-DD HH:mm').valueOf();
            dateAdjustment = moment(formattedDateTime).add(1, 'days').valueOf();
            dateText = moment(formattedDateTime).valueOf();
            $('#shiftAppr').data('added', 1);
        } else if ($('#shiftAppr').data('added') == 1) {
            dateAdjustment = moment(formattedDateTime).valueOf();
            dateText = moment(formattedDateTime).subtract(1, 'days').valueOf();
            $('#shiftAppr').data('added', 1);
        }
    } else if (inputTime == "23:59") {
        if (($('#shiftAppr').data('added') == undefined) || ($('#shiftAppr').data('added') == 0)) {
        }
        if ($('#shiftAppr').data('added') == 1) {
            dateAdjustment = moment(formattedDateTime).subtract(1, 'days').valueOf();
            dateText = dateAdjustment;
            $('#shiftAppr').data('added', 0);
        }
    } else {
        dateAdjustment = moment(formattedDateTime).valueOf();
        dateText = dateAdjustment;
    }
    dateToComputeAppr = moment(dateAdjustment).format('YYYY-MM-DD');
    dateToShowAppr = moment(dateText).format('YYYY-MM-DD');
    callback();
}

function unrenderedWordAdj(dateTime, callback) {
    var time = moment(dateTime, 'Y-MM-DD HH:mm').format('HH:mm');
    if (time == '00:00') {
        var dateToCompute = moment(dateTime).add(1, 'days').format('Y-MM-DD HH:mm');
        var dateTimeToShow = moment(dateTime).format('Y-MM-DD HH:mm');
    } else {
        var dateTimeToShow = moment(dateTime).format('Y-MM-DD HH:mm');
        var dateToCompute = moment(dateTime).format('Y-MM-DD HH:mm');
    }
    var wordAdjustment = {
        show: dateTimeToShow,
        compute: dateToCompute
    }
    callback(wordAdjustment);
}

function resetApprForm() {
    $('#errNotifyApprovalAhr').hide();
    $('.character-remaining').text('');
}

function checkIfChanged(callback) {
    var ahrType = $('#shiftAppr').data('ahrtype');
    var requestType = $('#filingTypeAppr').text();
    var changeBool = 0;
    var origVal = '';
    var newVal = '';
    if (requestType == 'rendered') {
        if (ahrType == 3) {
            changeBool = 0;
        } else {
            origVal = $('#renderedActualDtrDate').data('origrendereddate') + " " + $('#renderedActualDtrTime').data('origrenderedtime');
            newVal = $('#renderedActualDtrDate').val() + " " + $('#renderedActualDtrTime').val();
        }
    } else {
        if (ahrType == 1) {
            origVal = $('#endDateAppr').data('origenddate') + " " + $('#endTimeAppr').data('origendtime');
            newVal = $('#endDateAppr').val() + " " + $('#endTimeAppr').val();
        } else if (ahrType == 2) {
            origVal = $('#startDateAppr').data('origstartdate') + " " + $('#startTimeAppr').data('origstarttime');
            newVal = $('#startDateAppr').val() + " " + $('#startTimeAppr').val();
        } else if (ahrType == 3) {
            origVal = $('#shiftWordUnrendered').data('origacctime');
            newVal = $('#shiftWordUnrendered').val();
        }
    }
    if (origVal != newVal) {
        changeBool = 1;
    }
    callback(changeBool)
}


function setApprovalNoChange(decision, callback) {
    $.when(fetchPostData({
        approval: decision,
        ahrId: $('#approvalAhrModal').data('ahrid'),
        approvalId: $('#approvalAhrModal').data('approvalid'),
        remarks:  $('#note, #approveAhrBtn').val(),
        ahrType: $('#shiftAppr').data('ahrtype')
    }, '/additional_hour/set_aproval_decision_no_change')).then(function (updateStat) {
        stat = $.parseJSON(updateStat.trim());
        callback(stat);
    });
}

function setApprovalWithChange(decision, dateStartVal, timeStartVal, dateEndVal, timeEndVal, otHoursVal, shiftWordUnVal, callback) {
    $.when(fetchPostData({
        approval: decision,
        ahrId: $('#approvalAhrModal').data('ahrid'),
        approvalId: $('#approvalAhrModal').data('approvalid'),
        remarks:  $('#note, #approveAhrBtn').val(),
        ahrType: $('#shiftAppr').data('ahrtype'),
        dateStart: dateStartVal,
        timeStart: timeStartVal,
        dateEnd: dateEndVal,
        timeEnd: timeEndVal,
        otHours: otHoursVal,
        accTimeId: shiftWordUnVal
    }, '/additional_hour/set_approval_decision_with_change')).then(function (updateStat) {
        stat = $.parseJSON(updateStat.trim());
        callback(stat);
    });
}

function setApproval(decision) {
    checkIfChanged(function (changeBool) {
        if (changeBool == 1 && decision == 5) {
            var dateStartVal = '';
            var timeStartVal = '';
            var dateEndVal = '';
            var timeEndVal = '';
            var otHours = $('#renderedOt').data('otHours');
            var shiftWordUn = 0;

            var ahrType = $('#shiftAppr').data('ahrtype');
            var requestType = $('#filingTypeAppr').data('requesttype');
            if (requestType == 'rendered') {
                if (ahrType == 1) {
                    dateEndVal = moment($('#renderedActualDtrDate').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeEndVal = moment($('#renderedActualDtrTime').val(), 'hh:mm A').format("HH:mm");
                } else if (ahrType == 2) {
                    dateStartVal = moment($('#renderedActualDtrDate').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeStartVal = moment($('#renderedActualDtrTime').val(), 'hh:mm A').format("HH:mm");
                }
            } else {
                if (ahrType == 1) {
                    dateEndVal = moment($('#endDateAppr').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeEndVal = moment($('#endTimeAppr').val(), 'hh:mm A').format("HH:mm");
                } else if (ahrType == 2) {
                    dateStartVal = moment($('#startDateAppr').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeStartVal = moment($('#startTimeAppr').val(), 'hh:mm A').format("HH:mm");
                } else {
                    shiftWordUn = $('#shiftWordUnrendered').val();
                    dateEndVal = moment($('#endDateAppr').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeEndVal = moment($('#endTimeAppr').val(), 'hh:mm A').format("HH:mm");
                    dateStartVal = moment($('#startDateAppr').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeStartVal = moment($('#startTimeAppr').val(), 'hh:mm A').format("HH:mm");
                }
            }
            setApprovalWithChange(decision, dateStartVal, timeStartVal, dateEndVal, timeEndVal, otHours, shiftWordUn, function (apprStat) {
                if (parseInt(apprStat.set_current_approver_stat) != 0) {
                    var notifIds = [
                        apprStat.proceed_approval.set_notif_details.notif_id[0],
                        apprStat.proceed_approval.set_notif_details_requestor.notif_id[0]
                    ];
                    if (parseInt(apprStat.proceed_approval.preceeding)) {
                        $newNotifIds = notifIds.concat(apprStat.proceed_approval.set_notif_details_preceeding.notif_id);
                        notification($newNotifIds);
                    } else {
                        notification(notifIds);
                    }
                    if (apprStat.proceed_approval.finalized == 1) {
                        systemNotification(apprStat.proceed_approval.set_notif_details.notif_id);
                    }
                    swal("Request Approved!", "you have successfully approved the request", "success");
                } else {
                    swal("Error!", "Something went wrong during the approval process. Please report the issue immediately to your supervisor or to the system administrator", "error");
                }
                init_pending_approval_filters();
                $('#approvalRecordAhrDatatable').mDatatable('reload');
                $('#approvalAhrModal').modal('hide');
            })
        } else {
            setApprovalNoChange(decision, function (apprStat) {
                if (parseInt(apprStat.set_current_approver_stat) != 0) {
                    var notifIds = [
                        apprStat.proceed_approval.set_notif_details.notif_id[0],
                        apprStat.proceed_approval.set_notif_details_requestor.notif_id[0]
                    ];
                    if (parseInt(apprStat.proceed_approval.preceeding)) {
                        $newNotifIds = notifIds.concat(apprStat.proceed_approval.set_notif_details_preceeding.notif_id);
                        systemNotification($newNotifIds);
                    } else {
                        systemNotification(notifIds);
                    }
                    if (apprStat.proceed_approval.finalized == 1) {
                        systemNotification(apprStat.proceed_approval.set_notif_details.notif_id);
                    }
                    if (decision == 5){
                        swal("Request Approved!", "you have successfully approved the request", "success");
                    }else{
                        swal("Request Disapproved!", "Your dissaproval was set successfully", "success");
                    }
                } else {
                    swal("Error!", "Something went wrong during the approval process. Please report the issue immediately to your supervisor or to the system administrator", "error");
                }
                init_pending_approval_filters();
                $('#approvalRecordAhrDatatable').mDatatable('reload');
                $('#approvalAhrModal').modal('hide');
            })
        }
    });
}

function checkIfRequestStillExist(callback) {
    $.when(fetchPostData({
        ahrId: $('#approvalAhrModal').data('ahrid'),
    }, '/additional_hour/check_if_request_still_exist')).then(function (checkBool) {
        var exist = $.parseJSON(checkBool.trim());
        callback(exist);
    });
}

function initEmpSelectApproval(tabSelect, statusId, dateFrom, dateTo, callback) {
    $.when(fetchPostData({
        status: statusId,
        rangeStart: dateFrom,
        rangeEnd: dateTo,
        selectedTab: tabSelect
    }, '/additional_hour/get_requestor_names')).then(function (employee) {
        var empObj = $.parseJSON(employee.trim());
        var optionsAsString = "";
        if (empObj.length > 0) {
            optionsAsString += "<option value='0'>ALL</option>"
        }
        $.each(empObj, function (index, value) {
            optionsAsString += "<option value='" + value.userId + "'>" + value.lname + ", " + value.fname + "</option>";
        })
        if (tabSelect == 'request') {
            // $('#empSelect1').html(optionsAsString);
        } else {
            $('#empSelect2').html(optionsAsString);
        }
        $('.empSelect').select2({
            placeholder: "Select an Employee",
            width: '100%'
        });
        // $(".select2-container").tooltip({
        //     title: "Select an Employee",
        //     placement: "top"
        // });

        // $(".select2-selection span").attr('title', '');
        callback(empObj);
    });
}


function queryApprovalAhrRecord(approvalDateRange) {
    var dateRange = approvalDateRange.split('-');
    $('#approvalRecordAhrDatatable').mDatatable('destroy');
    approvalRecordAhrDatatable.init($('#approvalRequestStatus').val(), $('#empSelect2').val(), $('#approvalRecordSearch').val(), moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
}

function prepareDecision(decisionStat){
    checkIfRequestStillExist(function (exist) {
        if (parseInt(exist)) {
                setApproval(decisionStat);
        } else {
            swal("Request Does not Exist!", "Sorry. This request has just been cancelled", "error");
            $('#approvalAhrPendingDatatable').mDatatable('reload');
            $('#approvalAhrModal').modal('hide');
        }
    })
}

function initPendingAhrApprovalsDatatable(){
    $('#approvalAhrPendingDatatable').mDatatable('destroy');
    approvalPendingAhrDatatable.init($('#accountSelect').val(), $('#requestorSelect').val(), $('#approvalLevel').val(), $('#approvalPendingAhrSearch').val());
}

function reloadPendingAhrApprovalsDatatable(){
    $('#approvalAhrPendingDatatable').mDatatable('reload');
    approvalPendingAhrDatatable.init($('#accountSelect').val(), $('#requestorSelect').val(), $('#approvalLevel').val(), $('#approvalPendingAhrSearch').val());
}

$('#approvalAhrModal').on('hidden.bs.modal', function () {
    resetApprForm();
    formReset('#approvalAhrForm');
})
// after and pre for rendered
$('#renderedActualDtrTime, #renderedActualDtrDate').on('change', function () {
    if (($('#renderedActualDtrDate').val() != '') && ($('renderedActualDtrTime').val() != '')) {
        // dateAdjustments(moment($('#renderedActualDtrDate').val() + " " + $('#renderedActualDtrTime').val(), 'MM/DD/Y h:mm A').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
            var datedShift = $('#shiftAppr').data('dateshift');
            var computeDate = moment($('#renderedActualDtrDate').val(), 'MM/DD/Y').format('Y-MM-DD');
            var actualDtrTime = moment($('#renderedActualDtrTime').val(), 'h:mm A').format('kk:mm');
            var bctVal = $('#renderedBctVal').data('bct');
            var ahrType = $('#shiftAppr').data('ahrtype');
            var wordIn = '';
            var wordOut = '';
            var breakVal = ''
            computeAhrTimeRendered(datedShift, wordIn, wordOut, computeDate, actualDtrTime, ahrType, bctVal, breakVal, function (renderedAhr) {
                $('#renderedOt').text(renderedAhr.formatted);
                $('#renderedOt').data('otHours', renderedAhr.otHours);

                if (renderedAhr.otHours == 0.00) {
                    $('#errNotifyApprovalMssg').html('<b>Insufficient Coverage!</b> Hours Rendered is Zero, not enough time for overtime. Please Double check you input adjustments');
                    $('#renderedOt').addClass('text-danger');
                    $('#errNotifyApprovalAhr').show();
                    $('#approveAhrBtn').addClass('disabled');
                     $('#note, #approveAhrBtn').prop('disabled', true);
                }  else {
                    $('#errNotifyApprovalAhr').hide();
                    $('#renderedOt').removeClass('text-danger');
                    // $('#requestManualAhrBtn').prop('disabled', false);
                    checkIfTimeInRangeAppr(datedShift, ahrType, bctVal, computeDate, function (checkBool) {
                        if (checkBool) {
                            if (ahrType == 1) {
                                var descrip = '<b>Maximum Time Limit Reached!</b>You are not allowed to input time that is <i>equal</i>, or <i>beyond</i> your ';
                            } else if (ahrType == 2) {
                                var descrip = '<b>Mimimum Time Limit Reached!</b> You are not allowed to input time that is <i>earlier</i> than your';
                            }
                            $('#errNotifyApprovalMssg').html(' ' + descrip + ' DTR Log which is ' + $('#renderedDtrLogVal').text() + ' as specified in the "DTR DETAILS" below');
                            $('#errNotifyApprovalAhr').show();
                            $('#approveAhrBtn').addClass('disabled');
                            $('#note, #approveAhrBtn').prop('disabled', true);
                            $('#renderedOt').text('00 hr, 00 min');
                        } else {
                            $('#errNotifyApprovalAhr').hide();
                            $('#note, #approveAhrBtn').prop('disabled', false);
                            $('#approveAhrBtn').removeClass('disabled');
                        }
                    })
                }
            })
        // });
    }
});

// after and pre for unrendered
$('#startDateAppr, #startTimeAppr, #endDateAppr, #endTimeAppr').on('change', function () {
    var ahrType = $('#shiftAppr').data('ahrtype');
    if (($('#startDateAppr').val() != '') && ($('#startTimeAppr').val() != '') && ($('#endDateAppr').val() != '') && ($('#endTimeAppr').val() != '') && (ahrType != 3)) {
        var datedShift = $('#shiftAppr').data('dateshift');
        var actualDtrTime = '';
        var computeDate = '';
        var bctVal = $('#additionalDtrDetailsVal').data('bct');
        var wordIn = '';
        var wordOut = '';
        var breakVal = $('#additionalDtrDetailsVal').data('break');
        var dateAdjustCompute = '';
        if (ahrType == 1) {
            dateAdjustments(moment($('#endDateAppr').val() + " " + $('#endTimeAppr').val(), 'MM/DD/Y h:mm A').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
                dateAdjustCompute = dateAdjustment.compute;
                actualDtrTime = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('kk:mm');
                computeDate = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('Y-MM-DD');
            });
        } else if (ahrType == 2) {
            dateAdjustments(moment($('#startDateAppr').val() + " " + $('#startTimeAppr').val(), 'MM/DD/Y h:mm A').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
                dateAdjustCompute = dateAdjustment.compute;
                actualDtrTime = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('kk:mm');
                computeDate = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('Y-MM-DD');
            });
        }
        // computeAhrTimeRendered(datedShift, wordIn, wordOut, computeDate, actualDtrTime, ahrType, bctVal, breakVal, function (renderedAhr) {
        computeAhrTimeUnrendered(function (renderedAhr) {
            $('#renderedOt').text(renderedAhr.formatted);
            $('#renderedOt').data('otHours', renderedAhr.otHours);
            if (renderedAhr.otHours == 0.00) {
                $('#errNotifyApprovalMssg').html('<b>Insufficient Coverage!</b> Hours Rendered is Zero, not enough time for overtime. Please Double check you input adjustments');
                $('#errNotifyApprovalAhr').show();
                $('#approveAhrBtn').addClass('disabled');
                 $('#note, #approveAhrBtn').prop('disabled', true);
                $('#renderedOt').addClass('text-danger');
            } else if (renderedAhr.otHours > 12.0) {
                $('#renderedOt').addClass('text-danger');
                $('#errNotifyApprovalMssg').html('<b>Maximum Limit Reached! </b>Your adjustment exceeded maximum 12 hours of overtime.');
                $('#errNotifyApprovalAhr').show();
                $('#approveAhrBtn').addClass('disabled');
                 $('#note, #approveAhrBtn').prop('disabled', true);
            } else {
                $('#errNotifyApprovalAhr').hide();
                $('#renderedOt').removeClass('text-danger');
                $('#approveAhrBtn').removeClass('disabled');
                 $('#note, #approveAhrBtn').prop('disabled', false);
            }
        })
    }
});


$("#shiftWordUnrendered").on('change', function () {
    getShiftBreak($(this).val(), function (breakShift) {
        breakVal = breakShift.minutes;
        if (parseInt(breakShift.minutes) > 0) {
            $('#additionalDtrDetailsVal').data('break', breakShift.minutes);
            var breakHrUnrenderedWord = breakShift.minutes / 60;
            humanTimeFormat(breakHrUnrenderedWord, function (formattedBreak) {
                breaksUnrenderedWord = formattedBreak;
            });
        } else {
            breaksUnrenderedWord = "None";
        }
        $('#additionalDtrDetailsVal').text(breaksUnrenderedWord);
        var wordIn = '';
        var wordOut = '';
        var computeDate = '';
        var actualDtrTime = '';
        var bctVal = $('#additionalDtrDetailsVal').data('bct');

        assignTimeDateAppr(moment($('#dateRenderedAppr').text(), 'MMM D, Y').format('MM/DD/YYYY'), $('#shiftWordUnrendered').find('option:selected').text(), function (shiftDated) {
            unrenderedWordAdj(moment(shiftDated.start, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
                wordIn = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('MMM DD, Y hh:mm A');
                $('#startDateAppr').val(moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/YYYY'));
                setDate("#startDateAppr", moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
                $('#startTimeAppr').val(moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'));
                initializeActualDtrTimePicker('#startTimeAppr', moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'))
            });
            unrenderedWordAdj(moment(shiftDated.end, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'), function (dateAdjustment) {
                wordOut = moment(dateAdjustment.compute, 'Y-MM-DD kk:mm').format('MMM DD, Y hh:mm A');
                $('#endDateAppr').val(moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/YYYY'));
                setDate("#endDateAppr", moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('MM/DD/Y'));
                $('#endTimeAppr').val(moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'));
                initializeActualDtrTimePicker('#endTimeAppr', moment(dateAdjustment.show, 'Y-MM-DD kk:mm').format('h:mm A'))
            });
            // computeAhrTimeRendered(shiftDated, wordIn, wordOut, computeDate, actualDtrTime, 3, bctVal, breakVal, function (rendereAhr) {
            //     $('#renderedOt').text(rendereAhr.formatted);
            //     $('#renderedOt').data('otHours', rendereAhr.otHours);
            //     $('#approvalAhrModal').modal('show');
            // })
            computeAhrTimeUnrendered(function (rendereAhr) {
                $('#renderedOt').text(rendereAhr.formatted);
                $('#renderedOt').data('otHours', rendereAhr.otHours);
                $('#approvalAhrModal').modal('show');
            })
            
        });
    })
})

$('#requestorSelect, #approvalLevel, #approvalPendingAhrSearch').on('change', function () {
    initPendingAhrApprovalsDatatable();
});

$('#empSelect2').on('change', function () {
    queryApprovalAhrRecord($('#dateRangeApprovalRecord .form-control').val());
})

$('#approvalRequestStatus, #approvalRecordSearch').on('change', function () {
    var dateRange = $('#dateRangeApprovalRecord .form-control').val().split('-');
    initEmpSelectApproval("record", $('#approvalRequestStatus').val(), moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (empObj) {
        queryApprovalAhrRecord($('#dateRangeApprovalRecord .form-control').val());
    });
})




// word for unrendere

$(function () {
    var start = '';
    var end = '';
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#dateRangeApprovalRecord .form-control').val(start + ' - ' + end);
        // $('#dateRangeApprovalRecord .form-control').val(moment(date, 'Y-MM-DD').format('MM/DD/YYYY') + ' - ' + moment(date, 'Y-MM-DD').format('MM/DD/YYYY'));
        initEmpSelectApproval("record", 0, moment(date, 'Y-MM-DD').startOf('month').format('Y-MM-DD'), moment(date, 'Y-MM-DD').endOf('month').format('Y-MM-DD'), function (empObj) {});
        // initEmpSelectApproval("record", 0, date, date, function (empObj) {});
        initEmpSelectApproval("request", 0, moment(date, 'Y-MM-DD').startOf('month').format('Y-MM-DD'), moment(date, 'Y-MM-DD').endOf('month').format('Y-MM-DD'), function (empObj) {});

        // start = moment(date, 'Y-MM-DD').format('MM/DD/YYYY');
        // end = moment(date, 'Y-MM-DD').format('MM/DD/YYYY');
        // approvalRecordAhrDatatable.init(0, 0, '', date, date);
        $('#dateRangeApprovalRecord').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#dateRangeApprovalRecord .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initEmpSelectApproval("record", $('#approvalRequestStatus').val(), moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (empObj) {
                queryApprovalAhrRecord($('#dateRangeApprovalRecord .form-control').val());
            });
        });
        approvalRecordAhrDatatable.init(0, 0, '', moment(date, 'Y-MM-DD').startOf('month').format('Y-MM-DD'), moment(date, 'Y-MM-DD').endOf('month').format('Y-MM-DD'));

    });
    // approvalPendingAhrDatatable.init(0, '');
    // $('[data-toggle="tooltip"]').tooltip()
    $('#approvalAhrForm').formValidation({
        message: 'This value is not valid',
        live: 'enabled',
        excluded: ':disabled',
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'fa fa-refresh'
            
        },
        fields: {
            note: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Note is required'
                    }
                }
            },
        }
    }).on('success.form.fv', function (e, data) {
        e.preventDefault();

        var formValidator = $('#approvalAhrForm').data('formValidation');
        var btn = formValidator.getSubmitButton();
        if ($(btn).attr('id') == "approveAhrBtn"){
            swal({
                title: 'Are you sure you want to approve this request?',
                text: "You will not be able to change you decision. Please review first before finalizing your decision",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '<i class="fa fa-thumbs-o-up"></i> Yes, Approved!',
                cancelButtonText: 'Not Yet, will review',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    prepareDecision(5);
                } else if (result.dismiss === 'cancel') {
                    $('#approvalAhrForm').formValidation('disableSubmitButtons', false);
                }
            });
        } else if ($(btn).attr('id') == "rejectAhrBtn") {
            swal({
                title: 'Are you sure you want to disapprove this request?',
                text: "You will not be able to change you decision. Please review first before finalizing your decision",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '<i class="fa fa-thumbs-o-down"></i> Yes, disapproved!',
                cancelButtonText: 'Not Yet, will review',
                reverseButtons: true,
                // width: '500px' 
            }).then(function (result) {
                if (result.value) {
                     prepareDecision(6);
                } else if (result.dismiss === 'cancel') {
                    $('#approvalAhrForm').formValidation('disableSubmitButtons', false);
                }
            });
        }
        // $(e.relatedTarget)
    });    
});