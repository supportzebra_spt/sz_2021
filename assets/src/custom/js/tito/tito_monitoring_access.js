var accessDatatable = function () {
    var access = function (classVal, accVal, empIdVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/tito/get_monitoring_access_datatable",
                        params: {
                            query: {
                                class: classVal,
                                acc_id: accVal,
                                emp_id: empIdVal,
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#offenseSearchField'),
            },
            rows: {
                afterTemplate: function (row, data, index) {
                    $('th[data-field="offense"]').removeClass('m-datatable__cell--left');
                    $('th[data-field="offense"]').addClass('m-datatable__cell--center');
                    // $('.popoverTest').popover({
                    //     title: "Category ",
                    //     html: true,
                    //     container: 'body',
                    //     animation: true,
                    //     content:"test",
                    //     trigger: 'focus',
                    // })
                },
            },
            // columns definition
            columns: [{
                field: "titoMonitoringAccess_ID",
                title: "",
                width: 70,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null){
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#titoAccess" + row.titoMonitoringAccess_ID + "";
                    var html = '<img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '">';
                    return html;
                },
            },{
                field: 'assigned_to',
                width: 160,
                title: 'User',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = '<div class="pl-0" style="font-size: 0.9rem;">' + toTitleCase(row.fname) + ' ' + toTitleCase(row.lname) + '</div>';
                    return html;
                },
            }, {
                field: 'access_type',
                width: 100,
                title: 'Access Type',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = "";
                    if(row.access_type == 'admin'){
                        html = '<span data-access="'+row.access_type+'" data-accessid="'+row.titoMonitoringAccess_ID+'">' + toTitleCase(row.access_type) + '</span>';
                    }else{
                        html = '<span class="editableStyle accessTypeEditable" data-access="'+row.access_type+'" data-accessid="'+row.titoMonitoringAccess_ID+'">' + toTitleCase(row.access_type) + '</span>';
                    }
                    return html;
                },
            },{
                field: 'date_time_created',
                width: 160,
                title: 'Assigned On',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + moment(row.date_time_created, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</span>";
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    if(row.access_type != 'admin'){
                        html = '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air remove_access" style="width: 22px !important;height: 22px !important;" data-fname="'+toTitleCase(row.fname)+'" data-lname="'+toTitleCase(row.lname)+'" data-accessid="'+row.titoMonitoringAccess_ID+'"><i class="fa fa-close" style="font-size: 0.95rem !important;"></i></a>';
                    }
                    return html;                    
                },
            }
        ],
        };
        var datatable = $('#tito_access_datatable').mDatatable(options);
    };
    return {
        init: function (classVal, accVal, empIdVal) {
            access(classVal, accVal, empIdVal);
        }
    };
}();
function remove_user_access(element){
    $.when(fetchPostData({
        access_id: $(element).data('accessid'),
    }, '/tito/remove_monitoring_access')).then(function (removeStat) {
        let removeStatObj = $.parseJSON(removeStat.trim());
        if(parseInt(removeStatObj)){
            toastr.success('Successfully Removed Access');
        }else{
            toastr.error('Remove Access Error. Report to system administrator immediately');
        }
        initAccessDatatable('reload');
    });
}
function confirm_remove_access(element){
    swal({
        title: 'Are you sure you want to remove access for '+toTitleCase($(element).data('fname'))+' '+toTitleCase($(element).data('lname'))+'?',
        text: "You will not be able to revert back the current record.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete it!',
        cancelButtonText: 'No, Don\'t Delete',
        reverseButtons: true,
        // width: '500px'
    }).then(function (result) {
        if (result.value) {
            remove_user_access(element)
        } else if (result.dismiss === 'cancel') {
            $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
        }
    });
}
function get_accounts(class_val, element, filter_val, callback){
    $.when(fetchPostData({
        class: class_val,
        filter_type: filter_val
    }, '/tito/get_account_filters')).then(function (accounts) {
        let accountsObj = $.parseJSON(accounts.trim());
        // console.log(accountsObj);
        let optionsAsString = "";
        if(parseInt(accountsObj.exist)){
            optionsAsString += "<option value='0'>ALL</option>";
            $.each(accountsObj.record, function(index, value){
                optionsAsString += "<option value='" + value.acc_id + "'>" + toTitleCase(value.acc_name) + "</option>"
            })
        }
        $(element).html(optionsAsString);
        $(element).select2({
            placeholder: "Select an Account",
            width: '100%'
        }).trigger('change');
        callback(accountsObj);
    });
}
function get_user(class_val, acc_id_val, element, filter_val, callback){
    $.when(fetchPostData({
        class: class_val,
        acc_id: acc_id_val,
        filter_type: filter_val
    }, '/tito/get_emp_filter')).then(function (emp) {
        let empObj = $.parseJSON(emp.trim());
        let optionsAsString = "";
        if(filter_val == 'filter'){
            optionsAsString = "<option value='0'>ALL</option>";
        }
        if (parseInt(empObj.exist)){
            $.each(empObj.record, function(index, value){
                optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.lname) + ", " + toTitleCase(value.fname) + "</option>"
            })
        }
        $(element).html(optionsAsString);
        callback(empObj);
    });
}
function init_account_assignment(){
    console.log('init assign selects');
    get_accounts($('#tito_assign_class').val(), '#tito_assign_account', 'assign', function(){
        get_user($('#tito_assign_class').val(), $('#tito_assign_account').val(), '#tito_assign_employee', 'assign', function(emp_obj){
            BootstrapSelect.init('#tito_assign_employee', 'User', true, '', 'count', '{0} User');
            $('#tito_assign_employee').selectpicker('refresh');
        })
    });
}
function init_account_filter(){
    get_accounts($('#tito_access_class').val(), '#tito_access_account', 'filter', function(){
        get_user($('#tito_access_class').val(), $('#tito_access_account').val(), '#tito_access_employee', 'filter', function(){
            $('#tito_access_employee').select2({
                placeholder: "Select a User",
                width: '100%'
            }).trigger('change');
            initAccessDatatable('destroy');
        })
    });
}
function assign_users(){
    if($('#tito_assign_employee').val().length < 1){
        swal('No Users selected!', 'Kindly select users to add', 'error');
        $('#assign_user').fadeOut();
    }else{
        $.when(fetchPostData({
            emp_ids: $('#tito_assign_employee').val().toString(),
        }, '/tito/assign_monitoring_access')).then(function (assign) {
            let assignObj = $.parseJSON(assign.trim());
            if(parseInt(assignObj.stat)){
                swal('Successfully Assigned!','Successfully assigned acess to user/s!','success')
            }else{
                swal('Error!','Something went wrong while assigning. Please report this error immediately to your system administrator!','error')
            }
            open_access_filter();
        });
    }
}
function initAccessDatatable(init_type){
    $('#tito_access_datatable').mDatatable(init_type);
    accessDatatable.init($('#tito_access_class').val(), $('#tito_access_account').val(), $('#tito_access_employee').val());
}
function open_access_filter(){
    init_account_filter();
    $('.assign_btn').hide();
    $('#access_filter').fadeIn();
    $('#assign_filter').hide();
    $('#open_assign_user').fadeIn();
}
function open_assign_filter(){
    init_account_assignment();
    $('#access_filter').hide();
    $('#assign_filter').fadeIn();
    $('#close_assign_user').fadeIn();
    $('#open_assign_user').hide();
}
function update_montoring_access(){
    $.when(fetchPostData({
        access_id: $('#access_type_modal').data('access_id'),
        access: $('input[name="monitoringAccessType"]:checked').val(),
    }, '/tito/update_monitoring_access')).then(function (accessUpdate) {
        let accessUpdateObj = $.parseJSON(accessUpdate.trim());
        if(parseInt(accessUpdateObj)){
            toastr.success('Successfully Update Access');
        }else{
            toastr.error('Update Access Error. Report to system administrator immediately');
        }
        $('#access_type_modal').modal('hide');
        initAccessDatatable('reload');
        // open_access_filter();
    });
    // console.log($('input[name="monitoringAccessType"]:checked').val());
    // console.log($('#access_type_modal').data('access_id'));
}
$('#save_access').on('click', function(){
    update_montoring_access();
})
$('#tito_monitoring_access').on('change', '#tito_assign_class', function(){
    init_account_assignment();
});
$('#tito_monitoring_access').on('change', '#tito_access_class', function(){
    init_account_filter();
});
$('#tito_monitoring_access').on('change', '#tito_assign_account', function(){
    get_user($('#tito_assign_class').val(), $('#tito_assign_account').val(), '#tito_assign_employee' , 'assign', function(){
        BootstrapSelect.init('#tito_assign_employee', 'User', true, '', 'count', '{0} User');
        $('#tito_assign_employee').selectpicker('refresh');
    })
});
$('#tito_monitoring_access').on('change', '#tito_access_account', function(){
    get_user($('#tito_access_class').val(), $('#tito_access_account').val(), '#tito_access_employee', 'filter', function(){
        console.log('update users after account change');
        $('#tito_access_employee').select2({
            placeholder: "Select a User",
            width: '100%'
        }).trigger('change');
        initAccessDatatable('destroy');
    })
});
$('#tito_monitoring_access').on('click', '.accessTypeEditable', function(){
    $("input[name=monitoringAccessType][value='" + $(this).data('access')+ "']").prop("checked", true);
    $('#access_type_modal').data('orig_val', $(this).data('access'));
    $('#access_type_modal').data('access_id', $(this).data('accessid'));
    $('#access_type_modal').modal('show');
})
$('#tito_monitoring_access').on('click', '.remove_access', function(){
    confirm_remove_access(this);
})
$('#tito_monitoring_access').on('change', '#tito_access_employee', function(){
    initAccessDatatable('destroy');
});
// $('#tito_assign_employee').on('click', function(){
//     console.log($(this).val());
// })
$('#tito_monitoring_access').on('click', '#assign_user', function(){
    assign_users();
})
$('#tito_monitoring_access').on('click', '#close_assign_user', function(){
    open_access_filter();
});
$('#tito_monitoring_access').on('click', '#open_assign_user', function(){
    open_assign_filter();
});
$('#tito_monitoring_access').on('changed.bs.select', '#tito_assign_employee', function (e, clickedIndex, isSelected, previousValue) {
    e.preventDefault();
    console.log($(this).val());
    if($(this).val().length > 0){
        $('#assign_user').fadeIn();
    }else{
        // console.log('hide assign button');
        $('#assign_user').fadeOut();
    }
});