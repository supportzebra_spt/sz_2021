function humanTimeFormat(hours, callback) {
    var totalHours = parseInt(hours);
    var totalMinutes = Math.round(((hours - totalHours) * 60).toFixed(2));
    var totalTime = '';
    if (totalMinutes == 60) {
        totalMinutes = 0;
        totalHours++;
    }
    var totalTime = '';
    if ((totalHours == 0) && (totalMinutes == 1)) {
        totalTime = totalMinutes + " min";
    } else if ((totalHours == 0) && (totalMinutes != 1)) {
        totalTime = totalMinutes + " mins";
    } else if ((totalMinutes == 0) && (totalHours == 1)) {
        totalTime = totalHours + " hr";
    } else if ((totalMinutes == 0) && (totalHours != 1)) {
        totalTime = totalHours + " hrs";
    } else if ((totalHours >= 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " min";
    } else if ((totalHours == 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " mins";
    } else if ((totalHours == 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " min";
    } else if ((totalHours >= 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " mins";
    }
    callback(totalTime);
}
var titoRequestDatatable = function () {
    var titoRequest = function (statVal, typeVal, searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/tito/get_tito_request_datatable",
                        params: {
                            query: {
                                status: statVal,
                                type: typeVal,
                                search: searchVal,
                            },
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: "dtrRequest_ID",
                title: "TITO-ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var html = "<td'>" + row.dtrRequest_ID.padLeft(8)+ "</td>";
                    return html;
                }
            }, {
                field: 'dateRequest',
                width: 100,
                title: 'Schedule',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.dateRequest, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'dateCreated',
                width: 100,
                title: 'Filed On',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td>" + moment(row.dateCreated, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</td>";
                    return html;
                },
            }, {
                field: 'timetotal',
                width: 100,
                title: 'Time',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    var html ="";
                    humanTimeFormat(row.timetotal, function (formattedHours) {
                        // $('#additionalVal').text(formattedBct);
                        html = "<span class='text-center'>" + formattedHours + "</span>";
                    });
                    return html;
                },
            }, {
                field: 'description',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.status_ID) == 12) {
                        var badgeColor = 'btn-warning';
                        var dotAlert = "<span class='m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink mr-1' style='min-height: 7px;min-width: 7px;'></span>";
                    } else if (parseInt(row.status_ID) == 2) {
                        var badgeColor = 'btn-info';
                        var dotAlert = "";
                    }
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " tito_approver_info' data-titoid='"+row.dtrRequest_ID+"'>"+dotAlert+" " + row.description + "</span>";
                    return html;
                },

            }, {
                field: 'action',
                width: 70,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<span><a data-titorequestid="'+ row.dtrRequest_ID +'" class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air remove_tito"><i class="fa fa-remove"></i></a>&nbsp;&nbsp;<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View details" onClick="checkTitoRequest('+row.dtrRequest_ID+','+row.dtrRequestApproval_ID+')"><i class="fa fa-search"></i></a><span>';
                    return html;
                }
            }],
        };
        var datatable = $('#tito_request_datatable').mDatatable(options);
    };
    return {
        init: function (statVal, typeVal, searchVal) {
            titoRequest(statVal, typeVal, searchVal);
        }
    };
}();
var titoRecordDatatable = function () {
    var titoRecord = function (statVal, typeVal, searchVal, startDateVal, endDateVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/tito/get_tito_record_datatable",
                        params: {
                            query: {
                                status: statVal,
                                type: typeVal,
                                search: searchVal,
                                startDate: startDateVal, 
                                endDate: endDateVal
                            },
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: "dtrRequest_ID",
                title: "TITO-ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var html = "<td'>" + row.dtrRequest_ID.padLeft(8)+ "</td>";
                    return html;
                }
            }, {
                field: 'dateRequest',
                width: 100,
                title: 'Schedule',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.dateRequest, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'dateCreated',
                width: 100,
                title: 'Filed On',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td>" + moment(row.dateCreated, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</td>";
                    return html;
                },
            }, {
                field: 'timetotal',
                width: 100,
                title: 'Time',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    var html ="";
                    humanTimeFormat(row.timetotal, function (formattedHours) {
                        // $('#additionalVal').text(formattedBct);
                        html = "<span class='text-center'>" + formattedHours + "</span>";
                    });
                    return html;
                },
            }, {
                field: 'description',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.status_ID) == 12) {
                        var badgeColor = 'btn-warning';
                    } else if (parseInt(row.status_ID) == 2) {
                        var badgeColor = 'btn-info';
                    } else if (parseInt(row.status_ID) == 5){
                        var badgeColor = 'btn-success';
                    } else if (parseInt(row.status_ID) == 6) {
                        var badgeColor = 'btn-danger';
                    }
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide tito_approver_info' data-titoid='"+row.dtrRequest_ID+"'>" + row.description + "</span>";
                    return html;
                },
            }, {
                field: 'action',
                width: 70,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<span><a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" onClick="checkTitoRequest('+row.dtrRequest_ID+','+row.dtrRequestApproval_ID+')"><i class="fa fa-search"></i></a><span>';
                    return html;
                }
            }],
        };
        var datatable = $('#tito_record_datatable').mDatatable(options);
    };
    return {
        init: function (statVal, typeVal, searchVal, startDateVal, endDateVal) {
            titoRecord(statVal, typeVal, searchVal, startDateVal, endDateVal);
        }
    };
}();
function remove_tito(element){
    $.when(fetchPostData({
        tito_id: $(element).data('titorequestid'),
    }, '/tito/remove_tito_request')).then(function (removeStat) {
        let removeStatObj = $.parseJSON(removeStat.trim());
        let err_arr = [];
        let err_count = 0;
        let tito_id = ''+$(element).data('titorequestid');
        let tito_str = tito_id.padLeft(8);
        if(!parseInt(removeStatObj.delete_deadline_stat)){
            err_arr[err_count] = "Delete Deadlines error";
            err_count += 1;
        }
        if(!parseInt(removeStatObj.delete_approvers_stat)){
            err_arr[err_count] = "Delete Approvers error";
            err_count += 1;
        }
        if(!parseInt(removeStatObj.delete_tito_stat)){
            err_arr[err_count] = "Delete TITO error";
            err_count += 1;
        }
        if(!parseInt(removeStatObj.delete_ahr_stat)){
            err_arr[err_count] = "Delete AHR error";
            err_count += 1;
        }
        if(err_arr.lenth > 0){
            let tito_id = ''+$(element).data('titorequestid');
            let tito_str = tito_id.padLeft(8);
            $err_details_str =  err_arr.join(',');
            swal(
                'Error exist while Deleting TITO'+tito_str+'!','Delete error: '+$err_details_str+'. Report the error immediately to System Administrator','error'
            )
        }else{
            toastr.success('Successfully Removed TITO-'+tito_str);
        }
        initPendingTitoRequestDatatable('reload');
    });
}
function confirm_remove_tito(element){
    let tito_id = ''+$(element).data('titorequestid');
    let tito_str = tito_id.padLeft(8);
    swal({
        title: 'Are you sure you want to delete TITO-'+tito_str+'?',
        text: "You will not be able to revert back the current record.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete it!',
        cancelButtonText: 'No, Don\'t Delete',
        reverseButtons: true,
        // width: '500px'
    }).then(function (result) {
        if (result.value) {
            remove_tito(element)
        } else if (result.dismiss === 'cancel') {
        }
    });
}
function hide_tito_elements(){
    // hide alerts
    $('.hidden_elements').hide();
}
function toggle_fields($shift, $input_fields){
    //0 to disable
    //1 to enable
    $('#tito_shift').removeAttr('disabled');
	$('.tito_input_fields').removeAttr('disabled');
    // if($shift){
    //     $('#tito_shift').removeAttr('disabled');
    // }else{
    //     $('#tito_shift').attr('disabled','disabled');
    // }
    if($input_fields){
        $('.tito_input_fields').removeAttr('disabled');
    }else{
        $('.tito_input_fields').attr('disabled','disabled');
    }    
}
function init_ahr_log(ot_type, callback){
    let fix_content = '<div class="row px-4">'+
                            '<div class="col-7">'+
                                '<div class="form-group m-form__group mb-0">'+
                                    '<label class="mr-2">Date: </label><span id="ot_fix_date">September 30, 2020</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-5">'+
                                '<div class="form-group m-form__group mb-0">'+
                                    '<label class="mr-2">Time: </label><span id="ot_fix_time"></span> 12:00 PM'+
                                '</div>'+
                            '</div>'+
                        '</div>';
    let dynamic_val = '<div class="row px-4 pb-2">'+
                            '<div class="col-7">'+
                                '<label style="font-size:0.8rem">Date</label>'+
                                '<div class="form-group m-form__group">'+
                                    '<div class="input-group pull-right">'+
                                        '<input type="text" class="form-control m-input datepicker" readonly id="ot_edit_date" name="ot_edit_date"/>'+
                                        '<div class="input-group-append">'+
                                            '<span class="input-group-text">'+
                                                '<i class="la la-calendar-check-o"></i>'+
                                            '</span>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-5">'+
                                '<label style="font-size:0.8rem">Time</label>'+
                                '<div class="form-group m-form__group">'+
                                    '<div class="input-group pull-right" id="dateRangePersonalRecord">'+
                                        '<input type="text" class="form-control m-input timepicker" readonly id="ot_edit_time" name="ot_edit_time"/>'+
                                        '<div class="input-group-append">'+
                                            '<span class="input-group-text">'+
                                                '<i class="la la-clock-o"></i>'+
                                            '</span>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
    if(ot_type == 'pre'){
        $('#start_ot_content').html(dynamic_val);
        $('#end_ot_content').html(fix_content);
    }else{
        $('#start_ot_content').html(fix_content);
        $('#end_ot_content').html(dynamic_val);
    }
    callback(1);
}
function init_shift(callback){
    $.when(fetchGetData('/tito/get_shifts')).then(function (shifts) {
        let shiftsObj = $.parseJSON(shifts.trim());
        // let optionsAsString =  "<option value='0'>--</option>";
        let optionsAsString =  "<option></option>";
        if (parseInt(shiftsObj.exist)) {
            $.each(shiftsObj.record, function (index, value) {
                var selected = (index==0) ? "selected" : "";
                optionsAsString += "<option value='"+value.acc_time_id+"' "+selected+" >" + value.time_start+" - "+value.time_end+"</option>";
            })
        }
        $('#tito_shift').html(optionsAsString);
        callback(shiftsObj);
    });
}
function check_tito_filed(callback){
    $.when(fetchPostData({
        sched_date: moment($('#tito_sched_date').val(), 'Y-MM-DD').format('Y-MM-DD'),
        acc_time_id: $('#tito_shift').val(),
    }, '/tito/check_if_tito_filed')).then(function (titoFiled) {
        let titoFiledObj = $.parseJSON(titoFiled.trim());
    //    console.log(titoFiledObj);
       callback(titoFiledObj);
    });
}
function check_sched_attach_ahrs(callback){
    $.when(fetchPostData({
        sched_date: moment($('#tito_sched_date').val(), 'Y-MM-DD').format('Y-MM-DD'),
        acc_time_id: $('#tito_shift').val(),
    }, '/tito/check_if_has_sched_details'),
    fetchPostData({
        sched_date: moment($('#tito_sched_date').val(), 'Y-MM-DD').format('Y-MM-DD'),
        acc_time_id: $('#tito_shift').val(),
    }, '/tito/check_if_tito_filed'),
    fetchPostData({
        sched_date: moment($('#tito_sched_date').val(), 'Y-MM-DD').format('Y-MM-DD'),
    }, '/tito/check_num_of_tito_filed'),
    fetchPostData({
        sched_date: moment($('#tito_sched_date').val(), 'Y-MM-DD').format('Y-MM-DD'),
        acc_time_id: $('#tito_shift').val(),
    }, '/tito/check_if_sched_has_ahrs')).done(function (schedDetails, titoFiled, titoFiledNum, ahrDetails) {
        let schedDetailsStat = $.parseJSON(schedDetails[0].trim());
        let titoFiledStat = $.parseJSON(titoFiled[0].trim());
        let titoFiledNumStat = $.parseJSON(titoFiledNum[0].trim());
        let ahrDetailsStat = $.parseJSON(ahrDetails[0].trim());
        let disabled = 0;
        let disabledBool = 0;
        if (parseInt(schedDetailsStat.with_details)) {
            $('#sched_details').fadeIn();
        }else{
            $('#sched_details').fadeOut();
        }
        if(parseInt(titoFiledStat.exist)){
			$('#with_tito_err').fadeIn();
            disabled += 1;
		}else{
			$('#with_tito_err').hide();
        }
        if(parseInt(titoFiledNumStat.exist)){
			$('#max_tito_err').fadeIn();
            disabled += 1;
		}else{
			$('#max_tito_err').hide();
		}
        if (parseInt(ahrDetailsStat.with_ahr)) {
            if(parseInt(ahrDetailsStat.removable)){
                $('#remove_ahr_link').show();
               
            }else{
                $('#remove_ahr_link').hide();
            }
            $('#with_ahr_err').fadeIn();
            disabled += 1;
        }else{
            $('#with_ahr_err').fadeOut();
        }
        if(disabled > 0){
            toggle_fields(1, 0);
            disabledBool = 1;
            $('#tito_request_form').formValidation('disableSubmitButtons', true);
        }else{
            toggle_fields(1, 1);
            $('#tito_request_form').formValidation('disableSubmitButtons', false);
        }
        callback(disabledBool);
    });
}
function view_sched_details(callback){
    $.when(fetchPostData({
        sched_date: moment($('#tito_sched_date').val(), 'Y-MM-DD').format('Y-MM-DD'),
        acc_time_id: $('#tito_shift').val()
    }, '/tito/get_sched_attachments')).then(function (sched_details) {
        let sched_details_obj = $.parseJSON(sched_details.trim());
        $('#sched_log_details').html(sched_details_obj.logs_content);
        callback(sched_details_obj);
    });
}
function view_ahr_details(callback){
    $.when(fetchPostData({
        sched_date: moment($('#tito_sched_date').val(), 'Y-MM-DD').format('Y-MM-DD'),
        acc_time_id: $('#tito_shift').val()
    }, '/tito/get_ahr_attachments')).then(function (ahr_details) {
        let ahr_details_obj = $.parseJSON(ahr_details.trim());
        console.log(ahr_details_obj);
        $('#attach_ahr_details').html(ahr_details_obj.ahr_content);
        callback(ahr_details_obj);
    });
}
function delete_ahr(){
    $.when(fetchPostData({
        sched_date: moment($('#tito_sched_date').val(), 'Y-MM-DD').format('Y-MM-DD'),
        acc_time_id: $('#tito_shift').val()
    }, '/tito/remove_ahr')).then(function (remove_ahr) {
        let remove_ahr_obj = $.parseJSON(remove_ahr.trim());
        let err_count = 0;
        let err_details = [];
        let err_details_count = 0;
        if(parseInt(remove_ahr_obj.remove_approver) == 0){
            err_details[err_details_count]= "Approver/s";
            err_details_count++;
        }
        if(parseInt(remove_ahr_obj.remove_deadlines) == 0){
            err_details[err_details_count]= "Deadline/s";
            err_details_count++;
        }
        if(parseInt(remove_ahr_obj.remove_ahrs) == 0){
            err_details[err_details_count]= "AHR/s";
            err_details_count++;
        }
        if(parseInt(remove_ahr_obj.update_scheds) == 0){
            err_details[err_details_count]= "Updating of sched/s";
            err_details_count++;
        }
        if(parseInt(remove_ahr_obj.set_log) == 0){
            err_details[err_details_count]= "system logs";
            err_details_count++;
        }
        if(err_details.length > 0){
            $err_details_str = err_details.join(", ");
            swal(
                'Error in Deleting AHR!','Error in Deleting or Updating of records for the following: '+$err_details_str,'error'
            )
        }else{
            swal(
                'Successfully Delete AHRs!','','success'
            )
        }
        $('#tito_ahr_locked_details').modal('hide');
        check_sched_attach_ahrs();
        // console.log(remove_ahr_obj);
    });
}
function titoRequestRecordDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#tito_record_date_range').val(start + ' - ' + end);
        $('#tito_record_date_range').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            // console.log('record date range test');
            $('#tito_record_date_range').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            // initIrAbleDtrViolationRecordMonitoring(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
            initRecordTitoRequestDatatable('destroy');
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);
    });
}
function initPendingTitoRequestDatatable(init_type){
    let type_stat = $('.tito_req_type:checked').map(function () {
        return $(this).data('type'); 
    }).get();
    $('#tito_request_datatable').mDatatable(init_type);
    titoRequestDatatable.init($('#requestStatus').val(), type_stat.toString(), $('#pending_tito_search').val());
}
function initRecordTitoRequestDatatable(init_type){
    let request_stat = $('.tito_rec_status:checked').map(function () {
        return $(this).data('stat'); 
    }).get();
    let type_stat = $('.tito_rec_type:checked').map(function () {
        return $(this).data('type'); 
    }).get();
    dateRange = $('#tito_record_date_range').val().split('-');
    $('#tito_record_datatable').mDatatable(init_type);
    titoRecordDatatable.init(request_stat.toString(), type_stat.toString(), $('#titoRecordSearch').val(), moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'));
}
$('#tito_request_div').on('click', '.remove_tito', function(){
    confirm_remove_tito(this);
})
$('#tito_request_form').formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        tito_sched_date: {
            validators: {
                notEmpty: {
                    message: 'Date is required'
                },
            }
        },
        tito_start_date: {
            validators: {
                notEmpty: {
                    message: 'Clock in Date is required'
                },
            }
        }, 
        tito_start_time: {
            validators: {
                notEmpty: {
                    message: 'Clock in Time is required'
                },
            }
        },
        tito_end_date: {
            validators: {
                notEmpty: {
                    message: 'Clock out Date is required'
                },
            }
        }, 
        tito_end_time: {
            validators: {
                notEmpty: {
                    message: 'Clock out Time is required'
                },
            }
        },
        tito_reason: {
            validators: {
                notEmpty: {
                    message: 'Reason is required'
                },
            }
        },
    }
}).on('success.form.fv', function (e, data) {
    e.preventDefault();
    var arr = [];
		var  dateStart =  moment($("#tito_sched_date").val()).format('YYYY-MM-DD');
		var  time = $("#tito_shift option:selected").text().split("-");
		var  acc_time_id = $("#tito_shift").val();
		var  tito_reason = $("#tito_reason").val();
	 
		var  dateTimeStart = $("#tito_start_date").val()+" "+$("#tito_start_time").val();
		var  dateTimeEnd = $("#tito_end_date").val()+" "+$("#tito_end_time").val();
	  
		var  newtotal = $("#tito_total_time").data("newtotal");
		var  origdatestart = $("#tito_total_time").data("origdatestart");
		var  origdateend = $("#tito_total_time").data("origdateend");
	  
		var  newdatestart = $("#tito_total_time").data("newdatestart");
		var  newdateend = $("#tito_total_time").data("newdateend");

		var  otpre = $("#tito_total_time").data("otpre");
		var  otafter = $("#tito_total_time").data("otafter");
        var  otword = $("#tito_total_time").data("otword");

        var  otwordStart = $("#checkWord").data("otstartdatetime");
		var  otwordEnd = $("#checkWord").data("otenddatetime");
	 
	 	var  otpreStart = $("#checkPre").data("otstartdatetime");
		var  otpreEnd = $("#checkPre").data("otenddatetime");
	 
	 	var  otafterStart = $("#checkAfter").data("otstartdatetime");
        var  otafterEnd = $("#checkAfter").data("otenddatetime");
        
		var  checkWord = $("#checkWord").is(":checked");
		var  checkPre = $("#checkPre").is(":checked");
		var  checkAfter = $("#checkAfter").is(":checked");
        arr = {dateStart,time,acc_time_id,dateTimeStart,dateTimeEnd,newtotal,origdatestart,origdateend,newdatestart,newdateend,checkWord,checkPre,checkAfter,tito_reason,otpre,otafter,otword,otwordStart,otwordEnd,otpreStart,otpreEnd,otafterStart,otafterEnd};
        console.log(newtotal);
 		if(parseFloat(newtotal)>0){
        swal({
              title: 'Are you sure ?',
              text: "Please double check if the details are correct.",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Yes, Send!',
              cancelButtonText: 'No, Let me check it!',
              reverseButtons: true,
              // width: '500px'
          }).then(function (result) {
              if (result.value) {
                   $.ajax({			
                      type: "POST",
                      url:'/sz/tito/saveTitoDetails',
                      data: arr,
                      cache: false,
                      success: function(output) {
                           
                          // var rs = JSON.parse(output);
                          if (output > 0) {
                              swal('Your request was successfully sent!','','success')
                              initPendingTitoRequestDatatable('destroy');
                              titoRequestRecordDateRangePicker(function(){});
                              $("#tito_request_modal").modal("hide");
                          } else {
                              swal('Opps, Error!','Something went wrong!','error');
                          }
                      }
                  });
              } else if (result.dismiss === 'cancel') {


              }
          });
		}else{
			  swal('Opps, Error!','You have some invalid details. Please double check or refresh the page!','error');
		}
  
});
$('#deleteAhrBtn').on('click', function(){
    confirm_remove_ahr();
});
$('#view_sched_details').on('click', function(){
    view_sched_details(function(sched_details){
        $('#tito_sched_details').modal('show');
    })
});
$('#remove_ahr_link').on('click', function(){
    view_ahr_details(function(ahr){
        $('#tito_ahr_locked_details').modal('show');
    })
});
// $('#tito_sched_date').on('change', function(){
//     check_sched_attach_ahrs();
// });
$('#tito_ahr_modal').on('hidden.bs.modal', function(){
    formReset('#ahr_request_form');
    $('#ahr_request_form').formValidation('destroy');
});
$('#tito_request_modal').on('hidden.bs.modal', function(){
    hide_tito_elements();
    formReset('#tito_request_form');
});
$('#file_tito_request').on('click', function(){
	$(".tito_input_fields").attr("disabled",true);
	$(".ahr_form").html("00 hr, 00 min");
	$("#checkPre").prop("disabled",true);
	$("#checkAfter").prop("disabled",true);
    init_shift(function(shift_obj){
        $('#tito_request_modal').modal('show');
    });
});
$('#tito_shift').on('change', function(){
    $('#tito_request_form').formValidation('disableSubmitButtons', true);
    toggle_fields(1, 1);
});
//initalizations
$('#tito_shift').select2({
    placeholder: "Select a Shift",
    width: '100%'
});
$('.datepicker').datepicker({
    format: "yyyy-mm-dd",
});
// $('#tito_start_date').datepicker({
	    // format: 'yyyy-mm-dd',
	// }).on("change", function (e) {
			
		// var date =$(this).val();
		// // $('#tito_start_date').datepicker('setDate',date);
 		// $("#tito_start_date").val(date);
 			 
// }); 
 // $('#tito_end_date').datepicker({
	    // format: 'yyyy-mm-dd',
	// }).on("change", function (e) {
			
		// var date =$(this).val();
		// // $('#tito_end_date').datepicker('setDate',date);
 		// $("#tito_end_date").val(date);
 			 
// });  
$('.timepicker').timepicker({minuteStep: 1});
$('.tito_req_type').on('click', function(){
    initPendingTitoRequestDatatable('destroy');
})
$('#pending_tito_search').on('keyup', function(){
    initPendingTitoRequestDatatable('destroy');
})
$('#titoRecordSearch').on('keyup', function(){
    initRecordTitoRequestDatatable('destroy');
})
$('.tito_rec_type, .tito_rec_status').on('click', function(){
    initRecordTitoRequestDatatable('destroy');
})
$('.tito_request_nav').on('click', function(){
    console.log($(this).find('a').attr('href'));
    if($(this).find('a').attr('href') == '#pending_tito'){
        initPendingTitoRequestDatatable('destroy');
    }else{
        initRecordTitoRequestDatatable('destroy');
    }
})
$('.ahr_form').on('click', function(){
    init_ahr_log($(this).data('ottype'), function(){
        $('#ahr_request_form').formValidation({
            message: 'This value is not valid',
            excluded: ':disabled',
            //        live: 'disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                ot_edit_date: {
                    validators: {
                        notEmpty: {
                            message: 'Date is required'
                        },
                    }
                },
                ot_edit_time: {
                    validators: {
                        notEmpty: {
                            message: 'Time is required'
                        },
                    }
                }, 
                ahr_reason: {
                    validators: {
                        notEmpty: {
                            message: 'Reason is required'
                        },
                    }
                },
            }
        }).on('success.form.fv', function (e, data) {
            e.preventDefault();
        });
        $('.timepicker').timepicker({minuteStep: 1});
        $('#tito_ahr_modal').modal('show');
    })
});
$('#tito_request_div, #tito_record_div').on('click', '.tito_approver_info', function(){
    // console.log($(this).data('titoid'));
    tito_approver_info($(this).data('titoid'));
});
$(function(){
    // console.log($($('#tito_request_tabs').find('.nav-link.m-tabs__link.active')).attr('href'));
    initPendingTitoRequestDatatable('destroy');
    titoRequestRecordDateRangePicker(function(){});
})

