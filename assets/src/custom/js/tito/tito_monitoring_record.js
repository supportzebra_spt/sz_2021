function humanTimeFormat(hours, callback) {
    var totalHours = parseInt(hours);
    var totalMinutes = Math.round(((hours - totalHours) * 60).toFixed(2));
    var totalTime = '';
    if (totalMinutes == 60) {
        totalMinutes = 0;
        totalHours++;
    }
    var totalTime = '';
    if ((totalHours == 0) && (totalMinutes == 1)) {
        totalTime = totalMinutes + " min";
    } else if ((totalHours == 0) && (totalMinutes != 1)) {
        totalTime = totalMinutes + " mins";
    } else if ((totalMinutes == 0) && (totalHours == 1)) {
        totalTime = totalHours + " hr";
    } else if ((totalMinutes == 0) && (totalHours != 1)) {
        totalTime = totalHours + " hrs";
    } else if ((totalHours >= 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " min";
    } else if ((totalHours == 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " mins";
    } else if ((totalHours == 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " min";
    } else if ((totalHours >= 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " mins";
    }
    callback(totalTime);
}
var titoRecordMonitoringDatatable = function () {
    var titoRecordMonitoring = function (classVal, accVal, empVal, statVal, typeVal, startDateVal, endDateVal, searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/tito/get_tito_record_monitoring_datatable",
                        params: {
                            query: {
                                class: classVal,
                                acc_id: accVal,
                                emp_id: empVal,
                                status: statVal,
                                type: typeVal,
                                search: searchVal,
                                start_date: startDateVal,
                                end_date: endDateVal
                            },
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: "dtrRequest_ID",
                title: "TITO-ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var html = "<td'>" + row.dtrRequest_ID.padLeft(8)+ "</td>";
                    return html;
                }
            }, {
                field: 'emp_id',
                width: 240,
                title: 'Requested By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null){
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#titoMonitoring" + row.dtrRequest_ID + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'dateRequest',
                width: 100,
                title: 'Schedule',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.dateRequest, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'dateCreated',
                width: 100,
                title: 'Filed On',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td>" + moment(row.dateCreated, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</td>";
                    return html;
                },
            }, {
                field: 'timetotal',
                width: 100,
                title: 'Time',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    var html ="";
                    humanTimeFormat(row.timetotal, function (formattedHours) {
                        // $('#additionalVal').text(formattedBct);
                        html = "<span class='text-center'>" + formattedHours + "</span>";
                    });
                    return html;
                },
            }, {
                field: 'description',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.status_ID) == 12) {
                        var badgeColor = 'btn-warning';
                    } else if (parseInt(row.status_ID) == 2) {
                        var badgeColor = 'btn-info';
                    } else if (parseInt(row.status_ID) == 5){
                        var badgeColor = 'btn-success';
                    } else if (parseInt(row.status_ID) == 6) {
                        var badgeColor = 'btn-danger';
                    }
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide tito_approver_info' data-titoid='"+row.dtrRequest_ID+"'>" + row.description + "</span>";
                    return html;
                },
            }, {
                field: 'action',
                width: 70,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<span><a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View details" onClick="checkTitoRequest('+row.dtrRequest_ID+',0,2)"><i class="fa fa-search"></i></a><span>';
                    return html;
                }
            }],
        };
        var datatable = $('#tito_record_monitoring_datatable').mDatatable(options);
    };
    return {
        init: function (classVal, accVal, empVal, statVal, typeVal, startDateVal, endDateVal, searchVal) {
            titoRecordMonitoring(classVal, accVal, empVal, statVal, typeVal, startDateVal, endDateVal, searchVal);
        }
    };
}();
function initRecordMonitoringDatatable(init_type){
    console.log("init record datatable here");
    let request_stat = $('.tito_rec_status:checked').map(function () {
        return $(this).data('stat'); 
    }).get();
    let type_stat = $('.tito_rec_type:checked').map(function () {
        return $(this).data('type'); 
    }).get();
    $('#tito_record_monitoring_datatable').mDatatable(init_type);
    dateRange = $('#tito_monitoring_record_date_range').val().split('-');
    titoRecordMonitoringDatatable.init($('#record_monitoring_class').val(), $('#record_monitoring_acc').val(), $('#record_monitoring_emp').val(), request_stat.toString(), type_stat.toString(), moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'), $('#tito_record_monitoring_search').val());
} 
function get_record_accounts(callback){
    dateRange = $('#tito_monitoring_record_date_range').val().split('-');
    $.when(fetchPostData({
        class: $('#record_monitoring_class').val(),
        start_date: moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'),
        end_date: moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'),
    }, '/tito/get_monitoring_record_tito_acc')).then(function (accounts) {
        let accountsObj = $.parseJSON(accounts.trim());
        let optionsAsString = "<option value='0'>ALL</option>";
        if(parseInt(accountsObj.exist)){
            $.each(accountsObj.record, function(index, value){
                optionsAsString += "<option value='" + value.accId + "'>" + toTitleCase(value.acc_name) + "</option>"
            })
        }
        $("#record_monitoring_acc").html(optionsAsString);
        $("#record_monitoring_acc").select2({
            placeholder: "Select an Account",
            width: '100%'
        }).trigger('change');
        callback(accountsObj);
    });
}
function get_record_employee(callback){
    let request_stat = $('.tito_rec_status:checked').map(function () {
        return $(this).data('stat'); 
    }).get();
    let type_stat = $('.tito_rec_type:checked').map(function () {
        return $(this).data('type'); 
    }).get();
    $.when(fetchPostData({
        class: $('#record_monitoring_class').val(),
        acc_id: $('#record_monitoring_acc').val(),
        type: type_stat.toString(),
        status: request_stat.toString(),
        start_date: moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'),
        end_date: moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'),
    }, '/tito/get_monitoring_record_tito_emp')).then(function (emp) {
        let empObj= $.parseJSON(emp.trim());
        let optionsAsString = "<option value='0'>ALL</option>";
        if (parseInt(empObj.exist)){
            $.each(empObj.record, function(index, value){
                optionsAsString += "<option value='" + value.empId + "'>" + toTitleCase(value.lname) + ", " + toTitleCase(value.fname) + "</option>"
            })
        }
        $('#record_monitoring_emp').html(optionsAsString);
        $('#record_monitoring_emp').select2({
            placeholder: "Select an Employee",
            width: '100%'
        }).trigger('change');
        initRecordMonitoringDatatable('destroy');
        callback(empObj);
    });
}
function init_monitoring_record_filter(){
    get_record_accounts(function(){
        get_record_employee(function(){});
    });
}
function titoMonitoringRecordDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#tito_monitoring_record_date_range').val(start + ' - ' + end);
        $('#tito_monitoring_record_date_range').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#tito_monitoring_record_date_range').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            init_monitoring_record_filter();
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);
    });
}
$('#record_monitoring_acc').on('change', function(){
    get_record_employee(function(){});
})
$('#record_monitoring_emp').on('change', function(){
    initRecordMonitoringDatatable('destroy');
})
$('.tito_rec_type, .tito_rec_status').on('click', function(){
    get_record_employee(function(){});
});
$('#tito_record_monitoring_search').keypress(function (e) {
    if (e.which == 13) {
        initRecordMonitoringDatatable('destroy');
        return false;    //<---- Add this line
    }
});
$('#record_monitoring_class').on('change', function(){
    init_monitoring_record_filter();
});
function titoRequestAction(act){
	var decision = (act=="5") ? "approve" : "disapprove";
	$("#tito_request_modal").modal("hide");
	 swal({
        title: 'Are you sure you want to <b> '+decision+' </b> this request ?',
        html: "Approval decision will not be reverted once finalized.</br></br></br><b>Approval Remarks</b>",
        type: 'warning',
        input: 'textarea',
        showCancelButton: true,
        confirmButtonText: 'Yes, '+decision+' it!',
        cancelButtonText: 'No',
        reverseButtons: true,
		inputAttributes: {
			autocapitalize: 'on',
			maxlength: 300,
			minlength: 5,
		},
		inputValidator: (value) => {
			if (!value) {
			  return 'You need to write something!'
			}
			if (value.length<5) {
			  return 'Please enter at least 5 characters.'
			}
			if (value.length>300) {
			  return 'Only 300 characters are allowed.'
			}
		  },
    }).then(function (result) {
        if (result.value) {
		 var  appr_message = result.value;
		 var  tito_ID = $("#tito_total_time").data("titoid");
		 var  approverid = $("#tito_reason").data("approverid");

		 var  ahridPre = $("#checkPre").data("ahrid");
		 var  ahridAfter = $("#checkAfter").data("ahrid");
		 var  ahridWord = $("#checkWord").data("ahrid");
        
		 var  checkWord = $("#checkWord").is(":checked");
		 var  checkPre = $("#checkPre").is(":checked");
		 var  checkAfter = $("#checkAfter").is(":checked");
		 var  isMonitoring = 1;
         arr = {tito_ID,act,approverid,ahridPre,ahridAfter,ahridWord,checkWord,checkPre,checkAfter,appr_message,isMonitoring};
		console.log(arr);
		 let request_stat = $('.tito_rec_status:checked').map(function () {
			return $(this).data('stat'); 
		}).get();
		let type_stat = $('.tito_rec_type:checked').map(function () {
			return $(this).data('type'); 
		}).get();
       $.ajax({			
				type: "POST",
				url:baseUrl +'/tito/titoRequestAct',
				data: arr,
				cache: false,
				success: function(output) {
				console.log(output);
				  var rs = JSON.parse(output);
					if (parseInt(rs["error"]) ==0) {
						$("#tito_request_modal").modal("hide");
						
						swal('You have successfully '+decision+'d the request!','','success');
						location.reload();
					} else {
							swal('Opps, Error!','Something went wrong!','error');
					}
				}
			}); 
 		   $("#tito_request_modal").modal("hide");
        } else if (result.dismiss === 'cancel') {
            $("#tito_request_modal").modal("show");
        }
    });
	 /*  */
}
$(function(){
    titoMonitoringRecordDateRangePicker(function(){
        init_monitoring_record_filter();
    });
});