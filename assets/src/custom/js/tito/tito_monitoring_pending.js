function humanTimeFormat(hours, callback) {
    var totalHours = parseInt(hours);
    var totalMinutes = Math.round(((hours - totalHours) * 60).toFixed(2));
    var totalTime = '';
    if (totalMinutes == 60) {
        totalMinutes = 0;
        totalHours++;
    }
    var totalTime = '';
    if ((totalHours == 0) && (totalMinutes == 1)) {
        totalTime = totalMinutes + " min";
    } else if ((totalHours == 0) && (totalMinutes != 1)) {
        totalTime = totalMinutes + " mins";
    } else if ((totalMinutes == 0) && (totalHours == 1)) {
        totalTime = totalHours + " hr";
    } else if ((totalMinutes == 0) && (totalHours != 1)) {
        totalTime = totalHours + " hrs";
    } else if ((totalHours >= 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " min";
    } else if ((totalHours == 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " mins";
    } else if ((totalHours == 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " min";
    } else if ((totalHours >= 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " mins";
    }
    callback(totalTime);
}
var titoPendingMonitoringDatatable = function () {
    var titoPendingMonitoring = function (classVal, accVal, empVal, statVal, typeVal, searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/tito/get_tito_pending_monitoring_datatable",
                        params: {
                            query: {
                                class: classVal,
                                acc_id: accVal,
                                emp_id: empVal,
                                status: statVal,
                                type: typeVal,
                                search: searchVal,
                            },
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: "dtrRequest_ID",
                title: "TITO-ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var html = "<td'>" + row.dtrRequest_ID.padLeft(8)+ "</td>";
                    return html;
                }
            }, {
                field: 'emp_id',
                width: 240,
                title: 'Requested By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null){
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#titoMonitoring" + row.dtrRequest_ID + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'dateRequest',
                width: 100,
                title: 'Schedule',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.dateRequest, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'dateCreated',
                width: 100,
                title: 'Filed On',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td>" + moment(row.dateCreated, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</td>";
                    return html;
                },
            }, {
                field: 'timetotal',
                width: 100,
                title: 'Time',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    var html ="";
                    humanTimeFormat(row.timetotal, function (formattedHours) {
                        // $('#additionalVal').text(formattedBct);
                        html = "<span class='text-center'>" + formattedHours + "</span>";
                    });
                    return html;
                },
            }, {
                field: 'description',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.status_ID) == 12) {
                        var badgeColor = 'btn-warning';
                        var dotAlert = "<span class='m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink mr-1' style='min-height: 7px;min-width: 7px;'></span>";
                    } else if (parseInt(row.status_ID) == 2) {
                        var badgeColor = 'btn-info';
                        var dotAlert = "";
                    }
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " tito_approver_info' data-titoid='"+row.dtrRequest_ID+"'>"+dotAlert+" " + row.description + "</span>";
                    return html;
                },

            }, {
                field: 'action',
                width: 70,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<span><a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View details" onClick="checkTitoRequest('+row.dtrRequest_ID+',0,1,'+row.status_ID+')"><i class="fa fa-search"></i></a><span>';
                    return html;
                }
            }],
        };
        var datatable = $('#tito_pending_monitoring_datatable').mDatatable(options);
    };
    return {
        init: function (classVal, accVal, empVal, statVal, typeVal, searchVal) {
            titoPendingMonitoring(classVal, accVal, empVal, statVal, typeVal, searchVal);
        }
    };
}();
function initPendingMonitoringDatatable(init_type){
    // console.log("init record datatable here");
    let request_stat = $('.tito_req_status:checked').map(function () {
        return $(this).data('stat'); 
    }).get();
    let type_stat = $('.tito_req_type:checked').map(function () {
        return $(this).data('type'); 
    }).get();
    $('#tito_pending_monitoring_datatable').mDatatable(init_type);
    titoPendingMonitoringDatatable.init($('#pending_monitoring_class').val(), $('#pending_monitoring_acc').val(), $('#pending_monitoring_emp_list').val(), request_stat.toString(), type_stat.toString(), $('#tito_pending_monitoring_search').val());
}
function get_pending_accounts(callback){
    $.when(fetchPostData({
        class: $('#pending_monitoring_class').val(),
    }, '/tito/get_monitoring_pending_tito_acc')).then(function (accounts) {
        let accountsObj = $.parseJSON(accounts.trim());
        let optionsAsString = "<option value='0'>ALL</option>";
        if(parseInt(accountsObj.exist)){
            $.each(accountsObj.record, function(index, value){
                optionsAsString += "<option value='" + value.accId + "'>" + toTitleCase(value.acc_name) + "</option>"
            })
        }
        $("#pending_monitoring_acc").html(optionsAsString);
        $("#pending_monitoring_acc").select2({
            placeholder: "Select an Account",
            width: '100%'
        }).trigger('change');
        callback(accountsObj);
    });
}
function get_pending_employee(callback){
    let request_stat = $('.tito_req_status:checked').map(function () {
        return $(this).data('stat'); 
    }).get();
    let type_stat = $('.tito_req_type:checked').map(function () {
        return $(this).data('type'); 
    }).get();
    $.when(fetchPostData({
        class: $('#pending_monitoring_class').val(),
        acc_id: $('#pending_monitoring_acc').val(),
        type: type_stat.toString(),
        status: request_stat.toString(),
    }, '/tito/get_monitoring_pending_tito_emp')).then(function (emp) {
        let empObj= $.parseJSON(emp.trim());
        let optionsAsString = "<option value='0'>ALL</option>";
        if (parseInt(empObj.exist)){
            $.each(empObj.record, function(index, value){
                optionsAsString += "<option value='" + value.empId + "'>" + toTitleCase(value.lname) + ", " + toTitleCase(value.fname) + "</option>"
            })
        }
        $('#pending_monitoring_emp_list').html(optionsAsString);
        $('#pending_monitoring_emp_list').select2({
            placeholder: "Select an Employee",
            width: '100%'
        }).trigger('change');
        initPendingMonitoringDatatable('destroy');
        callback(empObj);
    });
}
function filter_pending_tito_monitoring(){
    get_pending_accounts(function(){
        get_pending_employee(function(){
            // initPendingMonitoringDatatable('destroy');
        })
    });
}
$('#tito_pending_monitoring_search').keypress(function (e) {
    if (e.which == 13) {
        initPendingMonitoringDatatable('destroy');
        return false;    //<---- Add this line
    }
});
$('#pending_monitoring_emp_list').on('change', function(){
    initPendingMonitoringDatatable('destroy');
});
$('#pending_monitoring_class').on('change', function(){
    filter_pending_tito_monitoring();
});
$('#pending_monitoring_acc').on('change', function(){
    get_pending_employee(function(){})
});
$('.tito_req_type, .tito_req_status').on('click', function(){
    get_pending_employee(function(){})
});
$(function(){
    filter_pending_tito_monitoring();
});