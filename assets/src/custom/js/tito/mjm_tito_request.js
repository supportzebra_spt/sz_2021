$("#file_tito_request").click(function(){
	$("#tito_request_modal").removeClass("mjmView");
	$("#tito_sched_date").prop("disabled",false);
	$("#requestAhrBtn").prop("disabled",false);
	$("#requestAhrBtn").css("display","inline-table");
	$("#divReason1").css("display","block");
	$("#divReason2").css("display","none");
	$("#checkWord").prop("checked", false);
	$("#checkPre").prop("checked", false);
	$("#checkAfter").prop("checked", false);
	$(".mjm-Checkbox").css("display","inline-block");

});
$("#tito_shift").change(function(){
	changeTitoShift();
});

$("#tito_sched_date").change(function(){
 changeTitoShift();
// $('#tito_shift').trigger("change");

});
$("#tito_start_date,#tito_start_time").change(function(e){
	 var ischeck =  $("#tito_total_time").data("ischeck");
		if (ischeck) {
			checkTimeManualMJM();
			date_diff_start();
		}
}); 

$(".select2").click(function(e){
 	console.log("click sched");
	 $("#tito_total_time").data("ischeck",0);
}); 
$("#tito_start_date").click(function(e){
	var dateStart =  $("#tito_start_date").val();
	$('#tito_start_date').datepicker('setDate',dateStart);
	 $("#tito_total_time").data("ischeck",1);

});
$("#tito_end_date").click(function(e){
	 $("#tito_total_time").data("ischeck",1);
	var dateEnd =  $("#tito_end_date").val();
	$('#tito_end_date').datepicker('setDate',dateEnd);
});
$("#tito_start_time,#tito_end_time").click(function(e){
 	console.log("click");
	 $("#tito_total_time").data("ischeck",1);
}); 

$("#tito_end_date,#tito_end_time").change(function(e){
	 var ischeck =  $("#tito_total_time").data("ischeck");
		if (ischeck) {
			checkTimeManualMJM();
		date_diff_end();
		}
});
$('#checkWord').click(function() {
    if($(this).is(':checked')) {
		 var origdatestart = $("#tito_total_time").data("origdatestart");
		 var origdateend = $("#tito_total_time").data("origdateend");
		 var newdatestart = $("#tito_start_date").val()+" "+$("#tito_start_time").val();
		 var newdateend = $("#tito_end_date").val()+" "+$("#tito_end_time").val();
		 var acctimeid  = $('#tito_total_time').data('acctimeid');

      $.post(baseUrl +"/tito/checkWordTotal", {origdatestart,origdateend,newdatestart,newdateend,acctimeid}, function(result){
		var rs  = result.split("_");
		$("#ot_time_word").html(rs[2]);
		$("#checkWord").data("otstartdatetime",rs[3]);
		$("#checkWord").data("otenddatetime",rs[4]);
		$("#tito_total_time").data("otword",rs[0]);

	});
    } else {
      $("#ot_time_word").html("00 hr, 00 min");
    }
});

function changeTitoShift(){
	var time = $("#tito_shift option:selected").text().split("-");
	var acc_time_id = $("#tito_shift").val();
	$('#tito_start_time').timepicker('setTime', time[0].trim());
	$('#tito_end_time').timepicker('setTime', time[1].trim());
	$.post(baseUrl +"/tito/get_allowable_break_acc", {acc_time_id}, function(result){
		$('#tito_total_time').data('acctimeid', result);		
	});
	$("#tito_total_time").data("ischeck",0);
	$("#checkWord").prop("checked", false);
	$("#checkWord").prop("disabled", false);
	check_sched_attach_ahrs(function(disabledBool){
		if(!disabledBool){
			checkTimeMJM();
		}
	})
}
function checkTimeManualMJM(){
	
		var dateTimeStart = $("#tito_start_date").val()+" "+$("#tito_start_time").val();
		var dateTimeEnd = $("#tito_end_date").val()+" "+$("#tito_end_time").val();
		var acctimeid  = $('#tito_total_time').data('acctimeid');

 	 date_diff(dateTimeStart,dateTimeEnd,acctimeid) ;
}
function checkTimeMJM(){
	var dateStart =  moment($("#tito_sched_date").val()).format('YYYY-MM-DD');
	 // $('#tito_start_date').datepicker('setDate',dateStart);
	var time = $("#tito_shift option:selected").text().split("-");
	console.log(time);
	var timeStart =  time[0].trim();
	var timeEnd =  time[1].trim();
	$.post(baseUrl +"/tito/checkTimeOfShift", {timeStart,timeEnd}, function(result){
		var rs = result.split("|");
		var start = "";
		var end = "";
		if(rs[1]=="Tomorrow"){
 				end= moment(dateStart).add(1, 'days').format('YYYY-MM-DD')+" "+timeEnd;
				start  = dateStart+" "+timeStart;
			$("#tito_end_date").val(moment(dateStart).add(1, 'days').format('YYYY-MM-DD'));
			$("#tito_start_date").val(dateStart);

 		}else{
		if(timeStart.trim() == "12:00:00 AM"){
				end= moment(dateStart).add(1, 'days').format('YYYY-MM-DD')+" "+timeEnd;
				start  = dateStart+" "+timeStart;
			$("#tito_end_date").val(moment(dateStart).add(1, 'days').format('YYYY-MM-DD'));
			$("#tito_start_date").val(dateStart);

		 }else{
			start = dateStart+" "+timeStart;
			end = dateStart+" "+timeEnd;
 			$("#tito_end_date").val(dateStart);
			$("#tito_start_date").val(dateStart);

		 }
	 
		}
	 
	

	// var beginningTime = moment(start);
	// var endTime = moment(end);
	// var difftm= beginningTime.diff(endTime, 'minutes'); 
		// console.log(difftm);

 date_diff_orig(start, end)

	});


}
function date_diff_orig(start, end){
var acctimeid  = $('#tito_total_time').data('acctimeid');
	$.post(baseUrl +"/tito/checkTotalTime", {timeStart: start,timeEnd: end,acctimeid}, function(result){
		var rs  = result.split("_");
		console.log(1);
		$("#titoUndertime").css("display","none");	
		$("#titoLate").css("display","none");

		$("#tito_total_time").data("newtotal",rs[3]);
		$("#tito_total_time").html(rs[2]);
		// $("#ot_time_word").html(rs[2]);
		$("#tito_total_time").data("origtotal",rs[0]);
		$("#tito_total_time").data("origdatestart",start);
		$("#tito_total_time").data("origdateend",end);
	});
}

function date_diff_end() {
	var newend = $("#tito_end_date").val()+" "+$("#tito_end_time").val();
	var origend = $("#tito_total_time").data("origdateend");
	var acctimeid  = $('#tito_total_time').data('acctimeid');
	$("#checkWord").prop("checked", false);
	$("#ot_time_word").html("00 hr, 00 min");
		if($("#tito_end_date").val()!=""){
			$.post(baseUrl +"/tito/checkTotalTimeAfterShiftOT", {timeStart: origend,timeEnd: newend}, function(result){
			var rs  = result.split("_");
			var val  =parseFloat(rs[0]);
			console.log("last "+val);
				$("#titoUndertime").css("display","none");	
		
				$("#tito_total_time").data("otafter",val);
				if(val<0){
					$("#ot_time_after").html("00 hr, 00 min");
					$("#titoUndertime").css("display","inline-table");	
					 					
					$("#checkAfter").prop("checked", false);	
					$("#checkAfter").prop("disabled", true);	
				}else if(val==0){
					$("#ot_time_after").html("00 hr, 00 min");
					$("#titoUndertime").css("display","none");	
					 					
					$("#checkAfter").prop("checked", false);	
					$("#checkAfter").prop("disabled", true);	
				}else{
					$("#ot_time_after").html(rs[1]);
					$("#checkAfter").data("otstartdatetime",rs[3]); 
					$("#checkAfter").data("otenddatetime",rs[4]); 
					$("#titoUndertime").css("display","none");
					$("#checkAfter").prop("disabled", false);	
				}
			});
		}
}
function date_diff_start() {
	var newstart = $("#tito_start_date").val()+" "+$("#tito_start_time").val();
	var origstart = $("#tito_total_time").data("origdatestart");
	var acctimeid  = $('#tito_total_time').data('acctimeid');
	$("#checkWord").prop("checked", false);
	$("#ot_time_word").html("00 hr, 00 min");
	if($("#tito_start_date").val()!=""){
		$.post(baseUrl +"/tito/checkTotalTimePreShiftOT", {timeStart:newstart ,timeEnd: origstart}, function(result){
			var rs  = result.split("_");
			var val  =parseFloat(rs[0]);
			console.log(rs[0]);
			$("#tito_total_time").data("otpre",rs[0]);
			$("#titoLate").css("display","none");
			if(val<=0){
				$("#ot_time_pre").html("00 hr, 00 min");
				var s = (val<0) ? "inline-table": "none";
				$("#titoLate").css("display",s);
				$("#checkPre").prop("disabled", true);						
				$("#checkPre").prop("checked", false);						
			}else{
				$("#ot_time_pre").html(rs[1]);
				$("#checkPre").data("otstartdatetime",rs[2]); 
				$("#checkPre").data("otenddatetime",rs[3]); 
				$("#titoLate").css("display","none");
				$("#checkPre").prop("disabled", false);	
			}
		});
	}
}
function date_diff(start, end,acctimeid) {
		$.post(baseUrl +"/tito/checkTotalTime", {timeStart: start,timeEnd: end,acctimeid}, function(result){
		var rs  = result.split("_");
		console.log(2);
		$("#tito_total_time").html(rs[2]);
		// $("#ot_time_word").html(rs[2]);

		$("#tito_total_time").data("newtotal",rs[3]);
		$("#tito_total_time").data("newdatestart",start);
		$("#tito_total_time").data("newdateend",end);

		var origTotal = $("#tito_total_time").data("origtotal");
		var origDateStart = $("#tito_total_time").data("origdatestart");
		 // $("#tito_total_time").data("origdateend",end);
		
		});
}



//Approval Section
$(function(){
	// shiftLoad();

$('.timepicker').timepicker({minuteStep: 1});
$('#tito_shift').select2({
    placeholder: "Select a Shift",
    width: '100%'
});
});
function shiftLoad(acc_time_id,acc_id=null){
		$.ajax({			
			type: "POST",
			url:'/sz/tito/get_shifts',
			data:{acc_id},
			cache: false,
			success: function(shifts) {
				let shiftsObj = $.parseJSON(shifts.trim());
				// let optionsAsString =  "<option value='0'>--</option>";
				let optionsAsString =  "<option></option>";
				if (parseInt(shiftsObj.exist)) {
					$.each(shiftsObj.record, function (index, value) {
						var selected = (parseInt(value.acc_time_id)==parseInt(acc_time_id)) ? "selected" : "";
						optionsAsString += "<option value='"+value.acc_time_id+"' "+selected+" >" + value.time_start+" - "+value.time_end+"</option>";
					})
				}
				$('#tito_shift').html(optionsAsString);
			
			}
		});
}
 
function checkTitoRequest(dtrRequest_ID,dtrRequestApproval_ID,act=null,status_ID=null){
	if(act!=null){
		if(act==1){
			$("#btnDisApprove").css("display","inline-table");
			$("#btnApprove").css("display","inline-table");

		}else{
			$("#btnDisApprove").css("display","none");
			$("#btnApprove").css("display","none");
		}
	}
	if(status_ID!=null){
		if(status_ID==2){
			$("#btnDisApprove").css("display","none");
			$("#btnApprove").css("display","none");
		}else{
			$("#btnDisApprove").css("display","inline-table");
			$("#btnApprove").css("display","inline-table");
		}
	}
	$("#tito_request_modal").addClass("mjmView");
		$("#tito_sched_date").prop("disabled",true);
	$("#requestAhrBtn").prop("disabled",true);
	$("#requestAhrBtn").css("display","none");
	$("#divReason1").css("display","none");
	$("#divReason2").css("display","block");
	$(".mjm-Checkbox").css("display","none");

	$.ajax({			
			type: "POST",
			url:'/sz/tito/checkTitoRequest',
			data: {dtrRequest_ID},
			cache: false,
			success: function(output) {
				$("#tito_request_modal").modal("show");
				  var rs = JSON.parse(output);
				  console.log(rs);
				  $(".no-edit").prop("disabled", true);
					 shiftLoad(rs["acc_time_id"],rs["acc_id"]);
 				    $('#tito_sched_date').val(rs["dateRequest"]);
					// $('#tito_shift').val(parseInt(rs["acc_time_id"])).change();
				// $("#tito_sched_date").select2("val", rs["dateRequest"]);
 

 				  	$('#tito_start_date').val(rs["startDateWorked"]);
 				  	$('#tito_start_time').val(rs["startTimeWorked"]);
				  	$('#tito_end_date').val(rs["endDateWorked"]);
				  	$('#tito_end_time').val(rs["endTimeWorked"]);
					$('#tito_reason').val(rs["message"]);
					$('#reasonRecord').html(rs["message"]);  
				  	$('#tito_reason').data("approverid",dtrRequestApproval_ID);
 					humanTimeFormat(parseFloat(rs["timetotal"]),function (formattedHours) {
						$("#tito_total_time").html("<span class='text-center'>" + formattedHours + "</span>");
					}); 
					$("#tito_total_time").data("titoid",rs["dtrRequest_ID"]);
						  // checkTimeManualMJM();
						    // changeTitoShift();
						   // date_diff_end();
						   // date_diff_start();

					//Type 
					// 1 = AFTER
					// 2 = PRE
					// 3 = WORD
					$("#checkAfter").prop("checked", false); 

					$("#checkPre").prop("checked", false);

					$("#checkWord").prop("disabled", true);
					$("#checkWord").prop("checked", false);
					$("#ot_time_pre").html("<span class='text-center'>00 hr, 00 min	</span>");
					$("#ot_time_after").html("<span class='text-center'>00 hr, 00 min	</span>");
					$("#ot_time_word").html("<span class='text-center'>00 hr, 00 min	</span>");
					 if(rs["withAhrRecord"].length>0){
						   $.each(rs["withAhrRecord"], function (index, value) {
							   console.log(value.additionalHourTypeId);
								 if(parseInt(value.additionalHourTypeId)==1){
									 $("#checkAfter").prop("disabled", false); 
									 $("#checkAfter").prop("checked", true);
									 $("#checkAfter").data("ahrid",value.additionalHourRequestId);									 
									humanTimeFormat(parseFloat(value.totalOtHours),function (formattedHours) {
										$("#ot_time_after").html("<span class='text-center'>" + formattedHours + "</span>");
									}); 

									 
								 }
								 if(parseInt(value.additionalHourTypeId)==2){
									 $("#checkPre").prop("disabled", false);
									 $("#checkPre").prop("checked", true);
									 $("#checkPre").data("ahrid",value.additionalHourRequestId);
									humanTimeFormat(parseFloat(value.totalOtHours),function (formattedHours) {
										$("#ot_time_pre").html("<span class='text-center'>" + formattedHours + "</span>");
									}); 


								 }
								 if(parseInt(value.additionalHourTypeId)==3){ 
									 $("#checkWord").prop("disabled", false);
									 $("#checkWord").prop("checked", true);
									 $("#checkWord").data("ahrid",value.additionalHourRequestId);
									humanTimeFormat(parseFloat(value.totalOtHours),function (formattedHours) {
										$("#ot_time_word").html("<span class='text-center'>" + formattedHours + "</span>");
									}); 
								 }
							});
					 }
					
// data-origtotal="0" data-origdatestart="" data-origdateend="" data-newdatestart="" data-newdateend="" data-newtotal="0" data-acctimeid="0" data-otpre="0" data-otafter="0" data-otword="0"				
			}
		});
	
}
 