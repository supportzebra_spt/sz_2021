function titoRequestAction(act){
	var decision = (act=="5") ? "approve" : "disapprove";
	$("#tito_request_modal").modal("hide");
	 swal({
        title: 'Are you sure you want to <b> '+decision+' </b> this request ?',
        html: "Approval decision will not be reverted once finalized.</br></br></br><b>Approval Remarks</b>",
        type: 'warning',
        input: 'textarea',
        showCancelButton: true,
        confirmButtonText: 'Yes, '+decision+' it!',
        cancelButtonText: 'No',
        reverseButtons: true,
		inputAttributes: {
			autocapitalize: 'on',
			maxlength: 300,
			minlength: 5,
		},
		inputValidator: (value) => {
			if (!value) {
			  return 'You need to write something!'
			}
			if (value.length<5) {
			  return 'Please enter at least 5 characters.'
			}
			if (value.length>300) {
			  return 'Only 300 characters are allowed.'
			}
		  },
    }).then(function (result) {
        if (result.value) {
		 var  appr_message = result.value;
		 var  tito_ID = $("#tito_total_time").data("titoid");
		 var  approverid = $("#tito_reason").data("approverid");

		 var  ahridPre = $("#checkPre").data("ahrid");
		 var  ahridAfter = $("#checkAfter").data("ahrid");
		 var  ahridWord = $("#checkWord").data("ahrid");
        
		 var  checkWord = $("#checkWord").is(":checked");
		 var  checkPre = $("#checkPre").is(":checked");
		 var  checkAfter = $("#checkAfter").is(":checked");
		 var  isMonitoring = 0;

         arr = {tito_ID,act,approverid,ahridPre,ahridAfter,ahridWord,checkWord,checkPre,checkAfter,appr_message,isMonitoring};
		console.log(arr);
		
       $.ajax({			
				type: "POST",
				url:baseUrl +'/tito/titoRequestAct',
				data: arr,
				cache: false,
				success: function(output) {
				console.log(output);
				  var rs = JSON.parse(output);
					if (parseInt(rs["error"]) ==0) {
						$("#tito_request_modal").modal("hide");
						initPendingTitoApprovalDatatable('destroy');
						swal('You have successfully '+decision+'d the request!','','success');
					} else {
							swal('Opps, Error!','Something went wrong!','error');
					}
				}
			}); 
 		   $("#tito_request_modal").modal("hide");
        } else if (result.dismiss === 'cancel') {
            $("#tito_request_modal").modal("show");
        }
    });
	 /*  */
}