var BootstrapSelect = function () {
    var options = function (elementId, titleVal, liveSearchBool, liveSearchStyleVal, selectedTextFormatVal, cuntSelectedTextVal) {
        $(elementId).selectpicker({
            title: titleVal,
            liveSearch: liveSearchBool,
            liveSearchStyle: liveSearchStyleVal,
            selectedTextFormat: selectedTextFormatVal,
            countSelectedText: cuntSelectedTextVal,
            // container: ".m-content",
            // dropupAuto: 'true',
            // virtualScroll: 'true',
            size: 'auto'
        });
    }
    return {
        init: function (elementId, titleVal, liveSearchBool, liveSearchStyleVal, selectedTextFormatVal, cuntSelectedTextVal) {
            options(elementId, titleVal, liveSearchBool, liveSearchStyleVal, selectedTextFormatVal, cuntSelectedTextVal);
        }
    };
}();
function show_active_content(element){
    // console.log(element);
    $('.monitoring_content').hide();
    $(element).fadeIn();
    // console.log($(element));
}
function titoReportDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#tito_report_date_range').val(start + ' - ' + end);
        $('#tito_report_date_range').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            // console.log('record date range test');
            $('#tito_report_date_range').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            // initIrAbleDtrViolationRecordMonitoring(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);
    });
}
function get_access_content(){
    $.when(fetchGetData('/tito/get_access_content')).then(function (content) {
        let contentHtml = $.parseJSON(content.trim());
        console.log(contentHtml);
        $('#tito_monitoring_access').html(contentHtml);
    });
}
function check_access(){
    $.when(fetchGetData('/tito/get_user_monitoring_access')).then(function (access) {
        let accessObj = $.parseJSON(access.trim());
        if(parseInt(accessObj.exist)){
            if(accessObj.access_type == 'admin'){
                get_access_content();
                $('#access_nav').show();
            }else{
                $('#tito_monitoring_access').empty();
                $('#tito_monitoring_access').hide();
                $('#access_nav').hide();
            }
        }
    });
}
function init_monitoring_content_fields(navcontent, callback){
    $("#tito_monitoring_nav").find('.li_nav').removeClass('li_nav');
    $("#tito_monitoring_nav").find('.li_nav_text').removeClass('li_nav_text');
    let nav_anchor = $("#tito_monitoring_nav").find("[data-navcontent='"+navcontent+"']").parent();
    let nav_text = $("#tito_monitoring_nav").find("[data-navcontent='"+navcontent+"']").children('span');
    if(navcontent == "tito_monitoring_access"){
        init_account_filter();
        $(nav_anchor).addClass('li_nav');
        $(nav_text).addClass('li_nav_text');
    }else if(navcontent == "tito_record"){
        $(nav_anchor).addClass('li_nav');
        $(nav_text).addClass('li_nav_text');
    }else if(navcontent == "tito_monitoring"){
        $(nav_anchor).addClass('li_nav');
        $(nav_text).addClass('li_nav_text');
    }
    callback(1);
}
$('.titoMainNav').click(function(){
    // console.log($(this).data('navcontent'));
    let nav_content = $(this).data('navcontent');
    init_monitoring_content_fields(nav_content, function(val){
        // console.log(val())
        show_active_content('#'+nav_content+'');
    });

})
$('.m-select2').select2({
    placeholder: "Select an Employee",
    width: '100%'
});
$('#tito_pending_monitoring_div, #tito_record_monitoring_div').on('click', '.tito_approver_info', function(){
    console.log($(this).data('titoid'));
    tito_approver_info($(this).data('titoid'));
});
$(function(){
    $("#tito_monitoring_nav").find("[data-navcontent='tito_monitoring']").parent().addClass('li_nav');
    $("#tito_monitoring_nav").find("[data-navcontent='tito_monitoring']").children('span').addClass('li_nav_text');    
    titoReportDateRangePicker(function(dateRanges){});
    check_access();
})