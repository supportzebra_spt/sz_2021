function humanTimeFormat(hours, callback) {
    var totalHours = parseInt(hours);
    var totalMinutes = Math.round(((hours - totalHours) * 60).toFixed(2));
    var totalTime = '';
    if (totalMinutes == 60) {
        totalMinutes = 0;
        totalHours++;
    }
    var totalTime = '';
    if ((totalHours == 0) && (totalMinutes == 1)) {
        totalTime = totalMinutes + " min";
    } else if ((totalHours == 0) && (totalMinutes != 1)) {
        totalTime = totalMinutes + " mins";
    } else if ((totalMinutes == 0) && (totalHours == 1)) {
        totalTime = totalHours + " hr";
    } else if ((totalMinutes == 0) && (totalHours != 1)) {
        totalTime = totalHours + " hrs";
    } else if ((totalHours >= 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " min";
    } else if ((totalHours == 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " mins";
    } else if ((totalHours == 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " min";
    } else if ((totalHours >= 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " mins";
    }
    callback(totalTime);
}
var pendingApprovalDatatable = function () {
    var pendingApproval = function (levelVal, accIdVal, empIdVal, typeVal, searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/tito/get_tito_pending_approval_datatable",
                        params: {
                            query: {
                                level: levelVal,
                                type: typeVal,
                                search: searchVal,
                                acc_id: accIdVal, 
                                emp_id: empIdVal
                            },
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: "dtrRequest_ID",
                title: "TITO-ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var html = "<td'>" + row.dtrRequest_ID.padLeft(8)+ "</td>";
                    return html;
                }
            }, {
                field: 'emp_id',
                width: 240,
                title: 'Requested By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null){
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#titoAccess" + row.dtrRequest_ID + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'dateRequest',
                width: 100,
                title: 'Schedule',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.dateRequest, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'dateCreated',
                width: 100,
                title: 'Filed On',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td>" + moment(row.dateCreated, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</td>";
                    return html;
                },
            }, {
                field: 'timetotal',
                width: 100,
                title: 'Time',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    var html ="";
                    humanTimeFormat(row.timetotal, function (formattedHours) {
                        // $('#additionalVal').text(formattedBct);
                        html = "<span class='text-center'>" + formattedHours + "</span>";
                    });
                    return html;
                },
            }, {
                field: 'deadline',
                width: 150,
                title: 'Deadline',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.deadline, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</span>";
                    return html;
                },
            }, {
                field: 'action',
                width: 70,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<span><a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" onClick="checkTitoRequest('+row.dtrRequest_ID+','+row.dtrRequestApproval_ID+',1)"><i class="fa fa-search"></i></a><span>';
                    return html;
                }
            }],
        };
        var datatable = $('#tito_pending_approval_datatable').mDatatable(options);
    };
    return {
        init: function (levelVal, accIdVal, empIdVal, typeVal, searchVal) {
            pendingApproval(levelVal, accIdVal, empIdVal, typeVal, searchVal);
        }
    };
}();
var titoApprRecordDatatable = function () {
    var titoApprRecord = function (accVal, empVal, statVal, typeVal, searchVal, startDateVal, endDateVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/tito/get_tito_record_approval_datatable",
                        params: {
                            query: {
                                status: statVal,
                                acc_id: accVal,
                                emp_id: empVal,
                                type: typeVal,
                                search: searchVal,
                                start_date: startDateVal, 
                                end_date: endDateVal
                            },
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: "dtrRequest_ID",
                title: "TITO-ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var html = "<td'>" + row.dtrRequest_ID.padLeft(8)+ "</td>";
                    return html;
                }
            },{
                field: 'emp_id',
                width: 240,
                title: 'Requested By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null){
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#titoAccess" + row.dtrRequest_ID + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'dateRequest',
                width: 100,
                title: 'Schedule',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.dateRequest, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'dateCreated',
                width: 100,
                title: 'Filed On',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<td>" + moment(row.dateCreated, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</td>";
                    return html;
                },
            }, {
                field: 'timetotal',
                width: 100,
                title: 'Time',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    var html ="";
                    humanTimeFormat(row.timetotal, function (formattedHours) {
                        // $('#additionalVal').text(formattedBct);
                        html = "<span class='text-center'>" + formattedHours + "</span>";
                    });
                    return html;
                },
            }, {
                field: 'description',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.approvalStatus_ID) == 12) {
                        var badgeColor = 'btn-warning';
                    } else if (parseInt(row.approvalStatus_ID) == 2) {
                        var badgeColor = 'btn-info';
                    } else if (parseInt(row.approvalStatus_ID) == 5){
                        var badgeColor = 'btn-success';
                    } else if (parseInt(row.approvalStatus_ID) == 6) {
                        var badgeColor = 'btn-danger';
                    }
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide tito_approver_info' data-titoid='"+row.dtrRequest_ID+"'>" + row.description + "</span>";
                    return html;
                },
            }, {
                field: 'action',
                width: 70,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<span><a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" onClick="checkTitoRequest('+row.dtrRequest_ID+','+row.dtrRequestApproval_ID+',2)"><i class="fa fa-search"></i></a><span>';
                    return html;
                }
            }],
        };
        var datatable = $('#tito_record_approval_datatable').mDatatable(options);
    };
    return {
        init: function (accVal, empVal, statVal, typeVal, searchVal, startDateVal, endDateVal) {
            titoApprRecord(accVal, empVal, statVal, typeVal, searchVal, startDateVal, endDateVal);
        }
    };
}();
function get_pending_accounts(element, levelVal, callback){
    $.when(fetchPostData({
        level: levelVal
    }, '/tito/get_pending_approval_accounts')).then(function (accounts) {
        let accountsObj = $.parseJSON(accounts.trim());
        let optionsAsString = "<option value='0'>ALL</option>";
        if(parseInt(accountsObj.exist)){
            $.each(accountsObj.record, function(index, value){
                optionsAsString += "<option value='" + value.accId + "'>" + toTitleCase(value.acc_name) + "</option>"
            })
        }
        $(element).html(optionsAsString);
        $(element).select2({
            placeholder: "Select an Account",
            width: '100%'
        }).trigger('change');
        callback(accountsObj);
    });
}
function get_record_accounts(callback){
    dateRange = $('#tito_appr_record_date_range').val().split('-');
    $.when(fetchPostData({
        start_date: moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'),
        end_date: moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD')
    }, '/tito/get_record_approval_accounts')).then(function (accounts) {
        let accountsObj = $.parseJSON(accounts.trim());
        let optionsAsString = "<option value='0'>ALL</option>";
        if(parseInt(accountsObj.exist)){
            $.each(accountsObj.record, function(index, value){
                optionsAsString += "<option value='" + value.accId + "'>" + toTitleCase(value.acc_name) + "</option>"
            })
        }
        $("#recordApprAcc").html(optionsAsString);
        $("#recordApprAcc").select2({
            placeholder: "Select an Account",
            width: '100%'
        }).trigger('change');
        callback(accountsObj);
    });
}
function get_pending_employee(element, levelVal, accIdVal, callback){
    $.when(fetchPostData({
        level: levelVal,
        account_id: accIdVal
    }, '/tito/get_pending_approval_emp')).then(function (emp) {
        let empObj= $.parseJSON(emp.trim());
        let optionsAsString = "<option value='0'>ALL</option>";
        if (parseInt(empObj.exist)){
            $.each(empObj.record, function(index, value){
                optionsAsString += "<option value='" + value.empId + "'>" + toTitleCase(value.lname) + ", " + toTitleCase(value.fname) + "</option>"
            })
        }
        $(element).html(optionsAsString);
        $(element).select2({
            placeholder: "Select an Account",
            width: '100%'
        }).trigger('change');
        callback(empObj);
    });
}
function get_record_employee(accIdVal, callback){
    dateRange = $('#tito_appr_record_date_range').val().split('-');
    $.when(fetchPostData({
        account_id: accIdVal,
        start_date: moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'),
        end_date: moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD')
    }, '/tito/get_record_approval_emp')).then(function (emp) {
        let empObj= $.parseJSON(emp.trim());
        let optionsAsString = "<option value='0'>ALL</option>";
        if (parseInt(empObj.exist)){
            $.each(empObj.record, function(index, value){
                optionsAsString += "<option value='" + value.empId + "'>" + toTitleCase(value.lname) + ", " + toTitleCase(value.fname) + "</option>"
            })
        }
        $("#recordApprEmp").html(optionsAsString);
        $("#recordApprEmp").select2({
            placeholder: "Select an Account",
            width: '100%'
        }).trigger('change');
        callback(empObj);
    });
}
function initPendingTitoApprovalDatatable(init_type){
    let type_stat = $('.tito_req_type:checked').map(function () {
        return $(this).data('type'); 
    }).get();
    $('#tito_pending_approval_datatable').mDatatable(init_type);
    pendingApprovalDatatable.init($('#apprLevel').val(), $('#pendingApprAccounts').val(), $('#pendingApprEmp').val(), type_stat.toString(), $('#pending_tito_approval_search').val());
}
function initRecordTitoApprovalDatatable(init_type){
    console.log("init record datatable here");
    let request_stat = $('.tito_rec_status:checked').map(function () {
        return $(this).data('stat'); 
    }).get();
    let type_stat = $('.tito_rec_type:checked').map(function () {
        return $(this).data('type'); 
    }).get();
    $('#tito_record_approval_datatable').mDatatable(init_type);
    dateRange = $('#tito_appr_record_date_range').val().split('-');
    titoApprRecordDatatable.init($('#recordApprAcc').val(), $('#recordApprEmp').val(), request_stat.toString(), type_stat.toString(), $('#appr_record_tito_search').val(), moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'));
}
function init_pending_tito_filter(){
    get_pending_accounts('#pendingApprAccounts', $('#apprLevel').val(), function(){
        get_pending_employee('#pendingApprEmp', $('#apprLevel').val(), $('#pendingApprAccounts').val(), function(){
            initPendingTitoApprovalDatatable('destroy');
        })
    });
}
function init_record_appr_tito_filter(){
    get_record_accounts(function(){
        get_record_employee($('#recordApprAcc').val(), function(){
            initRecordTitoApprovalDatatable('destroy');
        })
    })
}
function titoApprRecordDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#tito_appr_record_date_range').val(start + ' - ' + end);
        $('#tito_appr_record_date_range').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#tito_appr_record_date_range').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            init_record_appr_tito_filter();
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);
    });
}
$('#apprLevel').on('change', function(){
    init_pending_tito_filter();
})
$('#pendingApprAccounts').on('change', function(){
    get_pending_employee('#pendingApprEmp', $('#apprLevel').val(), $('#pendingApprAccounts').val(), function(){
        initPendingTitoApprovalDatatable('destroy');
    })
})
$('#recordApprAcc').on('change', function(){
    get_record_employee($('#recordApprAcc').val(), function(){
        initRecordTitoApprovalDatatable('destroy');
    });
})
$('#pendingApprEmp').on('change', function(){
    initPendingTitoApprovalDatatable('destroy');
});
$('#recordApprEmp').on('change', function(){
    initRecordTitoApprovalDatatable('destroy');
})
$('.tito_req_type').on('click', function(){
    initPendingTitoApprovalDatatable('destroy');
})
$('#pending_tito_approval_search').keypress(function (e) {
    if (e.which == 13) {
        initPendingTitoApprovalDatatable('destroy');
        return false;    //<---- Add this line
    }
});
$('#appr_record_tito_search').keypress(function (e) {
    if (e.which == 13) {
        initRecordTitoApprovalDatatable('destroy');
        return false;    //<---- Add this line
    }
});
$('.tito_appr_nav').on('click', function(){
    console.log($(this).find('a').attr('href'));
    if($(this).find('a').attr('href') == '#pending_appr_tito'){
        initPendingTitoApprovalDatatable('destroy');
    }else{
        titoApprRecordDateRangePicker(function(){
            init_record_appr_tito_filter();
        });
        // initRecordTitoRequestDatatable('destroy');
    }
});
$('#tito_pending_approval_div, #tito_record_approval_div').on('click', '.tito_approver_info', function(){
    console.log($(this).data('titoid'));
    tito_approver_info($(this).data('titoid'));
});
$('.tito_rec_type, .tito_rec_status').on('click', function(){
    initRecordTitoApprovalDatatable('destroy');
});
$(function(){
    init_pending_tito_filter(function(){});
})