function tito_approver_info(tito_id) {
    $.when(fetchPostData({
        titoId: tito_id,
    }, '/tito/get_tito_approver_info')).then(function (approval) {
        var approvalObj = $.parseJSON(approval.trim());
        var approvalHTM = "";
        console.log(approvalObj);
        if(parseInt(approvalObj.exist) > 0){
            $.each(approvalObj.record, function (index, value) {
                console.log(value);
                var icon = "";
                var color = "";
                var animate = "";
                var animateI = "";
                var current = "";
                var stat = "";
                var pic = imagePath + "" + value.pic;
                var dated = moment(value.dateTimeStatus, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y h:mm A');
                var elementId = "#requestApproval" + value.additionalHourRequestApproval_ID + "";
                if (parseInt(value.approvalStatus_ID) == 5) {
                    icon = "fa fa-thumbs-o-up";
                    color = "btn-success";
                    stat = value.description;
                    dated = "Approved last " + dated;
                } else if (parseInt(value.approvalStatus_ID) == 6) {
                    icon = "fa fa-thumbs-o-down";
                    color = "btn-danger";
                    stat = value.description;
                    dated = "Disapproved last " + dated;
                } else if ((parseInt(value.approvalStatus_ID) == 2) || (parseInt(value.approvalStatus_ID) == 4)) {
                    icon = "fa fa-spinner";
                    color = "btn-info";
                    animate = "fa-spin";
                    stat = "pending";
                    dated = "Pending since " + dated;
                    if (parseInt(value.approvalStatus_ID) == 4){
                        current = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink ml-2" style="min-height: 7px;min-width: 7px;"></span>';
                    }
                } else if (parseInt(value.approvalStatus_ID) == 12){
                    icon = "fa fa-exclamation-circle";
                    color = "btn-warning";
                    animateI = "m-animate-blink";
                    stat = value.description;
                    dated = "Deadline was on "+ moment(value.deadline, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y h:mm A');
                }
                else if (parseInt(value.approvalStatus_ID) == 7) {
                    icon = "fa fa-remove";
                    color = "btn-primary";
                    stat = value.description;
                    dated = "Cancelled last " + dated;
                }
                console.log(stat);
                if(value.remarks == null){
                    var note = "No Notes found.";
                }else{
                    var note =  value.remarks;                
                }
                if(value.mname == ''){
                    var midName = "";
                }else{
                    var midName = value.mname.charAt(0)+"."
                }
                console.log(stat);
                approvalHTM += '<div class="m-portlet bg-secondary">' +
                    '<div class="col-md-12 pt-2 pb-2" style="background: #505a6b30;">' +
                    '<div class="row">' +
                    '<div class="col-7 col-sm-7 col-md-7 pt-2">APPROVER <span>' + value.approvalLevel + '</span>'+current+'</div>' +
                    '<div class="col-5 col-sm-5 col-md-5">' +
                    '<div class="btn pull-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;background: #505a6b;">' +
                    '<div class="btn '+color+' m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air '+animate+'" style="width: 25px !important;height: 25px !important;">' +
                    '<i class="' + icon + ' '+animateI+'" style="font-size: 17 px;"></i>' +
                    '</div>' +
                    '<span class="button-content text-light text-capitalize pl-2">' + stat + '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>  ' +
                    '<div class="m-portlet__body pt-3 pl-4 pb-3 pr-4">' +
                    '<div class="row">' +
                    '<div class="col-2 col-sm-2 col-md-2 d-none d-sm-block">' +
                    '<img src="' + pic + '" onerror="noImageFound(this)" width="58" alt="" class="mx-auto rounded-circle" id="' + elementId + '">' +
                    '</div>' +
                    '<div class="col-12 col-sm-10 col-md-10 pl-1">' +
                    '<div class="col-md-12 font-weight-bold" style="font-size: 15px;">'+value.fname+' '+midName+' '+value.lname+'</div>' +
                    '<div class="col-md-12" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: darkcyan;">'+value.positionDescription+'</div>' +
                    '<div class="col-md-12" style="font-size: 11px;margin-top: -3px;font-weight: bolder;">' + dated + '</div>' +
                    '<div class="col-md-12 mt-3" style="font-size: 12px;"><i> - '+note+'</i>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-12 pt-3">' +
                    // '<span class="pull-right" style="font-size: 12px;">'+moment(value.dateTimeStatus, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y h:mm A')+'</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            });
            $('#approvalBody').html(approvalHTM);
            $('#approversInfoModal').modal('show');
        }else{
            swal(
                'Approvers not found','It seems that the approver records are missing. Please report this error immediately to the System Administrator','error'
            )
        }
    });
}
