function getRequestStatusCount(callback) {
    $.when(fetchGetData('/additional_hour/get_monitoring_request_stat_count')).then(function (countStat) {
        var countStatObj = $.parseJSON(countStat.trim());
        if (parseInt(countStatObj.request)>0){
            $('#ahrRequests').find('.requestMonitor').addClass('m-animate-shake');
        }else{
            $('#ahrRequests').find('.requestMonitor').removeClass('m-animate-shake');
        }
        if (parseInt(countStatObj.pending) > 0){
            $('#ahrRequests').find('.pendingMonitor').addClass('fa-spin');
        }else{
            $('#ahrRequests').find('.pendingMonitor').removeClass('fa-spin');
        }
        if (parseInt(countStatObj.missed) > 0){
            $('#ahrRequests').find('.missedMonitor').addClass('m-animate-blink');
            $('.dueCount').text(countStatObj.missed)
            $('#dueNotif').removeClass('d-none');
            $('#dueNotif')[0].removeAttribute('style');
        }else{
            $('#dueNotif').addClass('d-none');
            $('#ahrRequests').find('.missedMonitor').removeClass('m-animate-blink');
        }
        $('#monitoringRequestCount').text(countStatObj.request);
        $('#monitoringPendingCount').text(countStatObj.pending);
        $('#monitoringMissedCount').text(countStatObj.missed);
        callback(countStatObj);
    });
}

function getRecordStatusCount(callback) {
    var dateRange = $('#monitoringRecordDateRange').val().split('-');
    $.when(fetchPostData({
            startDate: moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'),
            endDate: moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD')
        }, '/additional_hour/get_monitoring_record_stat_count')).then(function (countStat) {
        var countStatObj = $.parseJSON(countStat.trim());
        $('#monitoringRecordCount').text(countStatObj.record);
        $('#monitoringApprovedCount').text(countStatObj.approved);
        $('#monitoringDisapprovedCount').text(countStatObj.disapproved);
        callback(countStatObj);
    });
}

function hide_quick_approval_buttons(){
    $('.quick_approval_btns').fadeOut();
}
var requestAhrDatatable = function () {
    var requestAhr = function (requestStat, userId, searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour/get_ahr_requests",
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            getRequestStatusCount(function(statCount){});
                            return dataSet;
                        },
                        params: {
                            query: {
                                status: requestStat,
                                user: userId,
                                requestAhrSearch: searchVal
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#accountRenderedAhrAssign'),
            },
            // columns definition
            columns: [{
                field: "requestId",
                title: '<label class="m-checkbox"><input id="all_check" type="checkbox"><span style="margin-left: 2px;"></span></label>',
                width: 35,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '';
                    if (parseInt(row.approvalStatus_ID) == 12) {
                        html = '<div style="margin-top:1px"><label class="m-checkbox"><input class="rowcheck" type="checkbox" data-ahrid="'+row.additionalHourRequestId+'"><span></span></label></div>';
                    }
                    return html;
                }
            },{
                field: "additionalHourRequestId",
                title: "AHR ID",
                width: 70,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var ahrId = row.additionalHourRequestId.padLeft(8);
                    var html = "<tr class='row'><td class = 'col-md-1'>" + ahrId + "</td>";
                    return html;
                }
            }, {
                field: 'requestor',
                width: 250,
                title: 'Requested By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = imagePath + "" + row.pic;
                    var elementId = "#requestMonitoring" + row.userId + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'sched_date',
                width: 120,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'status',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                   if (parseInt(row.approvalStatus_ID) == 12) {
                       var badgeColor = 'btn-warning';
                       var dotAlert = "<span class='m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink mr-1' style='min-height: 7px;min-width: 7px;'></span>";
                   } else if (parseInt(row.approvalStatus_ID) == 2) {
                       var badgeColor = 'btn-info';
                       var dotAlert = "";
                   }
                   var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + "' onclick='approverInfo(" + row.additionalHourRequestId + ")'>" + dotAlert + " " + row.statusDescription + "</span>";
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View Details" onclick="ahrRequestInit(' + row.additionalHourRequestId + ',' + row.approvalStatus_ID + ',' + row.additionalHourTypeId + ', \'' + row.requestType + '\')"><i class="la la-edit"></i></a>';
                    return html;
                },
            }],
        };
        options.search = {
            input: $('#requestAhrSearch'),
        };
        var datatable = $('#requestAhrTable').mDatatable(options);
        console.log(datatable);
    };
    return {
        init: function (requestStat, userId, searchVal) {
            requestAhr(requestStat, userId, searchVal);
        }
    };
}();


var monitoringRecordAhrDatatable = function () {
    var monitoringRecordAhr = function (statusVal, userId, searchVal, dateFrom, dateTo) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour/get_monitoring_records",
                        params: {
                            query: {
                                status: statusVal,
                                user: userId,
                                monitoringRecordSearch: searchVal,
                                rangeStart: dateFrom,
                                rangeEnd: dateTo
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            getRecordStatusCount(function(monitoringRecordCount){});
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#monitoringRecordSearch'),
            },
            // columns definition
            columns: [{
                field: "additionalHourRequestId",
                title: "AHR ID",
                width: 70,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var ahrId = row.additionalHourRequestId.padLeft(8);
                    var html = "<tr class='row'><td class = 'col-md-1'>" + ahrId + "</td>";
                    return html;
                }
            }, {
                field: 'requestor',
                width: 260,
                title: 'Requested By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = imagePath + "" + row.pic;
                    var elementId = "#recordMonitoring" + row.userId + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'sched_date',
                width: 120,
                title: 'Schedule',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'ahrTypeDescription',
                width: 60,
                title: 'Type',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
            }, {
                field: 'status',
                width: 100,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.approvalStatus_ID) == 12) {
                        var badgeColor = 'btn-warning';
                    } else if (parseInt(row.approvalStatus_ID) == 2) {
                        var badgeColor = 'btn-info';
                    } else if (parseInt(row.approvalStatus_ID) == 5) {
                        var badgeColor = 'btn-success';
                    } else if (parseInt(row.approvalStatus_ID) == 6) {
                        var badgeColor = 'btn-danger';
                    }
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide' onclick='approverInfo(" + row.additionalHourRequestId + ")'>" + row.statusDescription + "</span>";
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Details"><i class="fa fa-search" onclick="initRecordInfo(' + row.additionalHourRequestId + ',' + row.additionalHourTypeId + ', \'' + row.requestType + '\')"></i></a>';
                    return html;
                },
            }],
        };
        options.search = {
            input: $('#monitoringRecordSearch'),
        };
        var datatable = $('#monitoringRecordAhrDatatable').mDatatable(options);
    };
    return {
        init: function (statusVal, userId, searchVal, dateFrom, dateTo) {
            monitoringRecordAhr(statusVal, userId, searchVal, dateFrom, dateTo);
        }
    };
}();


// function ahrRequestInit(){
//     $('#pendingRequestModal').modal('show');
// }

function disableFields() {
    $('#renderedActualDtrDate').prop('disabled', true);
    $('#renderedActualDtrTime').prop('disabled', true);
    $('#startDateAppr').prop('disabled', true);
    $('#startTimeAppr').prop('disabled', true);
    $('#endDateAppr').prop('disabled', true);
    $('#endTimeAppr').prop('disabled', true);
    $('#noteDiv').hide();
    $('#decisionBtn').hide();
    $('#closeBtn').show();
}

function enableFields() {
    $('#renderedActualDtrDate').prop('disabled', false);
    $('#renderedActualDtrTime').prop('disabled', false);
    $('#startDateAppr').prop('disabled', false);
    $('#startTimeAppr').prop('disabled', false);
    $('#endDateAppr').prop('disabled', false);
    $('#endTimeAppr').prop('disabled', false);
    $('#noteDiv').show();
    $('#decisionBtn').show();
    $('#closeBtn').hide();
}

function getAhrDetailsWithTimeMonitor(ahrId) {
    $.when(fetchPostData({
        ahr_id: ahrId
    }, '/additional_hour/get_request_details')).then(function (requestorDetails) {
        var ahrDetails = jQuery.parseJSON(requestorDetails.trim());
        console.log(ahrDetails);
        if (parseInt(ahrDetails.additionalHourTypeId) == 1) {
            var ahrType = 'After Shift';
        } else if (parseInt(ahrDetails.additionalHourTypeId) == 2) {
            var ahrType = 'Pre Shift';
        } else {
            var ahrType = 'WORD';
        }
        // var typeId = 3;
        $('#approvalAhrModal').data('ahrid', ahrId);
        var nameExt = "";
        //INITIALIZE OT DETAILS
        if ((ahrDetails.nextension != "none") || (ahrDetails.nextension != "") || ((ahrDetails.nextension).trim() != "? undefine")) {
            // nameExt = recordObj.nextension;
            nameExt = "";
        }
        if (ahrDetails.mname == '') {
            var midName = "";
        } else {
            var midName = ahrDetails.mname.charAt(0) + "."
        }
        var empPic = imagePath + "" + ahrDetails.pic;
        $('#empPicApproval').attr('src', empPic)
        $('#employeeName').text(ahrDetails.fname + " " + midName + " " + ahrDetails.lname + " " + nameExt);
        $('#employeeAccount').text(ahrDetails.accountDescription);
        $('#employeeJob').text(ahrDetails.positionDescription);
        $('#ahrNo').text(ahrDetails.additionalHourRequestId.padLeft(8));
        $('#dateFiledAppr').text(moment(ahrDetails.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('MMM D, Y h:mm A'));
        $('#filingTypeAppr').text(ahrDetails.requestType);
        $('#filingTypeAppr').data('requesttype', ahrDetails.requestType);
        $('#ahrTypeAppr').text(ahrType);
        $('#dateRenderedAppr').text(moment(ahrDetails.sched_date, 'Y-MM-DD').format('MMM D, Y'));
        $('#shiftAppr').text(ahrDetails.time_start + "-" + ahrDetails.time_end);
        $('#shiftTypeAppr').text(ahrDetails.schedType);
        $('#reasonAppr').text(ahrDetails.reason);
        getOtDetailsApproval(ahrDetails);
    });
}

function getAhrDetailsWithOutTimeMonitor(ahrId) {
    $.when(fetchPostData({
        ahr_id: ahrId
    }, '/additional_hour/request_details_no_time')).then(function (requestorDetails) {
        var ahrDetails = jQuery.parseJSON(requestorDetails.trim());
        console.log(ahrDetails);
        if (parseInt(ahrDetails.additionalHourTypeId) == 1) {
            var ahrType = 'After Shift';
        } else if (parseInt(ahrDetails.additionalHourTypeId) == 2) {
            var ahrType = 'Pre Shift';
        } else {
            var ahrType = 'WORD';
        }
        // var typeId = 3;
        $('#approvalAhrModal').data('ahrid', ahrId);
        //INITIALIZE OT DETAILS
        var nameExt = "";
       
		 if ((ahrDetails.nextension != "none") || (ahrDetails.nextension != "") || ((ahrDetails.nextension).trim() != "? undefine")) {
            // nameExt = recordObj.nextension;
            nameExt = "";
        }
        if (ahrDetails.mname == '') {
            var midName = "";
        } else {
            var midName = ahrDetails.mname.charAt(0) + "."
        }
        var empPic = imagePath + "" + ahrDetails.pic;
        $('#empPicApproval').attr('src', empPic)
        $('#employeeName').text(ahrDetails.fname + " " + midName + " " + ahrDetails.lname + " " + nameExt);
        $('#employeeAccount').text(ahrDetails.accountDescription);
        $('#employeeJob').text(ahrDetails.positionDescription);
        $('#ahrNo').text(ahrDetails.additionalHourRequestId.padLeft(8));
        $('#dateFiledAppr').text(moment(ahrDetails.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('MMM D, Y h:mm A'));
        $('#filingTypeAppr').text(ahrDetails.requestType);
        $('#filingTypeAppr').data('requesttype', ahrDetails.requestType);
        $('#ahrTypeAppr').text(ahrType);
        $('#dateRenderedAppr').text(moment(ahrDetails.sched_date, 'Y-MM-DD').format('MMM D, Y'));
        // $('#shiftAppr').text(ahrDetails.time_start + "-" + ahrDetails.time_end);
        $('#shiftTypeAppr').text(ahrDetails.schedType);
        $('#reasonAppr').text(ahrDetails.reason);
        getOtDetailsApproval(ahrDetails);

    });
}

function ahrRequestInit(ahrId, approvalStat, ahrTypeId, requestType) {
    if ((requestType == 'unrendered') && (parseInt(ahrTypeId) == 3)) {
        getAhrDetailsWithOutTimeMonitor(ahrId);
        if (approvalStat == 12) {
            enableFields();
            if (parseInt(ahrTypeId) == 3) {
                $('#shiftWordUnrendered').prop('disabled', false);
            }
        } else {
            disableFields();
            $('#approvalAhrModal').data('hr', 1);
            if (parseInt(ahrTypeId) == 3) {
                $('#shiftWordUnrendered').prop('disabled', true);
            }
        }
    } else {
        getAhrDetailsWithTimeMonitor(ahrId);
         if (approvalStat == 12) {
             enableFields();
             if (parseInt(ahrTypeId) == 3) {
                 $('#shiftWordUnrendered').prop('disabled', false);
             }
         } else {
             disableFields();
             $('#approvalAhrModal').data('hr', 1);
             if (parseInt(ahrTypeId) == 3) {
                 $('#shiftWordUnrendered').prop('disabled', true);
             }
         }
    }
    // if (approvalStat == 12) {
    //     enableFields();
    //     if (parseInt(ahrTypeId) == 3) {
    //         $('#shiftWordUnrendered').prop('disabled', false);
    //     }
    // } else {
    //     disableFields();
    //     if (parseInt(ahrTypeId) == 3) {
    //         $('#shiftWordUnrendered').prop('disabled', true);
    //     }
    // }
}

function initEmpSelect(statusId, callback) {
    $.when(fetchPostData({
        status: statusId,
    }, '/additional_hour/get_employee_names')).then(function (employee) {
        var empObj = $.parseJSON(employee.trim());
        var optionsAsString = "";
        if (empObj.length > 0) {
            optionsAsString += "<option value='0'>ALL</option>"
        }
        $.each(empObj, function (index, value) {
            optionsAsString += "<option value='" + value.userId + "'>" + value.lname + ", " + value.fname + "</option>";
        })
        $('#empSelect').html(optionsAsString);
        $('#empSelect').select2({
            placeholder: "Select an Employee",
            width: '100%'
        });
        callback(empObj);
    });
}

function initEmpSelectRecord(statusId) {
    var dateRange = $('#monitoringRecordDateRange').val().split('-');
    $.when(fetchPostData({
        status: statusId,
        rangeStart: moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'),
        rangeEnd: moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD')
    }, '/additional_hour/get_requestor_names_monitoring_record')).then(function (employee) {
        var empObj = $.parseJSON(employee.trim());
        var optionsAsString = "";
        if (empObj.length > 0) {
            optionsAsString += "<option value='0'>ALL</option>"
        }
        $.each(empObj, function (index, value) {
            optionsAsString += "<option value='" + value.userId + "'>" + value.lname + ", " + value.fname + "</option>";
        })
        $('#empSelectMonitorRecord').html(optionsAsString);
        $('#empSelectMonitorRecord').select2({
            placeholder: "Select an Employee",
            width: '100%'
        });
        $(".select2-container").tooltip({
            title: "Select an Employee",
            placement: "top"
        });

        $(".select2-selection span").attr('title', '');
        // callback(empObj);
    });
}

function setApprovalNoChangeMonitor(decision, callback) {
    $.when(fetchPostData({
        approval: decision,
        ahrId: $('#approvalAhrModal').data('ahrid'),
        remarks: $('#note').val(),
        ahrType: $('#shiftAppr').data('ahrtype')
    }, '/additional_hour/set_aproval_decision_no_change_monitor')).then(function (updateStat) {
        stat = $.parseJSON(updateStat.trim());
        callback(stat);
    });
}

function setApprovalWithChangeMonitor(decision, dateStartVal, timeStartVal, dateEndVal, timeEndVal, otHoursVal, shiftWordUnVal, callback) {
    $.when(fetchPostData({
        approval: decision,
        ahrId: $('#approvalAhrModal').data('ahrid'),
        remarks: $('#note').val(),
        ahrType: $('#shiftAppr').data('ahrtype'),
        dateStart: dateStartVal,
        timeStart: timeStartVal,
        dateEnd: dateEndVal,
        timeEnd: timeEndVal,
        otHours: otHoursVal,
        accTimeId: shiftWordUnVal
    }, '/additional_hour/set_approval_decision_with_change_monitor')).then(function (updateStat) {
        stat = $.parseJSON(updateStat.trim());
        callback(stat);
    });
}

function setApprovalMonitor(decision) {
    checkIfChanged(function (changeBool) {
        if (changeBool == 1 && decision == 5) {
            var dateStartVal = '';
            var timeStartVal = '';
            var dateEndVal = '';
            var timeEndVal = '';
            var otHours = $('#renderedOt').data('otHours');
            var shiftWordUn = 0;

            var ahrType = $('#shiftAppr').data('ahrtype');
            var requestType = $('#filingTypeAppr').data('requesttype');
            if (requestType == 'rendered') {
                if (ahrType == 1) {
                    dateEndVal = moment($('#renderedActualDtrDate').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeEndVal = moment($('#renderedActualDtrTime').val(), 'hh:mm A').format("HH:mm");
                } else if (ahrType == 2) {
                    dateStartVal = moment($('#renderedActualDtrDate').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeStartVal = moment($('#renderedActualDtrTime').val(), 'hh:mm A').format("HH:mm");
                }
            } else {
                if (ahrType == 1) {
                    dateEndVal = moment($('#endDateAppr').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeEndVal = moment($('#endTimeAppr').val(), 'hh:mm A').format("HH:mm");
                } else if (ahrType == 2) {
                    dateStartVal = moment($('#startDateAppr').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeStartVal = moment($('#startTimeAppr').val(), 'hh:mm A').format("HH:mm");
                } else {
                    shiftWordUn = $('#shiftWordUnrendered').val();
                    dateEndVal = moment($('#endDateAppr').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeEndVal = moment($('#endTimeAppr').val(), 'hh:mm A').format("HH:mm");
                    dateStartVal = moment($('#startDateAppr').val(), 'MM/DD/Y').format("YYYY-MM-DD");
                    timeStartVal = moment($('#startTimeAppr').val(), 'hh:mm A').format("HH:mm");
                }
            }

            setApprovalWithChangeMonitor(decision, dateStartVal, timeStartVal, dateEndVal, timeEndVal, otHours, shiftWordUn, function (apprStat) {
                $('#approvalAhrModal').modal('hide');
                if (parseInt(apprStat.set_current_approver_stat) != 0) {
                    var notifIds = [
                        apprStat.notify_preceding.notif_id[0],
                        apprStat.notify_requestor.notif_id[0]
                    ];
                    systemNotification(notifIds);
                    swal({
                        title: 'Request Approved!',
                        text: "you have successfully approved the request",
                        type: 'success',
                        showCloseButton: true,
                        timer: 5000,
                        onClose: () => {
                            $('#requestAhrTable').mDatatable('reload');
                        }
                    })
                } else {
                    swal({
                        title: 'Error!',
                        text: "Something went wrong during the approval process. Please report the issue immediately to your supervisor or to the system administrator",
                        type: 'error',
                        showCloseButton: true,
                        timer: 5000,
                        onClose: () => {
                            $('#requestAhrTable').mDatatable('reload');
                        }
                    })
                }
            })
        } else {
            console.log("NO CHANGES");
            setApprovalNoChangeMonitor(decision, function (apprStat) {
                if (decision == 5){
                    var wordDecision = "Approved";
                    var textSent = "you have successfully approved the request";
                }else{
                    var wordDecision = "Dispproved";
                    var textSentvar = "Your dissaproval was set successfully";
                }
                if (parseInt(apprStat.set_current_approver_stat) != 0) {
                    var notifIds = [
                        apprStat.notify_preceding.notif_id[0],
                        apprStat.notify_requestor.notif_id[0]
                    ];
                    systemNotification(notifIds);
                    // swal("Successfull!", "Success", "success");
                    swal({
                        title: 'Request '+wordDecision+'!',
                        text: "Success",
                        type: 'success',
                        showCloseButton: true,
                        timer: 5000,
                        onClose: () => {
                            $('#requestAhrTable').mDatatable('reload');
                        }
                    })
                } else {
                     swal({
                         title: 'Error!',
                         text: "Something went wrong during the approval process. Please report the issue immediately to your supervisor or to the system administrator",
                         type: 'error',
                         showCloseButton: true,
                         timer: 5000,
                         onClose: () => {
                             $('#requestAhrTable').mDatatable('reload');
                         }
                     })
                }
                $('#approvalAhrModal').modal('hide');
                
            })
        }
    });
}

function initHidden() {
    hideElement(['#dueNotif']);
}

function prepareFinalDecision(decisionStat) {
    checkIfRequestStillExist(function (exist) {
        if (parseInt(exist)) {
           setApprovalMonitor(decisionStat);
        } else {
            swal("Request Does not Exist!", "Sorry. This request has just been cancelled", "error");
            $('#requestAhrTable').mDatatable('reload');
            $('#approvalAhrModal').modal('hide');
        }
    })
}

function queryMonitoringAhrRecord(approvalDateRange) {
    var dateRange = approvalDateRange.split('-');
    $('#monitoringRecordAhrDatatable').mDatatable('destroy');
    monitoringRecordAhrDatatable.init($('#monitoringRecordStatus').val(), $('#empSelectMonitorRecord').val(), $('#monitoringRecordSearch').val(), moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
}

function reloadAhrRequestDatatable() {
    initEmpSelect($('#requestStatus').val(), function(){
        $('#requestAhrTable').mDatatable('reload');
        requestAhrDatatable.init($('#requestStatus').val(), $('#empSelect').val(), $('#requestAhrSearch').val());
    });
}

$('#checkPendingRequest').click(function () {
    $('.nav-tabs').find('a[href="#ahrRequests"]').trigger('click');
});

$('#requestStatus, #requestAhrSearch').on('change', function () {
    $('#requestAhrTable').mDatatable('destroy');
    initEmpSelect($('#requestStatus').val(), function(){
        requestAhrDatatable.init($('#requestStatus').val(), $('#empSelect').val(), $('#requestAhrSearch').val());
    });
})

$('#empSelect').on('change', function () {
    $('#requestAhrTable').mDatatable('destroy');
    requestAhrDatatable.init($('#requestStatus').val(), $('#empSelect').val(), $('#requestAhrSearch').val());
})

$('#empSelectMonitorRecord').on('change', function () {
    queryMonitoringAhrRecord($('#monitoringRecordDateRange').val());
})

$('#monitoringRecordStatus, #monitoringRecordDateRange').on('change', function(){
    initEmpSelectRecord($('#monitoringRecordStatus').val());
    queryMonitoringAhrRecord($('#monitoringRecordDateRange').val());
})

$(function () {
    // socket.on('responseTime', function (data) {
        
    // });
    initEmpSelect(0, function(){
        requestAhrDatatable.init(0, 0, '');
    });

    $('#approvalAhrForm').formValidation({
        message: 'This value is not valid',
        live: 'enabled',
        excluded: ':disabled',
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'fa fa-refresh'

        },
        fields: {
            note: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Note is required'
                    }
                }
            },
        }
    }).on('success.form.fv', function (e, data) {
        e.preventDefault();
        var formValidator = $('#approvalAhrForm').data('formValidation');
        var btn = formValidator.getSubmitButton();
        if ($(btn).attr('id') == "approveAhrBtn") {
            swal({
                title: 'Are you sure you want to approve this request?',
                text: "You will not be able to change you decision. Please review first and Contact the Supervisors before finalizing your decision",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '<i class="fa fa-thumbs-o-up"></i> Yes, Approved!',
                cancelButtonText: 'Not Yet, will review',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    prepareFinalDecision(5);
                } else if (result.dismiss === 'cancel') {
                    $('#approvalAhrForm').formValidation('disableSubmitButtons', false);
                }
            });
        } else if ($(btn).attr('id') == "rejectAhrBtn") {
            swal({
                title: 'Are you sure you want to disapprove this request?',
                text: "You will not be able to change you decision. Please review first and Contact the Supervisors before finalizing your decision",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '<i class="fa fa-thumbs-o-down"></i> Yes, disapproved!',
                cancelButtonText: 'Not Yet, will review',
                reverseButtons: true,
                // width: '500px' 
            }).then(function (result) {
                if (result.value) {
                    prepareFinalDecision(6);
                } else if (result.dismiss === 'cancel') {
                    $('#approvalAhrForm').formValidation('disableSubmitButtons', false);
                }
            });
        }
    });

    // RECORD TAB
    getCurrentDateTime(function (date) {
        // monitoringRecordAhrDatatable.init(0, 0, '', moment(date, 'Y-MM-DD').startOf('month').format('Y-MM-DD'), moment(date, 'Y-MM-DD').endOf('month').format('Y-MM-DD'));
        // $('#dateRangePersonalRecord .form-control').val(moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY') + ' - ' + moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY'));
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        // personalRecordAhrDatatable.init($('input[name=approvalStat]:checked').val(), '', moment(date, 'Y-MM-DD').startOf('month').format('Y-MM-DD'), moment(date, 'Y-MM-DD').endOf('month').format('Y-MM-DD'))
        $('#monitoringRecordDateRange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            autoclose: true,
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#monitoringRecordDateRange').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            queryMonitoringAhrRecord($('#monitoringRecordDateRange').val());
        });
    });
    initEmpSelectRecord(0);
})