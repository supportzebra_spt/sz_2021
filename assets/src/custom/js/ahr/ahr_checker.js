var ajax_req = null;
function get_ahr_records(first_init = 0) {
    let first_init_bool = first_init;
    let limit = 0;
    if(!first_init){
        limit = parseInt($('#ahr_records').data('limit'));  
    }
    dateRange = $('#check_date_range').val().split('-');
    if (ajax_req != null) ajax_req.abort();
    ajax_req = $.ajax({
        type: 'POST',
        url: baseUrl + '/additional_hour/process_12_ahr',
        data: {
            limiters: limit,
            search_val: $('#ahr_employee_search').val(),
            start_date: moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'),
            end_date: moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'),
        },
        cache: false,
        beforeSend: function() {
            // $(placeholder).addClass('loading');
        },
        success: function(data) {
            let list = JSON.parse(data.trim());
            console.log(list);
            if (parseInt(list.exist)) {
                if (first_init_bool) {
                    $('#ahr_records').html(list.record_list);
                } else {
                    $('#ahr_records').append(list.record_list);

                }
                $('#ahr_records').data('limit', limit + 10);
            } else {
                if (first_init_bool) {
                    $('#ahr_records').html(list.record_list);
                } else {
                    $('#ahr_records').append(list.record_list);

                }
            }
        },
        error: function(xhr) { // if error occured
            console.log(xhr.statusText + xhr.responseText);
        },
        complete: function() {},
    });
}

function checkerDateRangePicker(callback) {
    getCurrentDateTime(function(date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#check_date_range').val(start + ' - ' + end);
        $('#check_date_range').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function(start, end, label) {
            // console.log('record date range test');
            $('#check_date_range').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            get_ahr_records(1);
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);
    });
}
$('#ahr_employee_search').on('keyup', function(){
    get_ahr_records(1);
})
$('#ahr_records').scroll(function() {
    scroll = $(this).scrollTop() + $(this).innerHeight();
    scroll_height = $(this)[0].scrollHeight;
    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        get_ahr_records(0);
    }
});
$(function() {
    checkerDateRangePicker(function(dateRanges) {
        get_ahr_records(1);
    });
})