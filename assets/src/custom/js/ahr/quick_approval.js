function initRequestors(callback){
    $.when(fetchPostData({
        status: 4,
        acc_id: $('#accountSelect').val()
    }, '/additional_hour/get_pending_approval_requestor_names')).then(function (employee) {
        var empObj = $.parseJSON(employee.trim());
        var optionsAsString = "<option value='0'>ALL</option>";
        if (parseInt(empObj.exist)) {
            $.each(empObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.userId + "'>" + value.lname + ", " + value.fname + "</option>";
            });
        }
        $('#requestorSelect').html(optionsAsString);
        $('#requestorSelect').select2({
            placeholder: "Select an Employee",
            width: '100%'
        }).trigger('change');
        callback(empObj);
    });
}

function initAccount(callback){
    $.when(fetchPostData({
        status: 4,
    }, '/additional_hour/get_requestor_accounts')).then(function (accounts) {
        var accObj = $.parseJSON(accounts.trim());
        var optionsAsString =  "<option value='0'>ALL</option>";
        if (parseInt(accObj.exist)) {
            $.each(accObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.acc_id + "'>" + value.acc_name + "</option>";
            })
        }
        $('#accountSelect').html(optionsAsString);
        $('#accountSelect').select2({
            placeholder: "Select an Account",
            width: '100%'
        }).trigger('change');
        callback(accObj);
    });
}

function init_pending_approval_filters(){
    initAccount(function(accObj){
        initRequestors(function(accObj){
            initPendingAhrApprovalsDatatable();
        })
    })
}

$('#pendingApprovalAhrRequest').on('click', '.approvalbtn', function(){
    console.log('quick approval');
})

$('.accountSelect').select2({
    placeholder: "Select an Account",
    width: '100%'
});
$(".select2-selection span").attr('title', '');

$('#accountSelect').on('change', function(){
    initRequestors(function(accObj){
        initPendingAhrApprovalsDatatable();
    });
});

$('#requestorSelect, #approvalPendingAhrSearch').on('change', function(){
    initPendingAhrApprovalsDatatable();
})

function quick_approval(approval_type, appr, remarks_val){
    let request_ids = [];
    let approval_ids = [];
    request_ids = $(".rowcheck:checked").map(function () {
        return $(this).data('ahrid'); 
    }).get();
    if(appr == 'approver'){
        approval_ids = $(".rowcheck:checked").map(function () {
            return $(this).data('ahrapprid'); 
        }).get();
    }
    $.when(fetchPostData({
        approval_type: approval_type,
        approver_type: appr,
        remarks: remarks_val.trim(),
        id_values: request_ids.toString(),
        approval_id_vals: approval_ids.toString()
    }, '/additional_hour/quick_approval')).then(function (quick_appr_stat) {
        var quick_appr_obj = $.parseJSON(quick_appr_stat.trim());
        // console.log(quick_appr_obj.not_process_ahr_count =);
        if(parseInt(quick_appr_obj.process_ahr_count) > 0){
            let success_msg = "";
            if(quick_appr_obj.not_process_ahr_count > 0){
                let linking = "are";
                let pronoun = "they";
                let dos = "they";
                
                if(quick_appr_obj.not_process_ahr_count == 1){
                    linking = "is";
                    pronoun = "it";
                    dos = "does";
                }
                success_msg = quick_appr_obj.not_process_ahr_count+" AHR request "+linking+" not "+approval_type+"d. Either "+pronoun+" "+pronoun+" already taken cared of or the record/s "+dos+" not exist at all.";
            }
            swal(quick_appr_obj.process_ahr_count+" AHR "+approval_type+"d successfully", success_msg, "success");
        }else{
            swal("Error in AHR approval", "Its seems that all the AHR records to be "+approval_type+"d are already taken cared of or does not exist at all. Please report the immediately to the system administrator", "error");
        }
        if(appr == "monitoring"){
            reloadAhrRequestDatatable();
        }else{
            // reloadPendingAhrApprovalsDatatable();
            init_pending_approval_filters();
        }
        hide_quick_approval_buttons();
    });
}

function confirm_quick_approval(approval_type, appr){
    let request_word = "";
    if($(".rowcheck:checked").length > 1){
        request_word = 's';
    }
    swal({
        title: 'Are you sure you want to <b>'+approval_type.toUpperCase()+'</b> '+$(".rowcheck:checked").length+' Additional Hour Request'+request_word+'?',
        html: "Approval decision will not be reverted once finalized.</br></br></br><b>Approval Remarks</b>",
        type: 'warning',
        input: 'textarea',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true,
        nputValidator: (value) => {
            if (!value) {
              return 'You need to write something!'
            }
          },
        // width: '500px'
    }).then(function (result) {
        if (result.value) {
            quick_approval(approval_type, appr, result.value);
        } else if (result.dismiss === 'cancel') {
            
        }
    });
}

function check_to_show_quick_approval_btn(){
    if($(".rowcheck:checked").length > 0){
        $('.quick_approval_btns').fadeIn();
    }else{
        $('#all_check').prop('checked', false);
        $('.quick_approval_btns').fadeOut();
    }
}

$('#requestAhrTable, #pendingApprovalAhrRequest').on('click','.rowcheck', function(){
    // alert($(".rowcheck:checked").length);
    check_to_show_quick_approval_btn();
})

$('.quick_appr_btn').on('click', function(){
    confirm_quick_approval($(this).data('approval'), $(this).data('appr'));
});

$('#ahrRequests, #pendingApprovalAhrRequest').on('click','#all_check', function(){
    if($(this).is(":checked")){
        $('.rowcheck').prop('checked', true);
    }else{
        $('.rowcheck').prop('checked', false);
    }
    check_to_show_quick_approval_btn();
})

// $('#ahrRequests, #pendingApprovalAhrRequest').on('datatable-on-update-perpage', '#', function(){
//     console.log('reloaded datatable');
// });
$('#pendingApprovalAhrRequest').on('datatable-on-init', '#approvalAhrPendingDatatable', function(){
    console.log('reloaded datatable');
})
// $('#ahrRequests, #pendingApprovalAhrRequest').on('datatable-on-reloaded', '#', function(){
//     console.log('reloaded datatable');
// })

$(function(){
    init_pending_approval_filters();
})