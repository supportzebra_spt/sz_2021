//MONTHLY
var yearText = '';
var yText = '';
var allowDecimalBool = false;
var subtitleText = '';
var unitMonthly = '';

//Filtered
var titleText = '';
var subtitleTextFilter = '';
var unitFilter = '';
var drillAccount;

// DATATABLE

var reportAhrRecordDatatable = function () {
    var reportAhrRecord = function (statusVal, userId, searchVal, dateFrom, dateTo, accountVal, empTypeVal, ahrTypeVal, paymentStatVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/additional_hour/get_drilldown_emp_record",
                        params: {
                            query: {
                                status: statusVal,
                                user: userId,
                                employeeAhrRecordSearch: searchVal,
                                rangeStart: dateFrom,
                                rangeEnd: dateTo,
                                account: accountVal,
                                empType: empTypeVal,
                                ahrType: ahrTypeVal,
                                paymentStat: paymentStatVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },

            search: {
                input: $('#employeeAhrRecordSearch'),
            },
            // columns definition
            columns: [{
                field: "additionalHourRequestId",
                title: "AHR ID",
                width: 70,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var ahrId = row.additionalHourRequestId.padLeft(8);
                    var html = "<tr class='row'><td class = 'col-md-1'>" + ahrId + "</td>";
                    return html;
                }
            }, {
                field: 'requestor',
                width: 260,
                title: 'Requested By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = imagePath + "" + row.pic;
                    var elementId = "#recordReporting" + row.userId + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'dateTimeFiled',
                width: 120,
                title: 'Date Filed',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') + "</span><br><span>" + moment(row.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('h:mm A') + "</span>";
                    return html;
                },
            }, {
                field: 'sched_date',
                width: 120,
                title: 'Schedule Date',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'totalOtHours',
                width: 100,
                title: 'Time',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    humanTimeFormat(row.totalOtHours, function (formattedHours) {
                        // $('#additionalVal').text(formattedBct);
                        html = "<span class='text-center'>" + formattedHours + "</span>";
                    });
                    return html;
                },
            }, {
                field: 'status',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.approvalStatus_ID) == 12) {
                        var badgeColor = 'btn-warning';
                    } else if (parseInt(row.approvalStatus_ID) == 2) {
                        var badgeColor = 'btn-info';
                    } else if (parseInt(row.approvalStatus_ID) == 5) {
                        var badgeColor = 'btn-success';
                    } else if (parseInt(row.approvalStatus_ID) == 6) {
                        var badgeColor = 'btn-danger';
                    }
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide' onclick='approverInfo(" + row.additionalHourRequestId + ")'>" + row.statusDescription + "</span>";
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View Details"><i class="fa fa-search" onclick="initRecordInfo(' + row.additionalHourRequestId + ',' + row.additionalHourTypeId + ', \'' + row.requestType + '\')"></i></a>';
                    return html;
                },
            }],
        };
        options.search = {
            input: $('#employeeAhrRecordSearch'),
        };
        var datatable = $('#employeeAhrRecordDatatable').mDatatable(options);
    };
    return {
        init: function (statusVal, userId, searchVal, dateFrom, dateTo, accountVal, empTypeVal, ahrTypeVal, paymentStatVal) {
            reportAhrRecord(statusVal, userId, searchVal, dateFrom, dateTo, accountVal, empTypeVal, ahrTypeVal, paymentStatVal);
        }
    };

}();

function queryreportingAhrRecord(monitoringDateRange) {
    var dateRange = monitoringDateRange.split('-');
    $('#reportingRecordAhrDatatable').mDatatable('destroy');
    reportingAhrRecordDatatable.init($('#reportingRequestStatus').val(), $('#empSelectReporting').val(), $('#reportingRecordAhrSearch').val(), moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
}

function initEmpSelectMonitoring(statusId, dateFrom, dateTo, callback) {
    $.when(fetchPostData({
        status: statusId,
        rangeStart: dateFrom,
        rangeEnd: dateTo
    }, '/additional_hour/get_requestor_names_monitoring')).then(function (employee) {
        var empObj = $.parseJSON(employee.trim());
        var optionsAsString = "";
        if (empObj.length > 0) {
            optionsAsString += "<option value='0'>ALL</option>"
        }
        $.each(empObj, function (index, value) {
            optionsAsString += "<option value='" + value.userId + "'>" + value.lname + ", " + value.fname + "</option>";
        })
        $('#empSelectReporting').html(optionsAsString);
        $('#empSelectReporting').select2({
            placeholder: "Select an Employee",
            width: '100%'
        });
        callback(empObj);
    });
}

$('#empSelectReporting').on('change', function () {
    queryreportingAhrRecord($('#dateRangeReportingRecord').val());
})

$('#reportingRequestStatus, #reportingRecordAhrSearch').on('change', function () {
    var dateRange = $('#dateRangeReportingRecord').val().split('-');
    initEmpSelectMonitoring($('#reportingRequestStatus').val(), moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (empObj) {
        queryreportingAhrRecord($('#dateRangeReportingRecord').val());
    });
})

function fetchGeneralChartData(dateVal, quantityTypeVal, empTypeVal, callback) {
    $.when(fetchPostData({
        dateSelected: dateVal,
        quantityType: quantityTypeVal,
        empType: empTypeVal
    }, '/additional_hour/get_monthly_chart')).then(function (chart) {
        var chartData = $.parseJSON(chart.trim());
        callback(chartData);
    });
}

function accountDrilldown(monthVal, empType, nameVal, ahrTypeVal, callback){
    $.when(fetchPostData({
       monthEnd: moment($('#yearSelect').val() + '-' + monthVal + '-01', 'Y-MM-DD').endOf('month').format('Y-MM-DD'),
       monthStart: moment($('#yearSelect').val() + '-' + monthVal + '-01', 'Y-MM-DD').format('Y-MM-DD'),
       empType: empType,
       name: nameVal,
       unit: $('#quantityType1').val(),
       month: monthVal,
       ahrType: ahrTypeVal

    }, '/additional_hour/get_gen_chart_drilldown')).then(function (chart) {
        callback($.parseJSON(chart.trim()));
    });
}

function generalChart(generalChartData) {

    Highcharts.setOptions(Highcharts.theme);
    Highcharts.chart('generalChart', {
        chart: {
            type: 'column',
            events: {
                drilldown: function (e) {
                    var chart = this;
                    accountDrilldown(e.point.drilldown, e.point.empType, e.point.name, e.point.series.userOptions.typeId, function (drilldown) {
                        var drilldownData = drilldown.drilldown
                        series = drilldownData[e.point.name];
                        // // // Show the loading label
                        // // // chart.showLoading('Simulating Ajax ...');
                        // // // setTimeout(function () {
                        // // // chart.hideLoading();
                        // console.log(e.point);
                        // console.log(series);
                        chart.addSeriesAsDrilldown(e.point, series);
                        chart.xAxis[0].setCategories(drilldown.category);
                    })
                },
                drillup: function (e) {
                    var chart = this;
                    chart.xAxis[0].setCategories(generalChartData.months);
                },
            }
        },

        title: {
            text: yearText + ' Additional Hour Request Monthly Statistics'
        },
        subtitle: {
            text: subtitleText
        },
        xAxis: {
            categories: generalChartData.months
        },
        yAxis: {
            allowDecimals: allowDecimalBool,
            title: {
                text: yText
            }
        },
    //    tooltip: {
    //        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    //        pointFormat: '<span style="color:{point.color}"><b>{point.name}</b></span>: Reviewed <b>{point.reviewed}</b> out of {point.total} answers<br/>'
    //    },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        drilldown: {
            series: []
        },
        credits: {
            enabled: false
        },
        series: generalChartData.seriesData
    });
}
// function generalChart(generalChartData) {
//     // console.log(generalChartData);
//     // console.log(generalChartData.admin);
//     // console.log(generalChartData.agent);
//     Highcharts.setOptions(Highcharts.theme);
//     Highcharts.chart('generalChart', {
//         chart: {
//             type: 'column',
//             // events: {
//             //     drilldown: function (e) {
//             //         var chart = this;
//             //         accountDrilldown(e.point.drilldown, e.point.empType, e.point.name, function (drilldownData){
//             //             console.log(drilldownData);
//             //             console.log(drilldownData[e.point.name]);
//             //             series = drilldownData[e.point.name];
//             //             // // Show the loading label
//             //             // // chart.showLoading('Simulating Ajax ...');

//             //             // // setTimeout(function () {
//             //             // // chart.hideLoading();
//             //             console.log(e.point);
//             //             console.log(series);
//             //             chart.addSeriesAsDrilldown(e.point, series);
//             //         })
//             //     },
//             //     drillup: function (e) {
//             //     }
//             // }
//         },
//         title: {
//             text: yearText + ' Additional Hour Request Monthly Statistics'
//         },
//         subtitle: {
//             text: subtitleText
//         },
//         legend: {
//             align: 'center',
//             verticalAlign: 'bottom',
//             layout: 'horizontal'
//         },
//         xAxis: {
//                 categories: generalChartData.months
//         },
//         yAxis: {
//             allowDecimals: allowDecimalBool,
//             title: {
//                 text: yText
//             }
//         },
//         series: [
//             generalChartData.combinedData
//         ],
//         drilldown: {
//             series: []
//         },
//         credits: {
//             enabled: false
//         },
//         plotOptions: {
//             series: {
//                 stacking: 'normal'
//             }
//         },
//         responsive: {
//             rules: [{
//                 condition: {
//                     maxWidth: 500
//                 },
//                 chartOptions: {
//                     legend: {
//                         align: 'center',
//                         verticalAlign: 'bottom',
//                         layout: 'horizontal'
//                     },
//                     yAxis: {
//                         labels: {
//                             align: 'left',
//                             x: 0,
//                             y: -5
//                         },
//                         title: {
//                             text: null
//                         }
//                     },
//                     subtitle: {
//                         text: subtitleText
//                     },
//                     credits: {
//                         enabled: false
//                     }
//                 }
//             }]
//         }
//     });
// }

function getReportSpecEmp(accountVal, empTypeVal){
    $.when(fetchPostData({
        rangeStart: moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'),
        rangeEnd: moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'),
        status: $('#ahrStatus').val(),
        empType: empTypeVal,
        account: accountVal,
        ahrType: $('#ahrTypeReport').val(),
        paymentStat: $('#paymentStat').val()
    }, '/additional_hour/get_report_specific_emp')).then(function (employee) {
        var empObj = $.parseJSON(employee.trim());
        var optionsAsString = "";
        if (empObj.length > 0) {
            optionsAsString += "<option value='0'>ALL</option>"
        }
        $.each(empObj, function (index, value) {
            optionsAsString += "<option value='" + value.userId + "'>" + value.lname + ", " + value.fname + "</option>";
        })
        $('#empSelect1').html(optionsAsString);
        $('#empSelect1').select2({
            placeholder: "Select an Employee",
            width: '100%'
        });
    });
}

function empDrillDown(mainData, callback) {
    console.log(mainData);
    drillAccount = mainData.drilldown;
    dateRange = $('#filteredReportDateRange').val().split('-');
    accName = mainData.name.split(' - ');
    $('#employeeAhrRecordDatatable').mDatatable('destroy');
    reportAhrRecordDatatable.init($('#ahrStatus').val(), 0, '', moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'), mainData.drilldown, accName[1], $('#ahrTypeReport').val(), $('#paymentStat').val());
    getReportSpecEmp(mainData.drilldown, accName[1]);
    console.log(accName[0]);
    $.when(fetchPostData({
        drill_id: mainData.drilldown,
        drill_name: accName[0],
        dateStart: moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'),
        dateEnd: moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'),
        ahrTypeId: $('#ahrTypeReport').val(),
        statusId: $('#ahrStatus').val(),
        employeeType: accName[1],
        quantityType: $('#quantityType2').val(),
        paymentStat: $('#paymentStat').val()
    }, '/additional_hour/get_drilldown')).then(function (chart) {
        var drilldownData = $.parseJSON(chart.trim());
        $('#empDatatable').show(500);
        $('#accountTable').hide(500);
        callback(drilldownData);
    });
}

function employeeTable(reportDetails) {
    console.log(reportDetails);
    var content = '';
    $.each(reportDetails.data, function (index, value) {
        content += '<tr>' +
            '<td>' +
            '<div class="row">' +
            '<div class="col-sm-4 col-md-4 d-none d-sm-block"><img src="' + baseUrl + '/assets/images/img/sz.png" width="30" alt="" class="img-circle pull-right"></div>' +
            '<div class="col-sm-8 col-md-8 text-left align-middle pt-1">' + value[0] + '</div>' +
            '</div>' +
            '</td>' +
            '<td><div class="col-md-12 pt-1">' + value[1] + '</div></td>' +
            '</tr>';
    })
    $('#bodyCount').html(content);
}

function accountTable(reportDetails) {
    console.log(reportDetails);
    var content = '';
    $.each(reportDetails.accounts, function (index, value) {
        content += '<tr><th scope="row">' + value.name + '</th><td>' + value.y + '</td></tr>';
    })
    $('#bodyCount').html(content);
}


function filteredChart(reportDetails) {
    accountTable(reportDetails);
    console.log($('#quantityType2').val());
    if ($('#quantityType2').val() == 1) {
        var unitCount = "Number of Request"
    } else {
        var unitCount = "Hours Rendered"
    }
    $("#accountLabel").text('Employee');
    $("#countLabel").text(unitCount);
    Highcharts.chart('filteredChart', {
        chart: {
            type: 'pie',
            events: {
                drilldown: function (e) {
                    var chart = this;
                    empDrillDown(e.point.options, function (drilldown) {
                        // console.log(drilldown);
                        drilldownData = drilldown.drilldown;

                        // console.log(drilldownData);
                        series = drilldownData[e.point.name];
                        // console.log(e.point);
                        // console.log(series);
                        // Show the loading label
                        // chart.showLoading('Simulating Ajax ...');

                        // setTimeout(function () {
                        // chart.hideLoading();
                        chart.addSeriesAsDrilldown(e.point, series);
                        
                        // }, 1000);
                        // }
                    });

                    // employeeTable(e.seriesOptions);
                },
                drillup: function (e) {
                    accountTable(reportDetails);
                    $("#accountLabel").text('Employee');
                    $("#countLabel").text(unitCount);
                    $('#empDatatable').hide(500);
                    $('#accountTable').show(500);
                }
            }
        },
        title: {
            text: titleText
        },
        subtitle: {
            text: subtitleTextFilter
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.2f}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> ' + unitFilter + '<br/>'
        },
        "series": [{
            "name": "Accounts",
            "colorByPoint": true,
            "data": reportDetails.accounts
        }],
        "drilldown": {
            "series": []
        }
    });
}


function initMonthlyChartData(year, quantityType, empType) {
    fetchGeneralChartData(year, quantityType, empType, function (generalChartData) {
        // console.log(generalChartData.combinedData);
        var vals = [];
        $count = 0;
        $.each(generalChartData.seriesData, function (index, value) {
            // console.log(value.data);
            $.each(value.data, function(index, value1){
                if (value1.y > 0){
                    vals[$count] = value1.y
                    $count++;
                }
            })
        })
        // console.log(vals);

        if (vals.length == 0) {
            $('#noMonthlyData').show(500);
            $('#generalChart').hide(500);
        } else {
            $('#noMonthlyData').hide(500);
            generalChart(generalChartData);
            $('#generalChart').show(500);
        }

    });
}

function initFilteredChartData(rangeStart, rangeEnd) {
    console.log(rangeStart + " = " + rangeEnd);
    $.when(fetchPostData({
        dateStart: rangeStart,
        dateEnd: rangeEnd,
        ahrTypeId: $('#ahrTypeReport').val(),
        statusId: $('#ahrStatus').val(),
        employeeType: $('#empType').val(),
        quantityType: $('#quantityType2').val(),
        paymentStat: $('#paymentStat').val()
    }, '/additional_hour/get_filtered_report')).then(function (chart) {
        var chartData = $.parseJSON(chart.trim());
        if (parseInt(chartData.accounts[0]) == 0) {
            $('.noFilteredData').show();
            $('#filteredChart').hide();
            $('#accountTable').hide(500);
            $('#empDatatable').hide(500);
            $('#downloadAhrReportBtn').hide();
        } else {
            $('.noFilteredData').hide();
            $('#downloadAhrReportBtn').show();

            // $('#empDatatable').hide(500);
            $('#filteredChart').show();
            $('#accountTable').show(500);
            filteredChart(chartData);
        }
        // if (chartData.accounts)
    });
}

function quantityType() {
    var options = '<option value="1">Request</option><option value = "2"> Hours Rendered </option>';
    $('.quantityType').html(options);
}

function getAhrTypeReport() {
    $.when(fetchGetData('/additional_hour/get_ahr_type_details')).then(function (ahrType) {
        var additionalHourTypes = jQuery.parseJSON(ahrType.trim());
        var optionsAsString = "<option value='0'>ALL</option>";
        $.each(additionalHourTypes, function (index, value) {
            optionsAsString += "<option value='" + value.additionalHourTypeId + "' class='m--font - transform - u'>" + value.description + "</option>";
        });
        $('#ahrTypeReport').html(optionsAsString);
    });
}

$('#empSelect1').on('change', function(){
     dateRange = $('#filteredReportDateRange').val().split('-');
     $('#employeeAhrRecordDatatable').mDatatable('destroy');
     reportAhrRecordDatatable.init($('#ahrStatus').val(), $(this).val(), $('#employeeAhrRecordSearch').val(), moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'), drillAccount, $('#empType').val(), $('#ahrTypeReport').val(), $('#paymentStat').val());
})

$('#yearSelect, #quantityType1, #empType1').on('change', function () {
    var selectedDate = '';
    yearText = $('#yearSelect').val();
    if ($('#quantityType1').val() == 1) {
        yText = "Number of AHR Request";
        allowDecimalBool = false;
        subtitleText = 'Approved Number of Request made by Admin and Agents';
        unitMonthly = 'Request/s';
    } else {
        yText = "Hours Rendered";
        allowDecimalBool = true;
        subtitleText = 'Approved Overtime Hours Rendered by Admin and Agents';
        unitMonthly = 'Hour/s';
    }
    getCurrentDateTime(function (date) {
        currentYear = moment(date, "Y-MM-DD").format("YYYY");
        if (parseInt($('#yearSelect').val()) < parseInt(currentYear)) {
            var startDate = moment($('#yearSelect').val() + '-12-01', 'Y-MM-DD');
            var endDate = startDate.clone().endOf('month');
            selectedDate = endDate.format('Y-MM-D');
        } else {
            selectedDate = date;
        }
        initMonthlyChartData(selectedDate, $('#quantityType1').val(), $('#empType1').val());
    });
});

function updateFilteredChart(){
    // alert("test");
    // console.log($('#filteredReportDateRange').val());
    dateRange = $('#filteredReportDateRange').val().split('-');
    if ($('#empType').val()==0){
        var subject = "All Employee"
    }else{
        var subject = $('#empType option:selected').text();
    }
    titleText = 'Additional Hour Ruquests of ' + subject + 's from ' + moment(dateRange[0], 'MM/DD/YYYY').format('MMMM D, Y') + ' to ' + moment(dateRange[1], 'MM/DD/YYYY').format('MMMM D, Y');
    if ($('#ahrTypeReport').val() == 0) {
        var requestType = 'Additional Hour Request';
    } else if ($('#ahrTypeReport').val() != 3) {
        var requestType = $('#ahrTypeReport option:selected').text() + ' shift Additional Hour';
    } else {
        var requestType = 'WORD';
    }
    if ($('#quantityType2').val() == 1) {
        subtitleTextFilter = $('#ahrStatus option:selected').text() + ' Number of ' + requestType + ' of ' + subject + 's';
        unitFilter = 'Request/s';
    } else {
        subtitleTextFilter = $('#ahrStatus option:selected').text() + ' Overtime Hours (' + requestType + ') rendered by ' + subject + 's';
        unitFilter = 'Hour/s';
    }
    initFilteredChartData(moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'));
    $('#empDatatable').hide(500);
}

$('#quantityType2, #ahrTypeReport, #ahrStatus, #empType, #paymentStat').on('change', function () {
   updateFilteredChart()
})

$('#toggleFilter').on('click', function () {
    $('#filters').slideToggle(700);
})

$('#downloadAhrReportBtn').on('click', function(){
    var dateRange = $('#filteredReportDateRange').val().split('-');
    // window.location.href = baseUrl + "/additional_hour/download_ahr_report/" + moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD') + "/" + moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD') + "/" + $('#ahrTypeReport').val() + "/" + $('#ahrStatus').val() + "/" + $('#empType').val() + "/" + $('#paymentStat').val();
    var excelFile = baseUrl + "/additional_hour/download_ahr_report/" + moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD') + "/" + moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD') + "/" + $('#ahrTypeReport').val() + "/" + $('#ahrStatus').val() + "/" + $('#empType').val() + "/" + $('#paymentStat').val();
    $.fileDownload(excelFile, {
        successCallback: function (url) {
            console.log(url);
            // $preparingFileModal.dialog('close');
        },
        failCallback: function (responseHtml, url) {
            console.log(responseHtml);
            console.log(url);
            // $preparingFileModal.dialog('close');
            // $("#error-modal").dialog({
            //     modal: true
            // });
        }
    });
    // $.when(fetchPostData({
    //     dateStart: moment(dateRange[0], 'MM/DD/YYYY').format('Y-MM-DD'),
    //     dateEnd: moment(dateRange[1], 'MM/DD/YYYY').format('Y-MM-DD'),
    //     ahrTypeId: $('#ahrTypeReport').val(),
    //     statusId: $('#ahrStatus').val(),
    //     employeeType: $('#empType').val(),
    //     paymentStat: $('#paymentStat').val()
    // }, '/additional_hour/download_ahr_report')).then(function (test) {
    //     // var chartData = $.parseJSON(chart.trim());
    //     // if (parseInt(chartData.accounts[0]) == 0) {
    //     //     $('.noFilteredData').show();
    //     //     $('#filteredChart').hide();
    //     //     $('#accountTable').hide(500);
    //     //     $('#empDatatable').hide(500);
    //     // } else {
    //     //     $('.noFilteredData').hide();
    //     //     // $('#empDatatable').hide(500);
    //     //     $('#filteredChart').show();
    //     //     $('#accountTable').show(500);
    //     //     filteredChart(chartData);
    //     // }
    //     // if (chartData.accounts)
    // });
})

$(function () {
    // $('#noMonthlyData').hide();
    // $('.noFilteredData').hide();
    quantityType();
    getAhrTypeReport();
    getCurrentDateTime(function (date) {
        // MONTHLY REPORT INIT
        var currentYear = moment(date, "Y-MM-DD").format("YYYY");
        yearText = currentYear;
        if ($('#empType').val() == 0) {
            var subject = "All Employee"
        } else {
            var subject = $('#empType option:selected').text();
        }
        // allowDecimalBool = false;
        yText = "Number of AHR Request";
        subtitleText = 'Approved Number of Request made by Admin and Agents';
        unitMonthly = 'Request/s';
        initMonthlyChartData(date, $('#quantityType1').val(), $('#empType1').val());
        $(".yearSelect").yearselect({
            start: 2017,
            end: currentYear,
            order: 'desc',
        });
        
        // FILTERED REPORT
        start = moment(date, 'Y-MM-DD').subtract(29, 'days').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').format('MM/DD/YYYY');
        initFilteredChartData(moment(start, 'MM/DD/YYYY').format('Y-MM-DD'), moment(end, 'MM/DD/YYYY').format('Y-MM-DD'));
        titleText = 'Additional Hour Ruquests of ' + subject + 's from ' + moment(start, 'MM/DD/YYYY').format('MMMM D, Y') + ' to ' + moment(end, 'MM/DD/YYYY').format('MMMM D, Y');
        subtitleTextFilter = $('#ahrStatus option:selected').text() + ' Number of Additional Hour Request of ' + subject + 's';
        unitFilter = 'Request/s';
        $('#filteredReportDateRange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#filteredReportDateRange').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            // initFilteredChartData(moment(start.format('MM/DD/YYYY'), 'MM/DD/YYYY').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/YYYY').format('Y-MM-DD'))
            //  titleText = 'Additional Hour Ruquests of ' + $('#empType option:selected').text() + 's from ' + moment(start, 'MM/DD/YYYY').format('MMMM D, Y') + ' to ' + moment(end, 'MM/DD/YYYY').format('MMMM D, Y');
            //  subtitleTextFilter = $('#ahrStatus option:selected').text() + ' Number of Additional Hour Request by ' + $('#empType option:selected').text() + 's';
            updateFilteredChart();
        });


        // //RECORD
        // $('#dateRangeReportingRecord').val(start + ' - ' + end);
        // // $('#dateRangeApprovalRecord .form-control').val(moment(date, 'Y-MM-DD').format('MM/DD/YYYY') + ' - ' + moment(date, 'Y-MM-DD').format('MM/DD/YYYY'));
        // initEmpSelectMonitoring(0, '2017-08-1', '2018-01-01', function (empObj) {});
        // reportingAhrRecordDatatable.init(0, 0, '', '2017-08-1', '2018-01-01');
        // // initEmpSelectApproval("record", 0, date, date, function (empObj) {});
        // $('#dateRangeReportingRecord').daterangepicker({
        //     buttonClasses: 'm-btn btn',
        //     applyClass: 'btn-primary',
        //     cancelClass: 'btn-secondary',
        //     startDate: start,
        //     endDate: end,
        //     ranges: {
        //         'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
        //         'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
        //         'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
        //         'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
        //         'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
        //         'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
        //     }
        // }, function (start, end, label) {
        //     $('#dateRangeReportingRecord').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
        //     // initFilteredChartData(moment(start.format('MM/DD/YYYY'), 'MM/DD/YYYY').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/YYYY').format('Y-MM-DD'))\
        //     initEmpSelectMonitoring(0, moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (empObj) {
        //         queryreportingAhrRecord($('#dateRangeReportingRecord').val());
        //     });
        // });

    });


})