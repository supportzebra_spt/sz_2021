var dateToCompute = '';
var dateToShow = '';
function hideElement(elementIdArray) {
    $.each(elementIdArray, function (index, value) {
        $(value).hide();
    });
}

function showElement(elementIdArray) {
    $.each(elementIdArray, function (index, value) {
        $(value).show();
    });
}
// FUNCTIONS ------------------------------------------------------------------------------------------
//NORMAL
function formReset(formId){
    $(formId).formValidation('resetForm', true);
}

function resetRenderedAhrForm() {
    $('#preAfterShiftType').text('');
    $('#dtrLogVal').text('');
    $('#actualDtrDate').val('');
    // formReset('#renderedAhrFilingForm');
    if ($('#ahrTypeRendered').val() != 3){
        $('#renderedAhrFilingForm').formValidation('resetField', 'actualDtrDate', true);
        $('#renderedAhrFilingForm').formValidation('resetField', 'actualDtrTime', true);
    }
    $('#actualDtrTime').val('');
    $('#renderedOt').text('00 hr, 00 min')
    $('#wordShiftType').text('');
    $('#preAfterShiftType').text('');
    $('#wordDtrIn').text('');
    $('#wordDtrOut').text('');
    $('#bctVal').text('');
    $('#dtrTardVal').text('');
    $('#breakVal').text('');
    $('.character-remaining').text('');
    $('#shift, #actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
}

function getAhrType() {
    $.when(fetchGetData('/additional_hour/get_ahr_type_details')).then(function (ahrType) {
        var additionalHourTypes = jQuery.parseJSON(ahrType.trim());
        var optionsAsString = "";
        $.each(additionalHourTypes, function (index, value) {
            optionsAsString += "<option value='" + value.additionalHourTypeId + "' class='m--font - transform - u'>" + value.description + "</option>";
        });
        $('select[name="ahrType"]').html(optionsAsString);
        identifyValidation();
    });
}

function assignDateToShift() {
    var shiftStart = moment(schedDate + " " + shift[0], "YYYY-MM-DD hh:mm A").valueOf();
    var shiftEnd = moment(schedDate + " " + shift[1], "YYYY-MM-DD hh:mm A").valueOf();
    if (shiftStart > shiftEnd) {
        shiftEnd = moment(shiftEnd).add(1, 'days').valueOf();
    }
}

function initializeActualDtrTimePicker(id, time) {
    $(id).timepicker({
        defaultTime: time,
        minuteStep: 1,
        showSeconds: false,
        showMeridian: true
    });

    $(id).timepicker(
        "setTime", time
    );
}

function getShiftBreak(acc_shift_id, callback) {
    $.when(fetchPostData({
        accShiftId: acc_shift_id,
    }, '/additional_hour/get_account_shift_break')).then(function (breakShift) {
        var shiftBreak = jQuery.parseJSON(breakShift.trim());
        callback(shiftBreak);
    });
}

function humanTimeFormat(hours, callback) {
    var totalHours = parseInt(hours);
    var totalMinutes = Math.round(((hours - totalHours) * 60).toFixed(2));
    var totalTime = '';
    if (totalMinutes == 60) {
        totalMinutes = 0;
        totalHours++;
    }
    var totalTime = '';
    if ((totalHours == 0) && (totalMinutes == 1)) {
        totalTime = totalMinutes + " min";
    } else if ((totalHours == 0) && (totalMinutes != 1)) {
        totalTime = totalMinutes + " mins";
    } else if ((totalMinutes == 0) && (totalHours == 1)) {
        totalTime = totalHours + " hr";
    } else if ((totalMinutes == 0) && (totalHours != 1)) {
        totalTime = totalHours + " hrs";
    } else if ((totalHours >= 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " min";
    } else if ((totalHours == 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " mins";
    } else if ((totalHours == 1) && (totalMinutes == 1)) {
        totalTime = totalHours + " hr, " + totalMinutes + " min";
    } else if ((totalHours >= 1) && (totalMinutes >= 1)) {
        totalTime = totalHours + " hrs, " + totalMinutes + " mins";
    }
    callback(totalTime);
}

function identifyShiftType() {
    if ($('#ahrTypeRendered').val() == 3) {
        $('#wordShiftType').text($('#shift').find('option:selected').attr('shifttype'));
    } else {
        $('#preAfterShiftType').text($('#shift').find('option:selected').attr('shifttype'));
    }
}

function dateAdjust(dateTime, callback) {
    var time = moment(dateTime, 'Y-MM-DD HH:mm').format('HH:mm');
    if (time == '00:00') {
        var dateToCompute = moment(dateTime, 'Y-MM-DD HH:mm').add(1, 'days').format('Y-MM-DD HH:mm');
        var dateToShow = moment(dateTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm');
    } else {
        var dateToShow = moment(dateTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm');
        var dateToCompute = moment(dateTime, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm');
    }
    var dateAdjustment = {
        show: dateToShow,
        compute: dateToCompute
    }
    callback(dateAdjustment);
}

function checkIfTimeInRange(actualDateTime, callback) {
    var test
    var datedShift = $('#shift').data('datedshift');
    var bct = parseFloat($('#bctVal').data("actualbct"));
    var bctMin = bct * 60;
    var shiftStartStr = moment(datedShift.start, "YYYY-MM-DD kk:mm").subtract(bctMin.toPrecision(2), 'minutes').valueOf();
    var shiftEndStr = moment(datedShift.end, "YYYY-MM-DD kk:mm").valueOf();
    var dtrLogValStr = moment(moment($('#dtrLogVal').text(), 'MMM DD, Y hh:mm A').format("YYYY-MM-DD kk:mm")).valueOf();
    let inputTime = moment($('#actualDtrDate').val() + " " + $('#actualDtrTime').val(), "MM/DD/Y hh:mm A").format('YYYY-MM-DD kk:mm');
    let inputTimeStr = moment(inputTime, "YYYY-MM-DD kk:mm").valueOf();
    if (moment(datedShift.start, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
        shiftStartStr = moment(datedShift.start, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
    }
    if (moment(datedShift.end, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
        shiftEndStr = moment(datedShift.end, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
    }
    if (moment($('#actualDtrDate').val() + " " + $('#actualDtrTime').val(), "MM/DD/Y hh:mm A").format('kk:mm') == "24:00") {
        inputTimeStr = moment(inputTime, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
    }
    var exceed = 0;
    if ($('#ahrTypeRendered').val() == 1) {
        // AFTER
        if (inputTimeStr > dtrLogValStr) {
            $('#errNotifyRenderedAhrMssg').html("<b>Maximum Time Limit Reached!</b> You are not allowed to input time that is <i>beyond</i> your DTR clock out which is <b>" + moment(dtrLogValStr).format('YYYY-MM-DD h:mm A') + "</b>.");            
            exceed = 1;
        } else if ((inputTimeStr == shiftEndStr) || (inputTimeStr < shiftEndStr)) {
            exceed = 1;
            $('#errNotifyRenderedAhrMssg').html("<b>Minimum Time Limit Reached!</b> You are not allowed to input time that is <i>equal</i> or <i>earlier</i> than your shift end which is <b>" + moment(shiftEndStr).format('YYYY-MM-DD h:mm A') + "</b>.");
        }
    } else if ($('#ahrTypeRendered').val() == 2) {
        //PRE
        if ((inputTimeStr > shiftStartStr) || (inputTimeStr == shiftStartStr)) {
            exceed = 1;
            $('#errNotifyRenderedAhrMssg').html("<b>Maximum Time Limit Reached!</b> You are not allowed to input time that is <i>equal</i>, or <i>beyond</i> your shift start which is <b>" + moment(shiftStartStr).format('YYYY-MM-DD h:mm A') + "</b>.");
        } else if (inputTimeStr < dtrLogValStr) {
            exceed = 1;
            $('#errNotifyRenderedAhrMssg').html("<b>Mimimum Time Limit Reached!</b> You are not allowed to input time that is <i>earlier</i> than your DTR clock in, which is <b>" + moment(dtrLogValStr).format('YYYY-MM-DD h:mm A') + "</b>.");
        }
    }
    callback(exceed);
}

function computeAhrTime(callback) {
    let datedShift = $('#shift').data('datedshift');
    let shiftStartStr = moment(datedShift.start, "YYYY-MM-DD kk:mm").valueOf();
    let shiftEndStr = moment(datedShift.end, "YYYY-MM-DD kk:mm").valueOf();
     if (moment(datedShift.start, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
         shiftStartStr = moment(datedShift.start, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
     }
     if (moment(datedShift.end, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
         shiftEndStr = moment(datedShift.end, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
     }
     if (dtrOut == "24:00") {
         dtrOutStr = moment(dtrOut, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
     }
    let inputTime = moment($('#actualDtrDate').val() + " " + $('#actualDtrTime').val(), "MM/DD/Y hh:mm A").format('YYYY-MM-DD kk:mm');
    if ($('#ahrTypeRendered').val() != 3) { //if not word
        let inputTimeStr = moment(inputTime, "YYYY-MM-DD kk:mm").valueOf();
        if (moment($('#actualDtrDate').val() + " " + $('#actualDtrTime').val(), "MM/DD/Y hh:mm A").format('kk:mm') == "24:00"){
            inputTimeStr = moment(inputTime, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
        }
        if ($('#ahrTypeRendered').val() == 1) {
            var start = shiftEndStr;
            var end = inputTimeStr;
        } else if ($('#ahrTypeRendered').val() == 2) {
            var start = inputTimeStr;
            var end = shiftStartStr;
        }
    } else { // if word
        var dtrIn = $('#wordDtrIn').data('datetocompute')
        var dtrOut = $('#wordDtrOut').data('datetocompute')
        let dtrInStr = moment(dtrIn, "YYYY-MM-DD HH:mm").valueOf();
        let dtrOutStr = moment(dtrOut, "YYYY-MM-DD HH:mm").valueOf();
        
        if (moment(dtrIn, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
            dtrInStr = moment(dtrIn, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
        }
        if (moment(dtrOut, "YYYY-MM-DD kk:mm").format('kk:mm') == "24:00") {
            dtrOutStr = moment(dtrOut, "YYYY-MM-DD kk:mm").add(1, 'days').valueOf();
        }

        if ((dtrInStr < shiftStartStr) || (dtrInStr == shiftStartStr)) { // word start
            start = shiftStartStr;
        } else {
            start = dtrInStr;
        }
        if ((dtrOutStr > shiftEndStr) || (dtrOutStr == shiftEndStr)) { // word end
            end = shiftEndStr;
        } else {
            end = dtrOutStr;
        }

    }
    var seconds = end - start;
    if (seconds > 0) {
       var hours = seconds / 3600000;
       if ($('#ahrTypeRendered').val() == 2) {
           var hours = hours - parseFloat($('#bctVal').data("actualbct"));
       } else if (($('#ahrTypeRendered').val() == 3)) {
           var breaks = parseFloat($('#shift').data('break')) / 60;
           var hours = hours - breaks;
       } else {
           var hours = seconds / 3600000;
       }
       humanTimeFormat(hours, function (humanTimeFormat) {
           var renderedOt = {
               otHours: hours.toFixed(2),
               formatted: humanTimeFormat
           };
           callback(renderedOt);
       })
    } else {
        var hours = 0;
        var renderedOt = {
            otHours: hours,
            formatted: '00 hr, 00 min'
        };
        callback(renderedOt);
    }
}

function computeAhrTimeOld(callback) {
    var datedShift = $('#shift').data('datedshift');
    var shiftStartStr = moment(datedShift.start, "YYYY-MM-DD HH:mm").valueOf();
    var shiftEndStr = moment(datedShift.end, "YYYY-MM-DD HH:mm").valueOf();
    var inputTimeStr = moment(inputTime, "YYYY-MM-DD HH:mm").valueOf();

    if ($('#ahrTypeRendered').val() != 3) { //if not word
        var inputTime = moment($('#actualDtrDate').data('datetocompute') + " " + $('#actualDtrTime').val(), 'MM/DD/Y h:mm A').format('YYYY-MM-DD HH:mm');
        var inputTimeStr = moment(inputTime, "YYYY-MM-DD HH:mm").valueOf();
        if ($('#ahrTypeRendered').val() == 1) {
            var start = shiftEndStr;
            var end = inputTimeStr;
        } else if ($('#ahrTypeRendered').val() == 2) {
            var start = inputTimeStr;
            var end = shiftStartStr;
        }
    } else { // if word
        var dtrIn = $('#wordDtrIn').data('datetocompute')
        var dtrOut = $('#wordDtrOut').data('datetocompute')
        var dtrInStr = moment(dtrIn, "YYYY-MM-DD HH:mm").valueOf();
        var dtrOutStr = moment(dtrOut, "YYYY-MM-DD HH:mm").valueOf();
        if ((dtrInStr < shiftStartStr) || (dtrInStr == shiftStartStr)) { // word start
            start = shiftStartStr;
        } else {
            start = dtrInStr;
        }
        if ((dtrOutStr > shiftEndStr) || (dtrOutStr == shiftEndStr)) { // word end
            end = shiftEndStr;
        } else {
            end = dtrOutStr;
        }
    }
    var seconds = end - start;
    if (seconds > 0) {
        var hours = seconds / 3600000;
        if ($('#ahrTypeRendered').val() == 2) {
            var hours = hours - parseFloat($('#bctVal').data("actualbct"));
        } else if (($('#ahrTypeRendered').val() == 3)) {
            var breaks = parseFloat($('#shift').data('break')) / 60;
            var hours = hours - breaks;
        } else {
            var hours = seconds / 3600000;
        }
        humanTimeFormat(hours, function (humanTimeFormat) {
            var renderedOt = {
                otHours: hours.toFixed(2),
                formatted: humanTimeFormat
            };
            callback(renderedOt);
        })
    } else {
        var hours = 0;
        var renderedOt = {
            otHours: hours,
            formatted: '00 hr, 00 min'
        };
        callback(renderedOt);
    }
}

function ifLateOrUt(time, callback) {
    if ((Math.abs(parseInt(time.hours)) == 0) && (Math.abs(parseInt(time.minutes)) == 1)) {
        var humanTime = Math.round(Math.abs(time.minutes)) + " minute";
    } else if ((Math.abs(parseInt(time.hours)) == 0) && (Math.abs(parseInt(time.minutes)) != 1)) {
        var humanTime = Math.round(Math.abs(time.minutes)) + " minutes";
    } else if ((Math.abs(parseInt(time.minutes)) == 0) && (Math.abs(parseInt(time.hours)) == 1)) {
        var humanTime = Math.abs(time.hours) + " hour";
    } else if ((Math.abs(parseInt(time.minutes)) == 0) && (Math.abs(parseInt(time.hours)) != 1)) {
        var humanTime = Math.abs(time.hours) + " hours";
    } else if ((Math.abs(parseInt(time.hours)) > 1) && (Math.abs(parseInt(time.minutes)) == 1)) {
        var humanTime = Math.abs(time.hours) + " hrs & " + Math.round(Math.abs(time.minutes)) + " min";
    } else if ((Math.abs(parseInt(time.hours)) == 1) && (Math.abs(parseInt(time.minutes)) > 1)) {
        var humanTime = Math.abs(time.hours) + " hr & " + Math.round(Math.abs(time.minutes)) + " mins";
    } else if ((Math.abs(parseInt(time.hours)) == 1) && (Math.abs(parseInt(time.minutes)) == 1)) {
        var humanTime = Math.abs(time.hours) + " hr & " + Math.round(Math.abs(time.minutes)) + " min";
    } else if ((Math.abs(parseInt(time.hours)) > 1) && (Math.abs(parseInt(time.minutes)) > 1)) {
        var humanTime = Math.abs(time.hours) + " hrs & " + Math.round(Math.abs(time.minutes)) + " mins";
    }
    callback(humanTime)
    return humanTime;
}

function checkIfAlreadyFiled(ahrTypeVal, schedIdVal, callback) {
    $.when(fetchPostData({
        ahrType: ahrTypeVal,
        schedId: schedIdVal
    }, '/additional_hour/check_if_already_filed')).then(function (checkIfFiled) {
        var checkIfFiledBool = $.parseJSON(checkIfFiled.trim());
        callback(checkIfFiledBool);
    });
}
function getAhrDetails(requestExist) {
     var time = $('#shift').find('option:selected').text().split('-');
     var startTime = moment(time[0], 'hh:mm:ss A').format('HH:mm');
     var endTime = moment(time[1], 'hh:mm:ss A').format('HH:mm');
     $.when(fetchPostData({
         ahrType: $('#ahrTypeRendered').val(),
         scheduleDate: moment($('.dateRendered').val(), 'MM/DD/YYYY').format("YYYY-MM-DD"),
         schedId: $('#shift').val(),
         shiftStart: startTime,
         shiftEnd: endTime,
         empId: $('#shift').data('empid'),
         accountId: $('#shift').data('accountid')
     }, '/additional_hour/get_ot_details')).then(function (dtrDetails) {
         var ahrDetails = jQuery.parseJSON(dtrDetails.trim());
         var lateStat = 0;
         var utStat = 0;
         var errStat = 0;
         var tard = "";
         if (ahrDetails == 0) {
             resetRenderedAhrForm();
             $('#errNotifyRenderedAhrMssg').html('Sorry. You have no DTR logs for this schedule. Please inform your supervisor.');
             $('#errNotifyRenderedAhr').show();
             $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
         } else {
             $('#errNotifyRenderedAhr').hide();
             $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);

             var dtrIn = '';
             var dtrOut = '';
             $.each(ahrDetails.dtrLog, function (index, value) {
                 if (value.entry === "I") {
                     dtrIn = value.log;
                     if (moment(value.log, 'Y-MM-DD HH:mm:ss').format('HH:mm') == '00:00') {
                         dtrIn = moment(value.log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD') + '24:00:00';
                     }
                 } else if (value.entry === "O") {
                     dtrOut = value.log;
                     if (moment(value.log, 'Y-MM-DD HH:mm:ss').format('HH:mm') == '00:00') {
                         dtrOut = moment(value.log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD') + ' 24:00:00';
                     }
                 }
             })
             $('#shift').data('datedshift', ahrDetails.datedShifts);
             var errorStat = 0;
             if ($('#ahrTypeRendered').val() == 1) { // IF AFTER SHIFT
                let shiftEnd = moment(ahrDetails.datedShifts.end).valueOf();
                let clockOutTime = moment(dtrOut).valueOf();
                afterOt = (clockOutTime - shiftEnd) / 3600000;
                 if (dtrOut == "") {
                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
                     $('#errNotifyRenderedAhrMssg').html('<b>Log Out Log not found!</b> Your log out log is needed in order to proceed after shift filing.');
                     $('#errNotifyRenderedAhr').show();
                     $('#requestAhrBtn').prop('disabled', true);
                 } else {
                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
                     $('#errNotifyRenderedAhr').hide();
                     $('#requestAhrBtn').prop('disabled', false);
                     if (ahrDetails.ut == 0) {
                         utStat = 0;
                         $("#dtrTardNotWord").hide();
                     } else {
                         utStat = 1;
                         humanTimeFormat(ahrDetails.ut.hoursTotal, function (formattedUt) {
                             tard += "<div class='col-md-12 pl-0'>" + formattedUt + " UT<div>";
                             $('#dtrTardNotWordVal').html(tard);
                             $("#dtrTardNotWord").show();
                         });
                     }
                     $('#errNotifyRenderedAhr').hide()
                     $('#requestAhrBtn').prop('disabled', false);
                    //  dateAdjust(moment(dtrOut, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD HH:mm'), function (dateAdj) {
                         $('#dtrLogVal').text(moment(dtrOut, 'Y-MM-DD kk:mm:ss').format('MMM DD, Y hh:mm A'));
                         $('#actualDtrDate').val(moment(dtrOut, 'Y-MM-DD kk:mm:ss').format('MM/DD/Y'));
                         $('#actualDtrDate').data('datetocompute', moment(dtrOut, 'Y-MM-DD kk:mm:ss').format('MM/DD/Y'));
                         setDate("#actualDtrDate", moment(dtrOut, 'Y-MM-DD kk:mm:ss').format('MM/DD/Y'));
                         $('#actualDtrTime').val(moment(dtrOut, 'Y-MM-DD kk:mm:ss A').format('hh:mm A'));
                         initializeActualDtrTimePicker('#actualDtrTime', moment(dtrOut, 'Y-MM-DD kk:mm:ss').format('hh:mm A'))
                         $('#renderedAhrFilingForm').formValidation('revalidateField', 'actualDtrDate');
                         $('#renderedAhrFilingForm').formValidation('revalidateField', 'actualDtrTime');
                    //  })
                 }
                //  computeAhrTime(function (renderedOt) {
                //     //  $('#renderedOt').text(renderedOt.formatted);
                //     //  $('#renderedOt').data('othours', renderedOt.otHours);
                //  });
             } else if ($('#ahrTypeRendered').val() == 2) {
                 if (dtrIn == "") {
                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
                     $('#errNotifyRenderedAhrMssg').html('<b>Log In Log not found!</b> Your log In log is needed in order to proceed Pre shift filing.');
                     $('#errNotifyRenderedAhr').show();
                     $('#requestAhrBtn').prop('disabled', true);
                 } else {
                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
                     $('#errNotifyRenderedAhr').hide();
                     $('#requestAhrBtn').prop('disabled', false);
                     if (ahrDetails.late == 0) {
                         lateStat = 0;
                         $("#dtrTardNotWord").hide();
                     } else {
                         lateStat = 1;
                         humanTimeFormat(ahrDetails.late.hoursTotal, function (formattedLate) {
                             tard += "<div class='col-md-12 pl-0'>" + formattedLate + " Late<div>";
                             $('#dtrTardNotWordVal').html(tard);
                             $("#dtrTardNotWord").show();
                         });
                     }
                     $('#errNotifyRenderedAhr').hide()
                     $('#requestAhrBtn').prop('disabled', false);
                    //  dateAdjust(moment(dtrIn, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD HH:mm'), function (dateAdj) {
                          $('#dtrLogVal').text(moment(dtrIn, 'Y-MM-DD kk:mm:ss').format('MMM DD, Y hh:mm A'));
                          $('#actualDtrDate').val(moment(dtrIn, 'Y-MM-DD kk:mm:ss').format('MM/DD/Y'));
                          $('#actualDtrDate').data('datetocompute', moment(dtrIn, 'Y-MM-DD kk:mm:ss').format('MM/DD/Y'));
                               setDate("#actualDtrDate", moment(dtrIn, 'Y-MM-DD kk:mm:ss').format('MM/DD/Y'));
                          $('#actualDtrTime').val(moment(dtrIn, 'Y-MM-DD kk:mm:ss A').format('hh:mm A'));
                          initializeActualDtrTimePicker('#actualDtrTime', moment(dtrIn, 'Y-MM-DD kk:mm:ss').format('hh:mm A'))
                          $('#renderedAhrFilingForm').formValidation('revalidateField', 'actualDtrDate');
                          $('#renderedAhrFilingForm').formValidation('revalidateField', 'actualDtrTime');
                    //  })
                     $('#bctVal').data("actualbct", ahrDetails.actual_bct);
                     humanTimeFormat(ahrDetails.actual_bct, function (formattedBct) {
                         $('#bctVal').text(formattedBct);
                     })
                 }
             } else {
                 if ((dtrIn == "") && (dtrOut == "")) {
                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
                     $('#errNotifyRenderedAhrMssg').html('<b>Incomplete Logs!</b> Please double if logs are complete before filing a WORD.');
                     $('#errNotifyRenderedAhr').show();
                     $('#requestAhrBtn').prop('disabled', true);
                 } else {
                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
                     $('#errNotifyRenderedAhr').hide();
                     $('#requestAhrBtn').prop('disabled', false);
                     $('#shift').data('break', ahrDetails.break.minutes);
                     var dtrIssueMssg = "";
                     if ((ahrDetails.ut == 0) && (ahrDetails.late != 0)) {
                         var dtrIssue = true;
                         ifLateOrUt(ahrDetails.late, function (lateDuration) {
                             dtrIssueMssg = "Please be informed that you are " + lateDuration + " late on this shift.";
                         });
                         humanTimeFormat(ahrDetails.late.hoursTotal, function (formattedLate) {
                             tard += "<div class='col-md-12 pl-0'>" + formattedLate + " Late<div>";
                         })
                         $('#shift').data('late', 1);
                         $('#shift').data('ut', 0);
                     } else if ((ahrDetails.ut != 0) && (ahrDetails.late == 0)) {
                         var dtrIssue = true;
                         ifLateOrUt(ahrDetails.ut, function (utDuration) {
                             dtrIssueMssg = "Please be informed that you clock out " + utDuration + " earlier from your shift.";
                         });
                         humanTimeFormat(ahrDetails.ut.hoursTotal, function (formattedUt) {
                             tard += "<div class='col-md-12 pl-0'>" + formattedUt + " UT<div>";
                         });
                         $('#shift').data('late', 0);
                         $('#shift').data('ut', 1);
                     } else if ((ahrDetails.ut != 0) && (ahrDetails.late != 0)) {
                         var dtrIssue = true;
                         var late = '';
                         var ut = '';
                         ifLateOrUt(ahrDetails.late, function (lateDuration) {
                             late = lateDuration
                         });
                         ifLateOrUt(ahrDetails.ut, function (utDuration) {
                             ut = utDuration
                         });
                         humanTimeFormat(ahrDetails.ut.hoursTotal, function (formattedUt) {
                             tard += "<div class='col-md-12 pl-0'>" + formattedUt + " UT<div>";
                         });
                         humanTimeFormat(ahrDetails.late.hoursTotal, function (formattedLate) {
                             tard += "<div class='col-md-12 pl-0'>" + formattedLate + " Late<div>";

                         })
                         dtrIssueMssg = "Please be informed that you are " + late + " late on this shift. You also clock out " + ut + " earlier from your shift.";
                         $('#shift').data('late', 1);
                         $('#shift').data('ut', 1);
                     }
                     if (dtrIssue) {
                         $('#infoNotifyRenderedAhrMssg').html(dtrIssueMssg);
                         $('#infoNotifyRenderedAhr').show();
                         $('#dtrTardVal').html(tard);
                         // $('#requestAhrBtn').prop('disabled', true);
                     } else {
                         $('#dtrTardVal').html('NONE');
                         $('#infoNotifyRenderedAhr').hide();
                         // $('#requestAhrBtn').prop('disabled', false);
                     }
                    //  dateAdjust(dtrOut, function (dateAdj) {
                        console.log("DTR OUT: " + dtrOut);
                         $('#wordDtrOut').text(moment(dtrOut, 'Y-MM-DD kk:mm:ss').format('MMM DD, Y hh:mm A'));
                         $('#wordDtrOut').data('datetocompute', dtrOut);
                    //  })
                    //  dateAdjust(dtrIn, function (dateAdj) {
                         $('#wordDtrIn').text(moment(dtrIn, 'Y-MM-DD kk:mm:ss').format('MMM DD, Y hh:mm A'));
                         $('#wordDtrIn').data('datetocompute', dtrIn);
                    //  })
                     $('#bctWordVal').data("actualbct", ahrDetails.actual_bct);
                     var breakMin = (ahrDetails.break.minutes) / 60;
                     humanTimeFormat(ahrDetails.actual_bct, function (formattedBct) {
                         $('#bctWordVal').text(formattedBct);
                     });
                     humanTimeFormat(breakMin, function (formattedBreak) {
                         $('#breakVal').text(formattedBreak);
                     })
                    //  computeAhrTime(function (renderedOt) {
                    //      $('#renderedOt').text(renderedOt.formatted);
                    //      $('#renderedOt').data('othours', renderedOt.otHours);
                    //  });
                 }
             }
             if (requestExist) {
                 $('#errNotifyRenderedAhrMssg').html('<b>Request Exist: </b>You have already filed a similar type of request for this schedule.');
                 $('#errNotifyRenderedAhr').show();
                 $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
             } else {
                 $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
                 $('#errNotifyRenderedAhr').hide();
                 if ((utStat == 1) && ($('#ahrTypeRendered').val() == 1)) {
                     errStat = 1;
                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
                     ifLateOrUt(ahrDetails.ut, function (utDuration) {
                         $('#errNotifyRenderedAhrMssg').html('Sorry. You cannot file After shift OT because you clock out ' + utDuration + ' earlier from your shift');
                         $('#errNotifyRenderedAhr').show();
                         $('#requestAhrBtn').prop('disabled', true);
                     });
                 } else if ((lateStat == 1) && ($('#ahrTypeRendered').val() == 2)) {
                     errstat = 1;
                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
                     ifLateOrUt(ahrDetails.late, function (lateDuration) {
                         $('#errNotifyRenderedAhrMssg').html('Sorry. You cannot file Pre shift overtime because you are ' + lateDuration + ' late.');
                         $('#errNotifyRenderedAhr').show();
                         $('#requestAhrBtn').prop('disabled', true);
                     });
                 }
                 if (!errStat) {
                     computeAhrTime(function (renderedOt) {
                         $('#renderedOt').text(renderedOt.formatted);
                         $('#renderedOt').data('othours', renderedOt.otHours);
                         if (renderedOt.otHours == 0) {
                             $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
                             $('#errNotifyRenderedAhrMssg').html('<b>Insufficient Coverage!</b> You don\'t have enough time to file an Additional Hour Request.');
                             $('#errNotifyRenderedAhr').show();
                             $('#requestAhrBtn').prop('disabled', true);
                         } else {
                             $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
                             $('#errNotifyRenderedAhr').hide();
                             $('#requestAhrBtn').prop('disabled', false);
                         }
                     });
                 }
             }
         }
     });
}

// function getAhrDetails(requestExist) {
//     var time = $('#shift').find('option:selected').text().split('-');
//     var startTime = moment(time[0], 'hh:mm:ss A').format('HH:mm');
//     var endTime = moment(time[1], 'hh:mm:ss A').format('HH:mm');
//     $.when(fetchPostData({
//         ahrType: $('#ahrTypeRendered').val(),
//         scheduleDate: moment($('.dateRendered').val(), 'MM/DD/YYYY').format("YYYY-MM-DD"),
//         schedId: $('#shift').val(),
//         shiftStart: startTime,
//         shiftEnd: endTime,
//         empId: $('#shift').data('empid'),
//         accountId: $('#shift').data('accountid')
//     }, '/additional_hour/get_ot_details')).then(function (dtrDetails) {
//         var ahrDetails = jQuery.parseJSON(dtrDetails.trim());
//         var lateStat = 0;
//         var utStat = 0;
//         var errStat = 0;
//         var tard = "";
//         if (ahrDetails == 0) {
//             resetRenderedAhrForm();
//             $('#errNotifyRenderedAhrMssg').html('Sorry. You have no DTR logs for this schedule. Please inform your supervisor.');
//             $('#errNotifyRenderedAhr').show();
//             $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
//         } else {
//             $('#errNotifyRenderedAhr').hide();
//             $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);

//             var dtrIn = '';
//             var dtrOut = '';
//             $.each(ahrDetails.dtrLog, function (index, value) {
//                 if (value.entry === "I") {
//                     dtrIn = value.log;
//                     if (moment(value.log, 'Y-MM-DD HH:mm:ss').format('HH:mm') == '00:00') {
//                          dtrIn = moment(value.log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD') + '24:00:00';
//                     }
//                 } else if (value.entry === "O") {
//                     dtrOut = value.log;
//                     if (moment(value.log, 'Y-MM-DD HH:mm:ss').format('HH:mm') == '00:00') {
//                         dtrOut = moment(value.log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD') + '24:00:00';
//                     }
//                 }
//             })
//             $('#shift').data('datedshift', ahrDetails.datedShifts);
//             var errorStat = 0;
//             if ($('#ahrTypeRendered').val() == 1) { // IF AFTER SHIFT
//                 if(dtrOut == ""){
//                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
//                     $('#errNotifyRenderedAhrMssg').html('<b>Log Out Log not found!</b> Your log out log is needed in order to proceed after shift filing.');
//                     $('#errNotifyRenderedAhr').show();
//                     $('#requestAhrBtn').prop('disabled', true);
//                 }else{
//                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
//                     $('#errNotifyRenderedAhr').hide();
//                     $('#requestAhrBtn').prop('disabled', false);
//                     if (ahrDetails.ut == 0) {
//                         utStat = 0;
//                         $("#dtrTardNotWord").hide();
//                     } else {
//                         utStat = 1;
//                         humanTimeFormat(ahrDetails.ut.hoursTotal, function (formattedUt) {
//                             tard += "<div class='col-md-12 pl-0'>" + formattedUt + " UT<div>";
//                             $('#dtrTardNotWordVal').html(tard);
//                             $("#dtrTardNotWord").show();
//                         });
//                     }
//                     $('#errNotifyRenderedAhr').hide()
//                     $('#requestAhrBtn').prop('disabled', false);
//                     dateAdjust(moment(dtrOut, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD HH:mm'), function (dateAdj) {
//                         $('#dtrLogVal').text(moment(dateAdj.show, 'Y-MM-DD HH:mm').format('MMM DD, Y hh:mm A'));
//                         $('#actualDtrDate').val(moment(dateAdj.show, 'Y-MM-DD HH:mm').format('MM/DD/Y'));
//                         $('#actualDtrDate').data('datetocompute', moment(dateAdj.compute, 'Y-MM-DD HH:mm').format('MM/DD/Y'));
//                         setDate("#actualDtrDate", moment(dateAdj.show, 'Y-MM-DD HH:mm').format('MM/DD/Y'));
//                         $('#actualDtrTime').val(moment(dtrOut, 'Y-MM-DD hh:mm:ss A').format('hh:mm A'));
//                         initializeActualDtrTimePicker('#actualDtrTime', moment(dtrOut, 'Y-MM-DD hh:mm:ss A').format('hh:mm A'))
//                         $('#renderedAhrFilingForm').formValidation('revalidateField', 'actualDtrDate');
//                         $('#renderedAhrFilingForm').formValidation('revalidateField', 'actualDtrTime');
//                     })
//                 }
//                 // computeAhrTime(function (renderedOt) {
//                 //     $('#renderedOt').text(renderedOt.formatted);
//                 //     $('#renderedOt').data('othours', renderedOt.otHours);
//                 // });
//             } else if ($('#ahrTypeRendered').val() == 2) {
//                 if (dtrIn == "") {
//                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
//                     $('#errNotifyRenderedAhrMssg').html('<b>Log In Log not found!</b> Your log In log is needed in order to proceed Pre shift filing.');
//                     $('#errNotifyRenderedAhr').show();
//                     $('#requestAhrBtn').prop('disabled', true);
//                 } else {
//                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
//                     $('#errNotifyRenderedAhr').hide();
//                     $('#requestAhrBtn').prop('disabled', false);
//                     if (ahrDetails.late == 0) {
//                         lateStat = 0;
//                         $("#dtrTardNotWord").hide();
//                     } else {
//                         lateStat = 1;
//                         humanTimeFormat(ahrDetails.late.hoursTotal, function (formattedLate) {
//                             tard += "<div class='col-md-12 pl-0'>" + formattedLate + " UT<div>";
//                             $('#dtrTardNotWordVal').html(tard);
//                             $("#dtrTardNotWord").show();
//                         });
//                     }
//                     $('#errNotifyRenderedAhr').hide()
//                     $('#requestAhrBtn').prop('disabled', false);
//                     dateAdjust(moment(dtrIn, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD HH:mm'), function (dateAdj) {
//                         $('#dtrLogVal').text(moment(dateAdj.show, 'Y-MM-DD HH:mm').format('MMM DD, Y hh:mm A'));
//                         $('#actualDtrDate').val(moment(dateAdj.show, 'Y-MM-DD HH:mm').format('MM/DD/Y'));
//                         $('#actualDtrDate').data('datetocompute', moment(dateAdj.compute, 'Y-MM-DD HH:mm').format('MM/DD/Y'));
//                         setDate("#actualDtrDate", moment(dateAdj.show, 'Y-MM-DD HH:mm').format('MM/DD/Y'));
//                         $('#actualDtrTime').val(moment(dateAdj.show, 'Y-MM-DD HH:mm').format('hh:mm A'));
//                         initializeActualDtrTimePicker('#actualDtrTime', moment(dateAdj.show, 'Y-MM-DD HH:mm').format('hh:mm A'));
//                         $('#renderedAhrFilingForm').formValidation('revalidateField', 'actualDtrDate');
//                         $('#renderedAhrFilingForm').formValidation('revalidateField', 'actualDtrTime');
//                     })
//                     $('#bctVal').data("actualbct", ahrDetails.actual_bct);
//                     humanTimeFormat(ahrDetails.actual_bct, function (formattedBct) {
//                         $('#bctVal').text(formattedBct);
//                     })
//                 }
//             } else {
//                 if ((dtrIn == "") && (dtrOut == "")) {
//                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
//                     $('#errNotifyRenderedAhrMssg').html('<b>Incomplete Logs!</b> Please double if logs are complete before filing a WORD.');
//                     $('#errNotifyRenderedAhr').show();
//                     $('#requestAhrBtn').prop('disabled', true);
//                 } else {
//                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
//                     $('#errNotifyRenderedAhr').hide();
//                     $('#requestAhrBtn').prop('disabled', false);
//                     $('#shift').data('break', ahrDetails.break.minutes);
//                     var dtrIssueMssg = "";
//                     if ((ahrDetails.ut == 0) && (ahrDetails.late != 0)) {
//                         var dtrIssue = true;
//                         ifLateOrUt(ahrDetails.late, function (lateDuration) {
//                             dtrIssueMssg = "Please be informed that you are " + lateDuration + " late on this shift.";
//                         });
//                         humanTimeFormat(ahrDetails.late.hoursTotal, function (formattedLate) {
//                             tard += "<div class='col-md-12 pl-0'>" + formattedLate + " Late<div>";
//                         })
//                         $('#shift').data('late', 1);
//                         $('#shift').data('ut', 0);
//                     } else if ((ahrDetails.ut != 0) && (ahrDetails.late == 0)) {
//                         var dtrIssue = true;
//                         ifLateOrUt(ahrDetails.ut, function (utDuration) {
//                             dtrIssueMssg = "Please be informed that you clock out " + utDuration + " earlier from your shift.";
//                         });
//                         humanTimeFormat(ahrDetails.ut.hoursTotal, function (formattedUt) {
//                             tard += "<div class='col-md-12 pl-0'>" + formattedUt + " UT<div>";
//                         });
//                         $('#shift').data('late', 0);
//                         $('#shift').data('ut', 1);
//                     } else if ((ahrDetails.ut != 0) && (ahrDetails.late != 0)) {
//                         var dtrIssue = true;
//                         var late = '';
//                         var ut = '';
//                         ifLateOrUt(ahrDetails.late, function (lateDuration) {
//                             late = lateDuration
//                         });
//                         ifLateOrUt(ahrDetails.ut, function (utDuration) {
//                             ut = utDuration
//                         });
//                         humanTimeFormat(ahrDetails.ut.hoursTotal, function (formattedUt) {
//                             tard += "<div class='col-md-12 pl-0'>" + formattedUt + " UT<div>";
//                         });
//                         humanTimeFormat(ahrDetails.late.hoursTotal, function (formattedLate) {
//                             tard += "<div class='col-md-12 pl-0'>" + formattedLate + " Late<div>";

//                         })
//                         dtrIssueMssg = "Please be informed that you are " + late + " late on this shift. You also clock out " + ut + " earlier from your shift.";
//                         $('#shift').data('late', 1);
//                         $('#shift').data('ut', 1);
//                     }
//                     if (dtrIssue) {
//                         $('#infoNotifyRenderedAhrMssg').html(dtrIssueMssg);
//                         $('#infoNotifyRenderedAhr').show();
//                         $('#dtrTardVal').html(tard);
//                         // $('#requestAhrBtn').prop('disabled', true);
//                     } else {
//                         $('#dtrTardVal').html('NONE');
//                         $('#infoNotifyRenderedAhr').hide();
//                         // $('#requestAhrBtn').prop('disabled', false);
//                     }
//                     dateAdjust(dtrOut, function (dateAdj) {
//                         $('#wordDtrOut').text(moment(dateAdj.show, 'Y-MM-DD HH:mm').format('MMM DD, Y hh:mm A'));
//                         $('#wordDtrOut').data('datetocompute', moment(dateAdj.compute, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'));
//                     })
//                     dateAdjust(dtrIn, function (dateAdj) {
//                         $('#wordDtrIn').text(moment(dateAdj.show, 'Y-MM-DD HH:mm').format('MMM DD, Y hh:mm A'));
//                         $('#wordDtrIn').data('datetocompute', moment(dateAdj.compute, 'Y-MM-DD HH:mm').format('Y-MM-DD HH:mm'));
//                     })
//                     $('#bctWordVal').data("actualbct", ahrDetails.actual_bct);
//                     var breakMin = (ahrDetails.break.minutes) / 60;
//                     humanTimeFormat(ahrDetails.actual_bct, function (formattedBct) {
//                         $('#bctWordVal').text(formattedBct);
//                     });
//                     humanTimeFormat(breakMin, function (formattedBreak) {
//                         $('#breakVal').text(formattedBreak);
//                     })
//                     computeAhrTime(function (renderedOt) {
//                         $('#renderedOt').text(renderedOt.formatted);
//                         $('#renderedOt').data('othours', renderedOt.otHours);
//                     });
//                 }   
//             }
//             if (requestExist) {
//                 $('#errNotifyRenderedAhrMssg').html('<b>Request Exist: </b>You have already filed a similar type of request for this schedule.');
//                 $('#errNotifyRenderedAhr').show();
//                 $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
//             } else {
//                 $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
//                 $('#errNotifyRenderedAhr').hide();
//                 if ((utStat == 1) && ($('#ahrTypeRendered').val() == 1)) {
//                     errStat = 1;
//                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
//                     ifLateOrUt(ahrDetails.ut, function (utDuration) {
//                         $('#errNotifyRenderedAhrMssg').html('Sorry. You cannot file After shift OT because you clock out ' + utDuration + ' earlier from your shift');
//                         $('#errNotifyRenderedAhr').show();
//                         $('#requestAhrBtn').prop('disabled', true);
//                     });
//                 } else if ((lateStat == 1) && ($('#ahrTypeRendered').val() == 2)) {
//                     errstat = 1;
//                     $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
//                     ifLateOrUt(ahrDetails.late, function (lateDuration) {
//                         $('#errNotifyRenderedAhrMssg').html('Sorry. You cannot file Pre shift overtime because you are ' + lateDuration + ' late.');
//                         $('#errNotifyRenderedAhr').show();
//                         $('#requestAhrBtn').prop('disabled', true);
//                     });
//                 }
//                 if (!errStat) {
//                     computeAhrTime(function (renderedOt) {
//                         $('#renderedOt').text(renderedOt.formatted);
//                         $('#renderedOt').data('othours', renderedOt.otHours);
//                         if (renderedOt.otHours == 0) {
//                             $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
//                             $('#errNotifyRenderedAhrMssg').html('<b>Insufficient Coverage!</b> You don\'t have enough time to file an Additional Hour Request.');
//                             $('#errNotifyRenderedAhr').show();
//                             $('#requestAhrBtn').prop('disabled', true);
//                         } else {
//                             $('#actualDtrDate, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
//                             $('#errNotifyRenderedAhr').hide();
//                             $('#requestAhrBtn').prop('disabled', false);
//                         }
//                     });
//                 }
//             }
//         }
//     });
// }

function requestAhr(ahrTypeVal, schedIdVal, otStartDateVal, otStartTimeVal, otEndDateVal, otEndTimeVal, otHoursVal, reasonVal, formTypeVal, acc_time_id, callback) {
    $.when(fetchPostData({
        ahrType: ahrTypeVal,
        schedId: schedIdVal,
        otStartDate: otStartDateVal,
        otStartTime: otStartTimeVal,
        otEndDate: otEndDateVal,
        otEndTime: otEndTimeVal,
        otHours: otHoursVal,
        reason: reasonVal,
        formType: formTypeVal,
        accTimeId: acc_time_id
    }, '/additional_hour/request_ahr')).then(function (insertStat) {
        callback(insertStat)
    });
}
function check_if_tito_exist(callback){
    $.when(fetchPostData({
        scheduleDate: moment($('.dateRendered').val(), 'MM/DD/Y').format('Y-MM-DD')
    }, '/additional_hour/check_if_tito_file')).then(function (exist) {
        console.log(exist);
        var tito_exist = jQuery.parseJSON(exist.trim());
        callback(tito_exist);
    });
}
function getShift(date, callback) {
    $.when(fetchPostData({
        scheduleDate: date
    }, '/additional_hour/get_shift')).then(function (shift) {
        var shift = jQuery.parseJSON(shift.trim());
        var optionsAsString = "";
        // console.log(shift);
        if (!shift) {
            var exist = 0;
            $('select[name="shift"]').html(optionsAsString);
        } else {
            var exist = 1;
            $('#errNotifyRenderedAhr').hide();
            if (shift.length > 1) {
                optionsAsString += "<option value ='"+''+"'></option>";
                // $('#renderedAhrFilingForm').formValidation('updateStatus', 'shift', 'VALIDATED').formValidation('validateField', 'shift');
            }
            $.each(shift, function (index, value) {
                optionsAsString += "<option value='" + value.schedId + "' shiftType='" + value.type + "'>" + value.time_start + " - " + value.time_end + "</option>";
                $('#shift').data('empid', value.empId);
                $('#shift').data('accountid', value.accountId);
            });
            $('select[name="shift"]').html(optionsAsString);

            $('select[name="shift"]').trigger('change');
            $('#renderedAhrFilingForm').formValidation('validateField', 'shift');

            if (shift.length == 1) {
                var ahrTypeVal = $('#ahrTypeRendered').val();
                var schedIdVal = $('#shift').val();
               

                checkIfAlreadyFiled(ahrTypeVal, schedIdVal, function (checkIfFiledBool) {
                    getAhrDetails(parseInt(checkIfFiledBool.exist));
                    identifyShiftType();
                })
            }else{
                //  $('#renderedAhrFilingForm').formValidation('enableFieldValidators','shift', false);
            }
         
        }
        callback(exist)
    });
}

function initiallyHide() {
    hideElement(['#errNotifyRenderedAhr', '#errNotifyUnrenderedAhr', '#infoNotifyRenderedAhr', '#errNotifyApprovalAhr', '#wordDetails', '#bct', '#additional', '.withRendered', '#shiftWord']);
}

function validateRenderedAhrForm(callback) {

    var fieldError = 0;
    var errorMssg = "Fields are required: ";
    if ($('#ahrTypeRendered').val() == '') {
        errorMssg += 'AHR TYPE';
        fieldError = 1;
    }

    if ($('#dateRendered').val() == '') {
        errorMssg += ' Date Rendered,';
        fieldError = 1;
    }

    if ($('#shift').val() == null || $('#shift').val() == 0) {
        errorMssg += ' Shift,';
        fieldError = 1;
    }
    if ($('#ahrTypeRendered').val() != 3) {
        if ($('#actualDtrTime').val() == '') {
            errorMssg += ' ACTUAL DTR,';
            fieldError = 1;
        }
    }

    if (!$.trim($("#reason").val())) {
        errorMssg += ' Reason,';
        fieldError = 1;
    }

    // if ($('#renderedOt').data('otHours') == undefined || $('#renderedOt').data('otHours') == 0) {
    //     fieldError = 1;
    // }

    if (fieldError) {
        $('#errNotifyRenderedAhrMssg').html(errorMssg);
        $('#errNotifyRenderedAhr').show();
        // $('#requestManualAhrBtn').prop('disabled', true);
    } else {
        $('#errNotifyRenderedAhr').hide();
        // $('#requestManualAhrBtn').prop('disabled', false);
    }
    callback(fieldError);
}

function getSpecificShift(accTimeId){
     $.when(fetchPostData({
        accTimeId: accTimeId,
    }, '/additional_hour/get_specific_shift_request')).then(function (shift) {
        var specShiftObj = $.parseJSON(shift.trim());
        $('#shiftRecord').text(specShiftObj.time_start+" - "+specShiftObj.time_end);
    })
}

function wordRequestDetails(ahrId, callback) {
    $.when(fetchPostData({
        ahr_id: ahrId,
    }, '/additional_hour/get_word_request_details')).then(function (wordRequest) {
        var wordRequestDetails = jQuery.parseJSON(wordRequest.trim());
        callback(wordRequestDetails);
    });
}

// RECORD INFO 
function getOtDetailsRecord(ahrDetails) {
    $.when(fetchPostData({
        ahrType: ahrDetails.additionalHourTypeId,
        scheduleDate: ahrDetails.sched_date,
        schedId: ahrDetails.schedId,
        shiftStart: ahrDetails.time_start,
        shiftEnd: ahrDetails.time_end,
        empId: ahrDetails.emp_id,
        accountId: ahrDetails.acc_id
    }, '/additional_hour/get_ot_details')).then(function (dtrDetails) {
        var dtrDetailsVal = $.parseJSON(dtrDetails.trim());
        $('#breakRecord').hide();
        $('#tardRecord').hide();
        $('#bctRecord').hide();
        //DTR DETAILS
        if (ahrDetails.requestType == 'rendered') {
            if (parseInt(ahrDetails.additionalHourTypeId) == 2) {
                $('#breakRecord').hide();
                $('#tardRecord').hide();
                if (parseFloat(dtrDetailsVal.actual_bct) > 0) {
                    humanTimeFormat(dtrDetailsVal.actual_bct, function (formattedBct) {
                        $('#bctRecordVal').text(formattedBct);
                        $('#bctRecord').show();
                    })
                } else {
                    $('#bctRecord').hide();
                }
            } else if (parseInt(ahrDetails.additionalHourTypeId) == 3) {
                $('#bctRecord').hide();
                //Tardines
                var late = 0;
                var ut = 0;
                if (parseFloat(dtrDetailsVal.late) != 0) {
                    late = 1;
                    humanTimeFormat(dtrDetailsVal.late.hoursTotal, function (formattedLate) {
                        $('#lateRecord').text(formattedLate);
                        $('#lateRecord').show();
                    })
                } else {
                    $('#lateRecord').hide();
                }
                if (parseFloat(dtrDetailsVal.ut) != 0) {
                    ut = 1
                    humanTimeFormat(dtrDetailsVal.ut.hoursTotal, function (formattedUt) {
                        $('#utRecord').text(formattedUt);
                        $('#utRecord').show();
                    })
                } else {
                    $('#utRecord').hide();
                }
                if (ut || late) {
                    $('#tardRecord').show();
                } else {
                    $('#tardRecord').hide();
                }

                // BREAK
                var breaks;
                if (parseInt(dtrDetailsVal.break.minutes) > 0){
                    var breakHr = dtrDetailsVal.break.minutes / 60;
                    humanTimeFormat(breakHr, function (formattedBreak) {
                        breaks = formattedBreak;
                        $('#breakRecord').show();
                    });
                }else{
                    breaks = "None";
                }
                $('#breakRecordVal').text(breaks);
            }
        } else {
            if (parseInt(ahrDetails.additionalHourTypeId) == 2) {
                $('#breakRecord').hide();
                if (parseFloat(dtrDetailsVal.actual_bct) > 0) {
                    humanTimeFormat(dtrDetailsVal.late.hoursTotal, function (formattedBct) {
                        $('#bctRecordVal').text(formattedBct);
                        $('#bctRecord').show();
                    })
                } else {
                    $('#bctRecord').hide();
                }
            } else if (parseInt(ahrDetails.additionalHourTypeId) == 3) {
                $('#bctRecord').hide();
                var breaks;
                
                wordRequestDetails(ahrDetails.additionalHourRequestId, function (wordRequestDetails) {
                    getSpecificShift(wordRequestDetails.accTimeId);
                   getShiftBreak(wordRequestDetails.accTimeId, function (breakShift) {
                       breakVal = breakShift.minutes;
                       if (parseInt(breakShift.minutes) > 0) {
                           var breakHrUnrenderedWord = breakShift.minutes / 60;

                           humanTimeFormat(breakHrUnrenderedWord, function (formattedBreak) {
                               breaksUnrenderedWord = formattedBreak;
                           });
                       } else {
                           breaksUnrenderedWord = "None";
                       }
                       $('#additionalDtrDetailsVal').text(breaksUnrenderedWord);
                       $('#breakRecordVal').text(breaksUnrenderedWord);
                   })
                });
            }
        }
        $('#recordInfoAhrModal').modal('show');
    });
}

function getApprovalRecordInfo(ahrId, callback) {
    $.when(fetchPostData({
        ahr_id: ahrId
    }, '/additional_hour/get_request_details')).then(function (record) {
        var recordObj = $.parseJSON(record.trim());
        callback(recordObj);
    });
}

function displayRecordInfoWithTime(ahrId){
     $.when(fetchPostData({
         ahr_id: ahrId
     }, '/additional_hour/get_request_details')).then(function (record) {
         var recordObj = $.parseJSON(record.trim());
         if (parseInt(recordObj.additionalHourTypeId) == 1) {
             var ahrType = 'After Shift';
         } else if (parseInt(recordObj.additionalHourTypeId) == 2) {
             var ahrType = 'Pre Shift';
         } else {
             var ahrType = 'WORD';
         }
         var nameExt = "";
        //  if ((recordObj.nextension !== "none") && (recordObj.nextension !== "")) {
        //      nameExt = recordObj.nextension;
        //  }
         if (recordObj.mname == '') {
             var midName = "";
         } else {
             var midName = recordObj.mname.charAt(0) + "."
         }
         var empPic = imagePath + "" + recordObj.pic;
         $('#empPicApproval').attr('src', empPic)
         $('#empNameRecord').text(recordObj.fname + " " + midName + " " + recordObj.lname + " ");
         $('#empAccountRecord').text(recordObj.accountDescription);
         $('#empJobRecord').text(recordObj.positionDescription);
         $('#ahrNoRecord').text(recordObj.additionalHourRequestId.padLeft(8));
         $('#dateFiledRecord').text(moment(recordObj.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('MMM D, Y h:mm A'));
         $('#filingTypeRecord').text(recordObj.requestType);
         $('#ahrTypeRecord').text(ahrType);
         $('#dateRenderedRecord').text(moment(recordObj.sched_date, 'Y-MM-DD').format('MMM D, Y'));
         $('#shiftRecord').text(recordObj.time_start + " - " + recordObj.time_end);
         $('#shiftTypeRecord').text(recordObj.schedType);
         $('#approvalStatRecord').text(recordObj.statusDescription);
         $('#reasonRecord').text(recordObj.reason);
         $('#startOtRecord').text(moment(recordObj.otStartDate , 'Y-MM-DD').format('MMM D, Y') + " " + moment(recordObj.otStartTime, 'kk:mm').format('h:mm A'));
         $('#endOtRecord').text(moment(recordObj.otEndDate, 'Y-MM-DD').format('MMM D, Y') + " " + moment(recordObj.otEndTime, 'kk:mm').format('h:mm A'));
         humanTimeFormat(recordObj.totalOtHours, function (humanTimeFormat) {
             $('#renderedOtRecord').text(humanTimeFormat);
         });
         getOtDetailsRecord(recordObj);
     });
}

function displayRecordInfoWithoutTime(ahrId) {
    $.when(fetchPostData({
        ahr_id: ahrId
    }, '/additional_hour/request_details_no_time')).then(function (record) {
        var recordObj = $.parseJSON(record.trim());
        if (parseInt(recordObj.additionalHourTypeId) == 1) {
            var ahrType = 'After Shift';
        } else if (parseInt(recordObj.additionalHourTypeId) == 2) {
            var ahrType = 'Pre Shift';
        } else {
            var ahrType = 'WORD';
        }
        var nameExt = "";
        // if ((recordObj.nextension !== "none") && (recordObj.nextension !== "")) {
        //     nameExt = recordObj.nextension;
        // }
        if (recordObj.mname == '') {
            var midName = "";
        } else {
            var midName = recordObj.mname.charAt(0) + "."
        }
        var empPic = imagePath + "" + recordObj.pic;
        $('#empPicApproval').attr('src', empPic)
        $('#empNameRecord').text(recordObj.fname + " " + midName + " " + recordObj.lname + " ");
        $('#empAccountRecord').text(recordObj.accountDescription);
        $('#empJobRecord').text(recordObj.positionDescription);
        $('#ahrNoRecord').text(recordObj.additionalHourRequestId.padLeft(8));
        $('#dateFiledRecord').text(moment(recordObj.dateTimeFiled, 'Y-MM-DD HH:mm:ss').format('MMM D, Y h:mm A'));
        $('#filingTypeRecord').text(recordObj.requestType);
        $('#ahrTypeRecord').text(ahrType);
        $('#dateRenderedRecord').text(moment(recordObj.sched_date, 'Y-MM-DD').format('MMM D, Y'));
        // $('#shiftRecord').text(recordObj.time_start + " - " + recordObj.time_end);
        $('#shiftTypeRecord').text(recordObj.schedType);
        $('#approvalStatRecord').text(recordObj.statusDescription);
        $('#reasonRecord').text(recordObj.reason);
        $('#startOtRecord').text(moment(recordObj.otStartDate, 'Y-MM-DD').format('MMM D, Y') + " " + moment(recordObj.otStartTime, 'HH:mm').format('h:mm A'));
        $('#endOtRecord').text(moment(recordObj.otEndDate + " " + recordObj.otEndTime, 'Y-MM-DD HH:mm').format('MMM D, Y h:mm A'));
        humanTimeFormat(recordObj.totalOtHours, function (humanTimeFormat) {
            $('#renderedOtRecord').text(humanTimeFormat);
        });
        getOtDetailsRecord(recordObj);
    });
}

function initRecordInfo(ahrId, ahrTypeId, requestType) {
    console.log("AHR TYPE ID : " +ahrTypeId);
    if ((requestType == 'unrendered') && (parseInt(ahrTypeId) == 3)) {
        displayRecordInfoWithoutTime(ahrId);
    } else {
        displayRecordInfoWithTime(ahrId);
    }
}


function approverInfo(ahr_id) {
    $.when(fetchPostData({
        ahrId: ahr_id,
    }, '/additional_hour/get_approval_info')).then(function (approval) {
        var approvalObj = $.parseJSON(approval.trim());
        var approvalHTM = "";
        $.each(approvalObj, function (index, value) {
            var icon = "";
            var color = "";
            var animate = "";
            var animateI = "";
            var current = "";
            var stat = "";
            var pic = imagePath + "" + value.pic;
            var dated = moment(value.dateTimeStatus, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y h:mm A');
            var elementId = "#requestApproval" + value.additionalHourRequestApproval_ID + "";
            if (parseInt(value.approvalStatus_ID) == 5) {
                icon = "fa fa-thumbs-o-up";
                color = "btn-success";
                stat = value.description;
                dated = "Approved last " + dated;
            } else if (parseInt(value.approvalStatus_ID) == 6) {
                icon = "fa fa-thumbs-o-down";
                color = "btn-danger";
                stat = value.description;
                dated = "Disapproved last " + dated;
            } else if ((parseInt(value.approvalStatus_ID) == 2) || (parseInt(value.approvalStatus_ID) == 4)) {
                icon = "fa fa-spinner";
                color = "btn-info";
                animate = "fa-spin";
                stat = "pending";
                dated = "Pending since " + dated;
                if (parseInt(value.approvalStatus_ID) == 4){
                    current = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink ml-2" style="min-height: 7px;min-width: 7px;"></span>';
                }
            } else if (parseInt(value.approvalStatus_ID) == 12){
                icon = "fa fa-exclamation-circle";
                color = "btn-warning";
                animateI = "m-animate-blink";
                stat = value.description;
                dated = "Deadline was on "+ moment(value.deadline, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y h:mm A');
            }
            else if (parseInt(value.approvalStatus_ID) == 7) {
                icon = "fa fa-remove";
                color = "btn-primary";
                stat = value.description;
                dated = "Cancelled last " + dated;
            }

            if(value.remarks == null){
                var note = "No Notes found.";
            }else{
                var note =  value.remarks;                
            }

            if(value.mname == ''){
                var midName = "";
            }else{
                var midName = value.mname.charAt(0)+"."
            }
            approvalHTM += '<div class="m-portlet bg-secondary">' +
                '<div class="col-md-12 pt-2 pb-2" style="background: #505a6b30;">' +
                '<div class="row">' +
                '<div class="col-7 col-sm-7 col-md-7 pt-2">APPROVER <span>' + value.approvalLevel + '</span>'+current+'</div>' +
                '<div class="col-5 col-sm-5 col-md-5">' +
                '<div class="btn pull-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;background: #505a6b;">' +
                '<div class="btn '+color+' m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air '+animate+'" style="width: 25px !important;height: 25px !important;">' +
                '<i class="' + icon + ' '+animateI+'" style="font-size: 17 px;"></i>' +
                '</div>' +
                '<span class="button-content text-light text-capitalize pl-2">' + stat + '</span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>  ' +
                '<div class="m-portlet__body pt-3 pl-4 pb-3 pr-4">' +
                '<div class="row">' +
                '<div class="col-2 col-sm-2 col-md-2 d-none d-sm-block">' +
                '<img src="' + pic + '" onerror="noImageFound(this)" width="58" alt="" class="mx-auto rounded-circle" id="' + elementId + '">' +
                '</div>' +
                '<div class="col-12 col-sm-10 col-md-10 pl-1">' +
                '<div class="col-md-12 font-weight-bold" style="font-size: 15px;">'+value.fname+' '+midName+' '+value.lname+'</div>' +
                '<div class="col-md-12" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: darkcyan;">'+value.positionDescription+'</div>' +
                '<div class="col-md-12" style="font-size: 11px;margin-top: -3px;font-weight: bolder;">' + dated + '</div>' +
                '<div class="col-md-12 mt-3" style="font-size: 12px;"><i> - '+note+'</i>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-12 pt-3">' +
                // '<span class="pull-right" style="font-size: 12px;">'+moment(value.dateTimeStatus, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y h:mm A')+'</span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
        });
        $('#approvalBody').html(approvalHTM);
        $('#approversInfoModal').modal('show');
    });

}
// function 
function dateRenderedCheck(){
    $('#infoNotifyRenderedAhr').hide();
    resetRenderedAhrForm();
    check_if_tito_exist(function(exist){
        if (parseInt(exist.exist)) {
            $('#actualDtrDate, #shift, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
            $('#errNotifyRenderedAhrMssg').html('<b>Cannot File AHR!</b> An ongoing TITO request is filed for this schedule');
            $('#errNotifyRenderedAhr').show();
            $('#requestAhrBtn').prop('disabled', true);
        }   else {
            $('#errNotifyRenderedAhr').hide();
            $('#requestAhrBtn').prop('disabled', false);
            $('#actualDtrDate, #shift, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
            getShift($('.dateRendered').val(), function (exist) {
                if (exist) {
                    if ($('#shift').val() == 0 || $('#shift').val() == null) {
                        resetRenderedAhrForm();
                    } else {
                        checkIfAlreadyFiled($('#ahrTypeRendered').val(), $('#shift').val(), function (checkIfFiledBool) {
                            getAhrDetails(parseInt(checkIfFiledBool.exist));
                            identifyShiftType();
                        })
                    }
                    $('#errNotifyRenderedAhr').hide();
                    $('#requestAhrBtn').prop('disabled', false);
                    $('#actualDtrDate, #shift, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', false);
                } else {
                    $('#actualDtrDate, #shift, #actualDtrTime, #reason, #requestAhrBtn').prop('disabled', true);
                    $('#errNotifyRenderedAhrMssg').html('<b>No Schedule found!</b> Please inform your Supervisor');
                    $('#errNotifyRenderedAhr').show();
                    $('#requestAhrBtn').prop('disabled', true);
                }
            });
        }
    });
}

function fileRenderedAhr() {
    var datedShift = $('#shift').data('datedshift');
    // console.log(datedShift);
    var shiftStartDate = moment(datedShift.start, "YYYY-MM-DD kk:mm").format('YYYY-MM-DD');
    var shiftStartTime = moment(datedShift.start, "YYYY-MM-DD kk:mm").format('kk:mm');
    var shiftEndDate = moment(datedShift.end, "YYYY-MM-DD kk:mm").format('YYYY-MM-DD');
    var shiftEndTime = moment(datedShift.end, "YYYY-MM-DD kk:mm").format('kk:mm');
    var inputDate = moment($('#actualDtrDate').val(), 'MM/DD/Y').format('YYYY-MM-DD');

    var inputTime = moment($('#actualDtrTime').val(), 'h:mm A').format('kk:mm');;
    if ($('#ahrTypeRendered').val() == 1) {
        var startDate = shiftEndDate;
        var startTime = shiftEndTime;
        var endDate = inputDate;
        var endTime = inputTime;

    } else if ($('#ahrTypeRendered').val() == 2) {
        var endDate = shiftStartDate;
        var endTime = shiftStartTime;
        var startDate = inputDate;
        var startTime = inputTime;
    } else {
        var dtrInDate = moment($('#wordDtrIn').data('datetocompute'), 'Y-MM-DD kk:mm:ss').format('YYYY-MM-DD');
        var dtrInTime = moment($('#wordDtrIn').data('datetocompute'), 'Y-MM-DD kk:mm:ss').format('kk:mm');
        var dtrOutDate = moment($('#wordDtrOut').data('datetocompute'), 'Y-MM-DD kk:mm:ss').format('YYYY-MM-DD');
        var dtrOutTime = moment($('#wordDtrOut').data('datetocompute'), 'Y-MM-DD kk:mm:ss').format('kk:mm');
        if (parseInt($('#shift').data('late'))) { // word start
            var startDate = dtrInDate;
            var startTime = dtrInTime;
        } else {
            var startDate = shiftStartDate;
            var startTime = shiftStartTime;
        }
        if (parseInt($('#shift').data('ut'))) { // word end
            var endDate = dtrOutDate;
            var endTime = dtrOutTime;
        } else {
            var endDate = shiftEndDate;
            var endTime = shiftEndTime;
        }
    }

    // console.log("START: " + startDate +" "+ startTime);
    // console.log("END: " + endDate + " " + endTime);
    // if (startTime == '00:00') {
    //     var startDate = moment(moment(startDate, "YYYY-MM-DD")).format('YYYY-MM-DD');
    // } else {
    //     var startDate = startDate;
    // }
    // if (endTime == '00:00') {
    //     var endDate = moment(moment(endDate, "YYYY-MM-DD")).format('YYYY-MM-DD');
    // } else {
    //     var endDate = endDate;
    // }
    requestAhr($('#ahrTypeRendered').val(), $('#shift').val(), startDate, startTime, endDate, endTime, $('#renderedOt').data('othours'), $('#reason').val(), 1, 0, function (insertStat) {
        var addStat = jQuery.parseJSON(insertStat.trim());
        if (parseInt(addStat.request_stat) != 0) {
            // var notif_id[0] = parseInt(addStat.set_notif_details.notif_id);
            notification(addStat.set_notif_details.notif_id);
            swal("Successfully Filed Request!", "Your request is now directed to your Immediate Supervisor for approval.", "success");
        } else {
            swal("File Request Error !", "Something went wrong while filing your request. Please report the issue immediately to your supervisor or to the system administrator", "error");
        }
        $('#personalAhrPendingDatatable').mDatatable('reload');
        $('#renderedAhrFilingModal').modal('hide');
    });
}

function verifyIfFinal(){
     swal({
         title: 'Is this AHR request Final?',
         text: "You will not be able to edit this request. Please review first before",
         type: 'warning',
         showCancelButton: true,
         confirmButtonText: 'Yes, This is final!',
         cancelButtonText: 'Not Yet, I will review',
         reverseButtons: true
     }).then(function (result) {
         if (result.value) {
            fileRenderedAhr();
         } else if (result.dismiss === 'cancel') {
             $('#renderedAhrFilingForm').formValidation('disableSubmitButtons', false);
         }
     });
}


function identifyValidation(){
    formReset('#renderedAhrFilingForm');
    $('#renderedAhrFilingForm').formValidation('destroy');
    if ($('#ahrTypeRendered').val() != 3) {
         $('#renderedAhrFilingForm')
             .find('[name="dateRendered"]')
             .change(function (e) {
                 $('#renderedAhrFilingForm').formValidation('revalidateField', 'dateRendered');
                 dateRenderedCheck();
             })
             .end()
             .formValidation({
                 message: 'This value is not valid',
                 excluded: ':disabled',
                 feedbackIcons: {
                     valid: 'fa fa-check',
                     invalid: 'fa fa-remove',
                     validating: 'fa fa-refresh'
                 },
                 fields: {
                     shift: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Shift is required'
                             }
                         }
                     },
                     dateRendered: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Date Rendered is required'
                             }
                         }
                     },
                     actualDtrDate: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Actual DTR Date is required'
                             }
                         }
                     },
                     actualDtrTime: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Actual DTR Time is required'
                             }
                         }
                     },
                     reason: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Reason is required'
                             },
                            stringLength: {
                                max: 100,
                                message: 'Reason must not exceed beyond 100 characters'
                            }
                         }
                     },
                 }
             })
    }else{
         $('#renderedAhrFilingForm')
             .find('[name="dateRendered"]')
             .change(function (e) {
                 $('#renderedAhrFilingForm').formValidation('revalidateField', 'dateRendered')
                 dateRenderedCheck();
             })
             .end()
             .formValidation({
                 message: 'This value is not valid',
                 live: 'enabled',
                 excluded: ':disabled',
                 feedbackIcons: {
                     valid: 'fa fa-check',
                     invalid: 'fa fa-remove',
                     validating: 'fa fa-refresh'
                 },
                 fields: {
                     shift: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Shift is required'
                             }
                         }
                     },
                     dateRendered: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Date Rendered is required'
                             }
                         }
                     },
                     reason: {
                         validators: {
                             notEmpty: {
                                 message: 'Oops! Reason is required'
                             },
                             stringLength: {
                                 max: 100,
                                 message: 'Reason must not exceed beyond 100 characters'
                             }
                         }
                     },
                 }
             })
    }
}

function validateRenderedForm(){
    $('#renderedAhrFilingForm').formValidation('destroy');
    if ($('#ahrTypeRendered').val() != 3) {
        $('#renderedAhrFilingForm')
        .find('[name="dateRendered"]')
        .change(function (e) {
            $('#renderedAhrFilingForm').formValidation('revalidateField', 'dateRendered');
            dateRenderedCheck();
        })
        .end()
        .formValidation({
            message: 'This value is not valid',
            live: 'enabled',
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'fa fa-refresh'
            },
            fields: {
                shift: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! Shift is required'
                        }
                    }
                },
                dateRendered: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! Date Rendered is required'
                        }
                    }
                },
                actualDtrDate: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! Actual DTR Date is required'
                        }
                    }
                },
                actualDtrTime: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! Actual DTR Time is required'
                        }
                    }
                },
                reason: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! Reason is required'
                        }
                    },
                    stringLength: {
                        max: 100,
                        message: 'Reason must not exceed beyond 100 characters'
                    }
                },
            }
        }).formValidation('validate').on('success.form.fv', function (e, data) {
            e.preventDefault();
            verifyIfFinal();
        });
    }else{
        $('#renderedAhrFilingForm')
            .find('[name="dateRendered"]')
            .change(function (e) {
                $('#renderedAhrFilingForm').formValidation('revalidateField', 'dateRendered');
                dateRenderedCheck();
            })
            .end()
            .formValidation({
                message: 'This value is not valid',
                live: 'enabled',
                excluded: ':disabled',
                feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-remove',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    shift: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! Shift is required'
                            }
                        }
                    },
                    dateRendered: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! Date Rendered is required'
                            }
                        }
                    },
                    reason: {
                        validators: {
                            notEmpty: {
                                message: 'Oops! Reason is required'
                            },
                            stringLength: {
                                max: 100,
                                message: 'Reason must not exceed beyond 100 characters'
                            }
                        }
                    },
                }
            }).formValidation('validate').on('success.form.fv', function (e, data) {
                e.preventDefault();
                verifyIfFinal();
            });
    }
}

function get_days_available(callback){
    $.when(fetchGetData('/additional_hour/get_days_available_to_file')).then(function (daysAvailable) {
        var daysAvailableObj = jQuery.parseJSON(daysAvailable.trim());
        callback(daysAvailableObj);
    });
}



// EVENT TRIGGERS --------------------------------------------------------------------------------------

// $('.dateRendered').on('change', function () {
//     validateDateRendered();
//     dateRenderedCheck()
// });

$('#ahrTypeRendered').on('change', function () {
    identifyValidation();
    $('#errNotifyRenderedAhr').hide();
    if ($(this).val() == 1) {
        $('#guide2').text("#2 Specify Actual Time Out")
        $('#dtrLogLabel').text('Clock Out');
        $('#inputTimeLabel').text('ACTUAL DTR OUT');
        $('#preAfterDetails').show();
        $('#wordDetails').hide();
        $('#bct').hide();
    } else if ($(this).val() == 2) {
        $('#guide2').text("#2 Specify Actual Time In")
        $('#dtrLogLabel').text('Clock In');
        $('#inputTimeLabel').text('ACTUAL DTR IN');
        $('#bct').show();
        $('#preAfterDetails').show();
        $('#wordDetails').hide();
    } else {
        $('#wordDetails').show();
        $('#preAfterDetails').hide();
    }
    if (!$('.dateRendered').val()) {
    } else {
        dateRenderedCheck();
    }
    if (($('#shift').val() == 0) || ($('#shift').val() == null) || ($('#shift').val() == "")) {
        resetRenderedAhrForm()
    } else {
        identifyShiftType();
        getAhrDetails();
    }
});

$('#actualDtrTime,#actualDtrDate').on('change', function () {
    if (($('#actualDtrDate').val() != '') && ($('#actualDtrTime').val() != '')) {
        // dateAdjust(moment($('#actualDtrDate').val() + " " + $('#actualDtrTime').val(), 'MM/DD/Y hh:mm A').format('Y-MM-DD HH:mm'), function (dateAdj) {
            checkIfTimeInRange($('#actualDtrDate').val(), function (checkBool) {
                // $('#actualDtrDate').val(moment(dateAdj.show, 'YYYY-MM-DD').format('MM/DD/Y'));
        //         $('#actualDtrDate').data('datetocompute', moment(dateAdj.compute, 'YYYY-MM-DD').format('MM/DD/Y'));
                if (checkBool) {
                    // $('#errNotifyRenderedAhrMssg').html('Sorry, You have exceeded your actual dtr log.');
                    $('#errNotifyRenderedAhr').show();
                    $('#reason, #requestAhrBtn').prop('disabled', true);
                    $('#renderedOt').text('00 hr, 00 min');
                } else {
                    $('#errNotifyRenderedAhr').hide();
                    $('#reason, #requestAhrBtn').prop('disabled', false);
                    computeAhrTime(function (renderedOt) {
                        $('#renderedOt').text(renderedOt.formatted);
                         $('#renderedOt').data('othours', renderedOt.otHours);
                        // setDate(moment(dateAdj.show, 'Y-MM-DD HH:mm').format('MM/DD/Y'));
                        if (renderedOt.otHours == 0) {
                            $('#errNotifyRenderedAhrMssg').html('<b>Insufficient Coverage!</b> You don\'t have enough time to file an Additional Hour Request.');
                            $('#errNotifyRenderedAhr').show();
                            $('#requestAhrBtn').prop('disabled', true);
                        } else {
                            $('#errNotifyRenderedAhr').hide();
                            $('#requestAhrBtn').prop('disabled', false);
                        }
                    });
                }
            });
        // })
    }
})

$('#shift').on('change', function () {
    var ahrTypeVal = $('#ahrTypeRendered').val();
    var schedIdVal = $('#shift').val();
     $('#renderedAhrFilingForm').formValidation('validateField', 'shift');
    checkIfAlreadyFiled(ahrTypeVal, schedIdVal, function (checkIfFiledBool) {
        getAhrDetails(parseInt(checkIfFiledBool.exist));
        identifyShiftType();
    })
});

$('#renderedAhrFilingModal').on('hidden.bs.modal', function () {
    formReset('#renderedAhrFilingForm');
    resetRenderedAhrForm();
    initiallyHide()
    $('#reason').val('');
    $("#ahrTypeRendered option").filter(function () {
        return $(this).val() == 1;
    }).prop('selected', true);
    $('.dateRendered').val('');
    $('select[name="shift"]').html('');
    $('#guide2').text("#2 Specify Actual Time Out")
    $('#dtrLogLabel').text('Clock Out');
    $('#inputTimeLabel').text('ACTUAL DTR OUT');
    $('#preAfterDetails').show();
});

// $('#reason').bind('input propertychange', function () {
//     if ($.isNumeric($("#reason").val())) {
//         $('.character-remaining').text("Please use proper words not just numbers in indicating your reason of filing and Additional Hour Request");
//         $('#requestAhrBtn').prop('disabled', true);
//     } else {
//         $('#requestAhrBtn').prop('disabled', false);
//     }
// })

$(function () {
    // personalAhrDatatable.init1();
    // personalAhrRecordDatatable.init2();

    getAhrType();
    initiallyHide();
    $('.unrenderedTimePicker').timepicker({
        minuteStep: 1,
        showSeconds: false,
        showMeridian: true
    });
    // $('.datePicker').datepicker({
    getCurrentDateTime(function (date) {
        // get_days_available(function(daysAvail) {
            $('.dateRendered').datepicker({
                todayBtn: "linked",
                useCurrent: false,
                todayHighlight: true,
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                },
                // startDate: moment(date, 'Y-MM-DD').subtract(parseInt(daysAvail.daysAvailable), "days").format('MM/DD/YYYY'),
                // endDate: moment(date, 'Y-MM-DD').format('MM/DD/YYYY')
            });
        // })        
        $('.dateRenderedManual').datepicker({
            todayBtn: "linked",
            useCurrent: false,
            todayHighlight: true,
            autoclose: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            },
            startDate: moment(date, 'Y-MM-DD').format('MM/DD/YYYY'),
            endDate: moment(date, 'Y-MM-DD').add(29, 'days').format('MM/DD/YYYY')
        });
    })
    
    $('#requestAhrBtn').on('click', function () {
        validateRenderedForm();
    })
    
    $(".text-counter").keyup(function () {
        var a = 100,
            b = $(this).val().length;
        if ((b < a) && (b != 0)){
            var c = a - b;
            $(".character-remaining").text(c + " characters left");
            $(".character-remaining").show();
        } else if (b == a) {
            $(".character-remaining").text("You have reached the Maximum character of 100");
            $(".character-remaining").show();
        }else{
             $(".character-remaining").hide();
        }
    });


    //layout change
    $(window).bind("resize", function () {
       if ($(this).width() < 975) {
            $('.borderLeft').removeClass('stack-border-left-only');
        } else {
            $('.borderLeft').addClass('stack-border-left-only');
        }
    }).resize();
   
    // $('.popover').popover({
    //     placement:'auto'
    // })
});