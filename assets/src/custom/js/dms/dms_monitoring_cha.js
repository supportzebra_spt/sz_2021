// ON EVENT 

$('#monitoring_records_button').on('click', function () {
    var irrecord = "1";
    initpending_all(irrecord);
    fetch_selectforIRrecords();
});
$('#eroPendingIR_tabbutton').on('click', function () {
    //  initpendinguploads();
    initpendingrecommendation();
    fetch_selectforPending();
    // initpending_all(irrecord);
    // fetch_selectforIRrecords();
});
$('#pending_uploadtab').on('click', function () {
    initpendinguploads();
    fetch_selectforPending();
});
$('#eroMissedDeadline_tab').on('click', function () {
    initmissedeadline();
    fetch_selectforPending();
});
$('#pendingRec_Tab').on('click', function () {
    initpendingrecommendation();
    fetch_selectforPending();
})
$('#pendingir_Tab').on('click', function () {
    var irrecord = "1";
    $("#hidden_base_url").data("tab","1");
    fetch_selectforIRrecords();
    initpending_all(irrecord);
});

//  IR records tab
$('#allrecordsir_Tab').on('click', function () {
    initRecordDateRangePicker_irrecords(function (dateRanges) {
        $("#hidden_base_url").data("tab","2");
        initir_forall(dateRanges.start, dateRanges.end);
        fetch_selectforIRrecords();
    });
});

$('#susTermination_Tab').on('click', function () {
    initsuspension_termination();
    // fetch_selectforsustermIRrecords();
    fetch_selectforIRrecords();
});

$('#irpending_subjectname, #irpending_account, #irpending_natureofoffense, #irpending_offense, #employeepending_type, #reportpending_type').on('change', function () {
    var irrecord = "1";
    initpending_all(irrecord);
});
$('#irall_subject, #employeeall_type, #liabilityall_type, #reportall_type, #accall_select, #irall_subject, #offenseall_filter, #natureall_offense').on('change', function () {
    var dateRange = $('#recordsall-date').val().split('-');
    initir_forall(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
});

$('#pendingir_searchid').on('change', function () {
    var irrecord = "1";
    initpending_all(irrecord);
})
$('#reportsall_irid').on('change', function () {
    var dateRange = $('#recordsall-date').val().split('-');
    initir_forall(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
})

$('#evidenceall-types').on('change', function () {
    fetch_display_ofevidences($("#modal_displayallirevidences").data('ir'));
})

$('#irsusterm_subjectname, #irsusterm_account, #irsusterm_discAction').on('change', function () {
    initsuspension_termination();
});
$('#pendingsubject_select, #pendingEroUp_select, #eropendingUp_irid, #requiredero_file').on('change', function () {
    initpendinguploads();
});
$('#irsusterm_searchid').on('change', function () {
    initsuspension_termination();
})
$('#pendingRecsubject_select, #pendingEroRec_select, #recoType_file, #eropendingRec_irid').on('change', function () {
    initpendingrecommendation();
})
$('#pendingmissedDeadline_subjectname, #pendingmissedDeadline_eroname, #pendingmissedDeadline_task, #pendingmissedDeadline_searchid').on('change', function () {
    initmissedeadline();
})
$("#btn-downloadallirrecords").click(function () {
    var st_date = $('#recordsall-date').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var en_date = $('#recordsall-date').data('daterangepicker').endDate.format('YYYY-MM-DD');
    $.ajax({
        url: baseUrl + "/Discipline/downloadExcel_allir",
        method: 'POST',
        data: {
            violation_class: $('#employeeall_type').val(),
            violation_type: $('#liabilityall_type').val(),
            report_type: $('#reportall_type').val(),
            violation_subject: $('#irall_subject').val(),
            offense: $('#offenseall_filter').val(),
            offense_nature: $('#natureall_offense').val(),
            violation_ir_id: $('#reportsall_irid').val(),
            violation_accounts: $('#accall_select').val(),
            incidentDate_start: $('#recordsall-date').data('daterangepicker').startDate.format('YYYY-MM-DD'),
            incidentDate_end: $('#recordsall-date').data('daterangepicker').endDate.format('YYYY-MM-DD'),
        },
        beforeSend: function () {
            mApp.block('body', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg',
                message: 'Downloading...'
            });
        },
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'SZ_DMS_IR_' + st_date + '_to_' + en_date + '.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
            mApp.unblock('body');
        },
        error: function (data) {
            $.notify({
                message: 'Error in exporting excel'
            }, {
                type: 'danger',
                timer: 1000
            });
            mApp.unblock('body');
        }
    });
})

$('#mediairall_display').on('mouseover', '.attachedFile', function () {
    let filename = $(this).data('filename');
    // $(this).find('.xbutton').show();

    $(this).tooltip({
        title: filename,
        placement: "top"
    });
    $(this).tooltip('show')
})


$('#mediairall_display').on('mouseleave', '.attachedFile', function () {
    // $(this).find('.xbutton').hide();
    $(this).tooltip('hide')
})

$('#mediairall_display').on('click', '.a_audio_link3', function () {
    link = $(this).data('audiolink');
    audio_stringxx = "";
    audio_stringxx += "<audio controls style='width: -webkit-fill-available;'>";
    audio_stringxx += "<source src='" + baseUrl + "/" + link + "' type='audio/mp3'>";
    audio_stringxx += "<source src='" + baseUrl + "/" + link + "' type='audio/mpeg'>";
    audio_stringxx += "<source src='" + baseUrl + "/" + link + "' type='audio/mpeg'>";
    audio_stringxx += "</audio>";
    $("#audio_evidence_player3").html(audio_stringxx);
})

$('#ir_col_all, #ir_col_pending, #ir_col_susterm, #ir_col_penRec, #ir_col_penUps, #ir_col_missedDead').on('click', '.view_IRdet', function () {
    var irid = $(this).data('irid');
    allirmoredetails(irid);
})
$('#ir_col_all').on('click', '.view_attachmentsIR', function () {
    var irid = $(this).data('irid');
    fetch_display_ofevidences(irid);
})

$('#ir_col_susterm').on('click', '.calendarthisbutton', function () {
    var irid = $(this).data('irid');
    fetchsus_term_dates(irid);
})

// SELECT 

$(function () {
    $('#irpending_subjectname, #pendingsubject_select, #irall_subject, #irsusterm_subjectname, #pendingRecsubject_select, #pendingmissedDeadline_subjectname').select2({
        placeholder: "Select subject",
        width: '100%'
    });
    $('#irpending_account, #accall_select, #irsusterm_account').select2({
        placeholder: "Select account",
        width: '100%'
    });
    $('#offenseall_filter').select2({
        placeholder: "Select offense",
        width: '100%'
    });
    $('#natureall_offense').select2({
        placeholder: "Select offense type",
        width: '100%'
    });
    // ERO PENDING 
    $('#pendingEroUp_select, #pendingEroRec_select, #pendingmissedDeadline_eroname').select2({
        placeholder: "Select employee relations officer",
        width: '100%'
    });
    $('#pendingmissedDeadline_task').select2({
        placeholder: "Select task",
        width: '100%'
    });
    $('#irpending_natureofoffense').select2({
        placeholder: "Select Nature of Offense",
        width: '100%'
    });
    $('#irpending_offense').select2({
        placeholder: "Select Offense",
        width: '100%'
    });
    // $('#irsusterm_discAction').select2({
    //     placeholder: "Select disciplinary action",
    //     width: '100%'
    // });


    // Calendar 

    $('#susTermDatepicker_all').datepicker({
        format: 'yyyy-mm-dd'
        // startDate: '+2d'
    });
    $('#calendar_displaydate').on("show.bs.modal", function (e) {
        $("#susTermDatepicker_all .datepicker-days").on('click', 'td.day', function () {

            // setTimeout(function() {
            //     submitEffectiveTermDate();
            // }, 200);
        });

    });

});


// Auto load

// function call 
function initpending_all(ird) {
    if (ird == "1") {
        $('#irrecords_pendingDatatable').mDatatable('destroy');
    } else {
        $('#irrecords_allDatatable').mDatatable('destroy');
    }
    recordsIRDatatable.init(ird);
}
function initpendinguploads() {
    $('#pendinguploads_Datatable').mDatatable('destroy');
    recordsIRDatatable_pendinguploads.init();
}
function initpendingrecommendation() {
    $('#erorecommendation_pendingDatatable').mDatatable('destroy');
    recordsIRDatatable_pendingrecommendation.init();
}
function initir_forall(startDate, endDate) {
    $('#irrecords_allDatatable').mDatatable('destroy');
    recordsIRDatatable_allir.init(startDate, endDate);
}
function initsuspension_termination() {
    $('#irsusterm_Datatable').mDatatable('destroy');
    recordsIRDatatable_suspension.init();
}
function initmissedeadline() {
    $('#missedDeadline_Datatable').mDatatable('destroy');
    recordsIRDatatable_missedDeadline.init();
}
function fetch_display_ofevidences(irid) {
    var et = $("#evidenceall-types").val();
    var stringx = "";
    $.ajax({
        url: baseUrl + "/discipline/fetch_report_evidences/" + irid,
        type: "POST",
        data: {
            evidencetype: et
        },
        cache: false,
        success: function (data) {
            var result = JSON.parse(data);
            var stringxx = "";
            var total_evidences = result.display.length;
            var label_a = "";
            if (total_evidences == 0 || total_evidences == 1) {
                label_a = "Attachment";
            } else {
                label_a = "Attachments";
            }
            $("#label_attach").text(total_evidences + " " + label_a);
            var file_name = "";
            stringxx = "<small id='irinfo' data-idir='" + irid + "' style='display:none'></small>";
            $("#passed_irid").html(stringxx);
            if (result.display.length != 0) {
                $.each(result.display, function (key, data) {
                    if (data.link.includes("uploads/dms/nor_nte/")) {
                        file_name = data.link.replace("uploads/dms/nor_nte/", " ");
                    } else if (data.link.includes("uploads/dms/explain/")) {
                        file_name = data.link.replace("uploads/dms/explain/", " ");
                    } else if (data.link.includes("uploads/dms/client/")) {
                        file_name = data.link.replace("uploads/dms/client/", " ");
                    } else {
                        file_name = data.link.replace("uploads/dms/evidences/", " ");
                    }
                    if (data.mediaType == "image/jpeg" || data.mediaType == "image/png" || data
                        .mediaType == "image/jpg") {
                        stringx += "<div class='col-lg-4 col-md-12 mb-4 attachedFile' data-filename='" + file_name + "'>";
                        stringx += "<a href='" + baseUrl + "/" + data.link +
                            "' data-lightbox='roadtrip'>";
                        stringx += "<img src='" + baseUrl + "/" + data.link +
                            "' class='h_wdisplay z-depth-1'>";
                        stringx += "</a>";
                        stringx += "<div class='col-lg-12 col-md-6 mb-4 text-truncate'>";
                        stringx += "<span class='evidence_file_name'>" + file_name + "</span>";
                        stringx += "</div>";
                        if (data.incidentReportAttachmentPurpose_ID == "4") {
                            stringx += "<button type='link'   onclick='remove_attachment(" + data.ir_attachment_ID + "," + irid + ")' style='display:none' class='xbutton'>x</button>";
                        }
                        stringx += "</div>";
                    } else if (data.mediaType == "video/mp4" || data.mediaType == "video/mov" || data
                        .mediaType == "video/3gp" || data.mediaType == "video/mpeg" || data.mediaType ==
                        "video/avi" || data.mediaType == "video/mpg" || data.mediaType == "video/m4v") {
                        stringx += "<div class='col-lg-4 col-md-6 mb-4 attachedFile' style='position: relative;' data-filename='" + file_name + "'>";
                        stringx += "<div id='vidBox'>";
                        stringx += "<div id='videCont'>";
                        stringx += "<video width='120' height='86' id='example' controls>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/mp4'>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/mov'>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/avi'>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/mpg'>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/m4v'>";
                        stringx += "</video>";
                        stringx += "<div class='col-lg-12 col-md-6 mb-4 text-truncate'>";
                        stringx += "<span class='evidence_file_name'>" + file_name + "</span>";
                        stringx += "</div>";
                        stringx += "</div>";
                        stringx += "</div>";
                        if (data.incidentReportAttachmentPurpose_ID == "4") {
                            stringx += "<button type='link' onclick='remove_attachment(" + data.ir_attachment_ID + "," + irid + ")' style='display:none' class='xbutton'>x</button>";
                        }
                        stringx += "</div>";
                    } else if (data.mediaType == "audio/mp3" || data.mediaType == "audio/3gp" || data
                        .mediaType == "audio/wav" || data.mediaType == "audio/mpeg") {
                        stringx += "<div class='col-lg-4 col-md-6 mb-4 text-truncate attachedFile' style='position: relative;' data-filename='" + file_name + "' >";
                        // stringx += "<div>";
                        stringx += "<a href='#' class='a_audio_link3' data-toggle='modal' data-target='#audio_modal_evidences3' id='attached_button' data-audiolink='" +
                            data.link + "'>";
                        stringx += "<i class='fa fa-music icon_size2'></i>";
                        stringx += "<br><span>" + file_name + "</span>";
                        stringx += "</a>";
                        if (data.incidentReportAttachmentPurpose_ID == "4") {
                            stringx += "<button type='link' onclick='remove_attachment(" + data.ir_attachment_ID + "," + irid + ")' style='display:none' class='xbutton'>x</button>";
                        }
                        stringx += "</div>";
                    } else {
                        stringx += "<div class='col-lg-4 col-md-12 mb-4 text-truncate attachedFile' style='position: relative;' data-filename='" + file_name + "'>";
                        stringx += "<a target='_blank' href='" + baseUrl + "/" + data.link +
                            "'>";
                        if (data.mediaType == "application/vnd.open") {
                            stringx += "<i class='fa fa-file-word-o icon_size'></i>";
                        } else if (data.mediaType == "application/pdf") {
                            stringx += "<i class='fa fa-file-pdf-o icon_size'></i>";
                        } else if (data.mediaType == "application/vnd.ms-e") {
                            stringx += "<i class='fa fa-file-excel-o icon_size'></i>";
                        } else {
                            stringx += "<i class='fa fa-file icon_size'></i>";
                        }
                        stringx += "<br><span>" + file_name + "</span>";
                        stringx += "</a>";
                        if (data.incidentReportAttachmentPurpose_ID == "4") {
                            stringx += "<button type='link' onclick='remove_attachment(" + data.ir_attachment_ID + "," + irid + ")' style='display:none' class='xbutton'>x</button>";
                        }
                        stringx += "</div>";
                    }
                });
                console.log("exist");
            } else {
                stringx += "<div class='col-lg-12 col-md-12 mb-4 text-center mt-5'>";
                stringx += ""
                stringx += "<h4><i class='fa fa-paperclip' style='color:crimson;font-size:unset'></i> No attachments</h4>";
                stringx += "</div>";
                console.log("none");
            }
            $("#mediairall_display").html(stringx);
            $("#modal_displayallirevidences").data('ir', irid);
            $("#modal_displayallirevidences").modal('show');
            // $('#evidence_display').find('.attachedFile').tooltip();
            // console.log($('#evidence_display').find('.attachedFile'))
        }
    });
}


// All and Pending IR records

var recordsIRDatatable = function () {
    var irrecord = function (ird) {
        var func_ir = (ird === "1") ? "/discipline/list_irpending_datatable" : (ird === "2") ? "/discipline/list_irall_datatable" : (ird === "3") ? "" : " ";
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + func_ir,
                        params: {
                            query: {
                                subject_id: $('#irpending_subjectname').val(),
                                account_id: $('#irpending_account').val(),
                                search_irid: $('#pendingir_searchid').val(),
                                off_all: $('#irpending_offense').val(),
                                off_t: $('#irpending_natureofoffense').val(),
                                empall: $('#employeepending_type').val(),
                                reptype: $('#reportpending_type').val(),
                                access: $("#pendingir_Tab").data("type"),
                                // subject: $('#ir_subject').val(),
                                // offense: $('#offense_filter').val(),
                                // offense_nature: $('#nature_offense').val(),
                                // re_irid: $('#reports_irid').val(),
                                // startDate: start,
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "incidentReport_ID",
                title: "IR ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.incidentReport_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "subjectEmp_ID",
                title: "Subject",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.subjectFname + " " + row.subjectLname;
                    return html;
                },
            }, {
                field: "incidentDate",
                title: "Incident Date",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.incidentDate, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            }, {
                field: "finalAction",
                title: "Disciplinary Action",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.finalAction;
                    return html;
                },
            },
            {
                field: "",
                title: "View",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html += '<button type="button" class="btn btn-sm btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air view_IRdet" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-eye"></i></button>';
                    return html;
                },
            },
            ],
        };
        var datatable;
        if (ird === "1") {
            datatable = $('#irrecords_pendingDatatable').mDatatable(options);
        } else if (ird === "2") {
            datatable = $('#irrecords_allDatatable').mDatatable(options);
        }
    };
    return {
        init: function (ird) {
            irrecord(ird);
        }
    };


}();

// All IR records 

var recordsIRDatatable_allir = function () {
    var irrecord = function (start, end) {
        // var func_ir = (ird === "1") ? "/discipline/list_irpending_datatable" : (ird=== "2") ? "/discipline/list_irall_datatable" : (ird === "3") ? "" : " ";  
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/list_irall_datatable",
                        params: {
                            query: {
                                empall: $('#employeeall_type').val(),
                                liaall: $('#liabilityall_type').val(),
                                reptype: $('#reportall_type').val(),
                                acc: $('#accall_select').val(),
                                empsub: $('#irall_subject').val(),
                                off_all: $('#offenseall_filter').val(),
                                off_t: $('#natureall_offense').val(),
                                re_irid: $('#reportsall_irid').val(),
                                access: $('#allrecordsir_Tab').data('type'),
                                startDate: start,
                                endDate: end
                                // subject: $('#ir_subject').val(),
                                // offense: $('#offense_filter').val(),
                                // offense_nature: $('#nature_offense').val(),
                                // re_irid: $('#reports_irid').val(),
                                // startDate: start,
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "incidentReport_ID",
                title: "IR ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.incidentReport_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "subjectEmp_ID",
                title: "Subject",
                width: 130,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.subjectFname + " " + row.subjectLname;
                    return html;
                },
            }, {
                field: "incidentDate",
                title: "Incident Date",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.incidentDate, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            }, {
                field: "finalAction",
                title: "Disciplinary Action",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.finalAction;
                    return html;
                },
            },
            {
                field: "liabilityStat",
                title: "Action",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    var pdf = baseUrl + "/discipline/downloadPdf_ir/" + row.incidentReport_ID;
                    html += '<button type="button" class="btn btn-sm btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air view_IRdet" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-eye"></i></button>';            
                    if(row.liabilityStat!=2){
                       html += '&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air evidencesdis view_attachmentsIR" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-paperclip"></i></button>';
                       html += '&nbsp;&nbsp;<a href="' + pdf + '"  target="_blank" class="btn btn-sm btn-outline-success m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air download_ir" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-download"></i></a>';   
                    } 
                    return html;
                },
            }
            ],
        };
        var datatable;
        datatable = $('#irrecords_allDatatable').mDatatable(options);
    };
    return {
        init: function (start, end) {
            irrecord(start, end);
        }
    };


}();

// Suspension and Termination IR records

var recordsIRDatatable_suspension = function () {
    var irrecord = function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/list_irsusterm_datatable",
                        params: {
                            query: {
                                subjectname: $('#irsusterm_subjectname').val(),
                                ir_account: $('#irsusterm_account').val(),
                                discaction: $('#irsusterm_discAction').val(),
                                ir_recid: $('#irsusterm_searchid').val(),
                                access:  $('#allrecordsir_Tab').data('type')
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "incidentReport_ID",
                title: "IR ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.incidentReport_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "subjectEmp_ID",
                title: "Subject",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.subjectFname + " " + row.subjectLname;
                    return html;
                },
            }, {
                field: "incidentDate",
                title: "Incident Date",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.incidentDate, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            }, {
                field: "finalAction",
                title: "Disciplinary Action",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.finalAction;
                    return html;
                },
            },
            {
                field: "",
                title: "Action",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    var pdf = baseUrl + "/discipline/downloadPdf_ir/" + row.incidentReport_ID;
                    html += '<button type="button" class="btn btn-sm btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air view_IRdet" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-eye"></i></button>';
                    html += '&nbsp;&nbsp;<button type="button" id="cal' + row.incidentReport_ID + '" class="btn btn-sm btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air calendarthisbutton" data-irid="' + row.incidentReport_ID + '" data-label="' + row.label + '" data-day="' + row.days + '"><i class="fa fa-calendar"></i></button>';
                    return html;
                },
            }
            ],
        };
        var datatable = $('#irsusterm_Datatable').mDatatable(options);
    };
    return {
        init: function () {
            irrecord();
        }
    };


}();

// Pending Uploads

var recordsIRDatatable_pendinguploads = function () {
    var irrecord = function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/list_irpendingUploads_datatable",
                        params: {
                            query: {
                                subjectname: $('#pendingsubject_select').val(),
                                eroname: $('#pendingEroUp_select').val(),
                                req_erofile: $('#requiredero_file').val(),
                                ir_recuploadid: $('#eropendingUp_irid').val()
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "incidentReport_ID",
                title: "IR ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.incidentReport_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "subjectEmp_ID",
                title: "Subject",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.subjectFname + " " + row.subjectLname;
                    return html;
                },
            },
            {
                field: "eroEmp_ID",
                title: "Employee Relations Officer",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.eroFname + " " + row.eroLname;
                    return html;
                },
            }, {
                field: "incidentDate",
                title: "Incident Date",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.incidentDate, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            },
            {
                field: "",
                title: "Required File",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    if (row.purpose == "nte") {
                        html = '<span class="m-menu__link-badge"><span class="m-badge m-badge--primary m-badge--wide">NTE</span></span>';
                    } else {
                        html = '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger m-badge--wide">NOR</span></span>';
                    }
                    return html;
                },
            },
            {
                field: "action",
                title: "View",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html = '<button type="button" class="btn btn-sm btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air view_IRdet" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-eye"></i></button>';
                    return html;
                },
            }
            ],
        };
        var datatable = $('#pendinguploads_Datatable').mDatatable(options);
    };
    return {
        init: function () {
            irrecord();
        }
    };
}();

// Pending Recommendation


var recordsIRDatatable_pendingrecommendation = function () {
    var irrecord = function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/list_irpendingRecommendation_datatable",
                        params: {
                            query: {
                                subjectname: $('#pendingRecsubject_select').val(),
                                eroname: $('#pendingEroRec_select').val(),
                                reco_erofile: $('#recoType_file').val(),
                                ir_recpendingid: $('#eropendingRec_irid').val()
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "incidentReport_ID",
                title: "IR ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.incidentReport_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "subjectEmp_ID",
                title: "Subject",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.subjectFname + " " + row.subjectLname;
                    return html;
                },
            },
            {
                field: "eroEmp_ID",
                title: "Employee Relations Officer",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.eroFname + " " + row.eroLname;
                    return html;
                },
            }, {
                field: "incidentDate",
                title: "Incident Date",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.incidentDate, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            },
            {
                field: "",
                title: "Recommendation Type",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    if (row.type == "last") {
                        html = '<span class="m-menu__link-badge"><span class="m-badge m-badge--info m-badge--wide">LAST</span></span>';
                    } else {
                        html = '<span class="m-menu__link-badge"><span class="m-badge m-badge--accent m-badge--wide">FINAL</span></span>';
                    }
                    return html;
                },
            },
            {
                field: "action",
                title: "View",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html = '<button type="button" class="btn btn-sm btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air view_IRdet" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-eye"></i></button>';
                    return html;
                },
            }
            ],
        };
        var datatable = $('#erorecommendation_pendingDatatable').mDatatable(options);
    };
    return {
        init: function () {
            irrecord();
        }
    };
}();


// Missed Deadline 

var recordsIRDatatable_missedDeadline = function () {
    var irrecord = function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/list_irmissedDeadline_datatable",
                        params: {
                            query: {
                                subjectname: $('#pendingmissedDeadline_subjectname').val(),
                                eroname: $('#pendingmissedDeadline_eroname').val(),
                                task: $('#pendingmissedDeadline_task').val(),
                                ir_idmissed: $('#pendingmissedDeadline_searchid').val()
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "incidentReport_ID",
                title: "IR ID",
                width: 110,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.incidentReport_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "subjectEmp_ID",
                title: "Subject",
                width: 110,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.subjectFname + " " + row.subjectLname;
                    return html;
                },
            },
            {
                field: "eroEmp_ID",
                title: "Employee Relations Officer",
                width: 110,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.eroFname + " " + row.eroLname;
                    return html;
                },
            }, {
                field: "incidentDate",
                title: "Incident Date",
                width: 110,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.incidentDate, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            }
                ,
            {
                field: "deadline",
                title: "Deadline Date",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html = moment(row.deadline, 'Y-MM-DD HH:mm:ss').format('MMM D, Y HH:mm:ss A');
                    return html;
                },
            },
            {
                field: "task",
                title: "Task",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    if (row.a_purpose == "nte") {
                        html = "<span style='font-size:13px; font-weight: bolder; color: mediumorchid'>upload NTE</span>";
                    } else if (row.a_purpose == "nor") {
                        html = "<span style='font-size:13px; font-weight: bolder; color: cornflowerblue'>upload NOR</span>";
                    }
                    else {
                        html = "<span style='font-size:13px; font-weight: bolder; color: darkcyan'>" + row.typeDescription + "</span>";
                    }
                    return html;
                },
            },
            {
                field: "action",
                title: "View",
                width: 110,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html = '<button type="button" class="btn btn-sm btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air view_IRdet" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-eye"></i></button>';
                    return html;
                },
            }
            ],
        };
        var datatable = $('#missedDeadline_Datatable').mDatatable(options);
    };
    return {
        init: function () {
            irrecord();
        }
    };
}();

// Date 

function initRecordDateRangePicker_irrecords(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#recordsall-date .form-control').val(start + ' - ' + end);
        $('#recordsall-date').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#recordsall-date .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            // initEmployeeRecorNameField(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function(obj){
            initir_forall(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'));
            //  initReport(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'));
            //    });
        });
        var dateRanges = {
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        };
        callback(dateRanges);
    });
}

function fetch_selectforIRrecords() {
    //if 1= access tab is pending, if 2 = access tab is 
    $.ajax({
        url: baseUrl + "/discipline/allir_recordsfilter",
        type: "POST",
        cache: false,
        data:{
            tab:$("#hidden_base_url").data("tab"),
            access:$("#pendingir_Tab").data('type')
        },
        success: function (data) {
            var result = JSON.parse(data);
            var stringsubname_pending = "";
            var stringaccount_pending = "";
            var stringsubname_all = "";
            var stringaccount_all = "";
            var stringall_offense = "";
            var stringall_offenseType = "";
            var stringpending_offense = "";
            var stringpending_offenseType = "";
            var stringsus_account = "";
            var stringsus_subject = "";

            stringsubname_pending += "<option disabled selected=''>Select subject</option>";
            stringsubname_pending += "<option value='0'>All</option>";

            stringsubname_all += "<option disabled selected=''>Select subject</option>";
            stringsubname_all += "<option value='0'>All</option>";

            stringaccount_pending += "<option disabled selected=''>Select account</option>";
            stringaccount_pending += "<option value='0'>All</option>";

            stringaccount_all += "<option disabled selected=''>Select account</option>";
            stringaccount_all += "<option value='0'>All</option>";

            stringall_offense += "<option disabled selected=''>Select offense</option>";
            stringall_offense += "<option value='0'>All</option>";

            stringall_offenseType += "<option disabled selected=''>Select nature of offense</option>";
            stringall_offenseType += "<option value='0'>All</option>";


            stringpending_offense += "<option disabled selected=''>Select offense</option>";
            stringpending_offense += "<option value='0'>All</option>";

            stringpending_offenseType += "<option disabled selected=''>Select nature of offense</option>";
            stringpending_offenseType += "<option value='0'>All</option>";

            stringsus_subject += "<option disabled selected=''>Select subject</option>";;
            stringsus_subject += "<option value='0'>All</option>";

            stringsus_account += "<option disabled selected=''>Select account</option>";;
            stringsus_account += "<option value='0'>All</option>";

            $.each(result.pending_subject, function (key, data) {
                stringsubname_pending += "<option value=" + data.subjectEmp_ID + ">" + data.subjectLname + ", " + data
                    .subjectFname + "</option>";
            });
            $.each(result.pending_account, function (key, data) {
                stringaccount_pending += "<option value=" + data.acc_id + ">" + data.subjectAccount + "</option>";
            });
            $.each(result.all_subject, function (key, data) {
                stringsubname_all += "<option value=" + data.subjectEmp_ID + ">" + data.subjectLname + ", " + data
                    .subjectFname + "</option>";
            });
            $.each(result.all_acc, function (key, data) {
                stringaccount_all += "<option value=" + data.acc_id + ">" + data.subjectAccount + "</option>";
            });
            $.each(result.pending_offense, function (key, data) {
                stringpending_offense += "<option value=" + data.offense_ID + ">" + data.offense + "</option>";
            });
            $.each(result.pending_offenseType, function (key, data) {
                stringpending_offenseType += "<option value=" + data.offenseType_ID + ">" + data.offenseType + "</option>";
            });
            $.each(result.all_offense, function (key, data) {
                stringall_offense += "<option value=" + data.offense_ID + ">" + data.offense + "</option>";
            });
            $.each(result.all_offenseType, function (key, data) {
                stringall_offenseType += "<option value=" + data.offenseType_ID + ">" + data.offenseType + "</option>";
            });
            $.each(result.susterm_account, function (key, data) {
                stringsus_account += "<option value=" + data.acc_id + ">" + data.subjectAccount + "</option>";
            });
            $.each(result.susterm_subject, function (key, data) {
                stringsus_subject += "<option value=" + data.subjectEmp_ID + ">" + data.subjectLname + ", " + data
                    .subjectFname + "</option>";
            });
            $("#irpending_subjectname").html(stringsubname_pending);
            $("#irpending_account").html(stringaccount_pending);
            $("#irall_subject").html(stringsubname_all);
            $("#accall_select").html(stringaccount_all);
            $("#irpending_natureofoffense").html(stringpending_offenseType);
            $("#irpending_offense").html(stringpending_offense);
            $("#offenseall_filter").html(stringall_offense);
            $("#natureall_offense").html(stringall_offenseType);
            $("#irsusterm_subjectname").html(stringsus_subject);
            $("#irsusterm_account").html(stringsus_account);
        }
    });
}

function fetch_selectforPending() {
    $.ajax({
        url: baseUrl + "/discipline/eropending_recordsfilter",
        type: "POST",
        cache: false,
        success: function (data) {
            var result = JSON.parse(data);
            var stringeroup_subject = "";
            var stringeroup_eronames = "";
            var stringreco_subject = "";
            var stringreco_eronames = "";
            var stringmd_subject = "";
            var stringmd_eronames = "";
            var stringmd_task = "";

            stringeroup_subject += "<option disabled selected=''>Select subject</option>";
            stringeroup_subject += "<option value='0'>All</option>";

            stringeroup_eronames += "<option disabled selected=''>Select employee relation officer</option>";
            stringeroup_eronames += "<option value='0'>All</option>";

            stringreco_subject += "<option disabled selected=''>Select subject</option>";
            stringreco_subject += "<option value='0'>All</option>";

            stringreco_eronames += "<option disabled selected=''>Select employee relation officer</option>";
            stringreco_eronames += "<option value='0'>All</option>";

            stringmd_subject += "<option disabled selected=''>Select subject</option>";
            stringmd_subject += "<option value='0'>All</option>";

            stringmd_eronames += "<option disabled selected=''>Select employee relation officer</option>";
            stringmd_eronames += "<option value='0'>All</option>";

            stringmd_task += "<option disabled selected=''>Select task</option>";
            stringmd_task += "<option value='0'>All</option>";

            $.each(result.penUpload_subject, function (key, data) {
                stringeroup_subject += "<option value=" + data.subjectEmp_ID + ">" + data.subjectLname + ", " + data
                    .subjectFname + "</option>";
            });
            $.each(result.penUpload_eroname, function (key, data) {
                stringeroup_eronames += "<option value=" + data.eroEmp_ID + ">" + data.eroLname + ", " + data
                    .eroFname + "</option>";
            });

            $.each(result.penRecom_subject, function (key, data) {
                stringreco_subject += "<option value=" + data.subjectEmp_ID + ">" + data.subjectLname + ", " + data
                    .subjectFname + "</option>";
            });
            $.each(result.penRecom_eroname, function (key, data) {
                stringreco_eronames += "<option value=" + data.empEro_id + ">" + data.eroLname + ", " + data
                    .eroFname + "</option>";
            });

            $.each(result.penMissedDeadline_subject, function (key, data) {
                stringmd_subject += "<option value=" + data.subjectEmp_ID + ">" + data.subjectLname + ", " + data
                    .subjectFname + "</option>";
            });
            $.each(result.penMissedDeadline_eroname, function (key, data) {
                stringmd_eronames += "<option value=" + data.eroEmp_ID + ">" + data.eroLname + ", " + data
                    .eroFname + "</option>";
            });
            $.each(result.penMissedDeadline_task, function (key, data) {
                stringmd_task += "<option value=" + data.dmsDeadlineType_ID + ">" + data.typeDescription + "</option>";
            });
            $("#pendingsubject_select").html(stringeroup_subject);
            $("#pendingEroUp_select").html(stringeroup_eronames);
            $("#pendingRecsubject_select").html(stringreco_subject);
            $("#pendingEroRec_select").html(stringreco_eronames);
            $("#pendingmissedDeadline_subjectname").html(stringmd_subject);
            $("#pendingmissedDeadline_eroname").html(stringmd_eronames);
            $("#pendingmissedDeadline_task").html(stringmd_task);
        }
    });
}

function fetchsus_term_dates(irr_id) {
    var d_label = $("#cal" + irr_id).data('label');
    var day_s = $("#cal" + irr_id).data('day');
    if (d_label == "t") {
        $("#modaldate_title").text("Termination Date");
    } else {
        $("#modaldate_title").text("Suspension Dates");
    }
    $.ajax({
        url: baseUrl + "/discipline/fetch_sustermdetails",
        type: "POST",
        data: {
            irid: irr_id
        },
        cache: false,
        success: function (data) {
            var result = JSON.parse(data);
            console.log(result);
            var datedisplay = "";
            var memdate = "";
            var lengthdate = result.length;
            var stringxx = "";
            day_s = day_s - lengthdate;
            $.each(result, function (key, data) {
                memdate = moment(data.sustermdate, 'Y-MM-DD').format('MMM DD, Y');
                var a = moment(data.sustermdate).format('ddd');
                var date_now = moment().format('Y-MM-DD');
                if (data.label == "t") {
                    stringxx = "The date below is the <strong> Termination Date </strong> set by the direct supervisor.";
                    $("#remaining_days").html(stringxx);
                    if (date_now > data.sustermdate) {
                        // nilabay na ang termination day 
                        datedisplay += "<li class='list-group-item' style='background:#f2cabc'><i class='fa fa-calendar-check-o text-success mx-2'></i>" + memdate + " - " + a + " " + " <strong>&nbsp;&nbsp; : TERMINATION DAY </strong> has passed " + "</li>";
                    } else if (date_now == data.sustermdate) {
                        // Termination day
                        datedisplay += "<li class='list-group-item' style='background:#cbeeb3'><i class='fa fa-calendar-check-o text-success mx-2'></i>" + memdate + " - " + a + " " + " <strong>&nbsp;&nbsp; : TERMINATION DAY </strong>" + "</li>";
                    } else {
                        datedisplay += "<li class='list-group-item' style='background:lemonchiffon'><i class='fa fa-calendar-check-o text-success mx-2'></i>" + memdate + " - " + a + "</li>";
                    }
                } else {
                    if (day_s != 0) {
                        stringxx = "<span>There are still <strong>" + day_s + "</strong> remaining suspension dates that are not yet determined by the Direct Supervisor.</span>";
                    } else {
                        stringxx = "Below are/is the <strong> Suspension Date/s </strong> set by the direct supervisor.";
                    }
                    $("#remaining_days").html(stringxx);
                    if (date_now > data.sustermdate) {
                        datedisplay += "<li class='list-group-item' style='background:#f2cabc'><i class='fa fa-calendar-check-o text-success mx-2'></i>" + memdate + " - " + a + " " + " <strong>&nbsp;&nbsp; : SUSPENSION DAY </strong> has passed " + "</li>";
                    } else if (date_now == data.sustermdate) {
                        datedisplay += "<li class='list-group-item' style='background:#cbeeb3'><i class='fa fa-calendar-check-o text-success mx-2'></i>" + memdate + " - " + a + " " + " <strong>&nbsp;&nbsp; : SUSPENSION DAY </strong>" + "</li>";
                    } else {
                        datedisplay += "<li class='list-group-item' style='background:lightcyan'><i class='fa fa-calendar-check-o text-info mx-2'></i>" + memdate + " - " + a + "</li>";
                    }
                }
            });
            $("#irsusterm_dates").html(datedisplay);
            $("#calendar_displaydate").modal('show');

        }
    });

}
function allirmoredetails(irid) {
    $('#modal_displayevidences').data('irid', irid);
    $.ajax({
        url: baseUrl + "/discipline/access_ir_details_ero",
        type: "POST",
        data: {
            irid: irid
        },
        cache: false,
        success: function (data) {
            var result = JSON.parse(data);
            $("#allir_filed_details").html(result.ir_details_layout);
            $("#allirDetails").modal();
        }
    });
}