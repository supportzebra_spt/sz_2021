// PENDING DTR VIO COUNT
function pendingDtrVioCountMonitoring(late, ob, ut, forgotToLogout, incBreak, incDtr, absent) {
    if (late > 0) {
        $('#pendingLateCountDivMonitoring').fadeIn();
        $('#pendingLateCountMonitoring').text(late);
    } else {
        $('#pendingLateCountDivMonitoring').hide();
    }
    if (ob > 0) {
        $('#pendingObCountDivMonitoring').fadeIn();
        $('#pendingObCountMonitoring').text(ob);
    } else {
        $('#pendingObCountDivMonitoring').hide();
    }
    if (ut > 0) {
        $('#pendingUtCountDivMonitoring').fadeIn();
        $('#pendingUtCountMonitoring').text(ut);
    } else {
        $('#pendingUtCountDivMonitoring').hide();
    }
    if (forgotToLogout > 0) {
        $('#pendingForgotToLogoutCountDivMonitoring').fadeIn();
        $('#pendingForgotToLogoutCountMonitoring').text(forgotToLogout);
    } else {
        $('#pendingForgotToLogoutCountDivMonitoring').hide();
    }
    if (incBreak > 0) {
        $('#pendingIncBreakCountDivMonitoring').fadeIn();
        $('#pendingIncBreakCountMonitoring').text(incBreak);
    } else {
        $('#pendingIncBreakCountDivMonitoring').hide();
    }
    if (incDtr > 0) {
        $('#pendingIncDtrCountDivMonitoring').fadeIn();
        $('#pendingIncDtrCountMonitoring').text(incDtr);
    } else {
        $('#pendingIncDtrCountDivMonitoring').hide();
    }
    if (absent > 0) {
        $('#pendingAbsentCountDivMonitoring').fadeIn();
        $('#pendingAbsentCountMonitoring').text(absent);
    } else {
        $('#pendingAbsentCountDivMonitoring').hide();
    }
}

function recordDtrVioCountMonitoring(late, ob, ut, forgotToLogout, incBreak, incDtr, absent) {
    if (late > 0) {
        $('#recordLateCountDivMonitoring').fadeIn();
        $('#recordLateCountMonitoring').text(late);
    } else {
        $('#recordLateCountDivMonitoring').hide();
    }
    if (ob > 0) {
        $('#recordObCountDivMonitoring').fadeIn();
        $('#recordObCountMonitoring').text(ob);
    } else {
        $('#recordObCountDivMonitoring').hide();
    }
    if (ut > 0) {
        $('#recordUtCountDivMonitoring').fadeIn();
        $('#recordUtCountMonitoring').text(ut);
    } else {
        $('#recordUtCountDivMonitoring').hide();
    }
    if (forgotToLogout > 0) {
        $('#recordForgotToLogoutCountDivMonitoring').fadeIn();
        $('#recordForgotToLogoutCountMonitoring').text(forgotToLogout);
    } else {
        $('#recordForgotToLogoutCountDivMonitoring').hide();
    }
    if (incBreak > 0) {
        $('#recordIncBreakCountDivMonitoring').fadeIn();
        $('#recordIncBreakCountMonitoring').text(incBreak);
    } else {
        $('#recordIncBreakCountDivMonitoring').hide();
    }
    if (incDtr > 0) {
        $('#recordIncDtrCountDivMonitoring').fadeIn();
        $('#recordIncDtrCountMonitoring').text(incDtr);
    } else {
        $('#recordIncDtrCountDivMonitoring').hide();
    }
    if (absent > 0) {
        $('#recordAbsentCountDivMonitoring').fadeIn();
        $('#recordAbsentCountMonitoring').text(absent);
    } else {
        $('#recordAbsentCountDivMonitoring').hide();
    }
}

function missedAlert(missedCount){
    if(missedCount > 0){
        var highlight = "";
        var Mssg = ""
        if (missedCount > 1){
            highlight = missedCount+" DTR Violations";
            Mssg = "were failed to be filed on time.";
        }else{
            highlight = missedCount + " DTR Violation";
            Mssg = "was failed to be filed on time.";
        }
        $('#missedHighlight').text(highlight);
        $('#missedMssg').text(Mssg);
        $('#missedAlert').fadeIn();
    }else{
        $('#missedAlert').hide();
    }
}

// ORDINARY INIT FUNCTIONS
// BADGES
function initDtrViolationBadgeMonitoring() {
    var dtrViolationBadge = 0;
    dtrViolationBadge += parseInt($('#fileDtrViolationIrBadgeCountMonitoring').attr('count'));
    if (dtrViolationBadge > 0) {
        $('#dtrViolationBadgeCountMonitoring').text(dtrViolationBadge);
        $('#dtrViolationBadgeMonitoring').fadeIn();
    } else {
        $('#dtrViolationBadgeMonitoring').hide();
    }
}

function initfileDtrViolationIrBadgeMonitoring(callback) {
    $.when(fetchGetData('/discipline/get_file_dtr_violation_ir_count_monitoring')).then(function (count) {
        var countObj = $.parseJSON(count.trim());
        if (parseInt(countObj.count) > 0) {
            $('#fileDtrViolationIrBadgeCountMonitoring').text(countObj.count);
            $('#fileDtrViolationIrBadgeCountMonitoring').attr('count', countObj.count);
            $('#fileDtrViolationIrBadgeMonitoring').fadeIn();
        } else {
            $('#fileDtrViolationIrBadgeMonitoring').hide();
        }
        callback(countObj);
    });
}

// DATATABLES
var irableDtrViolationMonitoringDatatable = function () {
    var irableDtrViolationMonitoring = function (empIdVal, violationTypeVal, schedVal, statVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_irable_dtr_violation_monitoring_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                                violationType: violationTypeVal,
                                sched: schedVal,
                                stat: statVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            var late = 0;
                            var ob = 0;
                            var ut = 0;
                            var forgotToLogout = 0;
                            var incBreak = 0;
                            var incDtr = 0;
                            var absent = 0;
                            var missed = 0;
                            
                            $.each(raw.data, function (index, value) {
                                if (parseInt(value.dtrViolationType_ID) == 1) {
                                    late++;
                                } else if (parseInt(value.dtrViolationType_ID) == 2) {
                                    ut++;
                                } else if (parseInt(value.dtrViolationType_ID) == 3) {
                                    forgotToLogout++;
                                } else if (parseInt(value.dtrViolationType_ID) == 4) {
                                    ob++;
                                } else if (parseInt(value.dtrViolationType_ID) == 5) {
                                    absent++;
                                } else if (parseInt(value.dtrViolationType_ID) == 6) {
                                    incBreak++;
                                } else if (parseInt(value.dtrViolationType_ID) == 7) {
                                    incDtr++;
                                }

                                if (parseInt(value.status_ID) == 12){
                                    missed++;
                                }
                            })
                            missedAlert(missed);
                            pendingDtrVioCountMonitoring(late, ob, ut, forgotToLogout, incBreak, incDtr, absent)
                            dataCount = dataSet.length;
                            console.log(dataSet);
                            console.log(dataCount);
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: 'qualifiedUserDtrViolation_ID',
                width: 80,
                title: 'Q-ID',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + row.qualifiedUserDtrViolation_ID.padLeft(8) + "</span>";
                    return html;
                },
            },{
                field: 'employee',
                width: 160,
                title: 'Employee',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if(row.status_ID == 12){
                        textColor = "text-danger";
                    }
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null) {
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#irFiling" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="row"><span class="col-2"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"></span><span class="' + textColor + ' col-10">' + toTitleCase(row.fname + ' ' + row.lname) + '</span></div>';
                    return html;
                },
            }, {
                field: 'description',
                width: 60,
                title: 'Type',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if(row.status_ID == 12){
                        textColor = "text-danger";
                    }
                    var html = "<span class='"+textColor+"'>" + toTitleCase(row.description)+"</span>";
                    return html;
                }
            }, {
                field: 'sched_date',
                width: 90,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'supEmp_id',
                width: 200,
                title: 'Current Filer',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.supPic != null) {
                        var picName = row.supPic.substr(0, row.supPic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#irFiler" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="row"><span class="col-2"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"></span> <span class="' + textColor + ' col-10">' + toTitleCase(row.supFname + ' ' + row.supLname) + '</span></div>';
                    // var html = toTitleCase(row.supFname + ' ' + row.supLname);
                    return html;
                },
            }, {
                field: 'action',
                width: 80,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air qualifiedDtrViolation" title="View DTR Violation" data-qualifiedid="' + row.qualifiedUserDtrViolation_ID + '"><i class="la la-edit"></i></a>';
                    if (parseInt(row.supStatus_ID) == 12){
                        var html = '<span class="btn btn-sm m-btn--pill btn-warning" style="font-size: 0.7rem;">missed</span>';
                    }
                    return html;
                },
            }],
        };
        var datatable = $('#irAbleDtrViolationMonitoringDatatable').mDatatable(options);
    };
    return {
        init: function (empIdVal, violationTypeVal, schedVal, statVal) {
            irableDtrViolationMonitoring(empIdVal, violationTypeVal, schedVal, statVal);
        }
    };
}();

var irableDtrViolationRecordMonitoringDatatable = function () {
    var irableDtrViolationRecordMonitoring = function (empIdVal, violationTypeVal, dateStartVal, dateEndVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_irable_dtr_violation_record_monitoring_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                                violationType: violationTypeVal,
                                dateStart: dateStartVal,
                                dateEnd: dateEndVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            var late = 0;
                            var ob = 0;
                            var ut = 0;
                            var forgotToLogout = 0;
                            var incBreak = 0;
                            var incDtr = 0;
                            var absent = 0;
                            var missed = 0;

                            $.each(raw.data, function (index, value) {
                                if (parseInt(value.dtrViolationType_ID) == 1) {
                                    late++;
                                } else if (parseInt(value.dtrViolationType_ID) == 2) {
                                    ut++;
                                } else if (parseInt(value.dtrViolationType_ID) == 3) {
                                    forgotToLogout++;
                                } else if (parseInt(value.dtrViolationType_ID) == 4) {
                                    ob++;
                                } else if (parseInt(value.dtrViolationType_ID) == 5) {
                                    absent++;
                                } else if (parseInt(value.dtrViolationType_ID) == 6) {
                                    incBreak++;
                                } else if (parseInt(value.dtrViolationType_ID) == 7) {
                                    incDtr++;
                                }

                                if (parseInt(value.status_ID) == 12) {
                                    missed++;
                                }
                            })

                            recordDtrVioCountMonitoring(late, ob, ut, forgotToLogout, incBreak, incDtr, absent)
                            dataCount = dataSet.length;

                            if (dataCount > 0) {
                                $('#noIrableDtrViolationRecordMonitoring').hide();
                                $('#irableDtrViolationRecordMonitoring').fadeIn();
                            } else {
                                $('#noIrableDtrViolationRecordMonitoring').fadeIn();
                                $('#irableDtrViolationRecordMonitoring').hide();
                            }

                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: 'qualifiedUserDtrViolation_ID',
                width: 80,
                title: 'Q-ID',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + row.qualifiedUserDtrViolation_ID.padLeft(8) + "</span>";
                    return html;
                },
            }, {
                field: 'employee',
                width: 180,
                title: 'Employee',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null) {
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }

                    var elementId = "#irMonitoringRecord" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="row"><span class="col-2"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"></span><span class="' + textColor + ' col-10">' + toTitleCase(row.fname + ' ' + row.lname) + '</span></div>';
                    return html;
                },
            }, {
                field: 'description',
                width: 60,
                title: 'Type',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + toTitleCase(row.description) + "</span>";
                    return html;
                }
            }, {
                field: 'sched_date',
                width: 90,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'supEmp_id',
                width: 200,
                title: 'Filed By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null) {
                        var picName = row.supPic.substr(0, row.supPic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#irFiler" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="row"><span class="col-2"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"></span> <span class="' + textColor + ' col-10">' + toTitleCase(row.supFname + ' ' + row.supLname) + '</span></div>';
                    // var html = toTitleCase(row.supFname + ' ' + row.supLname);
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air viewQualifiedDtrRecord" title="File Qualified DTR Violation" data-qualifiedid="' + row.qualifiedUserDtrViolation_ID + '"><i class="la la-edit"></i></a>';
                    return html;
                },
            }],
        };
        var datatable = $('#irAbleDtrViolationRecordMonitoringDatatable').mDatatable(options);
    };
    return {
        // ($('#fileDtrViolationEmpMonitoringRecordSelect').val(), $('#violationTypeMonitoringRecord').val(), dateStart, dateEnd)
        init: function (empIdVal, violationTypeVal, dateStartVal, dateEndVal) {
            irableDtrViolationRecordMonitoring(empIdVal, violationTypeVal, dateStartVal, dateEndVal);
        }
    };
}();

var dismissedDtrViolationDatatable = function () {
    var dismissedDtrViolation = function (empIdVal, violationTypeVal, dateStartVal, dateEndVal) {
        var options = {
            data: { 
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_dismissed_dtr_violation_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                                violationType: violationTypeVal,
                                dateStart: dateStartVal,
                                dateEnd: dateEndVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            if (dataCount > 0) {
                                $('#noDismissedQualifiedDtrViolations').hide();
                                $('#dismissedQualifiedDtrViolations').fadeIn();
                                $('#downloadDismissed').prop('disabled', false);
                            } else {
                                $('#noDismissedQualifiedDtrViolations').fadeIn();
                                $('#dismissedQualifiedDtrViolations').hide();
                                $('#downloadDismissed').prop('disabled', true);
                            }
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: 'qualifiedUserDtrViolation_ID',
                width: 80,
                title: 'Q-ID',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + row.qualifiedUserDtrViolation_ID.padLeft(8) + "</span>";
                    return html;
                },
            },{
                field: 'emp_id',
                width: 160,
                title: 'Employee',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null) {
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }

                    var elementId = "#irDismissed" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="row"><span class="col-2"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"></span><span class="' + textColor + ' col-10">' + toTitleCase(row.fname + ' ' + row.lname) + '</span></div>';
                    return html;
                }
            },{
                field: 'description',
                width: 60,
                title: 'Type',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + toTitleCase(row.description) + "</span>";
                    return html;
                }
            }, {
                field: 'sched_date',
                width: 90,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'supEmp_id',
                width: 200,
                title: 'Dismissed By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.supPic != null) {
                        var picName = row.supPic.substr(0, row.supPic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#irFiler" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="row"><span class="col-2"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"></span> <span class="' + textColor + ' col-10">' + toTitleCase(row.supFname + ' ' + row.supLname) + '</span></div>';
                    // var html = toTitleCase(row.supFname + ' ' + row.supLname);
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air dismissedQualifiedDtrRecord" title="View Details" data-qualifiedid="' + row.qualifiedUserDtrViolation_ID + '"><i class="fa fa-search"></i></a>';
                    return html;
                },
            }],
        };
        var datatable = $('#dismissedQualifiedDtrVioDatatable').mDatatable(options);
    };
    return {
        init: function (empIdVal, violationTypeVal, dateStartVal, dateEndVal) {
            dismissedDtrViolation(empIdVal, violationTypeVal, dateStartVal, dateEndVal);
        }
    };
}();

function initIrAbleDtrViolationMonitoringRecordDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#fileDtrViolationMonitoringRecordDateRange .form-control').val(start + ' - ' + end);
        $('#fileDtrViolationMonitoringRecordDateRange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            console.log('record date range test');
            $('#fileDtrViolationMonitoringRecordDateRange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initIrAbleDtrViolationRecordMonitoring(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);
    });
}
function initDismissedDtrViolationDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#dismissedEmpMonitoringDateRange .form-control').val(start + ' - ' + end);
        $('#dismissedEmpMonitoringDateRange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            console.log('dismissed date range picker');
            $('#dismissedEmpMonitoringDateRange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            get_emp_dtr_dismissed_vio_ir_select(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {
                initDismissedDtrViolationDatatable(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), 'destroy', function (notEmpty) {});
            });
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);
    });
}

function get_emp_dtr_vio_ir_select_monitoring(callback) {
    var sched_date = $('#dtrViolationSchedMonitoring').val();
    if (sched_date != '') {
        sched_date = moment($('#dtrViolationSchedMonitoring').val(), 'MM/DD/YYYY').format('Y-MM-DD');
    }
    $.when(fetchPostData({
        violationType: $('#violationTypeMonitoring').val(),
        sched: sched_date,
        stat: $('#fileDtrViolationMonitoringStatus').val(),
    }, '/discipline/get_specific_qualified_emp_ir_monitoring')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        $('#fileDtrViolationEmpSelectMonitoring').empty().trigger('change');
        console.log(empObj.record);
        if (empObj.record != 0) {
            $('#noIrableDtrViolationMonitoring').hide();
            $('#irableDtrViolationMonitoring').fadeIn();
        } else {
            $('#noIrableDtrViolationMonitoring').fadeIn();
            $('#irableDtrViolationMonitoring').hide();
        }
        init_select2(empObj, "#fileDtrViolationEmpSelectMonitoring", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

function get_emp_dtr_vio_ir_monitoring_record_select(startDate, endDate, callback) {
    $.when(fetchPostData({
        violationType: $('#violationTypeMonitoringRecord').val(),
        dateStart: startDate,
        dateEnd: endDate
    }, '/discipline/get_specific_qualified_emp_ir_record_monitoring')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        console.log(empObj);
        $('#fileDtrViolationEmpMonitoringRecordSelect').empty().trigger('change');
        init_select2(empObj, "#fileDtrViolationEmpMonitoringRecordSelect", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

function get_emp_dtr_dismissed_vio_ir_select(startDate, endDate, callback) {
    $.when(fetchPostData({
        violationType: $('#violationTypeMonitoringDismissed').val(),
        dateStart: startDate,
        dateEnd: endDate
    }, '/discipline/get_specific_dismissed_qualified_emp')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        $('#dismissedEmpMonitoring').empty().trigger('change');
        init_select2(empObj, "#dismissedEmpMonitoring", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

// DATATABLE INIT
function initIrAbleDtrViolationMonitoring() {
    var sched_date = $('#dtrViolationSchedMonitoring').val();
    if (sched_date != '') {
        sched_date = moment($('#dtrViolationSchedMonitoring').val(), 'MM/DD/YYYY').format('Y-MM-DD');
    }
    $('#irAbleDtrViolationMonitoringDatatable').mDatatable('destroy');
    irableDtrViolationMonitoringDatatable.init($('#fileDtrViolationEmpSelectMonitoring').val(), $('#violationTypeMonitoring').val(), sched_date, $('#fileDtrViolationMonitoringStatus').val());
}

function initIrAbleDtrViolationRecordMonitoringDatatable(dateStart, dateEnd, initType) {
    $('#irAbleDtrViolationRecordMonitoringDatatable').mDatatable(initType);
    irableDtrViolationRecordMonitoringDatatable.init($('#fileDtrViolationEmpMonitoringRecordSelect').val(), $('#violationTypeMonitoringRecord').val(), dateStart, dateEnd);
}

function initDismissedDtrViolationDatatable(dateStart, dateEnd, initType) {
    $('#dismissedQualifiedDtrVioDatatable').mDatatable(initType);
    dismissedDtrViolationDatatable.init($('#dismissedEmpMonitoring').val(), $('#violationTypeMonitoringDismissed').val(), dateStart, dateEnd);
}

function initIrAbleDtrViolationRecordMonitoring(startDate, endDate, callback) {
    get_emp_dtr_vio_ir_monitoring_record_select(startDate, endDate, function (notEmpty) {
        initIrAbleDtrViolationRecordMonitoringDatatable(startDate, endDate, 'destroy');
        callback(notEmpty);
    });
}

function initDismissedDtrViolationRecord(startDate, endDate, callback) {
    get_emp_dtr_dismissed_vio_ir_select(startDate, endDate, function (notEmpty) {
        initDismissedDtrViolationDatatable(startDate, endDate, 'destroy');
        callback(notEmpty);
    });
}



// MIC QUALIFIED MODAL
function initQualifiedModal(element) {
    $.when(fetchPostData({
        qualifiedViolationId: $(element).data('qualifiedid'),
    }, '/discipline/get_qualified_dtr_vio_details')).then(function (qualified) {
        let qualifiedObj = $.parseJSON(qualified.trim());
        let incidentDetails = "";
        let excess = "";
        let expectedActionDetails = "";
        let allowableTable = "";
        let coveredTable = "";
        let missingLogsDetails = "";
        let missingLogs = "";
        let expectedAction = "";
        let narrativeDetails = ""
        let pronoun = "";
        let shiftTime = qualifiedObj.details.shift.split('-');
        let reasonVal = "";
        let dismissBtn = "";
        //  + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') + "), which is " + qualifiedObj.details.incident_details.duration + " later than " + pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD HH:mm').format('MMM DD, Y')
        if (qualifiedObj.details.emp_details.gender == 'Male') {
            pronoun = "his";
        } else {
            pronoun = "her";
        }
        if (parseInt(qualifiedObj.error) == 0) {
            var allowableTableBody = "";
            var coveredTableBody = "";
            // EMPLOYEE DETAILS
            $('#qualifiedModal').data('qualifiedid', $(element).data('qualifiedid'));
            $('#empPicApproval').attr('src', baseUrl + "/" + qualifiedObj.details.emp_details.pic);
            $('#qualifiedEmployeeName').text(toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname));
            $('#qualifiedEmployeeName').data('empid', qualifiedObj.details.emp_details.emp_id);
            $('#qualifiedEmployeeJob').text(qualifiedObj.details.emp_details.positionDescription);
            $('#qualifiedPosEmpStat').text(" -" + qualifiedObj.details.emp_details.empStatus);
            $('#qualifiedPosEmpStat').data('posempstat', qualifiedObj.details.emp_details.empStatus);
            $('#qualifiedEmployeeJob').data('position', qualifiedObj.details.emp_details.positionDescription);
            $('#qualifiedEmployeeAccount').text(toTitleCase(qualifiedObj.details.emp_details.accountDescription));
            $('#qualifiedEmployeeAccount').data('account', toTitleCase(qualifiedObj.details.emp_details.accountDescription));
            $('#qualifiedEmployeeAccount').data('accountid', toTitleCase(qualifiedObj.details.emp_details.acc_id));
            $('#qualifiedEmployeeAccount').data('emptype', toTitleCase(qualifiedObj.details.emp_details.empType));
            // Date and Incidents
            // $('#qualifiedDateIncident').text(moment(qualifiedObj.details.sched_date, 'Y-MM-DD').format('MMM DD, Y'));
            // $('#qualifiedDateIncident').data('incidentdate', qualifiedObj.details.sched_date);
            $('#qualifiedIncidentType').text(toTitleCase(qualifiedObj.details.violation_type));
            // Incident Details
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) { // OB
                $('#qualifiedDateIncident').text(moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('MMM DD, Y'));
                $('#qualifiedDateIncident').data('incidentdate', moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('Y-MM-DD'));
                $('#qualifiedDateIncident').data('incidenttime', moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('h:mm A'));
                expectedAction = "Should have not exceeded from the " + qualifiedObj.details.incident_details.total_allowable + " allowable break duration";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " has consumed a total of " + qualifiedObj.details.incident_details.total_covered + " break duration, which is " + qualifiedObj.details.incident_details.excess + " more than " + pronoun + " allowable break of " + qualifiedObj.details.incident_details.total_allowable + ".";
                incidentDetails = '<div class="col-12">' +
                    '<span class="font-weight-bold mr-2">Shift:</span>' +
                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                    '</div>' +
                    '<div class="col-12 mt-2 font-weight-bold">Break Details:</div>';
                excess = '<div class="col-12 mb-2">' +
                    '<span class="font-weight-bold mr-2">Over Break Duration:</span>' +
                    '<span class="">' + qualifiedObj.details.incident_details.excess + '</span>' +
                    '</div>';

                if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                    $.each(qualifiedObj.details.incident_details.break, function (index, value) {
                        allowableTableBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + value.type + '</td>' +
                            '<td class="text-center">' + value.allowable + '</td>' +
                            '</tr>';
                        coveredTableBody += '<tr>' +
                            '<td class="text-center">' +moment(value.out, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A') +  '</td>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.in, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A') + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                            '</tr>';
                    })
                    allowableTable = '<div class="col-12 col-md-4">' +
                        '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                        '<thead style="background: #555;color: white;">' +
                        '<tr>' +
                        '<th class="text-center">Type</th>' +
                        '<th class="text-center">Allowable</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        allowableTableBody +
                        '</tbody>' +
                        '</table>' +
                        '</div>';
                    coveredTable = '<div class="col-12 col-md-8 pl-0">' +
                        '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                        '<thead style="background: #555;color: white;">' +
                        '<tr>' +
                        '<th class="text-center">Out for Break</th>' +
                        '<th class="text-center">Back from Break</th>' +
                        '<th class="text-center">Covered</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        coveredTableBody +
                        '</tbody>' +
                        '</table>' +
                        '</div>';
                    incidentDetails += allowableTable + "" + coveredTable + "" + excess + "" + expectedActionDetails;
                } else {
                    // alert error
                }
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 5) { // ABSENT
                $('#qualifiedDateIncident').text(moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('MMM DD, Y'));
                $('#qualifiedDateIncident').data('incidentdate', moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('Y-MM-DD'));
                $('#qualifiedDateIncident').data('incidenttime', moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('h:mm A'));
                $('.withRule').hide();
                expectedAction = "Should have taken all necessary DTR transaction that proves you are present at work.";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " was determinded to have no DTR Log transactions in " + pronoun + " " + qualifiedObj.details.shift + " shift last " + moment(qualifiedObj.details.sched_date, 'Y-MM-DD').format('MMM DD, Y') + ", which proves " + pronoun +" absence on the said schedule date.";
                incidentDetails = '<div class="col-12 col-md-6">' +
                    '<span class="font-weight-bold mr-2">Shift:</span>' +
                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                    '</div>' +
                    '<div class="col-12 col-md-6 text-md-right mb-2">' +
                    '<span class="font-weight-bold mr-2">Logs:</span>' +
                    '<span class="">No Log Records Found</span>' +
                    '</div>';
            } else if ((parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6) || (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7)) {
                $('#qualifiedDateIncident').text(moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('MMM DD, Y'));
                $('#qualifiedDateIncident').data('incidentdate', moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('Y-MM-DD'));
                $('#qualifiedDateIncident').data('incidenttime', moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('h:mm A'));

                expectedAction = "Should have taken all the necessary DTR Transactions to complete all the required DTR Logs.";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " was determinded to have no logs of the following: " + qualifiedObj.details.incident_details.missing.join(", ");
                incidentDetails = '<div class="col-12 col-md-4">' +
                    '<span class="font-weight-bold mr-2">Shift:</span>' +
                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                    '</div>' +
                    '<div class="col-12 col-md-3 text-md-right">' +
                    '<span class="font-weight-bold mr-2">Missing Logs:</span>' +
                    '</div>';
                if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                    $.each(qualifiedObj.details.incident_details.missing, function (index, value) {
                        missingLogs += '<li>' + value + '</li>';
                    })
                    missingLogsDetails = '<div class="col-12 col-md-5">' +
                        '<ol>' + missingLogs +
                        '</ol>' +
                        '</div>';
                    incidentDetails += missingLogsDetails;
                } else {
                    // alert error
                }
            } else {
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1) {
                    expectedAction = "Should have clocked in at least 30 minutes before " + shiftTime[0];
                    narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " clocked in by " + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') + "), which is " + qualifiedObj.details.incident_details.duration + " later than " + pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('MMM DD, Y') + ") shift start.";
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                    expectedAction = "Should have not clocked out before " + shiftTime[1] + " to avoid Undertime.";
                    narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') + "), which is " + qualifiedObj.details.incident_details.duration + " earlier before " + pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('MMM DD, Y') + ") shift end.";
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 3) {
                    expectedAction = "Should have not clocked out beyond " + qualifiedObj.details.incident_details.allowable_logout_formatted + " clock out allowance after" + shiftTime[1];
                    narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') + "), which is " + qualifiedObj.details.incident_details.duration + " past from " + pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('MMM DD, Y') + ") shift end exceeding the allowable clock-out allowance of " + qualifiedObj.details.incident_details.allowable_logout_formatted + ".";
                    reasonVal = '<div class="col-12 pt-2">' +
                        '<div class="row">' +
                        '<div class="col-12 col-md-2 font-weight-bold">Reason:</div>' +
                        '<div class="col-12 col-md-10 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType"><i class="fa fa-quote-left mr-1" style="font-size: 0.9rem;"></i>' + qualifiedObj.details.reason.explanation + '<i class="fa fa-quote-right ml-1" style="font-size: 0.9rem;"></i></div>' +
                        '</div>' +
                        '</div>';
                }
                $('#qualifiedDateIncident').text(moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y'));
                $('#qualifiedDateIncident').data('incidentdate', moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD'));
                $('#qualifiedDateIncident').data('incidenttime', moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('kk:mm'));
                var durationVal = parseFloat(qualifiedObj.details.incident_details.duration) / 60;
                incidentDetails = '<div class="col-12 col-md-5">' +
                    '<p class = "font-weight-bold mb-0" > Shift: </p> ' +
                    '<p class = "" id="qualifiedShift" >' + qualifiedObj.details.shift + '</p>' +
                    '</div> ' +
                    '<div class = "col-12 col-md-4" >' +
                    '<p class = "font-weight-bold mb-0">' + qualifiedObj.details.incident_details.log_label + ':</p>' +
                    '<p class = "" >' + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + '</p>' +
                    '</div>' +
                    '<div class = "col-12 col-md-3" >' +
                    '<p class = "font-weight-bold mb-0"> Duration: </p> ' +
                    '<p class = "">' + qualifiedObj.details.incident_details.duration + '</p> ' +
                    '</div>';
            }

            incidentDetails += '<div class="col-12 pl-3 pr-3">' +
                '<div class="col-md-12 ahr_quote pt-3 pb-3 text-center">' +
                '<blockquote style="margin: 0 0 0rem;">' +
                '<i class="fa fa-quote-left"></i>' +
                '<span class="mb-0 mr-1 ml-1" id="narrativeIncidentDetails">' + narrativeDetails + '</span>' +
                '<i class="fa fa-quote-right"></i>' +
                '</blockquote>' +
                '</div>' +
                '</div>';

            $('#incidentDetails').html(incidentDetails + reasonVal);
            $('#incidentDetails').data('narrative', narrativeDetails);

            // Rule
            // Check if previous occurence exist
            if (parseInt(qualifiedObj.details.with_prev) == 1) {
                var withPreviousDetails = "";
                var withIrHistoryDetails = "";
                var previousDetailsHeader = "";
                var previousDetailsBody = "";
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) {
                    previousDetailsHeader = '<th class="text-center">Date</th>' +
                        '<th class="text-center">Shift</th>' +
                        '<th class="text-center">Allowable</th>' +
                        '<th class="text-center">Covered</th>' +
                        '<th class="text-center">Over Break</th>';
                    $.each(qualifiedObj.details.previous_details, function (index, value) {
                        previousDetailsBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.date, 'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                            '<td class="text-center">' + value.shift + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.allowable + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.ob + '</td>' +
                            '</tr>';
                    });
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1 || parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                    var durationLabel = "Late";
                    if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                        durationLabel = "Undertime"
                    }
                    previousDetailsHeader = '<th class="text-center">Date</th>' +
                        '<th class="text-center">Shift</th>' +
                        '<th class="text-center">Clock In</th>' +
                        '<th class="text-center">' + durationLabel + '</th>';
                    $.each(qualifiedObj.details.previous_details, function (index, value) {
                        previousDetailsBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.date, 'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                            '<td class="text-center">' + value.shift + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A') + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.formatted_duration + '</td>' +
                            '</tr>';
                    });
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6 || parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7) {
                    previousDetailsHeader = '<th class="text-center">Date</th>' +
                        '<th class="text-center">Shift</th>' +
                        '<th class="text-center"> Note</th>';
                    $.each(qualifiedObj.details.previous_details, function (index, value) {
                        previousDetailsBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.date, 'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                            '<td class="text-center">' + value.shift + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.note + '</td>' +
                            '</tr>';
                    });
                }
                withPreviousDetails = '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">' +
                    '<thead style="background: #555;color: white;">' +
                    '<tr>' + previousDetailsHeader +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' + previousDetailsBody +
                    '</tbody>' +
                    '</table>';
                $('#previousDetails').html(withPreviousDetails);
                $('#withPreviousUserDtrViolation').show();
            } else {
                $('#withPreviousUserDtrViolation').hide();
            }
            // Check if IR history exist
            if (parseInt(qualifiedObj.details.with_history)) {
                var withIrHistoryBody = "";
                $.each(qualifiedObj.details.ir_history, function (index, value) {
                    var cureDateVal = "N/A";
                    if (value.label !== 't') {
                        cureDateVal = moment(value.prescriptionEnd, 'Y-MM-DD').format('MMMM DD, Y');
                    }
                    withIrHistoryBody += '<tr>' +
                        '<td class="text-center">' + value.incidentReport_ID.padLeft(8) + '</td>' +
                        '<td class="text-center">' + moment(value.datedLiabilityStat, 'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                        '<td class="text-center">' + value.level + '</td>' +
                        '<td class="text-center">' + value.action + '</td>' +
                        '<td class="text-center">' + cureDateVal + '</td>' +
                        '</tr>';
                });
                withIrHistoryDetails = '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">' +
                    '<thead style="background: #555;color: white;">' +
                    '<tr>' +
                    '<th class="text-center">IR-ID</th>' +
                    '<th class="text-center">Dated As Liable</th>' +
                    '<th class="text-center">Level</th>' +
                    '<th class="text-center">Disciplinary Action</th>' +
                    '<th class="text-center">Cure Date</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' + withIrHistoryBody +
                    '</tbody>' +
                    '</table>'
                $('#irHistoryDetails').html(withIrHistoryDetails);
                // $('.withRule').hide();
                $('#withIrHistory').show();
            }

            if (parseInt(qualifiedObj.details.with_rule)) {
                $('#qualifiedRule').html(qualifiedObj.details.ir_rule.rule);
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) != 5) {
                    $('.withRule').show();
                } else {
                    $('.withRule').hide();
                }
                // $('#withIrHistory').hide();
            }

            $('#qualifiedExpectedAction').text(expectedAction);
            $('#qualifiedExpectedAction').data('expectedaction', expectedAction);
            // Offense Details
            $('#qualifiedOffenseType').text(qualifiedObj.details.offense_details.offense_type);
            $('#qualifiedOffense').text(qualifiedObj.details.offense_details.offense_num + '. ' + qualifiedObj.details.offense_details.offense);
            $('#qualifiedOffense').data('offense', qualifiedObj.details.offense_details.offense);
            $('#qualifiedOffense').data('offenseid', qualifiedObj.details.offense_details.offense_id);
            $('#qualifiedOffense').data('discactioncategid', qualifiedObj.details.action_details.discActionCateg_ID);
            $('#qualifiedOffense').data('finaldiscactioncategid', qualifiedObj.details.action_details.discActionCateg_ID);
            $('#qualifiedOffense').data('discactioncategsettingsid', qualifiedObj.details.action_details.discActionCategSettings_ID);
            $('#qualifiedOffense').data('prescriptiveid', qualifiedObj.details.prescriptive_id);
            $('#qualifiedOffense').data('occurence', qualifiedObj.details.occurence);
            $('#qualifiedCategory').text(qualifiedObj.details.action_details.category);
            $('#qualifiedLevel').text(qualifiedObj.details.action_details.level + " Level");
            $('#qualifiedDisciplinaryAction').text(qualifiedObj.details.action_details.action.toUpperCase());
            var qualifiedPeriodUnit = qualifiedObj.details.action_details.unit_to_follow;
            if (parseInt(qualifiedObj.details.action_details.period_to_follow) > 1) {
                qualifiedPeriodUnit += 's';
            }
            $('#qualifiedPeriod').text(qualifiedObj.details.action_details.period_to_follow + " " + toTitleCase(qualifiedPeriodUnit));
            $('#qualifiedPeriod').data('periodnum', qualifiedObj.details.action_details.period_to_follow);
            $('#qualifiedPeriod').data('periodunit', qualifiedObj.details.action_details.unit_to_follow);
            $('#qualifiedCureDate').text(moment(qualifiedObj.details.action_details.cure_date, 'Y-MM-DD').format('MMMM DD, Y'));
            $('#qualifiedCureDate').data('curedate', qualifiedObj.details.action_details.cure_date);
            $('#qualifiedModal').modal('show');
        } else {

        }
    });
}

function initDismissQualifiedDtrView(qualifiedIdVal) {
    $.when(fetchPostData({
        qualifiedViolationId: qualifiedIdVal
    }, '/discipline/dismissed_qualified_dtr_details_view')).then(function (qualified) {
        let qualifiedObj = $.parseJSON(qualified.trim());
        let incidentDetails = "";
        let excess = "";
        let expectedActionDetails = "";
        let allowableTable = "";
        let coveredTable = "";
        let missingLogsDetails = "";
        let missingLogs = "";
        let expectedAction = "";
        let narrativeDetails = ""
        let pronoun = "";
        let allowableTableBody = "";
        let coveredTableBody = "";
        let shiftTime = qualifiedObj.details.shift.split('-');
        let reasonVal = "";
        console.log(qualifiedObj);
        // console.log(dtrViolationContent);
        $('#qualifiedDetailsModalMonitor').data('qualifiedid', qualifiedIdVal);
        $('#empPicApprovalMonitor').attr('src', baseUrl + "/" + qualifiedObj.details.emp_details.pic);
        $('#qualifiedViewEmployeeNameMonitor').text(toTitleCase(qualifiedObj.details.emp_details.fname + " " +qualifiedObj.details.emp_details.lname));
        $('#qualifiedViewEmployeeNameMonitor').data('empid', qualifiedObj.details.emp_details.emp_id);
        $('#qualifiedViewEmployeeJobMonitor').text(qualifiedObj.details.emp_details.positionDescription);
        $('#qualifiedViewPosEmpStatMonitor').text(" -" + qualifiedObj.details.emp_details.empStatus);
        $('#qualifiedViewPosEmpStatMonitor').data('posEmpStat', qualifiedObj.details.emp_details.empStatus);
        $('#qualifiedViewEmployeeJobMonitor').data('position', qualifiedObj.details.emp_details.positionDescription);
        $('#qualifiedViewEmployeeAccountMonitor').text(toTitleCase(qualifiedObj.details.emp_details.accountDescription));
        $('#qualifiedViewEmployeeAccountMonitor').data('account', toTitleCase(qualifiedObj.details.emp_details.accountDescription));
        // Date and Incidents
        // $('#qualifiedDateIncident').text(moment(qualifiedObj.details.sched_date, 'Y-MM-DD').format('MMM DD, Y'));
        // $('#qualifiedDateIncident').data('incidentDate', qualifiedObj.details.sched_date);
        $('#qualifiedViewIncidentTypeMonitor').text(toTitleCase(qualifiedObj.details.violation_type));
        // $('#qualifiedDetailsModal').modal('show');
        if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) { // OB
            $('#qualifiedViewDateIncidentMonitor').text(moment(qualifiedObj.details.dated_shift['shift_end'],
                'Y-MM-DD HH:mm').format('MMM DD, Y'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentDate', moment(qualifiedObj.details.dated_shift[
                'shift_end'], 'Y-MM-DD HH:mm').format('Y-MM-DD'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentTime', moment(qualifiedObj.details.dated_shift[
                'shift_end'], 'Y-MM-DD HH:mm').format('kk:mm'));
            expectedAction = "Should have not exceeded from the " + qualifiedObj.details.incident_details
                .total_allowable + " allowable break duration";
            narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details
                    .emp_details.lname) + " has consumed a total of " + qualifiedObj.details.incident_details
                .total_covered + " break duration, which is " + qualifiedObj.details.incident_details.excess +
                " more than " + pronoun + " allowable break of " + qualifiedObj.details.incident_details
                .total_allowable + ".";
            incidentDetails = '<div class="col-12">' +
                '<span class="font-weight-bold mr-2">Shift:</span>' +
                '<span class="">' + qualifiedObj.details.shift + '</span>' +
                '</div>' +
                '<div class="col-12 mt-2 font-weight-bold">Break Details:</div>';
            excess = '<div class="col-12 mb-2">' +
                '<span class="font-weight-bold mr-2">Over Break Duration:</span>' +
                '<span class="">' + qualifiedObj.details.incident_details.excess + '</span>' +
                '</div>';

            if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                $.each(qualifiedObj.details.incident_details.break, function (index, value) {
                    allowableTableBody += '<tr>' +
                        '<td class="text-center" style="font-weight:400">' + value.type + '</td>' +
                        '<td class="text-center">' + value.allowable + '</td>' +
                        '</tr>';
                    coveredTableBody += '<tr>' +
                        '<td class="text-center">' + moment(value.in, 'Y-MM-DD HH:mm:ss').format(
                            'MMM DD, Y HH:mm:ss A') + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.out,
                            'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A') + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                        '</tr>';
                })
                allowableTable = '<div class="col-12 col-md-4">' +
                    '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                    '<thead style="background: #555;color: white;">' +
                    '<tr>' +
                    '<th class="text-center">Type</th>' +
                    '<th class="text-center">Allowable</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    allowableTableBody +
                    '</tbody>' +
                    '</table>' +
                    '</div>';
                coveredTable = '<div class="col-12 col-md-8 pl-0">' +
                    '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                    '<thead style="background: #555;color: white;">' +
                    '<tr>' +
                    '<th class="text-center">Out for Break</th>' +
                    '<th class="text-center">Back from Break</th>' +
                    '<th class="text-center">Covered</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    coveredTableBody +
                    '</tbody>' +
                    '</table>' +
                    '</div>';
                incidentDetails += allowableTable + "" + coveredTable + "" + excess + "" +
                    expectedActionDetails;
            } else {
                // alert error
            }
        } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 5) { // ABSENT
            $('#qualifiedViewDateIncidentMonitor').text(moment(qualifiedObj.details.dated_shift['shift_start'],
                'Y-MM-DD HH:mm').format('MMM DD, Y'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentDate', moment(qualifiedObj.details.dated_shift[
                'shift_start'], 'Y-MM-DD HH:mm').format('Y-MM-DD'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentTime', moment(qualifiedObj.details.dated_shift[
                'shift_start'], 'Y-MM-DD HH:mm').format('kk:mm'));
            $('#withRule').hide();
            expectedAction =
                "Should have taken all necessary DTR transaction that proves you are present at work.";
            narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details
                    .emp_details.lname) + " was determinded to have no DTR Log transactions in " + pronoun +
                " " + qualifiedObj.details.shift + " shift last " + moment(qualifiedObj.details.sched_date,
                    'Y-MM-DD').format('MMM DD, Y') + ", which proves " + pronoun +" absence on the said schedule date.";
            incidentDetails = '<div class="col-12 col-md-6">' +
                '<span class="font-weight-bold mr-2">Shift:</span>' +
                '<span class="">' + qualifiedObj.details.shift + '</span>' +
                '</div>' +
                '<div class="col-12 col-md-6 text-md-right mb-2">' +
                '<span class="font-weight-bold mr-2">Logs:</span>' +
                '<span class="">No Log Records Found</span>' +
                '</div>';
        } else if ((parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6) || (parseInt(
                qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7)) {
            $('#qualifiedViewDateIncidentMonitor').text(moment(qualifiedObj.details.dated_shift['shift_end'],
                'Y-MM-DD HH:mm').format('MMM DD, Y'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentDate', moment(qualifiedObj.details.dated_shift[
                'shift_end'], 'Y-MM-DD HH:mm').format('Y-MM-DD'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentTime', moment(qualifiedObj.details.dated_shift[
                'shift_end'], 'Y-MM-DD HH:mm').format('kk:mm'));

            expectedAction =
                "Should have taken all the necessary DTR Transactions to complete all the required DTR Logs.";
            narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details
                    .emp_details.lname) + " was determinded to have no logs of the following: " + qualifiedObj
                .details.incident_details.missing.join(", ");
            incidentDetails = '<div class="col-12 col-md-4">' +
                '<span class="font-weight-bold mr-2">Shift:</span>' +
                '<span class="">' + qualifiedObj.details.shift + '</span>' +
                '</div>' +
                '<div class="col-12 col-md-3 text-md-right">' +
                '<span class="font-weight-bold mr-2">Missing Logs:</span>' +
                '</div>';
            if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                $.each(qualifiedObj.details.incident_details.missing, function (index, value) {
                    missingLogs += '<li>' + value + '</li>';
                })
                missingLogsDetails = '<div class="col-12 col-md-5">' +
                    '<ol>' + missingLogs +
                    '</ol>' +
                    '</div>';
                incidentDetails += missingLogsDetails;
            } else {
                // alert error
            }
        } else {
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1) {
                expectedAction = "Should have clocked in at least 30 minutes before " + shiftTime[0];
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj
                        .details.emp_details.lname) + " clocked in by " + moment(qualifiedObj.details
                        .incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj
                        .details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') +
                    "), which is " + qualifiedObj.details.incident_details.duration + " later than " + pronoun +
                    " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_start'],
                        'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift[
                        'shift_start'], 'Y-MM-DD HH:mm').format('MMM DD, Y') + ") shift start.";
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                expectedAction = "Should have not clocked out before " + shiftTime[1] + " to avoid Undertime.";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj
                        .details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details
                        .incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj
                        .details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') +
                    "), which is " + qualifiedObj.details.incident_details.duration + " earlier before " +
                    pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'],
                        'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift[
                        'shift_end'], 'Y-MM-DD HH:mm').format('MMM DD, Y') + ") shift end.";
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 3) {
                expectedAction = "Should have not clocked out beyond " + qualifiedObj.details.incident_details
                    .allowable_logout_formatted + " clock out allowance after" + shiftTime[1];
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj
                        .details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details
                        .incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj
                        .details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') +
                    "), which is " + qualifiedObj.details.incident_details.duration + " past from " + pronoun +
                    " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'],
                        'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift[
                        'shift_end'], 'Y-MM-DD HH:mm').format('MMM DD, Y') +
                    ") shift end exceeding the allowable clock-out allowance of " + qualifiedObj.details
                    .incident_details.allowable_logout_formatted + ".";
                reasonVal = '<div class="col-12 pt-2">' +
                    '<div class="row">' +
                    '<div class="col-12 col-md-2 font-weight-bold">Reason:</div>' +
                    '<div class="col-12 col-md-10 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType"><i class="fa fa-quote-left mr-1" style="font-size: 0.9rem;"></i>' +
                    qualifiedObj.details.reason.explanation +
                    '<i class="fa fa-quote-right ml-1" style="font-size: 0.9rem;"></i></div>' +
                    '</div>' +
                    '</div>';
            }
            $('#qualifiedViewDateIncidentMonitor').text(moment(qualifiedObj.details.incident_details.log,
                'Y-MM-DD HH:mm:ss').format('MMM DD, Y'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentDate', moment(qualifiedObj.details.incident_details
                .log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentTime', moment(qualifiedObj.details.incident_details
                .log, 'Y-MM-DD HH:mm:ss').format('kk:mm'));
            var durationVal = parseFloat(qualifiedObj.details.incident_details.duration) / 60;
            incidentDetails = '<div class="col-12 col-md-5">' +
                '<p class = "font-weight-bold mb-0" > Shift: </p> ' +
                '<p class = "" id="qualifiedShift" >' + qualifiedObj.details.shift + '</p>' +
                '</div> ' +
                '<div class = "col-12 col-md-4" >' +
                '<p class = "font-weight-bold mb-0">' + qualifiedObj.details.incident_details.log_label +
                ':</p>' +
                '<p class = "" >' + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss')
                .format('MMM DD, Y h:mm A') + '</p>' +
                '</div>' +
                '<div class = "col-12 col-md-3" >' +
                '<p class = "font-weight-bold mb-0"> Duration: </p> ' +
                '<p class = "">' + qualifiedObj.details.incident_details.duration + '</p> ' +
                '</div>';
        }

        incidentDetails += '<div class="col-12 pl-3 pr-3">' +
            '<div class="col-md-12 ahr_quote pt-3 pb-3 text-center">' +
            '<blockquote style="margin: 0 0 0rem;">' +
            '<i class="fa fa-quote-left"></i>' +
            '<span class="mb-0 mr-1 ml-1" id="narrativeIncidentDetails">' + narrativeDetails + '</span>' +
            '<i class="fa fa-quote-right"></i>' +
            '</blockquote>' +
            '</div>' +
            '</div>';
        $('#incidentDetailsViewMonitor').html(incidentDetails + reasonVal);
        // $('#incidentDetailsView').data('narrative', narrativeDetails);

        // Rule
        // Check if previous occurence exist
        if (parseInt(qualifiedObj.details.with_prev) == 1) {
            var withPreviousDetails = "";
            var withIrHistoryDetails = "";
            var previousDetailsHeader = "";
            var previousDetailsBody = "";
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) {
                previousDetailsHeader = '<th class="text-center">Date</th>' +
                    '<th class="text-center">Shift</th>' +
                    '<th class="text-center">Allowable</th>' +
                    '<th class="text-center">Covered</th>' +
                    '<th class="text-center">Over Break</th>';
                $.each(qualifiedObj.details.previous_details, function (index, value) {
                    previousDetailsBody += '<tr>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.date,
                            'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                        '<td class="text-center">' + value.shift + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.allowable + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.ob + '</td>' +
                        '</tr>';
                });
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1 || parseInt(
                    qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                var durationLabel = "Late";
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                    durationLabel = "Undertime"
                }
                previousDetailsHeader = '<th class="text-center">Date</th>' +
                    '<th class="text-center">Shift</th>' +
                    '<th class="text-center">Clock In</th>' +
                    '<th class="text-center">' + durationLabel + '</th>';
                $.each(qualifiedObj.details.previous_details, function (index, value) {
                    previousDetailsBody += '<tr>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.date,
                            'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                        '<td class="text-center">' + value.shift + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.log,
                            'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A') + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.formatted_duration +
                        '</td>' +
                        '</tr>';
                });
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6 || parseInt(
                    qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7) {
                previousDetailsHeader = '<th class="text-center">Date</th>' +
                    '<th class="text-center">Shift</th>' +
                    '<th class="text-center"> Note</th>';
                $.each(qualifiedObj.details.previous_details, function (index, value) {
                    previousDetailsBody += '<tr>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.date,
                            'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                        '<td class="text-center">' + value.shift + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.note + '</td>' +
                        '</tr>';
                });
            }
            withPreviousDetails =
                '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">' +
                '<thead style="background: #555;color: white;">' +
                '<tr>' + previousDetailsHeader +
                '</tr>' +
                '</thead>' +
                '<tbody>' + previousDetailsBody +
                '</tbody>' +
                '</table>';
            $('#previousDetailsViewMonitor').html(withPreviousDetails);
            $('#withPreviousUserDtrViolationViewMonitor').show();
        } else {
            $('#withPreviousUserDtrViolationViewMonitor').hide();
        }

        $('#qualifiedExpectedActionViewMonitor').text(expectedAction);
        $('#qualifiedExpectedActionViewMonitor').data('expectedAction', expectedAction);
        $('#directSupPicMonitor').attr('src', baseUrl + "/" + qualifiedObj.details.sup_details.pic)
        $('#directSupNameMonitor').text(toTitleCase(qualifiedObj.details.sup_details.fname + " " + qualifiedObj.details
            .sup_details.lname));
        $('#directSupJobMonitor').text(qualifiedObj.details.sup_details.positionDescription);
        $('#reasonMonitor').text(qualifiedObj.details.dismiss_reason.reason);
        $('#dismissDate').text("Dismissed last "+moment(qualifiedObj.details.dismiss_reason.dateTime, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y hh:mm:ss A'));
        // // Check if IR history exist
        if (parseInt(qualifiedObj.details.with_rule)) {
            $('#qualifiedRuleViewMonitor').html(qualifiedObj.details.ir_rule.rule);
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) != 5) {
                $('#withRuleViewMonitor').show();
            } else {
                $('#withRuleViewMonitor').hide();
            }
        }
        $('#qualifiedDetailsModalMonitor').modal('show');
    })
}
// END OF MIC
function exportExcelDismissed(){
    let dateRange = $('#dismissedEmpMonitoringDateRange').val().split('-');
    $.ajax({
        url: baseUrl + "/discipline/export_dismissed_dtr_vio",
        method: 'POST',
        data: {
            emp_id: $('#dismissedEmpMonitoring').val(),
            vio_type_id: $('#violationTypeMonitoring').val(),
            date_start: moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'),
            date_end: moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'),
        },
        beforeSend: function () {
            //  $('#loadingExportModal').modal('show');
        },
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'Dismissed DTR Violations ' + moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD') + '-' + moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD')+'.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
        },
    });
}

$('#downloadDismissed').on('click', function(){
    exportExcelDismissed();
})

$('#irableDtrViolationMonitoring').on('click', '.qualifiedDtrViolation', function () {
    initQualifiedModal(this);
})

$('#irableDtrViolationRecordMonitoring').on('click', '.viewQualifiedDtrRecord', function () {
    initQualifiedDtrView($(this).data('qualifiedid'), 0);
})

$('#dismissedQualifiedDtrViolations').on('click', '.dismissedQualifiedDtrRecord', function () {
    initDismissQualifiedDtrView($(this).data('qualifiedid'));
    // initDismissQualifiedDtrView($(this).data('qualifiedid'));
    // $('#qualifiedDetailsModalMonitor').modal('show');
})

$('.dmsMonitoringMainNav').on('click', function (event) {
    $(".m-nav--inline").find(".active").removeClass("active");
    $(this).parent().addClass("active");
    $('.m-portlet .active').find(".active").removeClass("active");
    $('.m-portlet').hide();
    $('#' + $(this).data('navcontent') + '').addClass("active");
    if ($(this).data('navcontent') !== 'monitoringIrProfile') {
        $('#monitoringIrProfile').hide();
        $('#showIrPofileRecords').hide();
        $('#empIrProfileSelect').hide();
        $('#offenseHistory').hide();
    }
    $('#' + $(this).data('navcontent') + '').fadeIn();
    $(this).addClass("active");
})

$('#fileDtrViolationIrMonitoringTab').on('click', function () {
    initDatePicker('#dtrViolationSchedMonitoring', true, function () {
        get_emp_dtr_vio_ir_select_monitoring(function (notEmpty) {
            initIrAbleDtrViolationMonitoring();
        });
    });
});

$('#fileDtrViolationIrRecordMonitoringTab').on('click', function () {
    initIrAbleDtrViolationMonitoringRecordDateRangePicker(function (dateRanges) {
        var dateRange = $('#fileDtrViolationMonitoringRecordDateRange').val().split('-');
        initIrAbleDtrViolationRecordMonitoring(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
    });
});

$('#dismissedDtrViolationsTab').on('click', function () {
    initDismissedDtrViolationDateRangePicker(function (dateRanges) {
        console.log(dateRanges);
        var dateRange = $('#dismissedEmpMonitoringDateRange').val().split('-');
        initDismissedDtrViolationRecord(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
    });
});

$('#fileDtrViolationEmpSelectMonitoring').on('change', function () {
    initIrAbleDtrViolationMonitoring();
});

$('#fileDtrViolationEmpMonitoringRecordSelect').on('change', function () {
    console.log('change record emp')
    var dateRange = $('#fileDtrViolationMonitoringRecordDateRange').val().split('-');
    initIrAbleDtrViolationRecordMonitoringDatatable(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), 'destroy', function (notEmpty) {});
});

$('#dismissedEmpMonitoring, #violationTypeMonitoringDismissed').on('change', function () {
    var dateRange = $('#dismissedEmpMonitoringDateRange').val().split('-');
    initDismissedDtrViolationDatatable(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), 'destroy', function (notEmpty) {});
})

$('#violationTypeMonitoring, #dtrViolationSchedMonitoring, #fileDtrViolationMonitoringStatus').on('change', function () {
    get_emp_dtr_vio_ir_select_monitoring(function (notEmpty) {
        initIrAbleDtrViolationMonitoring();
    });
});

$('#violationTypeMonitoringRecord').on('change', function () {
    var dateRange = $('#fileDtrViolationMonitoringRecordDateRange').val().split('-');
    initIrAbleDtrViolationRecordMonitoring(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
});

$('#monitoringIrProfileContent').on('click', function () {
     initEmployeeIrProfile(1, function (exist) {
         if (parseInt(exist)){
            initEmployeeProfile();
            $('#monitoringIrProfile').fadeIn();
         }else{
            $('#monitoringIrProfile').hide();
         }
     });
});

$('#qualifiedModal').on('hidden.bs.modal', function () {
    $('#withIrHistory').hide();
    $('.withRule').hide();
});

$(function () {
    initfileDtrViolationIrBadgeMonitoring(function (countObj) {
        // initDtrViolationBadgeMonitoring();
    });
})