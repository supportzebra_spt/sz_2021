// on load

$('#memoform')
    .find('[name="memodate"]')
    .change(function (e) {
        $('#memoform').formValidation('revalidateField', 'memodate');
    })
    .end()
    .formValidation({
        message: 'This value is not valid',
        excluded: ':disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            memodate: {
                validators: {
                    notEmpty: {
                        message: 'Memo Date is Required!'
                    },
                }
            },
            memotitle: {
                validators: {
                    notEmpty: {
                        message: 'Memo Title is Required!'
                    },
                }
            },
            memonumber: {
                validators: {
                    notEmpty: {
                        message: 'Memo Number is Required!'
                    },
                }
            },
            upload_memo: {
                validators: {
                    notEmpty: {
                        message: 'Memo File is Required!'
                    },
                }
            },
        }
    }).on('success.form.fv', function (e, data) {
        e.preventDefault();
        $('#specialIRForm').formValidation('disableSubmitButtons', true);
        var frm = new FormData(this);
        var uploadval = $("#upload_memo").val();
        console.log("vallllue" + uploadval);
        if (uploadval == "") {
            swal("No File Selected!", "You cannot submit memo without proper memo file type attached.", "warning");
        } else {
            swal({
                title: 'Are you sure you want to submit this Memo?',
                text: 'Please review and double check all details.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Submit Memo'
            }).then(function (result) {
                if (result.value) {
                    frm.append("memotitle", $("#memotitle").val());
                    frm.append("memonumber", $("#memonumber").val());
                    frm.append("memodate", $("#memodate").val());
                    $.ajax({
                        type: "POST",
                        url: baseUrl + "/discipline/save_memo",
                        data: frm,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (res) {
                            // var result = JSON.parse(res);
                            swal({
                                position: 'top-center',
                                type: 'success',
                                title: 'Successfully submitted a Memo',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $("#modal_addmemo").modal('hide');
                            initMemo();
                        },
                        error: function (res) {
                            swal(
                                'Oops!',
                                'Something is wrong please contact adminstrator!',
                                'error'
                            )
                        }
                    });
                }
            });

        }
    });

$(function () {
    initMemo();
});
// event

$('#searchMemoid').on('keyup', function () {
    initMemo();
})

$('#date_search').on('change', function () {
    initMemo();
})

$("#upload_memo").on('change', function (e) {
    // var maxsize = 5000000;
    // var exceed = 0;
    var file = $(this).val();
    var input = document.getElementById("upload_memo");
    var file_size = input.files[0].size;
    var file_ext = file.split('.').pop();
    file = (file.length > 0) ? file : "fakepath";
    var name = file.split('fakepath');

    if (file_ext == "pdf") {
        $("#error_label").html(name[1].slice(1));
        $("#error_label").removeClass("not_included");
    } else {
        $("#error_label").text("Invalid File Type. Please Upload different file.");
        $("#error_label").addClass("not_included");
        $(this).val('')
        $('#memoform').formValidation('disableSubmitButtons', true);
    }
});

// Modal form dismiss 
$('#modal_addmemo').on('hidden.bs.modal', function () {
    formReset('#memoform');
    $("#error_label").text("");
});

var recordmemoDatatable = function () {
    var memo = function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/list_memo_datatable",
                        params: {
                            query: {
                                search_memoid: $('#searchMemoid').val(),
                                search_date: $('#date_search').val(),
                                // startDate: start,
                                // endDate: end,
                                // empId: $('#empSelectPulseRecord').val(),
                                // statId: $('#ongoingStatus').val()
                            },
                        },
                        // map: function (raw) {
                        //     // sample data mapping
                        //     var dataSet = raw;
                        //     if (typeof raw.data !== 'undefined') {
                        //         dataSet = raw.data;
                        //     }
                        //     if (dataSet.length == 0) {
                        //         $('#downloadDailyPulseReportBtn').fadeOut();
                        //     } else {
                        //         $('#downloadDailyPulseReportBtn').fadeIn();
                        //     }
                        //     // $('#pendingCount').text(dataSet.length);
                        //     return dataSet;
                        // },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#reportSearch'),
            },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "number",
                title: "Memo Number",
                width: 100,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.number;
                    return html;
                },
            },
            {
                field: "title",
                title: "Title",
                width: 110,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.title;
                    return html;
                },
            }, {
                field: "memoDate",
                title: "Effectivity Date",
                width: 100,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.memoDate, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            }, {
                field: "dateUploaded",
                title: "Date Uploaded",
                width: 100,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate2 = moment(row.dateUploaded, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate2;
                    return html;
                },
            }, {
                field: "dmsMemo_ID",
                title: "File",
                width: 30,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html +='<a href="#" class="pdfclass'+row.dmsMemo_ID+' text-danger" id="pdfid' + row.dmsMemo_ID + '" data-pdflink="' + row.link + '" onclick="event.preventDefault();getpdf(' + row.dmsMemo_ID +')" data-target="#pdfmodal" data-toggle="modal" ><i class="fa fa-file-pdf-o" style="font-size:x-large"></i></a>';
                    return html;
                },
            }, {
                field: "",
                title: "Action",
                width: 50,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    html += '<button onclick="removememo('+row.dmsMemo_ID+')" type="button" class="btn btn-sm btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air"><i class="fa fa-trash"></i></button>';
                    // html +='<a href="#" onclick="removememo('+row.dmsMemo_ID+')"><i class="fa fa-trash" style="color:#f4516c;font-size:x-large"></i></a>';
                    return html;
                },
            }],
        };
        var datatable = $('#memoDatatable').mDatatable(options);
    };
    return {
        init: function () {
            memo();
        }
    };
}();

// function
function fetchdatepicker_memo() {
    $("#memodate").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayBtn: true
    });
}
function fetchdatepicker2() {
    $("#date_search").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayBtn: true
    });
}
function initMemo() {
    $('#memoDatatable').mDatatable('destroy');
    recordmemoDatatable.init();
}
function getpdf(memoid) {
    var pdflink = $(".pdfclass"+memoid).data('pdflink');
    var options = {
        pdfOpenParams: {
            navpanes: 0,
            toolbar: 0,
            statusbar: 0,
            pagemode: 'thumbs',
            view: "FitH"
        }
    };
    var myPDF = PDFObject.embed(baseUrl + "/"+pdflink, "#pdf_display", options);
    console.log(baseUrl + "/"+pdflink);
    console.log((myPDF) ? "PDFObject successfully embedded" : "Uh-oh, the embed didn't work.");
}
function removememo(memoid){
    swal({
        title: 'Are you sure you want to remove this memo?',
        text: "This will not be reverted once removed.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!'
      }).then(function(result) {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: baseUrl +"/discipline/remove_memo",
            data: {
                memo_id: memoid
            },
            cache: false,
            success: function () {
             swal(
              'Deleted!',
              'Memo successfully removed.',
              'success'
              )
              initMemo();
            //   fetch_media_evidences(irid);
            },
            error: function (res){
              swal(
                'Oops!',
                'Something is wrong please contact adminstrator!',
                'error'
                )
            }
          });
        }else if (result.dismiss === 'cancel') {
          swal(
            'Cancelled',
            'You cancelled the removing of memo',
            'error'
            )
        }
      });
}


