function noImageFound(element) {
    $(element).attr('src', baseUrl + '/assets/images/img/sz.png');
}

var offenseDatatable = function () {
    var offense = function (offenseCategoryIdVal, searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_offenses_datatable",
                        params: {
                            query: {
                                offenseCategoryId: offenseCategoryIdVal,
                                offenseSearchField: searchVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#offenseSearchField'),
            },
            rows: {
                afterTemplate: function (row, data, index) {
                    $('th[data-field="offense"]').removeClass('m-datatable__cell--left');
                    $('th[data-field="offense"]').addClass('m-datatable__cell--center');
                    // $('.popoverTest').popover({
                    //     title: "Category ",
                    //     html: true,
                    //     container: 'body',
                    //     animation: true,
                    //     content:"test",
                    //     trigger: 'focus',
                    // })
                },
            },
            // columns definition
            columns: [{
                field: "orderNum",
                title: "#",
                width: 70,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.orderNum+".";
                    return html;
                },
            }, {
                field: 'offense',
                width: 320,
                title: 'Offense Description',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
            }, 
            {
                field: 'multiCategoryLetters',
                width: 90,
                title: 'Category',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    // var html = row.category.toUpperCase();
                    var categoryValue = "";
                    if ((row.multiCategoryLetters == 'all') || (row.multiCategoryLetters == '0')){
                        categoryValue = "<span style='font-size:10px'>Depends on the gravity of the Offense</span>";
                    }else{
                        var multiCategoryLettersArr =  row.multiCategoryLetters.split(',');
                        if (multiCategoryLettersArr.length > 1) {
                            var categString = row.multiCategoryLetters.toString()
                            // array.replace(/,/g, ", ")
                            categoryValue = categString.replace(/,/g, ", ").toUpperCase() + " <span style='font-size:10px'>(depending on the gravity of the Offense)</span>";
                        }else{
                            categoryValue = row.multiCategoryLetters.toString().toUpperCase();
                        }
                    }
                    return categoryValue;
                },
            },
             {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2 popoverTest" style="width: 22px !important;height: 22px !important;" data-offenseid="' + row.offense_ID + '" onclick="getSpecificOffense(this)">' +
                        '<i class="fa fa-edit" style="font-size: 0.95rem !important;"></i>' +
                        '</a>' +
                        '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-offense="' + row.offense + '" data-offenseid="' + row.offense_ID + '" data-offenseCategory="' + row.offenseType_ID + '" onclick="confirmDeleteOffense(this)">' +
                        '<i class="fa fa-remove" style="font-size: 0.95rem !important;"></i>' +
                        '</a>' ;
                    return html;
                },
            }],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
        init: function (offenseCategoryIdVal, searchVal) {
            offense(offenseCategoryIdVal, searchVal);
        }
    };
}();

var qipAssignDatatable = function () {
    var qipAssign = function (searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_qip_assignment_datatable",
                        params: {
                            query: {
                                qipSearchField: searchVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            // if(parseInt(dataCount) > 0){
                            //     $('#noAssignedQip').hide();
                            //     $('#showQipDatatable').fadeIn();
                            // }else{
                            //     $('#showQipDatatable').hide();
                            //     $('#noAssignedQip').fadeIn();
                            // }
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#qipSearchField'),
            },
            rows: {
                afterTemplate: function (row, data, index) {
                    // $('th[data-field="offense"]').removeClass('m-datatable__cell--left');
                    // $('th[data-field="offense"]').addClass('m-datatable__cell--center');
                    // $('.popoverTest').popover({
                    //     title: "Category ",
                    //     html: true,
                    //     container: 'body',
                    //     animation: true,
                    //     content:"test",
                    //     trigger: 'focus',
                    // })
                },
            },
            // columns definition
            columns: [{
                    field: "fname",
                    title: "Employee Name",
                    width: 220,
                    selector: false,
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = toTitleCase(row.fname + " " + row.lname);
                        return html;
                    },
                }, 
                {
                    field: 'action',
                    width: 60,
                    title: 'Action',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-fname="' + row.fname + '" data-lname="' + row.lname + '" data-qipid="' + row.qipAssignment_ID + '" onclick="confirmRemoveQip(this)">' +
                            '<i class="fa fa-close" style="font-size: 0.95rem !important;"></i>' +
                            '</a>';
                        return html;
                    },
                }
            ],
        };
        var datatable = $('#qipDatatable').mDatatable(options);
    };
    return {
        init: function (searchVal) {
            qipAssign(searchVal);
        }
    };
}();

var eroAssignDatatable = function () {
    var eroAssign = function (searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_ero_assignment_datatable",
                        params: {
                            query: {
                                eroSearchField: searchVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            // if (parseInt(dataCount) > 0) {
                            //     $('#noAssignedEro').hide();
                            //     $('#showEroDatatable').fadeIn();
                            // } else {
                            //     $('#showEroDatatable').hide();
                            //     $('#noAssignedEro').fadeIn();
                            // }
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#eroSearchField'),
            },
            rows: {
                afterTemplate: function (row, data, index) {
                },
            },
            // columns definition
            columns: [{
                    field: "emp_id",
                    title: "Employee Name",
                    width: 160,
                    selector: false,
                    sortable: 'asc',
                    textAlign: 'left',
                    template: function (row, index, datatable) {
                        var html = toTitleCase(row.appEroFname + " " + row.appEroLname);
                        return html;
                    },
                }, {
                    field: "accounts",
                        title: "Accounts",
                        width: 70,
                        selector: false,
                        sortable: 'asc',
                        textAlign: 'center',
                        template: function (row, index, datatable) {
                            var html = row.acc_id;
                            return html;
                        }
                }, {
                    field: "appEroSupFname",
                        title: "DMS Supervisor",
                        width: 160,
                        selector: false,
                        sortable: 'asc',
                        textAlign: 'center',
                        template: function (row, index, datatable) {
                            var html = toTitleCase(row.appEroSupFname + " " + row.appEroSupLname);
                            return html;
                        }
                }, {
                    field: 'action',
                    width: 60,
                    title: 'Action',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = '<a class="btn btn-outline-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2" style="width: 22px !important;height: 22px !important;" data-empid="' + row.emp_id + '" data-fname="' + row.appEroFname + '" data-lname="' + row.appEroLname + '" data-eroid="' + row.employeeRelationsOfficer_ID + '" onclick="initEroAccountsModal(this)">' +
                            '<i class="fa fa-search" style="font-size: 0.95rem !important;"></i>' +
                            '</a><a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-fname="' + row.appEroFname + '" data-lname="' + row.appEroLname + '" data-empid="' + row.emp_id + '" onclick="confirmRemoveEro(this)">' +
                            '<i class="fa fa-close" style="font-size: 0.95rem !important;"></i>' +
                            '</a>';
                        return html;
                    },
                }
            ],
        };
        var datatable = $('#eroDatatable').mDatatable(options);
    };
    return {
        init: function (searchVal) {
            eroAssign(searchVal);
        }
    };
}();

var eroAccountDatatable = function () {
    var eroAccount = function (searchVal, classVal, siteVal, empIdVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_ero_account_datatable",
                        params: {
                            query: {
                                eroAccountSearchField: searchVal,
                                class: classVal,
                                siteId: siteVal,
                                empId: empIdVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#eroAccountSearchField'),
            },
            rows: {
                afterTemplate: function (row, data, index) {
                },
            },
            // columns definition
            columns: [{
                field: "acc_name",
                title: "Account",
                width: 190,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.acc_name);
                    return html;
                },
            }, {
                field: "code",
                title: "SITE",
                width: 60,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.code;
                    return html;
                },
            }, {
                field: 'action',
                width: 50,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-eroid="' + row.employeeRelationsOfficer_ID + '" data-accname="'+row.acc_name+'" onclick="confirmRemoveEroAccount(this)">' +
                        '<i class="fa fa-remove" style="font-size: 0.95rem !important;"></i>' +
                        '</a>';
                    return html;
                },
            }],
        };
        var datatable = $('#eroAccountDatatable').mDatatable(options);
    };
    return {
        init: function (searchVal, classVal, siteVal, empIdVa) {
            eroAccount(searchVal, classVal, siteVal, empIdVa);
        }
    };
}();

var offenseTypeAssignDatatable = function () {
    var offenseTypeAssign = function (accIdVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_offense_type_assignment_datatable",
                        params: {
                            query: {
                                accId: accIdVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            rows: {
                afterTemplate: function (row, data, index) {
                },
            },
            // columns definition
            columns: [{
                field: "acc_name",
                title: "Account",
                width: 300,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.acc_name);
                    return html;
                },
            }, {
                field: "offenseTypeCount",
                title: "Offense Type",
                width: 120,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.offenseTypeCount;
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-outline-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2" style="width: 22px !important;height: 22px !important;" data-accname="'+row.acc_name+'" data-accid="' + row.accId + '" onclick="initAssignedOffenseTypeModal(this)">' +
                        '<i class="fa fa-search" style="font-size: 0.95rem !important;"></i>' +
                        '</a><a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-accname="'+row.acc_name+'" data-accid="' + row.accId + '" onclick="confirmRemoveAssignedOffenseTypeByAccId(this)">' +
                        '<i class="fa fa-close" style="font-size: 0.95rem !important;"></i>' +
                        '</a>';
                    return html;
                },
            }],
        };
        var datatable = $('#offenseTypeAssDatatable').mDatatable(options);
    };
    return {
        init: function (accIdVal) {
            offenseTypeAssign(accIdVal);
        }
    };
}();

var recommendationNumberDatatable = function () {
    var recommendationNumber = function (accIdVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_account_recommendation_number_datatable",
                        params: {
                            query: {
                                accId: accIdVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            rows: {
                afterTemplate: function (row, data, index) {
                },
            },
            // columns definition
            columns: [{
                field: "acc_name",
                title: "Account",
                width: 300,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.acc_name);
                    return html;
                },
            }, {
                field: "number",
                title: "Levels",
                width: 120,
                selector: false,
                sortable: 'asc',
                textAlign: 'center'
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2" style="width: 22px !important;height: 22px !important;" data-accname="' + row.acc_name + '" data-accid="' + row.acc_id + '" data-recommendationnumid="'+row.accountRecommendationNumber_ID+'" onclick="getSpecificRecommendationNum(this)">' +
                        '<i class="fa fa-edit" style="font-size: 0.95rem !important;"></i></a>' +
                        '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-accname="' + row.acc_name + '" data-accid="' + row.acc_id + '" data-recommendationnumid="'+row.accountRecommendationNumber_ID+'" onclick="confirmRemoveRecommendationNum(this)">' +
                        '<i class="fa fa-close" style="font-size: 0.95rem !important;"></i>' +
                        '</a>';
                    return html;
                },
            }],
        };
        var datatable = $('#customRecommendationNumberDatatable').mDatatable(options);
    };
    return {
        init: function (accIdVal) {
            recommendationNumber(accIdVal);
        }
    };
}();

var customChangesDatatable = function () {
    var customChanges = function (empIdVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_custom_changes_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            rows: {
                afterTemplate: function (row, data, index) {},
            },
            // columns definition
            columns: [{
                field: "emp_id",
                title: "Recommender",
                width: 160,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.fname) + " " + toTitleCase(row.lname);
                    return html;
                },
            }, {
                field: "changeCount",
                title: "Reassignment",
                width: 110,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var badgColor = "m-badge--secondary";
                    onclickVal = "";
                    if (parseInt(row.changeCount) > 0){
                        badgColor = "m-badge--primary"
                        onclickVal = "initCustomChangeModal(this)";
                    }
                    var html = '<span class="m-badge ' + badgColor + ' m-badge--wide" style="font-size:14px" onclick="' + onclickVal + '" data-empid="'+row.emp_id+'" data-type="change">' + row.changeCount + '</span>';
                    return html;
                },
            }, {
                field: "changeExempt",
                title: "Exemption",
                width: 100,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var badgColor = "m-badge--secondary"
                    var onclickVal = "";
                    if (parseInt(row.changeExempt) > 0) {
                        badgColor = "m-badge--primary"
                        onclickVal = "initCustomChangeModalExemption(this)";
                    }
                    var html = '<span class="m-badge ' + badgColor + ' m-badge--wide" style="font-size:14px" onclick="' + onclickVal + '" data-empid="' + row.emp_id + '" data-type="exempt">' + row.changeExempt + '</span>';
                    return html;
                }
            }],
        };
        var datatable = $('#customChangesDatatable').mDatatable(options);
    };
    return {
        init: function (empIdVal) {
            customChanges(empIdVal);
        }
    };
}();

var assignedOffenseTypeDatatable = function () {
    var assignedOffenseType = function (searchVal, accIdVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_assigned_offenses_datatable",
                        params: {
                            query: {
                                assignedOffenseTypeSearch: searchVal,
                                accId: accIdVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            rows: {
                afterTemplate: function (row, data, index) {
                },
            },
            search: {
                input: $('#assignedOffenseTypeSearch'),
            },
            // columns definition
            columns: [{
                field: "letter",
                title: "Letter",
                width: 60,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.letter);
                    return html;
                },
            }, {
                field: "offenseType",
                title: "Offense Type",
                width: 190,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.offenseType;
                    return html;
                },
            }, {
                field: 'action',
                width: 50,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-offtypeletter="' + row.letter + '" data-offtype="' + row.offenseType + '" data-offtypeassid="' + row.offenseTypeAssignment_ID + '" onclick="confirmRemoveAssignedOffenseType(this)">' +
                        '<i class="fa fa-close" style="font-size: 0.95rem !important;"></i>' +
                        '</a>';
                    return html;
                },
            }],
        };
        var datatable = $('#assignedOffenseTypeDatatable').mDatatable(options);
    };
    return {
        init: function (searchVal, accIdVal) {
            assignedOffenseType(searchVal, accIdVal);
        }
    };
}();

var reassignmentDatatable = function () {
    var reassignment = function (accIdVal, empIdVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_reassignment_datatable",
                        params: {
                            query: {
                                accId: accIdVal,
                                empId: empIdVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            rows: {
                afterTemplate: function (row, data, index) {},
            },
            // columns definition
            columns: [{
                field: "acc_name",
                title: "Account",
                width: 120,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.acc_name);
                    return html;
                },
            }, {
                field: "level",
                title: "Level",
                width: 50,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.level;
                    return html;
                },
            }, {
                field: "emp_id",
                title: "Reassign To",
                width: 150,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.fname) + " " + toTitleCase(row.lname);
                    return html;
                },
            }, {
                field: 'action',
                width: 50,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-fullname="' + toTitleCase(row.fname) + " " + toTitleCase(row.lname) + '" data-recid="' + row.dmsCustomRecommendation_ID + '" data-type="change" onclick="confirmRemoveCustomChanges(this)">' +
                        '<i class="fa fa-remove" style="font-size: 0.95rem !important;"></i>' +
                        '</a>';
                    return html;
                },
            }],
        };
        var datatable = $('#reassignmentDatatable').mDatatable(options);
    };
    return {
        init: function (accIdVal, empIdVal) {
            reassignment(accIdVal, empIdVal);
        }
    };
}();

var exemptionDatatable = function () {
    var exemption = function (accIdVal, empIdVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_exemption_datatable",
                        params: {
                            query: {
                                accId: accIdVal,
                                empId: empIdVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            rows: {
                afterTemplate: function (row, data, index) {},
            },
            // columns definition
            columns: [{
                field: "acc_name",
                title: "Account",
                width: 200,
                selector: false,
                sortable: 'asc',
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = toTitleCase(row.acc_name);
                    return html;
                },
            }, {
                field: "level",
                title: "Level",
                width: 60,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.level;
                    return html;
                },
            }, {
                field: 'action',
                width: 50,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-recid="' + row.dmsCustomRecommendation_ID + '" data-type="exempt" onclick="confirmRemoveCustomChanges(this)">' +
                        '<i class="fa fa-remove" style="font-size: 0.95rem !important;"></i>' +
                        '</a>';
                    return html;
                },
            }],
        };
        var datatable = $('#reassignmentDatatable').mDatatable(options);
    };
    return {
        init: function (accIdVal, empIdVal) {
            exemption(accIdVal, empIdVal);
        }
    };
}();
// CREATE FUNCTIONS ---------------------------------------------------------------------------------------
function addDisciplinaryAction() {
    $.when(fetchPostData({
        action: $('#action').val(),
        abbr: $('#abreviation').val(),
        period: $('#prescreptivePeriod').val(),
    }, '/discipline/add_disciplinary_action')).then(function (addActionStat) {
        var addActionStatus = $.parseJSON(addActionStat.trim());
        if (parseInt(addActionStatus)) {
            toastr.success('Successfully Add Disciplinary Action');
            getDisciplinaryAction();
            $('#disciplinaryActionModal').modal('hide');
        } else {
            toastr.error("Something went wrong while adding the Disciplinary Action. Please inform your system administrator");
        }
    });
}

function addCategory(){
     $.when(fetchPostData({
         category: $('#category').val(),
     }, '/discipline/add_category')).then(function (addOffenseCategoryStat) {
         var addCategoryStatus = $.parseJSON(addOffenseCategoryStat.trim());
         if (parseInt(addCategoryStatus)) {
             toastr.success('Successfully Added Category');
             getOffenseCategory();
             $('#categoryModal').modal('hide');
         } else {
             toastr.error("Something went wrong while adding the Category. Please inform your system administrator");
         }
     });
}

function addOffenseLevel(){
     $.when(fetchPostData({
        disciplinaryActionId: $('#offenseDisciplinaryAction').val(),
        categoryId: $('#offenseCategory').data('categoryid'),
     }, '/discipline/add_offense_level')).then(function (addCategoryStat) {
         var addCategoryStatus = $.parseJSON(addCategoryStat.trim());
         if (parseInt(addCategoryStatus)) {
             getOffenseCategory();
             toastr.success('Successfully Added New Offense level for Category ' + $('#offenseCategory').data('category'));
             $('#offenseLevelModal').modal('hide');
         } else {
             toastr.error("Something went wrong while adding the Offense level. Please inform your system administrator");
         }
     });
}

function addOffenseCategory(){
    $.when(fetchPostData({
        categoryLetter: $('#categoryLetter').val(),
        offenseCategory: $('#offenseCategoryVal').val(),
    }, '/discipline/add_offense_category')).then(function (addOffenseCategory) {
       var addOffenseCategoryStatus = $.parseJSON(addOffenseCategory.trim());
       if (parseInt(addOffenseCategoryStatus)) {
           getSpecOffenseCategory();
           toastr.success('Successfully Added Offense Type');
           $('#offenseCategoryModal').modal('hide');
       } else {
           toastr.error("Something went wrong while adding the Offense Type. Please inform your system administrator");
       }
    });
}

function addOffense(){
    var multiCategVal = "";
    var categList = [];
    if ($('#allCateg').prop("checked") == true) {
        multiCategVal = 'all';
        categList[0] = 'all';
    } else if ($('#allCateg').prop("checked") == false) {
        if ($('#categoryList').val() == ''){
            multiCategVal = 0;
            categList[0] = 0;
        }else{
            multiCategVal = $('#categoryList').val().toString();
            $('#categoryList').find('option:selected').each(function(index, value){
                categList[index] = $(this).text().toLowerCase();
            });
        }
    }
    $.when(fetchPostData({
        offenseNum: $('#offenseOrderNum').val(),
        offenseCategoryId: $('#offenseCategoryLabel').data('offensecategoryid'),
        disciplineCategoryLetter: categList.toString(),
        offense: $('#offenseDescription').val(),
        multiCateg: multiCategVal
    }, '/discipline/add_offense')).then(function (addOffenseStat) {
        var addOffenseStatus = $.parseJSON(addOffenseStat.trim());
        if (parseInt(addOffenseStatus)) {
            toastr.success('Successfully Added Offense');
            initOffenseDatatable($('#offenseCategoryLabel').data('offensecategoryid'))
            initSelectOffenseCategory($('#offenseCategoryLabel').data('offensecategoryid'));
            $('#offenseModal').modal('hide');
        } else {
            toastr.error("Something went wrong while adding the Offense. Please inform your system administrator");
        }
    });
}

function assignQip(){
    $.when(fetchPostData({
        qipEmpList: $('#employeeQip').val()
    }, '/discipline/assign_qip')).then(function (addQipStat) {
        var addQipStatus = $.parseJSON(addQipStat.trim());
        if (parseInt(addQipStatus)) {
            toastr.success('Successfully assigned with QIP IR Filing Privilege');
            // initOffenseDatatable($('#offenseCategoryLabel').data('offensecategoryid'))
            // initSelectOffenseCategory($('#offenseCategoryLabel').data('offensecategoryid'));
            // $('#offenseModal').modal('hide');
        } else {
            toastr.error("Something went wrong while assigning the QIP IR Filing Privilege. Please inform your system administrator");
        }
        reloadQipDatatable();
        initAssignmentTabContent();
        $('#qipAssignmentModal').modal('hide');
    });
}

function assignEro() {
    $.when(fetchPostData({
        empId: $('#employeeEro').val(),
        siteId: $('#eroSiteId').val(),
        accountIds: $('#eroAccountId').val()
    }, '/discipline/assign_ero')).then(function (assignEroStat) {
        var assignEroStatus = $.parseJSON(assignEroStat.trim());
        if (parseInt(assignEroStatus.assign_ero)) {
            toastr.success('Successfully assigned as an Employee Relations Officer');
            // initOffenseDatatable($('#offenseCategoryLabel').data('offensecategoryid'))
            // initSelectOffenseCategory($('#offenseCategoryLabel').data('offensecategoryid'));
            // $('#offenseModal').modal('hide');
        } else {
            toastr.error("Something went wrong with the Employee Relations Assignment. Please inform your system administrator");
        }
        reloadEroDatatable();
        initAssignmentTabContent();
        $('#eroAssignmentModal').modal('hide');
    });
}

function assignOffenseType() {
    $.when(fetchPostData({
        accId: $('#accountOffenseType').val(),
        offenseTypeId: $('#OffenseTypeSelect').val()
    }, '/discipline/assign_offense_type')).then(function (assignOffenseStat) {
        var assignOffenseStatus = $.parseJSON(assignOffenseStat.trim());
        if (parseInt(assignOffenseStatus.assign_offense_type)) {
            toastr.success('Successfully assigned Offense Type');
        } else {
            toastr.error("Something went wrong while assigning the Offense Type. Please inform your system administrator");
        }
        initAssignmentTabContent();
        reloadOffenseTypeAssignDatatable();
        $('#assignOffenseTypeModal').modal('hide');
    });
}

function assignRecommendationNumber() {
    $.when(fetchPostData({
        accId: $('#accountRecommendationNumber').val(),
        number: $('#recommendationLevel').val()
    }, '/discipline/assign_recommendation_number')).then(function (assignRecommendationStat) {
        var assignRecommendationStatus = $.parseJSON(assignRecommendationStat.trim());
        if (parseInt(assignRecommendationStatus)) {
            toastr.success('Successfully assigned Recommendation Number');
        } else {
            toastr.error("Something went wrong while assigning the Recommendation Number. Please inform your system administrator");
        }
        reloadRecommendationSettings();
        $('#recommendationNumberModal').modal('hide');
    });
}

function assignDmsCustomRecommendation(accIdVal, levelVal, recommenderVal, assignToVal, changeTypeVal) {
    $.when(fetchPostData({
        accId: accIdVal,
        level: levelVal,
        recommender: recommenderVal,
        assignTo: assignToVal,
        changeType: changeTypeVal
    }, '/discipline/assign_recommendation_change')).then(function (assignRecommendationStat) {
        var assignRecommendationStatus = $.parseJSON(assignRecommendationStat.trim());
        if (parseInt(assignRecommendationStatus.assign_stat)) {
            toastr.success('Successfully set Recommendation Change');
        } else {
            toastr.error("Something went wrong while setting the Recommendation Change. Please inform your system administrator");
        }
        initCustomChanges(function(empObj){});
        if (changeTypeVal == "change"){
            $('#assignChangeModal').modal('hide');
        }else{
            $('#assignExemptionModal').modal('hide');
        }
    });
}

function checkIfCustomRecommendationChangeExist(accIdVal, levelVal, recommenderVal, assignToVal, changeTypeVal, alertId){
    $.when(fetchPostData({
        accId: accIdVal,
        level: levelVal,
        recommender: recommenderVal,
        assignTo: assignToVal,
        changeType: changeTypeVal
    }, '/discipline/check_if_already_assigned_recommendation_change')).then(function (checkRecommendation) {
        var checkRecommendationStatus = $.parseJSON(checkRecommendation.trim());
        if (parseInt(checkRecommendationStatus.exist)) {
            console.log(checkRecommendationStatus.record)
            if (changeTypeVal == "change"){
                $('#assignedTo').text(checkRecommendationStatus.record.toFname+" "+checkRecommendationStatus.record.toLname);
            }
            $(alertId).fadeIn();
        } else {
            $(alertId).hide();
            if (changeTypeVal == "change"){
                if ($('#empAssignChanges').val() == $('#empAssignToChanges').val()) {
                    $('#sameChangeAlert').fadeIn();
                    $('#assignChangeForm').formValidation('disableSubmitButtons', true);
                } else {
                    $('#sameChangeAlert').hide();
                    $('#assignChangeForm').formValidation('disableSubmitButtons', false);
                    assignDmsCustomRecommendation(accIdVal, levelVal, recommenderVal, assignToVal, changeTypeVal);
                }
            }else{
                    assignDmsCustomRecommendation(accIdVal, levelVal, recommenderVal, assignToVal, changeTypeVal);
            }
            
        }
    });
}


function initOffenseLevelForm(element){
    $('#offenseCategory').data('category',$(element).data('category'));
    $('#offenseCategory').data('categoryid',$(element).data('disciplinecategoryid'));
    getUnusedDisciplinaryAction($(element).data('disciplinecategoryid'), function (notEmpty){
        if(notEmpty){
            $('#offenseLevelModal').modal('show');
        }else{
            swal("No Disciplinary Action available", "All disciplinary actions are already assigned to this Offense Category", "error");
        }
    })
}

function initSelectOffenseCategory(offenseCategoryIdVal){
    $.when(fetchPostData({
        offenseCategoryId: offenseCategoryIdVal,
    }, '/discipline/get_selected_offense_category_details')).then(function (offenseCategoryDetails) {
        var offenseCategoryDetailsObj = $.parseJSON(offenseCategoryDetails.trim());
        $('#offenseCategoryLabel').text(offenseCategoryDetailsObj.category.letter.toUpperCase() + ". " + offenseCategoryDetailsObj.category.offenseType)
        $('#offenseCategoryLabel').data('offensecategoryid', offenseCategoryIdVal);
        $('#deleteOffenseTypeBtn').data('offensecategoryid', offenseCategoryIdVal);
        $('#editOffenseTypeBtn').data('offensecategoryid', offenseCategoryIdVal);
        $('#emptyOffenseTypeDeleteBtn').data('offensecategoryid', offenseCategoryIdVal);
        $('#specOffenseCategoryList').find('li').removeClass("list-active");
        $('#specOffenseCategoryList').find('span').removeClass("active-nav-a");
        $('#specOffenseCategoryList').find('button').removeClass("active-nav-a");
        // $('#specOffenseCategoryList').hasClass('m-nav__link-text').removeClass("font-weight-bold");
        $('#offenseCategory' + offenseCategoryIdVal).addClass("list-active");
        $('#offenseCategory' + offenseCategoryIdVal).find('span').addClass("active-nav-a");
        $('#offenseCategory' + offenseCategoryIdVal).find('button').addClass("active-nav-a");
        // $('#offenseCategory' + offenseCategoryIdVal).find('m-nav__link-text').addClass("font-weight-bold");
        if (parseInt(offenseCategoryDetailsObj.offenses.exist)) {
            initOffenseDatatable($('#offenseCategoryLabel').data('offensecategoryid'));
            $('#noOffenses').hide();
            $('#offenseDiv').show();
            // $("#specOffenseCategoryList").data("max-height", ($('#OffenseDetails').height()));
        } else {
            $('#noOffenses').show();
            $('#offenseDiv').hide();
            // toastr.error("Something went wrong while adding the Offense Category. Please inform your system administrator");
        }
    });
}

function initOffenseSelectDiscCateg(){
//      $.when(fetchPostData({
//          offenseCategoryId: $('#offenseCategoryLabel').data('offensecategoryid'),
//      }, '/discipline/distinct_offense_category')).then(function (distinctOffenseCateg) {
//          var distinctOffenseCategObj = $.parseJSON(distinctOffenseCateg.trim());
//          console.log(distinctOffenseCategObj);
//          var optionAsString = '<option value="0">ALL</option>';
//          if (parseInt(distinctOffenseCategObj.exist)) {
//             $.each(distinctOffenseCategObj.record, function (index, value) {
//                 optionAsString += '<option value="'+value.disciplineCategory_ID+'">'+value.category.toUpperCase()+'</option>';
//             })
//             $('#offenseCategorySelect').html(optionAsString);
//          } else {
//             $('#offenseCategorySelect').html(optionAsString);
//              // toastr.error("Something went wrong while adding the Offense Category. Please inform your system administrator");
//          }
//      });
}

function get_emp_details(empIdVal ,callback){
    $.when(fetchPostData({
        empId: empIdVal
    }, '/discipline/get_ero_details')).then(function (eroDetails) {
        var eroDetailsObj = $.parseJSON(eroDetails.trim());
        callback(eroDetailsObj);
    });
}

function get_acc_details(accIdVal, callback) {
    $.when(fetchPostData({
        accId: accIdVal
    }, '/discipline/get_acc_details')).then(function (accDetails) {
        var accDetailsObj = $.parseJSON(accDetails.trim());
        callback(accDetailsObj);
    });
}

function initEroAccountsModal(element){
    getSite('#siteAccountEro', function (site) {
        initEroAccountDatatable($(element).data('empid'));
        $('#eroNameInfo').data('empid', $(element).data('empid'));
    })
    getEmployee(0, 0, 'ero', '#eroSup', function (empExistStat) {
        get_emp_details($(element).data('empid'), function(empDetails){
            if (parseInt(empDetails.record_exist)) {
                $('#eroNameInfo').text(empDetails.record.fname+" "+empDetails.record.mname+" "+empDetails.record.lname)
                $('#eroJobInfo').text(empDetails.record.positionDescription);
                $('#eroAccountInfo').text(empDetails.record.accountDescription);
                $('#eroPic').attr('src',empDetails.record.pic);
                $('#eroAccountsModal').modal('show');
                $('#eroSup').val(empDetails.ero_sup.employeeRelationsSupervisor);
                $('#eroSup').data('origsup', empDetails.ero_sup.employeeRelationsSupervisor);
                $('#eroSup').select2({
                    placeholder: "Select an Employee",
                    width: '100%'
                }).trigger('change');
            }else{
                swal("Record Not Found!", "Cannot find employee details. Please report this immediately to the System Administrator", "error");
            }
        });
    })

}

function initAssignedOffenseTypeModal(element) {
    get_acc_details($(element).data('accid'), function(accDetails){
        $('#accNameInfo').text(accDetails.acc_name);
        $('#accNameInfo').data('accid',accDetails.acc_id);
        $('#depNameInfo').text(accDetails.dep_details);
        $('#offenseTypeAssignmentModal').modal('show');
        initAssignOffenseTypeDatatable(accDetails.acc_id);
    })
}

function initOffenseDatatable(offenseCategoryIdVal) {
    $('#offenseDatatable').mDatatable('destroy');
    offenseDatatable.init(offenseCategoryIdVal, $('#offenseSearchField').val());
}

function initReassignmentDatatable(initType, empId) {
    $('#reassignmentDatatable').mDatatable(initType);
    reassignmentDatatable.init($('#accountCustomChanges').val(), empId);
}

function initExemptionDatatable(initType, empId) {
    $('#reassignmentDatatable').mDatatable(initType);
    exemptionDatatable.init($('#accountCustomChanges').val(), empId);
}

function initCustomChangeModal(element){
    var empIdVal = $(element).data('empid');
    var changeTypeVal = $(element).data('type');
    initCustomRecommendationInfo(empIdVal, changeTypeVal, function (recommendationInfoObj) {
        initReassignmentDatatable('destroy', $(element).data('empid'));
        $('#customChangesModal').modal('show');
        $('#customChangesModal').data('type', 'change');
        $('#recommenderName').data('empid', $(element).data('empid'));
    })
}

function initRecommendationInfoChanges(empIdVal, changeTypeVal) {
    $.when(fetchPostData({
        empId: $('#recommenderName').data('empid'),
        type: changeTypeVal
    }, '/discipline/check_specific_custom_change_record')).then(function (customChangeCount) {
        var customChangeCountObj = $.parseJSON(customChangeCount.trim());
        if (parseInt(customChangeCountObj.customRecCount) == 0){
            reloadRecommendationSettings(function (accountObj) {
                $('#customChangesModal').modal('hide');
            });
        }else{
            initCustomRecommendationInfo(empIdVal, changeTypeVal, function (recommendationInfoObj) {
                if (changeTypeVal == "change") {
                    initReassignmentDatatable('reload', $('#recommenderName').data('empid'));
                }else{
                    initExemptionDatatable('reload', $('#recommenderName').data('empid'));
                }
            })
        }
    });
}

function initCustomChangeModalExemption(element){
    var empIdVal = $(element).data('empid');
    var changeTypeVal = $(element).data('type');
    initCustomRecommendationInfo(empIdVal, changeTypeVal, function (recommendationInfoObj) {
        initExemptionDatatable('destroy', $(element).data('empid'));
        $('#customChangesModal').modal('show');
        $('#customChangesModal').data('type', 'exempt');
        $('#recommenderName').data('empid', $(element).data('empid'));
    })
}

function initAccountListOffenseType(callback){
    $.when(fetchGetData('/discipline/get_assigned_accounts')).then(function (account) {
        var accountObj = $.parseJSON(account.trim());
        var optionsAsString = ""
        if (parseInt(accountObj.exist)){
            optionsAsString += "<option value='0'>ALL</option>";
            $.each(accountObj.record, function(index, value){
                optionsAsString += "<option value='" + value.accId + "'>" + toTitleCase(value.acc_name) + "</option>"
            })
        }
        $("#offenseTypeAccount").html(optionsAsString);
        callback(accountObj);
    });
    $('#offenseTypeAccount').select2({
        placeholder: "Select an Account",
        width: '100%'
    })
}



function initCustomLevel(callback){
    initAccountListRecommendationNum(function (accountObj) {
        initRecommendationNumberDatatable();
        callback(accountObj);
    });
}

function initCustomChanges(callback){
    initRecommendationChangeSupervisorList(function (empObj) {
        initCustomChangeDatatable('destroy');
        callback(empObj);
    })
}

function reloadRecommendationSettings(callback){
    initAccountListRecommendationNum(function (accountObj) {
        initCustomChanges(function(empObj){
            reloadRecommendationNumberDatatable();
            callback(accountObj);
        })
    });
}

function initRecommendationSettings(callback) {
    initCustomChanges(function(empObj){});
    initCustomLevel(function (accountObj) {
        callback(accountObj);
    });
    
}

function initAccountListRecommendationNum(callback){
    $.when(fetchGetData('/discipline/get_custom_recommendation_accounts')).then(function (account) {
        var accountObj = $.parseJSON(account.trim());
        var optionsAsString = ""
        if (parseInt(accountObj.exist)){
            optionsAsString += "<option value='0'>ALL</option>";
            $.each(accountObj.record, function(index, value){
                optionsAsString += "<option value='" + value.acc_id + "'>" + toTitleCase(value.acc_name) + "</option>"
            })
            $('#accountRecommendationNum').fadeIn();
            $('#noRecommendationSet').hide();
        }else{
            $('#noRecommendationSet').fadeIn();
            $('#accountRecommendationNum').hide();
        }
        $("#recommendationNumAccount").html(optionsAsString);
        callback(accountObj);
    });
    $('#recommendationNumAccount').select2({
        placeholder: "Select an Account",
        width: '100%'
    })
}

function initRecommendationChangeSupervisorList(callback){
    $.when(fetchGetData('/discipline/get_distinct_custom_recommendation_emp')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        var optionsAsString = ""
        if (parseInt(empObj.exist)){
            optionsAsString += "<option value='0'>ALL</option>";
            $.each(empObj.record, function(index, value){
                optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.lname) + ", " + toTitleCase(value.fname) + "</option>"
            })
            $('#customChanges').fadeIn();
            $('#noCustomChangesSet').hide();
        }else{
            $('#noCustomChangesSet').fadeIn();
            $('#customChanges').hide();
        }
        $("#recommendationSupervisorChange").html(optionsAsString);
        callback(empObj);
    });
    $('#recommendationSupervisorChange').select2({
        placeholder: "Select an Account",
        width: '100%'
    })
}

function initQipDatatable(){
    getAccountQipFilter(function (accExistStat) {
        $('#qipDatatable').mDatatable('destroy');
        qipAssignDatatable.init($('#qipSearchField').val());
    });
}

function initCustomRecommendationInfo(empIdVal, changeTypeVal, callback) {
    $.when(fetchPostData({
        empId: empIdVal,
        changeType: changeTypeVal,
    }, '/discipline/get_custom_recommendation_info')).then(function (recommendationInfo) {
        var recommendationInfoObj = $.parseJSON(recommendationInfo.trim());
        var optionsAsString = ""
        if (parseInt(recommendationInfoObj.exist)){
            $('#recommenderName').text(recommendationInfoObj.emp_details.fname +" "+ recommendationInfoObj.emp_details.lname)
            optionsAsString += "<option value='0'>ALL</option>";
            $.each(recommendationInfoObj.accounts, function (index, value) {
                optionsAsString += "<option value='" + value.acc_id + "'>" + toTitleCase(value.acc_name) + "</option>"
            })
            $("#accountCustomChanges").html(optionsAsString);
            $('#accountCustomChanges').select2({
                placeholder: "Select an Account",
                width: '100%'
            })
        }
        callback(recommendationInfoObj);
    });
}

function reloadQipDatatable(){
    $('#qipDatatable').mDatatable('reload');
    qipAssignDatatable.init($('#qipSearchField').val());
}

function initEroDatatable(){
    $('#eroDatatable').mDatatable('destroy');
    eroAssignDatatable.init($('#eroSearchField').val());
}

function reloadEroDatatable(){
    $('#eroDatatable').mDatatable('reload');
    eroAssignDatatable.init($('#eroSearchField').val());
}

function initEroAccountDatatable(empId){
    $('#eroAccountDatatable').mDatatable('destroy');
    eroAccountDatatable.init($('#eroAccountSearchField').val(), $('#classEroAccount').val(), $('#siteAccountEro').val(), empId);
}
function reloadEroAccountDatatable(empId){
    $('#eroAccountDatatable').mDatatable('reload');
    eroAccountDatatable.init($('#eroAccountSearchField').val(), $('#classEroAccount').val(), $('#siteAccountEro').val(), empId);
}

function initOffenseTypeAssignDatatable(){
    $('#offenseTypeAssDatatable').mDatatable('destroy');
    offenseTypeAssignDatatable.init($('#offenseTypeAccount').val());
}
function reloadOffenseTypeAssignDatatable(){
    $('#offenseTypeAssDatatable').mDatatable('reload');
    offenseTypeAssignDatatable.init($('#offenseTypeAccount').val());
}

function initAssignOffenseTypeDatatable(acc_id){
    $('#assignedOffenseTypeDatatable').mDatatable('destroy');
    assignedOffenseTypeDatatable.init($('#assignedOffenseTypeSearch').val(), acc_id);
}
function reloadAssignOffenseTypeDatatable(acc_id) {
    $('#assignedOffenseTypeDatatable').mDatatable('reload');
    assignedOffenseTypeDatatable.init($('#assignedOffenseTypeSearch').val(), acc_id);
}

function initRecommendationNumberDatatable() {
    $('#customRecommendationNumberDatatable').mDatatable('destroy');
    recommendationNumberDatatable.init($('#recommendationNumAccount').val());
}

function reloadRecommendationNumberDatatable() {
    $('#customRecommendationNumberDatatable').mDatatable('reload');
    recommendationNumberDatatable.init($('#recommendationNumAccount').val());
}

function initCustomChangeDatatable(initType) {
    $('#customChangesDatatable').mDatatable(initType);
    customChangesDatatable.init($('#recommendationSupervisorChange').val());
}

function initAutomationTabContent(){
    initDtrViolationSettings($('.dtrAutoIrSideTab.m-nav__item.list-active').data('violationtypeid'));
}

function initAssignmentTabContent(){
    initAssignmentSettings($('.assignmentSideTab.m-nav__item.list-active').data('assignmenttype'));
}

function initAssignmentSettings(assignTypeId){
    $('.assignContent').hide();
    if (assignTypeId == 1){
        checkIfEroRecordExist(function (exist) {
            if (exist) {
                $('#noAssignedEro').hide();
                $('#showEroDatatable').fadeIn();
            } else {
                $('#eroAccountsModal').modal('hide');
                $('#showEroDatatable').hide();
                $('#noAssignedEro').fadeIn();
            }
            initEroDatatable();
            $('#eroAssign').fadeIn();
        })
    } else if (assignTypeId == 2){
        checkIfOffTypeRecordExist(function (exist) {
            if(exist){
                $('#noAssignedOffenseType').hide();
                initAccountListOffenseType(function (accountList) {
                });
                $('#showOffenseTypeAssignmentDatatable').fadeIn();
            }else{
                $('#offenseTypeAssignmentModal').modal('hide');                
                $('#showOffenseTypeAssignmentDatatable').hide();
                $('#noAssignedOffenseType').fadeIn();
            }
            initOffenseTypeAssignDatatable();
            $('#offenseTypeAssign').fadeIn();
        })
        
    } else if (assignTypeId == 3) {
        checkIfQipRecordExist(function(exist){
            if(exist){
                $('#noAssignedQip').hide();
                $('#showQipDatatable').fadeIn();
            }else{
                $('#showQipDatatable').hide();
                $('#noAssignedQip').fadeIn();
            }
            initQipDatatable();
            $('#qipAssign').fadeIn();
        })
        
    } else {
        initDefaultRecommendationNumber();
        initRecommendationSettings(function(accountObj){
            $('#recommendationSettings').fadeIn();
        })
    }
}

function initDtrViolationSettings(violationTypeId){
    $.when(fetchPostData({
        dtrViolationTypeId: violationTypeId
    }, '/discipline/get_dtr_violation_settings_details')).then(function (dtrViolation) {
        var dtrViolationObj = $.parseJSON(dtrViolation.trim());
        $('[data-toggle="popover"],[data-original-title]').popover('hide').popover('dispose');
        var oneTimeValUnit = "Minute";
        var consecValUnit = "Time";
        var nonConsecValUnit = "Minute";
        if (parseInt(dtrViolationObj.record.oneTimeVal) > 1){
            oneTimeValUnit  += "s";
        }
        if (parseInt(dtrViolationObj.record.consecutiveVal) > 1){
            consecValUnit += "s";
        }
        if (violationTypeId == 3) 
        {
            nonConsecValUnit = 'Time';
            if (parseInt(dtrViolationObj.record.nonConsecutiveVal) > 1) {
                nonConsecValUnit += "s";
            }
        }
        else
        {
            if (parseInt(dtrViolationObj.record.nonConsecutiveVal) > 1) {
                nonConsecValUnit += "s";
            }
        }
        $('#violationTypeVal').data('violationtypeid', violationTypeId);
        $('#violationTypeVal').text($('#dtrViolations').find('span.m-nav__link-text.d-none.d-sm-block.active-nav-a').text());

        var popoverBtn = '<button type="button" class="submitBtn btn btn-success m-btn m-btn--icon m-btn--icon-only mr-1 disabled" onclick="popOverSubmitBtn(this)" disabled>' +
                                '<i class="fa fa-check"></i>'+
                            '</button>'+
                            '<button type="button" class="cancelBtn btn btn-danger m-btn m-btn--icon m-btn--icon-only" onclick="closePoper()">' +
                                '<i class="fa fa-close"></i>' +
                            '</button>';
        var twoFieldHead = '<div class="col-md-12">' +
                                    '<div class="form-group m-form__group row" style="margin-bottom: -7px;">' +
                                        '<div class="col-lg-12 mb-1 popoverErr" style="color:#ff7d7d; font-size: 11px; display:none"></div>';
        var twoFieldFoot = '<div class="col-lg-12 p-1 mt-2">' +
                                            '<p class="text-right" style="padding-top: 0.1rem;">' +
                                                popoverBtn +
                                            '</p>'+
                                        '</div>' +
                                    '</div>'+
                                '</div>';
        
                                        
        var oneFieldPopoverHead = '<div class="col-md-12">' +
                                    '<div class="form-group m-form__group row" style="margin-bottom: -7px;">' +
                                        '<div class="col-lg-12 mb-1 popoverErr" style="color:#ff7d7d; font-size: 11px; display:none"></div>' +
                                        '<div class="col-8 pt-0 pb-0 pl-0 pr-1">' +
                                            '<div class="input-group">';
        var oneFieldPopoverFoot =  '<div class="input-group-append">'+
                                                    '<span class="input-group-text">'+
                                                        '<i class="fa fa-clock-o"></i>' +
                                                    '</span>'+
                                                '</div>'+
                                            '</div>;'+
                                        '</div>'+
                                        '<div class="col-4 p-0">'+
                                            '<p class="text-center" style="padding-top: 0.1rem;">' +
                                                popoverBtn +
                                            '</p>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>';
        var formHead = "";
        var formFoot = '</form>';

        var oneTimeField = '<input type="number" class="form-control m-input" name="oneTimeNumVal" id="oneTimeNumVal" onchange="oneTimeChange(this)" onkeyup="oneTimeChange(this)" min="0" max="1000" value="' + parseInt(dtrViolationObj.record.oneTimeVal) + '" data-field="oneTimeVal">';
        formHead = '<form id="oneTimeForm" method="post">';
        var oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        $('#oneTimeVal').popover({
            title: "Minutes Duration",
            html: true,
            container: 'body',
            animation: true,
            content: oneTimeFieldPopoverBody,
            trigger: 'click',
        }).click(function (event) {
            $('#oneTimeVal').popover('show')
        });

        var consecutiveField = '<input type="number" class="form-control m-input" name="consecutiveNumVal" id="consecutiveNumVal" onchange="consecChange(this)" onkeyup="consecChange(this)" min="2" max="1000" value="' + parseInt(dtrViolationObj.record.consecutiveVal) + '" data-field="consecutiveVal">';
        formHead = '<form id="consecutiveForm" method="post">';
        var consecutivePopoverBody = formHead + oneFieldPopoverHead + consecutiveField + oneFieldPopoverFoot + formFoot;
        $('#consecutiveVal').popover({
            title: "No. of Consecutive Occurence",
            html: true,
            container: 'body',
            animation: true,
            content: consecutivePopoverBody,
            trigger: 'click',
        }).click(function (event) {
            $('#consecutiveVal').popover('show')
        });


        var nonConsecutiveField = '<input type="number" class="form-control m-input" name="nonConsecutiveNumVal" id="nonConsecutiveNumVal" onchange="nonConsecChange(this)" onkeyup="nonConsecChange(this)" min="1" max="1000" value="' + parseInt(dtrViolationObj.record.nonConsecutiveVal) + '" data-field="nonConsecutiveVal">';
        formHead = '<form id="nonConsecutiveForm" method="post">';
        var nonConsecutivePopoverBody = formHead + oneFieldPopoverHead + nonConsecutiveField + oneFieldPopoverFoot + formFoot;
        var popOverTitle = "Accumulated Time in Minutes";
        if (violationTypeId == 3){
            popOverTitle = "Number of Occurence"
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('.comment').text('Number of Non-consecutive occurence:');
        }else{
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('.comment').text('Accumulated Time:');
        }
        $('#nonConsecutiveVal').popover({
            title: popOverTitle,
            html: true,
            container: 'body',
            animation: true,
            content: nonConsecutivePopoverBody,
            trigger: 'focus',
        }).click(function (event) {
            $('#nonConsecutiveVal').popover('show')
        });

        var incompleteLogField = '<input type="number" class="form-control m-input" name="nonConsecutiveNumVal" id="nonConsecutiveNumVal" onchange="nonConsecChange(this)" onkeyup="nonConsecChange(this)" min="1" max="1000" value="' + parseInt(dtrViolationObj.record.nonConsecutiveVal) + '" data-field="nonConsecutiveVal">';
        formHead = '<form id="nonConsecutiveIncForm" method="post">';
        var incompleteLogPopoverBody = formHead + oneFieldPopoverHead + incompleteLogField + oneFieldPopoverFoot + formFoot;
        $('#nonConsecutiveValInc').popover({
            title: "Occurence Number",
            html: true,
            container: 'body',
            animation: true,
            content: incompleteLogPopoverBody,
            trigger: 'focus',
        }).click(function (event) {
            $('#nonConsecutiveValInc').popover('show')
        });


        formHead = '<form id="fixedDurationForm" method="post">';
        var optionVal = "";
        if (dtrViolationObj.record.durationUnit == 'month'){
            optionVal = '<option value="month" selected>Month</option>' +
                '<option value="week">Week</option>' ;
        }else if (dtrViolationObj.record.durationUnit == 'week'){
            optionVal = '<option value="month">Month</option>' +
                '<option value="week" selected>Week</option>';
        }
        var twoFieldPopoverBody = '<div class="col-lg-6 p-1">' +
                                            '<input type="number" class="form-control bootstrap-touchspin-vertical-btn" id="fixedDurationVal" name="fixedDurationVal" placeholder="1, 2..." type="number" onchange="fixedDurationsChange(this)" onkeyup="fixedDurationsChange(this)" value="' + dtrViolationObj.record.durationVal + '" data-field="durationVal">' +
                                        '</div>'+
                                        '<div class="col-lg-6 p-1">' +
                                            '<select class="custom-select form-control" id="fixedDurationUnit" name="fixedDurationUnit" placeholder="Disciplinary Action" onchange="fixedDurationsChange(this)" onkeyup="fixedDurationsChange(this)" data-field="durationUnit">' +
                                                optionVal+
                                            '</select>'+
                                        '</div>';
        var twoFiledContent = formHead + twoFieldHead + twoFieldPopoverBody + twoFieldFoot + formFoot
        $('#fixedDurationsVal').popover({
            title: "Fixed Duration",
            html: true,
            container: 'body',
            animation: true,
            content: twoFiledContent,
            trigger: 'focus',
        }).click(function (event) {
            $('#fixedDurationsVal').popover('show');
        });
        
        var customDurationsField = '<input type="number" class="form-control m-input" name="customDurationNumVal" id="customDurationNumVal" min="1" max="1000" onkeyup="customDurationChange(this)" value="' + dtrViolationObj.record.durationVal + '" data-field="durationVal">';
        formHead = '<form id="customDurationForm" method="post">';
        var customDurationsPopoverBody = formHead+oneFieldPopoverHead + customDurationsField + oneFieldPopoverFoot+formFoot;
        $('#customDurationsVal').popover({
            title: "Duration in Days",
            html: true,
            container: 'body',
            animation: true,
            content: customDurationsPopoverBody,
            trigger: 'focus',
        }).click(function (event) {
            $('#customDurationsVal').popover('show');
        });
        // Auto IR Notification CHKBOX STATUS
        if (parseInt(dtrViolationObj.record.oneTimeStat)) {
            $("#oneTimeOccChkbox").prop("checked", "checked");
            $('#oneTimeOccChkbox').parents('.rowCheckBox').find('label').css('color', '');
            $('#oneTimeOccChkbox').parents('.rowCheckBox').find('.comment').css('color', '');
            $('#oneTimeOccChkbox').parents('.rowCheckBox').find('a').css('color', '');
            $('#oneTimeOccChkbox').parents('.rowCheckBox').find('a').popover('enable');
            $('#oneTimeOccChkbox').parents('.rowCheckBox').find('a').addClass('editableStyle');
        } else {
            $("#oneTimeOccChkbox").prop("checked", "");
            $('#oneTimeOccChkbox').parents('.rowCheckBox').find('label').css('color', '#cccccc');
            $('#oneTimeOccChkbox').parents('.rowCheckBox').find('.comment').css('color', '#cccccc');
            $('#oneTimeOccChkbox').parents('.rowCheckBox').find('a').css('color', '#cccccc');
            $('#oneTimeOccChkbox').parents('.rowCheckBox').find('a').removeClass('editableStyle');
            $('#oneTimeVal').popover('disable');
        }
        if (parseInt(dtrViolationObj.record.consecutiveStat)) {
            $("#consecOccChkbox").prop("checked", "checked");
            $('#consecOccChkbox').parents('.rowCheckBox').find('label').css('color', '');
            $('#consecOccChkbox').parents('.rowCheckBox').find('.comment').css('color', '');
            $('#consecOccChkbox').parents('.rowCheckBox').find('a').css('color', '');
            $('#consecOccChkbox').parents('.rowCheckBox').find('a').popover('enable');
            $('#consecOccChkbox').parents('.rowCheckBox').find('a').addClass('editableStyle');
        } else {
            $("#consecOccChkbox").prop("checked", "");
            $('#consecOccChkbox').parents('.rowCheckBox').find('label').css('color', '#cccccc');
            $('#consecOccChkbox').parents('.rowCheckBox').find('.comment').css('color', '#cccccc');
            $('#consecOccChkbox').parents('.rowCheckBox').find('a').css('color', '#cccccc');
            $('#consecOccChkbox').parents('.rowCheckBox').find('a').removeClass('editableStyle');
            $('#consecutiveVal').popover('disable');
        }
        if (parseInt(dtrViolationObj.record.nonConsecutiveStat)) {
            $("#nonConsecOccChkbox").prop("checked", "checked");
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('label').css('color', '');
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('.comment').css('color', '');
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('a').css('color', '');
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('a').popover('enable');
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('a').addClass('editableStyle');

            $("#incOccurence").prop("checked", "checked");
            $('#incOccurence').parents('.rowCheckBox').find('label').css('color', '');
            $('#incOccurence').parents('.rowCheckBox').find('.comment').css('color', '');
            $('#incOccurence').parents('.rowCheckBox').find('a').css('color', '');
            $('#incOccurence').parents('.rowCheckBox').find('a').popover('enable');
            $('#incOccurence').parents('.rowCheckBox').find('a').addClass('editableStyle');

        } else {
            $("#nonConsecOccChkbox").prop("checked", "");
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('label').css('color', '#cccccc');
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('.comment').css('color', '#cccccc');
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('a').css('color', '#cccccc');
            $('#nonConsecOccChkbox').parents('.rowCheckBox').find('a').removeClass('editableStyle');
            $('#nonConsecutiveVal').popover('disable');

            $("#incOccurence").prop("checked", "");
            $('#incOccurence').parents('.rowCheckBox').find('label').css('color', '#cccccc');
            $('#incOccurence').parents('.rowCheckBox').find('.comment').css('color', '#cccccc');
            $('#incOccurence').parents('.rowCheckBox').find('a').css('color', '#cccccc');
            $('#incOccurence').parents('.rowCheckBox').find('a').removeClass('editableStyle');
            $('#nonConsecutiveValInc').popover('disable');
        }


        // if (parseInt(dtrViolationObj.record.nonConsecutiveStat)) {
        // } else {
            
        // }
        

        // Auto IR Notification Value
        $('#oneTimeVal').text(dtrViolationObj.record.oneTimeVal + " " + oneTimeValUnit);
        $('#oneTimeVal').data('onetimeval',dtrViolationObj.record.oneTimeVal);
        
        $('#consecutiveVal').text(dtrViolationObj.record.consecutiveVal + " " + consecValUnit);
        $('#consecutiveVal').data('consecutiveval', dtrViolationObj.record.consecutiveVal);
        $('#nonConsecutiveVal').text(dtrViolationObj.record.nonConsecutiveVal + " " + nonConsecValUnit);
        $('#nonConsecutiveVal').data('nonconsecutiveval', dtrViolationObj.record.nonConsecutiveVal);

        // PRESCRIPTIVE PERIOD

        $("input[name=dtrPrescriptive][value='" + dtrViolationObj.record.durationType + "']").prop("checked", true);
        var durationUnits = dtrViolationObj.record.durationUnit;
        if (parseInt(dtrViolationObj.record.durationType) == 1) {
            if (parseInt(dtrViolationObj.record.durationVal) > 1) {
                durationUnits = dtrViolationObj.record.durationUnit + "s";
            } else {
                durationUnits = dtrViolationObj.record.durationUnit;
            }
        }else{
            if (parseInt(dtrViolationObj.record.durationVal) > 1){
                durationUnits = "Days";
            }else{
                durationUnits = "Day";
            }
        }

        if (parseInt(dtrViolationObj.record.durationType) == 1){
            $('#fixedDurationsVal').text(dtrViolationObj.record.durationVal +" "+durationUnits);
            $('#fixedDurationsVal').data('durationVal',dtrViolationObj.record.durationVal);
            $('#customDurationsVal').text("0 Day");
            $('#customDurationsVal').data('durationVal',0);
            $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.prescriptiveValue').addClass('editableStyle');

        }else if(parseInt(dtrViolationObj.record.durationType) == 2){
            $('#fixedDurationsVal').text("1 " + dtrViolationObj.record.durationUnit);
            $('#fixedDurationsVal').data('durationVal', 1);
            $('#customDurationsVal').text(dtrViolationObj.record.durationVal +" "+durationUnits);
            $('#customDurationsVal').data('durationVal', dtrViolationObj.record.durationVal);
            $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.prescriptiveValue').addClass('editableStyle');

        } 
        else if(parseInt(dtrViolationObj.record.duratioTypel) == 3){
            // COD
            $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.prescriptiveValue').removeClass('editableStyle');

        }
        $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('label').css('color', '');
        $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.comment').css('color', '');
        $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.prescriptiveValue').css('color', '');
        $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.prescriptiveValue').popover('enable');
        $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('label').css('color', '#cccccc');
        $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('.comment').css('color', '#cccccc');
        $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('.prescriptiveValue').css('color', '#cccccc');
        $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('.prescriptiveValue').popover('disable');
        $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('.prescriptiveValue').removeClass('editableStyle');
        // Offense
        if (dtrViolationObj.offense == null) {
            $('#offenseTypeVal').text("No Default Offense Tag was set.");
            $('#offenseTypeVal').data('offenseid', 0);
            $('#offenseTypeVal').data('offensecategoryid', 0);
            $('#categoryVal').text('');
            $('#offenseVal').text('');
        } else {
            $('#offenseTypeVal').text(dtrViolationObj.offense.offenseType);
            $('#offenseTypeVal').data('offenseid', dtrViolationObj.offense.offense_ID);
            $('#offenseTypeVal').data('offensecategoryid', dtrViolationObj.offense.offenseType_ID);
            var categoryValue = "";
            if ((dtrViolationObj.offense.multiCategoryLetters == 'all') || (dtrViolationObj.offense.multiCategoryLetters == '0')) {
                categoryValue = "depends on the gravity of the Offense";
            } else {
                var multiCategoryLettersArr = dtrViolationObj.offense.multiCategoryLetters.split(',');
                if (multiCategoryLettersArr.length > 1) {
                    var categString = dtrViolationObj.offense.multiCategoryLetters.toString()
                    // array.replace(/,/g, ", ")
                    categoryValue = categString.replace(/,/g, ", ").toUpperCase() + " depending on the gravity of the Offense";
                } else {
                    categoryValue = dtrViolationObj.offense.multiCategoryLetters.toString().toUpperCase();
                }
            }
            // $('#categoryVal').text("Category " + categoryValue);
            $('#offenseVal').text('"'+dtrViolationObj.offense.offense+'"');
        }
        console.log(dtrViolationObj.record.category_ID);
        if (dtrViolationObj.record.category_ID == null){
             $('#categoryVal').text("No Default Category set.");
             $('#categoryVal').data('categoryid',0);
        }else{
            $('#categoryVal').data('categoryid', dtrViolationObj.record.category_ID);
            $('#categoryVal').text("Category " + dtrViolationObj.record.category.toUpperCase());
        }
        var nonConsecutiveValInc = "";
        if (parseInt(dtrViolationObj.record.nonConsecutiveVal) > 1) {
            nonConsecutiveValInc = "s";
        }
        $('#nonConsecutiveValInc').text(dtrViolationObj.record.nonConsecutiveVal + " " + "Time" + nonConsecutiveValInc);
        $('#nonConsecutiveValInc').data('nonconsecutiveval', dtrViolationObj.record.nonConsecutiveVal);

        if (violationTypeId == 5) {
            $('#dtr3rule').hide();
            $('#incompleteLogsRule').hide();
            $('#prescriptiveRule').hide();
            // $('#offenseTagRule').hide();
        } else if ((violationTypeId == 6) || (violationTypeId == 7)) {
            $('#dtr3rule').hide();
            $('#incompleteLogsRule').fadeIn();
            $('#prescriptiveRule').fadeIn();

            if (dtrViolationObj.offense == null) {
                $('#dtr3rule').hide();
                $('#incompleteLogsRule').hide();
                $('#prescriptiveRule').hide();
            } else {
                $('#dtr3rule').hide();
                $('#incompleteLogsRule').fadeIn();
                $('#prescriptiveRule').fadeIn();
            }
        } else {
            $('#dtr3rule').fadeIn();
            $('#incompleteLogsRule').hide();
            $('#prescriptiveRule').fadeIn();

            if (dtrViolationObj.offense == null) {
                $('#dtr3rule').hide();
                $('#incompleteLogsRule').hide();
                $('#prescriptiveRule').hide();
            } else {
                $('#dtr3rule').fadeIn();
                $('#incompleteLogsRule').hide();
                $('#prescriptiveRule').fadeIn();
            }
        }
        $('#dtrVioAutIrContainer').hide();
        $('#dtrVioAutIrContainer').fadeIn();
        $(".submitBtn").off('click');
    });
    
}

function deadlineDtrVioValChange(element){
    var errPopover = 0;
    var errText = "";
    if ($(element).val() == '') {
        errPopover = 1;
        errText = "Days Duration is Required"
    } else {
        if (parseInt($(element).val()) > 30) {
            errPopover = 1;
            errText = "You have exceeded the maximum (30)";
        } else if (parseInt($(element).val()) < 1) {
            errPopover = 1;
            errText = "You have exceeded the minimum (1)";
        }
    }
    if (errPopover) {
        $('.popoverErr').show();
        $('.popoverErr').text(errText);
        $('.submitBtn').addClass('disabled');
        $('.submitBtn').prop('disabled', true);
    } else {
        $('.popoverErr').hide();
        $('.popoverErr').text("");
        $('.submitBtn').removeClass('disabled');
        $('.submitBtn').prop('disabled', false);
    }
}

function initDeadlineDtrViolation(oneFieldPopoverHead, oneFieldPopoverFoot, formFoot, callback) {
    $.when(fetchPostData({
        dtrViolationTypeId: $('#deadlineDtrViolation').val()
    }, '/discipline/get_dtr_violation_settings_details')).then(function (dtrViolation) {
        var dtrViolationObj = $.parseJSON(dtrViolation.trim());
        var unit = "day"
        $('[data-toggle="popover"],[data-original-title]').popover('hide').popover('dispose');

        if (dtrViolationObj.record.daysDeadline > 1) {
            unit = unit+"s";
        }
        //deadline
        $('#dtrDeadlineVal').text(dtrViolationObj.record.daysDeadline + " " + unit);
        var oneTimeField = '<input type="number" class="form-control m-input" name="dtrVioDeadlineVal" id="dtrVioDeadlineVal" min="1" max="1000" value="' + parseInt(dtrViolationObj.record.daysDeadline) + '" onchange="deadlineDtrVioValChange(this)" onkeyup="deadlineDtrVioValChange(this)" data-field="daysDeadline">';
        var formHead = '<form id="dtrViolationDeadlineForm" method="post">';
        var oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        $('#dtrDeadlineVal').popover({
            title: "Days Duration",
            html: true,
            container: 'body',
            animation: true,
            content: oneTimeFieldPopoverBody,
            trigger: 'click',
        }).click(function (event) {
            $('#dtrDeadlineVal').popover('show')
        });
        // Missed Option
        $("input[name=dtrViolationMissedOption][value='" + dtrViolationObj.record.deadlineOption + "']").prop("checked", true);
        $('input[name="dtrViolationMissedOption"]:checked').parents('.dtrRowMissedOption').find('label').css('color', '');
        $('input[name="dtrViolationMissedOption"]:checked').parents('.dtrRowMissedOption').find('.comment').css('color', '');
        $('input[name="dtrViolationMissedOption"]:not(:checked)').parents('.dtrRowMissedOption').find('label').css('color', '#cccccc');
        $('input[name="dtrViolationMissedOption"]:not(:checked)').parents('.dtrRowMissedOption').find('.comment').css('color', '#cccccc');
        $(".submitBtn").off('click');
        callback(dtrViolationObj);
    });
}

function initDeadlineExplain(oneFieldPopoverHead, oneFieldPopoverFoot, formFoot, callback) {
    $.when(fetchGetData('/discipline/get_ir_deadline_settings_details')).then(function (irDeadline){
        var irDeadlineObj = $.parseJSON(irDeadline.trim());
        var unit = "day"
        $('[data-toggle="popover"],[data-original-title]').popover('hide').popover('dispose');

        if (irDeadlineObj.record.deadlineToExplain > 1) {
            unit = unit + "s";
        }
        //deadline
        $('#deadlineExplainVal').text(irDeadlineObj.record.deadlineToExplain + " " + unit);
        var oneTimeField = '<input type="number" class="form-control m-input" name="deadlineToExplain" id="deadlineToExplain" min="1" max="1000" value="' + parseInt(irDeadlineObj.record.deadlineToExplain) + '" onchange="deadlineDtrVioValChange(this)" onkeyup="deadlineDtrVioValChange(this)" data-deadlinetype="2">';
        var formHead = '<form id="deadlineExplainForm" method="post">';
        var oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        $('#deadlineExplainVal').popover({
            title: "Days Duration",
            html: true,
            container: 'body',
            animation: true,
            content: oneTimeFieldPopoverBody,
            trigger: 'click',
        }).click(function (event) {
            $('#deadlineExplainVal').popover('show')
        });
        // Missed Option
        $("input[name='explainMissedOption'][value='" + irDeadlineObj.record.deadlineToExplainOption + "']").prop("checked", true);
        $('input[name="explainMissedOption"]:checked').parents('.explainRowMissedOption').find('label').css('color', '');
        $('input[name="explainMissedOption"]:checked').parents('.explainRowMissedOption').find('.comment').css('color', '');
        $('input[name="explainMissedOption"]:not(:checked)').parents('.explainRowMissedOption').find('label').css('color', '#cccccc');
        $('input[name="explainMissedOption"]:not(:checked)').parents('.explainRowMissedOption').find('.comment').css('color', '#cccccc');
        $(".submitBtn").off('click');
        callback(irDeadlineObj);
    });
}

function initDeadlineRecommend(oneFieldPopoverHead, oneFieldPopoverFoot, formFoot, callback) {
    $.when(fetchGetData('/discipline/get_ir_deadline_settings_details')).then(function (irDeadline){
        var irDeadlineObj = $.parseJSON(irDeadline.trim());
        var unit = "day"
        $('[data-toggle="popover"],[data-original-title]').popover('hide').popover('dispose');

        if (irDeadlineObj.record.deadlineToRecommend > 1) {
            unit = unit + "s";
        }
        //deadline
        $('#deadlineToRecommendVal').text(irDeadlineObj.record.deadlineToRecommend + " " + unit);
        var oneTimeField = '<input type="number" class="form-control m-input" name="deadlineToRecommend" id="deadlineToRecommend" min="1" max="1000" value="' + parseInt(irDeadlineObj.record.deadlineToRecommend) + '" onchange="deadlineDtrVioValChange(this)" onkeyup="deadlineDtrVioValChange(this)" data-deadlinetype="3">';
        var formHead = '<form id="deadlineRecommendForm" method="post">';
        var oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        $('#deadlineToRecommendVal').popover({
            title: "Days Duration",
            html: true,
            container: 'body',
            animation: true,
            content: oneTimeFieldPopoverBody,
            trigger: 'click',
        }).click(function (event) {
            $('#deadlineToRecommendVal').popover('show')
        });
        // Recommendation Flow
        $("input[name='deadlineToRecommendOption'][value='" + irDeadlineObj.record.deadlineToRecommendOption + "']").prop("checked", true);
        $('input[name="deadlineToRecommendOption"]:checked').parents('.recommendationFlow').find('label').css('color', '');
        $('input[name="deadlineToRecommendOption"]:checked').parents('.recommendationFlow').find('.comment').css('color', '');
        $('input[name="deadlineToRecommendOption"]:not(:checked)').parents('.recommendationFlow').find('label').css('color', '#cccccc');
        $('input[name="deadlineToRecommendOption"]:not(:checked)').parents('.recommendationFlow').find('.comment').css('color', '#cccccc');
        $(".submitBtn").off('click');
        callback(irDeadlineObj);
    });
}

function initDeadlineLastRecommendation(oneFieldPopoverHead, oneFieldPopoverFoot, formFoot, callback) {
    $.when(fetchGetData('/discipline/get_ir_deadline_settings_details')).then(function (irDeadline){
        var irDeadlineObj = $.parseJSON(irDeadline.trim());
        var lastUnit = "day";
        var finalUnit = "day";
        var uploadUnit = "day";
        $('[data-toggle="popover"],[data-original-title]').popover('hide').popover('dispose');
        if (irDeadlineObj.record.deadlineLastRecommendation > 1) {
            lastUnit = lastUnit + "s";
        }
        //deadline
        $('#deadlineLastRecommendationVal').text(irDeadlineObj.record.deadlineLastRecommendation + " " + lastUnit);
        var oneTimeField = '<input type="number" class="form-control m-input" name="deadlineLastRecommendation" id="deadlineLastRecommendation" min="1" max="1000" value="' + parseInt(irDeadlineObj.record.deadlineLastRecommendation) + '" onchange="deadlineDtrVioValChange(this)" onkeyup="deadlineDtrVioValChange(this)" data-deadlinetype="4">';
        var formHead = '<form id="deadlineLastRecommendationForm" method="post">';
        var oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        $('#deadlineLastRecommendationVal').popover({
            title: "Days Duration",
            html: true,
            container: 'body',
            animation: true,
            content: oneTimeFieldPopoverBody,
            trigger: 'click',
        }).click(function (event) {
            $('#deadlineLastRecommendationVal').popover('show')
        });

        if (irDeadlineObj.record.deadlineFinalDecision > 1) {
            finalUnit = finalUnit + "s";
        }
        $('#deadlineFinalDecisionVal').text(irDeadlineObj.record.deadlineFinalDecision + " " + finalUnit);
        oneTimeField = '<input type="number" class="form-control m-input" name="deadlineFinalDecision" id="deadlineFinalDecision" min="1" max="1000" value="' + parseInt(irDeadlineObj.record.deadlineFinalDecision) + '" onchange="deadlineDtrVioValChange(this)" onkeyup="deadlineDtrVioValChange(this)" data-deadlinetype="4">';
        formHead = '<form id="deadlineFinalDecisionForm" method="post">';
        oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        $('#deadlineFinalDecisionVal').popover({
            title: "Days Duration",
            html: true,
            container: 'body',
            animation: true,
            content: oneTimeFieldPopoverBody,
            trigger: 'click',
        }).click(function (event) {
            $('#deadlineFinalDecisionVal').popover('show')
        });
        
        if (irDeadlineObj.record.deadlineUploadDocs > 1) {
            uploadUnit = uploadUnit + "s";
        }
        $('#deadlineUploadDocsVal').text(irDeadlineObj.record.deadlineUploadDocs + " " + uploadUnit);
        oneTimeField = '<input type="number" class="form-control m-input" name="deadlineUploadDocs" id="deadlineUploadDocs" min="1" max="1000" value="' + parseInt(irDeadlineObj.record.deadlineUploadDocs) + '" onchange="deadlineDtrVioValChange(this)" onkeyup="deadlineDtrVioValChange(this)" data-deadlinetype="4">';
        formHead = '<form id="deadlineUploadDocsForm" method="post">';
        oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        oneTimeFieldPopoverBody = formHead + oneFieldPopoverHead + oneTimeField + oneFieldPopoverFoot + formFoot;
        $('#deadlineUploadDocsVal').popover({
            title: "Days Duration",
            html: true,
            container: 'body',
            animation: true,
            content: oneTimeFieldPopoverBody,
            trigger: 'click',
        }).click(function (event) {
            $('#deadlineUploadDocsVal').popover('show')
        });
        $(".submitBtn").off('click');
        callback(irDeadlineObj);
    });
}

function initIrDeadline(deadlineTypeVal){
    var popoverBtn = '<button type="button" class="submitBtn btn btn-success m-btn m-btn--icon m-btn--icon-only mr-1 disabled" onclick="irDeadsPopOverSubmitBtn(this)" disabled>' +
                                '<i class="fa fa-check"></i>'+
                            '</button>'+
                            '<button type="button" class="cancelBtn btn btn-danger m-btn m-btn--icon m-btn--icon-only" onclick="closePoper()">' +
                                '<i class="fa fa-close"></i>' +
                            '</button>';
    var oneFieldPopoverHead = '<div class="col-md-12">' +
                                '<div class="form-group m-form__group row" style="margin-bottom: -7px;">' +
                                    '<div class="col-lg-12 mb-1 popoverErr" style="color:#ff7d7d; font-size: 11px; display:none"></div>' +
                                    '<div class="col-8 pt-0 pb-0 pl-0 pr-1">' +
                                        '<div class="input-group">';
    var oneFieldPopoverFoot =  '<div class="input-group-append">'+
                                                '<span class="input-group-text">'+
                                                    '<i class="fa fa-clock-o"></i>' +
                                                '</span>'+
                                            '</div>'+
                                        '</div>;'+
                                    '</div>'+
                                    '<div class="col-4 p-0">'+
                                        '<p class="text-center" style="padding-top: 0.1rem;">' +
                                            popoverBtn +
                                        '</p>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
    var formFoot = '</form>';
    $('.deadlineContent').hide();
    if (deadlineTypeVal == 1){
        initDeadlineDtrViolation(oneFieldPopoverHead, oneFieldPopoverFoot, formFoot, function(obj){});
        $('#deadlineDtr').fadeIn();
    } else if (deadlineTypeVal == 2) {
        initDeadlineExplain(oneFieldPopoverHead, oneFieldPopoverFoot, formFoot, function(obj){});
        $('#deadlineExplain').fadeIn();
    } else if (deadlineTypeVal == 3) {
        initDeadlineRecommend(oneFieldPopoverHead, oneFieldPopoverFoot, formFoot, function (obj) {});
        $('#deadlineRecommend').fadeIn();
    } else if (deadlineTypeVal == 4) {
        initDeadlineLastRecommendation(oneFieldPopoverHead, oneFieldPopoverFoot, formFoot, function (obj) {});
        $('#deadlineLastRecommend').fadeIn();
    }
}

function initDtrViolationsMenu(){
    $.when(fetchGetData('/discipline/get_disciplinary_action')).then(function (disciplinaryAction) {
        var disciplinaryActionObj = $.parseJSON(disciplinaryAction.trim());
        var tableBody = "";
        if (disciplinaryActionObj.length > 0) {
            $('#noDisciplinaryAction').hide();
            $('#disciplinaryActionTableContainer').fadeIn();
        } else {
            $('#noDisciplinaryAction').fadeIn();
            $('#disciplinaryActionTableContainer').hide();
        }
        $.each(disciplinaryActionObj, function (index, value) {
            var month = "Month";
            if (parseFloat(value.periodMonth) > 1) {
                month = "Months";
            }
            tableBody += '<tr class="d-flex">' +
                '<th scope="row" class="text-capitalize text-center col-5">' + value.action + '</th>' +
                '<td class = "text-center text-uppercase col-2" >' + value.abbreviation + '</td>' +
                '<td class = "text-center col-3" >' + value.periodMonth + ' ' + month + '</td>' +
                '<td class="text-center col-2">' +
                '<a class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2" style="width: 22px !important;height: 22px !important;" data-disciplinaryid="' + value.disciplinaryAction_ID + '" onclick="getSpecificDisciplinaryAction(this)">' +
                '<i class="fa fa-edit"></i>' +
                '</a>' +
                '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-disciplinaryid="' + value.disciplinaryAction_ID + '" onclick="confirmDeleteDisciplinaryAction(this)">' +
                '<i class="fa fa-trash"></i>' +
                '</a>' +
                '</td></tr>';
        });
        $('#disciplinaryActionTableBody').html(tableBody);
    });
}

// END OF CREATE FUNCTIONS ----------------------------------------------------------------------------------

// READ FUNCTION -----------------------------------------------------------------------------------
function getDisciplinaryAction(){
     $.when(fetchGetData('/discipline/get_disciplinary_action')).then(function (disciplinaryAction) {
         var disciplinaryActionObj = $.parseJSON(disciplinaryAction.trim());
         var tableBody = "";
         if(disciplinaryActionObj.length > 0){
             $('#noDisciplinaryAction').hide();
             $('#disciplinaryActionTableContainer').fadeIn();
        }else{
            $('#noDisciplinaryAction').fadeIn();
            $('#disciplinaryActionTableContainer').hide();
         }
         $.each(disciplinaryActionObj, function (index, value) {
            var month = "Month";
            if (parseFloat(value.periodMonth) > 1){
                month = "Months";
            }
            tableBody += '<tr class="d-flex">'+
            '<th scope="row" class="text-capitalize text-center col-5">' + value.action + '</th>' +
            '<td class = "text-center text-uppercase col-2" >' + value.abbreviation + '</td>' +
            '<td class = "text-center col-3" >' + value.periodMonth + ' ' + month + '</td>'+
            '<td class="text-center col-2">' +
                '<a class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2" style="width: 22px !important;height: 22px !important;" data-disciplinaryid="' + value.disciplinaryAction_ID + '" onclick="getSpecificDisciplinaryAction(this)">' +
                    '<i class="fa fa-edit"></i>'+
                '</a>'+
                '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="width: 22px !important;height: 22px !important;" data-disciplinaryid="' + value.disciplinaryAction_ID + '" onclick="confirmDeleteDisciplinaryAction(this)">' +
                    '<i class="fa fa-trash"></i>'+
                '</a>'+
            '</td></tr>';
         });
         $('#disciplinaryActionTableBody').html(tableBody);
     });
}

function getOffenseCategory(){
     $.when(fetchGetData('/discipline/get_offense_category')).then(function (offenseCategory) {
         var offenseCategoryObj = $.parseJSON(offenseCategory.trim());
         var tableBody = "";
         if (offenseCategoryObj.length < 1) {
            $('#noOffenseCategory').fadeIn();
            $('#OffenseCategoryTableContainer').hide();
            // $('#mainCategoryBtn').show();
        }else{
            $('#noOffenseCategory').hide();
            $('#OffenseCategoryTableContainer').fadeIn();
            // $('#mainCategoryBtn').hide();
         }
         $.each(offenseCategoryObj, function (index, value) {
            tableBody += '<tr class="">';
            rowSpanVal = value.offense_per_category.length;
            if (rowSpanVal< 1){
                rowSpanVal++;
            }
            tableBody += '<th scope="row" rowspan="' + rowSpanVal + '" class="text-center text-capitalize align-middle" style="width: 19rem !important;border-top: 3px solid #b3b3b3;border-left: 5px solid #b3b3b3;border-bottom: 3px solid #b3b3b3;">' + value.category + '<button type="button" class="categoryActionBtn btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air pull-right " data-category="' + value.category + '" data-categoryid="' + value.disciplineCategory_ID + '"><i class="fa fa-ellipsis-v" style="color: #bbbaba;"></i></button></th>';
                        
            if (value.offense_per_category.length < 1){
                    tableBody += '<td class = "text-center text-uppercase" style="width: 11rem !important;border-top: 3px solid #b3b3b3;border-bottom: 3px solid #b3b3b3;">NOT SET</td>' +
                    '<td class = "text-center" style="border-top: 3px solid #b3b3b3;border-bottom: 3px solid #b3b3b3;">NOT SET</td style=""><td style="width: 7rem !important;border-bottom: 3px solid #b3b3b3;border-right: 3px solid #b3b3b3"></td></tr>';
            }else{
                var offenseCount = value.offense_per_category_count;
                var categoryId = value.disciplineCategory_ID;
                $.each(value.offense_per_category, function (index, value) {
                    if (index == 0) {
                        var borderTop = "border-top: 3px solid #b3b3b3"
                    }
                    var isLastElement = parseInt(offenseCount) - 1;
                    var mr = "mr-1";
                    var up = 1;
                    var down = 2;
                    var upBtn = '<button type="button" class="btn btn-sm btn-outline-metal m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air mr-1" data-discactioncategid="'+value.disciplinaryActionCategory_ID+'" data-categoryid="'+categoryId+'" data-level="'+value.level+'" onclick="rearrangeOffenseLevel(this, 1)" style="width: 22px !important;height: 22px !important;"><i class="fa fa-arrow-up" style="color: #bbbaba;"></i></button>'
                    var downBtn = '<button type="button" class="btn btn-sm btn-outline-metal m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air mr-1" data-discactioncategid="'+value.disciplinaryActionCategory_ID+'" data-categoryid="'+categoryId+'" data-level="'+value.level+'" onclick="rearrangeOffenseLevel(this, 2)" style="width: 22px !important;height: 22px !important;"><i class="fa fa-arrow-down" style="color: #bbbaba;"></i></button>'
                    if (index == isLastElement) {
                        downBtn = "";
                        var borderBottom = "border-bottom: 3px solid #b3b3b3"
                         mr = 'margin-right: 29px;';
                         upBtn = '<button type="button" class="btn btn-sm btn-outline-metal m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" data-discactioncategid="'+value.disciplinaryActionCategory_ID+'" data-categoryid="'+categoryId+'" data-level="'+value.level+'" onclick="rearrangeOffenseLevel(this, 1)" style="width: 22px !important;height: 22px !important;' + mr + '"><i class="fa fa-arrow-up" style="color: #bbbaba;"></i></button>'
                    } else if (index == 0) {
                        upBtn = "";
                         mr = 'margin-left: 24px;';
                        var downBtn = '<button type="button" class="btn btn-sm btn-outline-metal m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air mr-1" data-discactioncategid="'+value.disciplinaryActionCategory_ID+'" data-categoryid="'+categoryId+'" data-level="'+value.level+'" onclick="rearrangeOffenseLevel(this, 2)" style="width: 22px !important;height: 22px !important;' + mr + '"><i class="fa fa-arrow-down" style="color: #bbbaba;"></i></button>'
                    }
                    var offenseActionBtn = '<td style="width: 7rem !important;' + borderTop + ';' + borderBottom + ';border-right: 3px solid #b3b3b3">';
                    if (parseInt(offenseCount) > 1){ 
                        offenseActionBtn += upBtn + downBtn;
                        offenseActionBtn += '<button type="button" class="btn btn-sm btn-outline-danger m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" data-discactioncategid="' + value.disciplinaryActionCategory_ID + '" data-categoryid="' + categoryId + '" data-level="' + value.level + '" onclick="deleteOffenseLevel(this)" style="width: 22px !important;height: 22px !important;"><i class="fa fa-times" style="color: #bbbaba;"></i></button></td>';
                    } else if(parseInt(offenseCount) == 1) {
                        offenseActionBtn += '<button type="button" class="btn btn-sm btn-outline-danger m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" data-discactioncategid="' + value.disciplinaryActionCategory_ID + '" data-categoryid="' + categoryId + '" data-level="' + value.level + '" onclick="deleteOffenseLevel(this)" style="width: 22px !important;height: 22px !important;margin-left: 50px;"><i class="fa fa-times" style="color: #bbbaba;"></i></button></td>';
                    }
                    tableBody += '<td class = "text-center text-uppercase" style="width: 11rem !important;' + borderTop + ';' + borderBottom + '">' + value.level + '</td>' +
                    '<td class = "text-center" style="' + borderTop + ';' + borderBottom + '">' + value.action + '</td>' +
                    offenseActionBtn+
                    '</tr>';
                })
            }
            tableBody += '</tr>';
         });
        
        $('#OffenseCategoryTableBody').html(tableBody);
        $('.categoryActionBtn').on('mouseover', function (event) {
            popoverBody = '<p style="color:#6C6E8D">'+
                '<button type="button" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2" data-category="' + $(event.currentTarget).data('category').toUpperCase() + '" data-disciplinecategoryid="' + $(event.currentTarget).data('categoryid') + '" onclick="initOffenseLevelForm(this)"><i class="fa fa-plus" style="color: #bbbaba;"></i></button>'+
                '<button type="button" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" data-category="' + $(event.currentTarget).data('category').toUpperCase() + '" data-disciplinecategoryid="' + $(event.currentTarget).data('categoryid') + '" onclick="confirmDeleteOffenseCategory(this)"><i class="fa fa-trash" style="color: #bbbaba;"></i></button></p>';
            $(event.currentTarget).popover({
                title: "Category " + $(event.currentTarget).data('category').toUpperCase(),
                html: true,
                container: 'body',
                animation: true,
                content: popoverBody,
                trigger: 'focus',
            }).click(function (event) {
                $(event.currentTarget).popover('show')
            })
        })
     });
}

function getSpecificDisciplinaryAction(element) {
    $.when(fetchPostData({
        disciplinaryActionId: $(element).data('disciplinaryid')
    }, '/discipline/get_specific_diciplinary_action')).then(function (disciplinaryAction) {
        var disciplinaryActionObj = $.parseJSON(disciplinaryAction.trim());
        if (disciplinaryActionObj == '0') {
            getDisciplinaryAction();
            swal("Record Does Not Exist!", "It seems that the record you are about to update was removed. Please report this immediately to the System Administrator", "error");
        } else {
            $('#addDisciplanyActionBtn').hide();
            $('#updateDisciplanyActionBtn').show();
            $('#disciplinaryActionForm').data('formtype', 'edit');
            $('#disciplinaryActionForm').data('disciplinaryid', $(element).data('disciplinaryid'));
            $('#action').val(disciplinaryActionObj.action);
            $('#abreviation').val(disciplinaryActionObj.abbreviation);
            $('#prescreptivePeriod').val(disciplinaryActionObj.periodMonth);
            $('#disciplinaryActionModal').modal('show');
        }
    });
}

function getSpecificOffenseCategory(element){
    $.when(fetchPostData({
        offenseCategoryId: $(element).data('offensecategoryid')
    }, '/discipline/get_one_specific_offense_category')).then(function (specificOffenseCateg) {
        var specificOffenseCategObj = $.parseJSON(specificOffenseCateg.trim());
        // console.log(specificOffenseCategObj);
        if (parseInt(specificOffenseCategObj.exist)) {
            // console.log(specificOffenseCategObj.spec_offense_category);
            $('#addSpecOffenseCategoryBtn').hide();
            $('#updateSpecOffenseCategoryBtn').show();
            $('#offenseCategoryForm').data('formtype', 'edit');
            $('#offenseCategoryForm').data('specoffensecategoryid', $(element).data('offensecategoryid'));
            $('#categoryLetter').val(specificOffenseCategObj.spec_offense_category.letter.toUpperCase());
            $('#categoryLetter').prop('disabled', true);
            $('#offenseCategoryVal').val(specificOffenseCategObj.spec_offense_category.offenseType);
            $('#offenseCategoryVal').data('orig',specificOffenseCategObj.spec_offense_category.offenseType);
            $('#offenseCategoryModal').modal('show');
        } else {
            getSpecOffenseCategory();
            swal("Record Does Not Exist!", "It seems that the Specific Offense Category record that you are about to update was removed. Please report this immediately to the System Administrator", "error");
        }
    });
}

function getMaxCategory() {
    $.when(fetchGetData('/discipline/get_max_category')).then(function (maxCategory) {
        var maxCategoryStat = $.parseJSON(maxCategory.trim());
        if (parseInt(maxCategoryStat.exist) == 0) {
            $('#category').val('A');
        } else {
            var nextLetter = nextChar(maxCategoryStat.record.category);
            $('#category').val(nextLetter.toUpperCase());
        }
        $('#categoryModal').modal('show');
    });
}

function getMaxOffenseCategory(){
     $.when(fetchGetData('/discipline/get_max_offense_category')).then(function (maxCategory) {
         var maxOffenseCategoryStat = $.parseJSON(maxCategory.trim());
         if (parseInt(maxOffenseCategoryStat.exist) == 0) {
             $('#categoryLetter').val('A');
         } else {
             var nextLetter = nextChar(maxOffenseCategoryStat.record.letter);
             $('#categoryLetter').val(nextLetter.toUpperCase());
         }
         $('#offenseCategoryModal').modal('show');
     });
}

function getUnusedDisciplinaryAction(categoryIdVal, callback) {
    $.when(fetchPostData({
        categoryId: categoryIdVal
    }, '/discipline/get_add_offense_level_form_details')).then(function (disciplinaryAction) {
        var disciplinaryActionObj = $.parseJSON(disciplinaryAction.trim());
        var optionsAsString = "";
        var notEmpty = 1;
        $('#offenseLevel').text(parseInt(disciplinaryActionObj.max_level)+1);
        // console.log(disciplinaryActionObj.length);
        if (parseInt(disciplinaryActionObj.record) == 0) {
            notEmpty = 0;
        }else{
            $.each(disciplinaryActionObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.disciplinaryAction_ID + "'>" + value.action.toUpperCase() + "</option>";
            })
            $('#offenseDisciplinaryAction').html(optionsAsString);
        }
        $('#offenseDisciplinaryAction').select2({
            placeholder: "Select a Disciplinary Action",
            width: '100%'
        });
        callback(notEmpty);
    });
}

function getSpecOffenseCategory(){
    $.when(fetchPostData({
            searchOffenseCategory: $('#searchOffenseCategoryVal').val()
        }, '/discipline/get_all_specific_offense_category')).then(function (specOffenseCateg) {
        var specOffenseCategObj = $.parseJSON(specOffenseCateg.trim());
        // $('[data-toggle="tooltip"]').tooltip('dispose');
        if ((parseInt(specOffenseCategObj.exist) == 0) && ($('#searchOffenseCategoryVal').val() != "")) {
            var specOffenseCateg = "";
            specOffenseCateg += '<li class="m-nav__item m-nav__item text-center font-weight-bold">No Offense Category Found</li>';
            $('#specOffenseCategoryList').html(specOffenseCateg);
                $('#offenseSearchVal').text('"'+$('#searchOffenseCategoryVal').val()+'" Not Found');
                $('#noSpecificOffenseCategory').hide();
                $('#noOffenses').hide();
                $('#offenseDiv').hide();
                $('#noOffenseType').show();
        } else if ((parseInt(specOffenseCategObj.exist) == 0) && ($('#searchOffenseCategoryVal').val() == "")){
            $('#specificOffenseCategoryDiv').hide();
            $('#noSpecificOffenseCategory').show();
            // $('#offenseDiv').hide();
        }else {
            var specOffenseCateg = "";
            $.each(specOffenseCategObj.spec_offense_category, function(index, value){
                specOffenseCateg += '<li class="codOffense m-nav__item" id="offenseCategory' + value.offenseType_ID + '" style="padding-left: 0.5rem; padding-right: 0.5rem;" data-offensecategoryid="' + value.offenseType_ID + '">' +
                                        '<a class="m-nav__link" class="specOffenseCategory" data-offensecategoryid="' + value.offenseType_ID + '" onclick="initSelectOffenseCategory(' + value.offenseType_ID + ')" style="padding: 0.4rem !important;">' +
                                            '<span class="m-nav__link-icon"><button type="button" class="offenseCategoryAction btn btn-sm btn-secondary m-btn" data-offensecategoryletter="' + value.letter + '" data-offensecategoryid="' + value.offenseType_ID + '" style="background: unset !important;font-size: 15px;border: none;padding-left: 0.5 rem;padding-top: 0.3rem;" data-offensetype="'+value.offenseType+'" onmouseover="showTooltip(this)">' + value.letter.toUpperCase() + '.</button></span>' +
                                            '<span class="m-nav__link-text d-none d-sm-block" style="padding-top: 0.3rem;padding-bottom: 0.1rem;">' + value.offenseType + '</span>' +
                                        '</a>'+
                                    '</li>';
            })
            $('#specOffenseCategoryList').html(specOffenseCateg);
            
            initSelectOffenseCategory($('#specOffenseCategoryList').find('li').first().data('offensecategoryid'));
            // $('.offenseTypeAction').on('mouseover', function (event) {
            //     // console.log($(event.currentTarget).data('offensecategoryletter').toUpperCase());
            //     popoverBody = '<p style="color:#6C6E8D">' +
            //         '<button type="button" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2" data-offensecategoryletter="' + $(event.currentTarget).data('offensecategoryletter') + '" data-offensecategoryid="' + $(event.currentTarget).data('offensecategoryid') + '" onclick="getSpecificOffenseCategory(this)"><i class="fa fa-pencil" style="color: #bbbaba;"></i></button>' +
            //         '<button type="button" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" data-offensecategoryletter="' + $(event.currentTarget).data('offensecategoryletter') + '" data-offensecategoryid="' + $(event.currentTarget).data('offensecategoryid') + '" onclick="confirmDeleteSpecificOffenseCategory(this)"><i class="fa fa-trash" style="color: #bbbaba;"></i></button></p>';
            //     $(event.currentTarget).popover({
            //         title: "Offense Type " + $(event.currentTarget).data('offensecategoryletter').toUpperCase(),
            //         html: true,
            //         container: 'body',
            //         animation: true,
            //         content: popoverBody,
            //         trigger: 'focus',
            //     }).dblclick(function (event) {
            //         $(event.currentTarget).popover('show')
            //     })
            // });
            $('#specificOffenseCategoryDiv').show();
            $('#noSpecificOffenseCategory').hide();
            $('#noOffenseType').hide();

        }
        // $('#offenseCategoryModal').modal('show');
    });
}

function getMultiOffenseTypeSelect(selectedVals, callback) {
    $.when(fetchGetData('/discipline/get_all_offense_category')).then(function (offense) {
        var offenseObj = $.parseJSON(offense.trim());
        var optionAsString = "";
        if (parseInt(offenseObj.exist)) {
            if (selectedVals == '[0]'){
                $.each(offenseObj.spec_offenses, function (index, value) {
                    optionAsString += '<option value="' + value.disciplineCategory_ID + '" >' + value.category.toUpperCase() + '</option>';
                })
                $('#categoryList').html(optionAsString);
                $('#categoryList').select2({
                    placeholder: "Select Multiple Categories",
                    width: '100%'
                });
                $('#allCateg').prop("checked", false);
            } else if (selectedVals == '[all]') {
                $.each(offenseObj.spec_offenses, function (index, value) {
                    optionAsString += '<option value="' + value.disciplineCategory_ID + '" >' + value.category.toUpperCase() + '</option>';
                })
                $('#categoryList').html(optionAsString);
                $('#categoryList').select2({
                    placeholder: "Select Multiple Categories",
                    width: '100%'
                });
                $('#categoryList').prop('disabled', true);
                $('#allCateg').prop("checked", true);
            }else{
                $.each(offenseObj.spec_offenses, function (index, value) {
                    var selected2 = "";
                    if ($.inArray(parseInt(value.disciplineCategory_ID), JSON.parse(selectedVals)) > -1) {
                        selected2 = "selected";
                    }
                    optionAsString += '<option value="' + value.disciplineCategory_ID + '" '+selected2+'>' + value.category.toUpperCase() + '</option>';
                })
                $('#categoryList').html(optionAsString);
                $('#categoryList').select2({
                    placeholder: "Select Multiple Categories",
                    width: '100%'
                });
                $('#categoryList').val(JSON.parse(selectedVals));
                // $('#multiCategoryField').show();
                // $('#multiCategCancel').show()
            }

        } else {
            //  $('#noDisciplinaryAction').show(500);
            //  $('#disciplinaryActionTableContainer').hide(500);
        }
        callback(offenseObj);
    })
}

function getAllOffenseCategory(callback){
     $.when(fetchGetData('/discipline/get_all_offense_category')).then(function (offense) {
         var offenseObj = $.parseJSON(offense.trim());
         var optionAsString = "";
         if (parseInt(offenseObj.exist)) {
             $.each(offenseObj.spec_offenses, function(index, value){
                    optionAsString += '<option value="'+value.disciplineCategory_ID+'">'+value.category.toUpperCase()+'</option>';
             })
             $('#offenseCategoryLetter').html(optionAsString);
            //  $('#noDisciplinaryAction').hide(500);
            //  $('#disciplinaryActionTableContainer').show(500);
         } else {
            //  $('#noDisciplinaryAction').show(500);
            //  $('#disciplinaryActionTableContainer').hide(500);
         }
         callback(offenseObj);
     });
}

function getCurrentOffenseOrder(offenseCategoryIdVal ,callback){
    $.when(fetchPostData({
        offenseCategoryId: offenseCategoryIdVal
    }, '/discipline/get_max_offense_order')).then(function (offenseOrder) {
        var offenseOrderObj = $.parseJSON(offenseOrder.trim());
        var maxNum = offenseOrderObj.max_order;
        if ($('#offenseForm').data('formtype') == 'edit'){
            maxNum--;
        }
        $('#offenseOrderNum').data('currentnum', maxNum);
        $('#offenseOrderNum').TouchSpin({
            min: 1,
            max: offenseOrderObj.max_order,
            stepinterval: 1,
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',
            verticalbuttons: true,
            verticalupclass: 'la la-plus',
            verticaldownclass: 'la la-minus',
            prefix: 'No.'
        });
        $('#offenseOrderNum').trigger("touchspin.updatesettings", {
            max: maxNum
        });
        callback(maxNum);
    });
}

function initOffenseModal(){
    $('#offenseCategLabelModal').data('offensecategoryid', $('#offenseCategoryLabel').data('offensecategoryid'));
    $('#offenseCategLabelModal').text($('#offenseCategoryLabel').text());
    getCurrentOffenseOrder($('#offenseCategoryLabel').data('offensecategoryid'), function (offenseNum) {
        $('#offenseOrderNum').val(parseInt(offenseNum));
    });
    // getAllOffenseCategory(function(offenseObj){
        getMultiOffenseTypeSelect('[0]', function (offenseCateg) {
            $('#offenseModal').modal('show');
        })
    // })
}

function initDefaultRecommendationNumber(){
    $.when(fetchGetData('/discipline/get_default_recommendation_number')).then(function (defaultNum) {
        var defaultRecNum = $.parseJSON(defaultNum.trim());
        var level = "level";
        if (parseInt(defaultRecNum) > 1) {
            level =  "levels";
        }
        $('#defaultRecommendationNum').text(defaultRecNum + " " + level);
    });
}

function getSpecificOffense(element){
    $.when(fetchPostData({
        offenseId: $(element).data('offenseid')
    }, '/discipline/get_specific_offense')).then(function (specificOffense) {
        var specificOffenseObj = $.parseJSON(specificOffense.trim());
        if (parseInt(specificOffenseObj.exist)) {
            $('#offenseCategLabelModal').data('offensecategoryid', specificOffenseObj.record.offenseType_ID);
            $('#offenseCategLabelModal').text($('#offenseCategoryLabel').text());
            $('#offenseDescription').val(specificOffenseObj.record.offense);
            $('#offenseDescription').data('orig',specificOffenseObj.record.offense);
            $('#addOffenseBtn').hide();
            $('#updateOffenseBtn').show();
            $('#offenseForm').data('formtype', 'edit');
            $('#offenseForm').data('offenseid', $(element).data('offenseid'));
            $('#offenseForm').data('multicateg', '['+specificOffenseObj.record.multiCategory+']');
            getCurrentOffenseOrder(specificOffenseObj.record.offenseType_ID, function (offenseNum) {
                $('#offenseOrderNum').val(parseInt(specificOffenseObj.record.orderNum));
                $('#offenseOrderNum').data('orig',parseInt(specificOffenseObj.record.orderNum));
            });
            getAllOffenseCategory(function (offenseObj) {
                $('#offenseCategoryLetter option').filter(function () {
                    return $(this).val() == specificOffenseObj.record.disciplineCategory_ID;
                }).prop('selected', true);
                $('#offenseCategoryLetter').data('orig', specificOffenseObj.record.disciplineCategory_ID)
                $('#offenseCategoryLetter').data('origLetter', $('#offenseCategoryLetter').find('option:selected').text())
                getMultiOffenseTypeSelect('[' + specificOffenseObj.record.multiCategory + ']', function (offenseCateg) {
                    $('#offenseModal').modal('show');
                })
            })
        } else {
            initOffenseDatatable($('#offenseCategoryLabel').data('offensecategoryid'));            
            swal("Record Does Not Exist!", "It seems that the Specific Offense record that you are about to update was removed. Please report this immediately to the System Administrator", "error");
        }
    });
}

function getSpecificRecommendationNum(element) {
    $.when(fetchPostData({
        recommendationNumId: $(element).data('recommendationnumid')
    }, '/discipline/get_specific_recommendation_num')).then(function (specificRecommendationNum) {
        var specificRecommendationNumObj = $.parseJSON(specificRecommendationNum.trim());
        $('#recommendationNumberModal').data('recommendationNumberId', specificRecommendationNumObj.record.accountRecommendationNumber_ID);
        $('#recommendationLevel').data('orig', specificRecommendationNumObj.record.number);
        $('#recommendationLevel').val(specificRecommendationNumObj.record.number);
        $('#addRecommendationNumBtn').hide();
        $('#updateRecommendationNumBtn').show();
        $('#recomendationNumberForm').data('formtype', 'edit');
        getAccountFilters($('#employeeTypeRecommendationNum').val(), '#accountRecommendationNumber', function (accExistStat) {
            $('#accountRecommendationNumber').val(specificRecommendationNumObj.record.account_ID);
            $('#accountRecommendationNumber').select2({
                placeholder: "Select an Account",
                width: '100%',
            }).trigger('change');
            $('#accountRecommendationNumber').prop('disabled', true);
            $('#recommendationNumberModal').modal('show');
        });

    });
}

function getOffenseTypeSelect(callback) {
    $.when(fetchGetData('/discipline/get_specific_offense_type')).then(function (offenseType) {
        var offenseTypeObj = $.parseJSON(offenseType.trim());
        var optionsAsString = "";
        if (parseInt(offenseTypeObj.exist) == 0) {
            optionsAsString += "";
        } else {
            optionsAsString += "<option value='0'>ALL</option>";
            $.each(offenseTypeObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.offenseType_ID + "'>" + value.letter.toUpperCase() + ". " + value.offenseType + "</option>";
            })
        }
        $('#defaultOffenseType').html(optionsAsString);
        $('#defaultOffenseType').select2({
            placeholder: "Select an Offense Type",
            width: '100%'
        });
        callback(offenseTypeObj);
    });
}

function offensesTab(){
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    $('.dms').find('[data-navcontent="codSettings"]').trigger('click');
    $('#offenseTab').trigger('click');
    swal.close();
}

function categoryTab(){
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    $('.dms').find('[data-navcontent="codSettings"]').trigger('click');
    $('#offenseCategoryTab').trigger('click');
    // swal.close();
    $('#defaultCategoryModal').modal('hide');
}

function initDefaultOffenseModal(){
    getOffenseTypeSelect(function(offenseType){
        console.log($('#defaultOffenseType').val());
        if ($('#defaultOffenseType').val()){
            getOffenseSelect($('#defaultOffenseType').val(), $('#offenseTypeVal').data('offenseid'), function (defaultOffense) {
                if (parseInt($('#offenseTypeVal').data('offensecategoryid')) !== 0) {
                    $('#defaultOffenseType').val($('#offenseTypeVal').data('offensecategoryid'))
                    $('#defaultOffenseType').select2({
                        placeholder: "Select an Offense Type",
                        width: '100%'
                    }).trigger('change');
                }
                $('#updateDefaultOffenseBtn').prop('disabled', true);
                $('#dtrViolationTypeDefOff').text($('#violationTypeVal').text());
                // $("#updateDefaultOffenseBtn").off('click');
                // $('updateDefaultOffenseBtn').addClass('disabled');
                $('#defaultOffenseModal').modal('show');
            })
        }else{
            swal("No Offenses Was Set!", "Please Add an Offense first.<br> <button type='button' class='btn btn-outline-accent btn-sm m-btn m-btn--icon'><span><i class='fa fa-info-circle'></i><span onclick=offensesTab()>ADD HERE</span></span></button>", "error");
        }
    })
}

function setAssignableCategory(callback){
    $.when(fetchGetData('/discipline/get_all_offense_category')).then(function (category) {
        var categoryObj = $.parseJSON(category.trim());
        var optionAsString = "<option disabled selected value></option>";
        console.log(categoryObj);
        if (parseInt(categoryObj.exist)) {
            $('#defaultCategoryAlert').hide();
            $.each(categoryObj.spec_offenses, function (index, value) {
                optionAsString += '<option value="' + value.disciplineCategory_ID + '" > Category ' + value.category.toUpperCase() + '</option>';
            })
        }else{
            $('#defaultCategoryAlert').fadeIn();
        }
        $('#defaultCategoryVal').html(optionAsString);
        callback(categoryObj);
    })
}

function initDefaultCategoryModal(callback){
    setAssignableCategory(function (categoryObj) {
        if ($('#categoryVal').data('categoryid') != 0){
            $('#defaultCategoryVal').val($('#categoryVal').data('categoryid'));
        }
        $('#dtrViolationTypeDefCateg').text($('#violationTypeVal').text());
        $('#defaultCategoryModal').modal('show');
    })
}

function getOffenseSelect(offenseCategoryIdVal, offenseIdVal, callback) {
    console.log(offenseIdVal);
    $.when(fetchPostData({
            offenseCategoryId: offenseCategoryIdVal
        }, '/discipline/get_default_offense')).then(function (defaultOffense) {
        var defaultOffenseObj = $.parseJSON(defaultOffense.trim());
        var optionsAsString = "";
        if (parseInt(defaultOffenseObj.exist) == 0) {
            optionsAsString += "";
        } else {
            optionsAsString += "<option></option>";
            $.each(defaultOffenseObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.offense_ID + "' data-offensecategoryid='" + value.offenseType_ID + "'>" + value.offense + "</option>";
            })
        }
        $('#defaultOffense').html(optionsAsString);
        $('#defaultOffense').select2({
            placeholder: "Select an Default Offense",
            width: '100%',
            allowClear: true
        });
        if (parseInt(offenseIdVal) !== 0){
            $('#defaultOffense').val(offenseIdVal);
            $('#defaultOffense').select2({
                placeholder: "Select an Default Offense",
                width: '100%',
                allowClear: true
            }).trigger('change');
        }

        $('#offenseFormGroup').show();
        callback(defaultOffenseObj);
    });
}


function getAccount(qipTypeVal, callback) {
    $.when(fetchPostData({
        class: qipTypeVal
    }, '/discipline/get_account')).then(function (account) {
        var accountObj = $.parseJSON(account.trim());
        callback(accountObj);
    });
}

function getAccountExist(qipTypeVal, callback){
    $.when(fetchPostData({
        class: qipTypeVal
    }, '/discipline/get_account')).then(function (account) {
        var accountObj = $.parseJSON(account.trim());
        callback(accountObj);
    });
}

function getRecommendationNumAssignableAccount(classVal, callback){
    $.when(fetchPostData({
        class: classVal
    }, '/discipline/get_assignable_acc_recommendation_num')).then(function (account) {
        var accountObj = $.parseJSON(account.trim());
        var optionsAsString = "";
        if (parseInt(accountObj.exist)){
            optionsAsString += "<option></option>";
            $.each(accountObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.acc_id + "'>" + value.acc_name + "</option>";
            })
        }else{
            optionsAsString += "<option></option>";
        }
        $("#accountRecommendationNumber").html(optionsAsString);
        $("#accountRecommendationNumber").select2({
                placeholder: "Select an Account",
                width: '100%',
            }); 
        callback(accountObj);
    });
}

function getDirectSupervisors(element, callback){
    $.when(fetchGetData('/discipline/get_direct_supervisors')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        var optionsAsString = "";
        if (parseInt(empObj.exist)) {
            optionsAsString += "<option></option>";
            $.each(empObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.lname) + ", " + toTitleCase(value.fname) + "</option>";
            });
        } else {
            optionsAsString += "<option></option>";
        }
        $(element).html(optionsAsString);
        $(element).select2({
            placeholder: "Select a Person",
            width: '100%',
        });
        callback(empObj);
    });
}

function getAssignableAccount(empIdVal, siteIdVal, callback) {
    $.when(fetchPostData({
        empId: empIdVal,
        siteId: siteIdVal
    }, '/discipline/get_assignable_account')).then(function (account) {
        var accountObj = $.parseJSON(account.trim());
        callback(accountObj);
    });
}

function assignableAccount(empIdVal, siteId, elementId, callback) {
    getAssignableAccount(empIdVal, siteId, function (accountObj) {
        var optionsAsString = "";
        var notEmpty = 1;
        if (parseInt(accountObj.exist) == 0) {
            notEmpty = 0;
        } else {
            optionsAsString += "<option val='0'></option>";
            $.each(accountObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.acc_id + "'>" + value.acc_name + "</option>";
            })
            $(elementId).html(optionsAsString);
        }
        $(elementId).select2({
            placeholder: "Select an Account",
            width: '100%',
            allowClear: true
        });
        callback(accountObj);
    })
}

function getSite(elementId, callback){
    $.when(fetchGetData('/discipline/get_site')).then(function (site) {
        var siteObj = $.parseJSON(site.trim());
        var optionsAsString = "<option></option>";
        var notEmpty = 1;
        if (parseInt(siteObj.record) == 0) {
            notEmpty = 0;
        } else {
            $.each(siteObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.site_ID + "'>" + value.code + "</option>";
            })
            $(elementId).html(optionsAsString);
        }
        $(elementId).select2({
            placeholder: "Select a site",
            width: '100%',
        });
        callback(siteObj);
    });
}

function getEroAccountAssignment(){
    var optionsAsString = "";
    var notEmpty = 1;
    if (parseInt(accountObj.record) == 0) {
        notEmpty = 0;
    } else {
        optionsAsString += "<option val='0'>All Accounts</option>";
        $.each(accountObj.record, function (index, value) {
            optionsAsString += "<option value='" + value.acc_id + "'>" + value.acc_name + "</option>";
        })
        $(elementId).html(optionsAsString);
    }
    $(elementId).select2({
        placeholder: "Select an Account",
        width: '100%',
    });
    callback(accountObj);
}

function getAccountFilters(employeeTypeVal, elementId, callback){
    getAccountExist(employeeTypeVal, function (accountObj) {
        var optionsAsString = "";
        var notEmpty = 1;
        if (parseInt(accountObj.record) == 0) {
            notEmpty = 0;
        } else {
            optionsAsString += "<option val='0'>All Accounts</option>";
            $.each(accountObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.acc_id + "'>" + value.acc_name + "</option>";
            })
            $(elementId).html(optionsAsString);
        }
        $(elementId).select2({
            placeholder: "Select an Account",
            width: '100%',
        });
        callback(accountObj);
    })
}

function getAccountWithoutAll(employeeTypeVal, elementId, callback) {
    getAccountExist(employeeTypeVal, function (accountObj) {
        var optionsAsString = "";
        var notEmpty = 1;
        if (parseInt(accountObj.record) == 0) {
            notEmpty = 0;
        } else {
            optionsAsString += "<option></option>";
            $.each(accountObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.acc_id + "'>" + value.acc_name + "</option>";
            })
            $(elementId).html(optionsAsString);
        }
        $(elementId).select2({
            placeholder: "Select an Account",
            width: '100%',
        });
        callback(accountObj);
    })
}

function getDefaultRecommendationLevel(callback){
    $.when(fetchPostData({
        accId: $('#accountAssignChanges').val()
    }, '/discipline/get_default_recommendation_number')).then(function (defaultLevel) {
        var defaultLevelObj = $.parseJSON(defaultLevel.trim());
        // console.log(recommendationLevelObj);
        callback(defaultLevelObj);
    });
}

function getAccountCustomLevel(element ,callback){
    $.when(fetchPostData({
        accId: $(element).val()
    }, '/discipline/get_specific_recommendation_level_by_account')).then(function (recommendationLevel) {
        var recommendationLevelObj = $.parseJSON(recommendationLevel.trim());
        // console.log(recommendationLevelObj);
        callback(recommendationLevelObj);
    });
}

function initTouchpin(element, maxVal){
    $(element).TouchSpin({
        buttondown_class: 'btn btn-secondary',
        buttonup_class: 'btn btn-secondary',
        min: 1,
        max: maxVal,
        stepinterval: 50,
        maxboostedstep: 10000000,
        prefix: 'Level'
    });
    $(element).trigger("touchspin.updatesettings", {
        max: maxVal
    });
    $(element).val(parseInt(maxVal));
}

function getSpecificAccountFilters(employeeTypeVal, elementId, callback){
    getAccountExist(employeeTypeVal, function (accountObj) {
        var optionsAsString = "";
        var notEmpty = 1;
        if (parseInt(accountObj.record) == 0) {
            notEmpty = 0;
        } else {
            optionsAsString += "<option></option>";
            $.each(accountObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.acc_id + "'>" + value.acc_name + "</option>";
            })
            $(elementId).html(optionsAsString);
        }
        $(elementId).select2({
            placeholder: "Select an Account",
            width: '100%',
            allowClear: true
        });
        callback(accountObj);
    })
}

function getAccountQipFilter(callback){
    getAccount($('#qipClassFilter').val(), function (accountObj) {
        var optionsAsString = "";
        var notEmpty = 1;
        if (parseInt(accountObj.record) == 0) {
            notEmpty = 0;
        } else {
            optionsAsString += "<option val='0'>All Accounts</option>";
            $.each(accountObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.acc_id + "'>" + value.acc_name + "</option>";
            })
            $('#qipAccountFilter').html(optionsAsString);
        }
        $('#qipAccountFilter').select2({
            placeholder: "Select an Account",
            width: '100%',
        });
        callback(accountObj);
    })
}

function getEmployee(classVal, accountVal, assTypeVal, elementId, callback) {
    $.when(fetchPostData({
        class: classVal,
        acc_id: accountVal,
        ass_type: assTypeVal
    }, '/discipline/get_emp')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        console.log(empObj);
        var optionsAsString = "<option></option>";
        var notEmpty = 1;
        if (parseInt(empObj.record) == 0) {
            notEmpty = 0;
        } else {
            $.each(empObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.lname + ", " +value.fname) + "</option>";
            })
            $(elementId).html(optionsAsString);
        }
        $(elementId).select2({
            placeholder: "Select an Employee",
            width: '100%',
            allowClear: true
        });
        
        callback(notEmpty);
    });
}

function getEmployeeMult(classVal, accountVal, assTypeVal, elementId, placeholderText, callback) {
    $.when(fetchPostData({
        class: classVal,
        acc_id: accountVal,
        ass_type: assTypeVal
    }, '/discipline/get_emp')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        console.log(empObj);
        var optionsAsString = "";
        var notEmpty = 1;
        if (parseInt(empObj.record) == 0) {
            notEmpty = 0;
        } else {
            $.each(empObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.lname + ", " +value.fname) + "</option>";
            })
            $(elementId).html(optionsAsString);
        }
        $(elementId).select2({
            placeholder: placeholderText,
            width: '100%',
            allowClear: true
        });
        callback(notEmpty);
    });
}

function getOffenseTypeAssignmentSelect(elementId, callback) {
     $.when(fetchPostData({
        accId: $('#accountOffenseType').val()
     }, '/discipline/get_offense_type_select')).then(function (offenseType) {
        var offenseTypeObj = $.parseJSON(offenseType.trim());
        var optionsAsString = "<option></option>";
        var notEmpty = 1;
        if (parseInt(offenseTypeObj.record) == 0) {
            notEmpty = 0;
        } else {
            $.each(offenseTypeObj.record, function (index, value) {
                optionsAsString += "<option value='" + value.offenseType_ID + "'>" + value.letter + ". " + value.offenseType + "</option>";
            })
            $(elementId).html(optionsAsString);
        }
        $(elementId).select2({
            placeholder: "Select Offense Type",
            width: '100%',
        });
        callback(offenseTypeObj);
    });
}

function initQipFilters(callback){
    getAccountFilters($('#employeeTypeQip').val(), '#accountQip', function (accExistStat) {
        getEmployee($('#employeeTypeQip').val(), $('#accountQip').val(), 'qip', '#employeeQip', function (empExistStat) {
            callback(empExistStat);
        })
    });
}

function initEroFilters(callback){
    getAccountFilters($('#employeeTypeEro').val(), '#accountEro', function (accExistStat) {
        getEmployee($('#employeeTypeEro').val(), $('#accountEro').val(), 'ero' , '#employeeEro', function (empExistStat) {
            callback(empExistStat);
        })
    });
}

function initOffenseTypeFilters(callback){
    getSpecificAccountFilters($('#employeeTypeOffenseType').val(), '#accountOffenseType', function (accExistStat) {
        callback(accExistStat);
    });
}
function initRecommendationNumberFilters(callback){
    getRecommendationNumAssignableAccount($('#employeeTypeRecommendationNum').val(), function (accExistStat) {
        callback(accExistStat);
    });
}


function initQipAssignmentModal(){
    // console.log($('#employeeTypeQip').val());
    initQipFilters(function(){
        $('#qipAssignmentModal').modal('show');
    })
}

function initEroAssignmentModal(){
    initEroFilters(function () {
        getSite('#eroSiteId', function (site) {
            assignableAccount($('#employeeEro').val(), $('#eroSiteId').val(), '#eroAccountId', function (assignableAccount) {
                $('#eroAssignmentModal').modal('show');
            })
            // callback(empExistStat);
        })
    })
}

function initOffenseTypeAssignmentModal(){
    initOffenseTypeFilters(function () {
        getOffenseTypeAssignmentSelect("#OffenseTypeSelect", function (offenseTypeObj) {
            $('#assignOffenseTypeModal').modal('show');
        })
    })
}

function initEroAssignmentModal2() {
    $('#employeeEro').val($('#eroNameInfo').data('empid'));
    // $('#defaultOffenseType').select2({
    //     placeholder: "Select an Offense Type",
    //     width: '100%'
    // }).trigger('change');
    $('#eroAccountsModal').modal('hide');
    initEroFilters(function () {
        getSite('#eroSiteId', function (site) {
            assignableAccount($('#employeeEro').val(), $('#eroSiteId').val(), '#eroAccountId', function (assignableAccount) {
                $('#eroAssignmentModal').modal('show');
            })
            // callback(empExistStat);
        })
    })
}

function initRecommendationNumberModal(){
    initRecommendationNumberFilters(function () {
        $('#recommendationNumberModal').modal('show');
    });
}

function initAssignChangeModal(){
    getDirectSupervisors("#empAssignChanges", function (empObj) {
        getAccountWithoutAll(0, '#accountAssignChanges', function (accExistStat) {
            getDefaultRecommendationLevel(function(defaultLevelObj){
                initTouchpin("#levelAssignChanges", defaultLevelObj);
                $('#assignChangeModal').modal('show');
            });
            getEmployee(0, 0, 0, '#empAssignToChanges', function (empExistStat) {
            });
        });
    })
}

function initAssignExemptModal(){
    getDirectSupervisors("#empAssignExemption", function (empObj) {
        getAccountWithoutAll(0, '#accountAssignExemption', function (accExistStat) {
            getDefaultRecommendationLevel(function(defaultLevelObj){
                initTouchpin("#levelAssignExemption", defaultLevelObj);
                $('#assignExemptionModal').modal('show');
            });
        });
    })
}

function checkIfQipRecordExist(callback){
    $.when(fetchGetData('/discipline/check_if_qip_record_exist')).then(function (record) {
        var recordObj = $.parseJSON(record.trim());
        callback(parseInt(recordObj.exist));
    });
}
function checkIfOffTypeRecordExist(callback){
    $.when(fetchGetData('/discipline/check_if_off_type_ass_record_exist')).then(function (record) {
        var recordObj = $.parseJSON(record.trim());
        callback(parseInt(recordObj.exist));
    });
}
function checkIfEroRecordExist(callback){
    $.when(fetchGetData('/discipline/check_if_ero_record_exist')).then(function (record) {
        var recordObj = $.parseJSON(record.trim());
        callback(parseInt(recordObj.exist));
    });
}
// END OF READ FUNCTIONS ----------------------------------------------------------------------------------

// UPDATE FUNCTIONS ---------------------------------------------------------------------------------------
function updateDisciplinaryAction() {
    $.when(fetchPostData({
        disciplinaryActionId: $('#disciplinaryActionForm').data('disciplinaryid'),
        action: $('#action').val(),
        abbr: $('#abreviation').val(),
        period: $('#prescreptivePeriod').val(),
    }, '/discipline/update_disciplinary_action')).then(function (updateActionStat) {
        var updateActionStatus = $.parseJSON(updateActionStat.trim());
        if (parseInt(updateActionStatus)) {
            toastr.success('Successfully Updated Disciplinary Action');
            getDisciplinaryAction();
            $('#disciplinaryActionModal').modal('hide');
        } else {
            toastr.error("Something went wrong while updating the Disciplinary Action. Please inform your system administrator");
        }
    });
}

function UpdateSpecificOffenseCategory(){
     $.when(fetchPostData({
         specificOffenseCategoryId: $('#offenseCategoryForm').data('specoffensecategoryid'),
         offenseCategoryVal: $('#offenseCategoryVal').val(),
     }, '/discipline/update_specific_offense_category')).then(function (updateSpecificOffenseCategory) {
         var updateSpecificOffenseCategoryStat = $.parseJSON(updateSpecificOffenseCategory.trim());
         if (parseInt(updateSpecificOffenseCategoryStat)) {
             toastr.success('Successfully Updated Specific Offense Category Description');
             getSpecOffenseCategory();
             $('#offenseCategoryModal').modal('hide');
         } else {
             toastr.error("Something went wrong while updating the Specific Offense Category Description. Please inform your system administrator");
         }
     });
}
function updateRecommendationNumber(){
     $.when(fetchPostData({
        accountRecommendationNumId: $('#recommendationNumberModal').data('recommendationNumberId'),
        recommendationNumberVal: $('#recommendationLevel').val(),
     }, '/discipline/update_account_recommendation_number')).then(function (updateAccountRecNum) {
        var updateAccountRecNumStat = $.parseJSON(updateAccountRecNum.trim());
        if (parseInt(updateAccountRecNumStat)) {
            toastr.success('Successfully Updated Recommendation Number');
            initRecommendationSettings(function(accountObj){
                $('#recommendationNumberModal').modal('hide');
            })
        } else {
            toastr.error("Something went wrong while updating the Recommendation Number. Please inform your system administrator");
        }
     });
}

function updateDefaultCategory(){
    $.when(fetchPostData({
        categoryId: $('#defaultCategoryVal').val(),
        violationType: $('#violationTypeVal').data('violationtypeid'),
    }, '/discipline/update_default_category')).then(function (updateCategory) {
        var updateCategoryStat = $.parseJSON(updateCategory.trim());
        if (parseInt(updateCategoryStat)) {
            toastr.success('Successfully Updated Default Category');
            initDtrViolationSettings($('#violationTypeVal').data('violationtypeid'));
            $('#defaultCategoryModal').modal('hide');
        } else {
            toastr.error("Something went wrong while updating the Default Category. Please inform your system administrator");
        }
    });
}

function rearrangeOffenseLevel(element, direction){
    var newLevelVal = 0;
    if(direction == 1){
        newLevelVal = $(element).data('level') - 1;
        exchangeLevelVal = $(element).data('level');
    } else if (direction == 2) {
        newLevelVal = $(element).data('level') + 1;
        exchangeLevelVal = $(element).data('level');
    }
    $.when(fetchPostData({
        newLevel: newLevelVal,
        exchangeLevel: exchangeLevelVal,
        categoryId: $(element).data('categoryid'),
        disciplinaryActionCategoryId: $(element).data('discactioncategid')
    }, '/discipline/rearrange_level')).then(function (rearrange) {
        var rearrangeStat = $.parseJSON(rearrange.trim());
        getOffenseCategory();
    });
}

function updateOffense(){
    var multiCategVal = "";
    var categList = [];
    if ($('#allCateg').prop("checked") == true) {
        multiCategVal = 'all';
        categList[0] = 'all';
    } else if ($('#allCateg').prop("checked") == false) {
        if ($('#categoryList').val() == '') {
            multiCategVal = 0;
            categList[0] = 0;
        } else {
            multiCategVal = $('#categoryList').val().toString();
            $('#categoryList').find('option:selected').each(function (index, value) {
                categList[index] = $(this).text().toLowerCase();
            });
        }
    }
    $.when(fetchPostData({
        offenseId: $('#offenseForm').data('offenseid'),
        offenseNum: $('#offenseOrderNum').val(),
        origOffenseNum: $('#offenseOrderNum').data('orig'),
        disciplineCategoryLetter: categList.toString(),
        offense: $('#offenseDescription').val(),
        offenseCategoryId: $('#offenseCategLabelModal').data('offensecategoryid'),
        multiCateg: multiCategVal
    }, '/discipline/update_offense')).then(function (update_offense) {
        var update_offenseStat = $.parseJSON(update_offense.trim());
        if (parseInt(update_offenseStat)) {
            toastr.success('Successfully Updated Offense');
            $('#offenseDatatable').mDatatable('reload');
            $('#offenseModal').modal('hide');
        } else {
            toastr.error("Something went wrong while updating the Offense. Please inform your system administrator");
        }
    });
}

function updateDtrViolationSetting(violationIdVal, field1Val, value1Val, field2Val, value2Val, successUpdateMssg, errUpdateMssg, callback) {
    console.log(violationIdVal);
    console.log(field1Val);
    console.log(value1Val);
    console.log(successUpdateMssg);
    console.log(errUpdateMssg);
    $.when(fetchPostData({
        violationId: violationIdVal,
        field1: field1Val,
        value1: value1Val,
        field2: field2Val,
        value2: value2Val
    }, '/discipline/update_dtr_violation_settings')).then(function (update_dtr_violation_settings) {
        var update_dtr_violation_settings_stat = $.parseJSON(update_dtr_violation_settings.trim());
        if (parseInt(update_dtr_violation_settings_stat)) {
            toastr.success('Successfully ' + successUpdateMssg);
        } else {
            toastr.error("Something went wrong while " + errUpdateMssg + ". Please inform your system administrator");
        }
        callback(update_dtr_violation_settings_stat);
    });
}

function updateIrDeadlineSetting(fieldVal, valueVal, successUpdateMssg, errUpdateMssg, callback) {
    $.when(fetchPostData({
        field: fieldVal,
        value: valueVal
    }, '/discipline/update_ir_deadline_settings')).then(function (update_ir_violation_settings) {
        var update_ir_violation_settings_stat = $.parseJSON(update_ir_violation_settings.trim());
        if (parseInt(update_ir_violation_settings_stat)) {
            toastr.success('Successfully ' + successUpdateMssg);
        } else {
            toastr.error("Something went wrong while " + errUpdateMssg + ". Please inform your system administrator");
        }
        callback(update_ir_violation_settings_stat);
    });
}

function updateEroSup(callback){
    $.when(fetchPostData({
        empId: $('#eroNameInfo').data('empid'),
        supId: $('#eroSup').find('option:selected').val()
    }, '/discipline/update_ero_supervisor')).then(function (update_ero_sup) {
        var update_ero_sup_stat = $.parseJSON(update_ero_sup.trim());
        if (parseInt(update_ero_sup_stat)) {
            toastr.success('Successfully changed DMS supervisor');
        } else {
            toastr.error("Something went wrong while changing the DMS supervisor. Please inform your system administrator");
        }
        $('#eroSup').data('origsup', $('#eroSup').find('option:selected').val());
        initEroDatatable();
        callback(update_ero_sup_stat);
    });
}
// END OF UPDATE FUNCTIONS ----------------------------------------------------------------------------------

// DELETE FUNCTIONS ----------------------------------------------------------------------------------
function deleteDisciplinaryAction(element) {
    $.when(fetchPostData({
        disciplinaryActionId: $(element).data('disciplinaryid')
    }, '/discipline/delete_disciplinary_action')).then(function (deleteDisciplinaryAction) {
        var deleteDisciplinaryActionStat = $.parseJSON(deleteDisciplinaryAction.trim());
        if (parseInt(deleteDisciplinaryActionStat)) {
            getDisciplinaryAction();
            swal("Delete Successful!", "You have successfully deleted the Disciplinary Action", "success");
        } else {
            swal("Delete Unsuccessful!", "Something went wrong while deleting the Disciplinary Action. Please report this immediately to the System Administrator", "error");
        }
    });
}

function deleteOffenseLevel(element) {
    $.when(fetchPostData({
        level: $(element).data('level'),
        disciplinaryActionCategoryId: $(element).data('discactioncategid'),
        categoryId: $(element).data('categoryid'),
    }, '/discipline/remove_offense_disciplinary_action')).then(function (removeOffenseLevel) {
       var removeOffenseLevelStat = $.parseJSON(removeOffenseLevel.trim());
       if (parseInt(removeOffenseLevelStat.delete_offense_level)){
          toastr.success('Successfully Removed Disciplinary Action');
       }else{
          toastr.error("Something went wrong while removing the Disciplinary Action. Please inform your system administrator");
       }
       getOffenseCategory();
    });
}

function deleteOffenseCategory(element){
    $.when(fetchPostData({
        categoryId: $(element).data('disciplinecategoryid'),
    }, '/discipline/delete_specific_category')).then(function (deleteOffenseCategory) {
        var deleteOffenseCategoryStat = $.parseJSON(deleteOffenseCategory.trim());
        if (parseInt(deleteOffenseCategoryStat.remove_category_levels) && parseInt(deleteOffenseCategoryStat.delete_offense_category)) {
            toastr.success('Successfully Deleted Category ' +$(element).data('category'));
        } else {
            toastr.error("Something went wrong while deleting the Category. Please inform your system administrator");
        }
        getOffenseCategory();
    });
}

function deleteSpecificOffenseCategory(element){
    $.when(fetchPostData({
        offenseCategoryId: $(element).data('offensecategoryid'),
    }, '/discipline/remove_specific_offense_type')).then(function (deleteSpecificOffenseCategory) {
        var specificOffenseCategoryStat = $.parseJSON(deleteSpecificOffenseCategory.trim());
        if (parseInt(specificOffenseCategoryStat.deactivate_offense_type_stat)) {
            toastr.success('Successfully Deleted Offense Type');
        } else {
            toastr.error("Something went wrong while deleting the Offense Type. Please inform your system administrator");
        }
        getSpecOffenseCategory();
    });
}

function deleteOffense(element){
    $.when(fetchPostData({
        offenseId: $(element).data('offenseid'),
        offenseCategoryId: $(element).data('offenseCategory')
    }, '/discipline/deactivate_offense_process')).then(function (deleteOffense) {
        var deleteOffenseStat = $.parseJSON(deleteOffense.trim());
        if (parseInt(deleteOffenseStat.deactivate_stat)) {
            toastr.success('Successfully Deleted Offense');
        } else {
            toastr.error("Something went wrong while deleting the Offense. Please inform your system administrator");
        }
        initOffenseDatatable($('#offenseCategoryLabel').data('offensecategoryid'))
        initSelectOffenseCategory($('#offenseCategoryLabel').data('offensecategoryid'));
    });
}

function deleteQipAssignment(element){
    $.when(fetchPostData({
        qipId: $(element).data('qipid')
    }, '/discipline/remove_qip_assignment')).then(function (deleteQip) {
        var deleteQipStat = $.parseJSON(deleteQip.trim());
        if (parseInt(deleteQipStat.remove_stat)) {
            toastr.success('Successfully remove QIP Privilege Filing from ' + toTitleCase($(element).data('fname') + ' ' + $(element).data('lname')));
        } else {
            toastr.error("Something went wrong while removing the QIP Assigment. Please inform your system administrator");
        }
        reloadQipDatatable();
        initAssignmentTabContent();
    });
}


function deleteEroAccount(element) {
    $.when(fetchPostData({
        eroId: $(element).data('eroid'),
        empId: $('#eroNameInfo').data('empid')
    }, '/discipline/remove_ero_account')).then(function (deleteEroAccount) {
        var deleteEroAccountStat = $.parseJSON(deleteEroAccount.trim());
        if (parseInt(deleteEroAccountStat.remove_stat)) {
            toastr.success('Successfully unassigned "' + toTitleCase($(element).data('accname')) + '" from ' + $('#eroNameInfo').text());
        } else {
            toastr.error("Something went wrong while unassigning the acccount. Please inform your system administrator");
        }
        reloadEroAccountDatatable($('#eroNameInfo').data('empid'));
        initAssignmentTabContent()
    });
}

function deleteEro(element) {
    $.when(fetchPostData({
        empId: $(element).data('empid')
    }, '/discipline/remove_employee_relations_officer')).then(function (deleteEro) {
        var deleteEroStat = $.parseJSON(deleteEro.trim());
        if (parseInt(deleteEroStat.remove_sup_stat) && parseInt(deleteEroStat.remove_ero_stat)) {
            toastr.success('Successfully removed "' + toTitleCase($(element).data('fname') + ' ' + $(element).data('lname'))+'" as an Employee Relations Officer.');
        } else {
            toastr.error("Something went wrong while removing an Employee Relations Officer. Please inform your system administrator");
        }
        reloadEroDatatable();
        initAssignmentTabContent()
    });
}

function deleteAssignedOffenseType(element) {
    $.when(fetchPostData({
        offTypeAssId: $(element).data('offtypeassid')
    }, '/discipline/remove_offense_type_by_offense_type_ass_id')).then(function (deleteOffType) {
        var deleteOffTypeStat = $.parseJSON(deleteOffType.trim());
        if (parseInt(deleteOffTypeStat.remove_stat)) {
            toastr.success('Successfully removed "' + $(element).data('offtype') + '".');
        } else {
            toastr.error("Something went wrong while removing the assigned Offense type. Please inform your system administrator");
        }
        initAssignmentTabContent();
        reloadAssignOffenseTypeDatatable($('#accNameInfo').data('accid'));
    });
}

function deleteAssignedOffenseTypeByAccId(element) {
    $.when(fetchPostData({
        accId: $(element).data('accid')
    }, '/discipline/remove_offense_type_by_acc_id')).then(function (deleteOffType) {
        var deleteOffTypeStat = $.parseJSON(deleteOffType.trim());
        if (parseInt(deleteOffTypeStat.remove_stat)) {
            toastr.success('Successfully removed "' + $(element).data('accname') + '" and all the offense type assigned to it.');
        } else {
            toastr.error("Something went wrong while removing the account in the offense type assignment. Please inform your system administrator");
        }
        initAssignmentTabContent();
        reloadOffenseTypeAssignDatatable();
    });
}

function removeRecommendationNum(element){
    $.when(fetchPostData({
        recommendationNumId: $(element).data('recommendationnumid'),
    }, '/discipline/remove_recommendation_num')).then(function (removeRecommendationNum) {
        var removeRecommendationNumStat = $.parseJSON(removeRecommendationNum.trim());
        if (parseInt(removeRecommendationNumStat.remove_stat)) {
            toastr.success('Successfully Deleted Recommendation Levels set for"' + $(element).data('accname') + '"');
        } else {
            toastr.error('Something went wrong while deleting the Recommendation Levels set for"' + $(element).data('accname') + '". Please inform your system administrator');
        }
        reloadRecommendationSettings();
    });
}

function removeCustomRecommendation(recIdVal, typeVal){
    $.when(fetchPostData({
        cusRecdId: recIdVal,
        type: typeVal
    }, '/discipline/remove_custom_recommendation')).then(function (removeCustomRec) {
        var removeCustomRecStat = $.parseJSON(removeCustomRec.trim());
        if (parseInt(removeCustomRecStat.remove_stat)) {
            toastr.success('Successfully removed Custom Recommendation Change');
        } else {
            toastr.error('Something went wrong while deleting the Custom Recommendation Changes". Please inform your system administrator');
        }
        initRecommendationInfoChanges($('#recommenderName').data('empid'), typeVal)
    });
}
// END OF DELETE FUNCTIONS ----------------------------------------------------------------------------------

// CONFIRMATION FUNCTIONS -----------------------------------------------------------------------------------
function confirmUpdateDisciplinaryAction() {
    swal({
        title: 'Are you sure you want to update the record?',
        text: "You will not be able to revert back the current record.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Update it!',
        cancelButtonText: 'No, Don\'t Update',
        reverseButtons: true,
        // width: '500px'
    }).then(function (result) {
        if (result.value) {
            updateDisciplinaryAction();
        } else if (result.dismiss === 'cancel') {
            $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
        }
    });
}

function confirmUpdateOffenseCategory(){
    if ($('#offenseCategoryVal').data('orig') == $('#offenseCategoryVal').val()){
        swal("No Changes made!",  "Your edited version is the same with the current record. Please double check", "info");
        $('#offenseCategoryForm').formValidation('disableSubmitButtons', false);
    }else{
        swal({
            title: 'Are you sure you want to update the Offense Category Type?',
            text: "Update \"" + $('#offenseCategoryVal').data('orig') + "\" to \"" + $('#offenseCategoryVal').val() + "\". You will not be able to revert back the record after the update.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Update it!',
            cancelButtonText: 'No, Don\'t Update',
            reverseButtons: true,
            // width: '500px'
        }).then(function (result) {
            if (result.value) {
                UpdateSpecificOffenseCategory();
            } else if (result.dismiss === 'cancel') {
                $('#offenseCategoryForm').formValidation('disableSubmitButtons', false);
            }
        });
    }
}

function confirmUpdateRecommendationNumber(){
    if (parseInt($('#recommendationLevel').data('orig')) == $('#recommendationLevel').val()) {
        swal("No Changes made!",  "Your edited version is the same with the current record. Please double check", "info");
        $('#recomendationNumberForm').formValidation('disableSubmitButtons', false);
    }else{
        swal({
            title: 'Are you sure you want to update the Recommendation Number of ' + $('#accountRecommendationNumber').find('option:selected').text() + '?',
            html: "Update \"" + $('#recommendationLevel').data('orig') + "\" to \"" + $('#recommendationLevel').val() + "\".<br><br> You will not be able to revert back the record after the update.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Update it!',
            cancelButtonText: 'No, Don\'t Update',
            reverseButtons: true,
            // width: '500px'
        }).then(function (result) {
            if (result.value) {
                updateRecommendationNumber();
            } else if (result.dismiss === 'cancel') {
                $('#recomendationNumberForm').formValidation('disableSubmitButtons', false);
            }
        });
    }
}

function confirmUpdateDefaultCategory(){
    if (parseInt($('#categoryVal').data('categoryid')) == $('#defaultCategoryVal').val()) {
        swal("No Changes made!",  "Your edited version is the same with the current record. Please double check", "info");
        $('#defaultCategoryForm').formValidation('disableSubmitButtons', false);
    }else{
        console.log($('#defaultCategoryVal').find('option:selected').text());
        swal({
            title: 'Are you sure you want to change the Default Category to ' + $('#defaultCategoryVal').find('option:selected').text() + '?',
            html: "You will not be able to revert back the record after the update.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Update it!',
            cancelButtonText: 'No, Don\'t Update',
            reverseButtons: true,
            // width: '500px'
        }).then(function (result) {
            if (result.value) {
                updateDefaultCategory();
            } else if (result.dismiss === 'cancel') {
                $('#defaultCategoryForm').formValidation('disableSubmitButtons', false);
            }
        });
    }
}

function confirmDeleteDisciplinaryAction(element) {
    $.when(fetchPostData({
        disciplinaryActionId: $(element).data('disciplinaryid')
    }, '/discipline/check_if_disc_action_has_category')).then(function (checkDisciplinaryAction) {
        var checkDisciplinaryActionStat = $.parseJSON(checkDisciplinaryAction.trim());
        if (parseInt(checkDisciplinaryActionStat.exist)) {
             var category = "ies";
             var categoryLink = "are"
             if (parseInt(checkDisciplinaryActionStat.disciplinary_action_count) == 1) {
                 var offense = "y";
                 var offenseLink = "is"
             }
            swal("Deleting this Disciplinary Action is prohibited!", checkDisciplinaryActionStat.disciplinary_action_count + " Offense Categor" + category + " "+categoryLink+" assigned with this Displinary Action", "error");
        } else {
            swal({
                title: 'Are you sure you want to delete the record?',
                text: "You will not be able to revert back the record after deletion.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete it!',
                cancelButtonText: 'No, Don\'t Delete',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    deleteDisciplinaryAction(element);
                } else if (result.dismiss === 'cancel') {
                    $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
                }
            });
        }
    });
}

function confirmDeleteOffenseCategory(element) {
    $.when(fetchPostData({
        categoryId: $(element).data('disciplinecategoryid')
    }, '/discipline/check_if_category_has_offenses')).then(function (hasOffenses) {
        var hasOffensesStat = $.parseJSON(hasOffenses.trim());
        // if (parseInt(hasOffensesStat.exist)) {
        //     var offense = "s";
        //     var offenseLink = "are"
        //     if(parseInt(hasOffensesStat.offense_count) == 1){
        //         var offense = "";
        //         var offenseLink = "is"
        //     }
        //     swal("Deleting Category " + $(element).data('category') + " is Prohibited", hasOffensesStat.offense_count + " Specific Offense"+offense+" "+offenseLink+" assigned with this category", "error");
        // } else {
            swal({
                title: 'Are you sure you want to Delete Category ' + $(element).data('category') + '?',
                text: "You will not be able to revert back this category and all the disciplinary actions assigned to it.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete it!',
                cancelButtonText: 'No, Don\'t Delete',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    deleteOffenseCategory(element);
                } else if (result.dismiss === 'cancel') {
                    // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
                }
            });
        // }
    });
    
}
function confirmDeleteSpecificOffenseCategory(element) {
    $.when(fetchPostData({
        offenseCategoryId: $(element).data('offensecategoryid')
    }, '/discipline/check_if_category_type_has_offenses'), fetchPostData({
    offenseTypeId: $(element).data('offensecategoryid')
    }, '/discipline/check_offense_type_is_used')).done(function (hasOffenses, offenseTypeUsed) {
        var hasOffensesStat = $.parseJSON(hasOffenses[0].trim());
        var offenseTypeUsedStat = $.parseJSON(offenseTypeUsed[0].trim());
        if (parseInt(offenseTypeUsedStat.exist)) {
            swal(
                'Deactivate Action is Forbidden!',
                'You are not allowed to deactive this offense type because some of its offense descriptions are being use on some ongoing Incident Reports!',
                'error'
            )
        }else{
            var hasOffenses = "";
            var textStr = "This Offense Type will be permanently deleted."
            if (parseInt(hasOffensesStat.exist)) {
                hasOffenses = 'This Offense Type has ' + hasOffensesStat.offense_count + ' Specific Offense Description.';
                textStr = "All the offense description under this Offense Type will also be deactivated permanently."
            }
            swal({
                title: hasOffenses + ' Are you sure you want to deactivate this Offense Type?',
                text: textStr,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Deactivate!',
                cancelButtonText: 'Don\'t Deactivate',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    deleteSpecificOffenseCategory(element);
                } else if (result.dismiss === 'cancel') {}
            });
        }
    });
}

function confirmUpdateOffense(){
    var nochange = 0;
    var nochange2 = 0;
    // if (($('#offenseOrderNum').data('orig') == $('#offenseOrderNum').val()) && ($('#offenseCategoryLetter').data('orig') == $('#offenseCategoryLetter').val()) && ($('#offenseDescription').data('orig') == $('#offenseDescription').val())) {
    if (($('#offenseOrderNum').data('orig') == $('#offenseOrderNum').val()) && ($('#offenseDescription').data('orig') == $('#offenseDescription').val())) {
        nochange = 1;
    }

    if ($('#offenseForm').data('multicateg') == '[all]'){
        if ($('#allCateg').prop("checked") == true){
            nochange2 = 1;
        }
    } else if (parseInt($('#offenseForm').data('multicateg')) == '[0]'){
        if (($('#categoryList').val() == '') && ($('#allCateg').prop("checked") == false)) {
            nochange2 = 1;
        }
    }else{
        if ($('#categoryList').val() !== ''){
            if (($('#categoryList').val().length === JSON.parse($('#offenseForm').data('multicateg')).map(String).length && $('#categoryList').val().sort().every(function (value, index) {
                    return value === JSON.parse($('#offenseForm').data('multicateg')).map(String).sort()[index]
                })) && ($('#allCateg').prop("checked") == false)) {
                nochange2 = 1;
            }
        }
    }

    if (nochange && nochange2){
        swal("No Changes made!", "Your edited version is the same with the current record. Please double check", "info");
        $('#offenseForm').formValidation('disableSubmitButtons', false);
    } else {
        var changes = "";
        if ($('#offenseOrderNum').data('orig') != $('#offenseOrderNum').val()){
            changes += "<i>- Reorder from \"" + $('#offenseOrderNum').data('orig') + "\" to \"" + $('#offenseOrderNum').val() + "\" </i><br>";
        }
        // if ($('#offenseCategoryLetter').data('orig') != $('#offenseCategoryLetter').val()){
        //     changes += "<i>- Set Category from \"" + $('#offenseCategoryLetter').data('origLetter') + "\" to \"" + $('#offenseCategoryLetter').find('option:selected').text() + "\" </i><br>";
        // }
        if ($('#offenseDescription').data('orig') != $('#offenseDescription').val()){
            changes += "<i>- Modify Offense from \"" + $('#offenseDescription').data('orig') + "\" to \"" + $('#offenseDescription').val() + "\" </i><br>";
        }
        swal({
            title: 'Are you sure you want to update the Offense?',
            html: changes +"<br>You will not be able to revert back the changes after the update.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Update it!',
            cancelButtonText: 'No, Don\'t Update',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                updateOffense();
            } else if (result.dismiss === 'cancel') {
                $('#offenseForm').formValidation('disableSubmitButtons', false);
            }
        });
    }
}

function confirmDeleteOffense(element) {
    $.when(fetchPostData({
        offenseId: $(element).data('offenseid')
    }, '/discipline/get_specific_offense'), 
    fetchPostData({ offenseId: $(element).data('offenseid')
    }, '/discipline/check_offense_is_used')).done(function (checkOffense, offenseIsUsed) {
        var checkOffenseStat = $.parseJSON(checkOffense[0].trim());
        var offenseIsUsedStat = $.parseJSON(offenseIsUsed[0].trim());
        console.log()
        if (parseInt(offenseIsUsedStat.exist)) {
            swal(
                'Deactivate Action is Forbidden!',
                'You are not allowed to deactive this offense because there are ongoing Incident Reports that are currently using this offense!',
                'error'
            )
        }else{
            if (parseInt(checkOffenseStat.exist)) {
                swal({
                    title: 'Are you sure you want to deactivate this offense?',
                    html: "\"<i>" + $(element).data('offense') + "\"</i><br><br> You will not be able to revert back the record after deactivation.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, Deactivate!',
                    cancelButtonText: 'Don\'t Deactivate',
                    reverseButtons: true,
                    // width: '500px'
                }).then(function (result) {
                    if (result.value) {
                        deleteOffense(element);
                    } else if (result.dismiss === 'cancel') {
                        // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
                    }
                });
            } else {
                swal("Record Does Not Exist!", "It seems that the offense record you are about to deactivate was removed. Please report this immediately to the System Administrator", "error");
            }
        }
    });
}

function confirmRemoveQip(element){
    $.when(fetchPostData({
        qipId: $(element).data('qipid')
    }, '/discipline/get_specific_qip')).then(function (qip) {
        var qipStat = $.parseJSON(qip.trim());
        if (parseInt(qipStat.exist)) {
            swal({
                title: 'Are you sure you want to remove QIP Filing Privilege from ' + toTitleCase($(element).data('fname') + ' ' + $(element).data('lname')) + '?',
                html: "You will not be able to revert back the record after deletion.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Remove it!',
                cancelButtonText: 'No, Don\'t Delete',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    deleteQipAssignment(element);
                } else if (result.dismiss === 'cancel') {
                    // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
                }
            });
        } else {
            swal("Record Does Not Exist!", "It seems that the record you are about to remove does not exist. Please report this immediately to the System Administrator", "error");
        }
    });
}

function confirmRemoveEroAccount(element) {
    $.when(fetchPostData({
        eroId: $(element).data('eroid')
    }, '/discipline/get_specific_ero_account')).then(function (eroAccount) {
        var eroAccountStat = $.parseJSON(eroAccount.trim());
        if (parseInt(eroAccountStat.exist)) {
            swal({
                title: 'Are you sure you want to unassign <i>"' + toTitleCase($(element).data('accname')) + '"</i><br> from <br>' + $('#eroNameInfo').text() + '?',
                html: "You will not be able to revert back the record after deletion.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes!',
                cancelButtonText: 'No!',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    deleteEroAccount(element);
                } else if (result.dismiss === 'cancel') {
                    // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
                }
            });
        } else {
            reloadEroAccountDatatable($('#eroNameInfo').data('empid'));
            swal("Record Does Not Exist!", "It seems that the record you are about to remove does not exist. Please report this immediately to the System Administrator", "error");
        }
    });
}
function confirmRemoveEro(element) {
    $.when(fetchPostData({
        empId: $(element).data('empid')
    }, '/discipline/get_specific_ero_account_by_emp')).then(function (ero) {
        var eroStat = $.parseJSON(ero.trim());
        var accPlural = "";
        var accLink = "is";
        if (parseInt(eroStat.exist)>1){
            accPlural = "s";
            accLink = "are";
        }
        if (parseInt(eroStat.exist) !== 0) {
            var htmlMssg ="";
            if (parseInt(eroStat.exist)> 0) {
                htmlMssg = "There " + accLink + " " + eroStat.exist + " account" + accPlural + " assigned to this officer. It will be unassigned after officer removal";
            }else{
                htmlMssg = "You will not be able to revert back the record after deletion.";
            }
            swal({
                title: 'Are you sure you want remove <br><i>"' + toTitleCase($(element).data('fname') + ' ' + $(element).data('lname')) + '"</i><br> as an Employee Relations Officer?',
                html: htmlMssg,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes!',
                cancelButtonText: 'No!',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    deleteEro(element);
                } else if (result.dismiss === 'cancel') {
                    // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
                }
            });
        } else {
            reloadEroDatatable();
            swal("Record Does Not Exist!", "It seems that the record you are about to remove does not exist. Please report this immediately to the System Administrator", "error");
        }
    });
}

function confirmRemoveAssignedOffenseTypeByAccId(element) {
    $.when(fetchPostData({
        accId: $(element).data('accid')
    }, '/discipline/get_specific_assigned_offense_type_by_acc_id')).then(function (offType) {
        var offTypeStat = $.parseJSON(offType.trim());
        var offTypeLink = "is";
        if (parseInt(offTypeStat.exist) > 1) {
            offTypeLink = "are";
        }
        if (parseInt(offTypeStat.exist) !== 0) {
            var htmlMssg = "";
            if (parseInt(offTypeStat.exist) > 0) {
                htmlMssg = "There " + offTypeLink + " " + offTypeStat.exist + " Offense Type assigned to this account. It will be unassigned after officer removal";
            } else {
                htmlMssg = "You will not be able to revert back the record after deletion.";
            }
            swal({
                title: 'Are you sure you want remove <br><i>"' + toTitleCase($(element).data('accname')) + '"</i><br> and all the offense type assigned to this account?',
                html: htmlMssg,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes!',
                cancelButtonText: 'No!',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    deleteAssignedOffenseTypeByAccId(element);
                } else if (result.dismiss === 'cancel') {
                    // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
                }
            });
        } else {
            reloadOffenseTypeAssignDatatable();
            swal("Record Does Not Exist!", "It seems that the record you are about to remove does not exist. Please report this immediately to the System Administrator", "error");
        }
    });
}

function confirmRemoveAssignedOffenseType(element) {
    $.when(fetchPostData({
        offTypeAssId: $(element).data('offtypeassid')
    }, '/discipline/get_specific_assigned_offense_type')).then(function (offType) {
        var offTypeStat = $.parseJSON(offType.trim());
        if (parseInt(offTypeStat.exist) !== 0) {
            swal({
                title: 'Are you sure you want unassign <br><i>"' + $(element).data('offtype') + '"</i><br> from ' + $('#accNameInfo').text() + '?',
                html: "You will not be able to revert back the record after deletion.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes!',
                cancelButtonText: 'No!',
                reverseButtons: true,
                // width: '500px'
            }).then(function (result) {
                if (result.value) {
                    deleteAssignedOffenseType(element);
                } else if (result.dismiss === 'cancel') {
                    // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
                }
            });
        } else {
            reloadEroDatatable();
            swal("Record Does Not Exist!", "It seems that the record you are about to remove does not exist. Please report this immediately to the System Administrator", "error");
        }
    });
}

function confirmUpdateEroSup(){
    // console.log($('#eroSup').find('option:selected').text());
    // console.log($('#eroSup').find('option[value="' + $('#eroSup').data('origsup') + '"]').text());
    // console.log($('#eroSup').find('option:selected').val());
    // console.log($('#eroSup').data('origsup'));
    if (parseInt($('#eroSup').find('option:selected').val()) !== parseInt($('#eroSup').data('origsup'))){
        swal({
            title: 'Are you sure you want change the DMS supervisor from <br><i>"' + toTitleCase($('#eroSup').find('option[value="' + $('#eroSup').data('origsup') + '"]').text()) + '"</i><br> to <br><i> "' + $('#eroSup').find('option:selected').text() + '"</i> ?',
            html: 'You will not be able to revert back the current record.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!',
            reverseButtons: true,
            allowOutsideClick: false
            // width: '500px'
        }).then(function (result) {
            if (result.value) {
                updateEroSup(function(updateEroSupStat){});
            } else if (result.dismiss === 'cancel') {
                $('#eroSup').val($('#eroSup').data('origsup'));
                $('#eroSup').select2({
                    placeholder: "Select an Employee",
                    width: '100%'
                }).trigger('change');
                // $('#disciplinaryActionForm').formValidation('disableSubmitButtons', false);
            }
        });
    }
}

function confirmRemoveRecommendationNum(element) {
    $.when(fetchPostData({
        recommendationNumId: $(element).data('recommendationnumid')
    }, '/discipline/check_if_account_recommendation_num_exist')).then(function (recNumExist) {
        var recNumExistStat = $.parseJSON(recNumExist.trim());
        if (parseInt(recNumExistStat.exist)) {
            swal({
                title: 'Are you sure you want to remove the recommendation number set for "' + $(element).data('accname') + '"?',
                text: 'You will not be able to revert back the current record.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Remove!',
                cancelButtonText: 'Don\'t Remove',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    removeRecommendationNum(element);
                } else if (result.dismiss === 'cancel') {}
            });
        }else{
            toastr.error("The custom recommendation number does not exist. Please inform your system administrator");
        }
    });
}

function confirmRemoveCustomChanges(element) {
    $.when(fetchPostData({
        cusRecId: $(element).data('recid')
    }, '/discipline/check_if_custom_recommendation_exist')).then(function (cusRecExist) {
        var cusRecExistStat = $.parseJSON(cusRecExist.trim());
        if (parseInt(cusRecExistStat.exist)) {
            var option = "";
            if ($(element).data('type') == "change"){   
                option = "this reassignment option";
                assignTo = "<br>Assigned To: " + $(element).data('fullname');
            }else{
                option = "this exemption option";
                assignTo = "";
            }
            console.log(cusRecExistStat.record);
            swal({
                title: 'Are you sure you want to remove '+option+'?',
                html: 'Account: ' + toTitleCase(cusRecExistStat.record.acc_name) + '<br> Recommmendation Level:' + cusRecExistStat.record.level + '' + assignTo,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Remove!',
                cancelButtonText: 'Don\'t Remove',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    removeCustomRecommendation($(element).data('recid'), $(element).data('type'));
                } else if (result.dismiss === 'cancel') {}
            });
        } else{
            toastr.error("The Custom Recommendation Change you are about to remove does not exist. Please inform your system administrator");
            if ($(element).data('type') == "change") {
                initReassignmentDatatable('reload', $('#recommenderName').data('empid'));
                initRecommendationInfoChanges($('#recommenderName').data('empid'), 'change');
            }else{
                initExemptionDatatable('reload', $('#recommenderName').data('empid'));
                initRecommendationInfoChanges($('#recommenderName').data('empid'), 'exempt');
            }
        }
    });
}

function check_if_def_offense_exist(){
    console.log($('#violationTypeVal').data('violationtypeid'));
    console.log($('#offenseVal').val());
}
// END OF CONFIRMATION FUNCTIONS -----------------------------------------------------------------------------------

// VALIDATION FUNCTIONS --------------------------------------------------------------------------------------------

function oneTimeChange(element){
    var errPopover = 0;
    var errText = "";
    if ($(element).val() == ''){
        errPopover = 1;
        errText = "Time Duration is Required"
    }else{
        if(parseInt($(element).val()) > 1000){
            errPopover = 1;
            errText = "You have exceeded the maximum (1000)";
        } else if (parseInt($(element).val()) < 0) {
            errPopover = 1;
            errText = "You have exceeded the minimum (0)";
        }
    }
    if (errPopover){
        $('.popoverErr').show();
        $('.popoverErr').text(errText);
        $('.submitBtn').addClass('disabled');
        $('.submitBtn').prop('disabled', true);
    }else{
        $('.popoverErr').hide();
        $('.popoverErr').text("");
        $('.submitBtn').removeClass('disabled');
        $('.submitBtn').prop('disabled', false);
    }
}

function consecChange(element) {
    var errPopover = 0;
    var errText = "";
    if ($(element).val() == '') {
        errPopover = 1;
        errText = "Number of Occurence is Required"
    } else {
        if (parseInt($(element).val()) > 10) {
            errPopover = 1;
            errText = "You have exceeded the maximum (10)";
        } else if (parseInt($(element).val()) < 2) {
            errPopover = 1;
            errText = "You have exceeded the minimum (2)";
        }
    }
    if (errPopover) {
        $('.popoverErr').show();
        $('.popoverErr').text(errText);
        $('.submitBtn').addClass('disabled');
        $('.submitBtn').prop('disabled', true);
    } else {
        $('.popoverErr').hide();
        $('.popoverErr').text("");
        $('.submitBtn').removeClass('disabled');
        $('.submitBtn').prop('disabled', false);
    }
}

function nonConsecChange(element) {
    var errPopover = 0;
    var errText = "";
    if ($(element).val() == '') {
        errPopover = 1;
        errText = "Accumulated Time is Required"
    } else {
        if (parseInt($(element).val()) > 1000) {
            errPopover = 1;
            errText = "You have exceeded the maximum (1000)";
        } else if (parseInt($(element).val()) < 1) {
            errPopover = 1;
            errText = "You have exceeded the minimum (1)";
        }
    }
    if (errPopover) {
        $('.popoverErr').show();
        $('.popoverErr').text(errText);
        $('.submitBtn').addClass('disabled');
        $('.submitBtn').prop('disabled', true);
    } else {
        $('.popoverErr').hide();
        $('.popoverErr').text("");
        $('.submitBtn').removeClass('disabled');
        $('.submitBtn').prop('disabled', false);
    }
}

function fixedDurationsChange(element){
    var errPopover = 0;
    var errText = "";
    if ($(element).parents('form').find('input').val() == '') {
        errPopover = 1;
        errText = "Fixed Duration value is required"
    } else {
        // var startUnit = "";
        // var addUnit = "";
        // Used for Fixed Durations Calculations
        // if ($('#fixedDurationUnit').val() == "week"){
        //         startUnit = "isoWeek";
        //         addUnit = "weeks";
        // } else {
        //         startUnit = "month";
        //         addUnit = "months";
        // }
        // console.log(moment(moment('01/10/2019', 'MM/DD/YYYY').startOf(startUnit).format('MM/DD/YYYY'), 'MM/DD/YYYY').add(parseInt($(element).val()), addUnit).format('MM/DD/YYYY'));
        if (parseInt($(element).parents('form').find('input').val()) > 12) {
            errPopover = 1;
            errText = "You have exceeded the maximum (12)";
        } else if (parseInt($(element).parents('form').find('input').val()) < 1) {
            errPopover = 1;
            errText = "You have exceeded the minimum (1)";
        }
    }
    
    if (errPopover) {
        $('.popoverErr').show();
        $('.popoverErr').text(errText);
        $('.submitBtn').addClass('disabled');
        $('.submitBtn').prop('disabled', true);
    } else {
        $('.popoverErr').hide();
        $('.popoverErr').text("");
        $('.submitBtn').removeClass('disabled');
        $('.submitBtn').prop('disabled', false);
    }
}

function customDurationChange(element) {
    var errPopover = 0;
    var errText = "";
    if ($(element).val() == '') {
        errPopover = 1;
        errText = "Number of days is required"
    } else {
        if (parseInt($(element).val()) > 365) {
            errPopover = 1;
            errText = "You have exceeded the maximum (365)";
        } else if (parseInt($(element).val()) < 1) {
            errPopover = 1;
            errText = "You have exceeded the minimum (1)";
        }
    }
    if (errPopover) {
        $('.popoverErr').show();
        $('.popoverErr').text(errText);
        $('.submitBtn').addClass('disabled');
        $('.submitBtn').prop('disabled', true);
    } else {
        $('.popoverErr').hide();
        $('.popoverErr').text("");
        $('.submitBtn').removeClass('disabled');
        $('.submitBtn').prop('disabled', false);
    }
}

function popOverSubmitBtn(element){
    var field2 = 0;
    var value2 = 0;
    var successUpdateMssg = "";
    var errUpdateMssg = "";
    var errPopover = 0;
    var errText = "";
    if ($(element).parents('form').attr('id') == "oneTimeForm"){
        successUpdateMssg = "updated One Time Occurence Value";
        errUpdateMssg = "updating One Time Occurence Value";
        if ($($(element).parents('form')).find('input').val() == '') {
            errPopover = 1;
            errText = "Time Duration is Required"
        } else {
            if (parseInt($($(element).parents('form')).find('input').val()) > 1000) {
                errPopover = 1;
                errText = "You have exceeded the maximum (1000)";
            } else if (parseInt($($(element).parents('form')).find('input').val()) < 0) {
                errPopover = 1;
                errText = "You have exceeded the minimum (0)";
            }
        }
    } else if ($(element).parents('form').attr('id') == "consecutiveForm") {
        successUpdateMssg = "updated Consecutive Occurence Value";
        errUpdateMssg = "updating Consecutive Occurence Value";
        if ($($(element).parents('form')).find('input').val() == '') {
            errPopover = 1;
            errText = "Number of Occurence is Required"
        } else {
            if (parseInt($($(element).parents('form')).find('input').val()) > 10) {
                errPopover = 1;
                errText = "You have exceeded the maximum (10)";
            } else if (parseInt($($(element).parents('form')).find('input').val()) < 2) {
                errPopover = 1;
                errText = "You have exceeded the minimum (2)";
            }
        }
    } else if ($(element).parents('form').attr('id') == "nonConsecutiveForm") {
        successUpdateMssg = "updated Non-Consecutive Occurence Value";
        errUpdateMssg = "updating Non-Consecutive Occurence Value";
        if ($($(element).parents('form')).find('input').val() == '') {
            errPopover = 1;
            errText = "Accumulated Time is Required"
        } else {
            if (parseInt($($(element).parents('form')).find('input').val()) > 1000) {
                errPopover = 1;
                errText = "You have exceeded the maximum (1000)";
            } else if (parseInt($($(element).parents('form')).find('input').val()) < 1) {
                errPopover = 1;
                errText = "You have exceeded the minimum (1)";
            }
        }
    } else if ($(element).parents('form').attr('id') == "nonConsecutiveIncForm") {
        successUpdateMssg = "updated Number of Occurence Value";
        errUpdateMssg = "updating Number of Occurence Value";
        if ($($(element).parents('form')).find('input').val() == '') {
            errPopover = 1;
            errText = "Number of Occurence is Required"
        } else {
            if (parseInt($($(element).parents('form')).find('input').val()) > 1000) {
                errPopover = 1;
                errText = "You have exceeded the maximum (1000)";
            } else if (parseInt($($(element).parents('form')).find('input').val()) < 1) {
                errPopover = 1;
                errText = "You have exceeded the minimum (1)";
            }
        }
    }else if ($(element).parents('form').attr('id') == "fixedDurationForm") {
        field2 = $('#fixedDurationUnit').data('field');
        value2 = $('#fixedDurationUnit').val();
        successUpdateMssg = "updated the Fixed Duration Value";
        errUpdateMssg = "updating the Fixed Duration Value";
        if ($(element).parents('form').find('input').val() == '') {
            errPopover = 1;
            errText = "Fixed Duration value is required"
        } else {
            if (parseInt($(element).parents('form').find('input').val()) > 12) {
                errPopover = 1;
                errText = "You have exceeded the maximum (12)";
            } else if (parseInt($(element).parents('form').find('input').val()) < 1) {
                errPopover = 1;
                errText = "You have exceeded the minimum (1)";
            }
        }
    } else if ($(element).parents('form').attr('id') == "customDurationForm") {
        successUpdateMssg = "updated the Custom Duration Value";
        errUpdateMssg = "updating the Custom Duration Value";
        if ($($(element).parents('form')).find('input').val() == '') {
            errPopover = 1;
            errText = "Number of days is required"
        } else {
            if (parseInt($($(element).parents('form')).find('input').val()) > 365) {
                errPopover = 1;
                errText = "You have exceeded the maximum (365)";
            } else if (parseInt($($(element).parents('form')).find('input').val()) < 1) {
                errPopover = 1;
                errText = "You have exceeded the minimum (1)";
            }
        }
    }
    // console.log($('#violationTypeVal').data('violationtypeid'));
    // console.log($($(element).parents('form')).find('input').data('field'));
    // console.log($($(element).parents('form')).find('input').val());
    // console.log(field2);
    // console.log(value2);
    // console.log(successUpdateMssg);
    // console.log(errUpdateMssg);
    // var successUpdateMssg = "set Prescriptive period to " + $('input[name="dtrPrescriptive"]:checked').parents('label').text().trim();
    // var errUpdateMssg = "setting Prescriptive period to " + $('input[name="dtrPrescriptive"]:checked').parents('label').text().trim();
    if (errPopover) {
        $('.popoverErr').show();
        $('.popoverErr').text(errText);
        $('.submitBtn').addClass('disabled');
    } else {
        //  console.log(field2);
        //  console.log(value2);
        updateDtrViolationSetting($('#violationTypeVal').data('violationtypeid'), $($(element).parents('form')).find('input').data('field'), $($(element).parents('form')).find('input').val(), field2, value2, successUpdateMssg, errUpdateMssg, function(dtrVioUpdateStat){
             initDtrViolationSettings($('#violationTypeVal').data('violationtypeid'));
        })
    }
}

function irDeadsPopOverSubmitBtn(element){
    var field2 = 0;
    var value2 = 0;
    var successUpdateMssg = "";
    var errUpdateMssg = "";
    var errPopover = 0;
    var errText = "";
    var deadlineUnit = "day";

    if ($($(element).parents('form')).find('input').val() == '') {
        errPopover = 1;
        errText = "Days Duration is Required"
    } else {
        if (parseInt($($(element).parents('form')).find('input').val()) > 30) {
            errPopover = 1;
            errText = "You have exceeded the maximum (30)";
        } else if (parseInt($($(element).parents('form')).find('input').val()) < 1) {
            errPopover = 1;
            errText = "You have exceeded the minimum (1)";
        }
    }
    if (errPopover) {
        $('.popoverErr').show();
        $('.popoverErr').text(errText);
        $('.submitBtn').addClass('disabled');
        $('.submitBtn').prop('disabled', true);
    } else {
        $('.popoverErr').hide();
        $('.popoverErr').text("");
        $('.submitBtn').removeClass('disabled');
        $('.submitBtn').prop('disabled', false);
        if ($(element).parents('form').attr('id') == 'dtrViolationDeadlineForm'){
            if(parseInt($($(element).parents('form')).find('input').val()) > 1){
                deadlineUnit = "days";
            }
            var successUpdateMssg = "set Deadline value to " + $($(element).parents('form')).find('input').val() + " " + deadlineUnit;
            var errUpdateMssg = "setting the deadline";
            updateDtrViolationSetting($('#deadlineDtrViolation').val(), 'daysDeadline', $($(element).parents('form')).find('input').val(), 0, 0, successUpdateMssg, errUpdateMssg, function (dtrVioUpdateStat) {
                initIrDeadline(1);
            })
            console.log('dtrVio')
        }else{
            console.log($($(element).parents('form')).find('input').attr('name'));
            if (parseInt($($(element).parents('form')).find('input').val()) > 1) {
                deadlineUnit = "days";
            }
            var successUpdateMssg = "set Deadline value to " + $($(element).parents('form')).find('input').val() + " " + deadlineUnit;
            var errUpdateMssg = "setting the deadline";
            updateIrDeadlineSetting($($(element).parents('form')).find('input').attr('name'), $($(element).parents('form')).find('input').val(), successUpdateMssg, errUpdateMssg, function(updateStat){
                initIrDeadline($($(element).parents('form')).find('input').data('deadlinetype'));
            })
        }
    }
    
}

function closePoper(){
    $('[data-toggle="popover"],[data-original-title]').popover('hide')
}

function showTooltip(element){
    $('.offenseCategoryAction').tooltip('hide');
    $(element).tooltip({
        html: true,
        title: $(element).data('offensetype'),
        trigger: "hover focus"
    });
    $(element).tooltip('show')
}
// END OF VALIDATION FUNCTIONS --------------------------------------------------------------------------------------------


// FORMVALIDATION

$('#categoryForm').formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    //        live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        category: {
            validators: {
                notEmpty: {
                    message: 'Oops! Category label is required'
                },
                remote: {
                    message: 'This Category label is already used',
                    url: baseUrl + '/discipline/check_if_category_exist',
                    data: {
                        category: function () {
                            return $('#category').val();
                        }
                    },
                    type: 'POST'
                },
            }
        },
    }
}).on('success.form.fv', function (e, data) {
    e.preventDefault();
    $('#categoryForm').formValidation('disableSubmitButtons', true);
    if ($('#categoryForm').data('formtype') == "add") {
        addCategory();
    } else {
        confirmUpdateDisciplinaryAction();
    }
});

$('#offenseLevelForm')
    .find('[name="offenseDisciplinaryAction"]')
    .change(function (e) {
        $('#offenseLevelForm').formValidation('revalidateField', 'offenseDisciplinaryAction');
    })
    .end()
    .formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    //        live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        offenseDisciplinaryAction: {
            validators: {
                notEmpty: {
                    message: 'Oops! Disciplinary is required'
                },
            }
        },
    }
}).on('success.form.fv', function (e, data) {
    e.preventDefault();
    $('#offenseLevelForm').formValidation('disableSubmitButtons', true);
    addOffenseLevel()
});

$('#offenseCategoryForm').formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    //        live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        categoryLetter: {
            validators: {
                notEmpty: {
                    message: 'Oops! Category label is required'
                },
                remote: {
                    message: 'This Category label is already used',
                    url: baseUrl + '/discipline/check_if_offense_category_exist',
                    data: {
                        categoryLetter: function () {
                            return $('#category').val();
                        }
                    },
                    type: 'POST'
                },
            }
        },
        offenseCategoryVal: {
             validators: {
                 notEmpty: {
                     message: 'Oops! Category Description is required'
                 }
             }
        }
    }
}).on('success.form.fv', function (e, data) {
    e.preventDefault();
    $('#categoryForm').formValidation('disableSubmitButtons', true);
    if ($('#offenseCategoryForm').data('formtype') == "add") {
        addOffenseCategory();
    } else {
        confirmUpdateOffenseCategory();
    }
});

$('#disciplinaryActionForm').formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    //        live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        action: {
            validators: {
                notEmpty: {
                    message: 'Oops! Disciplinary Action is required'
                },
            }
        },
        abreviation: {
            validators: {
                notEmpty: {
                    message: 'Oops! Abbreviation is required'
                },
            }
        },
        prescreptivePeriod: {
            validators: {
                notEmpty: {
                    message: 'Oops! Prescriptive period is required'
                },
            }
        }
    }
}).on('success.form.fv', function (e, data) {
    e.preventDefault();
    $('#disciplinaryActionForm').formValidation('disableSubmitButtons', true);
    if ($('#disciplinaryActionForm').data('formtype') == "add") {
        addDisciplinaryAction();
    } else {
        confirmUpdateDisciplinaryAction();
    }
});

$('#offenseForm').formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    //        live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        offenseOrderNum: {
            validators: {
                notEmpty: {
                    message: 'Oops! Offense Number is required'
                },
            }
        },
        offenseDescription: {
            validators: {
                notEmpty: {
                    message: 'Oops! Offense Description is required'
                },
            }
        }
    }
}).on('success.form.fv', function (e, data) {
    e.preventDefault();
    $('#offenseForm').formValidation('disableSubmitButtons', true);
    if (($('#categoryList').val() == '') && ($('#allCateg').prop('checked') == false)){
        $('#categoryAlert').show();
    }else{
        if ($('#offenseForm').data('formtype') == "add") {
            addOffense();
        } else {
            confirmUpdateOffense();
        }
    }
});

$('#qipAssignmentForm')
    .find('[name="employeeQip"]')
    .change(function (e) {
        $('#qipAssignmentForm').formValidation('revalidateField', 'employeeQip');
    })
    .end()
    .formValidation({
        message: 'This value is not valid',
        excluded: ':disabled',
        //        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            employeeQip: {
                validators: {
                    notEmpty: {
                        message: 'Oops! EmployeeQip is Required'
                    },
                }
            },
        }
    }).on('success.form.fv', function (e, data) {
        e.preventDefault();
        $('#qipAssignmentForm').formValidation('disableSubmitButtons', true);
        assignQip();
    });

$('#eroAssignmentForm')
    .find('[name="employeeEro"]')
    .change(function (e) {
        $('#eroAssignmentForm').formValidation('revalidateField', 'employeeEro');
    })
    .end()
    .find('[name="eroSiteId"]')
    .select2()
    .change(function (e) {
        $('#eroAssignmentForm').formValidation('revalidateField', 'eroSiteId');
    })
    .end()
    .find('[name="eroAccountId"]')
    .change(function (e) {
        $('#eroAssignmentForm').formValidation('revalidateField', 'eroAccountId');
    })
    .end()
    .formValidation({
        message: 'This value is not valid',
        excluded: ':disabled',
        //        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            employeeEro: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Employee is Required'
                    },
                }
            },
            eroSiteId: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Site is Required'
                    },
                }
            },
            eroAccountId: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Account is Required'
                    },
                }
            }
        }
    }).on('success.form.fv', function (e, data) {
        e.preventDefault();
        $('#eroAssignmentForm').formValidation('disableSubmitButtons', true);
        assignEro();
    });

    $('#assignOffenseTypeForm')
        .find('[name="accountOffenseType"]')
        .change(function (e) {
            $('#assignOffenseTypeForm').formValidation('revalidateField', 'accountOffenseType');
        })
        .end()
        .find('[name="OffenseTypeSelect"]')
        .select2()
        .change(function (e) {
            $('#assignOffenseTypeForm').formValidation('revalidateField', 'OffenseTypeSelect');
        })
        .end()
        .formValidation({
            message: 'This value is not valid',
            excluded: ':disabled',
            //        live: 'disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                accountOffenseType: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! Employee is Required'
                        },
                    }
                },
                OffenseTypeSelect: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! Site is Required'
                        },
                    }
                }
            }
        }).on('success.form.fv', function (e, data) {
            e.preventDefault();
            $('#assignOffenseTypeForm').formValidation('disableSubmitButtons', true);
            assignOffenseType();
        });

    $('#recomendationNumberForm')
        .find('[name="accountRecommendationNumber"]')
        .change(function (e) {
            $('#recomendationNumberForm').formValidation('revalidateField', 'accountRecommendationNumber');
        })
        .end()
        .formValidation({
            message: 'This value is not valid',
            excluded: ':disabled',
            //        live: 'disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                accountRecommendationNumber: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! Account is required'
                        },
                    }
                },
                recommendationLevel: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! recommendation level is Required'
                        },
                    }
                }
            }
        }).on('success.form.fv', function (e, data) {
            e.preventDefault();
            $('#recomendationNumberForm').formValidation('disableSubmitButtons', true);
            if ($('#recomendationNumberForm').data('formtype') == "add") {
                assignRecommendationNumber();
            } else {
                confirmUpdateRecommendationNumber();
            }
        });

    $('#defaultCategoryForm').formValidation({
            message: 'This value is not valid',
            excluded: ':disabled',
            //        live: 'disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                defaultCategoryVal: {
                    validators: {
                        notEmpty: {
                            message: 'Oops! Default Category is required'
                        },
                    }
                },
            }
        }).on('success.form.fv', function (e, data) {
            e.preventDefault();
            $('#defaultCategoryForm').formValidation('disableSubmitButtons', true);
            confirmUpdateDefaultCategory();
        });

    $('#assignChangeForm')
    .find('[name="empAssignChanges"]')
    .select2()
    .change(function (e) {
        $('#assignChangeForm').formValidation('revalidateField', 'empAssignChanges');
    })
    .end()
    .find('[name="accountAssignChanges"]')
    .select2()
    .change(function (e) {
        $('#assignChangeForm').formValidation('revalidateField', 'accountAssignChanges');
    })
    .end()
    .find('[name="empAssignToChanges"]')
    .select2()
    .change(function (e) {
        $('#assignChangeForm').formValidation('revalidateField', 'empAssignToChanges');
    })
    .end()
    .formValidation({
        message: 'This value is not valid',
        excluded: ':disabled',
        //        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            empAssignChanges: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Recommender is required'
                    },  
                }
            },
            accountAssignChanges: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Account is Required'
                    },
                }
            },
            levelAssignChanges: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Recommendation Level is Required'
                    },
                }
            },
            empAssignToChanges: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Employee to be assigned to is Required'
                    },
                }
            }
        }
    }).on('success.form.fv', function (e, data) {
        e.preventDefault();
        $('#assignChangeForm').formValidation('disableSubmitButtons', true);
        checkIfCustomRecommendationChangeExist($('#accountAssignChanges').val(), $('#levelAssignChanges').val(), $('#empAssignChanges').val(), $('#empAssignToChanges').val(), 'change', '#existChangeAlert');
    });

    $('#assignExemptionForm')
    .find('[name="empAssignExemption"]')
    .select2()
    .change(function (e) {
        $('#assignExemptionForm').formValidation('revalidateField', 'empAssignExemption');
    })
    .end()
    .find('[name="accountAssignExemption"]')
    .select2()
    .change(function (e) {
        $('#assignExemptionForm').formValidation('revalidateField', 'accountAssignExemption');
    })
    .end()
    .formValidation({
        message: 'This value is not valid',
        excluded: ':disabled',
        //        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            empAssignExemption: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Recommender is required'
                    },
                }
            },
            accountAssignExemption: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Account is Required'
                    },
                }
            },
            levelAssignExemption: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Recommendation Level is Required'
                    },
                }
            }
        }
    }).on('success.form.fv', function (e, data) {
        e.preventDefault();
        $('#assignExemptionForm').formValidation('disableSubmitButtons', true);
        checkIfCustomRecommendationChangeExist($('#accountAssignExemption').val(), $('#levelAssignExemption').val(), $('#empAssignExemption').val(), 0, 'exempt', '#existExemptionAlert');
        // checkIfCustomRecommendationChangeExist($('#empAssignToChanges').val(), 'exempt');
    });

// EVENT TRIGGERS -------------------------------------------------------
$('#disciplinaryActionModal').on('hidden.bs.modal', function () {
    formReset('#disciplinaryActionForm');
    $('#addDisciplanyActionBtn').show();
    $('#updateDisciplanyActionBtn').hide();
    $('#disciplinaryActionForm').data('formtype', 'add');
})
$('#categoryModal').on('hidden.bs.modal', function () {
    formReset('#categoryForm');
})
$('#offenseCategoryModal').on('hidden.bs.modal', function () {
    formReset('#offenseCategoryForm');
    $('#offenseCategoryForm').data('formtype', 'add');
    $('#categoryLetter').prop('disabled', false);
    $('#addSpecOffenseCategoryBtn').show();
    $('#updateSpecOffenseCategoryBtn').hide();
})
$('#offenseLevelModal').on('hidden.bs.modal', function () {
    formReset('#offenseLevelForm');
})
$('#offenseModal').on('hidden.bs.modal', function () {
    formReset('#offenseForm');
    $('#addOffenseBtn').show();
    $('#updateOffenseBtn').hide();
    $('#offenseForm').data('formtype', 'add');
})

$('#qipAssignmentModal').on('hidden.bs.modal', function () {
    formReset('#qipAssignmentForm');
    $('#assignQipFilter').hide();
})
$('#eroAssignmentModal').on('hidden.bs.modal', function () {
    formReset('#eroAssignmentForm');
    $('#assignEroFilter').hide();
})
$('#assignOffenseTypeModal').on('hidden.bs.modal', function () {
    formReset('#assignOffenseTypeForm');
    $('#assignOffenseTypeFilter').hide();
})
$('#recommendationNumberModal').on('hidden.bs.modal', function () {
    formReset('#recomendationNumberForm');
    $('#recommendationNumberFilter').hide();
    $('#addRecommendationNumBtn').show();
    $('#updateRecommendationNumBtn').hide();
    $('#recomendationNumberForm').data('formtype', 'add');
    $('#accountRecommendationNumber').prop('disabled', false);
})

$('#assignChangeModal').on('hidden.bs.modal', function () {
    formReset('#assignChangeForm');
    $('#existChangeAlert').hide();
    $('#sameChangeAlert').hide();
    
});

$('#assignExemptionModal').on('hidden.bs.modal', function () {
    formReset('#assignExemptionForm');
    $('#existExemptionAlert').hide();
});

$('#defaultCategoryModal').on('hidden.bs.modal', function () {
    formReset('#defaultCategoryForm');
});

$('#disciplinaryActionTab').on('click', function(){
    getDisciplinaryAction();
})
$('#offenseCategoryTab').on('mouseover', function () {
    getOffenseCategory();
})
$('#offenseTab').on('mouseover', function () {
    $('#addOffenseCategoryDiv').show();
    $('#searchOffenseCategory').hide();
    $('#searchOffenseCategoryVal').val('');
    getSpecOffenseCategory();
})

$('#searchOffense').on('click', function () {
    $('#offenseOptions').hide(700);
    $('#offenseSearch').show(700);
})

$('#searchQip').on('click', function () {
    $('#qipOptions').hide(700);
    $('#qipSearch').show(700);
})

$('#searchEro').on('click', function () {
    $('#eroOptions').hide(700);
    $('#eroSearch').show(700);
})

$('.closeOffenseSearchBtn').on('click', function () {
    $('#offenseSearch').hide(700);
    $('#offenseOptions').show(700);
})

$('.closeQipSearchBtn').on('click', function () {
    $('#qipSearchField').val('');
    initQipDatatable()
    $('#qipSearch').hide(700);
    $('#qipOptions').show(700);
})

$('.closeEroSearchBtn').on('click', function () {
    $('#eroSearchField').val('');
    initQipDatatable()
    $('#eroSearch').hide(700);
    $('#eroOptions').show(700);
})

$('#showSearchOffenseCategoryBtn').on('click', function () {
    $('#searchOffenseCategory').fadeIn();
    $('#addOffenseCategoryDiv').hide();
})
$('#closeSearchOffenseCategoryBtn').on('click', function () {
    $('#searchOffenseCategoryVal').val('');
    getSpecOffenseCategory();
    $('#searchOffenseCategory').hide();
    $('#addOffenseCategoryDiv').fadeIn();
})
$('#searchOffenseCategoryVal').on('keyup', function () {
    getSpecOffenseCategory();
})

$('#customRecommendationTab').on('click', function () {
    // $('#recommendationSettings').find('.active').removeClass('active')
    // $('#recommendationSettings').find('.show').removeClass('show')
    // $('#customRecommendationTab').addClass('active');
    // $('#customRecommendationTab').addClass('show');
    // $('#customRecommendation').addClass('active');
    // $('#customRecommendation').addClass('show');
})

$('#offenseSearchField').on('keyup', function () {
    initOffenseDatatable($('#offenseCategoryLabel').data('offensecategoryid'));
})

$('#oneTimeNumVal').on('', function () {
    $('.popoverErr').text('Error Popover');
})

$('#categoryList').on('change', function(){
    $('#offenseForm').formValidation('disableSubmitButtons', false);
})

$('#allCateg').on('change', function(){
    if ($(this).prop("checked") == true) {
        $('#categoryList').prop('disabled', true);
    } else if ($(this).prop("checked") == false) {
        $('#categoryList').prop('disabled', false);
    }
    $('#offenseForm').formValidation('disableSubmitButtons', false);
})

$('#oneTimeOccChkbox, #consecOccChkbox, #nonConsecOccChkbox, #incOccurence').on('change', function () {
    var chkBoxStat = 0;
    var chkBoxStatProp ="";
    if ($(this).prop('checked')){
        chkBoxStat = 1;
        successChkBoxStatProp = "enabled";
        errChkBoxStatProp = "enabling";
        $(this).parents('.rowCheckBox').find('label').css('color', '');
        $(this).parents('.rowCheckBox').find('.comment').css('color', '');
        $(this).parents('.rowCheckBox').find('a').css('color', '');
        $(this).parents('.rowCheckBox').find('a').popover('enable');
        $(this).parents('.rowCheckBox').find('a').addClass('editableStyle');
    }else{
        chkBoxStat = 0;
        successChkBoxStatProp = "disabled";
        errChkBoxStatProp = "disabling";
        $(this).parents('.rowCheckBox').find('label').css('color', '#cccccc');
        $(this).parents('.rowCheckBox').find('.comment').css('color', '#cccccc');
        $(this).parents('.rowCheckBox').find('a').css('color', '#cccccc');
        $(this).parents('.rowCheckBox').find('a').popover('disable');
        $(this).parents('.rowCheckBox').find('a').removeClass('editableStyle');

    }
    var successUpdateMssg = successChkBoxStatProp+" "+ $(this).parents('label').text().trim();
    var errUpdateMssg = errChkBoxStatProp+" "+ $(this).parents('label').text().trim();
    updateDtrViolationSetting($('#violationTypeVal').data('violationtypeid'), $(this).data('field'), chkBoxStat, 0, 0, successUpdateMssg, errUpdateMssg, function (dtrVioUpdateStat) {
        initDtrViolationSettings($('#violationTypeVal').data('violationtypeid'));
    })
})

$('input[name="dtrPrescriptive"]').on('change', function(){
    $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('label').css('color', '');
    $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.comment').css('color', '');
    $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.prescriptiveValue').css('color', '');
    $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.prescriptiveValue').popover('enable');
    $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('label').css('color', '#cccccc');
    $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('.comment').css('color', '#cccccc');
    $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('.prescriptiveValue').css('color', '#cccccc');
    $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('.prescriptiveValue').popover('disable');
    $('input[name="dtrPrescriptive"]:not(:checked)').parents('.rowDtrPrescriptive').find('.prescriptiveValue').removeClass('editableStyle');
    console.log($('input[name="dtrPrescriptive"]:checked').val());
    if(parseInt($('input[name="dtrPrescriptive"]:checked').val()) !== 3){
        $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.prescriptiveValue').addClass('editableStyle');
    }else{
        $('input[name="dtrPrescriptive"]:checked').parents('.rowDtrPrescriptive').find('.prescriptiveValue').removeClass('editableStyle');
        check_if_def_offense_exist();
    }
    var successUpdateMssg = "set Prescriptive period to "+$('input[name="dtrPrescriptive"]:checked').parents('label').text().trim();
    var errUpdateMssg = "setting Prescriptive period to "+$('input[name="dtrPrescriptive"]:checked').parents('label').text().trim();
    updateDtrViolationSetting($('#violationTypeVal').data('violationtypeid'), 'durationType', $('input[name="dtrPrescriptive"]:checked').val(), 0, 0, successUpdateMssg, errUpdateMssg, function (dtrVioUpdateStat) {
        initDtrViolationSettings($('#violationTypeVal').data('violationtypeid'));
    })
})

$('input[name="dtrViolationMissedOption"]').on('change', function(){
    $('input[name="dtrViolationMissedOption"]:checked').parents('.dtrRowMissedOption').find('label').css('color', '');
    $('input[name="dtrViolationMissedOption"]:checked').parents('.dtrRowMissedOption').find('.comment').css('color', '');
    $('input[name="dtrViolationMissedOption"]:not(:checked)').parents('.dtrRowMissedOption').find('label').css('color', '#cccccc');
    $('input[name="dtrViolationMissedOption"]:not(:checked)').parents('.dtrRowMissedOption').find('.comment').css('color', '#cccccc');
    var successUpdateMssg = "set Missed Deadline Options to \" " + $('input[name="dtrViolationMissedOption"]:checked').parents('label').text().trim()+ "\"";
    var errUpdateMssg = "setting the Missed Deadline Options";
    updateDtrViolationSetting($('#deadlineDtrViolation').val(), 'deadlineOption', $('input[name="dtrViolationMissedOption"]:checked').val(), 0, 0, successUpdateMssg, errUpdateMssg, function (dtrVioUpdateStat) {
        initIrDeadline(1);
    })
})
$('input[name="explainMissedOption"]').on('change', function(){
    $('input[name="explainMissedOption"]:checked').parents('.explainRowMissedOption').find('label').css('color', '');
    $('input[name="explainMissedOption"]:checked').parents('.explainRowMissedOption').find('.comment').css('color', '');
    $('input[name="explainMissedOption"]:not(:checked)').parents('.explainRowMissedOption').find('label').css('color', '#cccccc');
    $('input[name="explainMissedOption"]:not(:checked)').parents('.explainRowMissedOption').find('.comment').css('color', '#cccccc');
    var successUpdateMssg = "set Missed Deadline Options to \" " + $('input[name="explainMissedOption"]:checked').parents('label').text().trim() + "\"";
    var errUpdateMssg = "setting the Missed Deadline Options";
    updateIrDeadlineSetting($('input[name="explainMissedOption"]:checked').data('field'), $('input[name="explainMissedOption"]:checked').val(), successUpdateMssg, errUpdateMssg, function (updateStat) {
        initIrDeadline($('input[name="explainMissedOption"]:checked').data('deadlinetype'));
    })
})
$('input[name="deadlineToRecommendOption"]').on('change', function(){
    $('input[name="deadlineToRecommendOption"]:checked').parents('.recommendationFlow').find('label').css('color', '');
    $('input[name="deadlineToRecommendOption"]:checked').parents('.recommendationFlow').find('.comment').css('color', '');
    $('input[name="deadlineToRecommendOption"]:not(:checked)').parents('.recommendationFlow').find('label').css('color', '#cccccc');
    $('input[name="deadlineToRecommendOption"]:not(:checked)').parents('.recommendationFlow').find('.comment').css('color', '#cccccc');
    var successUpdateMssg = "set Recommendation Flow to \" " + $('input[name="deadlineToRecommendOption"]:checked').parents('label').text().trim() + "\"";
    var errUpdateMssg = "setting the Missed Deadline Options";
    updateIrDeadlineSetting($('input[name="deadlineToRecommendOption"]:checked').data('field'), $('input[name="deadlineToRecommendOption"]:checked').val(), successUpdateMssg, errUpdateMssg, function (updateStat) {
        initIrDeadline($('input[name="deadlineToRecommendOption"]:checked').data('deadlinetype'));
    })
})

$('#employeeTypeQip').on('change', function () {
    initQipFilters(function () {})
});
$('#accountQip').on('change', function () {
    getEmployee($('#employeeTypeQip').val(), $(this).val(), 'qip', '#employeeQip', function (empExistStat) {});
});

$('#employeeTypeEro').on('change', function () {
    initEroFilters(function () {
        $('#eroAssignmentForm').formValidation('revalidateField', 'employeeEro');
    })
});
$('#accountEro').on('change', function () {
    getEmployee($('#employeeTypeEro').val(), $(this).val(), 'ero', '#employeeEro', function (empExistStat) {
        $('#eroAssignmentForm').formValidation('revalidateField', 'employeeEro');
    });
});

$('#classEroAccount, #siteAccountEro, #eroAccountSearchField').on('change', function () {
    initEroAccountDatatable($('#eroNameInfo').data('empid'));
})

$('#employeeTypeOffenseType').on('change', function(){
    initOffenseTypeFilters(function (accExistStat) {
        $('#assignOffenseTypeForm').formValidation('revalidateField', 'accountOffenseType');
        $('#assignOffenseTypeForm').formValidation('revalidateField', 'OffenseTypeSelect');
    })
})

$('#employeeTypeRecommendationNum').on('change', function () {
    initRecommendationNumberFilters(function (accExistStat) {
        $('#recomendationNumberForm').formValidation('revalidateField', 'accountRecommendationNumber');
        $('#recomendationNumberForm').formValidation('revalidateField', 'recommendationLevel');
    })
})

$('#assignQipFilterBtn').on('click', function(){
    if($(this).data("stat") == 0){
        $('#assignQipFilter').fadeIn();
        $(this).data("stat", 1);
    }else{
        $('#assignQipFilter').fadeOut();
        $(this).data("stat", 0);
    }
})
$('#assignEroFilterBtn').on('click', function(){
    if($(this).data("stat") == 0){
        $('#assignEroFilter').fadeIn();
        $(this).data("stat", 1);
    }else{
        $('#assignEroFilter').fadeOut();
        $(this).data("stat", 0);
    }
})

$('#assignOffenseTypeFilterBtn').on('click', function () {
    if($(this).data("stat") == 0){
        $('#assignOffenseTypeFilter').fadeIn();
        $(this).data("stat", 1);
    }else{
        $('#assignOffenseTypeFilter').fadeOut();
        $(this).data("stat", 0);
    }
})

$('#recommendationNumberFilterBtn').on('click', function () {
    if($(this).data("stat") == 0){
        $('#recommendationNumberFilter').fadeIn();
        $(this).data("stat", 1);
    }else{
        $('#recommendationNumberFilter').fadeOut();
        $(this).data("stat", 0);
    }
})

$('#eroSup').on('change', function(){
    confirmUpdateEroSup();
})

$('#accountOffenseType').on('change', function(){
    getOffenseTypeAssignmentSelect("#OffenseTypeSelect", function (offenseTypeObj) {})
})

// END OF EVENT TRIGGERS -------------------------------------------------------

 $('#prescreptivePeriod').TouchSpin({
     buttondown_class: 'btn btn-secondary',
     buttonup_class: 'btn btn-secondary',
     min: 1,
     max: 24,
     stepinterval: 50,
     maxboostedstep: 10000000,
     postfix: 'Month/s'
 });


 $('#recommendationLevel').TouchSpin({
     buttondown_class: 'btn btn-secondary',
     buttonup_class: 'btn btn-secondary',
     min: 1,
     max: 10,
     stepinterval: 50,
     maxboostedstep: 10000000,
     postfix: 'Level/s'
 });


$('.dmsMainNav').on('click', function (event) {
     $(".m-nav--inline").find(".active").removeClass("active");
     $(this).parent().addClass("active");
     $('.m-portlet .active').find(".active").removeClass("active");
     $('.m-portlet').fadeOut();
     $('#' + $(this).data('navcontent') + '').addClass("active");
     $('#' + $(this).data('navcontent') + '').fadeIn();
     $(this).addClass("active");
})

$('#deadlineDtrViolation').on('change', function(){
    initIrDeadline(1);
    $('#deadlineviolationType').text($('#deadlineDtrViolation').find('option:selected').text());
    $('#deadlineviolationType').data('deadlinevaltype',$('#deadlineDtrViolation').val());
})

$('#deadlineTab').on('click', function(){
    initIrDeadline($('#deadlineList').find(".deadlineSideTab.m-nav__item.list-active").data('deadlinetype'));
})
$('#dtrAutIrTab').on('click', function () {
    initDtrViolationSettings($('#violationTypeVal').data('violationtypeid'));
})

$('.dtrAutoIrSideTab').on('click', function (event) {
    initDtrViolationSettings($(this).data('violationtypeid'));
     $(".dtrAutoIrSideTab").removeClass("list-active");
     $(".dtrAutoIrSideTab").find(".m-nav__link").removeClass("active");
     $(".dtrAutoIrSideTab").find(".active-nav-a").removeClass("active-nav-a");
     $(this).addClass("list-active");
     $(this).find(".m-nav__link").addClass("active");
     $(this).find(".m-nav__link-icon").addClass("active-nav-a");
     $(this).find(".m-nav__link-text").addClass("active-nav-a");
})

$('.deadlineSideTab').on('click', function (event) {
    initIrDeadline($(this).data('deadlinetype'));
    $(".deadlineSideTab").removeClass("list-active");
    $(".deadlineSideTab").find(".m-nav__link").removeClass("active");
    $(".deadlineSideTab").find(".active-nav-a").removeClass("active-nav-a");
    $(this).addClass("list-active");
    $(this).find(".m-nav__link").addClass("active");
    $(this).find(".m-nav__link-icon").addClass("active-nav-a");
    $(this).find(".m-nav__link-text").addClass("active-nav-a");
})

$('.assignmentSideTab').on('click', function (event) {
    initAssignmentSettings($(this).data('assignmenttype'));
    $(".assignmentSideTab").removeClass("list-active");
    $(".assignmentSideTab").find(".m-nav__link").removeClass("active");
    $(".assignmentSideTab").find(".active-nav-a").removeClass("active-nav-a");
    $(this).addClass("list-active");
    $(this).find(".m-nav__link").addClass("active");
    $(this).find(".m-nav__link-icon").addClass("active-nav-a");
    $(this).find(".m-nav__link-text").addClass("active-nav-a");
})


$('#defaultOffenseType').on('change', function(){
    getOffenseSelect($(this).val(), $('#offenseTypeVal').data('offenseid'), function (defaultOffense) {

    })
})

$('#defaultOffense').on('change', function () {
    if ($(this).val() == ''){
        $('#updateDefaultOffenseBtn').prop('disabled', true);
    }else{
        if (parseInt($(this).val()) == parseInt($('#offenseTypeVal').data('offenseid'))) {
            $('#updateDefaultOffenseBtn').prop('disabled', true);
            // $("#updateDefaultOffenseBtn").off('click');
        } else {
            $('#updateDefaultOffenseBtn').prop('disabled', false);
            // $("#updateDefaultOffenseBtn").on('click');
        }
    }
})



$("#updateDefaultOffenseBtn").on('click', function(){
    if ($('#defaultOffense').val() == '') {
        alert()
        $('#updateDefaultOffenseBtn').prop('disabled', true);
    } else {
        if (parseInt($(this).val()) == parseInt($('#offenseTypeVal').data('offenseid'))) {
            $('#updateDefaultOffenseBtn').prop('disabled', true);
            // $("#updateDefaultOffenseBtn").off('click');
        } else {
            var successUpdateMssg =  " updated the Default Offense" ;
            var errUpdateMssg = " updating the Default Offense";
            updateDtrViolationSetting($('#violationTypeVal').data('violationtypeid'), 'offense_ID', parseInt($('#defaultOffense').val()), 0, 0, successUpdateMssg, errUpdateMssg, function (dtrVioUpdateStat) {
                initDtrViolationSettings($('#violationTypeVal').data('violationtypeid'));
            })
            $('#defaultOffenseModal').modal('hide');
        }
    }
})

$('#employeeEro, #eroSiteId').on('change', function() {
    assignableAccount($('#employeeEro').val(), $('#eroSiteId').val(), '#eroAccountId', function (assignableAccount) {
        if ($('#employeeEro').val() !==""){
            $('#eroAssignmentForm').formValidation('revalidateField', 'eroAccountId');
        } 
    })
})

$('#offenseTypeAccount').on('change', function(){
    initOffenseTypeAssignDatatable();
})

$('#recommendationNumAccount').on('change', function () {
    initRecommendationNumberDatatable();
});

$('#accountCustomChanges').on('change', function () {
    if($('#customChangesModal').data('type') == 'change'){
        initReassignmentDatatable('destroy', $('#recommenderName').data('empid'));
    }else{
        initExemptionDatatable('destroy', $('#recommenderName').data('empid'));
    }
})
 
$('#accountAssignChanges').on('change', function(){
    getAccountCustomLevel('#accountAssignChanges', function (recommendationLevelObj) {
        var levelsNum = 0;
        if (parseInt(recommendationLevelObj.exist)){
            initTouchpin("#levelAssignChanges", recommendationLevelObj.record.number);
        }else{
            getDefaultRecommendationLevel(function (defaultLevelObj){
                initTouchpin("#levelAssignChanges", defaultLevelObj);
            })
        }
    })
})
$('#accountAssignExemption').on('change', function () {
    getAccountCustomLevel('#accountAssignExemption', function (recommendationLevelObj) {
        var levelsNum = 0;
        if (parseInt(recommendationLevelObj.exist)){
            console.log(recommendationLevelObj);
            initTouchpin("#levelAssignExemption", recommendationLevelObj.record.number);
        }else{
            getDefaultRecommendationLevel(function (defaultLevelObj){
                initTouchpin("#levelAssignExemption", defaultLevelObj);
            })
        }
    })
})

$(document).mouseup(function (e) {
    var container = $("#searchOffenseCategoryVal");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $('#searchOffenseCategory').hide();
        $('#addOffenseCategoryDiv').fadeIn();
    }
});

$(document).on('click', function (e) {
    $('[data-toggle="popover"],[data-original-title]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false // fix for BS 3.3.6
        }
    });
});


$(function () {
    getDisciplinaryAction();
    // dynamicProcessStrValidation()
    // getSpecOffenseCategory();
    // $("#offenseType").resizable();

    // TEST 
    
})