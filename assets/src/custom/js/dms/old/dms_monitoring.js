// PENDING DTR VIO COUNT
function pendingDtrVioCountMonitoring(late, ob, ut, forgotToLogout, incBreak, incDtr, absent) {
    if (late > 0) {
        $('#pendingLateCountDivMonitoring').fadeIn();
        $('#pendingLateCountMonitoring').text(late);
    } else {
        $('#pendingLateCountDivMonitoring').hide();
    }
    if (ob > 0) {
        $('#pendingObCountDivMonitoring').fadeIn();
        $('#pendingObCountMonitoring').text(ob);
    } else {
        $('#pendingObCountDivMonitoring').hide();
    }
    if (ut > 0) {
        $('#pendingUtCountDivMonitoring').fadeIn();
        $('#pendingUtCountMonitoring').text(ut);
    } else {
        $('#pendingUtCountDivMonitoring').hide();
    }
    if (forgotToLogout > 0) {
        $('#pendingForgotToLogoutCountDivMonitoring').fadeIn();
        $('#pendingForgotToLogoutCountMonitoring').text(forgotToLogout);
    } else {
        $('#pendingForgotToLogoutCountDivMonitoring').hide();
    }
    if (incBreak > 0) {
        $('#pendingIncBreakCountDivMonitoring').fadeIn();
        $('#pendingIncBreakCountMonitoring').text(incBreak);
    } else {
        $('#pendingIncBreakCountDivMonitoring').hide();
    }
    if (incDtr > 0) {
        $('#pendingIncDtrCountDivMonitoring').fadeIn();
        $('#pendingIncDtrCountMonitoring').text(incDtr);
    } else {
        $('#pendingIncDtrCountDivMonitoring').hide();
    }
    if (absent > 0) {
        $('#pendingAbsentCountDivMonitoring').fadeIn();
        $('#pendingAbsentCountMonitoring').text(absent);
    } else {
        $('#pendingAbsentCountDivMonitoring').hide();
    }
}

function recordDtrVioCountMonitoring(late, ob, ut, forgotToLogout, incBreak, incDtr, absent) {
    if (late > 0) {
        $('#recordLateCountDivMonitoring').fadeIn();
        $('#recordLateCountMonitoring').text(late);
    } else {
        $('#recordLateCountDivMonitoring').hide();
    }
    if (ob > 0) {
        $('#recordObCountDivMonitoring').fadeIn();
        $('#recordObCountMonitoring').text(ob);
    } else {
        $('#recordObCountDivMonitoring').hide();
    }
    if (ut > 0) {
        $('#recordUtCountDivMonitoring').fadeIn();
        $('#recordUtCountMonitoring').text(ut);
    } else {
        $('#recordUtCountDivMonitoring').hide();
    }
    if (forgotToLogout > 0) {
        $('#recordForgotToLogoutCountDivMonitoring').fadeIn();
        $('#recordForgotToLogoutCountMonitoring').text(forgotToLogout);
    } else {
        $('#recordForgotToLogoutCountDivMonitoring').hide();
    }
    if (incBreak > 0) {
        $('#recordIncBreakCountDivMonitoring').fadeIn();
        $('#recordIncBreakCountMonitoring').text(incBreak);
    } else {
        $('#recordIncBreakCountDivMonitoring').hide();
    }
    if (incDtr > 0) {
        $('#recordIncDtrCountDivMonitoring').fadeIn();
        $('#recordIncDtrCountMonitoring').text(incDtr);
    } else {
        $('#recordIncDtrCountDivMonitoring').hide();
    }
    if (absent > 0) {
        $('#recordAbsentCountDivMonitoring').fadeIn();
        $('#recordAbsentCountMonitoring').text(absent);
    } else {
        $('#recordAbsentCountDivMonitoring').hide();
    }
}

function missedAlert(missedCount){
    if(missedCount > 0){
        var highlight = "";
        var Mssg = ""
        if (missedCount > 1){
            highlight = missedCount+" DTR Violations";
            Mssg = "were failed to be filed on time.";
        }else{
            highlight = missedCount + " DTR Violation";
            Mssg = "was failed to be filed on time.";
        }
        $('#missedHighlight').text(highlight);
        $('#missedMssg').text(Mssg);
        $('#missedAlert').fadeIn();
    }else{
        $('#missedAlert').hide();
    }
}

// ORDINARY INIT FUNCTIONS
// BADGES
function initDtrViolationBadgeMonitoring() {
    var dtrViolationBadge = 0;
    dtrViolationBadge += parseInt($('#fileDtrViolationIrBadgeCountMonitoring').attr('count'));
    if (dtrViolationBadge > 0) {
        $('#dtrViolationBadgeCountMonitoring').text(dtrViolationBadge);
        $('#dtrViolationBadgeMonitoring').fadeIn();
    } else {
        $('#dtrViolationBadgeMonitoring').hide();
    }
}

function initfileDtrViolationIrBadgeMonitoring(callback) {
    $.when(fetchGetData('/discipline/get_file_dtr_violation_ir_count_monitoring')).then(function (count) {
        var countObj = $.parseJSON(count.trim());
        if (parseInt(countObj.count) > 0) {
            $('#fileDtrViolationIrBadgeCountMonitoring').text(countObj.count);
            $('#fileDtrViolationIrBadgeCountMonitoring').attr('count', countObj.count);
            $('#fileDtrViolationIrBadgeMonitoring').fadeIn();
        } else {
            $('#fileDtrViolationIrBadgeMonitoring').hide();
        }
        callback(countObj);
    });
}

// DATATABLES
var irableDtrViolationMonitoringDatatable = function () {
    var irableDtrViolationMonitoring = function (empIdVal, violationTypeVal, schedVal, statVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_irable_dtr_violation_monitoring_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                                violationType: violationTypeVal,
                                sched: schedVal,
                                stat: statVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            var late = 0;
                            var ob = 0;
                            var ut = 0;
                            var forgotToLogout = 0;
                            var incBreak = 0;
                            var incDtr = 0;
                            var absent = 0;
                            var missed = 0;
                            
                            $.each(raw.data, function (index, value) {
                                if (parseInt(value.dtrViolationType_ID) == 1) {
                                    late++;
                                } else if (parseInt(value.dtrViolationType_ID) == 2) {
                                    ut++;
                                } else if (parseInt(value.dtrViolationType_ID) == 3) {
                                    forgotToLogout++;
                                } else if (parseInt(value.dtrViolationType_ID) == 4) {
                                    ob++;
                                } else if (parseInt(value.dtrViolationType_ID) == 5) {
                                    absent++;
                                } else if (parseInt(value.dtrViolationType_ID) == 6) {
                                    incBreak++;
                                } else if (parseInt(value.dtrViolationType_ID) == 7) {
                                    incDtr++;
                                }

                                if (parseInt(value.status_ID) == 12){
                                    missed++;
                                }
                            })
                            missedAlert(missed);
                            pendingDtrVioCountMonitoring(late, ob, ut, forgotToLogout, incBreak, incDtr, absent)
                            dataCount = dataSet.length;
                            console.log(dataSet);
                            console.log(dataCount);
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: 'employee',
                width: 200,
                title: 'Employee',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if(row.status_ID == 12){
                        textColor = "text-danger";
                    }
                    var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                    var pic = imagePath + "" + picName + "_thumbnail.jpg";

                    var elementId = "#irFiling" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="pull-left ml-1"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;<span class="' + textColor + '">' + toTitleCase(row.fname + ' ' + row.lname) + '</span></div>';
                    return html;
                },
            }, {
                field: 'description',
                width: 60,
                title: 'Type',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if(row.status_ID == 12){
                        textColor = "text-danger";
                    }
                    var html = "<span class='"+textColor+"'>" + toTitleCase(row.description)+"</span>";
                    return html;
                }
            }, {
                field: 'sched_date',
                width: 80,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'supEmp_id',
                width: 200,
                title: 'Current',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var picName = row.supPic.substr(0, row.supPic.indexOf(".jpg"))
                    var pic = imagePath + "" + picName + "_thumbnail.jpg";

                    var elementId = "#irFiler" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="pull-left"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp; <span class="' + textColor + '">' + toTitleCase(row.supFname + ' ' + row.supLname) + '</span></div>';
                    // var html = toTitleCase(row.supFname + ' ' + row.supLname);
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="File IR"><i class="la la-edit"></i></a>';
                    return html;
                },
            }],
        };
        var datatable = $('#irAbleDtrViolationMonitoringDatatable').mDatatable(options);
    };
    return {
        init: function (empIdVal, violationTypeVal, schedVal, statVal) {
            irableDtrViolationMonitoring(empIdVal, violationTypeVal, schedVal, statVal);
        }
    };
}();

var irableDtrViolationRecordMonitoringDatatable = function () {
    var irableDtrViolationRecordMonitoring = function (empIdVal, violationTypeVal, dateStartVal, dateEndVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_irable_dtr_violation_record_monitoring_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                                violationType: violationTypeVal,
                                dateStart: dateStartVal,
                                dateEnd: dateEndVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            var late = 0;
                            var ob = 0;
                            var ut = 0;
                            var forgotToLogout = 0;
                            var incBreak = 0;
                            var incDtr = 0;
                            var absent = 0;
                            var missed = 0;

                            $.each(raw.data, function (index, value) {
                                if (parseInt(value.dtrViolationType_ID) == 1) {
                                    late++;
                                } else if (parseInt(value.dtrViolationType_ID) == 2) {
                                    ut++;
                                } else if (parseInt(value.dtrViolationType_ID) == 3) {
                                    forgotToLogout++;
                                } else if (parseInt(value.dtrViolationType_ID) == 4) {
                                    ob++;
                                } else if (parseInt(value.dtrViolationType_ID) == 5) {
                                    absent++;
                                } else if (parseInt(value.dtrViolationType_ID) == 6) {
                                    incBreak++;
                                } else if (parseInt(value.dtrViolationType_ID) == 7) {
                                    incDtr++;
                                }

                                if (parseInt(value.status_ID) == 12) {
                                    missed++;
                                }
                            })

                            recordDtrVioCountMonitoring(late, ob, ut, forgotToLogout, incBreak, incDtr, absent)
                            dataCount = dataSet.length;

                            if (dataCount > 0) {
                                $('#noIrableDtrViolationRecordMonitoring').hide();
                                $('#irableDtrViolationRecordMonitoring').fadeIn();
                            } else {
                                $('#noIrableDtrViolationRecordMonitoring').fadeIn();
                                $('#irableDtrViolationRecordMonitoring').hide();
                            }

                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: 'employee',
                width: 200,
                title: 'Employee',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                    var pic = imagePath + "" + picName + "_thumbnail.jpg";

                    var elementId = "#irFiling" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="pull-left ml-1"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;<span class="' + textColor + '">' + toTitleCase(row.fname + ' ' + row.lname) + '</span></div>';
                    return html;
                },
            }, {
                field: 'description',
                width: 60,
                title: 'Type',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + toTitleCase(row.description) + "</span>";
                    return html;
                }
            }, {
                field: 'sched_date',
                width: 80,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'supEmp_id',
                width: 200,
                title: 'Filed By',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var picName = row.supPic.substr(0, row.supPic.indexOf(".jpg"))
                    var pic = imagePath + "" + picName + "_thumbnail.jpg";

                    var elementId = "#irFiler" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="pull-left"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp; <span class="' + textColor + '">' + toTitleCase(row.supFname + ' ' + row.supLname) + '</span></div>';
                    // var html = toTitleCase(row.supFname + ' ' + row.supLname);
                    return html;
                },
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="File IR"><i class="la la-edit"></i></a>';
                    return html;
                },
            }],
        };
        var datatable = $('#irAbleDtrViolationRecordMonitoringDatatable').mDatatable(options);
    };
    return {
        // ($('#fileDtrViolationEmpMonitoringRecordSelect').val(), $('#violationTypeMonitoringRecord').val(), dateStart, dateEnd)
        init: function (empIdVal, violationTypeVal, dateStartVal, dateEndVal) {
            irableDtrViolationRecordMonitoring(empIdVal, violationTypeVal, dateStartVal, dateEndVal);
        }
    };
}();

function initIrAbleDtrViolationMonitoringRecordDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#fileDtrViolationMonitoringRecordDateRange .form-control').val(start + ' - ' + end);
        $('#fileDtrViolationMonitoringRecordDateRange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#fileDtrViolationMonitoringRecordDateRange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initIrAbleDtrViolationRecordMonitoring(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);
    });
}

function get_emp_dtr_vio_ir_select_monitoring(callback) {
    var sched_date = $('#dtrViolationSchedMonitoring').val();
    if (sched_date != '') {
        sched_date = moment($('#dtrViolationSchedMonitoring').val(), 'MM/DD/YYYY').format('Y-MM-DD');
    }
    $.when(fetchPostData({
        violationType: $('#violationTypeMonitoring').val(),
        sched: sched_date,
        stat: $('#fileDtrViolationMonitoringStatus').val(),
    }, '/discipline/get_specific_qualified_emp_ir_monitoring')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        $('#fileDtrViolationEmpSelectMonitoring').empty().trigger('change');
        console.log(empObj.record);
        if (empObj.record != 0) {
            $('#noIrableDtrViolationMonitoring').hide();
            $('#irableDtrViolationMonitoring').fadeIn();
        } else {
            $('#noIrableDtrViolationMonitoring').fadeIn();
            $('#irableDtrViolationMonitoring').hide();
        }
        init_select2(empObj, "#fileDtrViolationEmpSelectMonitoring", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

function get_emp_dtr_vio_ir_monitoring_record_select(startDate, endDate, callback) {
    $.when(fetchPostData({
        violationType: $('#violationTypeMonitoringRecord').val(),
        dateStart: startDate,
        dateEnd: endDate
    }, '/discipline/get_specific_qualified_emp_ir_record_monitoring')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        console.log(empObj);
        $('#fileDtrViolationEmpMonitoringRecordSelect').empty().trigger('change');
        init_select2(empObj, "#fileDtrViolationEmpMonitoringRecordSelect", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

// DATATABLE INIT
function initIrAbleDtrViolationMonitoring() {
    var sched_date = $('#dtrViolationSchedMonitoring').val();
    if (sched_date != '') {
        sched_date = moment($('#dtrViolationSchedMonitoring').val(), 'MM/DD/YYYY').format('Y-MM-DD');
    }
    $('#irAbleDtrViolationMonitoringDatatable').mDatatable('destroy');
    irableDtrViolationMonitoringDatatable.init($('#fileDtrViolationEmpSelectMonitoring').val(), $('#violationTypeMonitoring').val(), sched_date, $('#violationTypeMonitoring').val());
}

function initIrAbleDtrViolationRecordMonitoringDatatable(dateStart, dateEnd, initType) {
    $('#irAbleDtrViolationRecordMonitoringDatatable').mDatatable(initType);
    irableDtrViolationRecordMonitoringDatatable.init($('#fileDtrViolationEmpMonitoringRecordSelect').val(), $('#violationTypeMonitoringRecord').val(), dateStart, dateEnd);
}

function initIrAbleDtrViolationRecordMonitoring(startDate, endDate, callback) {
    get_emp_dtr_vio_ir_monitoring_record_select(startDate, endDate, function (notEmpty) {
        initIrAbleDtrViolationRecordMonitoringDatatable(startDate, endDate, 'destroy');
        callback(notEmpty);
    })
}

$('.dmsMonitoringMainNav').on('click', function (event) {
    $(".m-nav--inline").find(".active").removeClass("active");
    $(this).parent().addClass("active");
    $('.m-portlet .active').find(".active").removeClass("active");
    $('.m-portlet').hide();
    $('#' + $(this).data('navcontent') + '').addClass("active");
    $('#' + $(this).data('navcontent') + '').fadeIn();
    $(this).addClass("active");
})

$('#fileDtrViolationIrMonitoringTab').on('mouseover', function () {
    initDatePicker('#dtrViolationSchedMonitoring', true, function () {
        get_emp_dtr_vio_ir_select_monitoring(function (notEmpty) {
            initIrAbleDtrViolationMonitoring();
        });
    });
});

$('#fileDtrViolationIrRecordMonitoringTab').on('mouseover', function () {
    initIrAbleDtrViolationMonitoringRecordDateRangePicker(function (dateRanges) {
        var dateRange = $('#fileDtrViolationMonitoringRecordDateRange').val().split('-');
        initIrAbleDtrViolationRecordMonitoring(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
    });
});

$('#fileDtrViolationEmpSelectMonitoring').on('change', function () {
    initIrAbleDtrViolationMonitoring();
});

$('#violationTypeMonitoring, #dtrViolationSchedMonitoring, #fileDtrViolationMonitoringStatus').on('change', function () {
    get_emp_dtr_vio_ir_select_monitoring(function (notEmpty) {
        initIrAbleDtrViolationMonitoring();
    });
});

$('#violationTypeMonitoringRecord').on('change', function () {
    var dateRange = $('#fileDtrViolationMonitoringRecordDateRange').val().split('-');
    initIrAbleDtrViolationRecordMonitoring(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
});

$(function () {
    initfileDtrViolationIrBadgeMonitoring(function (countObj) {
        initDtrViolationBadgeMonitoring();
    });
})