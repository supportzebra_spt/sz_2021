// PENDING DTR VIO COUNT
function pendingDtrVioCount(late, ob, ut, forgotToLogout, incBreak, incDtr, absent){
    if (late > 0){
        $('#pendingLateCountDiv').fadeIn();
        $('#pendingLateCount').text(late);
    }else{
        $('#pendingLateCountDiv').hide();
    }
    if (ob > 0) {
        $('#pendingObCountDiv').fadeIn();
        $('#pendingObCount').text(ob);
    }else{
        $('#pendingObCountDiv').hide();
    }
    if (ut > 0) {
        $('#pendingUtCountDiv').fadeIn();
        $('#pendingUtCount').text(ut);
    }else{
        $('#pendingUtCountDiv').hide();
    }
    if (forgotToLogout > 0) {
        $('#pendingForgotToLogoutCountDiv').fadeIn();
        $('#pendingForgotToLogoutCount').text(forgotToLogout);
    }else{
        $('#pendingForgotToLogoutCountDiv').hide();
    }
    if (incBreak > 0) {
        $('#pendingIncBreakCountDiv').fadeIn();
        $('#pendingIncBreakCount').text(incBreak);
    }else{
        $('#pendingIncBreakCountDiv').hide();
    }
    if (incDtr > 0) {
        $('#pendingIncDtrCountDiv').fadeIn();
        $('#pendingIncDtrCount').text(incDtr);
    }else{
        $('#pendingIncDtrCountDiv').hide();
    }
    if (absent > 0) {
        $('#pendingAbsentCountDiv').fadeIn();
        $('#pendingAbsentCount').text(absent);
    }else{
        $('#pendingAbsentCountDiv').hide();
    }
}

function recordDtrVioCount(late, ob, ut, forgotToLogout, incBreak, incDtr, absent){
    if (late > 0){
        $('#recordLateCountDiv').fadeIn();
        $('#recordLateCount').text(late);
    }else{
        $('#recordLateCountDiv').hide();
    }
    if (ob > 0) {
        $('#recordObCountDiv').fadeIn();
        $('#recordObCount').text(ob);
    }else{
        $('#recordObCountDiv').hide();
    }
    if (ut > 0) {
        $('#recordUtCountDiv').fadeIn();
        $('#recordUtCount').text(ut);
    }else{
        $('#recordUtCountDiv').hide();
    }
    if (forgotToLogout > 0) {
        $('#recordForgotToLogoutCountDiv').fadeIn();
        $('#recordForgotToLogoutCount').text(forgotToLogout);
    }else{
        $('#recordForgotToLogoutCountDiv').hide();
    }
    if (incBreak > 0) {
        $('#recordIncBreakCountDiv').fadeIn();
        $('#recordIncBreakCount').text(incBreak);
    }else{
        $('#recordIncBreakCountDiv').hide();
    }
    if (incDtr > 0) {
        $('#recordIncDtrCountDiv').fadeIn();
        $('#recordIncDtrCount').text(incDtr);
    }else{
        $('#recordIncDtrCountDiv').hide();
    }
    if (absent > 0) {
        $('#recordAbsentCountDiv').fadeIn();
        $('#recordAbsentCount').text(absent);
    }else{
        $('#recordAbsentCountDiv').hide();
    }
}

// DATATABLES
var irableDtrViolationDatatable = function () {
    var irableDtrViolation = function (empIdVal, violationTypeVal, schedVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_irable_dtr_violation_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                                violationType: violationTypeVal,
                                sched: schedVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            var late = 0;
                            var ob = 0;
                            var ut = 0;
                            var forgotToLogout = 0;
                            var incBreak = 0;
                            var incDtr = 0;
                            var absent = 0;
                            $.each(raw.data, function(index, value){
                                if(parseInt(value.dtrViolationType_ID) == 1){
                                    late++;
                                } else if (parseInt(value.dtrViolationType_ID) == 2) {
                                    ut++;
                                } else if (parseInt(value.dtrViolationType_ID) == 3) {
                                    forgotToLogout++;
                                } else if (parseInt(value.dtrViolationType_ID) == 4) {
                                    ob++;
                                } else if (parseInt(value.dtrViolationType_ID) == 5) {
                                    absent++;
                                } else if (parseInt(value.dtrViolationType_ID) == 6) {
                                    incBreak++;
                                } else if (parseInt(value.dtrViolationType_ID) == 7) {
                                    incDtr++;
                                }
                            })

                            pendingDtrVioCount(late, ob, ut, forgotToLogout, incBreak, incDtr, absent)
                            dataCount = dataSet.length;
                            console.log(dataSet);
                            console.log(dataCount);
                            // if (dataCount>0){
                            //     $('#noIrableDtrViolation').hide();
                            //     $('#irableDtrViolation').fadeIn();
                            // }else{
                            //     $('#noIrableDtrViolation').fadeIn();
                            //     $('#irableDtrViolation').hide();
                            // }
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: 'employee',
                width: 200,
                title: 'Employee',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                    var pic = imagePath + "" + picName+"_thumbnail.jpg";

                    var elementId = "#irFiling" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'description',
                width: 80,
                title: 'Violation',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
            }, {
                field: 'sched_date',
                width: 100,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'deadline',
                width: 150,
                title: 'Deadline',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.deadline, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</span>";
                    return html;
                },
            },  {
                field: 'action',
                width: 80,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="File IR" data-qualifiedid="' + row.qualifiedUserDtrViolation_ID + '" onclick="initQualifiedModal(this)"><i class="la la-edit"></i></a>';
                    return html;
                },
            }],
        };
        var datatable = $('#irAbleDtrViolationDatatable').mDatatable(options);
    };
    return {
        init: function (empIdVal, violationTypeVal, searchVal) {
            irableDtrViolation(empIdVal, violationTypeVal, searchVal);
        }
    };

}();

var irableDtrViolationRecordDatatable = function () {
    var irableDtrViolationRecord = function (empIdVal, violationTypeVal, statusVal, dateStartVal, dateEndVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_irable_dtr_violation_record_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                                violationType: violationTypeVal,
                                status: statusVal,
                                dateStart: dateStartVal,
                                dateEnd: dateEndVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            console.log(raw.data);
                            var late = 0;
                            var ob = 0;
                            var ut = 0;
                            var forgotToLogout = 0;
                            var incBreak = 0;
                            var incDtr = 0;
                            var absent = 0;
                            $.each(raw.data, function (index, value) {
                                if (parseInt(value.dtrViolationType_ID) == 1) {
                                    late++;
                                } else if (parseInt(value.dtrViolationType_ID) == 2) {
                                    ut++;
                                } else if (parseInt(value.dtrViolationType_ID) == 3) {
                                    forgotToLogout++;
                                } else if (parseInt(value.dtrViolationType_ID) == 4) {
                                    ob++;
                                } else if (parseInt(value.dtrViolationType_ID) == 5) {
                                    absent++;
                                } else if (parseInt(value.dtrViolationType_ID) == 6) {
                                    incBreak++;
                                } else if (parseInt(value.dtrViolationType_ID) == 7) {
                                    incDtr++;
                                }
                            })

                            recordDtrVioCount(late, ob, ut, forgotToLogout, incBreak, incDtr, absent)
                            dataCount = dataSet.length;

                            if (dataCount > 0) {
                                $('#noIrableDtrViolationRecord').hide();
                                $('#irableDtrViolationRecord').fadeIn();
                            } else {
                                $('#noIrableDtrViolationRecord').fadeIn();
                                $('#irableDtrViolationRecord').hide();
                            }
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: 'employee',
                width: 200,
                title: 'Employee',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                    var pic = imagePath + "" + picName + "_thumbnail.jpg";

                    var elementId = "#irFiling" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="pull-left ml-5"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"> &nbsp;' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'description',
                width: 80,
                title: 'Violation',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
            }, {
                field: 'sched_date',
                width: 100,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'statDescription',
                width: 150,
                title: 'Status',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.status_ID) == 12) {
                        var badgeColor = 'btn-warning';
                        var dotAlert = "<span class='m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink mr-1' style='min-height: 7px;min-width: 7px;'></span>";
                    } else if (parseInt(row.status_ID) == 5) {
                        var badgeColor = 'btn-success';
                        var dotAlert = "";
                    } 
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + "' '>" + dotAlert + " " + row.statDescription + "</span>";
                    return html;
                }
            }, {
                field: 'action',
                width: 80,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="File IR"><i class="la la-edit"></i></a>';
                    return html;
                },
            }],
        };
        var datatable = $('#irAbleDtrViolationRecordDatatable').mDatatable(options);
    };
    return {
        init: function (empIdVal, violationTypeVal, statusVal, dateStartVal, dateEndVal) {
            irableDtrViolationRecord(empIdVal, violationTypeVal, statusVal, dateStartVal, dateEndVal);
        }
    };
}();

// DATATABLE INIT
function initIrAbleDtrViolation() {
    var sched_date = $('#dtrViolationSched').val();
    if (sched_date != ''){
        sched_date = moment($('#dtrViolationSched').val(), 'MM/DD/YYYY').format('Y-MM-DD');
    }
    $('#irAbleDtrViolationDatatable').mDatatable('destroy');
    irableDtrViolationDatatable.init($('#fileDtrViolationEmpSelect').val(), $('#violationType').val(), sched_date);
}

function initIrAbleDtrViolationRecord(dateStart, dateEnd) {
    $('#irAbleDtrViolationRecordDatatable').mDatatable('destroy');
    irableDtrViolationRecordDatatable.init($('#fileDtrViolationEmpRecordSelect').val(), $('#violationTypeRecord').val(), $('#fileDtrViolationRecordStatus').val(), dateStart, dateEnd);
}

// Plugin Initialization
function init_select2(obj, elementId, callback) {
    var optionsAsString = "<option value='0'>ALL</option>";
    var notEmpty = 1;
    if (parseInt(obj.record) == 0) {
        notEmpty = 0;
    } else {
        $.each(obj.record, function (index, value) {
            console.log(value.emp_id);
            optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.lname + ", " + value.fname) + "</option>";
        });
        $(elementId).html(optionsAsString);
    }
    $(elementId).select2({
        placeholder: "Select an Employee",
        width: '100%'
    }).trigger('change');
    callback(notEmpty);
}

function initDatePicker(id, clearBtn, callback){
    $(id).datepicker({
        todayBtn: "linked",
        useCurrent: false,
        todayHighlight: true,
        autoclose: true,
        clearBtn: clearBtn,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        },
        // startDate: moment(date, 'Y-MM-DD').subtract(parseInt(daysAvail.daysAvailable), "days").format('MM/DD/YYYY'),
        // endDate: moment(date, 'Y-MM-DD').format('MM/DD/YYYY')
    });
    callback(id);
}

function initIrAbleDtrViolationRecordDateRangePicker(callback){
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#fileDtrViolationRecordDateRange .form-control').val(start + ' - ' + end);
        $('#fileDtrViolationRecordDateRange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#fileDtrViolationRecordDateRange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            // initIrAbleDtrViolationRecord()
            initIrAbleDtrVioRecord(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});            
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);

    });
}

// fileDtrViolationIrBadge

// ORDINARY INIT FUNCTIONS
// BADGES
function initDtrViolationBadge(){
    var dtrViolationBadge = 0;
    dtrViolationBadge += parseInt($('#fileDtrViolationIrBadgeCount').attr('count'));
    if (dtrViolationBadge > 0){
        $('#dtrViolationBadgeCount').text(dtrViolationBadge);
        $('#dtrViolationBadge').fadeIn();
    }else{
        $('#dtrViolationBadge').hide();
    }
}
function initfileDtrViolationIrBadge(callback){
    $.when(fetchGetData('/discipline/get_file_dtr_violation_ir_count')).then(function (count) {
        var countObj = $.parseJSON(count.trim());
        if(parseInt(countObj.count) > 0){
            $('#fileDtrViolationIrBadgeCount').text(countObj.count);
            $('#fileDtrViolationIrBadgeCount').attr('count',countObj.count);
            $('#fileDtrViolationIrBadge').fadeIn();
        }else{
            $('#fileDtrViolationIrBadge').hide();
        }
        callback(countObj);
    });
}

function initIrAbleDtrVioRecord(startDate, endDate, callback){
    get_emp_dtr_vio_ir_record_select(startDate, endDate, function (notEmpty) {
        initIrAbleDtrViolationRecord(startDate, endDate)
        callback(notEmpty);
    })
}

// init Modals
function initQualifiedModal(element){
    $.when(fetchPostData({
        qualifiedViolationId: $(element).data('qualifiedid'),
    }, '/discipline/get_qualified_dtr_vio_details')).then(function (qualified) {
        var qualifiedObj = $.parseJSON(qualified.trim());
        var incidentDetails = "";
        var excess = "";
        var allowableTable = "";
        var coveredTable = "";
        var missingLogsDetails = "";
        var missingLogs = "";
        console.log(qualifiedObj);
        console.log(parseInt(qualifiedObj.error));
        if(parseInt(qualifiedObj.error) == 0)
        {
            var allowableTableBody = "";
            var coveredTableBody = "";
            // EMPLOYEE DETAILS
            $('#qualifiedEmployeeName').text(toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname));
            $('#qualifiedEmployeeJob').text(toTitleCase(qualifiedObj.details.emp_details.positionDescription));
            $('#qualifiedEmployeeAccount').text(toTitleCase(qualifiedObj.details.emp_details.accountDescription));
            // Date and Incidents
            $('#qualifiedDateIncident').text(moment(qualifiedObj.details.sched_date, 'Y-MM-DD').format('MMM DD, Y'));
            $('#qualifiedIncidentType').text(toTitleCase(qualifiedObj.details.violation_type));
            $('#withRule').show();
            // Incident Details
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4){
                incidentDetails = '<div class="col-12">'+
                                    '<span class="font-weight-bold mr-2">Shift:</span>'+
                                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                                '</div>'+
                                '<div class="col-12 mt-2 font-weight-bold">Break Details:</div>';
            excess = '<div class="col-12">'+
                        '<span class="font-weight-bold mr-2">Over Break Duration:</span>'+
                        '<span class="">' + qualifiedObj.details.incident_details.excess + '</span>' +
                    '</div>';
               
                            if (parseInt(qualifiedObj.details.incident_details.error) == 0){
                                $.each(qualifiedObj.details.incident_details.break, function(index, value){
                                    allowableTableBody +='<tr>'+
                                                '<td class="text-center" style="font-weight:400">'+value.type+'</td>'+
                                                '<td class="text-center">' + value.allowable + '</td>' +
                                            '</tr>';
                                    coveredTableBody += '<tr>'+
                                                '<td class="text-center">' + moment(value.in, 'Y-MM-DD HH:mm:ss').format('MMM M, Y HH:mm:ss A') + '</td>' +
                                                '<td class="text-center" style="font-weight:400">' + moment(value.out, 'Y-MM-DD HH:mm:ss').format('MMM M, Y HH:mm:ss A') + '</td>' +
                                                '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                                            '</tr>';
                                })
                                allowableTable = '<div class="col-12 col-md-4">'+
                                    '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                                        '<thead style="background: #555;color: white;">'+
                                            '<tr>'+
                                                '<th class="text-center">Type</th>'+
                                                '<th class="text-center">Allowable</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody>' + 
                                        allowableTableBody+
                                        '</tbody>' + 
                                    '</table>'+
                                '</div>';
                                coveredTable = '<div class="col-12 col-md-8 pl-0">' +
                                    '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">'+
                                        '<thead style="background: #555;color: white;">'+
                                            '<tr>'+
                                                '<th class="text-center">Out</th>'+
                                                '<th class="text-center">In</th>'+
                                                '<th class="text-center">Covered</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody>'+
                                        coveredTableBody+
                                        '</tbody>'+
                                    '</table>'+
                                '</div>';
                                incidentDetails += allowableTable + "" + coveredTable + "" + excess;
                            }else{
                                // alert error
                            }                
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 5){
                $('#withRule').hide();
                incidentDetails = '<div class="col-12 col-md-6">'+
                                    '<span class="font-weight-bold mr-2">Shift:</span>'+
                                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                                '</div>'+
                                '<div class="col-12 col-md-6 text-md-right">' +
                                    '<span class="font-weight-bold mr-2">Logs:</span>'+
                                    '<span class="">No Log Records Found</span>'+
                                '</div>';
            } else if ((parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6) || (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7)) {
                incidentDetails = '<div class="col-12 col-md-4">'+
                                    '<span class="font-weight-bold mr-2">Shift:</span>'+
                                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                                '</div>'+
                                '<div class="col-12 col-md-3 text-md-right">' +
                                    '<span class="font-weight-bold mr-2">Missing Logs:</span>'+
                                '</div>';
                            if (parseInt(qualifiedObj.details.incident_details.error) == 0){
                                $.each(qualifiedObj.details.incident_details.missing, function (index, value) {
                                    missingLogs += '<li>' + value + '</li>';
                                })
                                missingLogsDetails = '<div class="col-12 col-md-5">'+
                                    '<ol>' + missingLogs +
                                    '</ol>'+
                                '</div>';
                                incidentDetails += missingLogsDetails;
                            }else{
                                // alert error
                            }
                
            } else {
                    var durationVal = parseFloat(qualifiedObj.details.incident_details.duration) / 60;
                    incidentDetails = '<div class="col-12 col-md-5">' +
                        '<p class = "font-weight-bold mb-0" > Shift: </p> '+
                        '<p class = "" id="qualifiedShift" >' + qualifiedObj.details.shift+ '</p>' +
                    '</div> '+
                    '<div class = "col-12 col-md-4" >'+
                        '<p class = "font-weight-bold mb-0">' + qualifiedObj.details.incident_details.log_label + ':</p>' +
                        '<p class = "" >' + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM M, Y h:mm A') + '</p>' +
                    '</div>'+
                    '<div class = "col-12 col-md-3" >'+
                        '<p class = "font-weight-bold mb-0"> Duration: </p> '+
                        '<p class = "">' + qualifiedObj.details.incident_details.duration + '</p> ' +
                    '</div>';
            }

            
            $('#incidentDetails').html(incidentDetails);
            // Rule
            $('#qualifiedRule').html(qualifiedObj.details.ir_rule.rule);
            // Check if previous occurence exist
            if (parseInt(qualifiedObj.details.with_prev) == 1){
                var withPreviousDetails = "";
                var previousDetailsHeader = "";
                var previousDetailsBody = "";
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) {
                    previousDetailsHeader = '<th class="text-center">Date</th>'+
                                            '<th class="text-center">Shift</th>'+
                                            '<th class="text-center">Allowable</th>'+
                                            '<th class="text-center">Covered</th>'+
                                            '<th class="text-center">Over Break</th>';
                    $.each(qualifiedObj.details.previous_details, function (index, value) {
                        previousDetailsBody += '<tr>'+
                                                    '<td class="text-center" style="font-weight:400">' + moment(value.date, 'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                                                    '<td class="text-center">'+value.shift+'</td>'+
                                                    '<td class="text-center" style="font-weight:400">'+value.allowable+'</td>'+
                                                    '<td class="text-center" style="font-weight:400">'+value.covered+'</td>'+
                                                    '<td class="text-center" style="font-weight:400">'+value.ob+'</td>'+
                                                '</tr>';
                    });
                }
                withPreviousDetails = '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">'+
                                            '<thead style="background: #555;color: white;">'+
                                                '<tr>'+ previousDetailsHeader+
                                                '</tr>'+
                                            '</thead>'+
                                            '<tbody>' + previousDetailsBody+
                                            '</tbody>'+
                                        '</table>';
                $('#previousDetails').html(withPreviousDetails);
                $('#withPreviousUserDtrViolation').show();
            }else{
                $('#withPreviousUserDtrViolation').hide();
            }
            // Check if IR history exist
            if (parseInt(qualifiedObj.details.with_history) == 1) {
                $('#withIrHistory').show();
            }else{
                $('#withIrHistory').hide();
            }

            // Offense Details
            $('#qualifiedOffenseType').text(qualifiedObj.details.offense_details.offense_type);
            $('#qualifiedOffense').text(qualifiedObj.details.offense_details.offense);
            $('#qualifiedCategory').text(qualifiedObj.details.action_details.category);
            $('#qualifiedLevel').text(qualifiedObj.details.action_details.level+" Level");
            $('#qualifiedDisciplinaryAction').text(qualifiedObj.details.action_details.action.toUpperCase());
            var qualifiedPeriodUnit = 'Month';
            if (parseInt(qualifiedObj.details.action_details.period) > 1){
                qualifiedPeriodUnit +='s';
            }
            $('#qualifiedPeriod').text(qualifiedObj.details.action_details.period + " " + qualifiedPeriodUnit);
            getEmployeeMult(0, 0, 0, '#qualifiedWitnesses' ,'Add Witnesses here', function (empExistStat) {
                $('#qualifiedModal').modal('show');
            })
        }else{

        }
    });
}

function get_emp_dtr_vio_ir_select(callback) {
    var sched_date = $('#dtrViolationSched').val();
    if (sched_date != '') {
        sched_date = moment($('#dtrViolationSched').val(), 'MM/DD/YYYY').format('Y-MM-DD');
    }
    $.when(fetchPostData({
            violationType: $('#violationType').val(),
            sched: sched_date
        }, '/discipline/get_specific_qualified_emp_ir')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        $('#fileDtrViolationEmpSelect').empty().trigger('change');
        console.log(empObj.record);
        if (empObj.record != 0) {
            $('#noIrableDtrViolation').hide();
            $('#irableDtrViolation').fadeIn();
        } else {
            $('#noIrableDtrViolation').fadeIn();
            $('#irableDtrViolation').hide();
        }
        init_select2(empObj, "#fileDtrViolationEmpSelect", function(notEmpty){
            callback(notEmpty);
        });
    });
}

function get_emp_dtr_vio_ir_record_select(startDate, endDate, callback) {
    $.when(fetchPostData({
        status: $('#fileDtrViolationRecordStatus').val(),
        violationType: $('#violationTypeRecord').val(),
        dateStart: startDate,
        dateEnd: endDate
    }, '/discipline/get_specific_qualified_emp_ir_record')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        console.log(empObj);
        $('#fileDtrViolationEmpRecordSelect').empty().trigger('change');
        init_select2(empObj, "#fileDtrViolationEmpRecordSelect", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

function get_emp_dtr_vio_ir_monitoring_record_select(startDate, endDate, callback) {
    $.when(fetchPostData({
        status: $('#fileDtrViolationMonitoringRecordStatus').val(),
        violationType: $('#violationTypeMonitoringRecord').val(),
        dateStart: startDate,
        dateEnd: endDate
    }, '/discipline/get_specific_qualified_emp_ir_record')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        console.log(empObj);
        $('#fileDtrViolationEmpRecordSelect').empty().trigger('change');
        init_select2(empObj, "#fileDtrViolationEmpRecordSelect", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

// EVENTS 
$('.dmsSupervisionMainNav').on('click', function (event) {
    $(".m-nav--inline").find(".active").removeClass("active");
    $(this).parent().addClass("active");
    $('.m-portlet .active').find(".active").removeClass("active");
    $('.m-portlet').hide();
    $('#' + $(this).data('navcontent') + '').addClass("active");
    $('#' + $(this).data('navcontent') + '').fadeIn();
    $(this).addClass("active");
})

$('#fileDtrViolationIrTab').on('mouseover', function(){
    initDatePicker('#dtrViolationSched', true, function() {
        get_emp_dtr_vio_ir_select(function (notEmpty) {
            initIrAbleDtrViolation();
        });
    });
})

$('#fileDtrViolationIrRecordTab').on('mouseover', function () {
    initIrAbleDtrViolationRecordDateRangePicker(function(dateRanges){
        var dateRange = $('#fileDtrViolationRecordDateRange').val().split('-');
        initIrAbleDtrVioRecord(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
    });
})

$('#violationType, #dtrViolationSched').on('change', function () {
    get_emp_dtr_vio_ir_select(function(notEmpty){
        initIrAbleDtrViolation();
    })
});

$('#fileDtrViolationEmpSelect').on('change', function(){
    initIrAbleDtrViolation();    
});

$('#violationTypeRecord, #fileDtrViolationRecordStatus').on('change', function () {
    var dateRange = $('#fileDtrViolationRecordDateRange').val().split('-');
    initIrAbleDtrVioRecord(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
});

$('#fileDtrViolationEmpRecordSelect').on('change', function () {
    var dateRange = $('#fileDtrViolationRecordDateRange').val().split('-');
    initIrAbleDtrViolationRecord(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
});


$(function(){
    initfileDtrViolationIrBadge(function(countObj){
        initDtrViolationBadge();
    });
})