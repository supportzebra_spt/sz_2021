// FUNCTION CALLS----------------------------------------------------->
function makeFileList() {
    console.log("huhu");
    var input = document.getElementById("upload_evidence_files");
    var ul = document.getElementById("fileList");
    var maxsize = 5000000;
    var exceed = 0;
    $("#files_select_label").text("Files Selected");
    $("#display_div_file").show();
    while (ul.hasChildNodes()) {
        ul.removeChild(ul.firstChild);
    }
    for (var i = 0; i < input.files.length; i++) {
        var li = document.createElement("li");
        li.innerHTML = input.files[i].name;
        var file_size = input.files[i].size;
        var g = input.files[i].name;
        var file_ext = g.split('.').pop();
        if (file_ext == "doc" || file_ext == "docx" || file_ext == "pdf" || file_ext == "txt" || file_ext ==
            "xml" ||
            file_ext == "xlsx" || file_ext == "xlsm" || file_ext == "ppt" || file_ext == "pptx" || file_ext ==
            "jpeg" ||
            file_ext == "jpg" || file_ext == "JPEG" || file_ext == "PNG" ||
            file_ext == "png" || file_ext == "mp4" || file_ext == "mov" || file_ext ==
            "3gp" || file_ext == "m4v" ||
            file_ext == "wmv" || file_ext == "avi" || file_ext == "mpeg" || file_ext == "mp3" || file_ext ==
            "wav" ||
            file_ext == "csv" || file_ext == "xlxx") {

            if (file_size <= maxsize) {
                ul.appendChild(li);
                $("#fileList li").removeClass("not_included");
            } else {
                ul.appendChild(li);
                exceed = 1;
                $("#fileList li").addClass("not_included");
                $("#files_select_label").text("File/s exceeded to 5mb. Please upload another.");
            }
        } else {
            ul.appendChild(li);
            exceed = 1;
            $("#fileList li").addClass("not_included");
            $("#files_select_label").text("Invalid file type. Please reupload allowed file types.");
        }
    }
    if (!ul.hasChildNodes()) {
        var li = document.createElement("li");
        li.innerHTML = 'No Files Selected';
        ul.appendChild(li);
        $("#files_select_label").text("");
        // console.log("button remove");
        $("#up_button").attr("disabled", true);
    }

    if (exceed == 1) {
        $("#sub_button").hide();
    } else {
        $("#sub_button").show();
    }
}

function fetch_media_evidences(irid) {
    var et = $("#evidence-types_ero").val();
    var ap = $("#attachment-purpose").val();
    var stringx = "";
    $.ajax({
        url: baseUrl + "/discipline/fetch_report_evidences/" + irid,
        type: "POST",
        data: {
            evidencetype: et,
            purpose: ap
        },
        cache: false,
        success: function (data) {
            var result = JSON.parse(data);
            console.log(result);
            var stringxx = "";
            var total_evidences = result.display.length;
            $("#label_attach").text(total_evidences + " Attachments");
            var file_name = "";
            stringxx = "<small id='irinfo' data-idir='" + irid + "' style='display:none'></small>";
            $("#passed_irid").html(stringxx);
            if (result.display.length != 0) {
                $.each(result.display, function (key, data) {
                    if(data.link.includes("uploads/dms/nor_nte/")){
                        file_name = data.link.replace("uploads/dms/nor_nte/", " ");
                    }else if(data.link.includes("uploads/dms/explain/")){
                        file_name = data.link.replace("uploads/dms/explain/", " ");
                    }else if(data.link.includes("uploads/dms/client/")){
                        file_name = data.link.replace("uploads/dms/client/", " ");
                    }else{
                        file_name = data.link.replace("uploads/dms/evidences/", " ");
                    }
                    if (data.mediaType == "image/jpeg" || data.mediaType == "image/png" || data
                        .mediaType == "image/jpg") {
                        stringx += "<div class='col-lg-4 col-md-12 mb-4 attachedFile' data-filename='"+file_name+"'>";
                        stringx += "<a href='" + baseUrl + "/" + data.link +
                            "' data-lightbox='roadtrip'>";
                        stringx += "<img src='" + baseUrl + "/" + data.link +
                            "' class='h_wdisplay z-depth-1'>";
                        stringx += "</a>";
                        stringx += "<div class='col-lg-12 col-md-6 mb-4 text-truncate'>";
                        stringx += "<span class='evidence_file_name'>" + file_name + "</span>";
                        stringx += "</div>";
                        if (data.incidentReportAttachmentPurpose_ID == "4") {
                            stringx += "<button type='link'   onclick='remove_attachment(" + data.ir_attachment_ID + "," + irid + ")' style='display:none' class='xbutton'>x</button>";
                        }
                        stringx += "</div>";
                    } else if (data.mediaType == "video/mp4" || data.mediaType == "video/mov" || data
                        .mediaType == "video/3gp" || data.mediaType == "video/mpeg" || data.mediaType ==
                        "video/avi" || data.mediaType == "video/mpg" || data.mediaType == "video/m4v") {
                        stringx += "<div class='col-lg-4 col-md-6 mb-4 attachedFile' style='position: relative;' data-filename='"+file_name+"'>";
                        stringx += "<div id='vidBox'>";
                        stringx += "<div id='videCont'>";
                        stringx += "<video width='120' height='86' id='example' controls>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/mp4'>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/mov'>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/avi'>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/mpg'>";
                        stringx += "<source src='" + baseUrl + "/" + data.link +
                            "' type='video/m4v'>";
                        stringx += "</video>";
                        stringx += "<div class='col-lg-12 col-md-6 mb-4 text-truncate'>";
                        stringx += "<span class='evidence_file_name'>" + file_name + "</span>";
                        stringx += "</div>";
                        stringx += "</div>";
                        stringx += "</div>";
                        if (data.incidentReportAttachmentPurpose_ID == "4") {
                            stringx += "<button type='link' onclick='remove_attachment(" + data.ir_attachment_ID + "," + irid + ")' style='display:none' class='xbutton'>x</button>";
                        }
                        stringx += "</div>";
                    } else if (data.mediaType == "audio/mp3" || data.mediaType == "audio/3gp" || data
                        .mediaType == "audio/wav" || data.mediaType == "audio/mpeg") {
                        stringx += "<div class='col-lg-4 col-md-6 mb-4 text-truncate attachedFile' style='position: relative;' data-filename='"+file_name+"' >";
                        // stringx += "<div>";
                        stringx += "<a href='#' class='a_audio_link2' data-toggle='modal' data-target='#audio_modal_evidences2' id='attached_button' data-audiolink='" +
                        data.link + "'>";
                        stringx += "<i class='fa fa-music icon_size2'></i>";
                        stringx += "<br><span>" + file_name + "</span>";
                        stringx += "</a>";
                        if (data.incidentReportAttachmentPurpose_ID == "4") {
                            stringx += "<button type='link' onclick='remove_attachment(" + data.ir_attachment_ID + "," + irid + ")' style='display:none' class='xbutton'>x</button>";
                        }
                        stringx += "</div>";
                    } else {
                        stringx += "<div class='col-lg-4 col-md-12 mb-4 text-truncate attachedFile' style='position: relative;' data-filename='"+file_name+"'>";
                        stringx += "<a target='_blank' href='" + baseUrl + "/" + data.link +
                            "'>";
                        if (data.mediaType == "application/vnd.open") {
                            stringx += "<i class='fa fa-file-word-o icon_size'></i>";
                        } else if (data.mediaType == "application/pdf") {
                            stringx += "<i class='fa fa-file-pdf-o icon_size'></i>";
                        } else if (data.mediaType == "application/vnd.ms-e") {
                            stringx += "<i class='fa fa-file-excel-o icon_size'></i>";
                        } else {
                            stringx += "<i class='fa fa-file icon_size'></i>";
                        }
                        stringx += "<br><span>" + file_name + "</span>";
                        stringx += "</a>";
                        if (data.incidentReportAttachmentPurpose_ID == "4") {
                            stringx += "<button type='link' onclick='remove_attachment(" + data.ir_attachment_ID + "," + irid + ")' style='display:none' class='xbutton'>x</button>";
                        }
                        stringx += "</div>";
                    }
                });
                console.log("exist");
            } else {
                stringx += "<div class='col-lg-12 col-md-12 mb-4 text-center mt-5'>";
                stringx += ""
                stringx += "<h4><i class='fa fa-paperclip' style='color:crimson;font-size:unset'></i> No attachments</h4>";
                stringx += "</div>";
                console.log("none");
            }
            $("#attachment_display_ero").html(stringx);
            // $('#evidence_display').find('.attachedFile').tooltip();
            // console.log($('#evidence_display').find('.attachedFile'))
        }
    });
}
function fetch_attachmentpurposes(irid) {
    var stringx_select = "";
    $.ajax({
        url: baseUrl + "/discipline/fetch_purposes/" + irid,
        type: "POST",
        cache: false,
        success: function (data) {
            var result = JSON.parse(data);
            stringx_select += "<option disabled selected=''>Select purpose</option>";
            stringx_select += "<option value=''>All</option>";
            $.each(result, function (key, data) {
                stringx_select += "<option value='" + data.incidentReportAttachmentPurpose_ID + "'>" + data.purpose + "</option>";
            });
            $("#attachment-purpose").html(stringx_select);
        }
    });
}

function dismiss_audio_modal(irattachmentid) {
    $('#audio_modal' + irattachmentid).modal('toggle');
}

function remove_attachment(irattachmentid, irid) {
    swal({
        title: 'Are you sure you want to remove this attachment?',
        text: "This will not be reverted once removed.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: baseUrl + "/discipline/remove_attachment",
                data: {
                    ir_attachmentid: irattachmentid
                },
                cache: false,
                success: function () {
                    swal(
                        'Deleted!',
                        'Attachment successfully removed.',
                        'success'
                    )
                    fetch_media_evidences(irid);
                },
                error: function (res) {
                    swal(
                        'Oops!',
                        'Something is wrong please contact adminstrator!',
                        'error'
                    )
                }
            });
        } else if (result.dismiss === 'cancel') {
            swal(
                'Cancelled',
                'You cancelled the removing of attachment',
                'error'
            )
        }
    });
}

function initIRero(startDate, endDate) {
    $('#irrecordsDatatable').mDatatable('destroy');
    recordIRDatatable.init(startDate, endDate);
}
function hrattachment(irid) {
    $("#evidence-types_ero").val("");
    $("#attachment-purpose").val("");
    $("#modal_attachedfiles").modal();
    fetch_media_evidences(irid);
    fetch_attachmentpurposes(irid);
    console.log(irid);
}
function getfilterdetails() {
    $.ajax({
        url: baseUrl + "/discipline/ero_recordsfilter",
        type: "POST",
        cache: false,
        success: function (data) {
            var result = JSON.parse(data);
            console.log(result.subject);
            var stringsubname = "";
            var string_offense = "";
            var string_nature_offense = "";
            stringsubname += "<option disabled selected=''>Select subject</option>";
            stringsubname += "<option value='0'>All</option>";
            string_offense += "<option disabled selected=''>Select offense</option>";
            string_offense += "<option value='0'>All</option>";
            string_nature_offense += "<option disabled selected=''>Select nature of offense</option>";
            string_nature_offense += "<option value='0'>All</option>";
            $.each(result.subject, function (key, data) {
                stringsubname += "<option value=" + data.subjectEmp_ID + ">" + data.subjectLname + ", " + data
                    .subjectFname + "</option>";
            });
            $.each(result.offense_type, function (key, data) {
                string_nature_offense += "<option value=" + data.offenseType_ID + ">" + data.offenseType + "</option>";
            });
            $.each(result.offense, function (key, data) {
                string_offense += "<option value=" + data.offense_ID + ">" + data.offense + "</option>";
            });

            $("#ir_subject").html(stringsubname);
            $("#offense_filter").html(string_offense);
            $("#nature_offense").html(string_nature_offense);
        }
    });
}

//Date filter
function initRecordDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#records-date .form-control').val(start + ' - ' + end);
        $('#records-date').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#records-date .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            // initEmployeeRecorNameField(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function(obj){
            initIRero(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'));


            //  initReport(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'));
            //    });
        });
        var dateRanges = {
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        };
        callback(dateRanges);
    });
}

function irmoredetails(irid) {
    $.ajax({
        url: baseUrl + "/discipline/access_ir_details_ero",
        type: "POST",
        data: {
            irid: irid
        },
        cache: false,
        success: function (data) {
            var result = JSON.parse(data);
            $("#ir_filed_details").html(result.ir_details_layout);
            $("#irDetails").modal();
        }
    });
}


// EVENTS ----------------------------------------------------->
$('#modal_attachedfiles').on('hidden.bs.modal', function () {
    formReset('#upload_evidence');
    $("#files_select_label").text("");
    $("#display_div_file").hide();
    $("#sub_button").hide();
    $("#upload_evidence_files").val("");
});

$("#upload_evidence").on('submit', function (e) {
    e.preventDefault();
    var record = new FormData(this);
    var irid = $("#irinfo").data('idir');
    $.ajax({
        url: baseUrl + "/discipline/add_additionalEvidences/" + irid,
        type: "POST",
        data: record,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var result = JSON.parse(data);
            if (result == 1) {
                swal({
                    position: 'center',
                    type: 'success',
                    title: 'File/s has been saved',
                    showConfirmButton: false,
                    timer: 1500
                });
                finalLastRecommend(6);
                fetch_media_evidences(irid);
                formReset('#upload_evidence');
                $("#files_select_label").text("");
                $("#display_div_file").hide();
                $("#sub_button").hide();
                $("#upload_evidence_files").val("");
                // $("#subjectExplainModal").modal("hide");
            } else {
                swal("Error",
                    "File were not successfully uploaded. Please contact the Administrator.",
                    'error');
            }
            // $("#modal_attachedfiles").modal("hide");
        }
    });

});

$('#evidence-types_ero').on('change', function () {
    var irid = $("#irinfo").data('idir');
    fetch_media_evidences(irid);
});

$('#attachment-purpose').on('change', function () {
    var irid = $("#irinfo").data('idir');
    fetch_media_evidences(irid);
});

$('#fetch_all_attachment').on('click', function () {
    var irid = $(this).data('id');
    fetch_media_attachment(irid);
});

$('#irrecords_button').on('click', function () {
    // var irid = $("#row_irid").data('irid');
    // fetch_media_evidences(irid);
    initRecordDateRangePicker(function (dateRanges) {
        initIRero(dateRanges.start, dateRanges.end);
        getfilterdetails();
    });
});

// $('#irrecordsDatatable').on('click', '.irDetailsBtn', function(){
//     var ir_id=$(this).data('irid'); 
//     $.ajax({
//         url: baseUrl +"/discipline/access_ir_details_ero",
//         type: "POST",
//         data: {
//             irid: ir_id
//         },
//         cache: false,
//         success: function(data) {
//             var result = JSON.parse(data);
//             $("#ir_filed_details").html(result.ir_details_layout);
//             $("#irDetails").modal();
//         }
//     });
// });

$('#employee_type, #liability_type, #report_type, #ir_subject, #offense_filter, #nature_offense').on('change', function () {
    var dateRange = $('#records-date').val().split('-');
    initIRero(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
})
$('#reports_irid').on('keyup', function () {
    var dateRange = $('#records-date').val().split('-');
    initIRero(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
})

$('#attachment_display_ero').on('mouseover', '.attachedFile', function(){
    let filename = $(this).data('filename');
    $(this).find('.xbutton').show();

    $(this).tooltip({
        title: filename,
        placement: "top"
    });
    $(this).tooltip('show')
})


$('#attachment_display_ero').on('mouseleave', '.attachedFile', function(){
    $(this).find('.xbutton').hide();
    $(this).tooltip('hide')
})


// $('#select2-empSelectIntervene-container').parents('.select2-container').tooltip({
//     title: "Select a Respondent",
//     placement: "top"
// });

$("#btn-downloadirrecords").click(function () {
    var st_date=$('#records-date').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var en_date=$('#records-date').data('daterangepicker').endDate.format('YYYY-MM-DD');
    $('#records-date').data('daterangepicker').endDate.format('YYYY-MM-DD')
    $.ajax({
        url: baseUrl +"/Discipline/downloadExcel_ir",
        method: 'POST',
        data: {
            violation_class: $('#employee_type').val(),
            violation_type: $('#liability_type').val(),
            report_type: $('#report_type').val(),
            violation_subject: $('#ir_subject').val(),
            offense: $('#offense_filter').val(),
            offense_nature: $('#nature_offense').val(),
            violation_ir_id: $('#reports_irid').val(),
            incidentDate_start:  $('#records-date').data('daterangepicker').startDate.format('YYYY-MM-DD'),
            incidentDate_end: $('#records-date').data('daterangepicker').endDate.format('YYYY-MM-DD'),
        },
        beforeSend: function () {
            mApp.block('body', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg',
                message: 'Downloading...'
            });
        },
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'SZ_DMS_IRrecords_'+st_date+'_to_'+en_date+'.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
            mApp.unblock('body');
        },
        error: function (data) {
            $.notify({
                message: 'Error in exporting excel'
            }, {
                    type: 'danger',
                    timer: 1000
                });
            mApp.unblock('body');
        }
    });
})

function downloadIr(irId){
    
}
$('#irrecordsDatatable').on('click', '.download_ir', function(){
    downloadIr($(this).data('irid'));
})

$('#attachment_display_ero').on('click', '.a_audio_link2', function() {
    link = $(this).data('audiolink');
    audio_stringxx = "";
    audio_stringxx += "<audio controls style='width: -webkit-fill-available;'>";
    audio_stringxx += "<source src='" + baseUrl + "/" + link + "' type='audio/mp3'>";
    audio_stringxx += "<source src='" + baseUrl + "/" + link + "' type='audio/mpeg'>";
    audio_stringxx += "<source src='" + baseUrl + "/" + link + "' type='audio/mpeg'>";
    audio_stringxx += "</audio>";
    $("#audio_evidence_player2").html(audio_stringxx);
})
// on load 
$(function () {
    // Select2 initialization

    $('#ir_subject').select2({
        placeholder: "Select IR Subject",
        width: '100%'
    });

    $('#offense_filter').select2({
        placeholder: "Select Offense",
        width: '100%'
    });

    $('#nature_offense').select2({
        placeholder: "Select Nature of Offense",
        width: '100%'
    });
    $('#attachment-purpose').select2({
        placeholder: "Select purpose",
        width: '100%'
    });
});

var recordIRDatatable = function () {
    var irrecord = function (start, end) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/list_irero_datatable",
                        params: {
                            query: {
                                emp_type: $('#employee_type').val(),
                                liability_type: $('#liability_type').val(),
                                report_type: $('#report_type').val(),
                                subject: $('#ir_subject').val(),
                                offense: $('#offense_filter').val(),
                                offense_nature: $('#nature_offense').val(),
                                re_irid: $('#reports_irid').val(),
                                startDate: start,
                                endDate: end
                                // empId: $('#empSelectPulseRecord').val(),
                                // statId: $('#ongoingStatus').val()
                            },
                        },
                        // map: function (raw) {
                        //     // sample data mapping
                        //     var dataSet = raw;
                        //     if (typeof raw.data !== 'undefined') {
                        //         dataSet = raw.data;
                        //     }
                        //     if (dataSet.length == 0) {
                        //         $('#downloadDailyPulseReportBtn').fadeOut();
                        //     } else {
                        //         $('#downloadDailyPulseReportBtn').fadeIn();
                        //     }
                        //     // $('#pendingCount').text(dataSet.length);
                        //     return dataSet;
                        // },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#reportSearch'),
            },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "incidentReport_ID",
                title: "IR ID",
                width: 120,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.incidentReport_ID.padLeft(8);
                    return html;
                },
            },
            {
                field: "subjectEmp_ID",
                title: "Subject",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.subjectFname + " " + row.subjectLname;
                    return html;
                },
            }, {
                field: "incidentDate",
                title: "Incident Date",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var memdate = moment(row.incidentDate, 'Y-MM-DD').format('MMM DD, Y');
                    var html = memdate;
                    return html;
                },
            }, {
                field: "finalAction",
                title: "Disciplinary Action",
                width: 115,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = row.finalAction;
                    return html;
                },
            }, {
                field: "",
                title: "Action",
                width: 120,
                selector: false,
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "";
                    var pdf = baseUrl+"/discipline/downloadPdf_ir/" + row.incidentReport_ID;
                    html += '<button type="button" onclick="irmoredetails(' + row.incidentReport_ID + ')" class="btn btn-sm btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-eye"></i></button>';
                    html += '&nbsp;&nbsp;<button onclick="hrattachment(' + row.incidentReport_ID + ')" type="button" class="btn btn-sm btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-upload"></i></button>';
                    html += '&nbsp;&nbsp;<a href="'+pdf+'" class="btn btn-sm btn-outline-success m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air download_ir" data-irid="' + row.incidentReport_ID + '"><i class="fa fa-download"></i></a>';                    
                    return html;
                },
            }],
        };
        var datatable = $('#irrecordsDatatable').mDatatable(options);
    };
    return {
        init: function (start, end) {
            irrecord(start, end);
        }
    };


}();