// DATATABLE

var offenseDatatable = function () {
    var offense = function (offenseCategoryIdVal, searchVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_offenses_datatable",
                        params: {
                            query: {
                                offenseCategoryId: offenseCategoryIdVal,
                                offenseSearchField: searchVal,
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#offenseSearchField'),
            },
            rows: {
                afterTemplate: function (row, data, index) {
                    $('th[data-field="offense"]').removeClass('m-datatable__cell--left');
                    $('th[data-field="offense"]').addClass('m-datatable__cell--center');
                    // $('.popoverTest').popover({
                    //     title: "Category ",
                    //     html: true,
                    //     container: 'body',
                    //     animation: true,
                    //     content:"test",
                    //     trigger: 'focus',
                    // })
                },
            },
            // columns definition
            columns: [{
                    field: "orderNum",
                    title: "#",
                    width: 70,
                    selector: false,
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = row.orderNum + ".";
                        return html;
                    },
                }, {
                    field: 'offense',
                    width: 320,
                    title: 'Offense Description',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'left',
                },
                {
                    field: 'multiCategoryLetters',
                    width: 90,
                    title: 'Category',
                    overflow: 'visible',
                    sortable: 'asc',
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        // var html = row.category.toUpperCase();
                        var categoryValue = "";
                        if ((row.multiCategoryLetters == 'all') || (row.multiCategoryLetters == '0')) {
                            categoryValue = "<span style='font-size:10px'>Depends on the gravity of the Offense</span>";
                        } else {
                            var multiCategoryLettersArr = row.multiCategoryLetters.split(',');
                            if (multiCategoryLettersArr.length > 1) {
                                var categString = row.multiCategoryLetters.toString()
                                // array.replace(/,/g, ", ")
                                categoryValue = categString.replace(/,/g, ", ").toUpperCase() + " <span style='font-size:10px'>(depending on the gravity of the Offense)</span>";
                            } else {
                                categoryValue = row.multiCategoryLetters.toString().toUpperCase();
                            }
                        }
                        return categoryValue;
                    },
                }
            ],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
        init: function (offenseCategoryIdVal, searchVal) {
            offense(offenseCategoryIdVal, searchVal);
        }
    };
}();

function showOffenseTypeTooltip(element) {
    $('.offenseCategoryAction').tooltip('hide');
    $(element).tooltip({
        html: true,
        title: $(element).data('offensetype'),
        trigger: "hover focus"
    });
    $(element).tooltip('show')
}

function checkNaturOffense(callback) {
    $.when(fetchGetData('/discipline/get_emp_qip_offenses')).then(function (qipSpecOffense) {
        var qipSpecOffenseObj = $.parseJSON(qipSpecOffense.trim());
        callback(qipSpecOffenseObj);
    });
}

function initOffenses(){
    $('#offenseDatatable').mDatatable('destroy');
    offenseDatatable.init($('#natureOfOffenseLabel').data('natureoffenseid'), $('#offenseSearchField').val());
}

function initSelectedNatureOfOffense(natureOfId){
    $.when(fetchPostData({
        offenseCategoryId: natureOfId,
    }, '/discipline/get_selected_offense_category_details')).then(function (offenseCategoryDetails) {
        var offenseCategoryDetailsObj = $.parseJSON(offenseCategoryDetails.trim());
        console.log(offenseCategoryDetailsObj);
        $('#natureOfOffenseLabel').text(offenseCategoryDetailsObj.category.letter.toUpperCase() + ". " + offenseCategoryDetailsObj.category.offenseType)
        $('#natureOfOffenseLabel').data('natureoffenseid', natureOfId);
        $('#specOffenseCategoryList').find('li').removeClass("list-active");
        $('#specOffenseCategoryList').find('span').removeClass("active-nav-a");
        $('#specOffenseCategoryList').find('button').removeClass("active-nav-a");
        $('#offenseCategory' + natureOfId).addClass("list-active");
        $('#offenseCategory' + natureOfId).find('span').addClass("active-nav-a");
        $('#offenseCategory' + natureOfId).find('button').addClass("active-nav-a");
        console.log(offenseCategoryDetailsObj.offenses.exist);
        if (parseInt(offenseCategoryDetailsObj.offenses.exist)){
            initOffenses();
            $('#withOffense').fadeIn();
            $('#noOffenses').fadeOut();
        }else{
            $('#withOffense').fadeOut();
            $('#noOffenses').fadeIn();
        }
    });
}

function getSpecOffenseCategory(natureOfOffenseVal, callback) {
    $.when(fetchPostData({
        searchOffenseCategory: $('#seachNatureOffenseVal').val(),
        natureOfOffenseType: natureOfOffenseVal
    }, '/discipline/get_all_nature_offense_view_general')).then(function (specOffenseCateg) {
        var specOffenseCategObj = $.parseJSON(specOffenseCateg.trim());
        let natureOffensesExist = 0;
        var specOffenseCateg = "";
         if (parseInt(specOffenseCategObj.exist) == 0){
             specOffenseCateg += '<li class="m-nav__item m-nav__item text-center font-weight-bold">Nature of Offense Not Found</li>';
             $('#specOffenseCategoryList').html(specOffenseCateg);
             if ($('#seachNatureOffenseVal').val().trim() !== ""){
            }else{
                $('#noNatureOffenseVal').text('No Nature of Offense Set');
             }
             $('#noNatureOffense').fadeIn();
             $('#natureOffenseDetails').fadeOut();
             $('#withOffense').fadeOut();
         }else{
             natureOffensesExist = 1;
             $('#noNatureOffense').fadeOut(); 
             $('#natureOffenseDetails').fadeIn();
             var specOffenseCateg = "";
             $.each(specOffenseCategObj.spec_offense_category, function (index, value) {
                 specOffenseCateg += '<li class="codOffense m-nav__item" id="offenseCategory' + value.offenseType_ID + '" style="padding-left: 0.5rem; padding-right: 0.5rem;cursor: pointer;" data-offensecategoryid="' + value.offenseType_ID + '">' +
                     '<a class="m-nav__link" class="specOffenseCategory" data-offensecategoryid="' + value.offenseType_ID + '" style="padding: 0.4rem !important;">' +
                     '<span class="m-nav__link-icon"><button type="button" class="offenseCategoryAction btn btn-sm btn-secondary m-btn" data-offensecategoryletter="' + value.letter + '" data-offensecategoryid="' + value.offenseType_ID + '" style="background: unset !important;font-size: 15px;border: none;padding-left: 0.5 rem;padding-top: 0.3rem;" data-offensetype="' + value.offenseType + '" onmouseover="showOffenseTypeTooltip(this)">' + value.letter.toUpperCase() + '.</button></span>' +
                     '<span class="m-nav__link-text d-none d-sm-block" style="padding-top: 0.3rem;padding-bottom: 0.1rem;">' + value.offenseType + '</span>' +
                     '</a>' +
                     '</li>';
             })
             console.log(specOffenseCateg);
        }
        $('#specOffenseCategoryList').html(specOffenseCateg);
        $('#specOffenseCategoryList').fadeIn();
         callback(natureOffensesExist);
        // $('#offenseCategoryModal').modal('show');
    });
}

function initNatureOfOffense(){
    var natureOffType = 1;
    checkNaturOffense(function(qipExist){
        if (parseInt(qipExist)) {
            $('#natureOfOffenseTypeContainer').fadeIn();
            natureOffType = $('#natureOfOffenseType').val();
        }else{
            $('#natureOfOffenseTypeContainer').fadeOut();
        }
        getSpecOffenseCategory(natureOffType, function(natureOffensesExist){
            console.log("Nature of Offense exist:" + natureOffensesExist);
            $('#noOffenses').hide();
            if(natureOffensesExist){
                initSelectedNatureOfOffense($('#specOffenseCategoryList').find('li').first().data('offensecategoryid'));
            }
        });
    });
}

function getCategoryOffense() {
    $.when(fetchGetData('/discipline/get_offense_category')).then(function (offenseCategory) {
        var offenseCategoryObj = $.parseJSON(offenseCategory.trim());
        var tableBody = "";
        if (offenseCategoryObj.length < 1) {
            $('#noCategoryOffense').fadeIn();
            $('#withCategoryOffense').hide();
            // $('#mainCategoryBtn').show();
        } else {
            $('#noCategoryOffense').hide();
            $('#withCategoryOffense').fadeIn();
            // $('#mainCategoryBtn').hide();
        }
        $('#categoryCount').text(offenseCategoryObj.length);
        $.each(offenseCategoryObj, function (index, value) {
            tableBody += '<tr class="">';
            rowSpanVal = value.offense_per_category.length;
            if (rowSpanVal < 1) {
                rowSpanVal++;
            }
            tableBody += '<th scope="row" rowspan="' + rowSpanVal + '" class="text-center text-capitalize align-middle" style="width: 19rem !important;border-top: 3px solid #b3b3b3;border-left: 5px solid #b3b3b3;border-bottom: 3px solid #b3b3b3;">' + value.category + '</th>';
            if (value.offense_per_category.length < 1) {
                tableBody += '<td class = "text-center text-uppercase" style="width: 11rem !important;border-top: 3px solid #b3b3b3;border-bottom: 3px solid #b3b3b3;">NOT SET</td>' +
                    '<td class = "text-center" style="border-top: 3px solid #b3b3b3;border-bottom: 3px solid #b3b3b3;">NOT SET</td style=""><td style="width: 7rem !important;border-bottom: 3px solid #b3b3b3;border-right: 3px solid #b3b3b3"></td></tr>';
            } else {
                var offenseCount = value.offense_per_category_count;
                // var categoryId = value.disciplineCategory_ID;
                $.each(value.offense_per_category, function (index, value) {
                    if (index == 0) {
                        var borderTop = "border-top: 3px solid #b3b3b3"
                    }
                    var isLastElement = parseInt(offenseCount) - 1;
                    if (index == isLastElement) {
                        var borderBottom = "border-bottom: 3px solid #b3b3b3"
                    } 
                    tableBody += '<td class = "text-center" style="width: 11rem !important;' + borderTop + ';' + borderBottom + '">' + ordinal(value.level) + '</td>' +
                        '<td class = "text-center" style="' + borderTop + ';' + borderBottom + ';border-right: 3px solid #b3b3b3">' + value.action + '</td>' +
                        '</tr>';
                })
            }
            tableBody += '</tr>';
        });

        $('#categoryOffenseTableBody').html(tableBody);
        
    });
}

function initDisciplinaryActionView() {
    $.when(fetchGetData('/discipline/get_disciplinary_action')).then(function (disciplinaryAction) {
        var disciplinaryActionObj = $.parseJSON(disciplinaryAction.trim());
        var tableBody = "";
        console.log(disciplinaryActionObj);
        if (disciplinaryActionObj.length > 0) {
            $('#noDiscAction').hide();
            $('#withDiscAction').fadeIn();
        } else {
            $('#noDiscAction').fadeIn();
            $('#withDiscAction').hide();
        }
        $.each(disciplinaryActionObj, function (index, value) {
            var month = "Month";
            var prescPeriod = value.periodMonth + ' ' + month;
            if (parseFloat(value.periodMonth) > 1) {
                month = "Months";
            }
            if(value.label == 't'){
                prescPeriod = "N/A"
            }
            tableBody += '<tr class="d-flex">' +
                '<th scope="row" class="text-capitalize text-center col-5">' + value.action + '</th>' +
                '<td class="text-center text-uppercase col-3">' + value.abbreviation + '</td>' +
                '<td class="text-center col-4">' + prescPeriod + '</td>' +
               '</tr>';
        });
        $('#discActionTableBody').html(tableBody);
    });
}

function initDtrViolationView(violationTypeId) {
    $.when(fetchPostData({
        dtrViolationTypeId: violationTypeId
    }, '/discipline/get_dtr_vio_view')).then(function (dtrViolation) {
        $('#dtrVioIcon').attr('class', $('#dtrViolations').find('span.m-nav__link-icon.active-nav-a.text-center.text-sm-left').children('i').attr('class'));
        $('#dtrVioTypeLabel').text($('#dtrViolations').find('span.m-nav__link-text.d-none.d-sm-block.active-nav-a').text());
        var dtrViolationContent = $.parseJSON(dtrViolation.trim());
        $('#dtrVioIrRule').html(dtrViolationContent);
    })
}

function initCodView(){
    initDisciplinaryActionView();
    getCategoryOffense();
}

$('.dmsMainNav').on('click', function (event) {
    $(".m-nav--inline").find(".active").removeClass("active");
    $(this).parent().addClass("active");
    $('.m-portlet .active').find(".active").removeClass("active");
    $('.m-portlet').fadeOut();
    $('#' + $(this).data('navcontent') + '').addClass("active");
    $('#' + $(this).data('navcontent') + '').fadeIn();
    $(this).addClass("active");
    if ($(this).data('navcontent') == 'dtrViolation'){
        initDtrViolationView($('#dtrViolations').find('.dtrAutoIrSideTab.m-nav__item.list-active').data('violationtypeid'));
    } else if ($(this).data('navcontent') == 'tab-memo') {
        getAllMemos();
    }
})

$('.dtrAutoIrSideTab').on('click', function (event) {
    initDtrViolationView($(this).data('violationtypeid'));
    $(".dtrAutoIrSideTab").removeClass("list-active");
    $(".dtrAutoIrSideTab").find(".m-nav__link").removeClass("active");
    $(".dtrAutoIrSideTab").find(".active-nav-a").removeClass("active-nav-a");
    $(this).addClass("list-active");
    $(this).find(".m-nav__link").addClass("active");
    $(this).find(".m-nav__link-icon").addClass("active-nav-a");
    $(this).find(".m-nav__link-text").addClass("active-nav-a");
})

$('#seachNatureOffenseVal').on('change', function(){
    initNatureOfOffense();
})

$('#offenseViewTab').on('click', function(){
    initNatureOfOffense()
})

$('#specOffenseCategoryList').on('click','.codOffense', function () {
    initSelectedNatureOfOffense($(this).data('offensecategoryid'));
})

$('#natureOfOffenseType').on('change', function () {
    initNatureOfOffense();
})

$('#discActionCategTab').on('click', function(){
    initCodView();
})




$(function(){
    if ($('#codViewTabs').find('.nav-link.m-tabs__link.active').attr('id') == 'discActionCategTab') {
        initCodView();
    }
});