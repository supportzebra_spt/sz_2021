// charise start code-----------------------------------------------------------------


// #On load

$('#specialIRForm')
    .find('[name="offensetypes"]')
    .change(function (e) {
        $('#specialIRForm').formValidation('revalidateField', 'offensetypes');
    })
    .end()
    .find('[name="incidentDate"]')
    .change(function (e) {
        $('#specialIRForm').formValidation('revalidateField', 'incidentDate');
    })
    .end()
    .find('[name="incidentTime"]')
    .change(function (e) {
        $('#specialIRForm').formValidation('revalidateField', 'incidentTime');
    })
    .end()
    .formValidation({
        message: 'This value is not valid',
        excluded: ':disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            incidentDate: {
                validators: {
                    notEmpty: {
                        message: 'Incident Date is Required!'
                    },
                }
            },
            incidentTime: {
                validators: {
                    notEmpty: {
                        message: 'Incident Time is Required!'
                    },
                }
            },
            descriptionIncident: {
                validators: {
                    //   stringLength: {
                    //     max: 45,
                    //     message: 'Input must be less than 45 characters'
                    // },
                    notEmpty: {
                        message: 'Description is Required!'
                    },
                }
            },
            placeIncident: {
                validators: {
                    //   stringLength: {
                    //     max: 45,
                    //     message: 'Input must be less than 45 characters'
                    // },
                    notEmpty: {
                        message: 'Place of incident is Required!'
                    },
                }
            },
            courseactionIncident: {
                validators: {
                    //   stringLength: {
                    //     max: 45,
                    //     message: 'Input must be less than 45 characters'
                    // },
                    notEmpty: {
                        message: 'Course of Action is Required!'
                    },
                }
            },
            offensetypes: {
                validators: {
                    notEmpty: {
                        message: 'Offense type is required!'
                    },
                }
            },
            offenses_select: {
                validators: {
                    notEmpty: {
                        message: 'Offense is required!'
                    },
                }
            },
            category_select: {
                validators: {
                    notEmpty: {
                        message: 'Category is required!'
                    },
                }
            },
            employee_names: {
                validators: {
                    notEmpty: {
                        message: 'Employee is required!'
                    },
                }
            },
        }
    }).on('success.form.fv', function (e, data) {
        e.preventDefault();
        $('#specialIRForm').formValidation('disableSubmitButtons', true);
        var frm = new FormData(this);
        var a = $("#witnesses_select").val();
        var t_fetch = $("#incidentTime").val();
        var timeof_incident = moment(t_fetch, 'H:mm').format('kk:mm');
        var message_witness = "";
        if (a.length != 0) {
            message_witness = "Please review and double check all details. It will not be modified once submitted.";
        } else {
            message_witness =
                "Please review all details. It will not be modified once submitted. Take note that you have not added any witness.";
        }
        swal({
            title: 'Are you sure you want to submit this Incident Report?',
            text: message_witness,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Submit IR'
        }).then(function (result) {
            if (result.value) {
                frm.append("termination", $("#pres_end").data('term'));
                frm.append("ir_filingtype", $("#offense_det_action").data('filing'));
                frm.append("prescriptionid", $("#withIrHistory_display").data('prescid'));
                frm.append("empid_subj", $("#employee_names_irSub").val());
                frm.append("position", $("#positionDesc").text());
                frm.append("account", $("#accountName").text());
                frm.append("emp_stat", $("#employee_names_irSub").find(':selected').data('empstat'));
                frm.append("incident_type", $("#incident_label").data("incident"));
                frm.append("date_ofincident", $("#incidentDate").val());
                frm.append("time_ofincident", timeof_incident);
                frm.append("description", $("#descriptionIncident").val());
                frm.append("place", $("#placeIncident").val());
                frm.append("courseaction", $("#courseactionIncident").val());
                frm.append("offensetype_id", $("#offense-types").val());
                frm.append("offense_id", $("#offenses").val());
                frm.append("offense_desc", $("#offenses option:selected").text());
                frm.append("category_id", $("#categories").val());
                frm.append("disciplinecategoryid", $("#suggested_dis_display").data('dacategoryid'));
                frm.append("period", $("#suggested_dis_display").data('periodmonth'));
                frm.append("disActioncategoryid", $("#discipline_details").data('dcid'));
                frm.append("disActioncategorysettingsid", $("#discipline_details").data('dcsid'));
                frm.append("prescription_end", $("#pres_end").data('cend'));
                frm.append("stat_identify", $("#natureoffense").data('ongoing'));
                frm.append("period_num", $("#period_month").data('period'));
                frm.append("occurence", $("#suggested_dis_label").data('occurence'));
                frm.append("witnesses", $("#witnesses_select").val());
                frm.append("irid_pending", $("#pendingtext_display").data('pendingir'));
                frm.append("acc_id", $("#employee_names_irSub").find(':selected').data('acc'));
                frm.append("emp_type", $("#employee_names_irSub").find(':selected').data('emptype'));
                $.ajax({
                    type: "POST",
                    url: baseUrl + "/discipline/save_specialIR",
                    data: frm,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (res) {
                        var result = JSON.parse(res);
                        if (result.with_witness == 1) {
                            systemNotification(result.notify_wit.notif_id,'dms');
                        }
                        if (result.with_subject == 1) {
                            systemNotification(result.notify_sub.notifsubj_id,'dms');
                            console.log("with notif");
                        } else {
                            console.log("without notif");
                        }
                        swal({
                            position: 'top-center',
                            type: 'success',
                            title: 'Successfully Submitted Incident Report',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $("#FileSpecialIRModal").modal('hide');
                    }

                });
            }
        });

    });


$(function () {
    // Select2 initialization

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "6000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };

    $('#offense-types').select2({
        placeholder: "Select Offense Type",
        width: '100%'
    });   
    // .trigger("change");

    $('#employee_names_irSub,#dismissedEmpMonitoring_s').select2({
        placeholder: "Select Employee Name",
        width: '100%'
    });
    $('#dismissedEmpMonitoring_s').select2({
        placeholder: "Select Subject",
        width: '100%'
    });
    $('#offenses').select2({
        placeholder: "--",
        width: '100%'
    });
    $('#categories').select2({
        placeholder: "--",
        width: '100%'
    });
    $('#witnesses_select').select2({
        placeholder: "Select Witnesses",
        width: '100%',
        allowClear: true
    });
    fetch_employeenames();
    check_ifsessionemployee_qip();

});


// function

function resetForm() {
    $("#defaultForm").formValidation('resetForm', true);
}

function makeFileList_fileir() {
    var input = document.getElementById("upload_evidence_files_fileir");
    var ul = document.getElementById("fileList_fileir");
    var maxsize = 5000000;
    var exceed = 0;
    $("#files_select_label_fileir").text("Files Selected");
    $("#display_div_file_fileir").show();
    while (ul.hasChildNodes()) {
        ul.removeChild(ul.firstChild);
    }
    for (var i = 0; i < input.files.length; i++) {
        var li = document.createElement("li");
        li.innerHTML = input.files[i].name;
        var file_size = input.files[i].size;
        var g = input.files[i].name;
        var file_ext = g.split('.').pop();


        if (file_ext == "doc" || file_ext == "docx" || file_ext == "pdf" || file_ext == "txt" || file_ext ==
            "xml" ||
            file_ext == "xlsx" || file_ext == "xls" || file_ext == "xlsm" || file_ext == "ppt" || file_ext == "pptx" || file_ext ==
            "jpeg" || file_ext == "JPEG" || file_ext == "PNG" || file_ext == "jpg" ||
            file_ext == "png" || file_ext == "mp4" || file_ext == "mov" || file_ext ==
            "3gp" || file_ext == "m4v" ||
            file_ext == "wmv" || file_ext == "avi" || file_ext == "mpeg" || file_ext == "mp3" || file_ext ==
            "wav" ||
            file_ext == "csv" || file_ext == "xlxx") {

            if (file_size <= maxsize) {
                ul.appendChild(li);
                $("#fileList_fileir li").removeClass("included");
            } else {
                ul.appendChild(li);
                exceed = 1;
                $("#fileList_fileir li").addClass("not_included");
                $("#files_select_label_fileir").text("File/s exceeded to 5mb. Please upload another.");
            }
        } else {
            ul.appendChild(li);
            exceed = 1;
            $("#fileList_fileir li").addClass("not_included");
            $("#files_select_label_fileir").text("Invalid file type. Please reupload allowed file types.");
        }
    }
    if (!ul.hasChildNodes()) {
        var li = document.createElement("li");
        li.innerHTML = 'No Files Selected';
        ul.appendChild(li);
        $("#files_select_label_fileir").text("");
    }
    if (exceed) {
        $('#specialIRForm').formValidation('disableSubmitButtons', true);
    } else {
        $('#specialIRForm').formValidation('disableSubmitButtons', false);
    }
}

function savespecialIR(filez) {
    var a = $("#witnesses_select").val();
    var t_fetch = $("#incidentTime").val();
    var timeof_incident = moment(t_fetch, 'H:mm').format('kk:mm');
    var message_witness = "";
    if (a.length != 0) {
        message_witness = "Please review and double check all details. It will not be modified once submitted.";
    } else {
        message_witness =
            "Please review all details. It will not be modified once submitted. Take note that you have not added any witness.";
    }
    swal({
        title: 'Are you sure you want to submit this Incident Report?',
        text: message_witness,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Submit IR'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: baseUrl + "/discipline/save_specialIR",
                data: {
                    termination: $("#pres_end").data('term'),
                    ir_filingtype: $("#offense_det_action").data('filing'),
                    prescriptionid: $("#withIrHistory_display").data('prescid'),
                    empid_subj: $("#employee_names_irSub").val(),
                    position: $("#positionDesc").text(),
                    account: $("#accountName").text(),
                    emp_stat: $("#employee_names_irSub").find(':selected').data('empstat'),
                    incident_type: $("#incident_label").data("incident"),
                    date_ofincident: $("#incidentDate").val(),
                    time_ofincident: timeof_incident,
                    description: $("#descriptionIncident").val(),
                    place: $("#placeIncident").val(),
                    courseaction: $("#courseactionIncident").val(),
                    offensetype_id: $("#offense-types").val(),
                    offense_id: $("#offenses").val(),
                    offense_desc: $("#offenses option:selected").text(),
                    category_id: $("#categories").val(),
                    disciplinecategoryid: $("#suggested_dis_display").data('dacategoryid'),
                    period: $("#suggested_dis_display").data('periodmonth'),
                    disActioncategoryid: $("#discipline_details").data('dcid'),
                    disActioncategorysettingsid: $("#discipline_details").data('dcsid'),
                    prescription_end: $("#pres_end").data('cend'),
                    stat_identify: $("#natureoffense").data('ongoing'),
                    period_num: $("#period_month").data('period'),
                    occurence: $("#suggested_dis_label").data('occurence'),
                    witnesses: $("#witnesses_select").val(),
                    irid_pending: $("#pendingtext_display").data('pendingir'),
                    filez: filez
                },
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    var result = JSON.parse(res);
                    if (result.with_witness == 1) {
                        systemNotification(result.notify_wit.notif_id,'dms');
                    }
                    if (result.with_subject == 1) {
                        systemNotification(result.notify_sub.notifsubj_id,'dms');
                    }
                    swal({
                        position: 'top-center',
                        type: 'success',
                        title: 'Successfully submitted an IR',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    $("#FileSpecialIRModal").modal('hide');
                }

            });
        }
    });
}

function swal_addevidence() {
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
            // result.dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
        } else if (result.dismiss === 'cancel') {
            swal(
                'Cancelled',
                'Your imaginary file is safe :)',
                'error'
            )
        }
    })
}

function reset_formfields() {
    $("#period_month").show();
    $("#pres_end").show();
    $("#period_label").show();
    $("#cureend_label").show();
    $("#offense_level").text("--");
    $("#disciplinary_action").text("--");
    $("#period_month").text("--");
    $("#pres_end").text("--");
    $("#withIrHistory_display").slideUp();
    $("#irhistory_content").empty();
    $("#categories").empty();
    $("#offenses").empty();
    $("#pendingtext_display").text("");
    $("#positionDesc").text("{ Position }");
    $("#accountName").text("{ Account / Department }");
    $("#witnesses_select").val([]).trigger("change");
    $("#upload_evidence_files_fileir").val("");
    $("#files_select_label_fileir").text("");
    $("#display_div_file_fileir").hide();
    $("#alert_notif").hide();
    $("#empPicSpecialir").attr("src","http://3.18.221.50/sz/assets/images/img/sz.png");
    reset_name_offense_selection();
    // $("#employee_names").val([]).trigger("change");
    // $("#offense-types").val([]).trigger("change");
}

function reset_name_offense_selection() {
    $("#employee_names_irSub").empty();
    $("#offense-types").empty();
    fetch_employeenames();
    check_ifsessionemployee_qip();
}

function reset_offensedetails_discaction() {
    $("#offense-types").val(null);
    $("#offense-types option:eq(0)").prop("selected", true);
    $("#offense_level").text("--");
    $("#disciplinary_action").text("--");
    $("#period_month").text("--");
    $("#pres_end").text("--");
    $("#withIrHistory_display").slideUp();
    $("#irhistory_content").empty();
    $("#categories").empty();
    $("#offenses").empty();
    $("#alert_notif").hide();
    $("#pendingtext_display").text("");
    select_offensetypes(1);
    // $("#offense-types").empty();
    // check_ifsessionemployee_qip();
}



function fetchdatepicker_ir() {
    getCurrentDateTime(function (date) {
        $("#incidentDate").datepicker({
            autoclose: true,
            todayBtn: true,
            endDate: moment(date).format('MM/DD/YYYY')
        });
    });
}

function fetchtimepicker() {
    $("#incidentTime").timepicker({
        format: 'h:mm:ss p',
        autoclose: true,
        todayBtn: true,
        // interval: 1
        minuteStep: 1
    });
}
function fetch_employeenames() {
    $.ajax({
        type: "POST",
        url: baseUrl + "/discipline/get_all_employees",
        cache: false,
        success: function (res) {
            var result = JSON.parse(res);
            var stringx = "<option disabled selected=''>{ Select employee name }</option>";
            var stringx_wit = "";
            $.each(result, function (key, data) {
                stringx += "<option data-acc=" + data.acc_id + " data-emptype=" + data.acc_description + " data-empstat=" + data.estatus + " value=" + data
                    .emp_id + ">" +
                    data.lname + ", " + data
                        .fname + "</option>";
                stringx_wit += "<option data-acc=" + data.acc_id + " data-emptype=" + data.acc_description + " data-empstat=" + data.estatus + " value=" + data
                    .emp_id +
                    ">" + data.lname + ", " + data
                        .fname + "</option>";
            });
            $("#employee_names_irSub").html(stringx);
            $("#witnesses_select").html(stringx_wit);
            // $("#employee_names").trigger("change");
        }
    });
}

function fetch_selected_all_options() {
    $.ajax({
        type: "POST",
        url: baseUrl + "/discipline/get_all_employees",
        cache: false,
        success: function (res) {
            var result = JSON.parse(res);
            var stringx = "<option disabled selected=''>Select employee name</option>";
            $.each(result, function (key, data) {
                stringx += "<option value=" + data.emp_id + ">" + data.lname + ", " + data
                    .fname + "</option>";
            });
            $("#employee_names_irSub").html(stringx);
            $("#employee_names_irSub").trigger("change");
        }
    });
}

function select_offensetypes(access) {
    $("#suggested_dis_label").slideDown();
    $("#suggested_dis_display").slideDown();
    $("#pendingtext_display").text("");
    $("#alert_notif").hide();
    var selected_offense=0;
    var empid = $("#employee_names_irSub").val();


    // if access have not yet selected pop up select 
    if(access==1){
        selected_offense = $("#incident_label").data("incident");
    }else{
        selected_offense = $("#selectIncident_type").val();
    }
    var offense_id = $('#incident_select').data('offenseTypeid');
    var account_id = $('#incident_select').data('accqip');
    var stringx = "<option disabled selected=''>Select Offense Type</option>";
    var stringxemp = "";
    var stringx_wit = "";

    $("#incident_label").data('incident', selected_offense);

    if (empid != null) {
        $.ajax({
            type: "POST",
            url: baseUrl + "/discipline/incident_type",
            data: {
                offense_val: selected_offense,
                offense_id: offense_id,
                accountID: account_id,
                emp_id: empid
            },
            cache: false,
            success: function (res) {
                var result = JSON.parse(res);
                if (selected_offense == 2) {
                    $("#incident_select").text("QUALITY IMPROVEMENT PLAN");
                    $("#incident_label").data('incident', '2');
                } else {
                    $("#incident_select").text("NORMAL");
                    $("#incident_label").data('incident', '1');
                }
                if (result.offense_val != 0) {
                    $.each(result.offense_val, function (key, item) {
                        stringx += "<option value=" + item.offenseType_ID + ">" + item.letter +
                            ". " +
                            item.offenseType + "</option>";
                    });
                } else {
                    var message =
                        "No nature of offense set as QIP under the selected subject's account";
                    var message_type = "1";
                    alert_notif_message(message, message_type);
                    toastr.error("No nature of offense set as QIP under the selected subject's account.");
                }
                stringxemp = "<option disabled selected=''>{ Select employee name }</option>";
                $.each(result.employee_qip, function (key, data) {
                    stringxemp += "<option value=" + data.emp_id + ">" + data.lname + ", " + data
                        .fname + "</option>";
                    stringx_wit += "<option value=" + data.emp_id + ">" + data.lname + ", " + data
                        .fname + "</option>";
                });
                $("#offense-types").html(stringx);
                $("#witnesses_select").html(stringx_wit);
                $("#categories").empty();
                $("#offenses").empty();
                $("#withIrHistory_display").slideUp();
                $("#irhistory_content").empty();
                $("#offense_level").text("--");
                $("#disciplinary_action").text("--");
                $("#period_month").text("--");
                $("#pres_end").text("--");
                (($('[data-toggle="popover"],[data-original-title]').popover('dispose').data(
                    'bs.popover') || {}).inState || {}).click = false;
            }
        });
    } else {
        alert_notif_message('Please select an employee first as subject for IR', '2');
        toastr.error("Please select an employee first as subject for IR.");
    }
}

function evaluate_employeeselected() {
    $("#witnesses_select").empty();
    var empid = $("#employee_names_irSub").val();
    var a = $("#employee_names_irSub").find(':selected').data('empstat');
    var acc = $("#employee_names_irSub").find(':selected').data('acc');
    var emptype = $("#employee_names_irSub").find(':selected').data('emptype');
    $("#alert_notif").slideUp();
    $("#period_month").show();
    $("#pres_end").show();
    $("#period_label").show();
    $("#cureend_label").show();
    $("#offense_level").text("--");
    $("#disciplinary_action").text("--");
    $("#period_month").text("--");
    $("#pres_end").text("--");
    $("#pendingtext_display").text("");
    $.ajax({
        type: "POST",
        url: baseUrl + "/discipline/get_selected_employee_forIR",
        data: {
            emp_id: empid,
        },
        cache: false,
        success: function (res) {
            var result = JSON.parse(res);
            var stringx_wit = "";
            if (result != null) {
                $("#categories").empty();
                $("#offense-types").val(null);
                $("#offenses").empty();
                $("#withIrHistory_display").slideUp();
                $("#irhistory_content").empty();
                var account_id = result.empdet.acc_id;
                var name_string = "";
                if (empid != null) {
                    $("#positionDesc").text(result.empdet.positionDescription);
                    $("#accountName").text(result.empdet.accountDescription);
                    $("#empPicSpecialir").attr("src",baseUrl +"/"+ result.empdet.pic);
                }
              $.each(result.witselect, function (key, data) {
                stringx_wit += "<option data-acc=" + data.acc_id + " data-emptype=" + data.acc_description + " data-empstat=" + data.estatus + " value=" + data
                    .emp_id +
                    ">" + data.lname + ", " + data
                        .fname + "</option>";
            });
             $("#witnesses_select").html(stringx_wit);
            select_offensetypes(1);
            }
        }
    });
}
function check_ifsessionemployee_qip() {
    // Check if employee selected is assigned with QIP filing privilege
    var stringx = "<option disabled selected=''>Select Offense Type</option>";
    $.ajax({
        type: "POST",
        url: baseUrl + "/discipline/qip_privilege",
        cache: false,
        success: function (res) {
            var result = JSON.parse(res);
            if (result.enable == "1") {
                $('#incident_select').data('qip', 'yes');
                $('#incident_select').data('accqip', result.offense_types_qip[0].acc_id);
                $('#incident_select').data('offenseTypeid', result.offense_types_qip[0]
                    .offenseType_ID);
                $('#incident_select').data('offenseTypeid_ass', result.offense_types_qip[0]
                    .offenseTypeAssignment_ID);
            } else {
                $('#incident_select').data('qip', 'no');
            }
            $.each(result.offense_type, function (key, item) {
                stringx += "<option value=" + item.offenseType_ID + ">" + item.letter +
                    ". " +
                    item.offenseType + "</option>";
            });
            $("#offense-types").html(stringx);

        }
    });
}

function selectPopupIncidentType(element) {
    var qip_priv = $(element).data('qip');
    var normal = "";
    var qip = "";
    var incident_sel = $("#incident_label").data("incident");

    if (incident_sel == "1") {
        normal = "selected";
    } else {
        qip = "selected";
    }
    var oneFieldPopoverHead = '<div class="col-md-12">' +
        '<div class="form-group m-form__group row" style="margin-bottom: -7px;">' +
        '<div class="col-lg-12 mb-1 popoverErr" style="color:#ff7d7d; font-size: 11px; display:none"></div>';
    var oneTimeField = '<div class="col-lg-12 mb-3 pl-1 pr-1">' +
        '<select class="custom-select form-control" name="selectIncident_type" id="selectIncident_type" data-column="' +
        $(element).data('column') + '" data-columnid="' + $(element).attr('id') +
        '" onchange="select_offensetypes(2)">' +
        '<option value="1" ' + normal + '>NORMAL</option>' +
        '<option id="qip_opt" value="2" ' + qip + '>QIP</option>' +
        '</select>' +
        '</div>' +
        '</div></div>';

    var oneTimeFieldPopoverBody = oneFieldPopoverHead + oneTimeField;
    $(element).popover({
        title: $(element).data('column'),
        html: true,
        container: 'body',
        animation: true,
        content: oneTimeFieldPopoverBody,
        trigger: 'click',
    }).click(function (event) {
        $(element).popover('show');
        if (qip_priv == "no") {
            $("#qip_opt").remove();
        }
    });
}

$(document).on('click', function (e) {
    $('#FileSpecialIRModal').find('[data-toggle="popover"],[data-original-title]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            (($(this).popover('dispose').data('bs.popover') || {}).inState || {}).click = false // fix for BS 3.3.6
        }
    });
});

// $('html').on('click',':not(.popover),:not(#incident_select)', function(){
//     console.log('dispose popover');
//     $('#incident_select').popover('dispose');
// })

function alert_notif_message(message, message_type) {
    var strong_note = "";
    var notif_stringx = "";
    if (message_type == 2 || message_type == 3) {
        strong_note = "Wait there!";
    } else {
        strong_note = "Oops!";
    }
    notif_stringx +=
        "<div class='alert_details_custom m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible show' role='alert'>";
    notif_stringx +=
        "<div class='m-alert__icon'><i class='flaticon-exclamation-1'></i><span></span></div>";
    notif_stringx +=
        "<div class='m-alert__text'><strong>" + strong_note + "</strong>" + message + "</div>";
    notif_stringx += "<div class='m-alert__close'>";
    notif_stringx +=
        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'></button></div></div>";
    $("#alert_notif").show();
    $("#alert_notif").html(notif_stringx);
}

$('#link_attachedfiles').on('click', function () {
    $('#FileSpecialIRModal').modal('hide');
})

//event
// MODAL FORM RESET 
$('#FileSpecialIRModal').on('hidden.bs.modal', function () {
    formReset('#specialIRForm');
    reset_formfields();
});

$('#incidentDate').on('change', function () {
    reset_offensedetails_discaction(); 
});
$('#offense-types').on('change', function () {
    var selected_offensetype = $(this).val();
    $("#suggested_dis_label").slideDown();
    $("#suggested_dis_display").slideDown();
    $("#pendingtext_display").text("");
    $.ajax({
        type: "POST",
        url: baseUrl + "/discipline/get_offenses_selected_OffenseType",
        data: {
            offense_id: $(this).val(),
            tofetch: "1"
        },
        cache: false,
        success: function (res) {
            var result = JSON.parse(res);
            var stringx = "";
            var notif_stringx = "";
            $("#categories").empty();
            $("#withIrHistory_display").slideUp();
            $("#irhistory_content").empty();
            $("#offense_level").text("--");
            $("#disciplinary_action").text("--");
            $("#period_month").text("--");
            $("#pres_end").text("--");
            $("#period_month").show();
            $("#pres_end").show();
            $("#period_label").show();
            $("#cureend_label").show();
            if (selected_offensetype != null) {
                if (result.exist == 1) {
                    $("#alert_notif").hide();
                    $("#submitIRbutton").prop('disabled', false);
                    stringx = "<option disabled selected=''>Select Offenses</option>";
                    $.each(result.record, function (key, data) {
                        stringx += "<option value=" + data.offense_ID + ">" + data
                            .orderNum + ". " + data.offense + "</option>";
                    });
                } else if (result.exist == 0) {
                    stringx = "<option disabled selected=''>No Set Offenses</option>";
                    notif_stringx +=
                        "<div class='alert_details_custom m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible show' role='alert'>";
                    notif_stringx +=
                        "<div class='m-alert__icon'><i class='flaticon-exclamation-1'></i><span></span></div>";
                    notif_stringx +=
                        "<div class='m-alert__text'><strong>Oops!</strong> No offense set for the selected offense type</div>";
                    notif_stringx += "<div class='m-alert__close'>";
                    notif_stringx +=
                        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'></button></div></div>";
                    $("#alert_notif").show();
                    $("#alert_notif").html(notif_stringx);
                    $("#submitIRbutton").attr("disabled", true);
                }
                $("#offenses").html(stringx);
                // $("#offenses").trigger("change");
            }
        }
    });
})

$('#offenses').on('change', function () {
    var selected_offense = $(this).val();
    var letters_each = "";
    var ir_stringx = "";
    var month_label = "";
    $("#suggested_dis_label").slideDown();
    $("#suggested_dis_display").slideDown();
    $("#pendingtext_display").text("");
    if ($("#employee_names_irSub").val() != null && $("#incidentDate").val() != "") {
        $.ajax({
            type: "POST",
            url: baseUrl + "/discipline/get_offenses_selected_OffenseType",
            data: {
                offense_id: $(this).val(),
                subj_emp_id: $("#employee_names_irSub").val(),
                incident_date: $("#incidentDate").val(),
                tofetch: "2"
            },
            cache: false,
            success: function (res) {
                var result = JSON.parse(res);
                $("#withIrHistory_display").slideUp();
                $("#irhistory_content").empty();
                $("#offense_level").text("--");
                $("#disciplinary_action").text("--");
                $("#period_month").text("--");
                $("#pres_end").text("--");
                $("#period_month").show();
                $("#pres_end").show();
                $("#period_label").show();
                $("#cureend_label").show();
                if (selected_offense != null) {
                    if (result != null) {
                        var stringx = "";
                        stringx = "<option disabled selected=''>Select Category</option>";

                        // First Level categories or all categories
                        if (result.exist_ornot == 0 || result.exist_ornot == 3) {
                            $("#offense_det_action").data('filing', '1');

                            $.each(result.categories, function (key, data) {
                                letters_each = data.category.toUpperCase();
                                stringx += "<option value=" + data
                                    .disciplineCategory_ID + ">Category" + " " +
                                    letters_each + "</option>";
                            });
                            
                        } else if (result.exist_ornot == 2) {
                            // Has pending or ongoing IR - attach as annex
                            $("#offense_det_action").data('filing', '2');
                            letters_each = result.categories.category.toUpperCase();
                            stringx += "<option value=" + result.categories
                                .disciplineCategory_ID + " selected>Category" + " " +
                                letters_each + "</option>";
                            $("#suggested_dis_label").slideUp();
                            $("#suggested_dis_display").slideUp();
                            $("#pendingtext_display").text(
                                "The employee has a related pending IR.The IR to be filed will serve as annex or attachment of the previous pending IR."
                            );
                            $("#natureoffense").data('ongoing', '2');
                            $("#suggested_dis_label").data('occurence', '0');
                            $("#pendingtext_display").data('pendingir', result.categories
                                .incidentReport_ID);
                            $("#discipline_details").data('dcid', result.categories
                                .disciplinaryActionCategory_ID);
                            $("#discipline_details").data('dcsid', result.categories
                                .disciplinaryActionCategorySettings_ID);
                        } else {
                            // Has IR history - IR history table display

                            $("#suggested_dis_label").data('occurence', result.categories
                                .occurence);
                            $("#offense_det_action").data('filing', '3');
                            letters_each = result.spec_category[0].category.toUpperCase();

                            stringx += "<option value=" + result.spec_category[0]
                                .disciplineCategory_ID + " selected>Category" + " " +
                                letters_each + "</option>";

                            $.each(result.history, function (key, data) {
                                var cdate = "N/A";
                                if (data.label !== 't') {
                                    cdate = moment(data.prescriptionEnd, 'Y-MM-DD').format('MMM DD, Y');
                                }
                                ir_stringx += "<tr>";
                                ir_stringx +=
                                    "<td class='text-center' style='font-weight:400'>" +
                                    data.incidentReport_ID.padLeft(8) + "</td>";
                                ir_stringx +=
                                    "<td class='text-center' style='font-weight:400'>" + moment(data.incidentDate, 'Y-MM-DD').format('MMM DD, Y') + "</td>";
                                ir_stringx +=
                                    "<td class='text-center' style='font-weight:400'>" +
                                    data.sugAbbreviation.toUpperCase() + "</td>";
                                ir_stringx +=
                                    "<td class='text-center' style='font-weight:400'>" +
                                    data.abbreviation.toUpperCase() + "</td>";
                                ir_stringx +=
                                    "<td class='text-center' style='font-weight:400'>" +
                                    cdate + "</td>";
                                ir_stringx += "<tr>";
                            });

                            $("table tbody#irhistory_content").html(ir_stringx);

                            $("#withIrHistory_display").slideToggle();
                            $("#withIrHistory_display").data('prescid', result.history[0]
                                .prescriptiveId);
                            console.log("Categ ID: " + result.nextlevel
                                .disciplinaryActionCategory_ID)
                            $("#discipline_details").data('dcid', result.nextlevel
                                .disciplinaryActionCategory_ID);
                            $("#discipline_details").data('dcsid', result.nextlevel
                                .disciplinaryActionCategorySettings_ID);
                            $("#offense_level").text(result.nextlevel.level);
                            $("#disciplinary_action").text(result.nextlevel.action
                                .toUpperCase());
                            if (result.nextlevel.periodMonth == 1) {
                                month_label = "month";
                            } else {
                                month_label = "months";
                            }
                            if (result.nextlevel.label == "t") {
                                $("#period_month").hide();
                                $("#pres_end").hide();
                                $("#period_label").hide();
                                $("#cureend_label").hide();
                                $("#pres_end").data('term', '1');
                            } else {
                                $("#period_month").show();
                                $("#pres_end").show();
                                $("#period_label").show();
                                $("#cureend_label").show();
                                $("#period_month").text(result.nextlevel.periodMonth + " " +
                                    month_label);
                                $("#pres_end").data('term', '0');
                            }
                            $("#period_month").data('period', result.nextlevel
                                .periodMonth);
                            var cdate = moment(result.curedate, 'Y-MM-DD').format('MMM DD, Y')
                            $("#pres_end").text(cdate);
                            $("#pres_end").data('cend', result.curedate);

                            $("#natureoffense").data('ongoing', '1');
                            $("#suggested_dis_display").data('dacategoryid', result.nextlevel
                                .disciplinaryActionCategory_ID);
                            $("#suggested_dis_display").data('periodmonth', result.nextlevel
                                .periodMonth);
                        }
                    } else {
                        stringx = "<option disabled selected=''>No Category</option>";
                    }
                    $("#categories").html(stringx);
                    // $("#categories").trigger("change");
                }
            }
        });
    } else {
        var message = "";
        var message_type = "";
        if ($("#employee_names_irSub").val() == null) {
            message_type = "2";
            message = "Please select an employee as a subject or Person of Interest of the IR to be filed.";
        } else if ($("#incidentDate").val() == "") {
            message_type = "3";
            message = "Please fill in all form fields in the Incident details section.";
        }
        alert_notif_message(message, message_type);
    }
})
$('#categories2').on('change', function () {
    var ir_stringx = "";
    $("#period_month").show();
    $("#pres_end").show();
    $("#period_label").show();
    $("#cureend_label").show();
    $("#offense_level").text("--");
    $("#disciplinary_action").text("--");
    $("#period_month").text("--");
    $("#pres_end").text("--");
    if ($("#employee_names_irSub").val() != null) {
        $.ajax({
            type: "POST",
            url: baseUrl + "/discipline/check_existing_IR_same_offense",
            data: {
                offense_id: $("#offenses").val(),
                category: $(this).val(),
                subject_emp_id: $("#employee_names_irSub").val(),
                incident_date: $("#incidentDate").val()
            },
            cache: false,
            success: function (res) {
                var result = JSON.parse(res);
                var month_label = "";
                $("#withIrHistory_display").slideUp();
                $("#irhistory_content").empty();

                if (result.firstlevel.periodMonth == 1) {
                    month_label = "month";
                } else {
                    month_label = "months";
                }
                var disact_firstlevel = result.firstlevel.action.toUpperCase();
                var cdate = moment(result.curedate, 'Y-MM-DD').format('MMM DD, Y');

                $("#offense_level").text(result.firstlevel.level);
                $("#disciplinary_action").text(disact_firstlevel);
                $("#discipline_details").data('dcid', result.firstlevel
                    .disciplinaryActionCategory_ID);
                $("#discipline_details").data('dcsid', result.firstlevel
                    .disciplinaryActionCategorySettings_ID);

                if (result.firstlevel.label == "t") {
                    // $("#period_month").text("No Period");
                    // $("#pres_end").text("No Cure Date");
                    $("#period_month").hide();
                    $("#pres_end").hide();
                    $("#period_label").hide();
                    $("#cureend_label").hide();
                    $("#pres_end").data('term', '1');
                } else {
                    $("#period_month").show();
                    $("#pres_end").show();
                    $("#period_label").show();
                    $("#cureend_label").show();
                    $("#period_month").text(result.firstlevel.periodMonth + " " + month_label);
                    $("#period_month").data("period", result.firstlevel.periodMonth);
                    $("#pres_end").data('term', '0');
                    $("#pres_end").text(cdate);
                    $("#pres_end").data('cend', result.curedate);
                }
                $("#natureoffense").data('ongoing', '0');
                $("#suggested_dis_display").data('dacategoryid', result.firstlevel
                    .disciplinaryActionCategory_ID);
                $("#suggested_dis_display").data('periodmonth', result.firstlevel.periodMonth);
                $("#suggested_dis_label").data('occurence', '1');
            }
        });
    } else {
        var message = "Please select an employee as a subject or Person of Interest of the IR to be filed.";
        var message_type = "2";
        alert_notif_message(message, message_type);
    }
});

//NEW CODE
$('#categories').on('change', function () {
    var ir_stringx = "";
    $("#period_month").show();
    $("#pres_end").show();
    $("#period_label").show();
    $("#cureend_label").show();
    $("#offense_level").text("--");
    $("#disciplinary_action").text("--");
    $("#period_month").text("--");
    $("#pres_end").text("--");
    if ($("#employee_names_irSub").val() != null) {
        $.ajax({
            type: "POST",
            url: baseUrl + "/discipline/check_existing_IR_same_offense2",
            data: {
                offense_id: $("#offenses").val(),
                category: $(this).val(),
                subject_emp_id: $("#employee_names_irSub").val(),
                incident_date: $("#incidentDate").val()
            },
            cache: false,
            success: function (res) {
                var result = JSON.parse(res);
                var month_label = "";
                var disact_firstlevel = result.firstlevel.action.toUpperCase();
                var cdate = moment(result.curedate, 'Y-MM-DD').format('MMM DD, Y');

                // $("#withIrHistory_display").slideUp();
                // $("#irhistory_content").empty();

                // check level of periods 
                if (result.firstlevel.periodMonth == 1) {
                    month_label = "month";
                } else {
                    month_label = "months";
                }

                // if it has history 

                if(result.history_stat==1){

                    $.each(result.history, function (key, data) {
                        var cdate = "N/A";
                        if (data.label !== 't') {
                            cdate = moment(data.prescriptionEnd, 'Y-MM-DD').format('MMM DD, Y');
                        }
                        ir_stringx += "<tr>";
                        ir_stringx +=
                            "<td class='text-center' style='font-weight:400'>" +
                            data.incidentReport_ID.padLeft(8) + "</td>";
                        ir_stringx +=
                            "<td class='text-center' style='font-weight:400'>" + moment(data.incidentDate, 'Y-MM-DD').format('MMM DD, Y') + "</td>";
                        ir_stringx +=
                            "<td class='text-center' style='font-weight:400'>" +
                            data.sugAbbreviation.toUpperCase() + "</td>";
                        ir_stringx +=
                            "<td class='text-center' style='font-weight:400'>" +
                            data.abbreviation.toUpperCase() + "</td>";
                        ir_stringx +=
                            "<td class='text-center' style='font-weight:400'>" +
                            cdate + "</td>";
                        ir_stringx += "<tr>";
                    });

                    $("table tbody#irhistory_content").html(ir_stringx);


                    //SECOND WAVE

                    $("#withIrHistory_display").data('prescid', result.history[0]
                    .prescriptiveId);
                    $("#discipline_details").data('dcid', result.nextlevel
                    .disciplinaryActionCategory_ID);
                    $("#discipline_details").data('dcsid', result.nextlevel
                        .disciplinaryActionCategorySettings_ID);
                    $("#offense_level").text(result.nextlevel.level);
                    $("#disciplinary_action").text(result.nextlevel.action
                        .toUpperCase());
                    if (result.nextlevel.periodMonth == 1) {
                        month_label = "month";
                    } else {
                        month_label = "months";
                    }

                    if (result.nextlevel.label == "t") {
                        $("#period_month").hide();
                        $("#pres_end").hide();
                        $("#period_label").hide();
                        $("#cureend_label").hide();
                        $("#pres_end").data('term', '1');
                    } else {
                        $("#period_month").show();
                        $("#pres_end").show();
                        $("#period_label").show();
                        $("#cureend_label").show();
                        $("#period_month").text(result.nextlevel.periodMonth + " " +
                            month_label);
                        $("#pres_end").data('term', '0');
                    }
                    $("#period_month").data('period', result.nextlevel
                    .periodMonth);
                    var cdate = moment(result.curedate, 'Y-MM-DD').format('MMM DD, Y')
                    $("#pres_end").text(cdate);
                    $("#pres_end").data('cend', result.curedate);

                    $("#natureoffense").data('ongoing', '1');
                    $("#suggested_dis_display").data('dacategoryid', result.nextlevel
                        .disciplinaryActionCategory_ID);
                    $("#suggested_dis_display").data('periodmonth', result.nextlevel
                        .periodMonth);
                }else{
                    $("#offense_level").text(result.firstlevel.level);
                    $("#disciplinary_action").text(disact_firstlevel);
                    $("#discipline_details").data('dcid', result.firstlevel
                        .disciplinaryActionCategory_ID);
                    $("#discipline_details").data('dcsid', result.firstlevel
                        .disciplinaryActionCategorySettings_ID);
    
                    if (result.firstlevel.label == "t") {
                        // $("#period_month").text("No Period");
                        // $("#pres_end").text("No Cure Date");
                        $("#period_month").hide();
                        $("#pres_end").hide();
                        $("#period_label").hide();
                        $("#cureend_label").hide();
                        $("#pres_end").data('term', '1');
                    } else {
                        $("#period_month").show();
                        $("#pres_end").show();
                        $("#period_label").show();
                        $("#cureend_label").show();
                        $("#period_month").text(result.firstlevel.periodMonth + " " + month_label);
                        $("#period_month").data("period", result.firstlevel.periodMonth);
                        $("#pres_end").data('term', '0');
                        $("#pres_end").text(cdate);
                        $("#pres_end").data('cend', result.curedate);
                    }
                    $("#natureoffense").data('ongoing', '0');
                    $("#suggested_dis_display").data('dacategoryid', result.firstlevel
                        .disciplinaryActionCategory_ID);
                    $("#suggested_dis_display").data('periodmonth', result.firstlevel.periodMonth);
                    $("#suggested_dis_label").data('occurence', '1');
                }
            }
        });
    } else {
        var message = "Please select an employee as a subject or Person of Interest of the IR to be filed.";
        var message_type = "2";
        alert_notif_message(message, message_type);
    }
});
// Upload Evidence
$("#upload_evidence_files_fileir").on('change', function (e) {
    var file = $(this).val();
    file = (file.length > 0) ? file : "fakepath";
    var name = file.split('fakepath');
    $("#evidence_filename").html(name[1].slice(1));
    $("#sub_button").show();
});

$('#witnesses_select').on('change', function () {
    $('#specialIRForm').formValidation('disableSubmitButtons', false);
});

$('#dismissedEmpMonitoring_s, #violationTypeMonitoringDismissed_s').on('change', function (){
    var dateRange = $('#dismissedEmpMonitoringDateRange_s').val().split('-');
    initdismissed_ir(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
});

$('#dismissedDtrViolationsTab_supervision').on('click', function () {
    $('#qualifiedRecordTabView_dismissed').hide();
    $('#secureQualifiedRecord_dismissed').show();
    verifyRecords_dismissed();
});

function init_dismissedrec(){
    initDismissedDtrViolationDateRangePicker_s(function (dateRanges) {
        initdismissed_ir(dateRanges.start, dateRanges.end);
        fetch_selectforDismissedrecords();
    });
}

function initdismissed_ir(startDate, endDate) {
    $('#dismissedQualifiedDtrVioDatatable_supervision').mDatatable('destroy');
    dismissedDtrViolationDatatable_s.init(startDate, endDate);
}

function fetch_selectforDismissedrecords(){
    $.ajax({
        url: baseUrl + "/discipline/get_filtersfordismissed",
        type: "POST",
        cache: false,
        success: function (data) {
            var result = JSON.parse(data);
            var stringsus_subject = "";
            stringsus_subject += "<option disabled selected=''>Select Subject</option>";
            stringsus_subject += "<option value='0'>All</option>";

            $.each(result, function (key, data) {
            stringsus_subject += "<option value=" + data.emp_id + ">" + data.lname + ", " + data.fname + "</option>";
            });
            
            $("#dismissedEmpMonitoring_s").html(stringsus_subject);
        }
    });
}
function initDismissedDtrViolationDateRangePicker_s(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#dismissedEmpMonitoringDateRange_s .form-control').val(start + ' - ' + end);
        $('#dismissedEmpMonitoringDateRange_s').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#dismissedEmpMonitoringDateRange_s .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initdismissed_ir(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'));
        });
        var dateRanges = {
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        };
        callback(dateRanges);
    });
}

var dismissedDtrViolationDatatable_s = function () {
    var dismissedDtrViolation = function (start, end) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_dismissed_dtr_violation_datatable_supervision",
                        params: {
                            query: {
                                empId: $('#dismissedEmpMonitoring_s').val(),
                                violationType: $('#violationTypeMonitoringDismissed_s').val(),
                                dateStart: start,
                                dateEnd: end
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // search: {
            //     input: $('#reportSearch'),
            // },
            rows: {
                afterTemplate: function (row, data, index) { },
            },
            // columns definition
            columns: [{
                field: "qualifiedUserDtrViolation_ID",
                title: "Q-ID",
                width: 80,
                selector: false,
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + row.qualifiedUserDtrViolation_ID.padLeft(8) + "</span>";
                    return html;
                },
            },
            {
                field: "emp_id",
                title: "Employee",
                width: 160,
                selector: false,
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                    var pic = baseUrl + "/" + picName + "_thumbnail.jpg";

                    var elementId = "#irDismissed" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<div class="row"><span class="col-2"><img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '"></span><span class="' + textColor + ' col-10">' + toTitleCase(row.fname + ' ' + row.lname) + '</span></div>';
                    return html;
                },
            }, {
                field: "description",
                title: "Type",
                width: 60,
                selector: false,
                // sortable: 'asc',
                overflow: 'visible',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + toTitleCase(row.description) + "</span>";
                    return html;
                },
            }, {
                field: "sched_date",
                title: "Schedule",
                width: 90,
                selector: false,
                overflow: 'visible',
                // sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var textColor = "";
                    if (row.status_ID == 12) {
                        textColor = "text-danger";
                    }
                    var html = "<span class='" + textColor + "'>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            },
            {
                field: "dtrViolationType_ID",
                title: "Action",
                width: 60,
                selector: false,
                // sortable: 'asc',
                overflow: 'visible',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air dismissedQualifiedDtrRecord" title="View Details" data-qualifiedid="' + row.qualifiedUserDtrViolation_ID + '"><i class="fa fa-search"></i></a>';
                    return html;
                },
            }
            ],
        };
        var datatable;
        datatable = $('#dismissedQualifiedDtrVioDatatable_supervision').mDatatable(options);
    };
    return {
        init: function (start, end) {
            dismissedDtrViolation(start, end);
        }
    };
}();

function initDismissQualifiedDtrView_supervision(qualifiedIdVal) {
    $.when(fetchPostData({
        qualifiedViolationId: qualifiedIdVal
    }, '/discipline/dismissed_qualified_dtr_details_view')).then(function (qualified) {
        let qualifiedObj = $.parseJSON(qualified.trim());
        let incidentDetails = "";
        let excess = "";
        let expectedActionDetails = "";
        let allowableTable = "";
        let coveredTable = "";
        let missingLogsDetails = "";
        let missingLogs = "";
        let expectedAction = "";
        let narrativeDetails = ""
        let pronoun = "";
        let allowableTableBody = "";
        let coveredTableBody = "";
        let shiftTime = qualifiedObj.details.shift.split('-');
        let reasonVal = "";
        // console.log(qualifiedObj);
        // console.log(dtrViolationContent);
        $('#qualifiedDetailsModalMonitor_supervision').data('qualifiedid', qualifiedIdVal);
        $('#empPicApprovalMonitor').attr('src', baseUrl + "/" + qualifiedObj.details.emp_details.pic);
        $('#qualifiedViewEmployeeNameMonitor').text(toTitleCase(qualifiedObj.details.emp_details.fname + " " +qualifiedObj.details.emp_details.lname));
        $('#qualifiedViewEmployeeNameMonitor').data('empid', qualifiedObj.details.emp_details.emp_id);
        $('#qualifiedViewEmployeeJobMonitor').text(qualifiedObj.details.emp_details.positionDescription);
        $('#qualifiedViewPosEmpStatMonitor').text(" -" + qualifiedObj.details.emp_details.empStatus);
        $('#qualifiedViewPosEmpStatMonitor').data('posEmpStat', qualifiedObj.details.emp_details.empStatus);
        $('#qualifiedViewEmployeeJobMonitor').data('position', qualifiedObj.details.emp_details.positionDescription);
        $('#qualifiedViewEmployeeAccountMonitor').text(toTitleCase(qualifiedObj.details.emp_details.accountDescription));
        $('#qualifiedViewEmployeeAccountMonitor').data('account', toTitleCase(qualifiedObj.details.emp_details.accountDescription));
        // Date and Incidents
        // $('#qualifiedDateIncident').text(moment(qualifiedObj.details.sched_date, 'Y-MM-DD').format('MMM DD, Y'));
        // $('#qualifiedDateIncident').data('incidentDate', qualifiedObj.details.sched_date);
        $('#qualifiedViewIncidentTypeMonitor').text(toTitleCase(qualifiedObj.details.violation_type));
        // $('#qualifiedDetailsModal').modal('show');
        if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) { // OB
            $('#qualifiedViewDateIncidentMonitor').text(moment(qualifiedObj.details.dated_shift['shift_end'],
                'Y-MM-DD HH:mm').format('MMM DD, Y'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentDate', moment(qualifiedObj.details.dated_shift[
                'shift_end'], 'Y-MM-DD HH:mm').format('Y-MM-DD'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentTime', moment(qualifiedObj.details.dated_shift[
                'shift_end'], 'Y-MM-DD HH:mm').format('kk:mm'));
            expectedAction = "Should have not exceeded from the " + qualifiedObj.details.incident_details
                .total_allowable + " allowable break duration";
            narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details
                    .emp_details.lname) + " has consumed a total of " + qualifiedObj.details.incident_details
                .total_covered + " break duration, which is " + qualifiedObj.details.incident_details.excess +
                " more than " + pronoun + " allowable break of " + qualifiedObj.details.incident_details
                .total_allowable + ".";
            incidentDetails = '<div class="col-12">' +
                '<span class="font-weight-bold mr-2">Shift:</span>' +
                '<span class="">' + qualifiedObj.details.shift + '</span>' +
                '</div>' +
                '<div class="col-12 mt-2 font-weight-bold">Break Details:</div>';
            excess = '<div class="col-12 mb-2">' +
                '<span class="font-weight-bold mr-2">Over Break Duration:</span>' +
                '<span class="">' + qualifiedObj.details.incident_details.excess + '</span>' +
                '</div>';

            if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                $.each(qualifiedObj.details.incident_details.break, function (index, value) {
                    allowableTableBody += '<tr>' +
                        '<td class="text-center" style="font-weight:400">' + value.type + '</td>' +
                        '<td class="text-center">' + value.allowable + '</td>' +
                        '</tr>';
                    coveredTableBody += '<tr>' +
                        '<td class="text-center">' + moment(value.in, 'Y-MM-DD HH:mm:ss').format(
                            'MMM DD, Y HH:mm:ss A') + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.out,
                            'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A') + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                        '</tr>';
                })
                allowableTable = '<div class="col-12 col-md-4">' +
                    '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                    '<thead style="background: #555;color: white;">' +
                    '<tr>' +
                    '<th class="text-center">Type</th>' +
                    '<th class="text-center">Allowable</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    allowableTableBody +
                    '</tbody>' +
                    '</table>' +
                    '</div>';
                coveredTable = '<div class="col-12 col-md-8 pl-0">' +
                    '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                    '<thead style="background: #555;color: white;">' +
                    '<tr>' +
                    '<th class="text-center">Out for Break</th>' +
                    '<th class="text-center">Back from Break</th>' +
                    '<th class="text-center">Covered</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    coveredTableBody +
                    '</tbody>' +
                    '</table>' +
                    '</div>';
                incidentDetails += allowableTable + "" + coveredTable + "" + excess + "" +
                    expectedActionDetails;
            } else {
                // alert error
            }
        } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 5) { // ABSENT
            $('#qualifiedViewDateIncidentMonitor').text(moment(qualifiedObj.details.dated_shift['shift_start'],
                'Y-MM-DD HH:mm').format('MMM DD, Y'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentDate', moment(qualifiedObj.details.dated_shift[
                'shift_start'], 'Y-MM-DD HH:mm').format('Y-MM-DD'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentTime', moment(qualifiedObj.details.dated_shift[
                'shift_start'], 'Y-MM-DD HH:mm').format('kk:mm'));
            $('#withRule').hide();
            expectedAction =
                "Should have taken all necessary DTR transaction that proves you are present at work.";
            narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details
                    .emp_details.lname) + " was determinded to have no DTR Log transactions in " + pronoun +
                " " + qualifiedObj.details.shift + " shift last " + moment(qualifiedObj.details.sched_date,
                    'Y-MM-DD').format('MMM DD, Y') + ", which proves his absence on the said schedule date.";
            incidentDetails = '<div class="col-12 col-md-6">' +
                '<span class="font-weight-bold mr-2">Shift:</span>' +
                '<span class="">' + qualifiedObj.details.shift + '</span>' +
                '</div>' +
                '<div class="col-12 col-md-6 text-md-right mb-2">' +
                '<span class="font-weight-bold mr-2">Logs:</span>' +
                '<span class="">No Log Records Found</span>' +
                '</div>';
        } else if ((parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6) || (parseInt(
                qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7)) {
            $('#qualifiedViewDateIncidentMonitor').text(moment(qualifiedObj.details.dated_shift['shift_end'],
                'Y-MM-DD HH:mm').format('MMM DD, Y'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentDate', moment(qualifiedObj.details.dated_shift[
                'shift_end'], 'Y-MM-DD HH:mm').format('Y-MM-DD'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentTime', moment(qualifiedObj.details.dated_shift[
                'shift_end'], 'Y-MM-DD HH:mm').format('kk:mm'));

            expectedAction =
                "Should have taken all the necessary DTR Transactions to complete all the required DTR Logs.";
            narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details
                    .emp_details.lname) + " was determinded to have no logs of the following: " + qualifiedObj
                .details.incident_details.missing.join(", ");
            incidentDetails = '<div class="col-12 col-md-4">' +
                '<span class="font-weight-bold mr-2">Shift:</span>' +
                '<span class="">' + qualifiedObj.details.shift + '</span>' +
                '</div>' +
                '<div class="col-12 col-md-3 text-md-right">' +
                '<span class="font-weight-bold mr-2">Missing Logs:</span>' +
                '</div>';
            if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                $.each(qualifiedObj.details.incident_details.missing, function (index, value) {
                    missingLogs += '<li>' + value + '</li>';
                })
                missingLogsDetails = '<div class="col-12 col-md-5">' +
                    '<ol>' + missingLogs +
                    '</ol>' +
                    '</div>';
                incidentDetails += missingLogsDetails;
            } else {
                // alert error
            }
        } else {
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1) {
                expectedAction = "Should have clocked in at least 30 minutes before " + shiftTime[0];
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj
                        .details.emp_details.lname) + " clocked in by " + moment(qualifiedObj.details
                        .incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj
                        .details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') +
                    "), which is " + qualifiedObj.details.incident_details.duration + " later than " + pronoun +
                    " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_start'],
                        'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift[
                        'shift_start'], 'Y-MM-DD HH:mm').format('MMM DD, Y') + ") shift start.";
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                expectedAction = "Should have not clocked out before " + shiftTime[1] + " to avoid Undertime.";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj
                        .details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details
                        .incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj
                        .details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') +
                    "), which is " + qualifiedObj.details.incident_details.duration + " earlier before " +
                    pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'],
                        'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift[
                        'shift_end'], 'Y-MM-DD HH:mm').format('MMM DD, Y') + ") shift end.";
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 3) {
                expectedAction = "Should have not clocked out beyond " + qualifiedObj.details.incident_details
                    .allowable_logout_formatted + " clock out allowance after" + shiftTime[1];
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj
                        .details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details
                        .incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj
                        .details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') +
                    "), which is " + qualifiedObj.details.incident_details.duration + " past from " + pronoun +
                    " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'],
                        'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift[
                        'shift_end'], 'Y-MM-DD HH:mm').format('MMM DD, Y') +
                    ") shift end exceeding the allowable clock-out allowance of " + qualifiedObj.details
                    .incident_details.allowable_logout_formatted + ".";
                reasonVal = '<div class="col-12 pt-2">' +
                    '<div class="row">' +
                    '<div class="col-12 col-md-2 font-weight-bold">Reason:</div>' +
                    '<div class="col-12 col-md-10 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType"><i class="fa fa-quote-left mr-1" style="font-size: 0.9rem;"></i>' +
                    qualifiedObj.details.reason.explanation +
                    '<i class="fa fa-quote-right ml-1" style="font-size: 0.9rem;"></i></div>' +
                    '</div>' +
                    '</div>';
            }
            $('#qualifiedViewDateIncidentMonitor').text(moment(qualifiedObj.details.incident_details.log,
                'Y-MM-DD HH:mm:ss').format('MMM DD, Y'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentDate', moment(qualifiedObj.details.incident_details
                .log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD'));
            $('#qualifiedViewDateIncidentMonitor').data('incidentTime', moment(qualifiedObj.details.incident_details
                .log, 'Y-MM-DD HH:mm:ss').format('kk:mm'));
            var durationVal = parseFloat(qualifiedObj.details.incident_details.duration) / 60;
            incidentDetails = '<div class="col-12 col-md-5">' +
                '<p class = "font-weight-bold mb-0" > Shift: </p> ' +
                '<p class = "" id="qualifiedShift" >' + qualifiedObj.details.shift + '</p>' +
                '</div> ' +
                '<div class = "col-12 col-md-4" >' +
                '<p class = "font-weight-bold mb-0">' + qualifiedObj.details.incident_details.log_label +
                ':</p>' +
                '<p class = "" >' + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss')
                .format('MMM DD, Y h:mm A') + '</p>' +
                '</div>' +
                '<div class = "col-12 col-md-3" >' +
                '<p class = "font-weight-bold mb-0"> Duration: </p> ' +
                '<p class = "">' + qualifiedObj.details.incident_details.duration + '</p> ' +
                '</div>';
        }

        incidentDetails += '<div class="col-12 pl-3 pr-3">' +
            '<div class="col-md-12 ahr_quote pt-3 pb-3 text-center">' +
            '<blockquote style="margin: 0 0 0rem;">' +
            '<i class="fa fa-quote-left"></i>' +
            '<span class="mb-0 mr-1 ml-1" id="narrativeIncidentDetails">' + narrativeDetails + '</span>' +
            '<i class="fa fa-quote-right"></i>' +
            '</blockquote>' +
            '</div>' +
            '</div>';
        $('#incidentDetailsViewMonitor').html(incidentDetails + reasonVal);
        // $('#incidentDetailsView').data('narrative', narrativeDetails);

        // Rule
        // Check if previous occurence exist
        if (parseInt(qualifiedObj.details.with_prev) == 1) {
            var withPreviousDetails = "";
            var withIrHistoryDetails = "";
            var previousDetailsHeader = "";
            var previousDetailsBody = "";
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) {
                previousDetailsHeader = '<th class="text-center">Date</th>' +
                    '<th class="text-center">Shift</th>' +
                    '<th class="text-center">Allowable</th>' +
                    '<th class="text-center">Covered</th>' +
                    '<th class="text-center">Over Break</th>';
                $.each(qualifiedObj.details.previous_details, function (index, value) {
                    previousDetailsBody += '<tr>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.date,
                            'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                        '<td class="text-center">' + value.shift + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.allowable + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.ob + '</td>' +
                        '</tr>';
                });
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1 || parseInt(
                    qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                var durationLabel = "Late";
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                    durationLabel = "Undertime"
                }
                previousDetailsHeader = '<th class="text-center">Date</th>' +
                    '<th class="text-center">Shift</th>' +
                    '<th class="text-center">Clock In</th>' +
                    '<th class="text-center">' + durationLabel + '</th>';
                $.each(qualifiedObj.details.previous_details, function (index, value) {
                    previousDetailsBody += '<tr>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.date,
                            'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                        '<td class="text-center">' + value.shift + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.log,
                            'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A') + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.formatted_duration +
                        '</td>' +
                        '</tr>';
                });
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6 || parseInt(
                    qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7) {
                previousDetailsHeader = '<th class="text-center">Date</th>' +
                    '<th class="text-center">Shift</th>' +
                    '<th class="text-center"> Note</th>';
                $.each(qualifiedObj.details.previous_details, function (index, value) {
                    previousDetailsBody += '<tr>' +
                        '<td class="text-center" style="font-weight:400">' + moment(value.date,
                            'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                        '<td class="text-center">' + value.shift + '</td>' +
                        '<td class="text-center" style="font-weight:400">' + value.note + '</td>' +
                        '</tr>';
                });
            }
            withPreviousDetails =
                '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">' +
                '<thead style="background: #555;color: white;">' +
                '<tr>' + previousDetailsHeader +
                '</tr>' +
                '</thead>' +
                '<tbody>' + previousDetailsBody +
                '</tbody>' +
                '</table>';
            $('#previousDetailsViewMonitor').html(withPreviousDetails);
            $('#withPreviousUserDtrViolationViewMonitor').show();
        } else {
            $('#withPreviousUserDtrViolationViewMonitor').hide();
        }

        $('#qualifiedExpectedActionViewMonitor').text(expectedAction);
        $('#qualifiedExpectedActionViewMonitor').data('expectedAction', expectedAction);
        $('#directSupPicMonitor').attr('src', baseUrl + "/" + qualifiedObj.details.sup_details.pic)
        $('#directSupNameMonitor').text(toTitleCase(qualifiedObj.details.sup_details.fname + " " + qualifiedObj.details
            .sup_details.lname));
        $('#directSupJobMonitor').text(qualifiedObj.details.sup_details.positionDescription);
        $('#reasonMonitor').text(qualifiedObj.details.dismiss_reason.reason);
        $('#dismissDate').text("Dismissed last "+moment(qualifiedObj.details.dismiss_reason.dateTime, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y hh:mm:ss A'));
        // // Check if IR history exist
        if (parseInt(qualifiedObj.details.with_rule)) {
            $('#qualifiedRuleViewMonitor').html(qualifiedObj.details.ir_rule.rule);
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) != 5) {
                $('#withRuleViewMonitor').show();
            } else {
                $('#withRuleViewMonitor').hide();
            }
        }
        $('#qualifiedDetailsModalMonitor_supervision').modal('show');
    })
}

$('#dismissed_ir').on('click', '.dismissedQualifiedDtrRecord', function () {
    initDismissQualifiedDtrView_supervision($(this).data('qualifiedid'));
})

// $('#irUL').on('click', '.yellowirbutton', function() {
//     console.log("tytyty");
// })

// charise end code--------------------------------------------------------------------