var base_url = $("#hidden_base_url").val();
function datatable_dtrviolations_init(url, paramsquery) {
    console.log(base_url);
    console.log(url);
    var datatable = $("#datatable_dtrviolations").mDatatable({
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    // sample GET method
                    method: 'POST',
                    url: base_url+'/Discipline/'+url,
                    map: function (raw) {
                        // sample data mapping
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    },
                    params: paramsquery
                }
            },
            saveState: {
                cookie: false,
                webstorage: false
            },
            pageSize: 5,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        // layout definition
        layout: {
            theme: 'default', // datatable theme
            class: '', // custom wrapper class
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            //                    height: 550, // datatable's body's fixed height
            footer: false // display/hide footer
        },
        // column sorting
        sortable: true,
        pagination: true,
        toolbar: {
            // toolbar placement can be at top or bottom or both top and bottom repeated
            placement: ['bottom'],
            // toolbar items
            items: {
                // pagination
                pagination: {
                    // page size select
                    pageSizeSelect: [5, 10, 20, 30, 50, 100],
                }
            }
        },
        // columns definition
        columns: [ {
            field: 'userDtrViolation_ID',
            title: 'Violation ID',
            sortable: true,
            template: function (row, index, datatable) {
                return "<span style='font-size:13px;'>" + (row.userDtrViolation_ID).padLeft(8) + "</span>";
            }
        },{
            field: 'description',
            title: 'Violation Type',
            sortable: true,
            template: function (row, index, datatable) {
                return "<span style='font-size:13px;text-transform:capitalize'>" + row.description + "</span>";
            }
        }, {
            field: 'sched_date',
            title: 'Schedule Date',
            sortable: true,
            template: function (row, index, datatable) {
                if (row.sched_date === null) {
                    var str = "<span style='text-italic;font-size:12px;color:#ffb822 !important' >Schedule Removed</span>";
                } else {
                    var str = "<span style='font-size:13px'>" + moment(row.sched_date).format('MMM DD, YYYY') + "</span>";
                }
                return str;
            }
        }, {
            field: 'lname',
            title: 'Name',
            sortable: true,
            template: function (row, index, datatable) {
                return "<span style='font-size:13px;text-transform:capitalize'>" + row.lname + ", " + row.fname + "</span>";
            }
        }, {
            field: 'dateRecord',
            title: 'Recorded On',
            sortable: true,
            template: function (row, index, datatable) {
                return "<span style='font-size:13px'>" + moment(row.dateRecord).format('MMM DD YYYY') + "<br/>" + moment(row.dateRecord).format(' hh:mm A') + "</span>";
            }
        }]
    });
    return datatable;
}

function get_unique_accounts_subordinates(callback) {
    $.ajax({
        type: "POST",
        url: base_url + "/Discipline/get_unique_accounts_subordinates",
        cache: false,
        success: function (result) {
            result = JSON.parse(result.trim());
            callback(result);
        }
    });
}
function get_ero_accounts(callback) {
    $.ajax({
        type: "POST",
        url: base_url + "/Discipline/get_ero_accounts",
        cache: false,
        success: function (result) {
            result = JSON.parse(result.trim());
            callback(result);
        }
    });
}

function get_all_accounts(callback) {
    $.ajax({
        type: "POST",
        url: base_url + "/Employee/get_all_accounts",
        cache: false,
        success: function (result) {
            result = JSON.parse(result.trim());
            callback(result);
        }
    });
}
function set_violation_types() {
    $.ajax({
        type: "POST",
        url: base_url + "/Discipline/get_violation_types",
        cache: false,
        success: function (result) {
            result = JSON.parse(result.trim());
            var stringTypeslist = '<option value="">All</option>';
            $.each(result.violationTypes, function (i, obj) {
                stringTypeslist += '<option value="' + obj.dtrViolationType_ID + '">' + obj.description + '</option>';
            });

            $("#violation-type").html(stringTypeslist);
        }
    });
}

function tz_date(thedate, format = null) {
    var tz = moment.tz(new Date(thedate), "Asia/Manila");
    return (format === null) ? moment(tz) : moment(tz).format(format);
}

var initialize_chart_dtrviolation = function (myoptions, accessType) {
    var vaccounts = $("#violation-accounts").val();
    var vtype = $("#violation-type").val();
    var vclass = $("#violation-class").val();
    var vsearch = $('#violation-search').val();
    var vdate_end = $("#violation-date").data('daterangepicker').endDate.format('YYYY-MM-DD');
    var vdate_start = $("#violation-date").data('daterangepicker').startDate.format('YYYY-MM-DD');
    var colors = [
        // "#64C1E8","#D85B63","#D680AD","#5C5C5C","#C0BA80","#FDC47D","#EA3B46"// nice
        // "#418171", "#AEADAB", "#F68951", "#E1B1A1", "#7E5D54", "#F69991", "#FDB954" //earthtones
        "#5867DD", "#34BFA3", "#36A3F7", "#FFB822", "#F4516C", "#9816F4", "#00C5DC"
    ];
    var colorcount = colors.length;
    var colorcnt = 0;
    $.ajax({
        type: "POST",
        url: base_url + "/Discipline/get_dashboard_dtr_system_violations",
        data: {
            violation_accounts: vaccounts,
            violation_type: vtype,
            violation_class: vclass,
            violation_search: vsearch,
            incidentDate_end: vdate_end,
            incidentDate_start: vdate_start,
            accessType: accessType
        },
        cache: false,
        success: function (res) {
            res = JSON.parse(res.trim());
            var violationTypes = res.violationsTypes;
            var dtrViolations = res.dtrViolations;
            var array_violationTypes = [];
            var stringlist = "";
            $.each(violationTypes, function (i, obj) {
                if (colorcnt === colorcount) {
                    colorcnt = 0;
                }
                var curcolor = colorcnt;
                array_violationTypes.push({
                    name: obj.description,
                    y: parseInt(obj.count),
                    color: colors[curcolor]
                });
                if (i % 2 === 0) {
                    stringlist += '<div class="row">';
                }
                var percentage = (parseInt(obj.count) / parseInt(res.total)) * 100;
                stringlist += '<div class="col-6">' +
                    '<div class="m-widget15__item">' +
                    '<span class="m-widget15__stats">' +
                    obj.count + '</span>' +
                    '<span class="m-widget15__text m--font-boldest text-capitalize" style="color:#000">' +
                    obj.description +
                    '</span>' +
                    // '<div class="m--space-10"></div>' +
                    '<div class="progress">' +
                    '<div class="progress-bar"  role="progressbar" style="width: ' + percentage.toFixed(1) + '%;background-color:' + colors[curcolor] + '" aria-valuenow="' + percentage.toFixed(1) + '" aria-valuemin="0" aria-valuemax="100"></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                 
                if (i % 2 !== 0) {
                    stringlist += '</div>';
                }
                colorcnt++;
            });

            $("#violationList").html(stringlist);
            $("#violation-header").html("Total DTR System Violations: " + res.total);
            // Build the chart
            Highcharts.chart('container-chart-pie', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45
                    }
                },
                title: null,

                colors: colors,
                plotOptions: {
                    pie: myoptions
                },
                series: [{
                    name: 'DTR System Violations',
                    colorByPoint: true,
                    data: array_violationTypes
                }]
            });
        }

    });
};