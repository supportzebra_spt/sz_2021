var base_url = $("#hidden_base_url").val();
$(function () {
    var currentdate = tz_date(moment());
    var start = moment(currentdate).startOf('year').format('MM/DD/YYYY');
    var end = moment(currentdate).endOf('year').format('MM/DD/YYYY');
    $('#chart-dashboard-date').val(start + ' - ' + end);
    $('#chart-dashboard-date').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        opens: "right",
        ranges: {
            'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
            'Yesterday': [moment(currentdate, 'YYY-MM-DD').subtract(1, 'days'), moment(currentdate,
                'YYYY-MM-DD').subtract(1, 'days')],
            'Last 7 Days': [moment(currentdate, 'YYYY-MM-DD').subtract(6, 'days'), moment(
                currentdate, 'YYYY-MM-DD')],
            'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate,
                'YYYY-MM-DD').endOf('month')],
            'Last Month': [moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').startOf('month'),
            moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').endOf('month')
            ]
        }
    }, function (start, end, label) {
        $('#chart-dashboard-date .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format(
            'MM/DD/YYYY'));
        update_ir_chart_offensetype();
    });

    get_all_accounts(function (res) {
        $("#chart-dashboard-accounts").html("<option selected disabled hidden>Accounts</option><option value=''>All</option>");
        $.each(res.accounts, function (i, item) {
            $("#chart-dashboard-accounts").append("<option value='" + item.acc_id +
                "' data-class='" + item.acc_description + "'>" + item.acc_name +
                "</option>");
        });
    });

    get_all_emp_stat(function (res) {
        $("#chart-dashboard-empStat").html("<option selected disabled hidden>Employee Status</option><option value=''>All</option>");
        $.each(res.emp_stat, function (i, item) {
            // $("#chart-dashboard-empStat").append("<option value='" + item.empstat_id + "'>" + item.status + "</option>");
            $("#chart-dashboard-empStat").append("<option value='" + item.status + "'>" + item.status + "</option>");
        });
    });

    get_all_nature_of_offense(function (res) {
        $("#chart-dashboard-natureOffense").html("<option selected disabled hidden>Nature of Offense</option><option value=''>All</option>");
        $.each(res.natureOffense, function (i, item) {
            $("#chart-dashboard-natureOffense").append("<option value='" + item.offenseType_ID + "'>" + item.offenseType + "</option>");
        });
    });


    $("#chart-dashboard-class").change(function () {
        var theclass = $(this).val();
        $("#chart-dashboard-accounts").children('option').css('display', '');
        $("#chart-dashboard-accounts").val('').change();
        if (theclass !== '') {
            $("#chart-dashboard-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css('display', 'none');
        }
    });
    //initialization
    update_ir_chart_offensetype();
})
function get_all_emp_stat(callback) {
    $.ajax({
        type: "POST",
        url: base_url + "/Employee/get_all_emp_stat",
        cache: false,
        success: function (result) {
            result = JSON.parse(result.trim());
            callback(result);
        }
    });
}
function get_all_nature_of_offense(callback) {
    $.ajax({
        type: "POST",
        url: base_url + "/Discipline/get_all_nature_of_offense",
        cache: false,
        success: function (result) {
            result = JSON.parse(result.trim());
            callback(result);
        }
    });
}
$("#chart-dashboard-empStat,#chart-dashboard-natureOffense,#chart-dashboard-accounts,#chart-dashboard-class").change(function () {
    $(this).css('color', 'black');
    var href = $("#nav-tab-chart").find('a.active').attr('href');
    if (href === '#tab-chart-offensetype') {
        update_ir_chart_offensetype();
    } else if (href === '#tab-chart-offense') {
        update_ir_chart_offense();
    } else {
        update_ir_chart_account();
    }
});
$('#nav-tab-chart a').on('show.bs.tab', function (event) {
    var href = $(event.target).attr('href');  // previous tab (relatedTarget for previous)
    if (href === '#tab-chart-offensetype') {
        update_ir_chart_offensetype();
    } else if (href === '#tab-chart-offense') {
        update_ir_chart_offense();
    } else {
        // $("#chart-dashboard-class").val('Agent');
        // $("#chart-dashboard-class").css('color', 'black');
        update_ir_chart_account();
    }
});

var update_ir_chart_offensetype = function () {
    var emp_stat = $("#chart-dashboard-empStat").val();
    var emp_type = $("#chart-dashboard-class").val();
    var natureOffense = $("#chart-dashboard-natureOffense").val();
    var accounts = $("#chart-dashboard-accounts").val();
    var date_end = $("#chart-dashboard-date").data('daterangepicker').endDate.format('YYYY-MM-DD');
    var date_start = $("#chart-dashboard-date").data('daterangepicker').startDate.format('YYYY-MM-DD');
    $.ajax({
        type: "POST",
        url: base_url + "/Discipline/get_ir_monitoring_chart_offenseType",
        data: {
            emp_stat: emp_stat,
            emp_type: emp_type,
            natureOffense: natureOffense,
            accounts: accounts,
            date_end: date_end,
            date_start: date_start
        },
        cache: false,
        success: function (res) {
            res = JSON.parse(res.trim());

            Highcharts.chart('dashboard-chart-offensetype', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: '<b>Nature of Offense Breakdown</b>'
                },
                exporting: {
                    enabled: true
                },
                subtitle: {
                    text: '<span style="font-size:16px">Total Records: ' + res.total + ' </span>'
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Total Records'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color};">{point.name}</span>: total of <b>{point.y}</b><br/>'
                },

                series: [
                    {
                        name: "Nature of Offense",
                        colorByPoint: true,
                        data: res.series
                    }
                ],
                drilldown: {
                    series: res.drilldownSeries
                }
            });
        }
    });


}

var update_ir_chart_offense = function () {
    var emp_stat = $("#chart-dashboard-empStat").val();
    var emp_type = $("#chart-dashboard-class").val();
    var natureOffense = $("#chart-dashboard-natureOffense").val();
    var accounts = $("#chart-dashboard-accounts").val();
    var date_end = $("#chart-dashboard-date").data('daterangepicker').endDate.format('YYYY-MM-DD');
    var date_start = $("#chart-dashboard-date").data('daterangepicker').startDate.format('YYYY-MM-DD');
    $.ajax({
        type: "POST",
        url: base_url + "/Discipline/get_ir_monitoring_chart_offense",
        data: {
            emp_stat: emp_stat,
            emp_type: emp_type,
            natureOffense: natureOffense,
            accounts: accounts,
            date_end: date_end,
            date_start: date_start
        },
        cache: false,
        success: function (res) {
            res = JSON.parse(res.trim());

            Highcharts.chart('dashboard-chart-offense', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: '<b>Incident Breakdown</b>'
                },
                exporting: {
                    enabled: true
                },
                subtitle: {
                    text: '<span style="font-size:16px">Total Records: ' + res.total + ' </span>'
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Total Records'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color};">{point.name}</span>: total of <b>{point.y}</b><br/>'
                },
                series: [
                    {
                        name: "Incidents",
                        colorByPoint: true,
                        data: res.series
                    }
                ],
            });
        }
    });


}
var update_ir_chart_account = function () {
    var emp_stat = $("#chart-dashboard-empStat").val();
    var emp_type = $("#chart-dashboard-class").val();
    var natureOffense = $("#chart-dashboard-natureOffense").val();
    var accounts = $("#chart-dashboard-accounts").val();
    var date_end = $("#chart-dashboard-date").data('daterangepicker').endDate.format('YYYY-MM-DD');
    var date_start = $("#chart-dashboard-date").data('daterangepicker').startDate.format('YYYY-MM-DD');
    $.ajax({
        type: "POST",
        url: base_url + "/Discipline/get_ir_monitoring_chart_account",
        data: {
            emp_stat: emp_stat,
            emp_type: emp_type,
            natureOffense: natureOffense,
            accounts: accounts,
            date_end: date_end,
            date_start: date_start
        },
        cache: false,
        success: function (res) {
            res = JSON.parse(res.trim());

            Highcharts.chart('dashboard-chart-account', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: '<b>Incidents Per Account Breakdown</b>'
                },
                exporting: {
                    enabled: true
                },
                subtitle: {
                    text: '<span style="font-size:16px">Total Records: ' + res.total + ' </span>'
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Total Records'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color};">{point.name}</span>: total of <b>{point.y}</b><br/>'
                },
                series: [
                    {
                        name: "Accounts",
                        colorByPoint: true,
                        data: res.series
                    }
                ],
                drilldown: {
                    series: res.drilldownSeries
                }
            });
        }
    });


}