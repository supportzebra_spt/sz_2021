// PENDING DTR VIO COUNT
function pendingDtrVioCount(late, ob, ut, forgotToLogout, incBreak, incDtr, absent) {
    if (late > 0) {
        $('#pendingLateCountDiv').fadeIn();
        $('#pendingLateCount').text(late);
    } else {
        $('#pendingLateCountDiv').hide();
    }
    if (ob > 0) {
        $('#pendingObCountDiv').fadeIn();
        $('#pendingObCount').text(ob);
    } else {
        $('#pendingObCountDiv').hide();
    }
    if (ut > 0) {
        $('#pendingUtCountDiv').fadeIn();
        $('#pendingUtCount').text(ut);
    } else {
        $('#pendingUtCountDiv').hide();
    }
    if (forgotToLogout > 0) {
        $('#pendingForgotToLogoutCountDiv').fadeIn();
        $('#pendingForgotToLogoutCount').text(forgotToLogout);
    } else {
        $('#pendingForgotToLogoutCountDiv').hide();
    }
    if (incBreak > 0) {
        $('#pendingIncBreakCountDiv').fadeIn();
        $('#pendingIncBreakCount').text(incBreak);
    } else {
        $('#pendingIncBreakCountDiv').hide();
    }
    if (incDtr > 0) {
        $('#pendingIncDtrCountDiv').fadeIn();
        $('#pendingIncDtrCount').text(incDtr);
    } else {
        $('#pendingIncDtrCountDiv').hide();
    }
    if (absent > 0) {
        $('#pendingAbsentCountDiv').fadeIn();
        $('#pendingAbsentCount').text(absent);
    } else {
        $('#pendingAbsentCountDiv').hide();
    }
}

function recordDtrVioCount(late, ob, ut, forgotToLogout, incBreak, incDtr, absent) {
    if (late > 0) {
        $('#recordLateCountDiv').fadeIn();
        $('#recordLateCount').text(late);
    } else {
        $('#recordLateCountDiv').hide();
    }
    if (ob > 0) {
        $('#recordObCountDiv').fadeIn();
        $('#recordObCount').text(ob);
    } else {
        $('#recordObCountDiv').hide();
    }
    if (ut > 0) {
        $('#recordUtCountDiv').fadeIn();
        $('#recordUtCount').text(ut);
    } else {
        $('#recordUtCountDiv').hide();
    }
    if (forgotToLogout > 0) {
        $('#recordForgotToLogoutCountDiv').fadeIn();
        $('#recordForgotToLogoutCount').text(forgotToLogout);
    } else {
        $('#recordForgotToLogoutCountDiv').hide();
    }
    if (incBreak > 0) {
        $('#recordIncBreakCountDiv').fadeIn();
        $('#recordIncBreakCount').text(incBreak);
    } else {
        $('#recordIncBreakCountDiv').hide();
    }
    if (incDtr > 0) {
        $('#recordIncDtrCountDiv').fadeIn();
        $('#recordIncDtrCount').text(incDtr);
    } else {
        $('#recordIncDtrCountDiv').hide();
    }
    if (absent > 0) {
        $('#recordAbsentCountDiv').fadeIn();
        $('#recordAbsentCount').text(absent);
    } else {
        $('#recordAbsentCountDiv').hide();
    }
}

// DATATABLES
var irableDtrViolationDatatable = function () {
    var irableDtrViolation = function (empIdVal, violationTypeVal, schedVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_irable_dtr_violation_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                                violationType: violationTypeVal,
                                sched: schedVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            var late = 0;
                            var ob = 0;
                            var ut = 0;
                            var forgotToLogout = 0;
                            var incBreak = 0;
                            var incDtr = 0;
                            var absent = 0;
                            $.each(raw.data, function (index, value) {
                                if (parseInt(value.dtrViolationType_ID) == 1) {
                                    late++;
                                } else if (parseInt(value.dtrViolationType_ID) == 2) {
                                    ut++;
                                } else if (parseInt(value.dtrViolationType_ID) == 3) {
                                    forgotToLogout++;
                                } else if (parseInt(value.dtrViolationType_ID) == 4) {
                                    ob++;
                                } else if (parseInt(value.dtrViolationType_ID) == 5) {
                                    absent++;
                                } else if (parseInt(value.dtrViolationType_ID) == 6) {
                                    incBreak++;
                                } else if (parseInt(value.dtrViolationType_ID) == 7) {
                                    incDtr++;
                                }
                            })

                            pendingDtrVioCount(late, ob, ut, forgotToLogout, incBreak, incDtr, absent)
                            dataCount = dataSet.length;
                            console.log(dataSet);
                            console.log(dataCount);
                            // if (dataCount>0){
                            //     $('#noIrableDtrViolation').hide();
                            //     $('#irableDtrViolation').fadeIn();
                            // }else{
                            //     $('#noIrableDtrViolation').fadeIn();
                            //     $('#irableDtrViolation').hide();
                            // }
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: 'qualifiedUserDtrViolation_ID',
                width: 80,
                title: 'Q-ID',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + row.qualifiedUserDtrViolation_ID.padLeft(8) + "</span>";
                    return html;                   
                },
            },{
                field: '',
                width: 50,
                title: '',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null){
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#irFiling" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '">';
                    return html;
                },
            },{
                field: 'employee',
                width: 160,
                title: 'Employee',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    var html = '<div class="pl-0" style="font-size: 0.9rem;">' + row.fname + ' ' + row.lname + '</div>';
                    return html;
                },
            }, {
                field: 'description',
                width: 80,
                title: 'Violation',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + row.description.toUpperCase() + "</span>";
                    return html;
                },
            }, {
                field: 'sched_date',
                width: 100,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'deadline',
                width: 130,
                title: 'Deadline',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + moment(row.deadline, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + "</span>";
                    return html;
                },
            }, {
                field: 'action',
                width: 50,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<a class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" title="File IR" data-qualifiedid="' + row.qualifiedUserDtrViolation_ID + '" onclick="initQualifiedModal(this)" style="width: 2rem; height: 2rem;"><i class="la la-edit"></i></a>';
                        // '<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air ml-2" title="Dismiss IR" data-qualifiedid="' + row.qualifiedUserDtrViolation_ID + '" style="width: 2rem; height: 2rem;"><i class="fa fa-close"></i></a>';
                    return html;
                },
            }],
        };
        var datatable = $('#irAbleDtrViolationDatatable').mDatatable(options);
    };
    return {
        init: function (empIdVal, violationTypeVal, searchVal) {
            irableDtrViolation(empIdVal, violationTypeVal, searchVal);
        }
    };

}();

var irableDtrViolationRecordDatatable = function () {
    var irableDtrViolationRecord = function (empIdVal, violationTypeVal, statusVal, dateStartVal, dateEndVal) {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline/get_irable_dtr_violation_record_datatable",
                        params: {
                            query: {
                                empId: empIdVal,
                                violationType: violationTypeVal,
                                status: statusVal,
                                dateStart: dateStartVal,
                                dateEnd: dateEndVal
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            console.log(raw.data);
                            var late = 0;
                            var ob = 0;
                            var ut = 0;
                            var forgotToLogout = 0;
                            var incBreak = 0;
                            var incDtr = 0;
                            var absent = 0;
                            $.each(raw.data, function (index, value) {
                                if (parseInt(value.dtrViolationType_ID) == 1) {
                                    late++;
                                } else if (parseInt(value.dtrViolationType_ID) == 2) {
                                    ut++;
                                } else if (parseInt(value.dtrViolationType_ID) == 3) {
                                    forgotToLogout++;
                                } else if (parseInt(value.dtrViolationType_ID) == 4) {
                                    ob++;
                                } else if (parseInt(value.dtrViolationType_ID) == 5) {
                                    absent++;
                                } else if (parseInt(value.dtrViolationType_ID) == 6) {
                                    incBreak++;
                                } else if (parseInt(value.dtrViolationType_ID) == 7) {
                                    incDtr++;
                                }
                            })

                            recordDtrVioCount(late, ob, ut, forgotToLogout, incBreak, incDtr, absent)
                            dataCount = dataSet.length;

                            if (dataCount > 0) {
                                $('#noIrableDtrViolationRecord').hide();
                                $('#irableDtrViolationRecord').fadeIn();
                            } else {
                                $('#noIrableDtrViolationRecord').fadeIn();
                                $('#irableDtrViolationRecord').hide();
                            }
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            // columns definition
            columns: [{
                field: 'qualifiedUserDtrViolation_ID',
                width: 80,
                title: 'Q-ID',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + row.qualifiedUserDtrViolation_ID.padLeft(8) + "</span>";
                    return html;                   
                },
            }, {
                field: '',
                width: 50,
                title: '',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var pic = baseUrl + "/" + "logo" + "_thumbnail.jpg";
                    if (row.pic != null) {
                        var picName = row.pic.substr(0, row.pic.indexOf(".jpg"))
                        pic = baseUrl + "/" + picName + "_thumbnail.jpg";
                    }
                    var elementId = "#irFilingRecord" + row.qualifiedUserDtrViolation_ID + "";
                    var html = '<img src="' + pic + '" onerror="noImageFound(this)" class = "rounded-circle" alt="employee photo" width="30" id="' + elementId + '">';
                    return html;
                },
            }, {
                field: 'employee',
                width: 160,
                title: 'Employee',
                overflow: 'visible',
                sortable: false,
                textAlign: 'left',
                 template: function (row, index, datatable) {
                     var html = '<div  style="font-size: 0.9rem;">' + row.fname + ' ' + row.lname + '</div>';
                     return html;
                 },
            }, {
                field: 'description',
                width: 80,
                title: 'Violation',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + row.description.toUpperCase() + "</span>";
                    return html;
                },
            }, {
                field: 'sched_date',
                width: 80,
                title: 'Schedule',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = "<span style='font-size: 0.9rem;'>" + moment(row.sched_date, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                    return html;
                },
            }, {
                field: 'statDescription',
                width: 80,
                title: 'Status',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    if (parseInt(row.status_ID) == 12) {
                        var badgeColor = 'btn-warning';
                        var dotAlert = "<span class='m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink mr-1' style='min-height: 7px;min-width: 7px;'></span>";
                    } else if (parseInt(row.status_ID) == 3) {
                        var badgeColor = 'btn-success';
                        var dotAlert = "";
                    }
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + "' '>" + dotAlert + " " + row.statDescription + "</span>";
                    return html;
                }
            }, {
                field: 'action',
                width: 60,
                title: 'Action',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var html = '<button class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air viewQualifiedDetailsRecord" data-qualifiedid="' + row.qualifiedUserDtrViolation_ID +'"><i class="fa fa-search"></i></button>';
                    return html;
                },
            }],
        };
        var datatable = $('#irAbleDtrViolationRecordDatatable').mDatatable(options);
    };
    return {
        init: function (empIdVal, violationTypeVal, statusVal, dateStartVal, dateEndVal) {
            irableDtrViolationRecord(empIdVal, violationTypeVal, statusVal, dateStartVal, dateEndVal);
        }
    };
}();

// DATATABLE INIT
function initIrAbleDtrViolation() {
    var sched_date = $('#dtrViolationSched').val();
    if (sched_date != '') {
        sched_date = moment($('#dtrViolationSched').val(), 'MM/DD/YYYY').format('Y-MM-DD');
    }
    $('#irAbleDtrViolationDatatable').mDatatable('reload');
    irableDtrViolationDatatable.init($('#fileDtrViolationEmpSelect').val(), $('#violationType').val(), sched_date);
    initDtrViolationBadges()
}

function initIrAbleDtrViolationRecord(dateStart, dateEnd) {
    $('#irAbleDtrViolationRecordDatatable').mDatatable('destroy');
    irableDtrViolationRecordDatatable.init($('#fileDtrViolationEmpRecordSelect').val(), $('#violationTypeRecord').val(), $('#fileDtrViolationRecordStatus').val(), dateStart, dateEnd);
}

// Plugin Initialization
function init_select2(obj, elementId, callback) {
    var optionsAsString = "<option value='0'>ALL</option>";
    var notEmpty = 1;
    if (parseInt(obj.record) == 0) {
        notEmpty = 0;
    } else {
        $.each(obj.record, function (index, value) {
            console.log(value.emp_id);
            optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.lname + ", " + value.fname) + "</option>";
        });
        $(elementId).html(optionsAsString);
    }
    $(elementId).select2({
        placeholder: "Select an Employee",
        width: '100%'
    }).trigger('change');
    callback(notEmpty);
}

function initDatePicker(id, clearBtn, callback) {
    $(id).datepicker({
        todayBtn: "linked",
        useCurrent: false,
        todayHighlight: true,
        autoclose: true,
        clearBtn: clearBtn,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        },
        // startDate: moment(date, 'Y-MM-DD').subtract(parseInt(daysAvail.daysAvailable), "days").format('MM/DD/YYYY'),
        // endDate: moment(date, 'Y-MM-DD').format('MM/DD/YYYY')
    });
    callback(id);
}

function initIrAbleDtrViolationRecordDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#fileDtrViolationRecordDateRange .form-control').val(start + ' - ' + end);
        $('#fileDtrViolationRecordDateRange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#fileDtrViolationRecordDateRange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            // initIrAbleDtrViolationRecord()
            initIrAbleDtrVioRecord(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) { });
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);

    });
}

// fileDtrViolationIrBadge

// ORDINARY INIT FUNCTIONS
// BADGES
function initDtrViolationBadge() {
    var dtrViolationBadge = 0;
    dtrViolationBadge += parseInt($('#fileDtrViolationIrBadgeCount').attr('count'));
    if (dtrViolationBadge > 0) {
        $('#dtrViolationBadgeCount').text(dtrViolationBadge);
        $('#dtrViolationBadge').fadeIn();
    } else {
        $('#dtrViolationBadge').hide();
    }
}
function initfileDtrViolationIrBadge(callback) {
    $.when(fetchGetData('/discipline/get_file_dtr_violation_ir_count')).then(function (count) {
        var countObj = $.parseJSON(count.trim());
        if (parseInt(countObj.count) > 0) {
            $('#fileDtrViolationIrBadgeCount').text(countObj.count);
            $('#fileDtrViolationIrBadgeCount').attr('count', countObj.count);
            $('#fileDtrViolationIrBadge').fadeIn();
        } else {
            $('#fileDtrViolationIrBadge').hide();
        }
        callback(countObj);
    });
}

function initIrAbleDtrVioRecord(startDate, endDate, callback) {
    get_emp_dtr_vio_ir_record_select(startDate, endDate, function (notEmpty) {
        initIrAbleDtrViolationRecord(startDate, endDate)
        callback(notEmpty);
    })
}

function dismissQualifiedDtrViolation(reasonVal){
    $.when(fetchPostData({
        qualifiedViolationId: $('#qualifiedModal').data('qualifiedid'),
        reason: reasonVal,
    }, '/discipline/dismiss_qualified_dtr_violation')).then(function (dismissal) {
        var dismissObj = $.parseJSON(dismissal.trim());
        initIrAbleDtrViolation();
        if (parseInt(dismissObj.dismissable)){
            if (parseInt(dismissObj.dismiss_status) == 1) {
                swal("Violation Dismissed!", "The qualified DTR violation record was successfully dismissed", "success");
            }else{
                swal("Dismissal Error", "Something went wrong while dismissing the record. Please immediately inform your system adminstrator", "error");
            }
            $('#qualifiedModal').modal('hide');
        }else{
            swal("Not allowed to dismiss", "It seems that the dismiss privilege is disabled. Please Inform your system adminstrator regarding this error", "error");
        }
    });
}

function dismissReason(){
    swal({
        title: 'Reason for Dismissing',
        showCancelButton: true,
        input: 'textarea',
        allowEscapeKey: 'false',
        allowOutsideClick: 'false',
        confirmButtonText: 'Submit',
        // cancelButtonText: 'No, Don\'t Dismiss',
        reverseButtons: true,
        inputValidator: (value) => {
            return new Promise((resolve) => {
                if (value.trim() !== '') {
                    dismissQualifiedDtrViolation(value.trim());
                    resolve()
                } else {
                    resolve('Oops! Reason is Required')
                }
            })
        }
    })
}

function dismissIrFilingConfirmation(){
    swal({
        title: 'Are you sure you want to dismiss the "' + $('#qualifiedIncidentType').text() + '" violation of ' + $('#qualifiedEmployeeName').text() + '?',
        text: 'You will not be able to revert back the record after dissmissal.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Dismiss!',
        cancelButtonText: 'No, Don\'t Dismiss',
        reverseButtons: true,
        // width: '500px'
    }).then(function (result) {
        if (result.value) {
            dismissReason();
        } else if (result.dismiss === 'cancel') {

        }
    });
}

// init Modals
function initQualifiedModal(element) {
    $.when(fetchPostData({
        qualifiedViolationId: $(element).data('qualifiedid'),
    }, '/discipline/get_qualified_dtr_vio_details')).then(function (qualified) {
        let qualifiedObj = $.parseJSON(qualified.trim());
        let incidentDetails = "";
        let excess = "";
        let expectedActionDetails = "";
        let allowableTable = "";
        let coveredTable = "";
        let missingLogsDetails = "";
        let missingLogs = "";
        let expectedAction = "";
        let narrativeDetails = ""
        let pronoun = "";
        let shiftTime = qualifiedObj.details.shift.split('-');
        let reasonVal = "";
        let dismissBtn = "";
        $('.withRule').hide();
        $('#withIrHistory').hide();
        //  + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') + "), which is " + qualifiedObj.details.incident_details.duration + " later than " + pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD HH:mm').format('MMM DD, Y')
        if (qualifiedObj.details.emp_details.gender == 'Male'){
            pronoun = "his";
        } else {
            pronoun = "her";
        }
        if (parseInt(qualifiedObj.error) == 0) {
            var allowableTableBody = "";
            var coveredTableBody = "";
            // EMPLOYEE DETAILS
            $('#qualifiedModal').data('qualifiedid', $(element).data('qualifiedid'));
            console.log("mao ni ang pic" + baseUrl + "/" + qualifiedObj.details.emp_details.pic);
            $('#empPicApproval').attr('src', baseUrl + "/" + qualifiedObj.details.emp_details.pic);
            $('#qualifiedEmployeeName').text(toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname));
            $('#qualifiedEmployeeName').data('empid', qualifiedObj.details.emp_details.emp_id);
            $('#qualifiedEmployeeJob').text(qualifiedObj.details.emp_details.positionDescription);
            $('#qualifiedPosEmpStat').text(" -" + qualifiedObj.details.emp_details.empStatus);
            $('#qualifiedPosEmpStat').data('posempstat', qualifiedObj.details.emp_details.empStatus);
            $('#qualifiedEmployeeJob').data('position', qualifiedObj.details.emp_details.positionDescription);
            $('#qualifiedEmployeeAccount').text(toTitleCase(qualifiedObj.details.emp_details.accountDescription));
            $('#qualifiedEmployeeAccount').data('account', toTitleCase(qualifiedObj.details.emp_details.accountDescription));
            $('#qualifiedEmployeeAccount').data('accountid', toTitleCase(qualifiedObj.details.emp_details.acc_id));
            $('#qualifiedEmployeeAccount').data('emptype', toTitleCase(qualifiedObj.details.emp_details.empType));
            // Date and Incidents
            // $('#qualifiedDateIncident').text(moment(qualifiedObj.details.sched_date, 'Y-MM-DD').format('MMM DD, Y'));
            // $('#qualifiedDateIncident').data('incidentdate', qualifiedObj.details.sched_date);
            $('#qualifiedIncidentType').text(toTitleCase(qualifiedObj.details.violation_type));
            // Incident Details
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) { // OB
                $('#qualifiedDateIncident').text(moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('MMM DD, Y'));
                $('#qualifiedDateIncident').data('incidentdate', moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('Y-MM-DD'));
                $('#qualifiedDateIncident').data('incidenttime', moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('h:mm A'));
                expectedAction = "Should have not exceeded from the " + qualifiedObj.details.incident_details.total_allowable + " allowable break duration";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " has consumed a total of " + qualifiedObj.details.incident_details.total_covered + " break duration, which is " + qualifiedObj.details.incident_details.excess + " more than " + pronoun + " allowable break of "+qualifiedObj.details.incident_details.total_allowable+".";
                incidentDetails = '<div class="col-12">' +
                    '<span class="font-weight-bold mr-2">Shift:</span>' +
                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                    '</div>' +
                    '<div class="col-12 mt-2 font-weight-bold">Break Details:</div>';
                excess = '<div class="col-12 mb-2">' +
                    '<span class="font-weight-bold mr-2">Over Break Duration:</span>' +
                    '<span class="">' + qualifiedObj.details.incident_details.excess + '</span>' +
                    '</div>';

                if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                    $.each(qualifiedObj.details.incident_details.break, function (index, value) {
                        let break_in = 'N/A';
                        let break_out = 'N/A';
                        if(value.out !== undefined && value.out !== null){
                            break_out = moment(value.out, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A');
                        }
                        if(value.in !== undefined && value.in !== null){
                            break_in = moment(value.in, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A');
                        }
                        allowableTableBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + value.type + '</td>' +
                            '<td class="text-center">' + value.allowable + '</td>' +
                            '</tr>';
                        coveredTableBody += '<tr>' +
                            '<td class="text-center">' + break_out + '</td>' +
                            '<td class="text-center" style="font-weight:400">' +  break_in + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                            '</tr>';
                    })
                    allowableTable = '<div class="col-12 col-md-4">' +
                        '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                        '<thead style="background: #555;color: white;">' +
                        '<tr>' +
                        '<th class="text-center">Type</th>' +
                        '<th class="text-center">Allowable</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        allowableTableBody +
                        '</tbody>' +
                        '</table>' +
                        '</div>';
                    coveredTable = '<div class="col-12 col-md-8 pl-0">' +
                        '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                        '<thead style="background: #555;color: white;">' +
                        '<tr>' +
                        '<th class="text-center">Out for Break</th>' +
                        '<th class="text-center">Back from Break</th>' +
                        '<th class="text-center">Covered</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        coveredTableBody +
                        '</tbody>' +
                        '</table>' +
                        '</div>';
                    incidentDetails += allowableTable + "" + coveredTable + "" + excess + "" + expectedActionDetails;
                } else {
                    // alert error
                }
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 5) { // ABSENT
                $('#qualifiedDateIncident').text(moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('MMM DD, Y'));
                $('#qualifiedDateIncident').data('incidentdate', moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('Y-MM-DD'));
                $('#qualifiedDateIncident').data('incidenttime', moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('h:mm A'));
                expectedAction = "Should have taken all necessary DTR transaction that proves you are present at work.";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " was determined to have no DTR Log transactions in " + pronoun + " " + qualifiedObj.details.shift + " shift last "+moment(qualifiedObj.details.sched_date, 'Y-MM-DD').format('MMM DD, Y')+", which proves "+pronoun+" absence on the said schedule date.";
                incidentDetails = '<div class="col-12 col-md-6">' +
                    '<span class="font-weight-bold mr-2">Shift:</span>' +
                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                    '</div>' +
                    '<div class="col-12 col-md-6 text-md-right mb-2">' +
                    '<span class="font-weight-bold mr-2">Logs:</span>' +
                    '<span class="">No Log Records Found</span>' +
                    '</div>';
            } else if ((parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6) || (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7)) {
                $('#qualifiedDateIncident').text(moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('MMM DD, Y'));
                $('#qualifiedDateIncident').data('incidentdate', moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('Y-MM-DD'));
                $('#qualifiedDateIncident').data('incidenttime', moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('h:mm A'));

                expectedAction = "Should have taken all the necessary DTR Transactions to complete all the required DTR Logs.";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " was determined to have no logs of the following: " + qualifiedObj.details.incident_details.missing.join(", ");
                incidentDetails = '<div class="col-12 col-md-4">' +
                    '<span class="font-weight-bold mr-2">Shift:</span>' +
                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                    '</div>' +
                    '<div class="col-12 col-md-3 text-md-right">' +
                    '<span class="font-weight-bold mr-2">Missing Logs:</span>' +
                    '</div>';
                if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                    $.each(qualifiedObj.details.incident_details.missing, function (index, value) {
                        missingLogs += '<li>' + value + '</li>';
                    })
                    missingLogsDetails = '<div class="col-12 col-md-5">' +
                        '<ol>' + missingLogs +
                        '</ol>' +
                        '</div>';
                    incidentDetails += missingLogsDetails;
                } else {
                    // alert error
                }
            } else {
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1){
                    expectedAction = "Should have clocked in at least 30 minutes before " + shiftTime[0];
                    narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " clocked in by " + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') + "), which is " + qualifiedObj.details.incident_details.duration + " later than " + pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift['shift_start'], 'Y-MM-DD kk:mm').format('MMM DD, Y') + ") shift start.";
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2){
                    expectedAction = "Should have not clocked out before " + shiftTime[1] + " to avoid Undertime.";
                    narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') + "), which is " + qualifiedObj.details.incident_details.duration + " earlier before " + pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('MMM DD, Y') + ") shift end.";
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 3){
                    expectedAction = "Should have not clocked out beyond " + qualifiedObj.details.incident_details.allowable_logout_formatted + " clock out allowance after" + shiftTime[1];
                    narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') + "), which is " + qualifiedObj.details.incident_details.duration + " past from " + pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift['shift_end'], 'Y-MM-DD kk:mm').format('MMM DD, Y') + ") shift end exceeding the allowable clock-out allowance of " + qualifiedObj.details.incident_details.allowable_logout_formatted+".";
                    reasonVal = '<div class="col-12 pt-2">'+
                        '<div class="row">'+
                            '<div class="col-12 col-md-2 font-weight-bold">Reason:</div>'+
                            '<div class="col-12 col-md-10 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType"><i class="fa fa-quote-left mr-1" style="font-size: 0.9rem;"></i>'+qualifiedObj.details.reason.explanation+'<i class="fa fa-quote-right ml-1" style="font-size: 0.9rem;"></i></div>'+
                        '</div>'+
                    '</div>';
                }
                $('#qualifiedDateIncident').text(moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y'));
                $('#qualifiedDateIncident').data('incidentdate', moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD'));
                $('#qualifiedDateIncident').data('incidenttime', moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('kk:mm'));
                var durationVal = parseFloat(qualifiedObj.details.incident_details.duration) / 60;
                incidentDetails = '<div class="col-12 col-md-5">' +
                    '<p class = "font-weight-bold mb-0" > Shift: </p> ' +
                    '<p class = "" id="qualifiedShift" >' + qualifiedObj.details.shift + '</p>' +
                    '</div> ' +
                    '<div class = "col-12 col-md-4" >' +
                    '<p class = "font-weight-bold mb-0">' + qualifiedObj.details.incident_details.log_label + ':</p>' +
                    '<p class = "" >' + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm A') + '</p>' +
                    '</div>' +
                    '<div class = "col-12 col-md-3" >' +
                    '<p class = "font-weight-bold mb-0"> Duration: </p> ' +
                    '<p class = "">' + qualifiedObj.details.incident_details.duration + '</p> ' +
                    '</div>';
            }

            incidentDetails += '<div class="col-12 pl-3 pr-3">' +
                                '<div class="col-md-12 ahr_quote pt-3 pb-3 text-center">' +
                                    '<blockquote style="margin: 0 0 0rem;">'+
                                        '<i class="fa fa-quote-left"></i>'+
                                        '<span class="mb-0 mr-1 ml-1" id="narrativeIncidentDetails">' + narrativeDetails + '</span>' +
                                        '<i class="fa fa-quote-right"></i>'+
                                    '</blockquote>'+
                                '</div>'+
                            '</div>';

            $('#incidentDetails').html(incidentDetails + reasonVal);
            $('#incidentDetails').data('narrative', narrativeDetails);

            // Rule
            // Check if previous occurence exist
            if (parseInt(qualifiedObj.details.with_prev) == 1) {
                var withPreviousDetails = "";
                var withIrHistoryDetails = "";
                var previousDetailsHeader = "";
                var previousDetailsBody = "";
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) {
                    previousDetailsHeader = '<th class="text-center">Date</th>' +
                        '<th class="text-center">Shift</th>' +
                        '<th class="text-center">Allowable</th>' +
                        '<th class="text-center">Covered</th>' +
                        '<th class="text-center">Over Break</th>';
                    $.each(qualifiedObj.details.previous_details, function (index, value) {
                        previousDetailsBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.date, 'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                            '<td class="text-center">' + value.shift + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.allowable + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.ob + '</td>' +
                            '</tr>';
                    });
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1 || parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                    var durationLabel = "Late";
                    if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                        durationLabel = "Undertime"
                    }
                    previousDetailsHeader = '<th class="text-center">Date</th>' +
                        '<th class="text-center">Shift</th>' +
                        '<th class="text-center">Clock In</th>' +
                        '<th class="text-center">' + durationLabel + '</th>';
                    $.each(qualifiedObj.details.previous_details, function (index, value) {
                        previousDetailsBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.date, 'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                            '<td class="text-center">' + value.shift + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A') + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.formatted_duration + '</td>' +
                            '</tr>';
                    });
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6 || parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7) {
                    previousDetailsHeader = '<th class="text-center">Date</th>' +
                        '<th class="text-center">Shift</th>' +
                        '<th class="text-center"> Note</th>';
                    $.each(qualifiedObj.details.previous_details, function (index, value) {
                        previousDetailsBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.date, 'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                            '<td class="text-center">' + value.shift + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.note + '</td>' +
                            '</tr>';
                    });
                }
                withPreviousDetails = '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">' +
                    '<thead style="background: #555;color: white;">' +
                    '<tr>' + previousDetailsHeader +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' + previousDetailsBody +
                    '</tbody>' +
                    '</table>';
                $('#previousDetails').html(withPreviousDetails);
                $('#withPreviousUserDtrViolation').show();
            } else {
                $('#withPreviousUserDtrViolation').hide();
            }
            // Check if IR history exist
            if (parseInt(qualifiedObj.details.with_history)) {
                var withIrHistoryBody = "";
                $.each(qualifiedObj.details.ir_history, function (index, value) {
                    var cureDateVal = "N/A";
                    if(value.label !== 't'){
                        cureDateVal = moment(value.prescriptionEnd, 'Y-MM-DD').format('MMMM DD, Y');
                    }
                    withIrHistoryBody += '<tr>' +
                        '<td class="text-center">' + value.incidentReport_ID.padLeft(8) + '</td>' +
                        '<td class="text-center">' + moment(value.incidentDate, 'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                        '<td class="text-center">' + value.sugAbbreviation.toUpperCase() + '</td>' +
                        '<td class="text-center">' + value.abbreviation.toUpperCase() + '</td>' +
                        '<td class="text-center">' + cureDateVal + '</td>' +
                        '</tr>';
                });
                withIrHistoryDetails = '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">' +
                    '<thead style="background: #555;color: white;">' +
                    '<tr>' +
                    '<th class="text-center">IR-ID</th>' +
                    '<th class="text-center">Incident Date</th>' +
                    '<th class="text-center">Suggested</th>' +
                    '<th class="text-center">Final</th>' +
                    '<th class="text-center">Cure Date</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' + withIrHistoryBody +
                    '</tbody>' +
                    '</table>'
                $('#irHistoryDetails').html(withIrHistoryDetails);
                // $('.withRule').hide();
                $('#withIrHistory').show();
            }
            if (parseInt(qualifiedObj.details.with_rule)) {
                $('#qualifiedRule').html(qualifiedObj.details.ir_rule.rule);
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) != 5){
                    $('.withRule').show();
                }else{
                    $('.withRule').hide();
                }
                // $('#withIrHistory').hide();
            }
            $('#qualifiedExpectedAction').text(expectedAction);
            $('#qualifiedExpectedAction').data('expectedaction', expectedAction);
            // Offense Details
            $('#qualifiedOffenseType').text(qualifiedObj.details.offense_details.offense_type);
            $('#qualifiedOffense').text(qualifiedObj.details.offense_details.offense_num + '. ' + qualifiedObj.details.offense_details.offense);
            $('#qualifiedOffense').data('offense', qualifiedObj.details.offense_details.offense);
            $('#qualifiedOffense').data('offenseid', qualifiedObj.details.offense_details.offense_id);
            $('#qualifiedOffense').data('discactioncategid', qualifiedObj.details.action_details.discActionCateg_ID);
            $('#qualifiedOffense').data('finaldiscactioncategid', qualifiedObj.details.action_details.discActionCateg_ID);
            $('#qualifiedOffense').data('discactioncategsettingsid', qualifiedObj.details.action_details.discActionCategSettings_ID);
            $('#qualifiedOffense').data('prescriptiveid', qualifiedObj.details.prescriptive_id);
            $('#qualifiedOffense').data('occurence', qualifiedObj.details.occurence);
            $('#qualifiedCategory').text(qualifiedObj.details.action_details.category);
            $('#qualifiedLevel').text(qualifiedObj.details.action_details.level + " Level");
            $('#qualifiedDisciplinaryAction').text(qualifiedObj.details.action_details.action.toUpperCase());
            var qualifiedPeriodUnit = qualifiedObj.details.action_details.unit_to_follow;
            if (parseInt(qualifiedObj.details.action_details.period_to_follow) > 1) {
                qualifiedPeriodUnit += 's';
            }
            $('#qualifiedPeriod').text(qualifiedObj.details.action_details.period_to_follow + " " + toTitleCase(qualifiedPeriodUnit));
            $('#qualifiedPeriod').data('periodnum', qualifiedObj.details.action_details.period_to_follow);
            $('#qualifiedPeriod').data('periodunit', qualifiedObj.details.action_details.unit_to_follow);
            $('#qualifiedCureDate').text(moment(qualifiedObj.details.action_details.cure_date, 'Y-MM-DD').format('MMMM DD, Y'));
            $('#qualifiedCureDate').data('curedate', qualifiedObj.details.action_details.cure_date);

            // check if dismissable
            // if (parseInt(qualifiedObj.details.dismissable)){
            //     dismissBtn = '<button type="button" id="dismissIrFiling" class="btn btn-danger m-btn m-btn--icon m-btn--wide"><span>Dismiss</span></button>';
            // }
            $('#dismissBtn').html(dismissBtn);
            getEmployeeMultExcept2(0, 0, 0, '#qualifiedWitnesses', 'Add Witnesses here', parseInt(qualifiedObj.details.emp_details.emp_id), parseInt(qualifiedObj.details.emp_details.Supervisor), function (empExistStat) {
                $('#qualifiedModal').modal('show');
            })
        } else {

        }
    });
}

function fileIrQualifiedDtrViolation(frm) {
    frm.append("qualifiedId", $('#qualifiedModal').data('qualifiedid'));
    frm.append("incidentDate", $('#qualifiedDateIncident').data('incidentdate'));
    frm.append("incidentTime", $('#qualifiedDateIncident').data('incidenttime'));
    frm.append("expectedAction",  $('#qualifiedExpectedAction').data('expectedaction'));
    frm.append("empId", $('#qualifiedEmployeeName').data('empid'));
    frm.append("position", $('#qualifiedEmployeeJob').data('position'));
    frm.append("empStat", $('#qualifiedPosEmpStat').data('posempstat'));
    frm.append("account", $('#qualifiedEmployeeAccount').data('account'));
    frm.append("accountId", $('#qualifiedEmployeeAccount').data('accountid'));
    frm.append("empType", $('#qualifiedEmployeeAccount').data('emptype'));
    frm.append("details", $('#incidentDetails').data('narrative'));
    frm.append("cureDate", $('#qualifiedCureDate').data('curedate'));
    frm.append("periodNum", $('#qualifiedPeriod').data('periodnum'));
    frm.append("periodunit", $('#qualifiedPeriod').data('periodunit'));
    frm.append("prescriptiveId", $('#qualifiedOffense').data('prescriptiveid'));
    frm.append("occurence", $('#qualifiedOffense').data('occurence'));
    frm.append("offenseId", $('#qualifiedOffense').data('offenseid'));
    frm.append("discActionCategId", $('#qualifiedOffense').data('discactioncategid'));
    frm.append("finalDiscActionCategId", $('#qualifiedOffense').data('finaldiscactioncategid'));
    frm.append("discActioncategSettingsId", $('#qualifiedOffense').data('discactioncategsettingsid'));
    frm.append("offense", $('#qualifiedOffense').data('offense'));
    frm.append("witnesses", $('#qualifiedWitnesses').val());
    frm.append("supervisorsNote", $('#qualifiedSupervisorsNote').val());
    $.ajax({
        type: "POST",
        url: baseUrl + "/discipline/file_ir_qualified_dtr_vio",
        data: frm,
        contentType: false,
        cache: false,
        processData: false,
        success: function (res) {
            var result = JSON.parse(res);
            if (result.with_witness == 1) {
                systemNotification(result.notify_wit.notif_id,'dms');
            }else{
                systemNotification(result.notify_sub.notifsubj_id,'dms');
            }
            initIrAbleDtrViolation();
            $('#qualifiedModal').modal('hide');
        }
    });
}

function confirmFileIrQualified(frm) {
    swal({
        title: 'Is this Final?',
        html: 'Kindly double check before you proceed. Record is not editable after submission',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'File IR',
        cancelButtonText: 'Back and Review',
        reverseButtons: true,
        // width: '500px'
    }).then(function (result) {
        if (result.value) {
            fileIrQualifiedDtrViolation(frm);
        } else if (result.dismiss === 'cancel') {
            $('#qualifiedDtrVioForm').formValidation('disableSubmitButtons', false);
        }
    });
}

function get_emp_dtr_vio_ir_select(callback) {
    var sched_date = $('#dtrViolationSched').val();
    if (sched_date != '') {
        sched_date = moment($('#dtrViolationSched').val(), 'MM/DD/YYYY').format('Y-MM-DD');
    }
    $.when(fetchPostData({
        violationType: $('#violationType').val(),
        sched: sched_date
    }, '/discipline/get_specific_qualified_emp_ir')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        $('#fileDtrViolationEmpSelect').empty().trigger('change');
        console.log(empObj.record);
        if (empObj.record != 0) {
            $('#noIrableDtrViolation').hide();
            $('#irableDtrViolation').fadeIn();
        } else {
            $('#noIrableDtrViolation').fadeIn();
            $('#irableDtrViolation').hide();
        }
        init_select2(empObj, "#fileDtrViolationEmpSelect", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

function get_emp_dtr_vio_ir_record_select(startDate, endDate, callback) {
    $.when(fetchPostData({
        status: $('#fileDtrViolationRecordStatus').val(),
        violationType: $('#violationTypeRecord').val(),
        dateStart: startDate,
        dateEnd: endDate
    }, '/discipline/get_specific_qualified_emp_ir_record')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        console.log(empObj);
        $('#fileDtrViolationEmpRecordSelect').empty().trigger('change');
        init_select2(empObj, "#fileDtrViolationEmpRecordSelect", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

function get_emp_dtr_vio_ir_monitoring_record_select(startDate, endDate, callback) {
    $.when(fetchPostData({
        status: $('#fileDtrViolationMonitoringRecordStatus').val(),
        violationType: $('#violationTypeMonitoringRecord').val(),
        dateStart: startDate,
        dateEnd: endDate
    }, '/discipline/get_specific_qualified_emp_ir_record')).then(function (emp) {
        var empObj = $.parseJSON(emp.trim());
        console.log(empObj);
        $('#fileDtrViolationEmpRecordSelect').empty().trigger('change');
        init_select2(empObj, "#fileDtrViolationEmpRecordSelect", function (notEmpty) {
            callback(notEmpty);
        });
    });
}

function makeFileListQualified() {
    var input = document.getElementById("upload_evidence_files_qualified");
    var ul = document.getElementById("fileListQualified");
    var maxsize = 5000000;
    var exceed = 0;
    $("#files_select_label_qualified").text("Files Selected");
    $("#display_div_file_qualified").show();
    while (ul.hasChildNodes()) {
        ul.removeChild(ul.firstChild);
    }
    for (var i = 0; i < input.files.length; i++) {
        var li = document.createElement("li");
        li.innerHTML = input.files[i].name;
        var file_size = input.files[i].size;
        var g = input.files[i].name;
        var file_ext = g.split('.').pop();


        if (file_ext == "doc" || file_ext == "docx" || file_ext == "pdf" || file_ext == "txt" || file_ext ==
            "xml" ||
            file_ext == "xlsx" || file_ext == "xlsm" || file_ext == "ppt" || file_ext == "pptx" || file_ext ==
            "jpeg" ||
            file_ext == "jpg" ||
            file_ext == "png" || file_ext == "mp4" || file_ext == "PNG" || file_ext == "JPEG" || file_ext == "JPG" || file_ext == "mov" || file_ext ==
            "3gp" || file_ext == "m4v" ||
            file_ext == "wmv" || file_ext == "avi" || file_ext == "mpeg" || file_ext == "mp3" || file_ext ==
            "wav" ||
            file_ext == "csv" || file_ext == "xlxx") {

            if (file_size <= maxsize) {
                ul.appendChild(li);
                $("#fileListQualified li").removeClass("included");
            } else {
                ul.appendChild(li);
                exceed = 1;
                $("#fileListQualified li").addClass("not_included");
                $("#files_select_label_qualified").text("File/s exceeded to 5mb. Please upload another.");
            }
        } else {
            ul.appendChild(li);
            exceed = 1;
            $("#fileListQualified li").addClass("not_included");
            $("#files_select_label_qualified").text("Invalid file type. Please reupload allowed file types.");
        }
    }
    if (!ul.hasChildNodes()) {
        var li = "";
        // li.innerHTML = 'No Files Selected';
        ul.html(li);
        $("#files_select_label_qualified").text("");
    }
    console.log('exceed: '+exceed);
    if (exceed) {
        console.log('disable button');
        $('#qualifiedDtrVioForm').formValidation('disableSubmitButtons', true);
    } else {
        $('#qualifiedDtrVioForm').formValidation('disableSubmitButtons', false);
    }
}


function resetQualifiedFormFields(){
    $("#qualifiedWitnesses").val([]).trigger("change");
    $("#upload_evidence_files_qualified").val("");
    $("#files_select_label_qualified").text("");
    $("#display_div_file_qualified").hide();
}

function initDtrViolationBadges(){
    initfileDtrViolationIrBadge(function (countObj) {
        initDtrViolationBadge();
    });
}

function initEmployeeIrProfile(pageNumVal,callback) {
    $.when(fetchPostData({
        pageNum: pageNumVal
    }, '/discipline_mic/get_employee_ir_profile_list')).then(function (emp) {
        let empObj = $.parseJSON(emp.trim());
        let optionsAsText = "";
        if (parseInt(empObj.exist) > 0){
            $.each(empObj.record, function(index, value){
                optionsAsText+="<option value='"+value.emp_id+"'>"+value.lname+" "+value.fname+"</option>";
            });
        }else{
            optionsAsText = "<option></option>";
        }
        $('#empListHistory').html(optionsAsText);
        $('#empListHistory').select2({
            placeholder: "Select an Employee",
            width: '100%'
        }).trigger('change');
        $('#empIrProfileSelect').show();
        callback(empObj.exist);
    });
}

function initSupEmpDetails(callback) {
    console.log('init emp deatils');
    $.when(fetchPostData({
        emp_id: $('#empListHistory').val()
    }, '/discipline_mic/access_employee_ir_profile')).then(function (emp_details) {
        let emp_details_obj = $.parseJSON(emp_details.trim());
        if (emp_details_obj != null) {
            console.log('dili siya null');
            $('#profilePic').attr('src', baseUrl +"/"+ emp_details_obj.pic);
            $('#profileName').text(toTitleCase(emp_details_obj.fname + " " + emp_details_obj.lname));
            $('#profileName').data('empId', emp_details_obj.emp_id);
            $('#profilePosition').text(emp_details_obj.positionDescription);
            $('#profileEmpStatus').text(emp_details_obj.empStatus);
            $('#profileAccount').text(emp_details_obj.accountDescription);
        }else{
            $('#irProfile').hide();
        }
        callback(emp_details_obj);
    });
}

function initEmployeeProfile(){
    $('#offenseHistory').hide();
    initSupEmpDetails(function(empExist){
        if (empExist != null) {
            initIrProfileDateRangePicker(function (dateRange) {
                var dateRange = $('#irProfileDateRange').val().split('-');
                initProfileNatureOfOffenses(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
            });
            $('#noIrPofileRecords').hide()
            $('#showIrPofileRecords').show();
        } else {
            $('#showIrPofileRecords').hide();
            $('#noIrPofileRecords').show()
        }
    });
}


function showMyIrView(ir_id_val) {
    $.when(fetchPostData({
        ir_id: ir_id_val
    }, '/discipline/get_ir_details')).then(function (res) {
        result = JSON.parse(res.trim());
        $("#layoutHistoryIrDetails").html(result.ir_details_layout);
        $("#irHistoryProfile").modal('show');
    });
}

function initQualifiedRecord(){
    initIrAbleDtrViolationRecordDateRangePicker(function (dateRanges) {
        var dateRange = $('#fileDtrViolationRecordDateRange').val().split('-');
        initIrAbleDtrVioRecord(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) { });
    });
}

// EVENTS 
$('.dmsSupervisionMainNav').on('click', function (event) {
   
    $(".m-nav--inline").find(".active").removeClass("active");
    $(this).parent().addClass("active");
    $('.m-portlet .active').find(".active").removeClass("active");
    $('.m-portlet').hide();
    $('#' + $(this).data('navcontent') + '').addClass("active");
    if ($(this).data('navcontent') !== 'irProfile') {
        $('#empIrProfileSelect').hide();
        $('#offenseHistory').hide();
        $('#showIrPofileRecords').hide();
        $('#AccessSupIrProfile').hide();
        $('#noAccessSupIrProfile').show();
    } 
    $('#' + $(this).data('navcontent') + '').fadeIn();
    $(this).addClass("active");
})

$('#fileDtrViolationIrTab').on('click', function () {
    initDatePicker('#dtrViolationSched', true, function () {
        get_emp_dtr_vio_ir_select(function (notEmpty) {
            // initIrAbleDtrViolation();
        });
    });
})

$('#fileDtrViolationIrRecordTab').on('click', function () {
    $('#qualifiedRecordTabView').hide();
    $('#secureQualifiedRecord').show();
    verifyQualifiedRecordAccess();
})

$('#violation-tabular-tab').on('click', function () {
    $('#datatable_dtrviolations').hide();
    $('#secureDtrViolationDatatable').show();
    verifyViolationTabularAccess();
})

$('#profileIrHistoryDatatable').on('click', '.profileIrView', function () {
    showMyIrView($(this).data('irid'));
})

$('#violationType, #dtrViolationSched').on('change', function () {
    get_emp_dtr_vio_ir_select(function (notEmpty) {
        // initIrAbleDtrViolation();
    })
});

// $('#fileDtrViolationEmpSelect').on('change', function () {
//     initIrAbleDtrViolation();
// });
$('#fileDtrViolationEmpSelect').on('change', function () {
    $('#qualifiedTabView').hide();
    $('#secureQualified').show();
    verifyQualifiedAccess();
});

$('#violationTypeRecord, #fileDtrViolationRecordStatus').on('change', function () {
    var dateRange = $('#fileDtrViolationRecordDateRange').val().split('-');
    initIrAbleDtrVioRecord(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) { });
});

$('#fileDtrViolationEmpRecordSelect').on('change', function () {
    var dateRange = $('#fileDtrViolationRecordDateRange').val().split('-');
    initIrAbleDtrViolationRecord(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
});

$('#irDetailTemplate').on('click', function () {
    $.when(fetchGetData('/discipline/access_ir_details')).then(function (ir_details) {
        var ir_template = $.parseJSON(ir_details.trim());
        $('#ir_details_template').html(ir_template.ir_details_layout);
        $('#irTemplate').modal('show');
    });
})

$('#qualifiedWitnesses').on('change', function(){
    $('#qualifiedDtrVioForm').formValidation('disableSubmitButtons', false);
})

$('#dismissBtn').on('click', '#dismissIrFiling', function () {
    dismissIrFilingConfirmation();
})

$('#dismissIrFiling').on('click', function () {
    dismissIrFilingConfirmation();
})

$('#qualifiedModal').on('hidden.bs.modal', function(){
    formReset('#qualifiedDtrVioForm');
    resetQualifiedFormFields();
})

$('#supervisionIrProfile').on('click', function(){
    verifyViewIrProfile();
})

$('#empListHistory').on('change', function(){
    initEmployeeProfile();
})

// $('#profileIrHistoryDatatable').on('click', '.profileIrView', function () {
//     console.log($(this).data('irid'));
//     // showMyIrView($(this).data('irid'));
// })
// $('#exportHistoryBtn').on('click', function () {
//     exportIrHistory();
// })
// FORM VALIDATION

$('#qualifiedDtrVioForm').formValidation({
    message: 'This value is not valid',
    excluded: ':disabled',
    //        live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        qualifiedSupervisorsNote: {
            validators: {
                notEmpty: {
                    message: 'Oops! Supervisor\'s Note is required'
                }
            }
        },
    }
}).on('success.form.fv', function (e, data) {

    // console.log($(e.target));
    e.preventDefault();
    $('#qualifiedDtrVioForm').formValidation('disableSubmitButtons', true);
    var frm = new FormData(this);
    // fileIrQualifiedDtrViolation(frm);
    confirmFileIrQualified(frm)
    
    // console.log(frm.values());
    // for (var value of frm.values()) {
    //     console.log(value);
    // }
    // console.log
    // if ($('#categoryForm').data('formtype') == "add") {
    //     addCategory();
    // } else {
    //     confirmUpdateDisciplinaryAction();
    // }
});


$(function () {
    var protocol = window.location.protocol;
    var pathName = window.location.pathname;
    var host = window.location.host;
    var baseUrl = '';
    var match = host.match(/\d{1,3}/g);
    var ipHost = (match != null ? match : 0);
    // console.log("protocol ->" + protocol);
    // console.log("pathName ->" + pathName);
    // console.log("host ->" + host);
    // console.log("match ->" + match);
    // console.log("ipHost ->" + ipHost);
    var pathArray = pathName.split('/');
    // console.log(pathArray);
    if (pathArray.length == 6) {
        console.log(pathArray[4]); //main tab
        console.log(pathArray[5]); //portlet tab
        var portletTab = '#' + pathArray[5];
        $('.dms').find('[data-navcontent="' + pathArray[4] + '"]').trigger('click');
        $(portletTab).trigger('click');
    }

    initDtrViolationBadges();
})

