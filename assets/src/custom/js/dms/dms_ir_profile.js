var personalIrHistoryDatatable = function () {
    var personalIrHistory = function (range_startVal, range_endVal, searchVal) {
        console.log(searchVal);
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + "/discipline_mic/offense_history_datatable",
                        params: {
                            query: {
                                offenseId: $('#specificOffenseHistory').val(),
                                liabilityStat: $('#irProfileLiabilityStat').val(),
                                range_start: range_startVal,
                                range_end: range_endVal,
                                searchProfileHistory: searchVal,
                                empId: $('#profileName').data('empId')
                            },
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            dataCount = dataSet.length;
                            return dataSet;
                        }
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#searchProfileHistory'),
            },
            // columns definition
            columns: [ {
                    field: 'incidentReport_ID',
                    width: 60,
                    title: 'IR-ID',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'left',
                    template: function (row, index, datatable) {
                        return "<span style='font-size:13px;'>" + (row.incidentReport_ID).padLeft(8) + "</span>";
                    }
                }, {
                    field: 'incidentDate',
                    width: 120,
                    title: 'Incident Date',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = "<span>" + moment(row.incidentDate, 'Y-MM-DD').format('MMM DD, Y') + "</span>";
                        return html;
                    },
                }, {
                    field: 'finalAction',
                    width: 180,
                    title: 'Disciplinary Action',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center'
                    // template: function (row, index, datatable) {
                    //     var html = row.offense_count;
                    //     return html;
                    // },
                }, {
                    field: 'prescriptionEnd',
                    width: 120,
                    title: 'Cure Date',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        let cureDate = 'N/A'; 
                        if (row.label !== 't'){
                            cureDate = moment(row.prescriptionEnd, 'Y-MM-DD').format('MMM DD, Y')
                        }
                        var html = "<span>" + cureDate + "</span>";
                        return html;
                    },
                }, {
                    field: 'action',
                    width: 50,
                    title: 'Action',
                    overflow: 'visible',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = '<a class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2 popoverTest profileIrView" data-irid="' + row.incidentReport_ID + '" style="width: 22px !important;height: 22px !important;">' +
                            '<i class="fa fa-search" style="font-size: 0.95rem !important;"></i>' +
                            '</a>' ;
                        return html;
                    },
                }
            ],
        };
        var datatable = $('#profileIrHistoryDatatable').mDatatable(options);
    };
    return {
        init: function (range_startVal, range_endVal, searchVal) {
            personalIrHistory(range_startVal, range_endVal, searchVal);
        }
    };
}();

function initProfileIrHistory() {
    $('#profileIrHistoryDatatable').mDatatable('destroy');
    var dateRange = $('#irProfileDateRange').val().split('-');
    personalIrHistoryDatatable.init(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), $('#searchProfileHistory').val());
}

function initEmpDetails() {
     $.when(fetchGetData('/discipline/access_personal_ir_profile')).then(function (emp_details) {
         var emp_details_obj = $.parseJSON(emp_details.trim());
         console.log(imagePath + emp_details_obj.pic);
         $('#profilePic').attr('src', baseUrl+'/'+ emp_details_obj.pic);
         $('#profileName').text(toTitleCase(emp_details_obj.fname + " " + emp_details_obj.lname));
         $('#profileName').data('empId', emp_details_obj.emp_id);
         $('#profilePosition').text(emp_details_obj.positionDescription);
         $('#profileEmpStatus').text(emp_details_obj.empStatus);
         $('#profileAccount').text(emp_details_obj.accountDescription);
     });
}

function initPersonalIrCounts(rangeStartVal, rangeEndVal, natureOffenseVal, callback) {
    $.when(fetchPostData({
        range_start: rangeStartVal,
        range_end: rangeEndVal,
        liabilityStat: $('#irProfileLiabilityStat').val(),
        natureOffense: natureOffenseVal,
        emp_id: $('#profileName').data('empId')
    }, '/discipline_mic/get_personal_profile_ir_counts')).then(function (irCounts) {
        let irCountsObj = $.parseJSON(irCounts.trim());
        let irCountList = "";
        let irTotalCount = 0;
        if (parseInt(irCountsObj.with_disc_action_count)){
            $.each(irCountsObj.record, function (index, value) {
                irTotalCount += parseInt(value.actionCount);
                irCountList += '<div class="m-widget1__item">'+
                    '<div class="row m-row--no-padding align-items-center">'+
                        '<div class="col">'+
                            '<h3 class="m-widget1__title" style="font-size: 1.2rem !important;">'+value.action+'</h3>'+
                        '</div>'+
                        '<div class="col-1 m--align-right">'+
                            '<span class="m-widget1__number m--font-brand" style="font-size: 1.2rem !important;">'+value.actionCount+'</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
            })
            $('#showMyIrStats').show();
            $('#notShowMyIrStats').hide();
        }else{
             irCountList += '<div class="m-widget1__item">'+
                '<div class="row m-row--no-padding align-items-center">'+
                    '<div class="col text-center">' +
                        '<h3 class="m-widget1__title" style="font-size: 1.2rem !important;">No Records To Show</h3>'+
                    '</div>'+
                '</div>'+
            '</div>';
            $('#showMyIrStats').hide();
            $('#notShowMyIrStats').show();
        }
        $('#discActionCount').html(irCountList);
        $('#irTotalCount').text(irTotalCount);
        callback(irCountsObj);
    });
}

// function initProfileChartData(rangeStartVal, rangeEndVal){
//     $.when(fetchPostData({
//         range_start: rangeStartVal,
//         range_end: rangeEndVal,
//         liabilityStat: $('#irProfileLiabilityStat').val()
//     }, '/discipline_mic/get_nature_offense_chart_data')).then(function (natureOffense) {

//     });
// }

function initProfileNatureOfOffenses(rangeStartVal, rangeEndVal, callback) {
    $.when(fetchPostData({
        range_start: rangeStartVal,
        range_end: rangeEndVal,
        liabilityStat: $('#irProfileLiabilityStat').val(),
        emp_id: $('#profileName').data('empId')
    }, '/discipline_mic/get_nature_offense_chart_data')).then(function (seriesData) {
        let seriesDataObj = $.parseJSON(seriesData.trim());
        console.log(seriesDataObj);
        initPersonalIrCounts(rangeStartVal, rangeEndVal, 0, function(){});
        natureOffenseChart(seriesDataObj.nature_offense_series, seriesDataObj.offense_series);
        callback(seriesDataObj);
    });
}

function initProfileOffenses(rangeStartVal, rangeEndVal, natureOffenseVal, callback) {
    $.when(fetchPostData({
        range_start: rangeStartVal,
        range_end: rangeEndVal,
        natureOffense: natureOffenseVal,
        liabilityStat: $('#irProfileLiabilityStat').val(),
        emp_id: $('#profileName').data('empId')
    }, '/discipline_mic/get_personal_offenses')).then(function (offense) {
        var offenseObj = $.parseJSON(offense.trim());
        var optionAsString = "";
        if (parseInt(offenseObj.with_offense)) {
            $.each(offenseObj.record, function (index, value) {
                optionAsString += '<option value="' + value.offenseId + '" >' + value.offense +'</option>';
            })
        }
        $('#specificOffenseHistory').html(optionAsString);
        $('#specificOffenseHistory').select2({
            placeholder: "Select an Offense",
            width: '100%'
        })
        callback(offenseObj);
    });
}

function initIrProfileDateRangePicker(callback) {
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#irProfileDateRange').val(start + ' - ' + end);
        $('#irProfileDateRange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#irProfileDateRange').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            // initIrAbleDtrViolationRecord()
            initProfileNatureOfOffenses(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
        });
        var dateRanges = [{
            'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
            'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
        }]
        callback(dateRanges);
    });
}

function natureOffenseChart(natureOffenseVal, drilldownVal){
    var dateRange = $('#irProfileDateRange').val().split('-');
    Highcharts.chart('natureOffenseChart', {
        chart: {
            type: 'bar',
            events: {
                 // click: function (e) {
                 //     console.log(e);
                 // },
                 drilldown: function (e) {
                     console.log(e.seriesOptions);
                    var chart = this;
                    initPersonalIrCounts(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), e.seriesOptions.id, function () {});
                    initProfileOffenses(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), e.seriesOptions.id, function () {
                        initProfileIrHistory();
                        $('#offenseHistory').fadeIn();
                    })
                 },
                 drillup: function (e) {
                    var chart = this;
                    initPersonalIrCounts(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), e.seriesOptions.id, function () {});
                    $('#offenseHistory').fadeOut();
                 },
             }
        },
        title: {
            text: 'IR Count by Nature of Offense'
        },
        // subtitle: {
        //     text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
        // },
        xAxis: {
                 type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total IR Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Nature of Offense',
            colorByPoint: true, 
            data: natureOffenseVal
        }],
        drilldown: {
            series: drilldownVal
        }
    });
}

function showMyIrView(ir_id_val) {
    $.when(fetchPostData({
        ir_id: ir_id_val
    }, '/Discipline/ir_details_myirs')).then(function (res) {
        var result = JSON.parse(res);
        $("#ir_details_template").html(result.ir_details_layout);
        $("#modal-ir-details").modal('show');
    });

}

function exportIrHistory(){
    var dateRange = $('#irProfileDateRange').val().split('-');
    console.log($('#profileName').data('empId'));
    $.fileDownload(baseUrl + "/discipline_mic/ir_history_excel_personal", {
        httpMethod: "POST",
        data: {
            range_start: moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'),
            range_end: moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'),
            empId: $('#profileName').data('empId'),
            liabilityStat: $('#irProfileLiabilityStat').val(),
            offenseId: $('#specificOffenseHistory').val(),
        },
        prepareCallback: function (url) {
            // toastr.options = {
            //     "closeButton": false,
            //     "debug": false,
            //     "newestOnTop": false,
            //     "progressBar": false,
            //     "positionClass": "toast-top-right",
            //     "preventDuplicates": false,
            //     "onclick": null,
            //     "showDuration": "0",
            //     "hideDuration": "0",
            //     "timeOut": "0",
            //     "extendedTimeOut": "0",
            //     "showEasing": "swing",
            //     "hideEasing": "linear",
            //     "showMethod": "fadeIn",
            //     "hideMethod": "fadeOut"
            // };
            // toastr.info("Exporting Excel File...");
        },
        successCallback: function (url) {
            // toastr.clear();
        },
        failCallback: function (responseHtml, url) {
            // swal({
            //     title: 'Export Report Error!',
            //     text: "Something went wrong while exporting the report. Please immediately inform the system administrator about the error",
            //     type: 'error',
            // }).then(function (result) {
            //     toastr.clear();

            // });
        }
    })
}
$('#irProfileLiabilityStat').on('change', function(){
    var dateRange = $('#irProfileDateRange').val().split('-');
    initProfileNatureOfOffenses(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
})

$('#profileTab').on('click', function(){
    initEmpDetails();
    initIrProfileDateRangePicker(function (dateRange) {
        var dateRange = $('#irProfileDateRange').val().split('-');
        initProfileNatureOfOffenses(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
    });
})

$('#specificOffenseHistory').on('change', function(){
    initProfileIrHistory();
})

$('#exportHistoryBtn').on('click', function(){
    exportIrHistory();
})

$('#offenseHistory').on('click', '.profileIrView', function () {
    showMyIrView($(this).data('irid'));
})

$(function(){
})