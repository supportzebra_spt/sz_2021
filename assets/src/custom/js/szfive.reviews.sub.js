var imagePath = baseUrl + '/assets/images/';
var startDate = moment().startOf('week');
var endDate = moment().endOf('week');

getCurrentDateTime(function (date) {
    startDate = moment(date, 'Y-MM-DD').startOf('week');
    endDate = moment(date, 'Y-MM-DD').endOf('week');
});

var rangeInfo = 'on ' + startDate.format('MMM D, Y') + ' - ' + endDate.format('MMM D, Y');

var ReviewersReviewsDatatable = function () {

    var reviewersSelector2 = function (data) {

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/szfive/get_review_details',
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
                        params: {
                            query: {
                                reviewType: data.reviewType,
                                startDate: data.startDate,
                                endDate: data.endDate,
                            },
                        },
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                scroll: false,
                footer: false,
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#detailsReviewSearch'),
            },
            translate: {
                records: {
                    processing: 'Please wait...',
                    noRecords: '<i class="flaticon-file-1" style="font-size: 24px;margin: 0 5px;"></i><br> No review data found.',
                },
            },
            columns: [{
                field: 'sched_date',
                title: 'Survey Date',
                width: 120,
                template: function (data) {
                    var schedDate = moment(data.sched_date).format('MMM D, YYYY');
                    return schedDate;
                }
            }, {
                field: 'reviewerLname',
                title: 'Reviewer',
                width: 170,
                textAlign: 'center',
                template: function (data) {
                    return '<span>' + data.reviewerLname + ', ' + data.reviewerFname + '</span>';
                }
            }, {
                field: 'answerLname',
                title: 'Reviewee',
                width: 170,
                textAlign: 'center',
                template: function (data) {
                    return '<span>' + data.answerLname + ', ' + data.answerFname + '</span>';
                }
            }, {
                field: 'dateAnswered',
                title: 'Date Answered',
                width: 160,
                template: function (data) {
                    var dateAnswered = moment(data.dateAnswered).format('MMM D, YYYY hh:mm:ss A');
                    return '<span>' + dateAnswered + '</span>';
                }
            }, {
                field: 'dateReviewed',
                title: 'Date Reviewed',
                width: 160,
                template: function (data) {
                    var dateReviewed = moment(data.dateReviewed).format('MMM D, YYYY hh:mm:ss A');
                    return '<span>' + dateReviewed + '</span>';
                }
            }, {
                sortable: false,
                field: 'answerId',
                title: 'Action',
                textAlign: 'center',
                width: 60,
                template: function (data) {
                    return '<a onClick="viewReviewsDetails(' + data.answerId + ')" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View "><i class="fa fa-eye"></i></a>';
                }
            }]
        };

        var datatable = $('.m_datatable_reviewers_reviews').mDatatable(options);
        var query = datatable.getDataSourceQuery();

        $('#select2__top_reviews').on('change', function () {
            var title = $("#select2__top_reviews :selected").text();
            var query = datatable.getDataSourceQuery();
            query.reviewType = $(this).val().toLowerCase();
            datatable.setDataSourceQuery(query);
            datatable.load();

            rangeInfo = 'on ' + startDate.format('MMM D, Y') + ' - ' + endDate.format('MMM D, Y');
            $('#review__type-head').html(title);
            $('#range-desc').html(rangeInfo);

        }).val(typeof query.reviewType !== 'undefined' ? query.reviewType : '');
    };

    $('#select2__top_reviews').select2({
        placeholder: "Select an option",
    });

    return {
        init: function (data) {
            reviewersSelector2(data);
        },
    };
}();

var DatePickers = function () {

    var reviewDates = function () {

        if ($('#details_review_daterange').length == 0) {
            return;
        }

        getCurrentDateTime(function (date) {
            var picker = $('#details_review_daterange');
            var start = moment(date, 'Y-MM-DD').startOf('isoWeek');
            var end = moment(date, 'Y-MM-DD').endOf('isoWeek');

            function cb(start, end, label) {

                var title = '';
                var range = '';

                if (start.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').startOf('isoWeek').format('YYYY-MM-DD') && end.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').endOf('isoWeek').format('YYYY-MM-DD')) {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Today') {
                    title = 'Today:';
                    range = start.format('MMM D, Y');
                } else if (label == 'Yesterday') {
                    title = 'Yesterday:';
                    range = start.format('MMM D, Y');
                } else if (label == 'This Week') {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Week') {
                    title = 'Last Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'This Month') {
                    title = 'This Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Month') {
                    title = 'Last Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else {
                    title = 'Custom:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                }

                picker.find('.m-subheader__daterange-date-reviews').html(range);
                picker.find('.m-subheader__daterange-title-reviews').html(title);

                var reviewType = $('#select2__top_reviews :selected').val();
                reviewType = reviewType != "" ? reviewType : 'all';

                var params = {
                    reviewType: reviewType,
                    startDate: start.format('YYYY-MM-DD'),
                    endDate: end.format('YYYY-MM-DD')
                };

                showReviewDetails(params);

                rangeInfo = 'on ' + start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');

                $('#range-desc').html(rangeInfo);

                startDate = start;
                endDate = end;
            }

            picker.daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: {
                    'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                    'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                    'This Week': [moment(date, 'Y-MM-DD').startOf('isoWeek'), moment(date, 'Y-MM-DD').endOf('isoWeek')],
                    'Last Week': [moment(date, 'Y-MM-DD').subtract(1, 'week').startOf('isoWeek'), moment(date, 'Y-MM-DD').subtract(1, 'week').endOf('isoWeek')],
                    'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                    'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end, '');
        });
    }

    var topReviewerDates = function () {

        if ($('#top_reviewers_daterange').length == 0) {
            return;
        }

        getCurrentDateTime(function (date) {
            var picker = $('#top_reviewers_daterange');
            var start = moment(date, 'Y-MM-DD').startOf('isoWeek');
            var end = moment(date, 'Y-MM-DD').endOf('isoWeek');

            function cb(start, end, label) {

                var title = '';
                var range = '';

                if (start.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').startOf('isoWeek').format('YYYY-MM-DD') && end.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').endOf('isoWeek').format('YYYY-MM-DD')) {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Today') {
                    title = 'Today:';
                    range = start.format('MMM D, Y');
                } else if (label == 'This Week') {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Week') {
                    title = 'Last Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'This Month') {
                    title = 'This Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Month') {
                    title = 'Last Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else {
                    title = 'Custom:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                }

                picker.find('.daterange-date-top-reviewer').html(range);
                picker.find('.daterange-title-top-reviewer').html(title);

                var params = {
                    startDate: start.format('YYYY-MM-DD'),
                    endDate: end.format('YYYY-MM-DD')
                };

                getTopReviewers(params);

                // rangeInfo = 'on ' + start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');

                // $('#range-desc').html(rangeInfo);
                $('#top-reviewer_dates').html('on ' + start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y'));


                startDate = start;
                endDate = end;
            }

            picker.daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: {
                    'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                    'This Week': [moment(date, 'Y-MM-DD').startOf('isoWeek'), moment(date, 'Y-MM-DD').endOf('isoWeek')],
                    'Last Week': [moment(date, 'Y-MM-DD').subtract(1, 'week').startOf('isoWeek'), moment(date, 'Y-MM-DD').subtract(1, 'week').endOf('isoWeek')],
                    'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                    'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end, '');
        });
    }

    return {
        init: function () {
            reviewDates();
            topReviewerDates();
        },
    };
}();

jQuery(document).ready(function () {

    var params = {
        reviewType: 'all',
        startDate: startDate.format('YYYY-MM-DD'),
        endDate: endDate.format('YYYY-MM-DD')
    };

    $('#range-desc').html(rangeInfo);

    DatePickers.init();
});

function showReviewDetails(params) {
    $('.m_datatable_reviewers_reviews').mDatatable('destroy');
    ReviewersReviewsDatatable.init(params);
}

function viewReviewsDetails(answerId) {
    $('#alreadyReviewed').html('');

    $.when(fetchPostData({
            answerId: answerId
        }, '/szfive/get_reviews_specific_answer'))
        .then(function (data) {

            var data = jQuery.parseJSON(data);

            header = '<div class="m-widget3__header" style="display: table;padding: 0px 10px;padding-top: 8px; ">';
            header += '<div class="m-widget3__user-img" style="margin-bottom: .7rem;">';
            header += '<img class="m-widget3__img answer-pic-dt' + data.uid + '" src="' + data.pic + '" onerror="noImage2(' + data.uid + ')" alt="" style="width: 3.2rem;border-radius: 50%;">';
            header += '</div>';
            header += '<div class="m-widget3__info" style="display:table-cell;width:100%; padding-left:1rem;font-size:1rem;vertical-align:middle;">';
            header += '<span class="m-widget3__username" style="font-size: 15px;color: #36817a;">' + data.fullname + ' answered ';
            header += '<u style=" font-size: 17px;">' + data.answer + '</u> out of';
            header += '<u>' + data.numberOfChoices + '</u> for the daily survey on ' + data.survey_date + '.';
            header += '</span>';
            header += '<br>';
            header += '<span class="m-widget3__time" style="font-size: .85rem;">' + data.answer_date + '</span>';
            header += '</div>';
            header += '</div>';

            $('#view_answer_review_header').html(header);

            str = '<div class="m-widget3__item">';
            str += '<div class="m-widget3__body" id="review_userAnswerId" data-id="' + answerId + '" style="padding: 0px 10px; margin-bottom: 25px; ">';
            str += '<blockquote class="blockquote">';
            str += '<p class="mb-0" style=" font-weight: 400;padding: 0 0 10px 0;">' + data.detailedAnswer + '</p>';
            str += '<footer class="blockquote-footer">Feeling ';
            str += '<img src="' + imagePath + data.choice_img + '" style=" width: 20px; ">';
            str += '<cite title="Source Title"> ' + data.choice_label + ' </cite>';
            str += '</footer>';
            str += '</blockquote>';
            str += '</div>';
            str += '</div>'

            $('.view_reviews_answer_content').html(str);

            var dateReviewed = moment(data.dateReviewed).format('MMMM D, YYYY hh:mm:ss A');
            var message = data.message ? data.message : 'No remarks';

            details = '<span class="m-badge m-badge--success m-badge--wide">Reviewed by</span>';
            details += '<span style="color: #649e99;">';
            details += '<b style="font-weight: 400;"> ' + data.supervisorFname + ' ' + data.supervisorLname + '</b> on';
            details += '<b style="font-weight: 400;"> ' + dateReviewed + '</b></span>';
            details += '<p style="border: 4px solid #f7f7fa;padding: 8px;margin-top: 10px;">';
            details += '<b style="color: #000000b5;font-weight: 600;">Remarks:</b> &nbsp;&nbsp' + message + '</p>';
            details += '<small>Reviewed <b><i>' + data.time_diff + '</i></b> after the team member answered the survey.</small>';

            $('#reviewTextarea').hide();
            $('#mark_as_review_wrapper').html(details);
        });

    setTimeout(function () {
        $('#view_specific_ans_reviews_modals').modal('show');
    }, 500);
}

function noImage2(id) {
    $('.answer-pic-dt' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}


function showSpinner() {
    mApp.block('#top-reviewers-wrapper', {
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Please wait...'
    });

    setTimeout(function () {
        mApp.unblock('#top-reviewers-wrapper');
    }, 500);
}

function topReviewerImg1(id) {
    $('.top-reviewer-1-' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function topReviewerImg2(id) {
    $('.top-reviewer-2-' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
}

function getTopReviewers(params) {
    $.when(fetchPostData({
            params
        }, '/szfive/get_top_reviewers'))
        .then(function (resp) {
            var resp = jQuery.parseJSON(resp);
            var obj = resp.data;

            if (obj.length != 0) {
                $('#no-top-reviewers').hide();

                var count = 1;
                showSpinner();
                Object.keys(obj).forEach(function (key) {
                    var data = obj[key];
                    $('#top_reviewer_title_' + count).html(data.title);
                    $('#top_reviewer_pic_' + count).html('<img class="top-reviewer-' + count + '-' + data.id + '" src="' + data.avatar + '" onerror="topReviewerImg' + count + '(' + data.id + ')" alt="" style = "height: 110px;border-radius: 50%;" >');
                    $('#top_reviewer_name_' + count).html(data.name);
                    $('#top_reviewer_position_' + count).html(data.dep_name + ' | ' + data.pos_details);
                    $('#top_reviewer_' + count).show();
                    count = count + 1;
                });
            } else {
                showSpinner();
                setTimeout(function () {
                    $('#top_reviewer_1').hide();
                    $('#top_reviewer_2').hide();
                    $('#no-top-reviewers').show();
                }, 600);
            }
        });
}