var imagePath = baseUrl + '/assets/images/';
var offsetCounter = 0;
var totalHighfives = 0;
var offset = 0;
var offsetCommentCounter = 0;
var offsetComment = 0;

function noImageFound(image) {
    $('.' + image).attr('src', baseUrl + '/assets/images/img/sz.png');
}
/* FUNCTIONS */

function addReaction(params) {

    var htmlData = '<i class="' + params.reactionName.toLowerCase() + 'IconSmall likeTypeSmall defaultIcon"></i>' + params.reactionName;

    $.when(fetchPostData({
                highfiveId: params.highfiveId,
                reactionType: params.reactionType
            },
            '/szfive/add_high_five_reaction'))
        .then(function (response) {

            var data = jQuery.parseJSON(response);
            if (data.success == true) {

                var val = data.details;

                $('#like-info' + params.highfiveId).html('<span style="margin-left: 10px;">' + val.likes_info);
                $('#like-info-comment' + params.highfiveId).html('<span style="margin-left: 10px;">' + val.likes_info);
                $('#all-reaction' + params.highfiveId).html(val.reactions);
                $('#all-reaction-comment' + params.highfiveId).html(val.reactions);
                $("#like" + params.highfiveId).html(htmlData).removeClass('reaction').removeClass('tooltipstered').addClass('unLike').attr('rel', 'unlike');
                $("#like-comment" + params.highfiveId).html(htmlData).removeClass('reaction').removeClass('tooltipstered').addClass('unLike-comment').attr('rel', 'unlike');
                $("#" + params.x).hide();

                if (data.system_notif_ids.length != 0) {
                    systemNotification(data.system_notif_ids,'snapsz');
                }
            }
        });
}

function removeReaction(params) {

    var htmlData = '<i class="likeIconDefault" ></i>Like';

    $.when(fetchPostData({
                highfiveId: params.highfiveId,
                reactionType: params.reactionType
            },
            '/szfive/delete_high_five_reaction'))
        .then(function (response) {
            var resp = jQuery.parseJSON(response);

            if (resp.success == 1) {

                if (resp.data.length == 0) {
                    $('#like-info' + params.highfiveId).html('');
                    $('#like-info-comment' + params.highfiveId).html('');
                    $('#all-reaction' + params.highfiveId).html("");
                    $('#all-reaction-comment' + params.highfiveId).html("");

                } else {
                    var val = resp.data;
                    $('#all-reaction' + params.highfiveId).html('<span style="margin-left: 10px;">' + val.reactions);
                    $('#all-reaction-comment' + params.highfiveId).html('<span style="margin-left: 10px;">' + val.reactions);
                    $('#like-info' + params.highfiveId).html(val.likes_info);
                    $('#like-info-comment' + params.highfiveId).html(val.likes_info);
                }
                $("#like" + params.highfiveId).html(htmlData).addClass('reaction').addClass('tooltipstered').removeClass('unLike');
                $("#like-comment" + params.highfiveId).html(htmlData).addClass('reaction').addClass('tooltipstered').removeClass('unLike-comment');
            }
        });
}

// Functions
function commentHi5(id) {
    $('#m-scrollable-comment').css('max-height', '250px');
    $('textarea.highfive_post_comment').mentionsInput({
        onDataRequest: function (mode, query, callback) {
            $.getJSON(baseUrl + '/szfive/user_comment_mentions', function (responseData) {
                responseData = _.filter(responseData, function (item) {
                    return item.name.toLowerCase().indexOf(query.toLowerCase()) >
                        -1
                });
                callback.call(this, responseData);
            });
        },
        templates: {
            mentionItemSyntax: _.template('<%= name %>'),
        }
    });
    $.when(fetchPostData({
            id: id,
            offset: 0,
            noOfItems: 10
        }, '/szfive/get_highfive_comments'))
        .then(function (response) {
            var res = jQuery.parseJSON(response);
            var data = res.entry;
            var pic = data.pic ? data.pic : "images/img/sz.png";
            var comments = (data.totalComment.count != 0) ? '<a data-html="true"  data-placement="top" data-toggle="tooltip" href="#" data-original-title="'+data.tooltip+'" class="like-info pull-right like-info' + data.hiFiveId + '">' + data.totalComment.count + ' comments</a>' : '';
            var likes_info = (data.likes_info != '') ? '<span style="margin-left: 10px;">' + data.likes_info + '</span>' : '';
            var dateCreated = moment(data.dateCreated).format('MMM D, YYYY hh:mm:ss A');

            header = '<div class="m-widget3__header" style="display: table;">';
            header += '<div class="m-widget3__user-img" style="margin-bottom: .7rem;">';
            header += ' <img class="m-widget3__img user-pic-dt' + data.emp_id + '" src="' + imagePath + pic + '" onerror="noImage(' + data.emp_id + ')"alt="" style="width: 3.2rem;border-radius: 50%;">';
            header += ' </div>';
            header += ' <div class="m-widget3__info" style="display:table-cell;width:100%; padding-left:1rem;font-size:1rem;vertical-align:middle;">';
            header += ' <span class="m-widget3__username" style="font-size: 15px;color: #36817a;">' + data.fullname + ' gave ' + data.header_msg + ' a High Five!</span>';
            header += '<br><span class="m-widget3__time" style="font-size: .85rem;">' + dateCreated + '</span>';
            header += '</div> </div>';
            str = '<div class="m-widget3__item" id="hi5-comment-modal" data-id="' + data.hiFiveId + '">';
            str += '<div class="m-widget3__body">';
            str += '<p class="m-widget3__text">' + data.message + '</p>';
            str += '<div>';
            str += '<span id="all-reaction-comment' + data.hiFiveId + '">' + data.reactions + '</span><a data-html="true"  data-placement="top" data-toggle="tooltip" href="#" data-original-title="'+data.tooltip+'" class="like-info" id="like-info-comment' + data.hiFiveId + '">' + likes_info + '</a> ' + comments + '</div>';
            str += '<div class="row" style="text-align: center;margin-left: 0px;margin-top: 12px;font-size: 12px;">';
            if (data.my_reaction == '') {

                str += '<a class="reaction" id="like-comment' + data.hiFiveId + '" rel="like" style="margin-right: 10px;">';
                str += '<i class="likeIconDefault" ></i> Like';
            } else {
                str += '<a class="unLike-comment" id="like-comment' + data.hiFiveId + '" rel="unlike" style="margin-right: 10px;">';
            }
            str += data.my_reaction;
            str += '</a>';
            str += '<a href="#" onClick="commentHi5(' + data.hiFiveId + ')"><i class="fa fa-comment" style="color: #afb4bd;font-size: 1rem;"></i> Comment</a></div></div>';
            str += '</div>';
            str += '</div></div></div>';

            $('#comment-modal-header').html(header);
            $('#comment-parent').html(str);
            $("input[name='hiFiveId']").val(data.hiFiveId);

            options = '';
            $.each(res.sent_to_users, function (key, val) {
                options += '<option value="' + key + '">' + val + '</option>';
            });
            $('#select_user').selectpicker('destroy');
            $('#select_user').html(options);
            $('#select_user').selectpicker();

            var cdata = {
                sessionId: data.session_id,
                postId: data.hiFiveId,
                total: data.totalComment.count
            };
            $('.like-info').tooltip();
            loadComments(res.child_comments, cdata);
        });
    setTimeout(function () {
        $('#comment_hi5_modal').modal('show');
    }, 1000);
}

function loadComments(comments, cdata) {
    str = '';

    $.each(comments, function (key, val) {

        var imageClass = "no-comment-image-" + val.emp_id;
        pic = val.pic ? val.pic : "images/img/sz.png";
        str += '<div class="m-widget3__header comment-cwrapper comment-cwrapper' + val.commentId + '">';
        str += '<div class="m-widget3__user-img">';
        str += '<img class="m-widget3__img comment-pic no-comment-image-' + val.emp_id + '" src="' + imagePath + pic + '" onerror="noImageFound(' + "'" + imageClass + "'" + ')" alt="">';
        str += '</div>';
        str += '<div class="m-widget3__info comment-info">';
        str += '<span class="m-widget3__username comment-name">' + val.fname + ' ' + val.lname + '</span><span style="font-size: 11px;font-style: italic;margin-left: 5px;">' + val.dateCreated + '</span>';
        str += '<p class="m-widget3__time comment-content" id="comment-content-' + val.commentId + '-">' + val.content + '</p>';
        str += '</div>';
        if (cdata.sessionId == val.uid) {
            str += '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="click" aria-expanded="true">';
            str += '<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn m-btn m-btn--link action-btn">';
            str += ' <i class="la la-ellipsis-h"></i>';
            str += ' </a>';
            str += ' <div class="m-dropdown__wrapper">';
            str += '<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 29.7032px;"></span>';
            str += '<div class="m-dropdown__inner">';
            str += ' <div class="m-dropdown__body">';
            str += '<div class="m-dropdown__content">';
            str += ' <ul class="m-nav">';
            str += '<li class="m-nav__item">';
            str += '<a href="#" onClick="showEditComment(' + val.commentId + ')" class="m-nav__link">';
            str += ' <i class="m-nav__link-icon la la-edit"></i>';
            str += ' <span class="m-nav__link-text">Edit</span>';
            str += '</a>';
            str += '</li>';
            str += ' <li class="m-nav__item">';
            str += ' <a href="#" class="m-nav__link" onClick="deleteHi5Comment(' + val.commentId + ',' + cdata.postId + ')">';
            str += '<i class="m-nav__link-icon la la-trash-o"></i>';
            str += '<span class="m-nav__link-text">Delete</span>';
            str += '</a>';
            str += '</li>';
            str += ' </ul>';
            str += ' </div>';
            str += '</div>';
            str += '</div>';
            str += ' </div>';
            str += '</div>';
        }
        str += '</div>';
    });

    $('#comment-child').append(str);

    offsetCommentCounter = offsetCommentCounter + 1;
    offsetComment = offsetCommentCounter * 10

    if (cdata.total > offsetComment) {
        $('.view-more').show();
    } else {
        $('.view-more').hide();
    }
}

function showSpinner() {
    mApp.block('#comment_hi5_modal .modal-content', {});

    setTimeout(function () {
        mApp.unblock('#comment_hi5_modal .modal-content');
    }, 1000);
}

function deleteHi5Comment(id, postId) {
    swal({
        title: '',
        text: 'Are you sure you want to permanently remove this comment?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No, cancel'
    }).then(function (result) {
        if (result.value === true) {

            $.ajax({
                url: baseUrl + "/szfive/delete_hi5_comment",
                type: 'POST',
                dataType: "json",
                data: {
                    id: id
                },
                success: function (data) {
                    showSpinner();
                    setTimeout(function () {
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "2000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };

                        toastr.success("Comment deleted.");
                        $('.comment-cwrapper' + id).remove();
                        var C = parseInt($(".like-info" + postId).html());
                        $(".like-info" + postId).html(C - 1 + ' comments');

                    }, 1000);
                },
                error: function () {
                    swal({
                        "title": "",
                        "text": "Error",
                        "type": "error",
                        "timer": 2000,
                        "showConfirmButton": false
                    });
                }
            });
        }
    });
}

function showEditComment(id) {
    $('textarea.highfive_edit_comment').mentionsInput({
        onDataRequest: function (mode, query, callback) {
            $.getJSON(baseUrl + '/szfive/user_comment_mentions', function (responseData) {
                responseData = _.filter(responseData, function (item) {
                    return item.name.toLowerCase().indexOf(query.toLowerCase()) >
                        -1
                });
                callback.call(this, responseData);
            });
        },
        templates: {
            mentionItemSyntax: _.template('<%= name %>'),
        }
    });
    $.when(fetchPostData({
            id: id
        }, '/szfive/get_hi5_comment_details'))
        .then(function (response) {

            var data = jQuery.parseJSON(response);

            $('textarea#hi5commentDetail').val(data.content);
            $('input[name="commentId"]').val(data.id);
            $('input[name="isPublic"]').val(data.isPublic);
        });

    setTimeout(function () {
        $('#edit_comment_hi5_modal').modal('show');
    }, 1000);
}

var General = function () {

    var submitComment = function () {
        $("#highfive_comment_form").validate({
            rules: {
                comment: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "Comment must not be empty.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {

                var comment = $('#highfive_comment_form').find('input[name="comment"]').val();
                var hiFiveId = $("input[name='hiFiveId']").val();
                var privateToUser = $('#select_user').val();
                var isPublic = 1;

                if ($('#checkPrivate').is(':checked')) {
                    isPublic = 0;
                }

                var content = '';
                var usermention = 0;

                $('textarea.highfive_post_comment').mentionsInput('val', function (userMention) {
                    if (userMention != '') {
                        content = userMention;
                    }
                });

                $('textarea.highfive_post_comment').mentionsInput('getMentions', function (data) {
                    if (data.length != 0) {
                        usermention = data;
                    }
                });

                var options = {
                    url: baseUrl + "/szfive/add_highfive_comment",
                    type: 'POST',
                    dataType: "json",
                    data: {
                        hiFiveId: hiFiveId,
                        privateToUser: privateToUser,
                        isPublic: isPublic,
                        content: content,
                        usermention: usermention
                    },
                    success: function (resp) {
                        var val = resp.data;
                        if (resp.system_notif_ids.length != 0) {
                            systemNotification(resp.system_notif_ids,'snapsz');
                        }

                        $('#inputHi5Comment').val("");

                        var img = $(".header-userpic" + val.session_id).attr('src')

                        var imageClass = "comment-pic-dt-" + val.emp_id;
                        str = '<div class="m-widget3__header comment-cwrapper comment-cwrapper' + val.commentId + '">';
                        str += '<div class="m-widget3__user-img">';
                        str += '<img class="m-widget3__img comment-pic ' + imageClass + '" src="' + img + '" onerror= ' + 'noImageFound("' + imageClass + '") ' + ' alt="">';
                        str += '</div>';
                        str += '<div class="m-widget3__info comment-info">';
                        str += '<span class="m-widget3__username comment-name">' + val.fullname + '</span><span style="font-size: 11px;font-style: italic;margin-left: 5px;">' + val.dateCreated + '</span>';
                        str += '<p class="m-widget3__time comment-content" id="comment-content-' + val.commentId + '-">' + val.content + '</p>';
                        str += '</div>';
                        str += '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="click" aria-expanded="true">';
                        str += '<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn m-btn m-btn--link action-btn">';
                        str += '<i class="la la-ellipsis-h"></i>';
                        str += ' </a>';
                        str += ' <div class="m-dropdown__wrapper">';
                        str += ' <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 29.7032px;"></span>';
                        str += '<div class="m-dropdown__inner">';
                        str += '<div class="m-dropdown__body">';
                        str += ' <div class="m-dropdown__content">';
                        str += '<ul class="m-nav">';
                        str += ' <li class="m-nav__item">';
                        str += '<a href="#" onClick="showEditComment(' + val.commentId + ')" class="m-nav__link">';
                        str += '<i class="m-nav__link-icon la la-edit"></i>';
                        str += '<span class="m-nav__link-text">Edit</span>';
                        str += '</a>';
                        str += '</li>';
                        str += ' <li class="m-nav__item">';
                        str += '<a href="#" class="m-nav__link" onClick="deleteHi5Comment(' + val.commentId + ',' + val.post_id + ')">';
                        str += ' <i class="m-nav__link-icon la la-trash-o"></i>';
                        str += ' <span class="m-nav__link-text">Delete</span>';
                        str += '</a>';
                        str += '</li>';
                        str += ' </ul>';
                        str += ' </div>';
                        str += '</div>';
                        str += '</div>';
                        str += ' </div>';
                        str += '</div>';
                        str += '</div>';

                        showSpinner();
                        setTimeout(function () {
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };

                            toastr.success("Comment successfully sent!");

                            $('#comment-child').append(str);
                            $('.view-more').hide();
                            var C = parseInt($(".like-info" + hiFiveId).html());
                            $(".like-info" + hiFiveId).html(C + 1 + ' comments');


                            var scrollHeight = $('.survey-answer-wrapper').find('#mCSB_2_container')[0].scrollHeight - 250;

                            $('.survey-answer-wrapper').find('#mCSB_2_container').css('top', -scrollHeight);

                            $('.comment-cwrapper' + val.commentId).addClass('highlighted', 1000);
                            setTimeout(function () {
                                $('.comment-cwrapper' + val.commentId).removeClass('highlighted', 1000);
                            }, 500);
                            $('#checkPrivate').prop('checked', false);
                        }, 1000);
                    },
                    error: function () {
                        alert('error');
                    },
                };
                $(form).ajaxSubmit(options);
            }
        });
    }

    var editComment = function () {

        $("#highfive_edit_comment_form").validate({
            rules: {
                commentEdit: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "Comment must not be empty.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {

                var content = $('#highfive_edit_comment_form').find('input[name="commentEdit"]').val();
                var commentId = $('input[name="commentId"]').val();
                var usermention = 0;
                var hiFiveId = $('#hi5-comment-modal').data('id');
                var isPublic = $('input[name="isPublic"]').val();

                $('textarea.highfive_edit_comment').mentionsInput('val', function (userMention) {
                    if (userMention != '') {
                        content = userMention;
                    }
                });

                $('textarea.highfive_edit_comment').mentionsInput('getMentions', function (data) {
                    if (data.length != 0) {
                        usermention = data;
                    }
                });

                var options = {
                    url: baseUrl + "/szfive/update_hi5_comment",
                    type: 'POST',
                    dataType: "json",
                    data: {
                        hiFiveId: hiFiveId,
                        commentId: commentId,
                        content: content,
                        usermention: usermention,
                        privateToUser: 0,
                        isPublic: isPublic,
                    },
                    success: function (response) {
                        if (response.status) {
                            if (response.system_notif_ids.length != 0) {
                                systemNotification(response.system_notif_ids,'snapsz');
                            }

                            mApp.block('#edit_comment_hi5_modal .modal-content', {
                                overlayColor: '#000000',
                                type: 'loader',
                                state: 'primary',
                                message: 'Processing...'
                            });

                            setTimeout(function () {
                                mApp.unblock(
                                    '#edit_comment_hi5_modal .modal-content'
                                );

                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-center",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "2000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                };

                                $('#comment-content-' + commentId + '-').html(response.comment);
                                $('#edit_comment_hi5_modal').modal('hide');

                                toastr.success("Comment updated.");

                            }, 1000);
                        } else {
                            swal({
                                "title": "Error",
                                "text": "There was ab error updating your comment!",
                                "type": "error",
                                "timer": 1500,
                                "showConfirmButton": false
                            });
                        }

                    },
                    error: function () {
                        alert('error');
                    },
                };
                $(form).ajaxSubmit(options);
            }
        });
    }

    return {
        init: function () {
            submitComment();
            editComment();
        }
    };
}();

jQuery(document).ready(function () {
    General.init();

    $(".likeTypeAction").tipsy({
        gravity: 's',
        live: true
    });

    $('.like-info').tooltip();

    $(".reaction").livequery(function () {
        var reactionsCode = '<span class="likeTypeAction" original-title="Like" data-reaction="1"><i class="likeIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Love" data-reaction="2"><i class="loveIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Haha" data-reaction="3"><i class="hahaIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Wow" data-reaction="4"><i class="wowIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Cool" data-reaction="5"><i class="coolIcon likeType"></i></span>' +
            // '<span class="likeTypeAction" original-title="Confused" data-reaction="6"><i class="confusedIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Sad" data-reaction="7"><i class="sadIcon likeType"></i></span>' +
            '<span class="likeTypeAction last" original-title="Angry" data-reaction="8"><i class="angryIcon likeType"></i></span>';

        $(this).tooltipster({
            contentAsHTML: true,
            interactive: true,
            multiple: true,
            content: $(reactionsCode),
        });
    });

    /*Reaction*/
    $("body").on("click", ".likeTypeAction", function () {
        var reactionType = $(this).attr("data-reaction");
        var reactionName = $(this).attr("original-title");
        var rel = $(this).parent().parent().attr("rel");
        var x = $(this).parent().parent().attr("id");
        var sid = x.split("reaction");
        var highfiveId = sid[1].replace(/-comment/, '');

        var params = {
            highfiveId: highfiveId,
            reactionType: reactionType,
            reactionName: reactionName,
            x: x,
        };
        addReaction(params);
        return false;
    });

    $("body").on("click", ".unLike", function () {
        var reactionType = '1';
        var x = $(this).attr("id");
        var sid = x.split("like");
        var highfiveId = sid[1];

        var params = {
            highfiveId: highfiveId,
            reactionType: reactionType,
            x: x,
        };
        removeReaction(params);
        return false;
    });

    $('#viewMoreComments').on("click", function () {
        var id = $("input[name='hiFiveId']").val();
        showSpinner();

        $.when(fetchPostData({
                id: id,
                offset: offsetComment,
                noOfItems: 10,
                type: 'sz_highfive'
            }, '/szfive/load_more_comments'))
            .then(function (response) {
                var data = jQuery.parseJSON(response);
                var cdata = {
                    sessionId: data.info.session_id,
                    postId: data.info.hiFiveId,
                    total: data.info.total
                };
                setTimeout(function () {
                    loadComments(data.comments, cdata);
                }, 1000);

            });
    });

    $('#comment_hi5_modal').on('hidden.bs.modal', function () {
        $('#comment-modal-header').html('');
        $('#checkPrivate').prop('checked', false);
        $('#comment-parent').html('');
        $('#comment-child').html('');
        $('#inputHi5Comment').val('');
        $('textarea.highfive_post_comment').mentionsInput('reset', function () {});

        offsetComment = 0;
        offsetCommentCounter = 0;
    });

    $('#edit_comment_hi5_modal').on('hidden.bs.modal', function () {
        $('textarea.highfive_edit_comment').mentionsInput('reset', function () {});
    });
});