var gulp = require('gulp'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect'),
    uglify = require('gulp-uglify'),
    cleanCss = require('gulp-clean-css'),
    sass = require('gulp-sass'),
    merge = require('merge-stream');

var path = {
    js: {
        src: [
            'assets/src/vendors/base/vendors.bundle.js',
            'assets/src/demo/default/base/scripts.bundle.js',
            'assets/src/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js',
            'assets/src/demo/default/custom/components/custom/formValidation/dist/js/formValidation.js',
            'assets/src/demo/default/custom/components/custom/formValidation/dist/js/framework/bootstrap.min.js',
            'assets/src/custom/js/general.js',
        ],
        dest: 'assets/js/'
    },
    sass: {
        src: [
            'assets/src/custom/sass/styles.scss',
        ],
    },
    css: {
        src: [
            'assets/src/vendors/base/vendors.bundle.css',
            'assets/src/demo/default/base/style.bundle.css',
            'assets/src/demo/default/custom/components/custom/formValidation/dist/css/formValidation.min.css',
        ],
        dest: 'assets/css/'
    },
};

// Compile JS
gulp.task('js', function () {
    return gulp.src(path.js.src)
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.js.dest))
        .pipe(connect.reload());

});

// Compile Sass
gulp.task('sass', function () {
    var sassStream = gulp.src(path.sass.src)
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCss());

    var cssStream = gulp.src(path.css.src)
        .pipe(cleanCss());

    return merge(cssStream, sassStream)
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(path.css.dest));
});

// Watch changes
gulp.task('watch', function () {
    gulp.watch(path.sass.src, ['sass']);
    gulp.watch(path.js.src, ['js']);
});

// Fire!
gulp.task('default', ['watch', 'js', 'sass']);