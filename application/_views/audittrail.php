<!DOCTYPE html> 
<html  lang="en">

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
            body { padding-right: 0 !important }
        </style>


        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/fontawesome/fontawesome.css"> -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">

        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });

        </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/datepicker/datepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
                /* Datepicker bootstrap */

                $(function () {
                    "use strict";
                    $('.bootstrap-datepicker').bsdatepicker({
                        format: 'yyyy-mm-dd'
                    });
                });

        </script>

        <!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/datepicker-ui/datepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

        <!-- Bootstrap Timepicker -->

        <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/timepicker/timepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/timepicker/timepicker.js"></script>
        <script type="text/javascript">

                /* Timepicker */

                $(function () {
                    "use strict";
                    $('.timepicker-example').timepicker();
                });
        </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/chosen/chosen.js"></script>
        <script>
                $(function () {
                    "use strict";
                    $(".chosen-select").chosen();
                    $(".chosen-search").append('<i class="glyph-icon icon-search"></i>');
                    $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
                });
        </script>

        <script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
    </head>

    <body>
        <div id="sb-site">
            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <div id="page-header" class="bg-black">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>

                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">


                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div id="page-title">
                            <div class="row">
                                <div class="col-md-8"> <h2>Audit Trail</h2></div>
                                <div class="col-md-4 text-right"><button id="save_image_locally" class='btn btn-default' disabled><i class="glyph-icon icon-download"></i></button> </div>
                            </div>
                            <hr>

                            <div class="panel" style='margin-bottom:0'>
                                <div class="panel-body" style='padding-bottom:0'>
                                    <div class="form-group row">

                                        <div class="col-md-4">
                                            <div class="panel" style='margin-bottom:0'>
                                                <div class="panel-heading">FROM:</div>
                                                <div class="panel-body">
                                                    <form action="" class="form-horizontal">
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-md-2">DATE:</label>
                                                            <div class="col-md-10">
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon">
                                                                        <i class="glyph-icon icon-calendar"></i>
                                                                    </span>
                                                                    <input type="text" class="bootstrap-datepicker form-control"  id='dateFrom'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-md-2">TIME:</label>
                                                            <div class="col-md-8">
                                                                <div class="bootstrap-timepicker dropdown">
                                                                    <input class="timepicker-example form-control" type="text" id='timeFrom'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel" style='margin-bottom:0'>
                                                <div class="panel-heading">TO:</div>
                                                <div class="panel-body">
                                                    <form action="" class="form-horizontal">
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-md-2">DATE:</label>
                                                            <div class="col-md-10">
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon">
                                                                        <i class="glyph-icon icon-calendar"></i>
                                                                    </span>
                                                                    <input type="text" class="bootstrap-datepicker form-control"  id='dateTo'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-md-2">TIME:</label>
                                                            <div class="col-md-8">
                                                                <div class="bootstrap-timepicker dropdown">
                                                                    <input class="timepicker-example form-control" type="text" id='timeTo'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel" style='margin-bottom:0'>
                                                <div class="panel-heading">EMPLOYEE NAME:</div>
                                                <div class="panel-body">
                                                    <select id="employeelist" class="chosen-select">
                                                        <option value="All" selected>All</option>
                                                        <?php foreach ($users as $user): ?>
                                                            <option value="<?php echo $user->uid; ?>"><?php echo $user->lname . ', ' . $user->fname; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="panel-footer text-right" style='padding-bottom:13px'>
                                                    <a href="#" class="btn btn-info"  id='btnFilter'>
                                                        <span class="glyph-icon icon-separator">
                                                            <i class="glyph-icon icon-filter"></i>
                                                        </span>
                                                        <span class="button-content">
                                                            FILTER LOGS
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer" style="padding:15px 10px">
                                    <div id="trailResult"></div>
                                </div>


                            </div>
                        </div>
                    </div>

                    <!-- JS Demo -->
                    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
                    </body>
                    <script>
                $(function () {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
                        cache: false,
                        success: function (html) {

                            $('#headerLeft').html(html);
                            //alert(html);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
                        cache: false,
                        success: function (html) {

                            $('#sidebar-menu').html(html);
                            // alert(html);
                        }
                    });
                });
                    </script>
                    <script>
                            $(function () {
                                $('#btnFilter').click(function () {

                                    $('#save_image_locally').prop('disabled', true);
                                    var employee = $("#employeelist").val();
                                    var dateFrom = $('#dateFrom').val();
                                    var timeFrom = $('#timeFrom').val();
                                    var dateTo = $('#dateTo').val();
                                    var timeTo = $('#timeTo').val();
                                    $.ajax({
                                        type: "POST",
                                        data: {
                                            dateFrom: dateFrom,
                                            timeFrom: timeFrom,
                                            dateTo: dateTo,
                                            timeTo: timeTo,
                                            employee: employee
                                        },
                                        url: "<?php echo base_url(); ?>/index.php/logintrail/auditLog",
                                        cache: false,
                                        success: function (result) {
                                            result = result.trim();
                                            if (result === 'Empty') {
                                                $("#trailResult").html("<h3 class='font-red text-center'>No Result</h3>");
                                            } else {
                                                var obj = JSON.parse(result);
                                                var tbl = "<table class='table table-bordered table-striped table-condensed table-hover'>" +
                                                        "<thead><tr>" +
                                                        "<th>IP Address</th>" +
                                                        "<th>Date</th>" +
                                                        "<th>Fullname</th>" +
                                                        "<th>Action</th>" +
                                                        "</tr>" +
                                                        " </thead><tbody>";

                                                $.each(obj, function (i, item) {
                                                    tbl += "<tr><td>" + item.ipaddress + "</td><td>" + moment(item.log).format('YYYY-MM-DD  hh:mm:ss A') + "</td><td>" + item.name + "</td><td>" + item.remark + "</td></tr>";
                                                });

                                                tbl += "</tbody></table>";
                                                $("#trailResult").html(tbl);
                                                if (employee == 'All') {
                                                    $('#save_image_locally').prop('disabled', true);
                                                } else {
                                                    $('#save_image_locally').prop('disabled', false);
                                                }

                                            }

                                        }
                                    });
                                });
                            });
                    </script>
                    <script>
                            $(function () {

                                $('#save_image_locally').click(function () {
                                    html2canvas($('#trailResult'),
                                            {
                                                onrendered: function (canvas) {
                                                    var a = document.createElement('a');
                                                    // toDataURL defaults to png, so we need to request a jpeg, then convert for file download.
                                                    a.href = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
                                                    a.download = moment().format('YYYY-MM-DD') + '_' + $("#employeelist option:selected").text() + '-audittrail.png';
                                                    a.click();
                                                }
                                            });
                                });
                            });
                    </script>
                    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

                    </html>