<!DOCTYPE html> 
<html  lang="en">

    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
            body { padding-right: 0 !important }
        </style>


        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> SUPPORTZEBRA | EMPLOYEE - ATM </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/fontawesome/fontawesome.css"> -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">

        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });

        </script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
        <!-- Input switch -->

        <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/input-switch/inputswitch.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/input-switch/inputswitch.js"></script>
        <script type="text/javascript">
                /* Input switch */

                $(function () {
                    "use strict";
                    $('.input-switch').bootstrapSwitch();
                });
        </script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/xeditable/xeditable.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/xeditable/xeditable.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/moment.js"></script>
    </head>
    <style type="text/css">
        input[type='radio']:after {
            width: 15px;
            height: 15px;
            border-radius: 15px;
            top: -2px;
            left: -1px;
            position: relative;
            background-color: #d1d3d1;
            content: '';
            display: inline-block;
            visibility: visible;
            border: 2px solid white;
        }

        input[type='radio']:checked:after {
            width: 15px;
            height: 15px;
            border-radius: 15px;
            top: -2px;
            left: -1px;
            position: relative;
            content: '';
            display: inline-block;
            visibility: visible;
            border: 2px solid white;
        }
        input[type='radio'][class='radio-green']:checked:after {
            background-color: #2f982f;
        }
        input[type='radio'][class='radio-blue']:checked:after {
            background-color: #19a4f9;
        }
        input[type='radio'][class='radio-red']:checked:after {
            background-color: #f73f3f;
        }
    </style>
    <body>
        <div id="sb-site">
            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <div id="page-header" class="bg-black">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>

                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">


                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div id="page-title">
                            <div class="example-box-wrapper">
                                <div class="content-box">
                                    <h3 class="content-box-header bg-black">
                                        <i class="glyph-icon icon-cog"></i>
                                        ACCOUNT LIST
                                    </h3>
                                    <div class="content-box-wrapper">
                                        <table class="table table-condensed table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Full Name</th>
                                                    <th>ATM Status</th>
                                                    <th>ATM Account Number</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($values as $v): ?>
                                                    <tr>
                                                        <td><?php echo $v->lname . ', ' . $v->fname . ' ' . $v->mname; ?></td>
                                                        <td>
                                                            <label class="radio-inline">
                                                                <input type="radio"  class="radio-green" onclick="setActiveness(this)" data-empid="<?php echo $v->emp_id; ?>" name="radio-<?php echo $v->emp_id; ?>" value="1" <?php
                                                                if ($v->isAtm == 1)
                                                                {
                                                                    echo "checked";
                                                                }
                                                                ?>>
                                                                ATM
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio"  class="radio-blue"  onclick="setActiveness(this)" data-empid="<?php echo $v->emp_id; ?>"  name="radio-<?php echo $v->emp_id; ?>" value="2" <?php
                                                                if ($v->isAtm == 2)
                                                                {
                                                                    echo "checked";
                                                                }
                                                                ?>>
                                                                On Hold
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="radio-red"  onclick="setActiveness(this)" data-empid="<?php echo $v->emp_id; ?>" name="radio-<?php echo $v->emp_id; ?>" value="0" <?php
                                                                if ($v->isAtm == 0)
                                                                {
                                                                    echo "checked";
                                                                }
                                                                ?>>
                                                                Non-ATM
                                                            </label>
                                                                <!--                            <input type="checkbox" data-on-color="info" data-off-color="danger" name="checkbox-example-1" class="input-switch" <?php
                                                            if ($v->isAtm == 1)
                                                            {
                                                                echo "checked";
                                                            }
                                                            ?> data-size="small" data-on-text="Yes" data-off-text="No" id='id-<?php echo $v->emp_id; ?>' onchange='setActiveness(<?php echo $v->emp_id; ?>)'>-->
                                                        </td>
                                                        <td><a href="#" class="group" data-type="text" data-pk="1" data-name="username" data-original-title="Enter username" data-empid='<?php echo $v->emp_id; ?>'><?php echo $v->atm_account_number ?></a></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>


    <!-- JS Demo -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
</body>
<script>
                                                                        $(function () {
                                                                            $.ajax({
                                                                                type: "POST",
                                                                                url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
                                                                                cache: false,
                                                                                success: function (html) {

                                                                                    $('#headerLeft').html(html);
                                                                                    //alert(html);
                                                                                }
                                                                            });
                                                                            $.ajax({
                                                                                type: "POST",
                                                                                url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
                                                                                cache: false,
                                                                                success: function (html) {

                                                                                    $('#sidebar-menu').html(html);
                                                                                    // alert(html);
                                                                                }
                                                                            });
                                                                        });
</script>
<script type="text/javascript">
        function setActiveness(that) {
            var emp_id = $(that).data('empid');
            var value = $(that).val();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/PositionRates/updateATM",
                data: {
                    emp_id: emp_id,
                    activevalue: value
                },
                cache: false,
                success: function (res) {
                    res = res.trim();
                    if (res === 'Success') {
                        $.jGrowl("Successfully Updated.", {sticky: !1, position: "top-right", theme: "bg-blue-alt"});
                    } else {
                        $.jGrowl("Failed Updated.", {sticky: !1, position: "top-right", theme: "bg-red"});
                    }
                }
            });
        }
</script>
<script>
        $(function () {
            $('.group').editable({
                mode: "inline"

            }).on("save", function (e, params) {
                var value = params.newValue;
                  $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/PositionRates/updateATMdetails",
                        data: {
                            value: params.newValue,
                            emp_id: $(this).data('empid')
                        },
                        cache: false,
                        success: function (result) {
                            if (result === 'Success') {
                                $.jGrowl("Successfully Updated.", {sticky: !1, position: "top-right", theme: "bg-blue-alt"});

                            } else {
                                $.jGrowl("Failed Updated.", {sticky: !1, position: "top-right", theme: "bg-red"});

                            }
                        }
                    });

            });


        });
</script>

</html>