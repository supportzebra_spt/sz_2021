<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}

</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript">
  $(window).load(function(){
   setTimeout(function() {
    $('#loading').fadeOut( 400, "linear" );
  }, 300);
   $("#tab1").trigger("click");
 });

</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
 $(function(){
  "use strict";
  var m = moment().format("MM/DD/YYYY");
  var end = moment().add(1,'year').format("MM/DD/YYYY");
  $("#daterange").daterangepicker({minDate: m,startDate: m,maxDate : end});   
  $("[name=daterangepicker_start]").attr('readonly',true);
  $("[name=daterangepicker_end]").attr('readonly',true);

});
</script>

<!-- Textarea -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/textarea/textarea.js"></script>
<script type="text/javascript">
  /* Textarea autoresize */

  $(function() { "use strict";
    $('.textarea-autosize').autosize();
  });
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/tooltip/tooltip.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
  });
</script>
<!-- jQueryUI Tabs -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/tabs-ui/tabs.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs-ui/tabs.js"></script>
<script type="text/javascript">
  /* jQuery UI Tabs */

  $(function() { "use strict";
    $(".tabs").tabs();
  });

  $(function() { "use strict";
    $(".tabs-hover").tabs({
      event: "mouseover"
    });
  });
</script>

<!-- Boostrap Tabs -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs/tabs.js"></script>

<!-- Tabdrop Responsive -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs/tabs-responsive.js"></script>
<script type="text/javascript">
  /* Responsive tabs */
  $(function() { "use strict";
    $('.nav-responsive').tabdrop();
  });
</script>

</head>

<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">
        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div id="page-title">
          <h2>Personal Request</h2>
          <hr>
          <a href="<?php echo base_url();?>index.php/Request/view_leave_main" class='btn btn-sm btn-warning' title="View List Request">
            <i class='glyph-icon icon-th-list'></i> Request List
          </a>

          <!-- <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#leavemodal"><i class='glyph-icon icon-plus'></i></button> -->

          <button type="button" class="btn btn-info btn-sm" id='collapser'>
            <i class='glyph-icon icon-plus'></i>
          </button>
          <br>
          <div  style='display:none' id='requestform'>
            <br>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title font-size-13 mrg5B mrg5T">REQUEST LEAVE</h3>
              </div>
              <div class="panel-body">
               <div class="row"> 
                 <div class="col-md-6">
                   <h4 class='font-bold'>LEAVE REQUEST FORM</h4>
                 </div>
                 <div class="col-md-6">
                   <h4 class='font-bold text-right'> <?php echo date('Y')?> AVAILABLE CREDITS: <span class='font-size-18 font-italic font-orange' id='creditsavailable'>--</span></h4>
                 </div><br>
                 <hr>
                 <div class="col-md-5">
                  <div class="form-group">

                    <label class="control-label">Leave Type <span id='leavetypen' class='font-red font-size-10' hidden> Required *</span></label>

                    <select class="form-control" id='leavetype'>
                      <option value=''>--</option>
                      <?php foreach($leaves as $l):?>
                        <option value='<?php echo $l->leave_id;?>'><?php echo $l->leave_name;?></option>
                      <?php endforeach;?>
                    </select>

                  </div>

                  <label class='control-label'>Date Range  <span id='daterangen' class='font-red font-size-10' hidden> Required *</span></label>
                  <div class="input-prepend input-group">
                    <span class="add-on input-group-addon">
                      <i class="glyph-icon icon-calendar"></i>
                    </span>
                    <input type="text" name="daterange" id="daterange" class="form-control" disabled>
                  </div>  

                </div>
                <div class="col-md-7">

                  <div class="form-group">
                    <label class="control-label">Reason  <span id='reasonn' class='font-red font-size-10' hidden> Required *</span></label>
                    <textarea name="" id="reason" class="form-control textarea-no-resize"  style='height:108px'></textarea>
                  </div>
                </div>

                <div class="col-md-6">
                  <h5 class="font-bold font-italic"><span class="font-blue" id='paid'>0</span> PAID  <small>(<?php echo date('Y')?>)</small></h5>
                  <h5 class="font-bold font-italic"><span class="font-red" id='unpaid'>0</span> UNPAID  <small>(<?php echo date('Y')?>)</small></h5>
                </div>
                <div class="col-md-6 text-right">
                  <button class="btn btn-info" id="submitleave" >Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="example-box-wrapper">
          <div class="content-box tabs">
            <h3 class="content-box-header bg-primary">
             <span class="header-wrapper">
              Personal List
            </span>
            <ul>
              <li>
                <a href="#tabs-example-1" title="Pending" id='tab1'>
                  Pending
                </a>
              </li>
              <li>
                <a href="#tabs-example-2" title="Accepted" id='tab2'>
                  Accepted
                </a>
              </li>
              <li>
                <a href="#tabs-example-3" title="Rejected" id='tab3'>
                  Rejected
                </a>
              </li>
            </ul>
          </h3>
          <div id="tabs-example-1">
           <table class="table table-condensed table-striped table-bordered">
            <thead>
              <tr>
                <th>Supervisor</th>
                <th>Type</th>
                <th>Details</th>
                <th>Date Requested</th>
                <th>Coverage</th>
                <th>Status</th>   
                <th>Action</th>  
              </tr>
            </thead>
            <tbody id='pendingtbody'>

            </tbody>
          </table>
        </div>
        <div id="tabs-example-2">
          <table class="table table-condensed table-striped table-bordered">
            <thead>
              <tr>
                <th>Supervisor</th>
                <th>Type</th>
                <th>Details</th>
                <th>Date Requested</th>
                <th>Coverage</th>
                <th>Status</th>   
                <th>Action</th> 
              </tr>
            </thead>
            <tbody id='acceptedtbody'>

            </tbody>
          </table>
        </div>
        <div id="tabs-example-3">
          <table class="table table-condensed table-striped table-bordered">
            <thead>
              <tr>
                <th>Supervisor</th>
                <th>Type</th>
                <th>Details</th>
                <th>Date Requested</th>
                <th>Coverage</th>
                <th>Status</th>   
                <th>Action</th>   
              </tr>
            </thead>
            <tbody id='declinedtbody'>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div id="tab1viewmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
           <img alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';"    class='img-circle' style='border:2px solid #ddd;width:100%' id='mimage'/>
           <hr style='margin:10px !important'>
           <h4 class='font-size-16 font-bold text-center' id='mfullname'></h4>
         </div>
         <div class="col-md-8">
           <br>
           <h5 class='mrg5B'>DATE COVERED</h5>
           <h5 class="font-bold" id='mdatecovered'></h5>
           <br>
           <h5 class='mrg5B'>TYPE OF REQUEST</h5>
           <h5 class="font-bold" id='mtype'></h5>
           <br>
           <h5 class='mrg5B'>REQUEST DETAILS</h5>
           <h5 class="font-bold" id='mdetails'></h5>
           <br>
           <h5 class='mrg5B'>STATUS</h5>
           <h5 class="font-bold"  id='mstatus'></h5>
         </div>
         <div class="col-md-12">
           <hr>
           <div class="pad10A">

             <h5 class='mrg5B'>REASON</h5>
             <h5 class="font-bold text-justify"  id='mreason'></h5>
           </div>
         </div>
       </div>

     </div>
     <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
<div id="tab2viewmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
       <div class="row">
        <div class="col-md-4">
         <img alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';"    class='img-circle' style='border:2px solid #ddd;width:100%' id='m2image'/>
         <hr style='margin:10px !important'>
         <h4 class='font-size-16 font-bold text-center' id='m2fullname'></h4>
       </div>
       <div class="col-md-8">
         <br>
         <h5 class='mrg5B'>DATE COVERED</h5>
         <h5 class="font-bold" id='m2datecovered'></h5>
         <br>
         <h5 class='mrg5B'>TYPE OF REQUEST</h5>
         <h5 class="font-bold" id='m2type'></h5>
         <br>
         <h5 class='mrg5B'>REQUEST DETAILS</h5>
         <h5 class="font-bold" id='m2details'></h5>
         <br>
         <h5 class='mrg5B'>STATUS</h5>
         <h5 class="font-bold"  id='m2status'></h5>
       </div>
       <div class="col-md-12">
         <hr>
         <div class="pad10A">

           <h5 class='mrg5B'>REASON</h5>
           <h5 class="font-bold text-justify"  id='m2reason'></h5>
         </div>
       </div>
     </div>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>   
  </div>
</div>
</div>
</div>
<div id="tab3viewmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
       <div class="row">
        <div class="col-md-4">
         <img alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';"    class='img-circle' style='border:2px solid #ddd;width:100%' id='m3image'/>
         <hr style='margin:10px !important'>
         <h4 class='font-size-16 font-bold text-center' id='m3fullname'></h4>
       </div>
       <div class="col-md-8">
         <br>
         <h5 class='mrg5B'>DATE COVERED</h5>
         <h5 class="font-bold" id='m3datecovered'></h5>
         <br>
         <h5 class='mrg5B'>TYPE OF REQUEST</h5>
         <h5 class="font-bold" id='m3type'></h5>
         <br>
         <h5 class='mrg5B'>REQUEST DETAILS</h5>
         <h5 class="font-bold" id='m3details'></h5>
         <br>
         <h5 class='mrg5B'>STATUS</h5>
         <h5 class="font-bold"  id='m3status'></h5>
       </div>
       <div class="col-md-12">
         <hr>
         <div class="pad10A">

           <h5 class='mrg5B'>REASON</h5>
           <h5 class="font-bold text-justify"  id='m3reason'></h5>
         </div>
         <hr>
         <div class="pad10A">

           <h5 class='mrg5B'>REJECT REASON</h5>
           <h5 class="font-bold text-justify"  id='m3reasonreject'></h5>
         </div>
       </div>
     </div>
   </div>
   <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">

 var passvalue = function(token){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>/index.php/Request/getRequestDetails",
    data: {
      token:token
    },
    cache: false,
    success: function(res)
    {
     res = res.trim();
     var result = JSON.parse(res);

     var s = moment(result.start_date).format("MMMM D, YYYY");
     var e = moment(result.end_date).format("MMMM D, YYYY");
     $("#mimage").attr('src','<?php echo base_url()?>assets/images/'+result.pic);
     $("#mfullname").html(result.lname+", "+result.fname);
     $("#mdetails").html(result.leave_name);
     $("#mtype").html(result.type);
     if(s==e){
      $("#mdatecovered").html(s);
    }else{    
      $("#mdatecovered").html(s + ' to ' +e);
    }
    $("#mstatus").html(result.status);
    $("#mreason").html(result.reason);
    $("#btn-reject").attr('data-token',result.token);
  }
});
};


</script>
<script type="text/javascript">
 var passvalue2 = function(token){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>/index.php/Request/getRequestDetails",
    data: {
      token:token
    },
    cache: false,
    success: function(res)
    {
      res = res.trim();
      var result = JSON.parse(res);

      var s = moment(result.start_date).format("MMMM D, YYYY");
      var e = moment(result.end_date).format("MMMM D, YYYY");
      $("#m2image").attr('src','<?php echo base_url()?>assets/images/'+result.pic);
      $("#m2fullname").html(result.lname+", "+result.fname);
      $("#m2details").html(result.leave_name);
      $("#m2type").html(result.type); 
      if(s==e){
        $("#m2datecovered").html(s);
      }else{    
        $("#m2datecovered").html(s + ' to ' +e);
      }
      $("#m2status").html(result.status);
      $("#m2reason").html(result.reason);
   }
 });
};

</script>
<script type="text/javascript">
 var passvalue3 = function(token){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>/index.php/Request/getRequestDetails",
    data: {
      token:token
    },
    cache: false,
    success: function(res)
    {
     res = res.trim();
     var result = JSON.parse(res);

     var s = moment(result.start_date).format("MMMM D, YYYY");
     var e = moment(result.end_date).format("MMMM D, YYYY");
     $("#m3image").attr('src','<?php echo base_url()?>assets/images/'+result.pic);
     $("#m3fullname").html(result.lname+", "+result.fname);
     $("#m3details").html(result.leave_name);
     $("#m3type").html(result.type);
     if(s==e){
      $("#m3datecovered").html(s);
    }else{    
      $("#m3datecovered").html(s + ' to ' +e);
    }
    $("#m3status").html(result.status);
    $("#m3reason").html(result.reason);
    $("#m3reasonreject").html(result.reason_reject);
  }
});
};
</script>
<script type="text/javascript">
  $("#requestaccepted").click(function(){
    var token = $("#btn-reject").attr('data-token');
    var reason_reject = $("#reason_reject").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/leaverequestAccepted",
      data: {
        token:token,
        reason_reject:reason_reject
      },
      cache: false,
      success: function(res)
      {
       if(jQuery.trim(res)=='Successful'){

        $.jGrowl("Successfully Accepted Request.",{sticky:!1,position:"top-right",theme:"bg-green"});
        $("#tab1").trigger("click");
      }else{

        $.jGrowl("Failed processing.",{sticky:!1,position:"top-right",theme:"bg-red"});
      }
    }
  });
  });
</script>

<script type="text/javascript">
  $("#requestrejected").click(function(){
    var token = $("#btn-reject").attr('data-token');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/leaverequestRejected",
      data: {
        token:token
      },
      cache: false,
      success: function(res)
      {
        // alert(jQuery.trim(res));
        if(jQuery.trim(res)=='Successful'){

          $.jGrowl("Successfully Rejected Request.",{sticky:!1,position:"top-right",theme:"bg-green"});
        }else{

          $.jGrowl("Failed processing.",{sticky:!1,position:"top-right",theme:"bg-red"});
        }
      }
    });
  });
</script>
<script type="text/javascript">
  $("#tab1").click(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/getLeaveMyRequests2",
      data: {
        status:'Pending'
      },
      cache: false,
      success: function(res)
      {

        res = res.trim();
        $("#pendingtbody").html("");
        if(res=='Empty'){
          $("#pendingtbody").append("<tr><td colspan=7 class='text-center font-bold'>No Pending Request.</td></tr>");
        }else{

         $.each(JSON.parse(res),function (i,elem){
          var tbody = "";
          tbody += "<tr>";
          tbody += "<td>"+elem.lname+", "+elem.fname+"</td>";
          tbody += "<td>"+elem.type+"</td>";
          tbody += "<td>"+elem.leave_name+"</td>";
          tbody += "<td>"+elem.requested_on+"</td>";
          if(elem.start_date==elem.end_date){
            tbody += "<td>"+elem.start_date+"</td>";
          }else{
            tbody += "<td>"+elem.start_date+" - "+elem.end_date+"</td>";
          }
          tbody += "<td>"+elem.status+"</td>";
          tbody += "<td><button class='btn btn-primary btn-xs' data-toggle='modal' data-target='#tab1viewmodal' onclick='passvalue("+elem.token+")'>View</button></td>";
          tbody += "</tr>";
          $("#pendingtbody").append(tbody);
        });
       }
     }
   });
  });
</script>
<script type="text/javascript">
  $("#tab2").click(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/getLeaveMyRequests2",
      data: {
        status:'Accepted'
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        $("#acceptedtbody").html("");
        if(res=='Empty'){
          $("#acceptedtbody").append("<tr><td colspan=7 class='text-center font-bold'>No Accepted Request.</td></tr>");
        }else{

         $.each(JSON.parse(res),function (i,elem){
          var tbody = "";
          tbody += "<tr>";
          tbody += "<td>"+elem.lname+", "+elem.fname+"</td>";
          tbody += "<td>"+elem.type+"</td>";
          tbody += "<td>"+elem.leave_name+"</td>";
          tbody += "<td>"+elem.requested_on+"</td>";          
          if(elem.start_date==elem.end_date){
            tbody += "<td>"+elem.start_date+"</td>";
          }else{
            tbody += "<td>"+elem.start_date+" - "+elem.end_date+"</td>";
          }
          tbody += "<td>"+elem.status+"</td>";
          tbody += "<td><button class='btn btn-primary btn-xs' data-toggle='modal' data-target='#tab2viewmodal' onclick='passvalue2("+elem.token+")'>View</button></td>";
          tbody += "</tr>";
          $("#acceptedtbody").append(tbody);
        });
       }
     }
   });
  });
</script>
<script type="text/javascript">
  $("#tab3").click(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/getLeaveMyRequests2",
      data: {
        status:'Rejected'
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        $("#declinedtbody").html("");
        if(res=='Empty'){
          $("#declinedtbody").append("<tr><td colspan=7 class='text-center font-bold'>No Rejected Request.</td></tr>");
        }else{

         $.each(JSON.parse(res),function (i,elem){
          var tbody = "";
          tbody += "<tr>";
          tbody += "<td>"+elem.lname+", "+elem.fname+"</td>";
          tbody += "<td>"+elem.type+"</td>";
          tbody += "<td>"+elem.leave_name+"</td>";
          tbody += "<td>"+elem.requested_on+"</td>";
          if(elem.start_date==elem.end_date){
            tbody += "<td>"+elem.start_date+"</td>";
          }else{
            tbody += "<td>"+elem.start_date+" - "+elem.end_date+"</td>";
          }
          tbody += "<td>"+elem.status+"</td>";
          tbody += "<td><button class='btn btn-primary btn-xs' data-toggle='modal' data-target='#tab3viewmodal' onclick='passvalue3("+elem.token+")'>View</button></td>";
          tbody += "</tr>";
          $("#declinedtbody").append(tbody);
        });
       }
     }
   });
  });
</script>
<script type="text/javascript">
  $("#submitleave").click(function(){
    var leavetype = $("#leavetype").val();
    var date = $("#daterange").val();
    var reason = $("#reason").val();
    var paidleaves = $(this).data('paidleaves');
    var unpaidleaves = $(this).data('unpaidleaves');
    if(leavetype==''){
      $("#leavetypen").show();
    }else{
      $("#leavetypen").hide();
    }
    if(date==''){
      $("#daterangen").show();
    }else{
      $("#daterangen").hide();
    }
    if(reason==''){
      $("#reasonn").show();
    }else{
      $("#reasonn").hide();
    }

    if(leavetype!=''&&date!=''&&reason!=''){

      var startdate =  $("#daterange").data('daterangepicker').startDate.format('YYYY-MM-DD');
      var enddate =  $("#daterange").data('daterangepicker').endDate.format('YYYY-MM-DD');
      // alert(startdate+" - "+enddate);
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Request/checkleavedaterange",
        data: {
          startdate:startdate,
          enddate:enddate
        },
        cache: false,
        success: function(res)
        {
          var Okay = false;
          console.log(res.trim());
          if(res.trim()=='Empty'){
            Okay = true;
            // $.jGrowl("No Schedule Set",{sticky:!1,position:"top-right",theme:"bg-info"});
          }else if(res.trim()=='Exists'){
            $.jGrowl("Request for chosen date Exists.",{sticky:!1,position:"top-right",theme:"bg-warning"});
            $("#daterange").val('');
          }else{
            var result = JSON.parse(res);
            var types = "";
            result.forEach(function(item){
              if(item.type!='Normal'){
                types += item.type+" ";
              }
            });
            if(types!=""){
             $.jGrowl("Schedules in the chosen dates contains <br><b> "+types+"</b>. <br> Please change dates.",{sticky:!1,position:"top-right",theme:"bg-red"}); 
             $("#daterange").val('');
           }else{
            Okay = true;
          }
        }
        console.log(Okay);
        if(Okay==true){
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Request/sendrequestleave",
            data: {
              startdate:startdate,
              enddate:enddate,
              leavetype:leavetype,
              reason:reason,
              requesttype:'Leave Request',
              paidleaves:paidleaves,
              unpaidleaves:unpaidleaves
            },
            cache: false,
            success: function(res)
            {
              if(res=='Failed'){
                $.jGrowl("Error occurred while processing request. <br> Please try again.",{sticky:!1,position:"top-right",theme:"bg-red"});
              }else if(res.trim()=='Exists'){
                $.jGrowl("Request for chosen date Exists.",{sticky:!1,position:"top-right",theme:"bg-warning"});
              }
              else{
               $.jGrowl("Successfully Requested to Supervisor.",{sticky:!1,position:"top-right",theme:"bg-green"});
               $("#tab1").trigger("click");
               $("#requestform").slideToggle('slow');
               $("#leavetype").val('');
               $("#daterange").val('');
               $("#reason").val('');
             }
           }
         });
        }

      }
    });
    }
  });
</script>
<script type="text/javascript">
  $("#leavetype").on('change',function(){
   $('#daterange').val('');
   $('#paid').html(0);
   $('#unpaid').html(0);
   $('#daterange').daterangepicker('destroy');
   var leavetype = $(this).val();
   if(leavetype==''){
    $("#creditsavailable").html("--");
    $("#daterange").prop('disabled',true);
  }else{
      //---------------------------------------------------------------------------------------------
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Request/getRemainingCredits",
        data:{
          leavetype:$(this).val()
        },
        cache: false,
        success: function(res)
        {
          res = res.trim();
          if(res=='Not Set'){
            $("#creditsavailable").html(res.trim());
            $("#daterange").prop('disabled',true);
          }else{
            $("#daterange").prop('disabled',false);
            $("#creditsavailable").html(res.trim());
          }
        }
      });

    //---------------------------------------------------------------------------------------------
  }
  if(leavetype==1){

   m = moment().add(3,'weeks').format("MM/DD/YYYY");
   end = moment().add(1,'year').format("MM/DD/YYYY");
   $("#daterange").daterangepicker({minDate: m,startDate: m,endDate: m,maxDate : end});
 }else if(leavetype==2||leavetype==7||leavetype==5){
   m = moment().subtract(2,'months').format("MM/DD/YYYY");
   end = moment().add(2,'months').format("MM/DD/YYYY");
   $("#daterange").daterangepicker({minDate: m,startDate: m,endDate: m,maxDate : end});
 }else if(leavetype==6){

   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Request/getBirthdate",
    cache: false,
    success: function(res)
    {
      var bday = $.trim(res);
      if(bday=='Empty'){

       m = moment().format("MM/DD/YYYY");
       end = moment().add(1,'year').format("MM/DD/YYYY");
       $.jGrowl("No Birth Date Set.",{sticky:!1,position:"top-right",theme:"bg-warning"});
     }else{
      var year = moment().format('YYYY');
      var monthday = moment(bday).format('MM/DD'); 
      var birthdate = monthday+"/"+year;
      m = moment(birthdate).subtract(1,'week').format("MM/DD/YYYY");
      end = moment(birthdate).add(1,'week').format("MM/DD/YYYY");
    }
  }
});

   $("#daterange").daterangepicker({minDate: m,startDate: m,endDate: m,maxDate : end});
 }else if(leavetype==3||leavetype==4){
   m = moment().subtract(3,'months').format("MM/DD/YYYY");
   end = moment().add(1,'year').format("MM/DD/YYYY");
   $("#daterange").daterangepicker({minDate: m,startDate: m,endDate: m,maxDate : end});
 }else{

  m = moment().format("MM/DD/YYYY");
  end = moment().add(1,'year').format("MM/DD/YYYY");
  $("#daterange").daterangepicker({minDate: m,startDate: m,endDate: m,maxDate : end});
}
$("[name=daterangepicker_start]").attr('readonly',true);
$("[name=daterangepicker_end]").attr('readonly',true);
$("#daterange").attr('readonly',true);


//--------------------------------------------------------------------------------------

$('#daterange').on('apply.daterangepicker', function(ev, picker) {
  var credit = $("#creditsavailable").html();
  // alert(credit);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Request/getPaidUnpaidLeaves",
    data:{
      creditsavailable: credit,
      startdate : picker.startDate.format('YYYY-MM-DD'),
      enddate : picker.endDate.format('YYYY-MM-DD')
    },
    cache: false,
    success: function(res)
    {
      // alert(res);
      res = res.trim();
      res = JSON.parse(res);
      $("#submitleave").attr('data-paidleaves',res.paid);
      $("#submitleave").attr('data-unpaidleaves',res.unpaid);
      $("#paid").html(res.paid);
      $("#unpaid").html(res.unpaid);
    }
  })
});
});

</script>
<script type="text/javascript">
  $("#collapser").click(function(){
    $("i",this).toggleClass("icon-plus icon-minus");
    $("#requestform").slideToggle('slow');
  });
</script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>