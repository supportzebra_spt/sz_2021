<!DOCTYPE html> 

<html  lang="en">
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  .two-line-ellipsis{
    display: inline-block;
    border: 0px;
    height: 30.5px;
    overflow: hidden;
    margin-bottom:6px;
    color:#777;
  }
  .one-line{
    line-height: 8px;
    height: 16px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    /*width: 80%;*/
    text-align:center !important;
  }

  a:hover,a:visited{
    text-decoration:none !important;
  }
  .style_prevu_kit:hover
  {
    box-shadow: 5px 5px 20px #888;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.1);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
  }
  ul,li{ 
    list-style:none;
    padding-left:0;
  }

</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





<script type="text/javascript">
  $(window).load(function(){
   setTimeout(function() {
    $('#loading').fadeOut( 400, "linear" );
  }, 300);
 });
</script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd-yyyy'
    });
  });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>

<!-- TABLES -->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
<script type="text/javascript">

  /* Datatables responsive */

  $(document).ready(function() {
    $('#datatable-responsive').DataTable( {
      responsive: true
    } );
  } );

  $(document).ready(function() {
    $('.dataTables_filter input').attr("placeholder", "Search..."); 

    $(".growl3").select(function(){
      $.jGrowl(
        "Security Code Mismatch.",
        {sticky:!1,position:"top-right",theme:"bg-red"}
        )
    });
    $(".growl4").select(function(){
      $.jGrowl(
        "Restricted Area.",
        {sticky:!1,position:"top-right",theme:"bg-red"}
        )
    });
    $(".growl5").select(function(){
      $.jGrowl(
        "No Data Found.",
        {sticky:!1,position:"top-right",theme:"bg-red"}
        )
    });
  });

  
</script>
<!-- TABLES -->


<script type="text/javascript" src="<?php echo base_url();?>assets/js/list.min.js"></script>
</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="<?php echo $setting['settings']; ?>">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>



    </div>
    <div id="page-sidebar">
     <div class="scroll-sidebar">


      <ul id="sidebar-menu">
      </ul>
    </div>
  </div>
  <div id="page-content-wrapper">
    <div id="page-content">

      <div class="container" >
        <h3>EMPLOYEES</h3>
        <hr>
        <div class="container-fluid" id="test-list">
         <div class="row">
           <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <select class='form-control' id='classes'>
             <option value='' <?php if($class==NULL){echo "selected";}?>>All</option>
             <option value='Agents' <?php if($class=='Agents'){echo "selected";}?>>Agents</option>
             <option value='Admin' <?php if($class=='Admin'){echo "selected";}?>>Admin</option>
           </select>
         </div>
         <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search..." class='form-control search'>
        </div>
      </div>
      <ul class="row list">
        <?php foreach($data as $key => $val){?>

        <li class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center style_prevu_kit" style='background:#fff;padding:0 !important;width:23%;margin:1%;border:1px solid #F3F3F4' data-fullname='<?php echo $val['lname'].", ".$val['fname'] .' '.$val['pos_details'] .' '.$val['status']; ?>'>
		
          <br>
          <a href="#" style="display: block;" data-empid="<?php echo $val['emp_id']; ?>" data-toggle="modal" data-target="#preview" >
            <div style='padding:1%'>
			
              <img src="<?php echo base_url()."assets/images/".strtolower($val['pic']); ?>"  alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';"  height="75"  class='img-circle' style='border:2px solid #ddd' />
              <br><br>
              <h5 class="name one-line" style='margin-bottom:9px;font-size:14px;color:#444;padding-top:2px'><b><?php echo $val['lname'].", ".$val['fname']; ?></b></h5>
			 
              <p  style='font-size:11px;color:#444' class='one-line'><?php echo $val['pos_details']; ?></p>
              <p  style='font-size:11px;white-space: nowrap;color:#444'>(<?php echo $val['status']; ?>)</p><br>
              <p style='font-size:12px;white-space: nowrap;color:#444'><b>Support Zebra</b></p>
              <p style='font-size:11px;' class='two-line-ellipsis'>&nbsp;<?php if($val['address']==''){echo '<span class="text-danger">No Address Set</span>';}else{$add = explode('|',$val['address']);echo $add[0];};?></p>

              <p style='font-size:11px;color:#444' class='one-line'><span class='icon-typicons-mail'></span> <?php if($val['email']==''){echo '<span class="text-danger">No Email Set</span>';}else{echo $val['email'];};?></p>
              <p style='font-size:11px;white-space: nowrap;color:#444'><span class='icon-typicons-phone-outline'></span>
                <?php if($val['cell']==''){echo '<span class="text-danger">No Contact Set</span>';}else{echo $val['cell'];};?></p>
              </div>

            </a>
            <hr style='width:100%;margin:12px 0'>
            <div class="btn-group" style='margin-bottom:10px'>
              <button class="btn btn-default btn-xs"><span class='icon-typicons-chat'></span> Chat</button>
              <button class="btn btn-default btn-xs"><span class='icon-typicons-user'></span> User</button>
            </div>
          </li>

          <?php } ?>
        </ul>
        <div class="nav row text-center">
          <div class="col-md-3">
           <ul class="pager">
            <li><a href="#" id='btn-first'>First</a></li>
            <li><a href="#" id='btn-prev'>Previous</a></li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul class="pagination" style='margin:0 !important'></ul>
        </div>
        <div class="col-md-3">
          <ul class="pager">
            <li><a href="#" id='btn-next'>Next</a></li>
            <li><a href="#" id='btn-last'>Last</a></li>
          </ul>
        </div>

      </div>
    </div>
  </div>

  <div class="row" hidden>
    <div class="col-md-8">
      <div class="panel">
        <div class="panel-body">
          <h3 class="title-hero">
            Recent sales activity
          </h3>
          <div class="example-box-wrapper">
            <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
          </div>
        </div>
      </div>




      <div class="content-box">
        <h3 class="content-box-header bg-default">
          <i class="glyph-icon icon-cog"></i>
          Live server status
          <span class="header-buttons-separator">
            <a href="#" class="icon-separator">
              <i class="glyph-icon icon-question"></i>
            </a>
            <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
              <i class="glyph-icon icon-refresh"></i>
            </a>
            <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
              <i class="glyph-icon icon-times"></i>
            </a>
          </span>
        </h3>
        <div class="content-box-wrapper">
          <div id="data-example-3" style="width: 100%; height: 250px;"></div>
        </div>
      </div>

    </div>

  </div>
</div>
</div>
</div>
</div>

<div id="preview" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:50%">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="color: white;background-color:rgba(0, 0, 0, 0.8);">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="row">
          <div class="col-md-3 text-center" style='padding:10px 0'>
            <img id="previewpic" alt="Image not found" onerror="this.src='<?php echo base_url('assets/images/sz.png'); ?>';" style='width:70%;border: 2px solid #777;background:white;' class='img-circle' />
          </div>
          <div class="col-md-9">
            <h3 style="margin-bottom:10px;"><b id="previewname"></b></h3>
            <h5 style="margin-bottom:5px;" id="previewposition"></h5>
            <h5 style="margin-bottom:5px;" id="previewstatus"></h5>
            <h5 style="margin-bottom:5px;" id="previewid"></h5>
          </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="panel">
          <div class="panel-heading font-blue font-bold">PERSONAL INFORMATION</div>
          <div class="panel-body">

           <!--  <div class="row" style="margin-bottom:6px">
              <div class="col-md-3"><b>Full Name:</b> </div>
              <div class="col-md-9" id="previewname2"></div>
            </div> -->
            <div class="row">
              <div class="col-md-6">
                <div class="row" style="margin-bottom:6px">
                  <div class="col-md-4"><b>Gender:</b>  </div>
                  <div class="col-md-8" id="previewgender"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row" style="margin-bottom:6px">
                  <div class="col-md-4"><b>Contact #:</b> </div>
                  <div class="col-md-8" id="previewcontact"></div>
                </div>
                
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="row" style="margin-bottom:6px">
                  <div class="col-md-4"><b>Birthday:</b> </div>
                  <div class="col-md-8" id="previewbirthday"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row" style="margin-bottom:6px">
                  <div class="col-md-4"><b>Age:</b> </div>
                  <div class="col-md-8" id="previewage"></div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="row" style="margin-bottom:6px">
                  <div class="col-md-4"><b>Religion:</b> </div>
                  <div class="col-md-8" id="previewreligion"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row" style="margin-bottom:6px">
                  <div class="col-md-4"><b>Blood Type:</b> </div>
                  <div class="col-md-8" id="previewblood"></div>
                </div> 
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div style="margin-bottom:6px">
                  <div><b>Address:</b> </div>
                  <div id="previewaddress"></div>
                </div>
              </div>
              <div class="col-md-6">
               <div class="row" style="margin-bottom:6px">
                <div class="col-md-4"><b>Supervisor:</b> </div>
                <div class="col-md-8" id="previewesupervisor"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
       <div class="col-md-7">
        <div class="panel" style='padding-bottom:0;margin-bottom:0'>
          <div class="panel-heading font-red font-bold">INCASE OF EMERGENCY, PLEASE NOTIFY</div>
          <div class="panel-body">
            <div class="row" style="margin-bottom:7px">
              <div class="col-md-4"><b>Name:</b> </div>
              <div class="col-md-8" id="previewemergencyname"></div>
            </div>

            <div class="row" style="margin-bottom:10px">
              <div class="col-md-4"><b>Contact No:</b> </div>
              <div class="col-md-8" id="previewemergencycontact"></div>
            </div>

          </div>
        </div>
      </div>
      <div class="col-md-5">
        <div class="panel" style='padding-bottom:0;margin-bottom:0'>
          <div class="panel-heading font-bold text-primary">DTR DETAILS</div>
          <div class="panel-body">
            <div id="securitydiv">
              <label class="control-label">Enter Code: <span class="text-danger" style="font-size:10px;" id='cnotif' hidden>Required.*</span></label>


              <div class="row">
                <div class="col-md-12">
                  <div class="input-group">
                    <input type="password" id="Securitycode" name="" class="form-control input-sm">
                    <div class="input-group-btn">
                      <a href="#" class="btn btn-sm btn-primary" id="submitcode">
                        <span class="glyph-icon icon-separator">
                          <i class="glyph-icon icon-eye"></i>
                        </span>
                        <span class="button-content">
                          View
                        </span>
                      </a>
                    </div>
                  </div>
                </div>

              </div>



            </div>
            <div id="dtrdiv" style="display:none;">
              <div class="row" style="margin-bottom:6px">
                <div class="col-md-4"><b>Username:</b> </div>
                <div class="col-md-8" id="previewusername"></div>
              </div>
              <div class="row" style="margin-bottom:6px">
                <div class="col-md-4"><b>Password:</b> </div>
                <div class="col-md-8" id="previewpassword"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#" data-dismiss='modal' class="btn btn-default">
      <span class="glyph-icon icon-separator">
        <i class="glyph-icon icon-times"></i>
      </span>
      <span class="button-content">
        Cancel
      </span>
    </a>
    <a href="#" class="btn btn-sm btn-primary"  id="previewempid" onclick="$('#previewempid').data('empid',$(this).attr('data-empid'))">
      <span class="glyph-icon icon-separator">
        <i class="glyph-icon icon-edit"></i>
      </span>
      <span class="button-content">
        Update
      </span>
    </a>
  </div>
</div>
</div>
</div>

<form method="POST" action="<?php echo base_url(); ?>index.php/employee/updateEmployee" id='employeeform'>
  <input type='hidden' name='emp_id' id='employeeid'>
</form>

<p class="growl3" hidden></p>
<p class="growl4" hidden></p>
<p class="growl5" hidden></p>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Templatedesign/headerLeft",
    cache: false,
    success: function(html)
    {

      $('#headerLeft').html(html);
		   //alert(html);
    }
  });	
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Templatedesign/sideBar",
    cache: false,
    success: function(html)
    {

      $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
 });	
</script>
<script type="text/javascript">
  $("#submitcode").click(function(){
    var code = $("#Securitycode").val();
    if(code==''){
      $("#cnotif").show();
    }else{
      $("#cnotif").hide();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Employee/matchUserCodeWithReturn",
        data: {
          password:code
        },
        cache: false,
        success: function(result)
        {
          if(result=='Empty'){
            $(".growl3").trigger('select');
          }else{
            var res = JSON.parse(result);
            if(res.description=='Administrator'){
              $("#dtrdiv").show();
              $("#securitydiv").hide();
            }else{
              $(".growl4").trigger('select');
            }
          }
        }
      })
    };

  })
</script>

<script type="text/javascript">
  $("#preview").on('show.bs.modal', function(e) {
    $("#dtrdiv").hide();
    $("#securitydiv").show();
    $("#Securitycode").val('');
    var empid = $(e.relatedTarget).data('empid');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Employee/getEmployeePreview",
      data: {
        emp_id:empid
      },
      cache: false,
      success: function(result)
      {
        // alert(result);
        if(result==''||result=="null"){
          // alert("EMPTY");
          $(".growl5").trigger('select');
        }else{
          var res = JSON.parse(result);
          $("#previewname").html(res.lname +", "+ res.fname+" "+res.mname);
          $("#previewposition").html("<b>Position :</b> "+res.pos_name +" - "+res.pos_details);
          $("#previewstatus").html("<b>Status :</b> "+res.status);
          if(res.id_num==null||res.id_num==''){
            $("#previewid").html("<b>ID No. </b><span class='text-danger'>Not Set</span>");
          }else{
           $("#previewid").html("<b>ID No. </b>"+res.id_num);
         }
         $("#previewname2").html(res.fname +" "+ res.lname);
         if(res.gender==null||res.gender=='' ){

          $("#previewgender").html("<span class='text-danger'>Not Set</span>");
        }else{

          $("#previewgender").html(res.gender);
        }

        if(res.birthday==null||res.birthday=='' ){
          $("#previewbirthday").html("<span class='text-danger'>Not Set</span>");
        }else{
          $("#previewbirthday").html(res.birthday);
        }

        if(res.age==null||res.age=='' ){
          $("#previewage").html("<span class='text-danger'>Not Set</span>");
        }else{
          $("#previewage").html(res.age+" years old");
        }
        if(res.cell==null||res.cell=='' ){
          $("#previewcontact").html("<span class='text-danger'>Not Set</span>");
        }else{
          $("#previewcontact").html(res.cell);
        }
        if(res.address==null||res.address=='' ){
          $("#previewaddress").html("<span class='text-danger'>Not Set</span>");
        }else{
          var add = res.address;
          add = add.split("|");
          $("#previewaddress").html(add[0]);
        }
        if(res.relijon==null||res.relijon=='' ){
          $("#previewreligion").html("<span class='text-danger'>Not Set</span>");
        }else{
          $("#previewreligion").html(res.relijon);
        }
        if(res.blood==null||res.blood=='' ){
          $("#previewblood").html("<span class='text-danger'>Not Set</span>");
        }else{
          $("#previewblood").html(res.blood);
        }
        $("#previewpic").attr("src","<?php echo base_url();?>assets/images/"+res.pic);
        if(res.emergency_fname==null&&res.emergency_mname==null&&res.emergency_lname==null||res.emergency_fname==''&&res.emergency_mname==''&&res.emergency_lname==''){
          $("#previewemergencyname").html("<span class='text-danger'>Not Set</span>");
        }else{
          $("#previewemergencyname").html(res.emergency_fname+" "+res.emergency_mname+" "+res.emergency_lname);
        }
        if(res.emergency_contact==null||res.emergency_contact=='' ){
          $("#previewemergencycontact").html("<span class='text-danger'>Not Set</span>");
        }else{
          $("#previewemergencycontact").html(res.emergency_contact);
        }
        
        if(res.Supervisor==null||res.Supervisor=='' ){
          $("#previewesupervisor").html("<span class='text-danger'>Not Set</span>");
        }else{
          $("#previewesupervisor").html( res.Supervisor);
        }
        

        $("#previewusername").html(res.username);
        $("#previewpassword").html(res.password);
        $("#previewempid").attr('data-empid',empid);
      }
    }
  });
  });

</script>
<script type="text/javascript">
  $("#classes").on('change',function(){
   var value =  $(this).val();
   if(value=='Agents'){
    window.location.href = "<?php echo base_url();?>index.php/Employee/index/Agents"; 
  }else if(value=='Admin'){
    window.location.href = "<?php echo base_url();?>index.php/Employee/index/Admin"; 
  }else{
    window.location.href = "<?php echo base_url();?>index.php/Employee"; 
  }
});
</script>

<script type="text/javascript">
  var monkeyList = new List('test-list', {
    valueNames: ['name'],
    page: 12,   
    pagination: true

  });
  // $('.nav').append('<div class="btn-next"> <i class="glyph-icon icon-chevron-right"></i> </div><div class="btn-last"> <i class="glyph-icon icon-forward"></i> </div>');
  // $('.nav').prepend('<div class="btn-first"> <i class="glyph-icon icon-rewind"></i> </div><div class="btn-prev"> <i class="glyph-icon icon-chevron-left"></i> </div>');

  $('#btn-next').on('click', function(){
    $('.pagination .active').next().trigger('click');
  })
  $('#btn-prev').on('click', function(){
    $('.pagination .active').prev().trigger('click');
  })
  $('#btn-first').on('click', function(){
    monkeyList.show(1,12)
  })
  $('#btn-last').on('click', function(){
    if(monkeyList.searched==true){
      var arr = Object.keys(monkeyList.matchingItems).length;
      var remainder = arr % 12;
      if(remainder==0){
        monkeyList.show(arr-11,12);
      }else{
        monkeyList.show((arr-remainder)+1,12);
      } 
    }else{
      var remainder = <?php echo count($data);?> % 12;
      if(remainder==0){
        monkeyList.show(monkeyList.size()-11,12);
      }else{
        monkeyList.show((monkeyList.size()-remainder)+1,12);
      }
    }

  })

</script>
<script type="text/javascript">
  $("#previewempid").click(function(){
    var empid = $(this).data('empid');
    $("#employeeid").val(empid);
    swal({
      title: 'Enter Password',
      input: 'password',
      showCancelButton: true,
      confirmButtonText: 'Continue',  
      preConfirm: function (password) {
        return new Promise(function (resolve, reject) {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Employee/matchEmployeeCode",
            data: {
              password: password
            },
            cache: false,
            success: function(res)
            {
             if(res=='Exists'){
              resolve();
            }else{
              reject('Code Mismatch.');
            }
          }
        });
        })
      },
      allowOutsideClick: false
    }).then(function () {
      $("#employeeform").submit();
    })
  })
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>