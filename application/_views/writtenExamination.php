<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/server-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
<head>
	<style>
		/* Loading Spinner */
		.spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
	</style>
	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<title> Server page 3 </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Favicons -->

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">




	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

	<!-- JS Core -->

	<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>




	<script type="text/javascript">
		$(window).load(function(){
			setTimeout(function() {
				$('#loading').fadeOut( 400, "linear" );
			}, 300);
		});
	</script>



</head>
<body>
	<div id="loading">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>

	<style type="text/css">
		/*html,body {
			height: 100%;
		}
		body {
			overflow: hidden;
			background: #fff;
		}*/

	</style>

	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/wow/wow.js"></script>
	<script type="text/javascript">
		/* WOW animations */

		wow = new WOW({
			animateClass: 'animated',
			offset: 100
		});
		wow.init();
	</script>

	<img src="<?php echo base_url();?>assets/image-resources/blurred-bg/blurred-bg-4.jpg" class="login-img wow fadeIn" alt="">
	<div class="modal fade" id="scoremodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:10%">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-blue text-center">
					<h2 class="modal-title text-bold">You have finished the exam.</h2>
				</div>
				<div class="modal-body text-center">
					<h3 id="score"  style="padding: 30px 3px;"></h3>
				</div>
				<div class="modal-footer text-center">
					<button type="button" class="btn btn-primary btn-lg" id="endexam">Okay</button>
				</div>
			</div>
		</div>
	</div>
	<div class="center-vertical">
		<div class="center-content row">
			<div class="col-md-8 center-margin">
				<br>
				<div class="panel-layout wow bounceInDown inverse">
					<div class="panel-box">

						<div class="panel-content" style='background: rgba(0,0, 0, 0.5);'>
							<h1 style="font-size:25px;font-weight: bold;"><div id='timer'></div>&nbsp;</h1><hr style='margin:8px 0;'>
							<h4 style="color:#eee;font-weight: bold;text-align:center;">Name: <?php echo $this->session->name;?></h4>
							<p style="color: #ddd;text-align:center;">Position Applied: <?php echo $this->session->positionApplied;?></p>
						</div>
						<div class="panel-content pad15A" style='background: rgba(0,0, 0, 0.2);'>
							<h5 style='color:beige;margin-bottom:5px;' class='pull-right'>TOTAL: <?php echo $totalscore;?> pts</h5>
							<form id="questionform">
								<div class="row">
									<?php $x=1; foreach($questions as $q):?>
									<div class="col-md-12">
										<div class="panel-layout">
											<div class="panel-box">

												<div class="panel-content bg-info text-left">
													<?php echo $x.'. '.$q->question; echo '<span class="text-primary"> ('.$q->points.' pts)</span>';?>
												</div>
												<div class="panel-content pad15A bg-white text-left">
													<div class="row">
														<?php 
														$choice = explode('|',$q->choices);
														shuffle($choice);
														foreach($choice as $c):?>
														<div class="col-md-6">
															<input type="radio" name="<?php echo $q->writtenid;?>" value="<?php echo $c;?>"> <?php echo $c;?>
														</div>
													<?php endforeach;?>
												</div>
											</div>

										</div>
									</div>
								</div>

								<?php $x++; endforeach;?>
								<div class="col-md-12">
									
									<input type="submit" class="btn btn-primary pull-right" value="Submit Answers">
								</div>
							</div> 
						</form>
					</div>

				</div>
			</div>


		</div>

	</div>
</div>



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script>
// console.log("TIME:"+<?php echo $this->session->userdata('endtime');?>);
// Set the date we're counting down to
var countDownDate = new Date("<?php echo $this->session->endtime;?>").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    //var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    //var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    
    // If the count down is over, write some text 
    if (distance < 0) {
    	clearInterval(x);
    	document.getElementById("timer").innerHTML = "<span class='pull-left'style='color:beige;'>Examination</span><span class='text-danger  pull-right'>Time is Up</span>";
    	$("#questionform :input").not(':input[type=submit]').attr("disabled", true);
    }else if(distance<600000){
    	document.getElementById("timer").innerHTML =  "<span class='pull-left'style='color:beige;'>Examination</span><span class='text-warning pull-right'>"+minutes + "m " + seconds + "s </span>";
    }else{
    	document.getElementById("timer").innerHTML =  "<span class='pull-left'style='color:beige;'>Examination</span><span class='pull-right' style='color: Turquoise ;'>"+minutes + "m " + seconds + "s </span>";
    }
}, 1000);
</script>
<script type="text/javascript">
	$('#endexam').click(function(){

		window.location = "<?php echo base_url();?>index.php/applicant/endExam";
	});
	$("#questionform").submit(function(){
		var data = $("#questionform").serialize();
		$.ajax({
			url: "<?php echo base_url();?>index.php/applicant/submitAnswer",
			data: data,
			type: "POST",
			success: function(msg) {
				if(msg=='Stop'){

					window.location = "<?php echo base_url();?>index.php/applicant/endExam";
				}else{

					var res = msg.split("|");
					if(res[2]=='Passed'){
						var notify = "You have passed the examination.  <br>Score: "+res[1]+" out of "+res[0];
					}else{
						var notify = "Human Resources Team will inform the result of your examination. <br><br>Just keep your lines open.";
					}
					$('#score').html(notify);
					$("#scoremodal").modal('show');	
				}
			}
		});
		return false;
	});
	$('#scoremodal').on('hide.bs.modal', function () { 
		window.location = "<?php echo base_url();?>index.php/applicant/endExam";
	}); 
</script>
</body>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/server-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:47 GMT -->
</html>