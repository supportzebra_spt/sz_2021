<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->


    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

 <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/uniform/uniform.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/uniform/uniform.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/uniform/uniform-demo.js"></script>




    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>
 

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'yyyy-mm-dd'
        });
    });

</script>
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="<?php echo $setting['settings']; ?>">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
<div class="form-group">
<?php 
 
echo "<h3>".$lname."'s Examination Details</h3>";
 
?>
<br>
<br>
		<a href="../applicant"><button class="btn btn-xs btn-success" id="AddMenu"> Back </button></a>
		<button class="btn btn-xs btn-success"  data-toggle="modal" data-target="#myModalAddMenu" id="AddMenu"> Add </button>
				<div id="myModalAddMenu" class="modal fade" role="dialog">
					<div class="modal-dialog" >

					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header" style="background: #00b19b; color: white;">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Examination details</h4>
					  </div>
					  <div class="modal-body"> 
					<div class="example-box-wrapper">	 
						<form class="form-horizontal bordered-row">

							<div class="form-group">
								<label class="col-sm-3 control-label">Exam</label>
								<div class="col-sm-6">
									<select  class="form-control" id="ExamDetails">
										<option>--</option>
										<option>Written</option>
										<option>Interview</option>
									 </select>
								</div>
							</div>
							</form>
								<div id="written" hidden>
								<form class="form-horizontal bordered-row">

									<div class="form-group">
									<label class="col-sm-3 control-label">Result</label>
									<div class="col-sm-6">
										<select name="" id='writeResult' class="form-control">
											<option>--</option>
											<option>Passed</option>
											<option>Failed</option>
										 </select>
									</div>
									</div>
									<div class="form-group">
									<label class="col-sm-3 control-label">Remarks</label>
									<div class="col-sm-6">
									<textarea name=""  rows="3"  id='writeRemark' class="form-control textarea-xs"></textarea>

									</div>
									</div>
									 <div class="form-group">
									<label for="" class="col-sm-3 control-label">Date</label>
									<div class="col-sm-6">
										<div class="input-prepend input-group">
											<span class="add-on input-group-addon">
												<i class="glyph-icon icon-calendar"></i>
											</span>
											<input type="text" class="bootstrap-datepicker form-control"   id='writeDate' data-date-format="yyyy-mm-dd">
										</div>
									</div>
									</div>
										</form>
										<Br>
									<div style="width: 35%;float: right;">
									<button class="btn btn-s btn-success"  id='btnAddWritten'> Add </button>
									</div>
								</div>
								
							
								

								<div id="interview" hidden >
								<form class="form-horizontal bordered-row">
									<div class="form-group">
									<label class="col-sm-3 control-label">Level</label>
									<div class="col-sm-6">
										<select name=""  id='interviewLevel' class="form-control">
											<option>--</option>
											<option>Initial</option>
											<option>Second</option>
											<option>Final</option>
										 </select>
									</div>
									</div>
									<div class="form-group">
									<label class="col-sm-3 control-label">Result</label>
									<div class="col-sm-6">
										<select name="" id='interviewResult' class="form-control">
											<option>--</option>
											<option>Passed</option>
											<option>Failed</option>
											<option>Considered</option>
										 </select>
									</div>
									</div>
									<div class="form-group">
									<label class="col-sm-3 control-label">Remarks</label>
									<div class="col-sm-6">
									<textarea name=""   rows="3" id='interviewRemarks' class="form-control textarea-xs"></textarea>

									</div>
									</div>
									<div class="form-group">
									<label class="col-sm-3 control-label">Interviewer</label>
									<div class="col-sm-6">
									<textarea name=""   rows="1" id='interviewPerson' class="form-control textarea-xs"></textarea>

									</div>
									</div>
									 <div class="form-group">
									<label for="" class="col-sm-3 control-label">Date</label>
									<div class="col-sm-6">
										<div class="input-prepend input-group">
											<span class="add-on input-group-addon">
												<i class="glyph-icon icon-calendar"></i>
											</span>
											<input type="text" id='interviewDate' class="bootstrap-datepicker form-control"   data-date-format="yyyy-mm-dd">
										</div>
									</div>
									</div>
									</form> 
									<Br>
									<div style="width: 35%;float: right;">
										<button class="btn btn-s btn-success" id='btnAddInterview'> Add </button>
									</div>
 								</div>
							
						
							
 					  </div>
 					  </div>
					 
					</div>

				  </div>
				</div>

				
<div class="example-box-wrapper">

<table id="datatable-reorder" class="table table-striped table-bordered" cellspacing="0" width="100%">
<thead>
<tr>
    <th>Position</th>
    <th>Date</th>
    <th>Count</th>
    <th>Level</th>
    <th>Exam</th>
    <th>Exam</th>
    <th>Result</th>
    <th>Interviewer</th>
</tr>
</thead>
							
<tfoot>
<tr>
	<th>Position</th>
    <th>Date</th>
    <th>Count</th>
    <th>Level</th>
    <th>Exam</th>
    <th>Exam</th>
    <th>Result</th>
    <th>Interviewer</th>
</tr>
</tfoot>

<tbody>
<?php 
if(isset($user))
foreach($user as $key => $val){?>
	<tr>
		<td><?php echo $val['pos_name']; ?></td>
		<td><?php echo $val['date_taken']; ?></td>
		<td><?php echo $val['count']; ?></td>
		<td><?php echo $val['time_count']; ?></td>
		<td><?php echo $val['test_info']; ?></td>
		<td><?php echo $val['remarks']; ?></td>
		<td><?php echo $val['result']; ?></td>
		<td><?php echo $val['interviewer']; ?></td>
	</tr>
<?php } ?>

</tbody>
</table>
</div>				
				
</div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
	    $.ajax({
		type: "POST",
		url: location.protocol + '//' + location.hostname+"/sz3/index.php/Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	
	$.ajax({
		type: "POST",
		url: location.protocol + '//' + location.hostname+"/sz3/index.php/Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
  });	
</script>
 <script>
  $(function(){
	  $("#ExamDetails").change(function(){
  		  var res = $('#ExamDetails').val()
		  if(res=='Written'){
			 $('#written').slideDown() ;
			 $('#interview').slideUp() ;
			  
		  }else if(res=='Interview'){
			   $('#interview').slideDown() ;
			 $('#written').slideUp() ;
			  
		  }else{
			  $('#written').slideUp() ; 
			   $('#interview').slideUp() ;
		  }
	  });
	  
	   $("#btnAddWritten").click(function(){
		   var result = $('#writeResult').val();
		   var remark = $('#writeRemark').val();
		   var date = $('#writeDate').val();
		   var apid =  <?php echo $apid; ?>;
		   var posid =  <?php echo $pos_id; ?>;
		   
		   var dataString = "result="+result+"&remark="+remark+"&date="+date+"&apid="+apid+"&posid="+posid;
 			 $.ajax({
			type: 'POST',
			url: 'writeApplicantEval',
			data: dataString,
			success: function(html){
				swal(
				  'Success',
				  'You have successfully inserted a record',
				  'success'
				)	
					  setTimeout(function(){location.reload();},2000);

				}
			  
			});		   
	  });
	  $("#btnAddInterview").click(function(){
		   var level = $('#interviewLevel').val();
		   var result = $('#interviewResult').val();
		   var remark = $('#interviewRemarks').val();
		   var interviewer = $('#interviewPerson').val();
		   var date = $('#interviewDate').val();
		   var apid =  <?php echo $apid; ?>;
		   var posid =  <?php echo $pos_id; ?>;

		   var dataString = "result="+result+"&remark="+remark+"&date="+date+"&level="+level+"&interviewer="+interviewer+"&apid="+apid+"&posid="+posid;
 		   	 $.ajax({
			type: 'POST',
			url: 'interviewApplicantEval',
			data: dataString,
			success: function(html){
					swal(
				  'Success',
				  'You have successfully inserted a record',
				  'success'
				)
					  setTimeout(function(){location.reload();},2000);

				}
			  
			});	
	  });
 
  });	
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>