<!DOCTYPE html> 
<html  lang="en">

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
            hr{
                padding:0 !important;
                margin:5px 0 !important;
            }
        </style>
      <!--   <style type="text/css">
          .horizontal-scroll-except-first-column {
            /*      width: 100%;*/
            overflow: auto;
            margin-left: 20em;
          }
      
          .horizontal-scroll-except-first-column > table > * > tr > th:first-child,
          .horizontal-scroll-except-first-column > table > * > tr > td:first-child {
            position: absolute;
            width: 20em;
            margin-left: -20em;
          }
      
          .horizontal-scroll-except-first-column > table > * > tr > th,
          .horizontal-scroll-except-first-column > table > * > tr > td {
          /* Without this, if a cell wraps onto two lines, the first column
          * will look bad, and may need padding. */
          white-space: nowrap;
        }
      </style> -->

        <style type="text/css">
            .myscrolling table {
                table-layout: inherit;
                *margin-left: -100px;/*ie7*/
            }
            .myscrolling td, .myscrolling th {
                vertical-align: top;
                padding: 10px;
                min-width: 100px;
                white-space: nowrap;
                border-left:1px solid black;
                border-right:1px solid black;
            }
            .myscrolling  th {
                position: absolute;
                *position: relative; /*ie7*/
                left: 0;
                width: 250px; 
                white-space: nowrap;
                text-overflow: ellipsis;    
                overflow: hidden;
            }
            .myouter {
                position: relative
            }
            .myinner {
                overflow-x: auto;
                overflow-y: visible;
                margin-left: 250px;
            }
        </style>
        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/fontawesome/fontawesome.css"> -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert2/sweetalert2.css">

        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });

        </script><!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/chosen/chosen.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/chosen/chosen.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/chosen/chosen-demo.js"></script>
        <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/daterangepicker/daterangepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript">
                $(function () {
                    "use strict";
                    var m = moment().format("MM/DD/YYYY");
                    var end = moment().add(1, 'year').format("MM/DD/YYYY");
                    $("#daterangepicker-example").daterangepicker({minDate: m, startDate: m, maxDate: end});
                });
        </script>
        <!-- Multi select -->

        <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/multi-select/multiselect.js"></script>
        <script type="text/javascript">
                /* Multiselect inputs */

                $(function () {
                    "use strict";
                    $("#employeelist").multiSelect();
                    $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
                });
        </script>
    </head>


    <body>
        <div id="sb-site">


            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <div id="page-header" class="bg-black">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>

                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">


                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div class="container">

                            <div id="page-title">
                                <div class="example-box-wrapper">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a class="btn" style="background:#FF9710;color:white"  href="<?php echo base_url(); ?>index.php/Schedule">
                                                <span class="glyph-icon icon-separator">
                                                    <i class="glyph-icon icon-arrow-circle-left"></i>
                                                </span>
                                                <span class="button-content">
                                                    Back to Schedule
                                                </span>
                                            </a>
                                            <a class="btn" style="background:#2294F0;color:white" id="upload">
                                                <span class="glyph-icon icon-separator">
                                                    <i class="glyph-icon icon-plus-square"></i>
                                                </span>
                                                <span class="button-content">
                                                    Upload Schedule
                                                </span>
                                            </a>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <h2 style="font-size:25px">MULTIPLE SCHEDULES</h2>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:15px;display: none;" id="uploadFileRow">
                                        <br>
                                        <div class="col-md-5" id="uploadFileDiv" style="display: none;">
                                            <div>
                                                <input type="file" class="form-control  sweet-3" id="uploadFile" onchange="checkfile(this);" title="Drag and Drop Excel Here">
                                            </div>
                                        </div> 
                                        <div class="col-md-3">
                                            <a class="btn btn-sm" style="display: none;background:#7AB734;color:white" id="upload2">
                                                <span class="glyph-icon icon-separator">
                                                    <i class="glyph-icon icon-upload"></i>
                                                </span>
                                                <span class="button-content">
                                                    Upload
                                                </span>
                                            </a>
                                            <a class="btn btn-sm" style="display: none;background:#ED61A0;color:white" id="save">
                                                <span class="glyph-icon icon-separator">
                                                    <i class="glyph-icon icon-check"></i>
                                                </span>
                                                <span class="button-content">
                                                    Save
                                                </span>
                                            </a>
<!--                                            <button class="btn btn-sm" id="upload2" style="display: none;background:#7AB734;color:white"><span class='icon-typicons-upload'> Upload</span></button>

                                            <button class="btn btn-sm" id="save" style="display: none;background:#ED61A0;color:white"><span class='icon-typicons-check'> Save</span></button>-->


                                        </div>
                                        <button class="btn btn-sm btn-primary" id="view" style="display: none;background:#ED6A59;color:white" hidden>View</button>
                                    </div><br>
                                    <div class="content-box" id="preview" style="display:none;">
                                        <h3 class="content-box-header" style="background:#EC6959;color:white"> 
                                            <i class="glyph-icon icon-list"></i>
                                            PREVIEW UPLOADED SCHEDULE
                                        </h3>
                                        <div class="content-box-wrapper">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="myscrolling myouter">
                                                        <div class="myinner">
                                                            <table class="table table-striped table-hover table-condensed" id="tbody">

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="content-box">
                                                <h3 class="content-box-header" style="background:#8766A4;color:white">
                                                    <i class="glyph-icon icon-search"></i>
                                                    SCHEDULE GENERATE EXCEL

                                                </h3>
                                                <div class="content-box-wrapper">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group" id='acc_descriptiondiv'>
                                                                        <label class='control-label'>Account Type</label>
                                                                        <select class='form-control' id='acc_description'>
                                                                            <option value='' id='removeme'>--</option>
                                                                            <option value='All'>All</option>
                                                                            <option value='Admin'>Admin</option>
                                                                            <option value='Agent'>Agent</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" id="acc_name_area">

                                                                    <div class="form-group" id='acc_namediv'>
                                                                        <label class='control-label'>Account Name</label>
                                                                        <select class='form-control' id='acc_name'></select>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-12">
                                                                    <label class='control-label'>Date Range</label>
                                                                    <div class="input-prepend input-group">
                                                                        <span class="add-on input-group-addon">
                                                                            <i class="glyph-icon icon-calendar"></i>
                                                                        </span>
                                                                        <input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control" readonly='true'>
                                                                    </div>
                                                                </div>                    
                                                            </div>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <div class="col-md-12"  id='employeelistdiv'>
                                                                <div class="row">
                                                                    <div class="col-md-10">
                                                                        <label class='control-label'>Employee List</label>
                                                                        <select multiple name="" id="employeelist"  class="multi-select"></select>
                                                                    </div>

                                                                    <div class="col-md-2"><br>
                                                                        <div class="btn-group-vertical">
                                                                            <button id="select" class='btn btn-xs' style="background:#8766A4;color:white;border:1px solid white">Select all</button>

                                                                            <button id="deselect" class='btn btn-xs' style="background:#8766A4;color:white;border:1px solid white">Deselect all</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="button-pane text-right">
                                                    <!--<button class="btn btn-primary" id='generateschedule'>Display Schedule</button>-->
                                                    <a href="#" class="btn" style="background:#8766A4;color:white"  id='generateexcel'>
                                                        <span class="glyph-icon icon-separator">
                                                            <i class="glyph-icon icon-download"></i>
                                                        </span>
                                                        <span class="button-content">
                                                            Download Schedule Excel
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

            <!-- JS Demo -->
            <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets/sweetalert2/sweetalert2.js"></script>

        </div>
    </body>
    <script>
                                                        $(function () {
                                                            /*     $.ajax({
                                                             type: "POST",
                                                             url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
                                                             cache: false,
                                                             success: function(html)
                                                             {
                                                             
                                                             $('#headerLeft').html(html);
                                                             //alert(html);
                                                             }
                                                             }); */
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
                                                                cache: false,
                                                                success: function (html) {

                                                                    $('#sidebar-menu').html(html);
                                                                    // alert(html);
                                                                }
                                                            });
                                                        });
    </script>
    <script type="text/javascript">
            $(function () {
                var role = "<?php echo $role; ?>";
//                alert(role);
                if (role == 'Administrator') {
                    $("#employeelist").prop('disabled', true);
                    $("#employeelist").multiSelect('refresh');
                    $("#select").prop('disabled', true);
                    $("#deselect").prop('disabled', true);
                    $("#acc_name").prop('disabled', true);
                    $("#acc_description").on('change', function () {
                        var desc = $(this).val();
                        $("#removeme").remove();
                        if (desc == 'All') {

                            $("#employeelist").prop('disabled', true);
                            $("#employeelist").multiSelect('refresh');
                            $("#select").prop('disabled', true);
                            $("#deselect").prop('disabled', true);
                            $("#acc_name").prop('disabled', true);
                        } else {

                            $("#acc_name").prop('disabled', false);
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getAccounts",
                                data: {
                                    desc: desc
                                },
                                cache: false,
                                success: function (res) {
                                    if (res != 'Empty') {
                                        $("#acc_name").html("<option value='All'>All</option>");
                                        $.each(JSON.parse(res), function (i, elem) {
                                            // alert(elem.rate);
                                            $("#acc_name").append("<option value='" + elem.acc_id + "'>" + elem.acc_name + "</option>");
                                        });
                                    }
                                }
                            });
                        }


                    });
                    //----------------------------------EVENT--------------------------------
                    $("#acc_name").on('change', function () {
                        var acc_id = $(this).val();
                        if (acc_id == 'All') {
                            $("#employeelist").prop('disabled', true);
                            $("#employeelist").multiSelect('refresh');
                            $("#select").prop('disabled', true);
                            $("#deselect").prop('disabled', true);
                        } else {
                            $("#employeelist").prop('disabled', false);
                            $("#employeelist").multiSelect('refresh');
                            $("#select").prop('disabled', false);
                            $("#deselect").prop('disabled', false);
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getAccountEmployee",
                                data: {
                                    acc_id: acc_id
                                },
                                cache: false,
                                success: function (res) {
                                    if (res != 'Empty') {
                                        $("#employeelist").html("");
                                        $.each(JSON.parse(res), function (i, elem) {

                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });
                                        $("#employeelist").multiSelect('refresh');

                                    } else {

                                        $("#employeelist").html("");
                                        $("#employeelist").multiSelect('refresh');
                                    }
                                }
                            });
                        }


                    });
                    //END OF EVENT --------------------------------------------------------------
                } else if (role == 'User Level 1') {
                    $("#acc_name_area").html("");
                    $("#employeelist").prop('disabled', true);
                    $("#employeelist").multiSelect('refresh');
                    $("#select").prop('disabled', true);
                    $("#deselect").prop('disabled', true);
                    $("#acc_description").on('change', function () {
                        var desc = $(this).val();
                        $("#removeme").remove();
                        if (desc == 'All') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getAllTeam",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');

                                    }
                                }
                            });
                        } else if (desc == 'Admin') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getTeamAdminEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');
                                    }
                                }
                            });
                        } else if (desc == 'Agent') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getTeamAgentEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');

                                    }

                                }
                            });
                        } else {
                            alert('ambot nganu');
                        }

                        $("#employeelist").prop('disabled', false);
                        $("#employeelist").multiSelect('refresh');
                        $("#select").prop('disabled', false);
                        $("#deselect").prop('disabled', false);
                    });
                } else {


                    $("#acc_name_area").html("");
                    $("#employeelist").prop('disabled', true);
                    $("#employeelist").multiSelect('refresh');
                    $("#select").prop('disabled', true);
                    $("#deselect").prop('disabled', true);
                    $("#acc_description").on('change', function () {
                        var desc = $(this).val();
                        $("#removeme").remove();
                        if (desc == 'All') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getSupervisoryAllEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');

                                    }
                                }
                            });
                        } else if (desc == 'Admin') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getSupervisoryAdminEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');
                                    }
                                }
                            });
                        } else if (desc == 'Agent') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getSupervisoryAgentEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');

                                    }

                                }
                            });
                        } else {
                            alert('ambot nganu');
                        }

                        $("#employeelist").prop('disabled', false);
                        $("#employeelist").multiSelect('refresh');
                        $("#select").prop('disabled', false);
                        $("#deselect").prop('disabled', false);
                    });
                }

            })
    </script>
    <script type="text/javascript">
            $(function () {
                $("#acc_description").on('change', function () {

                    $("#employeelist").html("");

                    $("#employeelist").multiSelect('refresh');


                })
            })
    </script>
    <script type="text/javascript">
            $(function () {

                $('#daterangepicker-example').on('apply.daterangepicker', function (ev, picker) {

                });
                $("#employeelist").on('change', function () {

                })
            })
    </script>
    <script type="text/javascript">
            $(function () {
                $('#select').click(function () {
                    $('#employeelist option').each(function () {

                        $("#employeelist").multiSelect('select_all');
                    });
                    $("#employeelist").multiSelect('refresh');
                    // alert('nicca');


                });
            })
    </script>
    <script type="text/javascript">
            $(function () {

                $('#deselect').click(function () {
                    $('#employeelist option').each(function () {

                        $("#employeelist").multiSelect('deselect_all');
                    });
                    $("#employeelist").multiSelect('refresh');


                });
            })
    </script>
    <script type="text/javascript">
            $("#generateexcel").click(function () {
                var date = $("#daterangepicker-example").val();
                var dater = date.split(" - ");
                var fromDateRaw = dater[0];
                var fromDateRaw2 = fromDateRaw.split("/");
                var fromDate = fromDateRaw2[2] + "-" + fromDateRaw2[0] + "-" + fromDateRaw2[1];
                var toDateRaw = dater[1];
                var toDateRaw2 = toDateRaw.split("/");
                var toDate = toDateRaw2[2] + "-" + toDateRaw2[0] + "-" + toDateRaw2[1];
                var acc_description = $("#acc_description").val();
                var acc_name = $("#acc_name").val();
                var employeelist = $("#employeelist").val();
                // alert(fromDate+" "+toDate);
                if (date == '') {

                    swal("Oops!", "Select a date range first!", "warning");
                } else {
                    var role = "<?php echo $role; ?>";
                    if (role == 'Administrator') {
//                        alert("admin");
                        if (acc_description == '') {
                            swal("Oops!", "Please choose account type!", "warning");
                        } else if (acc_description == 'All') {
                            window.location.href = "<?php echo base_url(); ?>index.php/Schedule/downloadSched/" + fromDate + "/" + toDate;

                        } else {
                            if (acc_name == 'All') {
                                window.location.href = "<?php echo base_url(); ?>index.php/Schedule/downloadSched/" + fromDate + "/" + toDate + "/" + acc_description;

                            } else {
                                if (employeelist == null) {

                                    swal("Oops!", "Select employee/s first!", "warning");
                                } else {

                                    var emplist = employeelist.toString().split(",");
                                    employeelist = emplist.join('-');
                                    window.location.href = "<?php echo base_url(); ?>index.php/Schedule/downloadSched/" + fromDate + "/" + toDate + "/0/" + employeelist;
                                }

                            }
                        }
                    } else if (role == 'User Level 1') {
//                        alert("level 1");
                        if (employeelist == null) {

                            swal("Oops!", "Select employee/s first!", "warning");
                        } else {
                            var dater = date.split(" - ");
                            var fromDateRaw = dater[0];
                            var fromDateRaw2 = fromDateRaw.split("/");
                            var fromDate = fromDateRaw2[2] + "-" + fromDateRaw2[0] + "-" + fromDateRaw2[1];
                            var toDateRaw = dater[1];
                            var toDateRaw2 = toDateRaw.split("/");
                            var toDate = toDateRaw2[2] + "-" + toDateRaw2[0] + "-" + toDateRaw2[1];

                            var emplist = employeelist.toString().split(",");
                            employeelist = emplist.join('-');
                            window.location.href = "<?php echo base_url(); ?>index.php/Schedule/downloadSched/" + fromDate + "/" + toDate + "/0/" + employeelist;
                        }
                    } else {
//                        alert("employee");
                        if (employeelist == null) {

                            swal("Oops!", "Select employee/s first!", "warning");
                        } else {
                            var dater = date.split(" - ");
                            var fromDateRaw = dater[0];
                            var fromDateRaw2 = fromDateRaw.split("/");
                            var fromDate = fromDateRaw2[2] + "-" + fromDateRaw2[0] + "-" + fromDateRaw2[1];
                            var toDateRaw = dater[1];
                            var toDateRaw2 = toDateRaw.split("/");
                            var toDate = toDateRaw2[2] + "-" + toDateRaw2[0] + "-" + toDateRaw2[1];

                            var emplist = employeelist.toString().split(",");
                            employeelist = emplist.join('-');
                            window.location.href = "<?php echo base_url(); ?>index.php/Schedule/downloadSched/" + fromDate + "/" + toDate + "/0/" + employeelist;
                        }
                    }
                }

            })
    </script>

    <script type="text/javascript">
            $("#upload").click(function () {
                $('#uploadFileDiv').toggle();
                $('#uploadFileRow').toggle();
            });
            $('#uploadFile').click(function () {
                $("#preview").hide();
            })

            $("#upload2").click(function (event) {

                var data = new FormData();
                $.each($("#uploadFile")[0].files, function (i, file) {
                    data.append("uploadFile", file);
                });

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/Schedule/uploadsched",
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        response = $.trim(response);
                        if (response == 'Error') {
                            swal("Error", "Problem Uploading Excel", "error");
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/viewuploadedschedules",
                                cache: false,
                                success: function (res) {
                                    res = res.trim();
                                    try {
                                        res = JSON.parse(res);
                                        if (res.status === 'Mismatch') {
                                            $("#upload2").hide();
                                            $("#uploadFileDiv").hide();
                                            $("#uploadFileRow").hide();
                                            $("#preview").hide();
                                            $("#save").hide();
                                            $("#uploadFile").val("");
                                            swal("Oops!", "Please use the template given!", "error");
                                        } else if (res.status == "FileError") {
                                            swal("Oops!", "Error Reading File", "error");
                                        } else if (res.status == "Success") {

                                            swal("Note!", "Please double check schedules before saving.", "warning");
                                            $('#tbody').empty();
                                            // $("#viewtable").show();
                                            var data = res.data;
                                            var body = "";
                                            for (var i = 0; i < data.length; i++) {
                                                body += "<tr>";
                                                if (i == 0) {
                                                    for (var j = 0; j < data[i].length; j++) {
                                                        if (j == 0) {
                                                            body += "<th style='background: #eee;color:black'>" + data[i][j] + "</th>";
                                                        } else {
                                                            if (data[i][j] == null || data[i][j] == '') {
                                                                body += "<td  class='text-left' style='background:#EE3B3B !important;'>&nbsp;</td>";
                                                            } else {
                                                                body += "<td  class='text-left' style='background: #eee;color:black'>" + data[i][j] + "</td>";
                                                            }
                                                        }

                                                    }
                                                } else {

                                                    for (var j = 0; j < data[i].length; j++) {
                                                        if (j == 0) {
                                                            body += "<th>" + data[i][j] + "</th>";
                                                        } else {
                                                            if (data[i][j] == null || data[i][j] == '') {
                                                                body += "<td  class='text-left' style='background:#CD5555 !important;'>&nbsp;</td>";
                                                            } else {
                                                                body += "<td  class='text-left'>" + data[i][j] + "</td>";
                                                            }
                                                        }

                                                    }
                                                    body += "</tr>";
                                                }


                                                // body +="</tr>";

                                            }
                                            $('#tbody').append(body);
                                            $("#preview").show();
                                            $("#save").show();
                                        } else if (res.status == "Empty") {

                                            swal("Oops!", "No data found", "error");
                                        } else {
                                            swal("Oops!", "Problem Reading the File", "error");
                                        }
                                    } catch (e) {
                                        alert(e); // error in the above string (in this case, yes)!
                                    }



                                }
                            });
                        }

                    },
                    error: function () {
                        swal("Error", "An error occurred while uploading. Please Try Again.", "error");
                    }
                });
                event.preventDefault();
            });
    </script>
    <script type="text/javascript">
            $("#save").click(function () {

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/Schedule/savemultiplesched",
                    cache: false,
                    success: function (res) {
                        swal("Success", 'Successfully Uploaded Schedules', 'success');
                        setTimeout(function () {
                            window.location = "<?php echo base_url(); ?>/index.php/Schedule/multiplesched";

                        }, 2000);


                    }
                });
            });
    </script>
    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

    <script type="text/javascript">
            function checkfile(sender) {
                var validExts = new Array(".xlsx", ".xls");
                var fileExt = sender.value;
                fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
                if (validExts.indexOf(fileExt) < 0) {
                    swal('Invalid file selected', "Valid files are of " +
                            validExts.toString() + " types.", "error");
                    $("#uploadFile").val("");

                    $("#upload2").hide();
                    $("#view").hide();
                    $("#save").hide();
                    return false;
                } else {
                    $("#upload2").show();
                    return true;
                }
            }
    </script>
</html>