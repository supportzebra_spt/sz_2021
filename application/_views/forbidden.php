<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/server-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
<head>
    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>
    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> 403 Page </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css">


<!-- HELPERS -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/helpers/animate.css">
       <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/helpers/utils.css">
 

<!-- ELEMENTS -->
 
 
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/elements/response-messages.css">
 
 
  
 



<!-- ICONS -->
 

 
 

 
    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js-core/jquery-core.js"></script>
  



    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>



</head>
<body>
<div id="loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<style type="text/css">
    html,body {
        height: 100%;
    }
    body {
        background: #fff;
        overflow: hidden;
    }

</style>

<script type="text/javascript" src="<?php echo base_url("assets/widgets/wow/wow.js") ?>"></script>
<script type="text/javascript">
    /* WOW animations */

    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
</script>

<img src="<?php echo base_url("assets/image-resources/blurred-bg/blurred-bg-7.jpg") ?>" class="login-img wow fadeIn" alt="">
 
<div class="center-vertical">
    <div class="center-content row">

        <div class="col-md-6 center-margin">
            <div class="server-message wow bounceInDown inverse">
                <h1>403</h1>
                <h2>Forbidden Error </h2>
                <p>This page is temporarily disabled. Please use the new SZ system. <br>Thank you.</p>

                <a href="../">                 
                    <button class="btn btn-lg btn-success">Return to Home page</button>
                </a>
				
				<?php 
				$base = explode("/",base_url());
				
				?>
				<?php
				// echo $page;
				if(isset($page) && $page=="dtrpage"){
				?>
				<a href="<?php echo $base[0]."/sz/dtr/attendance"; ?>">                 

                    <button class="btn btn-lg btn-primary">Visit New DTR</button>
                </a>
				<?php 
					}else{
						$pg = isset($page) ? $page : "";
				?>
				<a href="<?php echo $base[0]."/sz/".$pg; ?>">                 
                    <button class="btn btn-lg btn-primary">Visit New Site</button>
                </a>
				<?php
					}
				?>
				
            </div>
        </div>

    </div>
</div>

 
</body>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/server-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
</html>