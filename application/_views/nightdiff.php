<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>


</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
			<input type="button" class="btn btn-sucess" value="Add" id="addND" data-toggle="modal" data-target="#myModal">
				<div class="container"> 
					<div id="page-title">
						<div class="form-group">
					<div class="panel">
					<div class="panel-body">
					<h3 class="title-hero">
						Datatables export buttons
					</h3>
					<div class="example-box-wrapper">
					<table id="datatable-tabletools" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th>Time/Shift</th>
						<th>Night Differential</th>
						<th>ND-Shift In</th>
						<th>ND-Shift Out</th>
						<th>Action</th>
					</tr>
					</thead>
					<tfoot>
					<tr>
					  <th>Time/Shift</th>
						<th>Night Differential</th>
						<th>ND-Shift In</th>
						<th>ND-Shift Out</th>
						<th>Action</th></tr>
					</tfoot>

					<tbody>
						<?php foreach($nd as $row => $val){?>
					 
					<tr>
						<td> <?php echo $val["time_start"]." to".$val["time_end"]; ?></td>
						<td> <?php echo (isset($val["hour"])) ? $val["hour"]." hours & ".$val["minutes"]." mins."  : "--" ; ?></td>
						 <td> <?php echo $val["nd_time_start"]; ?></td>
						<td> <?php echo $val["nd_time_end"]; ?></td>
						<td> <input type="button" class="btn btn-info" value="update" data-toggle="modal" data-target="#myModal" onclick="ndModal(<?php echo $val["time_id"].",".$val["pnd_id"]; ?>)"></td>
					   </tr>
						<?php } ?>
					</tbody>
					</table>
					</div>
					</div>
					</div>	

					<div id="myModal" class="modal fade" role="dialog">
					  <div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header" style="background:#212121;color:white">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Night Differential Rates <span id="txtIDTym" hidden><span></h4>
						  </div>
						  <div class="modal-body">
						   <div class="form-group SelectShift" >
								<label class="col-sm-3 control-label">Shift</label>
								<div class="col-sm-6">
									<select name="" class="custom-select" id="ShiftSelect">
										<option>--</option>
										<?php 
										foreach($shiftND as $row => $val){
											echo "<option value=".$row.">".$val["time_start"]." to ".$val["time_end"]."</option>";
											
										}
										?>
 									</select>
								</div>
							</div>
							  <div class="form-group">
								<label for="email"><h3>Night Differential:</h3></label><br>
									  <div class="form-group">
										<label for="text">Total Time:</label>
										<input type="text" class="form-control" id="ndTotalTime">
									  </div>
							  </div>

							  <div class="form-group">
								<label for="email"><h3>Night Differential-Holiday:</h3></label><br>
									  <div class="form-group">
										<label for="text">Total Time (Start of Shift - 12MN):</label>
										<input type="text" class="form-control" id="ndTotalHolStart">
									  </div>
									  <div class="form-group">
										<label for="text">Total Time (12MN - End of Shift):</label>
										<input type="text" class="form-control" id="ndTotalHolEnd">
									  </div>
							  </div>

						  </div>
						  <div class="modal-footer">
						  </div>
						</div>
					  </div>
					 </div>
						</div>
						<div id='Templatedesign'>
						</div>
					</div>
				 </div>
			 </div>
		 </div>
	 </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
		$('#sidebar-menu').load('<?php echo base_url(); ?>/index.php/Templatedesign/sideBar');
		// $('#headerLeft').load('<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft');

  }); 
</script>
 <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/datatable/datatable.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

<script type="text/javascript">

    /* Datatables export */

    $(document).ready(function() {
        var table = $('#datatable-tabletools').DataTable();
        var tt = new $.fn.dataTable.TableTools( table );

        $( tt.fnContainer() ).insertBefore('#datatable-tabletools_wrapper div.dataTables_filter');

        $('.DTTT_container').addClass('btn-group');
        $('.DTTT_container a').addClass('btn btn-default btn-md');

        $('.dataTables_filter input').attr("placeholder", "Search...");

    } );

    /* Datatables reorder */

    $(document).ready(function() {
        $('#datatable-reorder').DataTable( {
            dom: 'Rlfrtip'
        });

        $('#datatable-reorder_length').hide();
        $('#datatable-reorder_filter').hide();

    });

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
		
			$("#addND").click(function(){
				$(".modal-footer").html('<button type="button" class="btn btn-success" onclick="btnSaveND()"> Save </button>');
				$(".SelectShift").show();
				
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>/index.php/payroll/checkNDisSet",
						cache: false,
						success: function(res)
						{
							 
								$("#ShiftSelect").html(res);
 						} 
					});	
							
						});
			
    });
function ndModal(time_id,pnd_id){
  $("#txtIDTym").text(time_id);
	$(".modal-footer").html('<button type="button" class="btn btn-info" onclick="btnUpdateND()"> Update </button>');
				$(".SelectShift").hide();

 
	$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/payroll/checkND",
			data: {
 			time_id:time_id,
 			pnd_id:pnd_id,
 			},
			cache: false,
			success: function(html)
			{
			 var rs = html.split("|");
			 $("#ndTotalTime").val(rs[0]);
			 $("#ndTotalHolStart").val(rs[1]);
			 $("#ndTotalHolEnd").val(rs[2]);
			}
		});	
}
function btnUpdateND(){
		 
	 var nd = $("#ndTotalTime").val();
	 var ndSHol = $("#ndTotalHolStart").val();
	 var ndEHol = $("#ndTotalHolEnd").val();
	 var txtIDTym = $("#txtIDTym").text();
		$.ajax({
		type: "POST",
			url: "<?php echo base_url(); ?>/index.php/payroll/updateND",
			data: {
 			nd:nd,
 			ndSHol:ndSHol,
 			ndEHol:ndEHol,
 			txtIDTym:txtIDTym,
 			},
			cache: false,
			success: function(html)
			{ 
			 var rs = html.split("|");
			 $("#ndTotalTime").val(rs[0]);
			 $("#ndTotalHolStart").val(rs[1]);
			 $("#ndTotalHolEnd").val(rs[2]);
			 location.reload();
			 }
		});	
		
 }
function btnSaveND(){
		 
	 var nd = $("#ndTotalTime").val();
	 var ndSHol = $("#ndTotalHolStart").val();
	 var ndEHol = $("#ndTotalHolEnd").val();
	 var ShiftSelect = $("#ShiftSelect").val();
 		$.ajax({
		type: "POST",
			url: "<?php echo base_url(); ?>/index.php/payroll/addND",
			data: {
 			nd:nd,
 			ndSHol:ndSHol,
 			ndEHol:ndEHol,
 			ShiftSelect:ShiftSelect,
  			},
			cache: false,
			success: function(html)
			{ 
			 var rs = html.split("|");
			 $("#ndTotalTime").val(rs[0]);
			 $("#ndTotalHolStart").val(rs[1]);
			 $("#ndTotalHolEnd").val(rs[2]);
			 location.reload();
			 }
		});	
		
 }
</script>


<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>