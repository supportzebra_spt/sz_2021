<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  th{
    background:#666 !important;
    color:white !important;
    padding-top:10px;
    padding-bottom:10px;
}
.table-striped>tbody>tr:nth-child(odd)>td, 
.table-striped>tbody>tr:nth-child(odd)>th {
    background-color: #eee;
    color:#555;
    font-size:13px;
}
.table-striped>tbody>tr:nth-child(even)>td, 
.table-striped>tbody>tr:nth-child(even)>th {
    background-color: white;
    color:#777;
    font-size:13px;
}
body { padding-right: 0 !important }
</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
  }, 300);
 });

</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

<script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lightbox/css/lightbox.min.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/lightbox/js/lightbox.min.js"></script>
</head>

<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<div id="page-wrapper">
  <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
      <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
      <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
  </div>
  <div id="header-logo" class="logo-bg">
     <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
     <a id="close-sidebar" href="#" title="Close sidebar">
      <i class="glyph-icon icon-angle-left"></i>
  </a>
</div>
<div id='headerLeft'>
</div>

</div>
<div id="page-sidebar">
  <div class="scroll-sidebar">


    <ul id="sidebar-menu">
    </ul>
</div>
</div>
<div id="page-content-wrapper">
  <div id="page-content">

    <div id="page-title">
        <h2>Kudos Approval</h2>
        <hr>
        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap table-condensed">
            <thead class='font-bold'>
                <tr>
                    <th>Name</th>
                    <th>Campaign</th>
                    <th>Type</th>
                    <th>Reward</th>
                    <th>Date Added</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot class='font-bold'>
                <tr>
                    <th>Name</th>
                    <th>Campaign</th>
                    <th>Type</th>
                    <th>Reward</th>
                    <th>Date Added</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody class=" font-size-14">
                <?php foreach($kudos as $k => $val): ?>
                    <tr>
                        <td class='font-bold'><?php echo $val->ambassador; ?></td>
                        <td><?php echo $val->campaign; ?></td>
                        <td><?php echo $val->kudos_type; ?></td>
                        <td><?php if($val->reward_status=='Pending'){ ?>
                            <p style="color: red;" class='font-bold'><?php echo $val->reward_status; ?></p>
                            <?php }?>
                            <?php if($val->reward_status=='Requested'){ ?>
                            <p style="color: #f29341;" class='font-bold'><?php echo $val->reward_status; ?></p>
                            <?php }?>
                            <?php if($val->reward_status=='Released'){ ?>
                            <p style="color: green;" class='font-bold'><?php echo $val->reward_status; ?></p>
                            <?php }?>
                            <?php if($val->reward_status=='Approved'){ ?>
                            <p style="color: #41b2f4;" class='font-bold'><?php echo $val->reward_status; ?></p>
                            <?php }?>
                        </td>
                        <td><?php echo $val->date_given; ?></td>
                        <td style="text-align: center;">
                            <a href="#" class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#myModalUpdate" data-kudosid="<?php echo $val->kudos_id; ?>">
                                <span class="glyph-icon icon-separator">
                                    <i class="glyph-icon icon-list-alt"></i>
                                </span>
                                <span class="button-content">
                                    View
                                </span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div> 
</div>
</div>
</div>
</div>
<div class="modal fade" id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style='width:40%'>
        <div class="modal-content">
            <div class="modal-header pad20A" style='color:white;background:#333'>
                <h4 class="modal-title font-bold"><i id='kudos_icon'></i> <span id='kudos_type'></span> <span class='pull-right' id='date_added'></span></h4>
            </div>
            <div class="modal-body">
                <div id="call" style='display:none'>
                    <div class="row">
                        <div class="col-md-3 text-center">
                            <img src=""  alt="Image not found" id='cemployeepic' class='img-circle' style='border:2px solid #ddd;width:70%' />
                        </div>
                        <div class="col-md-9">
                            <h3 id='cambassador' class='font-bold' style='margin-bottom:5px'></h3> 
                            <h5 id='cposition'></h5>
                            <h5 id='cstatus'></h5>
                            <h5 id='ccampaign' class='font-bold font-size-13' style='margin-top:5px'></h5>
                        </div>
                    </div>
                    <hr>
                    <blockquote style='margin:0;' >
                        <p id='ccomment'></p>
                        <footer id='cfooter' class='font-size-12'></footer>
                    </blockquote>
                    <hr>  
                    <div class="row text-center">
                        <div class="col-md-4">
                            <h5 class='font-bold'>Reward Type</h5>
                            <h5 id='creward'></h5>
                        </div>
                        <div class="col-md-4">
                            <h5 class='font-bold'>Proof Reading</h5>
                            <h5 id='cproofreading'></h5>
                        </div>
                        <div class="col-md-4">
                            <h5 class='font-bold'>Card Status</h5>
                            <h5 id='ccardstatus'></h5>
                        </div>
                    </div> 
                </div>
                <div id='email' style='display:none'>
                   <div class="row">
                    <div class="col-md-3 text-center">
                        <img src=""  alt="Image not found" id='eemployeepic' class='img-circle' style='border:2px solid #ddd;width:70%' />
                    </div>
                    <div class="col-md-9">
                        <h3 id='eambassador' class='font-bold' style='margin-bottom:5px'></h3> 
                        <h5 id='eposition'></h5>
                        <h5 id='estatus'></h5>
                        <h5 id='ecampaign' class='font-bold font-size-13' style='margin-top:5px'></h5>
                    </div>
                </div>
                <hr>
                <div class="row text-center">
                    <div class="col-md-12">
                        <h5 class='font-bold'>Email Screenshot</h5><br>
                        <a href="images/image-1.jpg" id='escreenshot' data-lightbox="image-2" data-title="Email Screenshot" class='img-thumbnail'><img  src="" id='escreenshot2' style="width: 100%;"></a>
                    </div>
                </div>
                <hr> 
                <div class="row text-center">
                    <div class="col-md-4">
                        <h5 class='font-bold'>Reward Type</h5>
                        <h5 id='ereward'></h5>
                    </div>
                    <div class="col-md-4">
                        <h5 class='font-bold'>Proof Reading</h5>
                        <h5 id='eproofreading'></h5>
                    </div>
                    <div class="col-md-4">
                        <h5 class='font-bold'>Card Status</h5>
                        <h5 id='ecardstatus'></h5>
                    </div>
                </div> 
            </div>
            <hr>
            <div class="row ">
                <div class="col-md-12 text-center">
                    <h5 class='font-bold'>Request Note:</h5><br>
                    <h5 id='requestnote2'></h5>
                    <textarea row=3 id='requestnote' class='form-control rs'></textarea>
                </div>
                <div class='col-md-12 text-center' id='btndiv'>
                    <hr>
                    <a href="#" class="btn btn-info btn-sm" id='approve'>
                        <span class="glyph-icon icon-separator">
                            <i class="glyph-icon icon-thumbs-up"></i>
                        </span>
                        <span class="button-content">
                            Approve
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-sm" id='reject'>
                        <span class="glyph-icon icon-separator">
                            <i class="glyph-icon icon-thumbs-down"></i>
                        </span>
                        <span class="button-content">
                           Reject
                       </span>
                   </a>
               </div>
           </div>
           <div class="row" id='rejectnotediv' style='display:none'>
            <div class="col-md-12">
                <hr>
                <h5 class='font-bold'>Disapprove Kudos Note:</h5><br>
                <textarea class='form-control' id='rejectnote'></textarea>
                <br>
                <div class="text-right">
                    <a href="#" class="btn btn-info btn-sm" id='submitreject'>
                        <span class="glyph-icon icon-separator">
                            <i class="glyph-icon icon-thumbs-up"></i>
                        </span>
                        <span class="button-content">
                           Confirm Disapprove
                       </span>
                   </a>
                   <a href="#" class="btn btn-warning btn-sm" id='cancelreject'>
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-times"></i>
                    </span>
                    <span class="button-content">
                        Cancel
                    </span>
                </a>
            </div>

        </div>
    </div>
</div>
<div class="modal-footer"  style='color:white;background:#333'>
    <a href="#" class="btn btn-default" data-dismiss="modal">
        <span class="glyph-icon icon-separator">
            <i class="glyph-icon icon-times"></i>
        </span>
        <span class="button-content">
            Close
        </span>
    </a>
</div>
</div>
</div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
       }
   });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
     }
 });
});   
</script>
<script type="text/javascript">
    $('#myModalUpdate').on('show.bs.modal', function(e) {
        var kudosid = $(e.relatedTarget).data('kudosid');
        $("#call").hide();
        $("#email").hide();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Kudos/getKudos",
            data: {
                kudosid:kudosid
            },
            cache: false,
            success: function(res)
            {
                res = res.trim();
                res = JSON.parse(res);
                console.log(res);
                $("#submitreject").attr('data-kudosid',kudosid);
                $("#approve").attr('data-kudosid',kudosid);
                $("#kudos_type").html(res.kudos_type);
                $("#date_added").html(res.date_given);
                if(res.kudos_type=='Call'){
                    $("#kudos_icon").html("<i class='glyph-icon icon-phone'></i>");
                    $("#ccomment").html('"'+res.comment+'"');
                    $("#cfooter").html(res.client_name+", "+res.phone_number);

                    $("#ccampaign").html(res.acc_name);
                    $("#cambassador").html(res.lname+", "+res.fname);
                    $("#creward").html(res.reward_type);
                    $("#cproofreading").html(res.proofreading);
                    $("#ccardstatus").html(res.reward_status);
                    var img = new Image();
                    img.src = "<?php echo base_url()?>assets/images/"+res.pic;
                    if(img.height != 0){
                        $("#cemployeepic").attr('src',"<?php echo base_url()?>assets/images/"+res.pic);
                    }else{
                        $("#cemployeepic").attr('src',"<?php echo base_url('assets/images/sz.png'); ?>");
                    }

                    $("#cposition").html(res.pos_details);
                    $("#cstatus").html(res.status);

                    $("#call").show();
                }else{
                    $("#kudos_icon").html("<i class='glyph-icon icon-envelope'></i>");

                    $("#ecampaign").html(res.acc_name);
                    $("#eambassador").html(res.lname+", "+res.fname);
                    $("#ereward").html(res.reward_type);
                    $("#eproofreading").html(res.proofreading);
                    $("#ecardstatus").html(res.reward_status);
                    var img = new Image();
                    img.src = "<?php echo base_url()?>assets/images/"+res.pic;
                    if(img.height != 0){
                        $("#eemployeepic").attr('src',"<?php echo base_url()?>assets/images/"+res.pic);
                    }else{
                        $("#eemployeepic").attr('src',"<?php echo base_url('assets/images/sz.png'); ?>");
                    }

                    $("#eposition").html(res.pos_details);
                    $("#estatus").html(res.status);

                    $("#escreenshot").attr('href',"<?php echo base_url()?>"+res.file);
                    $("#escreenshot2").attr('src',"<?php echo base_url()?>"+res.file);
                    $("#email").show();
                }

                $("#requestnote2").html(res.request_note);
                if(res.reward_status=='Pending'){
                    $(".rs").show();
                    $("#requestnote2").hide();
                }else{
                    $(".rs").hide();
                    $("#requestnote2").show();
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $("#approve").click(function(){
        swal({
            title: 'KUDOS',
            type: 'info',
            html:
            'Are you sure to approve this kudos?',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText:
            '<i class="glyph-icon icon-check"></i> Confirm',
            cancelButtonText:
            '<i class="glyph-icon icon-times"></i>'
        }).then(function () {
            var kudosid = $('#approve').data('kudosid');
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>/index.php/Kudos/approvekudos",
              data:{
                kudosid:kudosid
            },
            cache: false,
            success: function(res)
            {
                if(res.trim()=='Success'){
                 swal(
                    'Kudos Approved!',
                    'Kudos Request have been approved and confirmed.',
                    'success'
                    )
             }else{
                swal(
                    'Error Occured!',
                    'Please try again later.',
                    'error'
                    )
            }
            window.setTimeout(function(){  
                location.reload();
            } ,2000);



        }
    });

        })
    });
    $("#reject").click(function(){
        $("#rejectnotediv").show(1000);
        $("#btndiv").hide(1000);
    });
    $("#cancelreject").click(function(){
        $("#rejectnotediv").hide(1000);
        $("#btndiv").show(1000);
    });
    $("#submitreject").click(function(){
        var kudosid = $(this).data('kudosid');
        var rejectnote = $("#rejectnote").val();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/rejectkudos",
          data:{
            rejectnote:rejectnote,
            kudosid:kudosid
        },
        cache: false,
        success: function(res)
        {
           if(res.trim()=='Success'){
             swal(
                'Kudos Rejected!',
                'Kudos Request have been disapproved.',
                'warning'
                )
         }else{
            swal(
                'Error Occured!',
                'Please try again later.',
                'error'
                )
        }
        window.setTimeout(function(){  
            location.reload();
        } ,2000);
    }
});
    });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>