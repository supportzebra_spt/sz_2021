<!DOCTYPE html>
<html lang="en" >
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8" />

        <title><?php echo $title . ' | SupportZebra'; ?></title>
        <meta name="description" content="Initialized via remote ajax json data">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin::Web font -->
        <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
                WebFont.load({
                    google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
                    active: function () {
                        sessionStorage.fonts = true;
                    }
                });
        </script>
        <!--end::Web font -->
        <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/img/favicon.ico" />


        <script>
                /*  (function (i, s, o, g, r, a, m) {
                 i['GoogleAnalyticsObject'] = r;
                 i[r] = i[r] || function () {
                 (i[r].q = i[r].q || []).push(arguments)
                 }, i[r].l = 1 * new Date();
                 a = s.createElement(o),
                 m = s.getElementsByTagName(o)[0];
                 a.async = 1;
                 a.src = g;
                 m.parentNode.insertBefore(a, m)
                 })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                 ga('create', 'UA-37564768-1', 'auto');
                 ga('send', 'pageview'); */
        </script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-3d.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/no-data-to-display.js"></script>
        <script src="https://code.highcharts.com/modules/drilldown.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/app.js" type="text/javascript"></script>
        <script>

                function noImage(id) {
                    $('.user-pic' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
                    $('.header-userpic' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
                }

        </script>
        <!--FOOTABLES -->

        <link href="<?php echo base_url(); ?>assets/src/custom/css/footable.standalone.min.css" rel="stylesheet" />
        <script src="<?php echo base_url(); ?>assets/src/custom/js/footable.min.js" ></script>


        <script src="<?php echo base_url(); ?>assets/src/custom/js/moment-timezone.js" ></script>
        <script src="<?php echo base_url(); ?>assets/src/custom/js/moment-timezone-with-data.js" ></script>
        <script src="<?php echo base_url('node_modules/socket.io-client/dist/socket.io.js'); ?>"></script>
        <script type="text/javascript">

                var socket = io('http://' + window.location.hostname + ':2509');

                socket.emit('new user', {
                    emp_id: <?php echo $this->session->emp_id; ?>,
                    uid: <?php echo $this->session->uid; ?>
                });
                function checkTime() {
                    socket.emit('requestTime', {});
                }

                function notification(notification_ids) {
//                    console.log(notification_ids);
                    $.each(notification_ids, function (i, id) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('Leave/get_notification_details'); ?>",
                            data: {
                                notification_ID: id
                            },
                            cache: false,
                            success: function (json) {
                                json = json.trim();
                                if (json.status === 'Empty') {
//                                    alert("NO DETAILS");
                                } else {

                                    var result = JSON.parse(json);
                                    $.each(result.notifications.recipients, function (x, item) {
                                        socket.emit('new notifications', {
                                            uid: item.recipient_ID,
                                            notification_ID: id,
                                            link: result.notifications.link,
                                            notificationRecipient_ID: item.notificationRecipient_ID,
                                            status: item.status,
                                            pic: result.notifications.pic,
                                            sender: result.notifications.sender,
                                            message: result.notifications.message,
                                            timeElapsed: item.timeElapsed,
                                            createdOn: item.createdOn,
                                            unreadCount: item.unreadCount
                                        });
                                    });
                                }
                            }
                        });
                    });
                }
                function systemNotification(systemNotification_ids) {
                    $.each(systemNotification_ids, function (i, id) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('Leave/get_system_notification_details'); ?>",
                            data: {
                                systemNotification_ID: id
                            },
                            cache: false,
                            success: function (json) {
                                json = json.trim();
                                var result = JSON.parse(json);
//                                console.log(result);
                                if (json.status === 'Empty') {
//                                    alert("NO DETAILS");
                                } else {
                                    //Required Data are: createdOn,link,notificationRecipient_ID,status,notification,unreadcount,timeElapsed
                                    $.each(result.notifications.recipients, function (x, item) {
                                        //  console.log(item);
                                        socket.emit('new system_notification', {
                                            uid: item.recipient_ID,
                                            systemNotification_ID: id,
                                            link: result.notifications.link,
                                            systemNotificationRecipient_ID: item.systemNotificationRecipient_ID,
                                            status: item.status,
                                            message: result.notifications.message,
                                            timeElapsed: item.timeElapsed,
                                            createdOn: item.createdOn,
                                            unreadCount: item.unreadCount
                                        });
                                    });
                                }
                            }
                        });
                    });
                }

        </script>
        <script type="text/javascript">
                function getPendingAppr(callback){
                    $.when(fetchGetData('/general/get_pending_appr_counts'), fetchGetData('/general/get_approval_links')).then(function(pendingApprCount, apprLinks) {
                        var pendingApprCountObj = jQuery.parseJSON(pendingApprCount[0].trim());
                        var apprLinksObj = jQuery.parseJSON(apprLinks[0].trim());
                        // var popoverBody = '<p><span class="text-dark" style="font-size: 17px;">6</span><i class="fa fa-clock-o pr-1 pl-1"></i>Time-in and Time-out Requests</p><p><i class="fa fa-hourglass-half pr-1"></i>Additional Hour Requests</p><p><i class="fa fa-calendar pr-1"></i>Leave Requests</p><p><i class="fa fa-calendar-times-o pr-1"></i>Retract Leave Request</p>';
                        var popoverBody = '';
                        console.log("Pending Appr Counts");
                        console.log(pendingApprCountObj);
                        console.log(apprLinksObj);
                        var totalPendingAppr = parseInt(pendingApprCountObj.ahr_appr_pending)+parseInt(pendingApprCountObj.dtr_appr_pending)+parseInt(pendingApprCountObj.leave_appr_pending)+parseInt(pendingApprCountObj.leave_retract_appr_pending);
                        if(totalPendingAppr == 0){
                            $('#pendingAppr').hide();
                        }else{
                            if(totalPendingAppr > 1){
                                $('#apprPendingWordForm').text('approvals');
                            }else{
                                $('#apprPendingWordForm').text('approval');
                            }
                            $('#totalPendingAppr').text(totalPendingAppr);
                            // TITO
                            if(parseInt(pendingApprCountObj.dtr_appr_pending) > 0){
                                popoverBody += '<p style="color:#6C6E8D"><span class="" style="font-size: 17px; color:#6C6E8D">'+pendingApprCountObj.dtr_appr_pending+'</span><i class="fa fa-clock-o pr-1 pl-1"></i><a href="'+baseUrl +'/'+apprLinksObj.dtr_appr_link+'">Time-in and Time-out Requests</a></p>';
                            }
                            // AHR
                            if(parseInt(pendingApprCountObj.ahr_appr_pending) > 0){
                                popoverBody += '<p style="color:#6C6E8D"><span class="" style="font-size: 17px; color:#6C6E8D">'+pendingApprCountObj.ahr_appr_pending+'</span><i class="fa fa-hourglass-half pr-1 pl-1"></i><a href="'+baseUrl +'/'+apprLinksObj.ahr_appr_link+'">Additional Hour Requests</a></p>';
                            }
                            // LEAVE
                            if(parseInt(pendingApprCountObj.leave_appr_pending) > 0){
                                popoverBody += '<p style="color:#6C6E8D"><span class="" style="font-size: 17px; color:#6C6E8D">'+pendingApprCountObj.leave_appr_pending+'</span><i class="fa fa-calendar pr-1 pl-1"></i><a href="'+baseUrl +'/'+apprLinksObj.leave_appr_link+'">Leave Requests</a></p>';
                            }
                            // LEAVE RETRACTION
                            if(parseInt(pendingApprCountObj.leave_retract_appr_pending) > 0){
                                popoverBody += '<p style="color:#6C6E8D"><span class="" style="font-size: 17px; color:#6C6E8D">'+pendingApprCountObj.leave_retract_appr_pending+'</span><i class="fa fa-calendar-times-o pr-1 pl-1"></i><a href="'+baseUrl +'/'+apprLinksObj.leave_retract_appr_link+'">Retract Leave Request</a></p>';
                            }
                            $('#pendingApprPopover').popover({
                                title: "Pending Approvals",
                                html: true,
                                trigger: 'focus',
                                container: 'body',
                                animation: true,
                                content: popoverBody
                                // skin: 'dark'
                            });
                            $('#pendingApprPopover').next('.popover').addClass('popover-danger');
                            // $('#pendingApprPopover').css(maxWidth: "100%");
                            $('#pendingApprPopoverVal').text(totalPendingAppr);
                            $('#pendingAppr').show();
                        }
                        callback(totalPendingAppr);
                    });
                }
                function getTodayDeadAppr(callback){
                    $.when(fetchGetData('/general/get_deadline_today_appr_counts'),fetchGetData('/general/get_approval_links')).then(function (todayDeadApprCount, apprLinks) {
                        var popoverBody = '';
                        var todayDeadApprCountObj = jQuery.parseJSON(todayDeadApprCount[0].trim());
                        var apprLinksObj = jQuery.parseJSON(apprLinks[0].trim());

                        console.log("Deadine Appr Counts");
                        console.log(todayDeadApprCountObj);
                        var todayDeadAppr = parseInt(todayDeadApprCountObj.ahr_appr_today)+parseInt(todayDeadApprCountObj.dtr_appr_today)+parseInt(todayDeadApprCountObj.leave_appr_today)+parseInt(todayDeadApprCountObj.leave_retract_appr_today);
                        if(todayDeadAppr == 0){
                            $('#deadTodayAppr').hide();
                        }else{
                            $('#totalDeadToday').text(todayDeadAppr);
                            if(todayDeadAppr > 1){
                                $('#apprDeadWordForm').text('approvals');
                            }else{
                                $('#apprDeadWordForm').text('approval');
                            }
                            // TITO
                            if(parseInt(todayDeadApprCountObj.dtr_appr_today) > 0){
                                popoverBody += '<p style="color:#6C6E8D"><span class="" style="font-size: 17px; color:#6C6E8D">'+todayDeadApprCountObj.dtr_appr_today+'</span><i class="fa fa-clock-o pr-1 pl-1"></i><a href="'+baseUrl +'/'+apprLinksObj.dtr_appr_link+'">Time-in and Time-out Requests</a></p>';
                            }
                            // AHR
                            if(parseInt(todayDeadApprCountObj.ahr_appr_today) > 0){
                                popoverBody += '<p style="color:#6C6E8D"><span class="" style="font-size: 17px; color:#6C6E8D">'+todayDeadApprCountObj.ahr_appr_today+'</span><i class="fa fa-hourglass-half pr-1 pl-1"></i><a href="'+baseUrl +'/'+apprLinksObj.ahr_appr_link+'">Additional Hour Requests</a></p>';
                            }
                            // LEAVE
                            if(parseInt(todayDeadApprCountObj.leave_appr_today) > 0){
                                popoverBody += '<p style="color:#6C6E8D"><span class="" style="font-size: 17px; color:#6C6E8D">'+todayDeadApprCountObj.leave_appr_today+'</span><i class="fa fa-calendar pr-1 pl-1"></i><a href="'+baseUrl +'/'+apprLinksObj.leave_appr_link+'">Leave Requests</a></p>';
                            }
                            // LEAVE RETRACTION
                            if(parseInt(todayDeadApprCountObj.leave_retract_appr_today) > 0){
                                popoverBody += '<p style="color:#6C6E8D"><span class="" style="font-size: 17px; color:#6C6E8D">'+todayDeadApprCountObj.leave_retract_appr_today+'</span><i class="fa fa-calendar-times-o pr-1 pl-1"></i><a href="'+baseUrl +'/'+apprLinksObj.leave_retract_appr_link+'">Retract Leave Request</a></p>';
                            }
                            console.log(popoverBody)
                            $('#deadTodayApprPopover').popover({
                                title: "Due by 11:59 PM Today",
                                html: true,
                                trigger: 'focus',
                                container: 'body',
                                animation: true,
                                content: popoverBody
                            });
                            $('#deadTodayApprPopoverVal').text(todayDeadAppr);
                            $('#deadTodayAppr').show();
                        }
                        callback(todayDeadAppr);
                    });
                }
                function getApproversDeadline(callback) {
                    $.when(fetchGetData('/general/get_missed_deadline')).then(function (deadline) {
                        var dueDeadline = jQuery.parseJSON(deadline.trim());
                        // console.log(dueDeadline);
                        if (parseInt(dueDeadline.has_deadline)) {
                            ;
                            var systNotifIds = []
                            $.each(dueDeadline.missed_requests, function (index, values) {
                                $.each(values.missed_approvers, function (index1, values1) {
                                    if (values1.notify_follow_approver == undefined) {
                                        console.log('no follow');
                                    } else {
                                        systNotifIds.push(values1.notify_follow_approver.notif_id[0]);
                                    }
                                    systNotifIds.push(values1.notify_missed_approver.notif_id[0]);
                                    systNotifIds.push(values1.notify_requestor.notif_id[0]);

                                    if (values1.notify_preceding_approvers == undefined) {
                                        console.log('no preceding');
                                    } else {
                                        systNotifIds.push(values1.notify_preceding_approvers.notif_id[0]);
                                    }
                                    if (values1.notify_default_approver == undefined) {
                                        console.log('no default');
                                    } else {
                                        systNotifIds.push(values1.notify_default_approver.notif_id[0]);
                                    }
                                })
                            })
                            // console.log(systNotifIds);
                            systemNotification(systNotifIds);
                        } else {
                            // console.log('No due deadlines')
                        }
                        callback(dueDeadline);
                    });
                }

                function getPersonalDeadline(callback) {
                    $.when(fetchGetData('/general/get_missed_deadline_requestor')).then(function (deadline) {
                        var dueDeadline = jQuery.parseJSON(deadline.trim());
                        // console.log(dueDeadline);
                        if (parseInt(dueDeadline.has_deadline)) {
                            ;
                            var systNotifIds = []
                            $.each(dueDeadline.missed_requests, function (index, values) {
                                $.each(values.missed_approvers, function (index1, values1) {
                                    console.log(values1);
                                    if (values1.notify_follow_approver == undefined) {
                                        console.log('no follow');
                                    } else {
                                        systNotifIds.push(values1.notify_follow_approver.notif_id[0]);
                                    }
                                    systNotifIds.push(values1.notify_missed_approver.notif_id[0]);
                                    systNotifIds.push(values1.notify_requestor.notif_id[0]);

                                    if (values1.notify_preceding_approvers == undefined) {
                                        console.log('no preceding');
                                    } else {
                                        systNotifIds.push(values1.notify_preceding_approvers.notif_id[0]);
                                    }
                                    if (values1.notify_default_approver == undefined) {
                                        console.log('no default');
                                    } else {
                                        systNotifIds.push(values1.notify_default_approver.notif_id[0]);
                                    }
                                })
                            })
                            // console.log(systNotifIds);
                            systemNotification(systNotifIds);
                        } else {
                            // console.log('No due deadlines')
                        }
                        callback(dueDeadline);
                    });
                }

                function getMonitoringMissed(callback) {
                    $.when(fetchGetData('/general/get_missed_deadline_monitoring')).then(function (deadline) {
                        var dueDeadline = jQuery.parseJSON(deadline.trim());
                        // console.log(dueDeadline);
                        if (parseInt(dueDeadline.has_deadline)) {
                            ;
                            var systNotifIds = []
                            $.each(dueDeadline.missed_requests, function (index, values) {
                                $.each(values.missed_approvers, function (index1, values1) {
                                    console.log(values1);
                                    if (values1.notify_follow_approver == undefined) {
                                        console.log('no follow');
                                    } else {
                                        systNotifIds.push(values1.notify_follow_approver.notif_id[0]);
                                    }
                                    systNotifIds.push(values1.notify_missed_approver.notif_id[0]);
                                    systNotifIds.push(values1.notify_requestor.notif_id[0]);

                                    if (values1.notify_preceding_approvers == undefined) {
                                        console.log('no preceding');
                                    } else {
                                        systNotifIds.push(values1.notify_preceding_approvers.notif_id[0]);
                                    }
                                    if (values1.notify_default_approver == undefined) {
                                        console.log('no default');
                                    } else {
                                        systNotifIds.push(values1.notify_default_approver.notif_id[0]);
                                    }
                                })
                            })
                            // console.log(systNotifIds);
                            systemNotification(systNotifIds);
                        } else {
                            // console.log('No due deadlines')
                        }
                        callback(dueDeadline);
                    });
                }

                function reminder() {
                    toastr.success('You have pending approvals');
                }

                function checkDeadline() {
                    getApproversDeadline(function (approvalDeadlines) {
                        getPersonalDeadline(function (personalDeadlines) {
                            getMonitoringMissed(function (monitoringDeadlines) {})
                        });
                    });
                }

        </script>
        <script>
                function action() {
                    swal({
                        title: "You've been \"IDLE\" for a moment..",
                        text: "Please choose an action to proceed.",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: "<span><i class='la la-sign-out'></i><span>Logout</span></span>",
                        confirmButtonClass: "btn btn-danger m-btn m-btn--pill m-btn--air m-btn--icon",
                        showCancelButton: true,
                        cancelButtonText: "<span><i class='la la-refresh'></i><span>Reload!</span></span>",
                        cancelButtonClass: "btn btn-info m-btn m-btn--pill m-btn--icon",
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        allowOutsideClick: false

                    }).then((result) => {
                        if (result.value) {
                            location.href = "<?php echo base_url("/login/log_out"); ?>";

                        } else {
                            location.reload();
                        }
                    })
                }

                $(document).ready(function () {
                    checkDeadline();

                    setInterval(checkTime, 1000); // This will run checkTime every second
                 
                    $(document).idleTimer(3000000); // 1000 = 1sec ; set to 3 minute for ambassador

                });
                $(document).on("idle.idleTimer", function (event, elem, obj) {
                    action();
                });
                $(document).on("active.idleTimer", function (event, elem, obj, triggerevent) {
                    action();
                });
//                document.onkeydown = function (e) {
//                    if (e.keyCode === 123) {
//                        return false;
//                    }
//                    if (e.ctrlKey && e.shiftKey && e.keyCode === 'I'.charCodeAt(0)) {
//                        return false;
//                    }
//                    if (e.ctrlKey && e.shiftKey && e.keyCode === 'C'.charCodeAt(0)) {
//                        return false;
//                    }
//                    if (e.ctrlKey && e.shiftKey && e.keyCode === 'J'.charCodeAt(0)) {
//                        return false;
//                    }
//                    if (e.ctrlKey && e.keyCode === 'U'.charCodeAt(0)) {
//                        return false;
//                    }
//                };
        </script>

    </head>
     <!--oncontextmenu="return false"-->
    <body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default" style="padding-right:0 !Important" >
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">