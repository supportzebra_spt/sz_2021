
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>

<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
      
            <li class="m-menu__item <?php echo (isset($uri_segment[1]) AND $uri_segment[1] == "dashboard") ? 'm-menu__item--active' : '' ?> " aria-haspopup="true" >
                <a  href="<?php echo base_url('dashboard'); ?>" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap"><span class="m-menu__link-text">Dashboard</span></span>
                    </span>
                </a>
            </li>


            <li class="m-menu__item  <?php echo (isset($uri_segment[1]) AND $uri_segment[1] == "szfive") ? 'm-menu__item--active' : '' ?>" aria-haspopup="true">
                <a  href="<?php echo base_url('szfive'); ?>" class="m-menu__link m-menu__toggle"><span style="width: 35px; display: table-cell;"><img src="<?php echo base_url('assets/images/img/hi5_white.png'); ?>" width="30" style="margin-left: -5px;"></span><span class="m-menu__link-text">SnapSZ</span></a>
            </li>
            <li class="m-menu__item  <?php echo (isset($uri_segment[2]) AND $uri_segment[2] == "attendance") ? 'm-menu__item--active' : '' ?>" aria-haspopup="true">
                <a  href="<?php echo base_url('dtr/attendance'); ?>" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-calendar-3"></i> <span class="m-menu__link-text">Attendance</span></a>
            </li>
            <li class="m-menu__item <?php echo (isset($uri_segment[2]) AND $uri_segment[2] == "request") ? 'm-menu__item--submenu m-menu__item--open m-menu__item--expanded' : '' ?>" aria-haspopup="true">
                <a  href="#" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-calendar-3"></i><span class="m-menu__link-text">REQUEST</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">

                        <li class="m-menu__item <?php echo (isset($uri_segment[2]) AND ( strtolower($uri_segment[1]) == "additional_hour" && strtolower($uri_segment[2]) == "request")) ? 'm-menu__item--active' : '' ?>" aria-haspopup="true">
                            <a href="<?php echo base_url('additional_hour/request'); ?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">AHR</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php echo (isset($uri_segment[2]) AND ( strtolower($uri_segment[1]) == "leave" && strtolower($uri_segment[2]) == "request")) ? 'm-menu__item--active' : '' ?>" aria-haspopup="true">
                            <a href="<?php echo base_url('leave/request'); ?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">LEAVE</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php echo (isset($uri_segment[2]) AND ( strtolower($uri_segment[1]) == "dtr" && strtolower($uri_segment[2]) == "myrequest")) ? 'm-menu__item--active' : '' ?>" aria-haspopup="true">
                            <a href="<?php echo base_url('dtr/myrequest'); ?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">T.I.T.O.</span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>
            <hr/>
            <?php
            if (count($res) > 0)
            {
                ?>			 
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">Components</h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>

            <?php } ?>
            <?php
            foreach ($res as $row => $val)
            {
                $menu = explode("|", $row);
                ?>

                <li class="m-menu__item <?php echo (isset($uri_segment[1]) AND $uri_segment[1] == strtolower(str_replace(" ", "_", $menu[0]))) ? 'm-menu__item--submenu m-menu__item--open m-menu__item--expanded' : '' ?>" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  href="#" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-calendar-3"></i><span class="m-menu__link-text"><?php echo $menu[0]; ?></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <?php
                            foreach ($val as $row2)
                            {
                                $link = explode("/", $row2->item_link);
                                ?>
                                <li class="m-menu__item <?php echo (isset($uri_segment[2]) AND $uri_segment[2] === $link[1]) ? 'm-menu__item--active' : '' ?>" aria-haspopup="true">
                                    <a href="<?php echo base_url() . $row2->item_link; ?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?php echo $row2->item_title; ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
            <?php } ?>

        </ul>

    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js" type="text/javascript"></script>
