<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript">
  $(window).load(function(){
   setTimeout(function() {
    $('#loading').fadeOut( 400, "linear" );
  }, 300);
 });

</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs/tabs.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
</head>

<body>
  <div id="sb-site">
    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>
    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">
        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">
        <div id="page-title">
          <h2>Campaign Notifications</h2>
          <p>Update the reminders and notifications of each campaigns</p> <hr>
          <div class="row">
            <div class="col-md-3">
              <select class='form-control' id='campaign'>
                <option value=''>--</option>                
                <?php $x=0;foreach($campaigns as $camp):?>
                <option value='<?php echo $camp->acc_id;?>'><?php echo $camp->acc_name;?></option>
                <?php $x++;endforeach;?>
              </select>

            </div>
            <div class="col-md-9">
              <div class="panel" style='display:none' id='paneldisplay'>
                <div class="panel-heading">
                 <b class='font-size-15'><span id='heading'></span></b>
                 <span class='pull-right'>
                  <button class='btn btn-primary btn-xs' id='btncreate' data-toggle='modal' data-target="#createmodal">Create</button>
                  <button class='btn btn-info btn-xs' id='btnchange'  data-toggle='modal' data-target="#changemodal">Change</button>
                  <button class='btn btn-warning btn-xs' id='btnupdate'  data-toggle='modal' data-target="#updatemodal">Update</button>
                  <button class='btn btn-danger btn-xs' id='btndeactivate'>Deactivate</button>
                </span>
              </div>
              <div class="panel-body">
                <h5 id='boding'></h5>

                <p id='dateposted'></p>
              </div>
            </div>
          </div>
        </div>
      <!--     
         
          <div class="content-box">
            <h3 class="content-box-header bg-white">
              <i class="glyph-icon icon-cog"></i>
              List of Current Message Titles
              <div class="header-buttons">
                <button class="btn btn-xs btn-primary" data-toggle='modal' data-target="#createmodal">Create Message +</button>
              </div>
            </h3>
            <div class="content-box-wrapper">
             <div class="example-box-wrapper">
              <div class="panel-group" id="accordion">
                <?php $x=1;foreach($values as $val):?>
                <div class="panel">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $x;?>">
                        <?php echo $val->acc_name;?>
                      </a>
                    </h4>
                  </div>
                  <div id="collapse<?php echo $x;?>" class="panel-collapse collapse">
                    <div class="panel-body">

                     <h5> <?php echo $val->title;?></h5>
                     <p><?php echo $val->message;?></p>
                   </div>
                 </div>
               </div>
               <?php $x++;endforeach;?>
             </div>
           </div>
         </div>
       </div> -->
     </div> 
   </div>
 </div>
</div>
</div>

<div class="modal fade" id="createmodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">New Message +</h4>
      </div>
      <div class="modal-body">
        <div class="row"> 
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Title</label>
              <input type="text" class='form-control' id='title'>
            </div>
          </div>
          <hr>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Message</label>
              <textarea class='form-control' id='message' rows=15></textarea>
            </div>
          </div>
          <input type="hidden" id='acc_id_create'>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id='createbtn' class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="changemodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Change Active Notification</h4>
      </div>
      <div class="modal-body">
        <div class="row"> 
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Title</label>
              <select class='form-control' id='cn_id_change'>

              </select>
            </div>
          </div>
          <input type="hidden" id='acc_id_change'>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id='changebtn' class="btn btn-primary">Change</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Update Notification Details</h4>
      </div>
      <div class="modal-body">
        <div class="row"> 
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Title</label>
              <input type="text" class='form-control' id='utitle'>
            </div>
          </div>
          <hr>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Message</label>
              <textarea class='form-control' id='umessage' rows=15></textarea>
            </div>
          </div>
        </div>
        <input type="hidden" name="" id="cn_id_update">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id='updatebtn' class="btn btn-primary">Update</button>
      </div>
    </div>
  </div>
</div>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {
        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {
        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $("#createbtn").click(function(){
    var title = $("#title").val();
    var message = $("#message").val();
    var campaign = $("#acc_id_create").val();

    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/createnewcampnotif",
      data:{
        title:title,
        message:message,
        acc_id:campaign
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        if(res=='Success'){
          swal('Success','Successfully create new notification.','success');
        }else{
          swal('Error','Failed creating a new notification.','error');
        }
        $("#title").val('');
        $("#message").val('');        
        $('#createmodal').modal('hide');
      }
    });
  });
</script>
<script type="text/javascript">
  $("#changebtn").click(function(){
    var cn_id = $("#cn_id_change").val();
    var acc_id = $("#acc_id_change").val();

    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/updatecampnotifActive",
      data:{
        cn_id:cn_id,
        acc_id:acc_id
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        console.log(res);
        if(res=='Success'){
          swal('Success','Successfully Updated Active Notification.','success');
        }else{
          swal('Error','Failed updating active notification.','error');
        }
        $("#campaign").change();
        $('#changemodal').modal('hide');
      }
    });
  });
</script>
<script type="text/javascript">
  $("#updatebtn").click(function(){
    var title = $("#utitle").val();
    var message = $("#umessage").val();
    var cn_id = $("#cn_id_update").val();

    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/updatenotifdetails",
      data:{
        title:title,
        message:message,
        cn_id:cn_id
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        if(res=='Success'){
          swal('Success','Successfully Updated notification.','success');
        }else{
          swal('Error','Failed updating notification.','error');
        }
        $("#utitle").val('');
        $("#umessage").val('');        
        $("#campaign").change();
        $('#updatemodal').modal('hide');
      }
    });
  });
</script>
<script type="text/javascript">
  $("#btndeactivate").click(function(){
    var cn_id = $(this).data('cn_id');

    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/updatecampnotifInactive",
      data:{
        cn_id:cn_id
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        console.log(res);
        if(res=='Success'){
          swal('Success','Successfully Inactivate Notification.','success');
        }else{
          swal('Error','Failed Inactivate notification.','error');
        }
        $("#campaign").change();
      }
    });
  });
</script>
<script type="text/javascript">
  $('#createmodal').on('show.bs.modal', function(e) {
    var acc_id = $(e.relatedTarget).data('acc_id');
    $("#acc_id_create").val(acc_id);
  });
</script>
<script type="text/javascript">
  $('#updatemodal').on('show.bs.modal', function(e) {
    var cn_id = $(e.relatedTarget).data('cn_id');
    $("#cn_id_update").val(cn_id);

    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/getNotifdetails/"+cn_id,
      cache: false,
      success: function(res)
      {
        res = res.trim();
        res = JSON.parse(res); 
        if(res=='Empty'){

        }else{
          $("#utitle").val(res.title);
          $("#umessage").val(res.message);
        }
      }
    });
  });
</script>
<script type="text/javascript">
  $('#changemodal').on('show.bs.modal', function(e) {
    var acc_id = $(e.relatedTarget).data('acc_id');
    $("#acc_id_change").val(acc_id);
    // alert(acc_id);
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/getAccNotificationList/"+acc_id,
      cache: false,
      success: function(res)
      {
        res = res.trim(); 
        $("#cn_id_change").html("");
        var body = "";
        body += "<option value='' selected></option>";
        $.each(JSON.parse(res),function(i,item){
         if(item.isActive==1){
           body += "<option value='"+item.cn_id+"' selected>"+item.title+"</option>";
         }else{
           body += "<option value='"+item.cn_id+"'>"+item.title+"</option>";
         }
       })
        $("#cn_id_change").html(body);
      }
    });
  });
</script>
<script type="text/javascript">
  $("#campaign").on('change',function(){
    var acc_id = $(this).val();
    if(acc_id==''){
      $("#paneldisplay").hide();
    }else{
      $("#paneldisplay").show();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Survey/getNotificationPerCampaign/"+acc_id,
        cache: false,
        success: function(res)
        {
          res = res.trim();
          console.log(res);
          if(res=='Empty'){
            $("#heading").html('&nbsp;');
            $("#boding").html('<b class="font-orange">No Active Notification</b>');
            $("#dateposted").html('');
            $("#btnupdate").prop('disabled',true);
            $("#btndeactivate").prop('disabled',true);
          }else{
            res = JSON.parse(res);
            $("#heading").html(res.title);
            $("#boding").html(res.message);
            var date = moment(res.date_posted).format('MMMM Do YYYY, h:mm a');
            $("#dateposted").html("Posted On: "+date);
            $("#btnupdate").prop('disabled',false);
            $("#btndeactivate").prop('disabled',false);
            $("#btndeactivate").data('cn_id',res.cn_id);
            $("#btnupdate").data('cn_id',res.cn_id);
          }

          $("#btnchange").data('acc_id',acc_id);
          $("#btncreate").data('acc_id',acc_id);
        }
      });
    }
    
  });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>