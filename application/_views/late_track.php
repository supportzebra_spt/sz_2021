<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
 
 <script type="text/javascript">
   $(function(){
    "use strict";
    var end = moment().format("MM/DD/YYYY");
    var m = moment().format("MM/DD/YYYY");
    $("#daterangepicker-example").daterangepicker({ minDate: m,startDate: end,maxDate : end});
   });
</script>
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="<?php echo $setting['settings']; ?>">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
              <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>

        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
<div class="form-group">
                    <div class="col-sm-6">
                        <div class="row" style="width: 712px !important;">
                            <div class="col-md-6">
                            <label class="col-sm-3 control-label">Type</label>
                                <select single="" class="form-control" id="select-type">
                                    <option value="--">--</option>
                                    <option value="Admin">Admin</option>
                                    <option value="Agent">Agent</option>
                                </select>
                                <label class="col-sm-3 control-label">Account</label>
                                    <select single="" class="form-control" id="select-account">
                                </select>
                            </div>
                            <div class="col-md-6">
                               <label class="col-sm-3 control-label" style="width: 150px !important;">Date From - To:</label>
                               <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon">
                                        <i class="glyph-icon icon-calendar"></i>
                                    </span>
                                    <input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control" value="01/01/2017 - 01/15/2017">
                                </div>
                                <div class="title-hero" style="    padding-left: 1px;padding-top: 22px;">
                                     <span><button id="submit_view" type="button" class="btn btn-primary">VIEW</button></span>
                                      <span><a class="btn btn-primary active" href="latetracking" role="button">REFRESH</a></span>
                                      <span> <button id="submit_export" type="button" class="btn btn-primary">EXPORT</button> </span> 
                                </div>

                            </div>
                        </div>
										<div class="example-box-wrapper" style='width:200%'>
						<div id="loadingIMG" style="display: block;position: absolute;margin-left: 35%;">
							<center><img src="<?php echo base_url();?>assets/images/loading-gears-animation-3.gif"  ></center>
						</div>

<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">



<tbody>

</tbody>

</table>
</div>
                    </div>
	
                </div>
<div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
	$.ajax({
		type: "POST",
		url: "Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
  $.ajax({
		type: "POST",
		url: "Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		 // alert(html);
		}
  });
 
  });	
</script>
<script>
  $(function(){
	    $("#loadingIMG").hide();
 $('#submit_export').click(function(){
	var accType = $('#select-type').val();
var account = $('#select-account').val();
var date = $('#daterangepicker-example').val();
 var dataString = 'accType='+ accType+'&account='+ account+'&date='+ date;
	$.ajax({
type: "POST",
url: "latetracking/export",
data: dataString,
cache: false,
 beforeSend: function(){
        $('#loadingIMG').show();
		$(".container").css("opacity",".5");
    },
    complete: function(){
         $('#loadingIMG').fadeOut("slow");
			$(".container").css("opacity","1");

    },
success: function(result)
{ 
  window.location = "latetracking/exportExcel";
 
}
});
});
$('#submit_view').click(function(){
 var accType = $('#select-type').val();
var account = $('#select-account').val();
var date = $('#daterangepicker-example').val();
 var dataString = 'accType='+ accType+'&account='+ account+'&date='+ date;
           $('#datatable-responsive').empty();

		    $.ajax({
type: "POST",
url: "latetracking/get_emp_dtr_details",
data: dataString,
cache: false,
 beforeSend: function(){
        $('#loadingIMG').show();
		$(".container").css("opacity",".5");
    },
    complete: function(){
         $('#loadingIMG').fadeOut("slow");
			$(".container").css("opacity","1");

    },
success: function(result)
{ 
      //  console.log(result); // to see the object
var obj = jQuery.parseJSON(result);
      var tablehead = "<thead><tr role='row'>";
               $(obj.headers).each(function(key, value) 
                    {

					tablehead += "<th class='sorting' tabindex='0' aria-controls='datatable-responsive' rowspan='1' colspan='1' style='width: 99px;width: 99px;background: #00b19f;color: white;' aria-sort='ascending' aria-label="+value+": activate to sort column ascending"+" >"+value+"</th>";  });
					tablehead +='</tr></thead>';
                $("#datatable-responsive").append(tablehead);

             var objects = obj.user; 
                var tablebody_emp = "<tbody>";
                var count = 0;
                var num;
                    for(var key in objects) 
                    {
                        var value = objects[key];     
                        var objt = value;
                      
                        for(var k in objt) 
                            {
                                var val = objt[k];
                                 var check = Date.parse(k);
                                 if(check)
                                 {
                                    var ob = val;

                                     tablebody_emp += "<tr style='color:black !important;' ><td>"+k+"</td>";

                                    for(var i in ob) 
                                        {
                                           tablebody_emp+="<td>"+ob[i]+"</td>";
                                        }
                                         tablebody_emp+="</tr>";
                                 }
                                 else
                                 {
                                     tablebody_emp += "<tr><td class='dataTables_empty' colspan='9' valign='top' style='color: black !important;background: #c2efeb;'> <b>"+val+"</b></td></tr>";
                                 }
                            }
                    }
                    tablebody_emp += "</tbody>";
                 $("#datatable-responsive").append(tablebody_emp);      
 }
  });	
});
$('#submit_refresh').click(function(){
location.reload();
});
$('#select-type').change(function(){
 	var accType= $(this).val(); 
 	
 var dataString = 'accType='+ accType;

		    $.ajax({
type: "POST",
url: "latetracking/get_account",
data: dataString,
cache: false,
 beforeSend: function(){
        $('#loadingIMG').show();
		$(".container").css("opacity",".5");
    },
    complete: function(){
         $('#loadingIMG').fadeOut("slow");
			$(".container").css("opacity","1");

    },
success: function(html)
{
$('#select-account').html(html);
}
  });	  
		  
	
});


  });
  
  </script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>