<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        /* Loading Spinner */
        
        .spinner {
            margin: 0;
            width: 70px;
            height: 18px;
            margin: -35px 0 0 -9px;
            position: absolute;
            top: 50%;
            left: 50%;
            text-align: center
        }
        
        .spinner>div {
            width: 18px;
            height: 18px;
            background-color: #333;
            border-radius: 100%;
            display: inline-block;
            -webkit-animation: bouncedelay 1.4s infinite ease-in-out;
            animation: bouncedelay 1.4s infinite ease-in-out;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both
        }
        
        .spinner .bounce1 {
            -webkit-animation-delay: -.32s;
            animation-delay: -.32s
        }
        
        .spinner .bounce2 {
            -webkit-animation-delay: -.16s;
            animation-delay: -.16s
        }
        
        @-webkit-keyframes bouncedelay {
            0%,
            80%,
            100% {
                -webkit-transform: scale(0.0)
            }
            40% {
                -webkit-transform: scale(1.0)
            }
        }
        
        @keyframes bouncedelay {
            0%,
            80%,
            100% {
                transform: scale(0.0);
                -webkit-transform: scale(0.0)
            }
            40% {
                transform: scale(1.0);
                -webkit-transform: scale(1.0)
            }
        }
        
        #container {
            width: 150px;
            border: 1px solid #bbbbbb;
            padding: 20px;
            float: left;
            text-align: center;
            margin-right: 7px;
            margin-top: 7px;
            height: 180px;
            border-radius: 10px;
        }
        
        div#container:hover {
            border: 1px solid rgba(0, 150, 136, 0.35);
            background: rgba(0, 150, 136, 0.19);
            color: #009688;
            cursor: pointer;
        }
        
        .time ul {
            list-style-type: none;
            margin-left: -39px;
        }
        
        .note {
            font-size: initial;
            background: #4a4a4a;
            padding: 10px;
        }
        
        .timeDetails {
            padding: 10px;
            background: gray;
        }
        
        .time {
            border-radius: 13px;
            color: white;
            font-family: fantasy;
            display: inline-flex;
        }
        
        .highcharts-credits {
            display: none;
        }
        
        .ui-datepicker {
            padding: 10px;
        }
        
        .content-box .content-box-wrapper {
            line-height: 1.6em !important;
            padding: 7px !important;
        }
        
        div#wordWarning,
        div#alreadyFiled .content-box-wrapper {
            color: #803e0e;
            border-color: #dc6d0c;
            background: #e4ac7a;
            border-left: 10px solid #e67b1b;
            border-right: 1px solid #e67b1b;
            border-top: 1px solid #e67b1b;
            border-bottom: 1px solid #e67b1b;
        }
        
        div#noSched,
        div#noLogs,
        div#notEnoughTime,
        div#undertime,
        div#late {
            color: #b50909;
            border-color: #da0707;
            background: #f3c3c3;
            border-left: 10px solid #e81d1d;
            border-right: 1px solid #af0707;
            border-top: 1px solid #d00505;
            border-bottom: 1px solid #a00606;
        }
        
        table.ui-datepicker-calendar {
            font-size: 13px !important;
            line-height: 1.6em !important;
            width: 92% !important;
            margin: 0% !important;
            border-collapse: collapse !important;
        }
        
        div#time {
            padding: 0px;
        }
        
        div#specificDetails {
            color: #ffffff;
            margin-bottom: -10px;
            padding: 5px;
        }
        
        label.control-label.col-sm-4 {
            padding-top: 8px;
        }
        
        .modal-header {
            background: #484545;
            color: white;
        }
        
        .modal-footer {
            background: #484545;
        }
        
        .character-remaining.clear.input-description {
            font-size: 14px;
            margin-top: 5px;
            font-family: monospace;
            color: #ad2929;
        }
        
        small.help-block {
            font-size: 15px;
            float: left;
            color: red;
            margin-top: -23px;
            font-family: fantasy;
        }
        
        #requestPreDetails label {
            color: #9fa2a7;
        }
        
        .daterangepicker.dropdown-menu.ltr.show-calendar.opensleft {
            top: 341px;
            left: 584.578px;
            right: auto;
            display: block;
        }
        
        .checkbox-inline label,
        .radio-inline label {
            font-weight: 400;
            line-height: 7px !important;
        }
        
        div#basic-dialog {
            height: 150px !important;
            min-width: 100px !important;
            width: 274px;
            min-height: 95px;
            max-height: none;
        }
        
        .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.ui-dialog-buttons.ui-resizable {
            min-height: auto !important;
            /*width: auto !important;*/
            height: auto !important;
            width: 274px !important;
            top: 374.5px !important;
            left: 9.5px !important;
            position: fixed !important;
        }
        
        div#dialogContent {
            padding: 24px;
        }
        
        .table-hover tbody tr:hover td,
        .table-hover tbody tr:hover th {
            background-color: #eef0f1 !important;
        }
        
        table {
            border: 1px solid gainsboro;
        }
        
        a.btn.bg-primary.pull-right {
            margin-top: -13px;
        }
        
        a#requestBtn:hover {
            background-color: #187169;
            color: #e4dede;
        }
        
        #requestPreDetails label {
            color: #9fa2a7;
            margin-bottom: 13px;
        }
        
        #approversBody>tbody>tr>td {
            vertical-align: middle;
        }
        
        .noteIcon:hover {
            color: black;
        }
        
        .chosen-container-single .chosen-single span {
            height: 31px !important;
            line-height: 2.6em !important;
        }
        
        div#accountlist_chosen, div#positionlist_chosen {
            height: 35px !important;
        }
        
        .chosen-container-single .chosen-single div {
            height: 34px !important;
            line-height: 34px !important;
        }
    </style>

    <title>Position Assignment</title>

    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">


    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/datatable/datatable.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/BootstrapValidator/dist/css/bootstrapValidator.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/myCss/bootstrap-toggle.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/jgrowl-notifications/jgrowl.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/jgrowl-notifications/jgrowl.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/iconic/iconic.css">
    <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/elusive/elusive.css">-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/meteocons/meteocons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/spinnericon/spinnericon.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/datepicker/datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/micWidgets/daterangepicker/daterangepicker.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets-minified/admin-all-demo.css">




    <!-- Data tables -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>
    <script>
        $(function() {
            /* Datatable row highlight */
            $(document).ready(function() {
                var table = $('#datatable-row-highlight').DataTable();

                $('#datatable-row-highlight tbody').on('click', 'tr', function() {
                    $(this).toggleClass('tr-selected');
                });
            });
            $(document).ready(function() {
                $('.dataTables_filter input').attr("placeholder", "Search...");
            });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/touchspin/touchspin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/highcharts/code/css/highcharts.css">
    <!-- JS Demo -->

    <script type="text/javascript">
        $(window).load(function() {
            setTimeout(function() {
                $('#loading').fadeOut(400, "linear");
            }, 300);
        });
    </script>
</head>

<body>
    <div id="sb-site">
        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
        <div id="page-wrapper">
            <div id="page-header" class="bg-black">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                    <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                </div>
                <div id="header-logo" class="logo-bg">
                    <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                    <a id="close-sidebar" href="#" title="Close sidebar">
                        <i class="glyph-icon icon-angle-left"></i>
                    </a>
                </div>
                <div id='headerLeft'>
                    <div id="header-nav-left">
                    </div>
                    <!--header-nav-left-->
                    <div id="header-nav-right">
                    </div>
                    <!--#header-nav-right-->
                </div>
            </div>
            <div id="page-sidebar">
                <div class="scroll-sidebar">
                    <ul id="sidebar-menu">
                    </ul>
                </div>
            </div>
            <div id="page-content-wrapper">
                <div id="page-content">
                    </br>
                    <div class="panel">
                        <div class="panel-content bg-black">
                            <h3>Position Assignment</h3>
                        </div>
                        <div class="panel-body">
                            <div class="example-box-wrapper">
                                <table id="employeeAssignmentTable" class="table table-striped table-hover responsive no-wrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="col-md-4">Firstname</th>
                                            <th class="col-md-4">Lastname</th>
                                            <th class="col-md-4">Position</th>
                                        </tr>
                                    </thead>
                                    <tbody id="employeeAssignmentBody" style="font-size: 12px;">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="modal fade" id="positionAssignmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="positionAssignmentForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Module Form</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Account</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <span class="input-group-addon btn-primary">
                                                    <i class="glyph-icon icon-cogs"></i>
                                                </span>
                                                <select id="accountlist" name="accountlist" class="chosen-select"></select>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Position</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <span class="input-group-addon btn-primary">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span>
                                                <select id="positionlist" name="positionlist" class="chosen-select"></select>    
                                            </div>
                                        </div>
                                    </div>
                                </div>                        
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default cancel" data-dismiss="modal">Cancel</button>
                    </div>
                </form> 
            </div>            
        </div>
     </div>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/BootstrapValidator/dist/js/bootstrapValidator.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-tabletools.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/myJs/onkeyValidator.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/spinner/spinner.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/touchspin/touchspin.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/touchspin/touchspin-demo.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/yearSelector/lib/year-select.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/myJs/ucwords.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/jgrowl-notifications/jgrowl-demo.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker/datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker-ui/datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/timepicker/timepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/textarea/textarea.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/dialog/dialog.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/interactions-ui/resizable.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/interactions-ui/draggable.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/interactions-ui/sortable.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/interactions-ui/selectable.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/dialog/dialog-demo.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/uniform/uniform.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/micWidgets/daterangepicker/moment.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/micWidgets/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/widgets/chosen/chosen.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/micWidgets/yearSelector/lib/year-select.js"></script>

    <!--    system's JS-->
    <script src="<?php echo base_url('node_modules/socket.io/node_modules/socket.io-client/socket.io.js'); ?>"></script>
    
    <script src="<?php echo base_url()?>assets/highcharts/code/highcharts.js"></script>
    <script src="<?php echo base_url()?>assets/highcharts/code/highcharts-3d.js"></script>
    <script src="<?php echo base_url()?>assets/highcharts/code/modules/exporting.js"></script>
    <script src="<?php echo base_url()?>assets/highcharts/code/modules/drilldown.js"></script>
    <!-- <script src="<?php echo base_url()?>assets/highcharts/code/themes/dark-unica.js"></script>
    <script src="<?php echo base_url()?>assets/highcharts/code/themes/dark-unica.src.js"></script> -->

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/myJs/systemJs/overtime.js"></script>

    <script>
        $(function() {
            "use strict";
            $(".chosen-select").chosen();
            $(".chosen-search").append('<i class="glyph-icon icon-search"></i>');
            $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
        });
    </script>

    <script type="text/javascript">

        function getAccountPositionDetails(accountPosId, callback){
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>index.php/positionAssignment/getAccountPositionDetails',
                data:{
                    accountPositionId: accountPosId
                },
                success: function(res) {
                    console.log(res);
                    var accountPostionDetails = $.parseJSON(res.trim());
                    callback(accountPostionDetails);
                },
                error: function(xhr, textStatus, error) {
                    errorLog(xhr, textStatus, error);
                }
            });
        }

        function setUserDetails(userId, accountPositionId){
            $('#positionAssignmentModal').data('userId', userId);
            if(""+accountPositionId == 'null'){
                $('#accountlist').val(0);
                $('#positionlist').val(0);
                $('#accountlist').trigger('chosen:updated');
                $('#positionlist').trigger('chosen:updated');
                $('#positionAssignmentModal').modal('show');
            }else{
                getAccountPositionDetails(accountPositionId, function(accountPositionDetails){
                    $('#accountlist').val(accountPositionDetails.accounts_ID);
                    $.when(getPositions(accountPositionDetails.accounts_ID)).done(function(a) {
                        $('#positionlist').val(accountPositionDetails.positions_ID);
                        $('#accountlist, #positionlist').trigger('chosen:updated');
                    });
                    $('#positionAssignmentModal').modal('show');
                })
            }
            
        }

        function getUsers(){
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>index.php/positionAssignment/getUsers',
                success: function(res) {
                    console.log(res);
                     $('#employeeAssignmentTable').DataTable().destroy();
                    var users = $.parseJSON(res.trim());
                    var html = '';
                    $.each(users, function(index, value){
                        html += '<tr userId = "' + value.uid+ '" >';
                        html += '<td style="vertical-align: middle;text-align: center; padding:8px;">' + value.fname + '</td>';
                        html += '<td style="vertical-align: middle;text-align: center; padding:8px;">' + value.lname + '</td>';
                        html += '<td style="vertical-align: middle;text-align: center; padding:8px;" href="#" onclick="setUserDetails('+value.uid+','+value.accountPositionId+')" >' + value.position + '</td>';
                        html += '</tr>';
                    });
                    $("#employeeAssignmentBody").html(html);
                    $('#employeeAssignmentTable').DataTable().draw();
                    $('#employeeAssignmentTable_filter input').attr("placeholder", "Search...");                
                },
                error: function(xhr, textStatus, error) {
                    errorLog(xhr, textStatus, error);
                }
            });
        }

        function getAccounts(){
             $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>index.php/positionAssignment/getAccounts',
                success: function(res) {
                    console.log(res);
                    var accounts = $.parseJSON(res.trim());
                      var optionsAsString = "<option value='0'></option>";
                    $.each(accounts, function(index, value) {
                        optionsAsString += "<option value='"+value.acc_id+"'>"+value.acc_name+"</option>";
                    });
                    $('#accountlist').html(optionsAsString);
                    $('#accountlist option:contains(Base)').prop('selected', 'selected');
                    $('#accountlist').trigger('chosen:updated');               
                },
                error: function(xhr, textStatus, error) {
                    errorLog(xhr, textStatus, error);
                }
            });
        }

        function getPositions(accountsId){
            return $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>index.php/positionAssignment/getPosition',
                data:{
                    accountId: accountsId
                },
                success: function(res) {
                    console.log(res);
                    var position = $.parseJSON(res.trim());
                      var optionsAsString = "<option value='0'></option>";
                    $.each(position, function(index, value) {
                        optionsAsString += "<option value='"+value.pos_id+"'>"+value.pos_details+"</option>";
                    });
                    $('#positionlist').html(optionsAsString);
                    $('#positionlist option:contains(Base)').prop('selected', 'selected');
                    $('#positionlist').trigger('chosen:updated');               
                },
                error: function(xhr, textStatus, error) {
                    errorLog(xhr, textStatus, error);
                }
            });
        }

        $('#accountlist').on('change', function(){
            getPositions($(this).val());
        })

        $('#positionlist').on('change', function(){
            if(($('#accountlist').val() !=0) && ($('#positionlist').val() !=0)){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url();?>index.php/positionAssignment/setAccountPosition',
                    data:{
                        accountId: $('#accountlist').val(),
                        positionId: $('#positionlist').val(),
                        userId: $('#positionAssignmentModal').data('userId')
                    },
                    success: function(res) {
                        console.log(res);
                        var positionAssignmentStatus = $.parseJSON(res.trim());
                        getUsers();
                        $('#positionAssignmentModal').modal('hide'); 
                        if((parseInt(positionAssignmentStatus.setUserAccountPosition) === 1) && (parseInt(positionAssignmentStatus.setAccountOfEmployee) === 1)){
                            swal({
                                title: "Successfully set Position",
                                timer: 5000,
                                type: "success"
                            });
                        }else{
                            swal({
                                title: "Oops!, Something went wrong while setting the position. Please immediately inform your administrator",
                                timer: 5000,
                                type: "error"
                            });
                        }             
                    },
                    error: function(xhr, textStatus, error) {
                        errorLog(xhr, textStatus, error);
                    }
                });
            }
        });

    </script>


    <script>
        // ON LOAD INITIALIZED FUNCTIONS --------------------------------------------------------------------
        
        getUsers();
        getAccounts()
    
        $(function() {
            //          Page Loads *
            $('#sidebar-menu').load('<?php echo base_url(); ?>/index.php/Templatedesign/sideBar');
            $('#headerLeft').load('<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft');

        });
    </script>

</body>

</html>