<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  </style>


  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });
 </script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/xeditable/xeditable.css">
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable.js"></script>
 <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable-demo.js"></script> -->

 <!-- Bootstrap Summernote WYSIWYG editor -->

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/popover/popover.js"></script>

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 <script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd-yyyy'
    });
  });

</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>

</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div class="container">

          <script type="text/javascript">

            /* Datatables export */

            $(document).ready(function() {
              var table = $('#datatable-tabletools').DataTable();
            } );

            $(document).ready(function() {
              $('.dataTables_filter input').attr("placeholder", "Search...");
            });

          </script>


          <div id="page-title">
		   <a href="<?php echo base_url();?>"> Home</a> > <a href="<?php echo base_url()."index.php/Payroll/payroll_index/"; ?>">Payroll [CCA - Probationary & Regular]</a> > <a href="<?php echo base_url()."index.php/Payroll/GeneratePayrolls/"; ?>">Coverage</a> > <a href="<?php echo base_url()."index.php/Payroll/GeneratePayroll/".$id; ?>">Generate Payroll</a> > <b>Adjustment</b>
		  <br>
		  <br>
            <div class="row">
              <div class="col-md-3">
                <select id='date' class="form-control input-sm">
                  <?php foreach($coverage as $d):?>
                    <option value='<?php echo $d->coverage_id;?>' <?php if($d->coverage_id == $id){echo "selected";}?>>
                      <?php 
                      $new = explode(" - ",$d->daterange);
                      $date1=date_create($new[0]);
                      $date2=date_create($new[1]);
                      $date1 = date_format($date1,"F d,Y");
                      $date2 = date_format($date2,"F d,Y");
                      echo $date1." - ".$date2;
                      ?>
                    </option>
                  <?php endforeach;?>
                </select>
              </div>  
              <div class="col-md-1">
                <button <?php if($usercount->count==count($values)){echo "disabled='disabled'";}?> data-toggle="modal" data-target="#usermodal" class='btn btn-primary btn-sm'><span class='icon-typicons-user-add-outline'></span> User</button>
              </div>   
              <div class="col-md-2">
                <a href="<?php echo base_url();?>index.php/Payroll/addEmployeeDeductions" class='btn btn-primary btn-sm'><span class='icon-typicons-user-add-outline'></span> Deductions</a>
              </div>
            </div><br>
            <div class="row">
              <div class="col-md-12">
                <table class="table table-striped table-condensed" cellspacing="0" width="100%">
                  <thead>
                   <tr>
                     <th class='text-center' style='background: #787a7b;color: white;border-top-left-radius: 10px;'>Full Name</th>
                     <th class='text-center' style='background: #787a7b;color: white;'>Position</th>
                     <th class='text-center' style='background: #787a7b;color: white;'>Status</th>
                     <th class='text-center' style='background: #787a7b;color: white;border-top-right-radius: 10px;' colspan=3>Deductions Details</th>
                   </tr>
                 </thead>
                 <tbody>
                   <?php if($values==NULL){?>

                   <?php }else{ foreach($values as $v): $deductid = explode(',',$v->pdeduct_id);$deductvalue = explode(',',$v->value);$deductname = explode(',',$v->deductionname); $cnt = count($deductvalue);?>
                   <tr>
                    <td rowspan=<?php echo $cnt;?>><?php echo $v->lname.', '.$v->fname;?></td>
                    <td rowspan=<?php echo $cnt;?>><?php echo $v->pos_name;?></td>
                    <td rowspan=<?php echo $cnt;?>><?php echo $v->status;?></td>
                    <td><?php echo $deductname[0];?></td>
                    <td class='text-right'>
                      <?php if($cstatus->status=='Released'){ echo number_format($addvalue[0],2);}else{ ?>
                      <button <?php if($v->isFinal==1){echo 'disabled';}?> class='btn btn-xs btn-link' style="font-size:14px;" id="<?php echo $v->emp_id.''.$deductid[0];?>" data-id="<?php echo $v->emp_id.''.$deductid[0];?>" href="#" data-value="<?php echo (float)$deductvalue[0];?>" data-fullname="<?php echo $v->lname.', '.$v->fname;?>" data-coverageid="<?php echo $v->coverage_id;?>" data-empid="<?php echo $v->emp_id;?>" data-deductionid="<?php echo $deductid[0];?>" data-deductionname="<?php echo $deductname[0];?>" data-action="<?php echo base_url();?>index.php/Payroll/updateEmpDeduct" data-toggle="modal" data-target="#updatemodal"> <?php echo number_format($deductvalue[0],2);?> </button>
                      <?php }?>
                    </td>
                    <td class='text-center' <?php if($cstatus->status=='Released'){echo "hidden";}?>>
                      <?php if($v->isFinal!=1):?>
                        <button class='btn btn-link btn-xs' <?php if($count->count==$cnt){echo "disabled='disabled'";}?> href="#" data-empid="<?php echo $v->emp_id;?>" data-fullname="<?php echo $v->lname.', '.$v->fname;?>" data-ids="<?php for($y=0;$y<$cnt;$y++){echo $deductid[$y].'|';}?>" data-toggle="modal" data-target="#insertmodal"><span class='icon-typicons-plus'></span></button>
                      <?php else:?>
                        <span class='label label-success' data-toggle="tooltip" data-html="true" title="<span class='icon-typicons-info' style='font-size:20px;'></span></br><?php echo $v->lname;?>'s payroll is already finalized. Please make this employee's payroll status to draft so that you can modify adjustments."><span class='icon-typicons-ok'></span></span>
                      <?php endif;?>
                    </td>
                  </tr>
                </tr>
                <?php for($x=1;$x<$cnt;$x++):?>
                  <tr>
                    <td><?php echo $deductname[$x];?></td>
                    <td class='text-right'>
                      <?php if($cstatus->status=='Released'){ echo number_format($addvalue[0],2);}else{ ?>
                      <button <?php if($v->isFinal==1){echo 'disabled';}?> class='btn btn-xs btn-link' style="font-size:14px;" id="<?php echo $v->emp_id.''.$deductid[$x];?>" data-id="<?php echo $v->emp_id.''.$deductid[$x];?>" href="#" data-value="<?php echo (float)$deductvalue[$x];?>" data-fullname="<?php echo $v->lname.', '.$v->fname;?>" data-coverageid="<?php echo $v->coverage_id;?>" data-empid="<?php echo $v->emp_id;?>" data-deductionid="<?php echo $deductid[$x];?>" data-deductionname="<?php echo $deductname[$x];?>" data-action="<?php echo base_url();?>index.php/Payroll/updateEmpDeduct" data-toggle="modal" data-target="#updatemodal"> <?php echo number_format($deductvalue[$x],2);?> </button>
                      <?php }?>
                    </td>
                  </tr>
                <?php endfor;?>
              <?php endforeach; }?>
            </tbody>
          </table>
        </div>
      </div>

    </div>

    <div class="row" hidden>
      <div class="col-md-8">
        <div class="panel">
          <div class="panel-body">
            <h3 class="title-hero">
              Recent sales activity
            </h3>
            <div class="example-box-wrapper">
              <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
            </div>
          </div>
        </div>




        <div class="content-box">
          <h3 class="content-box-header bg-default">
            <i class="glyph-icon icon-cog"></i>
            Live server status
            <span class="header-buttons-separator">
              <a href="#" class="icon-separator">
                <i class="glyph-icon icon-question"></i>
              </a>
              <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                <i class="glyph-icon icon-refresh"></i>
              </a>
              <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                <i class="glyph-icon icon-times"></i>
              </a>
            </span>
          </h3>
          <div class="content-box-wrapper">
            <div id="data-example-3" style="width: 100%; height: 250px;"></div>
          </div>
        </div>

      </div>

    </div>
  </div>



</div>
</div>
</div>
<div class="modal fade " id='updatemodal' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:35%;">
    <div class="modal-content">
      <form method="POST" id='updateform'>
        <div class="modal-header" style="background: #1f2e2e; color: white;">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 class="modal-title">Update Employee Deduction</h3>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label class='control-label'>Employee Name:</label>
              <h4> <span id='fullname'></span></h4><br>
            </div>
            <div class="col-md-6">
              <label class='control-label'>Deduction Name:</label>
              <h4><span id='deductionname'></span></h4>
            </div>
            <div class="col-md-6">
              <label class='control-label'>Value: 
                <span class="text-danger" id='dn2' style='font-size:8px;' hidden>* (Required)</span>
                <span class="text-danger" id='dn3' style='font-size:8px;' hidden> Valid Numbers Only</span>
              </label>
              <input type="text" class="form-control" id='value'>
              <input type="hidden" class="form-control" id='coverageid'>
              <input type="hidden" class="form-control" id='empid'>
              <input type="hidden" class="form-control" id='deductionid'>
              <input type="hidden" class="form-control" id='id'>
            </div>

          </div> 
        </div>    
        <div class="modal-footer" id='footer'>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id='updatenow' disabled>Update</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade " id='insertmodal' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:35%;">
    <form id='savenow'>
      <div class="modal-content">
        <div class="modal-header" style="background: #1f2e2e; color: white;">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 class="modal-title">Add Single Deduction</h3>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label class='control-label'>Employee Name:</label>
              <h4> <span id='fullname2'></span></h4><br>
            </div>
            <div class="col-md-8">
              <label class='control-label'>Deduction: 
               <span class="text-danger" id='aa' style='font-size:8px;' hidden>* (Required)</span>
             </label>
             <select class="form-control" id='availabledeductions2'></select>
           </div>
           <div class="col-md-4">
            <label class='control-label'>Value: 
             <span class="text-danger" id='v2' style='font-size:8px;' hidden>* (Required)</span>
             <span class="text-danger" id='v3' style='font-size:8px;' hidden> Valid Numbers Only</span>
           </label>
           <input type='text' class="form-control" id='value2'>
           <input type='hidden' class="form-control" id='empid2'>
           <input type='hidden' class="form-control" id='coverid2'>
         </div>
       </div>

     </div>    
     <div class="modal-footer" id='footer'>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary">Insert</button>
    </div>
  </div>
</form>
</div>
</div>


<div class="modal fade " id='usermodal' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:35%;">
    <form id="insertusernow" method='post'>
      <div class="modal-content">
        <div class="modal-header" style="background: #1f2e2e; color: white;">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 class="modal-title">Add User Deductions</h3>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label class='control-label'>Employee Name: 
               <span class="text-danger" id='useree' style='font-size:8px;' hidden>* (Required)</span>
             </label>
             <select class="chosen-select" id='useravailable'>
              <option value=''>--</option>
              <?php foreach($notonlist as $user):?>
                <option value='<?php echo $user->emp_id;?>'><?php echo $user->lname.' '.$user->fname;?></option>
              <?php endforeach;?>
            </select>
          </div>  
          <div class="col-md-8">
            <label class='control-label'>Deductions: 
             <span class="text-danger" id='useraa' style='font-size:8px;' hidden>* (Required)</span>
           </label>
           <select class="form-control" id='useravailablededuction'>
             <option value=''>--</option>
             <?php foreach($alldeductions as $a):?>
              <option value='<?php echo $a->pdeduct_id;?>'><?php echo $a->deductionname;?></option>
            <?php endforeach;?>
          </select>
        </div>
        <div class="col-md-4">
          <label class='control-label'>Value: 
           <span class="text-danger" id='userv2' style='font-size:8px;' hidden>* (Required)</span>
           <span class="text-danger" id='userv3' style='font-size:8px;' hidden> Valid Numbers Only</span>
         </label>
         <input type='text' class="form-control" id='uservalue2'>
       </div>
     </div>

   </div>    
   <div class="modal-footer" id='footer'>
    <span class="form-control-static pull-left text-left text-danger" style="font-size:10px; display:none" id="payrollnote" >
     <b>NOTE:</b> The chosen employee has a finalized payroll data. <br> Please Contact HR if changes are to be made.
   </span>
   <button type="button" class="btn btn-default" data-dismiss="modal" style="margin:5px 0">Close</button>
   <button type="submit" class="btn btn-primary" id="payrollbutton">Save</button>
 </div>
</div>
</form>
</div>
</div>


<p class="growl1" hidden></p>
<p class="growl2" hidden></p>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $('#date').change(function () {
   var optionSelected = $(this).find("option:selected");
   var valueSelected  = optionSelected.val();
   var textSelected   = optionSelected.text();
   window.location = "<?php echo base_url();?>index.php/Payroll/employeeDeductions/"+valueSelected;
 });
</script>
<script type="text/javascript">
 $(document).ready(function () {

  $(".growl1").select(function(){
    $.jGrowl(
      "Successfully Updated.",
      {sticky:!1,position:"top-right",theme:"bg-blue-alt"}
      )
  });
  $(".growl2").select(function(){
    $.jGrowl(
      "<?php echo $this->session->flashdata('success');?>",
      {sticky:!1,position:"top-right",theme:"bg-blue-alt"}
      )
  });
  if("<?php echo $this->session->flashdata('success');?>"!=''){
    $(".growl2").trigger('select');
  }
  
  //------------------------
  $('#updatemodal').on('show.bs.modal', function (e) {

    $("#dn2").hide();
    $('#dn3').hide();
    $(this).find('#updateform').attr('action', $(e.relatedTarget).data('action'));
    $(this).find('#fullname').html($(e.relatedTarget).data('fullname'));
    $(this).find('#coverageid').val($(e.relatedTarget).data('coverageid'));
    $(this).find('#deductionid').val($(e.relatedTarget).data('deductionid'));
    $(this).find('#deductionname').html($(e.relatedTarget).data('deductionname'));
    $(this).find('#empid').val($(e.relatedTarget).data('empid'));
    $(this).find('#value').val($(e.relatedTarget).data('value'));
    $(this).find('#id').val($(e.relatedTarget).data('id'));
  });

  $("#updateform").submit(function(e){
    var coverageid = $('#coverageid').val();
    var empid = $('#empid').val();
    var deductionid = $('#deductionid').val();
    var value = $('#value').val();
    var id = $('#id').val();
    
    if(value==''){
      $('#dn2').show();
      $('#dn3').hide();
    }else if(!$.isNumeric( value )){
      $('#dn3').show();
      $("#dn2").hide();
    }else{
      $("#dn2").hide();
      $('#dn3').hide();

      $.ajax({
        type: "POST",
        url: $("#updateform").attr('action'),
        data: {
          coverageid: coverageid,
          empid: empid,
          deductionid: deductionid,
          value: value
        },
        cache: false,
        success: function(res)
        {
          // console.log(res);
          if(res=='Failed'){
            swal("Oops!", "An Error Occured while processing data.", "error");
          }else{
            // alert("okay");
            $('#'+id).text(parseFloat(res).toFixed(2));
            $('#'+id).data('value',parseFloat(res));
            
            $(".growl1").trigger('select');
          }
          $("#updatenow").attr("disabled",true);
          $('#updatemodal').modal('hide');
        }
      });
    }
    e.preventDefault();
  });
  $("#value").on("change paste keyup", function() {
    $("#updatenow").removeAttr('disabled');
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function () {

    $('#insertmodal').on('show.bs.modal', function (e) {
      var ids = $(e.relatedTarget).data('ids');
      var empid = $(e.relatedTarget).data('empid');
      var fullname = $(e.relatedTarget).data('fullname');
      var coverage_id = "<?php echo $id;?>";
      $("#fullname2").html(fullname);
      $("#coverid2").val(coverage_id);
      $("#empid2").val(empid);
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Payroll/getEmpAddExistDeductions",
        data: {
          coverage_id: coverage_id,
          empid: empid,
          ids: ids
        },
        cache: false,
        success: function(res)
        {
          if(res!='Empty'){
            $("#availabledeductions2").html("<option value=''>--</option>");
            $.each(JSON.parse(res),function (i,elem){
              // alert(elem.rate);
              $("#availabledeductions2").append("<option value='"+elem.pdeduct_id+"'>"+elem.deductionname+"</option>");
            });
          }
        }
      });
    });
  });

  $("#savenow").submit(function(e){
    var value = $("#value2").val();
    var deduction_id = $("#availabledeductions2").val();
    var empid = $("#empid2").val();
    var coverage_id = $("#coverid2").val();
    if(value==''){
      $("#v2").show();
      $("#v3").hide();
    }else if(!$.isNumeric(value)){
      $("#v3").show();
      $("#v2").hide();
    }else{
      $("#v3").hide();
      $("#v2").hide();
    }
    if(deduction_id==''){
      $("#aa").show();
    }else{
      $("#aa").hide();
    }
    if(value!=''&&deduction_id!=''&&$.isNumeric(value)){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Payroll/addExistEmpDeduction",
        data: {
          coverage_id: coverage_id,
          empid: empid,
          deduction_id: deduction_id,
          value:value
        },
        cache: false,
        success: function(res)
        {
          if(res=='Success'){
            location.reload();
          }else{
            swal("Oops!", "An Error Occured while processing data.", "error");
          }
        }
      });
    }
    e.preventDefault();
  })
</script>
<!--  useraa  userv2 userv3  insertusernow -->
<script type="text/javascript">

  $("#insertusernow").submit(function(e){
    var value = $("#uservalue2").val();
    var deduction_id = $("#useravailablededuction").val();
    var empid = $("#useravailable").val();
    var coverage_id = "<?php echo $id;?>";
    if(value==''){
      $("#userv2").show();
      $("#userv3").hide();
    }else if(!$.isNumeric(value)){
      $("#userv3").show();
      $("#userv2").hide();
    }else{
      $("#userv3").hide();
      $("#userv2").hide();
    }
    if(deduction_id==''){
      $("#useraa").show();
    }else{
      $("#useraa").hide();
    }
    if(empid==''){
      $("#useree").show();
    }else{
      $("#useree").hide();
    }
    if(empid!=''&&value!=''&&deduction_id!=''&&$.isNumeric(value)){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Payroll/addExistEmpDeduction",
        data: {
          coverage_id: coverage_id,
          empid: empid,
          deduction_id: deduction_id,
          value:value
        },
        cache: false,
        success: function(res)
        {
          if(res=='Success'){
            location.reload();
          }else{
            swal("Oops!", "An Error Occured while processing data.", "error");
          }
        }
      });
    }
    e.preventDefault();
  })
</script>
<script type="text/javascript">
  $("#useravailable").on('change',function(){
   var coverage_id = "<?php echo $id;?>";
   var emp_id = $(this).val();
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Payroll/onPayroll",
    data: {
      coverage_id: coverage_id,
      emp_id: emp_id
    },
    cache: false,
    success: function(res)
    {
      if(res=='Yes'){
        $("#payrollnote").show();
        $("#payrollbutton").attr('disabled','disabled');
      }else{
        $("#payrollnote").hide();
        $("#payrollbutton").removeAttr("disabled");
      }
    }
  });
 })
</script>
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
  });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>
