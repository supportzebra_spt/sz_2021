<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    @media (max-width: 768px) {
      .col-xs-10.text-right, .col-xs-10.text-left,.col-xs-12.text-right, .col-xs-12.text-left {
        text-align: center;
      } 
    }
  </style>


  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });
 </script>

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>
 <script type="text/javascript">
  /* Input switch */

  $(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
  });
</script>
<script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd-yyyy'
    });

    $(".growl1").select(function(){
      $.jGrowl(
        "Successfully Updated.",
        {sticky:!1,position:"top-right",theme:"bg-blue-alt"}
        )
    });
    $(".growl2").select(function(){
      $.jGrowl(
        "Error Updating.",
        {sticky:!1,position:"top-right",theme:"bg-red"}
        )
    });
    $(".growl3").select(function(){
      $.jGrowl(
        "Adjustment Already Exists. <br> Please contact administrator.",
        {sticky:!1,position:"top-right",theme:"bg-red"}
        )
    });
    $(".growl4").select(function(){
      $.jGrowl(
        "Error retrieving data",
        {sticky:!1,position:"top-right",theme:"bg-red"}
        )
    });
  });
</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>
<script type="text/javascript">

  /* Datatables responsive */

  $(document).ready(function() {
    $('#datatable-responsive').DataTable( {
      responsive: true,
      order: [[ 1, "desc" ]]
    } );
  } );

  $(document).ready(function() {
    $('.dataTables_filter input').attr("placeholder", "Search...");
  });

</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd-yyyy'
    });
  });


</script>
</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="<?php echo $setting['settings']; ?>">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%' alt="">
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>
    </div>
    <div id="page-sidebar">
     <div class="scroll-sidebar">

      <ul id="sidebar-menu">
      </ul>
    </div>
  </div>
  <div id="page-content-wrapper">
    <div id="page-content">

      <div class="container">
        <div id="page-title">

          <div class="row">
            <div class='col-md-4 col-xs-12 text-left'><br>
              <div class="btn-group">
               <a href='<?php echo base_url();?>index.php/EmployeeAdjustment' class='btn btn-warning btn-xs'>Back</a>
               <button data-toggle="modal" data-target="#adjustmodal" class='btn btn-primary btn-xs'><i class='icon-typicons-plus'></i> Adjustment</button><br><br>
             </div>

           </div>
           <?php if($details!=''||$details!=NULL):?>
            <div class="col-md-7  col-xs-10 text-right">
              <h4 style='margin-bottom:10px'><b><?php echo $details->fname." ".$details->lname;?></b></h4>
              <h5><?php echo $details->pos_name.' - '.$details->pos_details;?><br><?php echo $details->status;?></h5><br>
            </div>  
            <div class="col-md-1 col-xs-2">
              <img src="<?php echo base_url();?>assets/images/<?php echo $details->pic;?>" alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';" style='width:100%;border: 4px solid #777' class='img-circle' />
            </div>
          <?php endif;?>

        </div>
        <div class="form-group">
         <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
          <thead>
            <tr>
             <th>Adjustment</th>
             <th>Maximum Amount</th>
             <th>Monthly</th>
             <th>Quinsina</th>
             <th>Total Months</th>
             <th>Date</th>
             <th>Status</th>
             <th>Action</th>

           </tr>
         </thead>

         <tfoot>
          <tr> 
           <th>Adjustment</th>
           <th>Maximum Amount</th>
           <th>Monthly</th>
           <th>Quinsina</th>
           <th>Total Months</th>
           <th>Date</th>
           <th>Status</th>
           <th>Action</th>
         </tr>
       </tfoot>

       <tbody>
         <?php foreach($values as $v):?>
           <tr>
             <td><?php echo $v->deductionname;?></td>
             <td class='text-right'>P <?php echo  $v->max_amount;?></td>
             <td class='text-right'>P <?php echo  $v->adj_amount;?></td>
             <td class='text-right'>P <?php  echo number_format($v->adj_amount/2,2);?></td>
             <td class='text-right'><?php echo  $v->count;?></td>
             <td><?php echo  $v->adj_date;?></td>
             <td>
               <input type="checkbox" data-on-color="info" data-off-color="danger" name="" class="input-switch" data-size="mini" data-on-text="A" data-off-text="IA" id='activate<?php echo $v->pemp_adjust_id;?>' onchange="setActiveness(<?php echo $v->pemp_adjust_id.','.$v->pdeduct_id.','.$v->emp_id;?>)" <?php if($v->isActive==1){echo 'checked';} ?> >
             </td>
             <td class='text-right'>
               <div class="btn-group">

                 <?php if($v->updateable=='no'):?>
                   <button class='btn btn-primary btn-xs'  data-toggle="modal" data-target="#detailsmodal" onclick="toDisp(<?php echo $v->pemp_adjust_id?>)">Details</button>
                 <?php else:?>

                   <button data-toggle="modal" data-target="#updatemodal" class='btn btn-warning btn-xs' onclick="update(<?php echo $v->pemp_adjust_id?>)">Update</button>
                 <?php endif;?>
                 <button class='btn btn-info btn-xs' data-toggle="modal" data-target="#transactionsmodal">Summary</button>
               </div>
             </td>
           </tr>
         <?php endforeach;?>
       </tbody>
     </table>

   </div>
   <div id='Templatedesign'>
   </div>
 </div>

 <div class="row" hidden>
  <div class="col-md-8">
    <div class="panel">
      <div class="panel-body">
        <h3 class="title-hero">
          Recent sales activity
        </h3>
        <div class="example-box-wrapper">
          <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
        </div>
      </div>
    </div>
    <div class="content-box">
      <h3 class="content-box-header bg-default">
        <i class="glyph-icon icon-cog"></i>
        Live server status
        <span class="header-buttons-separator">
          <a href="#" class="icon-separator">
            <i class="glyph-icon icon-question"></i>
          </a>
          <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
            <i class="glyph-icon icon-refresh"></i>
          </a>
          <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
            <i class="glyph-icon icon-times"></i>
          </a>
        </span>
      </h3>
      <div class="content-box-wrapper">
        <div id="data-example-3" style="width: 100%; height: 250px;"></div>

      </div>
    </div>

  </div>

</div>
</div>

</div>
</div>
</div>
<div id="adjustmodal" class="modal fade" role="dialog">
  <div class="modal-dialog" style='width:500px'>

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #1f2e2e; color: white;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Adjustment</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8">
            <label class="control-label">
              Adjustment Name:
            </label>
            <select id='adjustname'  class='form-control'>
              <option value=''>--</option>
              <?php foreach($types as $type):?>
                <option value='<?php echo $type->pdeduct_id;?>'><?php echo $type->deductionname;?></option>
              <?php endforeach;?>
            </select>
            <p id='namer' class='text-danger' style='font-size:11px' hidden>Adjustment Name Required *</p><br>
          </div>

          <div class="col-md-4">
            <label class="control-label">
              Adjustment Date:
            </label>
            <input type="text" id='adjustdate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
            <p id='dater' class='text-danger' style='font-size:11px' hidden>Date Required *</p>
          </div>
          <div class="col-md-12">
            <label class="control-label">
              Remarks:
            </label>
            <textarea id='remarks' class='form-control'></textarea><br>
          </div>
          <div class="col-md-6">
            <label class="control-label">
             Max Amount:
           </label>
           <input type="text" id='amount' class='form-control' onkeyup="change()">
           <p id='amountr' class='text-danger' style='font-size:11px' hidden>Amount Required *</p>
           <p id='amounth' class='text-danger' style='font-size:11px' hidden>Amount should be greater that ZERO *</p><br>
         </div>
         <div class="col-md-6">
          <label class="control-label">
           Months to Pay:
         </label>
         <input type="text" id='count' class='form-control' onkeyup="change()">
         <p id='countr' class='text-danger' style='font-size:11px' hidden>Count Required *</p>
         <p id='counth' class='text-danger' style='font-size:11px' hidden>Count should be greater that ZERO *</p><br>
       </div>
       <div class="col-md-6 text-center" >
         <h5>PHP <b id='monthly'>0.00</b><br>Monthly</h5>
       </div>
       <div class="col-md-6 text-center">
        <h5 >PHP <b id='quinsina'>0.00</b><br>Quinsina</h5>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button id='adjustsubmit' class='btn btn-primary'>Submit</button>
  </div>
</div>
</div>
</div>

<div id="updatemodal" class="modal fade" role="dialog">
  <div class="modal-dialog" style='width:500px'>

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #1f2e2e; color: white;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Adjustment</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <input type="hidden" id='adjustid2' name="">
          <div class="col-md-8">
            <label class="control-label">
              Adjustment Name:
            </label>
            <p id='adjustname2'></p><br>
          </div>

          <div class="col-md-4">
            <label class="control-label">
              Adjustment Date:
            </label>
            <p id='adjustdate2'></p><br>
          </div>
          <div class="col-md-12">
            <label class="control-label">
              Remarks:
            </label>
            <textarea id='remarks2' class='form-control'></textarea><br>
          </div>
          <div class="col-md-6">
            <label class="control-label">
             Max Amount:
           </label>
           <input type="text" id='amount2' class='form-control' onkeyup="change2()">
           <p id='amountr2' class='text-danger' style='font-size:11px' hidden>Amount Required *</p>
           <p id='amounth2' class='text-danger' style='font-size:11px' hidden>Amount should be greater that ZERO *</p><br>
         </div>
         <div class="col-md-6">
          <label class="control-label">
           Months to Pay:
         </label>
         <input type="text" id='count2' class='form-control' onkeyup="change2()">
         <p id='countr2' class='text-danger' style='font-size:11px' hidden>Count Required *</p>
         <p id='counth2' class='text-danger' style='font-size:11px' hidden>Count should be greater that ZERO *</p><br>
       </div>
       <div class="col-md-6 text-center" >
         <h5>PHP <b id='monthly2'>0.00</b><br>Monthly</h5>
       </div>
       <div class="col-md-6 text-center">
        <h5 >PHP <b id='quinsina2'>0.00</b><br>Quinsina</h5>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button id='updatesubmit2' class='btn btn-primary'>Update</button>
  </div>
</div>
</div>
</div>

<div id="transactionsmodal" class="modal fade" role="dialog">
  <div class="modal-dialog" style='width:500px'>

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #1f2e2e; color: white;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Transactions</h4>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<div id="detailsmodal" class="modal fade" role="dialog">
  <div class="modal-dialog" style='width:500px'>

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #1f2e2e; color: white;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Adjustment Details</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <label class="control-label">Adjustment Name:</label>
            <h5 id='dispName'></h5><hr>
          </div>
          <div class="col-md-6">
            <label class="control-label">Adjustment Date:</label>
            <h5 id='dispDate'></h5><hr>
          </div>
          <div class="col-md-12">
            <label class="control-label">Remarks:</label>
            <h5 id='dispRemarks' class='text-center'></h5><br><hr>
          </div>
          <div class="col-md-6">
            <label class="control-label">Max Amount</label>
            <h5 id='dispMax'></h5><br>
          </div>
          <div class="col-md-6">
            <label class="control-label">Monthly</label>
            <h5 id='dispMonthly'></h5><br>
          </div>
          <div class="col-md-6">
            <label class="control-label">Months to Pay:</label>
            <h5 id='dispCount'></h5><br>
          </div>

          <div class="col-md-6">
            <label class="control-label">Quinsina:</label>
            <h5 id='dispQuinsina'></h5><br>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<p class="growl1" hidden></p>
<p class="growl2" hidden></p>
<p class="growl3" hidden></p>
<p class="growl4" hidden></p>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
    cache: false,
    success: function(html)
    {

      $('#headerLeft').html(html);
		   //alert(html);
     }
   });	
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
    cache: false,
    success: function(html)
    {

      $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
 });	
</script>
<script type="text/javascript">
 function toDisp(pemp_adjust_id){
  // alert(pemp_adjust_id);
  var pemp_adjust_id = pemp_adjust_id;
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/EmployeeAdjustment/getAdjustmentFromId",
    data: {
      pemp_adjust_id: pemp_adjust_id
    },
    cache: false,
    success: function(result)
    {
      if(result=='Failed'){
       $(".growl4").trigger('select');
     }else{
       var res = $.parseJSON(result);
       $("#dispName").html(res.deductionname);
       $("#dispDate").html(res.adj_date);
       $("#dispRemarks").html(res.remarks);
       $("#dispMax").html("PHP "+res.max_amount);
       $("#dispCount").html(res.count);  
       $("#dispMonthly").html("PHP "+(res.max_amount/res.count).toFixed(2));  
       $("#dispQuinsina").html("PHP "+((res.max_amount/res.count)/2).toFixed(2)); 
     }
     
   }
 });

}
</script>
<script type="text/javascript">
  $("#adjustsubmit").click(function(){

    var count = $("#count").val();
    var amount = $("#amount").val();
    var name = $("#adjustname").val();
    var date = $("#adjustdate").val();
    var remarks = $("#remarks").val();
    if(count==''){
      $("#countr").show();
      $("#counth").hide();
    }else if(count<1){
      $("#counth").show();
      $("#countr").hide();
    }else{
      $("#counth").hide(); 
      $("#countr").hide();
    }
    if(amount==''){
      $("#amountr").show();
      $("#amounth").hide(); 
    }else if(amount<1){
      $("#amounth").show();
      $("#amountr").hide();
    }else{
      $("#amounth").hide(); 
      $("#amountr").hide();
    }

    if(name==''){
      $("#namer").show();
    }else{
      $("#namer").hide();
    }   
    if(date==''){
      $("#dater").show();
    }else{
      $("#dater").hide();
    }

    if(count!=''&&amount!=''&&name!=''&&date!=''&&count>0&&amount>0){
     $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/EmployeeAdjustment/newAdjustment",
      data: {
        emp_id: <?php echo $emp_id;?>,
        pdeduct_id: name,
        max_amount:parseFloat(amount),
        adj_amount:parseFloat(amount/count),
        adj_date:date,
        count:count,
        remarks:remarks

      },
      cache: false,
      success: function(res)
      {
        if(res=='Success'){
          location.reload();
        }else{
          alert('Failed');
          $(".growl2").trigger('select');
        }
      }
    });
   }
 });
  function change(){
    var count = $("#count").val();
    var amount = $("#amount").val();
    var zero = 0;
    if(count!=''&&amount!=''){
      if(count<1){

       $("#monthly").html(zero.toFixed(2));
       $("#quinsina").html(zero.toFixed(2));
     }else if(amount<1){

       $("#monthly").html(zero.toFixed(2));
       $("#quinsina").html(zero.toFixed(2));
     }else{
       $("#monthly").html((amount/count).toFixed(2));
       $("#quinsina").html(((amount/count)/2).toFixed(2));
     }
   }
 }
</script>
<script type="text/javascript">
 $("#updatesubmit2").click(function(){

  var count = $("#count2").val();
  var amount = $("#amount2").val();
  var remarks = $("#remarks2").val();
  var adjustid = $("#adjustid2").val();
  if(count==''){
    $("#countr2").show();
    $("#counth2").hide();
  }else if(count<1){
    $("#counth2").show();
    $("#countr2").hide();
  }else{
    $("#counth2").hide(); 
    $("#countr2").hide();
  }
  if(amount==''){
    $("#amountr2").show();
    $("#amounth2").hide(); 
  }else if(amount<1){
    $("#amounth2").show();
    $("#amountr2").hide();
  }else{
    $("#amounth2").hide(); 
    $("#amountr2").hide();
  }

  if(count!=''&&amount!=''&&count>0&&amount>0){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/EmployeeAdjustment/updateAdjustment",
    data: {
      max_amount:parseFloat(amount),
      adj_amount:parseFloat(amount/count),
      count:count,
      remarks:remarks,
      adjustid:adjustid

    },
    cache: false,
    success: function(res)
    {
      if(res=='Success'){
        location.reload();
      }else{
        alert('Failed');
        $(".growl2").trigger('select');
      }
    }
  });
 }
});

 function change2(){
  var count = $("#count2").val();
  var amount = $("#amount2").val();
  var zero = 0;
  if(count!=''&&amount!=''){
    if(count<1){

     $("#monthly2").html(zero.toFixed(2));
     $("#quinsina2").html(zero.toFixed(2));
   }else if(amount<1){

     $("#monthly2").html(zero.toFixed(2));
     $("#quinsina2").html(zero.toFixed(2));
   }else{
     $("#monthly2").html((amount/count).toFixed(2));
     $("#quinsina2").html(((amount/count)/2).toFixed(2));
   }

 }else{

   $("#monthly2").html(zero.toFixed(2));
   $("#quinsina2").html(zero.toFixed(2));
 }

}
function update(pemp_adjust_id){
  var pemp_adjust_id = pemp_adjust_id;
  // alert(pemp_adjust_id);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/EmployeeAdjustment/getAdjustmentFromId",
    data: {
      pemp_adjust_id: pemp_adjust_id
    },
    cache: false,
    success: function(result)
    {
      if(result=='Failed'){

        $(".growl4").trigger('select');
      }else{
        var res = $.parseJSON(result);
            // console.log(res);
            $("#adjustid2").val(res.pemp_adjust_id);
            $("#adjustname2").html(res.deductionname);
            $("#adjustdate2").html(res.adj_date);
            $("#remarks2").val(res.remarks);
            $("#amount2").val(parseFloat(res.max_amount));
            $("#count2").val(res.count);  
            change2();
          }
        }
      });

}
</script>
<script type="text/javascript">

  function setActiveness(id,pdeduct_id,emp_id){
    if ($("#activate"+id).is(":checked")){
      var active = 1;
            // alert('active');
          }else{
            var active = 0;
            // alert('inactive');
          }
          // alert(active);
// alert(id);
$.ajax({
  type: "POST",
  url: "<?php echo base_url();?>index.php/EmployeeAdjustment/updateActiveness",
  data: {
    pemp_adjust_id: id,
    activevalue: active,
    pdeduct_id:pdeduct_id,
    emp_id:emp_id
  },
  cache: false,
  success: function(res)
  {
   if(res=='Success'){
     $(".growl1").trigger('select');
   }else if(res=='Failed'){
     $(".growl2").trigger('select');
   }else{
     $(".growl3").trigger('select');

   }
 }
});
}
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>