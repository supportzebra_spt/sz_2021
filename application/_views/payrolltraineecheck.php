<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		#tblShowTR td{
			color: green;
		}
		tbody#tblShow tr td {
			border: 1px solid #b9ffbc;
			vertical-align:middle;
		}
		tbody#tblShow tr:hover {
			background-color: #aaefad;
		}
       
        .dddiv{ 
             overflow-x:scroll;  
			margin-left: 193px;
			overflow-y:visible;
            padding-bottom:1px;
        }
        .headcol {
            position:absolute; 
			width: 20%;
			left:0;
            top:auto;
            border-right: 0px none black; 
            border-top-width:3px; /*only relevant for first row*/
         }
		 
		 table.blueTable {
  width: 100%;
  text-align: left;
  border-collapse: collapse;
}
table.blueTable td, table.blueTable th {
  padding: 9px 2px;
}
table.blueTable tbody td {
  font-size: 13px;
}
table.blueTable tr:nth-child(even) {
  background: #E7F5E6;
}
table.blueTable thead {
  background: #5AA450;
  background: -moz-linear-gradient(top, #83bb7c 0%, #6aad61 66%, #5AA450 100%);
  background: -webkit-linear-gradient(top, #83bb7c 0%, #6aad61 66%, #5AA450 100%);
  background: linear-gradient(to bottom, #83bb7c 0%, #6aad61 66%, #5AA450 100%);
}
table.blueTable thead th {
  font-size: 15px;
  font-weight: bold;
  color: #FFFFFF;
}
table.blueTable tfoot td {
  font-size: 14px;
}
table.blueTable tfoot .links {
  text-align: right;
}
table.blueTable tfoot .links a{
  display: inline-block;
  background: #1C6EA4;
  color: #FFFFFF;
  padding: 2px 8px;
  border-radius: 5px;
}
td.headcol {
    height: 116px;
}
      </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">






    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

 
</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

 		<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper" >
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">

<div class="col-md-12">
        <div class="panel">
            <div class="panel-body" >
   	<a href="../../../Payroll/GeneratePayroll/<?php echo $stat["status"]."/".$period["coverage_id"]; ?>" class="btn btn-xs btn-success glyph-icon   icon-mail-reply" title="" style="margin-bottom: 9px;"> Back</a>

                 <div class="example-box-wrapper" >
                    <form class="form-horizontal bordered-row" role="form">
		
                         <div class="form-group" id="employzResult">
                    <div class="col-sm-12">
                        <!--<select multiple class="multi-select" id="employz">
 								<?php 
								foreach($employee as $k => $v){
									
									echo "<option value=".$v['emp_id'].">".$v['fname']." ".$v['lname']."</option>";
								}
								
								?>
                         </select>-->
						 
          <form id="demoform" action="#" method="post">
		  		 
					 <div class="form-group">
                    <label class="col-sm-3 control-label">Payroll Coverage:</label>
                    <div class="col-sm-6">
                        <select name="" class="chosen-select" style="display: none;" id="payrollC">
								 
                                <option disabled="disabled" selected >--</option>
							 
							  <optgroup label="<?php echo $period["month"]; ?>">
										<option selected><?php echo $period["daterange"]; ?></option>
								</optgroup>
								 
                        </select>
	 
                    </div>

                </div>
            <select multiple="multiple" size="10" name="duallistbox_demo1[]" id="employz">
           <?php 
			foreach($employee as $k => $v){
				
				?>
									
									 <option value="<?php echo $v['emp_id']; ?>" <?php echo ($v["payroll_id"]>0) ? "selected" : " "?>><?php echo $v['lname'].", ".$v['fname']; ?></option> 
							<?php 	}
								
								?>
            </select>
             </form>

                    </div>
                </div>
					<input type="button" class="btn btn-xs btn-primary" id="btnShow" value="Show">
					<input type="button" class="btn btn-xs btn-info" id="btnSync" value="Loans Sync">
					<input type="button" class="btn btn-xs btn-info" id="btnExport" value="Export">
					<a href="<?php echo base_url()."reports/payroll/".$period["year"]."/".$period["month"]."/".$period["period"]."/report/SZ_Payslip_".strtoupper($stat["status"])."_".strtoupper($period["month"])."_".$period["period"]."_".$period["coverage_id"].".pdf"; ?>" target="_blank"><input type="button" class="btn btn-xs btn-info"  value="Export2" id="Export2" style="display:none"></a>
					<input type="button" class="btn btn-xs btn-info" id="btnSavePayroll" value="Save">
 
     
                    </form>
                </div>
            </div>
        </div>
   
       
    </div>
<div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                
                <div class="example-box-wrapper"  >
						<!--<div id="loadingIMG" style="position: fixed; margin: -20% auto 0px; width: 100%; z-index: 1000; display: none;margin-left:20%;">
							 <img src="<?php echo base_url();?>assets/images/loading-gears-animation-3.gif"  > 
						</div>-->
                    <div class="form-horizontal bordered-row dddiv" role="form">

                         <table id="datatable-row-highlight" class="table" style="color:black; "> 
							 <tr style="background: #4CAF50;color: white;">
						 <th class="headcol" style="background: #4CAF50;color: white;height: 57px;">Fullname</th>
						 <td class='long'>Position</td>
						 <td class='long'>Monthly</td>
						 <td class='long'>Quinsina</td>
						 <td class='long'>Daily</td>
						 <td class='long'>Hourly</td>
						 <td class='long'>Total</td>
						 <td class='long' style='text-align:center;' colspan=3>  Basic Allowance </td>
 						 <td class='long'>Bonus</td>
 						 <td class='long' hidden>OT-BCT</td>
						 <td class='long'>ND</td>
						 <td class='long'>HP</td>
						 <td class='long'>HO</td>
						 <td class='long'>SSS</td>
						 <td class='long'>PHIC</td>
						 <td class='long'>HDMF</td>
						 <td class='long'>BIR</td>
						 <td class='long'>Adjust(+)</td>
						 <td class='long'>Adjust(-)</td>
						 <td class='long'>Earning</td>
						 <td class='long'>Deduction</td>
						 <td class='long'>TakeHome Pay</td>
						 </tr>
						 <tbody id="tblShow">
						 </tbody>
							
                         </table>
                          <div class="loading-spinner" id="loads">
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
       
    </div>
	

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>
<input type="button" value="test" id="test">
 <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #545454;color: #ffffff;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id="fullnym"></span>  <span id="empsName"> </span> </h4>
      </div>
      <div class="modal-body">
	  <div id="DeductReload"></div>
	  <br>
		<span id="empsIs" hidden></span>
		<table class="blueTable">
		<thead>
			<tr>
			<th>Deduction</th>
			<th>Amount</th>
			</tr>
		</thead>
			<tbody id="detailedd"> </tbody>
		</table>
	 
       </div>
      <div class="modal-footer" id="footerEmp">
        <button type="button" class="btn btn-success" data-toggle='modal' data-target='#modalAddDeduction'>Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
	
  </div>
</div>
 <div id="modalAddDeduction" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #545454;color: #ffffff;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id="fullnym"></span> Deduction details</h4>
      </div>
      <div class="modal-body">
		<form class="form-horizontal bordered-row" >

				<div class="form-group">
                    <label class="col-sm-3 control-label">Type of Deduction</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="TypeDeduct">
                            <option disabled selected>--</option>
                            <option value="1">Loan</option>
                            <option value="2">Other Deduction</option>
                         </select>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label">Deduction</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="NameDeduction">
                            <option>--</option>
						</select>
                    </div>
                </div>
				<div class="form-group" id="isDeductVal" hidden>
                    <label class="col-sm-3 control-label">Value</label>
                    <div class="col-sm-6">
                         <input type="text" class="form-control"  placeholder="Value" id="DeductVal">
                    </div>
                </div>
       </form>
       </div>
      <div class="modal-footer" id="btnSampleDeduct" >
      </div>
    </div>

  </div>
</div>
     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>

</div>
</body>
<script>
  $(function(){
		$("#btnShow").trigger('click'); 
		$('#sidebar-menu').load('<?php echo base_url(); ?>/index.php/Templatedesign/sideBar');
		// $('#headerLeft').load('<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft');
  });	
</script> 
<script>
  $(function(){
    $("#loads").hide();
		
	  $("#btnSync").click(function(){
 		   $.ajax({
                //url of the function
                    url: '<?php echo base_url() ?>index.php/payrolltraineechecker/loan',
                    type: 'POST',
 					beforeSend:function(){
						$("#loads").show();
						$("#tblShow").html("");

					},
                    success:function(res)
                    {
					
					}
		   });
	  });
	  $("#btnExport").click(function(){
 		   $.ajax({
                //url of the function
                    url: '<?php echo base_url() ?>index.php/payrolltraineechecker/exportPayrollP/<?php echo $period["coverage_id"]."/". $stat["status"]?>',
                    type: 'POST',
					beforeSend:function(){
						$("#loads").show();
						$("#tblShow").html("");

					},
                    success:function(res)
                    {
						$("#loads").hide();
						$("#Export2").trigger("click");
					}
		   });
	  });
	  $("#btnShow").click(function(){
		  
			var payrollC = $("#payrollC").val();
			var employz = $("#employz").val();
			var pid = <?php echo $pid; ?>;
			var dataString = "payrollC="+payrollC+"&employz="+employz+"&pid="+pid;
 
 		    $.ajax({
                //url of the function
                    url: '<?php echo base_url() ?>index.php/payrolltraineechecker/emptype/1',
                    type: 'POST',
                    data:  dataString,
					beforeSend:function(){
						$("#loads").show();
						$("#tblShow").html("");

					},
                    success:function(res)
                    {
					$("#loads").hide();
							var td = "";
						var obj = jQuery.parseJSON(res);
						var timeTotal=0;
						var utTotal=0;
						var utTotal2=0;
						var TotalAmountHoliday=0;
						var timeTotalOTBCT=0;
						var timeTotalBCT;
						var timeTotalVal;
						var Mrate=0;
						var daily =0;
						var hourly =0;
						var ndExplode = "";
						var ndRate=0;
						var ndRatexBCT=0;
						var ndRatexBCT2=0;
						var ndRatexBCT3=0;
						var NDvalBCT=0;
						var HPval=0;
						var HPval2=0;
						var HPval3=0;
						var SSS;
						var PhilHealthCompute;
						var PagIbig;
						var pos_name;
						var hpRate;
						var fname;
						var salarymode;
						var emp_promoteId;
						var sss_id;
						var philhealth_id;
						var tax_id;
						var BIRCompute=0;
						var percent=0;
						var salarybase=0;
						var BIRTotalCompute=0;
						var BIRDescription;
						var TotalGross=0;
						var TotalNet=0;
						var TotalPay=0;
						var adjustSalary;
						var deductSalary;
						var payroll_adjDeduct_amount;
						var pemp_adjust_id;
						var empPosAllowance;
						var empPosBonus;
						var HPComputeHolidayLogin=0;
						var NDComputeHolidayLogin=0;
						var NDComputeHolidayLogout=0;
						var HPComputeHolidayLogout=0;
						var HPComputeHoliday=0;
						var NDComputeHoliday=0;
						var BCTComputeHoliday=0;
						var rate2=0;
						var isHoliday=0;
						var isND=0;
						var latecalc=0;
						var dayType=0;
						var ddaytypp = 1;
						var totalworkedtime=0;
						var breaks;
						var calchol=0;
 						var shift_type=0;
						var TotalHazardPayFinal=0;
						var TotalNighDiffFinal=0;
						var account;
						var account2;
						var breakConsumed=0;
						var NDvalBCT2=0;
						var MrateBIR2=0;
						var MrateBIR3=0;
						var dailySalary=0;

					if(res!=0){
						$(obj.employee).each(function(key,value){
							 $.each(value,function(k, v ){
								$.each(v,function(ky, vy ){ 
									breakConsumed = (vy["breaks"]>= vy["shiftBreak"]) ?  vy["shiftBreak"] : ((vy["breaks"]>=50) || (vy['total']>vy['totalwork']) ? vy["breaks"] : vy["shiftBreak"]);
									account2 = vy["account"].split("|");
									breaks = (account2[0] ==1) ? ((vy['totalwork']>0) ? 1 :0 ) : breakConsumed/60;
									totalworkedtime = (vy['total'] <= vy['totalwork']) ? vy['total'] : vy['totalwork'] ;
									timeTotalVal = (parseFloat(vy['totalwork'])>=parseFloat("9.0")) ? (parseFloat(vy['resLogNEW'])-parseFloat(breaks)) : ""+parseFloat(totalworkedtime)-parseFloat(breaks)+"" ;
									// timeTotalBCT = (vy['bct']=='Q') ? "0.21" : "0.00";
									timeTotalBCT ="0.00";
 									 daily = vy['rate'];
									 hourly = daily/8; 
										NDvalBCT = (vy['ndRemark']=='Y' && timeTotalBCT != 0.00) ?  "0.21" : "0.00" ;

									if(vy['hpRate']>0){
										hpRate  =vy['hpRate'];
										latecalc = vy['latecalc'];
									}else{
									hpRate  =0;
									latecalc  =0;
									}
									if(vy['nd']!=null){
										ndRatexBCT =   vy['nd']; 
 									 	HPval = ndRatexBCT;
									 }else{
										 ndRatexBCT = 0.00;
										 HPval =0.00;
									 }
 										//HPval = parseFloat(ndRate).toFixed(2);
									 
									// totalTimee =   (timeTotalVal >0) ? timeTotalVal - vy['latemins'] : "0.0";
/* 										totalTimee =   timeTotalVal;

									totalTimee2 =  (timeTotalVal >0) ? timeTotalVal - vy['latemins']  : "0.0";
 */									
/* 									totalTimee =  (timeTotalVal >0) ? timeTotalVal - vy['latemins'] : "0.0";
									totalTimee2 =  (timeTotalVal >0) ? timeTotalVal - vy['latemins'] : "0.0";
									 

									utTotal  += (totalTimee<vy['shiftTotalHours'] && totalTimee!=0) ? vy['shiftTotalHours'] - totalTimee : 0;
 */									// console.log(vy['date']+" =  "+utTotal+" , totalTimee->"+totalTimee+" , timeTotalVal->"+timeTotalVal);
									  totalTimee =   timeTotalVal;
									totalTimee2 =  (timeTotalVal >0) ? timeTotalVal - vy['latemins'] : "0.0";
 									utTotal2  = (totalTimee<vy['shiftTotalHours'] && totalTimee!=0) ? vy['shiftTotalHours'] - totalTimee : 0;

									//utTotal += (totalTimee<8 && totalTimee!=0) ? 8 - parseFloat(Math.round(totalTimee * 100) / 100) : 0;
									utTotal  += (totalTimee<vy['shiftTotalHours'] && totalTimee!=0) ? vy['shiftTotalHours'] - totalTimee : 0;
									   // console.log(vy['date']+" utTotal2= ##"+utTotal2+"+++++++ shiftTotalHours= ##"+vy['shiftTotalHours']+"+++++++ totalTimee= ##"+totalTimee+"+++++++ breaks= ##"+breaks+"+++++++ account= ##"+vy["account"]);
									
									 timeTotal +=parseFloat(totalTimee);
	
										////console.log(vy['date']+" = ##"+(parseFloat(vy['resLogNEW'])-parseFloat(breaks))+" ##"+vy['shiftTotalHours']+"- "+totalTimee+"  =  "+utTotal);
								 
									 isHoliday +=parseFloat(vy['isHoliday']);
									 
 									
									if(vy['dayType']>0){
										dayType += 0;
										dailySalary += 0;
									}else{
										dayType += 0;
										dailySalary += parseFloat(daily);
									}
									 console.log(vy['date']+" = "+dailySalary+" "+daily);
										// calchol = parseFloat(vy['calcHoliday1'] + vy['calcHoliday2']);
									if(vy['HolidayType1'] !='null'){
										timeTotalOTBCT +=parseFloat((timeTotalBCT)*vy['calcHoliday1'])+parseFloat(timeTotalBCT);
										NDvalBCT2 +=parseFloat((NDvalBCT)*vy['calcHoliday1'])+parseFloat(NDvalBCT);
										//alert("100 = "+parseFloat((timeTotalBCT)*1)+parseFloat(timeTotalBCT));
									}else{
										timeTotalOTBCT+=parseFloat(timeTotalBCT);
										NDvalBCT2+=parseFloat(NDvalBCT);
									}
									shift_type = (vy['shift_type']=="WORD") ? 0.5 : 0.0;

									  ndRatexBCT2+=parseFloat(ndRatexBCT)+parseFloat(NDvalBCT2)+parseFloat(vy['NDComputeHolidayLogin'])+parseFloat(vy['NDComputeHolidayLogout'])- parseFloat(vy['latecalc']); // Night Diff + ( [Special/Regular] Holiday ND) + WORD...
 									HPval2 +=parseFloat(HPval)+parseFloat(vy['HPComputeHolidayLogin'])+parseFloat(vy['HPComputeHolidayLogout']) + parseFloat(NDvalBCT2) - parseFloat(vy['latecalc']); // Hazard Pay + ( [Special/Regular] Holiday HP)
  									SSS = parseFloat(vy['SSS']/2).toFixed(2);
									// PhilHealthCompute = parseFloat(vy['PhilHealthCompute']/2).toFixed(2); // OLD WAY
									PagIbig = parseFloat((vy['rate']*.02)/2).toFixed(2);
									pos_name =vy['pos_name'];
									// hpRate  =  (vy['hpRate']!="0.0") ? vy['hpRate'];
									 
									
									account  =vy['account'];
									 // //console.log(hpRate);
									fname =vy['fname'];
									salarymode =vy['salarymode'];
									sss_id =vy['sss_id'];
									philhealth_id =vy['philhealth_id'];
									tax_id =vy['tax_id'];
									emp_promoteId =vy['emp_promoteId'];
									BIRCompute =vy['BIRCompute'];
									percent =vy['BIRPercent'];
									salarybase =vy['BIRSalaryBase'];
									TotalAmountHoliday +=parseFloat(vy['TotalAmountHoliday']);
									// console.log(vy['date']+" = "+vy['TotalAmountHoliday'])
									// BIRDescription =  (Mrate>=21000) ? vy['BIRDescription'] : "---"; 
									payroll_adjDeduct_amount =parseFloat(vy['payroll_adjDeduct_amount']/2);
									pemp_adjust_id =vy['pemp_adjust_id'];
									adjustSalary = (vy['adjustSalary']!=null) ? vy['adjustSalary'].split(","): 0;
									deductSalary = (vy['deductSalary']!=null) ? vy['deductSalary'].split(","): 0;
									empPosAllowance = (vy['empPosAllowance']!=0) ? vy['empPosAllowance'].split(","): 0;
									empPosBonus = (vy['empPosBonus']!=0) ? vy['empPosBonus'].split(",") : 0;
										NDComputeHolidayLogin += parseFloat(vy['NDComputeHolidayLogin'])*parseFloat(hourly*.15);
										NDComputeHolidayLogout += parseFloat(vy['NDComputeHolidayLogout'])*parseFloat(hourly*.15);
										HPComputeHolidayLogout += parseFloat(vy['HPComputeHolidayLogout']);
										HPComputeHolidayLogin += parseFloat(vy['HPComputeHolidayLogin']);
 
										rate2 = vy['rate2'];
						TotalHazardPayFinal+=parseFloat(((HPval2 * (hourly*hpRate))));
 
								TotalNighDiffFinal+=((ndRatexBCT2*parseFloat(hourly*.15).toFixed(2)));
								  // console.log(vy['date']+" =  "+ndRatexBCT2+"*"+parseFloat(hourly*.15).toFixed(2)+" **"+((ndRatexBCT2*parseFloat(hourly*.15).toFixed(2)))+"**= TotalNighDiffFinal= "+TotalNighDiffFinal);
								////console.log(vy['date']+"----"+((ndRatexBCT2*parseFloat(hourly*.15).toFixed(2))));
									// console.log(vy['date']+" = "+ndRatexBCT2);

								var aaa = ((ndRatexBCT2*(hourly*.15))) -  parseFloat(latecalc * parseFloat(hourly*hpRate));
 								 HPval3 =HPval2;
								 HPval2 =0;
								 NDvalBCT2 =0;
								 ndRatexBCT3=ndRatexBCT2;
								 ndRatexBCT2 =0;
								 latecalc =0;
								 breakConsumed =0;
 
								});
								Mrate= dailySalary*2;
								if(Mrate<=10000){
										PhilHealthCompute =  (137.50/2);

									}else if(Mrate>=10000.01 && Mrate<=39999.99){
										PhilHealthCompute =  ((Mrate*0.0275)/4).toFixed(2);

									}else if(Mrate>=40000){
										PhilHealthCompute =  (550/2);

									}else{
										PhilHealthCompute =  0;

									}	
								BIRTotalCompute = parseFloat(((rate2-salarybase)*percent)) + parseFloat(BIRCompute);
/* 								console.log(parseFloat(((rate2 +"-"+salarybase) +"***"+percent)) +"+"+ parseFloat(BIRCompute));
								console.log(rate2 +"-"+salarybase +"***"+percent +"+"+ parseFloat(BIRCompute))
 */
								// var TotalBCTxHourly = timeTotalOTBCT.toFixed(2) * hourly.toFixed(2); //with OT BCT
								var TotalBCTxHourly = 0;
 									var i;
									var ii;
									var iii=0;
									var x;
									var xx;
									var xxx=0;
									var y;
									var yy;
									var yyy=0;
									var jjj=0;
									var HPHoliday=0;
									var NDHoliday=0;
									var BIRTotalCompute2=0;
									 HPHoliday=(parseFloat(HPComputeHolidayLogin+HPComputeHolidayLogout)*parseFloat(hourly*hpRate));
									 NDHoliday=NDComputeHolidayLogin+NDComputeHolidayLogout;
									var finalTotalGross=0;
									var TotalHazardPayFinal2;
 									  TotalHazardPayFinal2 = (TotalHazardPayFinal>0) ? TotalHazardPayFinal : 0;
									  TotalHazardPayFinal =0;
 									 var TotalNighDiffFinal2;
									 TotalNighDiffFinal2 =  (TotalNighDiffFinal>0) ? TotalNighDiffFinal : 0;
									 TotalNighDiffFinal=0;
 										BIRTotalCompute2 = (BIRTotalCompute>0) ? ((Mrate>=21000) ? BIRTotalCompute : 0 ):0 ;
 									 TotalGross =  parseFloat(TotalBCTxHourly)+parseFloat(TotalNighDiffFinal2)+parseFloat(TotalHazardPayFinal2);
									 //console.log(parseFloat(TotalBCTxHourly)+"***"+parseFloat(TotalNighDiffFinal2)+"***"+parseFloat(TotalHazardPayFinal2));

									td+="<tr>"+
								 
									"<td hidden>"+ emp_promoteId+"</td>"+
									"<td hidden>"+ sss_id+"</td>"+
									"<td hidden>"+ philhealth_id+"</td>"+
									"<td hidden>"+ tax_id+"</td>"+
									"<td  class='headcol'>"+ fname+"</td >"+
 									"<td class='long'>"+ pos_name+"</td>"+
 									"<td class='long'>--</td>"+
 									"<td class='long'>"+ dailySalary+"</td>"+
 									"<td class='long'>"+ daily+"</td>"+
 									"<td class='long'>"+ hourly.toFixed(2)+"</td>"+
									"<td class='long'>"+ timeTotal.toFixed(2)+"</td>";
								 
									if(empPosAllowance!=0){
										$.each(empPosAllowance,function(susi,balue){
											xx = balue.split("=");	
										td+="<td class='long'> <b>"+ parseFloat((xx[1] - ((xx[1]/10)*dayType))).toFixed(2)+" ("+xx[0]+") </b></td>";
										xxx += parseFloat((xx[1] - ((xx[1]/10)*dayType)));
 										});
										}else{
										td+="<td class='long'> <b>--</b></td><td> <b>--</b></td><td> <b>--</b></td>";
											xxx = 0;
 										}
										if(empPosBonus!=0){
										$.each(empPosBonus,function(susi,balue){
											
											yy = balue.split("=");	
 										yyy += parseFloat(yy[1]);
  										});
										}else{
 											yyy = 0;
 										}
									if(yyy!=0){
										td+="<td class='long'> <b>"+ parseFloat(yyy).toFixed(2)+"</b></td>";
									}else{
										td+="<td class='long'> <b>N/A</b> </td>";
									}
									td +="<td class='long' hidden><b>"+TotalBCTxHourly.toFixed(2)+"</b></td>"+
									"<td class='long'><b>"+TotalNighDiffFinal2.toFixed(2)+"</b></td>"+
									"<td class='long'><b>"+ TotalHazardPayFinal2.toFixed(2)+"</b></td>"+
									"<td class='long'><b>"+ TotalAmountHoliday.toFixed(2)+"</b></td>"+
									"<td class='long'><b>0.00</b></td>"+
									"<td class='long'><b>0.00</b></td>"+
									"<td class='long'><b>0.00</b></td>";
									 
									// td+="<td class='long'><b>"+BIRTotalCompute2.toFixed(2)+"<br>("+BIRDescription+")</b></td>";
									td+="<td class='long'><b>0.00<br>(--)</b></td>";
								 
										if(adjustSalary!=0){
										$.each(adjustSalary,function(yek,lav){
											
											ii = lav.split("=");	
 										iii += parseFloat(ii[1]);
 										});
										}else{
											iii = 0;
 										}
										//finalTotalGross = (parseFloat(TotalGross)+parseFloat(iii)+parseFloat(Mrate/2));
										finalTotalGross = (parseFloat(TotalAmountHoliday)+parseFloat(TotalGross)+parseFloat(xxx) +parseFloat(iii)+parseFloat(yyy)+parseFloat(Mrate/2));
										//console.log((parseFloat(TotalAmountHoliday)+"***"+parseFloat(TotalGross)+"***"+parseFloat(xxx)+"***"+parseFloat(iii)+"***"+parseFloat(yyy)+"***"+parseFloat(Mrate/2)))
									td+="<td class='long'><b>"+iii.toFixed(2)+"</b></td>";
										if(deductSalary!=0){
										$.each(deductSalary,function(yek,lav){
											
											ii = lav.split("=");	
 										jjj += parseFloat(ii[1]);
										//console.log(ii[0]+" = "+ii[1])
 										});
										}else{
											jjj = 0;
 										}
										// TotalNet =  parseFloat(SSS)+parseFloat(PhilHealthCompute)+parseFloat(PagIbig)+parseFloat(BIRTotalCompute)+parseFloat(jjj);
										// TotalPay = (finalTotalGross) - TotalNet;
										TotalNet =  parseFloat(jjj) + payroll_adjDeduct_amount  + parseFloat(latecalc * hourly)+ parseFloat(dayType * daily) +   parseFloat(utTotal * hourly);
										////console.log(parseFloat(utTotal * hourly));
										////console.log( parseFloat(SSS)+"+"+parseFloat(PhilHealthCompute)+"+"+parseFloat(PagIbig)+"+"+BIRTotalCompute2.toFixed(2)+"+"+parseFloat(jjj) +"+"+ payroll_adjDeduct_amount  +"+"+ parseFloat(latecalc * hourly) +"+"+ parseFloat(dayType * daily)+"+"+ parseFloat(utTotal * hourly) )
										TotalPay = (finalTotalGross) - TotalNet;
 									td+="<td class='long'><b>"+jjj.toFixed(2)+"</b></td>"+
									"<td class='long'><b><a href='#' data-toggle='modal' data-target='#myModal' onclick='grossDetails("+k+")'> "+finalTotalGross.toFixed(2)+" </a></td>"+
									"<td class='long'><b><a href='#' data-toggle='modal' data-target='#myModal' onclick='earningDetails("+k+")'>"+TotalNet.toFixed(2)+"</a> </td>"+
									"<td class='long' style='color:green;background: #ffe9c9;text-align: center;vertical-align: middle;font-size: medium;'><b>"+ TotalPay.toFixed(2)+" <b></td>"+
									"<td hidden> "+pemp_adjust_id+" </td>"+
									"<td hidden> "+isHoliday+" </td>"+
									"<td hidden> "+HPval3+" </td>"+
									"<td hidden> "+ndRatexBCT3+" </td>"+
									"<td hidden> "+account+" </td>"+
									 "<td hidden> "+((adjustSalary!=0) ? adjustSalary : 0)+" </td>"+
									 "<td hidden> "+((deductSalary!=0) ? deductSalary : 0)+" </td>"+
									 "<td hidden> "+parseFloat(parseFloat(latecalc * hourly)+ parseFloat(dayType * daily)+parseFloat(utTotal * hourly)).toFixed(2)+" </td>"+
									"<td hidden> "+((empPosBonus!=0) ? empPosBonus : 0)+" </td>"+
									"<td hidden>"+salarymode+"</td>"+



									 " </tr>";
			
 								 // td+="<tr><td colspan=24 style='background: #d0ffd2;'> </td></tr>";
									isHoliday =0;
									timeTotal =0;
									utTotal =0;
									timeTotalOTBCT =0;
									  MrateBIR2=0;
									  MrateBIR3=0;
									ndRatexBCT2 =0;									
									ndRatexBCT3 =0;									
									HPval2 =0;	
									HPval3 =0;	
									rate2= 0;	
									hpRate =0;
									latecalc =0;
									HPComputeHolidayLogin=0;
									NDComputeHolidayLogin=0;
									NDComputeHolidayLogout=0;
									HPComputeHolidayLogout=0;
									TotalAmountHoliday=0;
									BIRTotalCompute2=0;	
									dayType=0;	
									ddaytypp=0; 
									iii=0;									
									xxx=0;									
									yyy=0;									
									Mrate=0;									
									dailySalary=0;									
 							  });
						});
					}else{
						td+=" ";	
						}
						 $("#tblShow").html(td);
						
                    }
                }); 
	  });
		$("#btnSavePayroll").click(function(){
		swal({
			  title: "Are you sure?",
			  text: "Employees will be marked as final and payslips will be auto generated",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes!",
			  cancelButtonText: "No!",
			  closeOnConfirm: false,
			  closeOnCancel: false
		}).then(function(isConfirm) {
				if (isConfirm) {
				var rowvalue = [];
				$("#tblShow  > tr").each(function(i, v) {

				rowvalue[i] = $('td', this).map(function() {
				return $(this).text()
				}).get()

				});
				var dataSet = JSON.stringify(rowvalue);
				var coverage_id = "<?php echo $period["coverage_id"]; ?>";

				// //console.log(rowvalue);
				$.ajax({
					url: '<?php echo base_url() ?>index.php/payrolltraineechecker/savePayroll',
					type: "POST",
					data: {
					dataArray: dataSet,
					coverage_id: coverage_id,
					emp: '<?php echo $stat["status"]; ?>'
					},
					success: function (result) {
						swal("Success","Probationary employees have been created payslips.", "success");
					}
				});  
				}else{
					swal("Cancelled",":)",	"error");
				}
	});


});	
	$("#TypeDeduct").change(function(){
			var value = $(this).val();
			var emp_id = $("#empsIs").text();
			var pid = <?php echo $pid; ?>;
			var link = (value==1) ? "addLoan" : "addOtherDeduct";
				if(value==1){
 					$("#isDeductVal").fadeOut();
					$("#btnSampleDeduct").html("<button type='button' class='btn btn-default' data-dismiss='modal' onclick='btnSaveAddDeduct(1,"+pid+","+emp_id+")'>Save</button>");
				}else{
					$("#isDeductVal").fadeIn();
					$("#btnSampleDeduct").html("<button type='button' class='btn btn-default' data-dismiss='modal' onclick='btnSaveAddDeduct(2,"+pid+","+emp_id+")'>Save</button>");
				}
 			   $.ajax({
					url: '<?php echo base_url() ?>index.php/payrolltraineechecker/'+link,
					type: "POST",
					data: {
						value: value,
						pid: pid,
						emp_id: emp_id,
 						},
					success: function (result) {

						  $("#NameDeduction").html(result);
					}
				});  
	});
	 
	});	
</script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
   
  <script>
            var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
            $("#demoform").submit(function() {
             // alert($('[name="duallistbox_demo1[]"]').val());
              return false;
            });
		 
			function btnSaveAddDeduct(value,pid,emp_id){
				var nd = $("#NameDeduction").val();
				var deductVal = $("#DeductVal").val();
				var link = (value==1) ? "addDeductLonNew" : "addDeductNew";

				   $.ajax({
					url: '<?php echo base_url() ?>index.php/payrolltraineechecker/'+link,
					type: "POST",
					data: {
						nd: nd,
						pid: pid,
						emp_id: emp_id,
						deductVal: deductVal,
 						},
					success: function (result) {
						// alert(deductVal);
						 $("#btnReload").trigger('click');
					}
				}); 
			}
			function earningDetails(id){
		  
		  var pc = $("#payrollC").val();
		  var pid = <?php echo $pid; ?>;
 		   var dataString = "pc="+pc+"&emp_id="+id+"&pid="+pid;
			
			$("#DeductReload").html('<button type="button" class="btn btn-sm btn-info" id="btnReload" onclick="earningDetails('+id+')">Reload</button>').show();
 		    $.ajax({
                //url of the function
                    url: '<?php echo base_url() ?>index.php/payrolltraineechecker/emptype/2',
                    type: 'POST',
                    data:  dataString,
					beforeSend:function(){
 
					},
                    success:function(res)
                    {
 							var td = "";
						var obj = jQuery.parseJSON(res);
						var timeTotal=0;
						var utTotal=0;
						var utTotal2=0;
						var TotalAmountHoliday=0;
						var timeTotalOTBCT=0;
						var timeTotalBCT;
						var timeTotalVal;
						var Mrate=0;
						var MrateBIR=0;
						var MrateBIR2=0;
						var daily =0;
						var hourly =0;
						var ndExplode = "";
						var ndRate=0;
						var ndRatexBCT=0;
						var ndRatexBCT2=0;
						var ndRatexBCT3=0;
						var NDvalBCT=0;
						var HPval=0;
						var HPval2=0;
						var HPval3=0;
						var SSS;
						var PhilHealthCompute;
						var PagIbig;
						var pos_name;
						var hpRate;
						var fname;
						var emp_promoteId;
						var sss_id;
						var philhealth_id;
						var tax_id;
						var BIRCompute=0;
						var percent=0;
						var salarybase=0;
						var BIRTotalCompute=0;
						var BIRDescription;
						var TotalGross=0;
						var TotalNet=0;
						var TotalPay=0;
						var adjustSalary;
						var deductSalary;
						var payroll_adjDeduct_amount;
						var pemp_adjust_id;
						var empPosAllowance;
						var empPosBonus;
						var HPComputeHolidayLogin=0;
						var NDComputeHolidayLogin=0;
						var NDComputeHolidayLogout=0;
						var HPComputeHolidayLogout=0;
						var HPComputeHoliday=0;
						var NDComputeHoliday=0;
						var BCTComputeHoliday=0;
						var rate2=0;
						var isHoliday=0;
						var isND=0;
						var latecalc=0;
						var dayType=0;
						var ddaytypp = 1;
						var totalworkedtime=0;
						var breaks;
						var calchol=0;
 						var shift_type=0;
						var TotalHazardPayFinal=0;
						var TotalNighDiffFinal=0;
						var account;
						var deductName;
						var emp_idd;
						var breakConsumed;
						var NDvalBCT2=0;
						var AnnualSalaryBase=0;
						var Add=0;
						var rsBIR=0;

					if(res!=0){
						$(obj.employee).each(function(key,value){
							 $.each(value,function(k, v ){
								$.each(v,function(ky, vy ){
									breakConsumed = (vy["breaks"]>= vy["shiftBreak"]) ?  vy["shiftBreak"] : ((vy["breaks"]>=50) || (vy['total']>vy['totalwork']) ? vy["breaks"] : vy["shiftBreak"]);
									account2 = vy["account"].split("|");
									breaks = (account2[0] ==1) ? ((vy['totalwork']>0) ? 1 :0 ) : breakConsumed/60;
									totalworkedtime = (vy['total'] <= vy['totalwork']) ? vy['total'] : vy['totalwork'] ;
									timeTotalVal = (parseFloat(vy['totalwork'])>=parseFloat("9.0")) ? (parseFloat(vy['resLogNEW'])-parseFloat(breaks)) : ""+parseFloat(totalworkedtime)-parseFloat(breaks)+"" ;
									timeTotalBCT = (vy['bct']=='Q') ? "0.21" : "0.00";
									 Mrate = vy['rate'];
									 daily = vy['rate'];
									 hourly = daily/8; 
										NDvalBCT = (vy['ndRemark']=='Y' && timeTotalBCT != 0.00) ?  "0.21" : "0.00" ;

									if(vy['nd']!=null){
										ndRatexBCT =   vy['nd']; 
 									 	HPval = ndRatexBCT;
									 }else{
										 ndRatexBCT = 0.00;
										 HPval =0.00;
									 }
 										//HPval = parseFloat(ndRate).toFixed(2);
									 
									  totalTimee =   timeTotalVal;
									totalTimee2 =  (timeTotalVal >0) ? timeTotalVal - vy['latemins'] : "0.0";
									//utTotal += (totalTimee<8 && totalTimee!=0) ? 8 - parseFloat(Math.round(totalTimee * 100) / 100) : 0;
									utTotal  += (totalTimee<vy['shiftTotalHours'] && totalTimee!=0) ? vy['shiftTotalHours'] - totalTimee : 0;
 									 timeTotal +=parseFloat(totalTimee);
  									 isHoliday +=parseFloat(vy['isHoliday']);
									 
									 // console.log(vy['date']+" =  "+utTotal+" , totalTimee->"+totalTimee+" , timeTotalVal->"+timeTotalVal);

									if(vy['dayType']>0){
										dayType += vy['dayType'] ;
									}else{
										dayType += 0;
									}
									 // console.log(vy['date']+" "+vy['dayType']);
										// calchol = parseFloat(vy['calcHoliday1'] + vy['calcHoliday2']);
									if(vy['HolidayType1'] !='null'){
										timeTotalOTBCT +=parseFloat((timeTotalBCT)*vy['calcHoliday1'])+parseFloat(timeTotalBCT);
										NDvalBCT2 +=parseFloat((NDvalBCT)*vy['calcHoliday1'])+parseFloat(NDvalBCT);
										//alert("100 = "+parseFloat((timeTotalBCT)*1)+parseFloat(timeTotalBCT));
									}else{
										timeTotalOTBCT+=parseFloat(timeTotalBCT);
										NDvalBCT2+=parseFloat(NDvalBCT);
									}  
									shift_type = (vy['shift_type']=="WORD") ? 0.5 : 0.0;

									  ndRatexBCT2+=parseFloat(ndRatexBCT)+parseFloat(NDvalBCT2)+parseFloat(vy['NDComputeHolidayLogin'])+parseFloat(vy['NDComputeHolidayLogout'])- parseFloat(vy['latecalc']); // Night Diff + ( [Special/Regular] Holiday ND) + WORD...
 									HPval2 +=parseFloat(HPval)+parseFloat(vy['HPComputeHolidayLogin'])+parseFloat(vy['HPComputeHolidayLogout']) + parseFloat(NDvalBCT2) - parseFloat(vy['latecalc']); // Hazard Pay + ( [Special/Regular] Holiday HP)
									  ////console.log(vy['date']+" = "+NDvalBCT);
 									SSS = parseFloat(vy['SSS']/2).toFixed(2);
									// PhilHealthCompute = parseFloat(vy['PhilHealthCompute']/2).toFixed(2); // OLD WAY
									if(Mrate<=10000){
										PhilHealthCompute =  (137.50/2);

									}else if(Mrate>=10000.01 && Mrate<=39999.99){
										PhilHealthCompute =  ((Mrate*0.0275)/4).toFixed(2);

									}else if(Mrate>=40000){
										PhilHealthCompute =  (550/2);

									}else{
										PhilHealthCompute =  0;

									}
									
									PagIbig = parseFloat((vy['rate']*.02)/2).toFixed(2);
									pos_name =vy['pos_name'];
									// hpRate  =  (vy['hpRate']!="0.0") ? vy['hpRate'];
									 
									if(vy['hpRate']>0){
										hpRate  =vy['hpRate'];
										latecalc = vy['latecalc'];
									}else{
									hpRate  =0;
									latecalc  =0;
									}
									
	
									
									account  =vy['account'];
									 // //console.log(hpRate);
									fname =vy['fname'];
									emp_idd =vy['emp_id'];
									sss_id =vy['sss_id'];
									philhealth_id =vy['philhealth_id'];
									tax_id =vy['tax_id'];
									emp_promoteId =vy['emp_promoteId'];
									BIRCompute =vy['BIRCompute'];
									percent =vy['BIRPercent'];
									salarybase =vy['BIRSalaryBase'];
									TotalAmountHoliday +=parseFloat(vy['TotalAmountHoliday']);
									BIRDescription = (Mrate>=21000) ?  vy['BIRDescription'] : "---";
									payroll_adjDeduct_amount =parseFloat(vy['payroll_adjDeduct_amount']/2);
									pemp_adjust_id =vy['pemp_adjust_id'];
									adjustSalary = (vy['adjustSalary']!=null) ? vy['adjustSalary'].split(","): 0;
									deductSalary = (vy['deductSalary']!=null) ? vy['deductSalary'].split(","): 0;
									deductName = (vy['deductName']!=null) ? vy['deductName'].split(","): 0;
									empPosAllowance = (vy['empPosAllowance']!=0) ? vy['empPosAllowance'].split(",") : 0;
									empPosBonus = (vy['empPosBonus']!=0) ? vy['empPosBonus'].split(",") : 0;
									  // //console.log(vy['date']+" = "+latecalc+" => "+vy['latemins22']);
									NDComputeHolidayLogin += parseFloat(vy['NDComputeHolidayLogin'])*parseFloat(hourly*.15);
									NDComputeHolidayLogout += parseFloat(vy['NDComputeHolidayLogout'])*parseFloat(hourly*.15);
 									HPComputeHolidayLogout += parseFloat(vy['HPComputeHolidayLogout']);
									HPComputeHolidayLogin += parseFloat(vy['HPComputeHolidayLogin']);
									rate2 = vy['rate2'];
									TotalHazardPayFinal+=parseFloat(((HPval2 * (hourly*hpRate)))- parseFloat(latecalc * parseFloat(hourly*.15)));
 									TotalNighDiffFinal +=((ndRatexBCT2*(hourly*.15))) -  parseFloat(latecalc * parseFloat(hourly*hpRate));
 
									 HPval3 =HPval2;
									 HPval2 =0;
									 ndRatexBCT3=ndRatexBCT2;
									 ndRatexBCT2 =0;
									 latecalc =0;
									 NDvalBCT2 =0;
								});
								BIRTotalCompute = parseFloat(((rate2-salarybase)*percent)) + parseFloat(BIRCompute);
								
								var TotalBCTxHourly = timeTotalOTBCT.toFixed(2) * hourly.toFixed(2);
 									var i;
									var ii;
									var iii=0
									var dd;
									var dd2;
									var dd3=0;
									var x;
									var xx;
									var xxx=0;
									var y;
									var yy;
									var yyy=0;
									var jjj=0;
									var HPHoliday=0;
									var NDHoliday=0;
									var BIRTotalCompute2=0;
									var percentz=0;
									 HPHoliday=(parseFloat(HPComputeHolidayLogin+HPComputeHolidayLogout)*parseFloat(hourly*hpRate));
									 NDHoliday=NDComputeHolidayLogin+NDComputeHolidayLogout;
									var finalTotalGross=0;
									var TotalHazardPayFinal2;
 									  TotalHazardPayFinal2 = TotalHazardPayFinal;
									  TotalHazardPayFinal =0;
 									 var TotalNighDiffFinal2;
									 TotalNighDiffFinal2 = TotalNighDiffFinal;
									 TotalNighDiffFinal=0;
 									 MrateBIR = (PhilHealthCompute*2) + (SSS*2) + (PagIbig*2);//EXPENSES
									MrateBIR3 = Mrate-MrateBIR;//MRATE - EXPENSES
									MrateBIR2=MrateBIR3*12;
									if(MrateBIR2<=250000){
										BIRTotalCompute2=0;
										}
									else if(MrateBIR2<=400000){
										AnnualSalaryBase = 250000;
										Add = 0;
										percentz = 0.20;
										rsBIR = ((MrateBIR2 - AnnualSalaryBase)*percentz)+Add;
										BIRTotalCompute2 = rsBIR/ 24;
										
										}
									else if(MrateBIR2<=800000){
										AnnualSalaryBase = 400000;
										Add = 30000;
										percentz = 0.25;

										rsBIR = ((MrateBIR2 - AnnualSalaryBase)*percentz)+Add;
										BIRTotalCompute2 = rsBIR/ 24;

}
									else if(MrateBIR2<=2000000){
										AnnualSalaryBase = 800000;
										Add = 130000;
										percentz = 0.30;

										rsBIR = ((MrateBIR2 - AnnualSalaryBase)*percentz)+Add;
										BIRTotalCompute2 = rsBIR/ 24;

										}
									else if(MrateBIR2<=8000000){
										AnnualSalaryBase = 2000000;
										Add = 490000;
										percentz = 0.32;

										rsBIR = ((MrateBIR2 - AnnualSalaryBase)*percentz)+Add;
										BIRTotalCompute2 = rsBIR/ 24;

										}
									else if(MrateBIR2>8000000){
										AnnualSalaryBase = 2000000;
										Add = 2410000;
										percentz = 0.35;

										rsBIR = ((MrateBIR2 - AnnualSalaryBase)*percentz)+Add;
										BIRTotalCompute2 = rsBIR/ 24;

										}
									else{
										BIRTotalCompute2=0;

									}
										//BIRTotalCompute2 = (BIRTotalCompute<0) ? 0: ((Mrate>=21000) ? BIRTotalCompute : 0);

									 // TotalGross =  parseFloat(TotalBCTxHourly)+parseFloat((ndRatexBCT2*(hourly*.15)))+parseFloat((HPval2 * (hourly*hpRate)));
									 TotalGross =  parseFloat(TotalBCTxHourly)+parseFloat(TotalNighDiffFinal2)+parseFloat(TotalHazardPayFinal2);
									/* td+="<tr><td>SSS </td><td><b> P"+SSS+"</b></td></tr>"+
									"<tr><td>PHIC </td><td><b> P "+PhilHealthCompute+"</b></td></tr>"+
									"<tr><td>HDMF </td><td><b> P "+PagIbig+"</b></td></tr>"+
									"<tr><td>BIR ("+BIRDescription+") </td><td><b> P "+BIRTotalCompute2.toFixed(2)+"</b></td></tr>"; */
									td+="<tr><td>SSS </td><td><b> P0</b></td></tr>"+
									"<tr><td>PHIC </td><td><b> P 0</b></td></tr>"+
									"<tr><td>HDMF </td><td><b> P 0</b></td></tr>"+
									"<tr><td>BIR (--) </td><td><b> P 0</b></td></tr>";
										if(deductSalary!=0){
											$.each(deductSalary,function(yek,lav){
												
												ii = lav.split("=");	
										
											if(ii[1]>0){
												jjj += parseFloat(ii[1]);
												td+="<tr><td> "+ii[0]+"</td> <td>  P "+ii[1]+"</td></tr>";
											}
											});
										}else{
											jjj = 0;
 										}
										if(deductName!=0){
											$.each(deductName,function(keyy,vall){
												dd = vall.split("-");	
											if(dd[1]>0){
 												td+="<tr><td> "+dd[0]+"</td> <td>  P "+parseFloat(dd[1]/2).toFixed(2)+"</td></tr>";
											}
											});
										}else{
											dd2 = 0;
 										}
										/* if(dayType>0){
										td+="<tr><td> Absences [ "+dayType+" day(s) ]</td> <td>  P "+parseFloat(dayType * daily).toFixed(2)+"</td></tr>";
										} */
										if(latecalc>0){
										td+="<tr><td> Tardiness [ "+latecalc+" days(s)]</td> <td>  P "+parseFloat(latecalc * hourly).toFixed(2)+"</td></tr>";
										}
										if(utTotal>0){
										td+="<tr><td> Late/Undertime [ "+utTotal.toString().substr(0, 5)+" hour(s)]</td> <td>  P "+parseFloat(utTotal * hourly).toFixed(2)+"</td></tr>";
										}
										 TotalNet =  parseFloat(jjj) + payroll_adjDeduct_amount  + parseFloat(latecalc * hourly)+ parseFloat(utTotal * hourly);
											console.log(jjj+" "+payroll_adjDeduct_amount+" "+latecalc+" "+utTotal);
  									td+="<tr style='background: #f9f2b3;font-weight: 900;'><td>Total </td><td> P "+TotalNet.toFixed(2)+"</td></tr>";
 
 									AnnualSalaryBase =0;
 									isHoliday =0;
									timeTotal =0;
									utTotal =0;
									MrateBIR =0;
									MrateBIR2 =0;
									MrateBIR3 =0;
									utTotal2 =0;
									timeTotalOTBCT =0;
									ndRatexBCT2 =0;									
									ndRatexBCT3 =0;									
									HPval2 =0;	
									HPval3 =0;	
									rate2= 0;	
									hpRate =0;
									latecalc =0;
									 HPComputeHolidayLogin=0;
									NDComputeHolidayLogin=0;
									NDComputeHolidayLogout=0;
									HPComputeHolidayLogout=0;
									TotalAmountHoliday=0;
									BIRTotalCompute2=0;	
									dayType=0;	
									ddaytypp=0;	
									iii=0;									
									xxx=0;									
 							  });
						});
					}else{
						td+=" ";	
					}
						 $("#detailedd").html(td);
						 $("#empsIs").html(+emp_idd);
						 $("#empsName").html(" Deduction details  of "+fname);
						 $("#footerEmp").fadeIn();

						
                    }
                }); 				
				}
				
				function grossDetails(id){
			var pc = $("#payrollC").val();
			var pid = <?php echo $pid; ?>;
			var dataString = "pc="+pc+"&emp_id="+id+"&pid="+pid;
			$("#DeductReload").hide();
			// $("#DeductReload").html('<button type="button" class="btn btn-sm btn-info" id="btnReload" onclick="earningDetails('+id+')">Reload</button>');
 		    $.ajax({
                //url of the function
                    url: '<?php echo base_url() ?>index.php/payrolltraineechecker/emptype/3',
                    type: 'POST',
                    data:  dataString,
					beforeSend:function(){
 
					},
                    success:function(res)
                    {
 							var td = "";
						var obj = jQuery.parseJSON(res);
						var timeTotal=0;
						var utTotal=0;
						var TotalAmountHoliday=0;
						var timeTotalOTBCT=0;
						var timeTotalBCT;
						var timeTotalVal;
						var Mrate=0;
						var daily =0;
						var hourly =0;
						var ndExplode = "";
						var ndRate=0;
						var ndRatexBCT=0;
						var ndRatexBCT2=0;
						var ndRatexBCT3=0;
						var NDvalBCT=0;
						var HPval=0;
						var HPval2=0;
						var HPval3=0;
						var SSS;
						var PhilHealthCompute;
						var PagIbig;
						var pos_name;
						var hpRate;
						var fname;
						var emp_promoteId;
						var sss_id;
						var philhealth_id;
						var tax_id;
						var BIRCompute=0;
						var percent=0;
						var salarybase=0;
						var BIRTotalCompute=0;
						var BIRDescription;
						var TotalGross=0;
						var TotalNet=0;
						var TotalPay=0;
						var adjustSalary;
						var deductSalary;
						var payroll_adjDeduct_amount;
						var pemp_adjust_id;
						var empPosAllowance;
						var empPosBonus;
						var HPComputeHolidayLogin=0;
						var NDComputeHolidayLogin=0;
						var NDComputeHolidayLogout=0;
						var HPComputeHolidayLogout=0;
						var HPComputeHoliday=0;
						var NDComputeHoliday=0;
						var BCTComputeHoliday=0;
						var rate2=0;
						var isHoliday=0;
						var isND=0;
						var latecalc=0;
						var dayType=0;
						var ddaytypp = 1;
						var totalworkedtime=0;
						var breaks;
						var calchol=0;
 						var shift_type=0;
						var TotalHazardPayFinal=0;
						var TotalNighDiffFinal=0;
						var account;
						var deductName;
						var emp_idd;
						var NDvalBCT2=0;
						var dailySalary2=0;

					if(res!=0){
						$(obj.employee).each(function(key,value){
							 $.each(value,function(k, v ){
								$.each(v,function(ky, vy ){
									breakConsumed = (vy["breaks"]>= vy["shiftBreak"]) ?  vy["shiftBreak"] : ((vy["breaks"]>=50) || (vy['total']>vy['totalwork']) ? vy["breaks"] : vy["shiftBreak"]);
									account2 = vy["account"].split("|");
									breaks = (account2[0] ==1) ? ((vy['totalwork']>0) ? 1 :0 ) : breakConsumed/60;
									totalworkedtime = (vy['total'] <= vy['totalwork']) ? vy['total'] : vy['totalwork'] ;
									timeTotalVal = (parseFloat(vy['totalwork'])>=parseFloat("9.0")) ? (parseFloat(vy['resLogNEW'])-parseFloat(breaks)) : ""+parseFloat(totalworkedtime)-parseFloat(breaks)+"" ;
									// timeTotalBCT = (vy['bct']=='Q') ? "0.21" : "0.00";
									timeTotalBCT ="0.00";
 									 daily = vy['rate'];
									 hourly = daily/8; 
										NDvalBCT = (vy['ndRemark']=='Y' && timeTotalBCT != 0.00) ?  "0.21" : "0.00" ;

									if(vy['hpRate']>0){
										hpRate  =vy['hpRate'];
										latecalc = vy['latecalc'];
									}else{
									hpRate  =0;
									latecalc  =0;
									}
									if(vy['nd']!=null){
										ndRatexBCT =   vy['nd']; 
 									 	HPval = ndRatexBCT;
									 }else{
										 ndRatexBCT = 0.00;
										 HPval =0.00;
									 }
 										//HPval = parseFloat(ndRate).toFixed(2);
									 
									// totalTimee =   (timeTotalVal >0) ? timeTotalVal - vy['latemins'] : "0.0";
										totalTimee =   timeTotalVal;

									totalTimee2 =  (timeTotalVal >0) ? timeTotalVal - vy['latemins']  : "0.0";
									
								 
									utTotal  += (totalTimee<vy['shiftTotalHours'] && totalTimee!=0) ? vy['shiftTotalHours'] - totalTimee : 0;
									 
									 timeTotal +=parseFloat(totalTimee);
	
										////console.log(vy['date']+" = ##"+(parseFloat(vy['resLogNEW'])-parseFloat(breaks))+" ##"+vy['shiftTotalHours']+"- "+totalTimee+"  =  "+utTotal);
								 
									 isHoliday +=parseFloat(vy['isHoliday']);
									 
 									
									if(vy['dayType']>0){
										dayType += vy['dayType'] ;
										dailySalary2  += 0;
									}else{
										dayType += 0;
										dailySalary2 += parseFloat(daily);
									}
									 
										// calchol = parseFloat(vy['calcHoliday1'] + vy['calcHoliday2']);
									if(vy['HolidayType1'] !='null'){
										timeTotalOTBCT +=parseFloat((timeTotalBCT)*vy['calcHoliday1'])+parseFloat(timeTotalBCT);
										NDvalBCT2 +=parseFloat((NDvalBCT)*vy['calcHoliday1'])+parseFloat(NDvalBCT);
										//alert("100 = "+parseFloat((timeTotalBCT)*1)+parseFloat(timeTotalBCT));
									}else{
										timeTotalOTBCT+=parseFloat(timeTotalBCT);
										NDvalBCT2+=parseFloat(NDvalBCT);
									}
									shift_type = (vy['shift_type']=="WORD") ? 0.5 : 0.0;

									  ndRatexBCT2+=parseFloat(ndRatexBCT)+parseFloat(NDvalBCT2)+parseFloat(vy['NDComputeHolidayLogin'])+parseFloat(vy['NDComputeHolidayLogout'])- parseFloat(vy['latecalc']); // Night Diff + ( [Special/Regular] Holiday ND) + WORD...
 									HPval2 +=parseFloat(HPval)+parseFloat(vy['HPComputeHolidayLogin'])+parseFloat(vy['HPComputeHolidayLogout']) + parseFloat(NDvalBCT2) - parseFloat(vy['latecalc']); // Hazard Pay + ( [Special/Regular] Holiday HP)
									  ////console.log(vy['date']+" = "+NDvalBCT);
 									SSS = parseFloat(vy['SSS']/2).toFixed(2);
									// PhilHealthCompute = parseFloat(vy['PhilHealthCompute']/2).toFixed(2); // OLD WAY
									if(Mrate<=10000){
										PhilHealthCompute =  (137.50/2);
									}else if(Mrate>=10000.01 && Mrate<=39999.99){
										PhilHealthCompute =  ((Mrate*0.0275)/4).toFixed(2);
									}else if(Mrate>=40000){
										PhilHealthCompute =  (550/2);
									}else{
										PhilHealthCompute =  0;
									}		
									PagIbig = parseFloat((vy['rate']*.02)/2).toFixed(2);
									pos_name =vy['pos_name'];
									// hpRate  =  (vy['hpRate']!="0.0") ? vy['hpRate'];
									 

									account  =vy['account'];
									 // //console.log(hpRate);
									fname =vy['fname'];
									sss_id =vy['sss_id'];
									philhealth_id =vy['philhealth_id'];
									tax_id =vy['tax_id'];
									emp_promoteId =vy['emp_promoteId'];
									BIRCompute =vy['BIRCompute'];
									percent =vy['BIRPercent'];
									salarybase =vy['BIRSalaryBase'];
									TotalAmountHoliday +=parseFloat(vy['TotalAmountHoliday']);
									BIRDescription = (Mrate>=21000) ?  vy['BIRDescription'] : "---";
									payroll_adjDeduct_amount =parseFloat(vy['payroll_adjDeduct_amount']/2);
									pemp_adjust_id =vy['pemp_adjust_id'];
									adjustSalary = (vy['adjustSalary']!=null) ? vy['adjustSalary'].split(","): 0;
									deductSalary = (vy['deductSalary']!=null) ? vy['deductSalary'].split(","): 0;
									empPosAllowance = (vy['empPosAllowance']!=0) ? vy['empPosAllowance'].split(","): 0;
									empPosBonus = (vy['empPosBonus']!=0) ? vy['empPosBonus'].split(",") : 0;
										NDComputeHolidayLogin += parseFloat(vy['NDComputeHolidayLogin'])*parseFloat(hourly*.15);
										NDComputeHolidayLogout += parseFloat(vy['NDComputeHolidayLogout'])*parseFloat(hourly*.15);
										HPComputeHolidayLogout += parseFloat(vy['HPComputeHolidayLogout']);
										HPComputeHolidayLogin += parseFloat(vy['HPComputeHolidayLogin']);
 
										rate2 = vy['rate2'];
						TotalHazardPayFinal+=parseFloat(((HPval2 * (hourly*hpRate))));
  						
								TotalNighDiffFinal+= ((ndRatexBCT2>0) ?((ndRatexBCT2*parseFloat(hourly*.15).toFixed(2))) : 0);
								////console.log(vy['date']+"----"+((ndRatexBCT2*parseFloat(hourly*.15).toFixed(2))));
							// //console.log((ndRatexBCT2+"*"+parseFloat(hourly*.15).toFixed(2)));
									console.log(vy['date']+" = "+NDvalBCT );
								var aaa = ((ndRatexBCT2*(hourly*.15))) -  parseFloat(latecalc * parseFloat(hourly*hpRate));
 								 HPval3 =HPval2;
								 HPval2 =0;
								 ndRatexBCT3=ndRatexBCT2;
								 ndRatexBCT2 =0;
								 latecalc =0;
								 breakConsumed =0;
								 NDvalBCT2 =0;
								});
								BIRTotalCompute = parseFloat(((rate2-salarybase)*percent)) + parseFloat(BIRCompute);
								
								// var TotalBCTxHourly = timeTotalOTBCT.toFixed(2) * hourly.toFixed(2);
								var TotalBCTxHourly = 0;
 									var i;
									var ii;
									var iii=0
									var dd;
									var dd2;
									var dd3=0;
									var x;
									var xx;
									var xxx=0;
									var y;
									var yy;
									var yyy=0;
									var jjj=0;
									var HPHoliday=0;
									var NDHoliday=0;
									var BIRTotalCompute2=0;
									 HPHoliday=(parseFloat(HPComputeHolidayLogin+HPComputeHolidayLogout)*parseFloat(hourly*hpRate));
									 NDHoliday=NDComputeHolidayLogin+NDComputeHolidayLogout;
									var finalTotalGross=0;
									var TotalHazardPayFinal2;
 									  TotalHazardPayFinal2 = TotalHazardPayFinal;
									  TotalHazardPayFinal =0;
 									 var TotalNighDiffFinal2;
									 TotalNighDiffFinal2 = TotalNighDiffFinal;
									 TotalNighDiffFinal=0;
 										BIRTotalCompute2 = (BIRTotalCompute>0) ? ((Mrate>=21000) ? BIRTotalCompute : 0 ):0 ;
 									 TotalGross =  parseFloat(TotalBCTxHourly)+parseFloat(TotalNighDiffFinal2)+parseFloat(TotalHazardPayFinal2);
										//console.log(parseFloat(TotalBCTxHourly)+"***"+parseFloat(TotalNighDiffFinal2)+"***"+parseFloat(TotalHazardPayFinal2));
									td+="<tr><td>Basic (Quinsina) </td><td><b> P "+dailySalary2+"</b></td></tr>";
									if(empPosAllowance!=0){
										$.each(empPosAllowance,function(susi,balue){
											xx = balue.split("=");	
										td+="<tr><td>"+xx[0]+"</td><td> <b>P"+ parseFloat((xx[1] - ((xx[1]/10)*dayType))).toFixed(2)+"</b></td></tr>";
										xxx += parseFloat((xx[1] - ((xx[1]/10)*dayType)));
 										});
										}else{
										// td+="<td class='long'> <b>--</b></td><td> <b>--</b></td><td> <b>--</b></td>";
											xxx = 0;
 										}
										if(empPosBonus!=0){
										$.each(empPosBonus,function(susi,balue){
											
											yy = balue.split("=");	
										// td+="<td> <b>P"+ parseFloat((yy[1])).toFixed(2)+"</b></td>";
											td+="<tr><td>"+yy[0]+"</td><td> <b>P"+parseFloat(yy[1]).toFixed(2)+"</b></td></tr>";
											yyy +=yy[1];
 										});
										}else{
										// td+="<td> <b>--</b></td><td> <b>--</b></td><td> <b>--</b></td>";
										td+="<tr><td>Bonus</td><td> <b>N/A</b> </td></tr>";
 										}
									 
									td +="<tr><td>Night Differential</td><td><b> P"+TotalNighDiffFinal2.toFixed(2)+"</b></td></tr>"+
									"<tr><td>Hazard Pay</td><td><b> P"+ TotalHazardPayFinal2.toFixed(2)+"</b></td></tr>"+
									"<tr><td>Holiday</td><td><b> P"+ TotalAmountHoliday.toFixed(2)+"</b></td></tr>";
									if(adjustSalary!=0){
										$.each(adjustSalary,function(yek,lav){
											
											ii = lav.split("=");	
											td+="<tr><td>"+ii[0]+"</td><td> <b>P"+ii[1]+"</b></td></tr>";
  										  iii += parseFloat(ii[1]);
 										});
										}else{
											iii = 0;
 										}
										
									finalTotalGross = (parseFloat(TotalAmountHoliday)+parseFloat(TotalGross)+parseFloat(xxx)+parseFloat(yyy)+parseFloat(iii)+dailySalary2);
										/////console.log((parseFloat(TotalAmountHoliday)+"***"+parseFloat(TotalGross)+"***"+parseFloat(xxx)+"***"+parseFloat(iii)+"***"+parseFloat(yyy)+"***"+parseFloat(Mrate/2)))

 									 
  									td+="<tr style='background: #f9f2b3;font-weight: 900;'><td>Total </td><td> P "+finalTotalGross.toFixed(2)+"</td></tr>";
 
 									yyy =0;
 									isHoliday =0;
									timeTotal =0;
									utTotal =0;
									timeTotalOTBCT =0;
									ndRatexBCT2 =0;									
									ndRatexBCT3 =0;									
									HPval2 =0;	
									HPval3 =0;	
									rate2= 0;	
									hpRate =0;
									latecalc =0;
									 HPComputeHolidayLogin=0;
									NDComputeHolidayLogin=0;
									NDComputeHolidayLogout=0;
									HPComputeHolidayLogout=0;
									TotalAmountHoliday=0;
									BIRTotalCompute2=0;	
									dayType=0;	
									ddaytypp=0;	
									iii=0;									
									xxx=0;									
 							  });
						});
					}else{
						td+=" ";	
					}
						 $("#detailedd").html(td);
						 $("#empsIs").html(+emp_idd);
						 $("#empsName").html("Gross Salary Details of "+fname);
						 $("#footerEmp").fadeOut();

						
                    }
                }); 				
				}
          </script>
 </html>