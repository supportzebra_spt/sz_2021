<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:popover-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  .some_class{
    width: 80px;
  }
</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

<script type="text/javascript">
  $(window).load(function(){
   setTimeout(function() {
    $('#loading').fadeOut( 400, "linear" );
  }, 300);
 });
</script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd'
    });
  });

</script>

<!-- Bootstrap Summernote WYSIWYG editor -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/popover/popover.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/xeditable/xeditable.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable-demo.js"></script> -->


<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>

</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapse" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div class="container">
          <script type="text/javascript">

            /* Datatables export */

            $(document).ready(function() {
              var table = $('#datatable-tabletools').DataTable();
            } );

            $(document).ready(function() {
              $('.dataTables_filter input').attr("placeholder", "Search...");
            });

          </script>
          <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>

          <script type="text/javascript">
            /* Input masks */

            $(function() { "use strict";
              $(".input-mask").inputmask();
            });

          </script>
          <!-- jQueryUI Tabs -->

          <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/tabs-ui/tabs.css">-->
          <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs-ui/tabs.js"></script>
          <script type="text/javascript">
            /* jQuery UI Tabs */

            $(function() { "use strict";
              $(".tabs").tabs();
            });

            $(function() { "use strict";
              $(".tabs-hover").tabs({
                event: "mouseover"
              });
            });
          </script>

          <!-- Chosen -->

          <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/chosen/chosen.css">-->
          <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
          <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
          <div id="page-title">
            <div class="row">
              <div class="col-md-12">
                <div class="content-box tabs">
                  <h3 class="content-box-header bg-blue">
                    <span>Leave Credits</span>
                    <ul>
                      <li>
                        <a href="#tabs-example-1" title="Single" id='single'>
                          Single
                        </a>
                      </li>
                      <li>
                        <a href="#tabs-example-2" title="Multiple" id='multiple'>
                          Multiple
                        </a>
                      </li>
                    </ul>
                  </h3>
                  <div id="tabs-example-1">
                    <div class="pad15A">
                      <h3>SET LEAVE CREDITS SINGLE TRANSACTION</h3><hr>
                      <div class="row">
                        <div class="col-md-4 form-group">
                          <label class="control-label">Type Of Leave</label>
                          <select class="form-control" id='sleavetype'>
                            <option value=''>--</option>
                            <?php foreach($leavelistWOVS as $ll):?>
                              <option value='<?php echo $ll->leave_id;?>'><?php echo $ll->leave_name;?></option>
                            <?php endforeach;?>
                          </select>
                        </div>

                        <div class="col-md-4 form-group">
                          <label class="control-label">Position Class</label>
                          <select class="form-control" id='sclass'>
                            <option value='Both'>Both</option>
                            <option value='Admin'>Admin</option>
                            <option value='Agent'>Agent</option>
                          </select>
                        </div>

                        <div class="col-md-4 form-group">
                          <label class="control-label">Position Status</label>
                          <select class="form-control" id='sstatus'>
                            <option value='Both'>Both</option>
                            <option value='Probationary'>Probationary</option>
                            <option value='Regular'>Regular</option>
                          </select>
                        </div>


                        <div class="col-md-4 form-group col-md-offset-2">
                          <label class="control-label">Maximum Credits</label>
                          <input type="number" min='0' class='form-control' id='scredits'>
                        </div>

                        <div class="col-md-3 form-group text-center"><br>
                          <button class="btn btn-info" id='ssavecredits'>Save Credits</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="tabs-example-2">
                    <div class="pad15A" id="demo1">
                      <h3>SET LEAVE CREDITS MULTIPLE TRANSACTION</h3><hr>
                      <div class="row">
                        <div class="col-md-6 form-group">
                          <label class="control-label">Type Of Leave</label>
                          <span class='pull-right'>
                            <button class="btn btn-xs btn-info" id='select'>Select all</button>
                            <button class="btn btn-xs btn-info" id='deselect'>Deselect all</button>
                          </span>
                          <select class="chosen-select" id='mleavetype' multiple data-placeholder="Choose..">
                            <?php foreach($leavelist as $ll):?>
                              <option value='<?php echo $ll->leave_id;?>'><?php echo $ll->leave_name;?></option>
                            <?php endforeach;?>
                          </select>
                        </div>

                        <div class="col-md-3 form-group">
                          <label class="control-label">Position Class</label>
                          <select class="form-control" id='mclass'>
                            <option value='Both'>Both</option>
                            <option value='Admin'>Admin</option>
                            <option value='Agent'>Agent</option>
                          </select>
                        </div>

                        <div class="col-md-3 form-group">
                          <label class="control-label">Position Status</label>
                          <select class="form-control" id='mstatus'>
                            <option value='Both'>Both</option>
                            <option value='Probationary'>Probationary</option>
                            <option value='Regular'>Regular</option>
                          </select>
                        </div>
                        <div class="col-md-12 form-group text-center"><br>
                          <!-- data-toggle="collapse" data-target="#demo2" -->
                          <button type="button" class="btn btn-info"  id='mgenerate'>
                            Generate
                          </button>
                        </div>
                      </div>
                    </div>
                    <div id="demo2" style='display:none'>
                     <!-- data-toggle="collapse" data-target="#demo1" -->
                     <button type="button" class="btn btn-info btn-link" id='viewabove'>
                      <i class="glyph-icon icon-arrow-up"></i> View Above
                    </button>
                    <div class="panel">
                      <div class="panel-heading">
                        <h3 class="panel-title">SET LEAVE CREDITS</h3>
                      </div>
                      <div class="panel-body">
                        <table  class="table table-striped table-bordered table-condensed table-hover font-size-13" border=1 cellspacing="0" width="100%">
                          <thead id='mthead'>

                          </thead>
                          <tbody id='mtbody'>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>

        <div class="row" hidden>
          <div class="col-md-8">
            <div class="panel">
              <div class="panel-body">
                <h3 class="title-hero">
                  Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                  <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
              </div>
            </div>




            <div class="content-box">
              <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                  <a href="#" class="icon-separator">
                    <i class="glyph-icon icon-question"></i>
                  </a>
                  <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                    <i class="glyph-icon icon-refresh"></i>
                  </a>
                  <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                    <i class="glyph-icon icon-times"></i>
                  </a>
                </span>
              </h3>
              <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
              </div>
            </div>

          </div>

        </div>
      </div>



    </div>
  </div>
</div>


<p class="growl1" hidden></p>
<p class="growl2" hidden></p>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>

<script>
  $(function(){
    $(".growl1").select(function(){
      $.jGrowl(
        "Successfully Updated.",
        {sticky:!1,position:"top-right",theme:"bg-blue-alt"}
        )
    });
    $(".growl2").select(function(){
      $.jGrowl(
        "Error Updating.",
        {sticky:!1,position:"top-right",theme:"bg-red"}
        )
    });

  })

</script>
<script type="text/javascript">
  $("#mgenerate").click(function(){   
    var leavetype = $("#mleavetype").val();
    var mclass = $("#mclass").val();
    var status = $("#mstatus").val();
    if(leavetype==null){     
      $.jGrowl("Choose A Leave Type. REQUIRED FIELD.",{sticky:!1,position:"top-right",theme:"bg-red"});
    }else{   

      $("#demo1").hide();
      $('#demo2').slideToggle('slow');
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/Request/getLeaveHeader",
        data: {
          leavetype: leavetype
        },
        cache: false,
        success: function(ress)
        {
          ress = ress.trim();
          var ressult = JSON.parse(ress);
          $("#mthead").html("");          
          var tbody = "";
          tbody += "<tr>";
          tbody += "<th>Position</th>"; 
          tbody += "<th>Status</th>"; 
          tbody += "<th>Class</th>"; 
          $.each(ressult,function(key1,val1){
            tbody += "<th>"+val1.leave_name+"</th>"; 
          });          
          tbody += "</tr>";
          $("#mthead").append(tbody);
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Request/getLeaveCredits",
            data: {
              leavetype: leavetype,
              class: mclass,
              status: status
            },
            cache: false,
            success: function(res)
            {
              res = res.trim();
              var result = JSON.parse(res);
              $("#mtbody").html("");
              var leavelist = ressult;
              console.log(leavelist.length);
              $.each(result,function(key,val){
                var tbody = "";
                tbody += "<tr>";
                tbody += "<td>"+val.pos_details+"</td>";
                tbody += "<td>"+val.status+"</td>";
                tbody += "<td>"+val.class+"</td>";
                for(var x=0;x<leavelist.length;x++){
                  if(val.leaveCredits==null){
                    tbody += "<td></td>";
                  }else{
                    var yes = false;
                    $.each(val.leaveCredits,function(i,item){
                // console.log(item);
                // alert(leavelist[x].leave_id+" "+item.leave_id);
                if(leavelist[x].leave_id==item.leave_id){
                  tbody += "<td><a href='' data-posempstat_id='"+val.posempstat_id+"' data-leave_id='"+item.leave_id+"' class='xedit'>"+item.credit+"</a></td>";
                  yes = true;
                } 
              });
                    if(yes==false){
                     tbody += "<td><a href='' data-posempstat_id='"+val.posempstat_id+"' data-leave_id='"+leavelist[x].leave_id+"' class='xedit'></a></td>";
                   }
                 }
            // alert('Nicca');
          }
          tbody += "</tr>";
          $("#mtbody").append(tbody);
          
        });
              $(".xedit").editable({
                type:"text",
                pk:1,
                name:"xedit",
                placeholder:"Input Credits",
                // mode:"popover",
                tpl:"<input type='text' style='width: 110px'>",
                validate: function(value) {
                  if ($.isNumeric(value) == '') {
                    return 'Only numbers are allowed';
                  }
                }
              })
              .on('save',function(e,params){
                var value = params.newValue;
                if(value==''||value==null){
                  $(".growl2").trigger('select');
                }else{
                  $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/Request/IUleavecredit",
                    data: {
                      posempstat_id: $(this).data('posempstat_id'),
                      leave_id: $(this).data('leave_id'),
                      value: value
                    },
                    cache: false,
                    success: function(res)
                    {
                     res = res.trim();
                     if(res=='Success'){
                       $(".growl1").trigger('select');
                     }else{
                       $(".growl2").trigger('select');
                     }

                   }
                 });
                }
              })
            }
          });
        }
      });
    }


  });
</script>
<script type="text/javascript">
  $("#viewabove").click(function(){
    $("#demo2").hide();
    $('#demo1').slideToggle('slow');
  });
</script>
<script type="text/javascript">
  $('#select').click(function(){
    $('#mleavetype option').prop('selected', true); 
    $('#mleavetype').trigger('chosen:updated');
  })

</script>
<script type="text/javascript">
 $('#deselect').click(function(){
  $('#mleavetype option:selected').removeAttr('selected');
  $('#mleavetype').trigger('chosen:updated');
});
</script>
<script type="text/javascript">
  $("#ssavecredits").click(function(){
    var leavetype = $("#sleavetype").val();
    var sclass = $("#sclass").val();
    var status = $("#sstatus").val();
    var credits = $("#scredits").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>index.php/Request/IUSingleTrans",
      data: {
        leavetype: leavetype,
        class: sclass,
        status: status,
        credits:credits
      },
      cache: false,
      success: function(res)
      {
        $(".growl1").trigger('select');
      }
    });
  });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>