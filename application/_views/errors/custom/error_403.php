<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="utf-8" />
    <title>Error 403|Forbidden Access</title>
    <meta name="description" content="Initialized via remote ajax json data">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: { "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"] },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/img/favicon.ico" />


    <script src="<?php echo base_url(); ?>assets/js/app.js" type="text/javascript"></script>
</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid  m-error-1" style="background-image: url(<?php echo base_url(); ?>assets/src/app/media/img/error/bg1.jpg);">
        <div class="m-error_container">
            <span class="m-error_number">
                <h1>
                    <span>
                        <!-- <img src="<?php echo base_url(); ?>assets/images/img/demotivated.gif" alt=""> -->
                        <i class="fa fa-lock text-danger" style="font-size: 239px;"></i>
                    </span> 
                    <span>403</span>
                    <span style="font-size: 53px;color: #444f54;"> - Forbidden</span>
                </h1>
            </span>
            <p class="m-error_desc" style="color: #3c3c3c;">
                <span style="color: #b31515;font-size: 27px;">OOPS!</span> You do not have permission to access this page.
            </p>
            <p class="m-error_desc" style="color: #3c3c3c;">
                Please inform your Supervisor or the System Administrator.
            </p>
            <p class="mt-5 m-error_desc">
                <a class="btn btn-outline-metal m-btn m-btn--icon text-dark" href="javascript:history.go(-1)" style="text-decoration: none;">
                    <span class="text-dark">
                        <i class="fa fa-chevron-left pr-1"></i>
                        BACK TO PREVIOUS PAGE
                    </span>
                    
                </a>
            </p>
        </div>
    </div>
</div>
</body>
</html>