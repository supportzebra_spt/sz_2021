<!DOCTYPE html> 
<html  lang="en">

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
        </style>


        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">
        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-tabletools.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-reorder.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });
        </script>

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/datepicker/datepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
                /* Datepicker bootstrap */

                $(function () {
                    "use strict";
                    $('.bootstrap-datepicker').bsdatepicker({
                        format: 'mm-dd'
                    });
                });
        </script>
        <style type="text/css">
            #datatable-tabletools td{
                color:#444 !Important;
            }
            #datatable-tabletools thead th{
                background:#333 !Important;
                color:white !important;
            }
            .pagination>.active>a,.pagination>.active>a{
                background-color:#333 !Important;
                border-color:#333 !important;
            }
            body { 
                padding-right: 0 !important
            }
            .btn-delete{
                line-height: 28px !important;
                height: 30px !important;
                min-width: 30px !important;
            }
        </style>
    </head>


    <body>
        <div id="sb-site">


            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <div id="page-header" class="bg-black">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>
                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">
                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">
                        <div class="container">
                            <div id="page-title">
                                <div class="container">
                                    <a href="../Payroll/" class="btn bg-google" >
                                        <span class="glyph-icon icon-separator">
                                            <i class="glyph-icon icon-mail-reply"></i>
                                        </span>
                                        <span class="button-content">
                                            Back
                                        </span>
                                    </a>
                                    <a href="#" class="btn bg-twitter" data-toggle="modal" data-target=".bs-example-modal-sm">
                                        <span class="glyph-icon icon-separator">
                                            <i class="glyph-icon icon-plus"></i>
                                        </span>
                                        <span class="button-content">
                                            New Holiday
                                        </span>
                                    </a>
                                    <a href="#" class="btn btn-blue-alt" onclick="location.reload()" >
                                        <i class="glyph-icon icon-refresh"></i>
                                    </a>
                                </div>
                                <br>
                                <div class="panel">
                                    <div class="panel-heading" style="font-size:16px;font-weight:bold;background:#333;color:white">HOLIDAY LIST</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table id="datatable-tabletools"  class="table table-striped table-bordered table-condensed" border=1 cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th class='text-center'>Holiday</th>
                                                            <th class='text-center'>Description</th>
                                                            <th class='text-center'>Type</th>
                                                            <th class='text-center'>Site</th>
                                                            <th class='text-center'>Date</th>
                                                            <th class='text-center'>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($values as $v): ?>
                                                            <tr>
                                                                <td><?php echo $v->holiday; ?></td>
                                                                <td><?php echo $v->description; ?></td>
                                                                <td><?php echo $v->type; ?></td>
                                                                <td><?php echo $v->sites_str; ?></td>
                                                                <td><?php echo $v->date2; ?></td>
                                                                <td><button class="btn btn-round btn-danger btn-delete" data-holidayid="<?php echo $v->holiday_id; ?>"><i class="glyph-icon icon-trash"></i></button></td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #333; color: white;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">New Holiday</h3>
                        </div>
                        <div class="modal-body" style="background:#eee">
                            <div class="panel" style="margin-bottom: 0 !important">
                                <div class="panel-body">
                                    <div class="row" id='forms'>
                                        <div class="col-md-6">
                                            <label class='control-label'>Holiday 
                                                <span class="text-danger" id='h' style='font-size:8px;' hidden>* (Required)</span>
                                            </label>
                                            <input type="text" class="form-control" id='holiday'>
                                        </div>
                                        <div class="col-md-3">
                                            <label class='control-label'>Type 
                                                <span class="text-danger" id='t' style='font-size:8px;' hidden>* (Required)</span>
                                            </label>
                                            <select id='type' class='form-control'>
                                                <option value=''>--</option>
                                                <option value='Regular'>Regular</option>
                                                <option value='Special'>Special</option>
                                            </select>
                                        </div> 
                                        <div class="bootstrap-timepicker dropdown col-md-3">
                                            <label class='control-label'>Date 
                                                <span class="text-danger" id='d2' style='font-size:8px;' hidden>* (Required)</span>
                                            </label>
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span>
                                                <input type="text" class="bootstrap-datepicker form-control" id="date" readonly="">
                                            </div>
                                        </div>
                                    </div> 
                                    <br />
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class='control-label'>Sites</label>
                                            <?php foreach ($sites as $s): ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="custom-checkbox" value="<?php echo $s->site_ID; ?>"> <?php echo $s->code; ?>
                                                    </label>
                                                </div>

                                            <?php endforeach; ?>

                                        </div>
                                        <div class="col-md-9">
                                            <label class='control-label'>Description 
                                                <span class="text-danger" id='d1' style='font-size:8px;' hidden>* (Required)</span>
                                            </label>
                                            <textarea class='form-control' id='description'></textarea>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="modal-footer" id='footer'>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <a href="#" class="btn bg-twitter" id='addnewholiday'>
                                <span class="glyph-icon icon-separator">
                                    <i class="glyph-icon icon-check"></i>
                                </span>
                                <span class="button-content">
                                    Add Holiday
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- JS Demo -->
            <script type="text/javascript" src="../../assets/widgets/jgrowl-notifications/jgrowl.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
        </div>
    </body>
    <script>
                                            $(function () {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
                                                    cache: false,
                                                    success: function (html) {

                                                        $('#headerLeft').html(html);
                                                        //alert(html);
                                                    }
                                                });
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
                                                    cache: false,
                                                    success: function (html) {

                                                        $('#sidebar-menu').html(html);
                                                        // alert(html);
                                                    }
                                                });
                                                var table = $('#datatable-tabletools').DataTable();
                                                $('.dataTables_filter input').attr("placeholder", "Search...");
                                                $("#datatable-tabletools").on("click", ".btn-delete", function () {
                                                    var that = this;
                                                    var holiday_id = $(that).data('holidayid');
                                                    if (confirm("Are you sure?")) {
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo base_url(); ?>/index.php/Payroll/deleteHoliday",
                                                            data: {
                                                                holiday_id: holiday_id
                                                            },
                                                            cache: false,
                                                            success: function (res) {
                                                                res = JSON.parse(res.trim());
                                                                if (res.status === 'Success') {
                                                                    $.jGrowl("Successfully deleted a holiday record.", {
                                                                        sticky: !1,
                                                                        position: "top-right",
                                                                        theme: "bg-green"
                                                                    });
                                                                    table.row($(that).closest('tr')).remove().draw();
                                                                } else {
                                                                    $.jGrowl("An error occured while processing your action.", {
                                                                        sticky: !1,
                                                                        position: "top-right",
                                                                        theme: "bg-red"
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            });
    </script>
    <script type="text/javascript">
            $("#addnewholiday").click(function () {
                var holiday = $('#holiday').val();
                var description = $('#description').val();
                var date = $('#date').val();
                var type = $('#type').val();
                if (holiday === '') {
                    $('#h').show();
                } else {
                    $("#h").hide();
                }
                if (description === '') {
                    $('#d1').show();
                } else {
                    $("#d1").hide();
                }
                if (date === '') {
                    $('#d2').show();
                } else {
                    $("#d2").hide();
                }
                if (type === '') {
                    $('#t').show();
                } else {
                    $("#t").hide();
                }

                if (holiday !== '' && description !== '' && date !== '' && type !== '') {
                    var sites = [];
                    $('[name=custom-checkbox]:checked').each(function () {
                        sites.push($(this).val());
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Payroll/saveholiday",
                        data: {
                            holiday: holiday,
                            description: description,
                            date: date,
                            type: type,
                            sites: sites.join()
                        },
                        cache: false,
                        success: function (res) {
                            $('.bs-example-modal-sm').modal('hide');
                            if (res === 'Success') {
                                $.jGrowl("Successfully created a holiday record.", {
                                    sticky: !1,
                                    position: "top-right",
                                    theme: "bg-green"
                                });
                            } else {
                                $.jGrowl("An error occured while processing your action.", {
                                    sticky: !1,
                                    position: "top-right",
                                    theme: "bg-red"
                                });
                            }
                        }
                    });
                }
            });
    </script>

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>