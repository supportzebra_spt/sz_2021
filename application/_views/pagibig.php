<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  </style>


  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });
 </script>

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 <script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd-yyyy'
    });
  });

</script>


</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div class="container">
          <!-- <script type="text/javascript" src="../../assets/widgets/datepicker/datepicker.js"></script> -->
          <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function() { "use strict";
              $('.bootstrap-datepicker').bsdatepicker({
                format: 'yyyy-mm-dd'
              });
            });

          </script>
          <script type="text/javascript">

            /* Datatables export */

            $(document).ready(function() {
              var table = $('#datatable-tabletools').DataTable();
            } );

            $(document).ready(function() {
              $('.dataTables_filter input').attr("placeholder", "Search...");
            });

          </script>
          <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>

          <script type="text/javascript">
            /* Input masks */

            $(function() { "use strict";
              $(".input-mask").inputmask();
            });

          </script>

          <div id="page-title">
		  	<a href="../Payroll/" class="btn btn-xs btn-success glyph-icon   icon-mail-reply" title="" style="margin-bottom: 9px;"> Back</a>
            <div class="row">
              <div class="col-md-2">
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm"><span class='icon-typicons-plus'></span> Pag-ibig</button>

                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header" style="background: #1f2e2e; color: white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title">New Pag-ibig Rate</h3>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-4">
                            <label class='control-label'>Employee Share</label>
                            <input type="text" class="input-mask form-control" data-inputmask="'mask':'9%'" id='employee'>
                            <p class="text-danger" id='ee' style='font-size:8px;' hidden>(Required)</p>
                          </div> 
                          <div class="col-md-4">
                            <label class='control-label'>Employer Share</label>
                            <input type="text" class="input-mask form-control" data-inputmask="'mask':'9%'" id='employer'>
                            <p class="text-danger" id='er' style='font-size:8px;' hidden>(Required)</p>
                          </div> 
                          
                          <div class="bootstrap-timepicker dropdown col-md-4">
                            <label class='control-label'>Effectivity Date</label>
                            <div class="input-prepend input-group">
                              <span class="add-on input-group-addon">
                                <i class="glyph-icon icon-calendar"></i>
                              </span>
                              <input type="text" class="bootstrap-datepicker form-control" id="date">
                            </div>
                            <p class="text-danger" id='d' style='font-size:8px;' hidden>(Required)</p>
                          </div>
                        </div> 
                      </div>    
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id='addnewpagibig'>Save</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div><br>
            <div class="row">
              <div class="col-md-12">
                <table id="datatable-tabletools"  class="table table-striped table-bordered table-condensed" border=1 cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th class='text-center' colspan='4' style='background: #787a7b;color: white;border-top-right-radius: 10px;border-top-left-radius: 10px;'>Percentage of Monthly Compensation</th>
                    </tr>
                    <tr>
                      <th class='text-center' style='background:#888;color:white'>Employee Share</th>
                      <th class='text-center' style='background:#888;color:white'>Employer Share</th>
                      <th class='text-center' style='background:#888;color:white'>Effectivity Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($values as $v):?>
                      <tr>
                        <td <?php if($v->isActive==1){echo "style='background:lightyellow'";}?>><?php echo $v->employeeshare;?> %</td>
                        <td <?php if($v->isActive==1){echo "style='background:lightyellow'";}?>><?php echo $v->employershare;?> %</td>
                        <td <?php if($v->isActive==1){echo "style='background:lightyellow'";}?>><?php echo $v->effectivity_date;?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>

          </div>

          <div class="row" hidden>
            <div class="col-md-8">
              <div class="panel">
                <div class="panel-body">
                  <h3 class="title-hero">
                    Recent sales activity
                  </h3>
                  <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                  </div>
                </div>
              </div>




              <div class="content-box">
                <h3 class="content-box-header bg-default">
                  <i class="glyph-icon icon-cog"></i>
                  Live server status
                  <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                      <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                      <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                      <i class="glyph-icon icon-times"></i>
                    </a>
                  </span>
                </h3>
                <div class="content-box-wrapper">
                  <div id="data-example-3" style="width: 100%; height: 250px;"></div>
                </div>
              </div>

            </div>

          </div>
        </div>



      </div>
    </div>
  </div>


  <!-- JS Demo -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $("#addnewpagibig").click(function(){
    var employer = $("#employer").val();
    var employee = $("#employee").val();
    var date = $("#date").val();
    if(employer==''){
      $("#er").show();
    }else{
      $("#er").hide();
    }
    if(employee==''){
      $("#ee").show();
    } else{
      $("#ee").hide();
    }
    if(date==''){
      $("#d").show();
    }else{
      $("#d").hide();
    }
    if(date != ''&&employer != ''&&employee !=''){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/Payroll/savepagibig",
        data: {
          employer: employer,
          employee: employee,
          date: date
        },
        cache: false,
        success: function(res)
        {
          $('.bs-example-modal-sm').modal('hide');
          if($res=='Success'){
            swal("Success", "Successfully Created New Rate", "success");
          }else{
            swal("Oops!", "An Error Occured while processing data.", "error");
          }
        }
      });
    }

  });
</script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>