<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  th{
    background:#666 !important;
    color:white !important;
    padding-top:10px;
    padding-bottom:10px;
}
.table-striped>tbody>tr:nth-child(odd)>td, 
.table-striped>tbody>tr:nth-child(odd)>th {
    background-color: #eee;
    color:#555;
    font-size:13px;
}
.table-striped>tbody>tr:nth-child(even)>td, 
.table-striped>tbody>tr:nth-child(even)>th {
    background-color: white;
    color:#777;
    font-size:13px;
}
body { padding-right: 0 !important }
</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> KUDOS | Add record </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">


<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript">
    $(window).load(function(){
       setTimeout(function() {
          $('#loading').fadeOut( 400, "linear" );
      }, 300);
   });

</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

<script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true,
            "order": [[ 6, "desc" ]]
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lightbox/css/lightbox.min.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/lightbox/js/lightbox.min.js"></script>
</head>

<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<div id="page-wrapper">
  <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
      <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
      <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
  </div>
  <div id="header-logo" class="logo-bg">
   <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
   <a id="close-sidebar" href="#" title="Close sidebar">
      <i class="glyph-icon icon-angle-left"></i>
  </a>
</div>
<div id='headerLeft'>
</div>

</div>
<div id="page-sidebar">
  <div class="scroll-sidebar">


    <ul id="sidebar-menu">
    </ul>
</div>
</div>
<div id="page-content-wrapper">
  <div id="page-content">

    <div id="page-title">
        <h2>CREATE KUDOS</h2>
        <hr>
        <div class="content-box">
            <h3 class="content-box-header bg-black">
                <i class="glyph-icon icon-gift"></i>
                Create A Kudos &nbsp;&nbsp;
                <span class="bs-badge badge-primary"><?php echo count($kudos);?></span>
                <div class="header-buttons-separator">
                    <a href="<?php echo base_url('index.php/Kudos/kudos_addmultiple');?>" class="icon-separator">
                        <i class="glyph-icon icon-tasks"></i>
                    </a>
                    <a href="#" id='single' class="icon-separator" data-toggle="modal" data-target="#myModal">
                        <i class="glyph-icon icon-plus"></i>
                    </a>
                </div>
            </h3>
            <div class="content-box-wrapper">

               <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap table-condensed">
                <thead class='font-bold'>
                    <tr>
                        <th>Name</th>
                        <th>Campaign</th>
                        <th>Type</th>
                        <th>Proofreading</th>
                        <th>Card</th>
                        <th>Reward</th>
                        <th>Date Added</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot class='font-bold'>
                    <tr>
                        <th>Name</th>
                        <th>Campaign</th>
                        <th>Type</th>
                        <th>Proofreading</th>
                        <th>Card</th>
                        <th>Reward</th>
                        <th>Date Added</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody class=" font-size-14">
                    <?php foreach($kudos as $k => $val): ?>
                        <tr>
                            <td class='font-bold'><?php echo $val->ambassador; ?></td>
                            <td><?php echo $val->campaign; ?></td>
                            <td><?php echo $val->kudos_type; ?></td>
                            <td><?php if($val->proofreading=='Pending'){ ?>
                                <p style="color: red;" class='font-bold'><?php echo $val->proofreading; ?></p>
                                <?php }?>
                                <?php if($val->proofreading=='Done'){ ?>
                                <p style="color: green;" class='font-bold'><?php echo $val->proofreading; ?></p>
                                <?php }?>
                                <?php if($val->proofreading=='In Progress'){ ?>
                                <p style="color: #f29341;" class='font-bold'><?php echo $val->proofreading; ?></p>
                                <?php }?>
                            </td>
                            <td><?php if($val->kudos_card=='Pending'){ ?>
                                <p style="color: red;" class='font-bold'><?php echo $val->kudos_card; ?></p>
                                <?php }?>
                                <?php if($val->kudos_card=='Done'){ ?>
                                <p style="color: green;" class='font-bold'><?php echo $val->kudos_card; ?></p>
                                <?php }?>
                                <?php if($val->kudos_card=='In Progress'){ ?>
                                <p style="color: #f29341;" class='font-bold'><?php echo $val->kudos_card; ?></p>
                                <?php }?>
                            </td>
                            <td><?php if($val->reward_status=='Pending'){ ?>
                                <p style="color: red;" class='font-bold'><?php echo $val->reward_status; ?></p>
                                <?php }?>
                                <?php if($val->reward_status=='Requested'){ ?>
                                <p style="color: #f29341;" class='font-bold'><?php echo $val->reward_status; ?></p>
                                <?php }?>
                                <?php if($val->reward_status=='Released'){ ?>
                                <p style="color: green;" class='font-bold'><?php echo $val->reward_status; ?></p>
                                <?php }?>
                                <?php if($val->reward_status=='Approved'){ ?>
                                <p style="color: #41b2f4;" class='font-bold'><?php echo $val->reward_status; ?></p>
                                <?php }?>
                            </td>
                            <td><?php echo $val->date_given; ?></td>
                            <td style="text-align: center;">
                               <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModalUpdate" data-kudosid="<?php echo $val->kudos_id; ?>">
                                <span class="glyph-icon icon-separator">
                                    <i class="glyph-icon icon-pencil"></i>
                                </span>
                                <span class="button-content">
                                    Update
                                </span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

</div>
</div> 
</div>
</div>
</div>
</div>
<div class="modal fade" id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header pad20A" style='color:white;background:#333'>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title font-bold">
                    <i class="glyph-icon icon-pencil"></i> Update Kudos</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <label class="control-label">Accounts <small id='uaccountsn' class='font-red' style='display:none'>Required *</small></label>
                            <select id='uaccounts' class='form-control' data-empid=''>
                                <option value=''>--</option>
                                <?php foreach($accounts as $acc):?>
                                    <option value='<?php echo $acc->acc_id;?>'><?php echo $acc->acc_name;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-md-7">
                            <label class="control-label">Ambassador <small id='uemployeen' class='font-red' style='display:none'>Required *</small></label>
                            <select id='uemployee' class='form-control'>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Preferred Reward <small id='urewardn' class='font-red' style='display:none'>Required *</small></label>
                            <select class='form-control' id='ureward'>
                                <option value=''>--</option>
                                <option value='Cash'>Cash</option>
                                <option value='Credit'>Credit</option>
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Kudos Type <small id='ukudostypen' class='font-red' style='display:none'>Required *</small></label>
                            <select class='form-control' id='ukudostype'>
                                <option value=''>--</option>
                                <option value='Call'>Call</option>
                                <option value='Email'>Email</option>
                            </select>
                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Client's Name <small id='uclientnamen' class='font-red' style='display:none'>Required *</small></label>
                            <input type="text" class='form-control' id='uclientname'>
                        </div>

                        <div class="col-md-6" style='display:none' id='uclientemaildiv'>
                            <label class="control-label">Client's Email Address <small id='uclientemailn' class='font-red' style='display:none'>Required *</small></label>
                            <input type="text" class='form-control' id='uclientemail'>
                        </div>

                        <div class="col-md-6" style='display:none' id='uclientnumberdiv'>
                            <label class="control-label">Client's Phone Number <small id='uclientnumbern' class='font-red' style='display:none'>Required *</small></label>
                            <input type="text" class='form-control' id='uclientnumber'>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12" style='display:none' id='ucommentdiv'>
                            <label class="control-label">Comment <small id='ucommentn' class='font-red' style='display:none'>Required *</small></label>
                            <textarea class='form-control' id='ucomment' row='3'></textarea>
                        </div>

                        <div class="col-md-12" style='display:none' id='uscreenshotdiv'>
                            <label class="control-label">Screenshot <small id='uuploadpicn' class='font-red' style='display:none'>Required *</small></label>
                            <div class="row">
                                <div class="col-md-7">
                                    <input type='file' id="uuploadpic" class='form-control'/>
                                </div>

                                <div class="col-md-5">
                                    <img id="upreview" arique/>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style='color:white;background:#333'>
                    <a href="#" class="btn btn-default" data-dismiss="modal">
                        <span class="glyph-icon icon-separator">
                            <i class="glyph-icon icon-times"></i>
                        </span>
                        <span class="button-content">
                            Close
                        </span>
                    </a>
                    <a href="#" class="btn btn-info" id='update'  data-loading-text="Updating record...">
                        <span class="glyph-icon icon-separator">
                            <i class="glyph-icon icon-pencil"></i>
                        </span>
                        <span class="button-content">
                            Save Changes
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header pad20A" style='color:white;background:#333'>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title font-bold">
                        <i class="glyph-icon icon-plus"></i> Create A Kudos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="control-label">Accounts <small id='naccountsn' class='font-red' style='display:none'>Required *</small></label>
                                <select id='naccounts' class='form-control'>
                                    <option value=''>--</option>
                                    <?php foreach($accounts as $acc):?>
                                        <option value='<?php echo $acc->acc_id;?>'><?php echo $acc->acc_name;?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="col-md-7">
                                <label class="control-label">Ambassador <small id='nemployeen' class='font-red' style='display:none'>Required *</small></label>
                                <select id='nemployee' class='form-control'>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">Preferred Reward <small id='nrewardn' class='font-red' style='display:none'>Required *</small></label>
                                <select class='form-control' id='nreward'>
                                    <option value=''>--</option>
                                    <option value='Cash'>Cash</option>
                                    <option value='Credit'>Credit</option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label class="control-label">Kudos Type <small id='nkudostypen' class='font-red' style='display:none'>Required *</small></label>
                                <select class='form-control' id='nkudostype'>
                                    <option value=''>--</option>
                                    <option value='Call'>Call</option>
                                    <option value='Email'>Email</option>
                                </select>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">Client's Name <small id='nclientnamen' class='font-red' style='display:none'>Required *</small></label>
                                <input type="text" class='form-control' id='nclientname'>
                            </div>

                            <div class="col-md-6" style='display:none' id='nclientemaildiv'>
                                <label class="control-label">Client's Email Address <small id='nclientemailn' class='font-red' style='display:none'>Required *</small></label>
                                <input type="text" class='form-control' id='nclientemail'>
                            </div>

                            <div class="col-md-6" style='display:none' id='nclientnumberdiv'>
                                <label class="control-label">Client's Phone Number <small id='nclientnumbern' class='font-red' style='display:none'>Required *</small></label>
                                <input type="text" class='form-control' id='nclientnumber'>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12" style='display:none' id='ncommentdiv'>
                                <label class="control-label">Comment <small id='ncommentn' class='font-red' style='display:none'>Required *</small></label>
                                <textarea class='form-control' id='ncomment' row='3'></textarea>
                            </div>

                            <div class="col-md-12" style='display:none' id='nscreenshotdiv'>
                                <label class="control-label">Screenshot <small id='nuploadpicn' class='font-red' style='display:none'>Required *</small></label>
                                <div class="row">
                                    <div class="col-md-7">
                                        <input type='file' id="nuploadpic" class='form-control'/>
                                    </div>

                                    <div class="col-md-5">
                                        <img id="npreview" src="#" style='height:150px'  class='img-thumbnail  text-center'/>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style='color:white;background:#333'>
                        <a href="#" class="btn btn-default" data-dismiss="modal">
                            <span class="glyph-icon icon-separator">
                                <i class="glyph-icon icon-times"></i>
                            </span>
                            <span class="button-content">
                                Close
                            </span>
                        </a>
                        <a href="#" class="btn btn-info" id='create' data-loading-text="Saving record...">
                            <span class="glyph-icon icon-separator">
                                <i class="glyph-icon icon-plus"></i>
                            </span>
                            <span class="button-content">
                                Create
                            </span>
                        </a>

                    </div>
                </div>
            </div>
        </div>
        <!-- JS Demo -->
        <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>

    </body>
    <script>
      $(function(){



       /*  $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
          cache: false,
          success: function(html)
          {

            $('#headerLeft').html(html);
           //alert(html);
       }
   });   */ 
   $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
     }
 });
});   
</script>
<script type="text/javascript">
    $('#myModalUpdate').on('show.bs.modal', function(e) {
        var kudosid = $(e.relatedTarget).data('kudosid');
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/getKudos",
          data:{
            kudosid:kudosid
        },
        cache: false,
        success: function(res)
        {
            res = res.trim();
            res = JSON.parse(res);
            $("#uaccounts").data('empid',res.emp_id);
            $("#uaccounts").val(res.acc_id).change();
            $("#upreview").attr('src',"<?php echo base_url();?>"+res.file);
            $("#ureward").val(res.reward_type).change();
            $("#ukudostype").val(res.kudos_type).change();
            $("#uclientname").val(res.client_name);
            $("#uclientnumber").val(res.phone_number);
            $("#ucomment").val(res.comment);
            $("#uclientemail").val(res.client_email);
            $("#update").data('kudosid',kudosid);
            if(res.file==null||res.file==''){   
                $("#upreview").css({"height":"0px"}); 
                $("#upreview").removeClass("img-thumbnail text-center");
                $("#update").data('okay','0');
            }else{
                // alert(res.file);
                $("#upreview").css({"height":"150px"});
                $("#upreview").addClass("img-thumbnail text-center");
                $("#update").data('okay','1');
            }
        }
    });
    });
</script>
<script type="text/javascript">

    $('#uaccounts').on('change', function(){
        var emp_id = $(this).data('empid');
        var acc_id = $(this).val();  
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/getEmpInAccounts",
          data:{
            acc_id:acc_id
        },
        cache: false,
        success: function(res)
        {
            res = res.trim();
            res = JSON.parse(res);
            $('#uemployee').html('');
            $('#uemployee').append('<option value="">--</option>');
            $.each(res,function (i,elem){
                if(emp_id==''){
                   $('#uemployee').append('<option value="'+elem.emp_id+'">'+elem.lname+", "+elem.fname+'</option>');
               }else{
                   if(emp_id==elem.emp_id){
                       $('#uemployee').append('<option value="'+elem.emp_id+'" selected>'+elem.lname+", "+elem.fname+'</option>');
                   }else{
                    $('#uemployee').append('<option value="'+elem.emp_id+'">'+elem.lname+", "+elem.fname+'</option>');
                }
            }
        })
        }
    });
    });  
</script>
<script type="text/javascript">
    $('#ukudostype').on('change', function(){
        var kudostype = $(this).val();  
        $("#uclientnumberdiv").hide();
        $("#ucommentdiv").hide();
        $("#uclientemaildiv").hide();
        $("#uscreenshotdiv").hide();
        if(kudostype=='Call'){
            $("#uclientnumberdiv").show();
            $("#ucommentdiv").show();

            $("#uclientemail").val('');
            $("#uuploadpic").val('');
            $("#upreview").attr('src','');
        }else if(kudostype=='Email'){
            $("#uclientemaildiv").show();
            $("#uscreenshotdiv").show();
            $("#uclientnumber").val('');
            $("#ucomment").val('');
        }else{
         swal(
            'Kudos!',
            'Please select kudos type.',
            'warning'
            )
     }

 });  

</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#upreview').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#uuploadpic").change(function(){
        readURL(this);
        $("#upreview").show();
    });
</script>
<script type="text/javascript">

    $('#naccounts').on('change', function(){
        var acc_id = $(this).val();  
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/getEmpInAccounts",
          data:{
            acc_id:acc_id
        },
        cache: false,
        success: function(res)
        {
            res = res.trim();
            res = JSON.parse(res);
            $('#nemployee').html('');
            $('#nemployee').append('<option value="">--</option>');
            $.each(res,function (i,elem){
                $('#nemployee').append('<option value="'+elem.emp_id+'">'+elem.lname+", "+elem.fname+'</option>');
            })
        }
    });
    });  
</script>
<script type="text/javascript">
    $('#nkudostype').on('change', function(){
        var kudostype = $(this).val();  
        $("#nclientnumberdiv").hide();
        $("#ncommentdiv").hide();
        $("#nclientemaildiv").hide();
        $("#nscreenshotdiv").hide();
        $("#npreview").hide();
        if(kudostype=='Call'){
            $("#nclientnumberdiv").show();
            $("#ncommentdiv").show();

            $("#nclientemail").val('');
            $("#nuploadpic").val('');
            $("#npreview").attr('src','');
        }else if(kudostype=='Email'){
            $("#nclientemaildiv").show();
            $("#nscreenshotdiv").show();
            $("#nclientnumber").val('');
            $("#ncomment").val('');
        }else{
            swal(
                'Kudos!',
                'Please select kudos type.',
                'warning'
                )
        }

    });  

</script>
<script type="text/javascript">
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#npreview').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#nuploadpic").change(function(){
        readURL2(this);
        $("#npreview").show();
    });
</script>
<script type="text/javascript">
    $("#create").on('click', function() {
        var isOkay = true;     
        var accounts= $("#naccounts").val();
        var accounts_text= $("#naccounts").find('option:selected').text();
        var employee = $("#nemployee").val();
        var employee_text = $("#nemployee").find('option:selected').text();
        var reward = $("#nreward").val();
        var kudostype = $("#nkudostype").val();
        var clientname  = $("#nclientname").val();
        var clientemail = $("#nclientemail").val();
        var clientnumber = $("#nclientnumber").val();
        var comment  = $("#ncomment").val(); 
        var file = $("#nuploadpic").val();

        if(accounts==''){
            isOkay = false;
            $('#naccountsn').show();
        }else{

            $('#naccountsn').hide();
        }
     // alert(employee);
     if(employee==null||employee==''){
        isOkay = false;
        $('#nemployeen').show();
    }else{

        $('#nemployeen').hide();
    }
    if(reward==''){
        isOkay = false;
        $('#nrewardn').show();
    }else{

        $('#nrewardn').hide();
    }
    if(kudostype==''){
        isOkay = false;
        $('#nkudostypen').show();
    }else{
       $('#nkudostypen').hide();
       if(kudostype=='Email'){
        if(clientemail==''){
            isOkay = false;
            $('#nclientemailn').show();
        }else{

            $('#nclientemailn').hide();
        }
        if(file!=''){
            $('#nuploadpicn').hide();
        }else{
            isOkay = false;
            $('#nuploadpicn').show();
        }
    }else if(kudostype=='Call'){
        if(clientnumber==''){
            isOkay = false;
            $('#nclientnumbern').show();
        }else{

            $('#nclientnumbern').hide();
        }
        if(comment==''){
            isOkay = false;
            $('#ncommentn').show();
        }else{

            $('#ncommentn').hide();
        }
    }
}
if(clientname==''){
    isOkay = false;
    $('#nclientnamen').show();
}else{

    $('#nclientnamen').hide();
}




if(isOkay==true){
    $("#create").button('loading');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Kudos/createkudos",
      data:{
        accounts:accounts,
        employee:employee,
        reward:reward,
        kudostype:kudostype,
        clientname:clientname,
        clientemail:clientemail,
        clientnumber:clientnumber,
        comment:comment,
        accounts_text:accounts_text,
        employee_text:employee_text,
    },
    cache: false,
    success: function(res)
    {
        res = res.trim();
        if(res=='Failed'){
          $("#create").button('reset');

          swal(
            'Error Occured!',
            'Please try again later.',
            'error'
            )
      }else{
         if(kudostype=='Email'){
           var kudosid = res;
           var data = new FormData();
           $.each($("#nuploadpic")[0].files,function(i,file){
            data.append("imagefile",file);
        });
           data.append("kudosid",kudosid);
           $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>/index.php/Kudos/uploadscreenshot",
              data:data,
              contentType: false,
              cache: false,
              processData:false,
              success: function(res2)
              {
                if(res2.trim()=='Success'){
                    $("#create").button('reset');

                    swal(
                        'Kudos Added',
                        'Successfully uploaded and inserted kudos details.',
                        'success'
                        )
                }else{
                   $("#create").button('reset');

                   swal(
                    'Error Occured!',
                    'Please try again later.',
                    'error'
                    )
               }
           }
       });
       }else{
        $("#create").button('reset');

        swal(
            'Kudos Added',
            'Successfully inserted kudos details.',
            'success'
            )
    }

}
window.setTimeout(function(){  
    location.reload();
} ,2000);
}
});
}

})
</script>
<script type="text/javascript">
    $("#update").click(function(){ 
        var isOkay = true;       
        var accounts= $("#uaccounts").val();
        var employee = $("#uemployee").val();
        var reward = $("#ureward").val();
        var kudostype = $("#ukudostype").val();
        var clientname  = $("#uclientname").val();
        var clientemail = $("#uclientemail").val();
        var clientnumber = $("#uclientnumber").val();
        var comment  = $("#ucomment").val(); 
        var kudosid = $(this).data('kudosid');
        var file = $("#uuploadpic").val();
        if(accounts==''){
            isOkay = false;
            $('#uaccountsn').show();
        }else{

            $('#uaccountsn').hide();
        }
     // alert(employee);
     if(employee==null||employee==''){
        isOkay = false;
        $('#uemployeen').show();
    }else{

        $('#uemployeen').hide();
    }
    if(reward==''){
        isOkay = false;
        $('#urewardn').show();
    }else{

        $('#urewardn').hide();
    }
    if(kudostype==''){
        isOkay = false;
        $('#ukudostypen').show();
    }else{
       $('#ukudostypen').hide();
       if(kudostype=='Email'){
        alert("email");
        if(clientemail==''){
            isOkay = false; alert("false clientemail");
            $('#uclientemailn').show();
        }else{

            $('#uclientemailn').hide();
        }

        if(file==''&&$(this).data('okay')==1){
            $('#uuploadpicn').hide();
        }else if(file!=''){
            $('#uuploadpicn').hide();
        }else{
            isOkay = false; alert("false file");
            $('#uuploadpicn').show();
        }
    }else if(kudostype=='Call'){

        alert("call");
        if(clientnumber==''){
            isOkay = false; alert("false clientnumber");
            $('#uclientnumbern').show();
        }else{

            $('#uclientnumbern').hide();
        }
        if(comment==''){
            isOkay = false; alert("false comment");
            $('#ucommentn').show();
        }else{

            $('#ucommentn').hide();
        }
    }
}
if(clientname==''){
    isOkay = false;
    $('#uclientnamen').show();
}else{

    $('#uclientnamen').hide();
}
if(isOkay==true){
    $("#update").button('loading');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Kudos/updatekudos",
      data:{
        kudosid:kudosid,
        accounts:accounts,
        employee:employee,
        reward:reward,
        kudostype:kudostype,
        clientname:clientname,
        clientemail:clientemail,
        clientnumber:clientnumber,
        comment:comment
    },
    cache: false,
    success: function(res)
    {
     if(kudostype=='Email'){
        var uploadpic = $("#uuploadpic").val();
        if(uploadpic!=''){
         var data = new FormData();
         $.each($("#uuploadpic")[0].files,function(i,file){
            data.append("imagefile",file);
        });
         data.append("kudosid",kudosid);
         $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/uploadscreenshot",
          data:data,
          contentType: false,
          cache: false,
          processData:false,
          success: function(res2)
          {
           if(res2.trim()=='Success'){
               swal(
                'Kudos Uploaded!',
                'Successfully Updated Kudos Details.',
                'success'
                )
           }else{
            swal(
                'Error Occured!',
                'Please try again later.',
                'error'
                )
        }
    }
});
     }
 }else{
    swal(
        'Kudos Updated!',
        'Successfully Updated Kudos Details.',
        'success'
        )
}

window.setTimeout(function(){  
    location.reload();
} ,2000);

}
});
}
})
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>