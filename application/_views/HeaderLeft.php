<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
<style>
#head1{
	background-image: url('<?php echo base_url()."assets/images/".$this->session->userdata('pic'); ?>');
	background-size: 100%; 
	height: 300px;
	background-repeat: no-repeat;
    background-position: 64% 42%;
}
#head2{
	background-color: rgba(0, 0, 0, 0.63);
    position: absolute;
    top: 0;
    left: 0;
    height: 300px;
    width: 100%;
}
.demo-icon-akoniversion{
	font-size: 22px;
    line-height: 40px;
    margin: 10px;
    color: #92A0B3;
}
</style>
<script src="<?php echo base_url('node_modules/socket.io/node_modules/socket.io-client/socket.io.js'); ?>"></script>
<script src="<?php echo base_url();?>assets/js/moment-timezone-with-data.js"></script>
	<div id="header-nav-left">
        <div class="user-account-btn dropdown">
            <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                <img width="35" src="<?php echo base_url()."assets/images/".$this->session->userdata('pic'); ?>" style="border: 3px solid rgb(123, 123, 123);">
                <span><?php echo $this->session->userdata('lname'); ?></span>
                <i class="glyph-icon icon-angle-down"></i>
            </a>
            
			<div class="dropdown-menu float-left">
                <div class="box-sm">
                    <div class="login-box clearfix">
                        <div class="user-img">
                            <a href="#" title="" class="change-img">Change photo</a>
                <!--<img width="28" src="<?php echo base_url()."assets/images/SZ_logo.png"; ?>" alt="Profile image">-->
                <img width="28" src="<?php echo base_url()."assets/images/".$this->session->userdata('pic'); ?>" alt="Profile image">
                        </div>
                        <div class="user-info">
                            <span>
								<?php echo $this->session->userdata('fname')." ".$this->session->userdata('lname'); ?>
								<i><?php echo $this->session->userdata('description'); ?></i>
                             </span>
                            <a href="#" title="Edit profile" data-toggle="modal" data-target="#myProfile">Edit profile</a>
                         </div>
                    </div>
                    <div class="divider"></div>
                  
                    <div class="pad5A button-pane button-pane-alt text-center">
                        <a href="<?php echo base_url()."index.php/";?>login/logout" class="btn display-block font-normal btn-danger">
                            <i class="glyph-icon icon-power-off"></i>
                            Logout
                        </a>
                    </div>
                </div>
            </div> 
			
        </div>
    </div><!-- #header-nav-left -->
	    <div id="header-nav-right">
			
		</div><!-- #header-nav-right -->
		
<div id="myProfile" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" id="head1">
		<div id="head2">
			   <button type="button" class="close" data-dismiss="modal">&times;</button>
			   <div class="user-img" style="text-align: center;margin-top: 18%;">
					<img   src="<?php echo base_url()."assets/images/".$this->session->userdata('pic'); ?>" style="border-radius: 67%;border: 4px solid #b3b3b3;width: 100px;">
					<br>
					<span style="font-weight: 900;color: #FFEB3B;"><?php echo $this->session->userdata('pos'); ?></span>
					<br>
					<span style="font-weight: 900;color: white;"><?php echo $this->session->userdata('acc_name'); ?></span>
			   </div>
		</div>
      </div>
      <div class="modal-body">
	      <div class="example-box-wrapper">

		<div class="content-box tabs">
            <h3 class="content-box-header bg-black">
                <i class="glyph-icon icon-elusive-user"></i> 
                <ul>
                    <li>
                        <a href="#tabs-example-1" title="Tab 1">
                            Personal
                        </a>
                    </li>
                    <li>
                        <a href="#tabs-example-2" title="Tab 2">
                            Account
                        </a>
                    </li>
                  
                </ul>
            </h3>
            <div id="tabs-example-1">
                <table style="color:black;width: 100%;">
					<tr> <td> <i class="glyph-icon demo-icon-akoniversion icon-typicons-user"></i><b>Name: </b></td><td><?php echo $this->session->userdata('fname')." ".$this->session->userdata('lname'); ?></td></tr>
					<tr> <td><i class="glyph-icon demo-icon-akoniversion icon-typicons-calendar-outlilne"></i><b>Birthday:</b> </td><td><?php echo ($this->session->userdata('birthday')!="0000-00-00") ? $this->session->userdata('birthday') : '<span style="color:red">Not Set</span>'; ?></td></tr>
					<tr> <td><i class="glyph-icon demo-icon-akoniversion icon-typicons-phone-outline"></i><b>Cell:</b> </td><td><?php echo $this->session->userdata('cell'); ?></td></tr>
					<tr> <td><i class="glyph-icon demo-icon-akoniversion icon-typicons-mail"></i><b>Email:</b> </td><td><?php echo $this->session->userdata('email'); ?></td></tr>
				</table>
            </div>
            <div id="tabs-example-2">
                <table style="color:black;width: 50%;">
					<tr> <td> <i class="glyph-icon demo-icon-akoniversion icon-typicons-user"></i><b>Username: </b></td><td><?php echo $this->session->userdata('uname'); ?></td></tr>
 				</table>
			 
                            <button type="button" class="btn btn-info mrg20B" data-toggle="collapse" data-target="#demo-2">
                                Change Password
                            </button>

                            <div id="demo-2" class="collapse">

                                <div class="panel">
                                   
                                    <div class="panel-body">
									<div id="ChangeOutpt" style="text-align: center;padding: 9px;border-radius: 8px;"></div>
                                        <table style="color:black;width: 100%;">
											<tr> <td> <i class="glyph-icon demo-icon-akoniversion icon-typicons-direction"></i><b>Current Password: </b></td><td><input type="password" id="CurrPass"></td></tr>
											<tr> <td> <i class="glyph-icon demo-icon-akoniversion icon-typicons-direction-outline"></i><b>New Password: </b></td><td><input type="password" id="NewPass"> <button id="showme">Show</button></td></tr>
											<tr> <td> <i class="glyph-icon demo-icon-akoniversion icon-typicons-direction"></i><b>Re-Type Password: </b></td><td><input type="password" id="NewRPass"> <button id="showme2">Show</button></td></tr>
										</table> 
										<button type="button" class="btn btn-success"  id="PassUpdate">Update</button> 
                                    </div>
                                </div>

                            </div>
                        

            </div>
           
        </div>
        </div>
		</div>
     </div>

  </div>
</div> 

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/tabs-ui/tabs.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs-ui/tabs.js"></script>
<script type="text/javascript">
    /* jQuery UI Tabs */

    $(function() { "use strict";
        $(".tabs").tabs();
    });

    $(function() { "use strict";
        $(".tabs-hover").tabs({
            event: "mouseover"
        });
    });
</script>

<script src="<?php echo base_url(); ?>assets/widgets/accordion-ui/accordion.js"></script>
<script src="<?php echo base_url(); ?>assets/idle-timer/idle-timer.min.js"></script>

<script type="text/javascript">
    /* jQuery UI Accordion */

    $(function() { "use strict";
        $(".accordion").accordion({
            heightStyle: "content"
        });
    });

    $(function() { "use strict";
        $("#accordion-hover")
                .accordion({
                    event: 'mouseover',
                    heightStyle: 'auto'
                });
    });
</script>

<script>
	function action(){
	swal({
	  title: "Idle",
	  text: "You've been inactive lately..Please choose an action to proceed.",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Logout",
	  cancelButtonText: "Reload",
	  closeOnConfirm: false,
	  closeOnCancel: false,
	  allowOutsideClick: false
	  
	}).then(function(){
		location.href ="<?php echo base_url(); ?>index.php/login/logout";
	},function(dismiss) {
		// dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	   if (dismiss === 'cancel') {
		location.reload();
	  } 
	});
		
	}
	
    $(document).ready(function () {
	<?php if($this->session->userdata('class')=="Agent"){ ?>

        $( document ).idleTimer(60000); // 1000 = 1sec ; set to 1 minute for ambassador
	<?php }else{?>
		
		$( document ).idleTimer(600000); // 1000 = 1sec ; set to 10 minutes for admin
	<?php }?>
    });
    $( document ).on( "idle.idleTimer", function(event, elem, obj){
   		action();
	});
    $( document ).on( "active.idleTimer", function(event, elem, obj, triggerevent){
		action();
    });
</script>
<script>
  $(function(){
  $("#PassUpdate").click(function(){
	  	var CurrentPass= $("#CurrPass").val().trim();   
	  	var NewPass= $("#NewPass").val().trim();   
	  	var NewRPass= $("#NewRPass").val().trim();   
		
		 $.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/home/password",
			data: {
			CurrentPass:CurrentPass,
 			NewPass:NewPass,
			NewRPass:NewRPass,
   			},
			cache: false,
			success: function(html)
			{
 				if(html==0){
 					 $("#ChangeOutpt").html("<span style='color:white'><b>Password Error</b></span>");
					$("#ChangeOutpt").css("background","#FF5722");

				}else if(html==1){
  					 $("#ChangeOutpt").html("<span style='color:white'><b>Success</b></span>");
					 $("#ChangeOutpt").css("background","#4CAF50");


				}else if(html==2){
 					$("#ChangeOutpt").html("<span style='color:white'><b>New Passwords are not matched!</b></span>");
 					$("#ChangeOutpt").css("background","#ca9b3e");
				}else if(html==3){
  					$("#ChangeOutpt").html("<span style='color:white'> <b>Passwords cannot be empty!</b></span>");
					$("#ChangeOutpt").css("background","#FF5722");
				}else{
					alert("Something went wrong!");
				}
 			}
		});	 
		   
 });
 $('#NewPass').keyup(function(){
    str = $(this).val()
    str = str.replace(/\s/g,'')
    $(this).val(str)
});
 $('#NewRPass').keyup(function(){
    str = $(this).val()
    str = str.replace(/\s/g,'')
    $(this).val(str)
});
$('#showme').on('mouseenter',function(){$('#NewPass').attr('type','text');})
$('#showme').on('mouseleave',function(){$('#NewPass').attr('type','password');})
$('#showme2').on('mouseenter',function(){$('#NewRPass').attr('type','text');})
$('#showme2').on('mouseleave',function(){$('#NewRPass').attr('type','password');})
/*   setInterval(function()
   {
     $('#header-nav-right').load('<?php echo base_url(); ?>/index.php/dtr/getTime');
	 if(window.console || window.console.firebug) {
		 console.clear();
	}
 
	}, 1000);   */
	      var socket = io.connect('http://' + window.location.hostname + ':3000');
           function checkTime() {
                socket.emit('requesttime', {});
            }
            $(document).ready(function () {
			
                setInterval(checkTime, 1000); // This will run checkTime every seconds
            });
             socket.on('responsetime', function (data) {
			// console.log(data);
                // var date = moment(new Date(data)).format('dddd, MMMM DD, YYYY');
                // var time = moment(new Date(data)).format('hh:mm:ss A');
                var date = moment(data).tz('Asia/Manila').format('dddd, MMMM DD, YYYY');
                var time = moment(data).tz('Asia/Manila').format('hh:mm:ss A');
                $('#header-nav-right').html("<h2>" + time + "</h2><p>" + date + "</p>");
            }); 
			
 
  });	
</script>