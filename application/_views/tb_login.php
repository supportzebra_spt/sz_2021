<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap Login Form Template</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<?php echo base_url();?>assets-tb/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets-tb/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets-tb/css/form-elements.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets-tb/css/style.css">
         <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="<?php echo base_url();?>assets-tb/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets-tb/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets-tb/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets-tb/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets-tb/ico/apple-touch-icon-57-precomposed.png">
		<style>
		body{
			background-image: url("http://10.200.101.250/supportzebra/assets-tb/img/backgrounds/1.jpg")
		}
		</style>
    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class=" ">
                <div class="container">
                    
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
						<img  src="<?php echo base_url();?>assets-tb/up.png">
                       
                            <div class="form-bottom">
			                    <div   class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username">
			                        </div>
			                       
			                        <button type="submit" class="btn" id="btnlogin" style="width: 100%;">Sign in!</button>
			                    </div>
		                    </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
	<button type="submit" class="btn" id="modal" data-toggle="modal" data-target="#myModal" style="display:none;">Sign in!</button>

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			  </div>
			  <div class="modal-body">
				<p>Some text in the modal.</p>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
        <!-- Javascript -->
        <script src="<?php echo base_url();?>assets-tb/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url();?>assets-tb/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets-tb/js/jquery.backstretch.min.js"></script>
        <script src="<?php echo base_url();?>assets-tb/js/scripts.js"></script>
        <script>
			$(function(){
				$("#btnlogin").click(function(){
					var uname = $("#form-username").val();
						$.ajax({
							type: "POST",
							url: "<?php echo base_url(); ?>job/addJob",
							data: {
							  uname: uname,
 							 },
							cache: false, 
							success: function(result)
							{
							   $.jGrowl("Job Successfully Posted.", {
									  sticky: false,
									  position: 'top-right',
									  theme: 'bg-green'
								  });
								  $(this).attr("disabled");
							 setTimeout(function(){location.reload();},2000);
							}
						  }); 
				});	
			});
		</script>
    

    </body>

</html>