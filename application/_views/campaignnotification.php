<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/lockscreen-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
<head>
  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  /*  body{
      background-color: #333 !important;
    }
    #moving {
      font-family: "Avant Garde", Avantgarde, "Century Gothic", CenturyGothic, "AppleGothic", sans-serif;
      font-size: 23px;
      padding: 20px 13px;
      text-align: center;
      text-rendering: optimizeLegibility;
      color: #e0dfdc;
      letter-spacing: .1em;
      text-shadow: 
      0 -1px 0 #fff, 
      0 1px 0 #2e2e2e,  
      0 2px 0 #2a2a2a, 
      0 3px 0 #262626, 
      0 4px 0 #222,  
      0 5px 0 #1e1e1e, 
      0 6px 0 #1a1a1a, 
      0 7px 0 #161616, 
      0 8px 0 #121212, 
      0 9px 10px rgba(0, 0, 0, 0.9);
    }*/
  </style>
  <style type="text/css">
    .notepad,.notepad2,.notepad3,.notepad4 {
      font-family: 'Handlee', cursive;
      width: 92%;
      min-height:300px;
      margin:20px auto;
      padding:2px;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
      background: #fcf59b;
      background:
      -webkit-gradient(
        linear,
        left top, left bottom,
        from(#81cbbc),
        color-stop(2%, #fcf59b)
        );
      background:
      -moz-repeating-linear-gradient(
        top,
        #fcf59b,
        #fcf59b 38px,
        #81cbbc 40px
        );
      background:
      repeating-linear-gradient(
        top,
        #fcf59b,
        #fcf59b 38px,
        #81cbbc 40px
        );    
      -webkit-background-size: 100% 40px;
    }
    .notepad2{
      top:0;
      right:10;
      z-index:-3;
      position:absolute;
      -ms-transform: rotate(-2deg); /* IE 9 */
      -webkit-transform: rotate(-2deg); /* Chrome, Safari, Opera */
      transform: rotate(-2deg);
    }
    .notepad3{
      top:0;
      z-index:-2;
      right:10;
      position:absolute;
      -ms-transform: rotate(5deg); /* IE 9 */
      -webkit-transform: rotate(5deg); /* Chrome, Safari, Opera */
      transform: rotate(5deg);
    }
    .notepad4{
      top:0;
      z-index:-1;
      right:10;
      position:absolute;
      -ms-transform: rotate(2deg); /* IE 9 */
      -webkit-transform: rotate(2deg); /* Chrome, Safari, Opera */
      transform: rotate(2deg);
    }
    #moving {
      font-size: 24px;
      line-height: 40px;
      text-align: center
    }
    #pin{
      -moz-transform: scaleX(-1);
      -o-transform: scaleX(-1);
      -webkit-transform: scaleX(-1);
      transform: scaleX(-1);
      filter: FlipH;
      -ms-filter: "FlipH";
    }
  </style>
  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> ANNOUNCEMENT </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>
  <script type="text/javascript">
    $(window).load(function(){

      // swal('Page Loaded!!','Successfully loaded the page.','success');
      setTimeout(function() {
        $('#loading').fadeOut( 400, "linear" );
      }, 300);
    });
  </script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/typeit.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert.css">
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/sweetalert.min.js"></script>
</head>
<body style='background-image:url("<?php echo base_url();?>assets/images/emoji/beige-paper.png");background-color:blancedalmond'>
  <div id="loading">
    <div class="spinner">
      <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div>
    </div>
  </div>
  <script>
    var unloadEvent = function (e) {
      var confirmationMessage = "Warning: Leaving this page will result in any unsaved data being lost. Are you sure you wish to continue?";

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Webkit, Safari, Chrome etc.
      };
      window.addEventListener("beforeunload", unloadEvent);
    </script>
 <!--      <div id="page-title">
        <h1 id="moving"></h1> 
      </div> -->
      <div class="notepad">
        <img id="pin" title='View Annoucement' src="<?php echo base_url();?>assets/images/emoji/pin/Map-Marker-Push-Pin-2-Left-Azure-icon.png" style='width:70px;height:70px;right:5%;top:1%;position:absolute;'>
        <div id="moving"></div>
      </div>
      <div class="notepad4"></div>
      <div class="notepad3"></div>
      <div class="notepad2"></div>

      <!-- JS Demo -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
      <script type="text/javascript">
        var note = "<?php echo $values->acc_name;?> Announcement !!";
        var title = "<?php echo $values->title;?>";
        var text = "<?php echo $values->message;?>";
        var textarr = text.split(".");    
        var pop = textarr.pop();
        if(pop!=""){
          textarr.push(pop);
        }
        var cnt = textarr.length;
        $('#moving').typeIt({
          speed: 100,
          lifeLike: false,
          cursor:true,
          startDelay:1000,
          loop:true,
          autoStart:true,
          deleteSpeed:30,
          loopDelay:1000,
          breakLines:true
        });

        $('#moving').tiBreak();
        $('#moving').tiType(note);
        $('#moving').tiPause(2000);
        $('#moving').tiDelete();
        $('#moving').tiSettings({speed: 75});
        $('#moving').tiBreak();
        $('#moving').tiType(title);
        $('#moving').tiPause(3000);
        $('#moving').tiBreak();
        for(var x=0;x<cnt;x++){
          var ccnt = textarr[x].length;
          $('#moving').tiType(textarr[x]);
          $('#moving').tiPause(3000);
          $('#moving').tiDelete(ccnt);
        }
      </script>
      <script type="text/javascript">
        $("#pin").hover(function(){
          $(this).css({'right':'4%','top':'1%'});
          $(this).attr("src", function(index, attr){
            return attr.replace("Map-Marker-Push-Pin-2-Left-Azure-icon.png", "Map-Marker-Push-Pin-1-Left-Azure-icon.png");
          });
        }, function(){
          $(this).css({'right':'5%','top':'1%'});
          $(this).attr("src", function(index, attr){
            return attr.replace("Map-Marker-Push-Pin-1-Left-Azure-icon.png", "Map-Marker-Push-Pin-2-Left-Azure-icon.png");
          });
        });
      </script>
    </body>

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/lockscreen-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
    </html>