<!DOCTYPE html> 
<html  lang="en">

<head>

    <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    .hidden{
        visibility: hidden;
    }
    .seen{
        visibility: visible;
    }

</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- jQueryUI Autocomplete -->



<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

<!--<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>-->

<!-- High Charts-->

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/themes/dark-unica.js"></script>

<script type="text/javascript">

</script>


<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<!-- JQUERY CONFIRM-->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.js"></script>


<!-- FORM MASKS-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>
<script type="text/javascript">
    /* Input masks */

    $(function() { "use strict";
        $(".input-mask").inputmask();
    });

</script>


<script type="text/javascript">
    $(window).load(function(){
       setTimeout(function() {
        $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });
</script>



<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.datepick').each(function(){
            $(this).datepicker();
        });
    });
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

<!-- Table to Excel -->
<script type="text/javascript" src="<?php echo base_url();?>assets/table_export/jquery.table2excel.js" ></script>

<!-- Parsley -->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>


<!-- This is what you need -->
<script src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.min.js"></script>
<link href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.min.css" rel="stylesheet"> 

</head>


<body>
    <div id="sb-site">


        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div id="page-wrapper">
            <div id="page-header" class="bg-black">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                    <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                </div>
                <div id="header-logo" class="logo-bg">
                 <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                 <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id='headerLeft'>
            </div>



        </div>
        <div id="page-sidebar">
            <div class="scroll-sidebar">


                <ul id="sidebar-menu">
                </ul>
            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content">

                <div class="container">
                    <div id="page-title">
                        <h2>Kudos Excel</h2>
                        <hr>
                        <div class="content-box">
                            <h3 class="content-box-header bg-black">
                                <i class="glyph-icon icon-gift"></i>
                                Create Multiple Kudos
                            </h3>
                            <div class="content-box-wrapper">
                                <a href="<?php echo base_url().'index.php/Kudos/kudos_add'; ?>" class="btn bg-orange">
                                    <span class="glyph-icon icon-separator">
                                        <i class="glyph-icon icon-arrow-left"></i>
                                    </span>
                                    <span class="button-content">
                                        Back
                                    </span>
                                </a>
                                <a href="<?php echo base_url(); ?>/reports/AddMultipleTemplate.xlsx" class="btn bg-blue" id="downloadTemplate">
                                    <span class="glyph-icon icon-separator">
                                        <i class="glyph-icon icon-download"></i>
                                    </span>
                                    <span class="button-content">
                                        Download
                                    </span>
                                </a>
                                <a href="#" class="btn bg-red" id="upload">
                                    <span class="glyph-icon icon-separator">
                                        <i class="glyph-icon icon-file"></i>
                                    </span>
                                    <span class="button-content">
                                        Add File
                                    </span>
                                </a>
                                <br><br>
                                <div class="row">
                                    <div class="form-group col-sm-5" id="uploadFileDiv" style="display: none;">
                                        <input type="file" class="form-control  sweet-3" id="uploadFile">
                                    </div>
                                    <div class="col-sm-7">
                                        <a href="#" class="btn  bg-primary" id="upload2" style='display:none'>
                                            <span class="glyph-icon icon-separator">
                                                <i class="glyph-icon icon-upload"></i>
                                            </span>
                                            <span class="button-content">
                                             Upload
                                         </span>
                                     </a>
                                     <a href="#" class="btn  bg-primary" id="view" style='display:none'>
                                        <span class="glyph-icon icon-separator">
                                            <i class="glyph-icon icon-list-alt"></i>
                                        </span>
                                        <span class="button-content">
                                         View
                                     </span>
                                 </a>
                                 <a href="#" class="btn  bg-primary" id="add" style='display:none'>
                                    <span class="glyph-icon icon-separator">
                                        <i class="glyph-icon icon-check"></i>
                                    </span>
                                    <span class="button-content">
                                     Save
                                 </span>
                             </a>
                         </div>
                     </div>
                     <div id="showTable" style="display: none;">
                         <hr>
                         <table class="table table-striped table-condensed table-bordered font-size-13 bg-black" id="table_report2" cellpadding="0" >
                            <thead>
                                <tr>
                                    <th>Employee </th>
                                    <th>Account </th>
                                    <th>Kudos Type</th>
                                    <th>Reward Type</th>
                                    <th>Client's Name</th>
                                    <th>Client's Number</th>
                                    <th>Client's Email</th>
                                    <th>Client's Comment</th>
                                    <th>Date Given</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>                                   
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>



</div>
</div>
</div>
</body>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/menu.js"></script>

<script>
    $(function(){
        $.ajax({
          type: "POST",
          url: "<?php echo site_url();?>index.php/Templatedesign/headerLeft",
          cache: false,
          success: function(html)
          {

            $('#headerLeft').html(html);
     //alert(html);
 }
}); 
        $.ajax({
          type: "POST",
          url:"<?php echo site_url();?>index.php/Templatedesign/sideBar",
          cache: false,
          success: function(html)
          {

            $('#sidebar-menu').html(html);
   // alert(html);
}
});
    }); 
</script>
<script>

    $(function(){

        $("#upload").click(function(){

            $('#uploadFileDiv').toggle();
            $("#view").attr("disabled", true);
            $("#add").attr("disabled", true);

        });
        $('#uploadFile').on('change', function(){
            if($('#uploadFile')[0].files[0].name == 'AddMultipleTemplate.xlsx'){
                $("#view").show();
                $("#add").show();
                $("#upload2").show();
                
            }else{
                // alert("INvalid FILE");
                
                $("#upload2").hide();
                $("#view").hide();
                $("#add").hide();
                swal("Oops!", "Please use the template given!", "error");
            }
            // if ($('#uploadFile').get(0).files.length !== 0) {
            //     $("#view").show();
            //     $("#add").show();
            //     $("#upload2").show();
            // }
        });

        $("#upload2").click(function(event){

            var data = new FormData();
            $.each($("#uploadFile")[0].files,function(i,file){
                data.append("uploadFile",file);
            });                      

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Kudos/uploadExcel/",
                data: data,
                cache: false,
                processData:false,
                contentType:false,
                success: function(response)
                {
                    swal('Success!','Successfully Uploaded','success'); 
                }
            });
            event.preventDefault();

            $("#view").removeAttr("disabled");
        });

        $("#view").click(function(){

            $.ajax({
                url: "<?php echo base_url(); ?>index.php/Kudos/viewAddMultiple/",
                success: function(data)
                {
                    $('#showTable').toggle();
                    $('#showTable tbody').html(data);
                }
            });

            $("#add").removeAttr("disabled");
        });

        $("#add").click(function(){
           swal({
            title: 'KUDOS',
            type: 'info',
            html:
            'Save Excel File data?',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: 
            '<i class="glyph-icon icon-check"></i> Confirm',
            cancelButtonText:
            '<i class="glyph-icon icon-times"></i>'
        }).then(function(){
           $.ajax({
            url: "<?php echo base_url(); ?>index.php/Kudos/addAddMultiple/",
            success: function()
            {
               swal(
                'Kudos Added',
                'Successfully Added Multiple Kudos',
                'success'
                );
           }
       });
           // window.setTimeout(function(){  
            // location.reload();
        // } ,2000);
       })         
    });



    });
</script>
<a href="<?php echo base_url(); ?>/reports/KudosCount.xls" style="display: none;"><input type="button" id="kcount" value="Test" class="btn btn-success"></a>
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>

</html>