<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		#tblShowTR td{
			color: green;
		}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  
    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>






    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

 		<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-gradient-9">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper" >
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">

<div class="col-md-12">
        <div class="panel">
            <div class="panel-body" >
   	<a href="../../Payroll/GeneratePayroll/<?php echo $period["coverage_id"]; ?>" class="btn btn-xs btn-success glyph-icon   icon-mail-reply" title="" style="margin-bottom: 9px;"> Back</a>

                 <div class="example-box-wrapper" >
                    <form class="form-horizontal bordered-row" role="form">
		
                         <div class="form-group" id="employzResult">
                    <div class="col-sm-12">
                        <!--<select multiple class="multi-select" id="employz">
 								<?php 
								foreach($employee as $k => $v){
									
									echo "<option value=".$v['emp_id'].">".$v['fname']." ".$v['lname']."</option>";
								}
								
								?>
                         </select>-->
						 
          <form id="demoform" action="#" method="post">
		  		 
					 <div class="form-group">
                    <label class="col-sm-3 control-label">Payroll Coverage:</label>
                    <div class="col-sm-6">
                        <select name="" class="chosen-select" style="display: none;" id="payrollC">
								 
                                <option disabled="disabled" selected >--</option>
							 
							  <optgroup label="<?php echo $period["month"]; ?>">
										<option selected><?php echo $period["daterange"]; ?></option>
								</optgroup>
								 
                        </select>
	 
                    </div>

                </div>
            <select multiple="multiple" size="10" name="duallistbox_demo1[]" id="employz">
           <?php 
			foreach($employee as $k => $v){
				
				?>
									
									 <option value="<?php echo $v['emp_id']; ?>" <?php echo ($v["payroll_id"]>0) ? "selected" : " "?>><?php echo $v['lname'].", ".$v['fname']; ?></option> 
							<?php 	}
								
								?>
            </select>
             </form>

                    </div>
                </div>
					<input type="button" class="btn btn-xs btn-primary" id="btnShow" value="Show">
					<input type="button" class="btn btn-xs btn-info" id="btnExport" value="Export">
					<input type="button" class="btn btn-xs btn-info" id="btnSavePayroll" value="Save">
 
     
                    </form>
                </div>
            </div>
        </div>
   
       
    </div>
<div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                
                <div class="example-box-wrapper" style="overflow-x: scroll;">
						<!--<div id="loadingIMG" style="position: fixed; margin: -20% auto 0px; width: 100%; z-index: 1000; display: none;margin-left:20%;">
							 <img src="<?php echo base_url();?>assets/images/loading-gears-animation-3.gif"  > 
						</div>-->
                    <form class="form-horizontal bordered-row" role="form">

                         <table  class="table table-hover" > 
						 <th>Fullname</th>
						 <th>Position</th>
						 <th>Monthly</th>
						 <th>Quinsina</th>
						 <th>Daily</th>
						 <th>Hourly</th>
						 <th>Total</th>
						 <th style='text-align:center;' colspan=3>  Basic Allowance </th>
 						 <th>Allowance</th>
 						 <th>OT-BCT</th>
						 <th>ND</th>
						 <th>HP</th>
						 <th>HO</th>
						 <th>SSS</th>
						 <th>PHIC</th>
						 <th>HDMF</th>
						 <th>BIR</th>
						 <th>Adjust(+)</th>
						 <th>Adjust(-)</th>
						 <th>Earning</th>
						 <th>Deduction</th>
						 <th>TakeHome Pay</th>
						 <tbody id="tblShow">
						 </tbody>
							
                         </table>
                        
                    </form>
                </div>
            </div>
        </div>
   
       
    </div>
	

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>
<input type="button" value="test" id="test">

     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
	  $("#btnShow").trigger('click'); 
	    $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
  });	
</script> 
<script>
  $(function(){
	  	    // $("#loadingIMG").hide();
		
	  $("#btnShow").click(function(){
		  
		  var payrollC = $("#payrollC").val();
		  var employz = $("#employz").val();
		   var dataString = "payrollC="+payrollC+"&employz="+employz;
 		    $.ajax({
                //url of the function
                    url: '<?php echo base_url() ?>index.php/attendance2/emptype',
                    type: 'POST',
                    data:  dataString,
					 // beforeSend: function(){
						// $('#loadingIMG').show();
						// $(".container").css("opacity",".5");
					// },
					// complete: function(){
						 // $('#loadingIMG').fadeOut("slow");
							// $(".container").css("opacity","1");

					// },
					success:function(res)
                    {
							var td = "";
						var obj = jQuery.parseJSON(res);
						var timeTotal=0;
						var TotalAmountHoliday=0;
						var timeTotalOTBCT=0;
						var timeTotalBCT;
						var timeTotalVal;
						var Mrate=0;
						var daily =0;
						var hourly =0;
						var ndExplode = "";
						var ndRate=0;
						var ndRatexBCT=0;
						var ndRatexBCT2=0;
						var NDvalBCT=0;
						var HPval=0;
						var HPval2=0;
						var SSS;
						var PhilHealthCompute;
						var PagIbig;
						var pos_name;
						var hpRate;
						var fname;
						var emp_promoteId;
						var sss_id;
						var philhealth_id;
						var tax_id;
						var BIRCompute=0;
						var percent=0;
						var salarybase=0;
						var BIRTotalCompute=0;
						var BIRDescription;
						var TotalGross=0;
						var TotalNet=0;
						var TotalPay=0;
						var adjustSalary;
						var deductSalary;
						var payroll_adjDeduct_amount;
						var pemp_adjust_id;
						var empPosAllowance;
						var empPosBonus;
						var HPComputeHolidayLogin=0;
						var NDComputeHolidayLogin=0;
						var NDComputeHolidayLogout=0;
						var HPComputeHolidayLogout=0;
						var HPComputeHoliday=0;
						var NDComputeHoliday=0;
						var BCTComputeHoliday=0;
						var rate2=0;
						var isHoliday=0;
						var isND=0;
					if(res!=0){
						$(obj.employee).each(function(key,value){
							
							 $.each(value,function(k, v ){
								$.each(v,function(ky, vy ){
								timeTotalVal = (parseFloat(vy['total'])>=parseFloat("8.0")) ? "8.0" : ""+parseFloat(vy['total'])-parseFloat("1.0")+"" ;
									timeTotalBCT = (vy['bct']=='Q') ? "0.21" : "0.00";
									 Mrate = vy['rate'];
									 daily = ((vy['rate']*12)/261);
									 hourly = daily/8; 
									 if(vy['nd']!=null){
										 ndExplode = vy['nd'].split("-");
										ndRate = parseFloat(ndExplode[0]); 
										ndRate += parseFloat(ndExplode[1]/60); 
										ndRatexBCT = (ndRate == 7)? parseFloat(ndRate).toFixed(2) :(parseFloat(ndRate.toFixed(2))+parseFloat(timeTotalBCT)).toFixed(2); 
										HPval = parseFloat(ndRate).toFixed(2);
									 }else{
										 ndRatexBCT = 0.00;
										 HPval =0.00;
									 }
										NDvalBCT = (vy['ndRemark']=='Y' && timeTotalBCT != 0.00) ?  "0.21" : "0.00" ;
 										//HPval = parseFloat(ndRate).toFixed(2);
									 
									 
									 timeTotal +=parseFloat(timeTotalVal);
									 isHoliday +=parseFloat(vy['isHoliday']);
									 
										// timeTotalOTBCT +=parseFloat(timeTotalBCT);
// timeTotalOTBCT +=(vy['HolidayType1'] =='Regular') ? parseFloat((timeTotalBCT)*1)+parseFloat(timeTotalBCT) : parseFloat((timeTotalBCT)*.30)+parseFloat(timeTotalBCT); //Total number of time for BCT 0.21 + (Holiday then it will be times two)
										// ndRatexBCT2+=parseFloat(ndRatexBCT);
										// HPval2 +=parseFloat(HPval);
 
									if(vy['HolidayType1'] =='Regular'){
										timeTotalOTBCT +=parseFloat((timeTotalBCT)*1)+parseFloat(timeTotalBCT);
										//alert("100 = "+parseFloat((timeTotalBCT)*1)+parseFloat(timeTotalBCT));
									}else if(vy['HolidayType1'] =='Special'){
										timeTotalOTBCT +=parseFloat((timeTotalBCT)*.30)+parseFloat(timeTotalBCT);
										//alert("30 = "+parseFloat((timeTotalBCT)*.30)+parseFloat(timeTotalBCT));
									}else{
										timeTotalOTBCT+=parseFloat(timeTotalBCT);
									}
									  ndRatexBCT2+=parseFloat(ndRatexBCT)+parseFloat(vy['NDComputeHolidayLogin'])+parseFloat(vy['NDComputeHolidayLogout']); // Night Diff + ( [Special/Regular] Holiday ND)...
 
									HPval2 +=parseFloat(HPval)+parseFloat(vy['HPComputeHolidayLogin'])+parseFloat(vy['HPComputeHolidayLogout']); // Hazard Pay + ( [Special/Regular] Holiday HP)
 									SSS = parseFloat(vy['SSS']/2).toFixed(2);
									PhilHealthCompute = parseFloat(vy['PhilHealthCompute']/2).toFixed(2);
									PagIbig = parseFloat((vy['rate']*.02)/2).toFixed(2);
									pos_name =vy['pos_name'];
									hpRate =vy['hpRate'];
									fname =vy['fname'];
									sss_id =vy['sss_id'];
									philhealth_id =vy['philhealth_id'];
									tax_id =vy['tax_id'];
									emp_promoteId =vy['emp_promoteId'];
									BIRCompute =vy['BIRCompute'];
									percent =vy['BIRPercent'];
									salarybase =vy['BIRSalaryBase'];
									TotalAmountHoliday +=parseFloat(vy['TotalAmountHoliday']);
									BIRDescription =vy['BIRDescription'];
									payroll_adjDeduct_amount =parseFloat(vy['payroll_adjDeduct_amount']/2);
									pemp_adjust_id =vy['pemp_adjust_id'];
									adjustSalary = (vy['adjustSalary']!=null) ? vy['adjustSalary'].split(","): 0;
									deductSalary = (vy['deductSalary']!=null) ? vy['deductSalary'].split(","): 0;
									empPosAllowance = (vy['empPosAllowance']!=0) ? vy['empPosAllowance'].split(","): 0;
									empPosBonus = (vy['empPosBonus']!=0) ? vy['empPosBonus'].split(","): 0;

						NDComputeHolidayLogin += parseFloat(vy['NDComputeHolidayLogin'])*parseFloat(hourly*.15);
						NDComputeHolidayLogout += parseFloat(vy['NDComputeHolidayLogout'])*parseFloat(hourly*.15);
						// HPComputeHolidayLogout += parseFloat(vy['HPComputeHolidayLogout'])*parseFloat(hourly*hpRate);
						// HPComputeHolidayLogin += parseFloat(vy['HPComputeHolidayLogin'])*parseFloat(hourly*hpRate);
						HPComputeHolidayLogout += parseFloat(vy['HPComputeHolidayLogout']);
						HPComputeHolidayLogin += parseFloat(vy['HPComputeHolidayLogin']);

										rate2 = vy['rate2'];
								});
								BIRTotalCompute = parseFloat(((rate2-salarybase)*percent)) + parseFloat(BIRCompute);

								var TotalBCTxHourly = timeTotalOTBCT.toFixed(2) * hourly.toFixed(2);
 									var i;
									var ii;
									var iii=0;
									var x;
									var xx;
									var xxx=0;
									var y;
									var yy;
									var yyy=0;
									var jjj=0;
									var HPHoliday=0;
									var NDHoliday=0;
									var BIRTotalCompute2=0;
									 HPHoliday=(parseFloat(HPComputeHolidayLogin+HPComputeHolidayLogout)*parseFloat(hourly*hpRate));
									 NDHoliday=NDComputeHolidayLogin+NDComputeHolidayLogout;
									var finalTotalGross=0;
									var TotalHazardPayFinal=0;
									 // TotalHazardPayFinal=((HPval2 * (hourly*hpRate))+HPHoliday);
									 TotalHazardPayFinal=((HPval2 * (hourly*hpRate)));
									 var TotalNighDiffFinal=0;
									 // TotalNighDiffFinal=((ndRatexBCT2*(hourly*.15))+NDHoliday);
									 TotalNighDiffFinal=((ndRatexBCT2*(hourly*.15)));
										BIRTotalCompute2 = (BIRTotalCompute<0) ? 0: BIRTotalCompute;

									 // TotalGross =  parseFloat(TotalBCTxHourly)+parseFloat((ndRatexBCT2*(hourly*.15)))+parseFloat((HPval2 * (hourly*hpRate)));
									 TotalGross =  parseFloat(TotalBCTxHourly)+parseFloat(TotalNighDiffFinal)+parseFloat(TotalHazardPayFinal);
									td+="<tr>"+
								 
									"<td hidden>"+ emp_promoteId+"</td>"+
									"<td hidden>"+ sss_id+"</td>"+
									"<td hidden>"+ philhealth_id+"</td>"+
									"<td hidden>"+ tax_id+"</td>"+
									"<td>"+ fname+"</td>"+
 									"<td>"+ pos_name+"</td>"+
 									"<td>P"+ parseFloat(Mrate).toFixed(2)+"</td>"+
 									"<td>P"+ parseFloat(Mrate/2).toFixed(2)+"</td>"+
 									"<td>P"+ daily.toFixed(2)+"</td>"+
 									"<td>P"+ hourly.toFixed(2)+"</td>"+
									"<td>"+ timeTotal.toFixed(2)+"</td>";
								 
									if(empPosAllowance!=0){
										$.each(empPosAllowance,function(susi,balue){
											
											xx = balue.split("=");	
										td+="<td> <b>P"+ parseFloat((xx[1])).toFixed(2)+" ("+xx[0]+") </b></td>";
										xxx += parseFloat(xx[1]);
 										});
										}else{
										td+="<td> <b>--</b></td><td> <b>--</b></td><td> <b>--</b></td>";

											xxx = 0;
 										}
										if(empPosBonus!=0){
										$.each(empPosBonus,function(susi,balue){
											
											yy = balue.split("=");	
										// td+="<td> <b>P"+ parseFloat((yy[1])).toFixed(2)+"</b></td>";
										yyy += parseFloat(yy[1]);
 										});
										}else{
										// td+="<td> <b>--</b></td><td> <b>--</b></td><td> <b>--</b></td>";
											yyy = 0;
 										}
									if(yyy!=0){
										td+="<td> <b>P"+ parseFloat((yy[1])).toFixed(2)+"</b></td>";
									}else{
										td+="<td> <b>N/A</b> </td>";
									}
									td +="<td><b>P"+TotalBCTxHourly.toFixed(2)+"s</b></td>"+
									"<td><b>P"+TotalNighDiffFinal.toFixed(2)+"</b></td>"+
									"<td><b>P"+ TotalHazardPayFinal.toFixed(2)+"</b></td>"+
									"<td><b>P"+ TotalAmountHoliday.toFixed(2)+"</b></td>"+
									"<td><b>P"+SSS+"</b></td>"+
									"<td><b>P"+PhilHealthCompute+"</b></td>"+
									"<td><b>P"+PagIbig+"</b></td>"+
									"<td><b>P"+BIRTotalCompute2.toFixed(2)+"<br>("+BIRDescription+")</b></td>";
										if(adjustSalary!=0){
										$.each(adjustSalary,function(yek,lav){
											
											ii = lav.split("=");	
 										iii += parseFloat(ii[1]);
 										});
										}else{
											iii = 0;
 										}
										//finalTotalGross = (parseFloat(TotalGross)+parseFloat(iii)+parseFloat(Mrate/2));
										finalTotalGross = (parseFloat(TotalAmountHoliday)+parseFloat(TotalGross)+parseFloat(xxx)+parseFloat(yyy)+parseFloat(iii)+parseFloat(Mrate/2));
									td+="<td><b>P"+iii.toFixed(2)+"</b></td>";
										if(deductSalary!=0){
										$.each(deductSalary,function(yek,lav){
											
											ii = lav.split("=");	
 										jjj += parseFloat(ii[1]);
 										});
										}else{
											jjj = 0;
 										}
										// TotalNet =  parseFloat(SSS)+parseFloat(PhilHealthCompute)+parseFloat(PagIbig)+parseFloat(BIRTotalCompute)+parseFloat(jjj);
										// TotalPay = (finalTotalGross) - TotalNet;
										TotalNet =  parseFloat(SSS)+parseFloat(PhilHealthCompute)+parseFloat(PagIbig)+parseFloat(BIRTotalCompute2)+parseFloat(jjj)+ payroll_adjDeduct_amount;
										TotalPay = (finalTotalGross) - TotalNet;

									td+="<td><b>P"+jjj.toFixed(2)+"</b></td>"+
									"<td><b>P"+finalTotalGross.toFixed(2)+" </td>"+
									"<td><b>P"+TotalNet.toFixed(2)+" </td>"+
									"<td style='color:green'><b>P"+ TotalPay.toFixed(2)+" <b></td>"+
									"<td hidden> "+pemp_adjust_id+" </td>"+
									"<td hidden> "+isHoliday+" </td>"+
									"<td hidden> "+HPval2+" </td>"+
									"<td hidden> "+ndRatexBCT2+" </td>"+
									" </tr>";
 
 								 td+="<tr><td colspan=24 style='background: #e8e8e8;'> </td></tr>";
										isHoliday =0;
 										timeTotal =0;
									timeTotalOTBCT =0;
									ndRatexBCT2 =0;									
									HPval2 =0;	
									rate2= 0;	
									 HPComputeHolidayLogin=0;
									NDComputeHolidayLogin=0;
									NDComputeHolidayLogout=0;
									HPComputeHolidayLogout=0;
									TotalAmountHoliday=0;
									BIRTotalCompute2=0;	
iii=0;									
xxx=0;									
 							  });
						});
					}else{
						td+=" ";	
						}
						 $("#tblShow").html(td);
						
                    }
                }); 
	  });
	  $("#btnSavePayroll").click(function(){
		  var rowvalue = [];
$("tbody > tr:odd").each(function(i, v) {
	 
     rowvalue[i] = $('td', this).map(function() {
        return $(this).text()
    }).get()
	 
});
var dataSet = JSON.stringify(rowvalue);
var coverage_id = "<?php echo $period["coverage_id"]; ?>";

   console.log(rowvalue);
   $.ajax({
	url: '<?php echo base_url() ?>index.php/attendance2/savePayroll',
    type: "POST",
    data: {
		dataArray: dataSet,
		coverage_id: coverage_id
		},
    success: function (result) {
		//alert(result);
    }
});  

	});	
	});	
</script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
   
  <script>
            var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
            $("#demoform").submit(function() {
              alert($('[name="duallistbox_demo1[]"]').val());
              return false;
            });
          </script>
 </html>