<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
        .hidden{
            visibility: hidden;
        }
        .seen{
            visibility: visible;
        }
        td{
            font-size:14px;
        }
    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- jQueryUI Autocomplete -->



    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

    <!--<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>-->

    <!-- High Charts-->

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/themes/dark-unica.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            chart = Highcharts.chart('chart_0', {
                chart: {
                    inverted: true,
                    polar: false,
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Kudos Count by Campaigns from <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
                },

                xAxis: {
                    categories: [<?php foreach($campaign_kcount as $kc){ echo "'";echo $kc->campaign; echo "', "; }?>]
                },

                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                        },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php foreach($campaign_kcount as $kc){ echo $kc->kudos_count; echo ","; }?>],
            showInLegend: false,
        }],


    });
            Highcharts.setOptions(Highcharts.theme);
            var chart = Highcharts.chart('chart_1', {
                chart: {
                    inverted: true,
                    polar: false,
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Kudos Count from <?php date_default_timezone_set('Asia/Manila'); echo date("Y");?>'
                },

                xAxis: {
                    categories: [<?php foreach($k_count as $kc){ echo "'";echo $kc->month; echo "', "; }?>]
                },

                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                        },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php foreach($k_count as $kc){ echo $kc->kudos_count; echo ","; }?>],
            showInLegend: false,
        }],


    });
            Highcharts.setOptions(Highcharts.theme);
            Highcharts.chart('chart_2', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Top 5 Ambassadors for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        },
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Kudos Count',
                    data: [

                    <?php foreach($top as $t){ ?>
                        {
                            name: '<?php echo $t->ambassador;?>',
                            y: <?php echo $t->kudos_count;?>
                        },
                        <?php }?>
                        ]
                    }]
                });
            Highcharts.setOptions(Highcharts.theme);
            Highcharts.chart('chart_3', {
                chart: {
                    inverted: true,
                    polar: false
                },
                title: {
                    text: 'Kudos Count from <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
                },
                credits: {
                    enabled: false
                },

                xAxis: {
                    categories: [<?php foreach($all as $a){ echo "'";echo $a->ambassador; echo "', "; }?>]
                },

                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                        },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php foreach($all as $a){ echo $a->kudos_count; echo ","; }?>],
            showInLegend: false
        }]

    });
            Highcharts.setOptions(Highcharts.theme);
            Highcharts.chart('chart_4', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Top 5 Campaigns for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Kudos Count',
                    data: [

                    <?php foreach($top_campaigns as $t){ ?>
                        {
                            name: '<?php echo ucwords($t->campaign);?>',
                            y: <?php echo ucwords($t->kudos_count);?>,
                            drilldown: '<?php echo $t->campaign;?>'
                        },
                        <?php }?>
                        ]
                    }],
                    drilldown: {
                        series: [
                        <?php foreach($top_campaigns as $t){ ?>
                            {
                                name: '<?php echo ucwords($t->campaign);?>',
                                id: '<?php echo ucwords($t->campaign);?>',
                                data: [ <?php foreach ($drilldown as $d){
                                    if($t->campaign==$d->campaign){?>
                                        ['<?php echo $d->ambassador?>', <?php echo $d->kudos_count?>],
                                        <?php }}?>
                                        ]
                                    },
                                    <?php }?>
                                    ]
                                }
                            });
        });
    </script>


    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>


    <!-- FORM MASKS-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>
    <script type="text/javascript">
        /* Input masks */

        $(function() { "use strict";
            $(".input-mask").inputmask();
        });

    </script>


    <script type="text/javascript">
        $(window).load(function(){
         setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
     });
 </script>



 <!-- jQueryUI Datepicker -->

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

 <script type="text/javascript">
    $(document).ready(function(){
        $('.datepick').each(function(){
            $(this).datepicker();
        });
    });
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

<!-- Table to Excel -->
<script type="text/javascript" src="<?php echo base_url();?>assets/table_export/jquery.table2excel.js" ></script>

<!-- Parsley -->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>
<script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true,
            "order": [[ 6, "desc" ]]
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>



</head>


<body>
    <div id="sb-site">


        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div id="page-wrapper">
            <div id="page-header" class="bg-black">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                    <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                </div>
                <div id="header-logo" class="logo-bg">
                   <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                   <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id='headerLeft'>
            </div>



        </div>
        <div id="page-sidebar">
            <div class="scroll-sidebar">


                <ul id="sidebar-menu">
                </ul>
            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content">

                <div class="container">

                    <div id="page-title">
                        <h2>List Of Kudos</h2>
                        <hr>
                        <div class="panel">
                            <div class="panel-content">
                                <div class="example-box-wrapper">
                                    <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Ambassador Name</th>
                                                <th>Campaign</th>
                                                <th>Kudos Type</th>
                                                <th>Proofreading</Ath>
                                                    <th>Kudos Card</th>
                                                    <th>Reward Status</th>
                                                    <th>Date Added</th>
                                                    <th>Action</th>

                                                </tr>
                                            </thead>

                                            <tfoot>
                                                <tr>

                                                    <th><b>Ambassador Name</b></th>
                                                    <th><b>Campaign</b></th>
                                                    <th><b>Kudos Type</b></th>
                                                    <th><b>Proofreading</b></th>
                                                    <th><b>Kudos Card</b></th>
                                                    <th><b>Reward Status</b></th>
                                                    <th><b>Date Added</b></th>
                                                    <th><b>Action</b></th>

                                                </tr>
                                            </tfoot>

                                            <tbody>
                                                <?php foreach($kudos as $k => $val){ ?>
                                                <tr>
                                                    <td><?php echo $val->ambassador; ?></td>
                                                    <td><?php echo $val->campaign; ?></td>
                                                    <td><?php echo $val->kudos_type; ?></td>
                                                    <td><?php if($val->proofreading=='Pending'){ ?>
                                                        <p style="color: red;""><?php echo $val->proofreading; ?></p>
                                                        <?php }?>
                                                        <?php if($val->proofreading=='Done'){ ?>
                                                        <p style="color: green;""><?php echo $val->proofreading; ?></p>
                                                        <?php }?>
                                                        <?php if($val->proofreading=='In Progress'){ ?>
                                                        <p style="color: #f29341;""><?php echo $val->proofreading; ?></p>
                                                        <?php }?>
                                                    </td>
                                                    <td><?php if($val->kudos_card=='Pending'){ ?>
                                                        <p style="color: red;""><?php echo $val->kudos_card; ?></p>
                                                        <?php }?>
                                                        <?php if($val->kudos_card=='Done'){ ?>
                                                        <p style="color: green;""><?php echo $val->kudos_card; ?></p>
                                                        <?php }?>
                                                        <?php if($val->kudos_card=='In Progress'){ ?>
                                                        <p style="color: #f29341;""><?php echo $val->kudos_card; ?></p>
                                                        <?php }?>
                                                    </td>
                                                    <td><?php if($val->reward_status=='Pending'){ ?>
                                                        <p style="color: red;""><?php echo $val->reward_status; ?></p>
                                                        <?php }?>
                                                        <?php if($val->reward_status=='Requested'){ ?>
                                                        <p style="color: #f29341;""><?php echo $val->reward_status; ?></p>
                                                        <?php }?>
                                                        <?php if($val->reward_status=='Released'){ ?>
                                                        <p style="color: green;""><?php echo $val->reward_status; ?></p>
                                                        <?php }?>
                                                        <?php if($val->reward_status=='Approved'){ ?>
                                                        <p style="color: #41b2f4;""><?php echo $val->reward_status; ?></p>
                                                        <?php }?>
                                                    </td>
                                                    <td><?php echo $val->date_given; ?></td>
                                                    <td style="text-align: center;">
                                                        <button style="width: 6em;" id="updateButton" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModalUpdate<?php echo $val->kudos_id; ?>">Update</button>
                                                    </td>


                                                </tr>

                                                <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/pretty-photo/prettyphoto.css">
                                                <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/pretty-photo/prettyphoto.js"></script>
                                                <script type="text/javascript">
                                                    /* PrettyPhoto */

                                                    $(document).ready(function() {
                                                        $(".prettyphoto").prettyPhoto();
                                                    });
                                                </script>
                                                <div id="myModalUpdate<?php echo $val->kudos_id; ?>" class="modal fade" role="dialog" style="height: auto !important;">
                                                    <div class="modal-dialog" style="width: 500px; height: auto !important;">

                                                        <div class="modal-content">
                                                            <div class="modal-header" style="background: #1f2e2e; color: white;">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <?php if($val->kudos_type=="Call"){?>
                                                                <h3 class="modal-title float-left"><img src="<?php echo base_url();?>assets/images/phone-icon.png" width="40" height="40">    Call Kudos</h3>
                                                                <?php }?>
                                                                <?php if($val->kudos_type=="Email"){?>
                                                                <h3 class="modal-title float-left"><img src="<?php echo base_url();?>assets/images/email-icon.png" width="40" height="40">    Email Kudos</h3>
                                                                <?php }?>

                                                                <h3 class="modal-title float-right" style="position: relative; top:5px; right: 20px;"><?php echo $val->date_given; ?></h3>
                                                            </div>
                                                            <div class="modal-body">

                                                                <div id="AddUserOption">  

                                                                    <div id="DivAddSingleUser"  >
                                                                        <form class="form-horizontal" data-parsley-validate id="updateForm<?php echo $val->kudos_id; ?>"> 
                                                                            <div class="example-box-wrapper">


                                                                                <div style="padding: 0; margin: 0; height: auto; overflow: auto; border: 1px solid black; border-radius: 5px; <?php if($val->kudos_type=="Call"){ echo "display: none;"; }?>" >
                                                                                    <div style="background-color: #1f2e2e; height: auto; overflow: auto; border-bottom: 1px solid black; padding: 5px;">
                                                                                        <h4 style="text-align: center; color: white;">From: <?php echo $val->client_name;?> (<?php echo $val->client_email?>)</h4>
                                                                                    </div>
                                                                                    <div align="center" style="padding: 5px; margin: 5px;">
                                                                                        <a href="<?php echo base_url();?><?php echo $val->file;?>" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="">
                                                                                            <img width="445px" height="250px" src="<?php echo base_url();?><?php echo $val->file;?>">
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <?php if (strlen($val->revision)!=0){ ?>
                                                                                <?php if($val->kudos_type=="Email"){ echo"<br>"; }?>

                                                                                <div style="padding: 0; margin: 0; height: auto; overflow: auto; border: 1px solid black; border-radius: 5px; <?php if($val->kudos_type=="Call"){ echo "display: none;"; }?>" >
                                                                                    <div style="background-color: #1f2e2e; height: auto; overflow: auto; border-bottom: 1px solid black; padding: 5px;">
                                                                                        <h4 style="text-align: center; color: white;">Proofread</h4>
                                                                                    </div>
                                                                                    <div align="center" style="padding: 5px; margin: 5px;">
                                                                                        <h4>&ldquo; <i><?php echo $val->revision;?></i> &rdquo;</h4><br>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group" style="height: auto; overflow: auto; <?php if($val->kudos_type=="Call"){ echo "display: none;"; }?>">
                                                                                    <h6 class="float-left">Edited by <?php echo $val->revised_by;?> last <?php echo $val->date_revised;?></h6>

                                                                                </div>
                                                                                <?php } ?>

                                                                                <?php if (strlen($val->revision)!=0){ ?>
                                                                                <div style="padding: 0; margin: 0; height: auto; overflow: auto; border: 1px solid black; border-radius: 5px; <?php if($val->kudos_type=="Email"){ echo "display: none;"; }?>" >
                                                                                    <div style="background-color: #1f2e2e; height: auto; overflow: auto; border-bottom: 1px solid black; padding: 5px;">
                                                                                        <h4 style="text-align: center; color: white;">Comment</h4>
                                                                                    </div>
                                                                                    <div align="center" style="padding: 5px; margin: 5px;">

                                                                                        <h4>&ldquo; <i><?php echo $val->revision;?></i> &rdquo;</h4><br>


                                                                                        <h4>- <b><?php echo $val->client_name;?></b>, <i><?php echo $val->phone_number;?></i></h4>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group" style="height: auto; overflow: auto; <?php if($val->kudos_type=="Email"){ echo "display: none;"; }?>">
                                                                                    <h6 class="float-left">Edited by <?php echo $val->revised_by;?> last <?php echo $val->date_revised;?></h6>

                                                                                </div>
                                                                                <?php } else { ?>
                                                                                <div style="padding: 0; margin: 0; height: auto; overflow: auto; border: 1px solid black; border-radius: 5px; <?php if($val->kudos_type=="Email"){ echo "display: none;"; }?>" >
                                                                                    <div style="background-color: #1f2e2e; height: auto; overflow: auto; border-bottom: 1px solid black; padding: 5px;">
                                                                                        <h4 style="text-align: center; color: white;"><b>Comment</b></h4>
                                                                                    </div>
                                                                                    <div align="center" style="padding: 5px; margin: 5px;">


                                                                                        <h4>&ldquo; <i><?php echo $val->comment;?></i> &rdquo;</h4><br>

                                                                                        <h4>- <b><?php echo $val->client_name;?></b>, <i><?php echo $val->phone_number;?></i></h4>
                                                                                    </div>
                                                                                </div>
                                                                                <?php }?>

                                                                                <br>
                                                                                <div class="form-group" style="display: none; height: auto; overflow: auto;">
                                                                                    <label class="col-sm-3 control-label float-left">Supervisor</label>
                                                                                    <div class="col-sm-9">
                                                                                        <input type="text" value="<?php echo $val->uid; ?>" class="form-control" id="addSupervisor<?php echo $val->kudos_id; ?>" >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group" style="height: auto; overflow: auto;">
                                                                                    <h4 class="col-sm-4 control-label float-left"><b>Campaign</b></h4>
                                                                                    <div class="col-sm-8">
                                                                                        <h4><?php echo $val->campaign;?></h4>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group" style="height: auto; overflow: auto;">
                                                                                    <h4 class="col-sm-4 control-label float-left"><b>Ambassador</b></h4>
                                                                                    <div class="col-sm-8">
                                                                                        <h4><?php echo $val->ambassador;?></h4>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group" style="height: auto; overflow: auto;">
                                                                                    <h4 class="col-sm-4 control-label float-left"><b>Reward</b></h4>
                                                                                    <div class="col-sm-8">
                                                                                        <h4><?php echo $val->reward_type;?></h4>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group" style="height: auto; overflow: auto;">
                                                                                    <h4 class="col-sm-4 control-label float-left"><b>Proofreading</b></h4>
                                                                                    <div class="col-sm-8">
                                                                                        <h4><?php echo $val->proofreading;?></h4>
                                                                                    </div>
                                                                                </div>
                                                                                <?php if($val->kudos_card_pic){ ?>
                                                                                <div class="form-group" style="height: auto; overflow: auto;">
                                                                                    <h4 class="col-sm-4 control-label float-left"><b>Kudos Card Status</b></h4>
                                                                                    <div class="col-sm-8">
                                                                                        <h4><?php echo $val->proofreading;?></h4>
                                                                                    </div>
                                                                                </div>
                                                                                <?php } ?>
                                                                                <div class="form-group" style="height: auto; overflow: auto;<?php if($val->kudos_card_pic){ echo 'display: none;'; } ?>">
                                                                                    <h4 class="col-sm-4 control-label float-left"><b>Kudos Card Status</b></h4>
                                                                                    <div class="col-sm-8">
                                                                                        <select class="form-control" id="addKudosCard<?php echo $val->kudos_id; ?>" required="true" data-trigger="change" <?php if($val->proofreading!="Done"){ echo "disabled";}?>>
                                                                                            <option value="">-- Select Kudos Card Status --</option>
                                                                                            <option
                                                                                            <?php
                                                                                            if ($val->kudos_card=='Pending'){
                                                                                                echo 'selected="selected"';
                                                                                            } 
                                                                                            ?>
                                                                                            >Pending</option>
                                                                                            <option
                                                                                            <?php
                                                                                            if ($val->kudos_card=='In Progress'){
                                                                                                echo 'selected="selected"';
                                                                                            } 
                                                                                            ?>
                                                                                            >In Progress</option>
                                                                                            <option
                                                                                            <?php
                                                                                            if ($val->kudos_card=='Done'){
                                                                                                echo 'selected="selected"';
                                                                                            } 
                                                                                            ?>
                                                                                            >Done</option>
                                                                                        </select>

                                                                                        <?php if($val->proofreading!="Done"){?>
                                                                                        <br>
                                                                                        <span id="span_kudosCard<?php echo $val->kudos_id; ?>" style="color: red;">Proofreading is required.</span>
                                                                                        <?php } ?>
                                                                                        <br>
                                                                                        <span id="span_span_kudosCardRequired<?php echo $val->kudos_id; ?>" style="color: red; display: none;">Kudos Card Status is required.</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group" style="height: auto; overflow: auto; display: none;" id="kudosPic<?php echo $val->kudos_id; ?>">
                                                                                    <h4 class="col-sm-4 control-label float-left"><b>Kudos Card Picture</b></h4>
                                                                                    <div class="col-sm-8">
                                                                                        <input type="file" class="form-control" name="kudosCardPicture" id="kudosCardPicture<?php echo $val->kudos_id; ?>" onchange="readURL(this);">
                                                                                        <span id="span_kudosPic<?php echo $val->kudos_id; ?>" style="color: red; display: none;">Kudos Card Picture is required.</span>
                                                                                    </div>
                                                                                </div>
                                                                                <?php if($val->kudos_card_pic) {?>
                                                                                <div class="form-group" style="height: auto; overflow: auto;" id="kudosPic<?php echo $val->kudos_id; ?>">
                                                                                    <h4 class="col-sm-5 control-label float-left"><b>Kudos Card Picture</b></h4>
                                                                                    <div class="col-sm-7">
                                                                                        <a href="<?php echo base_url();?><?php echo $val->kudos_card_pic;?>" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="">
                                                                                            <img src="<?php echo base_url();?><?php echo $val->kudos_card_pic;?>" style="width: 250px; height: 100px;">
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                                <?php }?>

                                                                                <div class="form-group" style="height: auto; overflow: auto;">
                                                                                    <h4 class="col-sm-4 control-label float-left"><b>Reward Status</b></h4>
                                                                                    <div class="col-sm-8">
                                                                                        <select class="form-control" id="addRewardStatus<?php echo $val->kudos_id; ?>" required="true" data-trigger="change" <?php if($val->proofreading!="Done"){ echo "disabled";}?>>
                                                                                            <option value="" >-- Select Reward Status --</option>
                                                                                            <option
                                                                                            <?php
                                                                                            if ($val->reward_status=='Pending'){
                                                                                                echo 'selected="selected"';
                                                                                            }
                                                                                            if ($val->reward_status=='Approved' || $val->reward_status=='Released'){
                                                                                                echo 'disabled';
                                                                                            }  
                                                                                            ?>
                                                                                            >Pending</option>
                                                                                            <?php if ($val->reward_type=="Cash"){?>
                                                                                            <option
                                                                                            <?php
                                                                                            if ($val->reward_status=='Requested'){
                                                                                                echo 'selected="selected"';
                                                                                            }
                                                                                            if ($val->reward_status=='Approved' || $val->reward_status=='Released'){
                                                                                                echo 'disabled';
                                                                                            } 
                                                                                            ?>
                                                                                            >Requested</option>
                                                                                            <?php if ($val->reward_status=="Approved"){ ?>
                                                                                            <option
                                                                                            <?php
                                                                                            if ($val->reward_status=='Approved'){
                                                                                                echo 'selected="selected" ';
                                                                                            } 
                                                                                            ?>
                                                                                            >Approved</option>
                                                                                            <?php } ?>
                                                                                            <?php if ($val->reward_status=="Released"){ ?>
                                                                                            <option
                                                                                            <?php
                                                                                            if ($val->reward_status=='Released'){
                                                                                                echo 'selected="selected"';
                                                                                            } 
                                                                                            ?>
                                                                                            >Released</option>
                                                                                            <?php } ?>
                                                                                            <!--
                                                                                            <option
                                                                                            <?php
                                                                                            if ($val->reward_status=='Approved'){
                                                                                                echo 'selected="selected"';
                                                                                            } 
                                                                                            ?>
                                                                                            >Approved</option>
                                                                                            <?php }?>
                                                                                            <?php if ($val->reward_type=="Credit"){?>
                                                                                            <option
                                                                                            <?php
                                                                                            if ($val->reward_status=='Released'){
                                                                                                echo 'selected="selected"';
                                                                                            } 
                                                                                            ?>
                                                                                            >Released</option>
                                                                                            <?php }?>-->
                                                                                        </select>
                                                                                        
                                                                                        <?php if($val->proofreading!="Done"){?>
                                                                                        <br>
                                                                                        <span id="span_rewardStatus<?php echo $val->kudos_id; ?>" style="color: red;">Proofreading is required.</span>
                                                                                        <?php } ?>
                                                                                        <br>
                                                                                        <span id="span_rewardStatusRequired<?php echo $val->kudos_id; ?>" style="color: red; display: none;">Reward Status is required.</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group" style="height: auto; overflow: auto; <?php if($val->reward_status!="Requested"){ echo "display: none;"; }?>" id="requestNoteDiv<?php echo $val->kudos_id; ?>">
                                                                                    <h4 class="col-sm-4 control-label float-left"><b>Request Note</b></h4>
                                                                                    <div class="col-sm-8">

                                                                                        <textarea rows="4" type="text" class="form-control" id="requestNote<?php echo $val->kudos_id; ?>" placeholder="Enter Request Note" style="margin: 1px 0px;" required="" data-parsley-range="[10,250]"><?php echo $val->request_note;?></textarea>

                                                                                        <?php if($val->proofreading!="Done"){?>
                                                                                        <br>
                                                                                        <span id="span_rewardStatus<?php echo $val->kudos_id; ?>" style="color: red;">Proofreading is required.</span>
                                                                                        <?php } ?>

                                                                                        <span id="span_note<?php echo $val->kudos_id; ?>" style="color: red; display: none;">Request note is required.</span>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                
                                                                                <div class="form-group" style="height: auto; overflow: auto;">

                                                                                    <div align="center">
                                                                                        <button class="btn btn-s btn-warning float-center" id='BtnBackAddMenu2' class="close" data-dismiss="modal"> Cancel</button>

                                                                                        <button type="button" class="btn btn-s btn-success float-center" id='kudosUpdate<?php echo $val->kudos_id; ?>' style="display: none;"> Update</button>

                                                                                        <button type="button" class="btn btn-s btn-success float-center" id='myModalUpdates<?php echo $val->kudos_id; ?>'<?php if($val->proofreading!="Done"){ echo "disabled";}?>> Update</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script>
                                                    function readURL(input) {
                                                        if (input.files && input.files[0]) {
                                                            var reader = new FileReader();

                                                            reader.onload = function (e) {
                                                                $('#picture<?php echo $val->kudos_id; ?>')
                                                                .attr('src', e.target.result)
                                                                    //.height('60%');
                                                                };

                                                                reader.readAsDataURL(input.files[0]);
                                                            }
                                                        }
                                                        $(function(){ 
                                                            $("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false,});

                                                            $('#myModalUpdates<?php echo $val->kudos_id; ?>').click(function(event){
                                                                if ($("#addRewardStatus<?php echo $val->kudos_id; ?>").val()!="Requested"){

                                                                    if ($("#addKudosCard<?php echo $val->kudos_id; ?>").val()=="" || $("#addRewardStatus<?php echo $val->kudos_id; ?>").val()==""){

                                                                        if($("#addKudosCard<?php echo $val->kudos_id; ?>").val()==""){
                                                                            $("#addKudosCard<?php echo $val->kudos_id; ?>").css("border", "1px solid red");

                                                                            $("#span_span_kudosCardRequired<?php echo $val->kudos_id; ?>").show();
                                                                        }

                                                                        if($("#addRewardStatus<?php echo $val->kudos_id; ?>").val()==""){
                                                                            $("#addRewardStatus<?php echo $val->kudos_id; ?>").css("border", "1px solid red");

                                                                            $("#span_rewardStatusRequired<?php echo $val->kudos_id; ?>").show();
                                                                        }
                                                                        event.preventDefault();
                                                                    } else {
                                                                        var data = new FormData();
                                                                        data.append("emp_id",<?php echo $val->emp_id; ?>);
                                                                        data.append("campaign",<?php echo $val->acc_id; ?>);
                                                                        data.append("k_type","<?php echo $val->kudos_type; ?>");
                                                                        data.append("c_name","<?php echo $val->client_name; ?>");
                                                                        data.append("p_number","<?php echo $val->phone_number; ?>");
                                                                        data.append("e_add","<?php echo $val->client_email; ?>");
                                                                        data.append("comment","<?php echo $val->comment; ?>");
                                                                        data.append("supervisor",$("#addSupervisor<?php echo $val->kudos_id; ?>").val());
                                                                        data.append("file_sc","<?php echo $val->file;?>");
                                                                        data.append("p_reward","<?php echo $val->reward_type; ?>");
                                                                        data.append("pfrd","<?php echo $val->proofreading; ?>");
                                                                        data.append("k_card", $("#addKudosCard<?php echo $val->kudos_id; ?>").val());
                                                                        data.append("r_status",$("#addRewardStatus<?php echo $val->kudos_id; ?>").val());
                                                                        data.append("r_note",$("#requestNote<?php echo $val->kudos_id; ?>").val());
                                                                        data.append("r_by", "<?php echo $_SESSION['fname'];?><?php echo " ";echo $_SESSION['lname'];?>");
                                                                        data.append("k_id", <?php echo $val->kudos_id; ?>);

                                                                        $.ajax({
                                                                            type: "POST",
                                                                            url: "<?php echo base_url(); ?>index.php/Kudos/updateKudosNoPic/",
                                                                            data: data,
                                                                            cache: false,
                                                                            processData:false,
                                                                            contentType:false,
                                                                            success: function(html)
                                                                            {

                                                                        //alert("Successfully Updated!");
                                                                        alert("Successfully Updated!");
                                                                        location.reload();
                                                                    }
                                                                });
                                                                    }
                                                                    event.preventDefault();
                                                                } else {
                                                                    if ($("#addKudosCard<?php echo $val->kudos_id; ?>").val()=="" || $("#addRewardStatus<?php echo $val->kudos_id; ?>").val()=="" || $("#requestNote<?php echo $val->kudos_id; ?>").val()==""){

                                                                        if($("#addKudosCard<?php echo $val->kudos_id; ?>").val()==""){
                                                                            $("#addKudosCard<?php echo $val->kudos_id; ?>").css("border", "1px solid red");

                                                                            $("#span_span_kudosCardRequired<?php echo $val->kudos_id; ?>").show();
                                                                        }

                                                                        if($("#addRewardStatus<?php echo $val->kudos_id; ?>").val()==""){
                                                                            $("#addRewardStatus<?php echo $val->kudos_id; ?>").css("border", "1px solid red");

                                                                            $("#span_rewardStatusRequired<?php echo $val->kudos_id; ?>").show();
                                                                        }

                                                                        if($("#requestNote<?php echo $val->kudos_id; ?>").val()==""){
                                                                            $("#requestNote<?php echo $val->kudos_id; ?>").css("border", "1px solid red");

                                                                            $("#span_note<?php echo $val->kudos_id; ?>").show();
                                                                        }

                                                                        event.preventDefault();
                                                                    } else {
                                                                        var data = new FormData();
                                                                        data.append("emp_id",<?php echo $val->emp_id; ?>);
                                                                        data.append("campaign",<?php echo $val->acc_id; ?>);
                                                                        data.append("k_type","<?php echo $val->kudos_type; ?>");
                                                                        data.append("c_name","<?php echo $val->client_name; ?>");
                                                                        data.append("p_number","<?php echo $val->phone_number; ?>");
                                                                        data.append("e_add","<?php echo $val->client_email; ?>");
                                                                        data.append("comment","<?php echo $val->comment; ?>");
                                                                        data.append("supervisor",$("#addSupervisor<?php echo $val->kudos_id; ?>").val());
                                                                        data.append("file_sc","<?php echo $val->file;?>");
                                                                        data.append("p_reward","<?php echo $val->reward_type; ?>");
                                                                        data.append("pfrd","<?php echo $val->proofreading; ?>");
                                                                        data.append("k_card", $("#addKudosCard<?php echo $val->kudos_id; ?>").val());
                                                                        data.append("r_status",$("#addRewardStatus<?php echo $val->kudos_id; ?>").val());
                                                                        data.append("r_note",$("#requestNote<?php echo $val->kudos_id; ?>").val());
                                                                        data.append("r_by", "<?php echo $_SESSION['fname'];?><?php echo " ";echo $_SESSION['lname'];?>");
                                                                        data.append("k_id", <?php echo $val->kudos_id; ?>);

                                                                        $.ajax({
                                                                            type: "POST",
                                                                            url: "<?php echo base_url(); ?>index.php/Kudos/updateKudosNoPic/",
                                                                            data: data,
                                                                            cache: false,
                                                                            processData:false,
                                                                            contentType:false,
                                                                            success: function(html)
                                                                            {

                                                                        //alert("Successfully Updated!");
                                                                        alert("Successfully Updated!");
                                                                        location.reload();
                                                                    }
                                                                });
                                                                    }
                                                                    event.preventDefault();
                                                                }

                                                            }); 

$('#kudosUpdate<?php echo $val->kudos_id; ?>').click(function(event){
    if ($("#addRewardStatus<?php echo $val->kudos_id; ?>").val()!="Requested"){

        if ($("#addKudosCard<?php echo $val->kudos_id; ?>").val()=="" || $("#addRewardStatus<?php echo $val->kudos_id; ?>").val()=="" || $("#kudosCardPicture<?php echo $val->kudos_id; ?>").val()==""){

            if($("#addKudosCard<?php echo $val->kudos_id; ?>").val()==""){
                $("#addKudosCard<?php echo $val->kudos_id; ?>").css("border", "1px solid red");

                $("#span_span_kudosCardRequired<?php echo $val->kudos_id; ?>").show();
            }

            if($("#addRewardStatus<?php echo $val->kudos_id; ?>").val()==""){
                $("#addRewardStatus<?php echo $val->kudos_id; ?>").css("border", "1px solid red");

                $("#span_rewardStatusRequired<?php echo $val->kudos_id; ?>").show();
            }

            if($("#kudosCardPicture<?php echo $val->kudos_id; ?>").val()==""){
                $("#kudosCardPicture<?php echo $val->kudos_id; ?>").css("border", "1px solid red");

                $("#span_kudosPic<?php echo $val->kudos_id; ?>").show();
            }
            event.preventDefault();
        } else {

            var data = new FormData();
            $.each($("#kudosCardPicture<?php echo $val->kudos_id; ?>")[0].files,function(i,file){
                data.append("kudosCardPicture<?php echo $val->kudos_id; ?>",file);
            });
                                                                //data.append('file', $('#addFile')[0].files[0]);
                                                                data.append("emp_id",<?php echo $val->emp_id; ?>);
                                                                data.append("campaign",<?php echo $val->acc_id; ?>);
                                                                data.append("k_type",'<?php echo $val->kudos_type; ?>');
                                                                data.append("c_name",'<?php echo $val->client_name; ?>');
                                                                data.append("p_number",'<?php echo $val->phone_number; ?>');
                                                                data.append("e_add",'<?php echo $val->client_email; ?>');
                                                                data.append("comment","<?php echo $val->comment; ?>");
                                                                data.append("supervisor",$("#addSupervisor<?php echo $val->kudos_id; ?>").val());
                                                                //data.append("file",$('#addKudosType').val());
                                                                data.append("p_reward",'<?php echo $val->reward_type; ?>');
                                                                data.append("pfrd",'<?php echo $val->proofreading; ?>');
                                                                data.append("k_card", $("#addKudosCard<?php echo $val->kudos_id; ?>").val());
                                                                data.append("r_status",$("#addRewardStatus<?php echo $val->kudos_id; ?>").val());
                                                                data.append("r_note",$("#requestNote<?php echo $val->kudos_id; ?>").val());
                                                                data.append("r_by", "<?php echo $_SESSION['fname'];?><?php echo " ";echo $_SESSION['lname'];?>");
                                                                data.append("k_id", <?php echo $val->kudos_id; ?>);

                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: "<?php echo base_url(); ?>index.php/Kudos/updateKudos/",
                                                                    data: data,
                                                                    cache: false,
                                                                    processData:false,
                                                                    contentType:false,
                                                                    success: function(response)
                                                                    {

                                                                        if(response=="no"){
                                                                            alert("File type not allowed.");
                                                                        } else {
                                                                            alert("Successfully updated.");
                                                                            location.reload();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            event.preventDefault();
                                                        } else {
                                                            if ($("#addKudosCard<?php echo $val->kudos_id; ?>").val()=="" || $("#addRewardStatus<?php echo $val->kudos_id; ?>").val()=="" || $("#requestNote<?php echo $val->kudos_id; ?>").val()=="" || $("#kudosCardPicture<?php echo $val->kudos_id; ?>").val()==""){

                                                                if($("#addKudosCard<?php echo $val->kudos_id; ?>").val()==""){
                                                                    $("#addKudosCard<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                                    
                                                                    $("#span_span_kudosCardRequired<?php echo $val->kudos_id; ?>").show();
                                                                }

                                                                if($("#addRewardStatus<?php echo $val->kudos_id; ?>").val()==""){
                                                                    $("#addRewardStatus<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                                    
                                                                    $("#span_rewardStatusRequired<?php echo $val->kudos_id; ?>").show();
                                                                }

                                                                if($("#requestNote<?php echo $val->kudos_id; ?>").val()==""){
                                                                    $("#requestNote<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                                    
                                                                    $("#span_note<?php echo $val->kudos_id; ?>").show();
                                                                }

                                                                if($("#kudosCardPicture<?php echo $val->kudos_id; ?>").val()==""){
                                                                    $("#kudosCardPicture<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                                    
                                                                    $("#span_kudosPic<?php echo $val->kudos_id; ?>").show();
                                                                }

                                                                event.preventDefault();
                                                            } else {
                                                                var data = new FormData();
                                                                $.each($("#kudosCardPicture<?php echo $val->kudos_id; ?>")[0].files,function(i,file){
                                                                    data.append("kudosCardPicture<?php echo $val->kudos_id; ?>",file);
                                                                });
                                                                //data.append('file', $('#addFile')[0].files[0]);
                                                                data.append("emp_id",<?php echo $val->emp_id; ?>);
                                                                data.append("campaign",<?php echo $val->acc_id; ?>);
                                                                data.append("k_type",'<?php echo $val->kudos_type; ?>');
                                                                data.append("c_name",'<?php echo $val->client_name; ?>');
                                                                data.append("p_number",'<?php echo $val->phone_number; ?>');
                                                                data.append("e_add",'<?php echo $val->client_email; ?>');
                                                                data.append("comment","<?php echo $val->comment; ?>");
                                                                data.append("supervisor",$("#addSupervisor<?php echo $val->kudos_id; ?>").val());
                                                                //data.append("file",$('#addKudosType').val());
                                                                data.append("p_reward",'<?php echo $val->reward_type; ?>');
                                                                data.append("pfrd",'<?php echo $val->proofreading; ?>');
                                                                data.append("k_card", $("#addKudosCard<?php echo $val->kudos_id; ?>").val());
                                                                data.append("r_status",$("#addRewardStatus<?php echo $val->kudos_id; ?>").val());
                                                                data.append("r_note",$("#requestNote<?php echo $val->kudos_id; ?>").val());
                                                                data.append("r_by", "<?php echo $_SESSION['fname'];?><?php echo " ";echo $_SESSION['lname'];?>");
                                                                data.append("k_id", <?php echo $val->kudos_id; ?>);

                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: "<?php echo base_url(); ?>index.php/Kudos/updateKudos/",
                                                                    data: data,
                                                                    cache: false,
                                                                    processData:false,
                                                                    contentType:false,
                                                                    success: function(response)
                                                                    {

                                                                        if(response=="no"){
                                                                            alert("File type not allowed.");
                                                                        } else {
                                                                            alert("Successfully updated.");
                                                                            location.reload();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            event.preventDefault();
                                                        }

                                                    }); 

$('#addCampaign<?php echo $val->kudos_id; ?>').on('change', function(){
    $.ajax({
        type : 'POST',
        data : 'campaign_id='+ $('#addCampaign<?php echo $val->kudos_id; ?>').val(),
        url : "<?php echo base_url(); ?>index.php/Kudos/getAgents/",
        success : function(data){
                                                                //data returns your name, iterate through it and add the name to another select
                                                                alert("Successfully Updated!");
                                                                console.log(data);
                                                            }
                                                        });
});

                                                    /*if($('#kudosCardPicture<?php echo $val->kudos_id; ?>').val()=="") {
                                                        $("#kudosCardPicture<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        $("#span_kudosPic<?php echo $val->kudos_id; ?>").show();
                                                    } else {
                                                            $("#kudosCardPicture<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
                                                            $("#span_note<?php echo $val->kudos_id; ?>").hide();
                                                        }*/

                                                        $('#requestNote<?php echo $val->kudos_id; ?>').on('keyup', function() {
                                                           if($("#requestNote<?php echo $val->kudos_id; ?>").val()==""){
                                                            $("#requestNote<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                            $("#span_note<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        } else {
                                                            $("#requestNote<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
                                                            $("#span_note<?php echo $val->kudos_id; ?>").text("");
                                                        }
                                                    });

                                                        $('#addProofreading<?php echo $val->kudos_id; ?>').on('change', function(){
                                                            if($("#addProofreading<?php echo $val->kudos_id; ?>").val()==""){
                                                                $("#addProofreading<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                                $("#span_proofreading<?php echo $val->kudos_id; ?>").text("This is required.");
                                                            } else {
                                                                $("#addProofreading<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
                                                                $("#span_proofreading<?php echo $val->kudos_id; ?>").text("");
                                                            }
                                                        });

                                                        $('#addKudosCard<?php echo $val->kudos_id; ?>').on('change', function(){
                                                            if($("#addKudosCard<?php echo $val->kudos_id; ?>").val()==""){
                                                                $("#addKudosCard<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                                $("#span_span_kudosCardRequired<?php echo $val->kudos_id; ?>").show();
                                                            } else {
                                                                $("#addKudosCard<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
                                                                $("#span_span_kudosCardRequired<?php echo $val->kudos_id; ?>").hide();
                                                            }
                                                            if($("#addKudosCard<?php echo $val->kudos_id; ?>").val()=="Done"){

                                                                $("#kudosPic<?php echo $val->kudos_id; ?>").show();
                                                                $("#Picture<?php echo $val->kudos_id; ?>").show();
                                                                $("#kudosUpdate<?php echo $val->kudos_id; ?>").show();
                                                                $("#myModalUpdates<?php echo $val->kudos_id; ?>").hide();
                                                            } else {
                                                                $("#Picture<?php echo $val->kudos_id; ?>").hide();
                                                                $("#kudosPic<?php echo $val->kudos_id; ?>").hide();
                                                                $("#kudosUpdate<?php echo $val->kudos_id; ?>").hide();
                                                                $("#myModalUpdates<?php echo $val->kudos_id; ?>").show();
                                                            }

                                                        });

                                                        $('#addRewardStatus<?php echo $val->kudos_id; ?>').on('change', function(){
                                                            if($("#addRewardStatus<?php echo $val->kudos_id; ?>").val()==""){
                                                                $("#addRewardStatus<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                                $("#span_rewardStatusRequired<?php echo $val->kudos_id; ?>").show();
                                                            } else {
                                                                $("#addRewardStatus<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
                                                                $("#span_rewardStatusRequired<?php echo $val->kudos_id; ?>").hide();
                                                            }
                                                            if($("#addRewardStatus<?php echo $val->kudos_id; ?>").val()=="Requested"){

                                                                $("#requestNoteDiv<?php echo $val->kudos_id; ?>").show();
                                                            } else {
                                                                $("#requestNoteDiv<?php echo $val->kudos_id; ?>").hide();
                                                            }
                                                        });
                                                    });


                                                </script>
                                                <?php }?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel" style="display: none;">
                            <div class="panel-body">
                                <div class="col-md-6">

                                    <div id="chart_1"></div>
                                    <br>
                                    <div class="form-group">
                                        <div>
                                            <div class="clearfix row">
                                                <label for="" class="col-sm-3 control-label">Date Range Picker</label>
                                                <div class="col-sm-3">
                                                    <input type="text" name="fromDate" id="fromDate" placeholder="From date..." class="float-left form-control datepick" required>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="toDate" id="toDate" placeholder="To date..." class="float-left form-control datepick" required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input class="btn btn-primary" type="button" name="datePicked" id="datePicked" value="Generate" style="background-color: #3498db; border-color: #3498db;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="chart_3"></div>
                                    <br>
                                    <div class="form-group">
                                        <div>
                                            <div class="clearfix row">
                                                <label for="" class="col-sm-3 control-label">Date Range Picker</label>
                                                <div class="col-sm-3">
                                                    <input type="text" name="fromDate" id="fromDate3" placeholder="From date..." class="float-left form-control datepick" required>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="toDate" id="toDate3" placeholder="To date..." class="float-left form-control datepick" required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input class="btn btn-primary" type="button" name="datePicked" id="datePicked3" value="Generate" style="background-color: #3498db; border-color: #3498db;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel" style="display: none;">
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div id="chart_2"></div>
                                    <br>

                                </div>
                                <div class="col-md-6">
                                    <div id="chart_4"></div>
                                    <br>

                                </div>
                            </div>
                        </div>
                        <div class="panel" style="display: none;">
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div id="chart_0"></div>
                                    <br>
                                    <div class="form-group">
                                        <div>
                                            <div class="clearfix row">
                                                <div class="col-sm-4">
                                                    <select class="form-control" id="addCampaign2" required="true" data-trigger="change">
                                                        <option selected="selected" value="">Campaign</option>
                                                        <?php 
                                                        foreach($acc as $row)
                                                        {
                                                            echo '<option value="'.$row->acc_id.'">'.$row->acc_name.'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="fromDate" id="fromDate2" placeholder="From date..." class="float-left mrg10R form-control datepick" required>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="toDate" id="toDate2" placeholder="To date..." class="float-left form-control datepick" required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button class="btn btn-primary" id="datePicked2" style="background-color: #3498db; border-color: #3498db;">Generate</button>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                </div>
                            </div>
                        </div>

                        <div class="panel" style="display: none;">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div>
                                        <h4 style="text-align: center;"><b>Generate Table for Kudos Count by Campaign</b></h4><br>
                                        <div class="clearfix row">
                                            <div class="col-sm-4">
                                                <select class="form-control" id="addCampaign4" required="true" data-trigger="change">
                                                    <option selected="selected" value="">Campaign</option>
                                                    <?php 
                                                    foreach($acc as $row)
                                                    {
                                                        echo '<option value="'.$row->acc_id.'">'.$row->acc_name.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="fromDate4" id="fromDate4" placeholder="From date..." class="float-left mrg10R form-control datepick" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="toDate4" id="toDate4" placeholder="To date..." class="float-left form-control datepick" required>
                                            </div>
                                            <div class="col-sm-2">
                                                <button class="btn btn-primary" id="datePicked4" style="background-color: #3498db; border-color: #3498db; width: 100%;">Generate</button>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div id="showTable1" style="display: none;">
                                    <button class="btn btn-success float-left" id="download_report1" style="background-color: #3498db; border-color: #3498db;">Download Excel File</button><br>
                                    <table class="table table-striped" id="table_report1" cellpadding="0" style="width: 100%; height: 100%; font-size: 15px;" >
                                        <thead>
                                            <tr>
                                                <td><b>Month and Year</b></td>
                                                <td><b>Campaign</b></td>
                                                <td><b>Kudos Count</b></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    
                                </div> 
                            </div>
                        </div>

                        <div class="panel" style="display: none;">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div>
                                        <h4 style="text-align: center;"><b>Generate Table for Overall Kudos Count</b></h4><br>
                                        <div align="center">
                                            <div class="col-sm-2" align="center">
                                            </div>
                                            <div class="col-sm-3" align="center">
                                                <input type="text" name="fromDate5" id="fromDate5" placeholder="From date..." class="mrg10R form-control datepick" required>
                                            </div>
                                            <div class="col-sm-3" align="center">
                                                <input type="text" name="toDate5" id="toDate5" placeholder="To date..." class="form-control datepick" required>
                                            </div>
                                            <div class="col-sm-2" align="center">
                                                <button class="btn btn-primary" id="datePicked5" style="background-color: #3498db; border-color: #3498db; width: 100%;">Generate</button>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div id="showTable2" style="display: none;">
                                    <br>
                                    <button class="btn btn-success" id="download_report2" style="background-color: #3498db; border-color: #3498db;">Download Excel File</button><br>
                                    <table class="table table-striped" id="table_report2" cellpadding="0" style="width: 100%; height: 100%; font-size: 15px;" >
                                        <thead>
                                            <tr>
                                                <td><b>Month and Year</b></td>
                                                <td><b>Kudos Count</b></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>                                   
                                </div> 
                            </div>
                        </div>

                        <div class="panel" style="display: none;">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div>
                                        <h4 style="text-align: center;"><b>Generate Table for Overall Kudos Count by Ambassador</b></h4><br>
                                        <div align="center">
                                            <div class="col-sm-2" align="center">
                                            </div>
                                            <div class="col-sm-3" align="center">
                                                <input type="text" name="fromDate6" id="fromDate6" placeholder="From date..." class="mrg10R form-control datepick" required>
                                            </div>
                                            <div class="col-sm-3" align="center">
                                                <input type="text" name="toDate6" id="toDate6" placeholder="To date..." class="form-control datepick" required>
                                            </div>
                                            <div class="col-sm-2" align="center">
                                                <button class="btn btn-primary" id="datePicked6" style="background-color: #3498db; border-color: #3498db; width: 100%;">Generate</button>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div id="showTable3" style="display: none;">
                                    <br>
                                    <button class="btn btn-success float-left" id="download_report3" style="background-color: #3498db; border-color: #3498db;">Download Excel File</button><br>
                                    <table class="table table-striped" id="table_report3" cellpadding="0" style="width: 100%; font-size: 15px;" >
                                        <thead>
                                            <tr>
                                                <td><b>Ambassador</b></td>
                                                <td><b>Campaign</b></td>
                                                <td><b>Kudos Count</b></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>
</div>
</body>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/menu.js"></script>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script>

    $(function(){
        $('#addKudosType').on('change', function(){
            if(this.value=='Call'){
                $('#append').show();
                //hide file and email add
                $('#addFile').addClass('hidden');
                $('#FileAdd').addClass('hidden');
                $('#addEmailAdd').addClass('hidden');
                $('#AddEmailAdd').addClass('hidden');
                //disable file and email add
                $('input[type="file"]').attr("disabled", true);
                $('input[type="email"]').attr("disabled", true);
                //remove comment class hidden and add seen
                $('#addCustomerComment').removeClass('hidden');
                $('#CustomerCommentAdd').removeClass('hidden');
                $('#addCustomerComment').addClass('seen');
                $('#CustomerCommentAdd').addClass('seen');
                //remove disabled attr for comment
                $('#addCustomerComment').attr("disabled", false);
                //remove hidden class in phone number and add seen
                $('#addPhoneNumber').removeClass('hidden');
                $('#PhoneNumberAdd').removeClass('hidden');
                $('#addPhoneNumber').addClass('seen');
                $('#PhoneNumberAdd').addClass('seen');
                $('#addPhoneNumber').attr("disabled", false);
            }
            if(this.value=='Email'){
                $('#append').show();
                $('input[type="file"]').attr("disabled", false);
                $('#addFile').removeClass('hidden');
                $('#FileAdd').removeClass('hidden');
                $('#addFile').addClass('seen');
                $('#FileAdd').addClass('seen');
                $('input[type="email"]').attr("disabled", false);
                $('#addEmailAdd').removeClass('hidden');
                $('#AddEmailAdd').removeClass('hidden');
                $('#addEmailAdd').addClass('seen');
                $('#AddEmailAdd').addClass('seen');
                $('#addCustomerComment').attr("disabled", "disabled");
                $('#addCustomerComment').addClass('hidden');
                $('#CustomerCommentAdd').addClass('hidden');
                $('#addPhoneNumber').attr("disabled", true);
                $('#addPhoneNumber').addClass('hidden');
                $('#PhoneNumberAdd').addClass('hidden');

            }
            if(this.value==''){
                $('#append').hide();
            }
        });
        
        $('#addCampaign').on('change', function(){
            $.ajax({
                type : 'POST',
                data : 'campaign_id='+ $('#addCampaign').val(),
                url : "<?php echo base_url(); ?>index.php/Kudos/getAgents/",
                //dataType: 'json',
                success : function(data){
                    $('#agentNames').html(data);
                    console.log(data);
                }
            });
        });


        $('#addForm').submit(function(event){ 
            var emp_id = $("#agentNames").val();
            var campaign = $("#addCampaign").val();
            var k_type = $("#addKudosType").val();
            var c_name = $("#addCustomerName").val();
            var p_number = $("#addPhoneNumber").val();
            var e_add = $("#addEmailAdd").val();
            var comment = $("#addCustomerComment").val();
            var supervisor = $("#addSupervisor").val();
            var file = $("#addFile").val();
            var p_reward = $("#addPrefReward").val();
            var pfrd = $("#addProofreading").val();
            var k_card = $("#addKudosCard").val();
            var r_status = $("#addRewardStatus").val();

            dataString = "emp_id="+emp_id+"&campaign="+campaign+"&k_type="+k_type+"&c_name="+c_name+"&p_number="+p_number+"&e_add="+e_add+"&comment="+comment+"&supervisor="+supervisor+"&file="+file+"&p_reward="+p_reward+"&pfrd="+pfrd+"&k_card="+k_card+"&r_status="+r_status;

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Kudos/addKudos/",
                data: dataString,
                cache: false,
                success: function(html)
                {
                    alert("Succesfully Added!");
                    location.reload();
                }
            });   
            event.preventDefault();
        });

        $("#datePicked").click(function(){
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();

            var arr_date1 = fromDate.split('-');
            var arr_date2 = toDate.split('-');

            var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            var month1 = arr_date1[1];
            var month2 = arr_date2[1];
            var day1 = arr_date1[2];
            var day2 = arr_date2[2];

            var m1 = month1.replace(/^0+/, '');
            var m2 = month2.replace(/^0+/, '');
            var d1 = day1.replace(/^0+/, '');
            var d2 = day2.replace(/^0+/, '');

            var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
            var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate',
                type: 'POST',
                dataType: 'json',
                data:  dataString,
                success:function(data)
                {
                    var month = data.map(function (value) {
                        return value.month;
                    });
                    var kudos_count = data.map(function (value) {
                        return parseInt(value.kudos_count);
                    });
                    console.log(month);
                    console.log(kudos_count);
                    chart = Highcharts.chart('chart_1', {
                        chart: {
                            inverted: true,
                            polar: false,
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Kudos Count '+fromDate2+' to '+toDate2
                        },

                        xAxis: {
                            categories: month,
                        },

                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                },
                                    //colors: ['#3399ff']
                                }
                            },

                            series: [{
                                type: 'column',
                                colorByPoint: true,
                                data: kudos_count,
                                showInLegend: false,
                            }],


                        });
                    Highcharts.setOptions(Highcharts.theme);
                }
            });
        });

        $("#datePicked2").click(function(){
            var fromDate = $("#fromDate2").val();
            var toDate = $("#toDate2").val();
            var campaign = $("#addCampaign2").val();
            var campaign_name = $("#addCampaign2 option[value='"+campaign+"']").text();

            var arr_date1 = fromDate.split('-');
            var arr_date2 = toDate.split('-');

            var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            var month1 = arr_date1[1];
            var month2 = arr_date2[1];
            var day1 = arr_date1[2];
            var day2 = arr_date2[2];

            var m1 = month1.replace(/^0+/, '');
            var m2 = month2.replace(/^0+/, '');
            var d1 = day1.replace(/^0+/, '');
            var d2 = day2.replace(/^0+/, '');

            var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
            var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

            var dataString = "fromDate="+fromDate+"&toDate="+toDate+"&campaign="+campaign;

                //alert(dataString);

                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/Kudos/getDate2',
                    type: 'POST',
                    dataType: 'json',
                    data:  dataString,
                    success:function(data)
                    {

                        var month = data.map(function (value) {
                            return value.month;
                        });
                        var kudos_count = data.map(function (value) {
                            return parseInt(value.kudos_count);
                        });
                        var c = data.map(function (value) {
                            return parseInt(value.campaign);
                        });

                        console.log(month);
                        console.log(kudos_count);
                        $("#datePicked4").trigger("click");
                        chart = Highcharts.chart('chart_0', {
                            chart: {
                                inverted: true,
                                polar: false,
                            },
                            credits: {
                                enabled: false
                            },
                            title: {
                                text: 'Kudos Count '+fromDate2+' to '+toDate2+' of '+campaign_name
                            },

                            xAxis: {
                                categories: month,
                            },

                            plotOptions: {
                                series: {
                                    dataLabels: {
                                        enabled: true,
                                    },
                                    //colors: ['#3399ff']
                                }
                            },

                            series: [{
                                type: 'column',
                                colorByPoint: true,
                                data: kudos_count,
                                showInLegend: false,
                            }],


                        });
                        Highcharts.setOptions(Highcharts.theme);
                    }
                });
            });

        $("#datePicked3").click(function(){
            var fromDate = $("#fromDate3").val();
            var toDate = $("#toDate3").val();

            var arr_date1 = fromDate.split('-');
            var arr_date2 = toDate.split('-');

            var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            var month1 = arr_date1[1];
            var month2 = arr_date2[1];
            var day1 = arr_date1[2];
            var day2 = arr_date2[2];

            var m1 = month1.replace(/^0+/, '');
            var m2 = month2.replace(/^0+/, '');
            var d1 = day1.replace(/^0+/, '');
            var d2 = day2.replace(/^0+/, '');

            var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
            var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate3',
                type: 'POST',
                dataType: 'json',
                data:  dataString,
                success:function(data)
                {
                    var ambassador = data.map(function (value) {
                        return value.ambassador;
                    });
                    var kudos_count = data.map(function (value) {
                        return parseInt(value.kudos_count);
                    });
                    console.log(ambassador);
                    console.log(kudos_count);
                    chart = Highcharts.chart('chart_3', {
                        chart: {
                            inverted: true,
                            polar: false,
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Kudos Count '+fromDate2+' to '+toDate2
                        },

                        xAxis: {
                            categories: ambassador,
                        },

                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                },
                                    //colors: ['#3399ff']
                                }
                            },

                            series: [{
                                type: 'column',
                                colorByPoint: true,
                                data: kudos_count,
                                showInLegend: false,
                            }],


                        });
                    Highcharts.setOptions(Highcharts.theme);
                }
            });
        });

        $("#datePicked4").click(function(){
            var fromDate = $("#fromDate2").val();
            var toDate = $("#toDate2").val();
            var campaign = $("#addCampaign2").val();
            var campaign_name = $("#addCampaign2 option[value='"+campaign+"']").text();

            var dataString = "fromDate="+fromDate+"&toDate="+toDate+"&campaign="+campaign;

                //alert(dataString);

                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/Kudos/getDate4',
                    type: 'POST',
                    data:  dataString,
                    success:function(data)
                    {
                        $("#table_report1 tbody").html(data);
                        $("#showTable1").show();
                        console.log(data);
                        //alert(JSON.stringify(data));
                    }
                });
            });

        $("#datePicked5").click(function(){
            var fromDate = $("#fromDate5").val();
            var toDate = $("#toDate5").val();

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate5',
                type: 'POST',
                data:  dataString,
                success:function(data)
                {
                    $("#table_report2 tbody").html(data);
                    $("#showTable2").show();
                }
            });
        });

        $("#datePicked6").click(function(){
            var fromDate = $("#fromDate6").val();
            var toDate = $("#toDate6").val();

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate6',
                type: 'POST',
                data:  dataString,
                success:function(data)
                {
                    $("#table_report3 tbody").html(data);
                    $("#showTable3").show();
                }
            });
        });

        
        $('#BtnBackAddMenu').click(function(){   
            $("#addAmbassadorName").val("");
            $("#addCampaign").val("");
            $("#addCustomerName").val("");
            $("#addCustomerComment").val("");
            $("#addFile").val("");
            $("#addPrefReward").val("");
            $("#addProofreading").val("");
            $("#addKudosCard").val("");
            $("#addRewardStatus").val("");

        });

        $("#myButton1").click(function (e) {
            $("#table1").table2excel({
                name: 'report',
                filename: 'report'
            });
        });

        $("#download_report1").click(function (e) {
            $("#table_report1").table2excel({
                name: 'report',
                filename: 'report'
            });
        });

        $("#download_report2").click(function (e) {
            $("#table_report2").table2excel({
                name: 'report',
                filename: 'report'
            });
        });

        $("#download_report3").click(function (e) {
            $("#table_report3").table2excel({
                name: 'report',
                filename: 'report'
            });
        });


        
    });
</script>
<script>
    $(function(){
        "use strict";
        var obj = JSON.parse(<?php echo "'".json_encode($name)."'"; ?>);
        var all = obj.map(function (value) {
            return value.name;
        });
        $("#addAmbassadorName2").autocomplete({source: all});
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>



<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>