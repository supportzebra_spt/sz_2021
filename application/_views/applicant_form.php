<!DOCTYPE html> 
<html  lang="en" >

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>
 
<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>
<script src="<?php echo base_url();?>assets/js/angular.min.js"></script>
<script type="text/javascript">
    /* Input masks */

    $(function() { "use strict";
        $(".input-mask").inputmask();
    });

</script>

 <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/uniform/uniform.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/uniform/uniform.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/uniform/uniform-demo.js"></script>

      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">

</head>


    <body ng-app="app" ng-controller="ctrl">
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper" style="background-image: url('http://192.168.2.244/Mark/sz3/assets/images/bg-apply.jpg');">
        <div id="page-header" style="background: #232323;color: white;">
	  <center>	<img src='<?php echo base_url();?>assets/images/banner.jpg' style="width: 40%;"> </center> 
  


 
</div>
         
   <div class="form-group" style="width: 800px;margin: 0 auto;">
<div class="panel">
     <div class="panel-body" style="box-shadow: 10px 10px 5px #dad7d7;">

 <div class="form-group" style="background: #232323;padding: 10px;width: 100%;color: white;">
			 <h4><img src='<?php echo base_url('assets/images/user-apply.jpg'); ?>'> <b> Personal Information: </b></h4>
				</div>
        <div class="example-box-wrapper">
            <form class="form-horizontal bordered-row" name="myForm">
			 
			 <div class="form-group">
                    <label class="col-sm-3 control-label">Fullname * : </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="firstName" id="firstName" placeholder="First name" ng-model="firstName" required>
                    </div> 
					<div class="col-sm-3">
                        <input type="text" class="form-control" name="middleName" id="middleName" placeholder="Middle name" ng-model="middleName" required>
                    </div> 
					<div class="col-sm-3">
                        <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Surname" ng-model="lastName" required>
                    </div>
                </div>
				
				<div class="form-group">
                    <label class="col-sm-3 control-label">Address * : </label>
                    <div class="col-sm-4">
						<input type="text" class="form-control" name="Lot" id="Lot" placeholder="Lot #/ St. / Blk." ng-model="Lot" required>
                        <input type="text" class="form-control" name="Barangay" id="Barangay" placeholder="Barangay" ng-model="Barangay" required>
                        <input type="text" class="form-control" name="City" id="City" placeholder="City/Town" ng-model="City" required>
                        <input type="text" class="form-control" name="Province" id="Province" placeholder="Province" ng-model="Province" required>
 				<input type="text" class="input-mask form-control" id="Zip" placeholder="Zip Code" ng-model="Zip"  data-inputmask="'mask':'9999'" >

                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label">Contact Number  : </label>
                    <div class="col-sm-3">
<input type="text" class="input-mask form-control" name="mask1" id="mask1" data-inputmask="'mask':'99999999999'" ng-model="mask1" required>
					<div class="help-block"><i>e.g. 09171234567</i></div>

                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label">Alt Contact Number  : </label>
                    <div class="col-sm-3">
<input type="text" class="input-mask form-control" name="mask2" id="mask2" data-inputmask="'mask':'99999999'" ng-model="mask2" required>
						<div class="help-block"><i>e.g. 85877777</i></div>

                    </div>
                </div>
				
				<div class="form-group">
                    <label class="col-sm-3 control-label">Religion * : </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="Religion" id="Religion" placeholder="..." ng-model="Religion" required>
                    </div> 
					 
                </div>
				
				 <div class="form-group">
                    <label class="col-sm-3 control-label">Gender * : </label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class=" radio-primary">
                                    <label>
                                        <input type="radio" name="Gender" id="Gender"  value="Male" class="custom-radio" ng-model="Gender" required>
                                        Male
                                    </label>
                                </div>
                      
                                <div class=" radio-danger">
                                    <label>
                                        <input type="radio" name="Gender" id="Gender" value="Female" class="custom-radio" ng-model="Gender" required>
                                        Female
                                    </label>
                                </div>
                            </div>
                         
                        </div>
                    </div>
                </div>
				</form>
				</div>
				<div class="form-group" style="background: #232323;padding: 10px;width: 100%;color: white;">
				<h4><img src='<?php echo base_url();?>assets/images/account-application-icon.jpg' style="width: 3%;"> <b>Application details:</b> </h4>
				</div>
				<div class="example-box-wrapper">
            <form class="form-horizontal bordered-row"  name="myForm2">
			 <div class="form-group">
                    <label class="col-sm-3 control-label">Position applied * : </label>
                    <div class="col-sm-6" >
					 
 
                    <a href="#" > 
                    </a>
                        <select class="form-control popover-button-default" data-content="If the desired position is unavailable, please inquire <b>Human Resources Team</b>." title="<font color='green'><b>Note:</b></font>" data-trigger="hover" data-placement="right"   name="position"   id="position" required ng-model="position">
						<option selected disabled>--</option>
					<?php foreach($data as $key =>$val){ ?>
						<option value="<?php echo $val['pos_details']; ?>"><?php echo $val['pos_details']; ?></option>
					<?php } ?>
                        </select>
                    <div id='result'>
                    </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label">Source / Referral * : </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="source" name="source"   placeholder="..." ng-model="source" required>
                    </div> 
					 
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label">Has call center experience: </label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class=" radio-primary">
                                    <label>
                                        <input type="radio" id="ccexpo" name="ccexpo" value="Y" class="custom-radio" ng-model="ccexpo" required>
                                        Yes
                                    </label>
                                </div>
                      
                                <div class=" radio-danger">
                                    <label>
                                        <input type="radio" id="ccexpo" name="ccexpo" value="N" class="custom-radio" ng-model="ccexpo" required>
                                        None
                                    </label>
                                </div>
                            </div>
                         
                        </div>
                    </div>
                </div>
				 <div class="form-group">
                    <label class="col-sm-3 control-label">Character reference #1 * : </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="crf1" name="crf1" placeholder="Fullname" ng-model="crf1" required>
                        <input type="text" class="input-mask form-control" data-inputmask="'mask':'(9999) 999-9999'" placeholder="Contact number" id="crm1" name="crm1" ng-model="crm1" required>
                        <input type="text" class="form-control" id="crp1" name="crp1" placeholder="Profession" ng-model="crp1" required>
                    </div> 
					  </div>
					  <div class="form-group">
					<label class="col-sm-3 control-label">Character references #2 * :  </label>

					 <div class="col-sm-3">
                        <input type="text" class="form-control" id="crf2" name="crf2" placeholder="Fullname" ng-model="crf2" required>
                        <input type="text" class="input-mask form-control" data-inputmask="'mask':'(9999) 999-9999'" placeholder="Contact number" id="crm2" name="crm2" ng-model="crm2" required>
                        <input type="text" class="form-control" id="crp2" name="crp2" placeholder="Profession" ng-model="crp2" required>
                    </div> 
					  </div>
					  <div class="form-group">
					<label class="col-sm-3 control-label">Character references #3 * : </label>

					 <div class="col-sm-3">
                        <input type="text" class="form-control" id="crf3" name="crf3" placeholder="Fullname" ng-model="crf3" required>
                        <input type="text" class="input-mask form-control" data-inputmask="'mask':'(9999) 999-9999'" placeholder="Contact number" id="crm3" name="crm3" ng-model="crm3" required>
                        <input type="text" class="form-control" id="crp3" name="crp3" placeholder="Profession" ng-model="crp3" required>
                    </div> 
					  </div>
                </div>
				
			</form>
			 <div class="form-group">
                    <button class="btn btn-primary"  data-toggle="modal" data-target="#myModal"><b>Apply!</b></button>
				</div>
		</div>	
	</div>	

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header" style="
    background: #232323;
    color: white;
">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
	  <center>	<img src='<?php echo base_url();?>assets/images/banner.jpg' style="width: 80%;"> </center> 
      </div>
      <div class="modal-body"  >
        <table style="width: 100%;" class="table table-bordered table-striped table-condensed cf">	
		 <thead class="cf">
                    <tr>
                        <th>Fields</th>
                        <th>Information</th>
                        <th>Note</th>
                         
                    </tr>
		</thead>
		<tbody>
			<tr>
				<td> <b>Fullname </b> </td>  <td> {{firstName }} {{ middleName }} {{ lastName }} </td>
				<td >
					<span ng-show="myForm.firstName.$invalid || myForm.middleName.$invalid ||  myForm.lastName.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm.firstName.$valid && myForm.middleName.$valid && myForm.lastName.$valid "> <font color='green'><img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"></font> </span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Address </b> </td>  <td> {{ Lot }} {{ Barangay }} {{ City }} {{ Province }}  {{ Zip }} </td>
				<td >
					<span ng-show="myForm.Lot.$invalid || myForm.Barangay.$invalid ||  myForm.City.$invalid ||  myForm.Province.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm.Lot.$valid && myForm.Barangay.$valid && myForm.City.$valid "> <font color='green'><img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"></font> </span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Contact # </b> </td> <td> {{ mask1 }}  </td>
				<td >
					<span ng-show="myForm.mask1.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm.mask1.$valid"> <font color='green'><img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"></font> </span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Alt Contact # </b> </td> <td> {{ mask2 }}  </td>
				<td >
					<span ng-show="myForm.mask2.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm.mask2.$valid"> <font color='green'><img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"></font> </span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Religion </b> </td> <td> {{ Religion }}  </td>
				<td >
					<span ng-show="myForm.Religion.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm.Religion.$valid"> <font color='green'><img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"></font> </span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Gender </b> </td> <td> {{ Gender }}   </td>
				<td >
					<span ng-show="myForm.Gender.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm.Gender.$valid"> <font color='green'><img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"></font> </span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Applying for </b> </td> <td> {{ position }}   </td>
				<td >
					<span ng-show="myForm2.position.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm2.position.$valid"> <img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"> </span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Source </b> </td> <td> {{ source }}   </td>
				<td >
					<span ng-show="myForm2.source.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm2.source.$valid"> <img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"></span> 
				</td>
			</tr>
			<tr>
 				<td> <b>CC Experience</b> </td> <td> {{ ccexpo }}   </td>
				<td >
					<span ng-show="myForm2.ccexpo.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm2.ccexpo.$valid"> <img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"></span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Character reference 1 </b> </td> <td> {{ crf1 }} <br> {{ crm1 }} <br> {{ crp1 }}   </td>
				<td >
					<span ng-show="myForm2.crf1.$invalid || myForm2.crm1.$invalid ||  myForm2.crp1.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm2.crf1.$valid && myForm2.crm1.$valid && myForm2.crp1.$valid "><img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"></span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Character reference 2 </b> </td> <td> {{ crf2 }}  <br> {{ crm2 }}  <br> {{ crp2 }}   </td>
				<td >
					<span ng-show="myForm2.crf2.$invalid || myForm2.crm2.$invalid ||  myForm2.crp2.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm2.crf2.$valid && myForm2.crm2.$valid && myForm2.crp2.$valid "> <img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"> </span> 
				</td>
			</tr>
			<tr>
 				<td> <b>Character reference 3 </b> </td> <td> {{ crf3 }}  <br> {{ crm3 }}  <br> {{ crp3 }}   </td>
				<td >
					<span ng-show="myForm2.crf3.$invalid || myForm2.crm3.$invalid ||  myForm2.crp3.$invalid"> <font color='red'> This field is required. </font> </span>
					<span ng-show="myForm2.crf3.$valid && myForm2.crm3.$valid && myForm2.crp3.$valid "><img src="<?php echo base_url("assets/images/check.png");  ?>" style="width: 30px;"> </span> 
				</td>
			</tr>
			</tbody>
        </table>
 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" ng-disabled="myForm.$invalid || myForm2.$invalid" ng-click="insertRecord()">Register</button>
      </div>
    </div>

  </div>
</div>
 

			
</div>	
	</div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
    <script>
    angular.module("app",[])
      .controller("ctrl",['$scope',function($scope){
 
      }])
	  
	  
	  var appz= angular.module('app',[]);
	  appz.controller('ctrl',function($scope,$http){
		  $scope.insertRecord = function(){
			$http.post(location.protocol + '//' + location.hostname+"/sz3/index.php/applicant/insertRecord",{
				"fname": $scope.firstName,
				"mname": $scope.middleName,
				"lname": $scope.lastName,
				"mobile": $scope.mask1,
				"tel": $scope.mask2,
				"Religion": $scope.Religion,
				"Gender": $scope.Gender,
				"position": $scope.position,
				"source": $scope.source,
				"ccexpo": $scope.ccexpo,
				"address": $scope.Lot +"|"+$scope.Barangay +"|"+$scope.City +"|"+$scope.Province +"|"+$scope.Zip,
				"cr1": $scope.crf1 +"|"+$scope.crm1 +"|"+$scope.crp1,
				"cr2": $scope.crf2 +"|"+$scope.crm2 +"|"+$scope.crp2,
				"cr3": $scope.crf3 +"|"+$scope.crm3 +"|"+$scope.crp3})
			.success(function(data,status,headers,config){
				swal(
				  'Success',
				  'You have just registered!',
				  'success'
				)
 
	  setTimeout(function(){location.reload();},2000);
		
			});  
			  
		  }
		  
	  });
	  
	  
	  
	  
	  
	  
  </script>
 

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>