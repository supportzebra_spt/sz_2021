<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
   /* td,th{
      white-space: nowrap;
    }*/
  </style>

  <style type="text/css">
    .myscrolling table {
      table-layout: inherit;
      *margin-left: -100px;/*ie7*/
    }
    .myscrolling td, .myscrolling th {
      vertical-align: top;
      padding: 10px;
      min-width: 100px;
      white-space: nowrap;
      border-left:1px solid black;
      border-right:1px solid black;
    }
    .myscrolling  th {
      position: absolute;
      *position: relative; /*ie7*/
      left: 0;
      width: 250px; 
      white-space: nowrap;
      text-overflow: ellipsis;    
      overflow: hidden;
    }
    .myouter {
      position: relative
    }
    .myinner {
      overflow-x: auto;
      overflow-y: visible;
      margin-left: 250px;
    }
	
 

	
  </style>
  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });

 </script><!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/chosen/chosen.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
 <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/daterangepicker/daterangepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
 <script type="text/javascript">
   $(function(){
    "use strict";
    var m = moment().format("MM/DD/YYYY");
    var end = moment().add(1,'year').format("MM/DD/YYYY");
    $("#daterangepicker-example").daterangepicker({ minDate: m,startDate: m,maxDate : end});
    $("[name=daterangepicker_start]").attr('readonly',true);
    $("[name=daterangepicker_end]").attr('readonly',true);
  });
  	 

   function isDoubleshft(){
	 var uempid = $("#uempid").val();
	 var acctimeIDdShift = $("#acctimeIDdShift").val();
	 	 $.ajax({
		type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/getTimeSched2",
		data: {
			emp_id: uempid
		},
		  cache: false,
		  success: function(res)
		  {
			var result = $.parseJSON(res);
 			$.each(JSON.parse(res),function (i,elem){
			   if(acctimeIDdShift==elem.acc_time_id){
				 $("#ustartend2").append("<option value='"+elem.acc_time_id+"' selected>"+elem.time_start+" - "+elem.time_end+"</option>");
			  }else{  
				 $("#ustartend2").append("<option value='"+elem.acc_time_id+"'>"+elem.time_start+" - "+elem.time_end+"</option>");
			    }
			});
		  }
		});
   }
    function addDS() {
		var shift = $("#ustartend2").val();
		var uempid = $("#uempid").val();
		var date = $("#uscheddate2").text().split("-");
		var switchstate = $('#uisWORD').bootstrapSwitch('state');

	 $.ajax({
		type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/addDoubleShiftSched",
		data: {
			emp_id: uempid,
			shift: shift,
			date: date[1],
			switchstate: switchstate,

		},
		  cache: false,
		  success: function(res)
		  {
	$('#myModal').modal('toggle');
		  }
		});
 	
	} 
    function updateDS() {
		var shift = $("#ustartend2").val();
		var uempid = $("#uempid").val();
		var sched_id = $("#schedIDdShift").val();
		var switchstate = $('#uisWORD').bootstrapSwitch('state');
	 $.ajax({
		type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/updateDoubleShiftSched",
		data: {
			emp_id: uempid,
			shift: shift,
			sched_id: sched_id,
			switchstate: switchstate,
		},
		  cache: false,
		  success: function(res)
		  {
	 $('#myModal').modal('toggle');
		  }
		});
 	
	} 
</script>
<!-- Input switch -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/input-switch/inputswitch.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>
<script type="text/javascript">
  /* Input switch */

  $(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
  });
</script>
<!-- Multi select -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
<script type="text/javascript">
  /* Multiselect inputs */

  $(function() { "use strict";
    $("#employeelist").multiSelect();
    $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
    $('input[type="radio"].custom-radio').uniform();
    $(".radio span").append('<i class="glyph-icon icon-circle"></i>');
  });
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/uniform/uniform.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/uniform/uniform-demo.js"></script> -->

</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div id="page-title">
          <h3>Schedule System</h3>
          <hr><br>
          <div class="example-box-wrapper">

            <div class="row">
              <div class="col-md-12">
                <div class="content-box">
                  <h3 class="content-box-header bg-primary">
                    <i class="glyph-icon icon-search"></i>
                    Search for Schedules
                    <div class="header-buttons-separator">
                      <a href="<?php echo base_url()?>index.php/Schedule/multiplesched" class="icon-separator">
                        <i class="glyph-icon icon-plus"></i>
                      </a> 
                      <a href="<?php echo base_url()?>index.php/Schedule/calendar" class="icon-separator">
                        <i class="glyph-icon icon-calendar"></i>
                      </a>
                      <a href="<?php echo base_url()?>index.php/Schedule/schedule_dtr" class="icon-separator">
                        <i class="glyph-icon icon-tasks"></i>
                      </a>
                    </div>
                  </h3>
                  <div class="content-box-wrapper">

                   <div class="row">
                    <div class="col-md-5">
                     <div class="row">
                      <div class="col-md-12">
                       <div class="form-group" id='acc_descriptiondiv'>
                        <label class='control-label'>Account Type</label>
                        <select class='form-control' id='acc_description'>
                          <option value='' id='removeme'>--</option>
                          <option value='All'>All</option>
                          <option value='Admin'>Admin</option>
                          <option value='Agent'>Agent</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">

                      <div class="form-group" id='acc_namediv'>
                        <label class='control-label'>Account Name</label>
                        <select class='form-control' id='acc_name'></select>
                      </div>
                    </div>


                    <div class="col-md-12">
                      <label class='control-label'>Date Range</label>
                      <div class="input-prepend input-group">
                        <span class="add-on input-group-addon">
                          <i class="glyph-icon icon-calendar"></i>
                        </span>
                        <input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control" >
                      </div>
                    </div>                    
                  </div>
                </div>
                <div class="col-md-7">
                  <div class="col-md-12"  id='employeelistdiv'>
                    <div class="row">
                      <div class="col-md-10">
                        <label class='control-label'>Employee List</label>
                        <select multiple name="" id="employeelist"  class="multi-select"></select>
                      </div>

                      <div class="col-md-2"><br>
                        <div class="btn-group-vertical">
                          <button id="select" class='btn btn-primary btn-xs'>Select all</button>
                          <button id="deselect" class='btn btn-primary btn-xs'>Deselect all</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="button-pane text-right">
              <button class="btn btn-primary" id='generateschedule'>Display Schedule</button>

            </div>
          </div>

        </div>
        <div class="col-md-12" id="errorlist" style="display:none">
         <div class="content-box">
          <h3 class="content-box-header bg-blue-alt">
            List of Employees With existing schedules
          </h3>
          <div class="content-box-wrapper">
           <div id="errorlisthtml"></div>
         </div>
       </div>
     </div>
   </div>
 </div>

</div> 
<div class="container" id="page-title">
  <div class="content-box" id="scheduleview" style="display:none;">
    <h3 class="content-box-header bg-blue-alt"> 
      <i class="glyph-icon icon-list"></i>
      SCHEDULE LIST
    </h3>
    <div class="content-box-wrapper">
     <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="myscrolling myouter">
          <div class="myinner">
            <table class="table table-striped table-hover table-condensed" id="preview">

            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="color: #ccc;border-color: #000;background: #2d2d2d;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">UPDATE SCHEDULE</h4>
      </div>
      <div class="modal-body">
        <h5 id="uschedtype" class="font-size-16 font-bold text-transform-upr text-center" style="background: gray;color: #fff;padding: 12px;margin-bottom: -19px;"></h5><br>
        <table class='table table-striped table-bordered table-condensed'>
          <tr id="trHead">
            <td class='text-center'>Date</td>
            <td class='text-center'>Time Start - End</td>
            <td class='text-center'>is Rest Day?</td>
          </tr>
          <tr>
            <input type="text" id="uschedid" name="" hidden>
            <input type="text" id="uempid" name="" hidden>
            <td id='uscheddate' class='text-center'></td>
            <td class='text-center'><select id='ustartend' class='form-control input-sm'></select></td>
            <td class='text-center'>

              <input type="checkbox" data-on-color="primary" data-off-color="warning" name="" class="input-switch" data-size="small" data-on-text="Yes" data-off-text="No" id='uisRD'>
            </td>
          </tr>
        </table>
		<input type="button" id="btnDoubleShift" class="btn btn-s btn-info" value="Double Shift" onclick="isDoubleshft()" style="display:none">
	 
		<div class="panel" id="topdivDoubleShift">
		<div class="panel-heading">
		
		<h3 class="panel-title"> <span id="spnDouble" style="color: red;font-size: small;"><input type="checkbox" id="isDoubled" class="custom-checkbox"> Click the checkbox if there is a double shift/Same day shift login.</span> </h3>
		</div>
		<div class="panel-body">
		<input type="text" id="acctimeIDdShift" hidden>
		<input type="text" id="schedIDdShift" hidden>
		<div id="divDoubleShift">
        <h5   class="font-size-16 font-bold text-transform-upr text-center" style="background: gray;color: #fff;padding: 12px;margin-bottom: -19px;">DOUBLE SHIFT</h5><br>
		<table class='table table-striped table-bordered table-condensed'>
		<tr style="background:#662A79; color: #fff;">
		<td class='text-center'>Time Start - End</td>
		<td class='text-center'>is WORD?</td>

		<td class='text-center'>Action</td>
		</tr>
		<tr>
            <td id='uscheddate2' class='text-center' hidden></td>

 		<td class='text-center'><select id='ustartend2' class='form-control input-sm'></select></td>
		 <td class='text-center'>
			<input type="checkbox" data-on-color="primary" data-off-color="warning" name="" class="input-switch" data-size="small" data-on-text="Yes" data-off-text="No" id='uisWORD'>
		</td>
		<td class='text-center' id="dsAct"></td>
		</tr>
		</table>
		</div> 
		</div>
		</div>
        <form>
          <div id="uleavechoice" style="display:none">
            <?php foreach($leave as $l):?>
             <div class="radio">
              <label>
                <input type="radio" name="uleaveradio" value="<?php echo trim($l->leave_id);?>">
                <?php echo trim($l->leave_name);?>  
              </label>
            </div>      
          <?php endforeach;?>

        </div>
      </form>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary" id='updatesched'>Save changes</button>
    </div>
  </div>
</div>
</div>
<div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title pad25R" >NEW SCHEDULE </h4>
        <!-- <span id='ifullname' class='pull-right'></span> -->
      </div>
      <div class="modal-body">
        <h5 id="ischedtype" class="font-size-16 font-bold text-transform-upr text-center" style="background: gray;color: #fff;padding: 12px;margin-bottom: -19px;"></h5><br>
        <table class='table table-striped table-bordered table-condensed'>
          <tr>
            <th class='text-center'>Date</th>
            <th class='text-center'>Time Start - End</th>
            <th class='text-center'>is Rest Day?</th>
          </tr>
          <tr>
            <input type="text" id="iempid" name="" hidden>
            <input type="text" id="ielemid" name="" hidden>
            <td id='ischeddate' class='text-center font-bold'></td>
            <td class='text-center'><select id='istartend' class='form-control input-sm'></select></td>
            <td class='text-center'>

              <input type="checkbox" data-on-color="primary" data-off-color="warning" name="" class="input-switch" data-size="small" data-on-text="Yes" data-off-text="No" id='iisRD'>
            </td>
          </tr>
        </table>

        <form>
          <div class="form-group" id="ileavechoice" style="display:none">
            <label class="control-label">Type Of Leave</label><br>
            <?php foreach($leave as $l):?>

              <input type="radio" name="ileaveradio" value="<?php echo trim($l->leave_id);?>">
              <?php echo trim($l->leave_name);?>         

            <?php endforeach;?>

          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id='insertsched'>Save Schedule</button>
      </div>
    </div>
  </div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        // $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $(function(){

   $("#employeelist").prop('disabled',true);
   $("#employeelist").multiSelect('refresh');
   $("#select").prop('disabled',true);
   $("#deselect").prop('disabled',true);
   $("#acc_name").prop('disabled',true);
   $("#acc_description").on('change',function(){
    var desc = $(this).val();
    $("#removeme").remove();
    if(desc=='All'){

     $("#employeelist").prop('disabled',true);
     $("#employeelist").multiSelect('refresh');
     $("#select").prop('disabled',true);
     $("#deselect").prop('disabled',true);
     $("#acc_name").prop('disabled',true);
   }else{

     $("#acc_name").prop('disabled',false);
     $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/getAccounts",
      data: {
        desc: desc
      },
      cache: false,
      success: function(res)
      {
        if(res!='Empty'){
          $("#acc_name").html("<option value='All'>All</option>");
          $.each(JSON.parse(res),function (i,elem){
              // alert(elem.rate);
              $("#acc_name").append("<option value='"+elem.acc_id+"'>"+elem.acc_name+"</option>");
            });
        }
      }
    });
   }
 });
 })
</script>
<script type="text/javascript">
  $(function(){
    $("#acc_name").on('change',function(){
      var acc_id = $(this).val();
      if(acc_id=='All'){
       $("#employeelist").prop('disabled',true);
       $("#employeelist").multiSelect('refresh');
       $("#select").prop('disabled',true);
       $("#deselect").prop('disabled',true);
     }else{
      $("#employeelist").prop('disabled',false);
      $("#employeelist").multiSelect('refresh');
      $("#select").prop('disabled',false);
      $("#deselect").prop('disabled',false);
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/getAccountEmployee",
        data: {
          acc_id: acc_id
        },
        cache: false,
        success: function(res)
        {
          if(res!='Empty'){
            $("#employeelist").html("");
            $.each(JSON.parse(res),function (i,elem){
              // alert(elem.rate);
              // $("#employeelist").append("<option value=''>Arique</option>");
              $('#employeelist').multiSelect('addOption', { value: elem.emp_id, text: elem.lname+", "+elem.fname, index: 0 });              

            });
            $("#employeelist").multiSelect('refresh');

          }else{

            $("#employeelist").html("");
            $("#employeelist").multiSelect('refresh');
          }
        }
      });
    }
  });
  })
</script>
<script type="text/javascript">
  $(function(){
    $("#acc_description").on('change',function(){
      $("#employeelist").html("");
      $("#employeelist").multiSelect('refresh');
    })
  })
</script>

<script type="text/javascript">
  $(function(){
    $('#select').click(function(){
      $("#employeelist").multiSelect('select_all');
    });
  })
</script>
<script type="text/javascript">
  $(function(){

    $('#deselect').click(function(){
      $("#employeelist").multiSelect('deselect_all');
    });
  })
</script>
<script type="text/javascript">
  $("#generateschedule").click(function(){

    $("#scheduleview").hide();
    var date = $("#daterangepicker-example").val();
    if(date==null||date==''){
      alert("date empty");
    }else{

      var dater = date.split(" - ");
      var fromDateRaw = dater[0];
      var fromDateRaw2 = fromDateRaw.split("/");
      var fromDate = fromDateRaw2[2]+"-"+fromDateRaw2[0]+"-"+fromDateRaw2[1];
      var toDateRaw = dater[1];
      var toDateRaw2 = toDateRaw.split("/");
      var toDate = toDateRaw2[2]+"-"+toDateRaw2[0]+"-"+toDateRaw2[1];
      var acc_description = $("#acc_description").val();
      var acc_name = $("#acc_name").val();
      var employeelist = $("#employeelist").val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/generatedate",
        data: {
          fromDate : fromDate,
          toDate : toDate
        },
        cache: false,
        success: function(sched)
        { 

          if(acc_description==''){
            alert("Please Choose Account Type");
          }else if(acc_description=='All'){
            $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>index.php/Schedule/getSchedAll",
              data: {
                fromDate : fromDate,
                toDate : toDate
              },
              cache: false,
              success: function(ress)
              { 
                $('#preview').empty();
                var result = $.parseJSON(ress);
                var schedule = $.parseJSON(sched);
                var header = "";
                header += "<tr>";
                header += "<th>Full Name</th>";
                for(var i=0;i<schedule.length;i++){
                  header += "<td>"+schedule[i].dates+"</td>";

                }

                header +="</tr>";

                $('#preview').append(header);
                result.forEach(function(res){ 
                  var tr = '<tr><th> '+res.lname+', '+res.fname+' </th>';
                  
                  for(var x=0;x<schedule.length;x++){
                    var istrue = false;
                    res.schedule.forEach(function(elem){
                      if(elem.schedule_date===schedule[x].dates){
                        // tr += '<td>'+elem.schedule_date+'</td>';
                        if(elem.time_start==null&&elem.type=="Leave"){

                          if(elem.leave_name==null){

                            tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >Leave (Draft)</a></td>';
                          }else{

                            tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.leave_name+'</a></td>';
                          }
                        }else if(elem.time_start==null&&elem.type=="Rest Day"){
                          tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.type+'</a></td>';
                        }else if(elem.time_start!=null&&elem.type=="WORD"){
                          tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.time_start+' - '+elem.time_end+' : '+elem.type+'</a></td>';
                        }else{
                          tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.time_start+' - '+elem.time_end+'</a></td>';
                        }

                        istrue = true;
                      }
                    });
                    if(istrue!=true){
                      tr += '<td  style="background:#EE3B3B !important;" id="'+res.emp_id+'-'+x+'"><a href="" data-id="'+res.emp_id+'-'+x+'" data-schedule="'+schedule[x].dates+'"  data-empid="'+res.emp_id+'" data-toggle="modal" data-target="#insertModal" class="display-block" style="text-decoration:none">&nbsp;</a></td>';
                      // data-fullname="'+res.lname+', '+res.fname+'"
                    }
                  }
                  tr += '</tr>';
                  $('#preview').append(tr);
                });
                
                $("#scheduleview").show();
              }
            });
}else{
  if(acc_name=='All'){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Schedule/getSchedPerType",
    data: {
      fromDate : fromDate,
      toDate : toDate,
      acc_description : acc_description
    },
    cache: false,
    success: function(ress)
    { 
      $('#preview').empty();
      var result = $.parseJSON(ress);
      var schedule = $.parseJSON(sched);
      var header = "";
      header += "<tr>";
      header += "<th>Full Name</th>";
      for(var i=0;i<schedule.length;i++){
        header += "<td>"+schedule[i].dates+"</td>";

      }

      header +="</tr>";

      $('#preview').append(header);
      result.forEach(function(res){ 
        var tr = '<tr><th> '+res.lname+', '+res.fname+' </th>';

        for(var x=0;x<schedule.length;x++){
          var istrue = false;
          res.schedule.forEach(function(elem){
            if(elem.schedule_date===schedule[x].dates){
              if(elem.time_start==null&&elem.type=="Leave"){

                if(elem.leave_name==null){

                  tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >Leave (Draft)</a></td>';
                }else{

                  tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.leave_name+'</a></td>';
                }
              }else if(elem.time_start==null&&elem.type=="Rest Day"){
                tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.type+'</a></td>';
              }else if(elem.time_start!=null&&elem.type=="WORD"){
                tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.time_start+' - '+elem.time_end+' : '+elem.type+'</a></td>';
              }else{
                tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.time_start+' - '+elem.time_end+'</a></td>';
              }

              istrue = true;
            }
          });
          if(istrue!=true){
            tr += '<td  style="background:#EE3B3B !important;" id="'+res.emp_id+'-'+x+'"><a href="" data-id="'+res.emp_id+'-'+x+'" data-schedule="'+schedule[x].dates+'"  data-empid="'+res.emp_id+'" data-toggle="modal" data-target="#insertModal" class="display-block" style="text-decoration:none">&nbsp;</a></td>';
                      // data-fullname="'+res.lname+', '+res.fname+'"
                    }
                  }
                  tr += '</tr>';
                  $('#preview').append(tr);
                });
      $("#scheduleview").show();
    }
  });
 }else{
  var emplist = employeelist.toString().split(",");
  employeelist = emplist.join('-');
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Schedule/getSchedPerAccountEmployee",
    data: {
      fromDate : fromDate,
      toDate : toDate,
      employeelist : employeelist
    },
    cache: false,
    success: function(ress)
    { 
     $('#preview').empty();
     var result = $.parseJSON(ress);
     var schedule = $.parseJSON(sched);
     var header = "";
     header += "<tr>";
     header += "<th>Full Name</th>";
     for(var i=0;i<schedule.length;i++){
      header += "<td>"+schedule[i].dates+"</td>";

    }

    header +="</tr>";

    $('#preview').append(header);
    result.forEach(function(res){ 
      var tr = '<tr><th> '+res.lname+', '+res.fname+' </th>';

      for(var x=0;x<schedule.length;x++){
        var istrue = false;
        res.schedule.forEach(function(elem){
          if(elem.schedule_date===schedule[x].dates){
            if(elem.time_start==null&&elem.type=="Leave"){

              if(elem.leave_name==null){

                tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >Leave (Draft)</a></td>';
              }else{

                tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.leave_name+'</a></td>';
              }
            }else if(elem.time_start==null&&elem.type=="Rest Day"){
              tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.type+'</a></td>';
            }else if(elem.time_start!=null&&elem.type=="WORD"){
              tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.time_start+' - '+elem.time_end+' : '+elem.type+'</a></td>';
            }else{
              tr += '<td><a href="" style="color:'+elem.style+'" data-toggle="modal" data-target="#myModal" data-empid="'+res.emp_id+'" data-schedule="'+elem.schedule_date+'" data-schedid="'+elem.sched_id+'" data-schedtype="'+elem.type+'" data-start="'+elem.time_start+'" data-end="'+elem.time_end+'"  data-leavenote="'+elem.leavenote+'" >'+elem.time_start+' - '+elem.time_end+'</a></td>';
            }

            istrue = true;
          }
        });
        if(istrue!=true){
          tr += '<td  style="background:#EE3B3B !important;" id="'+res.emp_id+'-'+x+'"><a href="" data-id="'+res.emp_id+'-'+x+'" data-schedule="'+schedule[x].dates+'"  data-empid="'+res.emp_id+'" data-toggle="modal" data-target="#insertModal" class="display-block" style="text-decoration:none">&nbsp;</a></td>';
        }
      }
      tr += '</tr>';
      $('#preview').append(tr);
    });
    $("#scheduleview").show();
  }
});

}
}

}
});

}

})
</script>
<script type="text/javascript">
	  function isEmptyObject( obj ) {
        var name;
        for ( name in obj ) {
            return false;
        }
        return true;
    }
  $('#myModal').on('show.bs.modal', function(e) {
    $('input[name="uleaveradio"]').prop('checked', false);
    var emp_id = $(e.relatedTarget).attr('data-empid');
    var schedid = $(e.relatedTarget).attr('data-schedid');
    var schedule = $(e.relatedTarget).attr('data-schedule');
    var schedtype = $(e.relatedTarget).attr('data-schedtype');
    var start = $(e.relatedTarget).attr('data-start');
    var end = $(e.relatedTarget).attr('data-end');
    var leavenote = $(e.relatedTarget).attr('data-leavenote').trim();
	 var acctimeIDdShift = $("#acctimeIDdShift").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/getTimeSched",
      data: {
        emp_id: emp_id
      },
      cache: false,
      success: function(res)
      {

        var result = $.parseJSON(res);
        if(schedtype=='Normal'){
          $("#uleavechoice").hide();
          $("#ustartend").html("<option value=''>--</option>");
         }else if(schedtype=='Leave'){
          $("#uleavechoice").show();
          $("#ustartend").html("<option value='' selected>--</option>");
         }else{
          $("#uleavechoice").hide();
          $("#ustartend").html("<option value='' selected>--</option>");
         }

        $.each(JSON.parse(res),function (i,elem){
          if(start==elem.time_start&&end==elem.time_end){
            $("#ustartend").append("<option value='"+elem.acc_time_id+"' selected>"+elem.time_start+" - "+elem.time_end+"</option>");
           }else{
            $("#ustartend").append("<option value='"+elem.acc_time_id+"'>"+elem.time_start+" - "+elem.time_end+"</option>");
           }
        });
		
        $("#uschedid").val(schedid);
        $("#uempid").val(emp_id);
        $("#uscheddate").html(schedule);
        $("#uscheddate2").html(schedule);
        if(schedtype=='Rest Day'||schedtype=='WORD'){
          $('#uisRD').bootstrapSwitch('state', true);
        }else{
          $('#uisRD').bootstrapSwitch('state', false);
        }
        $("#uschedtype").html(schedtype);
        $('input[name="uleaveradio"').each(function(i,obj) {
          var radiovalue = $.trim($(obj).val());
          // console.log(radiovalue);
          // console.log(leavenote);
          if(leavenote==radiovalue){
            $(obj).prop('checked', true);
          }
        });
      }
    });
  $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/confirmdoubleshift",
      data: {
        emp_id: emp_id,
        schedule: schedule
      },
      cache: false,
      success: function(res)
      {
	  var rs= JSON.parse(res);
 	console.log(JSON.stringify(res));
	if(JSON.stringify(res)=='"[]"') {
 			$( "#isDoubled" ).prop( "checked", false );
			$("#btnDoubleShift").trigger('click');
			$("#divDoubleShift").hide();
			$("#acctimeIDdShift").val("");
			$("#schedIDdShift").val("");
			$("#dsAct").html("<input type='button'   class='btn btn-sm btn-info' onclick='addDS()' value='ADD'>");
 
		} else {
			$("#acctimeIDdShift").val(rs[0].acc_time_id);
			$("#schedIDdShift").val(rs[0].sched_id);
			$( "#isDoubled" ).prop( "checked", true );
			$("#btnDoubleShift").trigger('click');
			$("#divDoubleShift").show();
			$("#dsAct").html("<input type='button' class='btn btn-sm btn-primary' onclick='updateDS()' value='UPDATE'>");

		}
		if(rs[0].isActive==1){
				$("#isDoubled").prop('checked', true);
				$("#divDoubleShift").show(); 

		}else{
				$("#isDoubled").prop('checked', false);
				$("#divDoubleShift").hide(); 

		}
      }
    });
   var switchstate = $('#uisRD').bootstrapSwitch('state');
    var ustartend = $(this).val();
    if(switchstate==true&&ustartend==''){
 	$("#trHead").css("background","#f6546a");
    $("#trHead").css("color","#fff");

   }else if(switchstate==true&&ustartend!=''){
     $("#trHead").css("background","#ff7f50");
    $("#trHead").css("color","#fff");

   }else if(switchstate==false&&ustartend!=''){
     $("#trHead").css("background","#4CAF50");
    $("#trHead").css("color","#fff");

  }else if(switchstate==false&&ustartend==''){
     $("#trHead").css("background","#088da5");
    $("#trHead").css("color","#fff");
  }else{
     $("#trHead").css("background","#4CAF50");
    $("#trHead").css("color","#fff");
	
  }
  })

</script>
<script type="text/javascript">
  $('#uisRD').bootstrapSwitch('onSwitchChange',function(){
   var switchstate = $(this).bootstrapSwitch('state');
   var ustartend = $("#ustartend").val();
   if(switchstate==true&&ustartend==''){
     $("#uschedtype").html('Rest Day');
     $("#uleavechoice").hide();
	$("#trHead").css("background","#f6546a");
    $("#trHead").css("color","#fff");

   }else if(switchstate==true&&ustartend!=''){
     $("#uschedtype").html('WORD');
     $("#uleavechoice").hide();
    $("#trHead").css("background","#ff7f50");
    $("#trHead").css("color","#fff");

   }else if(switchstate==false&&ustartend!=''){
    $("#uschedtype").html('Normal');
    $("#uleavechoice").hide();
    $("#trHead").css("background","#4CAF50");
    $("#trHead").css("color","#fff");

  }else if(switchstate==false&&ustartend==''){
    $("#uschedtype").html('Leave');
    $("#uleavechoice").show();
    $("#trHead").css("background","#088da5");
    $("#trHead").css("color","#fff");

  }else{
    $("#uschedtype").html('Normal');
    $("#uleavechoice").hide();
    $("#trHead").css("background","#4CAF50");
    $("#trHead").css("color","#fff");

  }
});
  $("#ustartend").on('change',function(){
    var switchstate = $('#uisRD').bootstrapSwitch('state');
    var ustartend = $(this).val();
    if(switchstate==true&&ustartend==''){
     $("#uschedtype").html('Rest Day');
     $("#uleavechoice").hide();
	$("#trHead").css("background","#f6546a");
    $("#trHead").css("color","#fff");

   }else if(switchstate==true&&ustartend!=''){
     $("#uschedtype").html('WORD');
     $("#uleavechoice").hide();
    $("#topdivDoubleShift").show();
    $("#trHead").css("background","#ff7f50");
    $("#trHead").css("color","#fff");

   }else if(switchstate==false&&ustartend!=''){
    $("#uschedtype").html('Normal');
    $("#uleavechoice").hide();
    $("#topdivDoubleShift").show();
    $("#trHead").css("background","#4CAF50");
    $("#trHead").css("color","#fff");

  }else if(switchstate==false&&ustartend==''){
    $("#uschedtype").html('Leave');
    $("#uleavechoice").show();
    $("#topdivDoubleShift").hide();
    $("#trHead").css("background","#088da5");
    $("#trHead").css("color","#fff");
  }else{
    $("#uschedtype").html('Normal');
    $("#uleavechoice").hide();
    $("#topdivDoubleShift").show();
    $("#trHead").css("background","#4CAF50");
    $("#trHead").css("color","#fff");
	
  }
})
</script>
<script type="text/javascript">
	
 
  $("#updatesched").click(function(){
    var schedid = $("#uschedid").val();
    var switchstate = $('#uisRD').bootstrapSwitch('state');
    var acc_time_id = $("#ustartend").val();
    var emp_id = $("#uempid").val();
    var scheddate = $("#uscheddate").text();
    var radioval = $('input[name=uleaveradio]:checked').val();
    if(switchstate==true&&acc_time_id==''){
      switchstate = 2;
      radioval=null;
    }else if(switchstate==true&&acc_time_id!=''){
     switchstate = 4
     radioval=null;
   }else if(switchstate==false&&acc_time_id!=''){
     switchstate = 1;
     radioval=null;
   }else if(switchstate==false&&acc_time_id==''){
    switchstate = 3;
  }else{
    switchstate = 1;
    radioval=null;
  }
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Schedule/updatesched",
    data: {
      schedid: schedid,
      switchstate: switchstate,
      acc_time_id: acc_time_id,
      emp_id: emp_id,
      scheddate: scheddate,
      leave_id:radioval
    },
    cache: false,
    success: function(res)
    { 
      if(res=='Failed'){
        alert('Failed');
      }else{
        var result = $.parseJSON(res);
        var dom = $('a').filter('[data-schedid="'+schedid+'"]');
        dom.data('data-schedtype',result[0].type);
        dom.data('data-start',result[0].time_start); 
        dom.data('data-end',result[0].time_end);
        if(result[0].time_start==null&&result[0].type=="Leave"){
          dom.text(result[0].type);
        }else if(result[0].time_start==null&&result[0].type=="Rest Day"){
          dom.text(result[0].type);
        }else if(result[0].time_start!=null&&result[0].type=="WORD"){
         dom.text(result[0].time_start+' - '+result[0].time_end+' : '+result[0].type);
       }else{
        dom.text(result[0].time_start+' - '+result[0].time_end);
      }
    }
    dom.removeClass();
    dom.css('color',result[0].style);
    $('#myModal').modal('toggle');
    $("#generateschedule").trigger('click');
  }
});
});
</script>
<script type="text/javascript">
  $('#iisRD').bootstrapSwitch('onSwitchChange',function(){
   var switchstate = $(this).bootstrapSwitch('state');
   var ustartend = $("#istartend").val();
   if(switchstate==true&&ustartend==''){
     $("#ischedtype").html('Rest Day');
     $("#ileavechoice").hide();
   }else if(switchstate==true&&ustartend!=''){
     $("#ischedtype").html('WORD');
     $("#ileavechoice").hide();
   }else if(switchstate==false&&ustartend!=''){
    $("#ischedtype").html('Normal');
    $("#ileavechoice").hide();
  }else if(switchstate==false&&ustartend==''){
    $("#ischedtype").html('Leave');
    $("#ileavechoice").show();
  }else{
    $("#ischedtype").html('Normal');
    $("#ileavechoice").hide();
  }
  $("#insertsched").prop('disabled',false);
});
  $("#istartend").on('change',function(){
    var switchstate = $('#iisRD').bootstrapSwitch('state');
    var ustartend = $(this).val();
    if(switchstate==true&&ustartend==''){
     $("#ischedtype").html('Rest Day');
     $("#ileavechoice").hide();
   }else if(switchstate==true&&ustartend!=''){
     $("#ischedtype").html('WORD');
     $("#ileavechoice").hide();
   }else if(switchstate==false&&ustartend!=''){
    $("#ischedtype").html('Normal');
    $("#ileavechoice").hide();
  }else if(switchstate==false&&ustartend==''){
    $("#ischedtype").html('Leave');
    $("#ileavechoice").show();
  }else{
    $("#ischedtype").html('Normal');
    $("#ileavechoice").hide();
  }
  $("#insertsched").prop('disabled',false);
})
</script>
<script type="text/javascript">
  $('#insertModal').on('show.bs.modal', function(e) {

    $('input[name="ileaveradio"]').prop('checked', false);
    var elemid = $(e.relatedTarget).attr('data-id');
    var emp_id = $(e.relatedTarget).attr('data-empid');
    var schedule = $(e.relatedTarget).attr('data-schedule');
    var fullname = $(e.relatedTarget).attr('data-fullname');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/getTimeSched",
      data: {
        emp_id: emp_id
      },
      cache: false,
      success: function(res)
      {
        var result = $.parseJSON(res);

        $("#istartend").html("<option value=''>--</option>");
        $.each(JSON.parse(res),function (i,elem){
          $("#istartend").append("<option value='"+elem.acc_time_id+"'>"+elem.time_start+" - "+elem.time_end+"</option>");

        });
        $("#iempid").val(emp_id);
        $("#ielemid").val(elemid);
        $("#ischeddate").html(schedule);
        $("#ischedtype").html('');
      // $("#ifullname").html(fullname);
      // $('#iisRD').bootstrapSwitch('state', true);
      // $('#iisRD').bootstrapSwitch('state', false);
      $("#ischedtype").html('No Schedule Set');
      $("#insertsched").prop('disabled',true);
    }
  });
  })
</script>
<script type="text/javascript">
  $("#insertsched").click(function(){
    var elemid = $("#ielemid").val();
    var switchstate = $('#iisRD').bootstrapSwitch('state');
    var acc_time_id = $("#istartend").val();
    var emp_id = $("#iempid").val();
    var scheddate = $("#ischeddate").text();  
    var radioval = $('input[name=ileaveradio]:checked').val();
    if(switchstate==true&&acc_time_id==''){
      switchstate = 2;
    }else if(switchstate==true&&acc_time_id!=''){
     switchstate = 4
   }else if(switchstate==false&&acc_time_id!=''){
     switchstate = 1;
   }else if(switchstate==false&&acc_time_id==''){
    switchstate = 3
  }else{
    switchstate = 1;
  }
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Schedule/insertsched",
    data: {
      switchstate: switchstate,
      acc_time_id: acc_time_id,
      emp_id: emp_id,
      scheddate: scheddate,
      leave_id:radioval
    },
    cache: false,
    success: function(res)
    { 
      if(res=='Failed'){
        alert('Failed');
      }else{
        var result = $.parseJSON(res);
        var dom = $('td').filter('[id="'+elemid+'"]');
        // console.log(dom);
        dom.removeAttr( 'style' );
        dom.empty();
        dom.html('<a href="" style="color:'+result[0].style+'" data-toggle="modal" data-target="#myModal" data-empid="'+emp_id+'" data-schedule="'+scheddate+'" data-schedid="'+result[0].sched_id+'" data-schedtype="'+result[0].type+'" data-start="'+result[0].time_start+'" data-end="'+result[0].time_end+'" ></a>');
        if(result[0].time_start==null&&result[0].type=="Leave"){
          dom.children("a").text(result[0].type);
        }else if(result[0].time_start==null&&result[0].type=="Rest Day"){
          dom.children("a").text(result[0].type);
        }else if(result[0].time_start!=null&&result[0].type=="WORD"){
         dom.children("a").text(result[0].time_start+' - '+result[0].time_end+' : '+result[0].type);
       }else{
        dom.children("a").text(result[0].time_start+' - '+result[0].time_end);
      }
    }
    $('#insertModal').modal('toggle');
    
    $("#generateschedule").trigger('click');
  }
});
});
</script>
<!--Mark CODES--->
<script>
$(document).ready(function(){

	 
	$('#isDoubled').change(function() {
   var date = $("#uscheddate2").text().split("-");
   var schedIDdShift = $("#schedIDdShift").val();
 	if(this.checked != true){
			$("#divDoubleShift").hide(); 
			var valz="0";
	}else{
			$("#divDoubleShift").show();
			$("#btnDoubleShift").trigger('click');
			var valz="1";
	}
 
		$.ajax({
		type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/deleteDoubleshift",
		data: {
 			shift: schedIDdShift,
			val: valz
 		},
		  cache: false,
		  success: function(res)
		  {
			if(res==1){
				alert("There is already logs that tagged to this Double Shift. In order for this to be disabled, you need to un-tag the shift.");		
			}else{
		
			}

		  }
		});
 
	 
});   

});

</script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>