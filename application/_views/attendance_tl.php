<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		#tblShowTR td{
			color: green;
		}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  
    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>






    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

 
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css"> 
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
 
<script type="text/javascript">
$(function() {
    $('#payrollC').daterangepicker();
});
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-gradient-9">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
 <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Employee
                </h3>
                <div class="example-box-wrapper">
                    <form class="form-horizontal bordered-row" role="form">
		
                         <div class="form-group" id="employzResult">
                    <div class="col-sm-12">
                        <!--<select multiple class="multi-select" id="employz">
 								<?php 
								foreach($employee as $k => $v){
									
									echo "<option value=".$v['emp_id'].">".$v['fname']." ".$v['lname']."</option>";
								}
								
								?>
                         </select>-->
						 
          <form id="demoform" action="#" method="post">
		  		 
					 <!--<div class="form-group">
						<label class="col-sm-3 control-label">Payroll Coverage:</label>
							<div class="col-sm-6">
								<select name="" class="chosen-select" style="display: none;" id="EmpType">
									<option disabled="disabled" selected >--</option>
									<option >Agent</option>
									<option >Admin</option>
								</select>
							</div>
						</div>-->
						 
							 <div class="form-group">
								<label for="" class="col-sm-2 control-label">Basic</label>
								<div class="col-sm-8">
									<div class="input-prepend input-group">
										<span class="add-on input-group-addon">
											<i class="glyph-icon icon-calendar"></i>
										</span>
										
  <input type="text" name="daterangepicker-example" id="payrollC"   class="form-control" value="<?php echo date("m/d/Y")." - ".date("m/d/Y")?>">
									</div>
								</div>
								</div>
            <select multiple="multiple" size="10" name="duallistbox_demo1[]" id="employz">
           <?php 
								foreach($employee as $k => $v){
									
									echo "<option value=".$v['emp_id'].">".$v['lname'].", ".$v['fname']."</option>";
								}
								
								?>
            </select>
			
             </form>

                    </div>
                </div>
            <input type="button" class="btn btn-xs btn-primary" id="btnShow" value="Show">
            <input type="button" class="btn btn-xs btn-info" id="btnExport" value="Export">
  
                    </form>
                </div>
            </div>
        </div>
   
       
    </div>
<div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                
                <div class="example-box-wrapper">
                    <form class="form-horizontal bordered-row" role="form">
			
                         <table  class="table" style="color:black;"> 
							 <tr style="background: #4CAF50;color: white;">
								 <td>Fullname</td>
								 <td>Date</td>
								 <td>Shift-Start</td>
								 <td>Shift-End</td>
								 <td>Login</td>
								 <td>Logout</td>
								 <td>Late</td>
 								 <td>Total</td>
							 </tr>
							 <tbody id="tblShow">
								
							 </tbody>
							
                         </table>
                        
                    </form>
					   <div class="loading-spinner" id="loads">
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                            <i class="bg-green"></i>
                        </div>
                </div>
            </div>
        </div>
   
       
    </div>
	

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
 
 
<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/daterangepicker/daterangepicker.css">-->
 

</div>
</body>
<script>
  $(function(){
/* 	    $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  }); */	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
  });	
</script> 
<script>
  $(function(){
  $("#loads").hide();
	  $("#btnShow").click(function(){
		  
		  var payrollC = $("#payrollC").val();
		  var employz = $("#employz").val();
		   var dataString = "payrollC="+payrollC+"&employz="+employz;
 		    $.ajax({
                //url of the function
                    url: '<?php echo base_url();?>index.php/Attendanceambs/emptype',
                    type: 'POST',
                    data:  dataString,
					beforeSend:function(){
						$("#loads").show();
						$("#tblShow").html("");

					},
                    success:function(res)
                    {
					$("#loads").hide();
 						var td = "";
						var obj = jQuery.parseJSON(res);
						var timeTotal=0;
						var timeTotalOTBCT=0;
						var timeTotalBCT;
						var timeTotalVal;
						var Mrate=0;
						var daily =0;
						var hourly =0;
						var ndExplode = "";
						var ndRate=0;
						var ndRatexBCT=0;
						var ndRatexBCT2=0;
						var NDvalBCT=0;
						var HPval=0;
						var HPval2=0;
						var SSS;
						var PhilHealthCompute;
						var PagIbig;
						var pos_name;
						var hpRate;
						var HPComputeHoliday=0;
						var NDComputeHoliday=0;
						var BCTComputeHoliday=0;
						var totalTimee=0;
						var totalTimee2=0;
  						$(obj.employee).each(function(key,value){
							
							 $.each(value,function(k, v ){
								$.each(v,function(ky, vy ){
									td+="<tr>";
									timeTotalVal = (parseFloat(vy['total'])>=parseFloat("8.0")) ? "8.0" : ""+parseFloat(vy['total'])-parseFloat("1.0")+"" ;
									timeTotalBCT = (vy['bct']=='Q') ? "0.21" : "0.00";
									  daily = ((vy['rate']*12)/261);
									 hourly = daily/8; 
									 if(vy['nd']!=null){
										 ndExplode = vy['nd'].split("-");
									 ndRate = parseFloat(ndExplode[0]); //hours
									 ndRate += parseFloat(ndExplode[1]/60); //minutes
									 ndRatexBCT = (ndRate ==7) ? parseFloat(ndRate).toFixed(2) :(parseFloat(ndRate.toFixed(2))+parseFloat(timeTotalBCT)).toFixed(2); 
									 
									 	HPval = parseFloat(ndRate).toFixed(2);
									 }else{
										 ndRatexBCT = 0.00;
										 HPval =0.00;
									 }
										NDvalBCT = (vy['ndRemark']=='Y' && timeTotalBCT != 0.00) ?  "0.21" : "0.00" ;
										// HPval = (vy['hpRemark']=='Y') ?  parseFloat(ndRate).toFixed(2) : "0.00" ;
										//HPval = parseFloat(ndRate).toFixed(2);
									  NDComputeHoliday = parseFloat(ndRatexBCT)+ parseFloat(vy['NDComputeHolidayLogin'])+parseFloat(vy['NDComputeHolidayLogout']) - parseFloat(vy['latecalc']);
									/* if(vy['NDComputeHolidayLogin']>0 || vy['NDComputeHolidayLogout'] >0){
										NDComputeHoliday = parseFloat(ndRatexBCT) *2 ;

									}else{
										NDComputeHoliday = parseFloat(ndRatexBCT) ;
									} */
									  // alert(vy['date']+" = "+vy['NDComputeHolidayLogin']+" --- "+vy['NDComputeHolidayLogout']);
									HPComputeHoliday =parseFloat(HPval)+parseFloat(vy['HPComputeHolidayLogin'])+parseFloat(vy['HPComputeHolidayLogout'])+ parseFloat(NDvalBCT)- parseFloat(vy['latecalc']);
									 if(vy['HolidayType1'] =='Regular') {
											BCTComputeHoliday = parseFloat((timeTotalBCT)*1)+parseFloat(timeTotalBCT);
 
									 }else if(vy['HolidayType1'] =='Special'){
										 BCTComputeHoliday = parseFloat((timeTotalBCT)*.30)+parseFloat(timeTotalBCT);
 
									 }else{
										 BCTComputeHoliday = parseFloat(timeTotalBCT);
 
									 }
									totalTimee =  (timeTotalVal >0) ? timeTotalVal - vy['latemins'] : "0.0";
									totalTimee2 =  (timeTotalVal >0) ? timeTotalVal - vy['latemins'] : "0.0";
									td += "<td>"+vy['fname']+
									"</td><td>"+vy['date']+
									"</td><td>"+vy['shiftStart']+
									"</td><td>"+vy['shiftEnd']+
									"</td><td>"+vy['login']+
									"</td><td>"+vy['logout']+
									"</td><td>"+vy['late']+
 									"</td><td>"+parseFloat(Math.round(totalTimee2 * 100) / 100)+
 									"</td> ";
									td+="</tr>";
									 timeTotal +=parseFloat(totalTimee);
									 timeTotalOTBCT +=parseFloat(timeTotalBCT);
									  ndRatexBCT2+=parseFloat(ndRatexBCT);
									HPval2 +=parseFloat(HPval);
									SSS = parseFloat(vy['SSS']/2).toFixed(2);
									PhilHealthCompute = parseFloat(vy['PhilHealthCompute']/2).toFixed(2);
									PagIbig = parseFloat((vy['rate']*.02)/2).toFixed(2);
									pos_name =vy['pos_name'];
									hpRate =vy['hpRate'];
									 
								});
									var TotalBCTxHourly = timeTotalOTBCT.toFixed(2) * hourly.toFixed(2);
									td+="<tr id='tblShowTR'>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
 									"<td> </td>"+
									"<td>"+timeTotal.toFixed(2)+"</td>"+
 									" </tr>";
									td+="<tr style='background: #e8e8e8;'>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
									"<td> </td>"+
  "									</tr>";
								
 									// td+="<tr><td colspan=15 style='background: #e8e8e8;'> </td></tr>";
									timeTotal =0;
									timeTotalOTBCT =0;
									ndRatexBCT2 =0;									
									HPval2 =0;									
									NDComputeHoliday =0;									
 							  });
						});
						 $("#tblShow").html(td);
						
                    }
                }); 
	  });
	  $("#EmpType").change(function(){
		         var Emptype = $(this).val();
                  var dataString = "Emptype="+Emptype;

                $.ajax({
                //url of the function
                    url: 'emptyperesult',
                    type: 'POST',
                    data:  dataString,
                    success:function(res)
                    {
						 var  opt = ' ';
						 
					 
                        var obj = jQuery.parseJSON(res);
						$(obj.emp).each(function(key, value) 
						{
							for(var key in value) 
							{
								var objt = value[key];
								 opt += '<option value='+objt.emp_id+'>'+objt.fname+'</option>';
							 
							}
							
						 });
 						 
						 $("#employz").html(opt); 
					 
  						 
                    }
                });  
	  });
	});	
</script>
<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
<script type="text/javascript">
    /* Multiselect inputs */

    $(function() { "use strict";
        $(".multi-select").multiSelect();
        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
    });
</script>

          <script>
            var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
            $("#demoform").submit(function() {
              alert($('[name="duallistbox_demo1[]"]').val());
              return false;
            });
          </script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>