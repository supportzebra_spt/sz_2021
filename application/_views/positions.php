<!DOCTYPE html> 
<html  lang="en">

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
        </style>


        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/fontawesome/fontawesome.css"> -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">

        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });

        </script>
        <!-- Data tables -->

        <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/datatable/datatable.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-tabletools.js"></script>

        <script type="text/javascript">

                /* Datatables basic */

                $(document).ready(function () {
                    $('#datatable-example').dataTable();
                });

                $(document).ready(function () {
                    $('.dataTables_filter input').attr("placeholder", "Search...");
                });

        </script>
    </head>

    <body>
        <div id="sb-site">
            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
            <div id="page-wrapper">
                <div id="page-header" class="bg-black">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>
                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">
                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">
                        <div id="page-title">
                            <!-- <h2>Positions</h2><hr> -->
                            <div class="content-box border-top border-blue">
                                <h3 class="content-box-header clearfix">
                                    <i class="glyph-icon icon-cog"></i>
                                    List of all positions
                                    <span class="btn-group btn-group-sm float-right" data-toggle="buttons">
                                        <a href="#" class="btn btn-primary" style='text-transform: capitalize' data-toggle='modal' data-target="#createmodal">
                                            <span class="glyph-icon icon-separator">
                                                <i class="glyph-icon icon-plus"></i>
                                            </span>
                                            <span class="button-content">
                                                New Position
                                            </span>
                                        </a>
                                    </span>
                                </h3>
                                <div class="content-box-wrapper">
                                    <div class="example-box-wrapper">
                                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-condensed" id="datatable-example">
                                            <thead>
                                                <tr>
                                                    <th>Code</th>
                                                    <th>Position</th>
                                                    <th>Classification</th>
                                                    <th>Department</th>
                                                    <th>isHiring?</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($positions as $pos): ?>
                                                    <tr>
                                                        <td><?php echo $pos->pos_name; ?></td>
                                                        <td><?php echo $pos->pos_details; ?></td>
                                                        <td class='text-center'><?php echo $pos->class; ?></td>
                                                        <td class='text-center'><?php echo $pos->dep_name; ?></td>
                                                        <td class='text-center'><?php echo $pos->isHiring; ?></td>
                                                        <td class='text-center'>
                                                            <button class='btn btn-info btn-xs' data-posid='<?php echo $pos->pos_id; ?>' data-code='<?php echo $pos->pos_name; ?>' data-position="<?php echo $pos->pos_details; ?>" data-class='<?php echo $pos->class; ?>' data-department='<?php echo $pos->dep_id; ?>' data-isHiring='<?php echo $pos->isHiring; ?>' data-toggle='modal' data-target="#updatemodal"><i class="glyph-icon icon-edit"></i> Update</button>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="createmodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog" style='width:40%;margin-top:10%'>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">CREATE NEW POSITION</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row"> 
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Code <small class='font-red' id='coden' style="display: none">Required *</small></label>
                                    <input type="text" class='form-control' id='code'>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="control-label">Position <small class='font-red' id='positionn' style="display: none">Required *</small></label>
                                    <input type="text" class='form-control' id='position'>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Classification <small class='font-red' id='classificationn' style="display: none">Required *</small></label>
                                    <select id='classification' class='form-control'>
                                        <option value=''>--</option>
                                        <option value='Admin'>Admin</option>
                                        <option value='Agent'>Agent</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">is Hiring ? <small class='font-red' id='isHiringn' style="display: none">Required *</small></label>
                                    <select id='isHiring' class='form-control'>
                                        <option value=''>--</option>
                                        <option value='Yes'>Yes</option>
                                        <option value='No'>No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Department <small class='font-red' id='departmentn' style="display: none">Required *</small></label>
                                    <select id='department' class='form-control'>
                                        <option value=''>--</option>
                                        <?php foreach ($departments as $dept): ?>
                                            <option value="<?php echo $dept->dep_id; ?>"><?php echo $dept->dep_details; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id='createbtn' class="btn btn-primary">Create</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog" style='width:40%;margin-top:10%'>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">UPDATE POSITION</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row"> 
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Code <small class='font-red' id='ucoden' style="display: none">Required *</small></label>
                                    <input type="text" class='form-control' id='ucode'>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="control-label">Position <small class='font-red' id='upositionn' style="display: none">Required *</small></label>
                                    <input type="text" class='form-control' id='uposition'>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Classification <small class='font-red' id='uclassificationn' style="display: none">Required *</small></label>
                                    <select id='uclassification' class='form-control'>
                                        <option value='Admin'>Admin</option>
                                        <option value='Agent'>Agent</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">is Hiring ? <small class='font-red' id='uisHiringn' style="display: none">Required *</small></label>
                                    <select id='uisHiring' class='form-control'>
                                        <option value='Yes'>Yes</option>
                                        <option value='No'>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Department <small class='font-red' id='udepartmentn' style="display: none">Required *</small></label>
                                    <select id='udepartment' class='form-control'>
                                        <option value=''>--</option>
                                        <?php foreach ($departments as $dept): ?>
                                            <option value="<?php echo $dept->dep_id; ?>"><?php echo $dept->dep_details; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id='updatebtn' class="btn btn-primary" disabled>Update</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- JS Demo -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
    </div>
</body>
<script>
                $(function () {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
                        cache: false,
                        success: function (html) {
                            $('#headerLeft').html(html);
                            //alert(html);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
                        cache: false,
                        success: function (html) {
                            $('#sidebar-menu').html(html);
                            // alert(html);
                        }
                    });
                });
</script>
<script type="text/javascript">
        $("#createbtn").click(function () {
            var code = $("#code").val();
            var position = $("#position").val();
            var classification = $("#classification").val();
            var isHiring = $("#isHiring").val();
            var department = $("#department").val();
            if (code != '') {
                $("#coden").hide();
            } else {
                $("#coden").show();
            }
            if (position != '') {
                $("#positionn").hide();
            } else {
                $("#positionn").show();
            }
            if (classification != '') {
                $("#classificationn").hide();
            } else {
                $("#classificationn").show();
            }
            if (isHiring != '') {
                $("#isHiringn").hide();
            } else {
                $("#isHiringn").show();
            }
            if (department != '') {
                $("#departmentn").hide();
            } else {
                $("#departmentn").show();
            }

            if (code != '' && position != '' && classification != '' && isHiring != '' && department != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/PositionRates/createnewposition",
                    data: {
                        code: code,
                        position: position,
                        classification: classification,
                        isHiring: isHiring,
                        department: department
                    },
                    cache: false,
                    success: function (res) {
                        res = res.trim();
                        if (res == 'Success') {
                            swal('Success', 'Successfully create position.', 'success');
                        } else {
                            swal('Error', 'Failed creating a new position.', 'error');
                        }
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }
                });
            }
        });
</script>
<script type="text/javascript">
        $("#updatebtn").click(function () {
            var pos_id = $(this).data('posid');
            var code = $("#ucode").val();
            var position = $("#uposition").val();
            var classification = $("#uclassification").val();
            var isHiring = $("#uisHiring").val();
            var department = $("#udepartment").val();
            if (code != '') {
                $("#ucoden").hide();
            } else {
                $("#ucoden").show();
            }
            if (position != '') {
                $("#upositionn").hide();
            } else {
                $("#upositionn").show();
            }
            if (classification != '') {
                $("#uclassificationn").hide();
            } else {
                $("#uclassificationn").show();
            }
            if (isHiring != '') {
                $("#uisHiringn").hide();
            } else {
                $("#uisHiringn").show();
            }

            if (department != '') {
                $("#udepartmentn").hide();
            } else {
                $("#udepartmentn").show();
            }

            if (pos_id != '' && code != '' && position != '' && classification != '' && isHiring != '' && department != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/PositionRates/updateposition",
                    data: {
                        pos_id: pos_id,
                        code: code,
                        position: position,
                        classification: classification,
                        isHiring: isHiring,
                        department: department
                    },
                    cache: false,
                    success: function (res) {
                        res = res.trim();
                        if (res == 'Success') {
                            swal('Success', 'Successfully Updated Position.', 'success');
                        } else {
                            swal('Error', 'Failed to Update Position.', 'error');
                        }
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }
                });
            }
        });
</script>
<script type="text/javascript">
        $('#updatemodal').on('show.bs.modal', function (e) {
            var posid = $(e.relatedTarget).data('posid');
            var code = $(e.relatedTarget).data('code');
            var position = $(e.relatedTarget).data('position');
            var classification = $(e.relatedTarget).data('class');
            var isHiring = $(e.relatedTarget).data('ishiring');
            var department = $(e.relatedTarget).data('department');
            $("#updatebtn").data('posid', posid);
            $("#ucode").val(code);
            $("#uposition").val(position);
            $("#uclassification").val(classification);
            $("#uisHiring").val(isHiring);
            $("#udepartment").val(department);
            $("#updatebtn").prop('disabled', true);
        });
</script>
<script>
        $("#ucode,#uposition,#uclassification,#uisHiring,#udepartment").on('change', function () {
            $("#updatebtn").prop('disabled', false);
        });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>