<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
 .applicantDiv {
	height:175px;
	overflow:hidden
 }
 #content-box {
	border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
 }
 #appDown {
    background: #c391df;
    padding: 5px;
    border-bottom-right-radius: 20px;
    border-bottom-left-radius: 20px;
	color:white;
 }
 #dateDown {
    background: #49bac7;
    padding: 5px;
    border-bottom-right-radius: 20px;
    border-bottom-left-radius: 20px;
	color:white;
 }
 
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
	
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>

 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    
			
 

<div id="page-title">
<div class="form-group">
 
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
             
        </h3>
        <div class="example-box-wrapper">
            <div class="row">
			<div class="col-md-12">
					<div class="alert alert-danger">
                        <div class="bg-red alert-icon">
                            <i class="glyph-icon icon-info"></i>
                        </div>
                        <div class="alert-content">
                            <h4 class="alert-title">ANNOUNCEMENT: NEW SZ SYSTEM </h4>
								<?php 
									$dir = explode("/",base_url());
								?>
						
                            <p>The new SZ System is already available and can now be accessed through this <a href="<?php echo $dir[0]."/sz"; ?>" target="_blank"><code>LINK</code></a>.</p>
							<br>
							<p><b>The following modules are also available:</b></p>
							<ul>
								<li> <a href="<?php echo $dir[0]."/sz/dtr/attendance"; ?>" target="_blank">New DTR</a></li>
								<li> <a href="<?php echo $dir[0]."/sz/szfive"; ?>" target="_blank">SnapSZ</a></li>
								<li> <a href="<?php echo $dir[0]."/sz/dtr/myrequest"; ?>" target="_blank">Time-in & Time-out request</a></li>
								<li> <a href="<?php echo $dir[0]."/sz/additional_hour/request"; ?>" target="_blank">AHR request</a></li>
								<li> <a href="<?php echo $dir[0]."/sz/leave/request"; ?>" target="_blank">Leave request</a></li>
							</ul>
							--
							<br>
							Thank you. <i class="glyph-icon icon-heart"  aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="content-box">
                        <h3 class="content-box-header bg-blue-alt" style="background-color: #49C7B1;">
                            Headcount
                        </h3>
                        <div class="content-box-wrapper"  >
						<div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div> 

                        </div>
                    </div>

                </div>
             
		 
				
            </div>
        </div>
    </div>
</div>
	 
				
 <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
				<?php
				$admin ="";
					foreach($accAdmin as $key => $val){
					$admin += $val['cntAdmin'];
					}
				?> 
				<?php
				$agent ="";
					foreach($accAgent as $key => $val){
					$agent += $val['cntAgent'];
					}
				?> 
		<script type="text/javascript">
 $(function () {Highcharts.chart('container', {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'SupportZebra Employees'
    },
    subtitle: {
        text: ' '
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:.f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'
    },
    series: [{
        name: 'Employees',
        colorByPoint: true,
        data: [{
            name: 'SZ Team',
            y: <?php echo $admin; ?>,
            drilldown: 'Admin'
        }, {
            name: 'Ambassador',
            y: <?php echo $agent; ?>,
            drilldown: 'Agent'
        } ]
    }],
    drilldown: {
        series: [{
            name: 'SZ Team',
            id: 'Admin',
            data: [
              /*   ['Systems Developement Team', 6],
                ['Human Resources Team', 12],
                ['Operation Team', 15],
                ['Security and Maintenance Team', 18],
                ['Operation Support Team', 4], */
				<?php
				foreach($accAdmin as $key => $val){
					echo $val['cnt'].",";
				}
				?> 
             ]
        }, {
            name: 'Ambassador',
            id: 'Agent',
            data: [
            <?php
				foreach($accAgent as $key => $val){
					echo $val['cnt2'].",";
				}
				?> 
            ]
        } ]
    }
});
});  
		</script>
<?php 
 		// echo "<center>  <img src=".base_url('assets/images/maintenance.png')." style='width:50%' ></center>";
	    // echo "<center>  <img src=".base_url('assets/images/welcome.gif')."  style='width:20%' ></center>";

 


?>
	
	</div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>

                    </div>

                

            </div>
		 
        </div>
		
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>

	  <script type="text/javascript">
            $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>index.php/Survey/getActiveNotification",
              cache: false,
              success: function(res)
              {
                res = res.trim();
                console.log(res);
                // alert(res);
                if(res=='null'){
                    // alert("NULL");
                    
                }else{
                    window.open("<?php echo base_url(); ?>index.php/Survey/campaignnotification",'nicca', 'width=600,height=370,menubar=1,resizable=1,status=0,titlebar=0,toolbar=0');
                }
              }
            });
	  	
	  </script>
	  
<script>
	
	$(function(){
		$('#sidebar-menu').load('<?php echo base_url(); ?>/index.php/Templatedesign/sideBar')
		$('#headerLeft').load('<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft')

	/* 	$.ajax({
			type: "POST",
			url:  "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
			cache: false,
			success: function(html)
			{

				$('#headerLeft').html(html);
		   //alert(html);
		}
	});	
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
			cache: false,
			success: function(html)
			{

				$('#sidebar-menu').html(html);
		 // alert(html);
		}
	}); */
	});	
</script>
   

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>