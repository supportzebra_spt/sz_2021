<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/org/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/custom/plugins/BootstrapValidator/dist/css/formValidation.min.css" />

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>
<script src="<?php echo base_url()."assets/org/go.js"?>"></script>
<script type="text/javascript">
	$(window).load(function(){
		 setTimeout(function() {
			$('#loading').fadeOut( 400, "linear" );
		}, 300);
	});
</script>
<style>
div#divSZorgChart {
    text-align: center;
    padding: 15px;
    background: #424242;
    color: #fff;
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    font-size: x-large;
    font-weight: bolder;
}
</style>
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
<div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>
</div>
<div id="page-sidebar">
	<div class="scroll-sidebar">
		<ul id="sidebar-menu">
		</ul>
	</div>
</div>
<div id="page-content-wrapper">
	<div id="page-content">
		
<div class="container">
<div id="page-title">
	<div class="form-group"> 
	<div id="divSZorgChart">
	SUPPORTZEBRA's ORGANIZATIONAL HIERARCHY
	</div>
	<div id="sample">
		<div id="myDiagramDiv" style="background-color: #696969; border: solid 1px black; height: 1000px"></div>
		<button onclick="load()" hidden>Load</button>
	</div>

		<div id="myModal" class="modal fade" role="dialog" data-id="0" data-accposId="0"> 
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header" style="background: #313131;color: #fff;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Organization Structure</h4>
			  </div>
			  <div class="modal-body">
				  <div id="myDiagramDiv2">
					
				  </div>
			  </div>
			  <div class="modal-footer">
				  <form id="basicBootstrapFormNode" style="display: block;background: #4c4e4c;padding: 19px 15px 1px 11px;display:none" class="form-horizontal"  >
							<div class="form-group">
								<label class="col-sm-2 control-label" style="color:#fff">Jobs</label>
								<div class="col-sm-9">
									<select class="form-control" name="pos_id2" id="pos_id2">
										<option></option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" style="color:#fff">Node</label>
								<div class="col-sm-9">
									<select class="form-control" name="level" id="level">
										<option></option>
										<option>Above</option>
										<option>Sibling</option>
										<option>Under</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-11">
									<button type="submit" class="btn btn-primary btn-s" name="signup">Submit</button>
									<input type="button" class="btn btn-warning btn-s" onclick='hideDiv("#basicBootstrapFormNode")' value="Cancel">
								</div>
							</div>
					</form>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		 
	<!----START of ADD Modal----->
		<div id="myModalAdd" class="modal fade" role="dialog" data-id="" >
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header" style="background: #313131;color: #fff;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="headerAddmodal">Organization Structures</h4>
			  </div>
			  <div class="modal-body">
				  <div id="modalDivAdd" style="margin-bottom: 13px;">
					 <form id="basicBootstrapForm" class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">Jobs</label>
							<div class="col-sm-9">
								<select class="form-control" name="pos_id" id="pos_id">
									<option></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-11">
								<button type="submit" class="btn btn-primary btn-s" name="signup">Submit</button>
								<input type="button" class="btn btn-warning btn-s" onclick="closeModal('#myModalAdd')" value="Cancel">
							</div>
						</div>
					</form>
				  </div>
				  <div id="modalDivList">
				  </div>
			  </div>
			</div>

		  </div>
		</div>
	<!----END of ADD MODAL----->
	
	</div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
</div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/org/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
<!-- Bootstrap Validator -->
<script src="<?php echo base_url(); ?>assets/custom/plugins/BootstrapValidator/dist/js/formValidation.js"></script>
<script src="<?php echo base_url(); ?>assets/custom/plugins/BootstrapValidator/dist/js/framework/bootstrap.min.js"></script>

</div>
</body>
<script>
  $(function(){
	    $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
   $('#basicBootstrapForm').formValidation({
        framework: 'bootstrap',
		 excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
		fields: {
            pos_id: {
                validators: {
                    notEmpty: {
                        message: 'Selecting job is required'
                    }
                }
            },
		}
   }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
			var $form = $(e.target),
			fv = $form.data('formValidation');
			var accid=$("#myModalAdd").data("id");
			 $.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>/index.php/org/addAccountPosition/1/"+accid,
				data:  $form.serialize(),
				cache: false,
				success: function (res){
					$('#myModalAdd').modal('hide'); 
				}
			});
        });
   $('#basicBootstrapFormNode').formValidation({
        framework: 'bootstrap',
		 excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
		fields: {
            pos_id2: {
                validators: {
                    notEmpty: {
                        message: 'Selecting job is required'
                    }
                }
            },
			level: {
                validators: {
                    notEmpty: {
                        message: 'Level is required'
                    }
                }
            },
		}
   }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
			var $form = $(e.target),
			fv = $form.data('formValidation');
			var accid= $("#myModalAdd").data("id");
			var accposId= $("#myModal").data("accposId");
			var accpos = accposId.split(",")
			 // alert(accposId); //position,acc_id of selected node 
			 // alert($form.serialize());
			    $.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>/index.php/org/addAccountPosition/2/"+accpos[0]+"-"+accpos[1],
				data:  $form.serialize(),
				cache: false,
				success: function (res){
					$('#myModalAdd').modal('hide'); 
					// alert(res);
					hideDiv("#basicBootstrapFormNode");
					calll(accpos[1]);
				}
			});  
        });
	
 
  });	
</script>
 <script>
	
	/* function loadJobs(acc_id){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/org/loadPositionAccount",
			data: {acc_id:acc_id},
			cache: false,
			success: function (res){
			var rs = JSON.parse(res);
			var opt="<option></option>";
				$.each( rs, function( key, value ) {
					  opt+="<option value="+key+">"+value.pos_details+"</option>";
				});
				$("#jobs").html(opt);
			}
		});
	} */	
	function hideDiv(nameDiv){	
		$(nameDiv).hide();
	}
	function showDiv(nameDiv){	
		$(nameDiv).show();
	}
	function closeModal(nameModal){
		$(nameModal).modal("hide");
	}
  function resetForm(form){
	$(form).formValidation('resetForm', true);
  }
  
  function init() {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram =
      $(go.Diagram, "myDiagramDiv", // must be the ID or reference to div
        {
          initialContentAlignment: go.Spot.Center,
          layout:
            $(go.TreeLayout,
              {
                treeStyle: go.TreeLayout.StyleLastParents,
                angle: 90,
                alternateAngle: 90,
                alternateAlignment: go.TreeLayout.AlignmentBus,
              }),
        });
		myDiagram2 =
      $(go.Diagram, "myDiagramDiv2", // must be the ID or reference to div
        {
          initialContentAlignment: go.Spot.Center,
          layout:
            $(go.TreeLayout,
              {
                treeStyle: go.TreeLayout.StyleLastParents,
                angle: 90,
                alternateAngle: 90,
                alternateAlignment: go.TreeLayout.AlignmentBus,
              }),
        });

    var levelColors = ["#424242", "#AC193D", "#2672EC", "#D24726"];

    myDiagram.layout.commitNodes = function() {
      go.TreeLayout.prototype.commitNodes.call(myDiagram.layout);  // do the standard behavior
      myDiagram.layout.network.vertexes.each(function(v) {
        if (v.node) {
          var level = v.level % (levelColors.length);
          var color = levelColors[level];
          var shape = v.node.findObject("SHAPE");
          if (shape) shape.fill = $(go.Brush, "Linear", { 0: color, 1: go.Brush.lightenBy(color, 0.05), start: go.Spot.Left, end: go.Spot.Right });
        }
      });
    };
 myDiagram2.layout.commitNodes = function() {
      go.TreeLayout.prototype.commitNodes.call(myDiagram2.layout);  // do the standard behavior
      myDiagram2.layout.network.vertexes.each(function(v) {
        if (v.node) {
          var level = v.level % (levelColors.length);
          var color = levelColors[level];
          var shape = v.node.findObject("SHAPE");
          if (shape) shape.fill = $(go.Brush, "Linear", { 0: color, 1: go.Brush.lightenBy(color, 0.05), start: go.Spot.Left, end: go.Spot.Right });
        }
      });
    };

    function textStyle() {
      return { font: "5pt  Segoe UI,sans-serif", stroke: "white" };
    }
    myDiagram.nodeTemplate =
      $(go.Node, "Auto",
        $(go.Shape, "Rectangle",
          {
            name: "SHAPE", fill: "white", stroke: null,
          }),
        $(go.Panel, "Horizontal",
          // define the panel where the text will appear
          $(go.Panel, "Table",
            {
              maxSize: new go.Size(80, 929),
              margin: new go.Margin(6, 6, 6, 6),
              defaultAlignment: go.Spot.Left
            },
            $(go.RowColumnDefinition, { column: 2, width: 4 }),
            $(go.TextBlock, textStyle(),  // the name
              {
                row: 0, column: 0, columnSpan: 5,
                font: "9pt Segoe UI,sans-serif",
                editable: false, isMultiline: false,
                minSize: new go.Size(5, 7),
              },
              new go.Binding("text", "name").makeTwoWay()),
            $(go.TextBlock, textStyle(),
              {
                row: 1, column: 0, columnSpan: 5,
                editable: false, isMultiline: false,
				font: "7pt Segoe UI,sans-serif",
                minSize: new go.Size(5, 7),
                margin: new go.Margin(0, 0, 0, 3)
              },
              new go.Binding("text", "title").makeTwoWay()),
          )  // end Table Panel
        ), // end Horizontal Panel
		{ click: function(e, obj) {
			var acc_id = obj.part.data.key;
				calll(acc_id);
		}}
      ); 
	  myDiagram2.nodeTemplate =
      $(go.Node, "Auto",
        $(go.Shape, "Rectangle",
          {
            name: "SHAPE", fill: "white", stroke: null,
          }),
        $(go.Panel, "Horizontal",
          // define the panel where the text will appear 
          $(go.Panel, "Table",
            {
              maxSize: new go.Size(100, 949),
              margin: new go.Margin(6, 6, 6, 6),
              defaultAlignment: go.Spot.Left
            },
            $(go.RowColumnDefinition, { column: 2, width: 4 }),
            $(go.TextBlock, textStyle(),  // the name
              {
                row: 0, column: 0, columnSpan: 5,
                font: "9pt Segoe UI,sans-serif",
                editable: false, isMultiline: false,
                minSize: new go.Size(5, 7)
              },
              new go.Binding("text", "name").makeTwoWay()),
            $(go.TextBlock, textStyle(),
              {
                row: 1, column: 1, columnSpan: 4,
                editable: false, isMultiline: false,
                minSize: new go.Size(5, 7),
                margin: new go.Margin(0, 0, 0, 3)
              },
              new go.Binding("text", "title").makeTwoWay()),
          )  // end Table Panel
        ), // end Horizontal Panel
		{ click: function(e, obj) {
			var pos_id = obj.part.data.key;
			
				calll2(pos_id);
		}}
      );  // end Node

		// define the Link template
		myDiagram.linkTemplate =
      $(go.Link, go.Link.Orthogonal,
        $(go.Shape, { strokeWidth: 4, stroke: "#00a4a4" }));
		myDiagram2.linkTemplate =
      $(go.Link, go.Link.Orthogonal,
        $(go.Shape, { strokeWidth: 4, stroke: "#00a4a4" }));  
	 
  }
	
			function calll2(pos_id) {
				swal({
					  title: 'Please select what process to proceed..',
					  text: "Please specify which level or node.",
					  type: 'error',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Add Node',
					  cancelButtonText: 'Delete Node',
					  confirmButtonClass: 'btn btn-success',
					  cancelButtonClass: 'btn btn-danger',
					  buttonsStyling: false,

				}).then(function (e) {
 				 // alert(e);
					var acc_pos_id = pos_id.split(",");
					$('#myModal').data("accposId",pos_id);
					addNodePos(pos_id);
					showDiv("#basicBootstrapFormNode");
					loadJobs(acc_pos_id[1]);
					
						var $form = $(e.target),
						fv = $form.data('formValidation');
						var accid = $('#myModal').data('id');
				  
				},function(dismiss) {
				  if (dismiss === 'cancel') { // you might also handle 'close' or 'timer' if you used those
					delNode(pos_id);
				  } else {
					throw dismiss;
					}
				  });
  }
	function delNode(acc_pos_id) {
				swal({
					  title: 'Are you sure you want to delete this node?',
					  text: "This will be removed forever.",
					  type: 'error',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes',
					  cancelButtonText: 'No',
					  confirmButtonClass: 'btn btn-success',
					  cancelButtonClass: 'btn btn-danger',
					  buttonsStyling: false,

				}).then(function (e) {
 				   
					deleteNode(acc_pos_id);
					var accpos = acc_pos_id.split(",")
					hideDiv("#basicBootstrapFormNode");
					calll(accpos[1]);
				  
				});
  
  }
  function deleteNode(acc_pos_id){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/org/deleteNode",
			data: {acc_pos_id:acc_pos_id},
			cache: false,
			success: function (res){
			if(res>0){
				swal('Good job!','You have successfully deleted the node.','success')
			}else{
			swal('OOPs!','Please reload the page.','error')
			}
			}
		});
	}	
  function calll(acc_id){
  
  // $('#myDiagramDiv2').html(acc_id); 
  	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/org/sh2/"+acc_id,
		cache: false,
		success: function (res){
			hideDiv("#basicBootstrapFormNode");
			if(res!=0){
				$('#myModal').modal('show'); 
				$('#myModal').data('id',acc_id); 
				
			 myDiagram2.model = go.Model.fromJson({ "class": "go.TreeModel","nodeDataArray": JSON.parse(res)});
			
			var div = myDiagram2.div;
			div.style.height = '300px';
			myDiagram2.requestUpdate(); // Needed!
			}else{
				swal({
				  title: 'No Data Found!',
				  text: "Are you going to assign position to this?",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes',
				  cancelButtonText: 'No',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  buttonsStyling: false
				}).then(function () {
					addPosition("sh3",acc_id);
				})
			}
		}
	});
  }
  function loadJobs(acc_id){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/org/loadPosition",
			data: {acc_id:acc_id},
			cache: false,
			success: function (res){
			var rs = JSON.parse(res);
			var opt="<option></option>";
				$.each( rs, function( key, value ) {
					  opt+="<option value="+value.pos_id+">"+value.pos_details+"</option>";
				});
				$("#pos_id").html(opt);
				$("#pos_id2").html(opt);
				$("#myModalAdd").data("id",acc_id);
			}
		});
	}
  
function addNodePos(pos_id){
	resetForm("#basicBootstrapFormNode");
	$('#myModalAddNode').modal('show'); 
	
}
function addPosition(type,acc_id){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/org/"+type+"/"+acc_id,
		cache: false,
		success: function (res){
		var rs = JSON.parse(res);
		resetForm("#basicBootstrapForm");
		$('#myModalAdd').modal('show'); 
		$("#headerAddmodal").text(rs[0].name);
		loadJobs(acc_id);
		}
	});
}
 function testIfOK(){
	 var accid=$("#myModalAdd").data("id");
	// alert(accid+" ok");
}
  function load() {
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/org/show",
		cache: false,
		success: function (res) {
	 /* 
	 myDiagram.model = go.Model.fromJson({ "class": "go.TreeModel",
		"nodeDataArray": JSON.parse(res)
	});*/

			myDiagram.model = go.Model.fromJson({ "class": "go.TreeModel","nodeDataArray": JSON.parse(res)});
			
			var div = myDiagram.div;
			div.style.height = '100%';
			myDiagram.requestUpdate(); // Needed!

		}
	});
   
  }
</script>
<script>
$(function(){
	init()
	load();
});
</script>
</html>