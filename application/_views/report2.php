<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
        .hidden{
            visibility: hidden;
        }
        .seen{
            visibility: visible;
        }

    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- jQueryUI Autocomplete -->



    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

    <!--<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>-->

    <!-- High Charts-->

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/themes/dark-unica.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            Highcharts.chart('chart_3', {
                chart: {
                    inverted: true,
                    polar: false
                },
                title: {
                    text: 'Kudos Count from <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
                },
                credits: {
                    enabled: false
                },

                xAxis: {
                    categories: [<?php foreach($all as $a){ echo "'";echo $a->ambassador; echo "', "; }?>]
                },

                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                        },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php foreach($all as $a){ echo $a->kudos_count; echo ","; }?>],
            showInLegend: false
        }]

    });
            Highcharts.setOptions(Highcharts.theme);
        });
    </script>


    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>


    <!-- FORM MASKS-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>
    <script type="text/javascript">
        /* Input masks */

        $(function() { "use strict";
            $(".input-mask").inputmask();
        });

    </script>


    <script type="text/javascript">
        $(window).load(function(){
           setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
       });
   </script>



   <!-- jQueryUI Datepicker -->

   <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
   <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

   <script type="text/javascript">
    $(document).ready(function(){
        $('.datepick').each(function(){
            $(this).datepicker();
        });
    });
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

<!-- Table to Excel -->
<script type="text/javascript" src="<?php echo base_url();?>assets/table_export/jquery.table2excel.js" ></script>

<!-- Parsley -->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>



</head>


<body>
    <div id="sb-site">


        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div id="page-wrapper">
            <div id="page-header" class="bg-black">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                    <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                </div>
                <div id="header-logo" class="logo-bg">
                 <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                 <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id='headerLeft'>
            </div>



        </div>
        <div id="page-sidebar">
            <div class="scroll-sidebar">


                <ul id="sidebar-menu">
                </ul>
            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content">

                <div class="container">
                    <div id="page-title">
                        <div class="panel">
                            <div class="panel-body">

                                <div id="chart_3"></div>
                                <br>
                                <div class="form-group">
                                    <div>
                                        <div class="clearfix row">
                                            <label for="" class="col-sm-3 control-label">Date Range Picker</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="fromDate" id="fromDate" placeholder="From date..." class="float-left form-control datepick" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="toDate" id="toDate" placeholder="To date..." class="float-left form-control datepick" required>
                                            </div>
                                            <div class="col-sm-2">
                                                <input class="btn btn-primary" type="button" name="datePicked" id="datePicked3" value="Generate" style="background-color: #3498db; border-color: #3498db;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <h4 style="text-align: center;"><b>Generate Table for Overall Kudos Count by Ambassador</b></h4><br>
                                        <div align="center" style="display: none;">
                                            <div class="col-sm-2" align="center">
                                            </div>
                                            <div class="col-sm-3" align="center">
                                                <input type="text" name="fromDate6" id="fromDate6" placeholder="From date..." class="mrg10R form-control datepick" required>
                                            </div>
                                            <div class="col-sm-3" align="center">
                                                <input type="text" name="toDate6" id="toDate6" placeholder="To date..." class="form-control datepick" required>
                                            </div>
                                            <div class="col-sm-2" align="center">
                                                <button class="btn btn-primary" id="datePicked6" style="background-color: #3498db; border-color: #3498db; width: 100%;">Generate</button>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div id="showTable3" style="display: none;">
                                    <button class="btn btn-success float-left" id="download_report3" style="background-color: #3498db; border-color: #3498db;">Export</button><br>
                                    <table class="table table-striped" id="table_report3" cellpadding="0" style="width: 100%; font-size: 15px;" >
                                        <thead>
                                            <tr>
                                                <td><b>Ambassador</b></td>
                                                <td><b>Campaign</b></td>
                                                <td><b>Kudos Count</b></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    
                                </div>                     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>
</div>
</body>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/menu.js"></script>
<script>
    $(function(){
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
          cache: false,
          success: function(html)
          {

            $('#headerLeft').html(html);
     //alert(html);
 }
}); 
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
          cache: false,
          success: function(html)
          {

            $('#sidebar-menu').html(html);
   // alert(html);
}
});
    }); 
</script>
<script>

    $(function(){

        $("#datePicked3").click(function(){
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();

            var arr_date1 = fromDate.split('-');
            var arr_date2 = toDate.split('-');

            var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            var month1 = arr_date1[1];
            var month2 = arr_date2[1];
            var day1 = arr_date1[2];
            var day2 = arr_date2[2];

            var m1 = month1.replace(/^0+/, '');
            var m2 = month2.replace(/^0+/, '');
            var d1 = day1.replace(/^0+/, '');
            var d2 = day2.replace(/^0+/, '');

            var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
            var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate3',
                type: 'POST',
                dataType: 'json',
                data:  dataString,
                success:function(data)
                {
                    var ambassador = data.map(function (value) {
                        return value.ambassador;
                    });
                    var kudos_count = data.map(function (value) {
                        return parseInt(value.kudos_count);
                    });
                    console.log(ambassador);
                    console.log(kudos_count);
                    $("#datePicked6").trigger("click");
                    chart = Highcharts.chart('chart_3', {
                        chart: {
                            inverted: true,
                            polar: false,
                            spacingRight:1
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Kudos Count '+fromDate2+' to '+toDate2
                        },

                        xAxis: {
                            categories: ambassador,
                        },

                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                },
                                    //colors: ['#3399ff']
                                }
                            },

                            series: [{
                                type: 'column',
                                colorByPoint: true,
                                data: kudos_count,
                                showInLegend: false,
                            }],


                        });
                    Highcharts.setOptions(Highcharts.theme);
                }
            });
        });

        $("#datePicked6").click(function(){
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate6',
                type: 'POST',
                data:  dataString,
                success:function(data)
                {
                    $("#table_report3 tbody").html(data);
                    $("#showTable3").show();
                }
            });
        });

        $("#download_report3").click(function (e) {
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
            //url of the function
            url: '<?php echo base_url(); ?>index.php/Kudos/excel_report2',
            type: 'POST',
            data:  dataString,
            success:function(data)
            {
                $("#kcount").trigger("click").attr("target", "_blank");
            }
        });
        });


        
    });
</script>
<a href="<?php echo base_url(); ?>/reports/KudosCountAmbassadors.xls" style="display: none;"><input type="button" id="kcount" value="Test" class="btn btn-success"></a>
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>



<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>