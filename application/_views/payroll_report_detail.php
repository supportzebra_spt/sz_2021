<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		#tblShowTR td{
			color: green;
		}
		tbody#tblShow tr td {
			border: 1px solid #b9ffbc;
			vertical-align:middle;
		}
		tbody#tblShow tr:hover {
			background-color: #aaefad;
		}
		thead {
			background: #7b7b7b;
			color: white;
			font-size: smaller;
		}
		tbody{
			color: black;
		} 
</style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> SUPPORTZEBRA | PAYROLL REPORT SUMMARY </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">






    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

 
</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

 		<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper" >
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">

<div class="col-md-12">
        <div class="panel">
            <div class="panel-body" >

                 <div class="example-box-wrapper" >
                    <form class="form-horizontal bordered-row" role="form">
		
                         <div class="form-group" id="employzResult">
                    <div class="col-sm-12">
                        <!--<select multiple class="multi-select" id="employz">
 								<?php 
								foreach($employee as $k => $v){
									
									echo "<option value=".$v['emp_id'].">".$v['fname']." ".$v['lname']."</option>";
								}
								
								?>
                         </select>-->
						 
                  
          <form id="demoform" action="#" method="post">
		  		 
					 <div class="form-group">
                    <label class="col-sm-3 control-label">Payroll Coverage:</label>
                    <div class="col-sm-6">
                         <select name="" class="chosen-select" style="display: none;" id="payrollC">
								 
                                <option disabled="disabled" selected >--</option>
							<?php foreach($period as $row => $val){?>
							<optgroup label="<?php echo $val["month"]; ?>">
									<?php 
									$exp = explode(",",$val["daterange"]);
										for($i=0;$i<count($exp);$i++){	
										$newVal = explode("|",$exp[$i]);
									?>
										<option value="<?php echo $newVal[0]; ?>"><?php echo $newVal[1]; ?></option>
									<?php } ?>
							</optgroup>
							<?php } ?>
                        </select>
	 
                    </div>

                </div>
          	<div class="form-group">
                    <label class="col-sm-3 control-label">Type</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="empzType">
                            <option value="this option">--</option>
                            <option value="Admin">SZ Team</option>
                            <option value="Agent">Ambassador</option>
                        </select>
                    </div>
                </div>
			<div class="form-group">
                    <label class="col-sm-3 control-label">Category</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="empzCat">
                            <option value="this option">--</option>
                        </select>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label">Site</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="empzSite">
                            <option value="">cdo</option>
                            <option value="-cebu">cebu</option>
                        </select>
                    </div>
                </div>
		
            </form>

                    </div>
                </div>
					<input type="button" class="btn btn-xs btn-primary" id="btnShow" value="Show">
					<input type="button" class="btn btn-xs btn-success" id="btnExport" value="Export">
					<input type='button' class="btn  btn-xs btn-success" id="excel_button2" value='Excel_get_coverage'  style="display:none;"> 

     
                    </form>
                </div>
            </div>
        </div>
   
       
    </div>
<div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                
                <div class="example-box-wrapper">
						<!--<div id="loadingIMG" style="position: fixed; margin: -20% auto 0px; width: 100%; z-index: 1000; display: none;margin-left:20%;">
							 <img src="<?php echo base_url();?>assets/images/loading-gears-animation-3.gif"  > 
						</div>-->
                    <form class="form-horizontal bordered-row" role="form" id="tblShow2" hidden>
 						 <div id="tblShow">
							 <table id="datatable-row-highlight" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										 <td>	LastName</td>
										 <td>	First Name</td>
										 <td>	Position</td>
										 <td>	ATM</td>
										 <td>	Basic Salary</td>
										 <td>	Clothing</td>
										 <td>	Laundry</td>
										 <td>	Rice Subsidy</td>
										 <td>	ND</td>
										 <td>	OT-BCT</td>
										 <td>	HP</td>
										 <td>	HO</td>
										 <td style="width: 250px;">	Allowance</td>
										 <td style="width: 250px;">	Adjustment</td>
										 <td>	Gross Pay</td>
										 <td>	Tax</td>
										 <td>	SSS</td>
										 <td>	HDMF</td>
										 <td>	PHIC</td>
										 <td>	ABSENCE/UT/LATE</td>
										 <td>	Deductions</td>
										 <td>	Net</td>
									 </tr>
								</thead>	 
								<tbody id="tblData">
								</tbody>
							 </table>
                         </div>
                    </form>
                </div>
            </div>
        </div>
   
       
    </div>
	

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
				</div>
			</div>
		</div>
	</div>
</div>
 <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #545454;color: #ffffff;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id="fullnym"></span>  <span id="empsName"> </span> </h4>
      </div>
      <div class="modal-body">
	  <div id="DeductReload"></div>
	  <br>
		<span id="empsIs" hidden></span>
		<table class="blueTable">
		<thead>
			<tr>
			<th>Deduction</th>
			<th>Amount</th>
			</tr>
		</thead>
			<tbody id="detailedd"> </tbody>
		</table>
	 
       </div>
      <div class="modal-footer" id="footerEmp">
        <button type="button" class="btn btn-success" data-toggle='modal' data-target='#modalAddDeduction'>Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
	
  </div>
</div>
 <div id="modalAddDeduction" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #545454;color: #ffffff;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id="fullnym"></span> Deduction details</h4>
      </div>
      <div class="modal-body">
		<form class="form-horizontal bordered-row" >

				<div class="form-group">
                    <label class="col-sm-3 control-label">Type of Deduction</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="TypeDeduct">
                            <option disabled selected>--</option>
                            <option value="1">Loan</option>
                            <option value="2">Other Deduction</option>
                         </select>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label">Deduction</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="NameDeduction">
                            <option>--</option>
						</select>
                    </div>
                </div>
				<div class="form-group" id="isDeductVal" hidden>
                    <label class="col-sm-3 control-label">Value</label>
                    <div class="col-sm-6">
                         <input type="text" class="form-control"  placeholder="Value" id="DeductVal">
                    </div>
                </div>
       </form>
       </div>
      <div class="modal-footer" id="btnSampleDeduct" >
      </div>
    </div>

  </div>
</div>
     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>

</div>
</body>
<script>
  $(function(){
		// $("#btnShow").trigger('click'); 
		$('#sidebar-menu').load('<?php echo base_url(); ?>/index.php/Templatedesign/sideBar');
		$('#headerLeft').load('<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft');
  });	
</script> 

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
        $('#datatable-example').dataTable();
    });
    $(document).ready(function() {
        var table = $('#datatable-hide-columns').DataTable( {
            "scrollY": "300px",
            "paging": false
        } );

        $('#datatable-hide-columns_filter').hide();

        $('a.toggle-vis').on( 'click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column( $(this).attr('data-column') );

            // Toggle the visibility
            column.visible( ! column.visible() );
        } );
    } );

    /* Datatable row highlight */

    $(document).ready(function() {
        var table = $('#datatable-row-highlight').DataTable();

        $('#datatable-row-highlight tbody').on( 'click', 'tr', function () {
            $(this).toggleClass('tr-selected');
        } );
    });
    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>
<script>
  $(function(){
   $("#loads").hide();
 	function initResultDataTable(){
    $('#datatable-row-highlight').DataTable({
                        "order": [],
                        "columnDefs": [ {
                        "targets"  : 'no-sort',
                        "orderable": false,
                        }]
                });
	}
	  $("#empzType").change(function(){
			var val = $(this).val();
			  var Agent = new Array("Trainee","Probationary", "Regular");
			  var Admin = new Array("Trainee","Probationary_and_Regular","Confidential");
			// $("#empzCat").html();
			var opt="<option>--</option>";
				$.each(((val=="Admin") ? Admin : Agent),function(index,val){
					opt+="<option>"+val+"</option>";
					});
					$("#empzCat").html(opt);
	  });
		$("#excel_button2").click(function(){
			var payrollC = $("#payrollC").val();
 			var empzType = $("#empzType").val();
			var empzCat = $("#empzCat").val();
			var empzSite = $("#empzSite").val();
 			var dataString = "cid="+payrollC+"&empzCat="+empzCat+"&empzType="+empzType+"&empzSite="+empzSite;


			 $.ajax({
                     url: '<?php echo base_url() ?>index.php/payroll/getcoverage_date',
                    data:  dataString,
                     type: 'POST',
                     success:function(res)
						{
							var str = res.split("|");
							 window.open("<?php echo base_url() ?>reports/payroll/"+str[0]+"/"+str[1]+"/"+str[2]+"/report/PAYROLL_REPORT"+empzSite.toUpperCase()+"_"+str[5]+"_"+str[6]+"_"+str[0]+"_"+str[4]+"_"+str[2]+"_pr"+str[3]+".xlsx",'_blank');

						}
                });  
		});
	  $("#btnExport").click(function(){
			var payrollC = $("#payrollC").val();
 			var empzType = $("#empzType").val();
			var empzCat = $("#empzCat").val();
			var empzSite = $("#empzSite").val();
 			var dataString = "payrollC="+payrollC+"&empzCat="+empzCat+"&empzType="+empzType+"&empzSite="+empzSite;

				$.ajax({
                    url: '<?php echo base_url() ?>index.php/payroll/exportpayrolldetail_excel',
                    type: 'POST',
                    data:  dataString,
					beforeSend:function(){
						$("#loads").show();
						// $("#tblShow").html("");
					},
                    success:function(res){
						
						$("#excel_button2").trigger("click");
					}			
				});
	  });
	  $("#btnShow").click(function(){
			var payrollC = $("#payrollC").val();
 			var empzType = $("#empzType").val();
			var empzCat = $("#empzCat").val();
			var empzSite = $("#empzSite").val();
 			var dataString = "payrollC="+payrollC+"&empzCat="+empzCat+"&empzType="+empzType+"&empzSite="+empzSite;
			$.ajax({
                //url of the function
                    url: '<?php echo base_url() ?>index.php/payroll/extractData',
                    type: 'POST',
                    data:  dataString,
					beforeSend:function(){
						$("#loads").show();
						// $("#tblShow").html("");
					},
                    success:function(res)
                    { 
					$("#loads").hide();
					$("#tblShow2").show();
					var obj = jQuery.parseJSON(res);
 						 var add_pagibig2 = 0;
						 var employeeShare= 0;
						  var txt = '';
   						$.each(obj.payroll,function(index, value){
						var laundry = value["laundry"].split("{");
						var clothing = value["clothing"].split("{");
						var rice = value["rice"].split("{");
						var tax = value["tax"].split("(");
						var adj_b = value["adjust_details"].split(",");
						var bonus = value["bonus_details"].split(",");
						var deduction = value["deduct_details"].split(",");
						var sum_adjustmentbunos=0;
						var sum_bunos=0;
						var sum_adjustment=0;
						var sum_deduction=0;
						var gross =0;
						var net =0;
						var total =0;
						var lan = (laundry[0].trim()=="--") ? 0: laundry[0];
						var clo = (clothing[0].trim()=="--") ? 0: clothing[0];
						var ric = (rice[0].trim()=="--") ? 0:rice[0];
						txt+='<tr id="searched-row-'+index+'" class="js-result-tbl-tbody-tr">'+
						'<td>'+value["lname"]+'</td>'+
						'<td>'+value["fname"]+'</td>'+
						'<td>'+value["pos_name"]+'</td>'+
						'<td>'+value["isAtm"]+'</td>'+
						'<td>'+value["quinsina"]+'</td>'+
						'<td>'+lan+'</td>'+
						'<td>'+clo+'</td>'+
						'<td>'+ric+'</td>'+
						'<td>'+value["nd"]+'</td>'+
						'<td>'+value["ot_bct"]+'</td>'+
						'<td>'+value["hp"]+'</td>'+
						'<td>'+value["ho"]+'</td>';
						txt+='<td style="width: 300px;">';
							
								if(adj_b!=0){
									for(var i=0;i<adj_b.length;i++){
										var adjbonus = adj_b[i].split("|");
										
										if(adjbonus[0].trim()=="Bonus"){
											var adjvalbon = adjbonus[1].split("=");
											txt+= "* "+adjvalbon[0]+" = <u>"+parseFloat(adjvalbon[1]).toFixed(2)+"</u><br>";	
											sum_adjustmentbunos+=parseFloat(adjvalbon[1]);
 										}
										
									}
								} 
								
								if(bonus!=0){
									for(var i=0;i<bonus.length;i++){
 										
 											var valbon = bonus[i].split("=");
											txt+= "* "+valbon[0]+" = <u>"+parseFloat(valbon[1]).toFixed(2)+"</u><br>";
											sum_bunos+=parseFloat(valbon[1]);
  									}
								}  
 						txt+='</td>';
						txt+='<td style="width: 300px;">';
						if(adj_b!=0){
									for(var i=0;i<adj_b.length;i++){
										var adjbonus = adj_b[i].split("|");
										
										if(adjbonus[0].trim()!="Bonus"){
											var adjvalbon = adjbonus[1].split("=");
											txt+=  "* "+adjvalbon[0]+" = <u>"+parseFloat(adjvalbon[1]).toFixed(2)+"</u><br>";	
											sum_adjustment+=parseFloat(adjvalbon[1]);

 										}
									}
								} 
						txt+='</td>';
							  gross = parseFloat(value["quinsina"]) + parseFloat(clo)+ parseFloat(lan)+ parseFloat(ric)+ parseFloat(value["nd"]) + parseFloat(value["ot_bct"])+ parseFloat(value["hp"]) + parseFloat(value["ho"]) + parseFloat(sum_adjustmentbunos) + parseFloat(sum_bunos) + parseFloat(sum_adjustment);
							 
							 // console.log(value["lname"]+ " **** "+ +  parseFloat(value["quinsina"]) + " **** "+ parseFloat(clo) + " **** "+ parseFloat(lan)+ " **** "+ parseFloat(ric) + " **** "+  parseFloat(rice[0]) + " **** "+  parseFloat(value["nd"]) + " **** "+  parseFloat(value["ot_bct"])+ " **** "+  parseFloat(value["hp"]) + " **** "+  parseFloat(value["ho"]) + " **** "+  parseFloat(sum_adjustmentbunos) + " **** "+  parseFloat(sum_bunos) + " **** "+  parseFloat(sum_adjustment))
						txt+='<td>'+parseFloat(gross).toFixed(2)+'</td>';
						
 							txt+= '<td>'+tax[0]+'</td>'+
							 '<td>'+value["sss"]+'</td>'+
							 '<td>'+value["hdmf"]+'</td>'+
							 '<td>'+value["phic"]+'</td>'+
							 '<td>'+value["absentLate"]+'</td>';
							  txt+='<td>';
							 if(deduction!=0){
								 for(var i=0;i<deduction.length;i++){
 										
 											var valdeduction = deduction[i].split("=");
											txt+= "* "+valdeduction[0]+" = <u>"+parseFloat(valdeduction[1]).toFixed(2)+"</u><br>";
											sum_deduction+=parseFloat(valdeduction[1]);
  									}
								}
							 txt+='</td>';
							 net = parseFloat(tax[0]) + parseFloat(value["sss"]) + parseFloat(value["hdmf"]) + parseFloat(value["phic"]) + parseFloat(value["absentLate"]) + parseFloat(sum_deduction);
							 total = parseFloat(gross) - parseFloat(net);
							 txt+='<td>'+parseFloat(total).toFixed(2)+'</td>'+

						'</tr>';
						sum_adjustmentbunos=0;
						sum_adjustment=0;
						sum_deduction=0;
						sum_bunos=0;
						gross=0;
						net=0;
						total=0;
							
						});	
						// $("#tblData").empty();
 						
						 // $('#datatable-row-highlight').DataTable().destroy();
						if ( $.fn.DataTable.isDataTable('#datatable-row-highlight') ) {
							  $('#datatable-row-highlight').DataTable().destroy();
							}

							$('#datatable-row-highlight tbody').empty();
							$('#tblData').html(txt);
							// ... skipped ...

							$('#datatable-row-highlight').dataTable({
								  "autoWidth":true
								, "info":true
								, "iDisplayLength":10
								, "JQueryUI":true
								, "ordering":true
								, "searching":true
								, "paging":true
								,"bFilter": false
								,"scrollX": true
								, "scrollCollapse":true
								,  "order": [[ 0, "asc" ]]
							});
						// initResultDataTable();
                    }
                });   
	  });
 
	});	
</script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
   
 </html>