<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  .tableheader{
    background: #4CAF50 !important;
    color: white !important;
  }
  .breaker{
    color:#888;
  }
</style>

<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript">
  $(window).load(function(){
   setTimeout(function() {
    $('#loading').fadeOut( 400, "linear" );
  }, 300);
   $("#tab1").trigger("click");
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Schedule/getSupervisoryAgentEmp",
    cache: false,
    success: function(res)
    { 
      if(res=="Empty"){
       $("#employeelist").prop('disabled',true);
       $("#employeelist").multiSelect('refresh');
       $("#select").prop('disabled',true);
       $("#deselect").prop('disabled',true);
       $("#generateschedule").prop('disabled',true);
     }else{
      $("#employeelist").html('');
      $.each(JSON.parse(res),function (i,elem){
        $('#employeelist').multiSelect('addOption', { value: elem.emp_id, text: elem.lname+", "+elem.fname, index: 0 });  
      });

      $("#employeelist").multiSelect('refresh');
      $("#generateschedule").prop('disabled',false);
    }

  }
});
 });

</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>

<!-- Multi select -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
<script type="text/javascript">
  /* Multiselect inputs */

  $(function() { "use strict";
    $("#employeelist").multiSelect();
    $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
  });
</script> 
<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
 $(function(){
  "use strict";
  var m = moment().format("MM/DD/YYYY");
  $("#daterangepicker-example").daterangepicker({ startDate: m,maxDate: m});    
  $("[name=daterangepicker_start]").attr('readonly',true);
  $("[name=daterangepicker_end]").attr('readonly',true);
});

</script>
</head>

<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">
        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div id="page-title">
          <h3>ATTENDANCE MONITORING</h3>
          <hr>
          <div class="content-box collapse in" id='generater'>
            <div class="content-box-wrapper">
              <div class="row">
                <div class="col-md-12"  id='employeelistdiv'>
                  <div class="row">

                    <div class="col-md-7">
                      <label class='control-label'>Date Range</label>
                      <div class="input-prepend input-group">
                        <span class="add-on input-group-addon">
                          <i class="glyph-icon icon-calendar"></i>
                        </span>
                        <input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-5">
                      <label class='control-label'>Select Option</label><br>
                      <div class="btn-group">
                        <button id="select" class='btn btn-primary btn-sm'>Select all</button>
                        <button id="deselect" class='btn btn-primary btn-sm'>Deselect all</button>
                      </div>
                    </div>
                    <div class="col-md-12"><br>
                      <label class='control-label'>Employee List</label>
                      <select multiple name="" id="employeelist"  class="multi-select"></select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="button-pane text-right">
              <button class="btn btn-primary" id='submitter' >View Result</button>
            </div>
          </div>

          <div class="loading-spinner" id='loader' style='display:none'>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
          </div>
          <div  id='generateresult'>
            <div id='resulttables'>

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- JS Demo -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
  </div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $(function(){
    $("#acc_description").on('change',function(){
      $("#employeelist").html("");
      $("#employeelist").multiSelect('refresh');
    })
  })
</script>

<script type="text/javascript">
  $(function(){
    $('#select').click(function(){

      $("#employeelist").multiSelect('select_all');
    });
  })
</script>
<script type="text/javascript">
  $(function(){

    $('#deselect').click(function(){

      $("#employeelist").multiSelect('deselect_all');
    });
  })
</script>

<script type="text/javascript">
  $("#submitter").click(function(){

    $("#generateresult").hide();
    var date = $("#daterangepicker-example").val();
    if(date==null||date==''){
      alert("date empty");
    }else{

      var dater = date.split(" - ");
      var fromDateRaw = dater[0];
      var fromDateRaw2 = fromDateRaw.split("/");
      var fromDate = fromDateRaw2[2]+"-"+fromDateRaw2[0]+"-"+fromDateRaw2[1];
      var toDateRaw = dater[1];
      var toDateRaw2 = toDateRaw.split("/");
      var toDate = toDateRaw2[2]+"-"+toDateRaw2[0]+"-"+toDateRaw2[1];
      var acc_description = $("#acc_description").val();
      var employeelist = $("#employeelist").val();
      if(employeelist==''){
        $.jGrowl("Empty Selection",{sticky:!1,position:"top-right",theme:"bg-red"});
      }else{
        var emplist = employeelist.toString().split(",");
        employeelist = emplist.join('-');
        employeelist = employeelist.replace(/\s+/g, '');
       // alert(employeelist);
       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/AttndMonitoring/getAttendancelist",
        data: {
          fromDate : fromDate,
          toDate : toDate,
          employeelist : employeelist
        },
        cache: false,
        beforeSend: function() {
          $("#loader").show();
        },
        success: function(ress)
        { 
            // alert(ress);
            ress = $.trim(ress);
            $("#generateresult").show(500);

            $("#resulttables").html("");
            $.each(JSON.parse(ress),function (i,elem){            
              var tbody = "<table class='table table-bordered table-condensed font-size-12 font-black'>";
              // tbody += "<thead>";
              tbody += " <tr>";
              tbody += " <th class='tableheader' width='80px'>Full Name</th>";
              tbody += " <th class='tableheader'>Date</th>";
              tbody += "<th class='tableheader' colspan=4>Schedule</th>";
              tbody += " <th class='tableheader'>Login</th>";
              tbody += " <th class='tableheader' colspan=2>First Break</th>";
              tbody += "<th class='tableheader' colspan=2>Lunch Break</th>";
              tbody += "<th class='tableheader' colspan=2>Last Break</th>";
              tbody += " <th class='tableheader'>Logout</th>";
              tbody += "<th class='tableheader'>Total Breaks</th>";
              tbody += "<th class='tableheader'>Total Hours</th>";
              tbody += "<th class='tableheader' width='80px'>Log Status</th>";
              tbody += "</tr>";
              // tbody += " </thead>";
              tbody += " <tbody>";
              $.each(elem.details,function (x,detail){   
                tbody += "<tr>";
                tbody += "<td width='80px'>"+elem.lname+", "+elem.fname+"</td>";
                tbody += "<td>"+detail.date+"</td>"; 
                if(detail.schedule==null){
                  tbody += "<td colspan=4 class='text-danger text-center'>No Schedule set</td>"; 
                }else if(detail.schedule.type=='Normal'){
                  tbody += "<td>"+detail.schedule.time_start+"</td>"; 
                  tbody += "<td colspan=3>"+detail.schedule.time_end+"</td>";   
                   // tbody += "<td>"+detail.schedStat+"</td>"; 
                }else if(detail.schedule.type=='WORD'){
                  tbody += "<td>"+detail.schedule.time_start+"</td>"; 
                  tbody += "<td>"+detail.schedule.time_end+"</td>"; 
                  tbody += "<td colspan=2>"+detail.schedule.type+"</td>"; 
                   // tbody += "<td>"+detail.schedStat+"</td>"; 
                }else{
                  tbody += "<td colspan=4 class='font-red' style='text-align: center;'>"+detail.schedule.type+"</td>"; 
                }

                if(detail.logs==null){
                  tbody += "<td>No Login</td>"; 
                }else if(detail.logs.log_out==null){
                  tbody += "<td>"+detail.logs.log_in_view+"</td>";
                }else{
                  tbody += "<td>"+detail.logs.log_in_view+"</td>"; 
                }
                if($.isEmptyObject(detail.firstbreak)){
                  tbody += "<td class='breaker' colspan=2>No Logs</td>";
                }else if(Object.keys(detail.firstbreak).length==1){
                  tbody += "<td class='breaker'>"+detail.firstbreak[0].logview+"</td><td class='breaker'>Current Break</td>";
                }else{
                  tbody += "<td class='breaker'>"+detail.firstbreak[0].logview+"</td><td class='breaker'>"+detail.firstbreak[1].logview+"</td>";
                }
                if($.isEmptyObject(detail.lunchbreak)){
                  tbody += "<td class='breaker' colspan=2>No Logs</td>";
                }else if(Object.keys(detail.lunchbreak).length==1){
                  tbody += "<td class='breaker'>"+detail.lunchbreak[0].logview+"</td><td class='breaker'>Current Break</td>";
                }else{
                  tbody += "<td class='breaker'>"+detail.lunchbreak[0].logview+"</td><td class='breaker'>"+detail.lunchbreak[1].logview+"</td>";
                }
                if($.isEmptyObject(detail.lastbreak)){
                  tbody += "<td class='breaker' colspan=2>No Logs</td>";
                }else if(Object.keys(detail.lastbreak).length==1){
                  tbody += "<td class='breaker'>"+detail.lastbreak[0].logview+"</td><td class='breaker'>Current Break</td>";
                }else{
                  tbody += "<td class='breaker'>"+detail.lastbreak[0].logview+"</td><td class='breaker'>"+detail.lastbreak[1].logview+"</td>";

                }
                if(detail.logs==null){
                  tbody += "<td>No Logout</td>"; 
                }else if(detail.logs.log_out==null){
                  tbody += "<td>No Logout</td>"; 
                }else{ 
                  tbody += "<td>"+detail.logs.log_out_view+"</td>"; 
                }
                tbody += "<td>"+detail.totalbreaks+"</td>"; 
                tbody += "<td>"+detail.totalhours+"</td>";
                if(detail.schedule==null){
                  tbody += detail.status; 
                }else if(detail.schedule.type=='Normal'&&detail.logs==null){
                  tbody += "<td width='80px' class='font-red'>Absent</td>"; 
                }else if(detail.schedule.type=='WORD'&&detail.logs==null){
                  tbody += "<td width='80px' class='font-red'>Absent</td>";  
                }else{
                  tbody += detail.status; 
                }
               
                tbody += "</tr>";
              });
              tbody += "</tbody>";
              tbody += " </table>";
              $("#resulttables").append(tbody);
            });
$("#loader").hide();
}
});
}


}

})
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html> 
                 <!--<th>Full Name</th>
                    <th>Date</th>
                    <th>Schedule</th>
                    <th>Login</th>
                    <th>Logout</th>
                    <th>First Break</th>
                    <th>Lunch Break</th>
                    <th>Last Break</th> -->