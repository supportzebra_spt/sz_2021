<!DOCTYPE html> 
<html  lang="en" >

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
 
		body {
		font-family: Helvetica, sans-serif;
		height: 100%;
		background-image: url(../../assets/images/sped/background.png);
		background-size: contain;
		}
 		#results { float:right; margin:20px; padding:20px; border:1px solid; background:#ccc;visibility:hidden; }
		 
		#td{
			padding-left:30px;
			vertical-align: middle;
			width: 300px;
		}
		 .col-sm-3 .control-label{
			color:white;
		}
		#ball{
 
			background-image: url("<?php echo base_url();?>assets/images/sped/ZEBRACKET_PER_PAGE.gif");
			height: 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-size: contain;
			background-color: black;
			z-index: 210;
		}
		#ball2{
 
			background-image: url("<?php echo base_url();?>assets/images/sped/GIF_before_final_resume_SZ_ThankYou.gif");
			height: 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-size: contain;
			background-color: white;
			z-index: 210;
		}
		#blah {
			width: 40%;
			margin-left: 20%;
			margin-top: -70%;
		}
		#panelTab2 {
			 width: 70%;
			 margin: 0 auto;
			 background: #0b1314;
			 border-width: thick;
 			 margin-right: 0px;
		}
		#panelMiniTab2 {
			width: auto;
			overflow: auto;
			height: 450px;
			z-index: 110;
			margin-top: -100px;
			overflow-x: hidden;

		}
		#panelMiniTab3 {
			width: 100%;
			overflow: auto;
			height: 400px;
			z-index: 110;
			margin-top: -100px;
			overflow-x: hidden;

		}
		#panelMiniTab4 {
			width: 100%;
			overflow: auto;
			height: 400px;
			z-index: 110;
			margin-top: -100px;
			overflow-x: hidden;

		}
		#panelTab3 {
			width: 50%;
			background: #0b1314;
			border-width: thick;
			position: absolute;
			right: 5%;
			height: 500px;
 		}
		#panelTab4 {
			width: 55%;
			margin: 0 auto;
			background: #0b1314;
			border-width: thick;
			right: 0px;
			position: absolute;
 		}
 		#image {
			height: 500px;
			margin-left: 360px;
			position: relative;
			margin-top: -600px;
			z-index: 70;
			width: 400px;			
		}
		label.col-sm-3.control-label {
			color: white;
		}
		#Lanch1{
			/*width: 70%;*/
			margin-left: 10%;
			margin-top: -70%;;
		}
		#Lanch0{
			width: 20%;
			margin-top: -50%;
			z-index: 50;
			position: absolute;
			margin-left: 40%;
		}
		
		.Lanch2{
			width: 20%;
			margin-left: 45%;
			margin-top: -20%;
		}
 		#tabOneCloud{
			width: 100%;
			z-index: 80;
			bottom: 0px;
			position: fixed;
			left: 0px;
		}
 		#tabTwoCloud{
			margin-top:120px;
			width: 100%;
			z-index: 80;
			bottom: 0px;
			position: fixed;
			left: 0px;
		}
	
 
				@media screen and (width: 768px) {
					#blah {
						width: 70%;
						margin-left: 10%;
						margin-top: 20px;
						}
					#panelMiniTab2 {
						 width: auto;
						 overflow: auto;
						 height: 650px;
						 overflow-x: hidden;
					}
					#image{
						    height: 600px;
							margin-left: 140px;
							margin-top: -590px;
							z-index: 81;
					}
					#panelMiniTab3 {
						width: 100%;
						overflow: auto;
 						margin-top: -100px;
						overflow-x: hidden;
						z-index: 110;
						height: 650px;
					}
					#panelTab3  {
						    width: 60%;
							background: #0b1314;
							border-width: thick;
							position: absolute;
							right: 5%;
							height: 700px;
 					}
					#Lanch1{
					    width: 100%;
						margin-top: -30%;
						margin-left: 0px;
					}
					.Lanch2{
					    width: 35%;
						margin-left: 40%;
						margin-top: 0px;
					}
					#tabOneCloud{
						width: 200%;
						z-index: 80;
						bottom: 0px;
						position: fixed;
						margin-left: -39%;
					}	
					#tabTwoCloud{
						margin-top: 120px;
						width: 204%;
						z-index: 80;
						bottom: 0px;
						position: fixed;
						margin-left: -36%;
					}	
					#Lanch0{
						width: 30%;
						margin-top: -30%;
						z-index: 50;
						position: absolute;
						margin-left: 35%;
					}					
				}
			 
		 
	</style>

    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> SUPPORTZEBRA | APPLICATION FORM </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'yyyy-mm-dd'
        });
    });

</script>
 
<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>
 <script src="<?php echo base_url();?>assets/js/angular.min.js"></script>

 <script type="text/javascript">
    /* Input masks */

    $(function() { "use strict";
        $(".input-mask").inputmask();
    });

</script>

 <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/uniform/uniform.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/uniform/uniform.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/uniform/uniform-demo.js"></script>

      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">
 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="<?php echo base_url();?>assets/js/jquery.addressPickerByGiro.js"></script>
 <script src="http://maps.google.com/maps/api/js?sensor=false&language=en"></script>
 <link href="<?php echo base_url();?>assets/css/jquery.addressPickerByGiro.css" rel="stylesheet" media="screen">
<!-- Bootstrap Wizard -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/wizard/wizard.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/wizard/wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/wizard/wizard-demo.js"></script>
 
</head>


    <body ng-app="app" ng-controller="ctrl">
 		<div id="rocket1" style="width:100%;height:100%;position:fixed;z-index:400;" hidden>
			<div id="ball">
			</div>
		</div>
		<div id="rocket2" style="width:100%;height:100%;position:fixed;z-index:400;" hidden>
			<div id="ball2">
			</div>
		</div>
     <div >
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper" >
      
         
 <div>
     <div class="panel-body">
        <h3 class="title-hero">
         </h3>
        <div class="example-box-wrapper">
            <div id="form-wizard-1" >
               <!-- <ul>
                    <li>
                        <a href="#step1" data-toggle="tab" id="step0">
                            <label class="wizard-step">1</label>
                      <span class="wizard-description">
                         Picture Taking
                       </span>
                        </a>
                    </li>
					<li>
                        <a href="#step2" data-toggle="tab" id="step1">
                            <label class="wizard-step">2</label>
                      <span class="wizard-description">
                         Personal Information
                       </span>
                        </a>
                    </li>
                    <li>
                        <a href="#step3" data-toggle="tab">
                            <label class="wizard-step">3</label>
                      <span class="wizard-description">
							Application  
                       </span>
                        </a>
                    </li>
                    <li>
                        <a href="#step4" data-toggle="tab">
                            <label class="wizard-step">4</label>
                      <span class="wizard-description">
                         Character References
                       </span>
                        </a>
                    </li>
                    <li>
                        <a href="#step5" data-toggle="tab" id="confirm">
                            <label class="wizard-step">5</label>
                      <span class="wizard-description">
                         Confirmation
                       </span>
                        </a>
                    </li>
                </ul> 
				      
				     <ul style="display:none;">-->
				
				     <ul style="display:none;">
                    <li><a href="#tab0" data-toggle="tab" id="clickwala">Zero</a></li>
                    <li><a href="#tab1" data-toggle="tab" id="clickUna">First</a></li>
                    <li><a href="#tab2" data-toggle="tab" id="clickDos">Second</a></li>
                    <li><a href="#tab3" data-toggle="tab" id="clickTres">Third</a></li>
                    <li><a href="#tab4" data-toggle="tab" id="clickkwatro">Forth</a></li>
                    <li><a href="#tab5" data-toggle="tab" id="clickSingco">Fifth</a></li>
					</ul>
                <div class="tab-content">
					<div class="tab-pane active" id="tab0" >
					<div  >
                        <img src="<?php echo base_url()."assets/images/sped/HOME_BG.jpg"?>"  style="width:100%">
<!--<input type="button" value="LAUNCH" id="btnLanch" class="btn btn-l btn-warning" style="width: 10%;margin-left: 45%;margin-top: -60%;">-->
                        <img src="<?php echo base_url()."assets/images/sped/SUPPORTZEBRA_FINAL_WHITE.png"?>"  id="Lanch0"  >
                        <img src="<?php echo base_url()."assets/images/sped/HOME PAGE.gif"?>"  id="Lanch1"  >
                        <img src="<?php echo base_url()."assets/images/sped/btnLaunch.png"?>"  id="btnLanch" class="Lanch2"  >

                    </div>
                    </div>
					<div class="tab-pane" id="tab1">
                        <div class=" ">
                          
 					<!--<input type="file" accept="image/*" capture="camera"   onchange="readURL(this);" class="fileinput-new"/>-->
					<div>
                        <div class="fileinput fileinput-new" data-provides="fileinput" style="text-align: center;">
                            <span class="btn-file">
									<img src="<?php echo base_url()."assets/images/sped/PHOTO_FRAME2.png"?>"  id="blah" >
									<input type="file"  onchange="readURL(this);" id="image" name="image" >
                            </span>
                        </div>
					</div>
						<script src="<?php echo base_url();?>assets/assets/cam/assets/js/script.js"></script>
                        </div>
                         <span>	
						  <img src="<?php echo base_url()."assets/images/sped/PHOTO_CLOUDS.png"?>" id="tabOneCloud"> 

						 <img src="<?php echo base_url()."assets/images/sped/next.png"?>" id="una" style="position: fixed;margin-left: 78%;z-index: 200;width: 15%;bottom: 10px;">
						</span>
                      
                    </div>
                    <div class="tab-pane" id="tab2" >
                        
                         
                            <div class="panel" id="panelTab2">
								<div class="panel-body"> 
									<div class="example-box-wrapper">
			 						<form class="form-horizontal bordered-row" id="demo-form" name="myForm" >
											<div class="row">
						<img src="<?php echo base_url()."assets/images/sped/HOME_ROCKET.png"; ?>" style="width: 40%;margin-left: -35%;z-index:130;margin-top: -10px;background: #0b1314;">

												<div class="col-md-5" id="panelMiniTab2" >
						
 						<div class="form-group personal-information"  >
						<div class="form-group">
							<label class="col-sm-3 control-label">Fullname : </label>
							<div class="col-sm-8">
								<input type="text" placeholder="Firstname" class="form-control" id="fname" name="fname" ng-model="fname" required> 
							</div>
  						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">  </label>
							<div class="col-sm-8">
								<input type="text" placeholder="Middlename" required class="form-control" id="mname" name="mname" ng-model="mname">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">  </label>
							<div class="col-sm-8">
								<input type="text" placeholder="Lastname" required class="form-control" id="lname" name="lname" ng-model="lname">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">  </label>
							<div class="col-sm-8">
						<select class="form-control "  id="suffix"  name="suffix" ng-model="suffix"  >
						<option> </option>
						<option>Jr.</option>
						<option>Sr.</option>
						<option>I</option>
						<option>II</option>
						<option>III</option>
						<option>IV</option>
						<option>IV</option>
 
						</select>
						</div>
						</div>
						</div>
									
									<div class="form-group personal-information">
										<label class="col-sm-3 control-label">Religion  : </label>
										<div class="col-sm-8">
                        <select class="form-control "  id="religion" name="religion" ng-model="religion" required>
 						<option>Roman Catholic</option>
						<option>Evangelical</option>
						<option>Iglesia Ni Cristo</option>
						<option>Seventh-day Adventist</option>
						<option>Jehovahs Witnesses</option>
						<option>Jesus is Lord Church</option>
						<option>United Church of Christ in the Philippines</option>
						<option>Others</option>

						</select>
						<input type="text" placeholder="Specify" id="relSpecific" required class="form-control" >
 
										</div>
									</div>
									
									<div class="form-group personal-information">
										<label class="col-sm-3 control-label">Civil Status  : </label>
										<div class="col-sm-8">
											<select class="form-control "  id="cs" name="cs" ng-model="cs"  >
											<option>Single</option>
											<option>Married </option>
											<option>Widowed </option>
											<option>Divorced</option>
											</select> 
										</div>
									</div>
									<div class="form-group personal-information">
										<label class="col-sm-3 control-label">Contact Number  : </label>
										<div class="col-sm-8">
											<input type="text" class="input-mask form-control" name="cm1" id="cm1" data-inputmask="'mask':'99999999999'" ng-model="cm1" required>
											<div class="help-block"><i>e.g. 09171234567</i></div>

										</div>
									</div>
									<div class="form-group personal-information">
										<label class="col-sm-3 control-label">Alternative Contact Number  : </label>
										<div class="col-sm-8">
										<input type="text" class="input-mask form-control" data-inputmask="'mask':'99999999999'" name="cm2" id="cm2"  ng-model="cm2" required >
											<div class="help-block"><i>e.g. 09171234567</i></div>

										</div>
									</div>
								 		<div class="form-group personal-information bdaychk">
                     <label for="" class="col-sm-3 control-label">Birthday</label>
										<div class="col-sm-8">
                            <div class="col-sm-8">
                                <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon">
                                        <i class="glyph-icon icon-calendar"></i>
                                    </span>
                                    <input type="text" class="bootstrap-datepicker form-control"   data-date-format="mm/dd/yy" id="bday" name="bday" ng-model="bday"  readonly="readonly" ><span id="age"></span>
                                </div>
                            </div>
										</div>
									</div>	
			<div class="form-group personal-information">
										<label class="col-sm-3 control-label">Gender  : </label>
										<div class="col-sm-8">
                        <select class="form-control "  id="gender" name="gender" ng-model="gender" required >
 						<option>Male</option>
						<option>Female </option>
 
						</select>
  
										</div>
									</div>
						
						<hr>
						
									<div class="form-group personal-information" id="presentaddressHover">
							<label class="col-sm-3 control-label"> Present Address</label>
							<div class="col-sm-9">
									<div class="container-fluid">
										  <div class="row-fluid">
											<div class="row-fluid">
												<form autocomplete="off" class="form-horizontal">
												<div class="row-fluid">
												  <div class="span6">
													<div class="row-fluid">
 														<div class="controls">
														  <!--<input type="text" class="input-xxlarge form-control" autocomplete="off" placeholder="Please tap me to input your address"   data-toggle="modal" data-target="#myModal">-->
														<label class="input-xxlarge form-control" data-toggle="modal" data-target="#myModal" id='presentOutput'  style="height: 100%;"> Please tap me to input your address</label>

														</div>
													</div>			  
												  </div>
												  
												</div>
 												</form>
											  </div><!--/span-->
										
											</div><!--/span-->
										  </div><!--/row-->	
							</div>
						</div>

							<div class="form-group personal-information"  id="permanentaddressHover">
							<label class="col-sm-3 control-label"> Permanent Address</label>
							<div class="col-sm-9">
									<div class="container-fluid">
										  <div class="row-fluid">
											<div class="row-fluid">
												<form autocomplete="off" class="form-horizontal">
												<div class="row-fluid">
												  <div class="span6">
													<div class="row-fluid">
 														<div class="controls">
														 <!-- <input type="text" class="input-xxlarge form-control"   autocomplete="off" placeholder="Please tap me to input your address" style="width: 100%;" data-toggle="modal" data-target="#myModal2" > -->
															<label class="input-xxlarge form-control" data-toggle="modal" data-target="#myModal2" id='permanentOutput' style="height: 100%;"> Please tap me to input your address</label>

														</div>
													</div>			  
												  </div>
												 
												</div>
 
												</form>
											  </div><!--/span-->
											 
											</div><!--/span-->
										  </div><!--/row-->	
							</div>
						</div>					 
								</div>
											 
											</div>
										 
										</form>
									</div>
								</div>
							</div>
					  
 						   <span>
							<img src="<?php echo base_url()."assets/images/sped/previous.png"?>" id="Backdos" style=" position: fixed;margin-left: 4%;z-index: 200;width: 20%;bottom:0px;">
						   </span>
 						<span>	

						 <img src="<?php echo base_url()."assets/images/sped/next.png"?>" id="dos" style=" position: fixed;margin-left: 75%;z-index: 200;width: 15%;bottom:0px;">
						</span> 
  					    <span>	

						<img src="<?php echo base_url()."assets/images/sped/PHOTO_CLOUDS_PERSONAL.png"?>" id="tabTwoCloud"> 
						</span> 
                    </div>
                    <div class="tab-pane" id="tab3">
                      
                   
                        <div class="panel" id="panelTab3">
								<div class="panel-body">
									<div class="example-box-wrapper">
									<img src="<?php echo base_url()."assets/images/sped/HOME_ROCKET.png"; ?>" style="width: 40%;margin-left: -35%;z-index:130;margin-top: -10px;background: #0b1314;">

										<form class="form-horizontal bordered-row" id="demo-form" name="myForm2">

											<div class="row">
												<div class="col-md-8" id="panelMiniTab3">
												<div class="form-group">
									<label class="col-sm-3 control-label">Position applied * : </label>
									<div class="col-sm-8"  >
							 
										<select class="form-control popover-button-default" data-content="If the desired position is unavailable, please choose <b>Pool</b>." title="<font color='green'><b>Note:</b></font>" data-trigger="hover" data-placement="top"   name="position"   id="position" required ng-model="position">
										<option selected disabled>--</option>
									<?php foreach($data as $key =>$val){ ?>
										<option value="<?php echo $val['pos_details']; ?>"><?php echo $val['pos_details']; ?></option>
									<?php } ?>
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="col-sm-3 control-label">Call Center Experience * : </label>
									<div class="col-sm-8" >
							 
										<select class="form-control"    name="ccexpo"   id="ccexpo" required ng-model="ccexpo" required>
										<option selected disabled>--</option>
										<option>Yes</option>
										<option>None</option>
								 
										</select>
									
									</div>
									
								</div>
								<div class="form-group">
										<div id="ccexpoDiv" hidden>
											<div class="form-group">
														<label class="col-sm-3 control-label">Previous Company </label>
														<div class="col-sm-8" >
															<input type="text"   class="form-control" name="pc1" id="pc1" ng-model="pc1" required>
														</div>
											</div>
											<div class="form-group">
														<label class="col-sm-3 control-label">Last year attended</label>
														<div class="col-sm-8" >
															<input type="text"  class="form-control" name="ly1" id="ly1" ng-model="ly1" required>
														</div>
											</div>
										</div>
								</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Source: </label>
										<div class="col-sm-8">
											<input type="text" placeholder="Source" required class="form-control" id="source" name="source" ng-model="source">
										</div>
									</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Do you have a referral?* : </label>
									<div class="col-sm-8" >
							 
										<select class="form-control" id="refAsk"  required>
										<option selected disabled>--</option>
										<option>Yes</option>
										<option>None</option>
								 
										</select>
									
									</div>
									
								</div>
								 <div class="referenceNum" hidden>
												<div class="form-group">
												<label class="col-sm-3 control-label">Referral #1 : </label>

  													<div class="col-sm-8" >
														<!--<input type="text" placeholder="Fullname"  class="form-control" name="rfn1" id="rfn1" ng-model="rfn1" required>-->
														<div class="form-group">
															<div class="form-group">
																<label class="col-sm-1 control-label">  </label>

																<div class="col-sm-10">
																	<input type="text" placeholder="Firstname" class="form-control" id="rfn1" name="rfn1" ng-model="rfn1" required> 
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-1 control-label">  </label>

																<div class="col-sm-10">
																	<input type="text" placeholder="Middlename" required class="form-control" id="rmn1" name="rmn1" ng-model="rmn1">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-1 control-label">  </label>
																<div class="col-sm-10">
																	<input type="text" placeholder="Lastname" required class="form-control" id="rln1" name="rln1" ng-model="rln1">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-1 control-label">  </label>

																<div class="col-sm-10">
															<select class="form-control "  id="rsuffix1"  name="rsuffix1" ng-model="rsuffix1" placeholder="suffix" >
															<option>Jr.</option>
															<option>Sr.</option>
															<option>I</option>
															<option>II</option>
															<option>III</option>
															<option>IV</option>
															<option>IV</option>
									 
															</select>
															</div>
															</div>
															<div class="form-group">
																<label class="col-sm-1 control-label">  </label>
																<div class="col-sm-10" >

																		<input type="text" class="input-mask form-control" placeholder="Contact number" data-inputmask="'mask':'99999999999'" name="rcm1" id="rcm1" ng-model="rcm1" required>
																		<div class="help-block"><i>e.g. 09171234567</i></div>
																</div>
															</div>
														</div>
														
														</div>
													 
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label">  </label>
														<div class="col-sm-6" >
													<input type="button" class="btn btn-xs btn-primary" id="btnrf1" value="Add one"> 
														</div>
													</div>
													 </div>
													 <div class="referenceNum1" hidden>
													 <hr>
													<div class="form-group">
													<label class="col-sm-3 control-label">Reference #2: </label>
													<div class="col-sm-6" >
														<input type="hidden" placeholder="Fullname"  class="form-control" name="rfn2" id="rfn2" ng-model="rfn2" required> 
														<div class="form-group">
							<label class="col-sm-1 control-label">  </label>

							<div class="col-sm-10">
 								<input type="text" placeholder="Firstname" class="form-control" id="rfn2" name="rfn2" ng-model="rfn2" required> 
							</div>
  						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label">  </label>

 							<div class="col-sm-10">
								<input type="text" placeholder="Middlename" required class="form-control" id="rmn2" name="rmn2" ng-model="rmn2">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label">  </label>
 							<div class="col-sm-10">
								<input type="text" placeholder="Lastname" required class="form-control" id="rln3" name="rln3" ng-model="rln3">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label">  </label>

 							<div class="col-sm-10">
						<select class="form-control "  id="rsuffix2"  name="rsuffix2" ng-model="rsuffix2" placeholder="suffix" >
 						<option>Jr.</option>
						<option>Sr.</option>
						<option>I</option>
						<option>II</option>
						<option>III</option>
						<option>IV</option>
						<option>IV</option>
 
						</select>
						</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label">  </label>
							<div class="col-sm-10" >

									<input type="text" class="input-mask form-control" placeholder="Contact number" data-inputmask="'mask':'99999999999'" name="rcm2" id="rcm2" ng-model="rcm2" required>
									<div class="help-block"><i>e.g. 09171234567</i></div>
							</div>
						</div>
													</div>
													 
													</div>
													
												 
													<div class="form-group">
														<label class="col-sm-3 control-label">  </label>
														<div class="col-sm-6" >
													<input type="button" id="btnrf2" class="btn btn-xs btn-primary" value="cancel"> 
														</div>
													</div>
													 </div>
													 
								
								
								
												 </div>
											 
												 </div>
												 </form>
												 </div>
												 </div>
												 </div>
					  
							<span>
								<img src="<?php echo base_url()."assets/images/sped/previous.png"?>" id="Backtres" style="margin-top: 25%;position: fixed;margin-left: 4%;z-index: 200;width: 20%;bottom:0px;">
						   </span>
 
							<span>	
								<img src="<?php echo base_url()."assets/images/sped/next.png"?>" id="tres" style="margin-top: 25%;position: fixed;margin-left: 75%;z-index: 200;width: 15%;bottom:0px;">
							</span>
						<img src="<?php echo base_url()."assets/images/sped/PAGE_3_CLOUDS_ROCKET_APPLICATION.png"?>" style="margin-top:120px;width: 100%;z-index: 80;bottom: 0px;position: fixed;left: 0px;"> 
							

                    </div>
                    <div class="tab-pane" id="tab4">
                       
                            
                        <div class="panel" id="panelTab4">
								<div class="panel-body">
									<div class="example-box-wrapper">
										<form class="form-horizontal bordered-row" id="demo-form" name="myForm3">
										 
												 </form>
												 <div class="row">
	<img src="<?php echo base_url()."assets/images/sped/HOME_ROCKET.png"; ?>" style="width: 40%;margin-left: -35%;margin-top: -30px;margin-top: -30px;background: #0b1314;">

            <div class="col-md-4"  id="panelMiniTab3">
                <div  style="height: 300px;">
                     <div  style="padding: 10px;color: #2bb672;text-align:center;">
                        <span><b>CHARACTER REFERENCE #1</b></span>
                     </div>

 												<div class="form-group">
												<label class="col-sm-3 control-label">Fullname</label>
													<div class="col-sm-8">				
													<input type="text" class="form-control" id="crfn1" name="crfn1" placeholder="Firstname" ng-model="crfn1" required>
													
													<input type="text" placeholder="Middlename" required class="form-control" id="crmn1" name="crmn1" ng-model="crmn1">
													
													<input type="text" placeholder="Lastname" required class="form-control" id="crln1" name="crln1" ng-model="crln1">
													
													<select class="form-control "  id="crsuffix1"  name="crsuffix1" ng-model="crsuffix1" placeholder="suffix" >
													<option>Jr.</option>
													<option>Sr.</option>
													<option>I</option>
													<option>II</option>
													<option>III</option>
													<option>IV</option>
													<option>IV</option>
							 
													</select>
												
												</div> 
											 
												<label class="col-sm-3 control-label">Contact Number</label>
													<div class="col-sm-8">		
													<input type="text" class="input-mask form-control" data-inputmask="'mask':'99999999999'" placeholder="Contact number" id="crm1" name="crm1" ng-model="crm1" required>
													<div class="help-block"><i>e.g. 09171234567</i></div>
													</div> 
										 
												<label class="col-sm-3 control-label">Profession</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="crp1" name="crp1" placeholder="Profession" ng-model="crp1" required>
												
												</div> 
												</div> 
													<div class="form-group">
 														<div class="col-sm-6" >
													<input type="button" class="btn btn-xs btn-primary" id="crfNum1" value="Add one"> 
														</div>
													</div>
												 
                </div>
											
 					
					<div style="height: 300px;text-align:center;" id="divSec" hidden>
					<hr>
                     <div style="padding: 10px;color: #00adef;">
                        <span><b>CHARACTER REFERENCE #2</b></span>
                     </div> 

 												<div class="form-group">
												<label class="col-sm-3 control-label">Fullname</label>
													<div class="col-sm-8">				
													<input type="text" class="form-control" id="crfn2" name="crfn2" placeholder="Firstname" ng-model="crfn2" required>
													<input type="text" placeholder="Middlename" required class="form-control" id="crmn2" name="crmn2" ng-model="crmn2">
													<input type="text" placeholder="Lastname" required class="form-control" id="crln2" name="crln2" ng-model="crln2">
													<select class="form-control "  id="crsuffix2"  name="crsuffix2" ng-model="crsuffix2" placeholder="suffix" >
													<option>Jr.</option>
													<option>Sr.</option>
													<option>I</option>
													<option>II</option>
													<option>III</option>
													<option>IV</option>
													<option>IV</option>
							 
													</select>
												
												</div> 
											 
												<label class="col-sm-3 control-label">Contact Number</label>
													<div class="col-sm-8">		
													<input type="text" class="input-mask form-control" data-inputmask="'mask':'99999999999'" placeholder="Contact number" id="crm2" name="crm2" ng-model="crm2" required>
													<div class="help-block"><i>e.g. 09171234567</i></div>
													</div> 
										 
												<label class="col-sm-3 control-label">Profession</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="crp2" name="crp2" placeholder="Profession" ng-model="crp2" required>
												
												</div> 
												</div> 
												<div class="form-group">
 														<div class="col-sm-6" >
													<input type="button" class="btn btn-xs btn-primary" id="crfNum2" value="Add another one"> 
														</div>
													</div>
												 
                </div>
                <div   style="height: 300px;" id="divThird" hidden>
				           <hr>

                     <div style="padding: 10px;color: #f7941d;text-align:center;">
                        <span><b>CHARACTER REFERENCE #3</b></span>
                     </div>

 												<div class="form-group">
												<label class="col-sm-3 control-label">Fullname</label>
													<div class="col-sm-8">				
													<input type="text" class="form-control" id="crfn3" name="crfn3" placeholder="Firstname" ng-model="crfn3" required>
													<input type="text" placeholder="Middlename" required class="form-control" id="crmn3" name="crmn3" ng-model="crmn3">
													<input type="text" placeholder="Lastname" required class="form-control" id="crln3" name="crln3" ng-model="crln3">
													<select class="form-control "  id="crsuffix3"  name="crsuffix3" ng-model="crsuffix3" placeholder="suffix" >
													<option>Jr.</option>
													<option>Sr.</option>
													<option>I</option>
													<option>II</option>
													<option>III</option>
													<option>IV</option>
													<option>IV</option>
							 
													</select>
												
												</div> 
											 
												<label class="col-sm-3 control-label">Contact Number</label>
													<div class="col-sm-8">		
													<input type="text" class="input-mask form-control" data-inputmask="'mask':'99999999999'" placeholder="Contact number" id="crm3" name="crm3" ng-model="crm3" required>
													<div class="help-block"><i>e.g. 09171234567</i></div>
													</div> 
										 
												<label class="col-sm-3 control-label">Profession</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="crp3" name="crp3" placeholder="Profession" ng-model="crp3" required>
												
												</div> 
												</div> 
												 
                </div>
            </div>
             
        
        </div>
												 </div>
												 </div>
												 </div>
						 
							<span>
								<img src="<?php echo base_url()."assets/images/sped/previous.png"?>" id="Backkwatro" style="margin-top: 25%;position: fixed;margin-left: 4%;z-index: 200;width: 20%;bottom:0px;">
						   </span>
 
							<span>	
								<img src="<?php echo base_url()."assets/images/sped/next.png"?>" id="kwatro" style="margin-top: 25%;position: fixed;margin-left: 75%;z-index: 200;width: 15%;bottom:0px;">
							</span> 
							<img src="<?php echo base_url()."assets/images/sped/PAGE_4_CLOUDS_ROCKET_REFERENCE.png"?>" style="margin-top:120px;width: 100%;z-index: 80;bottom: 0px;position: fixed;left: 0px;"> 

                    </div>
                    <div class="tab-pane" id="tab5">
                        <div class="content-box" style="margin-top: 10%;">
                            <div class="content-box-wrapper">
							 
								 <table style="width: 100%;" class="table table-bordered table-striped table-condensed cf"  >
								  <tr>
									<th colspan="6"> <b> Personal Information  </b></th>
								  </tr>
								  <tr>
									<td id="td"> <b> Fullname </b></td>
									<td colspan="4"> {{ fname }}  {{ mname }} {{ lname }} {{ suffix }}  </td>
									<td  rowspan="7" style="width:30%"><img id="blah2" src="<?php echo base_url()."assets/images/facebook-avatar.jpg"; ?>" alt="your image" style="width:100%;border: 10px solid #e2e2e2;"/></td>
								  </tr>
								  <tr>
									<td id="td"><b>Birthday </b></td>
									<td colspan="4"> <span id="bdayz"></span> <span id="ApplicantBdayError" style="color:red;"></span> </td>
								  </tr>
								  <tr>
									<td id="td"><b>Gender </b></td>
									<td colspan="4"> {{ gender }} <span id="ApplicantGenderError" style="color:red;"></span> </td>
								  </tr>
								  <tr>
									<td id="td"><b>Civil Status </b></td>
									<td colspan="4"> {{ cs }} <span id="ApplicantCSError" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Religion </b></td>
									<td colspan="4"> {{ religion }} <span id="ApplicantReligionError" style="color:red;" ></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Contact number </b> <br></td>
									<td colspan="4"> {{ cm1 }} <span id="ApplicantCM1Error" style="color:red;"> </span></td>
								  </tr>
								  <tr>
									<td id="td"><b> Alternative Contact number </b></td>
									<td colspan="6"> {{ cm2 }} <span id="ApplicantCM2Error" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Present Address </b> </td>
									<td colspan="6"><span id="presentAddress"></span> <span id="ApplicantPresentAddrError" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Permanent Address </b></td>
									<td colspan="6"><span id="permanentAddress"></span> <span id="ApplicantPermanentAddrError" style="color:red;"></span></td>
								  </tr>
								   <tr>
								 
								 <th colspan="6"> <b> Position Applied</b> </th>
								  </tr>
								  <tr>
									<td id="td"><b>Position</b></td>
									<td colspan="6">{{ position }} <span id="ApplicantPositionError" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Call Center Experience</b></td>
									<td colspan="6">{{ ccexpo }} <span id="ApplicantCCexpoError" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Previous Company</b></td>
									<td colspan="6">{{ pc1 }} ( {{ ly1 }} )  <span id="ApplicantPC1Error" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Source</b></td>
									<td colspan="6">{{ source }}  <span id="ApplicantPC1Error" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Reference</b></td>
									<td colspan="6">{{ rfn1 }} {{ rmn1 }} {{ rln1 }} {{ rsuffix1 }} ( <i>{{ rcm1 }} </i>) , {{ rfn2 }} {{ rmn2 }} {{ rln2 }} {{ rsuffix12 }} ( <i>{{ rcm2 }} </i>) <span id="ApplicantRF1Error" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<th colspan="6"><b> Character Reference</b></th>
								  </tr>
								  <tr>
									<td id="td"><b>Character reference #1</b></td>
									<td colspan="6">{{ crfn1 }} {{ crmn1 }} {{ crln1 }} <br> {{ crm1 }} <br> {{ crp1 }} <span id="ApplicantCR1Error" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Character reference #2</b></td>
									<td colspan="6">{{ crfn2 }} {{ crmn2 }} {{ crln2 }} <br> {{ crm2 }} <br> {{ crp2 }} <span id="ApplicantCR2Error" style="color:red;"></span></td>
								  </tr>
								  <tr>
									<td id="td"><b>Character reference #3</b></td>
									<td colspan="6">{{ crfn3 }} {{ crmn3 }} {{ crln3 }} <br> {{ crm3 }} <br> {{ crp3 }} <span id="ApplicantCR3Error" style="color:red;"></span></td>
								  </tr>
								 
								</table>
								<button type="button" class="btn btn-success" id="imageBTN" style="margin-left: 90%;"> <b> Apply </b></button>

						</div>
		
                        </div>
						 
					 
						 <span>
								<img src="<?php echo base_url()."assets/images/sped/previous1.png"?>" id="Backsingco" style="position: absolute;margin-left: 4%;z-index: 200;width: 20%;">
						   </span>
 
						 
                    </div>
				 
                </div>
            </div>
			
	

			
			
        </div>
    </div>
 	</div>	
 	</div>
    </div>
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog" style="width: 100%;">
				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Present Address</h4>
				  </div>
					<div class="modal-body">
						<div class="form-group personal-information" id="presentaddressHover">
							<label class="col-sm-3 control-label"> Present Address</label>
 									<div class="container-fluid">
 												<form autocomplete="off" class="form-horizontal">
												<div class="row-fluid">
												  <div class="span6">
													<div class="row-fluid">
 														<div class="controls">
														  <input type="text" class="inputAddress input-xxlarge form-control"   autocomplete="off" placeholder="Type in your address" style="width: 100%;" id="presentAddr" name="presentAddr" ng-model="presentAddr">
														</div>
													</div>			  
												  </div>
												  <div class="span6">
												  <div class="control-group">
													<label class="control-label">Formatted address</label>
													<div class="controls">
													  <input type="text" class="formatted_address input-xxlarge form-control" style="width: 100%;"  id="presentformatAddr" name="presentformatAddr" ng-model="presentformatAddr" required>
													</div>
												  </div>
													
												  <div class="control-group">
													<label class="control-label">Province</label>
													<div class="controls">
													  <input type="text" class="county form-control" style="width: 100%;" disabled="disabled" id="presentProvince" name="presentProvince" ng-model="presentProvince" required>
													</div>
												  </div>
												 
												  <div class="control-group">
													<label class="control-label">city</label>
													<div class="controls">
													  <input type="text" class="city form-control" style="width: 100%;" disabled="disabled" id="presentCity" name="presentCity" ng-model="presentCity" required>
													</div>
												  </div>
												  <div class="control-group">
													<label class="control-label">zip</label>
													<div class="controls">
													  <input type="text" class="zip form-control" style="width: 100%;" id="presentZip" name="presentZip" ng-model="presentZip"  >
													</div>
												  </div>
										 
											 
												  </div>
												</div>
												</form>
 										 
											</div><!--/span-->
 							</div>
						</div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnPresentAddr">Enter</button>
						  </div>

						</div>
 
    </div>

  </div>
  <div id="myModal2" class="modal fade" role="dialog">
			<div class="modal-dialog" style="width: 100%;">
				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Permanent Address</h4>
				  </div>
					<div class="modal-body">
						<div class="form-group personal-information"  id="permanentaddressHover">
							<label class="col-sm-3 control-label"> Permanent Address</label>
						 
									<div class="container-fluid">
												<form autocomplete="off" class="form-horizontal">
												<div class="row-fluid">
												  <div class="span6">
													<div class="row-fluid">
 														<div class="controls">
														  <input type="text" class="inputAddress2 input-xxlarge form-control"   autocomplete="off" placeholder="Type in your address" style="width: 100%;" id="permanentAddr" name="permanentAddr" ng-model="permanentAddr">
														</div>
													</div>			  
												  </div>
												  <div class="span6">
												  <div class="control-group">
													<label class="control-label">Formatted address</label>
													<div class="controls">
													  <input type="text" class="formatted_address2 input-xxlarge form-control" style="width: 100%;" disabled="disabled" id="permanentformatAddr" name="permanentformatAddr" ng-model="permanentformatAddr" required >
													</div>
												  </div>
											 
												  <div class="control-group">
													<label class="control-label">Province</label>
													<div class="controls">
													  <input type="text" class="county2 form-control" style="width: 100%;" disabled="disabled"  id="permanentProvince" name="permanentProvince" ng-model="permanentProvince" required>
													</div>
												  </div>
												 
												  <div class="control-group">
													<label class="control-label">city</label>
													<div class="controls">
													  <input type="text" class="city2 form-control" style="width: 100%;" disabled="disabled" id="permanentCity" name="permanentCity" ng-model="permanentCity" required>
													</div>
												  </div>
												  <div class="control-group">
													<label class="control-label">zip</label>
													<div class="controls">
													  <input type="text" class="zip2 form-control" style="width: 100%;" id="permanentZip" name="permanentZip" ng-model="permanentZip"   >
													</div>
												  </div>
										 
											 
												  </div>
												</div>
												</form>
											  </div><!--/span-->
										 
						 
						</div>
						</div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnPermanentAddr">Enter</button>
						  </div>

						</div>
 
    </div>

  </div>
 	<!--<div id="foot">
 <img src="<?php echo base_url()."assets/images/sped/PHOTO_CLOUDS.png"?>"style="width: 100%;z-index: 50;position: absolute;bottom:0px;"> 
 
	
	
	</div>-->
	

      <!-- JS Demo -->
	  	<script>
			$('.inputAddress').addressPickerByGiro({
				distanceWidget: true,
				boundElements: {
					'country': '.country',
					'country_code': '.country_code',
					'region': '.region',
					'region_code': '.region_code',
					'county': '.county',
					'county_code': '.county_code',
					'city': '.city',
					'city_district': '.city_district',
					'street': '.street',
					'street_number': '.street_number',
					'zip': '.zip',
					'latitude': '.latitude',
					'longitude': '.longitude',
					'formatted_address': '.formatted_address',
					'radius': '.radius'
				}
			});
			$('.inputAddress2').addressPickerByGiro({
													distanceWidget: true,
													boundElements: {
														'country': '.country2',
														'country_code': '.country_code2',
														'region': '.region2',
														'region_code': '.region_code2',
														'county': '.county2',
														'county_code': '.county_code2',
														'city': '.city2',
														'city_district': '.city_district2',
														'street': '.street2',
														'street_number': '.street_number2',
														'zip': '.zip2',
														'latitude': '.latitude2',
														'longitude': '.longitude2',
														'formatted_address': '.formatted_address2',
														'radius': '.radius2'
													}
												});
	</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
   <script>
    angular.module("app",[])
      .controller("ctrl",['$scope',function($scope){
 
      }])
    </script>
    <script>
     $(function(){
		
		 $("#step0").trigger("click");
		 
		 
		$("#imageBTN").click(function(){
			$(this).attr("disabled",true);
				swal({
 				  imageUrl: "<?php echo base_url()."assets/images/loading-gears-animation-3.gif";?>",
				  showConfirmButton: false

				});
			var data = new FormData();
			$.each($("#image")[0].files,function(i,file){
				data.append("image",file);
				
			});
			data.append("fname",$('#fname').val());
			data.append("mname",$('#mname').val());
			data.append("lname",$('#lname').val());
			data.append("suffix",$('#suffix').val());
			data.append("contactnum",$('#cm1').val());
			data.append("altcontactnum",$('#cm2').val());
			data.append("religion",$('#religion').val());
			data.append("bday",$('#bday').val());
			data.append("cs",$('#cs').val());
			data.append("gender",$('#gender').val());
			data.append("presentAddr",$('#presentformatAddr').val()+"|"+$('#presentProvince').val()+"|"+$('#presentCity').val()+"|"+$('#presentZip').val());
 			data.append("permanentAddr",$('#permanentformatAddr').val()+"|"+$('#permanentProvince').val()+"|"+$('#permanentCity').val()+"|"+$('#permanentZip').val());
 			data.append("position",$('#position').val());
			data.append("ccexpo",$('#ccexpo').val());
			data.append("sourceApply",$('#source').val());
			data.append("previous",$('#pc1').val()+"|"+$('#ly1').val());
 			data.append("source",$('#rfn1').val()+"$"+$('#rmn1').val()+"$"+$('#rln1').val()+"$"+$('#rsuffix1').val()+"$"+$('#rcm1').val()+"|"+$('#rfn2').val()+"$"+$('#rmn2').val()+"$"+$('#rln2').val()+"$"+$('#rsuffix2').val()+"$"+$('#rcm2').val());
 				data.append("creference1",$('#crfn1').val().trim()+"$"+$('#crmn1').val().trim()+"$"+$('#crln1').val().trim()+"|"+$('#crm1').val().trim()+"|"+$('#crp1').val().trim());
				data.append("creference2",$('#crfn2').val().trim()+"$"+$('#crmn2').val().trim()+"$"+$('#crln2').val().trim()+"|"+$('#crm2').val().trim()+"|"+$('#crp2').val().trim());
				data.append("creference3",$('#crfn3').val().trim()+"$"+$('#crmn3').val().trim()+"$"+$('#crln3').val().trim()+"|"+$('#crm3').val().trim()+"|"+$('#crp3').val().trim());
 				
 
		 $.ajax({
			url: location.protocol + '//' + location.hostname+"/sz3/index.php/applicant/applyUpload",
			type:"POST",
			processData:false,
			data:data,
			contentType:false,
			success:function(response){
 				if(response!="fail"){
					swal('Success','You have just successfully applied!','success')
					setTimeout(function(){location.reload();},2000);
				}else{
					swal('Error','All fields must be filled out!','error')
				}
			}	 
		 });  
 
 
 

			
		});
	 });
	</script>
 
	 
	<script language="JavaScript">
		  
	  $(function(){
		
	
		
		
	 });
		</script>
		<script>
		   function readURL(input) {
            if (input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    $('#blah').attr('src', e.target.result).width('600px');
					$('#blah2').attr('src', e.target.result);
                 };
                reader.readAsDataURL(input.files[0]);
				}
			}
		 $(function(){
			 $("#image").change(function(){
				 
 			 });
 		 });
 

 
		</script>
	
	 
       <script type="text/javascript" src="<?php echo base_url();?>assets/js/validation1.js"></script>

 <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>