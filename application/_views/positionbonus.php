<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}

  </style>


  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });
 </script>


 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/xeditable/xeditable.css">
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable.js"></script>
 <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable-demo.js"></script> -->

 <!-- Bootstrap Summernote WYSIWYG editor -->

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/popover/popover.js"></script>

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 <script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd'
    });
  });

</script>

</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div class="container">
          <script type="text/javascript">

            /* Datatables export */

            $(document).ready(function() {
              var table = $('#datatable-tabletools').DataTable();
            } );

            $(document).ready(function() {
              $('.dataTables_filter input').attr("placeholder", "Search...");
            });

          </script>
          <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>

          <script type="text/javascript">
            /* Input masks */

            $(function() { "use strict";
              $(".input-mask").inputmask();
            });

          </script>

          <div id="page-title">
            <div class="btn-group">
              <button class="btn btn-primary btn-sm" onclick="window.location = '<?php echo base_url();?>index.php/PositionRates'"><span class='icon-typicons-arrow-left'></span> Back</button>
              <button class="btn btn-info btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm"><span class='icon-typicons-plus'></span> Bonus</button>
            </div>
            <br><br>
            <div class="row">
              <div class="col-md-12">

                <table class="table table-striped table-condensed" >
                  <thead>
                    <tr>
                     <th class='text-center' style='background: #787a7b;color: white;border-top-left-radius: 10px;'>Position</th>
                     <th class='text-center' style='background: #787a7b;color: white;'>Status</th>
                     <th class='text-center' style='background: #787a7b;color: white;border-top-right-radius: 10px;' colspan=3>Bonus Details</th>
                   </tr>
                 </thead>
                 <tbody>
                   <?php if($bonus==NULL){?>

                   <?php }else{ foreach($bonus as $b): 
                    $bonusdetails = explode(',',$b->bonus); 
                    $cnt = count($bonusdetails);
                    for($x = 0;$x<$cnt;$x++){
                      $bonuses = explode('|',$bonusdetails[$x]);
                      $details[$x][0] = $bonuses[0];
                      $details[$x][1] = $bonuses[1];
                      $details[$x][2] = $bonuses[2]; 
                      $details[$x][3] = $bonuses[3]; 
                    }
                    ?>
                    <tr>
                      <td rowspan=<?php echo $cnt;?>><?php echo $b->pos_name.' - '.$b->pos_details;?></td>
                      <td rowspan=<?php echo $cnt;?>><?php echo $b->status;?></td>
                      <td><?php echo $details[0][2];?></td>
                      <td class='text-right'>

                        <button style="font-size:14px;" class='btn btn-xs btn-link' id="<?php echo $b->posempstat_id.''.$details[0][0];?>" data-id="<?php echo $b->posempstat_id.''.$details[0][0];?>" data-position="<?php echo $b->pos_name.' - '.$b->pos_details;?>" data-status="<?php echo $b->status;?>" data-posempstatid="<?php echo $b->posempstat_id;?>" data-amount="<?php echo $details[0][1];?>" data-pbonusid="<?php echo $details[0][0];?>" data-bonusid="<?php echo $details[0][3];?>" data-bonusname='<?php echo $details[0][2];?>' data-toggle="modal" data-target="#updatemodal"> <?php echo number_format($details[0][1],2);?> </button>

                      </td>
                      <td class='text-center'>
                        <button class='btn btn-link btn-xs' <?php if($count->count==$cnt){echo "disabled='disabled'";}?> data-posempstatid="<?php echo $b->posempstat_id;?>" data-position="<?php echo $b->pos_name.' - '.$b->pos_details;?>" data-status="<?php echo $b->status;?>" data-bonusids="<?php for($y=0;$y<$cnt;$y++){echo $details[$y][3].'|';}?>" href="#" data-toggle="modal" data-target="#insertmodal"><span class='icon-typicons-plus'></span></button> 
                      </td>
                    </tr>
                    <?php for($x=1;$x<$cnt;$x++):?>
                      <tr>
                       <td><?php echo $details[$x][2];?></td>
                       <td class='text-right'>

                        <button style="font-size:14px;" class='btn btn-xs btn-link' id="<?php echo $b->posempstat_id.''.$details[$x][0];?>" data-id="<?php echo $b->posempstat_id.''.$details[$x][0];?>" data-position="<?php echo $b->pos_name.' - '.$b->pos_details;?>" data-status="<?php echo $b->status;?>" data-posempstatid="<?php echo $b->posempstat_id;?>" data-amount="<?php echo $details[$x][1];?>" data-pbonusid="<?php echo $details[$x][0];?>" data-bonusid="<?php echo $details[$x][3];?>" data-bonusname='<?php echo $details[$x][2];?>' data-toggle="modal" data-target="#updatemodal"> <?php echo number_format($details[$x][1],2);?> </button>
                      </td>
                    </tr>
                  <?php endfor;?>
                <?php endforeach; }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="row" hidden>
        <div class="col-md-8">
          <div class="panel">
            <div class="panel-body">
              <h3 class="title-hero">
                Recent sales activity
              </h3>
              <div class="example-box-wrapper">
                <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
          </div>
          <div class="content-box">
            <h3 class="content-box-header bg-default">
              <i class="glyph-icon icon-cog"></i>
              Live server status
              <span class="header-buttons-separator">
                <a href="#" class="icon-separator">
                  <i class="glyph-icon icon-question"></i>
                </a>
                <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                  <i class="glyph-icon icon-refresh"></i>
                </a>
                <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                  <i class="glyph-icon icon-times"></i>
                </a>
              </span>
            </h3>
            <div class="content-box-wrapper">
              <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog" style='width:500px;'>
    <div class="modal-content">
      <div class="modal-header" style="background: #1f2e2e; color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title">New Position Bonus</h3>
      </div>
      <div class="modal-body">
        <div class="row" id='forms'>
          <div class="col-md-12">
            <label class='control-label'>Departments
              <span class="text-danger" id='dn' style='font-size:8px;' hidden>* (Required)</span>
            </label>
            <select class='form-control' id='department'>
              <option value=''>--</option>
              <?php foreach($departments as $d):?>
                <option value='<?php echo $d->dep_id;?>'><?php echo $d->dep_details;?></option>
              <?php endforeach;?>
            </select><br>
          </div>
          <div class="col-md-8">
            <label class='control-label'>Position
              <span class="text-danger" id='pn' style='font-size:8px;' hidden>* (Required)</span>
            </label>
            <select class='form-control' id='position'>
              <option value=''>--</option>

            </select><br>
          </div>
          <div class="col-md-4">

            <label class='control-label'>Status
              <span class="text-danger" id='sn' style='font-size:8px;' hidden>* (Required)</span>
            </label>
            <select class='form-control' id='status'>
              <option value=''>--</option>
            </select>
          </div>
          <div class="col-md-8">

            <label class='control-label'>Bonus Name
              <span class="text-danger" id='bn' style='font-size:8px;' hidden>* (Required)</span>
            </label>
            <select class='form-control' id='bonusname'>
              <option value=''>--</option>
              <?php foreach($bonuslist as $b):?>
                <option value='<?php echo $b->bonus_id;?>'><?php echo $b->bonus_name;?></option>
              <?php endforeach;?>
            </select>
          </div>
          <div class="col-md-4">
            <label class='control-label'>Amount
              <span class="text-danger" id='a1' style='font-size:8px;' hidden>* (Required)</span>
              <span class="text-danger" id='a2' style='font-size:8px;' hidden>* Valid Number Only</span>

            </label>
            <input type="text" name="" class='form-control' id='amount'>
          </div>
        </div> 
        <br>
      </div>    
      <div class="modal-footer" id='footer'>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id='addnewbonus'>Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade " id='updatemodal' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:35%;">
    <div class="modal-content">
      <form method="POST" id='updateform'>
        <div class="modal-header" style="background: #1f2e2e; color: white;">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 class="modal-title">Update Employee Addition</h3>
        </div>
        <div class="modal-body">
          <div class="row" id='forms'>

            <div class="col-md-8">
              <label class='control-label'>Position</label>
              <h4> <span id='uposition'></span></h4><br>
            </div>
            <div class="col-md-4">

              <label class='control-label'>Status</label>
              <h4> <span id='ustatus'></span></h4><br>
            </div>
            <div class="col-md-8">
              <label class='control-label'>Bonus Name</label>
              <h4> <span id='ubonusname'></span></h4><br>
            </div>
            <div class="col-md-4">
              <label class='control-label'>Amount
                <span class="text-danger" id='ua1' style='font-size:8px;' hidden>* (Required)</span>
                <span class="text-danger" id='ua2' style='font-size:8px;' hidden>* Valid Number Only</span>
              </label>
              <input type="text" name="" class='form-control' id='uamount'>
              <input type="hidden" id='uposempstatid' name="">
              <input type="hidden" id='ubonusid' name="">
              <input type="hidden" id='upbonusid' name="">
            </div>
          </div> 
          <br>
        </div>   
        <div class="modal-footer" id='footer'>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id='updatenow' disabled>Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade " id='insertmodal' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:35%;">
    <form id='savenow' method='post'>
      <div class="modal-content">
        <div class="modal-header" style="background: #1f2e2e; color: white;">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 class="modal-title">Add Single Addition</h3>
        </div>
        <div class="modal-body">
          <div class="row" id='forms'>

            <div class="col-md-8">
              <label class='control-label'>Position</label>
              <h4> <span id='iposition'></span></h4><br>
            </div>
            <div class="col-md-4">

              <label class='control-label'>Status</label>
              <h4> <span id='istatus'></span></h4><br>
            </div>
            <div class="col-md-8">

              <label class='control-label'>Bonus Name
                <span class="text-danger" id='ibn' style='font-size:8px;' hidden>* (Required)</span>
              </label>
              
              <select class="form-control" id='ibonusid'></select>
            </div>
            <div class="col-md-4">
              <label class='control-label'>Amount
                <span class="text-danger" id='ia1' style='font-size:8px;' hidden>* (Required)</span>
                <span class="text-danger" id='ia2' style='font-size:8px;' hidden>* Valid Number Only</span>
              </label>
              <input type="text" name="" class='form-control' id='iamount'>
              <input type="hidden" id='iposempstatid' name="">
            </div>
          </div> 
          <br>
        </div>   
        <div class="modal-footer" id='footer'>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Insert</button>
        </div>
      </div>
    </form>
  </div>
</div>
<p class="growl1" hidden></p>
<p class="growl2" hidden></p>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script>
  $(function(){
    $(".growl1").select(function(){
      $.jGrowl(
        "Successfully Updated.",
        {sticky:!1,position:"top-right",theme:"bg-blue-alt"}
        )
    });
    $(".growl2").select(function(){
      $.jGrowl(
        "Error Updating.",
        {sticky:!1,position:"top-right",theme:"bg-red"}
        )
    });
    
  });
</script>
<script type="text/javascript">
  $('#position').on('change', function(){
   var position_id = $('#position').val();
   $('#status').html('');
   $('#status').append('<option value="">--</option>');
   $.each(<?php echo json_encode($positions);?>,function (i,elem){
    if(elem.pos_id==position_id){
      var stats = elem.status;
      var stat = stats.split(",");
      for(var i=0;i<stat.length;i++){
        var newstat = stat[i].split("|");
        // alert(newstat);
        $('#status').append('<option value="'+newstat[0]+'">'+newstat[1]+'</option>');
      }
      
      
    }
  })

 });


  $('#department').on('change', function(){
   var department_id = $('#department').val();  
   $('#position').html('');
   $('#position').append('<option value="">--</option>');
   $.each(<?php echo json_encode($positions);?>,function (i,elem){
    // console.log(elem);
    if(elem.dep_id==department_id){
      console.log(elem);
      var position = elem.pos_details;
      var pos_id = elem.pos_id;
      $('#position').append('<option value="'+pos_id+'">'+position+'</option>');

    }
  })

 });
</script>
<script type="text/javascript">
  $("#addnewbonus").click(function(){
    var position = $("#position").val();
    var status = $("#status").val();
    var bonusname = $("#bonusname").val();
    var amount = $("#amount").val();
    if(position==''){
      $("#pn").show();
    }else{
      $("#pn").hide();
    }
    if(status==''){
      $("#sn").show();
    }else{
      $("#sn").hide();
    }
    if(bonusname==''){
      $("#bn").show();
    }else{
      $("#bn").hide();
    }
    if(amount==''){
      $("#a1").show();
      $("#a2").hide();
    }else if(!$.isNumeric(amount)){
      $("#a2").show();
      $("#a1").hide();
    }else{
      $("#a1").hide();
      $("#a2").hide();
    }

    if(bonusname!=''&&amount!=''&&position!=''&&status!=''&&$.isNumeric(amount)){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/PositionRates/insertNewBonus",
        data: {
          position: position,
          status:status,
          bonusid:bonusname,
          amount:amount
        },
        cache: false,
        success: function(result)
        {
          if(result=='Success'){
            location.reload();
          }else{
           $(".growl2").trigger('select');
         }
       }
     });
    }

  })
</script>
<script type="text/javascript">
  $('#updatemodal').on('show.bs.modal', function (e) {
    // $(this).find('#uid').val($(e.relatedTarget).data('id'));                       getEmpAddExistAdditions
    $(this).find('#uposition').html($(e.relatedTarget).data('position'));
    $(this).find('#ustatus').html($(e.relatedTarget).data('status'));
    $(this).find('#uposempstatid').val($(e.relatedTarget).data('posempstatid'));
    $(this).find('#upbonusid').val($(e.relatedTarget).data('pbonusid'));
    $(this).find('#ubonusid').val($(e.relatedTarget).data('bonusid'));
    $(this).find('#ubonusname').html($(e.relatedTarget).data('bonusname'));
    $(this).find('#uamount').val($(e.relatedTarget).data('amount'));
  });

  $('#insertmodal').on('show.bs.modal', function (e) {
    // $(this).find('#data-id').val($(e.relatedTarget).data('id'));
    $(this).find('#iposition').html($(e.relatedTarget).data('position'));
    $(this).find('#istatus').html($(e.relatedTarget).data('status'));
    $(this).find('#iposempstatid').val($(e.relatedTarget).data('posempstatid'));
    var posempstatid = $(e.relatedTarget).data('posempstatid');
    var bonusids = $(e.relatedTarget).data('bonusids');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/PositionRates/getPosAvailableBonus",
      data: {
        posempstatid: posempstatid,
        bonusids: bonusids
      },
      cache: false,
      success: function(res)
      {
        if(res!='Empty'){
          $("#ibonusid").html("<option value=''>--</option>");
          $.each(JSON.parse(res),function (i,elem){
              // alert(elem.rate);
              $("#ibonusid").append("<option value='"+elem.bonus_id+"'>"+elem.bonus_name+"</option>");
            });
        }
      }
    });
  });

  $("#uamount").on("change paste keyup", function() {
    $("#updatenow").removeAttr('disabled');
  });
  $('#updatemodal').on('hide.bs.modal', function (e) {
   $("#updatenow").attr('disabled','disabled');
 });
</script>
<script type="text/javascript">
  $("#updatenow").click(function(){
    var posempstatid = $("#uposempstatid").val();
    var pbonusid = $("#upbonusid").val();
    var amount = $("#uamount").val();
    var bonusid = $("#ubonusid").val();
    
    if(amount==''){
      $("#ua1").show();
      $("#ua2").hide();
    }else if(!$.isNumeric(amount)){
      $("#ua2").show();
      $("#ua1").hide();
    }else{
      $("#ua1").hide();
      $("#ua2").hide();
    }

    if(amount!=''&&$.isNumeric(amount)){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/PositionRates/updatePositionBonus",
        data: {
          posempstatid: posempstatid,
          pbonusid:pbonusid,
          bonusid:bonusid,
          amount:amount
        },
        cache: false,
        success: function(result)
        {
          if(result=='Success'){
            location.reload();
          }else{
           $(".growl2").trigger('select');
         }
       }
     });
    }

  })
</script>
<script type="text/javascript">
  $("#savenow").submit(function(e){
    // iposition istatus ibonusid iamount iposempstatid ibn ia1 ia2
    var posempstatid = $("#iposempstatid").val();
    var amount = $("#iamount").val();
    var bonusid = $("#ibonusid").val();
    
    if(bonusid==''){
      $("#ibn").show();
    }else{
      $("#ibn").hide();
    }
    if(amount==''){
      $("#ia1").show();
      $("#ia2").hide();
    }else if(!$.isNumeric(amount)){
      $("#ia2").show();
      $("#ia1").hide();
    }else{
      $("#ia1").hide();
      $("#ia2").hide();
    }

    if(bonusid!=''&&amount!=''&&$.isNumeric(amount)){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/PositionRates/insertNewBonus2",
        data: {
          posempstatid: posempstatid,
          bonusid:bonusid,
          amount:amount
        },
        cache: false,
        success: function(result)
        {
          if(result=='Success'){
            location.reload();
          }else{
           $(".growl2").trigger('select');
         }
       }
     });
    }
    e.preventDefault();
  })
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>