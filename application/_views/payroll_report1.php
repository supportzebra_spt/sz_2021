<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		#tblShowTR td{
			color: green;
		}
		tbody#tblShow tr td {
			border: 1px solid #b9ffbc;
			vertical-align:middle;
		}
		tbody#tblShow tr:hover {
			background-color: #aaefad;
		}
		thead {
			background: #7b7b7b;
			color: white;
			font-size: smaller;
		}
		tbody{
			color: black;
		} 
</style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> SUPPORTZEBRA | PAYROLL REPORT SUMMARY </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">






    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

 
</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

 		<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper" >
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">

<div class="col-md-12">
        <div class="panel">
            <div class="panel-body" >

                 <div class="example-box-wrapper" >
                    <form class="form-horizontal bordered-row" role="form">
		
                         <div class="form-group" id="employzResult">
                    <div class="col-sm-12">
                        <!--<select multiple class="multi-select" id="employz">
 								<?php 
								foreach($employee as $k => $v){
									
									echo "<option value=".$v['emp_id'].">".$v['fname']." ".$v['lname']."</option>";
								}
								
								?>
                         </select>-->
						 
                  
          <form id="demoform" action="#" method="post">
		  		 
					 <div class="form-group">
                    <label class="col-sm-3 control-label">Payroll Coverage:</label>
                    <div class="col-sm-6">
                         <select name="" class="chosen-select" style="display: none;" id="payrollC">
								 
                                <option disabled="disabled" selected >--</option>
							<?php foreach($period as $row => $val){?>
							<optgroup label="<?php echo $val["month"]; ?>">
									<?php 
									$exp = explode(",",$val["daterange"]);
										for($i=0;$i<count($exp);$i++){	
										$newVal = explode("|",$exp[$i]);
									?>
										<option value="<?php echo $newVal[0]; ?>"><?php echo $newVal[1]; ?></option>
									<?php } ?>
							</optgroup>
							<?php } ?>
                        </select>
	 
                    </div>

                </div>
          	<div class="form-group">
                    <label class="col-sm-3 control-label">Type</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="empzType">
                            <option value="this option">--</option>
                            <option value="Admin">SZ Team</option>
                            <option value="Agent">Ambassador</option>
                        </select>
                    </div>
                </div>
		
            </form>

                    </div>
                </div>
					<input type="button" class="btn btn-xs btn-info" id="btnShow" value="Show">
					<input type="button" class="btn btn-xs btn-success" id="btnExport" value="Export">
							<input type='button' class="btn  btn-xs btn-success" id="excel_button2" value='Excel_get_coverage'  style="display:none"> 

 
     
                    </form>
                </div>
            </div>
        </div>
   
       
    </div>
<div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                
                <div class="example-box-wrapper"  >
						<!--<div id="loadingIMG" style="position: fixed; margin: -20% auto 0px; width: 100%; z-index: 1000; display: none;margin-left:20%;">
							 <img src="<?php echo base_url();?>assets/images/loading-gears-animation-3.gif"  > 
						</div>-->
						<div class="loading-spinner" id="loads">
                            <i class="bg-black"></i>
                            <i class="bg-black"></i>
                            <i class="bg-black"></i>
                            <i class="bg-black"></i>
                            <i class="bg-black"></i>
                            <i class="bg-black"></i>
                        </div>
                    <div class="form-horizontal bordered-row dddiv" role="form">
					
                          
                    </div>
                </div>
            </div>
        </div>
   
       
    </div>
	

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
				</div>
			</div>
		</div>
	</div>
</div>


</div>
     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>

</div>
</body>
<script>
  $(function(){
		$("#btnShow").trigger('click'); 
		$('#sidebar-menu').load('<?php echo base_url(); ?>/index.php/Templatedesign/sideBar');
		$('#headerLeft').load('<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft');
  });	
</script> 

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
        $('#datatable-example').dataTable();
    });
    $(document).ready(function() {
        var table = $('#datatable-hide-columns').DataTable( {
            "scrollY": "300px",
            "paging": false
        } );

        $('#datatable-hide-columns_filter').hide();

        $('a.toggle-vis').on( 'click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column( $(this).attr('data-column') );

            // Toggle the visibility
            column.visible( ! column.visible() );
        } );
    } );

    /* Datatable row highlight */

    $(document).ready(function() {
        var table = $('#datatable-row-highlight').DataTable();

        $('#datatable-row-highlight tbody').on( 'click', 'tr', function () {
            $(this).toggleClass('tr-selected');
        } );
    });



    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>
<script>
  $(function(){
   $("#loads").hide();
 	function initResultDataTable(){
    $('#datatable-row-highlight').DataTable({
                        "order": [],
                        "columnDefs": [ {
                        "targets"  : 'no-sort',
                        "orderable": false,
                        }]
                });
	}
	$("#btnExport").click(function(){
			var payrollC = $("#payrollC").val();
 			var empzType = $("#empzType").val();
 			var dataString = "payrollC="+payrollC+"&empzType="+empzType;

				$.ajax({
                    url: '<?php echo base_url() ?>index.php/payroll/export_payroll_summary',
                    type: 'POST',
                    data:  dataString,
					beforeSend:function(){
						$("#loads").show();
						// $("#tblShow").html("");
					},
                    success:function(res){
						$("#loads").hide();
						 $("#excel_button2").trigger("click");
					}			
				});
			
	});
	$("#excel_button2").click(function(){
			var payrollC = $("#payrollC").val();
 			var empzType = $("#empzType").val();
			var empzCat = $("#empzCat").val();
 			var dataString = "cid="+payrollC+"&empzCat="+empzCat+"&empzType="+empzType;


			 $.ajax({
                     url: '<?php echo base_url() ?>index.php/payroll/getcoverage_date',
                    data:  dataString,
                     type: 'POST',
                     success:function(res)
						{
							var str = res.split("|");
							 window.open("<?php echo base_url() ?>reports/payroll/"+str[0]+"/"+str[1]+"/"+str[2]+"/report/PAYROLL_SUMMARY_REPORT_"+str[5]+"_"+str[0]+"_"+str[4]+"_"+str[2]+"_u"+str[3]+".xlsx",'_blank');
 	  // $file = 'PAYROLL_SUMMARY_REPORT_'.strtoupper($empzType).'_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_u'.$this->session->userdata('uid').'.xlsx';

						}
                });  
		});
	$("#btnShow").click(function(){
		  
			var payrollC = $("#payrollC").val();
			var employz = $("#employz").val();
			var empzType = $("#empzType").val();
 			var dataString = "payrollC="+payrollC+"&employz="+employz+"&empzType="+empzType;

 		    $.ajax({
                //url of the function
                    url: '<?php echo base_url() ?>index.php/payrollchecker/testGG2',
                    type: 'POST',
                    data:  dataString,
					beforeSend:function(){
						$("#loads").show();
						$("#tblShow").html("");
						$(".dddiv").html("");
						
					},
                    success:function(res)
                    { 
					$("#loads").hide();
					if(res!="0"){
						
						var obj = jQuery.parseJSON(res);
						 var tbl = '<table id="datatable-row-highlight" class="table table-striped table-bordered" cellspacing="0" width="100%">';
 						 tbl+='<thead><tr><td>Accounts</td><td>Headcount</td>';
  						$.each(obj.header,function(index, value){
							tbl += '<td>'+value+'</td>'
						});
						tbl+='</tr></thead><tbody>';
  						$.each(obj,function(index, value){
							if(index=="accounts"){
							$.each(value,function(index2, value2){
								// if(index2=="Agent"){
									$.each(value2,function(index3, value3){
									  var acc = index3.split("|")
									tbl += '<tr><td>'+acc[1]+'</td><td>'+acc[2]+'</td>';
										$.each(value3,function(index4, value4){
 
											  $.each(value4,function(index5, value5){
											// console.log(index4+" = "+index5+" = "+value5["total"]);
											tbl += '<td>'+value5+'</td>';
											 });
											
										});
										tbl += '</tr>';
									});
								// }
							});
						}
						});
						tbl += '</tbody></table>';
						if ( $.fn.DataTable.isDataTable('#datatable-row-highlight') ) {
							  $('#datatable-row-highlight').DataTable().destroy();
							}

							$('#datatable-row-highlight tbody').empty();
							$(".dddiv").html(tbl);
							// ... skipped ...

							$('#datatable-row-highlight').dataTable({
								  "autoWidth":true
								, "info":true
								, "iDisplayLength":20
								, "JQueryUI":true
								, "ordering":true
								, "searching":true
								, "paging":true
								,"bFilter": false
								,"scrollX": true
								, "scrollCollapse":true
								,  "order": [[ 2, "asc" ]]
							});
							
					}else{
						swal("No Data for "+ empzType,"Please coordinate with HR to finalize the payroll for this period.","error");
							$(".dddiv").html("");

					}	
                    }
                }); 
	  });
 
	});	
</script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
   
 </html>