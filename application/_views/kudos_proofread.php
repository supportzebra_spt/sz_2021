<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
        .hidden{
            visibility: hidden;
        }
        .seen{
            visibility: visible;
        }
        td,th{
            font-size:14px;
        }
    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- jQueryUI Autocomplete -->



    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

    <!--<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>-->

    



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">


    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>


    <!-- FORM MASKS-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>
    <script type="text/javascript">
        /* Input masks */

        $(function() { "use strict";
            $(".input-mask").inputmask();
        });

    </script>


    <script type="text/javascript">
        $(window).load(function(){
         setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
     });
 </script>



 <!-- jQueryUI Datepicker -->

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

 <!-- Bootstrap Daterangepicker -->

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>


 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

 <!-- Parsley -->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>
 <!-- Tooltip -->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/tooltip/tooltip.css">
 <!-- JQUERY CONFIRM-->

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.js"></script>

 <script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true,
            "order": [[ 3, "desc" ]]
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>
<script type="text/javascript">

</script>

</head>


<body>
    <div id="sb-site">


        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div id="page-wrapper">
            <div id="page-header" class="bg-black">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                    <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                </div>
                <div id="header-logo" class="logo-bg">
                   <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                   <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id='headerLeft'>
            </div>



        </div>
        <div id="page-sidebar">
            <div class="scroll-sidebar">


                <ul id="sidebar-menu">
                </ul>
            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content">

                <div class="container">
                    <div id="page-title">
                        <h2>Kudos Proofreading </h2><hr>
                        <div class="panel">
                            <div class="panel-body">   
                                <h3 class="title-hero ">List of Kudos</h3>        
                                <div class="example-box-wrapper">
                                    <table id="datatable-responsive" class="table table-striped table-bordered table-condensed responsive no-wrap" cellspacing="0" width="100%" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Ambassador Name</th>
                                                <th>Campaign</th>
                                                <th>Kudos Type</th>
                                                <th>Proofreading</th>
                                                <th>Date Added</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>

                                        <tfoot>
                                            <tr>

                                                <th><b>Ambassador Name</b></th>
                                                <th><b>Campaign</b></th>
                                                <th><b>Kudos Type</b></th>
                                                <th><b>Proofreading</b></th>
                                                <th><b>Date Added</b></th>
                                                <th><b>Action</b></th>

                                            </tr>
                                        </tfoot>

                                        <tbody>
                                            <?php foreach($kudos as $k => $val){ ?>
                                            <tr>
                                                <td><?php echo $val->ambassador; ?></td>
                                                <td><?php echo $val->campaign; ?></td>
                                                <td><?php echo $val->kudos_type; ?></td>
                                                <td><?php if($val->proofreading=='Pending'){ ?>
                                                    <p style="color: red;""><?php echo $val->proofreading; ?></p>
                                                    <?php }?>
                                                    <?php if($val->proofreading=='Done'){ ?>
                                                    <p style="color: green;""><?php echo $val->proofreading; ?></p>
                                                    <?php }?>
                                                    <?php if($val->proofreading=='In Progress'){ ?>
                                                    <p style="color: #f29341;""><?php echo $val->proofreading; ?></p>
                                                    <?php }?>
                                                </td>
                                                <td><?php echo $val->date_given; ?></td>
                                                <td style="text-align: center;">
                                                    <button style="width: 6em;" id="updateButton" class="btn btn-sm btn-info" data-toggle="modal" data-target="#myModalUpdate<?php echo $val->kudos_id; ?>">Update</button>
                                                </td>


                                            </tr>
                                            <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/pretty-photo/prettyphoto.css">
                                            <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/pretty-photo/prettyphoto.js"></script>
                                            <script type="text/javascript">
                                                /* PrettyPhoto */

                                                $(document).ready(function() {
                                                    $(".prettyphoto").prettyPhoto();
                                                });
                                            </script>
                                            <div id="myModalUpdate<?php echo $val->kudos_id; ?>" class="modal fade" role="dialog" style="height: auto !important;">
                                                <div class="modal-dialog" style="width: 500px; height: auto !important;">

                                                    <div class="modal-content">
                                                        <div class="modal-header" style="background: #1f2e2e; color: white;">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            
                                                            <h3 class="modal-title float-left"><img src="<?php echo base_url();?>assets/images/proofreading-icon.png" width="40" height="40">    Proofread</h3>
                                                            

                                                            <!--<h3 class="modal-title float-right" style="position: relative; top:5px; right: 20px;"><?php echo $val->date_given; ?></h3>-->
                                                        </div>
                                                        <div class="modal-body">

                                                            <div id="AddUserOption">  

                                                                <div id="DivAddSingleUser"  >
                                                                    <form class="form-horizontal" data-parsley-validate id="updateForm<?php echo $val->kudos_id; ?>"> 
                                                                        <div class="example-box-wrapper">

                                                                            <?php if($val->kudos_type=="Email"){ ?>
                                                                            <div style="padding: 0; margin: 0; height: auto; overflow: auto; border: 1px solid black; border-radius: 5px; " >
                                                                                <div style="background-color: #1f2e2e; height: auto; overflow: auto; border-bottom: 1px solid black; padding: 5px;">
                                                                                    <h4 style="text-align: center; color: white;">From: <?php echo $val->client_name;?> (<?php echo $val->client_email?>)</h4>
                                                                                </div>
                                                                                <div align="center" style="padding: 5px; margin: 5px;">
                                                                                    <a href="<?php echo base_url();?><?php echo $val->file;?>" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="">
                                                                                        <img width="445px" height="250px" src="<?php echo base_url();?><?php echo $val->file;?>">
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <?php } ?>
                                                                            <div style="padding: 0; margin: 0; height: auto; overflow: auto; border: 1px solid black; border-radius: 5px; <?php if($val->kudos_type=="Email"){ echo "display: none;"; }?>" id="revision<?php echo $val->kudos_id; ?>">
                                                                                <div style="background-color: #1f2e2e; height: auto; overflow: auto; border-bottom: 1px solid black; padding: 5px;">
                                                                                    <h4 style="text-align: center; color: white;">Comment</h4>
                                                                                </div>
                                                                                <textarea rows="8" type="text" class="form-control"  id="revisionText<?php echo $val->kudos_id; ?>" placeholder="Enter Revision" style="margin: 1px 0px; font-size: 18px;" required="" data-parsley-range="[10,250]"><?php if($val->kudos_type=="Call"){ echo $val->comment; }?></textarea>
                                                                                <span id="span_proofreading<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                                            </div>
                                                                            

                                                                            <div class="form-group" style="height: auto; overflow: auto; <?php if($val->kudos_type=="Call"){ echo "display: none;"; }?>">
                                                                                <button class="btn btn-s" id='reviseComment<?php echo $val->kudos_id?>' type="button">Edit</button>
                                                                            </div>
                                                                            <div class="form-group" style="display: none; height: auto; overflow: auto;">
                                                                                <label class="col-sm-3 control-label float-left">Supervisor</label>
                                                                                <div class="col-sm-9">
                                                                                    <input type="text" value="<?php echo $val->uid; ?>" class="form-control" id="addSupervisor<?php echo $val->kudos_id; ?>" >
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <div class="form-group" style="height: auto; overflow: auto;">
                                                                                <h4 class="col-sm-4 control-label float-left"><b>Campaign</b></h4>
                                                                                <div class="col-sm-8">
                                                                                    <h4><?php echo $val->campaign;?></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group" style="height: auto; overflow: auto;">
                                                                                <h4 class="col-sm-4 control-label float-left"><b>Ambassador</b></h4>
                                                                                <div class="col-sm-8">
                                                                                    <h4><?php echo $val->ambassador;?></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group" style="height: auto; overflow: auto; <?php if($val->kudos_type=="Email"){ echo "display: none;"; }?>">
                                                                                <h4 class="col-sm-4 control-label float-left"><b>Client's Name</b></h4>
                                                                                <div class="col-sm-8">
                                                                                    <h4><?php echo $val->client_name;?></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group" style="height: auto; overflow: auto; <?php if($val->kudos_type=="Email"){ echo "display: none;"; }?>">
                                                                                <h4 class="col-sm-4 control-label float-left"><b>Client's Phone Number</b></h4>
                                                                                <div class="col-sm-8">
                                                                                    <h4><?php echo $val->phone_number;?></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group" style="height: auto; overflow: auto; display: none;">
                                                                                <h4 class="col-sm-4 control-label float-left"><b>Kudos Card</b></h4>
                                                                                <div class="col-sm-8">
                                                                                    <select class="form-control" id="addKudosCard<?php echo $val->kudos_id; ?>" required="true" data-trigger="change" <?php if($val->proofreading!="Done"){ echo "disabled";}?>>
                                                                                        <option value="">-- Select Kudos Card Status --</option>
                                                                                        <option
                                                                                        <?php
                                                                                        if ($val->kudos_card=='Pending'){
                                                                                            echo 'selected="selected"';
                                                                                        } 
                                                                                        ?>
                                                                                        >Pending</option>
                                                                                        <option
                                                                                        <?php
                                                                                        if ($val->kudos_card=='In Progress'){
                                                                                            echo 'selected="selected"';
                                                                                        } 
                                                                                        ?>
                                                                                        >In Progress</option>
                                                                                        <option
                                                                                        <?php
                                                                                        if ($val->kudos_card=='Done'){
                                                                                            echo 'selected="selected"';
                                                                                        } 
                                                                                        ?>
                                                                                        >Done</option>
                                                                                    </select>
                                                                                    
                                                                                    <?php if($val->proofreading!="Done"){?>
                                                                                    <br>
                                                                                    <span id="span_kudosCard<?php echo $val->kudos_id; ?>" style="color: red;">Proofreading is required.</span>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group" style="height: auto; overflow: auto; display: none;">
                                                                                <h4 class="col-sm-4 control-label float-left"><b>Reward Status</b></h4>
                                                                                <div class="col-sm-8">
                                                                                    <select class="form-control" id="addRewardStatus<?php echo $val->kudos_id; ?>" required="true" data-trigger="change" <?php if($val->proofreading!="Done"){ echo "disabled";}?>>
                                                                                        <option value="" >-- Select Reward Status --</option>
                                                                                        <option
                                                                                        <?php
                                                                                        if ($val->reward_status=='Pending'){
                                                                                            echo 'selected="selected"';
                                                                                        } 
                                                                                        ?>
                                                                                        >Pending</option>
                                                                                        <option
                                                                                        <?php
                                                                                        if ($val->reward_status=='Requested'){
                                                                                            echo 'selected="selected"';
                                                                                        } 
                                                                                        ?>
                                                                                        >Requested</option>
                                                                                    </select>
                                                                                    
                                                                                    <?php if($val->proofreading!="Done"){?>
                                                                                    <br>
                                                                                    <span id="span_rewardStatus<?php echo $val->kudos_id; ?>" style="color: red;">Proofreading is required.</span>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group" style="height: auto; overflow: auto;">

                                                                                <div align="center">
                                                                                    <button class="btn btn-s btn-warning float-center" id='BtnBackAddMenu2' class="close" data-dismiss="modal"> Cancel</button>
                                                                                    <button type="submit" class="btn btn-s btn-success float-center" id='myModalUpdates<?php echo $val->kudos_id; ?>'>Update</button>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                            
                                                                            



                                                                            


                                                                        </div>
                                                                    </form>
                                                                </div> 




                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <script>
                                                $(function(){ 
                                                    $("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false,});
                                                    $("#reviseComment<?php echo $val->kudos_id?>").on("click", function(){

                                                     $("#revision<?php echo $val->kudos_id; ?>").toggle();

                                                 });

                                                    $('#updateForm<?php echo $val->kudos_id; ?>').submit(function(event){

                                                        if($("#revision<?php echo $val->kudos_id; ?>").is(":visible")){
                                                            //alert("visible");
                                                            if ($("#revisionText<?php echo $val->kudos_id; ?>").val()==""){

                                                                $("#revisionText<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                                $("#span_proofreading<?php echo $val->kudos_id; ?>").text("This is required.");
                                                                event.preventDefault();
                                                            } else {

                                                                var pfrd = "Done";
                                                                if ($("#revisionText<?php echo $val->kudos_id; ?>").val()=="<?php echo $val->comment;?>"){
                                                                    var comment = "<?php echo $val->comment;?>";
                                                                    var revision = "";
                                                                    var r_by = "";
                                                                } else {
                                                                    var revision = $("#revisionText<?php echo $val->kudos_id; ?>").val();
                                                                    var comment = "<?php echo $val->comment;?>";
                                                                    var r_by = "<?php echo $_SESSION['fname'];?><?php echo " ";echo $_SESSION['lname'];?>";
                                                                }
                                                                
                                                                
                                                                var k_id = <?php echo $val->kudos_id; ?>;

                                                                $.confirm({
                                                                    title: 'UPDATE KUDOS',
                                                                    content: 'Update Kudos?',
                                                                    buttons: {
                                                                        confirm: function () {
                                                                            dataString = "pfrd="+pfrd+"&comment="+comment+"&revision="+revision+"&r_by="+r_by+"&k_id="+k_id;
                                                                            $.ajax({
                                                                                type: "POST",
                                                                                url: "<?php echo base_url(); ?>index.php/Kudos/proofreadingUpdate/",
                                                                                data: dataString,
                                                                                cache: false,
                                                                                success: function(html)
                                                                                {
                                                                                    $.confirm({
                                                                                        title: 'SUCCESS!',
                                                                                        content: 'Successfully updated!',
                                                                                        buttons: {
                                                                                            ok: function(){
                                                                                                location.reload();

                                                                                            }
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        },
                                                                        cancel: function () {
                                                                        },
                                                                    }
                                                                });

                                                                

                                                            }

                                                        } else {
                                                            $.confirm({
                                                                title: 'UPDATE KUDOS',
                                                                content: 'Update Kudos?',
                                                                buttons: {
                                                                    confirm: function () {
                                                                        var pfrd = "Done";
                                                                        var revision = $("#revisionText<?php echo $val->kudos_id; ?>").val();
                                                                        var r_by = <?php echo $val->uid; ?>;
                                                                        var k_id = <?php echo $val->kudos_id; ?>;

                                                                        dataString = "pfrd="+pfrd+"&revision="+revision+"&r_by="+r_by+"&k_id="+k_id;
                                                                        $.ajax({
                                                                            type: "POST",
                                                                            url: "<?php echo base_url(); ?>index.php/Kudos/proofreadingUpdate/",
                                                                            data: dataString,
                                                                            cache: false,
                                                                            success: function(html)
                                                                            {

                                                                                $.confirm({
                                                                                    title: 'SUCCESS!',
                                                                                    content: 'Successfully updated!',
                                                                                    buttons: {
                                                                                        ok: function(){
                                                                                            location.reload();
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    },
                                                                    cancel: function () {
                                                                    },
                                                                }
                                                            });
                                                            
                                                        }



                                                        event.preventDefault();

                                                    });
$('#addCampaign<?php echo $val->kudos_id; ?>').on('change', function(){
    $.ajax({
        type : 'POST',
        data : 'campaign_id='+ $('#addCampaign<?php echo $val->kudos_id; ?>').val(),
        url : "<?php echo base_url(); ?>index.php/Kudos/getAgents/",
        success : function(data){
                                                                //data returns your name, iterate through it and add the name to another select
                                                                $('#agentNames<?php echo $val->kudos_id; ?>').html(data);
                                                                console.log(data);
                                                            }
                                                        });
});

$('#revisionText<?php echo $val->kudos_id; ?>').on('keyup', function() {
    if($("#revisionText<?php echo $val->kudos_id; ?>").val()==""){
        $("#revisionText<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
        $("#span_proofreading<?php echo $val->kudos_id; ?>").text("This is required.");
    } else {
        $("#revisionText<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
        $("#span_proofreading<?php echo $val->kudos_id; ?>").text("");
    }
});

$('#addProofreading<?php echo $val->kudos_id; ?>').on('change', function(){
    if($("#addProofreading<?php echo $val->kudos_id; ?>").val()==""){
        $("#addProofreading<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
        $("#span_proofreading<?php echo $val->kudos_id; ?>").text("This is required.");
    } else {
        $("#addProofreading<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
        $("#span_proofreading<?php echo $val->kudos_id; ?>").text("");
    }
});

$('#addKudosCard<?php echo $val->kudos_id; ?>').on('change', function(){
    if($("#addKudosCard<?php echo $val->kudos_id; ?>").val()==""){
        $("#addKudosCard<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
        $("#span_kudosCard<?php echo $val->kudos_id; ?>").text("This is required.");
    } else {
        $("#addKudosCard<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
        $("#span_kudosCard<?php echo $val->kudos_id; ?>").text("");
    }
});

$('#addRewardStatus<?php echo $val->kudos_id; ?>').on('change', function(){
    if($("#addRewardStatus<?php echo $val->kudos_id; ?>").val()==""){
        $("#addRewardStatus<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
        $("#span_rewardStatus<?php echo $val->kudos_id; ?>").text("This is required.");
    } else {
        $("#addRewardStatus<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
        $("#span_rewardStatus<?php echo $val->kudos_id; ?>").text("");
    }
});

});


</script>
<?php }?>

</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



</div>
</div>
</div>
</body>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/menu.js"></script>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script>
    $(function(){
        $('#addKudosType').on('change', function(){
            if(this.value=='Call'){
                $('#append').show();
                //hide file and email add
                $('#addFile').addClass('hidden');
                $('#FileAdd').addClass('hidden');
                $('#addEmailAdd').addClass('hidden');
                $('#AddEmailAdd').addClass('hidden');
                //disable file and email add
                $('input[type="file"]').attr("disabled", true);
                $('input[type="email"]').attr("disabled", true);
                //remove comment class hidden and add seen
                $('#addCustomerComment').removeClass('hidden');
                $('#CustomerCommentAdd').removeClass('hidden');
                $('#addCustomerComment').addClass('seen');
                $('#CustomerCommentAdd').addClass('seen');
                //remove disabled attr for comment
                $('#addCustomerComment').attr("disabled", false);
                //remove hidden class in phone number and add seen
                $('#addPhoneNumber').removeClass('hidden');
                $('#PhoneNumberAdd').removeClass('hidden');
                $('#addPhoneNumber').addClass('seen');
                $('#PhoneNumberAdd').addClass('seen');
                $('#addPhoneNumber').attr("disabled", false);
            }
            if(this.value=='Email'){
                $('#append').show();
                $('input[type="file"]').attr("disabled", false);
                $('#addFile').removeClass('hidden');
                $('#FileAdd').removeClass('hidden');
                $('#addFile').addClass('seen');
                $('#FileAdd').addClass('seen');
                $('input[type="email"]').attr("disabled", false);
                $('#addEmailAdd').removeClass('hidden');
                $('#AddEmailAdd').removeClass('hidden');
                $('#addEmailAdd').addClass('seen');
                $('#AddEmailAdd').addClass('seen');
                $('#addCustomerComment').attr("disabled", "disabled");
                $('#addCustomerComment').addClass('hidden');
                $('#CustomerCommentAdd').addClass('hidden');
                $('#addPhoneNumber').attr("disabled", true);
                $('#addPhoneNumber').addClass('hidden');
                $('#PhoneNumberAdd').addClass('hidden');

            }
            if(this.value==''){
                $('#append').hide();
            }
        });
        
        $('#addCampaign').on('change', function(){
            $.ajax({
                type : 'POST',
                data : 'campaign_id='+ $('#addCampaign').val(),
                url : "<?php echo base_url(); ?>index.php/Kudos/getAgents/",
                //dataType: 'json',
                success : function(data){
                    $('#agentNames').html(data);
                    //JSON.stringify(data);
                  /*  $.each(data,function(key,value){
                        alert(value.emp_id);
                        $('#agentNames').append('<option value="'+value.emp_id+'">'+value.fullname+'</option>');
                    });*/

                    console.log(data);
                }
            });
        });


        $('#addForm').submit(function(event){ 
            var emp_id = $("#agentNames").val();
            var campaign = $("#addCampaign").val();
            var k_type = $("#addKudosType").val();
            var c_name = $("#addCustomerName").val();
            var p_number = $("#addPhoneNumber").val();
            var e_add = $("#addEmailAdd").val();
            var comment = $("#addCustomerComment").val();
            var supervisor = $("#addSupervisor").val();
            var file = $("#addFile").val();
            var p_reward = $("#addPrefReward").val();
            var pfrd = $("#addProofreading").val();
            var k_card = $("#addKudosCard").val();
            var r_status = $("#addRewardStatus").val();

            dataString = "emp_id="+emp_id+"&campaign="+campaign+"&k_type="+k_type+"&c_name="+c_name+"&p_number="+p_number+"&e_add="+e_add+"&comment="+comment+"&supervisor="+supervisor+"&file="+file+"&p_reward="+p_reward+"&pfrd="+pfrd+"&k_card="+k_card+"&r_status="+r_status;

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Kudos/addKudos/",
                data: dataString,
                cache: false,
                success: function(html)
                {
                    alert("Succesfully Added!");
                    location.reload();
                }
            });   
            event.preventDefault();
        });


        
        $('#BtnBackAddMenu').click(function(){   
            $("#addAmbassadorName").val("");
            $("#addCampaign").val("");
            $("#addCustomerName").val("");
            $("#addCustomerComment").val("");
            $("#addFile").val("");
            $("#addPrefReward").val("");
            $("#addProofreading").val("");
            $("#addKudosCard").val("");
            $("#addRewardStatus").val("");

        });



        
    });
</script>
<script>
    $(function(){
        "use strict";
        var obj = JSON.parse(<?php echo "'".json_encode($name)."'"; ?>);
        var all = obj.map(function (value) {
            return value.name;
        });
        $("#addAmbassadorName2").autocomplete({source: all});
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>



<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>