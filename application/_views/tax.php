<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  </style>


  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });
 </script>

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 <script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd-yyyy'
    });
  });

</script>


</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div class="container">

          <script type="text/javascript">

            /* Datatables export */

            $(document).ready(function() {
              var table = $('#datatable-tabletools').DataTable();
            } );

            $(document).ready(function() {
              $('.dataTables_filter input').attr("placeholder", "Search...");
            });

          </script>


          <div id="page-title">
		  	<a href="../Payroll/" class="btn btn-xs btn-success glyph-icon   icon-mail-reply" title="" style="margin-bottom: 9px;"> Back</a>
            <div class="row">
              <div class="col-md-2">
                <select id='date' class="form-control input-sm">
                  <?php foreach($dates as $d):?>
                    <option value='<?php echo $d->t_effec_id;?>' <?php if($d->t_effec_id == $id){echo "selected";}?>>
                      <?php echo $d->effectivity_date;?>
                    </option>
                  <?php endforeach;?>
                </select>
              </div> 
              <div class="col-md-1">
                <a href="<?php echo base_url();?>index.php/Payroll/addtax" class='btn btn-primary btn-sm'><span class='icon-typicons-plus'></span> Tax</a>
              </div>   
            </div><br>
            <div class="row">
              <div class="col-md-12">

                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                 <?php if($values!=NULL){?>
                 <thead>
                  <tr>
                   <th class='text-center' colspan="2" style='background:#787a7b;color:white;border-top-left-radius: 10px'><?php echo $values[0]->type;?></th>
                   <?php $data = explode(",",$values[0]->col);?>
                   <?php foreach($data as $d):?>
                    <th class='text-center' style='background:#787a7b;color:white;<?php if ($d === end($data))echo 'border-top-right-radius: 10px';?>'><?php echo $d;?></th>
                  <?php endforeach;?>
                </tr>
                <tr>
                 <th class='text-center' colspan="2" style='background:#888;color:white;'>Exemption</th>
                 <?php $data = explode(",",$values[0]->deduction);?>
                 <?php foreach($data as $d):?>
                  <th class='text-right' style='background:#888;color:white;'><?php echo number_format($d,2);?></th>
                <?php endforeach;?>
              </tr>
              <tr>
               <th class='text-center' colspan="2" style='background:#787a7b;color:white;'>Status</th>
               <?php $data = explode(",",$values[0]->percent);?>
               <?php foreach($data as $d):?>
                <th class='text-right' style='background:#787a7b;color:white;'><?php echo $d;?></th>
              <?php endforeach;?>
            </tr>
          </thead>
          <tbody>

            <?php foreach($values as $v):?>
              <tr>
                <th class='text-left' style='font-weight: bold;background:#888;color:white;'><?php echo $v->description;?></th>
                <td class='text-right'><?php echo $v->rate;?></td>
                <?php $data = explode(",",$v->salarybase);?>
                <?php foreach($data as $d):?>
                  <td class='text-right'><?php echo number_format($d,2);?></td>
                <?php endforeach;?>
              </tr>
            <?php endforeach; ?>
          </tbody>

          <?php }else{echo 'No current tax rate.';}?>
        </table>
      </div>
    </div>

  </div>

  <div class="row" hidden>
    <div class="col-md-8">
      <div class="panel">
        <div class="panel-body">
          <h3 class="title-hero">
            Recent sales activity
          </h3>
          <div class="example-box-wrapper">
            <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
          </div>
        </div>
      </div>




      <div class="content-box">
        <h3 class="content-box-header bg-default">
          <i class="glyph-icon icon-cog"></i>
          Live server status
          <span class="header-buttons-separator">
            <a href="#" class="icon-separator">
              <i class="glyph-icon icon-question"></i>
            </a>
            <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
              <i class="glyph-icon icon-refresh"></i>
            </a>
            <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
              <i class="glyph-icon icon-times"></i>
            </a>
          </span>
        </h3>
        <div class="content-box-wrapper">
          <div id="data-example-3" style="width: 100%; height: 250px;"></div>
        </div>
      </div>

    </div>

  </div>
</div>



</div>
</div>
</div>


<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $('select').change(function () {
   var optionSelected = $(this).find("option:selected");
   var valueSelected  = optionSelected.val();
   var textSelected   = optionSelected.text();
   window.location = "<?php echo base_url();?>index.php/Payroll/tax/"+valueSelected;
 });
</script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>