 <!DOCTYPE html> 
 <html  lang="en">

 <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
 <head>

 	<style>
 		/* Loading Spinner */
 		.spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}

 		#container {
 			width: 150px;
 			border: 1px solid #bbbbbb;
 			padding: 20px;
 			float: left;
 			text-align:center;
 			margin-right: 7px;
 			margin-top: 7px;
 			height: 180px;
 			border-radius: 10px;
 		}
 		div#container:hover {
 			border: 1px solid rgba(0, 150, 136, 0.35);
 			background: rgba(0, 150, 136, 0.19);
 			color: #009688;
 			cursor: pointer;
 		}
 		.time ul{
 			list-style-type: none;
 			margin-left: -39px;
 		}
 		.note{
 			font-size: initial;
 			background: #4a4a4a;
 			padding: 10px;
 		}

 		.timeDetails{
 			padding: 10px;
 			background: gray;
 		}
 		.time{
 			border-radius: 13px;
 			color: white;
 			font-family: fantasy;
 			display: inline-flex;
 		}
 		.highcharts-credits{
 			display:none;
 		}
 		.grow { transition: all .2s ease-in-out; }
 		.grow:hover { transform: scale(1.5); }
 		@font-face {
 			font-family: customfont;
 			src: url('../assets/font/Aileron-Bold.otf');
 		}
 		@font-face {
 			font-family: customfonttwo;
 			src: url('../assets/font/Aileron-SemiBold.otf');
 		}
 		h3.step {
 			background:#888;
 			border-radius: 0.8em;
 			-moz-border-radius: 0.8em;
 			-webkit-border-radius: 0.8em;
 			color: #111;
 			display: inline-block;
 			font-weight: bold;
 			line-height: 1.6em;
 			margin-right: 5px;
 			text-align: center;
 			width: 1.6em; 
 		}
 	</style>

 	<meta charset="UTF-8">
 	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
 	<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
 	<meta name="description" content="">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

 	<!-- Favicons -->

 	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
 	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
 	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
 	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
 	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



 	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
 	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">
 	<link href="<?php echo base_url();?>assets/progress/ui-progress-bar.css" media="screen" rel="stylesheet" type="text/css" />

 	<!-- JS Core -->

 	<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>
 	<script type="text/javascript">
 		$(window).load(function(){
 			setTimeout(function() {
 				$('#loading').fadeOut( 400, "linear" );
 			}, 300);
 		});
 	</script>

 	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
 	<script type="text/javascript">
 		/* Datepicker bootstrap */

 		$(function(){"use strict";$("#daterangepicker-example").daterangepicker({startDate:moment().subtract("days",29),endDate:moment(),minDate:"04/01/2017",maxDate:new Date(),dateLimit:{days:60},showDropdowns:!0,showWeekNumbers:!0,timePicker:!1,timePickerIncrement:1,timePicker12Hour:!0,ranges:{Today:[moment(),moment()],Yesterday:[moment().subtract("days",1),moment().subtract("days",1)],"Last 7 Days":[moment().subtract("days",6),moment()],"Last 30 Days":[moment().subtract("days",29),moment()],"This Month":[moment().startOf("month"),moment().endOf("month")],"Last Month":[moment().subtract("month",1).startOf("month"),moment().subtract("month",1).endOf("month")]},opens:"left",buttonClasses:["btn btn-default"],applyClass:"small bg-green",cancelClass:"small ui-state-default",format:"MM/DD/YYYY",separator:" - ",locale:{applyLabel:"Apply",fromLabel:"From",toLabel:"To",customRangeLabel:"Custom Range",daysOfWeek:["Su","Mo","Tu","We","Th","Fr","Sa"],monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],firstDay:1}},function(a,b){console.log("Callback has been called!"),$("#daterangepicker-custom span").html(a.format("MMMM D, YYYY")+" - "+b.format("MMMM D, YYYY"))}),$("#daterangepicker-custom span").html(moment().subtract("days",29).format("MMMM D, YYYY")+" - "+moment().format("MMMM D, YYYY"))})

 	</script> 

 	<!-- jQueryUI Datepicker -->

 	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
 	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
 	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

 	<!-- Bootstrap Daterangepicker -->

 	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
 	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
 	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
 	<!--<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>-->
 	<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/datatable/datatable.css">-->
 	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
 	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
 	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>


 	<script>
 		$(function(){

 			/* Datatable row highlight */

 			$(document).ready(function() {
 				var table = $('#datatable-row-highlight').DataTable();

 				$('#datatable-row-highlight tbody').on( 'click', 'tr', function () {
 					$(this).toggleClass('tr-selected');
 				} );
 			});



 			$(document).ready(function() {
 				$('.dataTables_filter input').attr("placeholder", "Search...");
 			});

 		});
 	</script>

 </head>


 <body>
 	<div id="sb-site">


 		<div id="loading">
 			<div class="spinner">
 				<div class="bounce1"></div>
 				<div class="bounce2"></div>
 				<div class="bounce3"></div>
 			</div>
 		</div>

 		<div id="page-wrapper">
 			<div id="page-header" class="bg-black">
 				<div id="mobile-navigation">
 					<button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
 					<a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
 				</div>
 				<div id="header-logo" class="logo-bg">
 					<img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
 					<a id="close-sidebar" href="#" title="Close sidebar">

 						<i class="glyph-icon icon-angle-left"></i>

 					</a>
 				</div>
 				<div id='headerLeft'>
 					<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
 					<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
 					<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">


 					<div id="header-nav-left">
 						<div class="user-account-btn dropdown">
 							<a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
 								<img width="28" src="<?php echo base_url()."assets/images/SZ_logo.png"; ?>" alt="Profile image">
 								<span><?php echo $this->session->userdata('lname'); ?></span>
 								<i class="glyph-icon icon-angle-down"></i>
 							</a>

 							<div class="dropdown-menu float-left">
 								<div class="box-sm">
 									<div class="login-box clearfix">
 										<div class="user-img">
 											<a href="#" title="" class="change-img">Change photo</a>
 											<img width="28" src="<?php echo base_url()."assets/images/SZ_logo.png"; ?>" alt="Profile image">
 										</div>
 										<div class="user-info">
 											<span>
 												<?php echo $this->session->userdata('fname')." ".$this->session->userdata('lname'); ?> <i><?php echo $this->session->userdata('description'); ?></i>
 											</span>
 											<a href="#" title="Edit profile">Edit profile</a>
 										</div>
 									</div>
 									<div class="divider"></div>

 									<div class="pad5A button-pane button-pane-alt text-center">
 										<a href="<?php echo base_url()."index.php/";?>login/logout" class="btn display-block font-normal btn-danger">
 											<i class="glyph-icon icon-power-off"></i>
 											Logout
 										</a>
 									</div>
 								</div>
 							</div> 

 						</div>
 					</div><!-- #header-nav-left -->
 					<div id="header-nav-right">

 					</div><!-- #header-nav-right -->


 					<script>
 						$(function(){

 							setInterval(function()
 							{
 								$('#header-nav-right').load('<?php echo base_url(); ?>/index.php/dtr/getTime');
 								if(window.console || window.console.firebug) {
 									// console.clear();
 								}

 							}, 1000);  

 						});	
 					</script>
 				</div>



 			</div>
 			<div id="page-sidebar">
 				<div class="scroll-sidebar">


 					<ul id="sidebar-menu">
 					</ul>
 				</div>
 			</div>
 			<div id="page-content-wrapper">
 				<div id="page-content">

 					<div class="container">
 						<div id="page-title">
 							<div class="form-group">
 								<div class="col-md-6">
 									<div class="profile-box content-box" >
 										<div class="profile-box content-box">
 											<div class="content-box-header clearfix bg-black">
 												<div class="user-details">
 													<i class="glyph-icon icon-clock-o"></i> 
 													<?php if($dtr_log["entry"]=="I"){ ?>
 													DTR
 													<?php }else{ ?>
 													DTR
 													<span id="sched_id2" style="display:none"><?php echo $schedIn["sched_id"]; ?></span>
 													<span id="acc_time_id2" style="display:none"><?php echo $schedIn["acc_time_id"]; ?></span>
 													<?php } ?>
 												</div>
 											</div>
 											<?php if($_SESSION["class"]=="Admin"){ ?>
 											<div class="text-center" style="padding: 93px 15px 15px 15px;height: 400px;">
 												<?php }else{?>
 												<div class="text-center" style="padding: 48px 15px 68px 15px;">

 													<?php }?>
 													<?php if($dtr_log["entry"]=="O"){ ?>
 													<?php 
 													if($schedTypes!="none"){
 														if($schedTypes == "Normal"){
 															$firstTD = "Schedule";
 															$secondTD = date("l F j, Y");
 															$color = "#4CAF50";
 														}else if($schedTypes == "WORD" ){
 															$firstTD = "Work on Rest Day";
 															$secondTD = " ";
 															$color = "#FF9800";

 														}else if($schedTypes == "Rest Day" ){
 															$firstTD = "Rest Day";
 															$secondTD = " ";
 															$color = "#f14f4f";
 														}else if($schedTypes == "Leave" ){
 															$firstTD = "Leave";
 															$secondTD = " ";
 															$color = "#009688";
 														}else{
 															$firstTD = "No Schedule";
 															$secondTD = "Please inform your supervisor to set you a schedule.";
 															$color = "#f14f4f";
 														} 
 													}else{
 														$firstTD = "No Schedule";
 														$secondTD = "Please inform your supervisor to set you a schedule.";
 														$color = "#f14f4f";

 													} 
 													?>
 													<?php if(!empty($schedIn["shift"])){ ?>
 													<table style="border: 1px solid #dbdbdb;margin: 0 auto;">
 														<tbody>
 															<tr><td style="font-size: xx-large;background:<?php echo $color; ?>;color: white;padding: 1px;"><?php echo $firstTD; ?></td></tr>
 															<tr><td style="font-size: large;padding-left: 20px;padding-right: 10px;padding-top: 11px;padding-bottom: 4px;"> <?php echo $secondTD; ?> </td></tr>
 															<tr style=""><td style="padding-left: 100px;padding-right: 100px;"><?php echo $schedIn["shift"] ; ?></td></tr>
 														</tbody>
 													</table>
 													<?php }else{  ?>

 													<table style="border: 1px solid #dbdbdb;margin: 0 auto;">
 														<tbody>
 															<tr><td style="font-size: xx-large;background: <?php echo $color; ?>;color: white;padding: 1px;"><?php echo $firstTD; ?></td></tr>
 															<tr><td style="font-size: large;padding-left: 20px;padding-right: 10px;padding-top: 11px;padding-bottom: 4px;"> <?php echo $secondTD; ?> </td></tr>
 															<tr><td style="font-size: large;padding-left: 20px;padding-right: 10px;padding-top: 11px;padding-bottom: 4px;">  <?php echo "Default Schedule: ".$defaultshift; ?></td></tr>
 														</tbody>
 													</table>
 													<?php }  ?>
 													<br>
 													<?php
 													if(empty($schedIn["shift"])){
 														$id_sched="btn-cin2";
 														echo "<span id='defaultSched' hidden>".$defaultSched."</span>";
 													}else{
 														$id_sched="btn-cin";
 													}

 													?> 
 													<a href="#" class="btn vertical-button remove-border btn-info text-center" title="" id="<?php echo $id_sched; ?>"  style="width:50%">
 														<span class="glyph-icon icon-separator-vertical">
 															<i class="glyph-icon icon-clock-o"></i>
 														</span>
 														<span class="button-content">Clock in</span>
 													</a>

 													<?php }else{ ?>
 													<div> 
						 <!--<div> <b>Date:</b> <?php echo $schedOut["sched_date"] ; ?> </div> 
						 <div><b>Shift:</b> <?php echo $schedOut["shift"] ; ?></div> -->
						 

						</div> 
						<span id="sched_id" style="display:none"><?php echo $schedOut["sched_id"]; ?></span>
						<span id="acc_time_id" style="display:none"><?php echo $schedOut["acc_time_id"]; ?></span>
						<span id="dtr_id" style="display:none"><?php echo $schedOut["dtr_id"]; ?></span>
						<br>
						<h1 id="time-elapsed">

						</h1> <span style="color:green"><b>(Online)</b></span>
						<br>
						<a href="#" class="btn vertical-button remove-border btn-danger text-center" title="" id="btn-cout" style="width:50%">
							<span class="glyph-icon icon-separator-vertical">
								<i class="glyph-icon icon-clock-o"></i>
							</span>
							<span class="button-content">Clock out</span>
						</a>

						<?php }  ?>
					</div>
				</div>
				<?php if($_SESSION["class"]=="Agent"){ ?>
				<?php if($dtr_log["entry"]=="I"){ ?>
				<?php 
				$break = explode(",",$breakz["break"]);

/* 		 $fb = (in_array("FIRST BREAK", $break)) ? "inline-table" : "none";
		 $lb = (in_array("LUNCH ", $break)) ? "inline-table" : "none";
		 $last = (in_array("LAST BREAK", $break)) ? "inline-table" : "none";;
		
 */ 


		 if(count($break)>1){

		 	for($i=0;$i<count($break);$i++){

		 		$bk = explode("|",$break[$i]);
				 // echo $bk[0]."".$bk[1]."<br>";

		 		if($bk[1]=="FIRST BREAK"){
		 			$fb = $bk[0];
		 		}
		 		if($bk[1]=="LUNCH"){
		 			$lb =  $bk[0];
		 		}
		 		if($bk[1]=="LAST BREAK"){
		 			$last = $bk[0];
		 		}
		 	}
		 	?>

		 	<?php $fbs1= ($fbs["break"]>0) ? "disabled" : ""; ?>
		 	<?php $lunchs1= ($lunchs["break"]>0) ? "disabled" : ""; ?>
		 	<?php $lbs1= ($lbs["break"]>0) ? "disabled" : ""; ?>
		 	<div class="text-center" style="padding: 0px 15px 15px 15px;">
		 		<a href="#" class="btn vertical-button remove-border btn-default text-center" title="" style="width:30%;padding:0px;background: #2196F3;color: white;"  onclick="breakAdd(2,<?php echo $fb; ?>)"  <?php echo $fbs1; ?>>
		 			<span class="glyph-icon icon-separator-vertical">
		 				<i class="glyph-icon icon-sign-in"></i>
		 			</span>
		 			<span class="button-content">First Break</span>
		 		</a>
		 		<a href="#" class="btn vertical-button remove-border btn-default text-center" title="" style="width:30%;padding:0px;background: #4CAF50;color: white;" onclick="breakAdd(3,<?php echo $lb; ?>)"  <?php echo $lunchs1; ?>>
		 			<span class="glyph-icon icon-separator-vertical">
		 				<i class="glyph-icon icon-cutlery"></i>
		 			</span>
		 			<span class="button-content">Lunch</span>
		 		</a>
		 		<a href="#" class="btn vertical-button remove-border btn-default text-center" title="" style="width:30%;padding:0px;background: rgb(221, 69, 69);color: white;" onclick="breakAdd(4,<?php echo $last; ?>)"  <?php echo $lbs1; ?>>
		 			<span class="glyph-icon icon-separator-vertical">
		 				<i class="glyph-icon icon-sign-out"></i>
		 			</span>
		 			<span class="button-content">Last Break</span>
		 		</a> <?php }else{ ?>
		 		<div style="text-align:center;color:red"><h3>No Break Schedule</h3></div>
		 		<?php }?>
		 	</div>
		 	<?php } ?>
		 	<?php } ?>
		 </div>

		</div>
		
		<div class="col-md-6">
			<div class="profile-box content-box">
				<div class="profile-box content-box">
					<div class="content-box-header clearfix bg-black">
						<div class="user-details">

							<span><i class="glyph-icon icon-rocket"></i> Activity <small>(within 12 hours) </small></span>
						</div>
					</div>
					<div class="text-center dashbrd" id="container-side" style="height: 400px">

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="profile-box content-box">
			<div class="profile-box content-box">
				<div class="content-box-header clearfix bg-black">
					<div class="user-details">

						<span><i class="glyph-icon icon-calendar"></i> Attendance Logs</span>
					</div>

				</div>
			</div>
			<div class="text-center" style="padding: 0px 15px 15px 15px;">
				<div class="form-group btn text-left">
					<label for="" class="col-sm-1 control-label">Date</label>
					<div class="col-sm-5">
						<div class="input-prepend input-group">
							<span class="add-on input-group-addon">
								<i class="glyph-icon icon-calendar"></i>
							</span>
							<input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control" value=" <?php echo date("m/d/Y")." - ".date("m/d/Y")?>" readonly>
						</div>
					</div>

				</div>

			</div>

			<input type="button" class="btn btn-info btn-xs" value="Show Logs" id="show" style="margin-left: 38px;margin-top: -21px;" >
			<?php if($_SESSION["class"]=="Admin"){ ?>

			<input type="button" class="btn btn-success btn-xs" value="Show Details" id="showdetailsAdmin"  style="margin-left:5px;margin-top: -21px;" >
			<?php }else{ ?>

			<input type="button" class="btn btn-success btn-xs" value="Show Details" id="showdetails"  style="margin-left:5px;margin-top: -21px;" >
			<?php } ?>


			<div id="container2">

			</div>
			<div class="loading-spinner" hidden>
				<i class="bg-black"></i>
				<i class="bg-black"></i>
				<i class="bg-black"></i>
				<i class="bg-black"></i>
				<i class="bg-black"></i>
				<i class="bg-black"></i>
			</div>
		</div>


	</div>

	<div id='Templatedesign'>

	</div>
</div>

<div class="row" hidden>
	<div class="col-md-8">
		<div class="panel">
			<div class="panel-body">
				<h3 class="title-hero">
					Recent sales activity
				</h3>
				<div class="example-box-wrapper">
					<div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
				</div>
			</div>
		</div>




		<div class="content-box">
			<h3 class="content-box-header bg-default">
				<i class="glyph-icon icon-cog"></i>
				Live server status
				<span class="header-buttons-separator">
					<a href="#" class="icon-separator">
						<i class="glyph-icon icon-question"></i>
					</a>
					<a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
						<i class="glyph-icon icon-refresh"></i>
					</a>
					<a href="#" class="icon-separator remove-button" data-animation="flipOutX">
						<i class="glyph-icon icon-times"></i>
					</a>
				</span>
			</h3>
			<div class="content-box-wrapper">
				<div id="data-example-3" style="width: 100%; height: 250px;"></div>
			</div>
		</div>

	</div>

</div>
</div>



</div>
</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #f45353;color: white;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"> This modal is currently on-maintenance</h4>
			</div>
			<div class="modal-body">
				<img src="<?php echo base_url()."assets/images/maintenance.gif"?>" style="width:100%">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id='15fivemodal' style='top:80px' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog" style='width:60%'>
		<div class="modal-content">
			<div class="modal-header pad25A" id='15fivemodalheader' style="background: linear-gradient(#aaa,white);border:none !important">
				<h2 class='text-center'  id='15fivemodalheadertext' style='font-family:customfont;text-shadow: 0 0 10px white;'></h2>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<!-- <span class="mrg10A">Awful</span> -->
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
							<button class="btn mrg10A grow" id='15fivebtn1' onclick="changequestion(1)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/05static.png" class='emoji'></button><br><br>
							<hr>
							<h3 id='15fivesq1' class='step'>1</h3>
						</div>
						<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
							<button class="btn mrg10A grow" id='15fivebtn2' onclick="changequestion(2)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/16static.png" class='emoji'></button><br><br>
							<hr>
							<h3 id='15fivesq2' class='step'>2</h3>
						</div>
						<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
							<button class="btn mrg10A grow" id='15fivebtn3' onclick="changequestion(3)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/06static.png" class='emoji'></button><br><br>
							<hr>
							<h3 id='15fivesq3' class='step'>3</h3>
						</div>
						<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
							<button class="btn mrg10A grow" id='15fivebtn4' onclick="changequestion(4)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/17static.png" class='emoji'></button><br><br>
							<hr>
							<h3 id='15fivesq4' class='step'>4</h3>
						</div>
						<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
							<button class="btn mrg10A grow" id='15fivebtn5' onclick="changequestion(5)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/23static.png" class='emoji'></button><br><br>
							<hr>
							<h3 id='15fivesq5' class='step'>5</h3>
						</div>
						<div class="col-md-1"></div>
					</div>
					<!-- <span class="mrg10A">Amazing!</span> -->
				</div>
				<br>
				<div class='modal-footer pad25A' id='15fivequestiondiv' style="display:none;">
					<h4 class='text-center text-bold font-size-18' style='font-family:customfonttwo;text-shadow: 1px 1px  black;color:white' id='15fivequestion'></h4>
					<br>
					<div class="row">
						<div class="col-md-11">
							<div class="form-group">
								<input type="text" class='form-control' name="" placeholder='Let us hear your feedback ...'>
							</div>
						</div>
						<div class="col-md-1">
							<button class='btn' style='background:white'><i class="glyph-icon icon-send"></i></button>
						</div>
					</div>
				</div>
			</div>

			
		</div>
	</div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
</div>
</body>

<script>
	function showUnknowLog(){

		var datee = $("#daterangepicker-example").val();	
		var txt= "";

		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/dtr/viewLogsDetailsofNoSched",
			data: {
				datee:datee
			},
			cache: false,
			beforeSend: function(){
				$(".loading-spinner").show();
				$("#container2").hide();
			},
			success: function(html)
			{
				$(".loading-spinner").hide();
				$("#container2").show();
				
				
				
				var obj = jQuery.parseJSON(html);
				var div = '';

				var login_details = "";
				var logout_details = "";
				var logout_zero = "";
				var login_zero = "";
				var lunchb = "";
				var firstb = "";
				var lastb = "";
				var total = 0;
				
				txt+= '<br> <div><span style="color:red">Note: </span> Please click here if you dont see your logs. <input type="button" class="btn btn-danger btn-xs" value="Show"  onclick="showUnknowLog()" style="margin-left:5px;margin-top: -5px;" ></div><br>';
				txt+= '<div id="TblUnknowLog"><table id="datatable-row-highlight" class="table table-striped table-bordered" cellspacing="0" width="100%">';
				txt+= '<thead>';
				txt+= '<tr>';
				txt+= ' <th>Date</th>';
				txt+= '  <th>Login</th>';
				txt+= '  <th>First Break</th>';
				txt+= ' <th>Lunch</th>';
				txt+= '  <th>Last Break</th>';
				txt+= '  <th>Logout</th>';
				txt+= '</tr>';
				txt+= '</thead>';

				txt+= '<tfoot>';
				txt+= '<tr>';
				txt+= '  <th>Date</th>';
				txt+= '   <th>Login</th>';
				txt+= '   <th>First Break</th>';
				txt+= '  <th>Lunch</th>';
				txt+= '  <th>Last Break</th>';
				txt+= ' <th>Logout</th>';
				txt+= '</tr>';
				txt+= '</tfoot>';
				txt+= '<tbody>';

				$(obj.sched_date).each(function(key,value){
					$.each(value,function(k, v ){


						login_zero = ((v["login"]==0) ? "--" : v["login"]);
						logout_zero = ((v["logout"]==0) ? "<i>--</i>" : v["logout"]);
						lunchb = ((v["lunchb"]==null) ? "<i>--</i>" : " "+v["lunchb"]);
						firstb = ((v["firstb"]==null) ? "<i>--</i>" : " "+v["firstb"]);
						lastb = ((v["lastb"]==null) ? "<i>--</i>" : " "+v["lastb"]);
						txt+= '<tr>';
						txt+= '   <td style="color:red"> No Schedule Set </td>';
						txt+= '  <td> '+login_zero+'</td>';
						txt+= '   <td>'+firstb+'</td>';
						txt+= '  <td>'+lunchb+'</td>';
						txt+= '  <td>'+lastb+'</td>';
						txt+= ' <td>'+logout_zero+'</td>';
						txt+= '</tr>';
					});

				});	
				
				txt+= '</tbody>';
				txt+= '</table></div>';
				$("#container2").html(txt);

			}
			
		});

	};
	function showUnknowLogAdmin(){

		var datee = $("#daterangepicker-example").val();	
		var txt= "";

		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/dtr/viewLogsDetailsofNoSched",
			data: {
				datee:datee
			},
			cache: false,
			beforeSend: function(){
				$(".loading-spinner").show();
				$("#container2").hide();
			},
			success: function(html)
			{
				$(".loading-spinner").hide();
				$("#container2").show();
				
				
				
				var obj = jQuery.parseJSON(html);
				var div = '';

				var login_details = "";
				var logout_details = "";
				var logout_zero = "";
				var login_zero = "";
				var lunchb = "";
				var firstb = "";
				var lastb = "";
				var total = 0;
				
				txt+= '<br> <div><span style="color:red">Note: </span> Please click here if you dont see your logs. <input type="button" class="btn btn-danger btn-xs" value="Show"  onclick="showUnknowLog()" style="margin-left:5px;margin-top: -5px;" ></div><br>';
				txt+= '<div id="TblUnknowLog"><table id="datatable-row-highlight" class="table table-striped table-bordered" cellspacing="0" width="100%">';
				txt+= '<thead>';
				txt+= '<tr>';
				txt+= ' <th>Date</th>';
				txt+= '  <th>Login</th>';
				txt+= '  <th>First Break</th>';
				txt+= ' <th>Lunch</th>';
				txt+= '  <th>Last Break</th>';
				txt+= '  <th>Logout</th>';
				txt+= '</tr>';
				txt+= '</thead>';

				txt+= '<tfoot>';
				txt+= '<tr>';
				txt+= '  <th>Date</th>';
				txt+= '   <th>Login</th>';
				txt+= '   <th>First Break</th>';
				txt+= '  <th>Lunch</th>';
				txt+= '  <th>Last Break</th>';
				txt+= ' <th>Logout</th>';
				txt+= '</tr>';
				txt+= '</tfoot>';
				txt+= '<tbody>';

				$(obj.sched_date).each(function(key,value){
					$.each(value,function(k, v ){


						login_zero = ((v["login"]==0) ? "--" : v["login"]);
						logout_zero = ((v["logout"]==0) ? "<i>--</i>" : v["logout"]);
						lunchb = ((v["lunchb"]==null) ? "<i>--</i>" : " "+v["lunchb"]);
						firstb = ((v["firstb"]==null) ? "<i>--</i>" : " "+v["firstb"]);
						lastb = ((v["lastb"]==null) ? "<i>--</i>" : " "+v["lastb"]);
						txt+= '<tr>';
						txt+= '   <td style="color:red"> No Schedule Set </td>';
						txt+= '  <td> '+login_zero+'</td>';
						txt+= '   <td>'+firstb+'</td>';
						txt+= '  <td>'+lunchb+'</td>';
						txt+= '  <td>'+lastb+'</td>';
						txt+= ' <td>'+logout_zero+'</td>';
						txt+= '</tr>';
					});

				});	
				
				txt+= '</tbody>';
				txt+= '</table></div>';
				$("#container2").html(txt);

			}
			
		});

	};
	$(function(){
		$('#sidebar-menu').load('<?php echo base_url(); ?>/index.php/Templatedesign/sideBar')
		// $('#headerLeft').load('<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft')

		<?php if($dtr_log["entry"]=="I"){ ?>


			setInterval(function()
			{
				$('#time-elapsed').load('<?php echo base_url(); ?>/index.php/dtr/getTime2');
				if(window.console || window.console.firebug) {
		 // console.clear();
		}

	}, 1000);
			<?php }  ?>
			$("#showdetailsAdmin").click(function(){
				var datee = $("#daterangepicker-example").val();	
				var txt= "";

				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>/index.php/dtr/viewLogsDetails",
					data: {
						datee:datee
					},
					cache: false,
					beforeSend: function(){
						$(".loading-spinner").show();
						$("#container2").hide();
					},
					success: function(html)
					{
						$(".loading-spinner").hide();
						$("#container2").show();



						var obj = jQuery.parseJSON(html);
						var div = '';

						var login_details = "";
						var logout_details = "";
						var logout_zero = "";
						var login_zero = "";
						var lunchb = "";
						var firstb = "";
						var lastb = "";
						var total = 0;

						txt+= '<br> <div><span style="color:red">Note: </span> Please click here if you dont see your logs. <input type="button" class="btn btn-danger btn-xs" value="Show"  onclick="showUnknowLogAdmin()" style="margin-left:5px;margin-top: -5px;" ></div><br>';
						txt+= '<div id="TblUnknowLog"><table id="datatable-row-highlight" class="table table-striped table-bordered" cellspacing="0" width="100%">';
						txt+= '<thead>';
						txt+= '<tr>';
						txt+= ' <th>Date</th>';
						txt+= '  <th>Login</th>';
						txt+= '  <th>Logout</th>';
						txt+= '</tr>';
						txt+= '</thead>';

						txt+= '<tfoot>';
						txt+= '<tr>';
						txt+= '  <th>Date</th>';
						txt+= '   <th>Login</th>';
						txt+= ' <th>Logout</th>';
						txt+= '</tr>';
						txt+= '</tfoot>';
						txt+= '<tbody>';

						$(obj.sched_date).each(function(key,value){
							$.each(value,function(k, v ){


								login_zero = ((v["login"]==0) ? "--" : v["login"]);
								logout_zero = ((v["logout"]==0) ? "<i>--</i>" : v["logout"]);
								txt+= '<tr>';
								txt+= '   <td>'+k+'</td>';
								txt+= '  <td> '+login_zero+'</td>';
								txt+= ' <td>'+logout_zero+'</td>';
								txt+= '</tr>';
							});

						});	

						txt+= '</tbody>';
						txt+= '</table>';
						$("#container2").html(txt);

					}

				});
			});
			$("#showdetails").click(function(){
				var datee = $("#daterangepicker-example").val();	
				var txt= "";

				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>/index.php/dtr/viewLogsDetails",
					data: {
						datee:datee
					},
					cache: false,
					beforeSend: function(){
						$(".loading-spinner").show();
						$("#container2").hide();
					},
					success: function(html)
					{
						$(".loading-spinner").hide();
						$("#container2").show();



						var obj = jQuery.parseJSON(html);
						var div = '';

						var login_details = "";
						var logout_details = "";
						var logout_zero = "";
						var login_zero = "";
						var lunchb = "";
						var firstb = "";
						var lastb = "";
						var total = 0;

						txt+= '<br> <div><span style="color:red">Note: </span> Please click here if you dont see your logs. <input type="button" class="btn btn-danger btn-xs" value="Show"  onclick="showUnknowLog()" style="margin-left:5px;margin-top: -5px;" ></div><br>';
						txt+= '<div id="TblUnknowLog"><table id="datatable-row-highlight" class="table table-striped table-bordered" cellspacing="0" width="100%">';
						txt+= '<thead>';
						txt+= '<tr>';
						txt+= ' <th>Date</th>';
						txt+= '  <th>Login</th>';
						txt+= '  <th>First Break</th>';
						txt+= ' <th>Lunch</th>';
						txt+= '  <th>Last Break</th>';
						txt+= '  <th>Logout</th>';
						txt+= '</tr>';
						txt+= '</thead>';

						txt+= '<tfoot>';
						txt+= '<tr>';
						txt+= '  <th>Date</th>';
						txt+= '   <th>Login</th>';
						txt+= '   <th>First Break</th>';
						txt+= '  <th>Lunch</th>';
						txt+= '  <th>Last Break</th>';
						txt+= ' <th>Logout</th>';
						txt+= '</tr>';
						txt+= '</tfoot>';
						txt+= '<tbody>';

						$(obj.sched_date).each(function(key,value){
							$.each(value,function(k, v ){


								login_zero = ((v["login"]==0) ? "--" : v["login"]);
								logout_zero = ((v["logout"]==0) ? "<i>--</i>" : v["logout"]);
								lunchb = ((v["lunchb"]==null) ? "<i>--</i>" : ""+v["lunchb"]);
								firstb = ((v["firstb"]==null) ? "<i>--</i>" : ""+v["firstb"]);
								lastb = ((v["lastb"]==null) ? "<i>--</i>" : ""+v["lastb"]);
								txt+= '<tr>';
								txt+= '   <td>'+k+'</td>';
								txt+= '  <td> '+login_zero+'</td>';
								txt+= '   <td>'+firstb+'</td>';
								txt+= '  <td>'+lunchb+'</td>';
								txt+= '  <td>'+lastb+'</td>';
								txt+= ' <td>'+logout_zero+'</td>';
								txt+= '</tr>';
							});

						});	

						txt+= '</tbody>';
						txt+= '</table></div>';
						$("#container2").html(txt);

					}

				});
			});
			$("#show").click(function(){
				var datee = $("#daterangepicker-example").val();
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>/index.php/dtr/viewLogs",
					data: {
						datee:datee
					},
					cache: false,
					beforeSend: function(){
						$(".loading-spinner").show();
						$("#container2").hide();
					},
					success: function(html)
					{
						$(".loading-spinner").hide();
						$("#container2").show();

						var obj = jQuery.parseJSON(html);
						var div = '';

						var login_details = "";
						var logout_details = "";
						var logout_zero = "";
						var login_zero = "";
						var total = 0;
						$(obj.sched_date).each(function(key,value){

							$.each(value,function(k, v ){
								login_details = ((v["login_raw"]==0) ? getFormattedDate() : v["login_raw"]);
								logout_details = ((v["logout_raw"]==0) ? getFormattedDate() : v["logout_raw"]);
								login_zero = ((v["login"]==0) ? "..." : v["login"]);
								logout_zero = ((v["logout"]==0) ? "<i>...</i>" : v["logout"]);

					/* var config = "DD/MM/YYYY HH:mm:ss";
					var ms = moment( logout_details, config).diff(moment(login_details,config));
					var d = moment.duration(ms);
					var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss"); */
					var timeStart = new Date(login_details).getTime();
					var timeEnd = new Date(logout_details).getTime();
					var hourDiff = timeEnd - timeStart; //in ms
					var secDiff = hourDiff / 1000; //in s
					var minDiff = hourDiff / 60 / 1000; //in minutes
					var hDiff = hourDiff / 3600 / 1000; //in hours
					var humanReadable = {};
					humanReadable.hours = Math.floor(hDiff);
					humanReadable.minutes = minDiff - 60 * humanReadable.hours;
					var s= humanReadable["hours"]+" hr. & "+Math.floor(humanReadable["minutes"])+" min. ";
					var hhh = Math.floor(humanReadable["hours"]);
					

					total = (hhh/24)*100;
					div += "<div id='container' onclick='showDash("+v["sched_id"]+")' data-toggle='modal' data-target='#myModal'>";
					div +=  "<h4><b>"+v["sched"]+"</b></h4>";
					div +=  "<small style='color:"+v["style"]+"'>"+v["type"]+"</small>";
					div += "<br><br>";
					if(v["type"]=="Normal" || v["type"]=="WORD" ){
						div += "<div id='stage'>";
						div += "<section class='work'>";
						div += "<div class='ui-progress-bar ui-container' id='progress_bar'>";
						div += "<div class='ui-progress' style='width: "+total+"%;'>";
						div += "</div>";
						div += "</div>";
						div += "</section>";
						div += "</div>";
						div += "<table>";
						div += "<tr><td colspan=2>"+s+"</td></tr>";
						div += "<tr><td ><b>In:</b></td><td >"+login_zero+"</td></tr>";
						div += "<tr><td ><b>Out:</b></td><td >"+logout_zero+"</td></tr>";
						div += "</table>";
					}else if(v["type"]=="Rest Day"){
						div += "<img src='<?php echo base_url()."assets/images/icons/RD.png"; ?>' style='width: 60%;'>";
					}else if(v["type"]=="Leave"){
						div += "<img src='<?php echo base_url()."assets/images/icons/LEAVE.png"; ?>' style='width: 60%;'>";
					}
					div += "</div>";
						//alert();
						total = 0;
					});

						});		
						$("#container2").html(div);
					}
				});	
			});
			$("#btn-cin2").click(function(){
				var defaultSched = $("#defaultSched").text();
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>/index.php/dtr/login_wo_sched",
					data: {
						acc_time_id:defaultSched,
					},
					cache: false,
					success: function(html)
					{ 
						if(html==1){
							swal(
								'Opps.',
								'You have already login to this date. Please refresh your page',
								'warning'
								)
						}else{
							swal(
								'Clock in!',
								'Your have successfully logged in. Please remind your supervisor to set you a schedule today.',
								'success'
								)
							setTimeout(function(){ location.reload(); },2000);
						}
					}
				});	
			});

			$("#btn-cin").click(function(){
				var sched_id = $("#sched_id2").text();
				var acc_time_id = $("#acc_time_id2").text();
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>/index.php/dtr/login",
					data: {
						sched_id:sched_id,
						acc_time_id:acc_time_id,
					},
					cache: false,
					success: function(html)
					{
						// if(html==1){
						// 	swal(
						// 		'Opps.',
						// 		'You have already login to this date. Please wait until tomorrow to log back in.',
						// 		'warning'
						// 		)

						// }else{
						// 	swal(
						// 		'Clock in!',
						// 		'Your have successfully logged in.',
						// 		'success'
						// 		);

						// 	setTimeout(function(){ location.reload(); },2000);
						// }
						$('#15fivemodal').modal({
							backdrop: 'static',
							keyboard: true,
							escape:true, 
							show: true
						})
					}
				});	

			});

			$("#btn-cout").click(function(){
				var sched_id = $("#sched_id").text();
				var acc_time_id = $("#acc_time_id").text();
				var dtr_id = $("#dtr_id").text();
				swal({
					title: 'Are you sure?',
					text: 'You will be logged out in DTR.',
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Yes, Ill go home now!',
					cancelButtonText: 'No, not yet'
				}).then(function() {

					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>/index.php/dtr/logout",
						data: {
							sched_id:sched_id,
							acc_time_id:acc_time_id,
							dtr_id:dtr_id,
						},
						cache: false,
						success: function(html)
						{
							swal(
								'Clock out!',
								'Your have successfully logged out.',
								'success'
								)
							setTimeout(function(){ location.reload(); },2000);

						}
					});	

				}, function(dismiss) {
  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
  if (dismiss === 'cancel') {
  	swal(
  		'Cancelled',
  		'It means you are still working.. :)',
  		'error'
  		)
  }
})
			});
		});	
 function breakAdd(id,acc_time_id){
 	var sched_id = $("#sched_id").text();
 	var dtr_id = $("#dtr_id").text();
 	var break_type;

 	if(id==2){
 		break_type= "First Break";
 	}else if(id==3){
 		break_type= "Lunch Break";
 	}else{
 		break_type= "Last Break";
 	}
 	swal({
 		title: 'Do you want to take your <br>'+break_type+'?',
 		text: '',
 		type: 'warning',
 		showCancelButton: true,
 		confirmButtonText: 'Yes',
 		cancelButtonText: 'No'
 	}).then(function() {

 		$.ajax({
 			type: "POST",
 			url: "<?php echo base_url(); ?>/index.php/dtr/breaks",
 			data: {
 				sched_id:sched_id,
 				acc_time_id:acc_time_id,
 			},
 			cache: false,
 			success: function(html)
 			{
 				swal(
 					'Yehey, '+break_type+' na!!',
 					'Your have just logged out for '+break_type,
 					'success'
 					)
				//setTimeout(function(){ location.reload(); },2000);
				setTimeout(function(){ window.location.href="<?php echo base_url()."index.php/dtr/break_log"?>"; },2000);
			}
		});	

 	}, function(dismiss) {
  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
  if (dismiss === 'cancel') {
  	swal(
  		'Cancelled',
  		'It means you are still working.. :)',
  		'error'
  		)
  }
})	 
 }

 function getFormattedDate(){
 	var d = new Date();
 	d = d.getFullYear() + "-" + ('0' + (d.getMonth() + 1)).slice(-2) + "-" + ('0' + d.getDate()).slice(-2) + " " + ('0' + d.getHours()).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2) + ":" + ('0' + d.getSeconds()).slice(-2);
 	return d;
 }
</script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script>
	Highcharts.chart('container-side', {
		chart: {
			type: 'pie',
			options3d: {
				enabled: true,
				alpha: 45,
				beta: 0,
			},

		},
		title: {
			text: '<?php echo (!empty($schedOut["sched_date"])) ? date_format(date_create($schedOut["sched_date"]),"l F d, Y")."<br> (Current Shift)" : "No Activity detected" ; ?> '
		},
		tooltip: {
			pointFormat: '<span style="color:{point.color}">\u25CF</span> <b>{point.y} hour(s)</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				depth: 35,
				dataLabels: {
					enabled: true,
					format: '{point.name}'
				}
			}
		},
		series: [{
			type: 'pie',
			name: 'Browser share',
			data: [
			['Online', <?php echo ($dtr_log["entry"]=="O")? 0 : number_format($schedOut["diff"], 2); ?> ],
			['Offline', <?php echo ($dtr_log["entry"]=="O")? 12 : number_format((12-$schedOut["diff"]), 2); ?>],
			<?php if($fbs["break"]>0){ ?>['First Break', <?php echo number_format((($fbs["break"])), 2); ?>],<?php }?>
			<?php if($lunchs["break"]>0){ ?>['Lunch Break', <?php echo number_format($lunchs["break"], 2); ?>],<?php }?>
			<?php if($lbs["break"]>0){ ?>['Last Break', <?php echo number_format($lbs["break"], 2); ?>],<?php }?>
			]
		}]
	});
</script>
<script type="text/javascript">
	$('#15fivemodal').on('hide.bs.modal', function(e) {
		$(this).removeData("modal").modal({backdrop: 'static', keyboard: false});
	});
</script>
<script type="text/javascript">
	$(".emoji").hover( function () {		
		var src = $(this).attr('src');
		var newsrc = src.replace('static.png', '.gif');
		$(this).attr('src',newsrc);
	}, function() {
		var src = $(this).attr('src');
		var newsrc = src.replace('.gif', 'static.png');
		// alert($(this).attr('data-active'));
		if($(this).attr('data-active')=='true'){
			// alert('selected');
		}else{
			
			$(this).attr('src', newsrc);
		}
		// $(this).attr('src', newsrc);
	});
</script>
<script type="text/javascript">
	function changequestion(sequence){
		var colors = ["#ff6161", "#56b1bf", "#ff7f47","#ec7498","#ffd85d"]; 
		$("#15fivequestiondiv").css({'background-color':colors[sequence-1],'background-image':'url(../assets/images/emoji/diagmonds.png)'})
		$("#15fivemodalheader").css("background","linear-gradient("+colors[sequence-1]+",white)")
		var currentsrc = $("#15fivebtn"+sequence).children('img').attr('src');  
		$("button[id*='15fivebtn'] img").each(function(){
			var src = $(this).attr('src');
			var newsrc = src.replace('.gif', 'static.png');
			$(this).attr('src', newsrc);
			$(this).css('filter','grayscale(90%)');
		});
		for(var x=1;x<=5;x++){
			$("#15fivesq"+x).css('background','#888');
		}
		$("#15fivesq"+sequence).css('background',colors[sequence-1]);
		$("button[id*='15fivebtn'] img").attr('data-active','false');
		$("#15fivebtn"+sequence).children('img').attr('data-active','true');
		$("#15fivebtn"+sequence).children('img').attr('src',currentsrc);
		$("#15fivebtn"+sequence).children('img').css('filter','');
		$("button[id*='15fivebtn']").css('transform','');
		$("#15fivebtn"+sequence).css('transform', "scale(1.5)"); 
		$("#15fivequestiondiv").show(1000);
		$("#15fivequestion").hide();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>index.php/Survey/getSequenceQuestion",
			data: {
				sequence:sequence
			},
			cache: false,
			success: function(res)
			{
				res = res.trim();
				$("#15fivequestion").html(res).fadeIn();
			}
		});

	}
</script>
<script type="text/javascript">
	$('#15fivemodal').on('show.bs.modal', function() {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>index.php/Survey/getHeaderQuestion",
			cache: false,
			success: function(res)
			{
				res = res.trim();
				$("#15fivemodalheadertext").html(res);
			}
		});
	})
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>