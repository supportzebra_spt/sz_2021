<!DOCTYPE html> 

<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  .two-line-ellipsis{
    display: inline-block;
    border: 0px;
    height: 30.5px;
    overflow: hidden;
    margin-bottom:6px;
    color:#777;
  } 
  .one-line{
    line-height: 8px !important;
    height: 16px !important;
    overflow: hidden !important;
    white-space: nowrap !important;
    text-overflow: ellipsis !important;
    /*width: 80%;*/
    text-align:center !important;
  }
  a:hover,a:visited,a:active{
    text-decoration:none !important;
  }
  .style_prevu_kit:hover
  {
    box-shadow: 5px 5px 20px #888;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.1);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
  }
  ul,li{ 
    list-style:none;
    padding-left:0;
  }
  body { padding-right: 0 !important }

</style>

<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





<script type="text/javascript">
  $(window).load(function(){
   setTimeout(function() {
    $('#loading').fadeOut( 400, "linear" );
  }, 300);
 });
</script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/xeditable/xeditable.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable-demo.js"></script> -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/popover/popover.js"></script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->


<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>

<!-- TABLES -->


<script type="text/javascript" src="<?php echo base_url();?>assets/js/list.min.js"></script>
</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>



    </div>
    <div id="page-sidebar">
     <div class="scroll-sidebar">


      <ul id="sidebar-menu">
      </ul>
    </div>
  </div>
  <div id="page-content-wrapper">
    <div id="page-content">
      <h2>APPLICANT LIST</h2>
      <hr>
      <div class="container-fluid" id="test-list">
       <div class="row">
         <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
          <div class="input-group">
            <span class="input-group-addon"><i class='glyph-icon icon-search'></i></span>
            <input type="text" id="myInput" placeholder="Search Name..." class='form-control search'>
          </div>
          
        </div>
      </div>
      <ul class="row list">
        <?php foreach($data as $key => $val){?>

        <li class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center style_prevu_kit" style='background:#fff;padding:0 !important;width:23%;margin:1%;border:1px solid #F3F3F4' data-fullname='<?php echo $val->lname.", ".$val->fname .' '.$val->pos_details .' '.$val->status; ?>'>
          <br>
          <a href="#" style="display: block;" data-apid='<?php echo $val->apid; ?>' data-target="#preview"  data-toggle="modal">
            <div style='padding:1%'>
              <img src="<?php echo base_url()."assets/images/".strtolower($val->pic); ?>"  alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';"  height="75"  class='img-circle' style='border:2px solid #ddd' />
              <br><br>
              <h5 class="name one-line" style='margin-bottom:8px;font-size:14px;color:#444;padding-top:2px'><b><?php echo $val->lname.", ".$val->fname; ?></b></h5>
              <p  style='font-size:11px;color:#444;margin-bottom:6px' class='one-line'><?php echo $val->pos_details; ?></p>
              <p style='font-size:12px;white-space: nowrap;color:#444'><b>Support Zebra</b></p>
              <p style='font-size:11px;' class='two-line-ellipsis'>&nbsp;<?php if($val->presentAddrezs==''){echo '<span class="text-danger">No Address Set</span>';}else{$add = explode('|',$val->presentAddrezs);echo $add[0];};?></p>
              <p style='font-size:11px;white-space: nowrap;color:#444'><span class='icon-typicons-phone-outline'></span>
                <?php if($val->cell==''){echo '<span class="text-danger">No Contact Set</span>';}else{echo $val->cell;};?></p>
                <p style='font-size:11px;white-space: nowrap;color:#444'><span class='icon-typicons-users-outline'></span>
                  <?php if($val->gender==''){echo '<span class="text-danger">No Gender Set</span>';}else{echo $val->gender;};?></p><br>
                </div>

              </a>
            </li>
            <?php } ?>
          </ul>
          <div class="nav row text-center">
            <div class="col-md-3">
             <ul class="pager">
              <li><a href="#" id='btn-first'>First</a></li>
              <li><a href="#" id='btn-prev'>Previous</a></li>
            </ul>
          </div>
          <div class="col-md-6">
            <ul class="pagination" style='margin:0 !important'></ul>
          </div>
          <div class="col-md-3">
            <ul class="pager">
              <li><a href="#" id='btn-next'>Next</a></li>
              <li><a href="#" id='btn-last'>Last</a></li>
            </ul>
          </div>

        </div>
      </div>

      <div class="row" hidden>
        <div class="col-md-8">
          <div class="panel">
            <div class="panel-body">
              <h3 class="title-hero">
                Recent sales activity
              </h3>
              <div class="example-box-wrapper">
                <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
          </div>




          <div class="content-box">
            <h3 class="content-box-header bg-default">
              <i class="glyph-icon icon-cog"></i>
              Live server status
              <span class="header-buttons-separator">
                <a href="#" class="icon-separator">
                  <i class="glyph-icon icon-question"></i>
                </a>
                <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                  <i class="glyph-icon icon-refresh"></i>
                </a>
                <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                  <i class="glyph-icon icon-times"></i>
                </a>
              </span>
            </h3>
            <div class="content-box-wrapper">
              <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
</div>

<div id="preview" class="modal fade" role="dialog">
  <div class="modal-dialog" style='width:60%'>

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header font-bold" style='background:#333;color:white;'>
        <button type="button" class='close' style='margin-right:10px;color:white' data-dismiss="modal">&times;</button>
        <h3 class="modal-title">APPLICANT DETAILS</h3>
      </div>
      <div class="modal-body" style='padding:20px'>
       <div class="row">
        <div class="col-md-4">
          <img id="previewpic" alt="Image not found" class='img-thumbnail' onerror="this.src='<?php echo base_url('assets/images/sz.png'); ?>';" style='width:100%;background:white' />
          <br><br>
          <div class="panel">
            <div class="panel-body text-center">
              <p class='font-bold font-black font-size-17 text-center' id='previewname' style='margin-bottom:4px'></p>
              <p>Cellphone Number: <span class='font-bold font-size-12' id='previewcell'></span></p>
              <p>Alternative Number: <span class='font-bold font-size-12' id='previewtel'></span></p>

            </div> 
            <div class="panel-footer text-center"  style='background:#444;color:white;'>
              <a class="btn btn-sm btn-info" id='update'>
                <span class="glyph-icon icon-separator">
                  <i class="glyph-icon icon-edit"></i>
                </span>
                <span class="button-content">
                  Update
                </span>
              </a>
              <a class="btn btn-sm btn-primary" id='evaluate'>
                <span class="glyph-icon icon-separator">
                  <i class="glyph-icon icon-list-alt"></i>
                </span>
                <span class="button-content">
                  Evaluate
                </span>
              </a>
            </div>
          </div>


        </div>
        <div class="col-md-8" id='applicantdetails'>
          <div class="panel">
            <div class="panel-heading"  style='background:#444;color:white;'>
              <h5>PERSONAL DETAILS</h5>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6"><p>Status: <span class='font-bold font-size-12' id='previewstatus'></span></p></div>
                <div class="col-md-6"><p>Gender: <span class='font-bold font-size-12' id='previewgender'></span></p></div>
              </div>
              <div class="row">
                <div class="col-md-6"><p>Birthday: <span class='font-bold font-size-12' id='previewbday'></span></p></div>
                <div class="col-md-6"><p>Religion: <span class='font-bold font-size-12' id='previewreligion'></span></p></div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <p>City: <span class='font-bold font-size-12' id='previewcity'></span></p>
                </div>
                <div class="col-md-6">
                  <p>Province: <span class='font-bold font-size-12' id='previewprovince'></span></p>

                </div>
              </div>
              <div class="row">
                <div class="col-md-12" style='padding-top:3px'>
                  <!-- <p>ZipCode: <span class='font-bold font-size-12' id='previewzip'></span></p> -->
                  <p>Complete Address: <br><span class='font-bold font-size-12' id='previewpresent'></span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="panel">
            <div class="panel-heading"  style='background:#444;color:white;'>
              <h5>EMPLOYMENT DETAILS</h5>
            </div>
            <div class="panel-body">
             <p>Position Applied: <br><span class='font-bold font-size-14' id='previewposition'></span></p>
             <p>CCA Experience: <span class='font-bold font-size-12' id='previewcca'></span></p>
             <p>Source: <span class='font-bold font-size-12' id='previewsource'></span></p>
           </div>
         </div>

       </div>
       <div class="col-md-8" id='evaluation' style='display:none'>
         <div class="panel">
           <div class="panel-heading" style='background:#444;color:white;'>
            <button type="button" class='close font-white' id='openappdetails'>&#8629;</button>
            <h5>EVALUATION</h5>
          </div>
          <div class="panel-body">
            <p class='text-center'>
              THIS SECTION IS INTENDED EVALUATION PURPOSES.<br> CURRENTLY IN PRODUCTION.
            </p>
          </div>
          <div class="panel-footer text-center">
            <div class='pad10A'>
              <button class="btn btn-sm btn-success" id='pass'>
                <span class="glyph-icon icon-separator">
                  <i class="glyph-icon icon-thumbs-up"></i>
                </span>
                <span class="button-content">
                  Pass
                </span>
              </button>
              <button class="btn btn-sm btn-danger" id='fail'>
                <span class="glyph-icon icon-separator">
                  <i class="glyph-icon icon-thumbs-down"></i>
                </span>
                <span class="button-content">
                  Fail
                </span>
              </button>
            </div>
            <br>
            <div class="alert alert-warning text-center" id='inf_pool' style='display:none'>
             <h5><b style='color:red'>NOTE:</b> Please update applied position to continue. Currently <b>POOL</b>.</h5>
           </div>
           <div class="alert alert-warning text-center" id='inf_year' style='display:none;'>
            <h5><b style='color:red'>NOTE:</b> Please indicate years of Call Center Experience.<br> This is required to all <b>Customer Care Ambassador</b> applicants.</h5>
          </div>
          <div id='passdiv' style='display:none;'>
            <div class="content-box" style='padding:0;margin:0'>
              <h3 class="content-box-header bg-gray">
                <i class="glyph-icon icon-cog"></i>
                Employment Details Form
              </h3>
              <div class="content-box-wrapper" style='padding-bottom:0'>
                <div class="form-group">
                  <label class="control-label" >Employment Position:</label><br>
                  <a href="#" id="position"></a>
                </div>
              </div> 

              <div class="content-box-wrapper" style='padding-bottom:0'>
                <div class="form-group">
                  <label class="control-label">Effectivity Date:</label><br>
                  <a href="#" id="effec_date"></a>
                </div>
              </div> 
              <input type="hidden" name="" id='positionvalue'>
              <input type="hidden" name="" id='effecdatevalue'>
              <div class="button-pane text-right  bg-gray">
                <a class="btn btn-sm btn-info" id='createrecord'>
                  <span class="glyph-icon icon-separator">
                    <i class="glyph-icon icon-plus"></i>
                  </span>
                  <span class="button-content">
                    Employ
                  </span>
                </a>
                <a class="btn btn-sm" data-dismiss='modal'>
                  <span class="glyph-icon icon-separator">
                    <i class="glyph-icon icon-times"></i>
                  </span>
                  <span class="button-content">
                    Cancel
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Templatedesign/headerLeft",
    cache: false,
    success: function(html)
    {

      $('#headerLeft').html(html);
		   //alert(html);
     }
   });	
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Templatedesign/sideBar",
    cache: false,
    success: function(html)
    {

      $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
 });	
</script>

<script type="text/javascript">
  $('#preview').on('show.bs.modal', function (e) {
    if (e.namespace === 'bs.modal') {
      var apid = $(e.relatedTarget).data('apid');
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Applicant/getApplicantPreview",
        data: {
          apid:apid
        },
        cache: false,
        success: function(result)
        {
          result = result.trim();
          result = JSON.parse(result);
          console.log(result);
          $("#update").attr('href','<?php echo base_url(''); ?>index.php/Applicant/applicantUpdate/'+apid);
          $("#previewname").html(result.lname+", "+result.fname);
          $("#previewposition").html(result.pos_details);
          $("#previewpic").attr('src',"<?php echo base_url(); ?>assets/images/"+result.pic);

          $("#previewbday").html(result.birthday+" ("+result.age+" yrs old)");
          $("#previewstatus").html(result.civilStatus);
          $("#previewgender").html(result.gender);
          $("#previewreligion").html(result.relijon);
          var address = result.presentAddrezs.split('|');
          $("#previewpresent").html(address[0]);
          $("#previewprovince").html(address[1]);
          $("#previewcity").html(address[2]);
        // $("#previewzip").html(address[3]);
        $("#previewcca").html(result.ccaExp);
        $("#previewsource").html(result.Source);
        $("#previewtel").html(result.tel);
        $("#previewcell").html(result.cell);
        $("#pass").data('apid',apid);
        $("#pass").data('cc_yrs',result.cc_yrs);
        $("#pass").data('cc_yrs',result.ccaExp);
        $("#pass").data('class',result.class);
        $("#pass").data('pos',result.pos_details);
        $("#pass").data('pos_id',result.pos_id);
        $("#fail").data('apid',apid);
        $("#createrecord").data('apid',apid);
        $("#effec_date").editable({type:"date", pk:1,name:"effec_date",title:"Enter Effectivity Date",mode:"inline",combodate:{minYear:moment().format('YYYY'),maxYear:moment().add(2,'years').format('YYYY')}});
      }
    });
    }

  })
</script>

<script type="text/javascript">
  var monkeyList = new List('test-list', {
    valueNames: ['name'],
    page: 12,   
    pagination: true

  });
  // $('.nav').append('<div class="btn-next"> <i class="glyph-icon icon-chevron-right"></i> </div><div class="btn-last"> <i class="glyph-icon icon-forward"></i> </div>');
  // $('.nav').prepend('<div class="btn-first"> <i class="glyph-icon icon-rewind"></i> </div><div class="btn-prev"> <i class="glyph-icon icon-chevron-left"></i> </div>');

  $('#btn-next').on('click', function(){
    $('.pagination .active').next().trigger('click');
  })
  $('#btn-prev').on('click', function(){
    $('.pagination .active').prev().trigger('click');
  })
  $('#btn-first').on('click', function(){
    monkeyList.show(1,12)
  })
  $('#btn-last').on('click', function(){
    if(monkeyList.searched==true){
      var arr = Object.keys(monkeyList.matchingItems).length;
      var remainder = arr % 12;
      if(remainder==0){
        monkeyList.show(arr-11,12);
      }else{
        monkeyList.show((arr-remainder)+1,12);
      } 
    }else{
      var remainder = <?php echo count($data);?> % 12;
      if(remainder==0){
        monkeyList.show(monkeyList.size()-11,12);
      }else{
        monkeyList.show((monkeyList.size()-remainder)+1,12);
      }
    }

  })

</script>
<script type="text/javascript">
  $("#evaluate").click(function(){
    $("#applicantdetails").hide();
    $("#evaluation").show(500);
  });
  $("#openappdetails").click(function(){
    $("#applicantdetails").show(500);
    $("#evaluation").hide();
  })
</script>
<script type="text/javascript">
  $('#preview').on('hide.bs.modal', function () {
    $("#applicantdetails").show();
    $("#evaluation").hide();
    $("#inf_pool").hide();
    $("#inf_year").hide();
    $("#passdiv").hide();
  });
</script>

<script type="text/javascript">
  $("#pass").click(function(){
    var apid = $(this).data('apid');
    var cc_yrs = $(this).data('cc_yrs');
    var ccaExp = $(this).data('ccaExp');
    var aclass = $(this).data('class');
    var pos = $(this).data('pos');
    var pos_id = $(this).data('pos_id');
    if(cc_yrs==null && aclass=='Agent' && ccaExp=='Yes'){
      $("#inf_year").show();
      alert(cc_yrs +" - "+aclass);
    }else if(pos=='Pool'){
     $("#inf_pool").show();
   }else{
     $("#inf_pool").hide();
     $("#inf_year").hide();
     if(aclass!='Agent'){
       $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Applicant/getDepartmentPosition",
        data:{
          pos_id:pos_id
        },
        cache: false,
        success: function(res)
        {
         res = res.trim();
         // alert(res);
         // res = JSON.parse(res);
         $("#position").editable({
          mode:"inline",
          name:"position",
          type:"select",
          source: res
        }); 
         $('#position').editable('option', 'source', res);         
       }
     });
     }else{
       $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Applicant/getCCAPosition",
        cache: false,
        success: function(res)
        {
         res = res.trim();
         // res = JSON.parse(res);
         $("#position").editable({
          mode:"inline",
          name:"position",
          type:"select",
          source: res
        });  
         $('#position').editable('option', 'source', res);          
       }
     });
     }
     $("#passdiv").show(1000);
   }

 });
  $("#fail").click(function(){
    $("#passdiv").hide(500);
    var apid = $(this).data('apid');
    swal({
      title: 'Applicant Evaluation',
      type: 'error',
      html:
      'Are you sure to <b>FAIL</b> this applicant?',
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText:
      '<i class="glyph-icon icon-check"></i> Confirm',
      cancelButtonText:
      '<i class="glyph-icon icon-times"></i>'
    }).then(function () {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Applicant/failApplicant",
        data:{
          apid:apid
        },
        cache: false,
        success: function(res)
        {
          if(res.trim()=='Success'){
           swal(
            'Applicant Has Failed!',
            'Applicant Evaluation have been approved and confirmed.',
            'warning'
            )
         }else{
          swal(
            'Error Occured!',
            'Please try again later.',
            'error'
            )
        }
        window.setTimeout(function(){  
          location.reload();
        } ,2000);
      }
    });

    })
  });
</script>
<script type="text/javascript">
  $("#createrecord").click(function(){
    var apid = $(this).data('apid');
    var pos = $("#positionvalue").val();
    var date = $("#effecdatevalue").val();
    if(pos==''||pos==null){
      $.jGrowl( "Please choose employment position",{sticky:!1,position:"top-right",theme:"bg-orange"});
    }else if(date==''||date==null){
      $.jGrowl( "Please choose effectivity date",{sticky:!1,position:"top-right",theme:"bg-orange"});
    } else{
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Applicant/createemployeerecord",
        data:{
          apid:apid,
          posempstat_id:pos,
          effectivedate:date
        },
        cache: false,
        success: function(res)
        {
          if(res.trim()=='Success'){
           swal(
            'Applicant is Employed!',
            'Applicant Evaluation have been approved and confirmed.',
            'success'
            );
         }else{
          swal(
            'Error Occured!',
            'Please try again later.',
            'error'
            );
        }
        window.setTimeout(function(){  
          location.reload();
        } ,2000);
      }
    });
    }
  });
</script>
<script type="text/javascript">
  $("#position").on('save',function(e,params){$("#positionvalue").val(params.newValue)});
  $("#effec_date").on('save',function(e,params){$("#effecdatevalue").val(moment(params.newValue).format('YYYY-MM-DD'));});
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT --> 
</html>