<!DOCTYPE html> 
<html  lang="en">

<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> SUPPORTZEBRA - PAYROLL CONFIGURATION </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>


</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="<?php echo $setting['settings']; ?>">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
	<div class="form-group">
              
<div class="panel">
    <div class="panel-body">
        		 <div style="padding : 7px;background: #e1e1e1;"><img src="<?php echo base_url()."assets/images/GovLogo/flag.jpg"?>" style="width:2%"> <b> Government</b></div>

        <div class="example-box-wrapper">
		
            <div class="row">
                <div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
                       
                        <div class="tile-content-wrapper">
                            <img src="<?php echo base_url()."assets/images/GovLogo/SSS.png"?>" style="width:90%">
                        </div>
                        <a href="<?php echo base_url()."index.php/Payroll/SSS"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="SSS details">
                            view details
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
                        
                        <div class="tile-content-wrapper"  >
							<img src="<?php echo base_url()."assets/images/GovLogo/PhilHealth.png"?>" style="width:70%">
                        </div>
                        <a href="<?php echo base_url()."index.php/Payroll/Philhealth"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="Philhealth details">
                            view details
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>
                   <div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
                       
                        <div class="tile-content-wrapper">
                            <img src="<?php echo base_url()."assets/images/GovLogo/Pagibig.jpg"?>" style="width:70%">
                        </div>
                        <a href="<?php echo base_url()."index.php/Payroll/Pagibig"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="HDMF details">
                            view details
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>
				<div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
                       
                        <div class="tile-content-wrapper">
                            <img src="<?php echo base_url()."assets/images/GovLogo/BIR.png"?>" style="width:75%">
                        </div>
                        <a href="<?php echo base_url()."index.php/Payroll/TAX"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="BIR details">
                            view details
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
           
        </div>
		 <div style="padding : 7px;background: #e1e1e1;"><img src="<?php echo base_url()."assets/images/GovLogo/gear.png"?>" style="width:2%"><b>Settings</b></div>
        <div class="example-box-wrapper">
		
            <div class="row">
                <div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
                       
                        <div class="tile-content-wrapper">
                            <img src="<?php echo base_url()."assets/images/GovLogo/holiday.jpg"?>" style="width:60%">
                        </div>
                        <a href="<?php echo base_url()."index.php/Payroll/holiday"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="Set Holiday">
                            <b>Holidays</b>
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
                        
                        <div class="tile-content-wrapper"  >
							<img src="<?php echo base_url()."assets/images/GovLogo/income-tax.png"?>" style="width:36%">
                        </div>
                        <a href="<?php echo base_url()."index.php/Payroll/employeetaxtype"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="Assign Tax Type to Employees">
                            <b>Tax Type</b> (<i>per employee</i>)
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>
                   <div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
                       
                        <div class="tile-content-wrapper">
                            <img src="<?php echo base_url()."assets/images/GovLogo/adjustplus.jpg"?>" style="width:80%">
                        </div>
                        <a href="<?php echo base_url()."index.php/Payroll/additions"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="Set Additional Adjustment">
                            <b>Manual Additional</b> (<i> per payroll </i>)
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>
				<div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
                       
                        <div class="tile-content-wrapper">
                            <img src="<?php echo base_url()."assets/images/GovLogo/adjustminus.jpg"?>" style="width:80%">
                        </div>
                        <a href="<?php echo base_url()."index.php/Payroll/deductions"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="Set Deduction Adjustment">
                           <b>Manual Deductions</b> (<i> per payroll </i>)
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
                       
                        <div class="tile-content-wrapper">
                            <img src="<?php echo base_url()."assets/images/GovLogo/rate.jpg"?>" style="width:30%">
                        </div>
                        <a href="<?php echo base_url()."index.php/PositionRates"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="Set Rate to Position">
                            <b>Rates</b> (<i>per position</i>)
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>

				<div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
						<a href="<?php echo base_url()."index.php/EmployeeAdjustment"; ?>" >

                        <div class="tile-content-wrapper">
                            <img src="<?php echo base_url()."assets/images/GovLogo/adjustEmp.jpg"?>" style="width:45%">
                        </div>
						</a>
                        <a href="<?php echo base_url()."index.php/EmployeeAdjustment"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="Set Adjustments to Employee">
                           <b> Adjustments </b>(<i>Loan/Vaccine/etc.</i>)
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>
				<div class="col-md-3">
                    <div class="tile-box tile-box-alt bg-white content-box">
						<a href="<?php echo base_url()."index.php/PositionRates/positionbonus"; ?>" >
							<div class="tile-content-wrapper">
								<img src="<?php echo base_url()."assets/images/GovLogo/bonus.jpg"?>" style="width:35%">
							</div>
							</a>
						<a href="<?php echo base_url()."index.php/PositionRates/positionbonus"; ?>" class="tile-footer tooltip-button" data-placement="bottom" title="Set Adjustments to Employee">
                            <b>Employees' Bonus</b>
                            <i class="glyph-icon icon-arrow-right"></i>
                        </a>
                    </div>
                </div>

            </div>
           
        </div>
    </div>
</div>

	
	</div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
	    $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
  });	
</script>
 

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>