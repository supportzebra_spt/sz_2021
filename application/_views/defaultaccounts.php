<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  </style>

  
  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
     $('#accClass').val('').trigger('change');
   });

 </script>
 
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
</head>

<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div id="page-title">
          <h3>Account Default Schedule</h3>
          <hr>
          <div class="row">
            <div class="col-md-3">
              <select id='accClass' class='form-control'>
                <option value='' selected>All</option>
                <option value='Admin'>Admin</option>
                <option value='Agent'>Agent</option>
              </select>
            </div>
          </div>
          <br>
          <div class="example-box-wrapper">
            <div class="row">
              <div class="col-md-12">
                <div class="content-box">
                  <h3 class="content-box-header bg-primary">
                   List of Accounts
                 </h3>
                 <div class="content-box-wrapper">
                   <table class="table table-striped table-condensed table-bordered">
                     <thead>
                       <tr>
                         <th>Account Name</th>
                         <th>Class</th>
                         <th>Default Schedule</th>
                       </tr>
                     </thead>
                     <tbody id='tbody'></tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div> 
     </div>
   </div>
 </div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">

  $("#accClass").on('change',function(){
    var acc_description = $(this).val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/getAccountsSched",
      data:{
        acc_description:acc_description
      },
      cache: false,
      success: function(res)
      {
        console.log(res);
        var result = JSON.parse(res);
        $("#tbody").html("");
        $.each(result,function (i,elem){
          var tbody = "";
          tbody += "<tr>";
          tbody += "<td>"+elem.acc_name+"</td>";
          tbody += "<td>"+elem.acc_description+"</td>";          
          tbody += "<td><select class='form-control' onchange='changeshift(this.value)'>";
          tbody += "<option value=''>--</<option>";
          $.each(elem.shifts,function (i2,elem2){
            if(elem2.isDefault==1){

              tbody += "<option value='"+elem2.acc_time_id+"' selected>"+elem2.time_start+" - "+elem2.time_end+"</<option>";
            }else{

              tbody += "<option value='"+elem2.acc_time_id+"'>"+elem2.time_start+" - "+elem2.time_end+"</<option>";
            }
          });
          tbody += "</select></td>";
          tbody += "</tr>";
          $("#tbody").append(tbody);
        });
      }
    });
  });
</script>
<script type="text/javascript">
 function changeshift(acc_time_id){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Schedule/changeaccountdefault",
    data: {
      acc_time_id:acc_time_id
    },
    cache: false,
    success: function(res)
    {
      var result = $.trim(res);
      if(result=='Success'){

        $.jGrowl("Successfully Changed Default Schedule.",{sticky:!1,position:"top-right",theme:"bg-green"});
      }else{

        $.jGrowl("Problem Processing.",{sticky:!1,position:"top-right",theme:"bg-red"});
      }
    }
  });
 }
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>