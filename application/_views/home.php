<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

	<style>
		/* Loading Spinner */
		.spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		.applicantDiv {
			height:175px;
			overflow:hidden
		}
		#content-box {
			border-bottom-left-radius: 20px;
			border-bottom-right-radius: 20px;
		}
		#appDown {
			background: #c391df;
			padding: 5px;
			border-bottom-right-radius: 20px;
			border-bottom-left-radius: 20px;
			color:white;
		}
		#dateDown {
			background: #49bac7;
			padding: 5px;
			border-bottom-right-radius: 20px;
			border-bottom-left-radius: 20px;
			color:white;
		}

	</style>


	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Favicons -->

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

	<!-- JS Core -->

	<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





	<script type="text/javascript">
		$(window).load(function(){
			setTimeout(function() {
				$('#loading').fadeOut( 400, "linear" );
			}, 300);
		});
	</script>

	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
	<script type="text/javascript">
		/* Datepicker bootstrap */

		$(function() { "use strict";
			$('.bootstrap-datepicker').bsdatepicker({
				format: 'mm-dd-yyyy'
			});
		});

	</script>
	
	<!-- jQueryUI Datepicker -->

	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

	<!-- Bootstrap Daterangepicker -->

	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>

</head>


<body>
	<div id="sb-site">

1222
		<div id="loading">
			<div class="spinner">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>

		<div id="page-wrapper">
			<div id="page-header" class="<?php echo $setting['settings']; ?>">
				<div id="mobile-navigation">
					<button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
					<a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
				</div>
				<div id="header-logo" class="logo-bg">
					<img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
					<a id="close-sidebar" href="#" title="Close sidebar">
						<i class="glyph-icon icon-angle-left"></i>
					</a>
				</div>
				<div id='headerLeft'>
				</div>



			</div>
			<div id="page-sidebar">
				<div class="scroll-sidebar">


					<ul id="sidebar-menu">
					</ul>
				</div>
			</div>
			<div id="page-content-wrapper">
				<div id="page-content">

					<div class="container">


						<div class="panel">
							<div class="panel-body">
								<div class="col-md-12">
									<div id="container"></div> 
								</div>
							</div> 
						</div>
						<div class="panel">
							<div class="panel-body">
								<div class="col-md-6">
									<div id="chartx1"></div>
								</div>
								<div class="col-md-6">
									<div id="chartx2"></div>
								</div>
							</div>
						</div> 

						<div class="panel">
							<div class="panel-body">
								<div class="col-md-6">
									<div id="charter5"></div>
								</div>
								<div class="col-md-6">
									<div id="charter6"></div>
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-body">
								<div class="col-md-6">
									<div id="charter7"></div>
								</div>
							</div>
						</div>
                  <!-- <div class="col-md-4">
                    <div class="content-box">
                      <h3 class="content-box-header bg-blue" style="background-color: #49BaC7;">
                       <?php echo  date("l - F j, Y ").""; ?>
                     </h3> 
                     <div class="content-box-wrapper" style="height:175px;overflow:hidden">
                       <?php if(!empty($Bdate)){?>
                       <table style="width: 100%;">
                        <?php foreach($Bdate as $key => $val){
                          echo "<tr>
                          <td><img class='img-circle' width='40' src='".base_url()."assets/images/".$val['pic']."'></td><td>".$val['fname']." ".$val['lname']."</td><td> <img src='".base_url()."assets/images/gift.ico' width=20> Birthday</td>
                        </tr>";

                      }


                      ?>
                    </table>
                    <?php }else{  echo "<center><h2>NONE</h2></center>"; }?>
                  </div>
                  <div id="dateDown" style="text-decoration:none;color:white;">
                    <center> View more..  </center>
                  </div>
                </div>

              </div>
              <div class="col-md-4">
                <div class="content-box" id="content-box">
                  <h3 class="content-box-header bg-red" style="background-color: #c391df;">
                    Applicant
                  </h3>
                  <div class="content-box-wrapper applicantDiv" >
                    <table style="width: 100%;">
                      <?php
                      foreach($applicant as $key => $val){
                       echo "<tr><td><img class='img-circle' width='40' src='".base_url()."assets/images/".$val['pic']."'></td><td>".$val['fname']." ".$val['lname']."</td><td>".$val['pos_name']."</td></tr>";

                     }


                     ?>
                   </table>
                 </div>
                 <div id="appDown" style="text-decoration:none;color:white;">
                  <center> <a href="<?php echo base_url()."index.php/applicant"; ?>" style="text-decoration:none;color:white;"><?php echo (count($applicant) >=6) ?(count($applicant)-4)."  More.." : " "; ?></a> </center>
                </div>

              </div>

            </div>
            <div class="col-md-4">
              <div class="content-box">
                <h3 class="content-box-header bg-blue-alt" style="background-color: #49C7B1;">
                  Soon..
                </h3>
                <div class="content-box-wrapper" style="height:175px;overflow:hidden">

                </div>
              </div>

          </div> -->





          <script src="https://code.highcharts.com/highcharts.js"></script>
          <script src="https://code.highcharts.com/highcharts-3d.js"></script>
          <script src="https://code.highcharts.com/highcharts-more.js"></script>
          <script src="https://code.highcharts.com/modules/exporting.js"></script>
          <script src="https://code.highcharts.com/modules/drilldown.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/themes/dark-unica.js"></script>
          <?php
          $admin ="";
          foreach($accAdmin as $key => $val){
          	$admin += $val['cntAdmin'];
          }
          ?> 
          <?php
          $agent ="";
          foreach($accAgent as $key => $val){
          	$agent += $val['cntAgent'];
          }
          ?> 
          <script type="text/javascript">
          	$(function () {Highcharts.chart('container', {
          		chart: {
          			type: 'pie'
          		},
          		title: {
          			text: 'SupportZebra Employees'
          		},
          		subtitle: {
          			text: ' '
          		},
          		plotOptions: {
          			series: {
          				dataLabels: {
          					enabled: true,
          					format: '{point.name}: {point.y:.f}'
          				}
          			}
          		},

          		tooltip: {
          			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'
          		},
          		series: [{
          			name: 'Employees',
          			colorByPoint: true,
          			data: [{
          				name: 'SZ Team',
          				y: <?php echo $admin; ?>,
          				drilldown: 'Admin'
          			}, {
          				name: 'Ambassador',
          				y: <?php echo $agent; ?>,
          				drilldown: 'Agent'
          			} ]
          		}],
          		drilldown: {
          			series: [{
          				name: 'SZ Team',
          				id: 'Admin',
          				data: [
              /*   ['Systems Developement Team', 6],
                ['Human Resources Team', 12],
                ['Operation Team', 15],
                ['Security and Maintenance Team', 18],
                ['Operation Support Team', 4], */
                <?php
                foreach($accAdmin as $key => $val){
                	echo $val['cnt'].",";
                }
                ?> 
                ]
            }, {
            	name: 'Ambassador',
            	id: 'Agent',
            	data: [
            	<?php
            	foreach($accAgent as $key => $val){
            		echo $val['cnt2'].",";
            	}
            	?> 
            	]
            } ]
        }
    });
          });  
      </script>
      <script type="text/javascript">
      	Highcharts.setOptions(Highcharts.theme);
      	var chart = Highcharts.chart('chartx1', {
      		chart: {
      			inverted: true,
      			polar: false,
      		},
      		credits: {
      			enabled: false
      		},
      		title: {
      			text: 'Kudos Count of <?php date_default_timezone_set('Asia/Manila'); echo date("Y");?>'
      		},

      		xAxis: {
      			type: 'category'
      		},

      		plotOptions: {
      			series: {
      				dataLabels: {
      					enabled: true,
      				},
                //colors: ['#3399ff']
            }
        },

        series: [{
        	type: 'column',
        	name: 'Monthly Kudos Count',
        	colorByPoint: true,
        	data: [
        	<?php foreach($cntkudos as $kudos):?>

        	{
        		name: '<?php echo $kudos->monthyear;?>',
        		y: <?php echo $kudos->cnt;?>,
        		drilldown: '<?php echo $kudos->monthyear;?>'
        	},
        <?php endforeach;?>
        ]
    }],

    drilldown: {
    	series: [

    	<?php foreach($cntkudos as $kudos):?>
    	{
    		type: 'column',
    		name: '<?php echo $kudos->monthyear;?>',
    		id: '<?php echo $kudos->monthyear;?>',
    		data: [
    		<?php foreach($ambassadorkudos as $a):?>
    		<?php if($a->monthyear==$kudos->monthyear):?>
    		['<?php echo $a->lname.', '.$a->fname.' '.$a->mname;?>', <?php echo $a->counter;?>],
    	<?php endif;?>
    <?php endforeach;?>


    ]
},
<?php endforeach;?>

]
}


});
</script>

<script type="text/javascript">
	Highcharts.setOptions(Highcharts.theme);
	var chart = Highcharts.chart('chartx2', {
		chart: {
			type: 'bar'
		},
		title: {
			text: 'KUDOS COUNT OF <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
		},
		xAxis: {
			categories: [
			<?php foreach($currentmonthkudos as $kudos):?>
			'<?php  echo $kudos->lname.', '.$kudos->fname?>',
		<?php endforeach;?>]
	},
	yAxis: {
		min: 0,
		title: {
			text: 'VALUES'
		}
	},
	legend: {
		reversed: true
	},
	plotOptions: {
		series: {
			stacking: 'normal'
		}
	},
	series: [{

		colorByPoint: true,
		name: '<?php echo date("F Y");?> KUDOS',
		data: [
		<?php foreach($currentmonthkudos as $kudos):?>
		<?php  echo $kudos->counter;?>,
	<?php endforeach;?>


	]
}]
});
</script>

<script type="text/javascript">
	Highcharts.setOptions(Highcharts.theme);
	var chart = Highcharts.chart('charter5', {
		chart: {
			inverted: true,
			polar: false,
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Kudos Count By Campaigns of <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
		},

		xAxis: {
			type: 'category'
		},

		plotOptions: {
			series: {
				dataLabels: {
					enabled: true,
				},
                //colors: ['#3399ff']
            }
        },

        series: [{
        	type: 'column',
        	name: 'Monthly Kudos Count',
        	colorByPoint: true,
        	data: [
        	<?php foreach($allcampaign as $kudos):?>

        	{
        		name: '<?php echo $kudos->name;?>',
        		y: <?php echo $kudos->counter;?>,
        		drilldown: '<?php echo $kudos->name;?>'
        	},
        <?php endforeach;?>
        ]
    }],

    drilldown: {
    	series: [

    	<?php foreach($allcampaign as $kudos):?>
    	{
    		type: 'column',
    		name: '<?php echo $kudos->name;?>',
    		id: '<?php echo $kudos->name;?>',
    		data: [
    		<?php foreach($campaignambassador as $a):?>
    		<?php if($a->monthyear==$kudos->monthyear && $a->campaignname==$kudos->name):?>
    		['<?php echo $a->lname.', '.$a->fname?>', <?php echo $a->counter;?>],
    	<?php endif;?>
    <?php endforeach;?>


    ]
},
<?php endforeach;?>

]
}


});
</script>
<script type="text/javascript">
	Highcharts.setOptions(Highcharts.theme);
	Highcharts.chart('charter6', {
		chart: {
			type: 'pie',
			options3d: {
				enabled: true,
				alpha: 45,
				beta: 0
			}
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Top 5 Campaigns for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				depth: 35,
				dataLabels: {
					enabled: true,
					format: '{point.name}'
				},
			}
		},
		series: [{
			id: 'toplevel',
			name: 'Kudos Counts',
			data: [
			<?php $x = 1; foreach($newtopcampaign as $newtc):?>
			{name: 'Top <?php echo $x;?>', y:<?php echo $newtc->scores;?>, drilldown: 'Top<?php echo $x;?>'},
			<?php $x++; endforeach;?>
			]
		}],
		drilldown: {
			series: [ 

			<?php $x = 1; foreach($newtopcampaign as $newtc):?>
			{ 
				id:'Top<?php echo $x;?>',
				name: 'Top <?php echo $x;?>',
				data: [
				<?php foreach($allcampaign as $kudos):?>
				<?php if($newtc->monthyear==$kudos->monthyear && $newtc->scores==$kudos->counter):?>
				{name: '<?php echo $kudos->name;?>', y: <?php echo $kudos->counter;?>, drilldown: '<?php echo $kudos->name;?>'},
				<?php array_push($camp, $kudos); endif;?>
			<?php endforeach;?>
			] 
		},
		<?php $x++; endforeach;?>


		<?php foreach($camp as $c):?>

		{
			id: '<?php echo $c->name;?>',
			name: '<?php echo $c->name;?>',
			data: [
			<?php foreach ($campaignambassador as $tca):?>
			<?php if($c->monthyear==$tca->monthyear && $c->name==$tca->campaignname):?>
			['<?php echo $tca->lname.", <br>".$tca->fname;?>', <?php echo $tca->counter;?>],
		<?php endif;?>
	<?php endforeach;?>
	]
},

<?php endforeach;?>
]
}

	});
</script>


<script type="text/javascript">
	Highcharts.setOptions(Highcharts.theme);
	Highcharts.chart('charter7', {
		chart: {
			type: 'pie',
			options3d: {
				enabled: true,
				alpha: 45,
				beta: 0
			}
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Top 5 Ambassador for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				cursor: 'pointer',
				depth: 30,
				dataLabels: {
					enabled: true,
					format: '{point.name}'
				}
			}
		},
		series: [{
			type: 'pie',
			name: 'Kudos Count',
			data: [

			<?php $x=1; foreach($newtopambassador as $t): ?>
				{
					name: 'Top <?php echo $x;?>',
					y: <?php echo $t->scores;?>,
					drilldown: 'Top<?php echo $x;?>'
				},
				<?php $x++; endforeach;?>
				]
			}],
			drilldown: {

				series: [
				<?php $x=1; foreach($newtopambassador as $t): ?>
				{
					type: 'pie',
					name: 'Top <?php echo $x;?>',
					id: 'Top<?php echo $x;?>',
					data: [ 
					<?php foreach ($campaignambassador as $tca):?>
						<?php if($t->monthyear==$tca->monthyear && $t->scores==$tca->counter):?>
						['<?php echo $tca->lname.", <br>".$tca->fname;?>', <?php echo $tca->counter;?>],
						<?php endif;?>
					<?php endforeach;?>
						]
					},
				<?php  $x++; endforeach;?>
			]
		}
});
</script>
</div>

<div id='Templatedesign'>
</div>
</div>

<div class="row" hidden>
	<div class="col-md-8">
		<div class="panel">
			<div class="panel-body">
				<h3 class="title-hero">
					Recent sales activity
				</h3>
				<div class="example-box-wrapper">
					<div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
				</div>
			</div>
		</div>




		<div class="content-box">
			<h3 class="content-box-header bg-default">
				<i class="glyph-icon icon-cog"></i>
				Live server status
				<span class="header-buttons-separator">
					<a href="#" class="icon-separator">
						<i class="glyph-icon icon-question"></i>
					</a>
					<a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
						<i class="glyph-icon icon-refresh"></i>
					</a>
					<a href="#" class="icon-separator remove-button" data-animation="flipOutX">
						<i class="glyph-icon icon-times"></i>
					</a>
				</span>
			</h3>
			<div class="content-box-wrapper">
				<div id="data-example-3" style="width: 100%; height: 250px;"></div>
			</div>
		</div>

	</div>



	<!-- JS Demo -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
	  <script type="text/javascript">
	  	// window.open("<?php echo base_url(); ?>index.php/Survey/campaignnotification",'nicca', 'width=600,height=370,menubar=1,resizable=1,status=0,titlebar=0,toolbar=0');
	  </script> 

<script>
	$(function(){
	 $('#sidebar-menu').load('<?php echo base_url(); ?>/index.php/Templatedesign/sideBar')
		$.ajax({
			type: "POST",
			url:  "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
			cache: false,
			success: function(html)
			{

				$('#headerLeft').html(html);
		   //alert(html);
		}
	});	
		/* $.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
			cache: false,
			success: function(html)
			{

				$('#sidebar-menu').html(html);
		 // alert(html);
		}
	}); */
	});	
</script>


<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>