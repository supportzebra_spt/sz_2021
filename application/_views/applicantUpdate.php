<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		table td{
 			    padding-top: 5px;
			    padding-bottom: 15px;
		}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/starRate/jquery-rating.css">

    <!-- JS Core -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/starRate/jquery-2.1.1.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/starRate/jquery-rating.js"></script>


    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/popover/popover.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable.js"></script>

 <script src="<?php echo base_url();?>assets/js/jquery.addressPickerByGiro.js"></script>
 <script src="http://maps.google.com/maps/api/js?sensor=false&language=en"></script>
 <link href="<?php echo base_url();?>assets/css/jquery.addressPickerByGiro.css" rel="stylesheet" media="screen">

<script>
$(function(){
	$("#fname").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#mname").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#lname").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#dob").editable(),
	$("#religion").editable({source:[{value:"Roman Catholic",text:"Roman Catholic"},
	{value:"Evangelical",text:"Evangelical"},{value:"Iglesia Ni Cristo",text:"Iglesia Ni Cristo"},{value:"Seventh-day Adventist",text:"Seventh-day Adventist"},{value:"Jehovahs Witnesses",text:"SeventhJehovahs Witnesses"},{value:"Jesus is Lord Church",text:"Jesus is Lord Church"},{value:"United Church of Christ in the Philippines",text:"United Church of Christ in the Philippines"},{value:"Others",text:"Others"}]}),
	$("#sex").editable({source:[{value:"Male",text:"Male"},{value:"Female",text:"Female"}]}),
	$("#cs").editable({source:[{value:"Single",text:"Single"},{value:"Married",text:"Married"},{value:"Widowed",text:"Widowed"},{value:"Divorced",text:"Divorced"}]}),
	$("#amn").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#pos").editable({source:[<?php foreach($data as $key =>$val){  echo "{value:".$val["pos_id"].",text:'".$val["pos_details"]."'},"; }?>]}),
	$("#fnamesrc1").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#mnamesrc1").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#lnamesrc1").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#numsrc1").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#fnamesrc2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#mnamesrc2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#lnamesrc2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#numsrc2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#cca").editable({source:[{value:"Yes",text:"Yes"},{value:"None",text:"None"}]}),
	$("#pcnsrc2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#pcysrc2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccaf").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccam").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccal").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#extsrc1").editable({source:[{value:"Jr.",text:"Jr."},{value:"Sr.",text:"Sr."},{value:"I",text:"I"},{value:"II",text:"II"},{value:"III",text:"III"},{value:"IV",text:"IV"},{value:"V",text:"V"}]}),
	$("#extsrc2").editable({source:[{value:"Jr.",text:"Jr."},{value:"Sr.",text:"Sr."},{value:"I",text:"I"},{value:"II",text:"II"},{value:"III",text:"III"},{value:"IV",text:"IV"},{value:"V",text:"V"}]}),
	$("#exts").editable({source:[{value:"Jr.",text:"Jr."},{value:"Sr.",text:"Sr."},{value:"I",text:"I"},{value:"II",text:"II"},{value:"III",text:"III"},{value:"IV",text:"IV"},{value:"V",text:"V"}]}),
	$("#ccaext").editable({source:[{value:"Jr.",text:"Jr."},{value:"Sr.",text:"Sr."},{value:"I",text:"I"},{value:"II",text:"II"},{value:"III",text:"III"},{value:"IV",text:"IV"},{value:"V",text:"V"}]}),
	$("#ccapos").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccaf2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccam2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccal2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccaext2").editable({source:[{value:"Jr.",text:"Jr."},{value:"Sr.",text:"Sr."},{value:"I",text:"I"},{value:"II",text:"II"},{value:"III",text:"III"},{value:"IV",text:"IV"},{value:"V",text:"V"}]}),
	$("#ccapos2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccaf3").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccam3").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccal3").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccaext3").editable({source:[{value:"Jr.",text:"Jr."},{value:"Sr.",text:"Sr."},{value:"I",text:"I"},{value:"II",text:"II"},{value:"III",text:"III"},{value:"IV",text:"IV"},{value:"V",text:"V"}]}),
	$("#ccapos3").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccanum").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccanum2").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}}),
	$("#ccanum3").editable({validate:function(a){return""==$.trim(a)?"This field is required":void 0}})
	})
	 
 </script>
 
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/accordion-ui/accordion.js"></script>
<script type="text/javascript">
    /* jQuery UI Accordion */

    $(function() { "use strict";
        $(".accordion").accordion({
            heightStyle: "content"
        });
    });

    $(function() { "use strict";
        $("#accordion-hover")
                .accordion({
                    event: 'mouseover',
                    heightStyle: 'auto'
                });
    });
</script>
	
</head>
 					 
 

    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="<?php echo $setting['settings']; ?>">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
<div class="form-group">

 <?php if(isset($user)){ ?>
	<div class="example-box-wrapper">
					<a href="../applicant"><button class="btn btn-xs btn-primary"> Back</button></a>
					<a><button class="btn btn-xs btn-primary" id="UpdateApplicant"> Update</button></a>
					<a><button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#EvalAppModal" id="EvaluateApplicant"> Evaluate</button></a>
					<input type="hidden" id="apid" value="<?php echo $_POST['apid']?>">
	<div class="panel">
     <div class="panel-body" style="box-shadow: 10px 10px 5px #dad7d7;">

 <div class="form-group" style="background: #0098cf;padding: 10px;width: 100%;color: white;">
			 <h4>  <b> Personal Information: </b></h4>
				</div>
        <div class="example-box-wrapper" style="margin-top: -15px;">
            <form class="form-horizontal bordered-row" name="myForm">
<table style="width: 95%;margin: 0 auto;" cellpadding="4">
<tbody>
<tr>
<td style="width: 20%;"><b>Fullname</b></td>
<td>
<a href="#" id="fname" data-type="text" data-pk="1" data-title="Enter Firstname"><?php echo ucwords($user['fname']); ?></a>
<a href="#" id="mname" data-type="text" data-pk="1" data-title="Enter Middlename"><?php echo ucwords($user['mname']); ?></a>
<a href="#" id="lname" data-type="text" data-pk="1" data-title="Enter Lastname"><?php echo ucwords($user['lname']); ?></a>
<a href="#" id="exts" data-type="select" data-pk="1" data-title="Enter name extension"><?php echo (!empty($user['nextension']) ||  strcmp("? Undefined:undefined ?",$user['nextension'])>0) ? ucwords($user['nextension']) : ' '; ?> </a>

</td>
<td rowspan=7 style="width: 300px;"><img src="<?php echo base_url()."assets/images/".$user['pic']; ?>" style="width: 100%;"></td>
</tr>
<tr>
<td> <b> Birthday</b> </td>
<td>	<a href="#" id="dob" data-type="combodate" data-value="<?php echo $user['birthday']; ?>" data-format="YYYY-MM-DD" data-viewformat="MMM  D YYYY" data-template="MMM-D-YYYY" data-pk="1"  data-title="Select Date of birth"></a>
</td>
 </tr>
<tr>
<td><b>Gender</b></td>
<td>
<a href="#" id="sex" data-type="select" data-pk="1" data-value="<?php echo $user['gender']; ?>" data-title="Select gender"><?php echo $user['gender']; ?></a>

</td>
 
 </tr>
 <tr>
<td><b>Civil Status</b></td>
<td><a href="#" id="cs" data-type="select" data-pk="1" data-value="<?php echo $user['civilStatus']; ?>" data-title="Civil Status"><?php echo $user['civilStatus']; ?></a>
</td>
 
 </tr>
  <tr>
<td><b>Religion</b></td>
<td><a href="#" id="religion" data-type="select" data-pk="1" data-value="<?php echo $user['relijon']; ?>" data-title="Select gender"><?php echo $user['relijon']; ?></a>
</td>
 
 </tr>
  <tr>
<td><b>Mobile number</b></td>
<td><a href="#" id="mn" data-type="text" data-pk="1" data-title="Enter mobile number"><?php echo $user['cell']; ?></a>
</td>
 
 </tr>
 <tr>
<td><b>Alternative mobile no.</b></td>
<td colspan=2><a href="#" id="amn" data-type="text" data-pk="1" data-title="Enter mobile number"><?php echo (isset($user['tel'])) ? $user['tel'] : '' ; ?></a>
</td>
 
 </tr>
  <tr>
<td><b>Present Address</b></td>
<td colspan=2>
<span data-toggle="modal" data-target="#myModalPresent" style="text-decoration: none;border-bottom: dashed 1px #08c;color: #94a6af;cursor: pointer;">
<?php
 $str = explode("|",$user['presentAddrezs']); 
 echo ( !empty($str[0])) ? $str[0] : "<i style='color:red;'> Empty </i> " ;
 echo (isset($str[1])) ? " ( <i>".$str[1]." " : ''; ;
 echo (isset($str[2])) ? $str[2]." " : ''; ;
 echo (isset($str[3])) ? $str[3]."</i> )" : '';
 
   ?>
  </span>
  <div class="modal fade" id="myModalPresent" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Present Address</h4>
        </div>
        <div class="modal-body">
		<form autocomplete="off" class="form-horizontal">
												<div class="row-fluid">
												  <div class="span6">
													<div class="row-fluid">
 														<div class="controls">
														  <input type="text" class="inputAddress input-xxlarge form-control"   autocomplete="off" placeholder="Type in your address" style="width: 100%;" id="presentAddr" name="presentAddr" ng-model="presentAddr" value="<?php echo ( !empty($str[0])) ? $str[0] : " "  ?>">
														</div>
													</div>			  
												  </div>
												  <div class="span6">
												  <div class="control-group">
													<label class="control-label">Formatted address</label>
													<div class="controls">
													  <input type="text" class="formatted_address input-xxlarge form-control" style="width: 100%;"  id="presentformatAddr" name="presentformatAddr" ng-model="presentformatAddr" required>
													</div>
												  </div>
											 
												  <div class="control-group">
													<label class="control-label">Province</label>
													<div class="controls">
													  <input type="text" class="county form-control" style="width: 100%;" disabled="disabled" id="presentProvince" name="presentProvince" ng-model="presentProvince" required>
													</div>
												  </div>
												 
												  <div class="control-group">
													<label class="control-label">city</label>
													<div class="controls">
													  <input type="text" class="city form-control" style="width: 100%;" disabled="disabled" id="presentCity" name="presentCity" ng-model="presentCity" required>
													</div>
												  </div>
												  <div class="control-group">
													<label class="control-label">zip</label>
													<div class="controls">
			<input type="text" class="form-control" style="width: 100%;" id="presentZip" name="presentZip" ng-model="presentZip" value="<?php  echo (isset($str[3])) ? $str[3] : " ";?>" >
													</div>
												  </div>
												  <div class="control-group">
													<label class="control-label"> </label>
													<div class="controls">
														<input type="button" class="btn btn-xs btn-primary" id="btnPresentaddress" value="update">
													</div>
												  </div>
 												  </div>
												</div>
												</form>
											  </div><!--/span-->
											<script>
												$('.inputAddress').addressPickerByGiro({
													distanceWidget: true,
													boundElements: {
														'country': '.country',
														'country_code': '.country_code',
														'region': '.region',
														'region_code': '.region_code',
														'county': '.county',
														'county_code': '.county_code',
														'city': '.city',
														'city_district': '.city_district',
														'street': '.street',
														'street_number': '.street_number',
														'zip': '.zip',
														'latitude': '.latitude',
														'longitude': '.longitude',
														'formatted_address': '.formatted_address',
														'radius': '.radius'
													}
												});
											</script>        </div>
       
      </div>
      </div>
      </div>
      
  </td>
 </tr>
  <tr>
<td><b>Permanent Address</b></td>
 
 <td colspan=2>
 <span data-toggle="modal" data-target="#myModalPermanent" style="text-decoration: none;border-bottom: dashed 1px #08c;color: #94a6af;cursor: pointer;">

 <?php
 $str = explode("|",$user['permanentAddrezs']);
 echo ( !empty($str[0])) ? $str[0] : "<i style='color:red;'> Empty </i> " ;
 echo " ( <i>";
 echo (!empty($str[1])) ? $str[1]." " : ''; ;
 echo (!empty($str[2])) ? $str[2]." " : ''; ;
 echo (!empty($str[3])) ? $str[3]."</i> )" : '';;
 
 ?>
 </span>
  <div class="modal fade" id="myModalPermanent" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Permanent Address</h4>
        </div>
        <div class="modal-body">
<form autocomplete="off" class="form-horizontal">
												<div class="row-fluid">
												  <div class="span6">
													<div class="row-fluid">
 														<div class="controls">
														  <input type="text" class="inputAddress2 input-xxlarge form-control"   autocomplete="off" placeholder="Type in your address" style="width: 100%;" id="permanentAddr" name="permanentAddr" ng-model="permanentAddr" value="<?php echo ( !empty($str[0])) ? $str[0] : " "  ?>">
														</div>
													</div>			  
												  </div>
												  <div class="span6">
												  <div class="control-group">
													<label class="control-label">Formatted address</label>
													<div class="controls">
													  <input type="text" class="formatted_address input-xxlarge form-control" style="width: 100%;"  id="permanentformatAddr" name="permanentformatAddr" ng-model="permanentformatAddr" required>
													</div>
												  </div>
											 
												  <div class="control-group">
													<label class="control-label">Province</label>
													<div class="controls">
													  <input type="text" class="county form-control" style="width: 100%;" disabled="disabled" id="permanentProvince" name="permanentProvince" ng-model="permanentProvince" required>
													</div>
												  </div>
												 
												  <div class="control-group">
													<label class="control-label">city</label>
													<div class="controls">
													  <input type="text" class="city form-control" style="width: 100%;" disabled="disabled" id="permanentCity" name="permanentCity" ng-model="permanentCity" required>
													</div>
												  </div>
												  <div class="control-group">
													<label class="control-label">zip</label>
													<div class="controls">
			<input type="text" class="form-control" style="width: 100%;" id="permanentZip" name="permanentZip" ng-model="permanentZip" value="<?php  echo (isset($str[3])) ? $str[3] : " ";?>" >
													</div>
												  </div>
												  <div class="control-group">
													<label class="control-label"> </label>
													<div class="controls">
														<input type="button" class="btn btn-xs btn-primary" id="btnpermanentaddress" value="update">
													</div>
												  </div>
 												  </div>
												</div>
												</form> 
												</div>
													<script>
												$('.inputAddress2').addressPickerByGiro({
													distanceWidget: true,
													boundElements: {
														'country': '.country',
														'country_code': '.country_code',
														'region': '.region',
														'region_code': '.region_code',
														'county': '.county',
														'county_code': '.county_code',
														'city': '.city',
														'city_district': '.city_district',
														'street': '.street',
														'street_number': '.street_number',
														'zip': '.zip',
														'latitude': '.latitude',
														'longitude': '.longitude',
														'formatted_address': '.formatted_address',
														'radius': '.radius'
													}
												});
											</script>  
     
      </div>
      </div>
      </div>
 </td>
 </tr>
 
</tbody>
</table> 
 
 
 
				</form>
				</div>
			 <div class="form-group" style="background: #0098cf;padding: 10px;width: 100%;color: white;">
			 <h4>  <b> Application details: </b></h4>
				</div>	 
				
		<div class="example-box-wrapper" style="margin-top: -15px;">
            <form class="form-horizontal bordered-row" name="myForm">
				<table style="width: 95%;margin: 0 auto;" cellpadding="4">
				<tbody>
				 <tr>
				<td><b>Position applied</b></td>
				<td>
				<a href="#" id="pos" data-type="select" data-pk="1" data-value="<?php echo $user['pos_details']; ?>" data-title="Select gender"><?php echo $user['pos_details']; ?></a>
 				</td>
				 </tr>
				  <tr>
					<td><b>Source</b></td>
					<td colspan=2><?php

					 //$str = explode("|",$user['source']); 
					 
							//if(!empty($str[0])){
							if(!empty($user['source'])){
								$src =  explode("$",$str[0]);
								?>
					<!--<a href="#" id="fnamesrc1" data-type="text" data-pk="1" data-title="Enter Firstname"><?php  echo (!empty($src[0])) ? ucwords($src[0]) : " "; ?></a>
					<a href="#" id="mnamesrc1" data-type="text" data-pk="1" data-title="Enter Middlename"><?php  echo (!empty($src[1])) ? ucwords($src[1]) : " "; ?></a>
					<a href="#" id="lnamesrc1" data-type="text" data-pk="1" data-title="Enter Lastname"><?php  echo (!empty($src[2])) ? ucwords($src[2]) : " "; ?></a>
					<a href="#" id="extsrc1" data-type="select" data-pk="1" data-title="Enter name extension"><?php echo (!empty($src[3]) || strcmp("? Undefined:undefined ?",$src[3])>0) ? $src[3] : "--"; ?></a>
					<a href="#" id="numsrc1" data-type="text" data-pk="1" data-title="Enter number"><?php echo (!empty($src[4])) ? ucwords($src[4]) : " " ; ?></a>-->
<a href="#" id="fnamesrc1" data-type="text" data-pk="1" data-title="Enter Firstname"><?php echo ucwords($user['source']); ?></a>

					  
							<?php	 

							}else{
							  echo "<i style='color:red;'> Empty </i> "; 
							}
					 ?></td>
	</tr>
			<tr>
				<td><b>Call center experience</b></td>
				<td>
				<a href="#" id="cca" data-type="select" data-pk="1" data-value="<?php echo $user['ccaExp']; ?>" data-title="Select CCA experience"><?php echo $user['ccaExp']; ?></a>
					<?php 
					if($user['ccaExp']=="Yes"){
						$pc =  explode("|",$user['previouscompany']);
					echo "( ";
					?>	
					
					<a href="#" id="pcnsrc2" data-type="text" data-pk="1" data-title="Enter Firstname"><?php  echo (!empty($pc[0])) ? ucwords($pc[0]) : '';; ?></a>
					<a href="#" id="pcysrc2" data-type="text" data-pk="1" data-title="Enter Firstname"><?php  echo (!empty($pc[1])) ? ucwords($pc[1]) : '';; ?></a>
	
					<?php

					echo " )";
					}else{}
					?>
				
 				</td>
			</tr>
			<tr>
				<td><b>Character Reference 1</b></td>
				<td>
				<?php 
			 
						$cr1 =  explode("|",$user['cr1']);
						$crn1 =  explode("$",$cr1[0]);
						
					?>	
			 
				<a href="#" id="ccaf" data-type="text" data-pk="1" data-value="<?php  echo (!empty($crn1[0])) ? ucwords($crn1[0]) : ''; ?>" data-title="Select Firstname"><?php  echo (!empty($crn1[0])) ? ucwords($crn1[0]) : '';; ?></a>
				<a href="#" id="ccam" data-type="text" data-pk="1" data-value="<?php  echo (!empty($crn1[1])) ? ucwords($crn1[1]) : ''; ?>" data-title="Select Middlename"><?php  echo (!empty($crn1[1])) ? ucwords($crn1[1]) : '';; ?></a>
				<a href="#" id="ccal" data-type="text" data-pk="1" data-value="<?php  echo (!empty($crn1[2])) ? ucwords($crn1[2]) : ''; ?>" data-title="Select Lastname"><?php  echo (!empty($crn1[2])) ? ucwords($crn1[2]) : '';; ?></a>
				<a href="#" id="ccaext" data-type="select" data-pk="1" data-value="<?php  echo (!empty($crn1[3])) ? ucwords($crn1[3]) : ''; ?>" data-title="Select name extension"><?php  echo (!empty($crn1[3])) ? ucwords($crn1[3]) : ''; ?></a>
				 <br>
				<a href="#" id="ccanum" data-type="text" data-pk="1" data-value="<?php  echo (!empty($cr1[1])) ? ucwords($cr1[1]) : ''; ?>" data-title="Enter mobile number"><?php  echo (!empty($cr1[1])) ? ucwords($cr1[1]) : '';; ?></a>
				<br>
				<a href="#" id="ccapos" data-type="text" data-pk="1" data-value="<?php  echo (!empty($cr1[2])) ? ucwords($cr1[2]) : ''; ?>" data-title="Enter position"><?php  echo (!empty($cr1[2])) ? ucwords($cr1[2]) : '';; ?></a>

 				</td>
			</tr>
			<tr>
				<td><b>Character Reference 2</b></td>
				<td>
				<?php 
			 
						$cr2 =  explode("|",$user['cr2']);
						$crn2 =  explode("$",$cr2[0]);
						
					?>	
			 
				<a href="#" id="ccaf2" data-type="text" data-pk="1" data-value="<?php  echo (!empty($crn2[0])) ? ucwords($crn2[0]) : ''; ?>" data-title="Select Firstname"><?php  echo (!empty($crn2[0])) ? ucwords($crn2[0]) : '';; ?></a>
				<a href="#" id="ccam2" data-type="text" data-pk="1" data-value="<?php  echo (!empty($crn2[1])) ? ucwords($crn2[1]) : ''; ?>" data-title="Select Middlename"><?php  echo (!empty($crn2[1])) ? ucwords($crn2[1]) : '';; ?></a>
				<a href="#" id="ccal2" data-type="text" data-pk="1" data-value="<?php  echo (!empty($crn2[2])) ? ucwords($crn2[2]) : ''; ?>" data-title="Select Lastname"><?php  echo (!empty($crn2[2])) ? ucwords($crn2[2]) : '';; ?></a>
				<a href="#" id="ccaext2" data-type="select" data-pk="1" data-value="<?php  echo (!empty($crn2[3])) ? ucwords($crn2[3]) : ''; ?>" data-title="Select name extension"><?php  echo (!empty($crn2[3])) ? ucwords($crn2[3]) : ''; ?></a>
				 <br>
				<a href="#" id="ccanum2" data-type="text" data-pk="1" data-value="<?php  echo (!empty($cr2[1])) ? ucwords($cr2[1]) : ''; ?>" data-title="Enter mobile number"><?php  echo (!empty($cr2[1])) ? ucwords($cr2[1]) : '';; ?></a>
				<br>
				<a href="#" id="ccapos2" data-type="text" data-pk="1" data-value="<?php  echo (!empty($cr2[2])) ? ucwords($cr2[2]) : ''; ?>" data-title="Enter position"><?php  echo (!empty($cr2[2])) ? ucwords($cr2[2]) : '';; ?></a>

 				</td>
			</tr>
			<tr>
				<td><b>Character Reference 3</b></td>
				<td>
				<?php 
			 
						$cr3 =  explode("|",$user['cr3']);
						$crn3 =  explode("$",$cr3[0]);
						
					?>	
			 
				<a href="#" id="ccaf3" data-type="text" data-pk="1" data-value="<?php  echo (!empty($crn3[0])) ? ucwords($crn3[0]) : ''; ?>" data-title="Select Firstname"><?php  echo (!empty($crn3[0])) ? ucwords($crn3[0]) : '';; ?></a>
				<a href="#" id="ccam3" data-type="text" data-pk="1" data-value="<?php  echo (!empty($crn3[1])) ? ucwords($crn3[1]) : ''; ?>" data-title="Select Middlename"><?php  echo (!empty($crn3[1])) ? ucwords($crn3[1]) : '';; ?></a>
				<a href="#" id="ccal3" data-type="text" data-pk="1" data-value="<?php  echo (!empty($crn3[2])) ? ucwords($crn3[2]) : ''; ?>" data-title="Select Lastname"><?php  echo (!empty($crn3[2])) ? ucwords($crn3[2]) : '';; ?></a>
				<a href="#" id="ccaext3" data-type="select" data-pk="1" data-value="<?php  echo (!empty($crn3[3])) ? ucwords($crn3[3]) : ''; ?>" data-title="Select name extension"><?php  echo (!empty($crn3[3])) ? ucwords($crn3[3]) : ''; ?></a>
				 <br>
				<a href="#" id="ccanum3" data-type="text" data-pk="1" data-value="<?php  echo (!empty($cr3[1])) ? ucwords($cr3[1]) : ''; ?>" data-title="Enter mobile number"><?php  echo (!empty($cr3[1])) ? ucwords($cr3[1]) : '';; ?></a>
				<br>
				<a href="#" id="ccapos3" data-type="text" data-pk="1" data-value="<?php  echo (!empty($cr3[2])) ? ucwords($cr3[2]) : ''; ?>" data-title="Enter position"><?php  echo (!empty($cr3[2])) ? ucwords($cr3[2]) : '';; ?></a>

 				</td>
			</tr>
				</tbody>
				</table> 
 
 
 
				</form>
				</div>		
				
				
				
				
	</div>	 
	</div> 
 <?php }else{ ?>

		<?php echo "as"; ?>
  <?php }?>	
                </div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>
<script>
$(function(){
	$("#btnViewQuestion").click(function(){
		$("#IntQuery").slideDown("slow");
		// $("#btnSaveQuestion").attr("hidden",false);
		// $(this).attr("hidden",true);
		$("#btnSaveQuestion").css("visibility","visible");
		$(this).css("visibility","hidden");

	});
	$("#btnSaveQuestion").click(function(){
		$("#IntQuery").slideUp("slow");
		// $("#btnViewQuestion").attr("hidden",false);
		// $(this).attr("hidden",true);
		$("#btnViewQuestion").css("visibility","visible");
		$(this).css("visibility","hidden");

	});
	$("#btnSaveQuestion2").click(function(){
		$("#IntQuery").slideUp("slow");
 		// $("#btnViewQuestion").attr("hidden",false);
		// $("#btnSaveQuestion").attr("hidden",true);
		$("#btnSaveQuestion").css("visibility","hidden");
		$("#btnViewQuestion").css("visibility","visible");

	});
	$("#examType").change(function(){
		if($(this).val() == "Interview"){
			$(".InterviewQues").slideDown("slow");
		}else{
			$('.InterviewQues').slideUp("slow");

		}
	});
	
})
</script>
<div id="EvalAppModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #212121;color: white;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Applicant Evaluation (<?php echo $lvl["lvl"]; ?>)</h4> 
      </div>
      <div class="modal-body">
		

<div class="panel">
            <div class="panel-body">
                
                <div class="example-box-wrapper">
				  <div class="form-group">
							<label class="col-sm-3 control-label">Exam</label>
							<div class="col-sm-8" >
					 
								<select class="form-control" id="examType"  required>
								<option selected disabled>--</option>
								<option>Written</option>
								<option>Interview</option>
								</select>
							</div>
					</div>
					</div>
						 <div class="example-box-wrapper">  

								<div class="InterviewQues" hidden>
									<div class="form-group">
										<label class="col-sm-3 control-label">Interview Questions</label>
										<div class="col-sm-8" >
										<input type="button" class="btn btn-xs btn-info" value="view" id="btnViewQuestion" >
										 <span id='tots' style="float: right;font-size: x-large;"> </span> 
										</div>
									 </div>
									 <br>
									 <br>
									 <br>
									 <br>
									<div class="accordion" id="IntQuery" hidden >
										<?php 
											$cnt =0;
											If(!empty($catInt) ){
											foreach($catInt as $key => $val){ 
												$str = explode("*",$key);
												echo "<h3>".$str[0]   ?>
													<div style="width: 10px;float: right;" >
													 
														<div id="info<?php echo $cnt; ?>" class="infoClass" style='color:red'/>0</div>
														<div id="info1<?php echo $cnt; ?>" class="ScoreClass" style='color:red' hidden ><?php  echo $str[1]; ?>= 0</div>
													</div>
												</h3>
 
												<div>
												<ul>
												<?php 
												foreach($val as $key2 => $val2){
												echo "<li>".$val2['question']."</li>";
												}
 												  ?>
												</ul>
												<div class="group<?php echo $cnt; ?>" >
													<div class="jr-nomal"> </div>
													<div class="jr-nomal"> </div>
													<div class="jr-nomal"> </div>
													<div class="jr-nomal"> </div>
													<div class="jr-nomal"> </div>
													<div class="jr-nomal"> </div>
													<div class="jr-nomal"> </div>
													<div class="jr-nomal"> </div>
													<div class="jr-nomal"> </div>
													<div class="jr-nomal"> </div>
												</div>
												<script type="text/javascript">
													$('.group<?php echo $cnt; ?>').start(function(cur){
														 $('#info<?php echo $cnt; ?>').text(cur).css("color","#cc6600");
														 $('#info1<?php echo $cnt; ?>').text(<?php  echo $str[1]; ?>+"="+cur).css("color","#cc6600");

														 });
 	 
												</script>
												</div>
												<?php 	
													$cnt++;												
												}
												}else{
													echo "<span style='display: block;font-size: x-large;text-align: center;'>No question was set for the position applied!</span>";
												}
												 
												?>
										</div>
											<input type="button" class="btn btn-xs btn-success" value="save" id="btnSaveQuestion" style="visibility:hidden">
											<hr>
											<div class="form-group">
												<label class="col-sm-3 control-label">Comments</label>
												<div class="col-sm-8" >
													<textarea name=""  rows="3" class="form-control textarea-sm" id="commentInterview"></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Date</label>
												<div class="col-sm-8" >
													<input type="text" class="bootstrap-datepicker form-control"   data-date-format="mm/dd/yy" id="dateInterview">
												</div>
											</div>
										</div>
                </div>
            </div>
        </div> 
       
		</div>
		<script>
		$(function(){
			 $("#EvalAppModal").mouseover(function(){
			var sum = 0;
			var total = 0;
			var Ototal = 0;
			
			$(".infoClass").each(function(){
				sum += parseInt($(this).text()); 
				total  +=1;
			});
			total*=10;
			Ototal = parseFloat((sum/total)*100).toFixed(2);
			$("#tots").text(Ototal);
			if($("#tots").text() < 60){
				$("#tots").css("color","red");
			}else{
				$("#tots").css("color","blue");

			}
  		});
		 

		});
		</script>
      <div class="modal-footer">
        <button  id="btnEvall">Evaluate</button>
      </div>
    </div>

  </div>
</div>
<span id='testOutput'></span>
     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
	    $.ajax({
		type: "POST",
		url: "<?php echo base_url();?>index.php/Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>index.php/Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });

  });	
</script>

<script>
  $(function(){
 	
$('#UpdateApplicant').click(function(){
  var fName = $("#fname").text();
  var mName = $("#mname").text();
  var lName = $("#lname").text();
  var nextension = $("#exts").text();
  var dob = $("#dob").text().split(" ");
  var mn = $("#mn").text();
  var amn = $("#amn").text();
  var religion = $("#religion").text();
  var sex = $("#sex").text();
  var cs = $("#cs").text();
 	var presentformatAddr = $("#presentformatAddr").val();
	var presentProvince = $("#presentProvince").val();
	var presentCity = $("#presentCity").val();
	var presentZip = $("#presentZip").val();
	var prsntAddr =  presentformatAddr+"|"+presentProvince+"|"+presentCity+"|"+presentZip;

 	var permanentformatAddr = $("#permanentformatAddr").val();
	var permanentProvince = $("#permanentProvince").val();
	var permanentCity = $("#permanentCity").val();
	var permanentZip = $("#permanentZip").val();
	var prmntAddr =   permanentformatAddr+"|"+permanentProvince+"|"+permanentCity+"|"+permanentZip;
	
	var pos = $("#pos").text();
	var cca = $("#cca").text();
 	 var ccaf = $("#ccaf").text();
	 var ccam = $("#ccam").text();
	 var ccal = $("#ccal").text();
	 var ccaext = $("#ccaext").text();
	 var ccanum = $("#ccanum").text();
	 var ccapos = $("#ccapos").text();
 	 var characterRef1 = ccaf+"|"+ccam+"|"+ccal+"|"+ccaext+"|"+ccanum+"|"+ccapos;

	 var ccaf2 = $("#ccaf2").text();
	 var ccam2 = $("#ccam2").text();
	 var ccal2 = $("#ccal2").text();
	 var ccaext2 = $("#ccaext2").text();
	 var ccanum2 = $("#ccanum2").text();
	 var ccapos2 = $("#ccapos2").text(); 
	 var characterRef2 = ccaf2+"|"+ccam2+"|"+ccal2+"|"+ccaext2+"|"+ccanum2+"|"+ccapos2;

	 var ccaf3 = $("#ccaf3").text();
	 var ccam3 = $("#ccam3").text();
	 var ccal3 = $("#ccal3").text();
	 var ccaext3 = $("#ccaext3").text();
	 var ccanum3 = $("#ccanum3").text();
	 var ccapos3 = $("#ccapos3").text();
	 var characterRef3 = ccaf3+"|"+ccam3+"|"+ccal3+"|"+ccaext3+"|"+ccanum3+"|"+ccapos3;
	
	var fnamesrc1 = $("#fnamesrc1").text();
	var mnamesrc1 = $("#mnamesrc1").text();
	var lnamesrc1 = $("#lnamesrc1").text();
	var extsrc1 = $("#extsrc1").text();
	var numsrc1 = $("#numsrc1").text();
	
	var fnamesrc2 = $("#fnamesrc2").text();
	var mnamesrc2 = $("#mnamesrc2").text();
	var lnamesrc2 = $("#lnamesrc2").text();
	var extsrc2 = $("#extsrc2").text();
	var numsrc2 = $("#numsrc2").text();
var src = fnamesrc1+"$"+ mnamesrc1+"$"+ lnamesrc1+"$"+ extsrc1+"$"+ numsrc1+"|"+fnamesrc2+"$"+mnamesrc2+"$"+lnamesrc2+"$"+extsrc2+"$"+numsrc2;
	 
	 
	 
	 
 var apid = $("#apid").val();
 var month = {Jan:"01",Feb:"02",Mar:"03",Apr:"04",May:"05",Jun:"06",Jul:"07",Aug:"08",Sep:"09",Oct:"10",Nov:"11",Dec:"12"};
 var bday = dob[3]+"-"+month[dob[0]]+"-"+dob[2];
 	// alert(fName+" "+mName+" "+lName+" "+nextension+" "+bday+" "+sex+" "+cs+" "+religion+" "+mn+" "+amn+ " "+prsntAddr+" "+prmntAddr+" "+pos+" "+cca+" "+characterRef1+" "+characterRef2+" "+characterRef3+" "+src);
 
 dataString = "fName="+fName+"&mName="+mName+"&lName="+lName+"&nextension="+nextension+"&bday="+bday+"&gender="+sex+"&civilStatus="+cs+"&relijon="+religion+"&cell="+mn+"&tel="+amn+"&presentAddrezs="+prsntAddr+"&permanentAddrezs="+prmntAddr+"&pos_id="+pos+"&ccaExp="+cca+"&cr1="+characterRef1+"&cr2="+characterRef2+"&cr3="+characterRef3+"&Source="+src+"&apid="+apid;

  $.ajax({
type: "POST",
url: "updateApplicantss",
data: dataString,
cache: false,
success: function(html)
{
	swal(
  'Success',
  'You have just successfully Updated the record!',
  'success'
)
// $("#testOutput").html(html);
// alert(html);
 setTimeout(function(){location.reload();},2000);

}
});	  
});
$('#btnEvall').click(function(){
	var exam = $("#examType").val();
	var score = $("#tots").text();
	var dateInt = $("#dateInterview").val();
	var commentInt = $("#commentInterview").val();
	var level = "<?php echo $lvl["lvl"]; ?>";
	var pos_id = <?php echo $user['pos_id']; ?>;
	var apid = <?php echo $user['apid']; ?>;
	var uid = <?php echo $_SESSION['uid']; ?>;
	var interviewer = "<?php echo $_SESSION['fname']; ?>";
	var scoreDetails  = "";
	$(".ScoreClass").each(function(){
		if($(this).text()!=0){
				  scoreDetails += "|"+$(this).text();
		}
	});
 dataString = "exam="+exam+"&score="+score+"&dateInt="+dateInt+"&commentInt="+commentInt+"&scoreDetails="+scoreDetails+"&level="+level+"&pos_id="+pos_id+"&uid="+uid+"&apid="+apid+"&interviewer="+interviewer;
 
 $.ajax({
type: "POST",
url: "addExamApplicant",
data: dataString,
cache: false,
success: function(html)
{
	swal(
  'Success',
  'You have just successfully Updated the record!',
  'success'
)
// $("#testOutput").html(html);
  alert(html);
 //setTimeout(function(){location.reload();},2000);

}
});});
});
</script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>