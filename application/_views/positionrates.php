<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  </style>


  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });
 </script>

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>
 <script type="text/javascript">
  /* Input switch */

  $(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
  });
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd-yyyy'
    });
  });


</script>
<script type="text/javascript">
  $(function(){
    $(".growl1").select(function(){
      $.jGrowl(
        "<?php echo $this->session->flashdata('success');?>",
        {sticky:!1,position:"top-right",theme:"bg-blue-alt"}
        )
    });

    if("<?php echo $this->session->flashdata('success');?>" != ""){
      $(".growl1").trigger('select');
    }
  });
</script>


</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div class="container">

          <script type="text/javascript">

            /* Datatables export */

            $(document).ready(function() {
              var table = $('#datatable-tabletools').DataTable();
            });

            $(document).ready(function() {
              $('.dataTables_filter input').attr("placeholder", "Search...");
            });

          </script>
          
          <div id="page-title">
            <?php if(!in_array(DATE('j'),$days)): ?>

              <div class="btn-group">
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm"><span class='icon-typicons-plus'></span> Rate</button>

                <button class="btn btn-info btn-sm" onclick="location.reload();"><span class='icon-typicons-cw-outline'></span> Reload</button>

                <button class="btn btn-warning btn-sm" onclick="window.location = '<?php echo base_url();?>index.php/PositionRates/positionallowance'"><span class='icon-typicons-plus-outline'></span> Allowance</button>

              </div>
            <?php endif;?>
          </div>
          <br>
          <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header" style="background: #1f2e2e; color: white;">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 class="modal-title">New Rate</h3>
                </div>
                <div class="modal-body">
                  <div class="row" id='forms'>
                    <div class="col-md-8">
                      <label class='control-label'>Departments
                        <span class="text-danger" id='dn' style='font-size:8px;' hidden>* (Required)</span>
                      </label>
                      <select class='form-control' id='department'>
                        <option value=''>--</option>
                        <?php foreach($departments as $d):?>
                          <option value='<?php echo $d->dep_id;?>'><?php echo $d->dep_details;?></option>
                        <?php endforeach;?>
                      </select><br>
                    </div>
                    <div class="col-md-4">
                      <label class='control-label'>Effectivity Date
                        <span class="text-danger" id='en' style='font-size:8px;' hidden>* (Required)</span>
                      </label>
                      <div class="input-prepend input-group">
                        <span class="add-on input-group-addon">
                          <i class="glyph-icon icon-calendar"></i>
                        </span>
                        <input type="text" id='effecdate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <label class='control-label'>Position
                        <span class="text-danger" id='pn' style='font-size:8px;' hidden>* (Required)</span>
                      </label>
                      <select class='form-control' id='position' onchange="$('#rate').val('');" >
                        <option value=''>--</option>

                      </select><br>
                    </div>
                    <div class="col-md-5">

                      <label class='control-label'>Status
                        <span class="text-danger" id='sn' style='font-size:8px;' hidden>* (Required)</span>
                      </label>
                      <select class='form-control' id='status' onchange="$('#rate').val('');">
                        <option value=''>--</option>
                      </select>
                    </div>
                    <div class="col-md-7">
                      <label class='control-label'>Rate
                        <span class="text-danger" id='rn' style='font-size:8px;' hidden>* (Required)</span>
                        <span class="text-danger" id='rn2' style='font-size:8px;' hidden>* Valid Number Only</span>

                      </label>
                      <input type="text" name="" class='form-control' id='rate' list="dbrates">
                      <datalist id="dbrates">
                      </datalist><br>
                      <button type="button" class="btn text-primary btn-link btn-xs pull-right" id='allowance'>+ Add Allowance</button>
                    </div> 
                    <div class="col-md-12" style='display:none' id='allowancediv'>
                      <div class="row">
                        <div class="col-md-4">
                          <label class='control-label'>Clothing
                            <span class="text-danger" id='a1' style='font-size:8px;' hidden>* (Required)</span>
                            <span class="text-danger" id='a2' style='font-size:8px;' hidden>* Valid Number Only</span>

                          </label>
                          <input type="text" name="" class='form-control' id='clothing'>
                        </div>
                        <div class="col-md-4">
                          <label class='control-label'>Laundry
                            <span class="text-danger" id='b1' style='font-size:8px;' hidden>* (Required)</span>
                            <span class="text-danger" id='b2' style='font-size:8px;' hidden>* Valid Number Only</span>

                          </label>
                          <input type="text" name="" class='form-control' id='laundry'>
                        </div>
                        <div class="col-md-4">
                          <label class='control-label'>Rice
                            <span class="text-danger" id='c1' style='font-size:8px;' hidden>* (Required)</span>
                            <span class="text-danger" id='c2' style='font-size:8px;' hidden>* Valid Number Only</span>
                          </label>
                          <input type="text" name="" class='form-control' id='rice'>
                        </div> 
                      </div>
                    </div>
                  </div> 
                </div>    
                <div class="modal-footer" id='footer'>
                  <span id='availablerate'></span>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id='addnewrate'>Save</button>
                  <button type="button" class="btn btn-primary" id='reactivaterate' style='display:none;'>Reactivate</button>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">

              <table id="datatable-tabletools" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                <thead>
                  <tr>
                   <th class='text-center' style='background: #787a7b;color: white;border-top-left-radius: 10px;'>Code</th>
                   <th class='text-center' style='background: #787a7b;color: white;'>Description</th>
                   <th class='text-center' style='background: #787a7b;color: white;'>Status</th>
                   <th class='text-center' style='background: #787a7b;color: white;'>Class</th>
                   <th class='text-center' style='background: #787a7b;color: white;'>Department Name</th>
                   <th class='text-center' style='background: #787a7b;color: white;<?php if(in_array(DATE('j'),$days)){echo "border-top-right-radius: 10px";} ?>'>Rate</th>
                   <?php if(!in_array(DATE('j'),$days)): ?>
                     <th class='text-center' style='background: #787a7b;color: white;border-top-right-radius: 10px'>Active</th>
                   <?php endif;?>
                 </tr>
               </thead>
               <tfoot>
                <tr>
                 <th class='text-center' style='background: #787a7b;color: white;border-bottom-left-radius: 10px;'> Code</th>
                 <th class='text-center' style='background: #787a7b;color: white;'>Description</th>
                 <th class='text-center' style='background: #787a7b;color: white;'>Status</th>
                 <th class='text-center' style='background: #787a7b;color: white;'>Class</th>
                 <th class='text-center' style='background: #787a7b;color: white;'>Department</th>
                 <th class='text-center' style='background: #787a7b;color: white;<?php if(in_array(DATE('j'),$days)){echo "border-bottom-right-radius: 10px";} ?>'>Rate</th>
                 <?php if(!in_array(DATE('j'),$days)): ?>
                   <th class='text-center' style='background: #787a7b;color: white;border-bottom-right-radius: 10px'>Active</th>
                 <?php endif;?>
               </tr>
             </tfoot>
             <tbody>
               <?php foreach($values as $v):?>
                <tr>
                  <!-- <td><?php echo $v->rate_id;?></td> -->
                  <td><?php echo $v->pos_name;?></td>
                  <td><?php echo $v->pos_details;?></td>
                  <td><?php echo $v->status;?></td>
                  <td><?php echo $v->class;?></td>
                  <td><?php echo $v->dep_name.' - '.$v->dep_details;?></td>
                  <td class='text-right'><?php echo $v->rate;?></td>
                  <?php if(!in_array(DATE('j'),$days)): ?>

                    <td>
                      <input type="checkbox" data-on-color="info" data-off-color="danger" name="" class="input-switch" checked data-size="mini" data-on-text="A" data-off-text="IA" id='id-<?php echo $v->rate_id; ?>' onchange='setActiveness(<?php echo $v->rate_id; ?>)'>
                    </td>
                  <?php endif;?>
                </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
      </div>

    </div>

    <div class="row" hidden>
      <div class="col-md-8">
        <div class="panel">
          <div class="panel-body">
            <h3 class="title-hero">
              Recent sales activity
            </h3>
            <div class="example-box-wrapper">
              <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
            </div>
          </div>
        </div>




        <div class="content-box">
          <h3 class="content-box-header bg-default">
            <i class="glyph-icon icon-cog"></i>
            Live server status
            <span class="header-buttons-separator">
              <a href="#" class="icon-separator">
                <i class="glyph-icon icon-question"></i>
              </a>
              <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                <i class="glyph-icon icon-refresh"></i>
              </a>
              <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                <i class="glyph-icon icon-times"></i>
              </a>
            </span>
          </h3>
          <div class="content-box-wrapper">
            <div id="data-example-3" style="width: 100%; height: 250px;"></div>
          </div>
        </div>

      </div>

    </div>
  </div>



</div>
</div>
</div>

<p class="growl1" hidden></p>
<p class="growl2" hidden></p>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $("#addnewrate").click(function(){
    var position = $("#position").val();
    var status = $("#status").val();
    var rate = $("#rate").val();
    var effecdate = $("#effecdate").val();
    var clothing = $("#clothing").val();
    var laundry = $("#laundry").val();
    var rice = $("#rice").val();
    if(position==''){
      $("#pn").show();
    }else{
      $("#pn").hide();
    }
    if(status==''){
      $("#sn").show();
    }else{
      $("#sn").hide();
    }
    if(rate==''){
      $("#rn").show();
    }else if(!$.isNumeric( rate )){
      $('#rn2').show();
    }else{
      $("#rn").hide();
      $("#rn2").hide();
    }
    if(effecdate==''){
      $("#en").show();
    }else{
      $("#en").hide();
    }

    if($("#allowancediv").is(':visible')){
     if(clothing==''){
      $("#a1").show();
      $("#a2").hide();
    }else if(!$.isNumeric(clothing)){
      $("#a2").show();
      $("#a1").hide();
    }else{
      $("#a1").hide();
      $("#a2").hide();
    }
    if(laundry==''){
      $("#b1").show();
      $("#b2").hide();
    }else if(!$.isNumeric(laundry)){
      $("#b2").show();
      $("#b1").hide();
    }else{
      $("#b1").hide();
      $("#b2").hide();
    }
    if(rice==''){
      $("#c1").show();
      $("#c2").hide();
    }else if(!$.isNumeric(rice)){
      $("#c2").show();
      $("#c1").hide();
    }else{
      $("#c1").hide();
      $("#c2").hide();
    }
  }
  if(position!=''&&status!=''&&rate!=''&&$.isNumeric( rate )&&effecdate!=''){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/PositionRates/newRate",
    data: {
      position_id: position,
      status_id: status,
      rate: rate,
      effecdate : effecdate
    },
    cache: false,
    success: function(res)
    {
          // console.log(res);
          if(res=='Failed'){
            swal("Oops!", "An Error Occured while processing data.", "error");
          }else{
//------------------ALLOWANCE-------------------
if ($("#allowancediv").is(':visible')){

  if(clothing!=''&&laundry!=''&&rice!=''&&$.isNumeric(clothing)&&$.isNumeric(laundry)&&$.isNumeric(rice)){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>index.php/PositionRates/insertNewAllowance",
      data: {
        position: position,
        status:status,
        clothing:clothing,
        laundry:laundry,
        rice:rice
      },
      cache: false,
      success: function(result)
      {
        if(result=='Success'){
          location.reload();
        }else{
         $(".growl2").trigger('select');
       }
     }
   });
  }
}else{
  location.reload();
}
}
}
//------------------ALLOWANCE-------------------
});
 }
});
</script>
<script type="text/javascript">
  $('#position').on('change', function(){
   var position_id = $('#position').val();
   $('#status').html('');
   $('#status').append('<option value="">--</option>');
   $.each(<?php echo json_encode($positions);?>,function (i,elem){
    if(elem.pos_id==position_id){
      var stats = elem.status;
      var stat = stats.split(",");
      for(var i=0;i<stat.length;i++){
        var newstat = stat[i].split("|");
        // alert(newstat);
        $('#status').append('<option value="'+newstat[0]+'">'+newstat[1]+'</option>');
      }
      
      
    }
  })

 });


  $('#department').on('change', function(){
   var department_id = $('#department').val();
   $('#position').html('');
   $('#position').append('<option value="">--</option>');
   $.each(<?php echo json_encode($positions);?>,function (i,elem){
    if(elem.dep_id==department_id){
      // console.log(elem);
      var position = elem.pos_details;
      var pos_id = elem.pos_id;
      $('#position').append('<option value="'+pos_id+'">'+position+'</option>');

    }
  })

 });
</script>
<script type="text/javascript">
  function setActiveness(id){

    if ($('#id-'+id).is(":checked")){
      // alert("Yes - "+id);
      var active = 1;
    }else{
      // alert("No - "+id);
      var active = 0;
    }

    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/PositionRates/updateActiveRate",
      data: {
        rate_id: id,
        activevalue: active
      },
      cache: false,
      success: function(res)
      {
        // alert(res);
          // console.log(res);
          if(res=='Failed'){
            swal("Oops!", "An Error Occured while processing data.", "error");
          }
        }
      });
  }
</script>
<script type="text/javascript">
  $("#rate").keyup(function(){
    $("#dbrates").html('');
    var ratevalue = $(this).val();

    var position_id = $("#position").val();
    var status_id = $("#status").val();
    if(position_id==''){
      $("#pn").show();
    }else{
      $("#pn").hide();
    }
    if(status_id==''){
      $("#sn").show();
    }else{
      $("#sn").hide();
    }
    if(position_id!=''&&status_id!=''){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/PositionRates/getSimilarRateValue",
        data: {
          position_id : position_id,
          status_id : status_id,
          ratevalue: ratevalue
        },
        cache: false,
        success: function(res)
        {
          // alert(position_id+" "+status_id+" "+res);
          // // console.log(res);

          if(res=='Empty'){
          }else{

            $.each(JSON.parse(res),function (i,elem){
              // alert(elem.rate);
              $("#dbrates").append("<option data-value='"+elem.rate_id+"' value='"+parseFloat(elem.rate)+"'></option>");
            });
          }

        }
      });
      
    }else{
      $(this).val('');
    }


  });
</script>
<script type="text/javascript">
  $('#rate').on('input', function() {
    var userText = $(this).val();

    $("#dbrates").find("option").each(function() {
      if ($(this).val() == userText) {
        $("#addnewrate").hide();
        $("#reactivaterate").show();
      }else{
       $("#addnewrate").show();
       $("#reactivaterate").hide();
     }
   })
  }) 
</script>
<script type="text/javascript">
  $("#reactivaterate").click(function(){
    var position = $("#position").val();
    var status = $("#status").val();
    var rate = $("#rate").val();
    var clothing = $("#clothing").val();
    var laundry = $("#laundry").val();
    var rice = $("#rice").val();
    if(position==''){
      $("#pn").show();
    }else{
      $("#pn").hide();
    }
    if(status==''){
      $("#sn").show();
    }else{
      $("#sn").hide();
    }
    if(rate==''){
      $("#rn").show();
    }else if(!$.isNumeric( rate )){
      $('#rn2').show();
    }else{
      $("#rn").hide();
      $("#rn2").hide();
    }

    if($("#allowancediv").is(':visible')){
     if(clothing==''){
      $("#a1").show();
      $("#a2").hide();
    }else if(!$.isNumeric(clothing)){
      $("#a2").show();
      $("#a1").hide();
    }else{
      $("#a1").hide();
      $("#a2").hide();
    }
    if(laundry==''){
      $("#b1").show();
      $("#b2").hide();
    }else if(!$.isNumeric(laundry)){
      $("#b2").show();
      $("#b1").hide();
    }else{
      $("#b1").hide();
      $("#b2").hide();
    }
    if(rice==''){
      $("#c1").show();
      $("#c2").hide();
    }else if(!$.isNumeric(rice)){
      $("#c2").show();
      $("#c1").hide();
    }else{
      $("#c1").hide();
      $("#c2").hide();
    }
  }
  if(position!=''&&status!=''&&rate!=''&&$.isNumeric( rate )){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/PositionRates/activateExistingRate",
    data: {
      position_id: position,
      status_id: status,
      rate: rate
    },
    cache: false,
    success: function(res)
    {
          // console.log(res);
          if(res=='Failed'){
            swal("Oops!", "An Error Occured while processing data.", "error");
          }else{
           //------------------ALLOWANCE-------------------
           if ($("#allowancediv").is(':visible')){

            if(clothing!=''&&laundry!=''&&rice!=''&&$.isNumeric(clothing)&&$.isNumeric(laundry)&&$.isNumeric(rice)){
              $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/PositionRates/insertNewAllowance",
                data: {
                  position: position,
                  status:status,
                  clothing:clothing,
                  laundry:laundry,
                  rice:rice
                },
                cache: false,
                success: function(result)
                {
                  if(result=='Success'){
                    location.reload();
                  }else{
                   $(".growl2").trigger('select');
                 }
               }
             });
            }
          }else{
            location.reload();
          }
        }
         // alert(res);
       }
     });
 }
});
</script>
<script type="text/javascript">
  $("#allowance").click(function(){
    if ($("#allowancediv").is(':visible') ){
     $("#allowancediv").hide();
   }else{
    $("#allowancediv").show();
  }
});
  $('.bs-example-modal-sm').on('hidden.bs.modal', function () {
   $("#allowancediv").hide();
 });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>