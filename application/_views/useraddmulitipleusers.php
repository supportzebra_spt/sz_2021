<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

 

         <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.form.js"></script>
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-gradient-9">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
	

    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
				<ul id="sidebar-menu">
				</ul>
			</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
<div class="form-group">
	

 	

<div >
<a href="<?php echo base_url() ."index.php/user"; ?>"><button class="btn btn-xs btn-warning" id=' '> Back</button></a>
<a href="<?php echo base_url()."assets/files/Template-User.xlsx"; ?>"><button class="btn btn-xs btn-primary" > Template</button></a>
	<button class="btn btn-xs btn-info" id='btnViewUploadUser'  disabled> View</button>
<button class="btn btn-xs btn-success" id='btnSaveMultipleUserz' disabled> Save</button>
</div>
<br>
<form class="uploadform" method="post" action="do_upload" enctype="multipart/form-data">
<input type="file" multiple="multiple" id="userFile" name="userFile"/>
<input type="submit" value="Upload"  class="btn btn-xs btn-primary" id='submitbtn' disabled/>
</form>

<div class="example-box-wrapper">
<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
<thead>
<tr>
    <th>First Name</th>
    <th>Last Name</th>
    <th>User Level</th>
    <th>UserName</th>
    <th>Password</th>
</tr>
</thead>

<tfoot>
<tr>
     <th>First Name</th>
    <th>Last Name</th>
    <th>User Level</th>
    <th>UserName</th>
    <th>Password</th>
   
 
</tr>
</tfoot>

<tbody >
 
 
</tbody>
</table>
</div>

</div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
	    $.ajax({
		type: "POST",
		url: "headerLeft" ,
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
		});	
	$.ajax({
		type: "POST",
		url: "sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		  
		}
		});

  });	
</script>
      <script type="text/javascript" >
            $(document).ready(function() {
                $('#userFile').change(function() {
					var file = $(this).val();
					 var start = file.lastIndexOf(".");
					var res = file.substring(start+1);
					var start1 = file.split('\\');
					var res1 = file.substring(start1+1);
					
					
					 
				
						if(res == "xlsx"){
								if(start1[2] == "Template-User.xlsx"){
									
								$(this).css('color','black');
						
								$('#submitbtn').trigger('submit');
								$("#submitbtn").attr('disabled',false);
								}else{
									swal(
								  'INVALID FILE',
								  'Please use the template for the file upload.',
								  'error'
								)		
								}
						}else{
								$("#submitbtn").attr('disabled',true);
								$(this).css('color','red');
					
								swal(
								  'FILE TYPE ERROR',
								  'The file you uploaded is invalid! Please use the template for the file upload.',
								  'error'
								)	
					}
				 
					
					
					
				});
                $('#submitbtn').click(function() {
						$("#viewimage").html('');
                    $("#viewimage").html('<img src="img/loading.gif" />');
                    $(".uploadform").ajaxForm({
                        target: '#viewimage'
                    }).submit(); 
					
					$("#btnViewUploadUser").attr('disabled',false);

				 	swal(
					  'SUCCESS',
					  'Successfully uploaded!',
					  'success'
						)
						
				 
                });
				
				
            });
        </script> 
<script>
  $(function(){

	  $('#btnSaveMultipleUserz').click(function(){
		  var chik = $('#uzers:checked').val();
	  var selected_value = []; // initialize empty array 
    $("#uzers:checked").each(function(){
        selected_value.push($(this).val());
    });
	 // console.log(selected_value);
	  var dataString = 'selected_value='+ selected_value;
	 	$.ajax({
type: "POST",
url: "SaveuserUploadxls",
data: dataString,
 cache: false,
success: function(result)
{
	alert(result);
	
		}
		
		});
	  });
	  $('#btnViewUploadUser').click(function(){
		  $(this).attr('disabled','disabled');
		  $('#btnSaveMultipleUserz').attr('disabled',false);
 	$.ajax({
type: "POST",
url: "userUploadxls",
 cache: false,
success: function(result)
{

var obj = jQuery.parseJSON(result);
var tbody = "<tbody>";
for(var key in obj){
	  var value = obj[key];     
	 
	tbody += "<tr><td hidden><input type='checkbox' name='uzers[]' id='uzers' value='"+value.fname+"|"+value.lname+"|"+value.role+"|"+value.uname+"|"+value.pw+"'checked></td><td>"+value.fname+"</td><td>"+value.lname+"</td><td>"+value.role+"</td><td>"+value.uname+"</td><td>"+value.pw+"</td></tr>";
}
	tbody += "</tbody>";
	$("#datatable-responsive").append(tbody);      

}
});
});
 
 
 


  });
  
  </script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>