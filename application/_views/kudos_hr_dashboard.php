<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
        .hidden{
            visibility: hidden;
        }
        .seen{
            visibility: visible;
        }

    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- jQueryUI Autocomplete -->



    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

    <!--<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>-->

    <!-- High Charts-->

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/themes/dark-unica.js"></script>

<script type="text/javascript">
$(document).ready(function() {

        chart = Highcharts.chart('chart_0', {
        chart: {
            inverted: true,
            polar: false,
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Kudos Count by Campaigns of <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
        },

        xAxis: {
            type: 'category'
        },

        plotOptions: {
            series: {
                dataLabels: {
                enabled: true,
                },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            name: 'Kudos Count',
            colorByPoint: true,
            data: [

                <?php foreach($campaign_kcount as $kc){ ?>
                {
                    name: '<?php echo ucwords($kc->campaign);?>',
                    y: <?php echo ucwords($kc->kudos_count);?>,
                    drilldown: '<?php echo $kc->campaign;?>'
                },
                <?php }?>
            ]
        }],

        drilldown: {
            series: [
                    <?php foreach($campaign_kcount as $k){ ?>
                    {
                        type: 'column',
                        name: '<?php echo ucwords($k->campaign);?>',
                        id: '<?php echo ucwords($k->campaign);?>',
                        data: [ <?php foreach ($drilldown3 as $d){
                            if($k->campaign==$d->campaign){?>
                                ['<?php echo $d->ambassador?>', <?php echo $d->kudos_count?>],
                        <?php }}?>
                            ]
                        },
                  <?php }?>
            ]
        }


    });
    Highcharts.setOptions(Highcharts.theme);
    var chart = Highcharts.chart('chart_1', {
        chart: {
            inverted: true,
            polar: false,
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Kudos Count of <?php date_default_timezone_set('Asia/Manila'); echo date("Y");?>'
        },

        xAxis: {
            type: 'category'
        },

        plotOptions: {
            series: {
                dataLabels: {
                enabled: true,
                },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            name: 'Kudos Count',
            colorByPoint: true,
            data: [

                <?php foreach($k_count as $kc){ ?>
                {
                    name: '<?php echo ucwords($kc->month);?>',
                    y: <?php echo ucwords($kc->kudos_count);?>,
                    drilldown: '<?php echo $kc->month;?>'
                },
                <?php }?>
            ]
        }],

        drilldown: {
            series: [
                    <?php foreach($k_count as $k){ ?>
                    {
                        type: 'column',
                        name: '<?php echo ($k->month);?>',
                        id: '<?php echo ($k->month);?>',
                        data: [ <?php foreach ($drilldown2 as $d){
                            if($k->month==$d->month){?>
                                ['<?php echo $d->ambassador?>', <?php echo $d->kudos_count?>],
                        <?php }}?>
                            ]
                        },
                  <?php }?>
            ]
        }


    });
    Highcharts.setOptions(Highcharts.theme);
    Highcharts.chart('chart_2', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Top 5 Ambassadors for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
            }
        },
        series: [{
            type: 'pie',
            name: 'Kudos Count',
            data: [

                <?php foreach($top as $t){ ?>
                {
                    name: '<?php echo $t->ambassador;?>',
                    y: <?php echo $t->kudos_count;?>
                },
                <?php }?>
            ]
        }]
    });
    Highcharts.setOptions(Highcharts.theme);
    Highcharts.chart('chart_3', {
        chart: {
            inverted: true,
            polar: false
        },
        title: {
            text: 'Kudos Count of <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
        },
        credits: {
            enabled: false
        },

        xAxis: {
            categories: [<?php foreach($all as $a){ echo "'";echo $a->ambassador; echo "', "; }?>]
        },

        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php foreach($all as $a){ echo $a->kudos_count; echo ","; }?>],
            showInLegend: false
        }]

    });
    Highcharts.setOptions(Highcharts.theme);
    Highcharts.chart('chart_4', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Top 5 Campaigns for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Kudos Count',
            data: [

                <?php foreach($top_campaigns as $t){ ?>
                {
                    name: '<?php echo ucwords($t->campaign);?>',
                    y: <?php echo ucwords($t->kudos_count);?>,
                    drilldown: '<?php echo $t->campaign;?>'
                },
                <?php }?>
            ]
        }],
        drilldown: {
            series: [
                    <?php foreach($top_campaigns as $t){ ?>
                    {
                        name: '<?php echo ucwords($t->campaign);?>',
                        id: '<?php echo ucwords($t->campaign);?>',
                        data: [ <?php foreach ($drilldown as $d){
                            if($t->campaign==$d->campaign){?>
                                ['<?php echo $d->ambassador?>', <?php echo $d->kudos_count?>],
                        <?php }}?>
                            ]
                        },
                  <?php }?>
            ]
        }
    });
});
</script>

<?php echo $drilldown['campaign']; ?>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>


    <!-- FORM MASKS-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>
    <script type="text/javascript">
        /* Input masks */

        $(function() { "use strict";
            $(".input-mask").inputmask();
        });

    </script>


    <script type="text/javascript">
        $(window).load(function(){
           setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
       });
   </script>



   <!-- jQueryUI Datepicker -->

   <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
   <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

   <script type="text/javascript">
    $(document).ready(function(){
        $('.datepick').each(function(){
            $(this).datepicker();
        });
    });
    </script>

   <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

   <!-- Table to Excel -->
   <script type="text/javascript" src="<?php echo base_url();?>assets/table_export/jquery.table2excel.js" ></script>

  <!-- Parsley -->
   <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>
   <script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true,
            "order": [[ 6, "desc" ]]
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>



</head>


<body>
    <div id="sb-site">


        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div id="page-wrapper">
            <div id="page-header" class="bg-black">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                    <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                </div>
                <div id="header-logo" class="logo-bg">
                 <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                 <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id='headerLeft'>
            </div>



        </div>
        <div id="page-sidebar">
            <div class="scroll-sidebar">


                <ul id="sidebar-menu">
                </ul>
            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content">

                <div class="container">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-6">
                                
                                <div id="chart_1"></div>
                                <br>
                                <div class="form-group" style="display: none;">
                                    <div>
                                        <div class="clearfix row">
                                            <label for="" class="col-sm-3 control-label">Date Range Picker</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="fromDate" id="fromDate" placeholder="From date..." class="float-left form-control datepick" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="toDate" id="toDate" placeholder="To date..." class="float-left form-control datepick" required>
                                            </div>
                                            <div class="col-sm-2">
                                                <input class="btn btn-primary" type="button" name="datePicked" id="datePicked" value="Generate" style="background-color: #3498db; border-color: #3498db;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="chart_3"></div>
                                <br>
                                <div class="form-group" style="display: none;">
                                    <div>
                                        <div class="clearfix row">
                                            <label for="" class="col-sm-3 control-label">Date Range Picker</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="fromDate" id="fromDate3" placeholder="From date..." class="float-left form-control datepick" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="toDate" id="toDate3" placeholder="To date..." class="float-left form-control datepick" required>
                                            </div>
                                            <div class="col-sm-2">
                                                <input class="btn btn-primary" type="button" name="datePicked" id="datePicked3" value="Generate" style="background-color: #3498db; border-color: #3498db;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div id="chart_2"></div>
                                <br>
                                
                            </div>
                            <div class="col-md-6">
                                <div id="chart_4"></div>
                                <br>
                                
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div id="chart_0"></div>
                                <br>
                                <div class="form-group" style="display: none;">
                                    <div>
                                        <div class="clearfix row">
                                            <div class="col-sm-4">
                                            <select class="form-control" id="addCampaign2" required="true" data-trigger="change">
                                                <option selected="selected" value="">Campaign</option>
                                                <?php 
                                                    foreach($acc as $row)
                                                    {
                                                        echo '<option value="'.$row->acc_id.'">'.$row->acc_name.'</option>';
                                                    }
                                                ?>
                                            </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="fromDate" id="fromDate2" placeholder="From date..." class="float-left mrg10R form-control datepick" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="toDate" id="toDate2" placeholder="To date..." class="float-left form-control datepick" required>
                                            </div>
                                            <div class="col-sm-2">
                                                <button class="btn btn-primary" id="datePicked2" style="background-color: #3498db; border-color: #3498db;">Generate</button>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
</div>
</div>
</body>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/menu.js"></script>
<script>
    $(function(){
        $.ajax({
          type: "POST",
          url: location.protocol + '//' + location.hostname+"/Glenn/sz3/index.php/Templatedesign/headerLeft",
          cache: false,
          success: function(html)
          {

            $('#headerLeft').html(html);
     //alert(html);
 }
}); 
        $.ajax({
          type: "POST",
          url: location.protocol + '//' + location.hostname+"/Glenn/sz3/index.php/Templatedesign/sideBar",
          cache: false,
          success: function(html)
          {

            $('#sidebar-menu').html(html);
   // alert(html);
}
});
    }); 
</script>
<script>

    $(function(){
        $("#datePicked").click(function(){
                var fromDate = $("#fromDate").val();
                var toDate = $("#toDate").val();

                var arr_date1 = fromDate.split('-');
                var arr_date2 = toDate.split('-');

                var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

                var month1 = arr_date1[1];
                var month2 = arr_date2[1];
                var day1 = arr_date1[2];
                var day2 = arr_date2[2];

                var m1 = month1.replace(/^0+/, '');
                var m2 = month2.replace(/^0+/, '');
                var d1 = day1.replace(/^0+/, '');
                var d2 = day2.replace(/^0+/, '');

                var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
                var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

                var dataString = "fromDate="+fromDate+"&toDate="+toDate;

               $.ajax({
                //url of the function
                    url: '<?php echo base_url(); ?>index.php/Kudos/getDate',
                    type: 'POST',
                    dataType: 'json',
                    data:  dataString,
                    success:function(data)
                    {
                        var month = data.map(function (value) {
                            return value.month;
                        });
                        var kudos_count = data.map(function (value) {
                            return parseInt(value.kudos_count);
                        });
                        console.log(month);
                        console.log(kudos_count);
                        chart = Highcharts.chart('chart_1', {
                            chart: {
                                inverted: true,
                                polar: false,
                            },
                            credits: {
                                enabled: false
                            },
                            title: {
                                text: 'Kudos Count '+fromDate2+' to '+toDate2
                            },

                            xAxis: {
                                categories: month,
                            },

                            plotOptions: {
                                series: {
                                    dataLabels: {
                                    enabled: true,
                                    },
                                    //colors: ['#3399ff']
                                }
                            },

                            series: [{
                                type: 'column',
                                colorByPoint: true,
                                data: kudos_count,
                                showInLegend: false,
                            }],


                        });
                        Highcharts.setOptions(Highcharts.theme);
                    }
                });
        });

        $("#datePicked2").click(function(){
                var fromDate = $("#fromDate2").val();
                var toDate = $("#toDate2").val();
                var campaign = $("#addCampaign2").val();
                var campaign_name = $("#addCampaign2 option[value='"+campaign+"']").text();

                var arr_date1 = fromDate.split('-');
                var arr_date2 = toDate.split('-');

                var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

                var month1 = arr_date1[1];
                var month2 = arr_date2[1];
                var day1 = arr_date1[2];
                var day2 = arr_date2[2];

                var m1 = month1.replace(/^0+/, '');
                var m2 = month2.replace(/^0+/, '');
                var d1 = day1.replace(/^0+/, '');
                var d2 = day2.replace(/^0+/, '');

                var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
                var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

                var dataString = "fromDate="+fromDate+"&toDate="+toDate+"&campaign="+campaign;

                //alert(dataString);

                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/Kudos/getDate2',
                    type: 'POST',
                    dataType: 'json',
                    data:  dataString,
                    success:function(data)
                    {
                        
                        var month = data.map(function (value) {
                            return value.month;
                        });
                        var kudos_count = data.map(function (value) {
                            return parseInt(value.kudos_count);
                        });
                        var c = data.map(function (value) {
                            return parseInt(value.campaign);
                        });

                        console.log(month);
                        console.log(kudos_count);
                        $("#datePicked4").trigger("click");
                        chart = Highcharts.chart('chart_0', {
                            chart: {
                                inverted: true,
                                polar: false,
                            },
                            credits: {
                                enabled: false
                            },
                            title: {
                                text: 'Kudos Count '+fromDate2+' to '+toDate2+' of '+campaign_name
                            },

                            xAxis: {
                                categories: month,
                            },

                            plotOptions: {
                                series: {
                                    dataLabels: {
                                    enabled: true,
                                    },
                                    //colors: ['#3399ff']
                                }
                            },

                            series: [{
                                type: 'column',
                                colorByPoint: true,
                                data: kudos_count,
                                showInLegend: false,
                            }],


                        });
                        Highcharts.setOptions(Highcharts.theme);
                    }
                });
        });

        $("#datePicked3").click(function(){
                var fromDate = $("#fromDate3").val();
                var toDate = $("#toDate3").val();

                var arr_date1 = fromDate.split('-');
                var arr_date2 = toDate.split('-');

                var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

                var month1 = arr_date1[1];
                var month2 = arr_date2[1];
                var day1 = arr_date1[2];
                var day2 = arr_date2[2];

                var m1 = month1.replace(/^0+/, '');
                var m2 = month2.replace(/^0+/, '');
                var d1 = day1.replace(/^0+/, '');
                var d2 = day2.replace(/^0+/, '');

                var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
                var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

                var dataString = "fromDate="+fromDate+"&toDate="+toDate;

               $.ajax({
                //url of the function
                    url: '<?php echo base_url(); ?>index.php/Kudos/getDate3',
                    type: 'POST',
                    dataType: 'json',
                    data:  dataString,
                    success:function(data)
                    {
                        var ambassador = data.map(function (value) {
                            return value.ambassador;
                        });
                        var kudos_count = data.map(function (value) {
                            return parseInt(value.kudos_count);
                        });
                        console.log(ambassador);
                        console.log(kudos_count);
                        chart = Highcharts.chart('chart_3', {
                            chart: {
                                inverted: true,
                                polar: false,
                            },
                            credits: {
                                enabled: false
                            },
                            title: {
                                text: 'Kudos Count '+fromDate2+' to '+toDate2
                            },

                            xAxis: {
                                categories: ambassador,
                            },

                            plotOptions: {
                                series: {
                                    dataLabels: {
                                    enabled: true,
                                    },
                                    //colors: ['#3399ff']
                                }
                            },

                            series: [{
                                type: 'column',
                                colorByPoint: true,
                                data: kudos_count,
                                showInLegend: false,
                            }],


                        });
                        Highcharts.setOptions(Highcharts.theme);
                    }
                });
        });

        $("#datePicked4").click(function(){
                var fromDate = $("#fromDate2").val();
                var toDate = $("#toDate2").val();
                var campaign = $("#addCampaign2").val();
                var campaign_name = $("#addCampaign2 option[value='"+campaign+"']").text();

                var dataString = "fromDate="+fromDate+"&toDate="+toDate+"&campaign="+campaign;

                //alert(dataString);

                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/Kudos/getDate4',
                    type: 'POST',
                    data:  dataString,
                    success:function(data)
                    {
                        $("#table_report1 tbody").html(data);
                        $("#showTable1").show();
                        console.log(data);
                        //alert(JSON.stringify(data));
                    }
                });
        });

        $("#datePicked5").click(function(){
                var fromDate = $("#fromDate5").val();
                var toDate = $("#toDate5").val();

                var dataString = "fromDate="+fromDate+"&toDate="+toDate;

               $.ajax({
                //url of the function
                    url: '<?php echo base_url(); ?>index.php/Kudos/getDate5',
                    type: 'POST',
                    data:  dataString,
                    success:function(data)
                    {
                        $("#table_report2 tbody").html(data);
                        $("#showTable2").show();
                    }
                });
        });

        $("#datePicked6").click(function(){
                var fromDate = $("#fromDate6").val();
                var toDate = $("#toDate6").val();

                var dataString = "fromDate="+fromDate+"&toDate="+toDate;

               $.ajax({
                //url of the function
                    url: '<?php echo base_url(); ?>index.php/Kudos/getDate6',
                    type: 'POST',
                    data:  dataString,
                    success:function(data)
                    {
                        $("#table_report3 tbody").html(data);
                        $("#showTable3").show();
                    }
                });
        });


        
    });
</script>
<script>
    $(function(){
        "use strict";
        var obj = JSON.parse(<?php echo "'".json_encode($name)."'"; ?>);
        var all = obj.map(function (value) {
            return value.name;
        });
        $("#addAmbassadorName2").autocomplete({source: all});
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
 


<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>