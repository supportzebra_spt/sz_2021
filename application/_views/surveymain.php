<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/lockscreen-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
<head>
  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    .grow { transition: all .2s ease-in-out; }
    .grow:hover { transform: scale(1.5); }
    @font-face {
      font-family: customfont;
      src: url('<?php echo base_url();?>assets/font/Aileron-Bold.otf');
    }
    @font-face {
      font-family: customfonttwo;
      src: url('<?php echo base_url();?>assets/font/Aileron-SemiBold.otf');
    }
    h3.step {
      background:#888;
      border-radius: 0.8em;
      -moz-border-radius: 0.8em;
      -webkit-border-radius: 0.8em;
      color: #111;
      display: inline-block;
      font-weight: bold;
      line-height: 1.6em;
      margin-right: 5px;
      text-align: center;
      width: 1.6em; 
    }
  </style>
  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Lockscreen page 1 </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>
  <script type="text/javascript">
    $(window).load(function(){
      $('#15fivemodal').modal({
        backdrop: 'static',
        keyboard: true,
        escape:true, 
        show: true
      })
      setTimeout(function() {
        $('#loading').fadeOut( 400, "linear" );
      }, 300);
    });
  </script>



</head>
<body>
  <div id="loading">
    <div class="spinner">
      <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div>
    </div>
  </div>

  <style>
    html,body {
      height: 100%;
    }
    body {
      overflow: hidden;
      background: rgba(0,0,0, 0.5)
    }
  </style>

  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/wow/wow.js"></script>
  <script type="text/javascript">
    /* WOW animations */

    wow = new WOW({
      animateClass: 'animated',
      offset: 100
    });
    wow.init();//
  </script>

  <img src="<?php echo base_url();?>assets/image-resources/blurred-bg/blurred-bg-1.jpg" class="login-img wow fadeIn" alt="">

  <div class="modal fade" id='15fivemodal' style='top:80px' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" style='width:60%'>
      <div class="modal-content">
        <div class="modal-header pad25A" id='15fivemodalheader' style="background: linear-gradient(#aaa,white);border:none !important">
          <h2 class='text-center' id='15fivemodalheadertext' style='font-family:customfont;text-shadow: 0 0 10px white;'></h2>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <!-- <span class="mrg10A">Awful</span> -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                <button class="btn mrg10A grow" id='15fivebtn1' onclick="changequestion(1)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/05static.png" class='emoji'></button><br><br>
                <hr>
                <h3 id='15fivesq1' class='step'>1</h3>
              </div>
              <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                <button class="btn mrg10A grow" id='15fivebtn2' onclick="changequestion(2)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/16static.png" class='emoji'></button><br><br>
                <hr>
                <h3 id='15fivesq2' class='step'>2</h3>
              </div>
              <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                <button class="btn mrg10A grow" id='15fivebtn3' onclick="changequestion(3)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/06static.png" class='emoji'></button><br><br>
                <hr>
                <h3 id='15fivesq3' class='step'>3</h3>
              </div>
              <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                <button class="btn mrg10A grow" id='15fivebtn4' onclick="changequestion(4)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/17static.png" class='emoji'></button><br><br>
                <hr>
                <h3 id='15fivesq4' class='step'>4</h3>
              </div>
              <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                <button class="btn mrg10A grow" id='15fivebtn5' onclick="changequestion(5)" style='background-color: Transparent;outline:none'><img width="100%" src="<?php echo base_url();?>assets/images/emoji/Owls/23static.png" class='emoji'></button><br><br>
                <hr>
                <h3 id='15fivesq5' class='step'>5</h3>
              </div>
              <div class="col-md-1"></div>
            </div>
            <!-- <span class="mrg10A">Amazing!</span> -->
          </div>
          <br>
          <div class='modal-footer pad25A' id='15fivequestiondiv' style="display:none;">
            <h4 class='text-center text-bold font-size-18' style='font-family:customfonttwo;text-shadow: 1px 1px  black;color:white' id='15fivequestion'></h4>
            <br>
            <div class="row">
              <div class="col-md-11">
                <div class="form-group">
                  <input type="text" class='form-control' name="" placeholder='Let us hear your feedback ...'>
                </div>
              </div>
              <div class="col-md-1">
                <button class='btn' style='background:white'><i class="glyph-icon icon-send"></i></button>
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>


  <!-- JS Demo -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
  <script type="text/javascript">
    $('#15fivemodal').on('hide.bs.modal', function(e) {
      $(this).removeData("modal").modal({backdrop: 'static', keyboard: false});
    });
  </script>
  <script type="text/javascript">
    $(".emoji").hover( function () {    
      var src = $(this).attr('src');
      var newsrc = src.replace('static.png', '.gif');
      $(this).attr('src',newsrc);
    }, function() {
      var src = $(this).attr('src');
      var newsrc = src.replace('.gif', 'static.png');
    // alert($(this).attr('data-active'));
    if($(this).attr('data-active')=='true'){
      // alert('selected');
    }else{

      $(this).attr('src', newsrc);
    }
    // $(this).attr('src', newsrc);
  });
</script>
<script type="text/javascript">
  function changequestion(sequence){
    var colors = ["#ff6161", "#56b1bf", "#ff7f47","#ec7498","#ffd85d"]; 
    $("#15fivequestiondiv").css({'background-color':colors[sequence-1],'background-image':'url(<?php echo base_url();?>assets/images/emoji/diagmonds.png)'})
    $("#15fivemodalheader").css("background","linear-gradient("+colors[sequence-1]+",white)")
    var currentsrc = $("#15fivebtn"+sequence).children('img').attr('src');  
    $("button[id*='15fivebtn'] img").each(function(){
      var src = $(this).attr('src');
      var newsrc = src.replace('.gif', 'static.png');
      $(this).attr('src', newsrc);
      $(this).css('filter','grayscale(90%)');
    });
    for(var x=1;x<=5;x++){
      $("#15fivesq"+x).css('background','#888');
    }
    $("#15fivesq"+sequence).css('background',colors[sequence-1]);
    $("button[id*='15fivebtn'] img").attr('data-active','false');
    $("#15fivebtn"+sequence).children('img').attr('data-active','true');
    $("#15fivebtn"+sequence).children('img').attr('src',currentsrc);
    $("#15fivebtn"+sequence).children('img').css('filter','');
    $("button[id*='15fivebtn']").css('transform','');
    $("#15fivebtn"+sequence).css('transform', "scale(1.5)"); 
    $("#15fivequestiondiv").show(1000);
    $("#15fivequestion").hide();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/getSequenceQuestion",
      data: {
        sequence:sequence
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        $("#15fivequestion").html(res).fadeIn();
      }
    });
    
  }
</script>
<script type="text/javascript">
  $('#15fivemodal').on('show.bs.modal', function() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/getHeaderQuestion",
      cache: false,
      success: function(res)
      {
        res = res.trim();
        $("#15fivemodalheadertext").html(res);
      }
    });
  })
</script>
</body>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/lockscreen-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
</html>