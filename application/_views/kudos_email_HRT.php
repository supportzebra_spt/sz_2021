<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>SupportZebra Payslip</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

<style>
 

h1{font-size:35px;}
h2{font-size:30px;}
h3{font-size:25px;}
p {font-size:16px;}

 .quotemark {
    position: relative;
    top: -12px;
    padding-right: 10px;
}

blockquote {
    border-left: 10px solid #888888;
    margin: 30px 10px;
    padding: 10px 30px;
}
blockquote:before {
  color: #ccc;
  line-height: 0.1em;
  margin-right: 0.25em;
  vertical-align: -0.4em;
}
blockquote h1, h2, h3, p{
     display: inline;
    font-family: serif;
    font-size: large;
}
blockquote footer {
	font-size:18px;
	text-align:right;
	color:transparent;
}

.container {
	max-height:800px;
	margin: 10% auto auto auto;
}
.quotebox {
	background:#fff;
	max-width:500px;
	min-height:100px;
	border:1px solid #222;
	border-radius:5px;
	margin:10px;
}

.quotebutton {
	display:inline-block;
	padding:6px 12px;
}
	#quotesource{
     margin-top: 20px;
    color: black;
    font-size: medium;
    font-family: initial;
	}

.btn {margin-bottom:10px}

.social-icons {
	padding:6px 12px;
	display:inline-block;
	
}
.social-icons .fa {
	font-size: 1.8em;
	width: 40px;
	height: 40px;
	line-height: 40px;
	text-align: center;
	color: #FFF;
	-webkit-transition: all 0.3s ease-in-out;
	-moz-transition: all 0.3s ease-in-out;
	-ms-transition: all 0.3s ease-in-out;
	-o-transition: all 0.3s ease-in-out;
	transition: all 0.3s ease-in-out;
}
.social-icons.icon-rounded .fa{
	border-radius:5px;
}
.social-icons .fa:hover, .social-icons .fa:active {
	color: #FFF;
	-webkit-box-shadow: 1px 1px 3px #333;
	-moz-box-shadow: 1px 1px 3px #333;
	box-shadow: 1px 1px 3px #333; 
}
.social-icons .fa-twitter {
	background-color:#490A3D; 
	border-color:none;} 

.btn-primary {
	width: 100px;
	height: 40px;
	border:none;
	background:#490A3D;
	}

@media only screen and (max-width: 768px){  
    #quotetext {font-size:25px; }
	.container {	margin-right:20px;}
    }
@media only screen and (max-width: 320px){  
    #quotetext {font-size:18px; }

    }
	
.myButton {
	-moz-box-shadow: 0px 10px 14px -7px #276873;
	-webkit-box-shadow: 0px 10px 14px -7px #276873;
	box-shadow: 0px 10px 14px -7px #276873;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
	background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
	background-color:#599bb3;
	-moz-border-radius:8px;
	-webkit-border-radius:8px;
	border-radius:8px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
 	font-weight:bold;
    padding: 2px 8px;
	text-decoration:none;
	text-shadow:0px 1px 0px #3d768a;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}
.myButton:active {
	position:relative;
	top:1px;
}
</style>
</head>

<body>
<div >
	<div class="row">
		<div style="font-size:26px;font-weight:700;letter-spacing:-0.02em;line-height:32px;color:#41637e;font-family:sans-serif;text-align:center" align="center" id="m_-7518272001418123536emb-email-header">
   <img style="Margin-left:auto;Margin-right:auto" src="https://lh3.googleusercontent.com/NZNZzuQogtKpP32IPLfU7ycNwp-VO_a3pqnAB4Owr0sRkJdDEHENGxRulIG2eyEtLcWQdVR5NZOsStRx4zhQePBcFviP2V_F=w1366-h768-rw-no" class="CToWUd"></div>
   
   <br>
   <br>
		<div class=" ">
			
			Hi, <br><br>
			
			<?php echo $requested_by; ?> sent you a cash request. You can approve it   <a href="http://10.200.101.250/supportzebra/index.php/Kudos/kudos_approve" class="myButton">here </a>.
			<br><br>
			</div>
		<div class="quotebox">
			<div class="row">
				<div class="col-xs-12">
					<blockquote>
 						<h2 id="quotetext">" <?php	echo $note; ?></h2>
						<footer id="quotesource">- <?php	echo $requested_by; ?></footer>
					</blockquote>
				</div>
			</div>
			 
			</div>
		</div>
		<div class="col-xs-12 col-sm-3"></div>
	</div>
 </body>
</html> 