<!DOCTYPE html> 
<html  lang="en">

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
            .chosen-container-single .chosen-single{
                line-height:25px !important;
                height:25px !important;
                font-size:12px !important;
            }
            .chosen-results .active-result{

                font-size:12px !important;
            }
        </style>

        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/fontawesome/fontawesome.css"> -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">

        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });

        </script><!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/chosen/chosen.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/chosen/chosen.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/chosen/chosen-demo.js"></script>
        <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/daterangepicker/daterangepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript">
                $(function () {
                    "use strict";
                    var end = moment().format("MM/DD/YYYY");
                    var m = moment().subtract(1, 'year').format("MM/DD/YYYY");
                    $("#daterangepicker-example").daterangepicker({minDate: m, startDate: end, maxDate: end});
                    $("[name=daterangepicker_start]").attr('readonly', true);
                    $("[name=daterangepicker_end]").attr('readonly', true);
                });
        </script>
        <!-- Input switch -->

        <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/input-switch/inputswitch.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/input-switch/inputswitch.js"></script>
        <script type="text/javascript">
                /* Input switch */

                $(function () {
                    "use strict";
                    $('.input-switch').bootstrapSwitch();
                });
        </script>
        <!-- Multi select -->

        <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/multi-select/multiselect.js"></script>
        <script type="text/javascript">
                /* Multiselect inputs */

                $(function () {
                    "use strict";
                    $("#employeelist").multiSelect();
                    $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
                    $('input[type="radio"].custom-radio').uniform();
                    $(".radio span").append('<i class="glyph-icon icon-circle"></i>');
                });
        </script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/chosen/chosen.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/chosen/chosen-demo.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/uniform/uniform.js"></script>
        <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/uniform/uniform-demo.js"></script> -->
    </head>


    <body>
        <div id="sb-site">


            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <div id="page-header" class="bg-black">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>

                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">


                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div id="page-title">
                            <div class="example-box-wrapper">
                                <div class="row">
                                    <div class="col-md-4">
                                        <a class="btn" style="background:#FF9710;color:white"  href="<?php echo base_url(); ?>index.php/Schedule">
                                            <span class="glyph-icon icon-separator">
                                                <i class="glyph-icon icon-arrow-circle-left"></i>
                                            </span>
                                            <span class="button-content">
                                                Back to Schedule
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-md-8 text-right">
                                        <h2 style="font-size:25px">DTR LOG SCHEDULES</h2>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="content-box">
                                            <h3 class="content-box-header" style="background:#49AE51;color:white">
                                                <i class="glyph-icon icon-search"></i>
                                                Search for Schedules

                                            </h3>
                                            <div class="content-box-wrapper">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group" id='acc_descriptiondiv'>
                                                                    <label class='control-label'>Account Type</label>
                                                                    <select class='form-control' id='acc_description'>
                                                                        <option value='' id='removeme'>--</option>
                                                                        <option value='All'>All</option>
                                                                        <option value='Admin'>Admin</option>
                                                                        <option value='Agent'>Agent</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12" id="acc_name_area">

                                                                <div class="form-group" id='acc_namediv'>
                                                                    <label class='control-label'>Account Name</label>
                                                                    <select class='form-control' id='acc_name'></select>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-12">
                                                                <label class='control-label'>Date Range</label>
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon">
                                                                        <i class="glyph-icon icon-calendar"></i>
                                                                    </span>
                                                                    <input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control" readonly='true'>
                                                                </div>
                                                            </div>                    
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="col-md-12"  id='employeelistdiv'>
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <label class='control-label'>Employee List</label>
                                                                    <select multiple name="" id="employeelist"  class="multi-select"></select>
                                                                </div>

                                                                <div class="col-md-2"><br>
                                                                    <div class="btn-group-vertical">
                                                                        <button id="select" class='btn btn-xs' style="background:#49AE51;color:white;border:1px solid white">Select all</button>

                                                                        <button id="deselect" class='btn btn-xs' style="background:#49AE51;color:white;border:1px solid white">Deselect all</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="button-pane text-right">
                                                <!--<button class="btn btn-primary" id='generateschedule'>Display Schedule</button>-->
                                                <a href="#" class="btn" style="background:#49AE51;color:white"  id='generateschedule'>
                                                    <span class="glyph-icon icon-separator">
                                                        <i class="glyph-icon icon-desktop"></i>
                                                    </span>
                                                    <span class="button-content">
                                                        Display Schedule
                                                    </span>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="loading-spinner" id="loadingspinner" style="display:none">
                            <i style="background:#49AE51"></i>
                            <i style="background:#49AE51"></i>
                            <i style="background:#49AE51"></i>
                            <i style="background:#49AE51"></i>
                            <i style="background:#49AE51"></i>
                            <i style="background:#49AE51"></i>
                        </div>
                        <div class="container" id="page-title">

                            <div class="content-box" id="scheduleview" style="display:none;" style="background:#009183;color:white">
                                <h3 class="content-box-header bg-blue-alt"> 
                                    <i class="glyph-icon icon-list"></i>
                                    SCHEDULE LIST
                                </h3>
                                <div class="content-box-wrapper">

                                    <table class="table table-striped table-hover table-condensed table-bordered font-size-12" style="color:black !important">
                                        <thead>
                                        <th>Name</th>
                                        <th>Log In</th>
                                        <th style="width:25%">Schedule In</th>
                                        <th>Log Out</th>
                                        <th  style="width:25%">Schedule Out</th>
                                        </thead>
                                        <tbody id="tbody"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <!-- JS Demo -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>

    <script type="text/javascript">
                $(function () {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
                        cache: false,
                        success: function (html) {
                            $('#headerLeft').html(html);
                            ////alert(html);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
                        cache: false,
                        success: function (html) {
                            $('#sidebar-menu').html(html);
                            // //alert(html);
                        }
                    });
                });
    </script>
    <script type="text/javascript">
            $(function () {
                var role = "<?php echo $role; ?>";
//                //alert(role);
                if (role == 'Administrator') {
                    $("#employeelist").prop('disabled', true);
                    $("#employeelist").multiSelect('refresh');
                    $("#select").prop('disabled', true);
                    $("#deselect").prop('disabled', true);
                    $("#acc_name").prop('disabled', true);
                    $("#acc_description").on('change', function () {
                        var desc = $(this).val();
                        $("#removeme").remove();
                        if (desc == 'All') {

                            $("#employeelist").prop('disabled', true);
                            $("#employeelist").multiSelect('refresh');
                            $("#select").prop('disabled', true);
                            $("#deselect").prop('disabled', true);
                            $("#acc_name").prop('disabled', true);
                        } else {

                            $("#acc_name").prop('disabled', false);
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getAccounts",
                                data: {
                                    desc: desc
                                },
                                cache: false,
                                success: function (res) {
                                    if (res != 'Empty') {
                                        $("#acc_name").html("<option value='All'>All</option>");
                                        $.each(JSON.parse(res), function (i, elem) {
                                            // //alert(elem.rate);
                                            $("#acc_name").append("<option value='" + elem.acc_id + "'>" + elem.acc_name + "</option>");
                                        });
                                    }
                                }
                            });
                        }
                    });
                } else if (role == 'User Level 1') {
                    $("#acc_name").hide();
                    $("#acc_name_area").html("");
                    $("#employeelist").prop('disabled', true);
                    $("#employeelist").multiSelect('refresh');
                    $("#select").prop('disabled', true);
                    $("#deselect").prop('disabled', true);
                    $("#generateschedule").prop('disabled', true);
                    $("#acc_description").on('change', function () {
                        var desc = $(this).val();
                        $("#removeme").remove();
                        if (desc == 'All') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getAllTeam",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                        $("#generateschedule").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');
                                        $("#generateschedule").prop('disabled', false);
                                    }
                                }
                            });
                        } else if (desc == 'Admin') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getTeamAdminEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                        $("#generateschedule").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');
                                        $("#generateschedule").prop('disabled', false);
                                    }
                                }
                            });
                        } else if (desc == 'Agent') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getTeamAgentEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                        $("#generateschedule").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');
                                        $("#generateschedule").prop('disabled', false);
                                    }

                                }
                            });
                        } else {
                            //alert('ambot nganu');
                        }

                        $("#employeelist").prop('disabled', false);
                        $("#employeelist").multiSelect('refresh');
                        $("#select").prop('disabled', false);
                        $("#deselect").prop('disabled', false);
                    });
                } else {

                    $("#acc_name").hide();
                    $("#acc_name_area").html("");
                    $("#employeelist").prop('disabled', true);
                    $("#employeelist").multiSelect('refresh');
                    $("#select").prop('disabled', true);
                    $("#deselect").prop('disabled', true);
                    $("#generateschedule").prop('disabled', true);
                    $("#acc_description").on('change', function () {
                        var desc = $(this).val();
                        $("#removeme").remove();
                        if (desc == 'All') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getSupervisoryAllEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                        $("#generateschedule").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');
                                        $("#generateschedule").prop('disabled', false);
                                    }
                                }
                            });
                        } else if (desc == 'Admin') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getSupervisoryAdminEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                        $("#generateschedule").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');
                                        $("#generateschedule").prop('disabled', false);
                                    }
                                }
                            });
                        } else if (desc == 'Agent') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/getSupervisoryAgentEmp",
                                cache: false,
                                success: function (res) {
                                    if (res == "Empty") {
                                        $("#employeelist").prop('disabled', true);
                                        $("#employeelist").multiSelect('refresh');
                                        $("#select").prop('disabled', true);
                                        $("#deselect").prop('disabled', true);
                                        $("#generateschedule").prop('disabled', true);
                                    } else {
                                        $("#employeelist").html('');
                                        $.each(JSON.parse(res), function (i, elem) {
                                            $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});
                                        });

                                        $("#employeelist").multiSelect('refresh');
                                        $("#generateschedule").prop('disabled', false);
                                    }

                                }
                            });
                        } else {
                            //alert('ambot nganu');
                        }

                        $("#employeelist").prop('disabled', false);
                        $("#employeelist").multiSelect('refresh');
                        $("#select").prop('disabled', false);
                        $("#deselect").prop('disabled', false);
                    });
                }

            })
    </script>
    <script type="text/javascript">
            $(function () {
                $("#acc_name").on('change', function () {
                    var acc_id = $(this).val();
                    if (acc_id == 'All') {
                        $("#employeelist").prop('disabled', true);
                        $("#employeelist").multiSelect('refresh');
                        $("#select").prop('disabled', true);
                        $("#deselect").prop('disabled', true);
                    } else {
                        $("#employeelist").prop('disabled', false);
                        $("#employeelist").multiSelect('refresh');
                        $("#select").prop('disabled', false);
                        $("#deselect").prop('disabled', false);
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>index.php/Schedule/getAccountEmployee",
                            data: {
                                acc_id: acc_id
                            },
                            cache: false,
                            success: function (res) {
                                if (res != 'Empty') {
                                    $("#employeelist").html("");
                                    $.each(JSON.parse(res), function (i, elem) {
                                        // //alert(elem.rate);
                                        // $("#employeelist").append("<option value=''>Arique</option>");
                                        $('#employeelist').multiSelect('addOption', {value: elem.emp_id, text: elem.lname + ", " + elem.fname, index: 0});

                                    });
                                    $("#employeelist").multiSelect('refresh');

                                } else {

                                    $("#employeelist").html("");
                                    $("#employeelist").multiSelect('refresh');
                                }
                            }
                        });
                    }
                });
            })
    </script>
    <script type="text/javascript">
            $(function () {
                $("#acc_description").on('change', function () {
                    $("#employeelist").html("");
                    $("#employeelist").multiSelect('refresh');
                })
            })
    </script>

    <script type="text/javascript">
            $(function () {
                $('#select').click(function () {
                    $("#employeelist").multiSelect('select_all');
                });
            })
    </script>
    <script type="text/javascript">
            $(function () {

                $('#deselect').click(function () {
                    $("#employeelist").multiSelect('deselect_all');
                });
            })
    </script>
    <script type="text/javascript">
            function schedupdater(dtr_id, sched_id) {
                //alert(dtr_id + " == " + sched_id);
                if (sched_id == '') {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Schedule/schedupdaterdtr2",
                        data: {
                            dtr_id: dtr_id
                        },
                        cache: false,
                        success: function (res) {
                            // //alert(res);
                            if (res == 'Success' || res == 'Success2') {
                                $.jGrowl("Successfully Updated Schedule.", {sticky: !1, position: "top-right", theme: "bg-blue-alt"});
                            } else {
                                $.jGrowl("Problem Updating Schedule.", {sticky: !1, position: "top-right", theme: "bg-red"});
                            }
                        }
                    });
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Schedule/schedupdaterdtr",
                        data: {
                            sched_id: sched_id,
                            dtr_id: dtr_id
                        },
                        cache: false,
                        success: function (res) {
                            // //alert(res);
                            if (res == 'Success' || res == 'Success2') {
                                $.jGrowl("Successfully Updated Schedule.", {sticky: !1, position: "top-right", theme: "bg-blue-alt"});
                            } else {
                                $.jGrowl("Problem Updating Schedule.", {sticky: !1, position: "top-right", theme: "bg-red"});
                            }
                        }
                    });
                }
            }
    </script>
    <script type="text/javascript">

            $("#generateschedule").click(function () {

                $("#scheduleview").hide();
                var date = $("#daterangepicker-example").val();
                if (date == null || date == '') {
                    //alert("date empty");
                } else {

                    var dater = date.split(" - ");
                    var fromDateRaw = dater[0];
                    var fromDateRaw2 = fromDateRaw.split("/");
                    var fromDate = fromDateRaw2[2] + "-" + fromDateRaw2[0] + "-" + fromDateRaw2[1];
                    var toDateRaw = dater[1];
                    var toDateRaw2 = toDateRaw.split("/");
                    var toDate = toDateRaw2[2] + "-" + toDateRaw2[0] + "-" + toDateRaw2[1];
                    var acc_description = $("#acc_description").val();
                    var acc_name = $("#acc_name").val();
                    var employeelist = $("#employeelist").val();
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Schedule/generatedate",
                        data: {
                            fromDate: fromDate,
                            toDate: toDate
                        },
                        cache: false,
                        success: function (sched) {


                            var role = "<?php echo $role; ?>";
//                //alert(role);
                            if (role == 'Administrator') {

                                //alert("admin");
                                if (acc_description == '') {
                                    //alert("Please Choose Account Type");
                                } else if (acc_description == 'All') {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>index.php/Schedule/getSchedAllDTR",
                                        data: {
                                            fromDate: fromDate,
                                            toDate: toDate
                                        },
                                        cache: false,
                                        beforeSend: function () {
                                            $("#loadingspinner").show();
                                        },
                                        success: function (ress) {
                                            $("#loadingspinner").hide();

                                            $("#tbody").html("");
                                            $("#scheduleview").show();
                                            var res = JSON.parse(ress);
                                            var body = "";
                                            for (var i = 0; i < res.length; i++) {
                                                body += "<tr>";
                                                body += "<td>" + res[i].lname + ", " + res[i].fname + "</td>";
                                                body += "<td>" + res[i].log_in + "</td>";
                                                body += "<td>";
                                                if (res[i].availablesched != null) {
                                                    body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_in + ",this.value)'>";

                                                    body += "<option value=''>--</option>";
                                                    res[i].availablesched.forEach(function (item) {

                                                        if (item.type == 'Normal') {
                                                            body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                        } else if (item.type == 'WORD') {
                                                            body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                        } else {
                                                            body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                        }

                                                    });

                                                    body += "</select>";
                                                } else {
                                                    body += "No Schedule Available";
                                                }
                                                body += "</td>";
                                                if (res[i].log_out == null) {
                                                    body += "<td>Not Logged Out</td>";
                                                } else {
                                                    body += "<td>" + res[i].log_out + "</td>";
                                                }
                                                body += "<td>";
                                                if (res[i].availablesched != null) {
                                                    body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_out + ",this.value)'>";

                                                    body += "<option value=''>--</option>";
                                                    res[i].availablesched.forEach(function (item) {

                                                        if (item.type == 'Normal') {
                                                            body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                        } else if (item.type == 'WORD') {
                                                            body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                        } else {
                                                            body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                        }

                                                    });

                                                    body += "</select>";
                                                } else {
                                                    body += "No Schedule Available";
                                                }

                                                body += "</td>";
                                                body += "</tr>";

                                            }
                                            $("#tbody").append(body);
//                                        $(".chosen-select").chosen();
//                                        $(".chosen-search").append('<i class="glyph-icon icon-search"></i>');
//                                        $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
                                        }
                                    });
                                } else {
                                    if (acc_name == 'All') {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url(); ?>index.php/Schedule/getSchedPerTypeDTR",
                                            data: {
                                                fromDate: fromDate,
                                                toDate: toDate,
                                                acc_description: acc_description
                                            },
                                            cache: false,
                                            beforeSend: function () {
                                                $("#loadingspinner").show();
                                            },
                                            success: function (ress) {
                                                $("#loadingspinner").hide();

                                                $("#tbody").html("");
                                                $("#scheduleview").show();
                                                var res = JSON.parse(ress);
                                                var body = "";
                                                for (var i = 0; i < res.length; i++) {
                                                    body += "<tr>";
                                                    body += "<td>" + res[i].lname + ", " + res[i].fname + "</td>";
                                                    body += "<td>" + res[i].log_in + "</td>";
                                                    body += "<td>";
                                                    if (res[i].availablesched != null) {
                                                        body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_in + ",this.value)'>";

                                                        body += "<option value=''>--</option>";
                                                        res[i].availablesched.forEach(function (item) {

                                                            if (item.type == 'Normal') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                            } else if (item.type == 'WORD') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                            } else {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                            }

                                                        });

                                                        body += "</select>";
                                                    } else {
                                                        body += "No Schedule Available";
                                                    }
                                                    body += "</td>";
                                                    if (res[i].log_out == null) {
                                                        body += "<td>Not Logged Out</td>";
                                                    } else {
                                                        body += "<td>" + res[i].log_out + "</td>";
                                                    }
                                                    body += "<td >";
                                                    if (res[i].availablesched != null) {
                                                        body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_out + ",this.value)'>";

                                                        body += "<option value=''>--</option>";
                                                        res[i].availablesched.forEach(function (item) {

                                                            if (item.type == 'Normal') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                            } else if (item.type == 'WORD') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                            } else {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                            }

                                                        });

                                                        body += "</select>";
                                                    } else {
                                                        body += "No Schedule Available";
                                                    }

                                                    body += "</td>";
                                                    body += "</tr>";

                                                }
                                                $("#tbody").append(body);
//                                            $(".chosen-select").chosen();
//                                            $(".chosen-search").append('<i class="glyph-icon icon-search"></i>');
//                                            $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
                                            }
                                        });
                                    } else {
                                        var emplist = employeelist.toString().split(",");
                                        employeelist = emplist.join('-');

                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url(); ?>index.php/Schedule/getSchedPerAccountEmployeeDTR",
                                            data: {
                                                fromDate: fromDate,
                                                toDate: toDate,
                                                employeelist: employeelist
                                            },
                                            cache: false,
                                            beforeSend: function () {
                                                $("#loadingspinner").show();
                                            },
                                            success: function (ress) {
                                                $("#loadingspinner").hide();

                                                $("#tbody").html("");
                                                $("#scheduleview").show();
                                                var res = JSON.parse(ress);
                                                var body = "";
                                                for (var i = 0; i < res.length; i++) {
                                                    body += "<tr>";
                                                    body += "<td>" + res[i].lname + ", " + res[i].fname + "</td>";
                                                    body += "<td>" + res[i].log_in + "</td>";
                                                    body += "<td>";
                                                    if (res[i].availablesched != null) {
                                                        body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_in + ",this.value)'>";

                                                        body += "<option value=''>--</option>";
                                                        res[i].availablesched.forEach(function (item) {

                                                            if (item.type == 'Normal') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                            } else if (item.type == 'WORD') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                            } else if (item.type == 'Double') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                            } else {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                            }

                                                        });

                                                        body += "</select>";
                                                    } else {
                                                        body += "No Schedule Available";
                                                    }
                                                    body += "</td>";
                                                    if (res[i].log_out == null) {
                                                        body += "<td>Not Logged Out</td>";
                                                    } else {
                                                        body += "<td>" + res[i].log_out + "</td>";
                                                    }
                                                    body += "<td >";
                                                    if (res[i].availablesched != null) {
                                                        body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_out + ",this.value)'>";

                                                        body += "<option value=''>--</option>";
                                                        res[i].availablesched.forEach(function (item) {

                                                            if (item.type == 'Normal') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                            } else if (item.type == 'WORD') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                            } else if (item.type == 'Double') {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                            } else {
                                                                body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                            }

                                                        });

                                                        body += "</select>";
                                                    } else {
                                                        body += "No Schedule Available";
                                                    }

                                                    body += "</td>";
                                                    body += "</tr>";

                                                }
                                                $("#tbody").append(body);
//                                            $(".chosen-select").chosen();
//                                            $(".chosen-search").append('<i class="glyph-icon icon-search"></i>');
//                                            $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
                                            }
                                        });

                                    }
                                }
                            } else if (role == 'User Level 1') {


                                //alert("level 1");
                                var emplist = employeelist.toString().split(",");
                                employeelist = emplist.join('-');

                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url(); ?>index.php/Schedule/getSchedPerAccountEmployeeDTR",
                                    data: {
                                        fromDate: fromDate,
                                        toDate: toDate,
                                        employeelist: employeelist
                                    },
                                    cache: false,
                                    beforeSend: function () {
                                        $("#loadingspinner").show();
                                    },
                                    success: function (ress) {
                                        $("#loadingspinner").hide();

                                        $("#tbody").html("");
                                        $("#scheduleview").show();
                                        var res = JSON.parse(ress);
                                        var body = "";
                                        for (var i = 0; i < res.length; i++) {
                                            body += "<tr>";
                                            body += "<td>" + res[i].lname + ", " + res[i].fname + "</td>";
                                            body += "<td>" + res[i].log_in + "</td>";
                                            body += "<td>";
                                            if (res[i].availablesched != null) {
                                                body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_in + ",this.value)'>";

                                                body += "<option value=''>--</option>";
                                                res[i].availablesched.forEach(function (item) {

                                                    if (item.type == 'Normal') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                    } else if (item.type == 'WORD') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                    } else if (item.type == 'Double') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                    } else {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                    }

                                                });

                                                body += "</select>";
                                            } else {
                                                body += "No Schedule Available";
                                            }
                                            body += "</td>";
                                            if (res[i].log_out == null) {
                                                body += "<td>Not Logged Out</td>";
                                            } else {
                                                body += "<td>" + res[i].log_out + "</td>";
                                            }
                                            body += "<td >";
                                            if (res[i].availablesched != null) {
                                                body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_out + ",this.value)'>";

                                                body += "<option value=''>--</option>";
                                                res[i].availablesched.forEach(function (item) {

                                                    if (item.type == 'Normal') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                    } else if (item.type == 'WORD') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                    } else if (item.type == 'Double') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                    } else {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                    }

                                                });

                                                body += "</select>";
                                            } else {
                                                body += "No Schedule Available";
                                            }

                                            body += "</td>";
                                            body += "</tr>";

                                        }
                                        $("#tbody").append(body);
                                        $(".chosen-select").chosen();
                                        $(".chosen-search").append('<i class="glyph-icon icon-search"></i>');
                                        $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
                                    }
                                });
                            } else {
                                // //alert("EMPLOYEE");

                                var emplist = employeelist.toString().split(",");
                                employeelist = emplist.join('-');

                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url(); ?>index.php/Schedule/getSchedPerAccountEmployeeDTR",
                                    data: {
                                        fromDate: fromDate,
                                        toDate: toDate,
                                        employeelist: employeelist
                                    },
                                    cache: false,
                                    beforeSend: function () {
                                        $("#loadingspinner").show();
                                    },
                                    success: function (ress) {
                                        $("#loadingspinner").hide();

                                        $("#tbody").html("");
                                        $("#scheduleview").show();
                                        var res = JSON.parse(ress);
                                        var body = "";
                                        for (var i = 0; i < res.length; i++) {
                                            body += "<tr>";
                                            body += "<td>" + res[i].lname + ", " + res[i].fname + "</td>";
                                            body += "<td>" + res[i].log_in + "</td>";
                                            body += "<td>";
                                            if (res[i].availablesched != null) {
                                                body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_in + ",this.value)'>";

                                                body += "<option value=''>--</option>";
                                                res[i].availablesched.forEach(function (item) {

                                                    if (item.type == 'Normal') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                    } else if (item.type == 'WORD') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                    } else if (item.type == 'Double') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                    } else {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_in == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                    }

                                                });

                                                body += "</select>";
                                            } else {
                                                body += "No Schedule Available";
                                            }
                                            body += "</td>";
                                            if (res[i].log_out == null) {
                                                body += "<td>Not Logged Out</td>";
                                            } else {
                                                body += "<td>" + res[i].log_out + "</td>";
                                            }
                                            body += "<td >";
                                            if (res[i].availablesched != null) {
                                                body += "<select name='' class='chosen-select input-sm' onchange='schedupdater(" + res[i].dtr_id_out + ",this.value)'>";

                                                body += "<option value=''>--</option>";
                                                res[i].availablesched.forEach(function (item) {

                                                    if (item.type == 'Normal') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + "</option>";
                                                    } else if (item.type == 'WORD') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                    } else if (item.type == 'Double') {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.time_start + " - " + item.time_end + " " + item.type + "</option>";
                                                    } else {
                                                        body += "<option value='" + item.sched_id + "'" + " " + (res[i].sched_id_out == item.sched_id ? "selected" : "") + ">" + item.schedule_date + " " + item.type + "</option>";
                                                    }

                                                });

                                                body += "</select>";
                                            } else {
                                                body += "No Schedule Available";
                                            }

                                            body += "</td>";
                                            body += "</tr>";

                                        }
                                        $("#tbody").append(body);
                                        $(".chosen-select").chosen();
                                        $(".chosen-search").append('<i class="glyph-icon icon-search"></i>');
                                        $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
                                    }
                                });
                            }


                        }
                    });

                }

            })
    </script>

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>