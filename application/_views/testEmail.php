<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Anil Labs - Codeigniter mail templates</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
   <div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header">
   <img style="Margin-left: auto;Margin-right: auto;" src="https://lh3.googleusercontent.com/NZNZzuQogtKpP32IPLfU7ycNwp-VO_a3pqnAB4Owr0sRkJdDEHENGxRulIG2eyEtLcWQdVR5NZOsStRx4zhQePBcFviP2V_F=w1366-h768-rw-no"></div>
 <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
 <?php echo "Name: ".$Name;?></p> 
 <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
 <?php echo "Address: ".$Address;?></p> 
 <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
 <?php echo "Source: ".$source;?></p> 
 <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
 <?php echo "Gender: ".$gender;?></p>
 <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
 <?php echo "Contact: ".$contact;?></p> 
</div>
</body>
</html> 