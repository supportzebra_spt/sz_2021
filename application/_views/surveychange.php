<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  </style>

  
  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });

 </script>
</head>

<body>
  <div id="sb-site">
    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>
    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">
        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">
        <div id="page-title">
          <h2>Survey Details</h2>
          <p>Changes and updates with survey details.</p>
          <hr>
          <div class="content-box">
            <h3 class="content-box-header bg-white">
              <i class="glyph-icon icon-cog"></i>
              List of Header Questions / Title
              <div class="header-buttons">
                <div class="btn-group">
                  <div class="btn-group">
                    <button id="btn-group-example" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      Actions
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="btn-group-example">
                      <li><a href="#" data-toggle="modal" data-target="#createmodal">Create New Survey</a></li>
                      <li><a href="#" data-toggle="modal" data-target="#selectmodal">Select Active Survey</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </h3>
            <div class="content-box-wrapper">
              <div class="list-group">
                <?php foreach($survey as $sv):?>
                  <a href="#" class="list-group-item <?php if($sv->isActive==1){echo 'active';}?>" data-toggle="modal" data-target="#updatemodal" data-surveyid="<?php echo $sv->survey_id;?>">&#9654;  <?php echo $sv->headerquestion;?> </a>
                <?php endforeach;?>
              </div>
            </div>
          </div>
        </div> 
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="selectmodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog" style='width:50%'>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Select Active Survey</h4>
      </div>
      <div class="modal-body">
        <?php foreach($survey as $sv):?>
          <div class="radio">
            <label><input type="radio" name="survey" value="<?php echo $sv->survey_id;?>" <?php if($sv->isActive==1){echo 'checked';}?>><?php echo $sv->headerquestion;?></label>
          </div>
        <?php endforeach;?>
      </div>
      <div class="modal-footer">
        <button type="button" id='selectbtn' class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>Small modal content here ...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id='updatebtn' class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="createmodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Create New Survey</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Header Question</label>
              <input type="text" class='form-control' id='cheaderquestion'>
            </div>
          </div>
          <hr>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Level 1</label>
              <input type="text" class='form-control' id='clevel1'>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Level 2</label>
              <input type="text" class='form-control' id='clevel2'>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Level 3</label>
              <input type="text" class='form-control' id='clevel3'>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Level 4</label>
              <input type="text" class='form-control' id='clevel4'>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Level 5</label>
              <input type="text" class='form-control' id='clevel5'>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id='createbtn' class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {
        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {
        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $('#updatemodal').on('show.bs.modal', function() {
    // $.ajax({
    //   type: "POST",
    //   url: "<?php echo base_url();?>index.php/Survey/getHeaderQuestion",
    //   cache: false,
    //   success: function(res)
    //   {

    //   }
    // });
  })
</script>
<script type="text/javascript">
  $("#selectbtn").click(function(){
    var survey_id = $("input[name='survey']:checked").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/setActiveSurvey",
      data:{
        survey_id:survey_id
      },
      cache: false,
      success: function(res)
      {
        location.reload();
      }
    });
  });
</script>
<script type="text/javascript">
  $("#createbtn").click(function(){
    var headerquestion = $("#cheaderquestion").val();
    var level1 = $("#clevel1").val();
    var level2 = $("#clevel2").val();
    var level3 = $("#clevel3").val();
    var level4 = $("#clevel4").val();
    var level5 = $("#clevel5").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Survey/createnewsurvey",
      data:{
        headerquestion:headerquestion,
        level1:level1,
        level2:level2,
        level3:level3,
        level4:level4,
        level5:level5
      },
      cache: false,
      success: function(res)
      {
        location.reload();
      }
    });
  });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>