<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">
 
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>
 


    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

  

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>

<!-- TABLES --->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>
<script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>
<!-- TABLES --->
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="<?php echo $setting['settings']; ?>">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
<div class="form-group">

		<button class="btn btn-xs btn-success"  data-toggle="modal" data-target="#myModalAddMenu" id="AddMenu"> Add </button>
				<div id="myModalAddMenu" class="modal fade" role="dialog">
					<div class="modal-dialog" style="width: 400px;">

					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header" style="background: #00b19b; color: white;">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add System Menu</h4>
					  </div>
					  <div class="modal-body">

							<div id="AddUserOption">  
								 
								<div id="DivAddSingleUser"  >  
									<div class="example-box-wrapper">	 
									  <form class="form-horizontal bordered-row">

										<div class="form-group">
											<label class="col-sm-3 control-label">Description</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" id="addMenudesc">
											</div>
										</div> 
							 
										<div class="form-group">
											<label class="col-sm-3 control-label">Link</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" id="addMenuLink" >
											</div>
										</div> 
										<div class="form-group">
											<label class="col-sm-3 control-label">Menu Tab</label>
											<div class="col-sm-6">
												<select class="form-control" id="addMenu">
													<option value='--'>--</option>
													
												</select>
											</div>
										</div>
										  
										</form>

										<button class="btn btn-xs btn-warning" id='BtnBackAddMenu' class="close" data-dismiss="modal"> Cancel</button>
										<button class="btn btn-xs btn-success" id='BtnSaveAddMenu'> Save</button>
									</div> 
											
									
									
									
								</div>
							</div>

					  </div>
					 
					</div>

				  </div>
				</div>
			
	<div class="example-box-wrapper">
<table id=" " class="table table-striped table-bordered  " cellspacing="0" width="100%">
<thead>
<tr>
    <th>Tab Name</th>
    <th>Description</th>
    <th>Link</th>
    <th>IsActive?</th>
    <th>Actions</th>
   
</tr>
</thead>

<tfoot>
<tr>
  <th>Tab Name</th>
    <th>Description</th>
    <th>Link</th>
    <th>IsActive?</th>
    <th>Actions</th>
   
</tr>
</tfoot>

<tbody>
<?php 
 foreach($menu as $key => $value){
 if($value->is_active==1){
	 $value->is_active = "True";
	$chik ="1";
 }else{
	 $value->is_active = "False";
$chik ="0";
 }
  ?>
<tr>
    <td><?php echo $value->tab_name; ?></td>
    <td><?php echo $value->item_title; ?></td>
    <td><?php echo "/".$value->item_link; ?></td>
    <td><?php echo $value->is_active; ?></td>
    <td>
 <input type="button" id="myModalUpdated<?php echo $value->menu_item_id; ?>" class="btn btn-xs btn-info myModalUpdated<?php echo $value->menu_item_id; ?>"   data-toggle="modal" data-target="#myModalUpdate<?php echo $value->menu_item_id; ?>" value="Update"> 
	</td>

	
</tr>
  	<script>
	   $(function(){
		$('#myModalUpdated<?php echo $value->menu_item_id; ?>').click(function(){ 
				var desc = <?php echo $value->tab_id; ?>;
 				$.ajax({
						type: "POST",
						url: "Usermenu/getTabAssign",
						cache: false,
						success: function(result){
							// $('#select-account').html(html);
							var obj = jQuery.parseJSON(result);
							var text ="";
							 $(obj).each(function(key, value){
								 var ok = (value.tab_id == desc) ? 'selected' : ' ';
							text+="<option value="+value.tab_id+" "+ok+">"+value.tab_name+"</option>"
							 });
							 $("#addMenu<?php echo $value->menu_item_id; ?>" ).html(text); 
					}
				   }); 
				 
				});
				$('#myModalUpdates<?php echo $value->menu_item_id; ?>').click(function(){ 
				var desc = $("#addMenudesc<?php echo $value->menu_item_id; ?>").val();
				 var link = $("#addMenuLink<?php echo $value->menu_item_id; ?>").val(); 
				 var tab = $("#addMenu<?php echo $value->menu_item_id; ?>").val();
				 var isactib = $("#isActive<?php echo $value->menu_item_id.":checked"; ?>").val();
				 var menuID = <?php echo $value->menu_item_id; ?>;
				 dataString = "desc="+desc+"&link="+link+"&tab="+tab+"&isactib="+isactib+"&menuID="+menuID;
				 $.ajax({
				type: "POST",
				url: "Usermenu/UpdateMenuTabz",
				data: dataString,
				cache: false,
				success: function(html)
				{
				  alert("Updated");

				location.reload();
				  }
				  });	


				});
	   });
	</script>
  <div id="myModalUpdate<?php echo $value->menu_item_id; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 400px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #00b19b; color: white;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
  
    
      </div>
      <div class="modal-body"  >
			<div id="DivAddSingleUser"  >  
								<div class="example-box-wrapper">	 
 
										<div class="form-group">
											<label class="col-sm-3 control-label">Description</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" id="addMenudesc<?php echo $value->menu_item_id; ?>" value="<?php echo $value->item_title; ?>">
 											</div>
										</div> 
							 	</div> 
								<div class="example-box-wrapper">	 
										<div class="form-group">
											<label class="col-sm-3 control-label">Link</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" id="addMenuLink<?php echo $value->menu_item_id; ?>" value="<?php echo $value->item_link; ?>">
											</div>
										</div>
							 	</div> 
								<div class="example-box-wrapper">	 
										<div class="form-group">
											<label class="col-sm-3 control-label">Menu Tab</label>
											<div class="col-sm-6">
												<select class="form-control" id="addMenu<?php echo $value->menu_item_id; ?>">
													<option value='--'>--</option>
													
												</select>
											</div>
										</div>
							 	</div> 
								<div class="example-box-wrapper">	 
										<div class="form-group">
											<label class="col-sm-3 control-label">isActive?</label>
											<div class="col-sm-6">
<input type="checkbox"  id="isActive<?php echo $value->menu_item_id; ?>" <?php echo ($value->is_active=="True") ? "checked": " " ; ?> value="1">
											</div>
										</div>
							 	</div> 	 
										
									<div class="example-box-wrapper">	 
										<div class="form-group">
 											<div class="col-sm-6">
					<button class="btn btn-xs btn-info"  id="myModalUpdates<?php echo $value->menu_item_id; ?>">Update</button>	
										</div>
									</div>
							 	</div> 
							</div>
						  </div>
						</div>
					  </div>
					</div>
<?php  } ?>
</tbody>
</table>
 
</div>
                </div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
	    $.ajax({
		type: "POST",
		url: "Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	
	$.ajax({
		type: "POST",
		url: "Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });

  });	
</script>
<script>
  $(function(){
   
 
$('#BtnSaveAddMenu').click(function(){ 
var desc = $("#addMenudesc").val();
var link = $("#addMenuLink").val();
var tab = $("#addMenu").val();

dataString = "desc="+desc+"&link="+link+"&tab="+tab;

$.ajax({
type: "POST",
url: "Usermenu/addMenuTabz",
data: dataString,
cache: false,
success: function(html)
{
  alert("Added");

location.reload();
  }
  });	

});
$('#AddMenu').click(function(){ 
 	$.ajax({
		type: "POST",
		url: "Usermenu/getTabAssign",
		cache: false,
		success: function(result)
	{
 
  // $('#select-account').html(html);
		var obj = jQuery.parseJSON(result);
var text ="";
 $(obj).each(function(key, value){ 				 
text+="<option value="+value.tab_id+">"+value.tab_name+"</option>"
 
 
 });
  
 $("#addMenu").append(text); 
  
	}
	});	
 });
 


  });
  
  </script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>