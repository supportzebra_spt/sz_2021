<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  </style>


  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });
 </script>

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>
 <script type="text/javascript">
  /* Input switch */

  $(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
  });
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd-yyyy'
    });
  });


</script>
<script type="text/javascript">
  $(function(){
    $(".growl1").select(function(){
      $.jGrowl(
        "<?php echo $this->session->flashdata('success');?>",
        {sticky:!1,position:"top-right",theme:"bg-blue-alt"}
        )
    });
    $(".growl2").select(function(){
      $.jGrowl(
        "Success set bonus status",
        {sticky:!1,position:"top-right",theme:"bg-blue-alt"}
        )
    });
    $(".growl3").select(function(){
      $.jGrowl(
        "Failed bonus status update",
        {sticky:!1,position:"top-right",theme:"bg-red"}
        )
    });

    if("<?php echo $this->session->flashdata('success');?>" != ""){
      $(".growl1").trigger('select');
    }
  });
</script>


</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div class="container">

          <script type="text/javascript">

            /* Datatables export */

            $(document).ready(function() {
              var table = $('#datatable-tabletools').DataTable();
            });

            $(document).ready(function() {
              $('.dataTables_filter input').attr("placeholder", "Search...");
            });

          </script>
          
          <div id="page-title">
            <div class="btn-group">
              <button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm"><span class='icon-typicons-plus'></span> Bonus</button>

              <button class="btn btn-info btn-sm" onclick="location.reload();"><span class='icon-typicons-cw-outline'></span> Reload</button>

            </div>
          </div>
          <br>
          <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog" style='width:350px'>
              <div class="modal-content">
                <div class="modal-header" style="background: #1f2e2e; color: white;">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 class="modal-title">New Bonus</h3>
                </div>
                <div class="modal-body">
                  <div class="row" id='forms'>

                    <div class="col-md-12">
                      <label class='control-label'>Bonus Name
                        <span class="text-danger" id='bn' style='font-size:8px;' hidden>* (Required)</span>
                      </label>
                      <input type="text" name="" class='form-control' id='bonusname'>

                    </div> 
                    
                    <div class="col-md-12"><br>
                      <label class='control-label'>Description
                        <span class="text-danger" id='dn' style='font-size:8px;' hidden>* (Required)</span>

                      </label>
                      <textarea class='form-control' id='description'></textarea>

                    </div> 
                  </div> 
                </div>    
                <div class="modal-footer" id='footer'>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id='addnewbonus'>Save</button>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">

              <table id="datatable-tabletools" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th class='text-center' style='background: #787a7b;color: white;border-top-left-radius: 10px;'>Bonus Name</th>
                    <th class='text-center' style='background: #787a7b;color: white;'>Description</th>
                    <th class='text-center' style='background: #787a7b;color: white;border-top-right-radius: 10px'>Active</th>

                  </tr>
                </thead>
                <tfoot>
                  <tr>

                  <th class='text-center' style='background: #787a7b;color: white;border-bottom-left-radius: 10px;'>Bonus Name</th>
                    <th class='text-center' style='background: #787a7b;color: white;'>Description</th>
                    <th class='text-center' style='background: #787a7b;color: white;border-bottom-right-radius: 10px'>Active</th>
                  </tr>
                </tfoot>
                <tbody>
                 <?php foreach($values as $v):?>
                  <tr>
                    <td><?php echo $v->bonus_name;?></td>
                    <td><?php echo $v->description;?></td>
                    <td>
                      <input type="checkbox" data-on-color="info" data-off-color="danger" name="" class="input-switch" <?php if($v->isActive==1){echo 'checked';}?> data-size="mini" data-on-text="A" data-off-text="IA" id='id-<?php echo $v->bonus_id; ?>' onchange='setActiveness(<?php echo $v->bonus_id; ?>)'>
                    </td>
                  </tr>
                <?php endforeach;?>
              </tbody>
            </table>
          </div>
        </div>

      </div>

      <div class="row" hidden>
        <div class="col-md-8">
          <div class="panel">
            <div class="panel-body">
              <h3 class="title-hero">
                Recent sales activity
              </h3>
              <div class="example-box-wrapper">
                <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
          </div>




          <div class="content-box">
            <h3 class="content-box-header bg-default">
              <i class="glyph-icon icon-cog"></i>
              Live server status
              <span class="header-buttons-separator">
                <a href="#" class="icon-separator">
                  <i class="glyph-icon icon-question"></i>
                </a>
                <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                  <i class="glyph-icon icon-refresh"></i>
                </a>
                <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                  <i class="glyph-icon icon-times"></i>
                </a>
              </span>
            </h3>
            <div class="content-box-wrapper">
              <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
          </div>

        </div>

      </div>
    </div>



  </div>
</div>
</div>

<p class="growl1" hidden></p>
<p class="growl2" hidden></p>
<p class="growl3" hidden></p>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $("#addnewbonus").click(function(){
    var bonusname = $("#bonusname").val();
    var description = $("#description").val();
    if(bonusname==''){
      $("#bn").show();
    }else{
      $("#bn").hide();
    }
    if(description==''){
      $("#dn").show();
    }else{
      $("#dn").hide();
    }
    if(bonusname!=''&&description!=''){
     $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/PositionRates/newBonus",
      data: {
        bonusname: bonusname,
        description: description
      },
      cache: false,
      success: function(res)
      {
          // console.log(res);
          if(res=='Failed'){
            swal("Oops!", "An Error Occured while processing data.", "error");
          }else{
            location.reload();
          }
        }
//------------------ALLOWANCE-------------------
});
   }
 });
</script>

<script type="text/javascript">
  function setActiveness(id){

    if ($('#id-'+id).is(":checked")){
      // alert("Yes - "+id);
      var active = 1;
    }else{
      // alert("No - "+id);
      var active = 0;
    }

    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/PositionRates/updateActiveBonus",
      data: {
        bonus_id: id,
        activevalue: active
      },
      cache: false,
      success: function(res)
      {
        // alert(res);
          // console.log(res);
          if(res=='Failed'){
           $(".growl3").trigger('select');
         }else{
           $(".growl2").trigger('select');
         }
       }
     });
  }
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>