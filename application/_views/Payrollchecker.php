<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payrollchecker extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	
	public function index($stat=null,$pid=null){
					$status = explode("-",$stat);

 				  $query = $this->db->query("select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and (g.status='".ucfirst($status[1])."') and e.class='Agent'  and b.isActive='yes' and d.isActive=1 order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$pid);
 
		foreach ($query->result() as $row) 
			{
				$arr['employee'][$row->apid] = array(
				"apid" => $row->apid,
 				"fname" => $row->fname,
 				"lname" => $row->lname,
 				"emp_id" => $row->emp_id,
 				"payroll_id" => $row->payroll_id,
 				"emp_promoteId" => $row->emp_promoteId,
  				);
			}
	if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payrollchecker' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
				$arr['pid'] = $pid;

			if ($qry->num_rows() > 0){
					if($pid>0){
						$per_id =  $this->db->query("select coverage_id,daterange,month,year,period as period2 from tbl_payroll_coverage where coverage_id=".$pid."");
					foreach ($per_id->result() as $row)
					{
							$arr['period'] = array(
							"daterange" => $row->daterange,
							"coverage_id" => $row->coverage_id,
							"year" => $row->year,
							"month" => $row->month,
							"period" => $row->period2,
							
							);
					}
								$arr["stat"] = array("status" => $stat);

					$this->load->view('payrollcheck',$arr);
				}else{
					
				}
			}else{
				redirect('index.php/home/error');
			}
		}else{
					redirect('index.php/login');
		}
	
	
		}
		
		public function emptype($type){
		if($type==1){
			$payrollC = $this->input->post('payrollC');
			$employz = $this->input->post('employz');

		}else{
		$payrollC = $this->input->post('pc');
 		 $employz = $this->input->post('emp_id');

		}
		 $pid = $this->input->post('pid');
		  $date = explode("-",$payrollC);
  		$query = $this->db->query("select b.apid,fname,salarymode,lname,emp_id,(select concat(acc_id,'|',acc_name) from tbl_account where acc_id = b.acc_id) as account from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes' and b.emp_id in (".$employz.") order by lname");
		  foreach ($query->result() as $row){
			$DateRangeShift = $this->DateRangeShift($date);
			$empRate2 = $this->empRate($row->emp_id);
			$empDeductAdjustment2 = $this->empDeductAdjustment($row->emp_id,$pid);


				foreach($DateRangeShift as $row1 => $data){
 					$shift = $this->empshift($row->emp_id,$data);
						 
						$SSSCompute2 = $this->SSSCompute($row->emp_id);
						$PhilHealthCompute2 = $this->PhilHealthCompute($row->emp_id);
						$adjustSalary2 = $this->adjustSalary($row->emp_id,$payrollC);
						$deductSalary2 = $this->deductSalary($row->emp_id,$payrollC);
							$totalSSSPiPh =	(($PhilHealthCompute2[0]->employeeshare/2) + ($SSSCompute2[0]->sss_employee/2) + (($empRate2[0]->rate/2)*.02));
								$BIRCompute2 = $this->BIRCompute($row->emp_id,$empRate2[0]->rate,$totalSSSPiPh);
									$BIRCompute3 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->deduction : '0';
									$BIRCompute4 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->percent: '0';
									$BIRCompute5 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->salarybase : '0';
									$BIRCompute6 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->description : 'not set';
					foreach($shift as $row2 => $data2){
						$in = $this->empshiftin($row->emp_id,$data2->sched_id);
						$out = $this->empshiftout($row->emp_id,$data2->sched_id);
						$emplogsShift2 = $this->emplogsShift($data2->sched_id,$row->emp_id);
 							$empPosAllowance2 = $this->empPosAllowance($empRate2[0]->posempstat_id);
							$empPosAllowance1 = (count($empPosAllowance2)>0) ? $empPosAllowance2[0]->allowance : 0;
							$empPosBonus2 = $this->empPosBonus($empRate2[0]->posempstat_id);
							$empPosBonus1 = (count($empPosBonus2)>0) ? $empPosBonus2[0]->bonus : 0;
							
						$timeschedule = $this->timeschedule($data2->sched_id);
							$NDbegin = "10:10:00 PM";
							$NDend   = "6:00:00 AM";
							
							$HPbegin = "12:00:00 AM";
							$HPend   = "3:00:00 AM";
							
							$HPbegin2 = "2:00:00 PM";
							$HPend2   = "11:59:59 PM";
							
							$HPbegin3 = "4:00:00 AM";
							$HPend3   = "5:00:00 AM";
							
							
						if($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double"){
							if(count($in)>0){
							$shiftBreak=  $this->shiftBrk($in[0]->acc_time_id);
							
							$to_time= strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login)));
							$from_time = strtotime($data." ".$timeschedule[0]->time_start);
								$NDdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
								$NDdate2 = DateTime::createFromFormat('H:i a', $NDbegin);
								$NDdate3 = DateTime::createFromFormat('H:i a', $NDend);

								$HPdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
								$HPdate2 = DateTime::createFromFormat('H:i a', $HPbegin);
								$HPdate3 = DateTime::createFromFormat('H:i a', $HPend);
								
								$HPdate4 = DateTime::createFromFormat('H:i a', $HPbegin2);
								$HPdate5 = DateTime::createFromFormat('H:i a', $HPend2);
								
								$HPdate6 = DateTime::createFromFormat('H:i a', $HPbegin3);
								$HPdate7 = DateTime::createFromFormat('H:i a', $HPend3);
								if ($NDdate1 >= $NDdate2 or $NDdate1 <= $NDdate3){
								   $ndRemark = "Y";
								}else{
								   $ndRemark= "N";
								}
								if (($HPdate1 >= $HPdate4 and $HPdate1 <= $HPdate5) or ($HPdate1 >= $HPdate6 and $HPdate1 <= $HPdate7) ){
											$hpRemark = "Y";
											$hpRate = '0.08';
								}else if(($HPdate1 >= $HPdate2) and ($HPdate1 <= $HPdate3)){
										   $hpRemark = "Y";
											$hpRate = '0.10';
								}else{
											$hpRemark= "N";
											 $hpRate = '';	
								}
								$time_startNEWW  = ($timeschedule[0]->time_start  == "12:00:00 AM" ) ? "11:59:59 PM" : $timeschedule[0]->time_start;
								$ndTimeStarted = $data." ".$time_startNEWW;
								if($timeschedule[0]->time_start == "12:00:00 AM"){
									 // if(date('h:i:s a', strtotime($in[0]->login))>date('h:i:s a', strtotime("12:00:00 am"))){
									if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) >= strtotime(date('Y-m-d H:i:s', strtotime($data." 24:00:00")) )){

														 // $data= date('Y-m-d', strtotime('-1 day', strtotime($data)));
														 $from_time = strtotime($data." 24:00:00");
														$timediff = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
															$late = (round(abs($from_time - $to_time) / 60,2));
															$latemins = (round(abs($from_time - $to_time) / 60,2));

												 }else{
														 
														$from_time = strtotime($data." 24:00:00");
														$timediff = round(abs($from_time - $to_time) / 60,2)." mins. early";
															$latemins = 0;
															$late = 0;
												 }												 
											 }else{
											if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) >= strtotime(date('Y-m-d H:i:s', strtotime($data." ".$timeschedule[0]->time_start))) ){
										 
											$timediff = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
											$late=(round(abs($from_time - $to_time) / 60,2));
											$latemins = (round(abs($from_time - $to_time) / 60,2));
										}else{
											$timediff = round(abs($from_time - $to_time) / 60,2)." mins. early";
											$late=0;
											$latemins=0;
										}
											 }
								
								
								$BCTSched = date('H:i:00', strtotime('-10 mins', strtotime($timeschedule[0]->time_start)));
								$bct = ($data." ".$BCTSched.":00" > date('Y-m-d H:i:00', strtotime($in[0]->login)) ) ? "Q" : "DQ";
								$absent = 0;
							}else{
								$latemins=0;
								$late=0;
								$timediff = "<span style='color:red'><b>Absent</b></span>";
								$bct = "<span style='color:red'>DQ</span>";
								$hpRemark= "N";
								$hpRate = '';	
								$absent = 1;
								$ndRemark = "N";
								
							}
							if((count($emplogsShift2)>0)){
								
								$finalND = explode("-",$emplogsShift2[0]->nd); 
								// $finalND2 = (count($emplogsShift2[0]->nd)>0) ? $finalND[0]+($finalND[1]/60) : 0; 
								$finalND2 = (count($emplogsShift2[0]->nd)>0 and !empty($emplogsShift2[0]->nd)) ? $finalND[0]+($finalND[1]/60) : 0; 

							}else{
								$finalND2=0;
							}
							$totalShiftBreaks = (count($in) >0) ? $shiftBreak[0]->hr + $shiftBreak[0]->min  : 0;
						
						}else{
							 $timediff = "<span style='color:red'>".$data2->type."</span>";
							 $hpRemark= "N";
							    $hpRate = '0.0';	
							  	$empDeductAdjustmentFinal = 0;
							 $empDeductAdjustmentFinal_ID = 0;
							 $bct  = "DQ";
							 // $totalSSSPiPh =0;
							$ndRemark = "N";
							$late=0; 
							$latemins=0;
							$totalShiftBreaks=0;
								$time_startNEWW=0;
								$ndTimeStarted=0;

							$finalND2=0;
							   if($empRate2[0]->status == "Probationary"){
									$absent = ($data2->type=="Leave-WP" || $data2->type=="Leave-WoP" || $data2->type=="Suspension" || $data2->type=="Absent" || $data2->type=="VTO-WoP") ? 1 : 0 ;
							  }else{
									// $absent = ($data2->type=="Leave-WoP" || $data2->type=="VTO-WoP" || $data2->type=="Holiday Off-WoP"|| $data2->type=="Suspension"|| $data2->type=="Absent") ? 1 : 0 ;
									$absent = ($data2->isPaid==0) ? 1 : 0 ;
							  }
						}
							
								$adjustSalary1 = (count($adjustSalary2)>0) ? $adjustSalary2[0]->adjust : 0;
								$deductSalary1 = (count($deductSalary2)>0) ? $deductSalary2[0]->deduct : 0;
 								$empDeductAdjustmentFinal = (count($empDeductAdjustment2)>0) ? $empDeductAdjustment2[0]->adj_amount : 0;
								$empDeductAdjustmentFinal_ID = (count($empDeductAdjustment2)>0) ? $empDeductAdjustment2[0]->pemp_adjust_id : 0;
 						// $logouttTotalHours =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date_format(date_create($out[0]->logout),"Y-m-d")." ".$timeschedule[0]->time_end : $data." 00:00:00" : $data." 00:00:00";
 						$logouttTotalHours =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $out[0]->logout : $data." 00:00:00" : $data." 00:00:00";
						$logoutt =  (count($out)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $out[0]->logout : $data." 00:00:00") : $data." 00:00:00";
						
				
						 $logouttDTRID =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $out[0]->dtr_id : $data." 00:00:00" : $data." 00:00:00";
 						 $loginn =  (count($in)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? (($latemins>0) ? $in[0]->login : date('Y-m-d h:i:00 a', strtotime($data." ".$time_startNEWW))) : $data." 00:00:00") : $data." 00:00:00";
							$loginnTotalHours =  (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? $data." ".$time_startNEWW  : $data." 00:00:00" : $data." 00:00:00";
 
							$loginnNew =  (count($in)>0) ? (($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? ((strtotime($in[0]->login)> strtotime($loginnTotalHours) ) ?  date('Y-m-d H:i:00 ', strtotime($in[0]->login))  : $loginnTotalHours) : $data." 00:00:00") : $data." 00:00:00";
 
						$loginnDTRID =  (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $in[0]->dtr_id : $data." 00:00:00" : $data." 00:00:00";
						$logoutt2 =  ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_end : "00:00:00";
						$loginn2 =  ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_start : "00:00:00";
 							$loginn2_timeID =  ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_id : "0";
							$breaks = $this->breakIdentify($loginnDTRID,$logouttDTRID,$row->emp_id);
							$loginn2timeID= $this->loginn2timeID($loginn2_timeID);
							$loginnNdtimeID= $this->loginnNdtimeID($loginn2_timeID);
							$shiftTotalHr= $this->shiftTotalHr($loginn2_timeID);
							$from_timeLog1 = strtotime($loginn);
							$to_timeLog1 = strtotime($logoutt);
							$resLog1 = round(abs(($to_timeLog1-$from_timeLog1)/3600),2);
							$shiftTotalHours = (count($shiftTotalHr)>0) ? $shiftTotalHr[0]->ho_end_time + $shiftTotalHr[0]->ho_start_time : 0;
							// $logouttNew =  (count($out)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? (($resLog1>$shiftTotalHours) ? $logouttTotalHours :  $out[0]->logout) : $data." 00:00:00") : $data." 00:00:00";

							$logouttNew =  (count($out)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? (strtotime($out[0]->logout)>=(strtotime($logouttTotalHours )) ? $logouttTotalHours :  $out[0]->logout) : $data." 00:00:00") : $data." 00:00:00";

							$isLogoutHoliday2 = $this->isLogoutHoliday($logoutt,$logoutt2);
							$isLoginHoliday2 = $this->isLoginHoliday($loginn,$loginn2);
							
							 $TotalAmountHoliday1=0;
								$TotalAmountHoliday2=0;	
								$hrly=0;	
								$HolidayND=0;	
								$HolidayHP=0;	
								$NDComputeHolidayLogout=0;	
								$HPComputeHolidayLogout=0;	
								$NDComputeHolidayLogin=0;	
								$HPComputeHolidayLogin=0;	
								$HolidayType1 =0;
								$HolidayType2=0;
								$time_startNEWW=0;

					if(count($isLogoutHoliday2[0]->date)>0){
					
							// $HolidayType = ($isLogoutHoliday2[0]->type=='Regular') ? 1 : .30;
							$HolidayType =$isLogoutHoliday2[0]->calc;
						if(count($loginn2timeID)>0){
					
							if(count($out)>0){
								
							$isLogoutHoliday1  =  number_format(($loginn2timeID[0]->ho_end_time*$HolidayType),2);
 							 $TotalAmountHoliday1 = ((($empRate2[0]->rate*12)/261)/8)*$isLogoutHoliday1;  
							$NDComputeHolidayLogout =  (isset($loginnNdtimeID[0]->nd_time_end)) ? number_format(($loginnNdtimeID[0]->nd_time_end*$HolidayType),2) : 0;
							$HPComputeHolidayLogout =  (isset($loginnNdtimeID[0]->nd_time_end)) ? number_format(($loginnNdtimeID[0]->nd_time_end*$HolidayType),2) : 0;
							$HolidayType2 =  $isLogoutHoliday2[0]->type;
							}else{
							$isLogoutHoliday1  =  0 ;
							}
						}else{
							$isLogoutHoliday1  =  0 ;
						}
					}else{
					 $isLogoutHoliday1  =  0;
					}
					if(count($isLoginHoliday2[0]->date)>0){
 						// $HolidayType = ($isLoginHoliday2[0]->type=='Regular') ? 1 : .30;
						$HolidayType = $isLoginHoliday2[0]->calc;

					if(count($loginn2timeID)>0){
						if(count($in)>0){
						$isLoginHoliday1  =  number_format(($loginn2timeID[0]->ho_start_time*$HolidayType),2);
						 $TotalAmountHoliday2 = ((($empRate2[0]->rate*12)/261)/8)*$isLoginHoliday1;  

						 $NDComputeHolidayLogin = (isset($loginnNdtimeID[0]->nd_time_start)) ? number_format(($loginnNdtimeID[0]->nd_time_start*$HolidayType),2)  : 0;
						$HPComputeHolidayLogin = (isset($loginnNdtimeID[0]->nd_time_start)) ? number_format(($loginnNdtimeID[0]->nd_time_start*$HolidayType),2) : 0;
						$HolidayType1 =  $isLoginHoliday2[0]->type;
						}else{
							$isLoginHoliday1  =  0;

						}
					}else{
						$isLoginHoliday1  =  0 ;
					}
				  }else{
					 $isLoginHoliday1  =  0;
					 
					
				  }
				$shiftTotalHours = (count($shiftTotalHr)>0) ? $shiftTotalHr[0]->ho_end_time + $shiftTotalHr[0]->ho_start_time : 0;
				$shiftTotalHoursWithBreaks = (count($shiftTotalHr)>0) ? $shiftTotalHr[0]->ho_end_time + $shiftTotalHr[0]->ho_start_time+($totalShiftBreaks/60) : 0;

					
					$origLogin = (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date('Y-m-d h:i:s a', strtotime($in[0]->login)) : $data." 00:00:00" : $data." 00:00:00";
					$origLogin =  strtotime($origLogin);
					$origLogout = (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date('Y-m-d h:i:s a', strtotime($out[0]->logout)) : $data." 00:00:00" : $data." 00:00:00";
					$origLogout =  strtotime($origLogout);

					$from_timeLog1 = strtotime($loginnNew);
					$to_timeLog1 = strtotime($logouttNew);
					$from_timeLog = strtotime($loginnTotalHours);
					$to_timeLog = strtotime($logouttTotalHours);
					$from_timeLog_NEW = strtotime($loginnNew); 
					$to_timeLogNEW = strtotime($logouttNew);
				$resLog1 = round(abs(($to_timeLog1-$from_timeLog1)/3600),2); //totalwork
				$resLog = round(abs(($to_timeLog-$from_timeLog)/3600),2);//total
				$resLogOrignal = round(abs(($origLogout-$origLogin)/3600),2);//totalOriginal
				$resLogNEW = round(abs(($to_timeLogNEW-$from_timeLog_NEW)/3600),2); //resLogNEW
				
				if($finalND2!=0){
					if ($resLog1<$shiftTotalHours){
							$start = strtotime($ndTimeStarted);
							$end = strtotime($logouttNew);
							$brk = (count($breaks)>0) ? (($breaks[0]->hr >=1) ? 30 : $breaks[0]->hr*60)+$breaks[0]->min : 0;
							$NDuT = round(abs(($end-$start)/3600),2); //ND if UT
							$NDuT -= $brk/60; //ND if UT
						}elseif($resLog1==$shiftTotalHours){
							$NDuT = $finalND2; 

						}else{
							$NDuT = $finalND2;
						}
				}else{
						$NDuT = 0;
				}
						$arr["employee"][$row->emp_id][$data2->sched_id."-".$data2->sched_date]= array(
							"emp_id" => $row->emp_id,
							"salarymode" => $row->salarymode,
							"fname" => $row->lname.", ".$row->fname,
							"date" => $data,
							"shift_type" => $data2->type,
							"login" =>(count($in)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date('Y-m-d h:i:s a', strtotime($in[0]->login)) : $data2->type) : "No Log IN",
							"logout"=>(count($out)>0)? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date('Y-m-d h:i:s a', strtotime($out[0]->logout)): $data2->type) : "No Log OUT",
							"shiftStart" => ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_start : $data2->type,
							"shiftEnd" => ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_end : $data2->type,
							// "late" => $timediff,
							"latecalc" => (isset($late)) ? ceil($late/60) : 0,
							"latecalcTEST" => (isset($late)) ? $late : 0,
							"latemins22" =>(count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ?  date('Y-m-d H:i:00', strtotime($in[0]->login))."  >= ".date('Y-m-d H:i:00', strtotime($data." 24:00:00")) : $data2->type : "No Log IN",
							"latemins" => $latemins/60,
							"bct" => $bct,
							"ndRemark" =>   $ndRemark, 
							"hpRemark" =>   $hpRemark,
							"hpRate" =>   $hpRate,
							"logouttTotalHours" =>   $logouttTotalHours,
							"dayType" => $absent,
							"nd" =>  (count($emplogsShift2)>0) ? $NDuT : "0",
 							"empPosAllowance" =>   $empPosAllowance1,
							"empPosBonus" =>   $empPosBonus1,
							"isHoliday" =>    $isLogoutHoliday1+ $isLoginHoliday1,
							"isHoliday1" =>    $isLoginHoliday1,
							"isHoliday2" =>    $isLogoutHoliday1,
							"isLoginHoliday2" =>    $isLoginHoliday2[0]->date,
  							"calcHoliday1" =>    (isset($isLoginHoliday2[0]->calc)) ? $isLoginHoliday2[0]->calc : 0,
							"calcHoliday2" =>   (isset($isLogoutHoliday2[0]->calc)) ? $isLogoutHoliday2[0]->calc : 0,
							"TotalAmountHoliday" =>   number_format($TotalAmountHoliday1+$TotalAmountHoliday2,2, '.', ''),
							"TotalAmountHoliday1" =>   number_format($TotalAmountHoliday1,2, '.', ''),
 							"TotalAmountHoliday2" =>   number_format($TotalAmountHoliday2,2, '.', ''),
							"HPComputeHolidayLogin" =>   $HPComputeHolidayLogin,
							"NDComputeHolidayLogin" =>   $NDComputeHolidayLogin,
							"NDComputeHolidayLogout" =>   $NDComputeHolidayLogout,
							"HPComputeHolidayLogout" =>   $HPComputeHolidayLogout,
							"HolidayType1" =>   $HolidayType1,
							"HolidayType2" =>   $HolidayType2,
							// "total" =>   ($resLog>0) ? $resLog : 1,
							"hrly" =>   $hrly,
							"rate" =>   $empRate2[0]->rate,
							"emp_promoteId" =>  $empRate2[0]->emp_promoteId,
							"rate2" =>   (($empRate2[0]->rate/2)-($totalSSSPiPh)),
							"totalSSSPiPh" =>   $totalSSSPiPh,
							"pos_name" =>   $empRate2[0]->pos_name,
							"payroll_adjDeduct_amount" =>  $empDeductAdjustmentFinal ,
							"pemp_adjust_id" =>  $empDeductAdjustmentFinal_ID ,
							"sss_id" =>   $SSSCompute2[0]->sss_id,
							"SSS" =>   $SSSCompute2[0]->sss_employee,
							"philhealth_id" =>   $PhilHealthCompute2[0]->philhealth_id,
							"PhilHealthCompute" =>   $PhilHealthCompute2[0]->employeeshare,
							"tax_id" =>   (count($BIRCompute2) > 1) ? $BIRCompute2[0]->tax_id : 2,
							"BIRCompute" =>  $BIRCompute3,
							"BIRPercent" =>  $BIRCompute4,
							"BIRSalaryBase" =>  $BIRCompute5,
							"BIRDescription" =>  $BIRCompute6,
							"adjustSalary" =>  $adjustSalary1,
							"deductSalary" =>  $deductSalary1,
							// "total" =>   ($resLog>0) ? $resLog : 0,
							"resLogOrignal" =>   ($resLogOrignal>0) ? $resLogOrignal : 0,
							// "totalwork" =>   ($resLog1>0) ? ( ($resLog1<=$shiftTotalHoursWithBreaks) ? $resLog1 : $shiftTotalHoursWithBreaks)  : 0,
							// "totalwork2" =>   ($resLog1>0) ? floor($resLog1) : 0,
							"total" =>   ($resLog>0) ? ( ($resLog<=$shiftTotalHoursWithBreaks) ? $resLog : (($resLogOrignal<=$shiftTotalHoursWithBreaks) ? $resLogOrignal :$shiftTotalHoursWithBreaks) ): 0211,
							"totalwork" =>   ($resLog1>0) ? ( ($resLog1<=$shiftTotalHoursWithBreaks) ? $resLog1 : (($resLogOrignal<=$shiftTotalHoursWithBreaks) ? $resLogOrignal :$shiftTotalHoursWithBreaks))  : 0,

							"breaks" =>   (count($breaks)>0) ? $breaks[0]->hr+$breaks[0]->min : 0,
							"account" =>   $row->account,
							"shiftTotalHours" =>   isset($shiftTotalHours) ? $shiftTotalHours : 0,
							"logouttNew" =>   $logouttNew,
							"loginnNew" =>   $loginnNew,
							"loginn2" =>   $loginn2,
							"loginn" =>   $loginn,
							"logoutt" =>   $logoutt,
							"logoutt2" =>   $logoutt2,
							// "posempstatid" =>   $empRate2[0]->posempstat_id,
							"resLogNEW" =>   ($resLogNEW>0) ? ( ($resLogNEW<=$shiftTotalHoursWithBreaks) ? $resLogNEW : $shiftTotalHoursWithBreaks ): 0,
							"shiftBreak" =>  ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? ((count($in) >0) ? $shiftBreak[0]->hr + $shiftBreak[0]->min  : 0) : 0,   
							"totalShiftBreaks" =>  ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? ((count($in) >0) ? $totalShiftBreaks  : 0) : 0,   
							"z1" =>    $origLogin,
							"z2" =>    $origLogout,
							"z3" =>    $resLogOrignal,

						);
					}
				}
				$late=0;
			}
			echo json_encode($arr);
 		}
		 
		
		public function empshift($empid,$date){
		$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and a.isActive=1 and sched_date = '".$date."' and emp_id=".$empid);
			  return $query->result();
		 }
		 	 public function shiftBrk($acc_time_id){
		$query = $this->db->query("SELECT sum(hour*60) as hr, sum(min) as min FROM `tbl_shift_break` a,tbl_break_time_account b,tbl_break_time c where a.bta_id=b.bta_id and b.btime_id = c.btime_id and a.acc_time_id =$acc_time_id and a.isActive=1");
			  return $query->result();
		 }
		  public function breakIdentify($login,$logout,$empid){
		 		$query = $this->db->query("SELECT sum(hour) as hr,sum(min) as min FROM `tbl_dtr_logs` a,tbl_acc_time b,tbl_time c, tbl_break_time_account d, tbl_break e,tbl_break_time f where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and c.bta_id=d.bta_id and d.break_id=e.break_id and d.btime_id=f.btime_id and dtr_id between '".$login."' and '".$logout."' and emp_id=".$empid." and a.type='Break' and entry='O'");
			  return $query->result();

		 }
		  
		  public function empshiftin($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and sched_id=".$shift." and emp_id=".$empid);
			  return $query->result();
 		}
		  public function empshiftout($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as logout,acc_time_id from tbl_dtr_logs where entry='O' and type='DTR' and sched_id=".$shift." and emp_id=".$empid);
			  return $query->result();
 		}
		public function timeschedule($timeschedule){
			$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_acc_time b,tbl_time c  where a.acc_time_id = b.acc_time_id and b.time_id = c.time_id and sched_id=".$timeschedule);
			return $query->result();
		 }
		 public function DateRangeShift($date){
		$strDateFrom = date("Y-m-d", strtotime($date[0]));
		$strDateTo = date("Y-m-d", strtotime($date[1]));
		$aryRange=array();
		$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
			if($iDateTo>=$iDateFrom){
				array_push($aryRange,date('Y-m-d',$iDateFrom));
				 while($iDateFrom<$iDateTo){
					$iDateFrom+=86400; // add 24 hours
						array_push($aryRange,date('Y-m-d',$iDateFrom));
					}
			}
			 return $aryRange;
			 		
		}
	 
		 public function loan($id){
 
			$query = $this->db->query("select b.emp_id,a.fname,a.lname from tbl_applicant a,tbl_employee b,tbl_user c,tbl_emp_promote d where a.apid=b.apid and b.emp_id=d.emp_id and b.emp_id = c.emp_id and b.isActive='Yes' and c.isActive=1 and d.isActive=1");
				foreach ($query->result() as $row){
					$loanz = $this->loanList($row->emp_id);
					foreach ($loanz as $row1){
 						$loanz2 = $this->loanList2($row1->pemp_adjust_id);
						/*foreach ($loanz2 as $row12){*/
						if($loanz2[0]->cnt < $row1->count){
							$insert = $this->db->query("insert into tbl_payroll_deduct_child(coverage_id,emp_id,pemp_adjust_id,date_given) values($id,$row->emp_id,$row1->pemp_adjust_id,now())");

						}else{
							$update = $this->db->query("update tbl_payroll_emp_adjustment set isActive=0 where pemp_adjust_id=".$row1->pemp_adjust_id);

						}
					 
						// }
					}
				}
 
		 
		 }
		 
		 public function loanList($empid){
			$query = $this->db->query("SELECT * FROM `tbl_payroll_emp_adjustment` where isActive=1 and emp_id =".$empid);
			 return $query->result();

		 }
		 public function loanList2($pemp_adjust_id){
			$query = $this->db->query("SELECT count(*) as cnt FROM `tbl_payroll_deduct_child` where pemp_adjust_id =".$pemp_adjust_id);
			 return $query->result();

		 }
		 public function emplogs($date,$empid){
		$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and sched_date = '".$date."' and emp_id=".$empid);
			  return $query->result();
		 }
 
		public function addDeductLonNew(){
				$emp_id = $this->input->post('emp_id');
				$pid = $this->input->post('pid');
				$nd = $this->input->post('nd');
			$query = $this->db->query("insert into tbl_payroll_deduct_child(coverage_id,emp_id,pemp_adjust_id,date_given) values($pid,$emp_id,$nd,now())");
			if($query){
				echo 1;
			}else{
				echo 0;
			}
		}
		public function addDeductNew(){
				$emp_id = $this->input->post('emp_id');
				$pid = $this->input->post('pid');
				$nd = $this->input->post('nd');
				$DeductVal = floatval($this->input->post('deductVal'));
			$query = $this->db->query("insert into tbl_payroll_emp_deduction(coverage_id,emp_id,pdeduct_id,value) values($pid,$emp_id,$nd,$DeductVal)");
			if($query){
				echo 1;
			}else{
				echo 0;
			}
		}
		public function addLoan(){
				$emp_id = $this->input->post('emp_id');
				$pid = $this->input->post('pid');
				$value = $this->input->post('value');
			// $query = $this->db->query("Select pemp_adjust_id,deductionname from tbl_payroll_emp_adjustment where pemp_adjust_id not  in (SELECT pemp_adjust_id FROM `tbl_payroll_deduct_child` where  coverage_id=$pid ) and  emp_id=$emp_id and isActive=1");
			$query = $this->db->query("Select pemp_adjust_id,deductionname,adj_amount from tbl_payroll_emp_adjustment a, tbl_payroll_deductions b where a.pdeduct_id=b.pdeduct_id and pemp_adjust_id not  in (SELECT pemp_adjust_id FROM `tbl_payroll_deduct_child` where coverage_id=$pid ) and  emp_id=$emp_id and a.isActive=1");
				echo "<option selected disabled>--</option>";
			foreach($query->result() as $row){
				echo "<option value=".$row->pemp_adjust_id.">".$row->deductionname." = P".($row->adj_amount/2)."</option>";
			}	
				
			}
		public function addOtherDeduct(){
  			$emp_id = $this->input->post('emp_id');
			$pid = $this->input->post('pid');
			$value = $this->input->post('value');
			$query = $this->db->query("select  pdeduct_id,deductionname from tbl_payroll_deductions where  pdeduct_id not in (select pdeduct_id from tbl_payroll_emp_deduction where coverage_id =$pid and emp_id=$emp_id)");
				echo "<option selected disabled>--</option>";

			foreach($query->result() as $row){
				echo "<option value=".$row->pdeduct_id.">".$row->deductionname."</option>";
			}
		}
		public function empDeductAdjustment($empid,$pid){
			// $query = $this->db->query("select pemp_adjust_id,adj_amount from tbl_payroll_emp_adjustment where emp_id= ".$empid." and isActive=1");
			 // $query = $this->db->query("select GROUP_CONCAT(pemp_adjust_id) as pemp_adjust_id ,GROUP_CONCAT(deductionname) as deductName,sum(adj_amount) as adj_amount from tbl_payroll_emp_adjustment a,tbl_payroll_deductions b where a.pdeduct_id=b.pdeduct_id and  emp_id= ".$empid."  and isActive=1");
			 $query = $this->db->query("select GROUP_CONCAT(a.pemp_adjust_id) as pemp_adjust_id ,GROUP_CONCAT(concat(deductionname,'-',adj_amount)) as deductName,sum(adj_amount) as adj_amount from tbl_payroll_emp_adjustment a,tbl_payroll_deductions b,tbl_payroll_deduct_child c where a.pdeduct_id=b.pdeduct_id and a.pemp_adjust_id = c.pemp_adjust_id and a.emp_id= ".$empid." and c.coverage_id=".$pid." and a.isActive=1");
			 return $query->result();
 		}
		public function emplogsOut($dtrid,$empid){
			$query = $this->db->query("select dtr_id,emp_id,log as logout from tbl_dtr_logs where entry='O' and type='DTR' and note=".$dtrid." and emp_id=".$empid."");
			 return $query->result();
 		}
			public function emplogsShift($sched_id,$empid){
 
			// $query = $this->db->query("select time_start,time_end from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.acc_time_id=".$acc_time_id." and emp_id=".$empid."");
			$query = $this->db->query("select distinct(c.time_id),time_start,time_end,(select group_concat(concat(hour,'-',minutes))  from tbl_payroll_nd as s where s.time_id=c.time_id) as nd from tbl_dtr_logs a,tbl_acc_time b,tbl_time c,tbl_schedule d where b.acc_time_id=d.acc_time_id and b.time_id=c.time_id and a.sched_id=d.sched_id and a.sched_id=".$sched_id."  and a.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function empRate($empid){
 
			$query = $this->db->query("select d.emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,c.posempstat_id from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 and f.isActive=1 and b.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function NightDiff($nd){
 			$query = $this->db->query("select time_start,time_end,hour,minutes from tbl_payroll_nd a,tbl_time b where a.time_id=b.time_id and a.isActive=1 and b.time_start ='".$nd."'");
				
			 return $query->result();
  		}
		public function SSSCompute($empid){
 			// $query = $this->db->query("select sss_employee from (select rate from tbl_rate a,tbl_position b,tbl_employee c,tbl_applicant d where a.pos_id=b.pos_id and c.apid=d.apid and b.pos_id =d.pos_id and a.isActive=1 and c.isActive='yes' and c.emp_id=".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
 			  $query = $this->db->query("select sss_employee,sss_id from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
				
			 return $query->result();
  		}
		public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare,philhealth_id from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		}
		public function empPosAllowance($posempstatid){
 
			$query = $this->db->query("select pos_name,d.status,group_concat(concat(allowance_name,'=',value/2) order by allowance_name) as allowance from tbl_allowance a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id  and b.empstat_id=d.empstat_id and b.posempstat_id =".$posempstatid." and a.isActive=1 group by b.posempstat_id");
				
			 return $query->result();
 			 
 		}
		public function empPosBonus($posempstatid){
 
			$query = $this->db->query("select pos_name,d.status,group_concat(concat(bonus_name,'=',amount/2)) as bonus from tbl_pos_bonus a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_bonus e where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and e.bonus_id = a.bonus_id and b.posempstat_id =".$posempstatid." and a.isActive=1 and amount!=0 group by b.posempstat_id ");
				
			 return $query->result();
 			 
 		}
		     public function BIRCompute($empid,$rate,$totalSSSPiPh){
		 /*  public function BIRCompute(){
			   $empid=51;
			   $rate=10800;
			   $totalSSSPiPh=370.35; */
			  
			   
				if($totalSSSPiPh<=300){
					$base= (($rate/2)-500);
				}else{
					$base =  (($rate/2)-$totalSSSPiPh);
				}
   			   $query = $this->db->query("select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".$base." order by salarybase DESC limit 1 ");
			  return $query->result();
 			  //echo json_encode($query->result());
  		} 
 		  public function BIRComputeTest(){ //test Only
		 
			   $empid=100;
				$base = 5029.65;
				 
   			   $query = $this->db->query("select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".$base." order by salarybase DESC limit 1 ");
 
				echo "select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".$base." order by salarybase DESC limit 1 ";
 			 echo json_encode($query->result());
  		} 
 	  public function BIRCompute2(){
		 
			   $empid=484;
			   $rate=7500;
			   $totalSSSPiPh=186.25; 
  			   $tots = $totalSSSPiPh+(($rate/2)*.02);
  			   $query = $this->db->query("select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".(($rate/2)-$tots)." order by salarybase DESC limit 1 ");
			    echo "select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".(($rate/2)-$tots)." order by salarybase DESC limit 1 ";
				echo "<br>";
			 var_dump($query->result_array());
			 
  		}   
		 
 	public function adjustSalary($empid,$payrollCover){
			$query = $this->db->query("select emp_id,group_concat(concat(description,'|',additionname,'=',value)) as adjust from tbl_payroll_addition a,tbl_payroll_emp_addition b,tbl_payroll_coverage c where a.paddition_id=b.paddition_id and c.coverage_id=b.coverage_id and daterange ='".$payrollCover."' and emp_id=".$empid);
			 return $query->result();			  
  		}
		public function deductSalary($empid,$payrollCover){
			$query = $this->db->query("select emp_id,group_concat(concat(deductionname,'=',value)) as deduct from tbl_payroll_deductions a,tbl_payroll_coverage b, tbl_payroll_emp_deduction c where a.pdeduct_id=c.pdeduct_id and b.coverage_id=c.coverage_id and value>0 and daterange ='".$payrollCover."' and c.emp_id= ".$empid);
			 return $query->result();			  
  		}
	/* 	 	public function isLoginHoliday($login,$timeStart){
			$year = date_format(date_create($login),"Y");
			  $date = date_format(date_create($login),"Y-m-d");
			  $timeStart = date_format(date_create($timeStart),"h:i:s");
 			
	$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");

			return $query->result();
  		} */
				 public function isLoginHoliday($login,$timeStart){
 			$year = date_format(date_create($login),"Y");
			$date = date_format(date_create($login),"Y-m-d");
			$timeStart = date_format(date_create($timeStart),"h:i:s");
 			
			$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
  			 return $query->result();
			//echo json_encode($query->result());
		}
			 public function isLoginHoliday2(){
				  $login = "2017-12-25 2:00:00 AM";
				 $timeStart = "2:00:00 AM"; 
			$year = date_format(date_create($login),"Y");
			$date = date_format(date_create($login),"Y-m-d");
			$timeStart = date_format(date_create($timeStart),"h:i:s");
 			
			$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
			echo "select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')";
 			// return $query->result();
			echo json_encode($query->result());
		}
		public function loginn2timeID($time_id){
		 			$query = $this->db->query("SELECT * FROM tbl_time where time_id=".$time_id);
			 return $query->result();

		}
		public function shiftTotalHr($time_id){
		 			$query = $this->db->query("SELECT * FROM tbl_time where time_id=".$time_id);
			 return $query->result();

		}
		public function loginnNdtimeID($time_id){
		 			$query = $this->db->query("SELECT b.* FROM `tbl_time` a,tbl_payroll_nd b  where a.time_id=b.time_id and a.time_id=".$time_id);
			 return $query->result();

		}
		public function isLogoutHoliday($logout,$timeEnd){
			  $year = date_format(date_create($logout),"Y");
			  $date = date_format(date_create($logout),"Y-m-d");
			  $timeEnd = date_format(date_create($timeEnd),"h:i:s");
 			
	// $query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
		$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");


			return $query->result();
  		}
			public function savePayroll(){
				$coverage_id = $this->input->post("coverage_id");
				$type = $this->input->post("emp");
				   $query = $this->db->query("delete from tbl_payroll where emp_type='".$type."' and coverage_id=".$coverage_id );
				$data = json_decode($this->input->post("dataArray"));
				// print_r($data);
				 foreach($data as $key => $val){
					foreach($val as $k => $v){
						//echo  $val[26]." =";
					}
					  // echo  "**".$val[21]." =";
					  // echo  json_encode($val);
  // $query = $this->db->query("insert into tbl_payroll(coverage_id,emp_promoteId,pagibig_id,sss_id,tax_id,philhealth_id,monthly,quinsina,clothing,laundry,rice,ot_bct,nd,hp,ho,sss_details,phic_details,hdmf_details,bir_details,adjust_add,adjust_minus,total_earning,total_deduction,final,total_hours,isActive,pemp_adjust_id,hour_ho,hour_hp,hour_nd) values(".$coverage_id.",'$val[0]',1,'$val[1]','$val[3]' ,'$val[2]' ,'$val[6]' ,'$val[7]' ,'$val[11]' ,'$val[12]' ,'$val[13]' ,'$val[14]' ,'$val[15]' ,'$val[16]' ,'$val[17]' ,'$val[18]' ,'$val[19]' ,'$val[20]' ,'$val[21]' ,'$val[22]' ,'$val[23]' ,'$val[24]' ,'$val[25]' ,'$val[26]' ,'$val[10]',1,'$val[27]','$val[28]','$val[29]','$val[30]')");
					  if(count($data)>0){
						$isDaily= ($val[37]=="Monthly") ? 0 : 1;
					  $query = $this->db->query("insert into tbl_payroll(coverage_id,emp_promoteId,pagibig_id,sss_id,tax_id,philhealth_id,monthly,quinsina,clothing,laundry,rice,ot_bct,nd,hp,ho,sss_details,phic_details,hdmf_details,bir_details,adjust_add,adjust_minus,total_earning,total_deduction,final,total_hours,isActive,pemp_adjust_id,hour_ho,hour_hp,hour_nd,bonus,emp_type,account,adjust_details,deduct_details,absentLate,bonus_details,isDaily) values(".$coverage_id.",'$val[0]',1,'$val[1]','$val[3]' ,'$val[2]' ,'$val[6]' ,'$val[7]' ,'$val[11]' ,'$val[12]' ,'$val[13]' ,'$val[15]' ,'$val[16]' ,'$val[17]' ,'$val[18]' ,'$val[19]' ,'$val[20]' ,'$val[21]' ,'$val[22]' ,'$val[23]' ,'$val[24]' ,'$val[25]' ,'$val[26]' ,'$val[27]' ,'$val[10]',1,'$val[28]','$val[29]','$val[30]','$val[31]','$val[14]','$type','$val[32]','$val[33]','$val[34]','$val[35]','$val[36]',$isDaily)");
					  }
				 }
		}
			public function exportPayrollP2($coverage=null,$type2=null){
 		 // $query = $this->db->query("SELECT count(*) FROM `tbl_payroll` where coverage_id= $coverage and emp_type='$type'");
			 $query = $this->db->query("SELECT * FROM `tbl_payroll` a,tbl_emp_promote b,tbl_pos_emp_stat c,tbl_employee d,tbl_applicant e ,tbl_position f where f.pos_id=c.pos_id and a.emp_promoteID=b.emp_promoteID and b.posempstat_id =c.posempstat_id and d.emp_id = b.emp_id and e.apid =d.apid and coverage_id= $coverage and emp_type='$type2'"); 
			 $coveraged = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$coverage);
				$tbl='
				Legends: <br>
				ND = NIGHT DIFFERENTIAL<br>
				HP = HAZARD PAY<br>
				HO = HOLIDAY<br>
				<br><br>
				';
				$tbl.='
					<style>
					table.blueTable {
						border: 1px solid #6d6d6d;
						border-spacing: 0px;
					}
 					td {
						border: 1px solid #6d6d6d;
						border-spacing: 0px;
					}
					thead {
						background: dimgrey;
						color: white;
						font-size: 12px;
					}
					</style>
				';
				$tbl.='
					 
					<table class="blueTable">
					<thead>
					<tr>
					<th>No</th>
					<th>CUSTOMER CARE AMBASSADORS</th>
					<th>POSITION</th>
					<th>ATM</th>
					<th>ACCOUNT</th>
					<th>BASIC SALARY</th>
					<th>CLOTHING ALLOWANC</th>
					<th>LAUNDRY ALLOWANCE</th>
					<th>RICE SUBSIDY</th>
					<th>ND</th>
					<th>OVERTIME-BCT</th>
					<th>HP</th>
					<th>HO</th>
					<th>BONUS</th>
					<th>ADJUSTMENT</th>
					<th>GROSS PAY</th>
					<th>TAX</th>
					<th>SSS</th>
					<th>HDMF</th>
					<th>PHIC</th>
					<th>ABSENCE/UT/LATE</th>
					<th>DEDUCTIONS</th>
					<th>NET PAY</th>
 					</tr>
					</thead>
					<tbody>';
					$no=1;
					foreach($query->result() as $row){
						$isATM = (($row->isAtm==1) ? "Yes" : "No");
						$account = explode("|",$row->account);
						$clothing = explode("(",$row->clothing);
						$laundry = explode("(",$row->laundry);
						$rice = explode("(",$row->rice);
						$adjust_details = explode(",",$row->adjust_details);
						$deduct_details = explode(",",$row->deduct_details);
						$bir = explode("(",$row->bir_details);

  						$tbl.='<tr>
						<td>'.$no.'</td>
						<td>'.$row->fname.", ".$row->lname.'</td>
						<td>'.$row->pos_name.'</td>
						<td>'.$isATM.'</td>
						<td>'.$account[1].'</td>
						<td>'.$row->quinsina.'</td>
						<td>'.$clothing[0].'</td>
						<td>'.$laundry[0].'</td>
						<td>'.$rice[0].'</td>
						<td>'.$row->nd.'</td>
						<td>'.$row->ot_bct.'</td>
						<td>'.$row->hp.'</td>
						<td>'.$row->ho.'</td>
						<td>';
							for($i=0;$i<count($adjust_details);$i++){ //AdjustSalary uploaded per payroll <NOT SET TO BONUS TABLE>
							$bonus1 = explode("|",$adjust_details[$i]);
							if(trim($bonus1[0])=="Bonus"){
							$tbl.=  $bonus1[1]."<br>";
							}
							}
						
						$tbl.='</td>
						<td>';
							for($i=0;$i<count($adjust_details);$i++){ //AdjustSalary  
							$bonus1 = explode("|",$adjust_details[$i]);
							if(trim($bonus1[0])!="Bonus"){
							 $tbl.=  $bonus1[1]."<br>";
							}
							}
						
						$tbl.='</td>
						<td>'.$row->total_earning.'</td>
						<td>'.$bir[0].'</td>
						<td>'.$row->sss_details.'</td>
						<td>'.$row->hdmf_details.'</td>
						<td>'.$row->phic_details.'</td>
						<td>'.$row->absentLate.'</td>
						<td>';
							for($i=0;$i<count($deduct_details);$i++){ //AdjustSalary uploaded per payroll <NOT SET TO BONUS TABLE>
							$tbl.= $deduct_details[$i]."<br>";
							 
							}
						
						$tbl.='</td>
						<td>'.$row->final.'</td>

						</tr>';
						$no++;
					}
					$tbl.='</tbody>
					</tr>
					</table>';
				echo $tbl;
			}
		
	public function exportPayrollP($coverage=null,$type2=null){
// $query = $this->db->query("SELECT count(*) FROM `tbl_payroll` where coverage_id= $coverage and emp_type='$type'");
$query = $this->db->query("SELECT * FROM `tbl_payroll` a,tbl_emp_promote b,tbl_pos_emp_stat c,tbl_employee d,tbl_applicant e ,tbl_position f where f.pos_id=c.pos_id and a.emp_promoteID=b.emp_promoteID and b.posempstat_id =c.posempstat_id and d.emp_id = b.emp_id and e.apid =d.apid and coverage_id= $coverage and emp_type='$type2' order by lname"); 
$coveraged = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$coverage);
$year= $coveraged->result()[0]->year;
$month= $coveraged->result()[0]->month; 
$period= $coveraged->result()[0]->period;
$type= "report";

$savefile2 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/$year/$month/$period/$type/"; 

$this->load->library('Pdf');

$arr = array();
 // create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// $pdf->SetProtection(array('print', 'copy','modify'), "123", "123", 0, null);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('SupportZebra Payslip - MJM');
$pdf->SetTitle('Payslip');
$pdf->SetSubject('Payslip');
$pdf->SetKeywords('Payslip, Payslip, Payslip, Payslip, Payslip');

  $PDF_HEADER_LOGO = "logo2.png";//any image file. check correct path.
$PDF_HEADER_LOGO_WIDTH = "50";
$PDF_HEADER_TITLE = "SupportZebra Payroll Report";
$PDF_HEADER_STRING = $coveraged->result()[0]->daterange ;

 $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE,$PDF_HEADER_STRING);
  
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, PDF_MARGIN_TOP, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->AddPage('L', 'A5');
$pdf->SetFont('helvetica', '', 4);
 $pdf->Multicell(0,2," \n \n "); 

				 
				 
				$tbl ='<h2>	LEGENDS:</h2> <br>
				<h3>ND = NIGHT DIFFERENTIAL</h3>
				<h3>HP = HAZARD PAY</h3>
				<h3>HO = HOLIDAY</h3>
				<h3>CCA = CUSTOMER CARE AMBASSADORS</h3><br>
				<h3>OT-BCT = OVERTIME BEFORE CALLING TIME</h3><br>
				<br><br>';
				$tbl.='
					<style  type="text/css">
					table.blueTable {
						border: 1px solid #6d6d6d;
						border-spacing: 0px;
					}
					td {
						border: 1px solid #6d6d6d;
						border-spacing: 0px;
						vertical-align:middle;
					}
										
						 
					 
					</style>
				';
				$tbl.='
				 <table class="blueTable">
					<thead >
					<tr bgcolor="#737373" style=" color: white;font-size: 7px;">
					<td>No</td>
					<td width="40px">CCA</td>
 					<td>POSITION</td>
					<td>ATM</td>
 					<td>BASIC <br>SALARY</td>
					<td>CLOTHING</td>
					<td>LAUNDRY </td>
					<td>RICE <br>SUBSIDY</td>
					<td>ND</td>
					<td>OT-BCT</td>
					<td>HP</td>
					<td>HO</td>
					<td>ALLOWANCE</td>
					<td>ADJUSTMENT</td>
					<td>GROSS PAY</td>
					<td>TAX</td>
					<td>SSS</td>
					<td>HDMF</td>
					<td>PHIC</td>
					<td>ABSENCE/<br>UT/ <br>LATE</td>
					<td>DEDUCTIONS</td>
					<td width="60px">NET <br>PAY</td>
 					</tr>
					</thead>
				';
					$no=1;
					$total=0;
					$nonATM=0;
					$wATM=0;
					$Tbonus=0;
					$Tbonus2=0;
					$Tadjust=0;
					$Tadjust2=0;
					$Tdeduct=0;
					$Tdeduct2=0;
					$TGross=0;
					$TTax=0;
					$Tsss=0;
					$Tphic=0;
					$Thdmf=0;
					$Taul=0;
					$Tho=0;
					$Thp=0;
					$Totbct=0;
					$Tnd=0;
					$Trice=0;
					$Tclothing=0;
					$Tlaundry=0;
					$Tq=0;
  					foreach($query->result() as $row){

						$isATM = (($row->isAtm==1) ? "Yes" : "No");
						$account = explode("|",$row->account);
						$clothing = explode("(",$row->clothing);
						$laundry = explode("(",$row->laundry);
						$rice = explode("(",$row->rice);
						$adjust_details = explode(",",$row->adjust_details);
						$bonus_details = explode(",",$row->bonus_details);
						$deduct_details = explode(",",$row->deduct_details);
						$bir = explode("(",$row->bir_details);
  						$tbl.='<tr nobr="true">
						<td >'.$no.'</td>
						<td width="40px"><h4><b>'.$row->lname.'</b></h4><h4>'.$row->fname.'</h4></td>
						<td style="font-size:6px;">'.$row->pos_name.'</td>
						<td style="font-size:6px;">'.$isATM.'</td>
						<td style="font-size:6px;">'.$row->quinsina.'</td>
						<td style="font-size:6px;">'.$clothing[0].'</td>
						<td style="font-size:6px;">'.$laundry[0].'</td>
						<td style="font-size:6px;">'.$rice[0].'</td>
						<td style="font-size:6px;">'.$row->nd.'</td>
						<td style="font-size:6px;">'.$row->ot_bct.'</td>
						<td style="font-size:6px;">'.$row->hp.'</td>
						<td style="font-size:6px;">'.$row->ho.'</td>';
							for($i=0;$i<count($adjust_details);$i++){ //AdjustSalary uploaded per payroll <NOT SET TO BONUS TABLE>
							$bonus1 = explode("|",$adjust_details[$i]);
							if(trim($bonus1[0])=="Bonus"){
							$bb = explode("=",$bonus1[1]);
								if($bb[1]!="0.00"){
								// $tbl.=  $bonus1[1]."<br>";
								$Tbonus+=(float)$bb[1];
								$Tbonus2+=(float)$bb[1];
								}
							}
							}
							
							for($i=0;$i<count($bonus_details);$i++){ //AdjustSalary uploaded per payroll <NOT SET TO BONUS TABLE>
 							 
							$bb2 = explode("=",$bonus_details[$i]);
								if($bb2[1]!="0.00"){
								// $tbl.=  $bonus1[1]."<br>";
								$Tbonus1st+=(float)$bb2[1];
								$Tbonus2nd+=(float)$bb2[1];
								}
							 
							}
						
						$tbl.='<td>'.($Tbonus2+$Tbonus2nd).'</td>';
						if(count($adjust_details)>=1){
							for($i=0;$i<count($adjust_details);$i++){ //AdjustSalary  
							$bonus1 = explode("|",$adjust_details[$i]);
							if(trim($bonus1[0])!="Bonus"){
								$bb = explode("=",$bonus1[1]);
								if($bb[1]!="0.00"){
									// $tbl.=  $bonus1[1]."<br>";
									$Tadjust+=(float)$bb[1];
									$Tadjust2+=(float)$bb[1];
								}
								}
							}
							}else{
							$Tadjust2+=0;
							}
						
						$tbl.='<td>'.$Tadjust2.'</td>
						<td style="font-size:6px;">'.number_format((float)$row->total_earning,2).'</td>
						<td style="font-size:6px;">'.$bir[0].'</td>
						<td style="font-size:6px;">'.$row->sss_details.'</td>
						<td style="font-size:6px;">'.$row->hdmf_details.'</td>
						<td style="font-size:6px;">'.$row->phic_details.'</td>
						<td style="font-size:6px;">'.$row->absentLate.'</td>';
						
							for($i=0;$i<count($deduct_details);$i++){ //AdjustSalary uploaded per payroll <NOT SET TO BONUS TABLE>
							// $tbl.= $deduct_details[$i]."<br>";
							
							 if(count($deduct_details)>=1){
							  $bb = explode("=",$deduct_details[$i]);
							  // print_r($bb);
								$Tdeduct+=(float)$bb[1];
								$Tdeduct2+=(float)$bb[1];
							 }
							}
						
						$tbl.='<td>'.$Tdeduct2.'</td>';
						
						$tbl.='<td style="font-size:6px;" width="60px">'.$row->final.'</td>
						</tr>';
						$Tdeduct2=0;
						$Tbonus2=0;
						$Tbonus2nd=0;
						$Tadjust2=0;
						$no++;
						$total+=$row->final;
						$TGross+=(float)$row->total_earning;
						$TTax+=(float)$bir[0];
						$Tsss+=(float)$row->sss_details;
						$Thdmf+=(float)$row->hdmf_details;
						$Tphic+=(float)$row->phic_details;
						$Taul+=(float)$row->absentLate;
						$Tho+=(float)$row->ho;
						$Thp+=(float)$row->hp;
						$Tnd+=(float)$row->nd;
						$Trice+=(float)$rice[0];
						$Tlaundry+=(float)$laundry[0];
						$Tclothing+=(float)$clothing[0];
						$Tq+=(float)$row->quinsina;
						$Totbct+=(float)$row->ot_bct;
						$nonATM += (($row->isAtm==0) ? $row->final : 0);
						$wATM += (($row->isAtm==1) ? $row->final : 0);

					}
						
					$tbl.='<tr>
 					<td colspan="4"></td>
					<td>'.number_format($Tq,2).'</td>  
					<td>'.number_format($Tclothing,2).'</td>  
					<td>'.number_format($Tlaundry,2).'</td>  
					<td>'.number_format($Trice,2).'</td>  
					<td>'.number_format($Tnd,2).'</td>  
					<td>'.number_format($Totbct,2).'</td>  
					<td>'.number_format($Thp,2).'</td>  
					<td>'.number_format($Tho,2).'</td>  
					<td>'.number_format($Tbonus,2).'</td>  
					<td>'.number_format($Tadjust,2).'</td>
					<td>'.number_format($TGross,2).'</td>
					<td>'.number_format($TTax,2).'</td>
					<td>'.number_format($Tsss,2).'</td>
					<td>'.number_format($Thdmf,2).'</td>
					<td>'.number_format($Tphic,2).'</td>
					<td>'.number_format($Taul,2).'</td>
					<td >'.number_format($Tdeduct,2).'</td>
					<td width="60px" colspan="2" bgcolor="yellow" style="font-size:7px;"><b>P '.number_format($total,2).'</b></td></tr>';
					$tbl.=' 
					 
					</table>';
			            ob_end_clean();
					$pdf->writeHTML($tbl, true, false, false, false, '');
					$pdf->lastPage();

					$pdf->AddPage('L', 'A5');
					$pdf->SetFont('helvetica', '', 4);
					 $pdf->Multicell(0,2," \n \n "); 

					$tbl='
					 
					<h2><u>SUMMARY:</u></h2> 
					<p style="font-size:10px">Employees with ATM: <span> <u>P '.number_format($wATM,2).'</u></span> </p>
					<p style="font-size:10px">Employees don\'t have ATM: <u>P '.number_format($nonATM,2).'</u></p>
					<p style="font-size:10px">Total Allowances: <u>P '.number_format($Tbonus,2).'</u></p>
					<p style="font-size:10px">Total Adjustments: <u>P '.number_format($Tadjust,2).'</u></p>
					<p style="font-size:10px">Total Gross Pay: <u>P '.number_format($TGross,2).'</u></p>
					<p style="font-size:10px">Total Deduction: <u>P '.number_format($Tdeduct,2).'</u></p>
					<p style="font-size:10px"><b>Total Net Pay: <u>P '.number_format($total,2).'</u><b></p>
					';
					$tbl.='<br><br><hr><br><br><table align="center">';
					$tbl.='<tr><td><span style="font-size:10px" align="center">Prepared By:</span></td><td colspan="2" align="center"><span style="font-size:10px">Checked By:</span></td><td><span style="font-size:10px" align="center">Approved By:</span></td></tr>';
					$tbl.='
					<tr><td></td><td></td><td></td><td></td></tr>
					<tr><td></td><td></td><td></td><td></td></tr>
					<tr><td></td><td></td><td></td><td></td></tr>
					<tr><td></td><td></td><td></td><td></td></tr>
					<tr>
					<td><h4>AGNES CLARA B. LAMCO</h4><h4><u>KORNELIA KRISHNA C. LADAO</u></h4><small>Compensation and Benefits</small></td>
					<td><h4><u>DOROTHY JOY D. DAGCUTA</u></h4><small>General Accounting III</small></td>
					<td><h4><u>SHENDIE CHATTO-VALLENTE</u></h4><small>Senior Finance Officer</small></td>
					<td rowspan="2"><h4><u>DR. NIÑO MAE V. DURAN</u></h4><small>Managing Director</small></td>
					</tr>';
					$tbl.='</table>';
					$pdf->writeHTML($tbl, true, false, false, false, '');
					$pdf->lastPage();

//Close and output PDF document
			$pdfDirectory = "reports/payroll/$year/$month/$period/report/"; 

			$pdf->Output($savefile2.'SZ_Payslip_'.strtoupper($type2).'_'.strtoupper($month).'_'.$period.'_'.$coverage.'.pdf', 'F');
			$arr["payslipPDF"][$row->emp_id] = array(
				"TCPDF" => $row->lname.", ".$row->fname.'|'.$pdfDirectory.'SZ_Payslip_'.strtoupper($row->lname).'_'.strtoupper($month).'_'.$period.'_'.$row->emp_id.'.pdf',
			);
 	
	echo json_encode($arr);
	 }
	 
	 public function deduct_details($dname,$account,$emp_type,$cid){
	 
		$query = $this->db->query("SELECT a.deduct_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   deduct_details like '%$dname%' and account='$account' and emp_type like '%$emp_type%'");
		$total=0;
		  foreach ($query->result() as $row){
				$deduct = explode(",",$row->deduct_details);	
				  for($i=0;$i<count($deduct);$i++){
					$deduct1 = explode("=",$deduct[$i]);	
						  if(trim($deduct1[0])==$dname){
							$total += $deduct1[1];
						  }
				  }
		  }
			  return $total;
		}
		public function add_details($dname,$account,$emp_type,$cid){
		$query = $this->db->query("SELECT a.adjust_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   adjust_details like '%$dname%' and account='$account' and emp_type like '%$emp_type%'");
		$total=0;
		  foreach ($query->result() as $row){
				$added = explode(",",$row->adjust_details);	
				  for($i=0;$i<count($added);$i++){
				  $add = explode("|",$added[$i]);
					$add1 = explode("=",$add[1]);	
						  if(trim($add1[0])==$dname){
							$total += $add1[1];
						  }
				  }
		  }
			  return $total;
		}
		public function bonus_details($bname,$account,$emp_type,$cid){
	 
		$query = $this->db->query("SELECT a.bonus_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   bonus_details like '%$bname%' and account='$account' and emp_type like '%$emp_type%'");
		$total=0;
		  foreach ($query->result() as $row){
				$deduct = explode(",",$row->bonus_details);	
				  for($i=0;$i<count($deduct);$i++){
					$deduct1 = explode("=",$deduct[$i]);	
						  if(trim($deduct1[0])==$bname){
							$total += $deduct1[1];
						  }
				  }
		  }
			  return $total;
		}
		public function payroll($bname,$account,$emp_type,$cid){
	 
		$query = $this->db->query("SELECT a.".$bname." FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid  and account='$account' and emp_type like '%$emp_type%'");
		$total=0;
		  foreach ($query->result() as $row){
 						   if($bname=="bir_details"){
							$bir=explode("(",$row->$bname);
							   $total += $bir[0];
							   }else{
							$total += $row->$bname;

							   }
						  
 		  }
			  return $total;
		}
		/**********************************************REPORTS********************************************************/
		public function report2_test(){ //test only
			if($this->session->userdata('uid')){
						$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/payroll_report_detail' and is_assign=1 order by lname");

							$per_id =  $this->db->query("select GROUP_CONCAT(coverage_id,'|',daterange) as daterange,month from tbl_payroll_coverage where coverage_id>=89 group by month");
							$uid =  $this->db->query("select password from tbl_user where uid=".$this->session->userdata('uid'));
							foreach ($per_id->result() as $row){
									$arr['period'][$row->month] = array(
									"daterange" => $row->daterange,
									"month" => $row->month,
									);
							}
							foreach ($uid->result() as $row){
									$arr['user_id'] = array(
									"user_id" => $row->password,
									);
							}	
					if ($qry->num_rows() > 0){
							$this->load->view('payroll_report_report2_test',$arr);
						 
					}else{
						redirect('index.php/home/error');
					}
			}else{
						redirect('index.php/login');
			}
		}
		
		public function testGG2(){ //test only
			$payrollC = $this->input->post('payrollC');
			$empzType = $this->input->post('empzType');
			$acc = $this->db->query("SELECT a.*,d.fname,d.lname,c.emp_id, (select count(*) from tbl_payroll z where emp_type like '%".$empzType."%' and coverage_id=$payrollC and z.account=a.account) as headcount FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and emp_type like '%".$empzType."%' and coverage_id=$payrollC");
			// echo "SELECT a.*,d.fname,d.lname,c.emp_id, (select count(*) from tbl_payroll z where emp_type like '%".$empzType."%' and coverage_id=$payrollC and z.account=a.account) as headcount FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and emp_type like '%".$empzType."%' and coverage_id=$payrollC";
 			$deduct = $this->db->query("SELECT * FROM `tbl_payroll_deductions`");
 			$addition = $this->db->query("SELECT * FROM `tbl_payroll_addition`");
 			$bonus = $this->db->query("SELECT * FROM `tbl_bonus`");
			$other = array("quinsina","clothing","laundry","rice","ot_bct","nd","hp","ho","sss_details","phic_details","hdmf_details","bir_details");
			$other_head = array("QUINSINA","CLOTHING","LAUNDRY","RICE","OT-BCT","NIGHT PREMIUM","HAZARD PAY","HOLIDAY","SSS","PHIC","HDMF","BIR");
			// $acc = $this->db->query("SELECT * FROM `tbl_account`");
			  $total = 0;
			  if($acc->num_rows()>0){
			   foreach ($acc->result() as $row1){
				for($i=0;$i<count($other);$i++){
				$typ = explode("-",$row1->emp_type);
					$dd = $this->payroll($other[$i],$row1->account,$typ[0],$row1->coverage_id);
					
						 $arr["accounts"][ucfirst($typ[0])][trim($row1->account)."|".trim($row1->headcount)][$other[$i]] = array(
							  "total" =>  number_format($dd,2,".",","),
							  
						);
						$arr["header"][$other_head[$i]]= Array($other_head[$i]);
					$total=0;
				}
				}
				foreach ($acc->result() as $row1){
				foreach ($addition->result() as $row){
				$typ = explode("-",$row1->emp_type);
					$dd = $this->add_details($row->additionname,$row1->account,$typ[0],$row1->coverage_id);
					
						 $arr["accounts"][ucfirst($typ[0])][trim($row1->account)."|".trim($row1->headcount)][$row->additionname] = array(
							  "total" =>  number_format($dd,2,".",","),
							  
						);
						$arr["header"][$row->additionname]= Array($row->additionname);
					$total=0;
				}
				}   
			  foreach ($acc->result() as $row1){
				foreach ($bonus->result() as $row){
				$typ = explode("-",$row1->emp_type);
					$dd = $this->bonus_details($row->bonus_name,$row1->account,$typ[0],$row1->coverage_id);
					
						 $arr["accounts"][ucfirst($typ[0])][trim($row1->account)."|".trim($row1->headcount)][$row->bonus_name] = array(
							  "total" =>  number_format($dd,2,".",","),
							  
						);
						$arr["header"][$row->bonus_name]= Array($row->bonus_name);
					$total=0;
				}
				}
			      foreach ($acc->result() as $row1){
				foreach ($deduct->result() as $row){
				$typ = explode("-",$row1->emp_type);
					$dd = $this->deduct_details($row->deductionname,$row1->account,$typ[0],$row1->coverage_id);
					
						 $arr["accounts"][ucfirst($typ[0])][trim($row1->account)."|".trim($row1->headcount)][$row->deductionname] = array(
							  "total" =>  number_format($dd,2,".",","),
							  
						);
						$arr["header"][$row->deductionname]= Array($row->deductionname);
					$total=0;
				}
			  }  
			  
			 
			  echo json_encode($arr);}
			  else{
				  
				  echo 0;
				  }
		}
		public function report1(){
			if($this->session->userdata('uid')){
						$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/payroll_report_detail' and is_assign=1 order by lname");

							$per_id =  $this->db->query("select GROUP_CONCAT(coverage_id,'|',daterange) as daterange,month from tbl_payroll_coverage where coverage_id>=89 group by month");
							$uid =  $this->db->query("select password from tbl_user where uid=".$this->session->userdata('uid'));
							foreach ($per_id->result() as $row){
									$arr['period'][$row->month] = array(
									"daterange" => $row->daterange,
									"month" => $row->month,
									);
							}
							foreach ($uid->result() as $row){
									$arr['user_id'] = array(
									"user_id" => $row->password,
									);
							}	
					if ($qry->num_rows() > 0){
							$this->load->view('payroll_report1',$arr);
						 
					}else{
						redirect('index.php/home/error');
					}
			}else{
						redirect('index.php/login');
			}
		}
		public function testGG(){
			$payrollC = $this->input->post('payrollC');
			$empzType = $this->input->post('empzType');
			$acc = $this->db->query("SELECT a.*,d.fname,d.lname,c.emp_id, (select count(*) from tbl_payroll z where emp_type like '%".$empzType."%' and coverage_id=$payrollC and z.account=a.account) as headcount FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and emp_type like '%".$empzType."%' and coverage_id=$payrollC");
			// echo "SELECT a.*,d.fname,d.lname,c.emp_id, (select count(*) from tbl_payroll z where emp_type like '%".$empzType."%' and coverage_id=$payrollC and z.account=a.account) as headcount FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and emp_type like '%".$empzType."%' and coverage_id=$payrollC";
 			$deduct = $this->db->query("SELECT * FROM `tbl_payroll_deductions`");
			// $acc = $this->db->query("SELECT * FROM `tbl_account`");
			  $total = 0;
			  if($acc->num_rows()>0){
			  foreach ($acc->result() as $row1){
				foreach ($deduct->result() as $row){
				$typ = explode("-",$row1->emp_type);
					$dd = $this->deduct_details($row->deductionname,$row1->account,$typ[0],$row1->coverage_id);
					
						 $arr["accounts"][ucfirst($typ[0])][trim($row1->account)."|".trim($row1->headcount)][$row->deductionname] = array(
							  "total" => $dd,
							  
						);
						$arr["header"][$row->deductionname]= Array($row->deductionname);
					$total=0;
				}
			  }
			  echo json_encode($arr);}
			  else{
				  
				  echo 0;
				  }
		}
	}

 