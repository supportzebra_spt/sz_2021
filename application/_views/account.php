<!DOCTYPE html> 
<html  lang="en">

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
            body { padding-right: 0 !important }
        </style>


        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/fontawesome/fontawesome.css"> -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">

        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });

        </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-tabletools.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datatable/datatable-reorder.js"></script>

        <script type="text/javascript">

                $(document).ready(function () {
                    var table = $('#datatable-tabletools').DataTable();

                    $('.dataTables_filter input').attr("placeholder", "Search...");

                });


        </script>
    </head>

    <body>
        <div id="sb-site">
            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <div id="page-header" class="bg-black">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>

                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">


                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div id="page-title">
                            <h2>Account</h2><hr>
                            <div class="example-box-wrapper">
                                <div class="content-box">
                                    <h3 class="content-box-header bg-black">
                                        <i class="glyph-icon icon-cog"></i>
                                        Account List
                                        <div class="header-buttons-separator">
                                            <a href="#" class="icon-separator" data-toggle="modal" data-target="#myModal">
                                                <i class="glyph-icon icon-plus"></i>
                                            </a>
                                        </div>
                                    </h3>
                                    <div class="content-box-wrapper">
                                        <table id="datatable-tabletools" class="table table-striped table-bordered responsive no-wrap table-condensed">
                                            <thead>
                                                <tr>
                                                    <th>Account Name</th>
                                                    <th>Classification</th>
                                                    <th>Deparment</th>
                                                    <th>Default Shift</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id='tbody'>
                                                <?php foreach ($accounts as $acc): ?>
                                                    <tr>
                                                        <td><?php echo $acc->acc_name; ?></td>
                                                        <td><?php echo $acc->acc_description; ?></td>
                                                        <td><?php echo $acc->dep_details; ?></td>
                                                        <td><?php echo $acc->time_start . ' - ' . $acc->time_end; ?></td>
                                                        <td class='text-center'>
                                                            <a href="#" class="btn btn-xs btn-info" data-accid="<?php echo $acc->acc_id; ?>"  data-toggle="modal" data-target="#updateModal">
                                                                <span class="glyph-icon icon-separator">
                                                                    <i class="glyph-icon icon-edit"></i>
                                                                </span>
                                                                <span class="button-content">
                                                                    Edit
                                                                </span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">CREATE NEW ACCOUNT</h4>
                </div>
                <div class="modal-body">
                    <div class="panel">
                        <div class="panel-body">
                            <fieldset>
                                <legend>ACCOUNT DETAILS:</legend>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Account Name:</label> <span class='font-red font-bold' id='acc_name_n' style='display:none'>Required *</span>
                                            <input type="text" class='form-control' id='acc_name' name="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Classification:</label> <span class='font-red font-bold' id='acc_description_n' style='display:none'>Required *</span>
                                            <select class='form-control' id='acc_description'>
                                                <option value=''>--</option>
                                                <option value='Admin'>Admin</option>
                                                <option value='Agent'>Agent</option>
                                            </select>
                                        </div>
                                    </div>
									<div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Account Level:</label> <span class='font-red font-bold' id='uacc_name_n' style='display:none'>Required *</span>
                                            <input type="number" min="1" max="5" class='form-control' id='acc_level' name="" required>
                                        </div>
                                    </div>
									<div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">Account Reference:</label> <span class='font-red font-bold' id='uacc_reference_n' style='display:none'>Required *</span>
                                            <select class='form-control' id="acc_reference">
                                            </select>
                                        </div>
                                    </div>
									<div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">BCT</label> <span class='font-red font-bold' id='uacc_reference_n' style='display:none'>Required *</span>
                                            <input type="text" class='form-control' id='acc_bct' name="">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-body">
                            <fieldset>
                                <legend>OTHER DETAILS:</legend>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Department:</label> <span class='font-red font-bold' id='acc_department_n'  style='display:none'>Required *</span>
                                            <select class='form-control' id='acc_department'>
                                                <option value=''>--</option>
                                                <?php foreach ($departments as $dep): ?>
                                                    <option value='<?php echo $dep->dep_id; ?>'><?php echo $dep->dep_details; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Default Sched:</label> <span class='font-red font-bold' id='acc_defaultsched_n'  style='display:none'>Required *</span>
                                            <select class='form-control' id='acc_defaultsched'>
                                                <option value=''>--</option>
                                                <?php foreach ($schedules as $sched): ?>
                                                    <option value='<?php echo $sched->time_id; ?>'><?php echo $sched->time_start . ' - ' . $sched->time_end; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id='addaccount'>Create</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">UPDATE ACCOUNT</h4>
                </div>
                <div class="modal-body">
                    <div class="panel">
                        <div class="panel-body">
                            <fieldset>
                                <legend>ACCOUNT DETAILS:</legend>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Account Name:</label> <span class='font-red font-bold' id='uacc_name_n' style='display:none'>Required *</span>
                                            <input type="text" class='form-control' id='uacc_name' name="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Classification:</label> <span class='font-red font-bold' id='uacc_description_n' style='display:none'>Required *</span>
                                            <select class='form-control' id='uacc_description'>
                                                <option value=''>--</option>
                                                <option value='Admin'>Admin</option>
                                                <option value='Agent'>Agent</option>
                                            </select>
                                        </div>
                                    </div>
									<div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Account Level:</label> <span class='font-red font-bold' id='uacc_name_n' style='display:none'>Required *</span>
                                            <input type="number" min="1" max="5" class='form-control' id='uacc_level' name="" required>
                                        </div>
                                    </div>
									<div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">Account Reference:</label> <span class='font-red font-bold' id='uacc_reference_n' style='display:none'>Required *</span>
                                            <select class='form-control ' id="uacc_reference">
                                                <option value=''>--</option>
                                            </select>
                                        </div>
                                    </div>
									<div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">BCT</label> <span class='font-red font-bold' id='uacc_reference_n' style='display:none'>Required *</span>
                                            <input type="text" class='form-control' id='uacc_bct' name="">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-body">
                            <fieldset>
                                <legend>OTHER DETAILS:</legend>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Department:</label> <span class='font-red font-bold' id='uacc_department_n'  style='display:none'>Required *</span>
                                            <select class='form-control' id='uacc_department'>
                                                <option value=''>--</option>
                                                <?php foreach ($departments as $dep): ?>
                                                    <option value='<?php echo $dep->dep_id; ?>'><?php echo $dep->dep_details; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Default Sched:</label> <span class='font-red font-bold' id='uacc_defaultsched_n'  style='display:none'>Required *</span>
                                            <select class='form-control' id='uacc_defaultsched'>
                                                <option value=''>--</option>
                                                <?php foreach ($schedules as $sched): ?>
                                                    <option value='<?php echo $sched->time_id; ?>'><?php echo $sched->time_start . ' - ' . $sched->time_end; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id='uaddaccount'>Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- JS Demo -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
</body>
<script>
                $(function () {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
                        cache: false,
                        success: function (html) {

                            $('#headerLeft').html(html);
                            //alert(html);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
                        cache: false,
                        success: function (html) {

                            $('#sidebar-menu').html(html);
                            // alert(html);
                        }
                    });
                });
</script>
<script type="text/javascript">
        $(function () {
            $("#addaccount").click(function () {
                var acc_name = $("#acc_name").val().trim();
                var acc_description = $("#acc_description").val();
                var dep_id = $("#acc_department").val();
                var time_id = $("#acc_defaultsched").val();
				var acc_level = $("#acc_level").val();
                var acc_bct = $("#acc_bct").val();
                var acc_reference = $("#acc_reference").val();

                // alert(dep_id);
                if (acc_name == '') {
                    $("#acc_name_n").show();
                } else {
                    $("#acc_name_n").hide();
                }
                if (acc_description == '') {
                    $("#acc_description_n").show();
                } else {
                    $("#acc_description_n").hide();
                }
                if (dep_id == '') {
                    $("#acc_department_n").show();
                } else {
                    $("#acc_department_n").hide();
                }
                if (time_id == '') {
                    $("#acc_defaultsched_n").show();
                } else {
                    $("#acc_defaultsched_n").hide();
                }
                if (acc_name != '' && acc_description != '' && dep_id != '' && time_id != '') {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Schedule/createAccount",
                        data: {
                            acc_name: acc_name,
                            acc_description: acc_description,
                            dep_id: dep_id,
                            time_id: time_id,
							uacc_level: acc_level,
                            uacc_bct: acc_bct,
                            uacc_reference: acc_reference,

                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            if (res === 'Success') {
                                swal(
                                        'Success!!',
                                        'Successfully created a new account',
                                        'success'
                                        );
                            } else if (res === 'Check') {
                                swal(
                                        'Success!!',
                                        'Please check the shift and their respective breaks',
                                        'success'
                                        );
                            } else {
                                swal(
                                        'Error!!',
                                        'Failed to create account',
                                        'error'
                                        );
                            }
                            $("#myModal .close").click();
                            window.setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                    });
                }

            });
        })
</script>
<script type="text/javascript">
	function getAllAccount(){
	 $.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/Schedule/getAllAccount",
			cache: false,
			success: function (res) {
			   var accounts = $.parseJSON(res.trim());
			    var optionsAsString = "<option value='0'></option>";
			  $.each(accounts, function(index, value) {
                  optionsAsString += "<option value='"+value.acc_id+"'>"+value.acc_name+"</option>";
              });
			$("#uacc_reference").html(optionsAsString);
			$("#acc_reference").html(optionsAsString);
			// $('.uacc_reference').trigger('chosen:updated');  
			}
		});

	}
        $(function () {
		getAllAccount();
            $("#uaddaccount").click(function () {
                var acc_name = $("#uacc_name").val().trim();
                var acc_description = $("#uacc_description").val();
                var dep_id = $("#uacc_department").val();
                var time_id = $("#uacc_defaultsched").val();
                var uacc_level = $("#uacc_level").val();
                var uacc_bct = $("#uacc_bct").val();
                var uacc_reference = $("#uacc_reference").val();
                var acc_id = $(this).data('acc_id');
                // alert(dep_id);
                if (acc_name == '') {
                    $("#uacc_name_n").show();
                } else {
                    $("#uacc_name_n").hide();
                }
                if (acc_description == '') {
                    $("#uacc_description_n").show();
                } else {
                    $("#uacc_description_n").hide();
                }
                if (dep_id == '') {
                    $("#uacc_department_n").show();
                } else {
                    $("#uacc_department_n").hide();
                }
                if (time_id == '') {
                    $("#uacc_defaultsched_n").show();
                } else {
                    $("#uacc_defaultsched_n").hide();
                }
                if (acc_name != '' && acc_description != '' && dep_id != '' && time_id != '') {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Schedule/updateAccount",
                        data: {
                            acc_name: acc_name,
                            acc_description: acc_description,
                            dep_id: dep_id,
                            time_id: time_id,
                            uacc_level: uacc_level,
                            uacc_bct: uacc_bct,
                            uacc_reference: uacc_reference,
                            acc_id: acc_id
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            if (res == 'Success') {
                                swal(
                                        'Success!!',
                                        'Successfully updated account',
                                        'success'
                                        );
							$("#updateModal .close").click();			
							window.setTimeout(function () {
                                location.reload();
                            }, 2000);
                            } else {
                                swal(
                                        'Error!!',
                                        'Failed to update account',
                                        'error'
                                        )
                            }
                            
                            
                        }
                    });
                }

            });
        })
</script>
<script type="text/javascript">
        $('#updateModal').on('show.bs.modal', function (e) {
            var acc_id = $(e.relatedTarget).data('accid');
            $("#uaddaccount").data('acc_id', acc_id);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/index.php/Schedule/getAccountSpecific",
                data: {
                    acc_id: acc_id
                },
                cache: false,
                success: function (res) {
                    res = res.trim();
                    res = JSON.parse(res);
                    $("#uacc_name").val(res.acc_name);
                    $("#uacc_description").val(res.acc_description);
                    $("#uacc_department").val(res.dep_id);
                    $("#uacc_defaultsched").val(res.time_id);
                    $("#uacc_bct").val(res.beforeCallingTime);
                    $("#uacc_level").val(res.accountLevel);
                      $("#uacc_reference").val(res.accountReference);
					// $('.uacc_reference option[value='+res.accountReference+']').attr('selected','selected');
					// $('.uacc_reference').trigger('chosen:updated');  

                }
            });
        });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>