<!DOCTYPE html>
<html lang="en">
<?php
	if(empty($schedOut)){
		redirect("index.php/dtr");
		}
	
	?>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/lockscreen-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
<head>
    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>
    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> SupportZebra | Break</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="../../assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="../../assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.css">


<!-- HELPERS -->

<link rel="stylesheet" type="text/css" href="../../assets/helpers/animate.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/backgrounds.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/boilerplate.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/border-radius.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/grid.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/page-transitions.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/spacing.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/typography.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/utils.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/colors.css">

<!-- ELEMENTS -->

<link rel="stylesheet" type="text/css" href="../../assets/elements/badges.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/buttons.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/content-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/dashboard-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/forms.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/images.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/info-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/invoice.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/loading-indicators.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/menus.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/panel-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/response-messages.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/responsive-tables.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/ribbon.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/social-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/tables.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/tile-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/elements/timeline.css">



<!-- ICONS -->

<link rel="stylesheet" type="text/css" href="../../assets/icons/fontawesome/fontawesome.css">
<link rel="stylesheet" type="text/css" href="../../assets/icons/linecons/linecons.css">
<link rel="stylesheet" type="text/css" href="../../assets/icons/spinnericon/spinnericon.css">


<!-- WIDGETS -->

<link rel="stylesheet" type="text/css" href="../../assets/widgets/accordion-ui/accordion.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/calendar/calendar.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/carousel/carousel.css">

<link rel="stylesheet" type="text/css" href="../../assets/widgets/charts/justgage/justgage.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/charts/morris/morris.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/charts/piegage/piegage.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/charts/xcharts/xcharts.css">

<link rel="stylesheet" type="text/css" href="../../assets/widgets/chosen/chosen.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/colorpicker/colorpicker.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/datatable/datatable.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/datepicker/datepicker.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/datepicker-ui/datepicker.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/daterangepicker/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/dialog/dialog.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/dropdown/dropdown.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/file-input/fileinput.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/input-switch/inputswitch.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/input-switch/inputswitch-alt.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/ionrangeslider/ionrangeslider.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/jcrop/jcrop.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/jgrowl-notifications/jgrowl.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/loading-bar/loadingbar.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/maps/vector-maps/vectormaps.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/markdown/markdown.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/modal/modal.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-upload/fileupload.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/nestable/nestable.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/noty-notifications/noty.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/popover/popover.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/pretty-photo/prettyphoto.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/progressbar/progressbar.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/range-slider/rangeslider.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/slidebars/slidebars.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/slider-ui/slider.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/summernote-wysiwyg/summernote-wysiwyg.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/tabs-ui/tabs.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/theme-switcher/themeswitcher.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/timepicker/timepicker.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/tocify/tocify.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/tooltip/tooltip.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/touchspin/touchspin.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/uniform/uniform.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/wizard/wizard.css">
<link rel="stylesheet" type="text/css" href="../../assets/widgets/xeditable/xeditable.css">

<!-- SNIPPETS -->

<link rel="stylesheet" type="text/css" href="../../assets/snippets/chat.css">
<link rel="stylesheet" type="text/css" href="../../assets/snippets/files-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/snippets/login-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/snippets/notification-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/snippets/progress-box.css">
<link rel="stylesheet" type="text/css" href="../../assets/snippets/todo.css">
<link rel="stylesheet" type="text/css" href="../../assets/snippets/user-profile.css">
<link rel="stylesheet" type="text/css" href="../../assets/snippets/mobile-navigation.css">

<!-- APPLICATIONS -->

<link rel="stylesheet" type="text/css" href="../../assets/applications/mailbox.css">

<!-- Admin theme -->

<link rel="stylesheet" type="text/css" href="../../assets/themes/admin/layout.css">
<link rel="stylesheet" type="text/css" href="../../assets/themes/admin/color-schemes/default.css">

<!-- Components theme -->

<link rel="stylesheet" type="text/css" href="../../assets/themes/components/default.css">
<link rel="stylesheet" type="text/css" href="../../assets/themes/components/border-radius.css">

<!-- Admin responsive -->

<link rel="stylesheet" type="text/css" href="../../assets/helpers/responsive-elements.css">
<link rel="stylesheet" type="text/css" href="../../assets/helpers/admin-responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">

    <!-- JS Core -->

    <script type="text/javascript" src="../../assets/js-core/jquery-core.js"></script>
    <script type="text/javascript" src="../../assets/js-core/jquery-ui-core.js"></script>
    <script type="text/javascript" src="../../assets/js-core/jquery-ui-widget.js"></script>
    <script type="text/javascript" src="../../assets/js-core/jquery-ui-mouse.js"></script>
    <script type="text/javascript" src="../../assets/js-core/jquery-ui-position.js"></script>
    <!--<script type="text/javascript" src="../../assets/js-core/transition.js"></script>-->
    <script type="text/javascript" src="../../assets/js-core/modernizr.js"></script>
    <script type="text/javascript" src="../../assets/js-core/jquery-cookie.js"></script>
	
	
<style>
.time ul{
	  list-style-type: none;
    margin-left: -39px;
	}
	.note{
	font-size: initial;
    background: #4a4a4a;
    padding: 10px;
}
	 
	.timeDetails{
	    padding: 10px;
    background: gray;
	}
	.time{
    border-radius: 13px;
    color: white;
    font-family: fantasy;
    display: inline-flex;
	}
	 </style>


    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>



</head>
<body>
<div id="loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<style>
    html,body {
        height: 100%;
    }
    body {
        overflow: hidden;
        background: #fafafa;
    }
</style>

<script type="text/javascript" src="../../assets/widgets/wow/wow.js"></script>
<script type="text/javascript">
    /* WOW animations */

    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
</script>
 					<span id="acc_time_id" style="display:none"><?php echo $schedOut["acc_time_id"]; ?></span>
					<span id="dtr_id" style="display:none"><?php echo $schedOut["dtr_id"]; ?></span>

<div class="center-vertical">
    <div class="center-content row">
        <div class="col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin">
            <div class="panel-layout wow bounceInDown">
                <div class="panel-content pad0A bg-white">
                    <div class="meta-box meta-box-offset">
                        <img src="../../assets/images/SZ_logo.png" alt="" class="meta-image img-bordered img-circle" style="width: 122px;">
                        <h3 class="meta-heading font-size-16"><?php echo $_SESSION["fname"]." ".$_SESSION["lname"]?></h3>
                       <h1 id="time-elapsed"> </h1> 
                    </div>
                    <form action="#" class="form-inline" method="">
                        <div class="content-box-wrapper pad20A">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="Password" placeholder="Password" id="txtPassword" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button" id="breakBack"><i class="glyph-icon icon-unlock-alt"></i> Back</button>
                                    </span>
                                 </div>
								<span class="text-danger" id="message"> </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


    <!-- WIDGETS -->

<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.js"></script>

<!-- Bootstrap Dropdown -->

<!-- <script type="text/javascript" src="../../assets/widgets/dropdown/dropdown.js"></script> -->

<!-- Bootstrap Tooltip -->

<!-- <script type="text/javascript" src="../../assets/widgets/tooltip/tooltip.js"></script> -->

<!-- Bootstrap Popover -->

<!-- <script type="text/javascript" src="../../assets/widgets/popover/popover.js"></script> -->

<!-- Bootstrap Progress Bar -->

<script type="text/javascript" src="../../assets/widgets/progressbar/progressbar.js"></script>

<!-- Bootstrap Buttons -->

<!-- <script type="text/javascript" src="../../assets/widgets/button/button.js"></script> -->

<!-- Bootstrap Collapse -->

<!-- <script type="text/javascript" src="../../assets/widgets/collapse/collapse.js"></script> -->

<!-- Superclick -->

<script type="text/javascript" src="../../assets/widgets/superclick/superclick.js"></script>

<!-- Input switch alternate -->

<script type="text/javascript" src="../../assets/widgets/input-switch/inputswitch-alt.js"></script>

<!-- Slim scroll -->

<script type="text/javascript" src="../../assets/widgets/slimscroll/slimscroll.js"></script>

<!-- Slidebars -->

<script type="text/javascript" src="../../assets/widgets/slidebars/slidebars.js"></script>
<script type="text/javascript" src="../../assets/widgets/slidebars/slidebars-demo.js"></script>

<!-- PieGage -->

<script type="text/javascript" src="../../assets/widgets/charts/piegage/piegage.js"></script>
<script type="text/javascript" src="../../assets/widgets/charts/piegage/piegage-demo.js"></script>

<!-- Screenfull -->

<script type="text/javascript" src="../../assets/widgets/screenfull/screenfull.js"></script>

<!-- Content box -->

<script type="text/javascript" src="../../assets/widgets/content-box/contentbox.js"></script>

<!-- Overlay -->

<script type="text/javascript" src="../../assets/widgets/overlay/overlay.js"></script>

<!-- Widgets init for demo -->

<script type="text/javascript" src="../../assets/js-init/widgets-init.js"></script>

<!-- Theme layout -->

<script type="text/javascript" src="../../assets/themes/admin/layout.js"></script>

<!-- Theme switcher -->

<script src="../../assets/widgets/theme-switcher/themeswitcher.js"></script>
<script src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
<script src="<?php echo base_url(); ?>assets/idle-timer/idle-timer.min.js"></script>

</body>
<script>
	function action(){
	swal({
	  title: "Idle",
	  text: "You've been inactive lately..Please choose an action to proceed.",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Logout",
	  cancelButtonText: "Reload",
	  closeOnConfirm: false,
	  closeOnCancel: false,
	  allowOutsideClick: false
	}).then(function(){
		location.href ="<?php echo base_url(); ?>index.php/login/logout";
	},function(dismiss) {
		// dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	   if (dismiss === 'cancel') {
		location.reload();
	  } 
	});
		
	}
	
    $(document).ready(function () {
        $( document ).idleTimer(60000); // 1000 = 1sec
    });
    $( document ).on( "idle.idleTimer", function(event, elem, obj){
   		action();
	});
    $( document ).on( "active.idleTimer", function(event, elem, obj, triggerevent){
		action();
    });
</script>
<script>
	$(function(){
	  	  setInterval(function()
   {
     $('#time-elapsed').load('<?php echo base_url(); ?>index.php/dtr/running_time_break');
	 if(window.console || window.console.firebug) {
		 console.clear();
	}
 
	}, 1000); 
 
	$("#breakBack").click(function(){
		var pass = $("#txtPassword").val();
 		var acc_time_id = $("#acc_time_id").text();
		var dtr_id = $("#dtr_id").text();
		 $.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/dtr/break_back",
			data: {
			pass:pass,
 			acc_time_id:acc_time_id,
			dtr_id:dtr_id,
  			},
			cache: false,
			success: function(html)
			{
				if(html==0){
					$("#message").html("Password mismatch or Something went wrong, please refresh your page!");
				}else{
					$("#message").html("");
					swal(
					'You are now back.',
					'You have successfully logged back in.',
					'success'
				  );
					setTimeout(function(){ window.location.href="<?php echo base_url()."index.php/dtr/"?>"; },2000);

				}
 			}
		});	
	});
	
	
	
		});

</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/lockscreen-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
</html>