
<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

    <script type="text/javascript">
        $(window).load(function(){
           setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
       });
   </script>

   <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
   <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
   <script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>


</head>


<body>
    <div id="sb-site">


        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div id="page-wrapper">
            <div id="page-header" class="bg-black">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                    <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                </div>
                <div id="header-logo" class="logo-bg">
                 <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                 <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id='headerLeft'>
            </div>



        </div>
        <div id="page-sidebar">
            <div class="scroll-sidebar">


                <ul id="sidebar-menu">
                </ul>
            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content">

                <div class="container">

                    <script type="text/javascript">

                        /* Datatables export */

                        $(document).ready(function() {
                            var table = $('#datatable-tabletools').DataTable();
                        } );

                        $(document).ready(function() {
                            $('.dataTables_filter input').attr("placeholder", "Search...");
                        });

                    </script>


                    <div id="page-title">
                        <div class="row">
                            <div class='btn-group col-md-3'>
                                <a href="<?php echo base_url(); ?>index.php/Payroll/employeeDeductions" class="btn btn-sm btn-warning"><span class='icon-typicons-left'> Back</span></a>

                                <button class="btn btn-sm btn-info" id='downloadtemp'><span class='icon-typicons-download'> Template</span></button>

                                <button class="btn btn-sm btn-primary" id="upload">
                                    <span class='icon-typicons-plus'> File</span>
                                </button>
                            </div>
                            <div class="col-md-3" id="uploadFileDiv" style="display: none;">
                                <div>
                                    <input type="file" class="form-control  sweet-3" id="uploadFile">
                                </div>
                            </div> 
                            <div class="btn-group col-md-3">
                                <button class="btn btn-sm btn-primary" id="upload2" style="display: none;"><span class='icon-typicons-upload'> Upload</span></button>
                                
                                <button class="btn btn-sm btn-primary" id="save" style="display: none;"><span class='icon-typicons-check'> Save</span></button>


                            </div>
                            <button class="btn btn-sm btn-primary" id="view" style="display: none;" hidden>View</button>
                        </div><br>
                        <div class="row">
                            <div class="col-md-3">
                               <label class="control-label">Coverage: <span id="asterisk">*</span><span id='selecterror' class='text-danger text-left' style="font-size:8px" hidden>(Required)</span></label>
                               <select id='date' required  class="form-control">
                                <option value='' selected disabled>--</option>>
                                <?php foreach($coverages as $c):?>
                                    <option value='<?php echo $c->coverage_id;?>'>
                                        <?php 
                                        $new = explode(" - ",$c->daterange);
                                        $date1=date_create($new[0]);
                                        $date2=date_create($new[1]);
                                        $date1 = date_format($date1,"F d,Y");
                                        $date2 = date_format($date2,"F d,Y");
                                        echo $date1." - ".$date2;
                                        ?>
                                    </option>
                                <?php endforeach;?>
                            </select>

                        </div>
                        <div class="col-md-2">

                            <h5 id='selecterror' class='text-danger' style="font-weight: bold;" hidden>Required.</h5>
                        </div>
                    </div><br>
                    <div class='row'>
                        <div class="col-md-12">
                            <div class="example-box-wrapper">
                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">

                                    <tbody id='tbody'>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" hidden>
                    <div class="col-md-8">
                        <div class="panel">
                            <div class="panel-body">
                                <h3 class="title-hero">
                                    Recent sales activity
                                </h3>
                                <div class="example-box-wrapper">
                                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                                </div>
                            </div>
                        </div>




                        <div class="content-box">
                            <h3 class="content-box-header bg-default">
                                <i class="glyph-icon icon-cog"></i>
                                Live server status
                                <span class="header-buttons-separator">
                                    <a href="#" class="icon-separator">
                                        <i class="glyph-icon icon-question"></i>
                                    </a>
                                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                                        <i class="glyph-icon icon-refresh"></i>
                                    </a>
                                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                                        <i class="glyph-icon icon-times"></i>
                                    </a>
                                </span>
                            </h3>
                            <div class="content-box-wrapper">
                                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>



        </div>
    </div>
</div>


<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
        cache: false,
        success: function(html)
        {

          $('#headerLeft').html(html);
           //alert(html);
       }
   });   
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
        cache: false,
        success: function(html)
        {

          $('#sidebar-menu').html(html);
         // alert(html);
     }
 });
});   
</script>
<script type="text/javascript">
 // $("#viewtable").hide();
 $(function(){
    $("#save").click(function(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>/index.php/Payroll/saveEmployeeDeductions",
            cache: false,
            success: function(res)
            {
              if(res=='Exists'){
                swal("Error", "Already Uploaded a Record in this coverage.", "error");
            }else{ 
                window.location = "<?php echo base_url(); ?>/index.php/Payroll/employeeDeductions";
            }


        }
    });

    });
    $("#view").click(function(){
       $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/Payroll/viewEmployeeDeductions",
        cache: false,
        success: function(res)
        {
            $('#tbody').empty();
            // $("#viewtable").show();
            var data = $.parseJSON(res);
            var body = "";
            body += "<tr>";
            for(var j=0;j<=data[0].length-1;j++){
                body += "<th class='text-center'style='background: #787a7b;color:white'>"+data[0][j]+"</th>";
            }
            body +="</tr>";

            for(var i=1;i<data.length;i++){
                body += "<tr>";
                for(var j=0;j<=data[i].length-1;j++){
                    body += "<td>"+data[i][j]+"</td>";
                }
                body +="</tr>";

                
            }
            $('#tbody').append(body);
            
        }
    }); 
   });
    $("#upload").click(function(){
        $('#uploadFileDiv').toggle();
    });
    $('#uploadFile').on('change', function(){
        if($('#uploadFile')[0].files[0].name == 'EmployeeDeductionsTemplate.xlsx'){
            $("#upload2").show();
        }else{
            swal("Oops!", "Please use the template given!", "error");
            $("#upload2").hide();$("#save").hide();
        }
    });

    $("#upload2").click(function(event){
        // if($('#uploadFile').files.length == 0){
        //     swal("Oops!", "No Selected File", "error");
        //     $("#upload2").hide();$("#save").hide();
        // }else{
           var data = new FormData();
           $.each($("#uploadFile")[0].files,function(i,file){
            data.append("uploadFile",file);
        });                      

           $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Payroll/uploadEmployeeDeductions",
            data: data,
            cache: false,
            processData:false,
            contentType:false,
            success: function(response)
            {
                swal("Success", response, "success");

                $("#view").trigger('click');
                $("#save").show();
            },
            error: function(){
                swal("Error", "An error occurred while uploading. Please Try Again.", "error");
            }
        });
           event.preventDefault();
     // }

 });
});
</script>
<script type="text/javascript">


    $("#downloadtemp").click(function(){
        // alert("yup");
        var dateid = $("#date :selected").val();
        if(dateid==''){
            // alert('Effectivity Date is Required.');
            $("#selecterror").show();
            $("#asterisk").addClass("text-danger")
        }else{

            window.location = "<?php echo base_url(); ?>index.php/Payroll/downloadEmployeeDeductions/"+dateid;
        }
    })
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>