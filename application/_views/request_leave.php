<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  </style>

  
  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });

 </script>
 <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/daterangepicker/daterangepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
 <script type="text/javascript">
   $(function(){
    "use strict";
    var m = moment().format("MM/DD/YYYY");
    var end = moment().add(1,'year').format("MM/DD/YYYY");
    $("#daterange").daterangepicker({ minDate: m,startDate: m,maxDate : end});   
    $("[name=daterangepicker_start]").attr('readonly',true);
    $("[name=daterangepicker_end]").attr('readonly',true);
  });
</script>

<!-- Textarea -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/textarea/textarea.js"></script>
<script type="text/javascript">
  /* Textarea autoresize */

  $(function() { "use strict";
    $('.textarea-autosize').autosize();
  });
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
</head>

<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div id="page-title">
          <h3>LEAVE</h3>
          <hr>
          <div class="example-box-wrapper">
            <div class="content-box">
              <h3 class="content-box-header bg-primary">
                <i class="glyph-icon icon-cog"></i>
                LEAVE REQUEST FORM
                <div class="header-buttons-separator">
                 <a href="<?php echo base_url();?>index.php/Request/view_leave_main" class='icon-separator' title='View Request List'>
                   <i class='glyph-icon icon-th-list'></i>
                 </a>
                 <a href="<?php echo base_url();?>index.php/Request/view_leave_personal" class='icon-separator' title='View Personal Request'>
                   <i class='glyph-icon icon-user'></i>
                 </a>
               </div>
             </h3>
             <div class="content-box-wrapper">
              <div class="row"> 
                <div class="col-md-5">
                  <div class="form-group">

                    <label class="control-label">Leave Type <span id='leavetypen' class='font-red font-size-10' hidden> Required *</span></label>

                    <select class="form-control" id='leavetype'>
                      <option value=''>--</option>
                      <?php foreach($leaves as $l):?>
                        <option value='<?php echo $l->leave_id;?>'><?php echo $l->leave_name;?></option>
                      <?php endforeach;?>
                    </select>

                  </div>

                  <label class='control-label'>Date Range  <span id='daterangen' class='font-red font-size-10' hidden> Required *</span></label>
                  <div class="input-prepend input-group">
                    <span class="add-on input-group-addon">
                      <i class="glyph-icon icon-calendar"></i>
                    </span>
                    <input type="text" name="daterange" id="daterange" class="form-control">
                  </div>  

                </div>
                <div class="col-md-7">

                  <div class="form-group">
                    <label class="control-label">Reason  <span id='reasonn' class='font-red font-size-10' hidden> Required *</span></label>
                    <textarea name="" id="reason" class="form-control textarea-no-resize"  style='height:108px'></textarea>
                  </div>
                </div>

              </div>
            </div>
            <div class="button-pane text-right">
              <button class="btn btn-info" id="submitleave">Submit Form</button>
              <button class="btn btn-link font-gray-dark">Cancel</button>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<!-- <script type="text/javascript">
  $(function(){
    $("#daterange").on('apply.daterangepicker', function(ev, picker) {
    // alert("Nicca");
    var startdate = picker.startDate.format('YYYY-MM-DD');
    var enddate = picker.endDate.format('YYYY-MM-DD');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Request/checkleavedaterange",
      data: {
        startdate:startdate,
        enddate:enddate
      },
      cache: false,
      success: function(res)
      {
        console.log(res.trim());
        if(res.trim()=='Empty'){

          $.jGrowl("No Schedule Set",{sticky:!1,position:"top-right",theme:"bg-info"});
        }else if(res.trim()=='Exists'){
          $.jGrowl("Request for chosen date Exists.",{sticky:!1,position:"top-right",theme:"bg-warning"});
          $("#daterange").val('');
        }else{
          var result = JSON.parse(res);
          var types = "";
          result.forEach(function(item){
            if(item.type!='Normal'){
              types += item.type+" ";
            }
          });
          if(types!=""){
           $.jGrowl("Schedules in the chosen dates contains <br><b> "+types+"</b>. <br> Please change dates.",{sticky:!1,position:"top-right",theme:"bg-red"}); 
           $("#daterange").val('');
         } 
       }
     }
   });
  });
  })
</script> -->
<script type="text/javascript">
  $("#submitleave").click(function(){
    var leavetype = $("#leavetype").val();
    var date = $("#daterange").val();
    var reason = $("#reason").val();

    if(leavetype==''){
      $("#leavetypen").show();
    }else{
      $("#leavetypen").hide();
    }
    if(date==''){
      $("#daterangen").show();
    }else{
      $("#daterangen").hide();
    }
    if(reason==''){
      $("#reasonn").show();
    }else{
      $("#reasonn").hide();
    }

    if(leavetype!=''&&date!=''&&reason!=''){

      var startdate =  $("#daterange").data('daterangepicker').startDate.format('YYYY-MM-DD');
      var enddate =  $("#daterange").data('daterangepicker').endDate.format('YYYY-MM-DD');
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Request/checkleavedaterange",
        data: {
          startdate:startdate,
          enddate:enddate
        },
        cache: false,
        success: function(res)
        {
          var Okay = false;
          console.log(res.trim());
          if(res.trim()=='Empty'){
            Okay = true;
            // $.jGrowl("No Schedule Set",{sticky:!1,position:"top-right",theme:"bg-info"});
          }else if(res.trim()=='Exists'){
            $.jGrowl("Request for chosen date Exists.",{sticky:!1,position:"top-right",theme:"bg-warning"});
            $("#daterange").val('');
          }else{
            var result = JSON.parse(res);
            var types = "";
            result.forEach(function(item){
              if(item.type!='Normal'){
                types += item.type+" ";
              }
            });
            if(types!=""){
             $.jGrowl("Schedules in the chosen dates contains <br><b> "+types+"</b>. <br> Please change dates.",{sticky:!1,position:"top-right",theme:"bg-red"}); 
             $("#daterange").val('');
           } else{
            Okay = true;
          }
        }
        console.log(Okay);
        if(Okay==true){
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Request/sendrequestleave",
            data: {
              startdate:startdate,
              enddate:enddate,
              leavetype:leavetype,
              reason:reason,
              requesttype:'Leave Request'
            },
            cache: false,
            success: function(res)
            {
              if(res=='Failed'){
                $.jGrowl("Error occurred while processing request. <br> Please try again.",{sticky:!1,position:"top-right",theme:"bg-red"});
              }else if(res.trim()=='Exists'){
                $.jGrowl("Request for chosen date Exists.",{sticky:!1,position:"top-right",theme:"bg-warning"});
              }
              else{
               $.jGrowl("Successfully Requested to Supervisor.",{sticky:!1,position:"top-right",theme:"bg-green"});
             }
           }
         });
        }
        
      }
    });
    }
  });
</script>
<script type="text/javascript">
  $("#leavetype").on('change',function(){
    $('#daterange').daterangepicker('destroy');
    var leavetype = $(this).val();
    // $("#daterange").daterangepicker("update");
    if(leavetype==1){

      var m = moment().add(3,'weeks').format("MM/DD/YYYY");
      var end = moment().add(1,'year').format("MM/DD/YYYY");
    }else if(leavetype==2||leavetype==7||leavetype==5){
      var m = moment().subtract(2,'months').format("MM/DD/YYYY");
      var end = moment().add(2,'months').format("MM/DD/YYYY");
    }else if(leavetype==6){
     $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Request/getBirthdate",
      cache: false,
      success: function(res)
      {
        var bday = $.trim(res);
        if(bday=='Empty'){

          var m = moment().format("MM/DD/YYYY");
          var end = moment().add(1,'year').format("MM/DD/YYYY");

          $.jGrowl("No Birth Date Set.",{sticky:!1,position:"top-right",theme:"bg-warning"});
        }else{
          var year = moment().format('YYYY');
          var monthday = moment(bday).format('MM/DD'); 
          var birthdate = monthday+"/"+year;
          var m = moment(birthdate).subtract(1,'week').format("MM/DD/YYYY");
          var end = moment(birthdate).add(1,'week').format("MM/DD/YYYY");
        }
      }
    });

   }else if(leavetype==3||leavetype==4){
    var m = moment().subtract(3,'months').format("MM/DD/YYYY");
    var end = moment().add(1,'year').format("MM/DD/YYYY");
  }else{

    var m = moment().format("MM/DD/YYYY");
    var end = moment().add(1,'year').format("MM/DD/YYYY");
  }
  $("[name=daterangepicker_start]").attr('readonly',true);
  $("[name=daterangepicker_end]").attr('readonly',true);

  $("#daterange").daterangepicker({ minDate: m,startDate: m,endDate: m,maxDate : end});
  // console.log($("#daterange").);
});

  // $("#daterange").daterangepicker({ minDate: m,startDate: m,endDate: m,maxDate : end});
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>