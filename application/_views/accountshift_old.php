<!DOCTYPE html> 
<html  lang="en">

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
            body { padding-right: 0 !important }
        </style>


        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/fontawesome/fontawesome.css"> -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">

        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });

        </script>
    </head>

    <body>
        <div id="sb-site">
            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <div id="page-header" class="bg-black">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>

                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">


                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div id="page-title">
                            <h2>Account Shift</h2><hr>
                            <div class="row text-left bordered-row">
                                <form class="form-horizontal col-md-2" style='padding-bottom:0'>
                                    <div class="form-group text-right">
                                        <label class="col-sm-3 control-label">Type</label>
                                        <div class="col-sm-9">
                                            <select class='form-control' id='type'>
                                                <option value=''>--</option>
                                                <option value='Admin'>Admin</option>
                                                <option value='Agent'>Agent</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <form class="form-horizontal col-md-4" style='padding-bottom:0'>
                                    <div class="form-group text-left">
                                        <label class="col-sm-3 control-label">Account</label>
                                        <div class="col-sm-9">
                                            <select class='form-control' id='account'></select> 
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel">
                                        <div class="panel-heading font-bold font-blue">LIST OF SHIFTS</div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class='glyph-icon icon-search'></i></span>
                                                        <input disabled type="text" id="search1" onkeyup="myFunction1()" class="form-control" placeholder="Search for shifts...">  
                                                    </div>
                                                </div>
                                                <div class="col-md-4">

                                                    <button class="btn btn-info pull-right" id='addbtn' data-toggle="modal" data-target="#shiftmodal" disabled>
                                                        <span class="glyph-icon icon-separator">
                                                            <i class="glyph-icon icon-plus"></i>
                                                        </span>
                                                        <span class="button-content">
                                                            Add Shift
                                                        </span>
                                                    </button>
                                                </div>
                                            </div><br>

                                            <table class='table table-hover table-bordered table-condensed' id='table1'>
                                                <thead>
                                                    <tr>
                                                        <th class='text-center'>SHIFT</th>
                                                    </tr>
                                                </thead>
                                                <tbody id='shiftlist'></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-6">
                                    <div class="panel">
                                        <div class="panel-heading font-bold font-blue">LIST OF BREAKS</div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-info pull-right" id='addbreakbtn' data-toggle="modal" data-target="#breakmodal" disabled>
                                                        <span class="glyph-icon icon-separator">
                                                            <i class="glyph-icon icon-plus"></i>
                                                        </span>
                                                        <span class="button-content">
                                                            Add Break
                                                        </span>
                                                    </button>
                                                </div>
                                            </div><br>
                                            <table class='table table-hover table-bordered table-condensed'>
                                                <thead>
                                                    <tr>
                                                        <th class='text-center'>TYPE</th>
                                                        <th class='text-center'>TIME</th>
                                                    </tr>
                                                </thead>
                                                <tbody id='breaklist'></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="shiftmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style='width:30%'>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">ADDING SHIFT</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" >
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Shift:</label>
                            <div class="col-sm-10">
                                <select class='form-control' id='notshift'></select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id='savebtn'>Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="breakmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style='width:30%'>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">ADDING BREAK</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" >
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Break:</label>
                            <div class="col-sm-10">
                                <select class='form-control' id='notbreak'></select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id='savebreakbtn'>Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- JS Demo -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
</body>
<script>
                              $(function () {
                                  $.ajax({
                                      type: "POST",
                                      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
                                      cache: false,
                                      success: function (html) {

                                          $('#headerLeft').html(html);
                                          //alert(html);
                                      }
                                  });
                                  $.ajax({
                                      type: "POST",
                                      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
                                      cache: false,
                                      success: function (html) {

                                          $('#sidebar-menu').html(html);
                                          // alert(html);
                                      }
                                  });
                              });
</script>
<script type="text/javascript">
        $(function () {
            $("#type").on('change', function () {

                $("#addbreakbtn").prop('disabled', true);
                $("#addbtn").prop('disabled', true);
                $("#search1").prop('disabled', true);
                if ($(this).val() == 'Admin') {
                    var account = <?php echo $admins; ?>;
                    $("#account").html('');
                    var options = "<option value=''>--</option>";
                    $.each(account, function (i, item) {
                        options += "<option value='" + item.acc_id + "'>" + item.acc_name + "</option>";
                    });
                    $("#account").html(options);
                } else if ($(this).val() == 'Agent') {
                    var account = <?php echo $agents; ?>;
                    $("#account").html('');
                    var options = "<option value=''>--</option>";
                    $.each(account, function (i, item) {
                        options += "<option value='" + item.acc_id + "'>" + item.acc_name + "</option>";
                    });
                    $("#account").html(options);
                } else {
                    $("#account").html('');
                }
            });
        });
</script>
<script type="text/javascript">
        $(function () {
            $("#account").on('change', function () {
                // $("#shiftlist")
                $("#breaklist").html("");
                var acc_id = $(this).val();
                if (acc_id == '') {
                    $("#addbreakbtn").prop('disabled', true);
                    $("#addbtn").prop('disabled', true);
                    $("#search1").prop('disabled', true);
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Schedule/getAccShifts_old",
                        data: {
                            acc_id: acc_id
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            res = JSON.parse(res);
                            var options = "";
                            $.each(res, function (i, item) {
                                options += "<tr>";
                                options += "<td  class='text-center'>" + item.time_start + " - " + item.time_end + "</td>";
                                options += "</tr>";
                            });
                            $("#shiftlist").html(options);
                            $("#addbtn").data('accid', acc_id);
                            $("#savebtn").data('accid', acc_id);
                            $("#addbtn").prop('disabled', false);
                            $("#search1").prop('disabled', false);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Schedule/getAccBreaks_old",
                        data: {
                            acc_id: acc_id
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            res = JSON.parse(res);
                            var options = "";
                            $.each(res, function (i, item) {
                                options += "<tr>";

                                options += "<td  class='text-center'>" + item.break_type + "</td>";
                                if (item.hour == 0) {

                                    options += "<td  class='text-center'>" + item.min + " mins.</td>";
                                } else {

                                    options += "<td  class='text-center'>" + item.hour + "hr and " + item.min + " mins</td>";
                                }
                                options += "</tr>";
                            });
                            $("#breaklist").html(options);
                            $("#addbreakbtn").data('accid', acc_id);
                            $("#savebreakbtn").data('accid', acc_id);
                            $("#addbreakbtn").prop('disabled', false);
                        }
                    });
                }
            });
        })
</script>
<script>
        function myFunction1() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("search1");
            filter = input.value.toUpperCase();
            table = document.getElementById("table1");
            tr = table.getElementsByTagName("tr");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
</script>
<script type="text/javascript">
        $('#shiftmodal').on('show.bs.modal', function (e) {
            var acc_id = $(e.relatedTarget).data('accid');
            // alert(acc_id);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/index.php/Schedule/getNotShifts_old",
                data: {
                    acc_id: acc_id
                },
                cache: false,
                success: function (res) {
                    res = res.trim();
                    var account = JSON.parse(res);
                    $("#notshift").html('');
                    var options = "<option value=''>--</option>";
                    $.each(account, function (i, item) {
                        options += "<option value='" + item.time_id + "'>" + item.time_start + " - " + item.time_end + "</option>";
                    });
                    $("#notshift").html(options);
                }
            });
        });
</script>
<script type="text/javascript">
        $("#savebtn").click(function () {
            var acc_id = $(this).data('accid');
            var time_id = $("#notshift").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/index.php/Schedule/assignShift_old",
                data: {
                    acc_id: acc_id,
                    time_id: time_id
                },
                cache: false,
                success: function (res) {
                    res = res.trim();
                    if (res == 'Success') {
                        swal(
                                'Success!!',
                                'Successfully assigned shift to account',
                                'success'
                                )
                    } else {
                        swal(
                                'Error!!',
                                'Failed to assign shift to account',
                                'error'
                                )
                    }
                    $("#shiftmodal .close").click();
                    $("#account").change();
                    $("#search1").val('');
                }
            });
        });
</script>
<script type="text/javascript">
        $('#breakmodal').on('show.bs.modal', function (e) {
            var acc_id = $(e.relatedTarget).data('accid');
            // alert(acc_id);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Schedule/getNotBreaks_old",
                data: {
                    acc_id: acc_id
                },
                cache: false,
                success: function (res) {
                    res = res.trim();
                    var account = JSON.parse(res);
                    $("#notbreak").html('');
                    var options = "<option value=''>--</option>";
                    $.each(account, function (i, item) {
                        options += "<option value='" + item.bta_id + "'>" + item.break_type + " - " + item.hour + " hr " + item.min + " mins</option>";
                    });
                    $("#notbreak").html(options);
                }
            });
        });
</script>
<script type="text/javascript">
        $("#savebreakbtn").click(function () {
            var acc_id = $(this).data('accid');
            var bta_id = $("#notbreak").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/index.php/Schedule/assignBreak_old",
                data: {
                    acc_id: acc_id,
                    bta_id: bta_id
                },
                cache: false,
                success: function (res) {
                    res = res.trim();
                    if (res == 'Success') {
                        swal(
                                'Success!!',
                                'Successfully assigned break to account',
                                'success'
                                )
                    } else {
                        swal(
                                'Error!!',
                                'Failed to assign break to account',
                                'error'
                                )
                    }
                    $("#breakmodal .close").click();
                    $("#account").change();
                }
            });
        });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>