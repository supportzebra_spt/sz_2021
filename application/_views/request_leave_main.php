<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  </style>

  
  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
     $("#tab1").trigger("click");
   });

 </script>

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>

 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/tooltip/tooltip.css">
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
 <script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
  });
</script>
<!-- jQueryUI Tabs -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/tabs-ui/tabs.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs-ui/tabs.js"></script>
<script type="text/javascript">
  /* jQuery UI Tabs */

  $(function() { "use strict";
    $(".tabs").tabs();
  });

  $(function() { "use strict";
    $(".tabs-hover").tabs({
      event: "mouseover"
    });
  });
</script>

<!-- Boostrap Tabs -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs/tabs.js"></script>

<!-- Tabdrop Responsive -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs/tabs-responsive.js"></script>
<script type="text/javascript">
  /* Responsive tabs */
  $(function() { "use strict";
    $('.nav-responsive').tabdrop();
  });
</script>

</head>

<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div id="page-title">
          <h3>List of Request</h3>
          <hr>
          <a href="<?php echo base_url();?>index.php/Request/view_leave_personal" class='btn btn-sm btn-success' title="View Personal Request">
           <i class='glyph-icon icon-user'></i> Personal Request
         </a>
         <br>
         <br>
         <div class="example-box-wrapper">
          <div class="content-box tabs">
            <h3 class="content-box-header bg-primary">
             <span class="header-wrapper">
              Request List
            </span>
            <ul>
              <li>
                <a href="#tabs-example-1" title="Pending" id='tab1'>
                  Pending
                </a>
              </li>
              <li>
                <a href="#tabs-example-2" title="Accepted" id='tab2'>
                  Accepted
                </a>
              </li>
              <li>
                <a href="#tabs-example-3" title="Rejected" id='tab3'>
                  Rejected
                </a>
              </li>
            </ul>
          </h3>
          <div id="tabs-example-1">
           <table class="table table-condensed table-striped table-bordered">
            <thead>
              <tr>
                <th>Requested By</th>
                <th>Type</th>
                <th>Details</th>
                <th>Date Requested</th>
                <th>Date Coverage</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id='pendingtbody'>

            </tbody>
          </table>
        </div>
        <div id="tabs-example-2">
          <table class="table table-condensed table-striped table-bordered">
            <thead>
              <tr>
                <th>Requested By</th>
                <th>Type</th>
                <th>Details</th>
                <th>Date Requested</th>
                <th>Date Coverage</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id='acceptedtbody'>

            </tbody>
          </table>
        </div>
        <div id="tabs-example-3">
          <table class="table table-condensed table-striped table-bordered">
            <thead>
              <tr>
                <th>Requested By</th>
                <th>Type</th>
                <th>Details</th>
                <th>Date Requested</th>
                <th>Date Coverage</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id='declinedtbody'>

            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>
</div>
<div id="tab1viewmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
           <img alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';"    class='img-circle' style='border:2px solid #ddd;width:100%' id='mimage'/>
           <hr style='margin:10px !important'>
           <h4 class='font-size-16 font-bold text-center' id='mfullname'></h4>
         </div>
         <div class="col-md-8">
           <br>
           <h5 class='mrg5B'>DATE COVERED</h5>
           <h5 class="font-bold" id='mdatecovered'></h5>
           <br>
           <h5 class='mrg5B'>TYPE OF REQUEST</h5>
           <h5 class="font-bold" id='mtype'></h5>
           <br>
           <h5 class='mrg5B'>REQUEST DETAILS</h5>
           <h5 class="font-bold" id='mdetails'></h5>
           <br>
           <h5 class='mrg5B'>STATUS</h5>
           <h5 class="font-bold"  id='mstatus'></h5>
         </div>
         <div class="col-md-12">
           <hr>
           <div class="pad10A">

             <h5 class='mrg5B'>REASON</h5>
             <h5 class="font-bold text-justify"  id='mreason'></h5>
           </div>
         </div>
       </div>

     </div>
     <div class="modal-footer">
      <button class="btn btn-success"  title="Approve" id='btn-accept'>
        <i class="glyph-icon icon-thumbs-up"></i>
      </button>
      <button class="btn btn-danger" title="Disapprove" id='btn-reject'>
        <i class="glyph-icon icon-thumbs-down"></i>
      </button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      <div id="rejectreason" style="display:none;text-align:left">
        <hr>
        <div class="row">
          <div class="col-md-12">
           <div class="form-group">
            <label class="control-label">Enter the reason for rejecting request:</label>

            <textarea name="" rows="3" class="form-control textarea-no-resize" id="reason_reject"></textarea>
            <br>
            <button class='btn btn-primary pull-right btn-sm'  id="requestrejected">Submit</button>
          </div>
        </div>
      </div>
    </div>
    <div id="acceptreason" style="display:none;text-align:center">
      <hr>
      <div class="row">
        <div class="col-md-12">
          <h5>Are you sure to approve this request ?</h5> <br>
          <button class="btn btn-info btn-sm" id="requestaccepted">Approve</button>
          <button class="btn btn-default btn-sm" onclick="$('#acceptreason').hide(500)">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div id="tab2viewmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
       <div class="row">
        <div class="col-md-4">
         <img alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';"    class='img-circle' style='border:2px solid #ddd;width:100%' id='m2image'/>
         <hr style='margin:10px !important'>
         <h4 class='font-size-16 font-bold text-center' id='m2fullname'></h4>
       </div>
       <div class="col-md-8">
         <br>
         <h5 class='mrg5B'>DATE COVERED</h5>
         <h5 class="font-bold" id='m2datecovered'></h5>
         <br>
         <h5 class='mrg5B'>TYPE OF REQUEST</h5>
         <h5 class="font-bold" id='m2type'></h5>
         <br>
         <h5 class='mrg5B'>REQUEST DETAILS</h5>
         <h5 class="font-bold" id='m2details'></h5>
         <br>
         <h5 class='mrg5B'>STATUS</h5>
         <h5 class="font-bold"  id='m2status'></h5>
       </div>
       <div class="col-md-12">
         <hr>
         <div class="pad10A">

           <h5 class='mrg5B'>REASON</h5>
           <h5 class="font-bold text-justify"  id='m2reason'></h5>
         </div>
       </div>
     </div>
   </div>
   <div class="modal-footer">
    <button type='button' class='btn btn-danger' id='decline'>Decline</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

    <div style='display:none' id="declinequestion" class='text-center'><hr>
      <h5 class="mrg15B">Request is already approved. Are you sure to reject ?</h5>
      <button class='btn btn-info btn-sm' id='declineanswer'>Reject</button><button class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
    </div>
    <div style='display:none' id="changeschedule" class='text-center'>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <label>Enter the reason for rejecting request:</label>
          <textarea name="" rows="2" class="form-control textarea-no-resize" id="thereasonreject"></textarea>
        </div>
      </div>
      <hr>
      <p>Change of Schedule is required to continue action.</p><br>
      <table class='table table-condensed table-striped font-size-13 text-center font-black'>
        <thead>
          <tr class='info'> 
           <th class='text-center'>Date</th>
           <th class='text-center'>Shift</th>
           <th class='text-center'>Is Rest Day</th>
         </tr>
       </thead>
       <tbody id='scheduletable'></tbody>
     </table>
     <button class='btn btn-sm btn-primary pull-right' id='declinecontinue'>Continue</button>
   </div>     
 </div>
</div>
</div>
</div>
<div id="tab3viewmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
       <div class="row">
        <div class="col-md-4">
         <img alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';"    class='img-circle' style='border:2px solid #ddd;width:100%' id='m3image'/>
         <hr style='margin:10px !important'>
         <h4 class='font-size-16 font-bold text-center' id='m3fullname'></h4>
       </div>
       <div class="col-md-8">
         <br>
         <h5 class='mrg5B'>DATE COVERED</h5>
         <h5 class="font-bold" id='m3datecovered'></h5>
         <br>
         <h5 class='mrg5B'>TYPE OF REQUEST</h5>
         <h5 class="font-bold" id='m3type'></h5>
         <br>
         <h5 class='mrg5B'>REQUEST DETAILS</h5>
         <h5 class="font-bold" id='m3details'></h5>
         <br>
         <h5 class='mrg5B'>STATUS</h5>
         <h5 class="font-bold"  id='m3status'></h5>
       </div>
       <div class="col-md-12">
         <hr>
         <div class="pad10A">

           <h5 class='mrg5B'>REASON</h5>
           <h5 class="font-bold text-justify"  id='m3reason'></h5>
         </div>
         <hr>
         <div class="pad10A">

           <h5 class='mrg5B'>REJECT REASON</h5>
           <h5 class="font-bold text-justify"  id='m3reasonreject'></h5>
         </div>
       </div>
     </div>
   </div>
   <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">

 var passvalue = function(token){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>/index.php/Request/getRequestDetails",
    data: {
      token:token
    },
    cache: false,
    success: function(res)
    {
     res = res.trim();
     var result = JSON.parse(res);

     var s = moment(result.start_date).format("MMMM D, YYYY");
     var e = moment(result.end_date).format("MMMM D, YYYY");
     $("#mimage").attr('src','<?php echo base_url()?>assets/images/'+result.pic);
     $("#mfullname").html(result.lname+", "+result.fname);
     $("#mdetails").html(result.leave_name);
     $("#mtype").html(result.type);
     if(s==e){
      $("#mdatecovered").html(s);
    }else{    
      $("#mdatecovered").html(s + ' to ' +e);
    }
    $("#mstatus").html(result.status);
    $("#mreason").html(result.reason);
    $("#btn-reject").attr('data-token',result.token);
  }
});
};


</script>
<script type="text/javascript">
 var passvalue2 = function(token){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>/index.php/Request/getRequestDetails",
    data: {
      token:token
    },
    cache: false,
    success: function(res)
    {
      res = res.trim();
      var result = JSON.parse(res);

      var s = moment(result.start_date).format("MMMM D, YYYY");
      var e = moment(result.end_date).format("MMMM D, YYYY");
      $("#m2image").attr('src','<?php echo base_url()?>assets/images/'+result.pic);
      $("#m2fullname").html(result.lname+", "+result.fname);
      $("#m2details").html(result.leave_name);
      $("#m2type").html(result.type); 
      if(s==e){
        $("#m2datecovered").html(s);
      }else{    
        $("#m2datecovered").html(s + ' to ' +e);
      }
      $("#m2status").html(result.status);
      $("#m2reason").html(result.reason);
      if(moment(result.start_date) <= moment()){
        $("#declineanswer").hide(500);
        $("#decline").hide();
      }else{
       $("#declineanswer").show(500);
       $("#decline").show();
       $("#declineanswer").attr('data-token',result.token);

       $("#decline").attr('data-empid',result.requested_by);
       $("#decline").attr('data-startdate',result.start_date);
       $("#decline").attr('data-enddate',result.end_date);
     }
     $("#declineanswer").attr('data-token',result.token);

     $("#decline").attr('data-empid',result.requested_by);
     $("#decline").attr('data-startdate',result.start_date);
     $("#decline").attr('data-enddate',result.end_date);
   }
 });
};

</script>
<script type="text/javascript">
 var passvalue3 = function(token){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>/index.php/Request/getRequestDetails",
    data: {
      token:token
    },
    cache: false,
    success: function(res)
    {
     res = res.trim();
     var result = JSON.parse(res);

     var s = moment(result.start_date).format("MMMM D, YYYY");
     var e = moment(result.end_date).format("MMMM D, YYYY");
     $("#m3image").attr('src','<?php echo base_url()?>assets/images/'+result.pic);
     $("#m3fullname").html(result.lname+", "+result.fname);
     $("#m3details").html(result.leave_name);
     $("#m3type").html(result.type);
     if(s==e){
      $("#m3datecovered").html(s);
    }else{    
      $("#m3datecovered").html(s + ' to ' +e);
    }
    $("#m3status").html(result.status);
    $("#m3reason").html(result.reason);
    $("#m3reasonreject").html(result.reason_reject);
  }
});
};
</script>

<script type="text/javascript">
  $("#btn-reject").click(function(){
    $("#acceptreason").hide(500);
    $("#rejectreason").show(500);
  });

</script>
<script type="text/javascript">
  $("#btn-accept").click(function(){
    $("#acceptreason").show(500);
    $("#rejectreason").hide(500);
  })
</script>
<script type="text/javascript">
  $("#requestaccepted").click(function(){
    var token = $("#btn-reject").attr('data-token');
    var reason_reject = $("#reason_reject").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/leaverequestAccepted",
      data: {
        token:token,
        reason_reject:reason_reject
      },
      cache: false,
      success: function(res)
      {
        $("#tab1viewmodal").modal('toggle');
        if(jQuery.trim(res)=='Successful'){

        // $.jGrowl("Successfully Accepted Request.",{sticky:!1,position:"top-right",theme:"bg-green"});
        swal(
          'Success!',
          'Successfully Accepted Request',
          'success'
          );
        $("#tab1").trigger("click");

      }else{
        swal(
          'Error!',
          'Failed processing',
          'error'
          );
        // $.jGrowl("Failed processing.",{sticky:!1,position:"top-right",theme:"bg-red"});
      }
    }
  });
  });
</script>

<script type="text/javascript">
  $("#requestrejected").click(function(){
    var token = $("#btn-reject").attr('data-token');
    var reason_reject = $("#reason_reject").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/leaverequestRejected",
      data: {
        token:token,
        reason_reject:reason_reject
      },
      cache: false,
      success: function(res)
      {
        $("#tab1viewmodal").modal('toggle');
        // alert(jQuery.trim(res));
        if(jQuery.trim(res)=='Successful'){
          swal(
            'Success!',
            'Successfully Rejected Request',
            'success'
            );

          $("#tab1").trigger("click");
          // $.jGrowl("Successfully Rejected Request.",{sticky:!1,position:"top-right",theme:"bg-green"});
        }else{

          swal(
            'Error!',
            'Failed processing',
            'error'
            );
          // $.jGrowl("Failed processing.",{sticky:!1,position:"top-right",theme:"bg-red"});
        }

      }
    });
  });
</script>
<script type="text/javascript">
  $("#tab1").click(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/getLeaveRequestedToMe2",
      data: {
        status:'Pending'
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        $("#pendingtbody").html("");
        if(res=='Empty'){

          $("#pendingtbody").append("<tr><td colspan=7 class='text-center font-bold'>No Pending Request.</td></tr>");
        }else{

         $.each(JSON.parse(res),function (i,elem){
          var tbody = "";
          tbody += "<tr>";
          tbody += "<td>"+elem.lname+", "+elem.fname+"</td>";
          tbody += "<td>"+elem.type+"</td>";
          tbody += "<td>"+elem.leave_name+"</td>";
          tbody += "<td>"+elem.requested_on+"</td>";
          if(elem.start_date==elem.end_date){
            tbody += "<td>"+elem.start_date+"</td>";
          }else{
            tbody += "<td>"+elem.start_date+" - "+elem.end_date+"</td>";
          }
          tbody += "<td>"+elem.status+"</td>";
          tbody += "<td><button class='btn btn-primary btn-xs' data-toggle='modal' data-target='#tab1viewmodal' onclick='passvalue("+elem.token+")'>View</button></td>";
          tbody += "</tr>";
          $("#pendingtbody").append(tbody);
        });
       }
     }
   });
  });
</script>
<script type="text/javascript">
  $("#tab2").click(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/getLeaveRequestedToMe2",
      data: {
        status:'Accepted'
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        $("#acceptedtbody").html("");
        if(res=='Empty'){

          $("#acceptedtbody").append("<tr><td colspan=7 class='text-center font-bold'>No Accepted Request.</td></tr>");
        }else{

         $.each(JSON.parse(res),function (i,elem){
          var tbody = "";
          tbody += "<tr>";
          tbody += "<td>"+elem.lname+", "+elem.fname+"</td>";
          tbody += "<td>"+elem.type+"</td>";
          tbody += "<td>"+elem.leave_name+"</td>";
          tbody += "<td>"+elem.requested_on+"</td>";          
          if(elem.start_date==elem.end_date){
            tbody += "<td>"+elem.start_date+"</td>";
          }else{
            tbody += "<td>"+elem.start_date+" - "+elem.end_date+"</td>";
          }
          tbody += "<td>"+elem.status+"</td>";
          tbody += "<td><button class='btn btn-primary btn-xs' data-toggle='modal' data-target='#tab2viewmodal' onclick='passvalue2("+elem.token+")'>View</button></td>";
          tbody += "</tr>";
          $("#acceptedtbody").append(tbody);
        });
       }
     }
   });
  });
</script>
<script type="text/javascript">
  $("#tab3").click(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/getLeaveRequestedToMe2",
      data: {
        status:'Rejected'
      },
      cache: false,
      success: function(res)
      {
        res = res.trim();
        $("#declinedtbody").html("");
        if(res=='Empty'){

          $("#declinedtbody").append("<tr><td colspan=7 class='text-center font-bold'>No Rejected Request.</td></tr>");
        }else{

         $.each(JSON.parse(res),function (i,elem){
          var tbody = "";
          tbody += "<tr>";
          tbody += "<td>"+elem.lname+", "+elem.fname+"</td>";
          tbody += "<td>"+elem.type+"</td>";
          tbody += "<td>"+elem.leave_name+"</td>";
          tbody += "<td>"+elem.requested_on+"</td>";
          if(elem.start_date==elem.end_date){
            tbody += "<td>"+elem.start_date+"</td>";
          }else{
            tbody += "<td>"+elem.start_date+" - "+elem.end_date+"</td>";
          }
          tbody += "<td>"+elem.status+"</td>";
          tbody += "<td><button class='btn btn-primary btn-xs' data-toggle='modal' data-target='#tab3viewmodal' onclick='passvalue3("+elem.token+")'>View</button></td>";
          tbody += "</tr>";
          $("#declinedtbody").append(tbody);
        });
       }
     }
   });
  });
</script>
<script type="text/javascript">
  $("#decline").click(function(){
    var empid = $(this).attr('data-empid');
    var startdate = $(this).attr('data-startdate');
    var enddate = $(this).attr('data-enddate');
    var token = $("#declineanswer").attr('data-token');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/checkIfSchedLeave",
      data: {
        empid:empid,
        startdate:startdate,
        enddate:enddate,
        token:token
      },
      cache: false,
      success: function(res)
      {
        // alert(res);
        res = res.trim();
        if(res=='Success'){

          swal(
            'Schedule has changed!',
            'Changing Status to Rejected',
            'warning'
            );
          // $.jGrowl("Schedule has changed. <br> Changing Status to Rejected",{sticky:!1,position:"top-right",theme:"bg-blue"});
          $("#tab2viewmodal .close").click();    
          $("#tab2").trigger("click");
        }else if(res=='Failed'){
          swal(
            'Error!',
            'Failed processing',
            'error'
            );
          // $.jGrowl("Failed processing.",{sticky:!1,position:"top-right",theme:"bg-red"});
          $("#tab2viewmodal .close").click();    
          $("#tab2").trigger("click");
        }else{
          $("#declineanswer").attr("data-schedids",res);
          $("#declinequestion").show(500);  
          $("#changeschedule").hide(500);
        }
      }
    });
  });
  $("#declineanswer").click(function(){
    $("#changeschedule").show(500);    
    $("#declinequestion").hide(500);
    // var requestid = $(this).attr('data-requestid');

    var schedids = $(this).attr('data-schedids');
    // alert(schedids);
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/index.php/Request/getScheduleDetails",
      data: {
        schedids:schedids
      },
      cache: false,
      success: function(res)
      {
        // console.log(res);
        // alert(res);
        res = res.trim();

        $("#scheduletable").html("");
        $.each(JSON.parse(res),function (i,elem){
          var tbody = "";
          tbody += "<tr id='"+elem.sched_id+"'>";
          tbody += "<td style='vertical-align:middle'>"+elem.schedule_date+"</td>";
          tbody += "<td class='text-center' style='vertical-align:middle'><select class='form-control input-sm' id='shift"+elem.sched_id+"'>";
          $.each(elem.shift,function (x,shift){ 
            tbody += "<option value='"+shift.acc_time_id+"'>"+shift.time_start+" - "+shift.time_end+"</option>";
          });
          tbody += "</select></td>";
          tbody += "<td class='text-center' style='vertical-align:middle' style='vertical-align:middle'><input type='checkbox' id='isRD"+elem.sched_id+"'></td>";
          tbody += "</tr>";
          $("#scheduletable").append(tbody);
        });
      }
    });
  });
</script>
<script type="text/javascript">
  $('#tab2viewmodal').on('show.bs.modal', function() {
    $("#declinequestion").hide(500);
    $("#changeschedule").hide(500);
  })
</script>
<script type="text/javascript">
  $("#declinecontinue").click(function(){
    var schedids = $("#declineanswer").attr('data-schedids');
    var schedid = schedids.trim().split('|');
    var token = $("#declineanswer").attr('data-token');
    var thereasonreject = $("#thereasonreject").val();
    var isOkay = true;
    for(var x = 0;x<schedid.length;x++){
      var acc_time_id1 = $("#shift"+schedid[x]).val();   
      var isRD1 = $("#isRD"+schedid[x]).is(':checked');
      if(acc_time_id1==''&&!isRD1){
        isOkay = false; 
      }
    }
    if(thereasonreject==''){
      $.jGrowl("Please enter the reason for rejecting request...",{sticky:!1,position:"top-right",theme:"bg-red"});
    }else if(isOkay==false){
      $.jGrowl("Please complete the schedule to continue...",{sticky:!1,position:"top-right",theme:"bg-red"});
    }else{
      var superresult = 'Success';
      for(var x = 0;x<schedid.length;x++){
        var acc_time_id = $("#shift"+schedid[x]).val();   
        var isRD = $("#isRD"+schedid[x]).is(':checked');
        var result = $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>/index.php/Request/updateLeaveSchedule",
          data: {
            acc_time_id:acc_time_id,
            isRD:isRD,
            schedid:schedid[x]
          },
          cache: false,  
          global: false,
          async:false,
          success: function(res)
          {
            return res;
          }
        }).responseText;
        result=result.trim();
        if(result=='Failed'){
          superresult='Failed';
        }
      }
      if(superresult=='Success'){
        var ress = $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>/index.php/Request/updateRequestStatusDecline",
          data: {
            token:token,
            thereasonreject:thereasonreject
          },
          cache: false,  
          global: false,
          async:false,
          success: function(res2)
          {
            return res2;
          }
        }).responseText;
        ress= ress.trim();
        if(ress=='Success'){
          swal(
            'Success!',
            'Successfully Declined Request',
            'success'
            );
        // $.jGrowl("Successfully Declined Request.",{sticky:!1,position:"top-right",theme:"bg-blue"});
        $("#tab2viewmodal .close").click();
        $("#tab2").trigger("click");
      }else{
        swal(
          'Error!',
          'Problem Processing Request',
          'error'
          );
        // $.jGrowl("Problem Processing Request.",{sticky:!1,position:"top-right",theme:"bg-red"});
        $("#tab2viewmodal .close").click();   
        $("#tab2").trigger("click");
      }

    }else{
      swal(
        'Error!',
        'Failed processing',
        'error'
        );
      // $.jGrowl("Failed processing.",{sticky:!1,position:"top-right",theme:"bg-red"});
      $("#tab2viewmodal .close").click();  
      $("#tab2").trigger("click");
    }
  }

});


</script>
<script type="text/javascript">
  $('#tab1viewmodal').on('hidden.bs.modal', function () {
    $("#acceptreason").hide();
    $("#rejectreason").hide();
  });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>