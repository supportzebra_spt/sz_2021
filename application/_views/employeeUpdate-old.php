<!DOCTYPE html> 
<html  lang="en"  ng-app="app">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
        .editable-radiolist label {
            display: block;
        }
    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
         setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
     });
 </script>

 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
 <script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>

<!-- Bootstrap Wizard -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/wizard/wizard.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/wizard/wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/wizard/wizard-demo.js"></script>

<!-- Boostrap Tabs -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs/tabs.js"></script>

<!-- <script src="<?php echo base_url();?>assets/js/angular.min.js"></script> -->
<!--  <script src="<?php echo base_url();?>assets/js/xeditable.js"></script>
<script src="<?php echo base_url();?>assets/css/xeditable.css"></script>
-->

<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/extra/xeditable.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/xeditable/extra/xeditable.min.css">
-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/xeditable/xeditable.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable-demo.js"></script> -->

<!-- Bootstrap Summernote WYSIWYG editor -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/popover/popover.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-dateFormat.min.js"></script>
<!-- <script>
    var app = angular.module("app", ["xeditable"]);
    app.run(function(editableOptions) {
      editableOptions.theme = 'bs3'; 
  });

    app.controller('Ctrl', function($scope) {
      $scope.user = {
        fname: '<?php echo $emp['fname']; ?>',
        lname: '<?php echo $emp['lname']; ?>',
        mname: '<?php echo $emp['mname']; ?>',
        nikname: '<?php echo $emp['nikname']; ?>',
        bday: '<?php echo $emp['bday']; ?>',
        gender: '<?php echo $emp['gender']; ?>',
        civil: '<?php echo $emp['civil']; ?>',
        cell: '<?php echo $emp['cell']; ?>',
        tel: '<?php echo $emp['tel']; ?>',
        email: '<?php echo $emp['email']; ?>',
        blood: '<?php echo $emp['blood']; ?>',
        relijon: '<?php echo $emp['relijon']; ?>',
        school_name: '<?php echo $emp['school_name']; ?>',
        course: '<?php echo $emp['course']; ?>',
        school_note: '<?php echo $emp['school_note']; ?>',
        emergency_fname: '<?php echo $emp['emergency_fname']; ?>',
        emergency_mname: '<?php echo $emp['emergency_mname']; ?>',
        emergency_lname: '<?php echo $emp['emergency_lname']; ?>',
        emergency_extname: '<?php echo $emp['emergency_extname']; ?>',
        emergency_contact: '<?php echo $emp['emergency_contact']; ?>',
        id_num: '<?php echo $emp['id_num']; ?>',
        ccaExp: '<?php echo $emp['ccaExp']; ?>',
        other_latestEmp: '<?php echo $emp['other_latestEmp']; ?>',
        other_lastPosition: '<?php echo $emp['other_lastPosition']; ?>',
        other_inclusiveDate: '<?php echo $emp['other_inclusiveDate']; ?>',
        intellicare: '<?php echo $emp['intellicare']; ?>',
        pioneer: '<?php echo $emp['pioneer']; ?>',
        BIR: '<?php echo $emp['BIR']; ?>',
        philhealth: '<?php echo $emp['philhealth']; ?>',
        pagibig: '<?php echo $emp['pagibig']; ?>',
        sss: '<?php echo $emp['sss']; ?>',
        dtr_uname: '<?php echo $emp['dtr_uname']; ?>',
        dtr_password: '<?php echo $emp['dtr_password']; ?>',
        Supervisors: '<?php echo $emp['Supervisor']; ?>',
        supEmails: '<?php echo $emp['supEmails']; ?>',
        supName: '<?php echo $Supname; ?>',
    };   

    $scope.genders = [
    {value: '"Male"', text: 'Male'},
    {value: '"Female"', text: 'Female'}
    ]; 
    $scope.cca = [
    {value: '"Y"', text: 'Yes'},
    {value: '"N"', text: 'No'}
    ]; 
    $scope.civil = [
    {value: '"Single"', text: 'Single'},
    {value: '"Married"', text: 'Married'},
    {value: '"Legally Separated"', text: 'Legally Separated'},
    {value: '"Widower"', text: 'Widower'},
    ]; 
    $scope.blood = [
    {value: '"O"', text: 'O'},
    {value: '"A"', text: 'A'},
    {value: '"B"', text: 'B'},
    {value: '"AB"', text: 'AB'},
    ]; 
    $scope.relijon = [
    {value: 'Roman Catholic', text: 'Roman Catholic'},
    {value: 'Muslim', text: 'Muslim'},
    {value: 'Protestant', text: 'Protestant'},
    {value: 'Iglesia ni Cristo', text: 'Iglesia ni Cristo'},
    {value: 'El Shaddai', text: 'El Shaddai'},
    {value: 'Jehovahs Witnesses', text: 'Jehovah’s Witnesses'},
    ]; 



});

</script> -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
<!-- jQueryUI Autocomplete -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/menu.js"></script>
<script>

    $(function(){
        "use strict";

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/employee/listofSupervisor",
            cache: false,
            success: function(response)
            {
                var a = jQuery.parseJSON(response);


                $("#supName").autocomplete({
                 source:a,
                 focus: function( event, ui ) {
                    $( "#supName" ).val( ui.item.label );
                    return false;
                },
                select: function( event, ui ){
                    $( "#supName" ).val( ui.item.label );
                    $( "#supEmail" ).val( ui.item.email );
                    $( "#supID" ).val( ui.item.emp_id );
                    return false;
                }
            }).autocomplete( "instance" )._renderItem = function( ul, item ) {
                  return $( "<li>" ).append( "<div>" + item.label + "<br>" + item.email + "</div>" ).appendTo( ul );
              };

          }
      });   




    });


</script>
<!-- Touchspin -->
</head>

<body>
    <div id="sb-site">


        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div id="page-wrapper">
            <div id="page-header" class="<?php echo $setting['settings']; ?>">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                    <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                </div>
                <div id="header-logo" class="logo-bg">
                   <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                   <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id='headerLeft'>
            </div>
        </div>
        <div id="page-sidebar">
            <div class="scroll-sidebar">
                <ul id="sidebar-menu">
                </ul>
            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content">
                <div class="container">
                    <div id="page-title">
                        <div class="form-group">
                           <div class="panel col-md-8"  >
                            <div class="panel-body">
                                <a href="../employee"><button class="btn btn-xs btn-warning"> Back</button></a>
                                <hr>
                                <div class="example-box-wrapper">
                                    <div id="form-wizard-1">
                                        <ul>
                                            <li><a href="#tab1" data-toggle="tab">EMPLOYEE INFORMATION</a></li>
                                            <li><a href="#tab2" data-toggle="tab">EMPLOYMENT DETAILS</a></li>
                                            <li><a href="#tab4" data-toggle="tab">PAYROLL DETAILS</a></li>
                                        </ul>
                                        <div class="tab-content" >
                                            <div class="tab-pane" id="tab1">
                                                <div class="content-box">
                                                    <h3 class="content-box-header bg-primary">
                                                        Personal Information
                                                    </h3>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">First Name:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="fname"><?php echo $emp['fname']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Middle Name:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="mname"><?php echo $emp['mname']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Last Name:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="lname"><?php echo $emp['lname']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Nick Name:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="nikname"><?php echo $emp['nikname']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Birthday:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="bday"><?php echo $emp['bday']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Gender:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="gender" data-type="select" data-value="<?php echo $emp['gender']; ?>" data-pk="1" data-title="Select Tax Category"><?php echo $emp['gender']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Civil:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="civil" data-type="select" data-pk="1" data-value="<?php echo $emp['civil']; ?>" data-title="Select Tax Category"><?php echo $emp['civil']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Contact #:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="cell"><?php echo $emp['cell']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Alt Contact #:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="tel"><?php echo $emp['tel']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div> 

                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Email:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="email"><?php echo $emp['email']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Blood Type:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="bloodtype" data-type="select" data-value="<?php echo $emp['blood']; ?>" data-pk="1" data-title="Select Blood Type"><?php echo $emp['blood']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Religion:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="religion" data-type="select" data-value="<?php echo $emp['relijon']; ?>" data-pk="1" data-title="Select Religion"><?php echo $emp['relijon']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-box">
                                                    <h3 class="content-box-header bg-primary">
                                                        EDUCATIONAL ATTAINMENT

                                                    </h3>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">School Name:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="school_name"><?php echo $emp['school_name']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Course:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="course"><?php echo $emp['course']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">School Note:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="school_note"><?php echo $emp['school_note']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-box">
                                                    <h3 class="content-box-header bg-primary">
                                                        Emergency Contact Person
                                                    </h3>

                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">First Name:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="emergency_fname"><?php echo $emp['emergency_fname']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Middle Name:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="emergency_mname"><?php echo $emp['emergency_mname']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Last Name:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="emergency_lname"><?php echo $emp['emergency_lname']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Contact Number:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="emergency_contact"><?php echo $emp['emergency_contact']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab2">
                                                <div class="content-box">
                                                    <h3 class="content-box-header bg-primary" >
                                                        Employment history
                                                    </h3>

                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">CC Experience:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="ccaExp"><?php echo $emp['ccaExp']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Latest Employer:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="other_latestEmp"><?php echo $emp['other_latestEmp']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Last Held Position:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="other_lastPosition"><?php echo $emp['other_lastPosition']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Year Left:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="other_inclusiveDate"><?php echo $emp['other_inclusiveDate']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-box">
                                                    <h3 class="content-box-header bg-primary">
                                                        Supervisor
                                                    </h3>
                                                    <div class="content-box-wrapper">
                                                        <label class="col-sm-3 control-label">Full name</label>
                                                        <div >
                                                            <input type="text" name="" placeholder="Start typing to see results..." class="form-control autocomplete-input" style="width: 400px;" id='supName' value="<?php echo $Supname; ?>"/>  
                                                        </div>
                                                        <label class="col-sm-3 control-label"> Email Address </label>
                                                        <div ng-controller="Ctrl">
                                                            <input type="text" name="" placeholder="Email address" class="form-control" id='supEmail' style="width: 400px;" value="<?php echo $emp['supEmails']; ?>" disabled/>  
                                                            <input type="hidden" name="" placeholder="Email address" class="form-control" id='supID' style="width: 400px;"  value="<?php echo $emp['Supervisor']; ?>"  />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="content-box">
                                                    <h3 class="content-box-header bg-primary">
                                                        Benefits / Insurance

                                                    </h3>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Health Care Insurance:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="intellicare"><?php echo $emp['intellicare']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Other Insurance:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="pioneer"><?php echo $emp['pioneer']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">TIN/BIR Number:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="BIR"><?php echo $emp['BIR']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Philhealth Number:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="philhealth"><?php echo $emp['philhealth']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Pag-ibig Number:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="pagibig"><?php echo $emp['pagibig']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-box-wrapper">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">SSS Number:</label>
                                                            <div class="col-sm-8">
                                                                <a href="#" id="sss"><?php echo $emp['sss']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab4" style="height: 500px;">
                                             <div class="content-box">
                                                <h3 class="content-box-header bg-primary">
                                                    DTR Login details:
                                                </h3>
                                                <div class="content-box-wrapper">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Username:</label>
                                                        <div class="col-sm-8">
                                                            <a href="#" id="dtr_uname"><?php echo $emp['dtr_uname']; ?></a>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="content-box-wrapper">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Password:</label>
                                                        <div class="col-sm-8">
                                                            <a href="#" id="dtr_password"><?php echo $emp['dtr_password']; ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content-box">
                                                <h3 class="content-box-header bg-primary">
                                                    POSITION
                                                </h3>
                                                <div class="content-box-wrapper">
                                                    <div class="example-box-wrapper">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <b>Code:</b><br><?php echo $pospay->pos_name;?>
                                                            </div>  
                                                            <div class="col-md-6 text-left">
                                                               <b> Description</b>:<br><?php echo $pospay->pos_details;?>
                                                           </div>  
                                                           <div class="col-md-3 text-left">
                                                               <b> Status</b>:<br><?php echo $pospay->status;?>
                                                           </div>         
                                                       </div>                                                    
                                                   </div>

                                               </div>
                                           </div>
                                           <div class="content-box">
                                            <h3 class="content-box-header bg-primary">
                                                Rates
                                            </h3>
                                            <div class="content-box-wrapper">

                                                <div class="example-box-wrapper">
                                                    <div class='row'>
                                                        <div class="col-md-3 text-center">
                                                           <b> Monthly:</b><br>PHP <?php echo $pospay->monthly;?>
                                                       </div>
                                                       <div class="col-md-3 text-center">
                                                          <b> Semi-Monthly:</b><br>PHP <?php echo $pospay->quinsina;?>
                                                      </div>
                                                      <div class="col-md-3 text-center">
                                                        <b> Daily:</b><br>PHP <?php echo $pospay->daily;?>
                                                    </div>
                                                    <div class="col-md-3 text-center">
                                                        <b>Hourly:</b><br>PHP <?php echo $pospay->hourly;?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-box">
                                        <h3 class="content-box-header bg-primary">
                                            Benefits and Taxes
                                            <span class='pull-right'>Semi-Monthly</span>
                                        </h3>
                                        <div class="content-box-wrapper">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">Bureau of Internal Revenue</label>
                                                <div class="col-md-4">
                                                <a href="#" id="selecttax" data-type="select" data-pk="1" data-value="<?php if($taxemp!=NULL){echo $taxemp->tax_desc_id;}?>" data-title="Select Tax Category"></a>                                              </div>
                                             <div id='taxdisplay' class='col-md-3 text-right text-warning' style='font-weight: bold;'></div>
                                         </div>
                                     </div>
                                     <div class="content-box-wrapper">
                                        <div class="form-group">
                                            <label class="col-md-5 control-label">Social Security System</label>
                                            <div class='col-md-7 text-right text-warning' style='font-weight: bold;'>PHP <?php echo number_format($sss->sss_employee/2,2)?></div>
                                        </div>
                                    </div>

                                    <div class="content-box-wrapper">
                                        <div class="form-group">
                                            <label class="col-md-5 control-label">Philhealth</label>
                                            <div class='col-md-7 text-right text-warning' style='font-weight: bold;'>PHP <?php echo number_format($philhealth->employeeshare/2,2);?></div>
                                        </div>
                                    </div>
                                    <div class="content-box-wrapper">
                                        <div class="form-group">
                                            <label class="col-md-5 control-label">Home Development Mutual Fund</label>
                                            <div class='col-md-7 text-right text-warning' style='font-weight: bold;'>PHP <?php echo number_format($pagibig->employeeshare/2,2);?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div  style="background: #009688;"  ng-controller="Ctrl">
            <div class="panel-content image-box" style="background: #009688;">
                <img src="<?php echo base_url()."assets/images/".$emp['pic'];?>" alt="" style="width:300px;border: 20px solid #c6e6e3;">
            </div>
            <div class="panel-content image-box" style="background: #009688;margin-top:-10px;">
                <div  style="background: white;text-align:left;margin-top: -20px;" >
                    <hr>
                    &nbsp;&nbsp;<img src='<?php echo base_url()."assets/images/personal.png"?>' style='width: 25px'> &nbsp; <?php echo $emp['fname']; ?> <?php echo $emp['lname']; ?> <?php echo $emp['lname']; ?> "<i><?php echo $emp['nikname']; ?></i>"
                    <hr>
                    &nbsp;&nbsp;<img src='<?php echo base_url()."assets/images/id.png"?>' style='width: 25px'> &nbsp; ID number  <a href="#" editable-text="user.id_num" id="empidnum"><?php echo $emp['id_num']; ?></a>
                    <hr>
                    &nbsp;&nbsp;&nbsp;<img src='<?php echo base_url()."assets/images/application.png"?>' style='width: 20px'> &nbsp; Application Details &nbsp;&nbsp;&nbsp; <input type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#viewApply" value="view">
                    <hr>
                    &nbsp;&nbsp;&nbsp;<img src='<?php echo base_url()."assets/images/ir.png"?>' style='width: 20px'> &nbsp; Incident Report Summary &nbsp;&nbsp;&nbsp; <input type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#viewIR" value="view">&nbsp;<input type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#addIR" value="add">
                    <hr>
                </div>
            </div>
        </div>


    </div>
    <div id="viewApply" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
            <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
</div>
<div id="viewIR" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
    </div>
    <div class="modal-body">
        <p>Some text in the modal.</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>

</div>
</div>
<div id="addIR" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
    </div>
    <div class="modal-body">
        <p>Some text in the modal.</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>

</div>
</div>
<div id='Templatedesign'>
</div>
</div>

<div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>




        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>

</div>
</div>



</div>
</div>
</div>
<p class="growl1" hidden></p>
<p class="growl2" hidden></p>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
       }
   });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
     }
 });
});   
</script>
<script type="text/javascript">
    $( document ).ready(function() {
        var salary = <?php echo $pospay->quinsina;?>;

        if(<?php echo $taxemp->tax_desc_id;?>==''||<?php echo $taxemp->tax_desc_id;?>=='NULL'){

            $("#taxdisplay").html("");
        }else{
           $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Employee/getTaxEmployeeShare/"+<?php echo $taxemp->tax_desc_id;?>,
            data: {
                salary: salary
            },
            cache: false,
            success: function(res)
            {
                $("#taxdisplay").html("PHP "+parseFloat(res).toFixed(2));
            }
        })
       };
   });

</script>
<script>
    $(function(){
        $("#fname").editable({type:"text",pk:1,name:"fname",title:"Enter First Name",mode:"inline"}),   
        $("#mname").editable({type:"text",pk:1,name:"mname",title:"Enter Middle Name",mode:"inline"}),
        $("#lname").editable({type:"text",pk:1,name:"lname",title:"Enter Last Name",mode:"inline"}),   
        $("#nikname").editable({type:"text",pk:1,name:"nikname",title:"Enter Nick Name",mode:"inline"}), 
        $("#bday").editable({type:"date",pk:1,name:"bday",title:"Enter Birth Day",mode:"inline"}),
        $("#cell").editable({type:"text",pk:1,name:"cell",title:"Enter Contact #",mode:"inline"}),
        $("#tel").editable({type:"text",pk:1,name:"tel",title:"Enter Alt Contact #",mode:"inline"}),
        $("#email").editable({type:"email",pk:1,name:"email",title:"Enter Email",mode:"inline"}),
        $("#school_name").editable({type:"text",pk:1,name:"school_name",title:"Enter School Name",mode:"inline"}),
        $("#course").editable({type:"text",pk:1,name:"course",title:"Enter Course",mode:"inline"}),
        $("#school_note").editable({type:"text",pk:1,name:"school_note",title:"Enter Remarks",mode:"inline"}),
        $("#emergency_fname").editable({type:"text",pk:1,name:"emergency_fname",title:"Enter First Name",mode:"inline"}),
        $("#emergency_mname").editable({type:"text",pk:1,name:"emergency_mname",title:"Enter Middle Name",mode:"inline"}),
        $("#emergency_lname").editable({type:"text",pk:1,name:"emergency_lname",title:"Enter Last Name",mode:"inline"}),
        $("#emergency_contact").editable({type:"text",pk:1,name:"emergency_contact",title:"Enter Contact",mode:"inline"}),
        $("#ccaExp").editable({type:"text",pk:1,name:"ccaExp",title:"Enter CC Experience",mode:"inline"}),
        $("#other_latestEmp").editable({type:"text",pk:1,name:"other_latestEmp",title:"Enter Latest Employer",mode:"inline"}),
        $("#other_lastPosition").editable({type:"text",pk:1,name:"other_lastPosition",title:"Enter Last Held Position",mode:"inline"}),
        $("#other_inclusiveDate").editable({type:"text",pk:1,name:"other_inclusiveDate",title:"Enter Year Left",mode:"inline"}),
        $("#intellicare").editable({type:"text",pk:1,name:"intellicare",title:"Enter Health Care Insurance",mode:"inline"}),
        $("#pioneer").editable({type:"text",pk:1,name:"pioneer",title:"Enter Other Insurance",mode:"inline"}),
        $("#BIR").editable({type:"text",pk:1,name:"BIR",title:"Enter TIN/BIR Number",mode:"inline"}),
        $("#philhealth").editable({type:"text",pk:1,name:"philhealth",title:"Enter Philhealth Number",mode:"inline"}),
        $("#pagibig").editable({type:"text",pk:1,name:"pagibig",title:"Enter Pag-ibig Number",mode:"inline"}),
        $("#sss").editable({type:"text",pk:1,name:"sss",title:"Enter SSS Number",mode:"inline"}),
        $("#dtr_uname").editable({type:"text",pk:1,name:"dtr_uname",title:"Enter Username",mode:"inline"}),
        $("#dtr_password").editable({type:"text",pk:1,name:"dtr_password",title:"Enter Password",mode:"inline"})
    });
    $(function(){
        $("#civil").editable({
            mode:"inline",name:"civil",
            source:
            [
            {value: 'Single', text: 'Single'},
            {value: 'Married', text: 'Married'},
            {value: 'Legally Separated', text: 'Legally Separated'},
            {value: 'Widower', text: 'Widower'}
            ]
            
        })
        .on('save',function(e,params){updater('tbl_employee','civil',params.newValue);});
        $("#gender").editable({
            mode:"inline",name:"gender",
            source:
            [
            {value: 'Male', text: 'Male'},
            {value: 'Female', text: 'Female'}
            ]
            
        })
        .on('save',function(e,params){updater('tbl_applicant','gender',params.newValue);}); 
        $("#bloodtype").editable({
            mode:"inline",name:"gender",
            source:
            [
            {value: 'O', text: 'O'},
            {value: 'A', text: 'A'},
            {value: 'B', text: 'B'},
            {value: 'AB', text: 'AB'}
            ]
            
        })
        .on('save',function(e,params){updater('tbl_employee','blood',params.newValue);});   
        $("#religion").editable({
            mode:"inline",name:"religion",
            source:
            [
            {value: 'Roman Catholic', text: 'Roman Catholic'},
            {value: 'Muslim', text: 'Muslim'},
            {value: 'Protestant', text: 'Protestant'},
            {value: 'Iglesia ni Cristo', text: 'Iglesia ni Cristo'},
            {value: 'El Shaddai', text: 'El Shaddai'},
            {value: 'Jehovahs Witnesses', text: 'Jehovah’s Witnesses'}
            ]
            
        })
        .on('save',function(e,params){updater('tbl_applicant','relijon',params.newValue);}); 



    })
</script>
<script>
    $(function(){
        $("#selecttax").editable({
            mode:"inline",
            source:
            // {value:1,text:"Male"},
            // {value:2,text:"Female"}
            <?php echo $taxdesc;?>
            
        }).on('save',function(e,params){
            var salary = <?php echo $pospay->quinsina;?>;

            if(params.newValue==''){

                $("#taxdisplay").html("");
            }else{
               $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Employee/getTaxEmployeeShare/"+params.newValue,
                data: {
                    salary: salary
                },
                cache: false,
                success: function(res)
                {
                   $("#taxdisplay").html("PHP "+parseFloat(res).toFixed(2));
                   $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/Employee/insertNewTaxValue",
                    data: {
                        taxid: params.newValue,
                        emp_id: "<?php echo $emp['emp_id']; ?>"
                    },
                    cache: false,
                    success: function(result)
                    {
                      if(result=='Success'){
                       $(".growl1").trigger('select');
                   }else{
                       $(".growl2").trigger('select');
                   }
               }
           });

               }
           })
           };
       })

    });
</script>
<script type="text/javascript">
    $(".growl1").select(function(){
        $.jGrowl(
            "Successfully Updated.",
            {sticky:!1,position:"top-right",theme:"bg-blue-alt"}
            )
    });
    $(".growl2").select(function(){
        $.jGrowl(
            "Error Updating.",
            {sticky:!1,position:"top-right",theme:"bg-red"}
            )
    });
    function updater(table,name,value){
        if(table=='tbl_employee'){
            var id = "<?php echo $emp['emp_id']; ?>";
            var idname = 'emp_id';
        }else if(table=='tbl_applicant'){
            var id = "<?php echo $emp['apid']; ?>";
            var idname = 'apid';
        }
        if(name=='bday'){
            value = moment(value).format('YYYY-MM-DD');
        }
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Employee/UpdateEmployeeValue",
            data: {
                id: id,
                idname: idname,
                table: table,
                name: name,
                value: value
            },
            cache: false,
            success: function(res)
            {
                // alert(res);
                if(res=='Success'){
                   $(".growl1").trigger('select');
               }else{
                   $(".growl2").trigger('select');
               }

           }
       });
    }
</script>
<script type="text/javascript">
    $("#fname").on('save',function(e,params){updater('tbl_applicant','fname',params.newValue);}),   
    $("#mname").on('save',function(e,params){updater('tbl_applicant','mname',params.newValue);}),
    $("#lname").on('save',function(e,params){updater('tbl_applicant','lname',params.newValue);}),   
    $("#nikname").on('save',function(e,params){updater('tbl_employee','nikname',params.newValue);}), 
    $("#bday").on('save',function(e,params){updater('tbl_employee','bday',params.newValue);}),
    $("#cell").on('save',function(e,params){updater('tbl_applicant','cell',params.newValue);}),
    $("#tel").on('save',function(e,params){updater('tbl_applicant','tel',params.newValue);}),
    $("#email").on('save',function(e,params){updater('tbl_employee','email',params.newValue);}),
    $("#school_name").on('save',function(e,params){updater('tbl_employee','school_name',params.newValue);}),
    $("#course").on('save',function(e,params){updater('tbl_employee','course',params.newValue);}),
    $("#school_note").on('save',function(e,params){updater('tbl_employee','school_note',params.newValue);}),
    $("#emergency_fname").on('save',function(e,params){updater('tbl_employee','emergency_fname',params.newValue);}),
    $("#emergency_mname").on('save',function(e,params){updater('tbl_employee','emergency_mname',params.newValue);}),
    $("#emergency_lname").on('save',function(e,params){updater('tbl_employee','emergency_lname',params.newValue);}),
    $("#emergency_contact").on('save',function(e,params){updater('tbl_employee','emergency_contact',params.newValue);}),
    $("#ccaExp").on('save',function(e,params){updater('tbl_applicant','ccaExp',params.newValue);}),
    $("#other_latestEmp").on('save',function(e,params){updater('tbl_employee','other_latestEmp',params.newValue);}),
    $("#other_lastPosition").on('save',function(e,params){updater('tbl_employee','other_lastPosition',params.newValue);}),
    $("#other_inclusiveDate").on('save',function(e,params){updater('tbl_employee','other_inclusiveDate',params.newValue);}),
    $("#intellicare").on('save',function(e,params){updater('tbl_employee','intellicare',params.newValue);}),
    $("#pioneer").on('save',function(e,params){updater('tbl_employee','pioneer',params.newValue);}),
    $("#BIR").on('save',function(e,params){updater('tbl_employee','BIR',params.newValue);}),
    $("#philhealth").on('save',function(e,params){updater('tbl_employee','philhealth',params.newValue);}),
    $("#pagibig").on('save',function(e,params){updater('tbl_employee','pagibig',params.newValue);}),
    $("#sss").on('save',function(e,params){updater('tbl_employee','sss',params.newValue);}),
    $("#dtr_uname").on('save',function(e,params){updater('tbl_employee','dtr_uname',params.newValue);}),
    $("#dtr_password").on('save',function(e,params){updater('tbl_employee','dtr_password',params.newValue);})
</script>
<script type="text/javascript">
    $("#supName").on('change',function(){
        var supemail = $("#supEmail").val();
        var supid = $("#supID").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Employee/UpdateEmployeeSupervisor",
            data: {
                emp_id: "<?php echo $emp['emp_id']; ?>",
                email: supemail,
                supervisor: supid
            },
            cache: false,
            success: function(res)
            {
                // alert(res);
                if(res=='Success'){
                   $(".growl1").trigger('select');
               }else{
                   $(".growl2").trigger('select');
               }

           }
       });
    });
</script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>