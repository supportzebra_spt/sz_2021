<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
<head>
    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Login page 2 </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>



</head>
<body>
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <style type="text/css">

        html,body {
            height: 100%;
            background: #fff;
        }

    </style>

    <!-- Parsley -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>

    <div class="center-vertical">
        <div class="center-content row">
            <form action="#" id="loginform" class="col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin" method="POST"  data-parsley-validate>
                <h3 class="text-center pad25B font-gray text-transform-upr font-size-23">Applicant <span class="opacity-80">Login</span></h3>
                <div id="login-form" class="content-box bg-default">
                    <div class="content-box-wrapper pad20A">
                        <img class="mrg25B center-margin radius-all-100 display-block img-responsive"  src="<?php echo base_url();?>assets/images/logo2.png" alt="">
                        <div id="error" class="text-danger text-center"></div>       
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="username" placeholder="Enter Username" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-primary">Login</button>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>




    <!-- JS Demo -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
    <script type="text/javascript">
        $('#loginform').parsley().subscribe('parsley:form:validate', function (formInstance) {
        formInstance.submitEvent.preventDefault(); //stops normal form submit
        if (formInstance.isValid() == true) { // check if form valid or not
            //code for ajax event here
            var data = $("#loginform").serialize();
            $.ajax({
                url: "<?php echo base_url();?>index.php/applicant/doLogin",
                data: data,
                type: "POST",
                success: function(msg) {
                    if (msg != 'Success') {
                        $("#error").html(msg);
                    }else{
                        window.location = "<?php echo base_url();?>index.php/applicant/landing";
                    }
                }
            });
        }});
       

    </script>
</body>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Dec 2016 05:42:46 GMT -->
</html>