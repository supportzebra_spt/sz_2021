<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>


</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="<?php echo $setting['settings']; ?>">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
              <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
<div class="form-group">
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            User Access
        </h3>
        <div class="example-box-wrapper">
            <div id="form-wizard-3" class="form-wizard">
                <ul>
                    <li>
                        <a href="#step-1" data-toggle="tab" id='ones'>
                            <label class="wizard-step">1</label>
                      <span class="wizard-description">
                         User
					<small>List Of Users</small>

                       </span>
                        </a>
                    </li>
                    <li >
                        <a href="#step-2" data-toggle="tab" id='twos' >
                            <label class="wizard-step">2</label>
                      <span class="wizard-description" >
                         Menu
                         <small>List of all menu names</small>
                      </span>
                        </a>
                    </li>
                  
                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="step-1">
                        <div class="content-box">
                            <h3 class="content-box-header bg-blue">
                                List Of Users  
                            </h3>
                            <div class="content-box-wrapper" id='ListofUsers'>
 

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="step-2">
                        <div class="content-box">
                            <h3 class="content-box-header bg-azure">
 
 				List of all  menu  
<!--				<button class="btn btn-xs btn-default" style="float: right;" id="btnAddMenu" data-toggle="modal" data-target="#myModalMenu">Save</button>-->
				<button class="btn btn-xs btn-default" style="float: right;" id="btnAddMenu">Save</button>
                            </h3>
                            <div class="content-box-wrapper" id='ListofUsersTabs'>
							  <select multiple="" class="form-control" id = "select-menu">
                                                </select>
                             </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="step-3">
                        <div class="content-box">
                            <h3 class="content-box-header bg-green">
                                List of all menu tab <button class="btn btn-xs btn-default" style="float: right;">ADD</button>
                            </h3>
							
                            <div class="content-box-wrapper" id='ListofUsersTabitems'>
                             </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>	
                </div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>

 </div>

<div id="myModalMenu" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
		<div id="modalmenubody">
			


		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id='btnAddMenuToUser' >Save</button>
      </div>
    </div>

  </div>
</div>
</body>
<script>
  $(function(){
 	    $.ajax({
		type: "POST",
		url:   'Templatedesign/headerLeft',
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	
	$.ajax({
		type: "POST",
		url: "Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{ 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });

  });	
</script>
<script>
  $(function(){
   
 
$('#btnAddMenuToUser').click(function(){
  		var usertabsNo= $("#usertabsNo:checked").val(); 
 
		var dataString = 'usertabsNo='+ usertabsNo;
	if(usertabsNo == null){
		swal(
		'ERROR',
		'No Menu selected',
		'error'
		)
$('#ListofUsersTabs').html("<font color=red>NO RECORDS FOUND!</font>");

	}else{
			$.ajax({
			type: "POST",
			url: "useraccessrights/usertabsAdd",
			data: dataString,
			cache: false,
			success: function(html)
			{

			}
	  });	  
		  
		}	
});
$('#btnAddMenu').click(function(){
  		var userliz= $("#userliz:checked").val(); 
 
		  var arr = $('#usertabs:checked').map(function () {
			return this.value;
			}).get();

	 		var dataString = 'userliz='+ userliz+'&menu_item_idz='+arr;
     // alert(dataString);
if(userliz == null){
swal(
  'ERROR',
  'Please select a user',
  'error'
)
$('#ListofUsersTabs').html("<font color=red>NO RECORDS FOUND!</font>");

 }else{
	
	 	$.ajax({
			type: "POST",
			url: "useraccessrights/usertabsNo",
			data: dataString,
			cache: false,
			success: function(html)
			{
				swal(
  'Success',
  'You have just successfully saved the record!',
  'success'
)
			// alert(html);
		// $('#modalmenubody').html(text);
  
 			 
			}
	  });	    
		  
		}	
});
 
$('#twos').click(function(){
  		var userliz= $("#userliz:checked").val(); 
 
 var dataString = 'userliz='+ userliz;
 
 if(userliz == null){
swal(
  'ERROR',
  'Please select a user',
  'error'
)
$('#ListofUsersTabs').html("<font color=red>NO RECORDS FOUND!</font>");

 }else{
		$.ajax({
			type: "POST",
			url: "useraccessrights/usertabs",
			data: dataString,
			cache: false,
			success: function(result)
			{
			   // $('#ListofUsers').html(html);
   // console.log(html);
   var text= "";
   var i;
   var obj = jQuery.parseJSON(result);
  
  $(obj).each(function(key, value) {
			// if(value.is_selected==1){
				i = (value.is_selected==1) ? "checked" : " ";
				 
				text += "<input type='checkbox' id='usertabs' name='usertabs' value="+value.menu_item_id+" "+i+" >"+" <b>"+value.tab_name+"</b> "+value.item_title+"<br>";
			// }else{
				// text += "<input type='checkbox' id='usertabs' name='usertabs' value="+value.menu_item_id+" >"+" <b>"+value.tab_name+"</b> "+value.item_title+"<br>";
	
			// }

	});
		  $('#ListofUsersTabs').html(text);
	   
 
			 
			}
	  });	  
		  
		}
});
$('#ones').click(function(){
  	
 		    $.ajax({
type: "POST",
url: "useraccessrights/userlistaccess",
cache: false,
success: function(html)
{
 
  // $('#ListofUsers').html(html);
   // console.log(html);
   var text= "";
   var i ;
   var obj = jQuery.parseJSON(html);
   for(i in obj){
		var val = obj[i];
		var res = val.split("|");
			
 		   text += "<input type='radio' id='userliz' name='userliz' value="+res[0]+">"+" "+res[1]+" "+res[2]+"<br>";
	    
   }
		$('#ListofUsers').html(text);
	}
  });	  
		  
	
});
$('#select-type').change(function(){
	var accType= $(this).val(); 
 	
 var dataString = 'accType='+ accType;

		    $.ajax({
type: "POST",
url: "latetracking/get_account",
data: dataString,
cache: false,
success: function(html)
{
 
  $('#select-account').html(html);
 // alert(html);
  }
  });	  
		  
	
});


  });
  
  </script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>