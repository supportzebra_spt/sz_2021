<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> SUPPORTZEBRA | PAYROLL COVERAGE </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

<script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true,
			order: [[ 1, "desc" ]]
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });
function changeStatus(code,cid){
 if(code==1  ){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Payroll/statCoverage",
		data:  {code:code,cid:cid},
 		cache: false,
		success: function(html)
		{
			location.reload();
 		 
		}
  });
	 
 

}else if(code==2){
swal({
  title: "Are you sure?",
  text: "Records are already locked and can't be modified.",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, Lock it down!",
  cancelButtonText: "No, cancel it please!",
  closeOnConfirm: false,
  closeOnCancel: false
}).then(function(isConfirm) {
  if (isConfirm) {
 
		$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Payroll/statCoverage",
		data:  {code:code,cid:cid},
 		cache: false,
		success: function(html)
		{
			setTimeout(function(){location.reload();},1000);
			swal("Locked!","Records are already locked","success");
 		 
		}  });
  }else{
    swal("Cancelled",":)",	"error");
  }
});
}else{
	
swal({
  title: 'Are you sure you want to UNLOCK?',
  html: '<span style="color:red">NOTE: </span>There might be data that will be affected if you update some record.',
  input: 'password',
  showCancelButton: true,
  confirmButtonText: 'Submit',
  showLoaderOnConfirm: true,
  allowOutsideClick: false
}).then(function(email) {
   
		if(email=="HRT@2018"){
			$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Payroll/statCoverage",
		data:  {code:code,cid:cid},
 		cache: false,
		success: function(html)
		{
			setTimeout(function(){location.reload();},2000);
			swal({type: 'success',title: 'Unlocked',html:  'You have successfully unlocked the Payroll!',}); 
 		 
		}  });
		}else{
			swal({
			type: 'error',
			title: 'Something went wrong!'
			}); 
		}
   
 
})
}
}
</script>
<!-- jQueryUI Spinner -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/spinner/spinner.js"></script>
<script type="text/javascript">
    /* jQuery UI Spinner */

    $(function() { "use strict";
        $(".spinner-input").spinner();
    });
</script>
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
	<?php 
		// $status = explode("-",$stat["status"]);
		// $st1 = ($status[0]=="agent") ? "Agent" : "Admin";
		// $st2 = ($status[1]=="probationary" || $status[1]=="regular" || $status[1]=="trainee") ? ucfirst($status[1]) : "Probationary & Regular";
	?>
	
 
    	<a data-toggle="modal" data-target="#myModal" class="btn btn-xs btn-success glyph-icon   icon-iconic-plus" title="" style="margin-bottom: 9px;"> Add </a>
 
	<div class="form-group">

<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
		 
<thead>
<tr>
    <th >ID</th>
    <th >Month</th>
    <th>Date Range</th>
    <th>Transaction Date</th>
    <th>Status</th>
    <th>Summary</th>
   
</tr>
</thead>

<tfoot>
<tr> 
	<th >ID</th>
    <th>Month</th>
    <th>Date Range</th>
    <th>Transaction Date</th>
    <th>Status</th>
    <th>Summary</th>
</tr>
</tfoot>

<tbody>
 <?php foreach($pcover as $key => $val){?>
 <?php 
$month = explode("-",$val['daterange']);
$month1 = date("F",strtotime($month[0]));
 
	if($val['status']=="Final"){
		$statuz = "<i class='glyph-icon icon-lock' title='icon-lock' style='color: green;'> </i> <a href='#' onclick='changeStatus(3,".$val['coverage_id'].")'> Final</a>";
	}else if($val['status']=="Review"){
		$statuz = "<i class='glyph-icon icon-eye' title='icon-eye'  style='color: orange;'> </i>  <a href='#' onclick='changeStatus(2,".$val['coverage_id'].")'> Review</a>";

	
	}else{
		$statuz = "<i class='glyph-icon icon-dot-circle-o' title='.icon-dot-circle-o'  style='color: #5aecff;'> </i> <a href='#' onclick='changeStatus(1,".$val['coverage_id'].")'> Pending</a>";

	}
		
		
		?>
<tr>

	<td><?php echo  $val['coverage_id']; ?></td>
	<td><?php echo  $month1; ?></td>
    <td><?php echo  $val['daterange']; ?></td>
    <td><?php echo  $val['transact_date']; ?></td>
    <td><span id="stsusPayroll"><?php echo $statuz; ?></span></td>
   <!-- <td><a href="<?php //echo base_url()."index.php/Payroll/GeneratePayroll/".$stat["status"]."/".$val['coverage_id']?>">view</a></td>-->
	<td><a href="<?php echo base_url()."index.php/Payroll/payroll_index/".$val['coverage_id']; ?>">view</a></td>

</tr>
<?php } ?>
</tbody>
</table>
	</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payroll Period</h4>
      </div>
      <div class="modal-body">
 <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    PAYROLL SETUP
                </h3>
                <div class="example-box-wrapper">
                    <form class="form-horizontal bordered-row" role="form">
						   <div class="form-group">
								<label class="col-sm-3 control-label">Year</label>
								<div class="col-sm-4">
									<input type="text" name="" class="form-control spinner-input" value="<?php echo date("Y"); ?>" id='pcyear'/>
								</div>
							</div>
                        <div class="form-group">
							<label class="col-sm-3 control-label">Month</label>
							<div class="col-sm-6">
								<select class="form-control" id='pcmonth'>
									<option></option>
									<option>January</option>
									<option>February</option>
									<option>March</option>
									<option>April</option>
									<option>May</option>
									<option>June</option>
									<option>July</option>
									<option>August</option>
									<option>September</option>
									<option>October</option>
									<option>November</option>
									<option>December</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Period</label>
							<div class="col-sm-6">
								<select class="form-control" id='pcperiod'>
									<option></option>
									<option>1</option>
									<option>2</option>
								</select>
							</div>
						</div>
                         <div class="form-group">
								<label class="col-sm-3 control-label">Description</label>
								<div class="col-sm-8">
									<input type="text" name="" class="form-control" id="pcDescription" disabled="disabled"/>
								</div>
						 </div>
                    </form>
                </div>
            </div>
        </div> 
		</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" id="pcSave" >Save</button>
        <button type="button" class="btn btn-success" id="pcSave1" >Saves</button>
      </div>
    </div>

  </div>
</div>
    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>
            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>

</div>
</body>
<script>
  $(function(){
/* 	    $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	 */
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
  });	
</script>
<script> 

 $(function(){
	 $("#pcSave1").hide();
	$("#pcperiod").click(function(){
		 $("#pcSave1").hide();
		 $("#pcSave").show();
	});
	$("#pcmonth").click(function(){
		 $("#pcSave1").hide();
		 $("#pcSave").show();
	});
	$("#pcSave1").click(function(){
		var pcyear = $("#pcyear").val();
	var pcmonth = $("#pcmonth").val();
	var pcperiod = $("#pcperiod").val();
	var pcDescription = $("#pcDescription").val();
	var dataString = "pcyear="+pcyear+"&pcmonth="+pcmonth+"&pcperiod="+pcperiod+"&pcDescription="+pcDescription;
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Payroll/pcInsert",
		data:  dataString,
 		cache: false,
		success: function(html)
		{
		if(html!=0){
		 swal("Successfully inserted!", "success");
		 $("#pcSave1").prop("disabled", true);
		  // setTimeout(function() { location.reload(); }, 2000);
		     $.ajax({
                //url of the function
                    url: '<?php echo base_url() ?>index.php/payrollchecker/loan/'+html,
                    type: 'POST',
  					beforeSend:function(){
						$("#loads").show();
						$("#tblShow").html("");

					},
                    success:function(res)
                    {
						  //setTimeout(function() { location.reload(); }, 2000);
					}
		   });
		}else{
			swal("Record was not successfully inserted!", "error");

		}
		}
  });
	});
 
	$("#pcSave").click(function(){
        var monthz = {
            January: "01",
            February: "02",
            March: "03",
            April: "04",
            May: "05",
            June: "06",
            July: "07",
            August: "08",
            September: "09",
            October: "10",
            November: "11",
            December: "12"
        }

	 
	var pcyear = $("#pcyear").val();
	var pcmonth = $("#pcmonth").val();
	var pcperiod = $("#pcperiod").val();
	var dataString = "pcyear="+pcyear+"&pcmonth="+pcmonth+"&pcperiod="+pcperiod;
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Payroll/pcCheck",
		data:  dataString,
 		cache: false,
		success: function(html)
		{
		 
 		   if(html==1){
			   alert("Already have a record!");
		   }else if(html==0){
				var d = new Date(pcyear,parseInt(monthz[pcmonth]),0); //this is last day of current month   
 
			   if(pcperiod==1){
					$("#pcDescription").val(monthz[pcmonth]+"/01/"+pcyear+" - "+monthz[pcmonth]+"/"+15+"/"+pcyear);
					$("#pcSave1").show();
					$("#pcSave").hide();
			   }else{
					$("#pcDescription").val(monthz[pcmonth]+"/"+16+"/"+pcyear+" - "+monthz[pcmonth]+"/"+d.getDate()+"/"+pcyear);
					$("#pcSave1").show();
					$("#pcSave").hide();
			   }
		   }else{
				alert("All Fields must be filled-out!");
				$("#pcDescription").val("");
		   }
		}
  });
  });
	});
</script>
 

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>