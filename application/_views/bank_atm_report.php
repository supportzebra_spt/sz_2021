<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		thead {
			background: #7b7b7b;
			color: white;
			font-size: smaller;
		}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> SUPPORTZEBRA | BANK ATM REPORT </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">
	 
	<div class="col-md-12">
	
        <div class="panel">
            <div class="panel-body">
		<center><img src="<?php echo base_url(); ?>assets/images/pnb.jpg"></center>
                <div class="example-box-wrapper">
                    <form class="form-horizontal bordered-row" role="form">
		
                         <div class="form-group" id="employzResult">
                    <div class="col-sm-12">
                 
         	<form id="demoform" action="#" method="post">

			<div class="form-group">
			<label class="col-sm-3 control-label">Payroll Coverage:</label>
			<div class="col-sm-6">
			<select name="" class="chosen-select" id="payrollC">

			<option disabled="disabled" selected >--</option>
			<?php foreach($index as $row){?>
			<optgroup label="<?php echo $row->month; ?>">
			<?php 
			$exp = explode(",",$row->daterange);
			for($i=0;$i<count($exp);$i++){	
			$exp2 = explode("|",$exp[$i]);

			?>
			<option value="<?php echo $exp2[0]; ?>"><?php echo $exp2[1]; ?></option>
			<?php } ?>
			</optgroup>
			<?php } ?>
			</select>

			</div>

			</div>
			<div class="form-group">
                    <label class="col-sm-3 control-label">Mode</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="atmType">
                            <option value="3">BOTH</option>
                            <option value="2">HOLD</option>
                            <option value="1">ATM</option>
                            <option value="0">NON-ATM</option>
                        </select>
                    </div>
                </div>



			</form>

                    </div>
                </div>
		<input type="button" class="btn btn-xs btn-info" id="btnShow" value="Show">
		<input type='button' class="btn  btn-xs btn-success" id="excel_button1" value='Excel'>    
		<input type='button' class="btn  btn-xs btn-success" id="excel_button2" value='Excel_get_coverage'  style="display:none"> 
		

 		<!--<input type='button' class="btn btn-xs btn-info" id="print_button1" value='Print'>-->
                    </form>
						<div id="divNote"></div>
                </div>
            </div>
        </div>
		<a href="#" data-toggle="modal" data-target="#modalPDF"><input type='button' class="btn btn-xs btn-info" id="email_button2" value='PDF Send' style="display:none;"></a>

      
    </div>
<div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                
                <div class="example-box-wrapper">
						<!--<div id="loadingIMG" style="position: fixed; margin: -20% auto 0px; width: 100%; z-index: 1000; display: none;margin-left:20%;">
							 <img src="<?php echo base_url();?>assets/images/loading-gears-animation-3.gif"  > 
						</div>-->
                    <form class="form-horizontal bordered-row" role="form" id="tblShow2" >

                         
 						 <div id="tblShow">
						 <table id="datatable-row-highlight" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								 <td> ATM Account Number</td>
								 <td>	Last name</td>
								 <td>	First Name</td>
								 <td>	Middle Name</td>
								 <td>	Type </td>
								 <td>	Amount </td>
							 </tr>
						</thead>	 
							<tbody id="tblData">
							
							</tbody>
						 </table>
                         </div>
                        
                    </form>
                </div>
            </div>
        </div>
   
       
    </div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
	    $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
  });	
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

<script type="text/javascript">

    /* Datatables basic */

    $(document).ready(function() {
        $('#datatable-example').dataTable();
    });

    /* Datatables hide columns */

    $(document).ready(function() {
        var table = $('#datatable-hide-columns').DataTable( {
            "scrollY": "300px",
            "paging": false
        } );

        $('#datatable-hide-columns_filter').hide();

        $('a.toggle-vis').on( 'click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column( $(this).attr('data-column') );

            // Toggle the visibility
            column.visible( ! column.visible() );
        } );
    } );

    /* Datatable row highlight */

    $(document).ready(function() {
        var table = $('#datatable-row-highlight').DataTable();

        $('#datatable-row-highlight tbody').on( 'click', 'tr', function () {
            $(this).toggleClass('tr-selected');
        } );
    });



    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>
<script>
	function initResultDataTable(){
    $('#datatable-row-highlight').DataTable({
                        "order": [],
                        "columnDefs": [ {
                        "targets"  : 'no-sort',
                        "orderable": false,
                        }]
                });
}
	$(function(){
		$("#btnShow").click(function(){
			var cid = $("#payrollC").val();
			var atmType = $("#atmType").val();
			$.ajax({
				type: "POST",
				data: {cid:cid,atmType:atmType},
				url: "<?php echo base_url(); ?>/index.php/payroll/bank_atm_show_details",
				cache: false,
				success: function(html)
				{
						var obj = jQuery.parseJSON(html);
						  var txt = '';
   						$.each(obj,function(index, value){
							var atm_account_number = value["atm_account_number"];
							var isATM = (value["isAtm"]==1) ? "ATM" : "NON-ATM";
						txt+='<tr id="searched-row-'+index+'" class="js-result-tbl-tbody-tr"><td>'+((atm_account_number!=null && atm_account_number!="") ? atm_account_number : "<span style='color:red'>Not Set</span>")+"</td><td>"+value["lname"]+"</td><td>"+value["fname"]+"</td><td>"+value["mname"]+"</td><td>"+isATM+"</td><td>"+value["final"]+"</td></tr>";
						
						});	
						if ( $.fn.DataTable.isDataTable('#datatable-row-highlight') ) {
							  $('#datatable-row-highlight').DataTable().destroy();
							}

							$('#datatable-row-highlight tbody').empty();
							$('#tblData').html(txt);
							// ... skipped ...

							$('#datatable-row-highlight').dataTable({
								  "autoWidth":true
								, "info":true
								, "iDisplayLength":20
								, "JQueryUI":true
								, "ordering":true
								, "searching":true
								, "paging":true
								,"bFilter": false
								, "scrollCollapse":true
								,  "order": [[ 2, "asc" ]]
							});
						// initResultDataTable();
				}
			});
			
		});
		$("#excel_button1").click(function(){
			var cid = $("#payrollC").val();
			var atmType = $("#atmType").val();

			 $.ajax({
                     url: '<?php echo base_url() ?>index.php/payroll/exportATM_excel',
					 data: {cid:cid,atmType:atmType},
                     type: 'POST',
                     success:function(res)
						{
						
							$("#excel_button2").trigger("click");
						}
                });  
		});
		$("#excel_button2").click(function(){
			var cid = $("#payrollC").val();
			var atmType = $("#atmType").val();

			 $.ajax({
                     url: '<?php echo base_url() ?>index.php/payroll/getcoverage_date',
					 data: {cid:cid,atmType:atmType},
                     type: 'POST',
                     success:function(res)
						{
							var str = res.split("|");
							 window.open("<?php echo base_url() ?>reports/payroll/"+str[0]+"/"+str[1]+"/"+str[2]+"/report/PAYROLL_ATM_REPORT_"+str[0]+"_"+str[4]+"_"+str[2]+"_u"+str[3]+".xlsx",'_blank');

						}
                });  
		});
		});
</script>

</html>