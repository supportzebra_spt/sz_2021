<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    hr{
      padding:0 !important;
      margin:5px 0 !important;
    }
  </style>
<!--   <style type="text/css">
    .horizontal-scroll-except-first-column {
      /*      width: 100%;*/
      overflow: auto;
      margin-left: 20em;
    }

    .horizontal-scroll-except-first-column > table > * > tr > th:first-child,
    .horizontal-scroll-except-first-column > table > * > tr > td:first-child {
      position: absolute;
      width: 20em;
      margin-left: -20em;
    }

    .horizontal-scroll-except-first-column > table > * > tr > th,
    .horizontal-scroll-except-first-column > table > * > tr > td {
    /* Without this, if a cell wraps onto two lines, the first column
    * will look bad, and may need padding. */
    white-space: nowrap;
  }
</style> -->

<style type="text/css">
  .myscrolling table {
    table-layout: inherit;
    *margin-left: -100px;/*ie7*/
  }
  .myscrolling td, .myscrolling th {
    vertical-align: top;
    padding: 10px;
    min-width: 100px;
    white-space: nowrap;
    border-left:1px solid black;
    border-right:1px solid black;
  }
  .myscrolling  th {
    position: absolute;
    *position: relative; /*ie7*/
    left: 0;
    width: 250px; 
    white-space: nowrap;
    text-overflow: ellipsis;    
    overflow: hidden;
  }
  .myouter {
    position: relative
  }
  .myinner {
    overflow-x: auto;
    overflow-y: visible;
    margin-left: 250px;
  }
</style>
<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript">
  $(window).load(function(){
   setTimeout(function() {
    $('#loading').fadeOut( 400, "linear" );
  }, 300);
 });

</script><!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/chosen/chosen.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
 $(function(){
  "use strict";
  var m = moment().format("MM/DD/YYYY");
  var end = moment().add(1,'year').format("MM/DD/YYYY");
  $("#daterangepicker-example").daterangepicker({ minDate: m,startDate: m,maxDate : end});
});
</script>
<!-- Multi select -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
<script type="text/javascript">
  /* Multiselect inputs */

  $(function() { "use strict";
    $("#employeelist").multiSelect();
    $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
  });
</script>
</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div class="container">

          <div id="page-title">
            <h3>Schedule System</h3>
            <hr><br>
            <div class="row">
              <div class='btn-group col-md-3'>
                <a href="<?php echo base_url(); ?>index.php/Schedule" class="btn btn-sm btn-warning"><span class='icon-typicons-left'> Back</span></a>

                <button class="btn btn-sm btn-primary" id="upload">
                  <span class='icon-typicons-plus'> File</span>
                </button>
              </div>
              <div class="col-md-3" id="uploadFileDiv" style="display: none;">
                <div>
                  <input type="file" class="form-control  sweet-3" id="uploadFile">
                </div>
              </div> 
              <div class="btn-group col-md-3">
                <button class="btn btn-sm btn-primary" id="upload2" style="display: none;"><span class='icon-typicons-upload'> Upload</span></button>

                <button class="btn btn-sm btn-primary" id="save" style="display: none;"><span class='icon-typicons-check'> Save</span></button>


              </div>
              <button class="btn btn-sm btn-primary" id="view" style="display: none;" hidden>View</button>
            </div><br>

            <div class="content-box" id="preview" style="display:none;">
              <h3 class="content-box-header bg-blue-alt"> 
                <i class="glyph-icon icon-list"></i>
                PREVIEW UPLOADED SCHEDULE
              </h3>
              <div class="content-box-wrapper">
               <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="myscrolling myouter">
                    <div class="myinner">
                      <table class="table table-striped table-hover table-condensed" id="tbody">

                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="example-box-wrapper">
            <div class="row">
              <div class="col-md-12">
                <div class="content-box">
                  <h3 class="content-box-header bg-primary">
                    Generate Schedule Excel File
                  </h3>
                  <div class="content-box-wrapper">

                   <div class="row">
                    <div class="col-md-4">
                     <div class="row">
                      <div class="col-md-12">
                       <div class="form-group" id='acc_descriptiondiv'>
                        <label class='control-label'>Account Type</label>
                        <select class='form-control' id='acc_description'>
                          <option value='' id='removeme'>--</option>
                          <option value='All'>All</option>
                          <option value='Admin'>Admin</option>
                          <option value='Agent'>Agent</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">

                      <div class="form-group" id='acc_namediv'>
                        <label class='control-label'>Account Name</label>
                        <select class='form-control' id='acc_name'></select>
                      </div>
                    </div>


                    <div class="col-md-12">
                      <label class='control-label'>Date Range</label>
                      <div class="input-prepend input-group">
                        <span class="add-on input-group-addon">
                          <i class="glyph-icon icon-calendar"></i>
                        </span>
                        <input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control">
                      </div>
                    </div>                    
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="col-md-12"  id='employeelistdiv'>
                    <div class="row">
                      <div class="col-md-10">
                        <label class='control-label'>Employee List</label>
                        <select multiple name="" id="employeelist"  class="multi-select"></select>
                      </div>

                      <div class="col-md-2"><br>
                        <div class="btn-group-vertical">
                          <button id="select" class='btn btn-primary btn-xs'>Select all</button>
                          <button id="deselect" class='btn btn-primary btn-xs'>Deselect all</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="button-pane text-right">
              <button class="btn btn-primary" id='generateexcel'>Generate Excel</button>

            </div>
          </div>

        </div>
        <div class="col-md-12" id="errorlist" style="display:none">
         <div class="content-box">
          <h3 class="content-box-header bg-blue-alt">
            List of Employees With existing schedules
          </h3>
          <div class="content-box-wrapper">
           <div id="errorlisthtml"></div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>



</div>
</div>
</div>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>

</div>
</body>
<script>
  $(function(){
/*     $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       }); */   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $(function(){

   $("#employeelist").prop('disabled',true);
   $("#employeelist").multiSelect('refresh');
   $("#select").prop('disabled',true);
   $("#deselect").prop('disabled',true);
   $("#acc_name").prop('disabled',true);
   $("#acc_description").on('change',function(){
    var desc = $(this).val();
    $("#removeme").remove();
    if(desc=='All'){

     $("#employeelist").prop('disabled',true);
     $("#employeelist").multiSelect('refresh');
     $("#select").prop('disabled',true);
     $("#deselect").prop('disabled',true);
     $("#acc_name").prop('disabled',true);
   }else{

     $("#acc_name").prop('disabled',false);
     $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/getAccounts",
      data: {
        desc: desc
      },
      cache: false,
      success: function(res)
      {
        if(res!='Empty'){
          $("#acc_name").html("<option value='All'>All</option>");
          $.each(JSON.parse(res),function (i,elem){
              // alert(elem.rate);
              $("#acc_name").append("<option value='"+elem.acc_id+"'>"+elem.acc_name+"</option>");
            });
        }
      }
    });
   }

   
 });
 })
</script>
<script type="text/javascript">
  $(function(){
    $("#acc_name").on('change',function(){
      var acc_id = $(this).val();
      if(acc_id=='All'){
       $("#employeelist").prop('disabled',true);
       $("#employeelist").multiSelect('refresh');
       $("#select").prop('disabled',true);
       $("#deselect").prop('disabled',true);
     }else{
      $("#employeelist").prop('disabled',false);
      $("#employeelist").multiSelect('refresh');
      $("#select").prop('disabled',false);
      $("#deselect").prop('disabled',false);
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/getAccountEmployee",
        data: {
          acc_id: acc_id
        },
        cache: false,
        success: function(res)
        {
          if(res!='Empty'){
            $("#employeelist").html("");
            $.each(JSON.parse(res),function (i,elem){

              $('#employeelist').multiSelect('addOption', { value: elem.emp_id, text: elem.lname+", "+elem.fname, index: 0 }); 
            });
            $("#employeelist").multiSelect('refresh');

          }else{

            $("#employeelist").html("");
            $("#employeelist").multiSelect('refresh');
          }
        }
      });
    }

    
  });
  })
</script>
<script type="text/javascript">
  $(function(){
    $("#acc_description").on('change',function(){

      $("#employeelist").html("");
      
      $("#employeelist").multiSelect('refresh');

      
    })
  })
</script>
<script type="text/javascript">
  $(function(){

    $('#daterangepicker-example').on('apply.daterangepicker', function(ev, picker) {

    });
    $("#employeelist").on('change',function(){

    })
  })
</script>
<script type="text/javascript">
  $(function(){
    $('#select').click(function(){
      $('#employeelist option').each(function(){

        $("#employeelist").multiSelect('select_all');
      });
      $("#employeelist").multiSelect('refresh');
      // alert('nicca');

      
    });
  })
</script>
<script type="text/javascript">
  $(function(){

    $('#deselect').click(function(){
      $('#employeelist option').each(function(){

        $("#employeelist").multiSelect('deselect_all');
      });
      $("#employeelist").multiSelect('refresh');

      
    });
  })
</script>
<script type="text/javascript">
  $("#generateexcel").click(function(){
    var date = $("#daterangepicker-example").val();
    var dater = date.split(" - ");
    var fromDateRaw = dater[0];
    var fromDateRaw2 = fromDateRaw.split("/");
    var fromDate = fromDateRaw2[2]+"-"+fromDateRaw2[0]+"-"+fromDateRaw2[1];
    var toDateRaw = dater[1];
    var toDateRaw2 = toDateRaw.split("/");
    var toDate = toDateRaw2[2]+"-"+toDateRaw2[0]+"-"+toDateRaw2[1];
    var acc_description = $("#acc_description").val();
    var acc_name = $("#acc_name").val();
    var employeelist = $("#employeelist").val();
    // alert(fromDate+" "+toDate);
    if(date==''){

      swal("Oops!", "Select a date range first!", "warning");
    }else{
      if(acc_description==''){
       swal("Oops!", "Please choose account type!", "warning");
     }else if(acc_description=='All'){
      window.location.href ="<?php echo base_url();?>index.php/Schedule/downloadSched/"+fromDate+"/"+toDate;

    }else{
      if(acc_name=='All'){
        window.location.href ="<?php echo base_url();?>index.php/Schedule/downloadSched/"+fromDate+"/"+toDate+"/"+acc_description;

      }else{
        if(employeelist==null){

          swal("Oops!", "Select employee/s first!", "warning");
        }else{

          var emplist = employeelist.toString().split(",");
          employeelist = emplist.join('-');
          window.location.href ="<?php echo base_url();?>index.php/Schedule/downloadSched/"+fromDate+"/"+toDate+"/0/"+employeelist;
        }

      }
    }
  }

})
</script>
<script type="text/javascript">
  $("#checkinputs").click(function(){
    var date = $("#daterangepicker-example").val();
    
    // alert(fromDate+" "+toDate);
    if(date==''){
      swal("Oops!", "Select a date range first!", "warning");
    }else{
      var dater = date.split(" - ");
      var fromDateRaw = dater[0];
      var fromDateRaw2 = fromDateRaw.split("/");
      var fromDate = fromDateRaw2[2]+"-"+fromDateRaw2[0]+"-"+fromDateRaw2[1];
      var toDateRaw = dater[1];
      var toDateRaw2 = toDateRaw.split("/");
      var toDate = toDateRaw2[2]+"-"+toDateRaw2[0]+"-"+toDateRaw2[1];
      var acc_description = $("#acc_description").val();
      var acc_name = $("#acc_name").val();
      var employeelist = $("#employeelist").val();
      if(acc_description==''){

        swal("Oops!", "Please choose account type!", "warning");
      }else if(acc_description=='All'){
        $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>index.php/Schedule/checkAll",
          data: {
            fromDate:fromDate,
            toDate:toDate
          },
          cache: false,
          success: function(res)
          {
            if(res=='Okay'){
              $("#generateexcel").show();
              $("#errorlist").hide();
            }else{

             var result = JSON.parse(res);
             $("#errorlisthtml").html("");

             $("#errorlisthtml").append("<div class='row'");
             $("#errorlisthtml").append("<h5 class='text-left col-md-1'><b>ID NO.</b></h5>");
             $("#errorlisthtml").append("<h5 class='text-left col-md-3'><b>EMPLOYEE NAME</b></h5>");
             $("#errorlisthtml").append("<h5 class='text-left col-md-8'><b>SCHEDULES</b></h5>");
             $("#errorlisthtml").append("</div><br><br>");
             $.each(result, function(i, item) {
              $("#errorlisthtml").append("<div class='row'>");
              $("#errorlisthtml").append("<p class='text-left col-md-1'>"+item.id_num+"</p>");
              $("#errorlisthtml").append("<h5 class='text-left col-md-3'>"+item.lname+", "+item.fname+"</h5>");
              $("#errorlisthtml").append("<p class='text-left col-md-8'>"+item.sched_date+"</p>");
              $("#errorlisthtml").append("</div>");
              $("#errorlisthtml").append("<br><hr>");
            })

             $("#errorlist").show();
           }
         }
       });
      }else{
        if(acc_name=='All'){
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Schedule/checkPerType",
            data: {
              acc_description:acc_description,
              fromDate:fromDate,
              toDate:toDate
            },
            cache: false,
            success: function(res)
            {
              if(res=='Okay'){
                $("#generateexcel").show();
                $("#errorlist").hide();
              }else{

               var result = JSON.parse(res);
               $("#errorlisthtml").html("");
               $("#errorlisthtml").append("<div class='row'");
               $("#errorlisthtml").append("<h5 class='text-left col-md-1'><b>ID NO.</b></h5>");
               $("#errorlisthtml").append("<h5 class='text-left col-md-3'><b>EMPLOYEE NAME</b></h5>");
               $("#errorlisthtml").append("<h5 class='text-left col-md-8'><b>SCHEDULES</b></h5>");
               $("#errorlisthtml").append("</div><br><br>");
               $.each(result, function(i, item) {
                $("#errorlisthtml").append("<div class='row'>");
                $("#errorlisthtml").append("<p class='text-left col-md-1'>"+item.id_num+"</p>");
                $("#errorlisthtml").append("<h5 class='text-left col-md-3'>"+item.lname+", "+item.fname+"</h5>");
                $("#errorlisthtml").append("<p class='text-left col-md-8'>"+item.sched_date+"</p>");
                $("#errorlisthtml").append("</div>");
                $("#errorlisthtml").append("<br><hr>");
              })

               $("#errorlist").show();
             }
           }
         });
        }else{
          if(employeelist==''){
            swal("Oops!", "Select employee/s first!", "warning");
          }else{
           var emplist = employeelist.toString().split(",");
           employeelist = emplist.join('-');
           $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Schedule/checkPerAccountEmployee",
            data: {
              employeelist:employeelist,
              fromDate:fromDate,
              toDate:toDate
            },
            cache: false,
            success: function(res)
            {
             if(res=='Okay'){
              $("#generateexcel").show();
              $("#errorlist").hide();
            }else{

             var result = JSON.parse(res);
             $("#errorlisthtml").html("");
             $("#errorlisthtml").append("<div class='row'");
             $("#errorlisthtml").append("<h5 class='text-left col-md-1'><b>ID NO.</b></h5>");
             $("#errorlisthtml").append("<h5 class='text-left col-md-3'><b>EMPLOYEE NAME</b></h5>");
             $("#errorlisthtml").append("<h5 class='text-left col-md-8'><b>SCHEDULES</b></h5>");
             $("#errorlisthtml").append("</div><br><br>");
             $.each(result, function(i, item) {
              $("#errorlisthtml").append("<div class='row'>");
              $("#errorlisthtml").append("<p class='text-left col-md-1'>&nbsp;"+item.id_num+"</p>");
              $("#errorlisthtml").append("<h5 class='text-left col-md-3'>&nbsp;"+item.lname+", "+item.fname+"</h5>");
              $("#errorlisthtml").append("<p class='text-left col-md-8'>&nbsp;"+item.sched_date+"</p>");
              $("#errorlisthtml").append("</div>");
              $("#errorlisthtml").append("<br><hr>");
            })

             $("#errorlist").show();
           }
         }
       });
         }

       }
     }
   }

 });
</script>
<script type="text/javascript">
 $("#upload").click(function(){
  $('#uploadFileDiv').toggle();
});
 $('#uploadFile').click(function(){
  $("#preview").hide();
})
 $('#uploadFile').on('change', function(){
  if($('#uploadFile')[0].files[0].name == 'ScheduleTemplate.xlsx'){
    $("#upload2").show();
  }else{
    swal("Oops!", "Please use the template given!", "error");
  }
});

 $("#upload2").click(function(event){

  var data = new FormData();
  $.each($("#uploadFile")[0].files,function(i,file){
    data.append("uploadFile",file);
  });                      

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/Schedule/uploadsched",
    data: data,
    cache: false,
    processData:false,
    contentType:false,
    success: function(response)
    {
      response = $.trim(response);
      if(response=='Error'){
        swal("Error", "Problem Uploading Excel", "error");
      }else{
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Schedule/viewuploadedschedules",
          cache: false,
          success: function(res)
          {

            swal("Note!", "Please double check schedules before saving.", "warning");
            $('#tbody').empty();
            // $("#viewtable").show();
            var data = $.parseJSON(res);
            var body = "";
            for(var i=0;i<data.length;i++){
              body += "<tr>";
              if(i==0){
                for(var j=0;j<data[i].length;j++){
                  if(j==0){
                    body += "<th style='background: #eee;color:black'>"+data[i][j]+"</th>";
                  }else{
                    if(data[i][j]==null || data[i][j]==''){
                      body += "<td  class='text-left' style='background:#EE3B3B !important;'>&nbsp;</td>";
                    }else{
                      body += "<td  class='text-left' style='background: #eee;color:black'>"+data[i][j]+"</td>";
                    }
                  }

                }
              }else{

               for(var j=0;j<data[i].length;j++){
                 if(j==0){
                  body += "<th>"+data[i][j]+"</th>";
                }else{
                 if(data[i][j]==null || data[i][j]==''){
                  body += "<td  class='text-left' style='background:#CD5555 !important;'>&nbsp;</td>";
                }else{
                  body += "<td  class='text-left'>"+data[i][j]+"</td>";
                }
              }

            }
            body +="</tr>";
          }


            // body +="</tr>";

          }
          $('#tbody').append(body);

        }
      });
      }

      $("#save").show();
    },
    error: function(){
      swal("Error", "An error occurred while uploading. Please Try Again.", "error");
    }
  });
  $("#preview").show();
  event.preventDefault();
});
</script>
<script type="text/javascript">
 $("#save").click(function(){

   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/Schedule/savemultiplesched",
    cache: false,
    success: function(res)
    {
      window.location = "<?php echo base_url(); ?>/index.php/Schedule/multiplesched";

    }
  }); 
 });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>