<!DOCTYPE html> 
<html  lang="en">

<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		.tile-box-alt .tile-header {
    padding: 10px;
    text-align: center;
    background: #575757;
    color: white;
    </style>


    <meta charset="UTF-8">
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>


</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-black font-inverse">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 
<div id="page-title">
		
 <a href="<?php echo base_url();?>"> Home</a> >
 <a href="<?php echo base_url()."index.php/Payroll/GeneratePayrolls/";?>">Coverage </a> > 
 <a href="<?php echo base_url()."index.php/Payroll/payroll_index/".$period["coverage_id"];?>">Payroll </a>
 
 
 <br>
 <br>
	<div class="form-group">
              
<div class="panel">
	  <?php
	  $date = explode("-",$period["daterange"]);
	  echo "<h3 style='text-align:center;'> <b>Payroll Period: </b> ".date_format(date_create($date[0]),"F d, Y")." - ".date_format(date_create($date[1]),"F d, Y")."</h3><br>";
	  ?>
    <div class="panel-body">
        <h3 class="title-hero">
            Call Center Amabssadors
        </h3>

        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-md-2">
                    <a href="<?php echo base_url()."index.php/Payroll/GeneratePayroll/agent-trainee/".$period["coverage_id"]; ?>" title="Example tile shortcut" class="tile-box tile-box-alt">
                        <div class="tile-header">
                            Trainee  
                        </div>
                        <div class="tile-content-wrapper">
						<img src="<?php echo base_url()."assets/images/sz-ambs-trainee.png"?>">

                         </div>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="<?php echo base_url()."index.php/Payroll/GeneratePayroll/agent-probationary/".$period["coverage_id"]; ?>"  title="Example tile shortcut" class="tile-box tile-box-alt  ">
                        <div class="tile-header">
                            Probationary 
                        </div>
                        <div class="tile-content-wrapper">
						<img src="<?php echo base_url()."assets/images/probationary-cca.png"?>" style="border-radius: 85px;">

                         </div>
                    </a>
                </div>
				<div class="col-md-2">
                    <a href="<?php echo base_url()."index.php/Payroll/GeneratePayroll/agent-regular/".$period["coverage_id"]; ?>" title="Example tile shortcut" class="tile-box tile-box-alt  ">
                        <div class="tile-header">
                             Regular
                        </div>
                        <div class="tile-content-wrapper">
						<img src="<?php echo base_url()."assets/images/reg-cca.png"?>">

                         </div>
                    </a>
                </div>
				
                 
                
            </div>
        </div>
    </div>
</div>
<div class="panel">
	
    <div class="panel-body">
        <h3 class="title-hero">
            Cebu Call Center Amabssadors 
        </h3>

        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-md-2">
                    <a href="<?php echo base_url()."index.php/Payroll/GeneratePayroll/agent-probationary/".$period["coverage_id"]."/cebu"; ?>" title="Example tile shortcut" class="tile-box tile-box-alt  ">
                        <div class="tile-header" style="font-size: smaller;">
                             Cebu Probationary
                        </div>
                        <div class="tile-content-wrapper">
						<img src="<?php echo base_url()."assets/images/probationary-cca.png"?>">

                         </div>
                    </a>
                </div>
				<div class="col-md-2">
                    <a href="<?php echo base_url()."index.php/Payroll/GeneratePayroll/agent-regular/".$period["coverage_id"]."/cebu"; ?>" title="Example tile shortcut" class="tile-box tile-box-alt  ">
                        <div class="tile-header" style="font-size: smaller;">
                             Cebu Regular
                        </div>
                        <div class="tile-content-wrapper">
						<img src="<?php echo base_url()."assets/images/reg-cca.png"?>">

                         </div>
                    </a>
                </div>
                 
                
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            SupportZebra Team
        </h3>
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-md-2">
                    <a href="<?php echo base_url()."index.php/Payroll/GeneratePayroll/admin-trainee/".$period["coverage_id"]; ?>" title="Example tile shortcut" class="tile-box tile-box-alt">
                        <div class="tile-header">
                            Trainee  
                        </div>
                        <div class="tile-content-wrapper btn-white">
						<img src="<?php echo base_url()."assets/images/sz-trainee.png"?>">
                         </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?php echo base_url()."index.php/Payroll/GeneratePayroll/admin-both-nonconfi/".$period["coverage_id"]; ?>" title="Example tile shortcut" class="tile-box tile-box-alt  ">
                        <div class="tile-header">
                            Probationary and Regular   
                        </div>
                        <div class="tile-content-wrapper">
						<img src="<?php echo base_url()."assets/images/sz-pr.png"?>">

                         </div>
                    </a>
                </div>
				<?php //if($this->session->userdata('uid')!=142){ ?>
				
                <div class="col-md-2">
 					<a href="#" title="Example tile shortcut" class="tile-box tile-box-alt  " id="confiBtn">
                        <div class="tile-header">
                            Confidential  
                        </div>
                        <div class="tile-content-wrapper">
							<img src="<?php echo base_url()."assets/images/sz-confi.jpg"?>">

                         </div>
                    </a>
                </div>
                 
				<?php// } ?>
            </div>
        </div>
    </div>
</div>
	 
	</div>

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
	    $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
 		cache: false,
		success: function(html)
		{
		 
		  $('#headerLeft').html(html);
		   //alert(html);
		}
  });	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
 		cache: false,
		success: function(html)
		{
		 
		  $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
  $("#confiBtn").click(function(){
	  swal({
  title: 'Code Required',
  html: 'Please enter the code to continue.',
  input: 'password',
  showCancelButton: true,
  confirmButtonText: 'Submit',
  showLoaderOnConfirm: true,
  allowOutsideClick: false
}).then(function(email) {
   
		// if(email=="hrt@2018"){
		if(email=="4ft@hrt_2019"){
					
			location.href="<?php echo base_url()."index.php/Payroll/GeneratePayroll/admin-both-confi/".$period["coverage_id"]; ?>";

		}else{
		 $("#tblShow").html("");
			swal({
			type: 'error',
			title: 'Oh, Snap!'
			}); 
		}
   
 
})
	  });
  });	
</script>
 

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>