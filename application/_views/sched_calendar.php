<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
   /* td,th{
      white-space: nowrap;
    }*/
    .fc-event-title{
      text-align: center !important;vertical-align: middle !important;
    }
  </style>

  
  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });

 </script>
 <!-- Calendar -->

 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/interactions-ui/resizable.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/interactions-ui/draggable.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/interactions-ui/sortable.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/interactions-ui/selectable.js"></script>

 <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/calendar/calendar.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/calendar/calendar.js"></script>
 
 <script type="text/javascript">
  $(document).ready(function() {
    $('#calendar').fullCalendar({
      timeFormat: ' ',
      header: {
        left:'prevYear,nextYear',
        right: 'today prev,next',
        center:'title'
      },
      // header:{left:"prev,next today",center:"title",right:"month,agendaWeek,agendaDay"},
      editable: false,
      dayClick: function(date, jsEvent, view) {

        alert('Clicked on: ' + date.format());
        // $(this).css('background-color', 'red');

      },
      events: {
        url: '<?php echo base_url();?>index.php/Schedule/getCalendarEmpSched',
        type: 'POST',
        data: {
          uid: "<?php echo $uid;?>"
        },
        error: function() {
          alert('there was an error while fetching events!');
        }
      }, 
      // eventRender: function(event, element) { 
      //   element.find('.fc-event-title').append("<br/>" + event.description); 
      // } 
    })
  });
</script>
</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div id="page-title">
          <h3>Schedule Calendar</h3>
          <hr><br>

          <div class="col-md-2"  style="background: #424242;padding-bottom: 8px;">
          <h3 class='text-center'  style="color:white;padding-top: 20px;">LEGEND</h3><br>
            <?php foreach($legends as $l):?>
              <div class="bs-label" style="display:block;background: <?php echo $l->style;?> !important;margin-bottom:5px;padding:9px"><?php echo $l->type;?></div>
            <?php endforeach;?>
          </div>
          <div id='calendar' class="col-md-10"></div>
        </div> 
      </div>
    </div>
  </div>
</div>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>