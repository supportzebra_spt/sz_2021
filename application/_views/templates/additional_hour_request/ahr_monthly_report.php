<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Additional Hour Request Reporting</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Additional Hour Request Reporting</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
            <div>
                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover"
                    aria-expanded="true">
                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                        <i class="la la-plus m--hide"></i>
                        <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__section m-nav__section--first m--hide">
                                            <span class="m-nav__section-text">Quick Actions</span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                <span class="m-nav__link-text">Activity</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                <span class="m-nav__link-text">Messages</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                <span class="m-nav__link-text">FAQ</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                <span class="m-nav__link-text">Support</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__separator m-nav__separator--fit">
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-content ">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="m-portlet m-portlet--head-solid-bg" style="">
                    <div class="m-portlet__head col-md-12" style="padding: 1.2rem 2.2rem !important;background-color: #5e697d;">
                        <div class="row">
                            <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5" style="margin-top: 6px;">
                                <h3 class=" text-white">Monthly Statistics</h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-4" style="margin-top: 6px;">
                                <div class="input-group m-input-group m-input-group--solid">
                                    <div class="input-group-prepend ">
                                        <span class="input-group-text">Year:</span>
                                    </div>
                                    <!-- <input type="text" class="form-control m-input" placeholder="0.00" aria-describedby="basic-addon1"> -->
                                    <select class="custom-select form-control m-input yearSelect" id="yearSelect" name="yearSelect" placeholder="Select a Year"
                                        style="height: calc(2.53rem + 2px) !important;"></select>
                                </div>
                            </div>
                            <div class="col-md-4" style="margin-top: 6px;">
                                <div class="input-group m-input-group m-input-group--solid">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Number Of:</span>
                                    </div>
                                    <!-- <input type="text" class="form-control m-input" placeholder="0.00" aria-describedby="basic-addon1"> -->
                                    <select class="custom-select form-control m-input quantityType" id="quantityType1" name="quantityType1" placeholder="Select a Year"
                                        style="height: calc(2.53rem + 2px) !important;">
                                        <option value="1">Request</option>
                                        <option value="2">Hours Rendered</option>
                                    </select>
                                </div>
                            </div>
                            <div class=" col-md-4" style="margin-top: 6px;">
                                <div class="input-group m-input-group m-input-group--solid">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Employee Type:</span>
                                    </div>
                                    <!-- <input type="text" class="form-control m-input" placeholder="0.00" aria-describedby="basic-addon1"> -->
                                    <select class="custom-select form-control m-input empType" id="empType1" name="empType1" placeholder="Select Employee Type"
                                        style="height: calc(2.53rem + 2px) !important;">
                                        <option value="Admin">Admin</option>
                                        <option value="Agent">Ambassador</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="generalChart" class="generalChart bg-black col-md-12" data-style="dark" data-theme="bg-white"></div>
                        <div id="noMonthlyData" class="no-data col-md-12" style="text-align: center;display: inline-block;">
                            <div class="m-demo-icon__preview">
                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                            </div>
                            <span class="m-demo-icon__class">No Result Found</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="m-portlet m-portlet--head-solid-bg" style="">
                    <div class="m-portlet__head col-md-12" style="padding: 1.2rem 2.2rem !important;background-color: #5e697d;">
                        <div class="row">
                            <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5" style="margin-top: 6px;">
                                <h4 class=" text-white">Detailed Count</h4>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div id="generalChart" class="generalChart bg-black col-md-12" data-style="dark" data-theme="bg-white"></div>
                        <table class="table m-table m-table--head-no-border">
                            <thead>
                                <tr>
                                    <th>Month</th>
                                    <th>Pre Shift</th>
                                    <th>After Shift</th>
                                    <th>WORD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">January</th>
                                    <td>0</td>
                                    <td>1</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <th scope="row">February</th>
                                    <td>4</td>
                                    <td>0</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <th scope="row">March</th>
                                    <td>3</td>
                                    <td>7</td>
                                    <td>10</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="recordInfoAhrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="approvalAhrForm">
                <div class="modal-header">
                    <span class="m-portlet__head-icon">
                        <i class="la la-clock-o"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Additional Hour Request Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="<?php echo base_url()?>assets/images/img/sz.png" width="75" alt="" class="img-circle">
                                </div>
                                <div class="col-md-9">
                                    <div class="user-details" style="padding-top: 10px;padding-left: 9px;padding-bottom: 10px;">
                                        <span style="font-size: 19px;" id="empNameRecord">Michael Sanchez</span>
                                        </br>
                                        <span style="font-size: 12px" id="empAccountRecord">Software Programming Team</span>
                                        </br>
                                        <span style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="empJobRecord">Junior Software Developer 1</span>
                                    </div>
                                </div>
                            </div>
                            <hr style="padding-top:1px;" class="bg-primary">
                            <div class="col-md-12 mb-3">
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-md-5">AHR Number:</label>
                                    <div class="col-md-7 text-capitalize" id="ahrNoRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-md-5">Date Filed:</label>
                                    <div class="col-md-7 text-capitalize" id="dateFiledRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-md-5">Filing Type:</label>
                                    <div class="col-md-7 text-capitalize" id="filingTypeRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-md-5">Overtime Type:</label>
                                    <div class="col-md-7 .text-capitalize" id="ahrTypeRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-md-5">Date Rendered:</label>
                                    <div class="col-md-7" id="dateRenderedRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-md-5">Shift:</label>
                                    <div class="col-md-7" id="shiftRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-md-5">Shift Type:</label>
                                    <div class="col-md-7" id="shiftTypeRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-md-5">Status:</label>
                                    <div class="col-md-7" id="approvalStatRecord">APPROVED</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 stack-border-left-only">
                            <div class="col-md-12 mb-3">
                                <label>
                                    <b>REASON:</b>
                                </label>
                                <div class="col-md-12 ahr_quote pt-3 pb-1">
                                    <blockquote>
                                        <i class="fa fa-quote-left"></i>
                                        <span class="mb-0 text-capitalize" id="reasonRecord"></span>
                                        <i class="fa fa-quote-right"></i>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>
                                    <b>RENDER DETAILS:</b>
                                </label>
                                <table class="table table-bordered table-hover">
                                    <tbody class=" font-size-12">
                                        <tr>
                                            <th scope="row">START:</th>
                                            <td id="startOtRecord">Jul 27, 2018 12:00 AM</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">END</th>
                                            <td id="endOtRecord">Jul 28, 2018 9:00 AM</td>
                                        </tr>
                                        <tr id="breakRecord">
                                            <th scope="row">BREAK</th>
                                            <td id="breakRecordVal"></td>
                                        </tr>
                                        <tr id="bctRecord">
                                            <th scope="row">BCT</th>
                                            <td id="bctRecordVal"></td>
                                        </tr>
                                        <tr id="tardRecord">
                                            <th scope="row">TARDINESS</th>
                                            <td>
                                                <div class="col-md-12" id="lateRecord">LATE - 1 hr, 30 mins</div>
                                                <div class="col-md-12" id="utRecord">UT - 10 hrs, 20 mins</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <label>
                                    <b>COVERAGE:</b>
                                </label>
                                <div class="form-group">
                                    <h3 id="renderedOtRecord" class="font-size-39">00 hr, 00 min</h3>
                                    <span class="font-size-12 pull-right">overtime</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Close</button>
                </div>
        </div>
        </form>
    </div>
</div>

<div class="modal fade" id="approversInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Approval Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">
                    <div id="approvalBody"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>node_modules/moment/moment.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/plugins/yearSelector/lib/year-select.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/ahr_common.js"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/ahr_reporting.js"></script>