<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Additional Hour Request Settings</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Additional Hour Request Settings</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ahrSettings">
        <div class="m-portlet m-portlet--head-solid-bg">
             <div class="m-portlet__head col-md-12" style="padding: 1.2rem 2.2rem !important;background-color: #5e697d;">
                <div class="row">
                    <div class="col-9 col-sm-8 col-md-6 col-lg-6 col-xl-6" style="margin-top: 6px;">
                        <h5 class=" text-white">
                            <i class="la la-calendar-o text-light mr-3" style="font-size: 26px;"></i>Days Available to File (Rendered AHR only)</h5>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body input-group">
                <span class="col-md-10">An Employee is required to file an additional hour request <strong id="daysAvailableText" class="font-size-20"></strong> days after the overtime was rendered.</span>
                <input id="daysAvailable" type="text" class="form-control col-md-1" value="1" name="daysAvailable" placeholder="Select time" type="text">
            </div>
        </div>
        <div class="m-portlet m-portlet--head-solid-bg">
            <div class="m-portlet__head col-md-12" style="padding: 1.2rem 2.2rem !important;background-color: #5e697d;">
                <div class="row">
                    <div class="col-9 col-sm-8 col-md-6 col-lg-6 col-xl-6" style="margin-top: 6px;">
                        <h5 class=" text-white">
                            <i class="la la-thumb-tack text-light mr-3" style="font-size: 26px;"></i>Assign Rendered AHR Filing Feature</h5>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body pt-1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-12 text-center font-weight-bold p-3" style="font-size: 20px;">
                            <i class="fa fa-group text-primary mr-3" style="font-size: 26px;"></i>SZ TEAM
                        </div>
                        <div class="col-md-12 mb-2">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search SZ Team" id="assignRenderedAhrSearchAdmin">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-search"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="assignRenderedAhrTableAdmin"></div>
                        </div>
                    </div>                    
                    <div class="col-md-6">
                        <div class="col-md-12 text-center font-weight-bold p-3" style="font-size: 20px;">
                            <i class="fa fa-headphones text-primary mr-3" style="font-size: 26px;"></i>AMBASSADORS
                        </div>
                        <div class="col-md-12 mb-2">
                            <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search Ambassador Team" id="assignRenderedAhrSearchAgent">
                                <div class=" input-group-append ">
                                    <span class="input-group-text ">
                                        <i class="fa fa-search "></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="assignRenderedAhrTableAgent"></div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- </div> -->

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/ahr_settings.js"></script>



