<style>
    * {
        font-weight: 400;
    }

    @media (min-width: 476px) {
        .col-sm-6 {
            flex: 0 0 50%;
            max-width: 50%;
        }
    }

    @media (max-width: 477px) {
        .col-sm-6 {
            text-align: center;
            text-align: -webkit-center;
        }
        .pull-left,
        .pull-right {
            float: unset;
        }
        .row-header-avg {
            margin-bottom: 20px;
        }
    }

    .row-header-avg {
        /* position: absolute; */
        /* z-index: 99999; */
        margin-left: 0;
        width: 90%;
    }

    .highcharts-drilldown-axis-label {
        font-family: Poppins;
    }

    .m-datatable__pager {
        margin-right: 2rem;
        margin-left: 2rem;
    }

    #survey-scrollable {
        max-height: 450px;
    }

    .m-link {
        color: black;
    }

    #sa_header {
        color: #fff;
    }

    .fa-quote-left,
    .fa-quote-right {
        color: #929292;
        font-size: 13px;
        margin: 0 5px;
    }
    .daterangepicker {
        z-index: 99999 !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'Daily Survey', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive'], ['title' => 'Daily Survey', 'link' => 'szfive/daily_survey#']]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content" id="daily-survey-page">
        <?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list); ?>
        <div class="m-portlet" id="survey_main_filter" style="margin-bottom: 15px;box-shadow: 0 1px 8px rgba(0,0,0,0.15);border-radius: 3px;border: 1px solid #ffffff;">
            <div class="m-portlet__body" style="padding: 10px;">
                <div class="row">
                    <div class="col-lg-6" style="padding-right: 0; ">
                        <div class="row" style="margin-right: 0; ">
                            <div class="col-lg-2" style=" padding-right: 0; max-width: fit-content;margin-left: 18px;">
                                <a href="<?php echo base_url('szfive/feeling'); ?>" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--pill">
                                    <i class="la la-home"></i>
                                </a>
                            </div>
                            <div class="col-lg-10">
                                <div class="m-select2 m-select2--pill">
                                    <select class="form-control m-select2" id="select_survey" name="question">
                                        <?php foreach ($surveys as $key => $survey): ?>
                                        <option value="<?php echo $survey->surveyId; ?>" <?php if ($survey->isDefault == 1) {echo 'selected';} ;?>>
                                            <?php echo $survey->question; ?>
                                        </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6" style=" padding-left: 0; ">
                        <div class="row">
                            <div class="col-lg-9">
                                <div style=" width: 100%; ">
                                    <span class="m-subheader__daterange" id="survey_datepicker">
                                        <span class="m-subheader__daterange-label">
                                            <span class="daterange-title m-subheader__daterange-title-reviewers">This Week:</span>
                                            <span class="daterange-date m-subheader__daterange-date-reviewers m--font-brand">
                                                <?php echo $this_week; ?>
                                            </span>
                                        </span>
                                        <input type="hidden" name="start_date" value="<?php echo date('Y-m-d'); ?>" />
                                        <input type="hidden" name="end_date" value="<?php echo date('Y-m-d'); ?>" />
                                        <button class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" style="width: 20px;height: 20px;">
                                            <i class="la la-angle-down" style="color: white;"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div style=" margin-right: 10px; ">
                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" id="dropdown_report1" data-dropdown-toggle="click"
                                    aria-expanded="false">
                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-secondary m-btn m-btn--icon m-btn--pill">
                                        <i class="flaticon-download"></i>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 24.0909px;"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__section m-nav__section--first">
                                                            <span class="m-nav__section-text">Download</span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link" id="getReport">
                                                                <i class="m-nav__link-icon flaticon-file-1"></i>
                                                                <span class="m-nav__link-text">Detailed Report</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if ($session['role_id'] == 1): ?>
                            <div>
                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover"
                                    aria-expanded="true">
                                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                        <i class="flaticon-settings-1 m--font-brand"></i>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__section m-nav__section--first">
                                                            <span class="m-nav__section-text">Quick Actions</span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link" id="openSurveyModal">
                                                                <i class="m-nav__link-icon flaticon-graphic-2"></i>
                                                                <span class="m-nav__link-text">Survey List</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link" id="openQuestionModal">
                                                                <i class="m-nav__link-icon flaticon-list-3"></i>
                                                                <span class="m-nav__link-text">Question List</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link" id="openChoiceModal">
                                                                <i class="m-nav__link-icon flaticon-list-1"></i>
                                                                <span class="m-nav__link-text">Choices List</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="<?php echo base_url('szfive/daily_survey/settings'); ?>" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-settings-1"></i>
                                                                <span class="m-nav__link-text">Settings</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include '_modals.php';?>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body" id="main_chart_container" style="position: sticky;">
                <div class="row row-header-avg">
                    <div class="col-lg-6">
                        <div class="pull-left">
                            <h6 id="date-heading">
                                <!-- <?php echo $this_week; ?> -->
                            </h6>
                            <p id="total-answers"></p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="float-right" style="display: flex;">
                            <span id="survey-avg" style="border: 2px solid #716aca;border-width: 2px;padding: 7px 20px;border-radius: 2.3rem;font-weight: 500;padding-top: 3px;">AVERAGE: &nbsp;
                                <strong style="font-weight: 600;font-size: 20px;">0</strong>
                            </span>
                        </div>
                    </div>
                </div>
                <div id="survey_mainchart">
                    <div class="highcharts-container" style="margin-top: 20px;">
                        <div id="survey_chart_container"></div>
                    </div>
                    <div style="display: -webkit-box;text-align: -webkit-center;margin-top: -12px;text-align: -moz-center;">
                        <div class="row">
                            <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                                <div class="m-stack__item m-stack__item--center">
                                    <div id="customLegend" style="display: block;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="report-chart">
                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--mobile" style="background: #faf9fc;">
            <div class="row" style="padding: 20px;margin-left: 0;margin-right: 0;border-bottom: 1px solid #eee;background: white;">
                <div class="col-lg-4">
                    <div class="m-select2 m-select2--pill">
                        <input type="hidden" value="0" name="accountName" />
                        <select class="form-control m-select2" id="m_form_type_survey_ans">
                            <option val="all" selected]>All</span>
                            </option>
                            <?php foreach ($accounts as $account): ?>
                            <option value="<?php echo $account->acc_id; ?>">
                                <?php echo $account->acc_name; ?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4" style=" padding-left: 0; ">
                    <div class="m-select2 m-select2--pill">
                        <input type="hidden" value="0" name="employeeStatus" />
                        <select class="form-control m-select2" id="m_form_status_survey_ans">
                            <option val="all" selected]>All</option>
                            <?php foreach ($emp_statuses as $emp): ?>
                            <option value='<?php echo $emp->status; ?>'>
                                <?php echo $emp->status; ?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4" style=" padding-left: 0; ">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="m-form m-form--label-align-right">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input m-input--pill" placeholder="Type any keyword..." id="answerSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--right">
                                        <span>
                                            <i class="la la-search m--font-secondary"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div style=" margin-right: 10px; ">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" id="dropdown_report2" data-dropdown-toggle="click"
                                aria-expanded="true">
                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-secondary m-btn m-btn--icon m-btn--pill">
                                    <i class="flaticon-download"></i>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 24.0909px;"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__section m-nav__section--first">
                                                        <span class="m-nav__section-text">Download</span>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="#" class="m-nav__link" id="getReport_datatable">
                                                            <i class="m-nav__link-icon flaticon-file-1"></i>
                                                            <span class="m-nav__link-text">Detailed Report</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body" style="padding: 2rem 0;">
                <div id="survey_datatable_answers" class="m_datatable_answers"></div>
            </div>
        </div>
    </div>
</div>
<!--end: Daily Survey -->
<script src="<?php echo base_url('assets/src/custom/js/szfive.survey.js'); ?>" type="text/javascript"></script>