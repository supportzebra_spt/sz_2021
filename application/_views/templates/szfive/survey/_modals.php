<!-- Survey Modal-->
<div class="modal fade" id="survey_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">LIST OF SURVEYS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-8">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input sz-search-box" placeholder="Search..." id="surveySearch">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span>
                                                        <i class="la la-search"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if (strpos($permissions->settings, 'addSurveys') !== false): ?>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <button class="btn btn-sm btn-brand m-btn m-btn--custom m-btn--icon m-btn--air" type="button" data-toggle="modal" id="addSurvey">
                                        <span>
                                            <i class="fa fa-plus" style="font-size: 12px;"></i>
                                            <span style="font-size: 13px;">CREATE A SURVEY</span>
                                        </span>
                                    </button>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="m_datatable_survey"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Show Survey Modal -->
<div class="modal fade" id="add_survey_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">CREATE A SURVEY</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body survey">
                <form class="m-form m-form--fit m-form--label-align-right survey-form" id="add_survey_form">
                    <div class="m-portlet__body survey-form">
                        <div class="m-form__content">
                            <div class="m-alert m-alert--icon alert alert-warning m--hide" role="alert" id="m_form_2_msg">
                                <div class="m-alert__icon">
                                    <i class="la la-warning"></i>
                                </div>
                                <div class="m-alert__text">
                                    Oh snap! Change a few things up and try submitting again.
                                </div>
                                <div class="m-alert__close">
                                    <button type="button" class="close" data-close="alert" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Question</label>
                            <select style="width: 100%" class="form-control m-select2" id="select_question2" name="questionId">
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">No. of Choices</label>
                            <input class="form-control" type="number" id="add_numberOfChoices" name="add_numberOfChoices" min="1" placeholder="Type number of choices">
                        </div>
                        <div id="m_repeater_3">
                            <div class="form-group  m-form__group row" style="padding-left: 10px;padding-right: 40px;">
                                <label class="col-lg-3 col-form-label">Choices:</label>
                                <div data-repeater-list="" class="col-lg-9">
                                    <div data-repeater-item class="row m--margin-bottom-10">
                                        <div class="col-lg-10">
                                            <select style="width: 100%" class="form-control m-select2 choice-select2" placeholder="Choice label" name="choiceLabels">
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                <i class="la la-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col">
                                    <div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon" id="addNewChoice">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Add</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-9 ml-lg-auto">
                                    <button type="submit" class="btn btn-accent">Submit</button>
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- Edit Survey modal -->
<div class="modal fade" id="edit_survey_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">UPDATE SURVEY</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body survey">
                <form class="m-form m-form--fit m-form--label-align-right edit-survey-form" id="edit_survey_form">
                    <div class="m-portlet__body survey-form">
                        <div class="m-form__content">
                            <div class="m-alert m-alert--icon alert alert-warning m--hide" role="alert" id="m_form_2_msg">
                                <div class="m-alert__icon">
                                    <i class="la la-warning"></i>
                                </div>
                                <div class="m-alert__text">
                                    Oh snap! Change a few things up and try submitting again.
                                </div>
                                <div class="m-alert__close">
                                    <button type="button" class="close" data-close="alert" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Question:</label>
                            <input type="hidden" name="edit_surveyId">
                            <select style="width: 100%;" class="form-control m-select2" id="select_question_editsurvey" name="questionId">
                             
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">No. of Choices</label>
                            <input class="form-control" type="number" id="edit_numberOfChoices" name="edit_numberOfChoices" min="1">
                        </div>
                        <div id="m_repeater_3_2">
                            <div class="form-group  m-form__group row" style="padding-left: 10px;padding-right: 40px;">
                                <label class="col-lg-3 col-form-label">Choices:</label>
                                <div data-repeater-list="" class="col-lg-9">
                                    <div data-repeater-item class="row m--margin-bottom-10">
                                        <div class="col-lg-10">
                                            <select style="width: 100%" class="form-control m-select2 choice-select2_3" placeholder="Choice label" name="choiceLabels">
                                                <?php foreach ($choices as $key => $choice): ?>
                                                <option value="<?php echo $choice->choiceId; ?>">
                                                    <?php echo $choice->label; ?>
                                                </option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                <i class="la la-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col">
                                    <div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon" id="editNewChoice">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Add</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-9 ml-lg-auto">
                                    <button type="submit" class="btn btn-brand">Update</button>
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--             
<form class="repeater">
    <div data-repeater-list="group-a">
        <div data-repeater-item>
            <input type="text" name="text-input" value="A" />
            <input data-repeater-delete type="button" value="Delete" />
        </div>
        <div data-repeater-item>
            <input type="text" name="text-input" value="B" />
            <input data-repeater-delete type="button" value="Delete" />
        </div>
    </div>
    <input data-repeater-create type="button" value="Add" />
</form> -->
            </div>

        </div>
    </div>
</div>
<!-- Questions Modal-->
<div class="modal fade" id="survey_question_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">LIST OF QUESTIONS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-8">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input sz-search-box" placeholder="Search..." id="questionSearch">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span>
                                                        <i class="la la-search"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if (strpos($permissions->settings, 'addQuestions') !== false): ?>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <button class="btn btn-sm btn-brand m-btn m-btn--custom m-btn--icon m-btn--air" type="button" data-toggle="modal" id="addQuestion">
                                        <span>
                                            <i class="fa fa-plus" style="font-size: 12px;"></i>
                                            <span style="font-size: 13px;">CREATE A QUESTION</span>
                                        </span>
                                    </button>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="m_datatable_question" id="survey_question_selection"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Question Modal-->
<div class="modal fade" id="add_survey_question_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">CREATE A QUESTION</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="add_survey_question_form">
                    <div class="m-portlet__body">
                        <div class="m-form__content">
                            <div class="m-alert m-alert--icon alert alert-warning m--hide" role="alert" id="m_form_2_msg">
                                <div class="m-alert__icon">
                                    <i class="la la-warning"></i>
                                </div>
                                <div class="m-alert__text">
                                    Oh snap! Change a few things up and try submitting again.
                                </div>
                                <div class="m-alert__close">
                                    <button type="button" class="close" data-close="alert" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-4 col-sm-12">Survey Question *</label>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <textarea class="form-control m-input" id="question" name="question" placeholder="" value=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-9 ml-lg-auto">
                                    <button type="submit" class="btn btn-danger">Submit</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- View Survey -->
<div class="modal fade" id="view_question_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">QUESTION</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <span class="m-section__sub">Question: </span>
                <p style="padding: 20px; margin: 10px 0px 30px; border: 4px solid rgb(239, 239, 239);" id="blockui_question"></p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="view_survey_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="view_survey_title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <span class="m-section__sub">Question: </span>
                <p style="padding: 20px; margin: 10px 0px 30px; border: 4px solid rgb(239, 239, 239);" id="blockui_survey_question"></p>
                <span class="m-section__sub">Choices: </span>
                <p style="padding: 20px; margin: 10px 0px 30px; border: 4px solid rgb(239, 239, 239);" id="blockui_survey_choices"></p>
            </div>

        </div>
    </div>
</div>
<!-- Edit Question -->
<div class="modal fade" id="edit_question_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">UPDATE QUESTION</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="edit_question_form">
                    <div class="m-portlet__body">
                        <div class="m-form__content">
                            <div class="m-alert m-alert--icon alert alert-warning m--hide" role="alert" id="m_form_2_msg">
                                <div class="m-alert__icon">
                                    <i class="la la-warning"></i>
                                </div>
                                <div class="m-alert__text">
                                    Oh snap! Change a few things up and try submitting again.
                                </div>
                                <div class="m-alert__close">
                                    <button type="button" class="close" data-close="alert" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-4 col-sm-12">Question *</label>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <input type="hidden" name="questionId" value="" />
                                <textarea class="form-control m-input" id="question" name="question" placeholder="" value=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-9 ml-lg-auto">
                                    <button type="submit" class="btn btn-brand">Update</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- CHOICE -->
<!-- Choices Modal-->
<div class="modal fade" id="survey_choice_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">LIST OF CHOICES</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-8">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input sz-search-box" placeholder="Search..." id="choiceSearch">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span>
                                                        <i class="la la-search"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if (strpos($permissions->settings, 'addChoices') !== false): ?>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <button class="btn btn-sm btn-brand m-btn m-btn--custom m-btn--icon m-btn--air" type="button" data-toggle="modal" id="addChoice">
                                        <span>
                                            <i class="fa fa-plus" style="font-size: 12px;"></i>
                                            <span style="font-size: 13px;">CREATE A CHOICE</span>
                                        </span>
                                    </button>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="m_datatable_choice" id="survey_choice_selection"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Edit Choice -->
<div class="modal fade" id="edit_choice_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">UPDATE CHOICE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="edit_choice_form">
                    <div class="m-portlet__body">
                        <div class="m-form__content">
                            <div class="m-alert m-alert--icon alert alert-warning m--hide" role="alert" id="m_form_2_msg">
                                <div class="m-alert__icon">
                                    <i class="la la-warning"></i>
                                </div>
                                <div class="m-alert__text">
                                    Oh snap! Change a few things up and try submitting again.
                                </div>
                                <div class="m-alert__close">
                                    <button type="button" class="close" data-close="alert" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-4 col-sm-12">Label *</label>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <input type="hidden" name="choice" value="" />
                                <textarea class="form-control m-input" id="choiceLabel" name="choice" placeholder="" value=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-9 ml-lg-auto">
                                    <button type="submit" class="btn btn-brand">Update</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- View Choice -->
<div class="modal fade" id="view_choice_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">SURVEY CHOICE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <span class="m-section__sub">Label: </span>
                <p style="padding: 20px; margin: 10px 0px 30px; border: 4px solid rgb(239, 239, 239);" id="blockui_choice"></p>
            </div>

        </div>
    </div>
</div>
<!-- View Choice -->
<div class="modal fade" id="sample_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">SURVEY CHOICE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="view_choice_form">
                    <label>Choice</label>
                    <input type="text" name="choice" value="" readonly="">
                </form>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_survey_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title survey_ans_label"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body survey-answer-wrapper">
                    <div class="m-scrollable" id="survey-scrollable" data-scrollbar-shown="true" data-scrollable="true">
                        <div class="m-widget3">
                            <div class="spec-answer-wrapper"></div>
                        </div>

                    </div>
                    <div class="view-more">
                        <span class="spinner">
                            <img src="<?php echo base_url('assets/images/img/spinner.gif'); ?>" class="spinner-img">
                        </span>
                        <button type="button" class="btn btn-secondary btn-block" id="viewMoreAnswers">View More</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- start:: View Specific User Answer -->
<div class="modal fade" id="survey_user_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content" id="sa_content">
            <div class="modal-header">
                <div id="sa_header"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <table class="table  table-bordered table-hover">
                            <tbody class=" font-size-12">
                                <tr>
                                    <th scope="row">Account:</th>
                                    <td id="account"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Answer:</th>
                                    <td id="answer"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Survey Date:</th>
                                    <td id="date"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Date Answered:</th>
                                    <td id="date_answered"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Comments:</th>
                                    <td id="comments"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Supervisor/Reviewer:</th>
                                    <td id="supervisor"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Status:</th>
                                    <td id="status"></td>
                                </tr>
                                <tr id="dateReviewed">
                                    <th scope="row">Date Reviewed:</th>
                                    <td id="date_reviewed"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Link:</th>
                                    <td id="link"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: View Specific User Answer -->