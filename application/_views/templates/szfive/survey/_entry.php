<style>
    #reviewTextarea {
        display: none;
    }

    .m-portlet {
        margin-bottom: 10px;
    }

    .m-portlet__body {
        padding-top: 15px !important;
    }

    .m-widget5__pic {
        margin: 0 auto;
        text-align: center;
        vertical-align: middle;
    }

    .m-widget5__title {
        margin-bottom: 0;
        font-size: 24px !important;
        font-weight: 300 !important;
    }
</style>
<script>
    function noImageA(id) {
        $('.user-pic-answer' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    }

    function noImageSup(id) {
        $('.supervisor-pic-' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    }
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <?php $sub_header = ['title' => 'Survey', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive'], ['title' => 'Daily Survey', 'link' => 'szfive/daily_survey'], ['title' => 'User Answer', 'link' => uri_string()]]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content">
        <div class="row">
            <div class="col-lg-9">
                <div class="m-portlet m-portlet--bordered m-portlet--bordered-semi m-portlet--rounded" style="margin-bottom: 20px;">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="m-widget5">
                                    <div class="m-widget5__item" style="margin-bottom: 0;padding-bottom: 0;">
                                        <div class="m-widget5__pic">
                                            <img class="m-widget7__img user-pic-answer<?php echo $entry->uid; ?>" src="<?php echo $entry->pic; ?>" onerror="noImageA(<?php echo $entry->uid; ?>)"
                                                alt="" style="border-radius: 50%; width: 4rem;border: 1px solid #eee;">
                                        </div>
                                        <div class="m-widget5__content">
                                            <h1 class="m-widget5__title">
                                                <?php echo $entry->fullname; ?>
                                            </h1>
                                            <span class="m-widget5__desc">
                                                <?php echo $entry->department; ?>
                                            </span>
                                            <br>
                                            <span class="m-widget5__desc" style="color: #00BCD4;ont-size: 13px;font-weight: 500;">                                               
                                                <?php echo $entry->position; ?>
                                            </span>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="m-widget5">
                                    <div class="m-widget5__item" style="margin-bottom: 0;padding-bottom: 0;">
                                        <div class="m-widget5__content" style="padding-top: 10px;padding-left: 0;text-align: right;">
                                            <span class="m-widget5__desc" style="font-size: 11px;">
                                                Submitted on
                                                <span style=" font-weight: 500; ">
                                                    <?php echo $entry->date_answered; ?>
                                                </span>
                                            </span>
                                            <br>


                                            <?php if ($entry->isReviewed == 1): ?>
                                            <span class="m-widget5__desc" style="font-size: 11px;">
                                                Reviewed by
                                                <span style=" font-weight: 500; ">
                                                    <?php echo $entry->supervisorFname . ' ' . $entry->supervisorLname; ?>
                                                </span> on
                                                <span style=" font-weight: 500; ">
                                                    <?php echo $entry->date_reviewed; ?>
                                                </span>
                                            </span>
                                            <?php else: ?>
                                            <?php if($entry->supId == $session['emp_id']) : ?>
                                            <button type="button" class="btn m-btn--pill btn-outline-success btn-sm " onClick="viewReviewsDetails(<?php echo $entry->answerId; ?>)"
                                                style="margin-top: 8px;">Review</button>
                                            <?php endif;?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet m-portlet--bordered m-portlet--bordered-semi m-portlet--rounded">
                    <div class="m-portlet__body" style="margin-top: 10px;">
                        <div>
                            <h5 style="color: #555;font-weight: 400;border-bottom: 1px solid #eee;margin-bottom: 10px;">Feeling
                                <?php echo $entry->answer; ?>/
                                <?php echo $entry->numberOfChoices; ?>
                            </h5>
                            <p style="font-size: 16px;margin-bottom: 10px;color: #555;margin-top: 15px; font-weight: 400;">
                                <?php echo $entry->detailedAnswer; ?>
                            </p>
                            <cite title="Feeling " style="font-style: normal;color: #a7a7a7; font-size: 13px; "> - Feeling
                                <?php echo $entry->choice_label; ?>
                                <img src="<?php echo base_url( 'assets/images/') . $entry->choice_img; ?>" style=" width: 20px; "> </cite>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div style="text-align: center;">
                            <div style="margin-bottom: 10px; margin-top: 15px;">
                                <img class="supervisor-pic-<?php echo $entry->supId; ?>" src="<?php echo base_url('assets/images/') . $entry->supervisorPic; ?>"
                                    onerror="noImageSup(<?php echo $entry->supId; ?>)" style="border-radius: 50%;width: 4rem;border: 1px solid #eee;"
                                />
                            </div>
                            <p style="font-size: 13px;">
                                <span style="font-weight: 500;"><?php echo $entry->supervisorFname . ' ' . $entry->supervisorLname; ?></span>
                                <br>
                                <span>Reviewer</span>
                                <br>
                                <?php if ($entry->isReviewed == 1): ?>
                                <span class="m--font-success">
                                    <i class="fa fa-check m--font-success" style="font-size: 12px;"></i> Reviewed
                                </span>
                                <?php else: ?>
                                <!-- <span class="m--font-danger">
                                    <i class="fa fa-close m--font-danger" style="font-size: 12px;"></i> Not yet reviewed</span> -->
                                <?php endif;?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- start:: Reviews - View Specific Answer modal -->
        <div class="modal fade" id="view_specific_ans_reviews_modals" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: white;padding: 10px;">
                        <div id="view_answer_review_header"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #599791 !important;margin: 0;">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="view_reviews_answer_content"></div>
                        <div id="mark_as_review_wrapper"></div>
                        <div id="reviewTextarea" style="margin-top: 20px;">
                            <form class="m-form m-form--fit m-form--label-align-right" id="reviewAnswerForm">
                                <div class="form-group m-form__group" style=" padding: 0; ">
                                    <input type="hidden" name="answerId" />
                                    <textarea class="form-control m-input m-input--air" name="reviewMessage" id="reviewAnswerTextarea" rows="3 "></textarea>
                                    <span class="m-form__help">Please note: This field will appear if answer requires a remark/comment.</span>
                                    <div class="m-form__actions" style=" padding: 10px 0; ">
                                        <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn-sm"
                                            id="submitReview">Submit</button>
                                        <button type="reset" id="cancelReview" class="btn m-btn--pill btn-secondary btn-sm">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="alreadyReviewed"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var imagePath = baseUrl + '/assets/images/';

    $('#btn-reviewAnswer').on('click', function () {
        $('#reviewTextarea').slideDown();
    });

    $('#cancelReview').on('click', function () {
        $('#reviewTextarea').slideUp();
        $('#submitReview').removeClass('m-loader m-loader--light m-loader--right');

    });

    $('#submitReview').on('click', function () {
        $(this).addClass('m-loader m-loader--light m-loader--right');
    });

    var FormValidation = function () {

        var reviewForm = function () {
            $('#reviewAnswerForm').validate({
                rules: {
                    reviewMessage: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                },
                invalidHandler: function (event, validator) {
                    $('#submitReview').removeClass('m-loader m-loader--light m-loader--right');

                    swal({
                        "title": "",
                        "text": "There are some errors in your submission. Please correct them.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },
                submitHandler: function (form) {

                    var message = $('#reviewAnswerForm').find('textarea#reviewAnswerTextarea').val();
                    var answerId = $('#reviewAnswerForm').find('input[name=answerId]').val();
                    message = message.replace(/ +(?= )/g, '');

                    var options = {
                        url: baseUrl + '/szfive/submit_review_answer',
                        type: 'POST',
                        dataType: "json",
                        data: {
                            message: message,
                            answerId: answerId
                        },
                        success: function (response) {

                            if (response.status) {

                                swal({
                                    "title": "",
                                    "text": "You have successfully reviewed this answer.",
                                    "type": "success",
                                    "timer": 1000,
                                    "showConfirmButton": false
                                });
                                $('#view_specific_ans_reviews_modals').modal('hide');
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);
                            }
                        },
                        error: function () {
                            alert('error');
                        },
                    };

                    $(form).ajaxSubmit(options);
                }
            })
        }

        return {
            init: function () {
                reviewForm();
            }
        };
    }();
    jQuery(document).ready(function () {
        FormValidation.init();
    });

    function viewReviewsDetails(answerId) {
        $('#alreadyReviewed').html('');

        $.when(fetchPostData({
                answerId: answerId
            }, '/szfive/get_reviews_specific_answer'))
            .then(function (data) {

                var data = jQuery.parseJSON(data);

                header =
                    '<div class="m-widget3__header" style="display: table;padding: 0px 10px;padding-top: 8px; ">';
                header += '        <div class="m-widget3__user-img" style="margin-bottom: .7rem;">';
                header += '            <img class="m-widget3__img answer-pic-dt' + data.uid + '" src="' + data.pic +
                    '" onerror="noImage2(' + data.uid + ')" alt="" style="width: 3.2rem;border-radius: 50%;">';
                header += '        </div>';
                header +=
                    '        <div class="m-widget3__info" style="display:table-cell;width:100%; padding-left:1rem;font-size:1rem;vertical-align:middle;">';
                header += '            <span class="m-widget3__username" style="font-size: 15px;color: #36817a;">';
                header += '                ' + data.fullname + ' answered ';
                header += '                <u style=" font-size: 17px;">' + data.answer + '</u> out of';
                header += '                <u>' + data.numberOfChoices + '</u> for the daily survey on ' + data.survey_date +
                    '.';
                header += '            </span>';
                header += '            <br>';
                header += '            <span class="m-widget3__time" style="font-size: .85rem;">' + data.answer_date +
                    '</span>';
                header += '        </div>';
                header += '    </div>';

                $('#view_answer_review_header').html(header);

                str = '<div class="m-widget3__item">';
                str += '    <div class="m-widget3__body" id="review_userAnswerId" data-id="' + answerId +
                    '" style=" padding: 0px 10px; margin-bottom: 25px; ">';
                str += '        <blockquote class="blockquote">';
                str += '            <p class="mb-0" style=" font-weight: 400;padding: 0 0 10px 0;">' + data.detailedAnswer +
                    '</p>';
                str += '            <footer class="blockquote-footer">Feeling';
                str += '                <img src="' + imagePath + data.choice_img + '" style=" width: 20px; ">';
                str += '                <cite title="Source Title">';
                str += '                    ' + data.choice_label + ' </cite>';
                str += '            </footer>';
                str += '        </blockquote>';
                str += '     </div>';
                str += '    </div>'

                $('.view_reviews_answer_content').html(str);

                var str_review = '';

                if (parseInt(data.isReviewed) == 1) {

                    var dateReviewed = moment(data.dateReviewed).format('MMMM D, YYYY hh:mm:ss A');
                    var message = data.message ? data.message : 'No remarks';

                    str_review +=
                        '<span class="m-badge m-badge--success m-badge--wide">Reviewed by</span><span style="color: #649e99;"> <b style="font-weight: 400;">' +
                        data.supervisorFname + ' ' + data.supervisorLname + '</b> on <b style="font-weight: 400;">' +
                        dateReviewed + '</b></span>';
                    str_review +=
                        '<p style="border: 4px solid #f7f7fa;padding: 8px;margin-top: 10px;"><b style="color: #000000b5;font-weight: 600;">Remarks:</b> &nbsp;&nbsp;' +
                        message + '</p>';
                    str_review += '<small>Reviewed <b><i>' + data.time_diff +
                        '</i></b> after the team member answered the survey.</small>';
                    $('#reviewTextarea').hide();
                } else {
                    if (data.reviewerId == data.session_id) {
                        $('#reviewTextarea').hide();
                        $('#reviewAnswerForm').find('input[name=answerId]').val(answerId);

                        str_review +=
                            '<button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-info m-btn--gradient-to-accent btn-sm" id="btn-reviewAnswer" onClick="markAsReviewed(' +
                            data.answer + ',' + data.numberOfChoices + ')">Mark as Reviewed</button>';
                    } else {
                        str_review += '<span class="m-badge m-badge--danger m-badge--wide">Unreviewed</span>';
                    }
                }

                $('#mark_as_review_wrapper').html(str_review);
            });

        setTimeout(function () {
            $('#view_specific_ans_reviews_modals').modal('show');
        }, 500);
    }

    function markAsReviewed(answer, numberOfChoices) {

        var numbers = [1];
        numbers.push(numberOfChoices);

        var median = 0;
        var numsLen = numbers.length;
        numbers.sort();

        if (numsLen % 2 === 0) { // is even
            // average of two middle numbers
            median = (numbers[numsLen / 2 - 1] + numbers[numsLen / 2]) / 2;
        } else { // is odd
            // middle number only
            median = numbers[(numsLen - 1) / 2];
        }

        if (answer <= median) {
            $('#reviewTextarea').slideDown();
        } else {
            $('#reviewTextarea').css("display", "none");
            $.when(fetchPostData({
                    message: '',
                    answerId: $('#review_userAnswerId').data('id')
                }, '/szfive/submit_review_answer'))
                .then(function (response) {
                    var resp = jQuery.parseJSON(response);

                    if (resp.status == true) {

                        var data = resp.data;
                        swal({
                            "title": "",
                            "text": "You have successfully reviewed this answer.",
                            "type": "success",
                            "timer": 1500,
                            "showConfirmButton": false
                        });
                        location.reload();
                    } else {
                        swal({
                            "title": "Error",
                            "text": "There are some errors in your submission. Please correct them.",
                            "type": "error",
                            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                        });
                    }
                });
        }
    }
</script>