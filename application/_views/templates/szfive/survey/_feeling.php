<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'SZFive', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive#']]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content" id="szfive-home">
        <?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list);?>
        <?php $tabs = ['tabs' => [['title' => 'Highlights', 'link' => 'szfive', 'active' => ''], ['title' => 'Activity', 'link' => 'szfive/activity_public', 'active' => ''], ['title' => 'Feeling', 'link' => 'szfive/feeling#', 'active' => 'active'], ['title' => 'Comments', 'link' => 'szfive/comments', 'active' => '']]];?>
        <?php $this->load->view('templates/szfive/_includes/user_info_bar', $tabs);?>
        <?php include '_modals.php';?>
        <?php if($uri_segment[2] == 'feeling') {
            $pane1 = 'active';
            $pane2 = '';
        } 
            else if($uri_segment[2] == 'team_answers'){
            $pane1 = '';
            $pane2 = 'active';
        }?>
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--brand m-tabs-line" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php echo $pane1; ?>" href="<?php echo base_url('szfive/feeling'); ?>" role="tab">

                                My Answers
                            </a>
                        </li>
                        <?php if($has_subordinates == true) :?>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php echo $pane2; ?>" href="<?php echo base_url('szfive/team_answers'); ?>"
                                role="tab">
                                My Team
                            </a>
                        </li>
                        <?php endif;?>
                    </ul>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="m-form m-form--label-align-right">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input m-input--pill" placeholder="Type any keyword..."
                                                id="userAnswerSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--right">
                                                <span>
                                                    <i class="la la-search m--font-secondary"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body" style="padding: 0;">
                <div class="tab-content">
                    <input type="hidden" name="answer_type" value="<?php echo $answer_type; ?>">
                    <input type="hidden" name="user_id" value="<?php echo $session['uid']; ?>">
                    <input type="hidden" name="user_type" value="<?php echo $session['role_id']; ?>">

                    <div class="tab-pane <?php echo $pane1; ?>" role="tabpanel">
                        <?php if($uri_segment[2] == 'feeling') :?>
                        <?php if (strpos($permissions->settings, 'viewOwnAnswers') !== false): ?>
                        <div class="m_datatable_answers_user"></div>

                        <?php else: ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Note:</strong> You cannot access the survey answers.
                        </div>
                        <?php endif;?>
                        <?php endif;?>
                    </div>
                    <?php if($has_subordinates == true) :?>
                    <div class="tab-pane <?php echo $pane2; ?>" role="tabpanel">
                        <?php if($uri_segment[2] == 'team_answers') :?>
                        <div class="m_datatable_answers_user"></div>
                        <?php endif;?>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
    <!--end: Survey Answers -->
</div>
<?php $this->load->view('templates/szfive/_includes/scripts');?>
<script type="text/javascript">
    var imagePath = baseUrl + '/assets/images/';

    var UserAnswerDatatable = function () {

        var userAnswerSelector = function (data) {
            var user_id = $("input[name='user_id']").val();
            var user_type = $("input[name='user_type']").val();
            var answer_type = $("input[name='answer_type']").val();
            var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            method: 'POST',
                            url: baseUrl + '/szfive/get_user_answers',
                            headers: {
                                'x-my-custom-header': 'some value',
                                'x-test-header': 'the value'
                            },
                            params: {
                                query: {
                                    user_id: user_id,
                                    user_type: user_type,
                                    answer_type: answer_type,
                                },
                            },
                        }
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },
                layout: {
                    theme: 'default',
                    class: 'dashed-table',
                    scroll: false,
                    footer: false,
                    header: false
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    placement: ['bottom'],
                    items: {
                        pagination: {
                            pageSizeSelect: [5, 10, 20, 30, 50]
                        },
                    }
                },
                search: {
                    input: $('#userAnswerSearch'),
                },
                translate: {
                    records: {
                        processing: 'Please wait...',
                        noRecords: '<i class="flaticon-edit" style="font-size: 24px;margin: 0 5px;"></i><br> No answers found.',
                    },
                },
                columns: [{
                        field: 'answerId',
                        title: 'Score',
                        width: 750,
                        template: function (data) {
                            var pic = data.pic ? data.pic : "images/img/sz.png";
                            var detailedAnswer = (data.detailedAnswer ? data.detailedAnswer :
                                'No detailed answer found.')
                            str = '<div class="m-widget3__item" style="padding: 0 25px;">';
                            str += '<div class="m-widget3__header" style="display: table;">';
                            str +=
                                '<div class="m-widget3__user-img" style="margin-bottom: .7rem;">';
                            str += '<img class="m-widget3__img user-pic-dt' + data.emp_id + '" ';
                            str += '  src = "' + imagePath + pic + '" onerror="noImage(' + data.emp_id +
                                ')" alt = "" style="width: 3.2rem;border-radius: 50%;">';
                            str += '</div> ';
                            str +=
                                '<div class="m-widget3__info" style="display:table-cell;width:100%; padding-left:1rem;font-size:1rem;vertical-align:middle;"> ';
                            str +=
                                '<a style="color:inherit;" href="' + baseUrl + '/' + data.link +
                                '"><span class="m-widget3__username" style="font-size: 1rem;font-weight: 500;"> ';
                            str += data.user_name;
                            str += '</a></span>';
                            str += '<br>';
                            str += '<span class="m-widget3__time" style="font-size: .85rem;">';
                            str += data.dateAnswered;
                            str += '</span>';
                            str += '</div>';
                            str += '</div>';
                            str += '<div class="m-widget3__body" style="margin-left: 58px;">';
                            str += '<p class="m-widget3__text">';
                            str += detailedAnswer;
                            str += '</p>';
                            str += '</div>';
                            str += '</div>';

                            return str;
                        },
                    },
                    {
                        field: 'answer',
                        title: "Uname",
                        textAlign: 'center',
                        width: 180,
                        template: function (data) {

                            var status = {
                                1: {
                                    'title': 'Teary',
                                    'color': 'rgb(237, 77, 80)'
                                },
                                2: {
                                    'title': 'Sad',
                                    'color': 'rgb(247, 180, 50)'
                                },
                                3: {
                                    'title': 'Average',
                                    'color': 'rgb(252, 214, 51)'
                                },
                                4: {
                                    'title': 'Happy',
                                    'color': 'rgb(199, 212, 90)'
                                },
                                5: {
                                    'title': 'Amazing',
                                    'color': 'rgb(98, 212, 153)'
                                },
                            };
                            output =
                                '<div class="m-card-user m-card-user--sm">\
                                <div class="m-card-user__pic" style="padding-right: 20px;">\
                                    <div class="m-card-user__no-photo" style="color: black;background-color: ' +
                                status[data.answer].color + '"><span>' + data.answer +
                                '</span></div>\
                                </div>\
                            </div>';
                            return output;

                        }
                    },


                ],
            };

            $('.m_datatable_answers_user').mDatatable(options);

        };

        return {
            init: function () {
                userAnswerSelector();
            }
        };
    }();
    jQuery(document).ready(function () {
        UserAnswerDatatable.init();
    });

    function noImage(id) {
        $('.user-pic-dt' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    }
</script>