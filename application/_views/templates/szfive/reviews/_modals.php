<!-- start:: Reviewed Answers Datatable Modal -->
<div class="m-content">
    <div id="survey_reviews_modals">
        <div class="modal fade" id="reviewed_survey_answers_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="view_ans_review_title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="m-input-icon m-input-icon--right m-input--pill" id='reviews_team_list_daterange'>
                                                <input type='text' class="form-control m-input m-input--pill" readonly placeholder="Select date range" />
                                                <span class="m-input-icon__icon m-input-icon__icon--right">
                                                    <span class="m-nav__link-icon">
                                                        <i class="flaticon-calendar-3" style="color: #b5b4b4 !important;"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="m-form m-form--label-align-right">
                                                <div class="m-input-icon m-input-icon--right m-input--pill">
                                                    <input type="text" class="form-control m-input m-input--pill" placeholder="Search..." id="reviewersListSearch">
                                                    <span class="m-input-icon__icon m-input-icon__icon--right">
                                                        <span class="m-nav__link-icon">
                                                            <i class="flaticon-search-1" style="color: #b5b4b4 !important;"></i>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m_datatable_reviewed_survey_answers"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Reviewed Answers Datatable Modal -->
    </div>

    <!-- start:: Reviews - View Specific Answer modal -->
    <div class="modal fade" id="view_specific_ans_reviews_modals" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: white;padding: 10px;">
                    <div id="view_answer_review_header"></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #599791 !important;margin: 0;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="view_reviews_answer_content"></div>
                    <div id="mark_as_review_wrapper"></div>
                    <div id="reviewTextarea" style="display: none;">
                        <form class="m-form m-form--fit m-form--label-align-right" id="reviewAnswerForm">
                            <div class="form-group m-form__group" style=" padding: 0; ">
                                <input type="hidden" name="answerId" />
                                <textarea class="form-control m-input m-input--air" name="reviewMessage" id="reviewAnswerTextarea" rows="3 "></textarea>
                                <span class="m-form__help">Please note: This field will appear if answer requires a remark/comment.</span>
                                <div class="m-form__actions" style=" padding: 10px 0; ">
                                    <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn-sm"
                                        id="submitReview">Submit</button>
                                    <button type="reset" id="cancelReview" class="btn m-btn--pill btn-secondary btn-sm">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="alreadyReviewed"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Reviews - View Specific Answer modal -->