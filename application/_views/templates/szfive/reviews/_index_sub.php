<style>
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row.m-datatable__row--even>.m-datatable__cell {
        background: #ffffff;
    }

    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row.m-datatable__row--hover:not(.m-datatable__row--active)>.m-datatable__cell {
        background: #ffffff;
    }

    .m-link {
        color: #57616c;
    }

    .m-link:hover {
        color: #57616c;
    }

    .m-widget5__title {
        margin-bottom: 0;
        font-size: 24px !important;
        font-weight: 300 !important;
    }

    .symbol {
        border-radius: 50%;
        color: #212529;
        cursor: pointer;
        display: inline-block;
        font-size: 17.5px;
        height: 43px;
        width: 43px;
        text-align: center;
        margin-left: -12px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'Reviews', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive'], ['title' => 'Reviews', 'link' => 'szfive/reviews#']]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
	<div class="m-content" id="survey-reviews">
		<?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list);?>
        <div class="m-portlet">
            <div class="m-portlet__body" style="padding: 10px 20px;">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="m-widget5">
                            <div class="m-widget5__item" style="margin-bottom: 0;padding-bottom: 12px;">
                                <div class="m-widget5__pic">
                                    <?php $imageClass = 'main-image-' . $session['uid']; ?>
                                    <img class="m-widget7__img main-image-<?php echo $session['uid'];?>" src="<?php echo $user_details->avatar; ?>" onerror='noImageFound("<?php echo $imageClass; ?>")'
                                        alt="" style="border-radius: 50%; width: 6rem;border: 1px solid #eee;height: 6rem;">
                                </div>
                                <div class="m-widget5__content" style="padding-top: 14px;">
                                    <h1 class="m-widget5__title" style=" font-size: 18px;">
                                        <?php echo $user_details->name; ?>
                                    </h1>
                                    <p>
                                        <span class="m-widget5__desc">
                                            <?php echo $user_details->pos_details; ?>
                                        </span>
                                        <br>
                                        <span class="m-widget5__desc">
                                            <?php echo $user_details->dep_name . ' | ' . $user_details->dep_details; ?>
                                        </span>
                                    </p>
                                    <p style="font-size: 12px;">
                                        <span style="margin-right: 20px;">
                                            <i class="fa fa-envelope-o"></i>
                                            <?php echo $user_details->email; ?>
                                        </span>
                                        <span>
                                            <i class="fa fa-user-o"></i> Reviewer:
                                            <?php echo $user_details->supervisorName; ?>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="m-widget24" style="">
                            <div class="m-widget24__item">
                                <p style="text-align: center;margin-bottom: -10px;">Reviewed Answers
                                    <span class="m--font-success" style=" margin-left: 20px;font-size: 24px;">
                                        <?php echo $all_reviewed[0]->count; ?>
                                    </span>
                                </p>
                                <div class="progress m-progress--sm" style=" margin-top: 20px;">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: <?php echo $review_percentage; ?>%;" aria-valuenow="<?php echo $review_percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change" style="margin-bottom: 7px;">
                                    Complete
                                </span>
                                <span class="m-widget24__number">
                                    <?php echo $review_percentage; ?>%
                                </span>
                            </div>
                            <?php if ($all_unreviewed[0]->count != 0) : ?>
                            <div class="m-alert m-alert--outline alert alert-danger alert-dismissible fade show" role="alert" style="font-size: 13px;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                You have <strong style="font-weight: 700;"><?php echo $all_unreviewed[0]->count; ?></strong> unreviewed post.
                            </div>
                            <?php endif ;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet" id="review_details_tab" style="box-shadow: 0 1px 2px rgba(0,0,0,0.15);">
            <div class="m-portlet__body" style="padding: 0;">
                <div class="m-portlet m-portlet--unair">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Team SZFives
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row reviews-form-control" style="margin-bottom: 20px;">
                            <div class="col-lg-5">
                                <div>
                                    <span class="m-subheader__daterange" id="review_daterange">
                                        <span class="m-subheader__daterange-label">
                                            <span class=" m-subheader__daterange-title-reviews">This Week: </span>
                                            <span class=" m-subheader__daterange-date-reviews m--font-brand">
                                                <?php echo $this_week; ?>
                                            </span>
                                        </span>
                                        <a class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                            <i class="la la-angle-down"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <div class="m-select2 m-select2--pill">
                                    <input type="hidden" name="stat" />
                                    <select class="form-control m-select2" id="select_status" name="param" data-placeholder="Pill style">
                                        <option></option>
                                        <option value='1'>Reviewed</option>
                                        <option value='0'>Unreviewed</option>
                                        <option value="all">All</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="m-form m-form--label-align-right">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input m-input--pill" placeholder="Search" id="reviewsSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                            <span class="m-nav__link-icon">
                                                <i class="flaticon-search-1" style="color: #b5b4b4 !important;"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div>
                                    <!--begin: Datatable -->
                                    <div class="m_datatable_review_answers"></div>
                                    <!--end: Datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#btn-reviewAnswer').on('click', function () {
        $('#reviewTextarea').slideDown();
    });

    $('#cancelReview').on('click', function () {
        $('#reviewTextarea').slideUp();
        $('#submitReview').removeClass('m-loader m-loader--light m-loader--right');

    });

    $('#submitReview').on('click', function () {
        $(this).addClass('m-loader m-loader--light m-loader--right');
    });

    var imagePath = baseUrl + '/assets/images/';

    function noImageFound(image) {
        $('.' + image).attr('src', baseUrl + '/assets/images/img/sz.png');
    }
    var ReviewAnswersDatatable = function () {

        var colorStatus = {
            1: {
                'title': 'Teary',
                'fcolor': '#ed4d5026',
                'bcolor': 'rgb(237, 77, 80)'
            },
            2: {
                'title': 'Sad',
                'fcolor': '#f7b43233',
                'bcolor': '#f7b432'
            },
            3: {
                'title': 'Average',
                'fcolor': '#fcd6332b',
                'bcolor': '#fcd633'
            },
            4: {
                'title': 'Happy',
                'fcolor': 'rgba(199, 212, 90, 0.47)',
                'bcolor': 'rgba(199, 212, 90)'
            },
            5: {
                'title': 'Amazing',
                'fcolor': 'rgba(98, 212, 153, 0.2)',
                'bcolor': 'rgba(98, 212, 153)'
            },
        };

        var reviewAnswer = function (data) {

            var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            method: 'POST',
                            url: baseUrl + '/szfive/get_to_be_reviewed_answers',
                            headers: {
                                'x-my-custom-header': 'some value',
                                'x-test-header': 'the value'
                            },
                            params: {
                                query: {
                                    status: data.status,
                                    startDate: data.startDate,
                                    endDate: data.endDate,
                                },
                            },
                            map: function (raw) {
                                var dataSet = raw;
                                if (typeof raw.data !==
                                    'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            },
                        }
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },
                layout: {
                    class: 'm-table m-table--head-bg-brand',
                    scroll: false,
                    footer: false,
                    header: false
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    placement: ['bottom'],
                    items: {
                        pagination: {
                            pageSizeSelect: [5, 10, 20, 30, 50]
                        },
                    }
                },
                search: {
                    input: $('#reviewsSearch'),
                },
                columns: [{
                    field: "notifId",
                    title: "Notification",
                    sortable: false,
                    width: 900,
                    overflow: 'visible',
                    template: function (data) {
                        var dateAnswered = moment(data.dateCreated).format('ddd, MMMM D') +
                            ' at ' + moment(data.dateCreated).format('h:mm A');
                        str =
                            '<div class="m-widget3__header" style="display: table;">';
                        str +=
                            '<div class="m-widget3__user-img" style="margin-bottom: .7rem;white-space: nowrap!important;">';
                        str +=
                            '<img class="m-widget3__img user-pic-dt' + data.uid + '" src="' +
                            data.user_img + '"onerror="noUserImage(' + data.uid +
                            ')" alt="" style="width: 3.5rem;border-radius: 50%;display: inline-block;vertical-align: middle;border: 3px solid #fff;">';
                        str +=
                            '    <span class="symbol" style="background-color: ' +
                            colorStatus[data.answer]
                            .bcolor +
                            ';"><span style="margin-top: 8px;display: block;color: #fff">' +
                            data.answer + '</span></span>';
                        str += '</div>';
                        str +=
                            '<div class="m-widget3__info" style="display:table-cell;width:100%; padding-left:1rem;font-size:1rem;vertical-align:middle;">';
                        str +=
                            '<p style="margin-bottom: 0px;"><a href="' + baseUrl + '/' + data.link +
                            '"><span class="m-card-user__email m-link"><span class="m-widget3__username" style="font-size: 13px;font-weight: 500;">' +
                            data.empFname + ' ' + data.empLname + '</span>';
                        str +=
                            ' <span> answered </span>';
                        str +=
                            ' <span class="m-widget3__username" style="font-size: 13px;font-weight: 500;">' +
                            data.answer + ' out of ' + data.numberOfChoices +
                            '</span></span></a>';
                        str +=
                            ' <span>on the the daily survey</span> ';
                        str += '<small> ' + dateAnswered + '</small>';
                        str += '</p>';
                        str +=
                            '<blockquote class="blockquote" style="margin-bottom: 0;margin-top: 5px;"> <p class="mb-0" style=" font-weight: 400;padding: 0 0 10px 0;font-size: 14px;"> ' +
                            data.detailedAnswer +
                            ' </p> <footer class="blockquote-footer" style="font-weight: 100;margin-top: -6px;font-size: 13px;">Feeling <cite title="Feeling" style="font-style: normal;"> ' +
                            data.choice_label +
                            ' <img src="' +
                            imagePath + '/' + data.choice_img +
                            '" style=" width: 20px; "> </cite> </footer> ';
                        if (data.isReviewed == 1) {
                            str +=
                                '<p style="font-size: 12px;font-weight: 200;"><span class="m--font-success"><i class="fa fa-check m--font-success" style="font-size: 12px;"></i> Reviewed</span></p>';
                        } else {
                            str +=
                                '<p style="font-size: 12px;font-weight: 200;"><span class="m--font-danger"><i class="fa fa-close m--font-danger" style="font-size: 12px;"></i> Not yet reviewed</span></p>';
                        }

                        str += ' </blockquote>';
                        str += ' </div>';
                        str += ' </div>';
                        return str;
                    }
                }],
            };

            var datatable = $('.m_datatable_review_answers').mDatatable(options);
            var query = datatable.getDataSourceQuery();

            $('#select_status').on('change', function () {

                query.status = $(this).val() != 'all' ? $(this).val() : 'all';
                datatable.setDataSourceQuery(query);
                datatable.load();

                var val = $("#select_status :selected").val();

            }).val(typeof query.status !== 'undefined' ? query.status : '');
        };

        return {
            init: function (data) {
                reviewAnswer(data);
            },
        };
    }();

    var Select2 = function () {
        var statuses = function () {
            $('#select_status').select2({
                placeholder: "Select a status",
            });
        }
        return {
            init: function () {
                statuses();
            }
        };
    }();

    var DatePickers = function () {

        var reviewDates = function () {

            if ($('#review_daterange').length == 0) {
                return;
            }

            var picker = $('#review_daterange');
            var start = moment().startOf('isoWeek');
            var end = moment().endOf('isoWeek');

            function cb(start, end, label) {

                var title = '';
                var range = '';

                if (start.format('YYYY-MM-DD') == moment().startOf('isoWeek').format('YYYY-MM-DD') && end.format(
                        'YYYY-MM-DD') == moment().endOf('isoWeek').format('YYYY-MM-DD')) {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Today') {
                    title = 'Today:';
                    range = start.format('MMM D, Y');
                } else if (label == 'Yesterday') {
                    title = 'Yesterday:';
                    range = start.format('MMM D, Y');
                } else if (label == 'This Week') {
                    title = 'This Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Week') {
                    title = 'Last Week:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'This Month') {
                    title = 'This Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else if (label == 'Last Month') {
                    title = 'Last Month:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                } else {
                    title = 'Custom:';
                    range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                }

                picker.find('.m-subheader__daterange-date-reviews').html(range);
                picker.find('.m-subheader__daterange-title-reviews').html(title);

                var params = {
                    status: $("#select_status :selected").val(),
                    startDate: start.format('YYYY-MM-DD'),
                    endDate: end.format('YYYY-MM-DD')
                };
                showDatatable(params);
            }

            picker.daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'This Week': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
                    'Last Week': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1,
                        'week').endOf('isoWeek')],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                }
            }, cb);

            cb(start, end, '');
        }

        return {
            init: function () {
                reviewDates();
            },
        };
    }();
    jQuery(document).ready(function () {
        Select2.init();
        DatePickers.init();
    });

    function noUserImage(id) {
        $('.user-pic-dt' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    }

    function showDatatable(params) {
        $('.m_datatable_review_answers').mDatatable('destroy');
        ReviewAnswersDatatable.init(params);
    }
</script>