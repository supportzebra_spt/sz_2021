<div class="modal fade" id="high_five_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Give A High Five!
                    <img src="<?php echo base_url('assets/images/img/hi5.png'); ?>" width="50">
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12">Employee</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <textarea class="mention" rows="5" spellcheck="false"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__actions m-form__actions">
                        <div class="row">sendHi5Comment
                            <div class="col-lg-9 ml-lg-auto">
                                <button type="submit" class="btn btn-danger" id="submitMentions" disabled>Submit</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- start :: Comment on a High Five! -->
<div class="modal fade" id="comment_hi5_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content" id="comment_container">
            <div class="modal-header">
                <div id="comment-modal-header"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <div class="modal-body">
                <div id="comment-parent" style=" border-bottom: 1px solid #eaeaea; "></div>
                <div class="row comments-child-wrapper">
                    <div class="m-portlet__body survey-answer-wrapper">
                        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" id="m-scrollable-comment">
                            <div class="m-portlet__body">
                                <div id="comment-child"></div>
                                <div class="view-more">
                                    <span class="spinner">
                                        <img src="<?php echo base_url('assets/images/img/spinner.gif'); ?>" class="spinner-img">
                                    </span>
                                    <button type="button" class="btn m-btn--pill btn-outline-brand btn-sm" id="viewMoreComments">Load more...</button>
                                </div>
                            </div>
                        </div>
                        <form id="highfive_comment_form">
                            <div class="form-group m-form__group" style="margin-bottom: 0;">
                                <div class="m-input-icon m-input-icon--right" style="background: white !important;    margin-bottom: 0px;">
                                    <input type="hidden" name="hiFiveId">
                                    <!-- <input type="text" id="inputHi5Comment" class="form-control m-input m-input--pill" placeholder="Write something..." name="comment">
                                    <span class="m-input-icon__icon m-input-icon__icon--right">
                                        <span>
                                            <button type="submit" class="btn btn-outline-metal m-btn m-btn--icon m-btn--icon-only send-comment-icon" id="sendHi5Comment">
                                                <i class="fa fa-send"></i>
                                            </button>
                                        </span>
                                    </span> -->
                                    <textarea class="form-control m-input highfive_post_comment" name="comment" id="inputHi5Comment" placeholder='Write something...'></textarea>
                                    <div id="comment-options">
                                        <div class="row" style=" margin-top: 10px; ">
                                            <div class="col-lg-6">
                                                <div class="m-checkbox-inline">
                                                    <label class="m-checkbox">
                                                        <input type="checkbox" id="checkPrivate"> Private comment to
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <select class="form-control m-bootstrap-select m_selectpicker" name="postType" id="select_user">
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-brand" id="submitMentions">Post comment</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end :: Comment on a High Five! -->

<!-- start :: Edit Comment modal -->
<div class="modal fade" id="edit_comment_hi5_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #505a6b;padding: 15px;">
                <h5 class="modal-title">Edit Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <div class="modal-body">
                <form class="m-form m-form--fit m-form--label-align-right" id="highfive_edit_comment_form">
                    <div class="m-portlet__body survey-form">
                        <div class="m-form__content">
                            <div class="m-alert m-alert--icon alert alert-warning m--hide" role="alert" id="m_form_2_msg">
                                <div class="m-alert__icon">
                                    <i class="la la-warning"></i>
                                </div>
                                <div class="m-alert__text">
                                    Oh snap! Change a few things up and try submitting again.
                                </div>
                                <div class="m-alert__close">
                                    <button type="button" class="close" data-close="alert" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group" id="edit-hi5comment">
                            <input type="hidden" name="commentId" />
                            <input type="hidden" name="isPublic" />
                            <textarea class="form-control m-input m-input--air highfive_edit_comment" name="commentEdit" id="hi5commentDetail" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-brand">Update</button>
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end :: Edit Comment modal -->

<!-- start :: Edit High Five Post modal -->
<!-- <div class="modal fade" id="showEditHiFivePost" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <div class="modal-body">
                <form class="m-form m-form--fit m-form--label-align-right" id="hi5_comment_form">
                    <div class="m-portlet__body survey-form">
                        <div class="m-form__content">
                            <div class="m-alert m-alert--icon alert alert-warning m--hide" role="alert" id="m_form_2_msg">
                                <div class="m-alert__icon">
                                    <i class="la la-warning"></i>
                                </div>
                                <div class="m-alert__text">
                                    Oh snap! Change a few things up and try submitting again.
                                </div>
                                <div class="m-alert__close">
                                    <button type="button" class="close" data-close="alert" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group" id="edit-hi5comment">
                            <input type="hidden" name="commentId" />
                            <textarea class="form-control m-input m-input--air" name="comment" id="hi5commentDetail" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-9 ml-lg-auto">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> -->
<!-- end :: Edit High Five Post modal -->