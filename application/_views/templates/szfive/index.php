<style>
    .likeType {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        display: block;
        height: 40px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 40px;
        cursor: pointer;
    }

    .likeTypeSmall {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        float: left;
        margin-right: -3px;
        height: 20px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 20px;
        cursor: pointer;
    }

    .likeIconDefault {
        width: 17px;
        float: left;
        margin-right: 5px;
        background: url("<?php echo base_url('assets/images/img/like.png'); ?>");
        height: 16px;
        background-size: 100%;
    }

    #m-scroll-highfive-posts {
        max-height: 450px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'SZhoutOut', 'breadcrumbs' => [['title' => 'SZhoutOut', 'link' => 'szfive#']]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content survey-answers" id="highfive-page">
        <?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list);?>
        <?php $tabs = ['tabs' => [['title' => 'Highlights', 'link' => 'szfive#', 'active' => 'active'], ['title' => 'Activity', 'link' => 'szfive/activity_public', 'active' => ''], ['title' => 'Feeling', 'link' => 'szfive/feeling', 'active' => ''], ['title' => 'Comments', 'link' => 'szfive/comments', 'active' => '']]];?>
        <?php $this->load->view('templates/szfive/_includes/user_info_bar', $tabs);?>
        <div class="row">
            <?php $this->load->view('templates/szfive/highfive/_modals');?>
        </div>
        <div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <img  src="<?php echo base_url('/assets/images/img/hi5.png'); ?>" width="30" style="filter: saturate;">
                        </span>
                        <h3 class="m-portlet__head-text">
                            Recently received SZhoutOut
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                    <?php if (!empty($received_highfives)): ?>
                    <?php foreach ($received_highfives as $highfive): ?>
                    <?php $imageClass1 = "no-image1-" . $highfive->senderId;?>
                    <div style="margin-bottom: 20px;">
                        <div class="row" style="margin: 0;" id="highfive-item<?php echo $highfive->hiFiveId; ?>">
                            <div style="margin-left: 15px;">
                                <img class="m-widget3__img no-image1-<?php echo $highfive->senderId; ?>" src="<?php echo base_url('assets/images/') . $highfive->pic; ?>"
                                    onerror='noImageFound("<?php echo $imageClass1; ?>")' alt="" style="width: 3.7rem;height: 3.7rem;border-radius: 50%;display: inline-block;vertical-align: middle;border: 3px solid #ffffff;margin-left: -12px;">
                                <?php if ($highfive->moreThanOne == true): ?>
                                <span class="symbol" style="background-color: #716aca; text-align: center;">
                                    <span style="margin-top: 6px;display: block;color: #fff;font-size: 22px;font-weight: 500;">
                                        <?php echo $highfive->noOfRecepients; ?>
                                    </span>
                                </span>
                                <?php else: ?>
                                <?php $imageClass2 = "no-image2-" . $highfive->hi5recepientId;?>
                                <img class="m-widget3__img no-image2-<?php echo $highfive->hi5recepientId; ?>" src="<?php echo $highfive->hi5recepient; ?>"
                                    onerror='noImageFound("<?php echo $imageClass2; ?>")' alt="" style="width: 3.7rem;height: 3.7rem;border-radius: 50%;display: inline-block;vertical-align: middle;border: 3px solid #fff;margin-left: -12px;">
                                <?php endif;?>
                            </div>
                            <div class="col-md-10" style="word-wrap: break-word;">
                                <a href="<?php echo base_url() . $highfive->link; ?>" class="m-widget3__username" style="font-size: 13px;font-weight: 500;color: #525252;">
                                    <?php echo $highfive->user_name; ?>
                                    <span style="font-weight: 400;margin: 0 2px;"> to </span>
                                    <span>
                                        <?php echo $highfive->header_msg; ?>
                                    </span>
                                </a>
                                <br>
                                <span class="m-widget3__time" style="font-size: .85rem;">
                                    <?php echo $highfive->dateAnswered; ?>,
                                    <?php echo $highfive->dateSubmitted; ?>
                                    <?php if ($highfive->isPublic == 1): ?> &nbsp;
                                    <i class="fa fa-globe" style="font-size: 12px;color: #767e84d6;"></i>
                                </span>
                                <?php else: ?> &nbsp;
                                <i class="fa fa-lock" style="font-size: 12px;color: #767e84d6;"></i>
                                </span>
                                <?php endif;?>
                                <div class="m-widget3__body">
                                    <p class="m-widget3__text" style="font-size: 15px; margin-bottom: 0">
                                        <?php echo $highfive->message; ?>
                                    </p>
                                    <div id="tool_tip_reactions">
                                        <div style="margin-top: 15px;">
                                            <span id="all-reaction<?php echo $highfive->hiFiveId; ?>">
                                                <?php echo $highfive->reactions; ?>
                                            </span>
                                            <a  data-html="true"  data-placement="top" data-toggle="tooltip" href="#" data-original-title="<?php echo $highfive->tooltip; ?>" class="like-info" id="like-info<?php echo $highfive->hiFiveId; ?>">
                                                <?php echo $highfive->likes_info; ?>
                                            </a>
                                        </div>
                                        <?php $comment = 'Comment';?>
                                        <?php if ($highfive->totalComment->count != "0"): ?>
                                        <?php $comment = intval($highfive->totalComment->count) . ' comments';?>
                                        <?php endif;?>
                                        <div class="row" style="text-align: center;margin-left: 0px;margin-top: 5px;font-size: 12px;">
                                            <?php if ($highfive->my_reaction == ''): ?>
                                            <a class="reaction" id="like<?php echo $highfive->hiFiveId; ?>" rel="like"
                                                style="margin-right: 10px;">
                                                <i class="likeIconDefault"></i> Like
                                                <?php else: ?>
                                                <a class="unLike" id="like<?php echo $highfive->hiFiveId; ?>" rel="unlike"
                                                    style="margin-right: 10px;">
                                                    <?php endif;?>
                                                    <?php echo $highfive->my_reaction; ?>
                                                </a>
                                                <a style="cursor: pointer;" onClick="commentHi5(<?php echo $highfive->hiFiveId; ?>)">
                                                    <i class="fa fa-comment" style="color: #afb4bd;font-size: 1rem;"></i>
                                                    <?php echo $comment; ?>
                                                </a>
                                                <?php if ($highfive->senderId == $highfive->sessionId): ?>
                                                <a style="margin-left: 10px;cursor: pointer;" onClick="deleteHiFivepost(<?php echo $highfive->hiFiveId; ?>)">
                                                    <i class="fa fa-trash-o" style="color: #afb4bd;font-size: 1rem;"></i>
                                                    Delete</a>
                                                <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $imageClass = "";?>
                    <?php endforeach;?>
                    <?php else: ?>
                    <p>You have not received a SZhoutOut yet.</p>
                    <?php endif;?>
                </div>
            <!-- </div> -->
            <div class="m-portlet__foot">
                <p>
                    <a href="<?php echo base_url('szfive/high_fives'); ?>" class="m--font-bold">View all SZhoutOuts</a>
                </p>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
</div>
<?php $this->load->view('templates/szfive/_includes/scripts');?>
<script>
    function noImageFound2(id) {
        $('.no-image2-' + id).attr('src', "<?php echo base_url('assets/images/img/sz.png'); ?>");
    }
</script>