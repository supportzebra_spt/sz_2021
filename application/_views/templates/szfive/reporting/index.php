<style>
    th {
        /* border-left: 1px solid #969696 !important; */
    }
    .ranges {
        margin: 0 !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'SZFive', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive'], ['title' => 'Reporting', 'link' => 'szfive/reporting#']]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content" id="szfive-home">
        <?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list); ?>
        <div class="m-portlet" id="survey_main_filter" style="margin-bottom: 15px;box-shadow: 0 1px 8px rgba(0,0,0,0.15);border-radius: 3px;border: 1px solid #ffffff;">
            <div class="m-portlet__body" style="padding: 10px;">
                <div class="row">
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <div class="m-select2 m-select2--pill" style="
                        text-align: center;">
                            <input type="hidden" name="stat" />
                            <select class="form-control m-select2" id="select_user_type" name="param" data-placeholder="Pill style">
                                <option></option>
                                <option value="all">All</option>
                                <option value="Admin">Admin</option>
                                <option value="Agent">Ambassador</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="m-select2 m-select2--pill">
                            <select class="form-control m-select2" id="select_team" multiple name="param">
                                <option></option>
                                <?php foreach ($accounts as $account) :?>
                                <option value="<?php echo $account->acc_id; ?>">
                                    <?php echo $account->acc_name; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div style=" width: 100%; ">
                            <span class="m-subheader__daterange" id="survey_datepicker">
                                <span class="m-subheader__daterange-label">
                                    <span class="daterange-title m-subheader__daterange-title-reviewers">This
                                        Week:</span>
                                    <span class="daterange-date m-subheader__daterange-date-reviewers m--font-brand">
                                        <?php echo $this_week; ?>
                                    </span>
                                </span>
                                <button class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill"
                                    style="width: 20px;height: 20px;">
                                    <i class="la la-angle-down" style="color: white;"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div style=" margin-right: 10px; ">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                id="dropdown_report1" data-dropdown-toggle="click" aria-expanded="false">
                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-secondary m-btn m-btn--icon m-btn--pill"
                                    id="downloadReport">
                                    <i class="flaticon-download"></i> Download
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div>
                    <table class="table table-bordered m-table">
                        <tbody>
                            <tr>
                                <th scope="row">Employee:</th>
                                <td id="info_employee_type"></td>
                            </tr>
                            <tr>
                                <th scope="row">Department/Account:</th>
                                <td id="info_dept_name"></td>
                            </tr>
                            <tr>
                                <th scope="row">Duration:</th>
                                <td id="info_duration"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped m-table table-hover" id="report_table" style="display:none;text-align: center;">
                        <thead style="background: #4a4a4a;color: white;">
                            <tr id="table_header">
                            </tr>
                        </thead>
                        <tbody id="table_body">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var startDate = moment().startOf('week');
    var endDate = moment().endOf('week');

    getCurrentDateTime(function (date) {
        startDate = moment(date, 'Y-MM-DD').startOf('week');
        endDate = moment(date, 'Y-MM-DD').endOf('week');
    });

    var DateRangePicker = function () {
        var main = function () {

            if ($('#survey_datepicker').length == 0) {
                return;
            }

            getCurrentDateTime(function (date) {
                var picker = $('#survey_datepicker');
                var start = moment(date, 'Y-MM-DD').startOf('week');
                var end = moment(date, 'Y-MM-DD').endOf('week');

                function cb(start, end, label) {

                    var title = '';
                    var range = '';

                    if (start.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').startOf('week').format(
                            'YYYY-MM-DD') && end.format('YYYY-MM-DD') == moment(date, 'Y-MM-DD').endOf(
                            'week').format('YYYY-MM-DD')) {
                        title = 'This Week:';
                        range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                    } else if (label == 'Today') {
                        title = 'Today:';
                        range = start.format('MMM D, Y');
                    } else if (label == 'Yesterday') {
                        title = 'Yesterday:';
                        range = start.format('MMM D, Y');
                    } else if (label == 'This Week') {
                        title = 'This Week:';
                        range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                    } else if (label == 'Last Week') {
                        title = 'Last Week:';
                        range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                    } else if (label == 'This Month') {
                        title = 'This Month:';
                        range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                    } else if (label == 'Last Month') {
                        title = 'Last Month:';
                        range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                    } else {
                        title = 'Custom:';
                        range = start.format('MMM D, Y') + ' - ' + end.format('MMM D, Y');
                    }

                    picker.find('.m-subheader__daterange-date-reviewers').html(range);
                    picker.find('.m-subheader__daterange-title-reviewers').html(title);

                    $('#info_duration').html('<mark>' + range + '</mark>');

                    startDate = start.format('YYYY-MM-DD');
                    endDate = end.format('YYYY-MM-DD');

                    var param = {
                        startDate: startDate,
                        endDate: endDate,
                        account_id: $('#select_team').select2("val"),
                        user_type: $('#select_user_type').val()
                    };

                    console.log(param);

                    getReport(param, function () {
                        mApp.unblockPage();
                    });
                }

                picker.daterangepicker({
                    startDate: start,
                    endDate: end,
                    opens: 'left',
                    ranges: {
                        'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                        'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date,
                            'Y-MM-DD').subtract(1, 'days')],
                        'This Week': [moment(date, 'Y-MM-DD').startOf('isoWeek'), moment(date,
                            'Y-MM-DD').endOf('isoWeek')],
                        'Last Week': [moment(date, 'Y-MM-DD').subtract(1, 'week').startOf(
                            'isoWeek'), moment(date, 'Y-MM-DD').subtract(1, 'week').endOf(
                            'isoWeek')],
                        'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date,
                            'Y-MM-DD').endOf('month')],
                        'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf(
                            'month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf(
                            'month')]
                    }
                }, cb);

                cb(start, end, '');
            });

        }
        return {
            init: function () {
                main();
            }
        };
    }();

    var Select2 = function () {
        var selectors = function () {
            $('#select_user_type').select2({
                placeholder: "Employee",
            });
            $('#select_team, #select_team_validate').select2({
                placeholder: "Filter by account...",
            });
        }

        return {
            init: function () {
                selectors();
            }
        };
    }();

    jQuery(document).ready(function () {
        DateRangePicker.init();
        Select2.init();

        $('#info_employee_type').html('<mark>Ambassadors & Admins</mark>');
        $('#info_dept_name').html('<mark>All</mark>');

        $('#downloadReport').on('click', function () {
            var userType = $('#select_user_type').val();
            if (userType == "") {
                userType = 'all';
            }
            window.location.href = baseUrl + "/szfive/download_summary_report/" + startDate + "/" +
                endDate + "/" + userType;
        });

        $('#select_user_type').on('change', function () {
            var title = $(this).select2("data")[0].text;
            if(title == 'All') {
                title = "Ambassadors & Admins";
            }
            $('#info_employee_type').html('<mark>' + title.charAt(0).toUpperCase() + title.substr(
                1) + '</mark>');

            var param = {
                user_type: $(this).val(),
                startDate: startDate,
                endDate: endDate,
                account_id: $('#select_team').select2("val"),
            };
            $.when(fetchPostData({
                    param
                }, '/szfive/get_accounts'))
                .then(function (data) {
                    data = jQuery.parseJSON(data);
                    $('#select_team').html('');

                    $.each(data, function (key, val) {
                        $('#select_team').last().append('<option value="' + val.acc_id +
                            '">' + val.acc_name +
                            '</option>');
                    });
                });
            getReport(param, function () {
                mApp.unblockPage();
            });
        });

        $("#select_team").on('key change', function () {
            var m_select = $(this).select2("data");
            var str_accounts = '';
            $.each(m_select, function (key, val) {
                message = val.text;
                message = message.replace(/ +(?= )/g, '');
                str_accounts += "<mark>" + message + "</mark> &nbsp;&nbsp;";
            });

            $('#info_dept_name').html(str_accounts);

            var param = {
                startDate: startDate,
                endDate: endDate,
                account_id: $(this).select2("val"),
                user_type: $('#select_user_type').val()
            };


            getReport(param, function () {
                mApp.unblockPage();
            });
        });
    });

    // function 
    function getReport(param, callback) {
        mApp.blockPage({
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            message: 'Please wait...'
        });

        $.when(fetchPostData({
                param
            }, '/szfive/get_summary_report'))
            .then(function (response) {
                response = jQuery.parseJSON(response);
                if (response.dates.length != 0) {
                    thead = '';
                    tbody = '';
                    $.each(response.dates, function (key, val) {
                        thead += '<th>' + val + '</th>';
                    });
                    $.each(response.data, function (key, val) {
                        tbody += '<tr><td scope="row">' + val.title + '</td>';
                        $.each(val.data, function (key, val2) {
                            tbody += '<td>' + val2 + '</td>';
                        });
                        tbody += '</tr>';
                    });

                    $('#table_header').html(thead);
                    $('#table_body').html(tbody);
                    $('#report_table').show(500);
                } else {
                    $('#report_table').hide(500);
                }
                callback();
            });
    }
</script>