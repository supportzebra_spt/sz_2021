<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'High Fives', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive'], ['title' => 'High Fives', 'link' => 'szfive/high_fives'], ['title' => 'Feeds', 'link' => uri_string()]]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content survey-answers" id="highfive-page">
        <?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list);?>
        <div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true">
            <div class="m-portlet__body">
                <div class="m-section__content">
                    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                        <div class="m-demo__preview">
                            <h3>You cannot access this post because of several reasons:</h3>
                            <h6 class="text-muted">* This content is private.</h6>
                            <h6 class="text-muted">* This content is not sent/intended to you.</h6>
                            <h6 class="text-muted">* This content is no longer available.</h6><br>
                            <h7 class="text-muted">Go back to <a href="<?php echo base_url('szfive/high_fives'); ?>">High
                                    Fives</a> page.</h7>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>