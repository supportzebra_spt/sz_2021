<link href="<?php echo base_url('assets/src/custom/css/jquery.mentionsInput.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url('assets/src/custom/js/underscore.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/src/custom/js/jquery.elastic.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/src/custom/js/jquery.mentionsInput.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/src/custom/js/jquery.events.input.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/src/custom/js/jquery.livequery.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/src/custom/js/jquery.tooltipsterReaction.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/src/custom/js/jquery.tipsy.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/src/custom/js/szfive.general.js'); ?>" type="text/javascript"></script>