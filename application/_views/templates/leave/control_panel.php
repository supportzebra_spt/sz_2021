<style type="text/css">
    body{
        padding-right:0 !important;
    }
    .no-padding{
        padding:0 !important;
    }
    .padding-30A{
        padding:30px !important;
    }
    .portlet-header-report{
        background:#4B4C54 !important;
        color:white  !important;
        height:50px !important;
    }
    #highchart-daterange input{
        color:white !important;
    }
    #highchart-chart{
        height:400px !important;
    }
    .highcharts-credits,.highcharts-contextmenu,.highcharts-exporting-group{
        display:none;
    }

    .m-demo__preview{
        border-color:#eee !important;
    }
    .m-content .m-stack__item{
        padding:0 !important;
        background:0 !important;
        border:0 !important;
    }
    .table thead th{
        padding:  10px 4px
    }
    .highcharts-drilldown-axis-label,.highcharts-xaxis-labels{
        pointer-events: none !important;
        cursor: default !important;
        text-decoration: none !important;
        color: #000 !important;
    }
    .custom-view-date,.custom-view-employee{
        pointer-events: auto !important;
        cursor: pointer !important;
        color:#716aca !important;
    }
    label{
        font-size:12px;
    }
    .dashboard-button{
        color:white !important;
        padding-top:10px;
        padding-right:10px;
        padding-left:15px;
        padding-bottom:0px;
        border-radius:5px;
        border:2px solid white;
    }
    .m-content ul.m-subheader__breadcrumbs li,.m-content ul.m-subheader__breadcrumbs li a{
        padding:0 !important;
    }
    .m-content ul.m-subheader__breadcrumbs li a span:hover{
        background: #ddd !important;         
        color:black !important; 
    } 

    .m-content ul.m-subheader__breadcrumbs li a span{
        color:#fff !important;  
        padding:10px 20px !important; 
    } 
    .m-content ul.m-subheader__breadcrumbs{
        background:#4B4C54 !important; 
        padding:2px;
    }
    .my-nav-active{
        background: #eee !important;    
    } 
    li.my-nav-active a span{      
        color:black; 
    }
    .m-content hr{
        border:2.5px solid #eee !important;
    }
    .m-demo__preview{
        border-color:#fbfbfb !important;
    }

    .light-theme span,.light-theme a{
        line-height:20px !important;
        padding:0 5px !important;
        font-size:12px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Summary</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-md-9">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline" style="box-shadow:0px 1px 15px 1px rgba(69, 65, 78, 0.08);height:auto">
                    <li class="m-nav__item my-nav-active">
                        <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text" style="color:#555 !important"><i class="fa fa-list m--font-primary"></i> Summary</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/monitoring'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-dashboard m--font-info"></i> Monitoring</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/types'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-table m--font-success"></i> Types</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/rules'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-asterisk m--font-brand"></i> Rules</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/users'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-users m--font-danger"></i> Users</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/credits'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-ticket m--font-warning"></i> Credits</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 text-right">
                <button class="m-portlet__nav-link btn btn-danger m-btn--air m-btn--icon" style="padding: 13px;" id="btn-filters" data-stat="close"> <i class="la la-angle-down" style="font-size:13px;margin-right:5px"></i> Filters </button>
                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right" data-dropdown-toggle="click" data-dropdown-persistent="true">
                    <a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle"  style="padding: 13px;">
                        Reports
                    </a>
                    <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">              
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__section m-nav__section--first">
                                            <span class="m-nav__section-text">Reports</span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="<?php echo base_url('Leave/report_summary')?>" class="m-nav__link">
                                                <i class="m-nav__link-icon la  la-pie-chart"></i>
                                                <span class="m-nav__link-text">LOA Summary</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="<?php echo base_url('Leave/report_raw')?>" class="m-nav__link">
                                                <i class="m-nav__link-icon  la la-bar-chart"></i>
                                                <span class="m-nav__link-text">LOA Raw</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="m-portlet"  id="filters" style="display:none" data-stat="close">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-filter"></i>
                                </span>
                                <h4 class="m-portlet__head-text" style="color:white !important;font-size:16px">
                                    Filters
                                </h4>
                            </div>			
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="">Year:</label>
                                <div class="form-group m-form__group">
                                    <select class="form-control m-input m-input--square" id="search-year">

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="">Class:</label>
                                <select class="form-control m-input m-input--square" id="search-class">
                                    <option value="">All</option>
                                    <option value="Admin">SZ Team</option>
                                    <option value="Agent">Ambassador</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Paid:</label>
                                <select class="form-control m-input m-input--square" id="search-paid">
                                    <option value="">All</option>
                                    <option value="1">Paid</option>
                                    <option value="0">Unpaid</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="">Date Type:</label>
                                <select class="form-control m-input m-input--square" id="search-minutes">
                                    <option value="">All</option>
                                    <option value="480">Full Day</option>
                                    <option value="240">Half Day</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Released:</label>
                                <select class="form-control m-input m-input--square" id="search-released">
                                    <option value="">All</option>
                                    <option value="1">Released</option>
                                    <option value="0">Unreleased</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Department:</label>
                                <select class="form-control m-input m-input--square" id="search-department">
                                    <option value="">All</option>
                                    <?php foreach ($departments as $dep): ?>
                                        <option value="<?php echo $dep->dep_id; ?>"><?php echo $dep->dep_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>


                            <div class="col-md-6">
                                <label for="">Accounts:</label>
                                <select class="form-control m-input m-input--square" id="search-account" disabled>
                                    <option value="">All</option>
                                    <?php foreach ($accounts as $acc): ?>
                                        <option value="<?php echo $acc->acc_id; ?>"><?php echo $acc->acc_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>	
                <!--end::Portlet-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">	
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-bar-chart-o"></i>
                                </span>
                                <h4 class="m-portlet__head-text" style="color:white !important;font-size:16px ">
                                    <strong id="chart-title"></strong> Leave Chart 
                                </h4>
                            </div>			
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#"  id="export" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-success m-btn--icon btn-sm">
                                        <i class="fa fa-download"></i> Export
                                    </a>
                                </li>		
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="highchart-columnchart" style="height:420px"></div>
                            </div>
                        </div>
                    </div>
                </div>	
                <!--end::Portlet-->

            </div>
        </div>


    </div>
    <div class="modal fade" id="modal-report-leave-type" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title-status">Leave Report Per Day</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-widget3" id="widget-employees">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">
        var global_categories = [];
        var global_leave_types = [];
        var leaveTypeToDisplay = null;
        var colors = [
            " #F68C4E ",
            " #B869AA ",
            " #2977B7 ",
            " #29B8A6 ",
            " #F6EF3F ",
            " #F9A54D ",
            " #EF4A51 ",
            " #896AAE ",
            " #32A6D9 ",
            " #ADD038 ",
            " #FECD40 ",
            " #E668A7 "
        ];
        var getDaysArrayByMonth = function (date) {
            var daysInMonth = moment(date).daysInMonth();
            var arrDays = [];

            while (daysInMonth) {
                var current = moment(date).date(daysInMonth);
                arrDays.push(current.format('YYYY-MM-DD'));
                daysInMonth--;
            }
            return arrDays.reverse();
        };
        $(function () {

            var startyear = 2017;
            var endyear = moment().add(1, 'year').year();
            var years = '';
            for (var x = startyear; x <= endyear; x++) {
                if (moment().year() === x) {
                    years += '<option value="' + x + '" selected>' + x + '</option>';
                } else {
                    years += '<option value="' + x + '">' + x + '</option>';
                }
            }
            $("#search-year").html(years);
            $("#chart-title").html(moment().year());
            $("#search-department").change(function () {
                var dep_id = $(this).val();
                if (dep_id !== '') {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Leave/get_accounts_per_department",
                        data: {
                            dep_id: dep_id
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            res = JSON.parse(res);
                            var string = '<option value="" selected>All</option>';
                            $.each(res, function (i, item) {
                                string += '<option value="' + item.acc_id + '">' + item.acc_name + '</option>';
                            });
                            $("#search-account").html(string);
                        }
                    });
                    $("#search-account").prop('disabled', false);
                } else {
                    $("#search-account").html('<option value="" selected>All</option>');
                    $("#search-account").prop('disabled', true);
                }

            });
            $("#search-year,#search-class,#search-custom,#search-department,#search-account,#search-paid,#search-released,#search-minutes").change(function () {
                var categories2 = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
//                var categories2 = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                var year = $("#search-year").val();
                var clas = $("#search-class").val();
                var ispaid = $("#search-paid").val();
                var minutes = $("#search-minutes").val();
                var released = $("#search-released").val();
                var department = $("#search-department").val();
                var account = $("#search-account").val();
                var arr = [];

                $("#chart-title").html(year);
                for (var x = 1; x <= 12; x++) {
                    var date = moment(year + '-' + ('0' + x).slice(-2) + '-01', 'YYYY-MM-DD');
                    arr.push({startDate: date.startOf('month').format('YYYY-MM-DD'), endDate: date.endOf('month').format('YYYY-MM-DD')});
                }
                $("#export").click(function () {
                    window.location = "<?php echo base_url(); ?>/Leave/export_report_leave_type/" + year + "/" + clas + "/" + department + "/" + account;
                });
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/Leave/get_report_per_leave_type",
                    data: {
                        dateArray: arr,
                        class: clas,
                        ispaid: ispaid,
                        released: released,
                        minutes: minutes,
                        department: department,
                        account: account
                    },
                    beforeSend: function () {
                        mApp.block('body', {
                            overlayColor: '#000000',
                            type: 'loader',
                            state: 'brand',
                            size: 'lg'
                        });
                    },
                    cache: false,
                    success: function (res) {
                        res = res.trim();
                        res = JSON.parse(res);
                        var currentdata = [];
                        var iterator = 0;
                        var colorcount = colors.length;
                        var colorcnt = 0;
                        $.each(res.leave_types, function (x, obj) {
                            //HIGHCHARTS__________________________________________
                            var counts = obj.counts;
                            if (colorcnt === colorcount) {
                                colorcnt = 0;
                            }
                            var curcolor = colorcnt;

                            var series = [];
                            $.each(counts, function (i, y) {
                                series.push({
                                    name: categories2[i],
                                    y: parseFloat(y),
                                    drilldown: true,
                                    color: colors[curcolor]
                                });
                            });
                            currentdata[iterator] = {
                                name: obj.leaveType,
                                data: series
                            };
                            colorcnt++;
                            iterator++;
                            //END OF HIGHCHARTS__________________________________________
                        });
                        global_categories = categories2;
                        global_leave_types = res.leave_types;
                        // Build the chart 
                        Highcharts.chart('highchart-columnchart', {
                            chart: {
                                type: 'column',
                                events: {
                                    drilldown: function (e) {
                                        if (!e.seriesOptions) {
                                            var chart = this;
                                            var mydate = moment(year + "-" + e.point.name + "-01", "YYYY-MMM-DD").format("YYYY-MM-DD");
                                            var dateArray = getDaysArrayByMonth(mydate);
                                            var perDate = [];
                                            dateArray.forEach(function (item) {
                                                perDate.push(moment(item, 'YYYY-MM-DD').format('MMM DD'));
                                            });
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url(); ?>index.php/Leave/get_report_leave_type_per_date",
                                                beforeSend: function () {
                                                    chart.showLoading('Loading data ...');
                                                },
                                                data: {
                                                    dateArray: dateArray,
                                                    leaveType: e.point.series.name,
                                                    class: clas,
                                                    ispaid: ispaid,
                                                    released: released,
                                                    minutes: minutes,
                                                    department: department,
                                                    account: account
                                                },
                                                cache: false,
                                                success: function (res) {
                                                    res = res.trim();
                                                    res = JSON.parse(res);
                                                    var details = [];
                                                    var iterator = 0;
                                                    $.each(res.details, function (x, count) {
                                                        var datename = perDate[x];
                                                        details[iterator] = [datename, parseFloat(count)];
                                                        iterator++;
                                                    });
                                                    var arr = {
                                                        name: e.point.name,
                                                        data: details
                                                    };
                                                    chart.addSeriesAsDrilldown(e.point, arr);
                                                    chart.hideLoading();
//                                                    
                                                }
                                            });
                                        }

                                    }
                                }
                            },
                            title: null,
                            xAxis: {
                                type: 'category'
                            },
                            colors: colors,
                            yAxis: {
                                min: 0,
                                title: null,
                                stackLabels: {
                                    enabled: true,
                                    style: {
                                        fontWeight: 'bold',
                                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                    }
                                }
                            },
                            legend: {
                                enabled: true
                            },

                            plotOptions: {
                                series: {
                                    stacking: 'normal',
                                    cursor: 'pointer',
                                    borderWidth: 0,
                                    point: {
                                        events: {
                                            click: function () {
                                                var yearmonthdate = moment(this.name + " " + year, 'MMM DD YYYY', true); //for month
                                                if (yearmonthdate.isValid()) {
//                                                    FOR DRILLDOWN SERIES
                                                    var leaveType = this.series.chart.drilldownLevels[0].series.name;
                                                    var date = yearmonthdate.format('YYYY-MM-DD');
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo base_url(); ?>index.php/Leave/get_report_leave_type_per_employee",
                                                        data: {
                                                            leaveType: leaveType,
                                                            date: date,
                                                            class: clas,
                                                            ispaid: ispaid,
                                                            released: released,
                                                            minutes: minutes,
                                                            department: department,
                                                            account: account
                                                        },
                                                        beforeSend: function () {
                                                            mApp.block('body', {
                                                                overlayColor: '#000000',
                                                                type: 'loader',
                                                                state: 'brand',
                                                                size: 'lg'
                                                            });
                                                        },
                                                        cache: false,
                                                        success: function (res) {
                                                            res = res.trim();
                                                            res = JSON.parse(res);
                                                            var string = "";
                                                            $.each(res.names, function (x, name) {
                                                                string += '<div class="m-widget3__item">';
                                                                string += '<div class="m-widget3__header">';
                                                                string += '<div class="m-widget3__user-img">';
                                                                string += '<img class="m-widget3__img"  src="<?php echo base_url(); ?>' + name.pic + '" style="margin-top: -30px;" alt="">  ';
                                                                string += '</div>';
                                                                string += '<div class="m-widget3__info">';
                                                                string += '<span class="m-widget3__username">';
                                                                string += '' + name.fullname + '';
                                                                string += '</span><br> ';
                                                                string += '<span class="m-widget3__time">';
                                                                string += '' + name.pos_details + '';
                                                                string += '</span>		 ';
                                                                string += '</div>';
                                                                string += '<span class="m-widget3__status">';
                                                                var minutes = (name.minutes === "480") ? "Full" : "Half";
                                                                string += '<span class="m--font-info">' + minutes + '</span> <small>Day</small>';
                                                                string += '</span>	';
                                                                string += '</div>';
                                                                string += '</div>';
                                                            });
                                                            $("#widget-employees").html(string);
                                                            $("#employee-title").html(leaveType + " Leave<br /><small>" + moment(date, "YYYY-MM-DD").format("MMMM DD, YYYY") + "</small>");
                                                            mApp.unblock('body');
                                                            $(".m-widget3__img").on('error', function () {
                                                                $(this).attr('onerror', null);
                                                                $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                                                            });
                                                        }
                                                    });

                                                    $("#btn-modal-back").hide();
                                                    $("#modal-per-employee").show();
                                                    $("#modal-per-date").hide();
                                                    $("#modal-report-leave-type").modal("show");

                                                } else {
                                                    //FOR MAIN SERIES
                                                }
                                            }
                                        }
                                    }
                                }
                            },

                            series: currentdata,

                            drilldown: {
                                series: []
                            }
                        });
                        mApp.unblock('body');
                    }

                });
            });
            $("#search-year").change();
            $("#div-accordion-tabular").on("click", ".custom-view-date", function () {

                var that = this;
                var clas = $("#search-class").val();
                var ispaid = $("#search-paid").val();
                var minutes = $("#search-minutes").val();
                var released = $("#search-released").val();
                var department = $("#search-department").val();
                var account = $("#search-account").val();
                var leaveType = $(this).data('leavetype');
                var date = $(this).data('date');
                var dateArray = getDaysArrayByMonth(date);
                var perDate = [];
                dateArray.forEach(function (item) {
                    perDate.push(moment(item, 'YYYY-MM-DD').format('MMMM DD, YYYY'));
                });
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/Leave/get_report_leave_type_per_date",
                    beforeSend: function () {
                        mApp.block('body', {
                            overlayColor: '#000000',
                            type: 'loader',
                            state: 'brand',
                            size: 'lg'
                        });
                    },
                    data: {
                        dateArray: dateArray,
                        leaveType: leaveType,
                        class: clas,
                        ispaid: ispaid,
                        released: released,
                        minutes: minutes,
                        department: department,
                        account: account
                    },
                    cache: false,
                    success: function (res) {
                        res = res.trim();
                        res = JSON.parse(res);
                        var string = "";
                        $.each(res.details, function (x, count) {
                            string += '<tr>';
                            string += '<td>' + perDate[x] + '</td>';
                            string += '<td>' + count + '</td>';
                            if (count === '0') {
                                string += '<td>--</td>';

                            } else {
                                string += '<td><a data-date="' + dateArray[x] + '" data-leavetype="' + leaveType + '" class="m-link custom-view-employee"><i class="fa fa-users"></i> Employees</a></td>';

                            }
                            string += '</tr>';
                        });
                        $("#modal-per-date table tbody").html(string);
                        $("#date-title").html(leaveType + " Leave<small class='pull-right'>" + moment(date, "YYYY-MM-DD").format("MMMM YYYY") + "</small>");
                        mApp.unblock('body');
                    }
                });
                $("#modal-per-employee").hide();
                $("#modal-per-date").show();
                $("#modal-report-leave-type").modal("show");

            });
            $("#modal-report-leave-type").on('show.bs.modal', function (e) {

                var that = this;
                $(that).find('.modal-body').animate({
                    scrollTop: 0
                }, 500);
                //---------------------------------------------------------------------------------------------------------------------------
                $(that).on("click", ".custom-view-employee", function () {

                    var clas = $("#search-class").val();
                    var ispaid = $("#search-paid").val();
                    var minutes = $("#search-minutes").val();
                    var released = $("#search-released").val();
                    var department = $("#search-department").val();
                    var account = $("#search-account").val();
                    var leaveType = $(this).data('leavetype');
                    var date = $(this).data('date');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Leave/get_report_leave_type_per_employee",
                        data: {
                            leaveType: leaveType,
                            date: date,
                            class: clas,
                            ispaid: ispaid,
                            released: released,
                            minutes: minutes,
                            department: department,
                            account: account
                        },
                        beforeSend: function () {
                            mApp.block('body', {
                                overlayColor: '#000000',
                                type: 'loader',
                                state: 'brand',
                                size: 'lg'
                            });
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            res = JSON.parse(res);
                            var string = "";
                            $.each(res.names, function (x, name) {
                                string += '<li class="list-group-item">' + name.fullname + '</li>';
                            });
                            $("#modal-per-employee ul").html(string);
                            $("#employee-title").html(leaveType + " Leave<br /><small>" + moment(date, "YYYY-MM-DD").format("MMMM DD, YYYY") + "</small>");
                            mApp.unblock('body');
                        }
                    });

                    $("#btn-modal-back").show();
                    $("#modal-per-employee").show(500);
                    $("#modal-per-date").hide();

                });
            });

            $("#btn-modal-back").click(function () {
                $("#modal-per-employee").hide();
                $("#modal-per-date").show(500);
            });
            $("#btn-filters").click(function () {
                if ($(this).data('stat') === 'open') {
                    $("#filters").hide(700);
                    $(this).data('stat', 'close');
                } else {
                    $("#filters").show(700);
                    $(this).data('stat', 'open');
                }

                $('i', this).toggleClass('la-angle-down la-angle-up');
            });

        });
</script>