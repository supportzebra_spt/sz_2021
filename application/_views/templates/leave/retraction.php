<style type="text/css">  
    body{
        padding-right:0 !important;
    }
    .bg-custom-gray{
        background:#4B4C54 !important;
    }
    .font-white{
        color:white !important;
    }
    .font-poppins{
        font-family:'Poppins' !important;
    }
    .m-accordion__item{
        border:1.2px solid #545454 !important;
    }
    .m-accordion__item-mode{
        margin-left:5px;
        font-size:15px !important; 
    }
    .m-accordion__item-icon i{
        font-size:18px !important;  
    }
    .m-accordion__item-title{
        font-size:15px !important;  
    }
    .m-accordion__item-head{
        background:#545454 !important;  
        color:white !important;  
    }
    .m-accordion__item-head:hover{
        background:#777 !important;  
    }
    textarea {
        resize: none;
    }
    @media screen and (max-width: 500px) {
        .button-text {
            display: none !important;
        }

    }
    .m-demo__preview{
        border-color: #eee !important;
    }
    .modal .m-demo .m-demo__preview{
        border:2px solid #999 !important;
        border-radius: 2px !important;
    }
    .m-table thead th{
        background:#545454;
        color:white;
    }
    .m-table th{
        border: 1px solid #545454 !important;
        text-align:center;
    }
    .m-table tr td{
        border: 1px solid #9B9C9E !important;
        text-align:center;
    }
    .m-demo__preview{
        border-color:#eee !important;
    }
    div{
        word-wrap: break-word;
    }
    .m-content hr{
        border:3px solid #eee !important;
    }

    .light-theme a, .light-theme span{
        padding:1.5px 8px !important;
    }
    .m-content .m-stack__item{
        padding:0 !important;
        background:0 !important;
        border:0 !important;
    }
    .m-badge{
        font-weight:550;
    }

    /*DATERANGEPICKER*/
    .daterangepicker {
        font-size:13px !important;
    }
    .daterangepicker table td{
        width:28px !important;
        height:28px !important;
    }
    .daterangepicker_input input{
        padding-top:5px !important;
        padding-bottom:5px !important;
        line-height:1 !important;
    }
    .blockOverlay{
        opacity:0.15 !important;
        background-color:#746DCB !important;
    }
</style>


<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>         
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/request'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">My Request</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/retraction'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Retraction</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="alert alert-brand alert-dismissible fade show m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
            <strong>Please read!</strong> If you want to reschedule your leave date, you have to retract it first then file a leave request again.                      
        </div>
        <div class="row">

            <div class="col-lg-12"> 
                <!--begin::Portlet-->
                <div class="m-portlet"  id="m_blockui_2_portlet">
                    <div class="m-portlet__head bg-custom-gray" >
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins">
                                    <span class="font-white"> Leave Retraction</span> <small class="font-white"> Retraction of leave request dates</small>
                                </h3>
                            </div>          
                        </div>
                        <div class="m-portlet__head-tools">
                            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                <button type="button" class="m-btn btn"  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#fileRetractionModal" title="File a leave"><i class="fa fa-calendar-plus-o" style="margin-bottom:3px"></i> <span class="button-text">File Retraction</span></button>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-ongoing" role="tab"><i class="la la-tasks"></i> Retraction</a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-history" role="tab"><i class="la la-history"></i>Retraction History</a>
                            </li>
                        </ul>                        
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-ongoing" role="tabpanel">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h5>
                                            Retraction <br />
                                            <small class="text-muted">Requests for retraction and list of pending retraction requests.</small>
                                        </h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-ongoing" role="tablist">                      

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle" >
                                                <ul id="retraction-ongoing-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="retraction-ongoing-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="retraction-ongoing-count"></span> of <span id="retraction-ongoing-total"></span> records</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-history" role="tabpanel">

                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                        <h5>
                                            Retraction History  <br />
                                            <small class="text-muted">Previous records of retractions.</small>
                                        </h5>
                                    </div>
                                    <div class="col-lg-9 col-md-9">
                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">
                                                <div class="row">

                                                    <div class="col-md-5">
                                                        <div class="m-input-icon m-input-icon--left" id="retraction-history-daterange">
                                                            <input type="text" class="form-control m-input" placeholder="Date Range" readonly>
                                                            <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="input-group m-form__group">
                                                            <input type="text" class="form-control m-input m-input--air" placeholder="Search Reason..." id="search-custom-history-retraction">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-secondary" type="button" id="btn-search-history-retraction"><i class="la la-search m--font-brand"></i></button>
                                                            </div>                  
                                                        </div>
                                                    </div>
                                                </div>  

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-history" role="tablist">                      

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle" >
                                                <ul id="retraction-history-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="retraction-history-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="retraction-history-count"></span> of <span id="retraction-history-total"></span> records</div>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>  

                </div>  
                <!--end::Portlet-->
                <br />
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="fileRetractionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request Retraction</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:450px;overflow:auto">

                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                    <div class="m-demo__preview" style="padding:10px">
                        <p>Reason for retraction:</p>
                        <textarea name="" id="textarea-reason" maxlength="255" class="form-control m-input" rows="3" required></textarea>
                        <span class="m-form__help"><small>(You have <i class="m--font-danger" id="textarea-note">255</i> remaining characters)</small></span>
                    </div>
                </div>
                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false" style="margin-bottom:0">
                    <div class="m-demo__preview" style="padding:10px">

                        <p>Upcoming Leave Dates:  
                            <span class="pull-right">
                                <button id="btn-select" type="button" title="Select All" class="m-btn btn btn-sm btn-secondary"><i class="fa fa-check m--font-success"></i> <span class="button-text">Select All</span></button>
                                <button id="btn-clear" type="button" title="Clear All" class="m-btn btn btn-sm btn-secondary"><i class="fa fa-times m--font-danger"></i> <span class="button-text">Clear All</span></button>

                            </span> 
                        </p>
                        <div style="max-height:250px;overflow:auto;margin-top:20px">
                            <table class="table table-striped m-table table-sm" id="table-retract">
                                <thead>
                                    <tr>
                                        <th>Leave Dates</th>
                                        <th>Type</th>
                                        <th>Retract?</th>
                                    </tr>
                                </thead>
                                <tbody id="availableLeaveDates"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-outline-brand" id="btn-fileRetraction">Submit Retraction</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approvalDetailsModal-retraction" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Approval Process</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"  style="max-height:450px;overflow:auto;padding-top:0 !important;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-widget7">
                            <div class="m-widget7__user" style='margin-bottom:20px'>
                                <div class="m-widget7__user-img">                            
                                    <img class="m-widget7__img" id="view-pic-retraction" alt="" style="width:50px">  
                                </div>
                                <div class="m-widget7__info">
                                    <span class="m-widget7__username" id="view-name-retraction" style="font-size:20px">
                                    </span><br> 
                                    <span class="m-widget7__time m--font-brand" id="view-job-retraction" style="font-size:13px">
                                    </span>      
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>
                    <div class="col-lg-12">
                        <table class="table m-table table-bordered">

                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Reason:
                                <td  id="view-reason-retraction" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Date Filed:</td>
                                <td  id="view-requestdate-retraction" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                        </table>
                        <hr />
                    </div>
                    <div class="col-lg-12">

                        <p class="m--font-bolder">REQUEST APPROVALS</p>
                        <div   id="div-viewApproval-retraction">
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">
        var statusClass = ["", "", "warning", "", "info", "success", "danger", "focus", "", "", "", "", "metal", "primary"];
        function deleteRequestDetail(retractLeaveDetails_ID, that) {

            swal({
                title: 'Are you sure?',
                text: "Date will be removed!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, remove it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Leave/delete_retraction_detail",
                        data: {
                            retractLeaveDetails_ID: retractLeaveDetails_ID
                        },
                        cache: false,
                        success: function (res) {
                            res = JSON.parse(res.trim());
                            //                            console.log(res);
                            if (res.status === 'All') {
                                swal("Success!", "Retraction has been deleted!", "success");
                                systemNotification(res.notif_details[0].notif_id);
                                getOngoingRetraction(0, $("#retraction-history-perpage").val());
                            } else if (res.status === 'Success') {
                                swal("Success!", "Retraction date has deleted!", "success");
                                systemNotification(res.notif_details[0].notif_id);
                                $(that).collapse("hide");
                                $(that).collapse("show");
                            } else {
                                swal("Failed!", "Something happened while deleting leave retraction details!", "error");
                                getOngoingRetraction(0, $("#retraction-history-perpage").val());
                            }

                        }
                    });
                }
            });

        }
        function getOngoingRetraction(limiter, perPage, callback) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_ongoing_retraction_list",
                data: {
                    limiter: limiter,
                    perPage: perPage
                },
                beforeSend: function () {
                    mApp.block('#m_blockui_2_portlet', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    $("#retraction-ongoing-total").html(res.total);
                    $("#retraction-ongoing-total").data('count', res.total);

                    var string = "";
                    $.each(res.data, function (i, item) {
                        string += '<div class="m-accordion__item">';
                        string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-' + item.retractLeave_ID + '" aria-expanded="  false">';
                        string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                        string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9">' + item.reason + '</div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div> </div></span>';
                        string += '<span class="m-accordion__item-mode"></span>';
                        string += '</div>';
                        string += '<div class="m-accordion__item-body collapse" data-type="ongoing" data-retractleaveid="' + item.retractLeave_ID + '" id="accordion-item-' + item.retractLeave_ID + '" role="tabpanel" data-parent="#div-accordion-ongoing"> ';
                        string += '<div class="m-accordion__item-content" >';
                        string += '</div></div></div>';
                    });

                    if (string === '') {
                        string = '<div style="margin-top:15px" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record!</strong> You do not have ongoing leave request. </div>';
                    }
                    $("#div-accordion-ongoing").html(string);
                    var count = $("#retraction-ongoing-total").data('count');
                    var result = parseInt(limiter) + parseInt(perPage);
                    if (count === 0) {
                        $("#retraction-ongoing-count").html(0 + ' - ' + 0);
                    } else if (count <= result) {
                        $("#retraction-ongoing-count").html((limiter + 1) + ' - ' + count);
                    } else if (limiter === 0) {
                        $("#retraction-ongoing-count").html(1 + ' - ' + perPage);
                    } else {
                        $("#retraction-ongoing-count").html((limiter + 1) + ' - ' + result);
                    }
                    mApp.unblock('#m_blockui_2_portlet');
                    callback(res.total);
                }, error: function () {
                    swal("Error", "Please contact admin", "error");
                    mApp.unblock('#m_blockui_2_portlet');
                }
            });
        }
        function getHistoryRetraction(limiter, perPage, callback) {
            var searchcustom = $("#search-custom-history-retraction").val();
            var datestart = $("#retraction-history-daterange").data('daterangepicker').startDate._d;
            var dateend = $("#retraction-history-daterange").data('daterangepicker').endDate._d;
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_other_retraction_list",
                data: {
                    limiter: limiter,
                    perPage: perPage,
                    searchcustom: searchcustom,
                    datestart: moment(datestart).format("YYYY-MM-DD"),
                    dateend: moment(dateend).add(1, 'day').format("YYYY-MM-DD")
                },
                beforeSend: function () {
                    mApp.block('#m_blockui_2_portlet', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    $("#retraction-history-total").html(res.total);
                    $("#retraction-history-total").data('count', res.total);

                    var string = "";
                    $.each(res.data, function (i, item) {
                        string += '<div class="m-accordion__item">';
                        string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-' + item.retractLeave_ID + '" aria-expanded="  false">';
                        string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                        string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9">' + item.reason + '</div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div> </div></span>';
                        string += '<span class="m-accordion__item-mode"></span>';
                        string += '</div>';
                        string += '<div class="m-accordion__item-body collapse" data-type="history" data-retractleaveid="' + item.retractLeave_ID + '" id="accordion-item-' + item.retractLeave_ID + '" role="tabpanel" data-parent="#div-accordion-history"> ';
                        string += '<div class="m-accordion__item-content" >';
                        string += '</div></div></div>';
                    });
                    if (string === '') {
                        string = '<div style="margin-top:15px" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record!</strong> You do not have leave request history yet. </div>';
                    }
                    $("#div-accordion-history").html(string);
                    var count = $("#retraction-history-total").data('count');
                    var result = parseInt(limiter) + parseInt(perPage);
                    if (count === 0) {
                        $("#retraction-history-count").html(0 + ' - ' + 0);
                    } else if (count <= result) {
                        $("#retraction-history-count").html((limiter + 1) + ' - ' + count);
                    } else if (limiter === 0) {
                        $("#retraction-history-count").html(1 + ' - ' + perPage);
                    } else {
                        $("#retraction-history-count").html((limiter + 1) + ' - ' + result);
                    }
                    mApp.unblock('#m_blockui_2_portlet');
                    callback(res.total);
                }, error: function () {
                    swal("Error", "Please contact admin", "error");
                    mApp.unblock('#m_blockui_2_portlet');
                }
            });
        }
        $(function () {
            $("#fileRetractionModal").on('show.bs.modal', function () {
                var that = this;
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Leave/get_available_retraction_dates",
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            $("#availableLeaveDates").html("");
                            var string = "";
                            $.each(res.available_leavedates, function (i, item) {
                                string += '<tr>';
                                string += '<td>' + moment(item.date, 'YYYY-MM-DD').format('MMM DD, YYYY') + '</td>';
                                string += '<td>' + item.leave_name + '</td>';
                                string += '<td><label class="m-checkbox checkbox-retract" style="margin-bottom:13px" ><input type="checkbox" data-allowretraction="' + item.allowRetraction + '" data-requestleaveid="' + item.requestLeave_ID + '" data-requestleavedetailsid="' + item.requestLeaveDetails_ID + '"><span></span></label></td>';
                                string += '</tr>';
                            });

                            $("#availableLeaveDates").html(string);
                            $("#btn-select").click(function () {
                                $(that).find("[type=checkbox]").each(function (key, elem) {
                                    $(elem).prop('checked', true);
                                });
                            });
                            $("#btn-clear").click(function () {
                                $(that).find("[type=checkbox]").each(function (key, elem) {
                                    $(elem).prop('checked', false);
                                });
                            });

                            $("#btn-fileRetraction").prop('disabled', false);
                        } else {

                            $("#availableLeaveDates").html("<tr><td colspan=3>No Retractable Dates</td></tr>");
                            $("#btn-fileRetraction").prop('disabled', true);
                        }

                    }
                });
            });
            $("#fileRetractionModal").on('change', "[type=checkbox]", function () {
                var requestLeave_ID = $(this).data('requestleaveid');
                var allowRetraction = $(this).data('allowretraction');
                var stat = $(this).prop('checked');
                if (allowRetraction === 'group') {
                    $("#fileRetractionModal").find("[type=checkbox][data-requestleaveid=" + requestLeave_ID + "]").each(function (key, elem) {
                        if (stat === true) {
                            $(elem).prop('checked', true);
                        } else {
                            $(elem).prop('checked', false);
                        }
                    });
                }
            });
            $("#btn-fileRetraction").click(function () {
                var reason = $("#textarea-reason").val();
                if ($.trim(reason) === '') {
                    swal("Incomplete Fields", "Reason is empty", "warning");
                } else {
                    var $checkboxes = $("#fileRetractionModal").find("[type=checkbox]");
                    var checkLength = $checkboxes.length;
                    var checkedCnt = 0;
                    var uncheckedCnt = 0;
                    var data = [];
                    $checkboxes.each(function (key, elem) {
                        var requestLeaveDetails_ID = $(elem).data('requestleavedetailsid');

                        if ($(elem).prop('checked')) {
                            data.push({
                                requestLeaveDetails_ID: requestLeaveDetails_ID
                            });
                            checkedCnt++;
                        } else {
                            uncheckedCnt++;
                        }
                    });
                    if (checkLength === uncheckedCnt) {
                        swal("Incomplete Fields", "No leave date/s selected", "warning");
                    } else {
                        if (checkLength === checkedCnt) {
                            var text = "All leave dates will be retracted";
                        } else {
                            var text = "Chosen dates will be retracted";
                        }
                        swal({
                            title: 'Are you sure?',
                            text: text,
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Continue!'
                        }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url(); ?>Leave/submit_file_retraction",
                                    data: {
                                        data: data,
                                        reason: reason
                                    },
                                    cache: false,
                                    success: function (res) {
                                        res = JSON.parse(res.trim());

                                        $('#fileRetractionModal').modal('hide');
                                        if (res.status === 'Success') {
                                            swal("Good job!", "Leave Retraction request was sent!", "success");
                                            notification(res.notif_details.notif_id);
                                            $("#retraction-ongoing-perpage").change();
                                        } else {
                                            swal("Failed!", "Something happened while submitting retraction request!", "error");
                                        }
                                    }
                                });
                            }
                        });
                    }

                }

            });
            $('a[href="#tab-ongoing"]').on('show.bs.tab', function (event) {
                $("#div-accordion-history").html("");
                //ONGOING PAGINATION-------------------------------------------------
                $("#retraction-ongoing-perpage").change(function () {
                    var perPage = $("#retraction-ongoing-perpage").val();
                    getOngoingRetraction(0, perPage, function (total) {
                        $("#retraction-ongoing-pagination").pagination('destroy');
                        $("#retraction-ongoing-pagination").pagination({
                            items: total, //default
                            itemsOnPage: $("#retraction-ongoing-perpage").val(),
                            hrefTextPrefix: "#",
                            cssStyle: 'light-theme',
                            onPageClick: function (pagenumber) {
                                var perPage = $("#retraction-ongoing-perpage").val();
                                getOngoingRetraction((pagenumber * perPage) - perPage, perPage);
                            }
                        });
                    });
                });
                $("#retraction-ongoing-perpage").change();

                //END OF ONGOING PAGINATION --------------------------------------
            });
            $('a[href="#tab-history"]').on('show.bs.tab', function (event) {
                $("#div-accordion-ongoing").html("");
                $('#retraction-history-daterange .form-control').val(moment().startOf('month').format('MM/DD/YYYY') + ' - ' + moment().endOf('month').format('MM/DD/YYYY'));
                $('#retraction-history-daterange').daterangepicker({
                    startDate: moment().startOf('month'),
                    endDate: moment().endOf('month'),
                    opens: 'center',
                    drops: 'down',
                    autoUpdateInput: true
                },
                        function (start, end, label) {
                            $('#retraction-history-daterange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
                            $("#retraction-history-perpage").change();
                        });
                //HISTORY PAGINATION-----------------------------------------------------
                $("#retraction-history-perpage").change(function () {
                    var perPage = $("#retraction-history-perpage").val();
                    getHistoryRetraction(0, perPage, function (total) {
                        $("#retraction-history-pagination").pagination('destroy');
                        $("#retraction-history-pagination").pagination({
                            items: total, //default
                            itemsOnPage: $("#retraction-history-perpage").val(),
                            hrefTextPrefix: "#",
                            cssStyle: 'light-theme',
                            onPageClick: function (pagenumber) {
                                var perPage = $("#retraction-history-perpage").val();
                                getHistoryRetraction((pagenumber * perPage) - perPage, perPage);
                            }
                        });
                    });
                });

                $("#btn-search-history-retraction").click(function () {
                    $("#retraction-history-perpage").change();
                });
                $("#retraction-history-perpage").change();
                //END OF HISTORY PAGINATION----------------------------------------------
            });
            $(".tab-content").on('show.bs.collapse', ".m-accordion__item-body", function () {

                var retractLeave_ID = $(this).data('retractleaveid');
                var accordionType = $(this).data('type');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Leave/get_retraction_details",
                    data: {
                        retractLeave_ID: retractLeave_ID
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        //                        console.log(res);
                        var string = '<div class="m-accordion__item-content" >';
                        string += '<div>';
                        string += '<h5 class="pull-left">RETRACTION</h5><a href="#" class="btn btn-outline-brand btn-sm m-btn m-btn--icon m-btn--outline-2x pull-right"  data-retractleaveid="' + retractLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal-retraction" title="View Approval"><span><i class="fa  flaticon-list-2"></i><span class="button-text">View Retraction Approval</span></span></a><br /><br />';
                        string += '<div style="max-height:250px;overflow:auto"><table class="table table-sm m-table table-striped table-bordered table-hover" style="">';
                        string += '<thead>';
                        string += '<tr>';
                        string += '<th>Date</th>';
                        string += '<th>Type</th>';
                        string += '<th>Final Status</th>';
                        string += '<th>Description</th>';
                        string += '<th>Payment</th>';
                        string += (accordionType === 'ongoing') ? '<th>Actions</th>' : '';
                        string += '</tr></thead><tbody>';
                        $.each(res, function (i, item) {
                            var minutes = (item.minutes === "480") ? "Full" : "Half";
                            var isPaid = (item.isPaid === "1") ? "Paid" : "Unpaid";
                            string += "<tr>";
                            string += "<td class='m--font-bolder'>" + moment(item.date, 'YYYY-MM-DD').format("MMM DD, YYYY") + "</td>";
                            string += "<td>" + item.leaveType + " Leave</td>";
                            string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.status + '</span></td>';
                            string += "<td>" + minutes + " Day</td>";
                            string += "<td>" + isPaid + "</td>";
                            string += (accordionType === 'ongoing') ? '<td><a href="#" title="Delete Leave Date" class="btn btn-sm btn-outline-danger m-btn m-btn--icon m-btn--icon-only" onclick="deleteRequestDetail(' + item.retractLeaveDetails_ID + ',this)"><i class="la la-trash"></i></a></td>' : '';
                            string += "</tr>";
                        });
                        string += "</tbody></table></div></div>";
                        $("#accordion-item-" + retractLeave_ID).html(string);
                    }
                });
            });
             $("#approvalDetailsModal-retraction").on('show.bs.modal', function (e) {
                var retractLeave_ID = $(e.relatedTarget).data('retractleaveid');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Leave/get_retraction_approval_flow",
                    data: {
                        retractLeave_ID: retractLeave_ID
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            //                            console.log(res);
                            var retract_leave = res.retract_leave;
                            var retract_leave_approval = res.retract_leave_approval;
                            $("#view-name-retraction").html(retract_leave.fname + " " + retract_leave.lname);
                            $("#view-pic-retraction").attr('src', '<?php echo base_url(); ?>' + retract_leave.pic);
                            $("#view-job-retraction").html(retract_leave.job + " <small>(" + retract_leave.jobstat + ")</small>");
                            $("#view-requestdate-retraction").html("" + moment(retract_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "");
                            $("#view-reason-retraction").html('"' + retract_leave.reason + '"');
                            $("#div-viewApproval-retraction").html("");
                            var string = '<div class="m-section" style="margin-bottom:0px"><div class="m-section__content">';

                            $.each(retract_leave_approval, function (key, obj) {
                                var blinker = (obj.semiStatus==='4')?'<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>':'';
                                var comment = ($.trim(obj.comment) === '') ? 'No comment' : $.trim(obj.comment);
                                var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                                string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                                string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                                string += '<div class="m-portlet__head-caption">';
                                string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">';
                                string += '<p class="text-center">'+blinker+'<strong style="color:white">' + obj.fname + ' ' + obj.lname + '</strong> <small style="color:#ddd;">( ' + decisionOn + ' )</small><br></p>';
                                string += '</p></div></div>';

                                string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;color:black;background:#e6e6e6">';
                                string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                                string += '  <i class="fa fa-quote-left"></i>';
                                string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                                string += ' <i class="fa fa-quote-right"></i>';
                                string += ' </blockquote>';
                                string += '<div style="max-height:250px;overflow:auto;font-size:12px"><table class="table table-striped m-table table-sm">';
                                string += '<thead><tr><th>Date</th><th>Type</th><th>Description</th><th>Payment</th><th>Decision</th></tr></thead>';
                                string += '<tbody>';
                                $.each(obj.details, function (i, item) {
                                    var minutes = (item.minutes === '480') ? "Full Day" : "Half Day";
                                    var paid = (item.isPaid === '1') ? "Paid" : "Unpaid";
                                    string += '<tr>';
                                    string += '<td class="m--font-bolder">' + moment(item.date, 'YYYY-MM-DD').format('MMM DD, YYYY') + '</td>';
                                    string += '<td>' + item.leaveType + ' Leave</td>';
                                    string += '<td>' + minutes + '</td>';
                                    string += '<td>' + paid + '</td>';
                                    string += '<td><span class="m-badge m-badge--' + statusClass[item.status] + ' m-badge--wide">' + item.description + '</span></td>';
                                    string += '</tr>';
                                });
                                string += '</tbody></table></div>';

                                string += '</div></div></div>';
                            });
                            string += "</div></div>";
                            $("#div-viewApproval-retraction").html(string);
                        } else {
                            $(this).modal('hide');
                            swal("Failed!", "Something happened while retrieving details!", "error");
                        }
                        $(".m-widget7__img").on('error', function () {
                            $(this).attr('onerror', null);
                            $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                        });
                    }
                });
            });
            var $control = $('#textarea-reason'),
                    $remaining = $('#textarea-note'),
                    textLimit = parseInt($control.attr('maxlength'), 10);

            $remaining.text(textLimit - $('#textarea-reason').val().length);

            $control.on('keyup paste', function () {
                $control.val($control.val().substring(0, textLimit));
                $remaining.text(textLimit - parseInt($control.val().length, 10));
            });
            $('a[href="#tab-ongoing"]').click();

        });
</script>