<style type="text/css">
    body{
        padding-right:0 !important;
    }
    .m-datatable__cell{
        text-transform:  capitalize
    }
    .bg-custom-gray{
        background:#4B4C54 !important;
    }  
    .portlet-header-report{
        background:#4B4C54 !important;
        color:white  !important;
        height:50px !important;
    }
    .font-white{
        color:white !important;
    }
    .m-content ul.m-subheader__breadcrumbs li,.m-content ul.m-subheader__breadcrumbs li a{
        padding:0 !important;
    }
    .m-content ul.m-subheader__breadcrumbs li a span:hover{
        background: #ddd !important;         
        color:black !important; 
    } 

    .m-content ul.m-subheader__breadcrumbs li a span{
        color:#fff !important;  
        padding:10px 20px !important; 
    } 
    .m-content ul.m-subheader__breadcrumbs{
        background:#4B4C54 !important; 
        padding:2px;
    }
    .my-nav-active{
        background: #eee !important;    
    } 
    li.my-nav-active a span{      
        color:black; 
    }
    .m-content hr{
        border:2.5px solid #eee !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/rules'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Rules</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline" style="box-shadow:0px 1px 15px 1px rgba(69, 65, 78, 0.08);">
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-list m--font-primary"></i> Summary</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/monitoring'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-dashboard m--font-info"></i> Monitoring</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/types'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-table m--font-success"></i> Types</span>
                </a>
            </li>
            <li class="m-nav__item my-nav-active">
                <a href="<?php echo base_url('Leave/rules'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text" style="color:#555 !important"><i class="fa fa-asterisk m--font-brand"></i> Rules</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/users'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-users m--font-danger"></i> Users</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/credits'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-ticket m--font-warning"></i> Credits</span>
                </a>
            </li>
        </ul>
        <br />
        <br />
        <div class="row">
            <div class="col-lg-12">	
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins"   style="color:white !important;font-size:16px">
                                    Leave Rules
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-12">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span><i class="la la-search"></i></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <a href="#" class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="modal" data-target="#createLeaveModal">
                                        <span>
                                            <i class="la la-plus-circle"></i>
                                            <span>New Record</span>
                                        </span>
                                    </a>					
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->

                        <!--begin: Datatable -->
                        <div class="m_datatable" id="ajax_data"></div>
                        <!--end: Datatable -->
                    </div>
                </div>	
                <!--end::Portlet-->
            </div>
        </div>

    </div>
    <!--begin::Modal-->
    <div class="modal fade" id="createLeaveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">

                <form onsubmit="return false;" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="form_leave" novalidate="novalidate" method="POST">

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Leave Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div style="height:400px;width:100%;overflow-y:scroll;overflow-x:hidden">
                            <!--begin::Form-->
                            <div class="m-portlet__body">	
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-8">
                                        <label>Leave Name:</label>
                                        <input type="text" class="form-control m-input" placeholder="Enter leave name" name="leave_name">
                                        <span class="m-form__help">Name of leave</span>
                                    </div>

                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-12">
                                        <label>Description:</label>
                                        <textarea class="form-control m-input" rows="3" name="description"></textarea>
                                        <span class="m-form__help">Additional details of the leave</span>
                                    </div>
                                </div>
                                <div class="row form-group m-form__group ">
                                    <div class="col-lg-5">
                                        <label>Leave Type:</label>
                                        <select class="form-control m-input" name="leaveType">
                                        </select>
                                        <span class="m-form__help">Reference to leave type</span>

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-12">
                                        <label>Refresh States:</label>
                                        <div class="m-radio-inline">
                                            <label class="m-radio m-radio--solid">
                                                <input type="radio" name="refreshOccur" value="NULL"> Not Refreshable
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--solid">
                                                <input type="radio" name="refreshOccur" value="year"> Yearly
                                                <span></span>
                                            </label>

                                            <label class="m-radio m-radio--solid">
                                                <input type="radio" name="refreshOccur" value="semiannual"> Semi-Annually
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--solid">
                                                <input type="radio" name="refreshOccur" value="quarter"> Quarterly
                                                <span></span>
                                            </label>
                                        </div>
                                        <span class="m-form__help">Determines when the leave restores the original credits</span>
                                    </div>
                                </div>
                                <div class="row form-group m-form__group">
                                    <div class="col-lg-6">
                                        <label>Pre filing limit:</label>
                                        <div class="m-input-icon m-input-icon--right">
                                            <input name="preFilingLimit" type="text" class="form-control m-input" placeholder="Enter number of days">
                                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="flaticon-calendar-3"></i></span></span>
                                        </div>
                                        <span class="m-form__help">Number of days prior to allowed dates to file</span>

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-lg-6">
                                        <div class=" m-form__group">
                                            <label>Datepicker Date Start:</label>
                                            <div class="m-input-icon m-input-icon--right">
                                                <input name="allowedDateStart" type="text" class="form-control m-input" placeholder="Enter number of days">
                                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="flaticon-calendar-3"></i></span></span>
                                            </div>
                                            <span class="m-form__help">Number of days for overall start of date picker</span>

                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class=" m-form__group">

                                            <label>Datepicker Date End:</label>
                                            <div class="m-input-icon m-input-icon--right">
                                                <input  name="allowedDateEnd" type="text" class="form-control m-input" placeholder="Enter number of days">
                                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="flaticon-calendar-3"></i></span></span>
                                            </div>
                                            <span class="m-form__help">Number of days for overall end of date picker</span>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style='padding:0'></div>
                                <div class="row form-group m-form__group ">
                                    <div class="col-lg-5">
                                        <label>Datepicker Style:</label>
                                        <select class="form-control m-input" name="pickerMode">
                                            <option value="">--</option>
                                            <option value="picker">Picker</option>
                                            <option value="range">Range</option>
                                        </select>
                                        <span class="m-form__help">Display style of datepicker</span>

                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-lg-6" style="display:none" id="div-minimum">
                                        <div class=" m-form__group">
                                            <label>Minimum number of days to file:</label>
                                            <div class="m-input-icon m-input-icon--right">
                                                <input name="minimumDays" type="text" class="form-control m-input" placeholder="Enter number of days">
                                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="flaticon-calendar-3"></i></span></span>
                                            </div>
                                            <span class="m-form__help">Lowest number of days for specific checking of allowed dates </span>

                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class=" m-form__group">

                                            <label>Maximum number of days to file:</label>
                                            <div class="m-input-icon m-input-icon--right">
                                                <input  name="maximumDays" type="text" class="form-control m-input" placeholder="Enter number of days">
                                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="flaticon-calendar-3"></i></span></span>
                                            </div>
                                            <span class="m-form__help">Highest number of days for specific checking of allowed dates</span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group m-form__group row" style='padding:0'></div>
                                <div class="row form-group m-form__group">
                                    <div class="col-lg-5">
                                        <label>Allow Backup Leave:</label>
                                        <select class="form-control m-input" name="allowBackup">
                                            <option value="">--</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                        <span class="m-form__help">Allow usage of a reserved leave if leave credits are gone</span>
                                    </div>
                                </div>
                                <div class="row" id="div-backup" style="display:none">
                                    <div class="col-md-6">
                                        <div class="form-group m-form__group ">
                                            <label>Backup Leave Type:</label>
                                            <select class="form-control m-input" name="backupLeave">
                                                <option value="">--</option>
                                            </select>
                                            <span class="m-form__help">Referred leave as reserve</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group m-form__group ">
                                            <label>Maximum credits:</label>
                                            <div class="m-input-icon m-input-icon--right">
                                                <input type="text" class="form-control m-input" placeholder="Enter maximum credits" name='maximumCredits'>
                                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="flaticon-coins"></i></span></span>
                                            </div>
                                            <span class="m-form__help">Highest number of days allowed for reserved use</span>
                                        </div>
                                    </div>
                                </div> 
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-5">
                                        <label>Allow Retraction:</label>
                                        <select class="form-control m-input" name="allowRetraction">
                                            <option value="">--</option>
                                            <option value="no">No</option>
                                            <option value="single">Single</option>
                                            <option value="group">Group</option>
                                        </select>
                                        <span class="m-form__help">Determine if leave permits cancellation and/or change of approved leave</span>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-5">
                                        <label>Allow Half Day Leave:</label>
                                        <select class="form-control m-input" name="allowHalfDay">
                                            <option value="">--</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                        <span class="m-form__help">Determine if leave permits half day leave</span>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-5">
                                        <label>Default Payment:</label>
                                        <select class="form-control m-input" name="defaultPayment">
                                            <option value="">--</option>
                                            <option value="credit">Per Credit</option>
                                            <option value="paid">Paid</option>
                                            <option value="unpaid">Unpaid</option>
                                        </select>
                                        <span class="m-form__help">Determine the matter of payment of the leave rule</span>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!--end::Form-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" for="submit-form" tabindex="0">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
        function formReset() {
            $("[name=leave_name]").val("");
            $("[name=description]").val("");
            $("[name=leaveType]").val("");
//                        $("[name=refreshOccur]").val(details.refreshOccur);
            $("[name=refreshOccur]").prop("checked", false);
            $("[name=preFilingLimit]").val("");
            $("[name=allowedDateStart]").val("");
            $("[name=allowedDateEnd]").val("");
            $("[name=pickerMode]").val("");
            $("[name=minimumDays]").val("");
            $("[name=maximumDays]").val("");
            $("[name=allowBackup]").val("");
            $("[name=backupLeave]").val("");
            $("[name=maximumCredits]").val("");
            $("[name=allowRetraction]").val("");
            $("[name=allowHalfDay]").val("");
            $("[name=defaultPayment]").val("");

            $('#createLeaveModal').data('isupdate', false);
        }
        function updateModalTrigger(leave_id) {
            $.when(fetchPostData({leave_id: leave_id}, '/Leave/get_single_leave_details'))
                    .then(function (json) {
                        json = JSON.parse(json.trim());
                        var details = json.leave_details;
                        $("[name=leave_name]").val(details.leave_name);
                        $("[name=description]").val(details.description);
                        $("[name=leaveType]").val(details.leaveType_ID);
                        $("[name=refreshOccur]").prop("checked", false);
                        $("[name=refreshOccur][value='" + $.trim((details.refreshOccur === null) ? 'NULL' : details.refreshOccur) + "']").prop("checked", true);
                        $("[name=preFilingLimit]").val(details.preDays);
                        $("[name=allowedDateStart]").val(details.allowedDateStart);
                        $("[name=allowedDateEnd]").val(details.allowedDateEnd);
                        $("[name=pickerMode]").val(details.pickerMode).change();
                        $("[name=minimumDays]").val(details.minimumDays);
                        $("[name=maximumDays]").val(details.maximumDays);
                        $("[name=allowBackup]").val((details.allowBackup === '1') ? 'yes' : 'no').change();
                        $("[name=backupLeave]").val(details.backupLeave_ID);
                        $("[name=maximumCredits]").val(details.backupMax);
                        $("[name=allowRetraction]").val(details.allowRetraction);
                        $("[name=defaultPayment]").val(details.defaultPayment);
                        $("[name=allowHalfDay]").val((details.allowHalfDay === '1') ? 'yes' : 'no');
                        $("#createLeaveModal").data('isupdate', true);
                        $("#createLeaveModal").data('leave_id', leave_id);
                        $("#createLeaveModal").modal();

                    });
        }
        function datatable_init() {

            var datatable = $('.m_datatable').mDatatable({
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            // sample GET method
                            method: 'POST',
                            url: 'get_leave_lists',
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                // layout definition
                layout: {
                    theme: 'default', // datatable theme
                    class: '', // custom wrapper class
                    scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
//                    height: 550, // datatable's body's fixed height
                    footer: false // display/hide footer
                },
                // column sorting
                sortable: true,
                pagination: true,
                toolbar: {
                    // toolbar placement can be at top or bottom or both top and bottom repeated
                    placement: ['bottom'],
                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [5, 10, 20, 30, 50, 100]
                        }
                    }
                },
                search: {
                    input: $('#input-search')
                },
                // columns definition
                columns: [
                    {

                        field: 'leave_name',
                        title: 'Leave Name',
                        sortable: true,
                        width: 300
                    }, {
                        field: 'description',
                        title: 'Description',

                        sortable: true,
                        width: 300
                    }, {
                        field: 'Actions',
                        width: 110,
                        title: 'Actions',
                        sortable: false,
                        overflow: 'visible',
                        template: function (row, index, datatable) {
//                            row = {leave_name,leave_id}
                            return '\
                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"  onclick="updateModalTrigger(' + row.leave_id + ')">\
                                                        <i class="flaticon-edit-1"></i>\
                                                </a>\
                                        ';
                        }
                    }]
            });
            return datatable;
        }



        $(function () {
            $.when(fetchGetData('/Leave/names_init'))
                    .then(function (json) {
                        json = JSON.parse(json.trim());
                        var str = "<option value=''>--</option>";
                        $.each(json.leave_types, function (key, obj) {
                            str += "<option value='" + obj.leaveType_ID + "'>" + obj.leaveType + "</option>";
                        });
                        $("[name=backupLeave]").html(str);
                        var str = "<option value=''>--</option>";
                        $.each(json.leave_types, function (key, obj) {
                            str += "<option value='" + obj.leaveType_ID + "'>" + obj.leaveType + "</option>";
                        });
                        $("[name=leaveType]").html(str);
                        //START----------------------
                        var datatable = datatable_init();
                        $("#form_leave").validate({
                            // define validation rules

                            focusCleanup: true,
                            rules: {
                                leave_name: {
                                    required: true,
                                    minlength: 10,
                                    maxlength: 50,
                                    normalizer: function (value) {
                                        return $.trim(value);
                                    },
                                    remote: {
                                        url: "<?php echo base_url('Leave/check_exist') ?>",
                                        type: "post",
                                        data: {
                                            name: "leave_name",
                                            table: "tbl_leave",
                                            isUpdate: function () {
                                                if ($('#createLeaveModal').data('isupdate') === true) {
                                                    return JSON.stringify({name: "leave_id", id: $('#createLeaveModal').data('leave_id')});
                                                } else {
                                                    return false;
                                                }
                                            }
                                        }
                                    }
                                },
                                description: {
                                    required: true,
                                    minlength: 10,
                                    maxlength: 200,
                                    normalizer: function (value) {
                                        return $.trim(value);
                                    }
                                },
                                leaveType: {
                                    required: true
                                },
                                refreshOccur: {
                                    required: true
                                },
                                preFilingLimit: {
                                    required: true,
                                    digits: true,
                                    normalizer: function (value) {
                                        return $.trim(value);
                                    }
                                },
                                allowedDateStart: {
                                    required: true,
                                    digits: true,
                                    normalizer: function (value) {
                                        return $.trim(value);
                                    }
                                },
                                allowedDateEnd: {
                                    required: true,
                                    digits: true,
                                    normalizer: function (value) {
                                        return $.trim(value);
                                    }
                                },
                                pickerMode: {
                                    required: true
                                },
                                minimumDays: {
                                    required: true,
                                    digits: true,
                                    normalizer: function (value) {
                                        return $.trim(value);
                                    }
                                },
                                maximumDays: {
                                    required: true,
                                    digits: true,
                                    normalizer: function (value) {
                                        return $.trim(value);
                                    }
                                },
                                allowBackup: {
                                    required: true
                                },
                                backupLeave: {
                                    required: true
                                },
                                maximumCredits: {
                                    required: true,
                                    digits: true,
                                    normalizer: function (value) {
                                        return $.trim(value);
                                    }
                                },
                                allowRetraction: {
                                    required: true
                                },
                                allowHalfDay: {
                                    required: true
                                },
                                defaultPayment: {
                                    required: true
                                }
                            },
                            messages: {
                                leave_name: {
                                    remote: "Leave name already exists!"
                                }
                            },
                            //display error alert on form submit  
                            invalidHandler: function (event, validator) {
                                mApp.scrollTo("#form_leave");
                                swal({
                                    "title": "",
                                    "text": "There are some errors in your submission. Please correct them.",
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                            },
                            submitHandler: function (form) {
                                var serialize = $("#form_leave").serializeArray();
                                var newSerialize = [];
                                for (var x = 0; x < serialize.length; x++) {
                                    var name = serialize[x]['name'];
                                    var value = serialize[x]['value'];
                                    var obj = {};
                                    obj[name] = $.trim(value);
                                    newSerialize.push(obj);
                                }

                                if ($('#createLeaveModal').data('isupdate') === true) {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Leave/update_leave_record",
                                        data: {
                                            leave_id: $('#createLeaveModal').data('leave_id'),
                                            data: JSON.stringify(newSerialize)
                                        },
                                        cache: false,
                                        success: function (res) {
                                            res = JSON.parse(res.trim());
                                            $('#createLeaveModal').modal('hide');
                                            if (res.status === 'Success') {
                                                datatable.reload();
                                                swal("Good job!", "Successfully updated leave!", "success");
                                            } else {
                                                swal("Failed!", "Something happened while updating leave!", "error");
                                            }

                                        }
                                    });
                                } else {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Leave/new_leave_record",
                                        data: {
                                            data: JSON.stringify(newSerialize)
                                        },
                                        cache: false,
                                        success: function (res) {
                                            res = JSON.parse(res.trim());
                                            $('#createLeaveModal').modal('hide');
                                            if (res.status === 'Success') {
                                                datatable.reload();
                                                swal("Good job!", "Successfully added new leave!", "success");
                                            } else {
                                                swal("Failed!", "Something happened while adding a new leave!", "error");
                                            }

                                        }
                                    });
                                }

                            }
                        });
                    });
            $('[name=pickerMode]').change(function () {
                var pickerMode = $(this).val();
                $('[name=minimumDays]').val("");
                if (pickerMode === 'range') {
                    $('[name=minimumDays]').rules('remove', 'required');
                    $("#div-minimum").hide();
                } else {
                    $('[name=minimumDays]').rules('add', {required: true});
                    $("#div-minimum").show();
                }
            });
            $('[name=allowBackup]').change(function () {
                var allowBackup = $(this).val();
                $('[name=backupLeave]').val("");
                $('[name=maximumCredits]').val("");
                if (allowBackup === 'yes') {
                    $('[name=backupLeave]').rules('add', {required: true});
                    $('[name=maximumCredits]').rules('add', {required: true});
                    $("#div-backup").show();
                } else {
                    $('[name=backupLeave]').rules('remove', 'required');
                    $('[name=maximumCredits]').rules('remove', 'required');
                    $("#div-backup").hide();
                }
            });
            $('#createLeaveModal').on('hide.bs.modal', function () {
                console.log("RESET");
                formReset();
                $("#form_leave").validate().resetForm();
            });
        });
</script>