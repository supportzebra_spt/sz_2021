<style type="text/css">
    body{
        padding-right:0 !important;
    }
    .m-datatable__cell{
        text-transform:  capitalize
    }
    .bg-custom-gray{
        background:#4B4C54 !important;
    }
    .portlet-header-report{
        background:#4B4C54 !important;
        color:white  !important;
        height:50px !important;
    }
    .font-white{
        color:white !important;
    }
    .m-content ul.m-subheader__breadcrumbs li,.m-content ul.m-subheader__breadcrumbs li a{
        padding:0 !important;
    }
    .m-content ul.m-subheader__breadcrumbs li a span:hover{
        background: #ddd !important;         
        color:black !important; 
    } 

    .m-content ul.m-subheader__breadcrumbs li a span{
        color:#fff !important;  
        padding:10px 20px !important; 
    } 
    .m-content ul.m-subheader__breadcrumbs{
        background:#4B4C54 !important; 
        padding:2px;
    }
    .my-nav-active{
        background: #eee !important;    
    } 
    li.my-nav-active a span{      
        color:black; 
    }
    .m-content hr{
        border:2.5px solid #eee !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/types'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Types</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline" style="box-shadow:0px 1px 15px 1px rgba(69, 65, 78, 0.08);">
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-list m--font-primary"></i> Summary</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/monitoring'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-dashboard m--font-info"></i> Monitoring</span>
                </a>
            </li>
            <li class="m-nav__item my-nav-active">
                <a href="<?php echo base_url('Leave/types'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text" style="color:#555 !important"><i class="fa fa-table m--font-success"></i> Types</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/rules'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-asterisk m--font-brand"></i> Rules</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/users'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-users m--font-danger"></i> Users</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/credits'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-ticket m--font-warning"></i> Credits</span>
                </a>
            </li>
        </ul>
        <br />
        <br />
        <div class="row">
            <div class="col-lg-12">	
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">	
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins"  style="color:white !important;font-size:16px">
                                     Leave Types
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-12">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span><i class="la la-search"></i></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <a href="#" class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="modal" data-target="#createLeaveModal">
                                        <span>
                                            <i class="la la-plus-circle"></i>
                                            <span>New Type</span>
                                        </span>
                                    </a>					
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->

                        <!--begin: Datatable -->
                        <div class="m_datatable" id="ajax_data"></div>
                        <!--end: Datatable -->
                    </div>
                </div>	
                <!--end::Portlet-->
            </div>
        </div>

    </div>
    <!--begin::Modal-->
    <div class="modal fade" id="createLeaveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <form onsubmit="return false;" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="form_leave" novalidate="novalidate" method="POST">

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Leave Type</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--begin::Form-->
                        <div class="m-portlet__body">	
                            <div class="form-group m-form__group row">
                                <div class="col-lg-8">
                                    <label>Leave Type:</label>
                                    <input type="text" class="form-control m-input" placeholder="Enter leave name" name="leaveType">
                                    <span class="m-form__help">Please enter leave type</span>
                                </div>

                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <label>Description:</label>
                                    <textarea class="form-control m-input" rows="3" name="description"></textarea>
                                    <span class="m-form__help">Please enter description</span>
                                </div>
                            </div> 
                        </div>

                        <!--end::Form-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" for="submit-form" tabindex="0">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
        function formReset() {
            $("[name=leaveType]").val("");
            $("[name=description]").val("");
            $('#createLeaveModal').data('isupdate', false);
        }
        function updateModalTrigger(leaveType_ID) {
            $.when(fetchPostData({leaveType_ID: leaveType_ID}, '/Leave/get_single_leave_types'))
                    .then(function (json) {
                        json = JSON.parse(json.trim());
                        var details = json.leave_details;
                        $("[name=leaveType]").val(details.leaveType);
                        $("[name=description]").val(details.description);
                        $("#createLeaveModal").data('isupdate', true);
                        $("#createLeaveModal").data('leavetype_id', leaveType_ID);
                        $("#createLeaveModal").modal();
                    });
        }
        function datatable_init() {

            var datatable = $('.m_datatable').mDatatable({
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            // sample GET method
                            method: 'POST',
                            url: 'get_leave_types',
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                // layout definition
                layout: {
                    theme: 'default', // datatable theme
                    class: '', // custom wrapper class
                    scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
//                    height: 550, // datatable's body's fixed height
                    footer: false // display/hide footer
                },
                // column sorting
                sortable: true,
                pagination: true,
                toolbar: {
                    // toolbar placement can be at top or bottom or both top and bottom repeated
                    placement: ['bottom'],
                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [5, 10, 20, 30, 50, 100]
                        }
                    }
                },
                search: {
                    input: $('#input-search')
                },
                // columns definition
                columns: [
                    {

                        field: 'leaveType',
                        title: 'Leave Type',
                        sortable: true,
                        width: 300
                    }, {
                        field: 'description',
                        title: 'Description',
                        sortable: true,
                        width: 300
                    }, {
                        field: 'Actions',
                        width: 110,
                        title: 'Actions',
                        sortable: false,
                        overflow: 'visible',
                        template: function (row, index, datatable) {
//                            row = {leaveType,leaveType_ID}
                            return '\
                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"  onclick="updateModalTrigger(' + row.leaveType_ID + ')">\
                                                        <i class="flaticon-edit-1"></i>\
                                                </a>\
                                        ';
                        }
                    }]
            });
            return datatable;
        }



        $(function () {
            var datatable = datatable_init();
            jQuery.validator.addMethod("noSpace", function (value, element) {
                return $.trim(value) === '' || value.trim().length !== 0;
            }, "No space please and don't leave it empty");
            $("#form_leave").validate({
                // define validation rules
                focusCleanup: true,
                rules: {
                    leaveType: {
                        required: true,
                        minlength: 1,
                        maxlength: 50,
                        normalizer: function (value) {
                            return $.trim(value);
                        },
                        remote: {
                            url: "<?php echo base_url('Leave/check_exist') ?>",
                            type: "post",
                            data: {
                                name: "leaveType",
                                table: "tbl_leave_type",
                                isUpdate: function () {
                                    if ($('#createLeaveModal').data('isupdate') === true) {
                                        return JSON.stringify({name: "leaveType_ID", id: $('#createLeaveModal').data('leavetype_id')});
                                    } else {
                                        return false;
                                    }
                                }

                            }
                        }
                    },
                    description: {
                        required: true,
                        minlength: 10,
                        maxlength: 200,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    }
                },
                messages: {
                    leaveType: {
                        remote: "Leave type already exists!"
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    mApp.scrollTo("#form_leave");
                    swal({
                        "title": "",
                        "text": "There are some errors in your submission. Please correct them.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },
                submitHandler: function (form) {
                    var serialize = $("#form_leave").serializeArray();
                    var newSerialize = [];
                    for (var x = 0; x < serialize.length; x++) {
                        var name = serialize[x]['name'];
                        var value = serialize[x]['value'];
                        var obj = {};
                        obj[name] = $.trim(value);
                        newSerialize.push(obj);
                    }
                    if ($('#createLeaveModal').data('isupdate') === true) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>Leave/update_leave_type",
                            data: {
                                leaveType_ID: $('#createLeaveModal').data('leavetype_id'),
                                data: JSON.stringify(newSerialize)
                            },
                            cache: false,
                            success: function (res) {
                                res = JSON.parse(res.trim());
                                $('#createLeaveModal').modal('hide');
                                if (res.status === 'Success') {
                                    datatable.reload();
                                    swal("Good job!", "Successfully updated leave type!", "success");
                                } else {
                                    swal("Failed!", "Something happened while updating leave type!", "error");
                                }

                            }
                        });
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>Leave/new_leave_type",
                            data: {
                                data: JSON.stringify(newSerialize)
                            },
                            cache: false,
                            success: function (res) {
                                res = JSON.parse(res.trim());
                                $('#createLeaveModal').modal('hide');
                                if (res.status === 'Success') {
                                    datatable.reload();
                                    swal("Good job!", "Successfully added new leave type!", "success");
                                } else {
                                    swal("Failed!", "Something happened while adding a new leave type!", "error");
                                }

                            }
                        });
                    }

                }
            }
            );
            $('#createLeaveModal').on('hide.bs.modal', function () {
                console.log("RESET");
                formReset();

                $("#form_leave").validate().resetForm();
            });
        });
</script>