<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="utf-8" />
    <title>SupportZebra | 404 - Not Found</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link href="<?php echo base_url('assets/src/vendors/base/vendors.bundle.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/src/demo/default/base/style.bundle.css');?>" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/img/favicon.ico');?>" />
</head>

<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid  m-error-3" style="background-image: url(<?php echo base_url('assets/src/app/media/img/error/bg1.jpg');?>);">
            <div class="m-grid__item m-grid__item--fluid m-grid  m-error-1" style="background-image: url(assets/app/media/img/error/bg1.jpg);">
                <div class="m-error_container">
                    <span class="m-error_number">
                        <h1>404</h1>
                    </span>
                    <p class="m-error_desc">
                        OOPS! Page Not Found
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url('assets/src/vendors/base/vendors.bundle.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/src/demo/default/base/scripts.bundle.js');?>" type="text/javascript"></script>
</body>

</html>