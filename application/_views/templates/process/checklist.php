<style type="text/css">
.active:focus {
  background: #e9eeef;
  border-color: #00c5dc;
  color: #3f4047;
}
.activeinput:focus{
 background: #1d86da;
}
.droparea{
  text-align: center;
}
.droparea_outside{
  border-style: dashed;
  padding: 10px;
  margin: 10px;
  border-color:#a5a6ad;
}
.droparea_append{
  padding: 10px;
  margin: 10px;
}
.colorgreen{
  color: green;
  font-style: italic;
  font-size:0.9em;
}
.evenadding{
  padding: 15px;
  background:#f1f0f0;
}
.oddadding{
  padding: 15px;
  background: #f9ecec;
}
.buttonform{
  text-align: center;
}
.distance{
  margin-bottom: 20px;
}
.formdistance{
  margin-top: 10px;
}
.taskdistance{
  margin-top: 15px;
}
.label{
  font-size:1.2em;
  color:#00c5dc;
}
.labelheading{
  font-size:1.4em;
  font-weight: bold;
  color:#00c5dc;
}
.placeholdercolor::placeholder { 
  color: #d8dce8;
  opacity: 1;
}
button.h{
  border:1px solid blue;
  background:blue;
  color:white;
}
body.dragging, body.dragging * {
  cursor: move !important;
}
.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}
/*temp*/
.placeholder {
  border: 1px solid green;
  background-color: white;
  -webkit-box-shadow: 0px 0px 10px #888;
  -moz-box-shadow: 0px 0px 10px #888;
  box-shadow: 0px 0px 10px #888;
}
.tile {
  height: 100px;
}
.grid {
  margin-top: 1em;
}
.formpadding
{
  padding: 30px;
  margin-left: 15px;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>         
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo site_url('process');?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url('process/subfolder').'/'.$processinfo->folder_ID;?>" class="m-nav__link">
              <span class="m-nav__link-text">Source Folder</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet">
          <!--begin: Portlet Head-->
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                  <a href="#" onclick="doNotif2()" class="btn btn-outline-success btn-sm m-btn m-btn--icon">
                    <span>
                      <i class="fa fa-save"></i>
                      <span>Save</span>
                    </span>
                  </a>
                  <?php if($processinfo->processstatus_ID=="10"){?>
                    <a href="#" data-id="<?php echo $processinfo->process_ID;?>" class="btn btn-success btn-sm m-btn m-btn m-btn--icon" data-toggle="modal" data-target="#run_checklist-modal">   <span>
                      <i class="fa fa-play-circle-o"></i>
                      <span>Run checklist</span>
                    </span>
                  </a>
                <?php }?>
              </h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">

       <!--     <div class="sortableContainer">
            <div id="Element1" class="sortIt">Item 1
              <div>hello</div>
              <div>hi</div>
            </div>
            <div id="Element2" class="sortIt">Item 2</div>
            <div id="Element3" class="sortIt">Item 3</div>
            <div id="Element4" class="sortIt">Item 4</div>
          </div> -->


          <ul class="m-portlet__nav">
          </ul>
        </div> 
      </div>
      <!--end: Portlet Head-->
      <div class="m-portlet__body m-portlet__body--no-padding">
        <div class="m-wizard m-wizard--4 m-wizard--brand m-wizard--step-first" id="m_wizard">
          <div class="row m-row--no-padding">
            <div class="col-xl-6 col-lg-12 border"><br>
              &nbsp;&nbsp;<h5 style="color:#777;text-align:center;font-size:1.3em;"><strong><?php echo  $processinfo->processTitle;?> Template</strong></h5>
              <!--<button type="button" data-id="<?php echo $processinfo->process_ID;?>" onclick="addtask(this)" class="btn m-btn--square btn-metal btn-block"><i class="fa fa-plus"></i><span> Add Task</span></button> -->
              <div class="taskdistance" style="text-align: center">
                <a href="#" data-id="<?php echo $processinfo->process_ID;?>" onclick="addtask(this)" class="btn btn-outline-accent btn-sm  m-btn m-btn--icon m-btn--pill">
                  <span>
                    <i class="fa fa-plus"></i>
                    <span>Add Task</span>
                  </span>
                </a>
              </div><br>
              <input type="hidden" id="hiddenprocessID" name="hiddenprocessID" value="<?php echo $processinfo->process_ID;?>"> 
              <div class="m-portlet__body" style="height:350px;overflow-y:scroll; overflow-x:auto;background-color:">
                <div class="movetask form-group m-form__group" id="checkgroup" >
                </div>
              </div>             
            </div>
            <div id="contentdiv" class="col-xl-6 col-lg-12 border">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>



<!-- END-->
</div>

<!--begin::Modal-->
<div class="modal fade" id="run_checklist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Let's run a checklist!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formupdate" name="formupdate" method="post">
          <div class="form-group">
            <h5 style="color:#34bfa3">Base on <strong>'<?php echo $processinfo->processTitle; ?> Template'</strong></h5>
            <br>
            <?php foreach($user_info as $ui) {?>
              <label for="recipient-name" class="form-control-label">Name of your checklist (For example, the name of the employee you’re onboarding) :</label>
              <input type="text" maxlength="40" class="form-control" id="addchecklistName" name="addchecklistName" value="<?php echo $ui->fname;echo "&nbsp;";echo $ui->lname;echo "'s";echo "&nbsp;" ;echo date("h:i a");echo "&nbsp;";echo "checklist"?>" placeholder="Enter Checklist Name">
              <span style="font-size:0.9em;color:gray" id="charNum"></span>
              <input type="hidden" value="<?php echo $processinfo->process_ID;?>" id="fetchprocessid">
            <?php }?>
          </div>
        </form>
      </div>
      <?php foreach ($checklist as $ch){?>
        <div class="modal-footer">
          <button type="submit" name="submit" data-id="<?php echo $ch->checklist_ID; ?>" onclick="runChecklist(this)" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Run This Checklist</button>
        </div>
      <?php }?>
    </div>
  </div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="add_content-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create a Form!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Form</a>
          </li>
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li>
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
          <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span>
          <div class="row">
            <input type="hidden" id="taskformid" value="">
            <input type="hidden" id="sequenceid" value="">
            <div class="col-lg-6">
              <button type="button" onclick="add_form(this)" data-id="forminputtext" id="forminputtext" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-font"></i> Short Text Entry</button>
              <button type="button" onclick="add_form(this)" data-id="formtextarea" id="formtextarea" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-font"></i> Long Text Entry</button> 
              <button type="button" onclick="add_form(this)" data-id="formsingle" id="formsingle" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-dot-circle-o"></i> Single Select</button>
              <button type="button" onclick="add_form(this)" data-id="formmulti" id="formmulti" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-check-square-o"></i> Multiple Select</button>
              <button type="button" onclick="add_form(this)" data-id="formdropdown" id="formdropdown" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-toggle-down"></i> Dropdown Select</button>
              <button type="button" onclick="add_form(this)" data-id="formdate" id="formdate" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Date Picker</button>
              <button type="button" onclick="add_form(this)" data-id="formtime" id="formtime" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Time Picker</button>
              <button type="button" onclick="add_form(this)" data-id="formdatetime" id="formdatetime" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Date and Time Picker</button>      
              <br>
            </div>
            <div class="col-lg-6">
              <button type="button" onclick="add_form(this)" data-id="formheading" id="formheading" class="btn btn-accent btn-sm btn-block"><i class="fa fa-header"></i> Heading</button>
              <button type="button" onclick="add_form(this)" data-id="formtext" id="formtext" class="btn btn-accent btn-sm btn-block"><i class="fa fa-text-width"></i> Text</button>
              <button type="button" onclick="add_form(this)" data-id="formnumber" id="formnumber" class="btn btn-accent btn-sm btn-block"><i class="la la-sort-numeric-desc"></i> Number</button> 
              <button type="button" onclick="add_form(this)" data-id="formemail" id="formemail" class="btn btn-accent btn-sm btn-block"><i class="fa fa-toggle-down"></i> Email</button>
              <button type="button" onclick="add_form(this)" data-id="formspinner" id="formspinner" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-sort-numeric-asc"></i> Number Spinner</button>
              <button type="button" onclick="add_form(this)" data-id="formdivider" id="formdivider" class="btn btn-accent btn-sm btn-block"><i class="fa fa-i-cursor"></i> Divider</button>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-insert">
          <div class="row">
           <div class="col-lg-6">
            <button type="button" class="btn btn-accent btn-sm btn-block"><i class="fa fa-photo"></i>&nbsp;&nbsp;Photo</button>
            <button type="button" class="btn btn-accent btn-sm btn-block"><i class="fa fa-video-camera"></i>&nbsp;&nbsp;Video</button>
            <button type="button" class="btn btn-accent btn-sm btn-block"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;File</button> <br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="formproperties-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Entry Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
    <!--       <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li> -->
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="labelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="sublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Set Placeholder</label>
              <input type="text" id="placeholderform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Your Placeholder...">
            </div>
          </div>
          <div class="col-lg-5 formpadding">
            <div class="m-form__group form-group row">
              <label class="col-3 col-form-label">Set to required</label>
              <div class="col-6">
                <span class="m-switch m-switch--lg m-switch--icon">
                  <label>
                    <input class="requirecheckbox" id="requirecheckbox" type="checkbox" name="">
                    <span></span>
                  </label>
                </span>
              </div>
            </div>
            <div class="m-form__group form-group row">
              <label>Text Minimum / Number Range From</label>
              <input id="textmax" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div>
            <div class="m-form__group form-group row">
              <label>Text Maximum / Number Range To</label>
              <input id="textmin" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" name="submit" id="" class="btn btn-danger m-btn m-btn--icon">
      <span>
        <i class="fa fa-remove"></i>
        <span>Discard</span>
      </span>
    </button>
    <button type="submit" name="submit" id="btn-formdetailssave" onclick="saveformproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
      <span>
        <i class="fa fa-check"></i>
        <span>Save</span>
      </span>
    </button>
  </div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="formtextheading-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Header and Text Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
    <!--       <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li> -->
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="ht_labelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div id="sublab" class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="ht_sublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Text Alignment</label>
              <select class="form-control m-input m-input--solid" id="textalignment">
                <option>Left</option>
                <option>Center</option>
                <option>Right</option>
              </select>
            </div>
          </div>
          <div class="col-lg-5 formpadding">
            <div class="m-form__group form-group row">
              <label>Text Size</label>
              <select class="form-control m-input m-input--solid" id="textsize">
                <option>Default</option>
                <option>Large</option>
                <option>Small</option>
              </select>
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" name="submit" id="" class="btn btn-danger m-btn m-btn--icon">
      <span>
        <i class="fa fa-remove"></i>
        <span>Discard</span>
      </span>
    </button>
    <button type="submit" name="submit" id="btn-htdetailsave" onclick="savehtproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
      <span>
        <i class="fa fa-check"></i>
        <span>Save</span>
      </span>
    </button>
  </div>
</div>
</div>
</div>
<!--end::Modal-->


<!--begin::Modal-->
<div class="modal fade" id="formoptionlist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Radio, Checkbox and Dropdown Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="optionlabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="optionsublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
          </div>
          <div class="col-lg-5 formpadding">
           <div class="m-form__group form-group row">
            <label class="col-3 col-form-label">Set to required</label>
            <div class="col-6">
              <span class="m-switch m-switch--lg m-switch--icon">
                <label>
                  <input class="requirecheckbox" id="optionrequirecheckbox" type="checkbox" name="">
                  <span></span>
                </label>
              </span>
            </div>
          </div>
          <br>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="submit" name="submit" id="" class="btn btn-danger m-btn m-btn--icon">
    <span>
      <i class="fa fa-remove"></i>
      <span>Discard</span>
    </span>
  </button>
  <button type="submit" name="submit" id="btn-oldetailsave" onclick="saveolproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
    <span>
      <i class="fa fa-check"></i>
      <span>Save</span>
    </span>
  </button>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="formdatetime-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Date and Time Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
    <!--       <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li> -->
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="dtlabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="dtsublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Set Placeholder</label>
              <input type="text" id="dtplaceholderform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Your Placeholder...">
            </div>
          </div>
          <div class="col-lg-5 formpadding">
            <div class="m-form__group form-group row">
              <label class="col-3 col-form-label">Set to required</label>
              <div class="col-6">
                <span class="m-switch m-switch--lg m-switch--icon">
                  <label>
                    <input class="requirecheckbox" id="dtrequirecheckbox" type="checkbox" name="">
                    <span></span>
                  </label>
                </span>
              </div>
            </div>
          <!--   <div class="m-form__group form-group row">
              <label>Text Minimum / Number Range From</label>
              <input id="textmax" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div>
            <div class="m-form__group form-group row">
              <label>Text Maximum / Number Range To</label>
              <input id="textmin" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div> -->
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" name="submit" id="" class="btn btn-danger m-btn m-btn--icon">
      <span>
        <i class="fa fa-remove"></i>
        <span>Discard</span>
      </span>
    </button>
    <button type="submit" name="submit" id="btn-formdtdetailssave" onclick="saveformdtproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
      <span>
        <i class="fa fa-check"></i>
        <span>Save</span>
      </span>
    </button>
  </div>
</div>
</div>
</div>
<!--end::Modal-->

<script type="text/javascript">
  var timer=null;
  var process_id=$("#hiddenprocessID").val();
  $(function(){
   displaytasks();
   // $('.summernote').summernote();
 });
  function saveformproperties_changes(element){
    var taskid=$(element).data('id');
    var subtaid=$("#subtaskformid").val();
    var labelcontent=$("#labelform").val();
    var sublabelcontent=$("#sublabelform").val();
    var placeholdercontent=$("#placeholderform").val();
    var max=$("#textmax").val();
    var min=$("#textmin").val();
    var require="";
    if ($("#requirecheckbox").is(':checked'))
    {
      require="yes";
    }
    else
    {
      require="no";
    }
    $.ajax({
     type: "POST",
     url: "<?php echo base_url(); ?>process/update_inputformdetails",
     data: {
      subTask_ID: subtaid,
      complabel: labelcontent,
      sublabel: sublabelcontent,
      placeholder: placeholdercontent,
      required: require,
      min:min,
      max:max
    },
    cache: false,
    success: function (res) {
     var result = JSON.parse(res.trim());
     display_allcomponents(taskid);
     swal({
      position: 'center',
      type: 'success',
      title: 'Successfully Updated Properties!',
      showConfirmButton: false,
      timer: 1500
    });
     $("#formproperties-modal").modal('hide');
   }
 }); 
  }
  function savehtproperties_changes(element){
    var taskid=$(element).data('id');
    var subtaid=$("#subtaskformid").val();
    var htlabelform=$("#ht_labelform").val();
    var htsublabelform=$("#ht_sublabelform").val();
    var textalignment=$("#textalignment").val();
    var textsize=$("#textsize").val();
    $.ajax({
     type: "POST",
     url: "<?php echo base_url(); ?>process/update_headingtextdetails",
     data: {
      subTask_ID: subtaid,
      complabel: htlabelform,
      sublabel: htsublabelform,
      text_alignment: textalignment,
      text_size: textsize
    },
    cache: false,
    success: function (res) {
     var result = JSON.parse(res.trim());
     display_allcomponents(taskid);
     swal({
      position: 'center',
      type: 'success',
      title: 'Successfully Updated Properties!',
      showConfirmButton: false,
      timer: 1500
    });
     $("#formtextheading-modal").modal('hide');
   }
 }); 
  }
  function saveolproperties_changes(element){
   var taskid=$(element).data('id');
   var subtaid=$("#subtaskformid").val();
   var labelcontent=$("#optionlabelform").val();
   var sublabelcontent=$("#optionsublabelform").val();
   var require="";
   if ($("#optionrequirecheckbox").is(':checked'))
   {
    require="yes";
  }
  else
  {
    require="no";
  }
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/update_optiondetails",
   data: {
    subTask_ID: subtaid,
    complabel: labelcontent,
    sublabel: sublabelcontent,
    required: require,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   display_allcomponents(taskid);
   swal({
    position: 'center',
    type: 'success',
    title: 'Successfully Updated Properties!',
    showConfirmButton: false,
    timer: 1500
  });
   $("#formoptionlist-modal").modal('hide');
 }
}); 
}
function saveformdtproperties_changes(element){
 var taskid=$(element).data('id');
 var subtaid=$("#subtaskformid").val();
 var labelcontent=$("#dtlabelform").val();
 var sublabelcontent=$("#dtsublabelform").val();
 var placeholdercontent=$("#dtplaceholderform").val();
 var require="";
 if ($("#dtrequirecheckbox").is(':checked'))
 {
  require="yes";
}
else
{
  require="no";
}
$.ajax({
 type: "POST",
 url: "<?php echo base_url(); ?>process/update_datetimedetails",
 data: {
  subTask_ID: subtaid,
  complabel: labelcontent,
  sublabel: sublabelcontent,
  placeholder: placeholdercontent,
  required: require
},
cache: false,
success: function (res) {
 var result = JSON.parse(res.trim());
 display_allcomponents(taskid);
 swal({
  position: 'center',
  type: 'success',
  title: 'Successfully Updated Properties!',
  showConfirmButton: false,
  timer: 1500
});
 $("#formdatetime-modal").modal('hide');
}
}); 
}

function add_form(element){
  var taskid=$("#taskformid").val();
  var stringx="";
  var componentType=0;
  var id=$(element).data('id');
  if(id=='forminputtext'){
    componentType=1;
  }
  else if(id=='formtextarea'){
    componentType=2;
  }
  else if(id=='formheading'){
    componentType=3;  
  }
  else if(id=='formtext'){
    componentType=4;
  }
  else if(id=='formnumber'){
    componentType=5; 
  }
  else if(id=='formsingle'){
    componentType=6;
  }
  else if(id=='formdropdown'){
    componentType=7;
  }
  else if(id=='formmulti'){
    componentType=8;
  }
  else if(id=='formdivider'){
    componentType=9;
  }
  else if(id=='formdate'){
    componentType=10;  
  }
  else if(id=='formspinner'){
    componentType=11;   
  }
  else if(id=='formemail'){
    componentType=12;
  }
  else if(id=='formtime'){
    componentType=13;
  }
  else if(id=='formdatetime'){
    componentType=14;
  }
  add_component(componentType,taskid);
  $("#add_content-modal").modal('hide');
  $("#formaddcontent"+taskid).append(stringx);
}
function add_component(componentType,taskid){
  var sequencenum=$("#sequenceid").val();
  var seqnum=0;
  if(sequencenum==0){
    seqnum=1;
  }else{
    seqnum=sequencenum;
    seqnum++;
  }
  console.log(seqnum);
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/add_subtask",
   data: {
    component_ID: componentType,
    task_ID: taskid,
    sequence: seqnum
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    var data = res;
    var subtaskid= data.subTask_ID;
    fetch_lastinsertedform(subtaskid);
  }
});
}
function fetch_lastinsertedform(subtaskid){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_subtask",
    data: {
      subTask_ID:subtaskid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      var data = res;
      var stid= data.task_ID;
      var componentid= data.component_ID;
      addingform(stid,subtaskid,componentid);
    }
  });
}
//FORMS
function addingform(taskid,subtaskid,componentid){
  var stringx="";
  var stringcomp="";
  if(componentid==1){
    stringx+="<div id='"+subtaskid+"'class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type a Label</div>";
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill formdistance' placeholder='Something will be typed here...' disabled>";
    stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formproperties-modal'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(componentid==2){
    stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label formdistance'>Type a Label</div>";
    stringx+="<form>";
    stringx+="<textarea class='form-control m-input m-input--air m-input--pill' placeholder='Long text will be typed here...' disabled></textarea>";
    stringx+="</form>";
    stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button  data-component='"+componentid+"' data-id='"+subtaskid+"' data-toggle='modal' data-target='#formproperties-modal' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' onclick='fetch_inputtypeformdetails(this)'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(componentid==3){
    stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='labelheading'> Heading</div>";
    stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' style='font-size:1em;color:#b9bbc1'> Type a Sub label...</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button data-id='"+subtaskid+"' data-toggle='modal' data-target='#formtextheading-modal' onclick='fetch_headertextdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(componentid==4){
   stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
   stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'> Click to edit this text...</div>";
   stringx+="<div class='input-group-append formdistance'>";
   stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' data-toggle='modal' data-target='#formtextheading-modal' onclick='fetch_headertextdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
   stringx+="<i class='fa fa-edit'></i>";
   stringx+="</button>";
   stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
   stringx+="<i class='fa fa-close'></i>";
   stringx+="</button>";
   stringx+="</div>";
   stringx+="</div>";
 }
 else if(componentid==5){
   stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
   stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Number Label</div>";
   stringx+="<input type='number' class='form-control m-input m-input--air m-input--pill' placeholder='A number or numbers will be typed here...' disabled>";
   stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
   stringx+="<div class='input-group-append formdistance'>";
   stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formproperties-modal'>";
   stringx+="<i class='fa fa-edit'></i>";
   stringx+="</button>";
   stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
   stringx+="<i class='fa fa-close'></i>";
   stringx+="</button>";;
   stringx+="</div>";

   stringx+="</div>";
 }
 else if(componentid==6){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label formdistance'>Type a Label</div>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div id='radiolist"+subtaskid+"'class='row m-radio-list formdistance'>";
  stringx+="</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<a href='#' data-id='"+subtaskid+"' onclick='add_componentoption(this)'>";
  stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Option</label>";
  stringx+="</a>";
  stringx+="</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
  radio_option(subtaskid);
}
else if(componentid==7){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type a Label</div>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";

  stringx+="<div class='form-group m-form__group'>";
  stringx+="<select class='form-control formdistance m-input m-input--air m-input--pill' id='exampleSelect1' disabled>";
  stringx+="<option></option>";
  stringx+="</select>";
  stringx+="</div>";

  stringx+="<div class='form-group m-form__group formdistance'>";
  stringx+="<ul id='itemlist"+subtaskid+"' class='list-group'>";
  stringx+="</ul>";

  stringx+="</div>";

  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<a href='#' data-id='"+subtaskid+"' onclick='add_componentoption(this)'>";
  stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add List</label>";
  stringx+="</a>";
  stringx+="</div>";

  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button  data-component='"+componentid+"' data-id='"+subtaskid+"' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";

  stringx+="</div>";
  dropdown_list(subtaskid);
}
else if(componentid==8){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type a Label</div>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";

  stringx+="<div id='checkoption"+subtaskid+"' class='row m-checkbox-list formdistance'>";
  stringx+="</div>";

  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<a href='#' data-id='"+subtaskid+"' onclick='add_componentoption(this)'>";
  stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Checkbox</label>";
  stringx+="</a>";
  stringx+="</div>";

  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
  checkbox_option(subtaskid);
}
else if(componentid==9){
 stringx+="<div id='"+subtaskid+"' class='col-lg-12 distance'>";
 stringx+="<hr>";
 stringx+="<div class='input-group-append formdistance'>";
 stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-close'></i>";
 stringx+="</button>";
 stringx+="</div>";
 stringx+="</div>";
}
else if(componentid==10){
 stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type Date Label</div>";
 stringx+="<div class='input-group formdistance'>";
 stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
 stringx+="<div class='input-group-append'>";
 stringx+="<span class='input-group-text'>";
 stringx+="<i class='la la-calendar-check-o'></i>";
 stringx+="</span>";
 stringx+="</div>";
 stringx+="</div>";
 stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true'class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
 stringx+="<div class='input-group-append formdistance'>";
 stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
 stringx+="<i class='fa fa-edit'></i>";
 stringx+="</button>";
 stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-close'></i>";
 stringx+="</button>";
 stringx+="</div>";
 stringx+="</div>";
}
else if(componentid==11){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type Label</div>";
  stringx+="<input class='form-control m-input m-input--air m-input--pill formdistance' type='number' value='0' min='0' max='100' step='1'/>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formproperties-modal'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(componentid==12){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div  data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Email Label</div>";
  stringx+="<input type='email' class='form-control m-input m-input--air m-input--pill' placeholder='An email will be typed here...' disabled>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formproperties-modal'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(componentid==13){
 stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div  data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type Time Label</div>";
 stringx+="<div class='input-group formdistance'>"
 stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
 stringx+="<div class='input-group-append'>";
 stringx+="<span class='input-group-text'>";
 stringx+="<i class='la la-clock-o'></i>";
 stringx+="</span>";
 stringx+="</div>";
 stringx+="</div>";
 stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
 stringx+="<div class='input-group-append formdistance'>";
 stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
 stringx+="<i class='fa fa-edit'></i>";
 stringx+="</button>";
 stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-close'></i>";
 stringx+="</button>";
 stringx+="</div>";
 stringx+="</div>";
}
else if(componentid==14){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div  data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type Date and Time Label</div>";
  stringx+="<div class='input-group formdistance'>";
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date and time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  stringx+="<div class='input-group-append'>";
  stringx+="<span class='input-group-text'>";
  stringx+="<i class='la la-calendar-check-o'></i>";
  stringx+="</span>";
  stringx+="</div>";
  stringx+="</div>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button data-component='"+componentid+"' data-id='"+subtaskid+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
$("#add_content-modal").modal('hide');
$("#formaddcontent"+taskid).append(stringx);
countallforms(taskid);
}

function countallforms(taskid){
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/fetch_allsubtask",
  data: {
    task_ID:taskid
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   var countform=Object.keys(result).length;
   console.log('print'+countform);
   $("#sequenceid").val( countform);
 }
});
}
function display_allcomponents(taskid){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_allsubtask",
    data: {
      task_ID:taskid
    },
    cache: false,
    success: function (res) {
     var result =JSON.parse(res.trim());
     var stringx ="";
     $.each(result, function (key, item) {
      console.log(item.component_ID);
      if(item.component_ID==1){
        stringx+="<div id='"+item.subTask_ID+"'class='col-lg-12 evenadding distance'>";
        stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' contenteditable='true' class='label' onkeyup='autosavelabel(this)'>";
        if(item.complabel){
          stringx+=item.complabel;
        }else{
          stringx+="Type a label";
        }
        stringx+="</div>";
        if(item.placeholder){
          stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill formdistance' placeholder='"+item.placeholder+"' disabled>";
        }else{
          stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill formdistance' placeholder='Something will be typed here...' disabled>";
        }
        if(item.sublabel){
          stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
        }else{
          stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
        }
        stringx+="<div class='input-group-append formdistance'>";
        stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formproperties-modal'>";
        stringx+="<i class='fa fa-edit'></i>";
        stringx+="</button>";
        stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-close'></i>";
        stringx+="</button>";
        stringx+="</div>";
        stringx+="</div>";
      }
      else if(item.component_ID==2){
        stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
        stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' contenteditable='true' class='label' onkeyup='autosavelabel(this)'>";
        if(item.complabel!=null){
          stringx+=item.complabel;
        }else{
          stringx+="Type a label";
        }
        stringx+="</div>";
        stringx+="<form>";

        if(item.placeholder){
         stringx+="<textarea class='form-control m-input m-input--air m-input--pill' placeholder='"+item.placeholder+"' disabled></textarea>";
       }else{
         stringx+="<textarea class='form-control m-input m-input--air m-input--pill' placeholder='Long text will be typed here...' disabled></textarea>";
       }
       stringx+="</form>";
       if(item.sublabel){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
      }else{
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      }
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formproperties-modal' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' onclick='fetch_inputtypeformdetails(this)'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
  //HEADING
  else if(item.component_ID==3){
    stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='labelheading'>";
    if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Heading";
    }
    stringx+="</div>";
    if(item.sublabel){
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
    }else{
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    }
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formtextheading-modal' onclick='fetch_headertextdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
      //TEXT
      else if(item.component_ID==4){
        stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
        stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>"; 
        if(item.complabel){
          stringx+=item.complabel;
        }else{
          stringx+="Click to edit this text...";
        }
        stringx+="</div>";
        stringx+="<div class='input-group-append formdistance'>";
        stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formtextheading-modal' onclick='fetch_headertextdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-edit'></i>";
        stringx+="</button>";
        stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-close'></i>";
        stringx+="</button>";
        stringx+="</div>";
        stringx+="</div>";
      }
      else if(item.component_ID==5){
       stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
       stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
       if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Number label";
      }
      stringx+="</div>";
      if(item.placeholder){
        stringx+="<input type='number' class='form-control m-input m-input--air m-input--pill' placeholder='"+item.placeholder+"' disabled>";
      }else{
        stringx+="<input type='number' class='form-control m-input m-input--air m-input--pill' placeholder='A number or numbers will be typed here...' disabled>";
      }
      if(item.sublabel){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
      }else{
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      }
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formproperties-modal'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
    else if(item.component_ID==6){
      stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label formdistance'>";
      if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Type a label";
      }
      stringx+="</div>";
      if(item.sublabel){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
      }else{
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      }
      stringx+="<div id='radiolist"+item.subTask_ID+"' class='row m-radio-list formdistance'>";
      stringx+="</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
      stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Option</label>";
      stringx+="<a>";
      stringx+="</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
      radio_option(item.subTask_ID);
    }
    else if(item.component_ID==7){
      stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
      if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Type a label";
      }
      stringx+="</div>";
      if(item.sublabel){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
      }else{
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      }
      stringx+="<div class='form-group m-form__group'>";
      stringx+="<select class='form-control formdistance m-input m-input--air m-input--pill' id='exampleSelect1' disabled>";
      stringx+="<option></option>";
      stringx+="</select>";
      stringx+="</div>";
      stringx+="<div class='form-group m-form__group formdistance'>";
      stringx+="<ul id='itemlist"+item.subTask_ID+"' class='list-group'>";
      stringx+="</ul>";
      stringx+="</div>";

      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
      stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add List</label>";
      stringx+="</a>";
      stringx+="</div>";

      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
      dropdown_list(item.subTask_ID);
    }
    else if(item.component_ID==8){
      stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
      if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Type a label";
      }
      stringx+="</div>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      stringx+="<div id='checkoption"+item.subTask_ID+"' class='row m-checkbox-list formdistance'>";
      stringx+="</div>";

      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
      stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Checkbox</label>";
      stringx+="<a>";
      stringx+="</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
      checkbox_option(item.subTask_ID);
    }
    else if(item.component_ID==9){
     stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 distance'>";
     stringx+="<hr>";
     stringx+="<div class='input-group-append formdistance'>";
     stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
     stringx+="<i class='fa fa-close'></i>";
     stringx+="</button>";
     stringx+="</div>";
     stringx+="</div>";
   }
   else if(item.component_ID==10){
     stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
     stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
     if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Type Date label";
    }
    stringx+="</div>";
    stringx+="<div class='input-group formdistance'>";

    if(item.placeholder){
      stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='"+item.placeholder+"' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
    }else{
      stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
    }    
    stringx+="<div class='input-group-append'>";
    stringx+="<span class='input-group-text'>";
    stringx+="<i class='la la-calendar-check-o'></i>";
    stringx+="</span>";
    stringx+="</div>";
    stringx+="</div>";
    if(item.sublabel){
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
    }else{
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    }
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(item.component_ID==11){
    stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
    if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Type a label";
    }
    stringx+="</div>";
    stringx+="<input class='form-control m-input m-input--air m-input--pill formdistance' type='number' value='0' min='0' max='100' step='1'/>";

    if(item.sublabel){
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
    }else{
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    }
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formproperties-modal'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(item.component_ID==12){
    stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
    if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Email Label";
    }
    stringx+="</div>";

    if(item.placeholder){
     stringx+="<input type='email' class='form-control m-input m-input--air m-input--pill' placeholder='"+item.placeholder+"' disabled>";
   }else{
    stringx+="<input type='email' class='form-control m-input m-input--air m-input--pill' placeholder='An email will be typed here...' disabled>";
  }
  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  }
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formproperties-modal'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==13){
 stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
 if(item.complabel!=null){
  stringx+=item.complabel;
}else{
  stringx+="Type Time Label";
}
stringx+="</div>";
stringx+="<div class='input-group formdistance'>"
if(item.placeholder){
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='"+item.placeholder+"' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
}else{
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
} 
stringx+="<div class='input-group-append'>";
stringx+="<span class='input-group-text'>";
stringx+="<i class='la la-clock-o'></i>";
stringx+="</span>";
stringx+="</div>";
stringx+="</div>";
if(item.sublabel){
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
}else{
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
}
stringx+="<div class='input-group-append formdistance'>";
stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
stringx+="<i class='fa fa-edit'></i>";
stringx+="</button>";
stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
else if(item.component_ID==14){
  stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
  if(item.complabel){
    stringx+=item.complabel;
  }else{
    stringx+="Type Date and Time Label";
  }
  stringx+="</div>";
  stringx+="<div class='input-group formdistance'>";
  if(item.placeholder){
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='"+item.placeholder+"' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  }else{
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date and time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  } 
  stringx+="<div class='input-group-append'>";
  stringx+="<span class='input-group-text'>";
  stringx+="<i class='la la-calendar-check-o'></i>";
  stringx+="</span>";
  stringx+="</div>";
  stringx+="</div>";
  if(item.sublabel){
   stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
 }else{
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
}
stringx+="<div class='input-group-append formdistance'>";
stringx+="<button data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
stringx+="<i class='fa fa-edit'></i>";
stringx+="</button>";
stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
});
$("#add_content-modal").modal('hide');
$("#formaddcontent"+taskid).html(stringx);
}
});
}
//fetch input and textarea
function fetch_inputtypeformdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      console.log(data.required);
      if(data.required=="yes"){
        $(".requirecheckbox").prop("checked",true);
      }
      else{
        $(".requirecheckbox").prop("checked",false);
      }
      $("#labelform").val(data.complabel);
      $("#sublabelform").val(data.sublabel);
      $("#placeholderform").val(data.placeholder);
      $("#textmin").val(data.min);
      $("#textmax").val(data.max);
      $("#btn-formdetailssave").data('id',data.task_ID);
    }
  });
}
//fetch text and heading
function fetch_headertextdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      console.log('header:'+subtid);
      $("#ht_labelform").val(data.complabel);
      if(compid==3){
        $("#ht_sublabelform").val(data.sublabel);
      }else if(compid==4){
        $("#sublab").hide();
      }
      $("#textalignment").val(data.text_alignment).prop('selected', true);
      $("#textsize").val(data.text_size).prop('selected', true);
      $("#btn-htdetailsave").data('id',data.task_ID);
    }
  });
}
//fetch radio button, checkbox and dropdown
function fetch_optionlistdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      if(data.required=="yes"){
        $(".requirecheckbox").prop("checked",true);
      }
      else{
        $(".requirecheckbox").prop("checked",false);
      }
      $("#optionlabelform").val(data.complabel);
      $("#optionsublabelform").val(data.sublabel);   
      $("#btn-oldetailsave").data('id',data.task_ID);
    }
  });
}
//fetch date and time
function fetch_datetimeformdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      console.log(data.required);
      if(data.required=="yes"){
        $(".requirecheckbox").prop("checked",true);
      }
      else{
        $(".requirecheckbox").prop("checked",false);
      }
      $("#dtlabelform").val(data.complabel);
      $("#dtsublabelform").val(data.sublabel);
      $("#dtplaceholderform").val(data.placeholder);
      $("#btn-formdtdetailssave").data('id',data.task_ID);
    }
  });
}

//RADIO, MULTIPLE, DROPDOWN
function radio_option(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' class='m-radio m-radio--bold m-radio--state-primary' contenteditable='true'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Double click and type option...";
   }
   stringcomp+="<input type='radio' name='formrad' value='' disabled>";
   stringcomp+="<span></span>";
   stringcomp+="</label>";
   stringcomp+="</div>";
   stringcomp+="<div class='col-lg-2'>";
   stringcomp+="<a href='#' data-componentid='6' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)' ><i class='fa fa-remove'></i></a>";
   stringcomp+="</div>";
 });
   $("#radiolist"+subtaskid).html(stringcomp);
 }
});
}
function checkbox_option(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' class='m-checkbox m-checkbox--bold m-checkbox--state-brand' contenteditable='true'>";
    stringcomp+="<input type='checkbox' disabled>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
    stringcomp+="Type Checklist Label...";
  }
  stringcomp+="<span></span>";
  stringcomp+="</label>";
  stringcomp+="</div>";
  stringcomp+="<div class='col-lg-2'>";
  stringcomp+="<a href='#' data-componentid='8' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)'><i class='fa fa-remove'></i></a>";
  stringcomp+="</div>";
});
   $("#checkoption"+subtaskid).html(stringcomp);
 }
});
}
function dropdown_list(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<li class='list-group-item justify-content-between'>";
    stringcomp+="<span id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' contenteditable='true'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Item list...";
   }
   stringcomp+="</span>";
   stringcomp+="<a href='#' data-componentid='7' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)'><i class='float-right fa fa-remove'></i></a>";
   stringcomp+="</li>";
 });
   $("#itemlist"+subtaskid).html(stringcomp);
 }
});
}
//END
//Add option
function add_componentoption(element){
  var subtaskid=$(element).data('id');
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/add_subtaskcomponent",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var res = JSON.parse(res.trim());
   var data = res;
   var component=data.component_ID;
   console.log('success');
   console.log(component);
   if(component==6){
    radio_option(subtaskid);
  }
  else if(component==7){
    dropdown_list(subtaskid);
  }
  else if(component==8){
    checkbox_option(subtaskid);
  }
}
});
}
function delete_option(element){
  var compsubtaskid=$(element).data('id');
  var subtask=$(element).data('subtaskid');
  var componentid=$(element).data('componentid');
  $.ajax({
    type: "POST",
    url:"<?php echo base_url(); ?>process/delete_option",
    data: {
      componentSubtask_ID: compsubtaskid,
    },
    cache: false,
    success: function () {
      if(componentid=='6'){
        radio_option(subtask);
      }
      else if(componentid=='7'){
        dropdown_list(subtask);
      }
      else if(componentid=='8'){
        checkbox_option(subtask);
      }
    }
  });
}
function delete_component(element){
  var subtaskid=$(element).data('id');
  var task_id=$(element).data('compid');
  swal({
    title: 'Are you sure you want to delete this form element?',
    text: "",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url:"<?php echo base_url(); ?>process/delete_component",
        data: {
          subTask_ID: subtaskid,
        },
        cache: false,
        success: function () {
         display_allcomponents(task_id);
         swal(
          'Deleted!',
          'Successfully deleted.',
          'success'
          )
       },
       error: function (res){
        swal(
          'Oops!',
          'Please Check!',
          'error'
          )
      }
    });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deletion has been cancelled',
        'error'
        )
    }
  });
}
function displaytasks(){
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/display_tasks",
   data: {
    process_ID: process_id,
  },
  cache: false,
  success: function (res) {
    var result = JSON.parse(res.trim());
    var stringx = "";
    var stringcon= "";
    $.each(result, function (key, item) {
     stringx += "<div class='input-group m-form__group sortable' id='addcheckbox'>";
     stringx +="<div class='input-group-prepend'>";
     stringx +="<span class='input-group-text'>";
     stringx +="<label class=''>";
     stringx +="<span></span>";
     stringx +="</label>";
     stringx +="</span>";
     stringx +="</div>";
     if(item.taskTitle==null){
       stringx +="<input data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value=''>"; 
     }else{
      stringx +="<input data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask sampletask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value='"+item.taskTitle+"'>";
    } 
    stringx +="<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='"+item.process_ID+"'>";
    stringx +="<input type='hidden' id='htaskID' name='htaskID' value='"+item.task_ID+"'>";
    stringx +="<div class='input-group-append'>";
    stringx +="<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
    stringx +="<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
    stringx +="<i class='fa fa-delete'></i>";
    stringx +="</a>";
    stringx +="<div class='m-dropdown__wrapper'>";
    stringx +="<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
    stringx +="<div class='m-dropdown__inner'>";
    stringx +="<div class='m-dropdown__body'> ";             
    stringx +="<div class='m-dropdown__content'>";
    stringx +="<ul class='m-nav'>";
    stringx +="<li class='m-nav__item'>";
    stringx +="<a data-id='"+item.task_ID+"' onclick='deleteTask(this)' class='m-nav__link'>";
    stringx +="<i class='m-nav__link-icon fa fa-trash'></i>";
    stringx +="<span class='m-nav__link-text'>Delete</span>";
    stringx +="</a>";
    stringx +="</li>";
    stringx +="<li class='m-nav__item'>";
    stringx +="<a href='#' class='m-nav__link'>";
    stringx +="<i class='m-nav__link-icon fa fa-angle-double-right'></i>";
    stringx +="<span class='m-nav__link-text'>Task details</span>";
    stringx +="</a>";
    stringx +="</li>";
    stringx +="</ul>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";

    stringcon +="<div id='formcontent"+item.task_ID+"'class='m-portlet__body formcontent' style='height:480px;overflow-y:scroll; overflow-x:auto'>";
    stringcon +="<div id='formaddcontent"+item.task_ID+"' class='row droparea_append'>";
    stringcon +="</div>";
    stringcon += "<div class='row droparea_outside'>";
    stringcon += "<div class='col-lg-12 droparea' >";
    stringcon += "<a href='#' data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='taskid(this)' class='btn m-btn m-btn--icon m-btn--pill m-btn--air' data-toggle='modal' data-target='#add_content-modal'>";
    stringcon += "<span>";
    stringcon += "<i class='fa fa-plus-circle'></i>";
    stringcon += "<span>Add Content</span>";
    stringcon += "</span>";
    stringcon += "</a>";
    stringcon += "</div>";
    stringcon += "</div>";
    stringcon += "</div>";
  });

    $("#checkgroup").html(stringx);
    $(".inputtask:first").focus();
    $(".inputtask:first").css({"background-color":"#51aff9","color":"white"});
    $(".inputtask:first").addClass("placeholdercolor");
    var firstaskid=$(".inputtask:first").attr('id');
    display_allcomponents(firstaskid);
    $("#contentdiv").append(stringcon);
    $(".formcontent").hide();
    $(".formcontent:first").show();
  }
});
}
function displayformdiv(element){
  var taskid=$(element).data('id');
  display_allcomponents(taskid);
  $(".formcontent").hide();
  $("#formcontent"+taskid).show();
  $(".inputtask").css({"background-color":"","color":""});
  $(".inputtask").removeClass("placeholdercolor");
  $("input#"+taskid).css({"background-color":"#51aff9","color":"white"});
  $("input#"+taskid).addClass("placeholdercolor");
}
function taskid(element){
  var taskid=$(element).data('id');
  $("#taskformid").val(taskid);
}
//NOTIFS
function doNotif() {
 $.notify("Auto-saved!");
 setTimeout(function() {
  $.notifyClose('top-right');
}, 1000);
}
function doNotif2() {
 $.notify("Saved!");
 setTimeout(function() {
  $.notifyClose('top-right');
}, 1000);
}
$('#addchecklistName').keyup(function () {
  var max = 40;
  var len = $(this).val().length;
  if (len >= max) {
    $('#charNum').text('You have reached the limit');
  } else {
    var char = max - len;
    $('#charNum').text(char+'/40 characters remaining');
  }
});
function runChecklist(element){
  console.log(element);
  console.log($(element).data('id'));
  var checklistTitle = $("#addchecklistName").val();
  var process_ID = $("#fetchprocessid").val();
  if ($.trim($("#addchecklistName").val()) === "") {
    e.preventDefault();
  }else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_checklistdetails",
      data: {
        checklist_ID: $(element).data('id'),
        checklistTitle:checklistTitle,
        process_ID:process_ID
      },
      cache: false,
      success: function (res) {
        res = JSON.parse(res.trim());
        swal({
          position: 'center',
          type: 'success',
          title: 'Checklist On the run!',
          showConfirmButton: false,
          timer: 1500
        });
        window.location.href = "<?php echo base_url('process')?>";
      }
    });
  }
}
//autosave function
function autosaveoptionlabel(element){
  var compsubtaskid=$(element).data('id');
  var optionlabel=$("#optiondetails"+compsubtaskid).text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_optiondetails",
    data: {
      componentSubtask_ID: compsubtaskid,
      compcontent: optionlabel
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      clearTimeout(timer); 
      timer = setTimeout(doNotif, 1000)
    },
    error: function (res){
      console.log(res);
    }
  });
}
function autosavesublabel(element){
 var subtaskid=$(element).data('id');
 var sublabel=$("#sublabel"+subtaskid).text();
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/add_sublabeldetails",
  data: {
    subTask_ID: subtaskid,
    sublabel: sublabel
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    console.log(res);
    clearTimeout(timer); 
    timer = setTimeout(doNotif, 1000)
  },
  error: function (res){
    console.log(res);
  }
});
}
function autosavelabel(element){
  var subtaskid=$(element).data('id');
  var label=$("#label"+subtaskid).text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_subtaskdetails",
    data: {
      subTask_ID: subtaskid,
      complabel: label
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      clearTimeout(timer); 
      timer = setTimeout(doNotif, 1000)
    },
    error: function (res){
      console.log(res);
    }
  });
}
function autosavetask(element){
 var asid=$(element).data('id');
 console.log(asid);
 var taskTitle = $("input#"+asid).val();
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/add_taskdetails",
  data: {
    task_ID: asid,
    taskTitle:taskTitle
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    console.log(res);
    clearTimeout(timer); 
    timer = setTimeout(doNotif, 1000)
  },
  error: function (res){
    console.log(res);
  }
});
}
function addtask(element){
  var processID=$(element).data('id');
  console.log(processID);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_onetask",
    data: {
      process_ID: $(element).data('id')
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
      displaytasks();
      // window.location.href = "<?php echo base_url('process/checklist')?>"+"/"+processID;
    },
    error: function (res){
      console.log(res);
    }
  });
}
function deleteTask(element){
  var processID = $("#hiddentaskID").val();
  swal({
    title: 'Are you sure?',
    text: "All under this task will be deleted, too.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_task",
        data: {
          task_ID: $(element).data('id')
        },
        cache: false,
        success: function () {
         displaytasks();
         swal(
          'Deleted!',
          'Task successfully deleted.',
          'success'
          )
       },
       error: function (res){
        swal(
          'Oops!',
          'You cannot delete this task ',
          'error'
          )
      }
    });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deleting a process has been cancelled',
        'error'
        )
    }
  });
  console.log($(element).data('id'));
}

</script>



