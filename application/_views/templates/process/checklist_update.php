<style type="text/css">
.active:focus {
  background: #e9eeef;
  border-color: #00c5dc;
  color: #3f4047;
}
.activeinput:focus{
 background: #1d86da;
}
.droparea{
  text-align: center;
}
.droparea_outside{
  border-style: dashed;
  padding: 10px;
  margin: 10px;
  border-color:#a5a6ad;
}
.droparea_append{
  padding: 10px;
  margin: 10px;
}
.colorgreen{
  color: green;
  font-style: italic;
  font-size:0.9em;
}
.evenadding{
  padding: 15px;
  background:#f1f0f0;
}
.oddadding{
  padding: 15px;
  background: #f9ecec;
}
.buttonform{
  text-align: center;
}
.distance{
  margin-bottom: 20px;
}
.formdistance{
  margin-top: 10px;
}
.taskdistance{
  margin-top: 15px;
}
.label{
  font-size:1.2em;
  color:#00c5dc;
}
.labelheading{
  font-size:1.4em;
  font-weight: bold;
  color:#00c5dc;
}
.placeholdercolor::placeholder { 
  color: #d8dce8;
  opacity: 1;
}
/*this is to test*/
body.dragging, body.dragging * {
  cursor: move !important;
}
.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}
ol.example li.placeholder {
  position: relative;
  /** More li styles **/
}
ol.example li.placeholder:before {
  position: absolute;
  /** Define arrowhead **/
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>         
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo base_url('process')?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url('process/checklist_pending').'/'.$processinfo->process_ID;?>" class="m-nav__link">
              <span class="m-nav__link-text"><?php echo $processinfo->processTitle;?></span>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url('process/checklist_answerable').'/'.$processinfo->process_ID.'/'.$checklist->checklist_ID;?>" class="m-nav__link">
              <span class="m-nav__link-text"><?php echo $checklist->checklistTitle;?></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet">
          <!--begin: Portlet Head-->
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                  <a href="#" onclick="doNotif2()" class="btn btn-success btn-sm m-btn m-btn--icon">
                    <span>
                      <i class="fa fa-save"></i>
                      <span>Save</span>
                    </span>
                  </a>
                  <!-- <input type="text" id="datetime" readonly> -->
                  <?php if($processinfo->processstatus_ID=="10"){?>
                    <a href="#" data-id="<?php echo $processinfo->process_ID;?>" class="btn btn-success btn-sm m-btn m-btn m-btn--icon" data-toggle="modal" data-target="#run_checklist-modal">   <span>
                      <i class="fa fa-play-circle-o"></i>
                      <span>Run checklist</span>
                    </span>
                  </a>
                <?php }?>
              </h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                <div class="input-group m-input-icon m-input-icon--left">          
                  <input readonly="true" type="text" id="datetime" class="form-control form-control-sm m-input" placeholder="Set Due Date">
                  <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-calendar-plus-o"></i></span></span>  
                  <div class="input-group-append">
                    <button class="btn btn-success btn-sm m-btn m-btn--custom" data-id="<?php echo $checklist->checklist_ID;?>" onclick="saveduedate(this)" type="button">Set</button>
                  </div>
                </div>
              </li>   
            </ul>
          </div> 
        </div>
        <!--end: Portlet Head-->
        <div class="m-portlet__body m-portlet__body--no-padding">
          <div class="m-wizard m-wizard--4 m-wizard--brand m-wizard--step-first" id="m_wizard">
            <div class="row m-row--no-padding">
              <div class="col-xl-6 col-lg-12 border">
                <br>
                &nbsp;&nbsp;<h5 style="color:#777;text-align:center;font-size:1.3em;"><strong><?php echo  $processinfo->processTitle;?> Template</strong></h5>
                <div class="taskdistance" style="text-align: center">
                  <a href="#" data-id="<?php echo $processinfo->process_ID;?>" onclick="addtask(this)" class="btn btn-outline-accent btn-sm  m-btn m-btn--icon m-btn--pill">
                    <span>
                      <i class="fa fa-plus"></i>
                      <span>Add Task</span>
                    </span>
                  </a>
                </div>
                <?php if($checklist->dueDate!=NULL){?>
                  <div class="col-xl-12" style="text-align:center;">
                    <i class="fa fa-calendar-check-o"></i><span style="font-size:0.9em;"><strong style="color:#34bfa3"> &nbsp;Due Date &nbsp;</strong>
                      <?php 
                      $datetime=$checklist->dueDate;
                      $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
                      echo sprintf($date->format("D, M j, Y"))."&nbsp; at &nbsp;";
                      echo sprintf($date->format("g:ia"));
                      ?>
                    </span>
                  </div>
                <?php }?>
                <input type="hidden" id="hiddenprocessID" name="hiddenprocessID" value="<?php echo $processinfo->process_ID;?>">
                <div class="m-portlet__body" style="height:410px;overflow-y:scroll; overflow-x:auto;background-color:">
                  <div class="form-group m-form__group" id="checkgroup">
                  </div>
                  <input type="hidden" id="checkID" name="checkID" value="<?php echo $checklist->checklist_ID;?>">
                </div>              
              </div>
              <div id="contentdiv" class="col-xl-6 col-lg-12 border">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!--begin::Modal-->
<div class="modal fade" id="add_content-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create a Form!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Form</a>
          </li>
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li>
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
          <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span>
          <div class="row">
            <input type="hidden" id="taskformid" value="">
            <input type="hidden" id="sequenceid" value="">
            <div class="col-lg-6">
              <button type="button" onclick="add_form(this)" data-id="forminputtext" id="forminputtext" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-font"></i> Short Text Entry</button>
              <button type="button" onclick="add_form(this)" data-id="formtextarea" id="formtextarea" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-font"></i> Long Text Entry</button> 
              <button type="button" onclick="add_form(this)" data-id="formsingle" id="formsingle" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-dot-circle-o"></i> Single Select</button>
              <button type="button" onclick="add_form(this)" data-id="formmulti" id="formmulti" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-check-square-o"></i> Multiple Select</button>
              <button type="button" onclick="add_form(this)" data-id="formdropdown" id="formdropdown" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-toggle-down"></i> Dropdown Select</button>
              <button type="button" onclick="add_form(this)" data-id="formdate" id="formdate" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Date Picker</button>
              <button type="button" onclick="add_form(this)" data-id="formtime" id="formtime" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Time Picker</button>
              <button type="button" onclick="add_form(this)" data-id="formdatetime" id="formdatetime" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Date and Time Picker</button>      
              <br>
            </div>
            <div class="col-lg-6">
              <button type="button" onclick="add_form(this)" data-id="formheading" id="formheading" class="btn btn-accent btn-sm btn-block"><i class="fa fa-header"></i> Heading</button>
              <button type="button" onclick="add_form(this)" data-id="formtext" id="formtext" class="btn btn-accent btn-sm btn-block"><i class="fa fa-text-width"></i> Text</button>
              <button type="button" onclick="add_form(this)" data-id="formnumber" id="formnumber" class="btn btn-accent btn-sm btn-block"><i class="la la-sort-numeric-desc"></i> Number</button> 
              <button type="button" onclick="add_form(this)" data-id="formemail" id="formemail" class="btn btn-accent btn-sm btn-block"><i class="fa fa-toggle-down"></i> Email</button>
              <button type="button" onclick="add_form(this)" data-id="formspinner" id="formspinner" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-sort-numeric-asc"></i> Number Spinner</button>
              <button type="button" onclick="add_form(this)" data-id="formdivider" id="formdivider" class="btn btn-accent btn-sm btn-block"><i class="fa fa-i-cursor"></i> Divider</button>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-insert">
          <div class="row">
           <div class="col-lg-6">
            <button type="button" class="btn btn-accent btn-sm btn-block"><i class="fa fa-photo"></i>&nbsp;&nbsp;Photo</button>
            <button type="button" class="btn btn-accent btn-sm btn-block"><i class="fa fa-video-camera"></i>&nbsp;&nbsp;Video</button>
            <button type="button" class="btn btn-accent btn-sm btn-block"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;File</button> <br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<!--end::Modal-->
</div>
<script type="text/javascript">
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes();
  var dateTime = date+' '+time;
  var pro_id=$("#hiddenprocessID").val();
  var checkid=$("#checkID").val();
  $(function(){
    display_tasks();
    
  });
  $("#datetime").datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    autoclose: true,
    todayBtn: true,
    startDate : dateTime
  });
  function duedate(element){
   var taskid=$(element).data('id');
   $("#taskduedate"+taskid).datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    autoclose: true,
    todayBtn: true,
    startDate : dateTime
  });
 }

 function add_form(element){
  var taskid=$("#taskformid").val();
  var stringx="";
  var componentType=0;
  var id=$(element).data('id');
  if(id=='forminputtext'){
    componentType=1;
  }
  else if(id=='formtextarea'){
    componentType=2;
  }
  else if(id=='formheading'){
    componentType=3;  
  }
  else if(id=='formtext'){
    componentType=4;
  }
  else if(id=='formnumber'){
    componentType=5; 
  }
  else if(id=='formsingle'){
    componentType=6;
  }
  else if(id=='formdropdown'){
    componentType=7;
  }
  else if(id=='formmulti'){
    componentType=8;
  }
  else if(id=='formdivider'){
    componentType=9;
  }
  else if(id=='formdate'){
    componentType=10;  
  }
  else if(id=='formspinner'){
    componentType=11;   
  }
  else if(id=='formemail'){
    componentType=12;
  }
  else if(id=='formtime'){
    componentType=13;
  }
  else if(id=='formdatetime'){
    componentType=14;
  }
  add_component(componentType,taskid);
  $("#add_content-modal").modal('hide');
  $("#formaddcontent"+taskid).append(stringx);
}
function display_tasks(){
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/display_tasks_update",
  data: {
    process_ID: pro_id,
  },
  cache: false,
  success: function (res) {
    var result = JSON.parse(res.trim());
    var stringx="";
    var stringcon="";
    $.each(result, function (key, item) {
      stringx += "<div class='input-group m-form__group' id='addcheckbox'>";
      stringx +="<div class='input-group-prepend'>";
      stringx +="<span class='input-group-text'>";
      stringx +="<label class=''>";
      stringx +=" <span></span>";
      stringx +="</label>";
      stringx +="</span>";
      stringx +="</div> ";
      if(item.taskTitle==null){
       stringx +="<input data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value=''>"; 
     }else{
      stringx +="<input data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask sampletask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value='"+item.taskTitle+"'>";
    }
    stringx +="<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='"+item.process_ID+"'>";
    stringx +="<input type='hidden' id='htaskID' name='htaskID' value='"+item.task_ID+"'>";
    stringx +="<div class='input-group-append'>";
    stringx +="<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
    stringx +="<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
    stringx +="<i class='fa fa-delete'></i>";
    stringx +="</a>";
    stringx +="<div class='m-dropdown__wrapper'>";
    stringx +="<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
    stringx +="<div class='m-dropdown__inner'>";
    stringx +="<div class='m-dropdown__body'> " ;            
    stringx +="<div class='m-dropdown__content'>";
    stringx +="<ul class='m-nav'>";
    stringx +="<li class='m-nav__item'>";
    stringx +="<a data-id='"+item.task_ID+"' onclick='deleteTask(this)' class='m-nav__link'>";
    stringx +="<i class='m-nav__link-icon fa fa-trash'></i>";
    stringx +="<span class='m-nav__link-text'>Delete</span>";
    stringx +="</a>";
    stringx +="</li>";
    stringx +="</ul>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div> ";
    stringx +="</div>";

    stringcon +="<div id='formcontent"+item.task_ID+"'class='m-portlet__body formcontent' style='height:540px;overflow-y:scroll; overflow-x:auto'>";
    stringcon +="<input type='hidden' class='hiddentask' value='"+item.task_ID+"'>"
    stringcon +="<div id='rowdisplay"+item.task_ID+"' class='row'>";

    stringcon +="<div class='col-lg-6'>";
    stringcon +="<div class='input-group m-input-icon m-input-icon--left'> ";         
    stringcon +="<input readonly='true' type='text' data-id='"+item.task_ID+"' onmouseover='duedate(this)' id='taskduedate"+item.task_ID+"' class='form-control form-control-sm m-input' placeholder='Task Due Date'>";
    stringcon +="<span class='m-input-icon__icon m-input-icon__icon--left'><span><i class='fa fa-calendar-plus-o'></i></span></span> "; 
    stringcon +="<div class='input-group-append'>";
    stringcon +="<button class='btn btn-brand btn-sm m-btn m-btn--custom' data-id='"+item.task_ID+"' onclick='savetaskduedate(this)' type='button'>Set</button>";
    stringcon +="</div>";
    stringcon +="</div>";
    stringcon += "</div>";

    stringcon +="<div class='col-lg-2'>";
    stringcon += "</div>";

    stringcon +="</div>";
    stringcon +="<div id='formaddcontent"+item.task_ID+"' class='row droparea_append'>";
    stringcon +="</div>";
    stringcon += "<div class='row droparea_outside'>";
    stringcon += "<div class='col-lg-12 droparea' >";
    stringcon += "<a href='#' data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='taskid(this)' class='btn m-btn m-btn--icon m-btn--pill m-btn--air' data-toggle='modal' data-target='#add_content-modal'>";
    stringcon += "<span>";
    stringcon += "<i class='fa fa-plus-circle'></i>";
    stringcon += "<span>Add Content</span>";
    stringcon += "</span>";
    stringcon += "</a>";
    stringcon += "</div>";
    stringcon += "</div>";  
    stringcon += "</div>";
  });
$("#checkgroup").html(stringx);
$(".inputtask:first").focus();
$(".inputtask:first").css({"background-color":"#51aff9","color":"white"});
$(".inputtask:first").addClass("placeholdercolor");
var firstaskid=$(".inputtask:first").attr('id');
display_allcomponents(firstaskid);
$("#contentdiv").append(stringcon);
$(".formcontent").hide();
$(".formcontent:first").show();
var taskid=$(".inputtask:first").attr('id');
displayduedate(taskid);
}
});
}

function taskid(element){
  var taskid=$(element).data('id');
  $("#taskformid").val(taskid);
}

function display_allcomponents(taskid){
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/fetch_allsubtask",
  data: {
    task_ID:taskid
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   var stringx = "";
   $.each(result, function (key, item) {
    console.log(item.component_ID);
    if(item.component_ID==1){
      stringx+="<div id='"+item.subTask_ID+"'class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' contenteditable='true' class='label' onkeyup='autosavelabel(this)'>";
      if(item.complabel!=null){
        stringx+=item.complabel;
      }else{
        stringx+="Type a label";
      }
      stringx+="</div>";
      stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill formdistance' placeholder='Something will be typed here...' disabled>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
    else if(item.component_ID==2){
      stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' contenteditable='true' class='label' onkeyup='autosavelabel(this)'>";
      if(item.complabel!=null){
        stringx+=item.complabel;
      }else{
        stringx+="Type a label";
      }
      stringx+="</div>";
      stringx+="<form>";
      stringx+="<textarea class='form-control m-input m-input--air m-input--pill' placeholder='Long text will be typed here...' disabled></textarea>";
      stringx+="</form>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
    else if(item.component_ID==3){
      stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='labelheading'>";
      if(item.complabel!=null){
        stringx+=item.complabel;
      }else{
        stringx+="Heading";
      }
      stringx+="</div>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' style='font-size:1em;color:#b9bbc1'> Type a Sub label...</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
    else if(item.component_ID==4){
      stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>"; 
      if(item.complabel!=null){
        stringx+=item.complabel;
      }else{
        stringx+="Click to edit this text...";
      }
      stringx+="</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
    else if(item.component_ID==5){
     stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
     stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
     if(item.complabel!=null){
      stringx+=item.complabel;
    }else{
      stringx+="Number label";
    }
    stringx+="</div>";
    stringx+="<input type='number' class='form-control m-input m-input--air m-input--pill' placeholder='A number or numbers will be typed here...' disabled>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(item.component_ID==6){ 
    stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label formdistance'>";
    if(item.complabel!=null){
      stringx+=item.complabel;
    }else{
      stringx+="Type a label";
    }
    stringx+="</div>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    stringx+="<div id='radiolist"+item.subTask_ID+"' class='row m-radio-list formdistance'>";

    stringx+="</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
    stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Option</label>";
    stringx+="<a>";
    stringx+="</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
    radio_option(item.subTask_ID);
  }
  else if(item.component_ID==7){
    stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
    if(item.complabel!=null){
      stringx+=item.complabel;
    }else{
      stringx+="Type a label";
    }
    stringx+="</div>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";

    stringx+="<div class='form-group m-form__group'>";
    stringx+="<select class='form-control formdistance m-input m-input--air m-input--pill' id='exampleSelect1' disabled>";
    stringx+="<option></option>";
    stringx+="<option>2</option>";
    stringx+="<option>3</option>";
    stringx+="<option>4</option>";
    stringx+="<option>5</option>";
    stringx+="</select>";
    stringx+="</div>";

    stringx+="<div class='form-group m-form__group formdistance'>";
    stringx+="<ul id='itemlist"+item.subTask_ID+"' class='list-group'>";
    stringx+="</ul>";
    stringx+="</div>";

    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
    stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add List</label>";
    stringx+="</a>";
    stringx+="</div>";

    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
    dropdown_list(item.subTask_ID);
  }
  else if(item.component_ID==8){
    stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
    if(item.complabel!=null){
      stringx+=item.complabel;
    }else{
      stringx+="Type a label";
    }
    stringx+="</div>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    stringx+="<div id='checkoption"+item.subTask_ID+"' class='row m-checkbox-list formdistance'>";
    stringx+="</div>";

    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
    stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Checkbox</label>";
    stringx+="<a>";
    stringx+="</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
    checkbox_option(item.subTask_ID);
  }
  else if(item.component_ID==9){
   stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 distance'>";
   stringx+="<hr>";
   stringx+="<div class='input-group-append formdistance'>";
   stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
   stringx+="<i class='fa fa-close'></i>";
   stringx+="</button>";
   stringx+="</div>";
   stringx+="</div>";
 }

 else if(item.component_ID==10){
   stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
   stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
   if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="Type Date label";
  }
  stringx+="</div>";
  stringx+="<div class='input-group formdistance'>";
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  stringx+="<div class='input-group-append'>";
  stringx+="<span class='input-group-text'>";
  stringx+="<i class='la la-calendar-check-o'></i>";
  stringx+="</span>";
  stringx+="</div>";
  stringx+="</div>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true'class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";

}
else if(item.component_ID==11){
  stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
  if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="Type a label";
  }
  stringx+="</div>";
  stringx+="<input class='form-control m-input m-input--air m-input--pill formdistance' type='number' value='0' min='0' max='100' step='1'/>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==12){
  stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
  if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="Email Label";
  }
  stringx+="</div>";
  stringx+="<input type='email' class='form-control m-input m-input--air m-input--pill' placeholder='An email will be typed here...' disabled>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==13){
 stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
 if(item.complabel!=null){
  stringx+=item.complabel;
}else{
  stringx+="Type Time Label";
}
stringx+="</div>";
stringx+="<div class='input-group formdistance'>"
stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
stringx+="<div class='input-group-append'>";
stringx+="<span class='input-group-text'>";
stringx+="<i class='la la-clock-o'></i>";
stringx+="</span>";
stringx+="</div>";
stringx+="</div>";
stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
stringx+="<div class='input-group-append formdistance'>";
stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-edit'></i>";
stringx+="</button>";
stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
else if(item.component_ID==14){
  stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>";
  if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="Type Date and Time Label";
  }
  stringx+="</div>";
  stringx+="<div class='input-group formdistance'>";
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date and time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  stringx+="<div class='input-group-append'>";
  stringx+="<span class='input-group-text'>";
  stringx+="<i class='la la-calendar-check-o'></i>";
  stringx+="</span>";
  stringx+="</div>";
  stringx+="</div>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
});
$("#add_content-modal").modal('hide');
$("#formaddcontent"+taskid).html(stringx);
}
});
}

function addingform(taskid,subtaskid,componentid){
  var stringx="";
  var stringcomp="";
  if(componentid==1){
    stringx+="<div id='"+subtaskid+"'class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type a Label</div>";
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill formdistance' placeholder='Something will be typed here...' disabled>";
    stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(componentid==2){
    stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label formdistance'>Type a Label</div>";
    stringx+="<form>";
    stringx+="<textarea class='form-control m-input m-input--air m-input--pill' placeholder='Long text will be typed here...' disabled></textarea>";
    stringx+="</form>";
    stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(componentid==3){
    stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='labelheading'> Heading</div>";
    stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' style='font-size:1em;color:#b9bbc1'> Type a Sub label...</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(componentid==4){
   stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
   stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'> Click to edit this text...</div>";
   stringx+="<div class='input-group-append formdistance'>";
   stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
   stringx+="<i class='fa fa-edit'></i>";
   stringx+="</button>";
   stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
   stringx+="<i class='fa fa-close'></i>";
   stringx+="</button>";
   stringx+="</div>";
   stringx+="</div>";
 }
 else if(componentid==5){
   stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
   stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Number Label</div>";
   stringx+="<input type='number' class='form-control m-input m-input--air m-input--pill' placeholder='A number or numbers will be typed here...' disabled>";
   stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";

   stringx+="<div class='input-group-append formdistance'>";
   stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
   stringx+="<i class='fa fa-edit'></i>";
   stringx+="</button>";
   stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
   stringx+="<i class='fa fa-close'></i>";
   stringx+="</button>";;
   stringx+="</div>";

   stringx+="</div>";
 }
 else if(componentid==6){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label formdistance'>Type a Label</div>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div id='radiolist"+subtaskid+"'class='row m-radio-list formdistance'>";
  stringx+="</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<a href='#' data-id='"+subtaskid+"' onclick='add_componentoption(this)'>";
  stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Option</label>";
  stringx+="</a>";
  stringx+="</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
  radio_option(subtaskid);
}
else if(componentid==7){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type a Label</div>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";

  stringx+="<div class='form-group m-form__group'>";
  stringx+="<select class='form-control formdistance m-input m-input--air m-input--pill' id='exampleSelect1' disabled>";
  stringx+="<option></option>";
  stringx+="<option>2</option>";
  stringx+="<option>3</option>";
  stringx+="<option>4</option>";
  stringx+="<option>5</option>";
  stringx+="</select>";
  stringx+="</div>";

  stringx+="<div class='form-group m-form__group formdistance'>";
  stringx+="<ul id='itemlist"+subtaskid+"' class='list-group'>";
  stringx+="</ul>";

  stringx+="</div>";

  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<a href='#' data-id='"+subtaskid+"' onclick='add_componentoption(this)'>";
  stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add List</label>";
  stringx+="</a>";
  stringx+="</div>";

  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";

  stringx+="</div>";
  dropdown_list(subtaskid);
}
else if(componentid==8){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type a Label</div>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";

  stringx+="<div id='checkoption"+subtaskid+"' class='row m-checkbox-list formdistance'>";
  stringx+="</div>";

  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<a href='#' data-id='"+subtaskid+"' onclick='add_componentoption(this)'>";
  stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Checkbox</label>";
  stringx+="</a>";
  stringx+="</div>";

  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
  checkbox_option(subtaskid);
}
else if(componentid==9){
 stringx+="<div id='"+subtaskid+"' class='col-lg-12 distance'>";
 stringx+="<hr>";
 stringx+="<div class='input-group-append formdistance'>";
 stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-close'></i>";
 stringx+="</button>";
 stringx+="</div>";
 stringx+="</div>";
}
else if(componentid==10){
 stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type Date Label</div>";
 stringx+="<div class='input-group formdistance'>";
 stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
 stringx+="<div class='input-group-append'>";
 stringx+="<span class='input-group-text'>";
 stringx+="<i class='la la-calendar-check-o'></i>";
 stringx+="</span>";
 stringx+="</div>";
 stringx+="</div>";
 stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true'class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
 stringx+="<div class='input-group-append formdistance'>";
 stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-edit'></i>";
 stringx+="</button>";
 stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-close'></i>";
 stringx+="</button>";
 stringx+="</div>";
 stringx+="</div>";
}
else if(componentid==11){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type Label</div>";
  stringx+="<input class='form-control m-input m-input--air m-input--pill formdistance' type='number' value='0' min='0' max='100' step='1'/>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(componentid==12){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div  data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Email Label</div>";
  stringx+="<input type='email' class='form-control m-input m-input--air m-input--pill' placeholder='An email will be typed here...' disabled>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(componentid==13){
 stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div  data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type Time Label</div>";
 stringx+="<div class='input-group formdistance'>"
 stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
 stringx+="<div class='input-group-append'>";
 stringx+="<span class='input-group-text'>";
 stringx+="<i class='la la-clock-o'></i>";
 stringx+="</span>";
 stringx+="</div>";
 stringx+="</div>";
 stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
 stringx+="<div class='input-group-append formdistance'>";
 stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-edit'></i>";
 stringx+="</button>";
 stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-close'></i>";
 stringx+="</button>";
 stringx+="</div>";
 stringx+="</div>";
}
else if(componentid==14){
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div  data-id='"+subtaskid+"' id='label"+subtaskid+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label'>Type Date and Time Label</div>";
  stringx+="<div class='input-group formdistance'>"
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date and time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>"
  stringx+="<div class='input-group-append'>"
  stringx+="<span class='input-group-text'>"
  stringx+="<i class='la la-calendar-check-o'></i>"
  stringx+="</span>"
  stringx+="</div>"
  stringx+="</div>"
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
$("#add_content-modal").modal('hide');
$("#formaddcontent"+taskid).append(stringx);
countallforms(taskid);
}

function delete_component(element){
  var subtaskid=$(element).data('id');
  var task_id=$(element).data('compid');
  swal({
    title: 'Are you sure you want to delete this form element?',
    text: "",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url:"<?php echo base_url(); ?>process/delete_component",
        data: {
          subTask_ID: subtaskid,
        },
        cache: false,
        success: function () {
         display_allcomponents(task_id);
         swal(
          'Deleted!',
          'Successfully deleted.',
          'success'
          )
       },
       error: function (res){
        swal(
          'Oops!',
          'Please Check!',
          'error'
          )
      }
    });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deletion has been cancelled',
        'error'
        )
    }
  });
}
//RADIO, MULTIPLE, DROPDOWN
function radio_option(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' class='m-radio m-radio--bold m-radio--state-primary' contenteditable='true'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Double click and type option...";
   }
   stringcomp+="<input type='radio' name='formrad' value='' disabled>";
   stringcomp+="<span></span>";
   stringcomp+="</label>";
   stringcomp+="</div>";
   stringcomp+="<div class='col-lg-2'>";
   stringcomp+="<a href='#' data-componentid='6' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)' ><i class='fa fa-remove'></i></a>";
   stringcomp+="</div>";
 });
   $("#radiolist"+subtaskid).html(stringcomp);
 }
});
}
function checkbox_option(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' class='m-checkbox m-checkbox--bold m-checkbox--state-brand' contenteditable='true'>";
    stringcomp+="<input type='checkbox' disabled>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
    stringcomp+="Type Checklist Label...";
  }
  stringcomp+="<span></span>";
  stringcomp+="</label>";
  stringcomp+="</div>";
  stringcomp+="<div class='col-lg-2'>";
  stringcomp+="<a href='#' data-componentid='8' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)'><i class='fa fa-remove'></i></a>";
  stringcomp+="</div>";
});
   $("#checkoption"+subtaskid).html(stringcomp);
 }
});
}
function dropdown_list(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<li class='list-group-item justify-content-between'>";
    stringcomp+="<span id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' contenteditable='true'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Item list...";
   }
   stringcomp+="</span>";
   stringcomp+="<a href='#' data-componentid='7' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)'><i class='float-right fa fa-remove'></i></a>";
   stringcomp+="</li>";
 });
   $("#itemlist"+subtaskid).html(stringcomp);
 }
});
}
function countallforms(taskid){
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/fetch_allsubtask",
  data: {
    task_ID:taskid
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   var countform=Object.keys(result).length;
   console.log('print'+countform);
   $("#sequenceid").val( countform);
 }
});
}
function add_component(componentType,taskid){
 var sequencenum=$("#sequenceid").val();
 var seqnum=0;
 if(sequencenum==0){
  seqnum=1;
}else{
  seqnum=sequencenum;
  seqnum++;
}
$.ajax({
 type: "POST",
 url: "<?php echo base_url(); ?>process/add_subtask",
 data: {
  component_ID: componentType,
  task_ID: taskid,
  sequence: seqnum
},
cache: false,
success: function (res) {
  res = JSON.parse(res.trim());
  var data = res;
  var subtaskid= data.subTask_ID;
  fetch_lastinsertedform(subtaskid);
}
});
}

function add_componentoption(element){
  var subtaskid=$(element).data('id');
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/add_subtaskcomponent",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var res = JSON.parse(res.trim());
   var data = res;
   var component=data.component_ID;
   console.log('success');
   console.log(component);
   if(component==6){
    radio_option(subtaskid);
  }
  else if(component==7){
    dropdown_list(subtaskid);
  }
  else if(component==8){
    checkbox_option(subtaskid);
  }
}
});
}
function delete_option(element){
  var compsubtaskid=$(element).data('id');
  var subtask=$(element).data('subtaskid');
  var componentid=$(element).data('componentid');
  $.ajax({
    type: "POST",
    url:"<?php echo base_url(); ?>process/delete_option",
    data: {
      componentSubtask_ID: compsubtaskid,
    },
    cache: false,
    success: function () {
      if(componentid=='6'){
        radio_option(subtask);
      }
      else if(componentid=='7'){
        dropdown_list(subtask);
      }
      else if(componentid=='8'){
        checkbox_option(subtask);
      }
    }
  });
}
function fetch_lastinsertedform(subtaskid){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_subtask",
    data: {
      subTask_ID:subtaskid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      var data = res;
      var stid= data.task_ID;
      var componentid= data.component_ID;
      addingform(stid,subtaskid,componentid);
    }
  });
}
function displayformdiv(element){
  var taskid=$(element).data('id');
  console.log(taskid);
  display_allcomponents(taskid);
  displayduedate(taskid);
  $(".formcontent").hide();
  $("#formcontent"+taskid).show();
  $(".inputtask").css({"background-color":"","color":""});
  $(".inputtask").removeClass("placeholdercolor");
  $("input#"+taskid).css({"background-color":"#51aff9","color":"white"});
  $("input#"+taskid).addClass("placeholdercolor");

}
function displayduedate(taskid){
  var tid=taskid;
  var dd="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_duedate",
   data: {
    task_ID: tid,
    check_ID: checkid,
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    var data = res;
    if(data.taskdueDate!=null){
      var dateString=data.taskdueDate;
      var dateObj = new Date(dateString);
      var momentObj = moment(dateObj);
      var momentString = momentObj.format('llll');
      dd+="<div class='row taskduedate' style='padding:15px;'>";
      dd+="<label style='font-size:0.9em'> <i class='fa fa-calendar-check-o'></i><strong style='color:#34bfa3;'> Task Due Date:</strong> "+momentString+"</label>";
      dd+="</div>";
      $(".taskduedate").remove();
      $("#rowdisplay"+tid).after(dd);
    }
  }
});
}
function savetaskduedate(element){
 var taskid=$(element).data('id');
 var dueDate=$("#taskduedate"+taskid).val();
 var pid=$("#hiddentaskID").val();
 if ($.trim($("#taskduedate"+taskid).val()) === "") {
 }else {
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_taskDuedate",
    data: {
      checklist_ID: checkid,
      task_ID: taskid,
      taskdueDate:dueDate
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'Due Date Set and Updated!',
        showConfirmButton: false,
        timer: 1500
      });
      window.location.href = "<?php echo base_url('process/checklist_update')?>"+"/"+checkid+"/"+pro_id;
    },
    error: function (res){
      console.log(res);
      doNotif2();
    }
  });
 }
}
function saveduedate(element){
  var dueDate=$("#datetime").val();
  var checkID=$(element).data('id');
  var pid=$("#hiddentaskID").val();
  if ($.trim($("#datetime").val()) === "") {
    e.preventDefault();
  }else{
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_checklistDuedate",
    data: {
      checklist_ID: checkID,
      dueDate:dueDate
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'Due Date Set and Updated!',
        showConfirmButton: false,
        timer: 1500
      });
      window.location.href = "<?php echo base_url('process/checklist_update')?>"+"/"+checkID+"/"+pid;
    },
    error: function (res){
      console.log(res);
      doNotif2();
    }
  });
 }
}
/* */
var timer=null;
function doNotif() {
 $.notify("Auto-save!");
 setTimeout(function() {
  $.notifyClose('top-right');
}, 1000);
}
function doNotif2() {
 $.notify("Save!");
 setTimeout(function() {
  $.notifyClose('top-right');
}, 1000);
}

//autosave function

function autosaveoptionlabel(element){
  var compsubtaskid=$(element).data('id');
  var optionlabel=$("#optiondetails"+compsubtaskid).text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_optiondetails",
    data: {
      componentSubtask_ID: compsubtaskid,
      compcontent: optionlabel
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      clearTimeout(timer); 
      timer = setTimeout(doNotif, 1000)
    },
    error: function (res){
      console.log(res);
    }
  });
}
function autosavesublabel(element){
 var subtaskid=$(element).data('id');
 var sublabel=$("#sublabel"+subtaskid).text();
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/add_sublabeldetails",
  data: {
    subTask_ID: subtaskid,
    sublabel: sublabel
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    console.log(res);
    clearTimeout(timer); 
    timer = setTimeout(doNotif, 1000)
  },
  error: function (res){
    console.log(res);
  }
});
}
function autosavelabel(element){
  var subtaskid=$(element).data('id');
  var label=$("#label"+subtaskid).text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_subtaskdetails",
    data: {
      subTask_ID: subtaskid,
      complabel: label
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      clearTimeout(timer); 
      timer = setTimeout(doNotif, 1000)
    },
    error: function (res){
      console.log(res);
    }
  });
}
function autosavetask(element){
 var asid=$(element).data('id');
 console.log(asid);
 var taskTitle = $("input#"+asid).val();
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/add_taskdetails",
  data: {
    task_ID: asid,
    taskTitle:taskTitle
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    console.log(res);
    clearTimeout(timer); 
    timer = setTimeout(doNotif, 1000)
  },
  error: function (res){
    console.log(res);
  }
});
}
function addtask(element){
  var processID=$(element).data('id');
  var checkID = $("#checkID").val();
  console.log(processID);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_onetaskStatus",
    data: {
      process_ID: $(element).data('id'),
      checklist_ID: checkID
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
      display_tasks();
      // window.location.href = "<?php echo base_url('process/checklist_update')?>"+"/"+checkID+"/"+processID;
    },
    error: function (res){
      console.log(res);
    }
  });
}
function deleteTask(element){
  var processID = $("#hiddentaskID").val();
  swal({
    title: 'Are you sure?',
    text: "All under this task will be deleted, too.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_task",
        data: {
          task_ID: $(element).data('id')
        },
        cache: false,
        success: function () {
         window.location.href ="<?php echo base_url('process/checklist')?>"+"/"+processID;
         swal(
          'Deleted!',
          'Task successfully deleted.',
          'success'
          )
       },
       error: function (res){
        swal(
          'Oops!',
          'You cannot delete this task ',
          'error'
          )
      }
    });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deleting a process has been cancelled',
        'error'
        )
    }
  });
  console.log($(element).data('id'));
}

</script>



