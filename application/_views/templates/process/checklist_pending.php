<style type="text/css">
.colorwhite{
    color:white !important;
}
.coloricons{
    color:#009688 !important;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto"> 
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>         
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo site_url('process');?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"><</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('process/subfolder').'/'.$processinfo->folder_ID;?>" class="m-nav__link">
                            <span class="m-nav__link-text">Source Folder</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
         <div class="col-lg-12">  
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head portlet-header-report" style="background-color:#2c2e3eeb">
                    <div class="m-portlet__head-caption">   
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon font-white">
                                <i class="fa fa-wpforms"></i>
                            </span> 
                            <a href="<?php echo site_url('process');?>" class="m-portlet__head-text font-poppins"  style="color:white !important;font-size:16px">
                               <?php echo $processinfo->processTitle;?>
                           </a>
                       </div>
                   </div>
               </div>
               <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-8">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="searchChecklist form-control m-input" placeholder="Search by checklist name..." id="input-search">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="m-input-icon">
                                      <select class="form-control m-input m-input--square" onchange="getchecklistval(this)">
                                        <option value="100">All</option>
                                        <option value="2">Pending</option>
                                        <option value="3">Completed</option>
                                        <option value="16">Overdue</option>
                                        <option>Assigned</option>
                                        <option value="14">Archived</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--wide" data-toggle="modal" data-target="#run_checklist-modal">
                            <span>
                                <i class="fa fa-play"></i>
                                <span>Run Another Checklist</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <div class="row" id="checkrow">
                <?php foreach ($checklist as $ch) {?>
                    <div class="checkcontent col-lg-4 chli<?php echo $ch->status_ID;?>">
                        <?php 
                        $countChecklist=0;
                        $countCompleted=0;
                        foreach ($checklist_status as $chs) {
                            if($chs->checklist_ID==$ch->checklist_ID){
                                $countChecklist++;
                                if($chs->isCompleted=="3"){
                                    $countCompleted++;
                                }
                            }
                        }
                        $divide=$countCompleted/$countChecklist;
                        $result=$divide*100;
                        $finalcal=round($result);
                        ?>  
                        <div class="m-portlet m-portlet--collapsed m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_7">
                            <div class="m-portlet__head border-accent rounded" style="background-color:#91e4b5">
                                <!--  <div class="row" style="background-color: #86b4d8">hi</div> -->
                                <div class="row" style="margin-top:1em">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <span class="m-portlet__head-icon">
                                                <?php if($countCompleted==$countChecklist){?>
                                                    <i class="fa fa-check coloricons"></i>
                                                <?php }else{?>
                                                    <i class="fa fa-clock-o coloricons"></i>
                                                <?php }?>       
                                            </span>
                                            <a style="display:block;text-overflow: ellipsis;width: 250px;overflow: hidden; white-space: nowrap;" href="<?php echo site_url('process/checklist_answerable').'/'.$processinfo->process_ID.'/'.$ch->checklist_ID;?>" class="checkName m-portlet__head-text">
                                             <?php echo $ch->checklistTitle;?>
                                         </a>
                                         <input type="hidden" name="chid" id="chID" value="<?php echo $ch->checklist_ID;?>">                                       
                                     </div>          
                                 </div>
                             </div>
                             <span style="font-size: 0.9em;margin-left:1em">
                                <?php 
                                $datetime=$ch->dateTimeCreated;
                                $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
                                echo sprintf($date->format("M j, Y"))."&nbsp; at &nbsp;";
                                echo sprintf($date->format("g:ia"));
                                ?>
                            </span><br>
                            <span style="font-size: 0.9em;margin-left:1em">
                                <span class="m-badge m-badge--accent m-badge--wide">CH</span>
                                <span class="m-badge m-badge--accent m-badge--wide">SB</span>
                            </span>
                            <div class="row" style="margin-top:1em;margin-bottom: 1em;">
                                <div class=" col-md-12">
                                  <?php if($countCompleted<$countChecklist){?>
                                    <div class="progress">
                                      <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $finalcal;?>%" data-toggle="tooltip" data-placement="top" title="<?php echo $countCompleted;echo "/";echo $countChecklist;?> tasks completed"></div>
                                  </div>
                              <?php }else if($countCompleted==$countChecklist){?>
                                <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $finalcal;?>%" data-toggle="tooltip" data-placement="top" title="Completed" aria-valuenow="0" aria-valuemin="2" aria-valuemax="5"></div>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                    <div class="row" style="margin-top:1em;margin-bottom: 1em;">
                       &nbsp;&nbsp;
                       <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav" >
                            <li class="m-portlet__nav-item">
                                <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon" id="dropdownOption" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i 
                                    class="la la-cog coloricons"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownOption" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <button class="dropdown-item" type="button" data-id="<?php echo $ch->checklist_ID; ?>" onclick="updateChecklistFetch(this)" data-toggle="modal" data-target="#updateChecklistmodal"><i class="flaticon-file"></i>&nbsp;Edit checklist name</button>
                                        <button class="dropdown-item" type="button"><i class="flaticon-file">
                                        </i>&nbsp;Assign this checklist</button>
                                    </div> 
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i data-toggle="m-tooltip" data-placement="top" title data-original-title="Info" class="la la-info-circle coloricons"></i></a>  
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-portlet-tool="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i data-toggle="m-tooltip" data-placement="top" title data-original-title="Delete" class="fa fa-trash-o coloricons"></i></a>    
                                </li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if($ch->dueDate!=NULL){?>
                                    <li class="m-portlet__nav-item">
                                        <span class="m-menu__link-badge">
                                            <?php
                                            $dt=$ch->dueDate;
                                            $date = DateTime::createFromFormat("Y-m-d H:i:s",$dt);
                                            $date2=date_create($date->format("Y-m-d"));
                                            $date1=date_create(date("Y-m-d"));
                                            $diff=date_diff($date1,$date2);
                                            if($date2 > $date1){
                                                ?>
                                                <span class="m-badge m-badge--success m-badge--wide"  data-toggle="m-tooltip" data-placement="top" title data-original-title="Due Date: <?php echo $diff->format("%a day/s");?> left">
                                                 <?php 
                                                 $datetime=$ch->dueDate;
                                                 $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
                                                 echo sprintf($date->format("M j, Y"));
                                                 /*echo sprintf($date->format("g:ia"))*/;
                                                 ?>
                                             </span>
                                         <?php }else if($date2 < $date1){?>
                                          <span class="m-badge m-badge--danger m-badge--wide"  data-toggle="m-tooltip" data-placement="top" title data-original-title="Overdue!"> 
                                              <?php 
                                              $datetime=$ch->dueDate;
                                              $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
                                              echo sprintf($date->format("M j, Y"));
                                              /*echo sprintf($date->format("g:ia"))*/;
                                              ?>
                                          </span>
                                      <?php } else if($date2 == $date1){?>
                                          <span class="m-badge m-badge--warning m-badge--wide"  data-toggle="m-tooltip" data-placement="top" title data-original-title="Today is this checklist Due date! Please finish this before this day ends.">
                                             <?php 
                                             $datetime=$ch->dueDate;
                                             $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
                                             echo sprintf($date->format("M j, Y"));
                                             /*echo sprintf($date->format("g:ia"))*/;
                                             ?>
                                         </span>
                                     <?php }?>
                                 </span>   
                             </li> 
                         <?php }?> 
                     </ul>
                 </div>
             </div>
         </div>
         <div class="m-portlet__body" style="margin-top: 3em">
            <div class="col-md-12">
                <div class="m-input-icon m-input-icon--left">
                    <input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
                    <span class="m-input-icon__icon m-input-icon__icon--left">
                        <span><i class="la la-search"></i></span>
                    </span>
                </div>
            </div>
            <br> 
        </div>
    </div>  
</div>
<!--end::Portlet-->
<?php }?>
<div class="load-more col-lg-12" lastID="<?php echo $ch->checklist_ID;?>" style="display: none;">
  <h5 style="text-align: center;"> <div class="m-loader m-loader--info" style="width: 30px; display: inline-block;"></div> Loading more checklist...</h5>
</div>
</div>
</div>
</div>  
<!--end::Portlet-->
</div>
</div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="updateChecklistmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Checklist Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form id="formupdate" name="formupdate" method="post">
      <div class="form-group">
        <label for="recipient-name" class="form-control-label">Checklist Name:</label>
        <input type="text" class="form-control" id="updatechecklistName" name="updatechecklistName" placeholder="Enter Process Name">
        <input type="hidden" id="pid" value="<?php echo $processinfo->process_ID;?>">
    </div>
</form>
</div>
<div class="modal-footer">
    <button type="submit" name="submit" id="btn-checklistsavechanges" onclick="saveChangesChecklist(this)" class="btn btn-accent">Save Changes</button>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="run_checklist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Let's run another checklist!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form id="formupdate" name="formupdate" method="post">
      <div class="form-group">
        <h5 style="color:#34bfa3">Base on <strong>'<?php echo $processinfo->processTitle; ?> Template'</strong></h5>
        <br>
        <?php foreach($user_info as $ui) {?>
            <label for="recipient-name" class="form-control-label">Name of your checklist (For example, the name of the employee you’re onboarding) :</label>
            <input type="text" maxlength="40" class="form-control" id="addchecklistName" name="addchecklistName" value="<?php echo $ui->fname;echo "&nbsp;";echo $ui->lname;echo "'s";echo "&nbsp;" ;echo date("h:i a");echo "&nbsp;";echo "checklist"?>" placeholder="Enter Checklist Name">
            <span style="font-size:0.9em;color:gray" id="charNum"></span>
        <?php }?>
    </div>
</form>
</div>
<div class="modal-footer">
    <button type="submit" name="submit" data-id="<?php echo $processinfo->process_ID; ?>" onclick="runChecklist(this)" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Run This Checklist</button>
</div>
</div>
</div>
</div>
<!--end::Modal-->
</div>
<script type="text/javascript">
   $(function(){
     $(window).scroll(function(){
        var lastID = $('.load-more').attr('lastID');
        var processid=$("#pid").val();
        console.log();
        if(($(window).scrollTop() == $(document).height() - $(window).height()) && (lastID != 0)){
          $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>process/checklistload_more",
            data: {
              checklist_ID:lastID,
              process_ID:processid
          },
          beforeSend:function(){
              $('.load-more').show();
          },
          success:function(html){
              $('.load-more').remove();
              $('#checkrow').append(html);
          }
      });
      }
  });
 });
   function noresults(){
    $("div#noresult").remove();
    $("<div class='col-lg-12' id='noresult' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records Found!</strong></h5></div>").appendTo("div#checkrow");
}
$('.searchChecklist').keyup(function(){
   var text = $(this).val().toLowerCase();
   $('.checkcontent').hide();
   $('.checkcontent .checkName').each(function(){   
    if($(this).text().toLowerCase().indexOf(""+text+"") != -1 ){
        $(this).closest('.checkcontent').show(function(){
            $("div#noresult").remove();
        });
    }else if($(this).text().toLowerCase().indexOf(""+text+"") == -1 ){
        noresults();
    }
});
});
function getchecklistval(element){
    $('.checkcontent').hide();
    if(element.value=="100"){
      $('.checkcontent').show();
  }
  $('.checkcontent .checkName').each(function(){
      if(element.value=="2"){
        $('.chli2').show();
    }
    else if(element.value=="3"){
        $('.chli3').show();
    }   
});
} 
$('#addchecklistName').keyup(function () {
    var max = 40;
    var len = $(this).val().length;
    if (len >= max) {
      $('#charNum').text('You have reached the limit');
  } else {
      var char = max - len;
      $('#charNum').text(char+'/40 characters remaining');
  }
});
function runChecklist(element){
 var pid=$(element).data('id');
 if ($.trim($("#addchecklistName").val()) === "") {
  e.preventDefault();
}else{
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_anotherchecklist",
    data: {
      process_ID: $(element).data('id'),
      checklistTitle: $("#addchecklistName").val()
  },
  cache: false,
  success: function (res) {
    window.location.href = "<?php echo base_url('process/checklist_pending')?>"+"/"+pid;
    swal({
        position: 'center',
        type: 'success',
        title: 'Successfully Added checklist',
        showConfirmButton: false,
        timer: 1500
    });
    var result = JSON.parse(res.trim());
},
error: function (res){
  console.log(res);
}
});
}
}
function updateChecklistFetch(element){
  var checklistTitle = $("#updatechecklistName").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetchchecklist_update",
    data: {
      checklist_ID: $(element).data('id'),
      checklistTitle:checklistTitle
  },
  cache: false,
  success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      $("#updatechecklistName").val(data.checklistTitle);
      $("#btn-checklistsavechanges").data('id',data.checklist_ID);
  }
});
}
function saveChangesChecklist(element){
  var pid = $("#pid").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_checklist",
    data: {
      checklist_ID: $(element).data('id'),
      checklistTitle: $("#updatechecklistName").val()
  },
  cache: false,
  success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'checklist successfully updated',
        showConfirmButton: false,
        timer: 1500
    });
      $("#updateChecklistmodal").modal('hide');
      window.location.href = "<?php echo base_url('process/checklist_pending')?>"+"/"+pid;
  }
});
}   
</script>