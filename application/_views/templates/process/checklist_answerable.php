<style type="text/css">
.topdistance{
  margin-top: 15px;
}
.placeholdercolor::placeholder { 
  color: #d8dce8;
  opacity: 1;
}
.droparea_outside{
  border-style: dashed;
  padding: 10px;
  margin: 10px;
  border-color:#a5a6ad;
}
.droparea_append{
  padding: 10px;
  margin: 10px;
}
.buttonform{
  text-align: center;
}
.distance{
  margin-bottom: 25px;
}
.formdistance{
  margin-top: 10px;
}
.taskdistance{
  margin-top: 15px;
}
.label{
  font-size:1.2em;
  color:#00c5dc;
}
.inputcolor{
  background: #e5e6ef;
}
.label{
  font-size:1.2em;
  color:#00c5dc;
}
.labelheading{
  font-size:1.4em;
  font-weight: bold;
  color:#00c5dc;
}
.butpadding{
  padding: 45px;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>         
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo base_url('process')?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url('process/checklist_pending').'/'.$processinfo->process_ID;?>" class="m-nav__link">
              <span class="m-nav__link-text"><?php echo $processinfo->processTitle;?></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet">
          <!--begin: Portlet Head-->
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 id="appendbutton" class="m-portlet__head-text">
                  <?php if($checklist->status_ID=="2"){?>
                    <button id="checkall" onclick="checkall()" class="btn btn-success btn-sm m-btn m-btn--icon">
                      <span>
                        <i class="fa fa-check"></i>
                        <span>Complete this checklist</span>
                      </span>
                    </button>
                  <?php }else if($checklist->status_ID=="3"){?>
                    <button id="uncheckall" onclick="uncheckall()" class="btn btn-metal btn-sm m-btn m-btn--icon">
                      <span>
                        <i class="fa fa-repeat"></i>
                        <span>Unchecked all checkboxes</span>
                      </span>
                    </button>
                  <?php }?>
                  <?php if($processinfo->processstatus_ID=="10"){?>
                    <a href="#" data-id="<?php echo $processinfo->process_ID;?>" class="btn btn-success btn-sm m-btn m-btn m-btn--icon" data-toggle="modal" data-target="#run_checklist-modal">
                      <span>
                        <i class="fa fa-play-circle-o"></i>
                        <span>Run checklist</span>
                      </span>
                    </a>
                  <?php }?>
                </h3>
              </div>
            </div>
            <div class="m-portlet__head-tools">
              <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                  <a href="<?php echo base_url('process/checklist_update').'/'.$checklist->checklist_ID.'/'.$processinfo->process_ID;?>" class="btn btn-success btn-sm" data-toggle="m-tooltip" data-placement="top" title data-original-title="Edit Checklist Template">
                    <span>
                      <i class="fa fa-pencil"></i>
                    </span>
                  </a>
                  <a id="showmenu" href="#" class="btn btn-metal btn-sm">
                    <span>
                      <i class="fa fa-chevron-right"></i>
                    </span>
                  </a>  
                </li>   
              </ul>
            </div> 
          </div>
          <!--end: Portlet Head-->
          <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="m-wizard m-wizard--4 m-wizard--brand m-wizard--step-first" id="m_wizard">
              <div class="row m-row--no-padding">
                <div id="div1" class="col-xl-6 col-lg-12 border">
                  <br>
                  <h5 style="color:#777;text-align:center;font-size:1.3em;"><strong><?php echo $checklist->checklistTitle;?></strong></h5>
                  <?php if($checklist->dueDate!=NULL){?>

                    <div class="col-xl-12" style="text-align:center;">
                      <i class="fa fa-calendar-check-o"></i><span style="font-size:0.9em;"><strong style="color:#34bfa3"> &nbsp;Due Date &nbsp;</strong>
                        <?php 
                        $datetime=$checklist->dueDate;
                        $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
                        echo sprintf($date->format("D, M j, Y"))."&nbsp; at &nbsp;";
                        echo sprintf($date->format("g:ia"));
                        ?>
                      </span>
                    </div>
                  <?php }?>

                  <?php $alltask=count($task);?>
                  <input type="hidden" id="countTask" value="<?php echo $alltask;?>">
                  <input type="hidden" id="checklistID" value="<?php echo $checklist->checklist_ID;?>">
                  <input type="hidden" id="processID" value="<?php echo $processinfo->process_ID;?>">
                  <div class="m-portlet__body" style="height:410px;overflow-y:scroll; overflow-x:auto;background-color:">
                    <div class="form-group m-form__group topdistance" id="checkgroup">
                      <br><br>
                    </div>
                  </div>              
                </div>
                <div id="contentdiv" class="col-xl-6 col-lg-12 border" style="height:480px;overflow-y:scroll; overflow-x:auto"> 
               <!--    <div class="m-portlet__body">  
                    <h3>Task details here.</h3>                  
                  </div>   -->
                </div>
                <div id="div3" class="col-xl-2 col-lg-12 border" style="visibility: hidden;display: none">
                  <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                        <h6 class="m-portlet__head-text">
                          Menu
                        </h6>
                      </div>
                    </div>
                  </div> 
                  <div class="m-portlet__body">
                    <div class="row">
                     <button type="button" class="btn btn-success btn-sm">Complete Checklist</button>
                   </div><hr>
                   <div class="row">
                    <div class="col-12">
                      <ul class="list-inline text-center align-items-center">
                        <li class="list-inline-item align-middle"><a href="<?php echo base_url('process/checklist_update').'/'.$checklist->checklist_ID.'/'.$processinfo->process_ID;?>" >Edit this Template</a></li><br>
                        <hr>
                        <li class="list-inline-item align-middle"><a href="">Delete this Template</a></li><br><hr>
                        <li class="list-inline-item align-middle"><a href="">Delete this Template</a></li><br><hr>
                      </ul>
                    </div>
                  </div>                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
 var timer=null;
 var cid = $("#checklistID").val();
 var prid = $("#processID").val();
 var status="";
 var a=0;
 $(function(){
  display_tasks_answerable();
});
 $('h3#appendbutton').on('click', '#checkall', function() {
  checkall();
});
  //NOTIFS
  function doNotif() {
   $.notify("Auto-saved!");
   setTimeout(function() {
    $.notifyClose('top-right');
  }, 1000);
 }
 function doNotif2() {
   $.notify("Saved!");
   setTimeout(function() {
    $.notifyClose('top-right');
  }, 1000);
 }
 $('#m_datepicker_1').datepicker({
  todayHighlight: true,
  orientation: "bottom left",
  templates: {
    leftArrow: '<i class="la la-angle-left"></i>',
    rightArrow: '<i class="la la-angle-right"></i>'
  }
});
 function display_tasks_answerable(){
   $.ajax({
     type: "POST",
     url: "<?php echo base_url(); ?>process/display_tasks_answerable",
     data: {
      checklist_ID: cid,
    },
    cache: false,
    success: function (res) {
     var result = JSON.parse(res.trim());
     var stringx = "";
     var stringcon= "";
     var stringbutton="";
     $.each(result, function (key, item) {
      if(item.isCompleted==2){
        console.log('success!');
        stringx+="<div class='input-group m-form__group' id='addcheckbox'>";

        stringx+="<div class='input-group-prepend'>";
        stringx+="<span class='input-group-text'>";
        stringx+="<label class='m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-accent'>"
        stringx+="<input type='checkbox' class='checkedC' data-id='"+item.checklistStatus_ID+"' onchange='changecheckbox(this)' id='checkbox_id"+item.checklistStatus_ID+"'>";
        stringx+="<span></span>";
        stringx+="</label>";
        stringx+="</span>";
        stringx+="</div>";
        if(item.taskTitle==null){
         stringx+="<input data-taskid='"+item.task_ID+"' data-id='"+item.checklistStatus_ID+"' id='inputbox_id"+item.checklistStatus_ID+"' onclick='displayformdiv(this)' type='text' class='form-control crashout inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='No Task Label!' readonly='readonly'>";
       }else{
        stringx+="<input data-taskid='"+item.task_ID+"' data-id='"+item.checklistStatus_ID+"' id='inputbox_id"+item.checklistStatus_ID+"' onclick='displayformdiv(this)' type='text' class='form-control crashout inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='"+item.taskTitle+"' readonly='readonly'>";
      }
      stringx+="<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='"+item.process_ID+"'>";
      stringx+="<input type='hidden' id='staskID' name='staskID' value='"+item.checklistStatus_ID+"'>";

      stringx+="<div class='input-group-append'>";
      stringx+="<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
      stringx+="<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
      stringx+="<i class='fa fa-delete'></i>";
      stringx+="</a>";
      stringx+="<div class='m-dropdown__wrapper'>";
      stringx+="<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
      stringx+="<div class='m-dropdown__inner'>";
      stringx+="<div class='m-dropdown__body'>";              
      stringx+="<div class='m-dropdown__content'>";
      stringx+="<ul class='m-nav'>";;
      stringx+="<li class='m-nav__item'>";
      stringx+="<a id='taskdetails' href='#' class='m-nav__link'>";
      stringx+="<i class='m-nav__link-icon fa   fa-angle-double-right'></i>";
      stringx+="<span class='m-nav__link-text'>Task details</span>";
      stringx+="</a>";
      stringx+="</li>";
      stringx+="</ul>";
      stringx+="</div>";
      stringx+="</div>";
      stringx+="</div>";
      stringx+="</div>";
      stringx+="</div>";
      stringx+="</div>";

      stringx +="</div>";
      stringcon +="<div id='formaddcontent"+item.task_ID+"' class='m-portlet__body formcontent'>";
      stringcon +="<input type='hidden' class='hiddentask' value='"+item.checklistStatus_ID+"'>"

      stringcon +="<div id='rowdisplay"+item.checklistStatus_ID+"' class='row'>";
      stringcon +="</div>";
      stringcon += "</div>";
    }
    else if(item.isCompleted==3){
     stringx+="<div class='input-group m-form__group' id='addcheckbox'>";

     stringx+="<div class='input-group-prepend'>";
     stringx+="<span class='input-group-text'>";
     stringx+="<label class='m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-accent'>"
     stringx+="<input type='checkbox' class='checkedC' data-id='"+item.checklistStatus_ID+"' onchange='changecheckbox(this)' id='checkbox_id"+item.checklistStatus_ID+"' checked>";
     stringx+="<span></span>";                    
     stringx+="</label>";
     stringx+="</span>";
     stringx +="</div>";

     if(item.taskTitle==null){
       stringx +="<input data-taskid='"+item.task_ID+"' data-id='"+item.checklistStatus_ID+"' id='inputbox_id"+item.checklistStatus_ID+"' onclick='displayformdiv(this)' style='text-decoration: line-through;font-style: italic;color:#b3b2b2;' type='text' class='form-control inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='No Task Label!' readonly='readonly'>";
     }else{
       stringx +="<input data-taskid='"+item.task_ID+"' data-id='"+item.checklistStatus_ID+"' id='inputbox_id"+item.checklistStatus_ID+"' onclick='displayformdiv(this)' style='text-decoration: line-through;font-style: italic;color:#b3b2b2;' type='text' class='form-control inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='"+item.taskTitle+"' readonly='readonly'>";
     }
     stringx +="<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='"+item.process_ID+"'>";
     stringx +="<input type='hidden' id='staskID' name='staskID' value='"+item.checklistStatus_ID+"'>";

     stringx +="<div class='input-group-append'>";
     stringx +="<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
     stringx +="<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
     stringx +="<i class='fa fa-delete'></i>";
     stringx +="</a>";
     stringx +="<div class='m-dropdown__wrapper'>";
     stringx +="<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
     stringx +="<div class='m-dropdown__inner'>";
     stringx +="<div class='m-dropdown__body'>";           
     stringx +="<div class='m-dropdown__content'>";
     stringx +="<ul class='m-nav'>";
     stringx +="<li class='m-nav__item'>";
     stringx +="<a id='taskdetail' href='#' class='m-nav__link'>";
     stringx +="<i class='m-nav__link-icon fa fa-angle-double-right'></i>";
     stringx +="<span class='m-nav__link-text'>Task details</span>";
     stringx +="</a>";
     stringx +="</li>";
     stringx +="</ul>";
     stringx +="</div>";
     stringx +="</div>";
     stringx +="</div>";
     stringx +="</div>";
     stringx +="</div>";
     stringx +="</div>";

     stringx +="</div>";

     stringcon +="<div id='formaddcontent"+item.task_ID+"' class='m-portlet__body formcontent'>";
     stringcon +="<input type='hidden' class='hiddentask' value='"+item.checklistStatus_ID+"'>"

     stringcon +="<div id='rowdisplay"+item.checklistStatus_ID+"' class='row'>";
     stringcon +="</div>";
     stringcon += "</div>";
   }
 });
stringbutton +="<div class='col-12 butpadding'>";
stringbutton +="<hr><a href='#' class='btn btn-success m-btn  m-btn m-btn--icon'><span><i class='fa fa-check'></i><span>Complete Task</span></span></a>&nbsp;";
stringbutton +="<a href='#' class='btn btn-metal m-btn m-btn m-btn--icon'><span><span>Next</span>&nbsp;<i class='fa fa-chevron-right'></i></span></a>";
stringbutton +="</div>";

$("#checkgroup").html(stringx);
a=$('.checkedC:checked').length;
$(".inputtask:first").css({"background-color":"#51aff9","color":"white"});
$(".inputtask:first").addClass("placeholdercolor");
var firstaskid=$(".inputtask:first").attr('data-taskid');
$("#contentdiv").append(stringcon);
$("#contentdiv").append(stringbutton);
$(".formcontent").hide();
$(".formcontent:first").show();
display_allcomponents(firstaskid);
}
});
}
//display answers
function display_formanswers(taskid){
  console.log('taskid:'+taskid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_allanswers",
    data: {
      task_ID:taskid,
      checklist_ID: cid
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
      $.each(result, function (key, item) {
        var radioval="";
        $("#answer"+item.subtask_ID).val(item.answer);
        if(item.component_ID==7){
          $("#itemlist"+item.subtask_ID).val(item.answer);
        }
        else if(item.component_ID==6){
          radioval=$("#radioopt"+item.componentSubtask_ID).val();
          if(radioval==item.answer){
            $("#radioopt"+item.componentSubtask_ID).attr('checked',true);
          }
        }
        else if(item.component_ID==8){
         var checkval=item.answer;
         var cb=$("#checkBoxinput"+item.componentSubtask_ID).val();
         console.log('answer:'+checkval);
         console.log('YY:'+cb);
         if(cb==checkval){
           $("#checkBoxinput"+item.componentSubtask_ID).attr('checked',true);
         }
       }
       else if(item.component_ID==10){
        $("#getdate"+item.subtask_ID).val(item.answer);
      }
      else if(item.component_ID==13){
        $("#gettime"+item.subtask_ID).val(item.answer);
      }
      else if(item.component_ID==14){
       $("#getdatetime"+item.subtask_ID).val(item.answer);
     }
   });
    }
  });
}
function display_allcomponents(taskid){
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/fetch_allsubtask",
  data: {
    task_ID:taskid
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   var stringx = "";
   $.each(result, function (key, item) {
     if(item.component_ID==1){
      stringx+="<div id='"+item.subTask_ID+"'class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
      if(item.complabel!=null){
        stringx+=item.complabel;
      }else{
        stringx+="No label";
      }
      stringx+="</div>";
      stringx+="<input type='text' data-component='"+item.component_ID+"' id='answer"+item.subTask_ID+"' data-id='"+item.subTask_ID+"' onkeyup='autosaveanswer(this)' class='form-control m-input m-input--air m-input--pill formdistance inputcolor' >";
      if(item.sublabel!=null){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' class='formdistance' style='font-size:1em;color:#b9bbc1'>'"+item.sublabel+"'</div>";
      }
      stringx+="</div>";   
    }
    else if(item.component_ID==2){
      stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
      if(item.complabel!=null){
        stringx+=item.complabel;
      }else{
        stringx+="Type a label";
      }
      stringx+="</div>";
      stringx+="<form>";
      stringx+="<textarea id='answer"+item.subTask_ID+"' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onkeyup='autosaveanswer(this)' class='inputcolor form-control m-input m-input--air m-input--pill'></textarea>";
      stringx+="</form>";
      if(item.sublabel!=null){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' style='font-size:1em;color:#555658'>"+item.sublabel+"</div>";
      }
      stringx+="</div>";
    }
    else if(item.component_ID==3){
     stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
     stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='labelheading'>";
     if(item.complabel!=null){
      stringx+=item.complabel;
    }else{
      stringx+="Heading";
    }
    stringx+="</div>";
    if(item.sublabel!=null){
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' style='font-size:1.1em;color:#555658'>"+item.sublabel+"</div>";
    }
    stringx+="</div>";
  }
  else if(item.component_ID==4){
   stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
   stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>"; 
   if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="You have not yet typed any text in this field.";
  }
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==5){
 stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
 if(item.complabel!=null){
  stringx+=item.complabel;
}else{
  stringx+="No number label";
}
stringx+="</div>";
stringx+="<input type='number' data-component='"+item.component_ID+"' id='answer"+item.subTask_ID+"' data-id='"+item.subTask_ID+"' onchange='autosaveanswer(this)' class='form-control m-input m-input--air m-input--pill inputcolor'>";
if(item.sublabel!=null){
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' style='font-size:1em;color:#555658'>"+item.sublabel+"</div>";
}
stringx+="</div>";
}
else if(item.component_ID==6){
  stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label formdistance'>";
  if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="Type a label";
  }
  stringx+="</div>";

  if(item.sublabel!=null){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' style='font-size:1em;color:#555658'>"+item.sublabel+"</div>";
  }
  stringx+="<div id='radiolist"+item.subTask_ID+"' class='row m-radio-list formdistance'>";

  stringx+="</div>";
  stringx+="</div>";
  radio_option(item.subTask_ID,item.component_ID);
}
else if(item.component_ID==7){
  stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
  if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="Type a label";
  }
  stringx+="</div>";
  if(item.sublabel!=null){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' style='font-size:1em;color:#555658'>"+item.sublabel+"</div>";
  }

  stringx+="<div class='form-group m-form__group'>";
  stringx+="<select data-id='"+item.subTask_ID+"' onchange='autosaveanswer(this)' data-component='"+item.component_ID+"' id='itemlist"+item.subTask_ID+"' class='form-control formdistance m-input m-input--air m-input--pill' id='exampleSelect1'>";
  stringx+="</select>";
  stringx+="</div>";

  stringx+="<div class='form-group m-form__group formdistance'>";
  stringx+="<ul  class='list-group'>";
  stringx+="</ul>";
  stringx+="</div>";
  stringx+="</div>";
  dropdown_list(item.subTask_ID);
}
else if(item.component_ID==8){
 stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
 if(item.complabel!=null){
  stringx+=item.complabel;
}else{
  stringx+="No label";
}
stringx+="</div>";
if(item.sublabel!=null){
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' style='font-size:1em;color:#555658'>"+item.sublabel+"</div>";
}
stringx+="<div id='checkoption"+item.subTask_ID+"' class='row m-checkbox-list formdistance'>";
stringx+="</div>";
stringx+="</div>";
checkbox_option(item.subTask_ID,item.component_ID);
}
else if(item.component_ID==9){
 stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 distance'>";
 stringx+="<hr>";
 stringx+="</div>";
}
else if(item.component_ID==10){
 stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
 if(item.complabel!=null){
  stringx+=item.complabel;
}else{
  stringx+="No Date label";
}
stringx+="</div>";
stringx+="<div class='input-group formdistance'>";
stringx+="<input type='text' data-component='"+item.component_ID+"' onmouseover='fetchdatepicker(this)' data-id='"+item.subTask_ID+"' id='getdate"+item.subTask_ID+"' onchange='autosaveanswer(this)' class='form-control m-input m-input--air m-input--pill inputcolor' placeholder='Click to select date here...' name='date'>";
stringx+="<div class='input-group-append'>";
stringx+="<span class='input-group-text'>";
stringx+="<i class='la la-calendar-check-o'></i>";
stringx+="</span>";
stringx+="</div>";
stringx+="</div>";
if(item.sublabel!=null){
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
}
stringx+="</div>";
}
else if(item.component_ID==11){
  stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
  if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="Type a label";
  }
  stringx+="</div>";
  stringx+="<input id='answer"+item.subTask_ID+"' data-id='"+item.subTask_ID+"' onchange='autosaveanswer(this)' class='form-control m-input m-input--air m-input--pill formdistance' type='number' value='0' min='0' max='100' step='1'/>"; 
  if(item.sublabel!=null){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
  }
  stringx+="</div>";
}
else if(item.component_ID==12){
  stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
  if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="No Email Label";
  }
  stringx+="</div>";
  stringx+="<input type='email' data-component='"+item.component_ID+"' id='answer"+item.subTask_ID+"' data-id='"+item.subTask_ID+"' onkeyup='autosaveanswer(this)' class='form-control m-input m-input--air m-input--pill inputcolor'>";
  if(item.sublabel!=null){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
  }
  stringx+="</div>";
}
else if(item.component_ID==13){
 stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
 if(item.complabel!=null){
  stringx+=item.complabel;
}else{
  stringx+="Type Time Label";
}
stringx+="</div>";
stringx+="<div class='input-group formdistance'>"
stringx+="<input data-component='"+item.component_ID+"' onchange='autosaveanswer(this)' type='text' class='form-control m-input m-input--air m-input--pill inputcolor' onmouseover='fetchtimepicker(this)' data-id='"+item.subTask_ID+"' name='date' placeholder='Click to select time here...' id='gettime"+item.subTask_ID+"' aria-describedby='m_datepicker-error' aria-invalid='false'>";
stringx+="<div class='input-group-append'>";
stringx+="<span class='input-group-text'>";
stringx+="<i class='la la-clock-o'></i>";
stringx+="</span>";
stringx+="</div>";
stringx+="</div>";
if(item.sublabel!=null){
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
}
stringx+="</div>";
}
else if(item.component_ID==14){
  stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' class='label'>";
  if(item.complabel!=null){
    stringx+=item.complabel;
  }else{
    stringx+="Type Date and Time Label";
  }
  stringx+="</div>";
  stringx+="<div class='input-group formdistance'>";
  stringx+="<input type='text' onchange='autosaveanswer(this)' data-component='"+item.component_ID+"' class='form-control m-input m-input--air m-input--pill inputcolor' onmouseover='fetchdatetimepicker(this)' data-id='"+item.subTask_ID+"' name='date' id='getdatetime"+item.subTask_ID+"'placeholder='Click to select date and time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false'>";
  stringx+="<div class='input-group-append'>";
  stringx+="<span class='input-group-text'>";
  stringx+="<i class='la la-calendar-check-o'></i>";
  stringx+="</span>";
  stringx+="</div>";
  stringx+="</div>";

  if(item.sublabel!=null){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' class='formdistance' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
  }
  stringx+="</div>";
}
});
$("#formaddcontent"+taskid).html(stringx);
display_formanswers(taskid);
}
});
}
function displayformdiv(element){
  var taskid=$(element).data('taskid');
  var checkstatid=$(element).data('id');
  console.log(taskid);
  display_allcomponents(taskid);
  $(".formcontent").hide();
  $("#formaddcontent"+taskid).show();
  $(".inputtask").css({"background-color":"","color":""});
  $(".inputtask").removeClass("placeholdercolor");
  $("#inputbox_id"+checkstatid).css({"background-color":"#51aff9","color":"white"});
  $("#inputbox_id"+checkstatid).addClass("placeholdercolor");
}
function checkall(){
  status="3";
  $('.checkedC').prop("checked", true);
  $(".crashout").css({"text-decoration":"line-through","font-style":"italic","color":"#b3b2b2"});
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_taskAllStatus",
    data: {
      checklist_ID: cid,
      isCompleted: status
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
    },
    error: function (res){
      console.log(res);
    }
  });
  checkcompleted();
}
function uncheckall(){
  status="2";
  $('.checkedC').prop("checked", false);
  $(".crashout").css({"text-decoration":"","font-style":"","color":""});
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_taskAllStatus",
    data: {
      checklist_ID: cid,
      isCompleted: status
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
      swal({
        position: 'center',
        type: 'success',
        title: 'Reactivate Checklist!',
        showConfirmButton: false,
        timer: 1500
      });
      window.location.href = "<?php echo base_url('process/checklist_answerable')?>"+"/"+prid+"/"+cid;
    },
    error: function (res){
      console.log(res);
    }
  });
  checknotcomplete();
}
function changecheckbox(element){
  var statusID=$(element).data('id');
  var ct=$("#countTask").val();
  console.log('a='+a);
  console.log('ct='+ct);
  if ($("#checkbox_id"+statusID).is(':checked')) {
    status="3";
    $("#inputbox_id"+statusID).css({"text-decoration":"line-through","font-style":"italic","color":"#b3b2b2"});
    a++;
    console.log('after a='+a);  
  }
  else{
   status="2";
   $("#inputbox_id"+statusID).css({"text-decoration":"","font-style":"","color":""});
   $("#uncheckall").remove();
   $("#checkall").remove();
   $("h3#appendbutton").append("<button id='checkall' class='btn btn-success btn-sm m-btn m-btn--icon'><span><i class='fa fa-check'></i><span>Complete this checklist</span></span></button>");
   a--;
   console.log('dec a='+a);
 };
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/update_taskStatus",
  data: {
    checklistStatus_ID: statusID,
    isCompleted: status
  },
  cache: false,
  success: function (res) {
    var result = JSON.parse(res.trim());
    console.log(res);
  },
  error: function (res){
    console.log(res);
  }
});
 if (a == ct) {
  status="3";
  console.log("completed");
  checkcompleted();
}else{
  console.log("incomplete");
  checknotcomplete();
}
}
function checkcompleted(){
  status="3";
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_checklistStatus",
    data: {
      checklist_ID:cid,
      status_ID:status
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
      swal({
        position: 'center',
        type: 'success',
        title: 'Checklist Completed!',
        showConfirmButton: false,
        timer: 1500
      });
      window.location.href = "<?php echo base_url('process/checklist_answerable')?>"+"/"+prid+"/"+cid;   
    },
    error: function (res){
      console.log(res);
    }
  });
}
function checknotcomplete(){
 status="2";
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/update_checklistStatus",
  data: {
    checklist_ID: cid,
    status_ID: status
  },
  cache: false,
  success: function (res) {
    var result = JSON.parse(res.trim());    
  },
  error: function (res){
    console.log(res);
  }
});
}
function radio_option(subtaskid,componentid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' class='m-radio m-radio--bold m-radio--state-primary'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Double click and type option...";
   }
   stringcomp+="<input id='radioopt"+item.componentSubtask_ID+"' data-id='"+subtaskid+"' name='radioanswer' data-component='"+componentid+"' data-subtask='"+subtaskid+"' type='radio'  onchange='autosaveanswer(this)' value='"+item.compcontent+"'>";
   stringcomp+="<span></span>";
   stringcomp+="</label>";
   stringcomp+="</div>";
 });
   $("#radiolist"+subtaskid).html(stringcomp);
 }
});
}
function dropdown_list(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<option>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Item list...";
   }
   stringcomp+="</option>";
 });
   $("#itemlist"+subtaskid).html(stringcomp);
 }
});
}
function checkbox_option(subtaskid,componentid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   // var countform=Object.keys(result).length;
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' class='m-checkbox m-checkbox--bold m-checkbox--state-brand'>";
    stringcomp+="<input id='checkBoxinput"+item.componentSubtask_ID+"' onchange='savemultipleanswer(this)' class='checkedBox' data-id='"+subtaskid+"' name='checkboxanswer' data-component='"+componentid+"' type='checkbox' data-check='"+item.componentSubtask_ID+"' value='"+item.compcontent+"'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
    stringcomp+="No label...";
  }
  stringcomp+="<span></span>";
  stringcomp+="</label>";
  stringcomp+="</div>";
});
   $("#checkoption"+subtaskid).html(stringcomp);
   // var pr=$('.checkedBox:checked').length;
 }
});
}
function fetchdatepicker(element){
 var subtaskkid=$(element).data('id');
 $("#getdate"+subtaskkid).datepicker({
  format: 'yyyy-mm-dd',
  autoclose: true,
  todayBtn: true
});
}
function fetchdatetimepicker(element){
 var subtaskkid=$(element).data('id');
 $("#getdatetime"+subtaskkid).datetimepicker({
  format: 'yyyy-mm-dd hh:ii',
  autoclose: true,
  todayBtn: true
});
}
function fetchtimepicker(element){
 var subtaskkid=$(element).data('id');
 $("#gettime"+subtaskkid).timepicker({
  format: 'hh:ii',
  autoclose: true,
  todayBtn: true
});
}
//Save Answers
function savemultipleanswer(element){
  var subtaskid=$(element).data('id');
  var compsubtaskid=$(element).data('check');
  var answercontent="";
  if ($("#checkBoxinput"+compsubtaskid).is(':checked')) {
    console.log('checked');
    answercontent=$("#checkBoxinput"+compsubtaskid).val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_multiple_answer",
      data: {
        subtask_ID: subtaskid,
        checklistid: cid,
        answer: answercontent
      },
      cache: false,
      success: function (res) {
        res = JSON.parse(res.trim());
        var data=res;
        console.log(res);
        clearTimeout(timer); 
        timer = setTimeout(doNotif, 1000);
      },
      error: function (res){
        console.log(res);
      }
    });
  }else{
   console.log('Unchecked');
  //  answercontent=$("#checkBoxinput"+compsubtaskid).val();
  //  console.log(answercontent);
  //  $.ajax({
  //   type: "POST",
  //   url: "<?php echo base_url(); ?>process/",
  //   data: {
  //     subtask_ID: subtaskid,
  //     checklistid: cid,
  //     answer: answercontent
  //   },
  //   cache: false,
  //   success: function (res) {
  //     res = JSON.parse(res.trim());
  //     var data=res;
  //     console.log(res);
  //     clearTimeout(timer); 
  //     timer = setTimeout(doNotif, 1000);
  //   },
  //   error: function (res){
  //     console.log(res);
  //   }
  // });
}
}
function autosaveanswer(element){
  var subtaskid=$(element).data('id');
  var comp=$(element).data('component');
  var answercontent="";
  if(comp=="7"){
    answercontent=$("#itemlist"+subtaskid).val();
  }else if(comp=="10"){
    answercontent=$("#getdate"+subtaskid).val();
  }else if(comp=="13"){
   answercontent=$("#gettime"+subtaskid).val();
  }else if(comp=="14"){
   answercontent=$("#getdatetime"+subtaskid).val();
  }else if(comp=="6"){
   answercontent=$('input[name=radioanswer]:checked').val();
  }
  else{
  answercontent=$("#answer"+subtaskid).val(); 
  }
$.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/add_answer",
  data: {
    subtask_ID: subtaskid,
    checklistid: cid,
    answer: answercontent
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    var data=res;
    console.log(res);
    clearTimeout(timer); 
    timer = setTimeout(doNotif, 1000);
  },
  error: function (res){
    console.log(res);
  }
});
}
</script>



