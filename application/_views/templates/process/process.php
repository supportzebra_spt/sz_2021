<style type="text/css">
.tabtopcolor{
  background-color: #3d3e4d !important;
}
.dropdownstyle{
  position: absolute; 
  transform: translate3d(0px, 37px, 0px); 
  top: 0px; 
  left: 0px; 
  will-change: transform;
}
.foldercolor{
  background-color:#585858;
  border-width: 15px
}
.scrollstyle{
  overflow:hidden; 
  height: 200px;
}
.notestyle{
  font-size:0.9em;
  color:#4aab69;
}
.insidefolder{
  background:#e2ebec;
  margin-top: 3em;
}
.colorblue{
  color:#3297e4 !important;
}
.colorlightblue{
  background-color:#9cdfda;
}
.colorwhitesize{
  color:white !important;
  font-size:0.9em;
}
.colorwhiteicons{
  color:white;
}
.coloricons{
  color:#4371a2 !important;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>       
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="#" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="http://localhost/supportz/process" class="m-nav__link">
              <span class="m-nav__link-text">Folder</span>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="#" class="m-nav__link">
              <span class="m-nav__link-text">Process</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">
       <div class="m-portlet m-portlet--tabs">
        <div class="m-portlet__head tabtopcolor">
          <div class="m-portlet__head-tools">
            <ul class="nav nav-tabs m-tabs-line m-tabs-line--accent m-tabs-line--2x" role="tablist">
             <li class="nav-item m-tabs__item">
              <a class="nav-link m-tabs__link active" data-toggle="tab" href="#foldertab" role="tab">
                <i class="fa fa-folder"></i>Folders</a>
              </li>
              <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link" data-toggle="tab" href="#processtab" role="tab">
                  <i class="fa fa-file-text-o"></i>Process Templates</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="m-portlet__body">                   
            <div class="tab-content">
             <div class="tab-pane fade show active col-md-12" id="foldertab" role="tabpanel">
              <!--Begin- Search and filter -->
              <div class="m-form m-form--label-align-right m--margin-bottom-30">
                <div class="row align-items-center">
                  <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                      <div class="col-md-8">
                        <div class="m-input-icon m-input-icon--left">
                          <input type="text" class="form-control m-input " id="searchString" placeholder="Search by folder name..." >
                          <span class="m-input-icon__icon m-input-icon__icon--left">
                            <span><i class="la la-search"></i></span>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="m-input-icon">
                          <select id="selectStatus" class="form-control m-input m-input--square">
                            <option value="0">All</option>
                            <option value="10">Active</option>
                            <option value="11">Inactive</option>
                            <option value="00">Assigned</option>
                            <option value="15">Archived</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <button class="btn btn-accent dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i>&nbsp;
                      New
                    </button>
                    <div class="dropdown-menu dropdownstyle" aria-labelledby="dropdownMenu2" x-placement="bottom-start">
                      <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template</button>
                      <button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button>
                      <hr>
                      <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Folder</button>
                    </div>                 
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                  </div>
                </div>
              </div>
              <!--end: Search and Filter -->

              <!--start folder dispaly-->
              <div id="folrow" class="row">
              </div>
              <!--End folder display-->
            </div>
            <div class="tab-pane fade col-md-12" id="processtab" role="tabpanel">
             <div class="m-form m-form--label-align-right m--margin-bottom-30">
              <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="col-md-8">
                      <div class="m-input-icon m-input-icon--left">
                        <input id="searchProcess" type="text" class="form-control m-input" placeholder="Search by process, folder name...">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span><i class="la la-search"></i></span>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="m-input-icon">
                        <select id="selectedprocessval" class="form-control m-input m-input--square">
                          <option value="0">All</option>
                          <option value="10">Active</option>
                          <option value="2">Pending</option>
                          <option value="11">Inactive</option>
                          <option value="00">Assigned</option>
                          <option value="15">Archived</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                  <button class="btn btn-accent dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i>&nbsp;
                  New</button>
                  <div class="dropdown-menu dropdownstyle" aria-labelledby="dropdownMenu2" x-placement="bottom-start">
                    <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template</button>
                    <button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button><hr>
                    <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Folder</button>
                  </div>                 
                  <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
              </div>
            </div>
            <!--end: Search Form -->
            <div id="prorow" class="row">
            </div>
          </div>
          <div class="tab-pane fade col-md-12" id="checklisttab" role="tabpanel">
            <!--Begin- Search and filter -->
            <div class="m-form m-form--label-align-right m--margin-bottom-30">
              <div class="row align-items-center">
                <div class="col-xl-6 order-2 order-xl-1">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="col-md-8">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span><i class="la la-search"></i></span>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="m-input-icon">
                        <select class="form-control m-input m-input--square" id="">
                          <option>All</option>
                          <option>Active</option>
                          <option>Inactive</option>
                          <option>Assigned</option>
                          <option>Archived</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-6 order-1 order-xl-2 m--align-right">
                  <button class="btn btn-accent dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i>&nbsp;
                    New
                  </button>
                  <div class="dropdown-menu dropdownstyle" aria-labelledby="dropdownMenu2" x-placement="bottom-start">
                    <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template</button>
                    <button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button>
                    <hr>
                    <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Folder</button>
                  </div>                 
                  <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
              </div>
            </div>
            <!--end: Search and Filter -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--begin::Modal-->
  <div class="modal fade" id="folder-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Folder</h5>
          <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="formadd" name="formadd" method="post">
            <div class="form-group">
              <label for="recipient-name" class="form-control-label">Folder Name:</label>
              <input type="text" class="form-control errorprompt" id="folderName" name="folderName" placeholder="Enter Folder Name">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" name="submit" id="btn-foldersave" class="btn btn-accent">Save</button>
        </div>
      </div>
    </div>
  </div>
  <!--end::Modal-->
  <!--begin::Modal-->
  <div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Folder Name</h5>
          <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="formupdate" name="formupdate" method="post">
            <div class="form-group">
              <label for="recipient-name" class="form-control-label">Folder Name:</label>
              <input type="text" class="form-control" id="updatefolderName" name="updatefolderName" placeholder="Enter Folder Name">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" name="submit" id="btn-foldersavechanges" onclick="saveChangesFolder(this)" class="btn btn-accent">Save Changes</button>
        </div>
      </div>
    </div>
  </div>
  <!--end::Modal-->

  <!--begin::Modal-->
  <div class="modal fade" id="updateProcessmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Process Name</h5>
          <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="formupdate" name="formupdate" method="post">
            <div class="form-group">
              <label for="recipient-name" class="form-control-label">Process Name:</label>
              <input type="text" class="form-control" id="updateprocessName" name="updateprocessName" placeholder="Enter Process Name">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" name="submit" id="btn-processsavechanges" onclick="saveChangesProcess(this)" class="btn btn-accent">Save Changes</button>
        </div>
      </div>
    </div>
  </div>
  <!--end::Modal-->

  <!--begin::Modal-->
  <div class="modal fade" id="blank-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Blank Template</h5>
          <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="processadd" name="processadd" method="post">
            <div class="form-group">
              <label for="recipient-name" class="form-control-label">Template Name:</label>
              <input type="text" maxlength="100" class="form-control pErrorprompt" id="processTitle" name="processTitle">
              <span style="font-size:0.9em;color:gray" id="charNum"></span>
            </div>
            <div class="form-group">
              <label>Choose folder:</label>
              <select class="form-control m-input" id="folder_ID" name="folder_ID">
                <?php foreach ($folders as $folder) { ?>
                  <option value='<?php echo $folder->folder_ID ?>'><?php echo $folder->folderName ?></option>
                <?php } ?>
              </select>
              <p class="m-form__help text-center notestyle" id="leave_description"><i><strong>Note:</strong> You are going to customize your own template.</i></p>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" name="submit" id="btn-processsave" class="btn btn-accent m-btn m-btn--icon">
            <span>
              <i class="fa fa-check"></i>
              <span>Save</span>
            </span>
          </button>
        </div>
      </div>
    </div>
  </div>
  <!--end::Modal-->
</div>
</div>
</div>
<script type="text/javascript">
  $(function(){
    displayfolders();
    displayprocesstemp();
    $(window).scroll(function(){
     var str = $("#searchString").val();
     var strprocess = $("#searchProcess").val();
     var lastID = $('.load-more').attr('lastID');
     var processlastID = $('.processload-more').attr('processlastID');

     if(($(window).scrollTop() == $(document).height() - $(window).height()) && (processlastID != 0)){
      $.ajax({
        type:'POST',
        url: "<?php echo base_url(); ?>process/display_moreprocess",
        data: {
          process_ID:processlastID,
          str:strprocess
        },
        beforeSend:function(){
          $('.processload-more').show();
        },
        success:function(res){
         $('.processload-more').remove();
         var result = JSON.parse(res.trim());
         var pstringx="";
         var plastid=0;
         var pstringlastid="";
         $.each(result, function (key, item) {
          plastid=item.process_ID; 
          var processname=item.processTitle;
          var substringprocess="";
          if(processname.length > 22){
           substringprocess = processname.substring(0,22)+"...";
         }else{
          substringprocess = processname.substring(0,22);
        }
        pstringx +="<div class='col-lg-4 processcontent pr+processtatusid'>";
        pstringx +="<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>"
        pstringx +="<div class='m-portlet__head border-accent rounded colorlightblue'>";
        pstringx +="<div class='row' style='margin-top:1em'>";
        pstringx +="<div class='m-portlet__head-caption'>";
        pstringx +="<div class='m-portlet__head-title'>";
        pstringx +="<span class='m-portlet__head-icon'>";
        pstringx +="<i class='fa fa-file-text-o colorblue'></i>";
        pstringx +="</span>";
        if(item.processstatus_ID==10){
          pstringx +="<a data-toggle='tooltip' data-placement='bottom' title='"+item.processTitle+"' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist')?>"+'/'+item.process_ID+"' class='m-portlet__head-text protitle'>"+substringprocess+"</a>";
        }else if(item.processstatus_ID==2){
         pstringx +="<a data-toggle='tooltip' data-placement='bottom' title='"+item.processTitle+"' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist_pending')?>"+'/'+item.process_ID+"' class='m-portlet__head-text protitle'>"+item.processTitle+"</a>";
       }
       pstringx +="</div>";
       pstringx +="</div>";
       pstringx +="</div>";
       pstringx +="<span style='font-size: 0.9em;margin-left:1em'>";
       pstringx +="<span>"+item.dateTimeCreated+"</span><br>";
       pstringx +="</span>";
       pstringx += "<span class='folname' style='color:#088594;font-size: 0.9em;margin-left:1em'><b>"+item.folderName+"</b></span>";
       pstringx +="<input type='hidden' class='proStat' value='"+item.processstatus_id+"'>";
       pstringx +="<input type='hidden' class='eachprocessid' value="+item.process_id+">";
       pstringx +="<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
       pstringx +="<div class='m-portlet__head-tools'>";
       pstringx +="<ul class='m-portlet__nav'>";
       pstringx +="<li class='m-portlet__nav-item'>";
       pstringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
       pstringx +="<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";
       pstringx +="<button class='dropdown-item' type='button' data-id="+item.process_ID+" onclick='updateProcessFetch(this)' data-toggle='modal' data-target='#updateProcessmodal'><i class='flaticon-file'></i>&nbsp;Edit Process Name</button>";
       pstringx +="<button class='dropdown-item' type='button'><i class='flaticon-file'>";
       pstringx +="</i>&nbsp;Assign this Process</button>";
       pstringx +="</div> ";
       pstringx +="</li>";

       pstringx +="<li class='m-portlet__nav-item'>";
       pstringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle coloricons'></i></a> " ;
       pstringx +="</li>";

       pstringx +="<li class='m-portlet__nav-item'>";
       pstringx +="<a href='#' data-portlet-tool='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='fa fa-trash-o coloricons'></i></a> "  ; 
       pstringx +="</li> &nbsp;|&nbsp;";

       if(item.processstatus_ID==10){
        pstringx += " <li class='m-portlet__nav-item'>";
        pstringx +=" <span class='m-badge m-badge--info m-badge--wide' data-toggle='m-tooltip' data-placement='top' title data-original-title='Click process template to add, modify and run a checklist'>Run a Checklist</span>";
        pstringx +="</li> ";
      }else if(item.processstatus_ID==2){
        pstringx += "<li class='m-portlet__nav-item'>";
        pstringx += " <span class='m-menu__link-badge'>";
        pstringx += " <span class='m-badge m-badge--metal' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Checklist'>2</span>";
        pstringx +="</span>"  ; 
        pstringx +="</li> " ;
        pstringx +="<li class='m-portlet__nav-item'>";
        pstringx +=" <span class='m-menu__link-badge'>";
        pstringx +="<span class='m-badge m-badge--info' data-toggle='m-tooltip' data-placement='top' title data-original-title='Pending Checklist'>2</span>";
        pstringx +="</span>"; 
        pstringx +="</li>";
        pstringx +="<li class='m-portlet__nav-item'>";
        pstringx +="<span class='m-menu__link-badge'>";
        pstringx +="<span class='m-badge m-badge--danger' data-toggle='m-tooltip' data-placement='top' title data-original-title='Overdue Checklist'>2</span>";
        pstringx +="</span>";   
        pstringx +="</li>";
        pstringx +="<li class='m-portlet__nav-item'>";
        pstringx +="<span class='m-menu__link-badge'>";
        pstringx +="<span class='m-badge m-badge--success' data-toggle='m-tooltip' data-placement='top' title data-original-title='Completed Checklist'>2</span>";
        pstringx +=" </span>" ;  
        pstringx +="</li>";
      }
      pstringx +="</ul>";
      pstringx += "</div>";
      pstringx +="</div>";
      pstringx +="</div>";
      pstringx +="</div>"
      pstringx +="</div>";

    });
$("#prorow").append(pstringx);
pstringlastid+= "<div class='processload-more col-lg-12' processlastID='"+plastid+"' style='display: none;'>";
pstringlastid+= "<h5 style='text-align: center;'>";
pstringlastid+="<div class='m-loader m-loader--info' style='width: 30px; display: inline-block;'>";
pstringlastid+="</div> Loading more Process Templates...</h5></div>";
$("#prorow").append(pstringlastid);
}
});
}
if(($(window).scrollTop() == $(document).height() - $(window).height()) && (lastID != 0)){
  $.ajax({
    type:'POST',
    url: "<?php echo base_url(); ?>process/display_morefolders",
    data: {
      folder_ID:lastID,
      str:str
    },
    beforeSend:function(){
      $('.load-more').show();
    },
    success:function(res){
      $('.load-more').remove();
      var result = JSON.parse(res.trim());
      var stringx = "";
      var lastid=0;
      var stringlastid="";
      $.each(result, function (key, item) {
       lastid=item.folder_ID;
       var foldername=item.folderName;
       var substringname="";
       if(foldername.length > 22){
        substringname = foldername.substring(0,22)+"...";
      }else{
        substringname = foldername.substring(0,22);
      }
      stringx +=  "<div class='col-lg-4 foldercontent' id='"+item.status_ID+"'>"; 
      stringx +=  "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
      stringx +=  "<div class='m-portlet__head border-accent rounded foldercolor'>";

      stringx +="<div class='row' style='margin-top:1em'>";
      stringx +="<div class='m-portlet__head-caption'>";
      stringx += "<div class='m-portlet__head-title'>";
      stringx += "<span class='m-portlet__head-icon'>";
      stringx += "<i class='fa fa-folder' id='"+item.folder_ID+"'></i>";
      stringx +="</span>";
      stringx +="<a data-toggle='tooltip' data-placement='bottom' title='"+item.folderName+"' style='color:#9ce4f5;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/subfolder')?>"+'/'+item.folder_ID+"' class='foldname'>"+substringname+"</a>";
      stringx += "<input type='hidden' value='"+item.folder_ID+"' id='eachfolderid'>";
      stringx += "<input type='hidden' class='folStat' value='"+item.status_ID+"'";
      stringx += "<input type='hidden' class='fetchval' id='fetchval'>" ;
      stringx += "</div>"; 
      stringx +="</div>";
      stringx += "</div>";
      stringx +="&nbsp;&nbsp;&nbsp;&nbsp;<span class='colorwhitesize createdby'>"+item.fname+"&nbsp;"+item.lname+"</span>";

      stringx +="<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
      stringx +="<div class='m-portlet__head-tools'>";
      stringx += "<ul class='m-portlet__nav'>";
      stringx += "<li class='m-portlet__nav-item'>";
      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
      stringx += "<i class='la la-cog'></i></a>";
      stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
      stringx +="<button class='dropdown-item' type='button' data-id='"+item.folder_ID+"' onclick='updateFetch(this)' data-toggle='modal' data-target='#updatemodal'>";
      stringx +="<i class='flaticon-file'></i>&nbsp;Change folder name</button>";
      stringx +="<button class='dropdown-item' type='button'>";
      stringx +="<i class='la la-archive'></i>";
      stringx +="&nbsp;Archive this folder</button>";
      stringx +="<button class='dropdown-item' type='button' data-id='"+item.folder_ID+"' onclick='deleteFolder(this)'>";
      stringx +="<i class='la la-trash'></i>&nbsp;Delete this folder</button>";
      stringx += "</div>";
      stringx += "</li>";

      stringx +="<li class='m-portlet__nav-item'>";
      stringx +="<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle'></i></a> ";
      stringx +="</li>"
      stringx +="<li class='m-portlet__nav-item'>";
      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'>";
      stringx += "<i class='la la-users'></i>";
      stringx +="</a>";
      stringx +="</li>";
      stringx +="<li class='m-portlet__nav-item colorwhiteicons'>";
      stringx +="<span>|</span>";
      stringx +="</li>";
      stringx +="<li class='m-portlet__nav-item'>";
      stringx +="<span class='m-menu__link-badge'>";
      stringx +="<span class='m-badge m-badge--metal' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Process Templates'>1</span>";
      stringx +="</span>";  
      stringx +="</li>";
      stringx += " <li class='m-portlet__nav-item'>";
      stringx +="<span class='m-menu__link-badge'>";
      stringx +="<span class='m-badge m-badge--success' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Checklists'>1</span>";
      stringx +="</span> ";
      stringx += "</li>";
      stringx +="<li class='m-portlet__nav-item'>";
      stringx +="<span class='m-menu__link-badge'>";
      stringx += "<span class='m-badge m-badge--danger' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Pending Checklists'>1</span>";
      stringx +="</span> "; 
      stringx += "</li>";

      stringx +="</ul>";
      stringx +="</div>";
      stringx +="</div>";

      stringx += "</div>";
      stringx += "</div>";
      stringx +="</div>";
    });
$("#folrow").append(stringx);
stringlastid+= "<div class='load-more col-lg-12' lastID='"+lastid+"' style='display: none;'>";
stringlastid+= "<h5 style='text-align: center;'>";
stringlastid+="<div class='m-loader m-loader--info' style='width: 30px; display: inline-block;'>";
stringlastid+="</div> Loading more folder...</h5></div>";
$("#folrow").append(stringlastid);
}
});
}
});
});
$("#searchString").on("keyup",function(){
 var str = $(this).val();
 displayfolders(str);
});
$("#selectedprocessval").on("change", function(){
 var selectedval = $(this).val();
 if(selectedval=="0"){
   displayprocesstemp();
 }else if(selectedval=="00"){
  displayprocesstemp();
}
else if(selectedval=="15"){
  displayprocesstemp();
}else{
 $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/get_dropdownproinfo",
   data: {
    status_ID:selectedval
  },
  cache: false,
  success: function (res) {
    var result = JSON.parse(res.trim());
    var stringx="";
    var substringname="";
    $.each(result, function (key, item) {
     lastid=item.process_ID;
     var processname=item.processTitle;
     var substringprocess="";
     if(processname.length > 22){
       substringprocess = processname.substring(0,22)+"...";
     }else{
      substringprocess = processname.substring(0,22);
    }
    stringx +="<div class='col-lg-4 processcontent pr+processtatusid'>";
    stringx +="<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>"
    stringx +="<div class='m-portlet__head border-accent rounded colorlightblue'>";
    stringx +="<div class='row' style='margin-top:1em'>";
    stringx +="<div class='m-portlet__head-caption'>";
    stringx +="<div class='m-portlet__head-title'>";
    stringx +="<span class='m-portlet__head-icon'>";
    stringx +="<i class='fa fa-file-text-o colorblue'></i>";
    stringx +="</span>" ;

    if(item.processstatus_ID==10){
      stringx +="<a data-toggle='tooltip' data-placement='bottom' title='"+item.processTitle+"' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist')?>"+'/'+item.process_ID+"' class='m-portlet__head-text protitle'>"+substringprocess+"</a>";
    }else if(item.processstatus_ID==2){
     stringx +="<a data-toggle='tooltip' data-placement='bottom' title='"+item.processTitle+"' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist_pending')?>"+'/'+item.process_ID+"' class='m-portlet__head-text protitle'>"+substringprocess+"</a>";
   }   
   stringx +="</div>";
   stringx +="</div>";
   stringx +="</div>";
   stringx +="<span style='font-size: 0.9em;margin-left:1em'>";
   stringx +="<span>"+item.dateTimeCreated+"</span><br>";
   stringx +="</span>";
   stringx += "<span class='folname' style='color:#088594;font-size: 0.9em;margin-left:1em'><b>"+item.folderName+"</b></span>";
   stringx +="<input type='hidden' class='proStat' value='"+item.processstatus_ID+"'>";
   stringx +="<input type='hidden' class='eachprocessid' value="+item.process_ID+">";
   stringx +="<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
   stringx +="<div class='m-portlet__head-tools'>";
   stringx +="<ul class='m-portlet__nav'>";
   stringx +="<li class='m-portlet__nav-item'>";
   stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
   stringx +="<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";
   stringx +="<button class='dropdown-item' type='button' data-id='"+item.process_ID+"' onclick='updateProcessFetch(this)' data-toggle='modal' data-target='#updateProcessmodal'><i class='flaticon-file'></i>&nbsp;Edit Process Name</button>";
   stringx +="<button class='dropdown-item' type='button'><i class='flaticon-file'>";
   stringx +="</i>&nbsp;Assign this Process</button>";
   stringx +="</div> ";
   stringx +="</li>";

   stringx +="<li class='m-portlet__nav-item'>";
   stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle coloricons'></i></a> " ;
   stringx +="</li>";

   stringx +="<li class='m-portlet__nav-item'>";
   stringx +="<a href='#' data-portlet-tool='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='fa fa-trash-o coloricons'></i></a> "  ; 
   stringx +="</li> &nbsp;|&nbsp;";

   if(item.processstatus_ID==10){
    stringx += " <li class='m-portlet__nav-item'>";
    stringx +=" <span class='m-badge m-badge--info m-badge--wide' data-toggle='m-tooltip' data-placement='top' title data-original-title='Click process template to add, modify and run a checklist'>Run a Checklist</span>";
    stringx +="</li> ";
  }else if(item.processstatus_ID==2){  
    stringx += "<li class='m-portlet__nav-item'>";
    stringx += " <span class='m-menu__link-badge'>";
    stringx += " <span class='m-badge m-badge--metal' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Checklist'>2</span>";
    stringx +="</span>"  ; 
    stringx +="</li> " ;
    stringx +="<li class='m-portlet__nav-item'>";
    stringx +=" <span class='m-menu__link-badge'>";
    stringx +="<span class='m-badge m-badge--info' data-toggle='m-tooltip' data-placement='top' title data-original-title='Pending Checklist'>2</span>";
    stringx +="</span>"; 
    stringx +="</li>";
    stringx +="<li class='m-portlet__nav-item'>";
    stringx +="<span class='m-menu__link-badge'>";
    stringx +="<span class='m-badge m-badge--danger' data-toggle='m-tooltip' data-placement='top' title data-original-title='Overdue Checklist'>2</span>";
    stringx +="</span>";   
    stringx +="</li>";
    stringx +="<li class='m-portlet__nav-item'>";
    stringx +="<span class='m-menu__link-badge'>";
    stringx +="<span class='m-badge m-badge--success' data-toggle='m-tooltip' data-placement='top' title data-original-title='Completed Checklist'>2</span>";
    stringx +=" </span>" ;  
    stringx +="</li>";
  }
  stringx +="</ul>";
  stringx += "</div>";
  stringx +="</div>";
  stringx +="</div>";
  stringx +="</div>"
  stringx +="</div>";
});
$("#prorow").html(stringx);
}
});
}
});
function resetform(){
  $("#formadd").trigger( "reset" );
  $("#formupdate").trigger( "reset" );
  $("#processadd").trigger( "reset" );
}

$("#selectStatus").on("change",function(){
 var selectedval = $(this).val();
 if(selectedval=="0"){
  displayfolders();
}else if(selectedval=="00"){
  displayfolders();
}
else if(selectedval=="15"){
  displayfolders();
}else{
 $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/get_dropdownfolinfo",
   data: {
    status_ID:selectedval
  },
  cache: false,
  success: function (res) {
    var result = JSON.parse(res.trim());
    var stringx="";
    var substringname="";
    if(res!=0){
      $.each(result, function (key, item) {
        var foldername=item.folderName;
        if(foldername.length > 22){
          substringname = foldername.substring(0,22)+"...";
        }else{
          substringname = foldername.substring(0,22);
        }
        stringx +=  "<div class='col-lg-4 foldercontent' id='"+item.status_ID+"'>"; 
        stringx +=  "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
        stringx +=  "<div class='m-portlet__head border-accent rounded foldercolor'>";

        stringx +="<div class='row' style='margin-top:1em'>";
        stringx +="<div class='m-portlet__head-caption'>";
        stringx += "<div class='m-portlet__head-title'>";
        stringx += "<span class='m-portlet__head-icon'>";
        stringx += "<i class='fa fa-folder' id='"+item.folder_ID+"'></i>";
        stringx +="</span>";
        stringx +="<a data-toggle='tooltip' data-placement='bottom' title='"+item.folderName+"' style='color:#9ce4f5;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/subfolder')?>"+'/'+item.folder_ID+"' class='foldname'>"+substringname+"</a>";
        stringx += "<input type='hidden' value='"+item.folder_ID+"' id='eachfolderid'>";
        stringx += "<input type='hidden' class='folStat' value='"+item.status_ID+"'";
        stringx += "<input type='hidden' class='fetchval' id='fetchval'>" ;
        stringx += "</div>"; 
        stringx +="</div>";
        stringx += "</div>";
        stringx +="&nbsp;&nbsp;&nbsp;&nbsp;<span class='colorwhitesize createdby'>"+item.fname+"&nbsp;"+item.lname+"</span>";

        stringx +="<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
        stringx +="<div class='m-portlet__head-tools'>";
        stringx += "<ul class='m-portlet__nav'>";
        stringx += "<li class='m-portlet__nav-item'>";
        stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
        stringx += "<i class='la la-cog'></i></a>";
        stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
        stringx +="<button class='dropdown-item' type='button' data-id='"+item.folder_ID+"' onclick='updateFetch(this)' data-toggle='modal' data-target='#updatemodal'>";
        stringx +="<i class='flaticon-file'></i>&nbsp;Change folder name</button>";
        stringx +="<button class='dropdown-item' type='button'>";
        stringx +="<i class='la la-archive'></i>";
        stringx +="&nbsp;Archive this folder</button>";
        stringx +="<button class='dropdown-item' type='button' data-id='"+item.folder_ID+"' onclick='deleteFolder(this)'>";
        stringx +="<i class='la la-trash'></i>&nbsp;Delete this folder</button>";
        stringx += "</div>";
        stringx += "</li>";

        stringx +="<li class='m-portlet__nav-item'>";
        stringx +="<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle'></i></a> ";
        stringx +="</li>"
        stringx +="<li class='m-portlet__nav-item'>";
        stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'>";
        stringx += "<i class='la la-users'></i>";
        stringx +="</a>";
        stringx +="</li>";
        stringx +="<li class='m-portlet__nav-item colorwhiteicons'>";
        stringx +="<span>|</span>";
        stringx +="</li>";
        stringx +="<li class='m-portlet__nav-item'>";
        stringx +="<span class='m-menu__link-badge'>";
        stringx +="<span class='m-badge m-badge--metal' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Process Templates'>1</span>";
        stringx +="</span>";  
        stringx +="</li>";
        stringx += " <li class='m-portlet__nav-item'>";
        stringx +="<span class='m-menu__link-badge'>";
        stringx +="<span class='m-badge m-badge--success' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Checklists'>1</span>";
        stringx +="</span> ";
        stringx += "</li>";
        stringx +="<li class='m-portlet__nav-item'>";
        stringx +="<span class='m-menu__link-badge'>";
        stringx += "<span class='m-badge m-badge--danger' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Pending Checklists'>1</span>";
        stringx +="</span> "; 
        stringx += "</li>";

        stringx +="</ul>";
        stringx +="</div>";
        stringx +="</div>";

        stringx += "</div>";
        stringx += "</div>";
        stringx +="</div>";
      });

}else{
  stringx+="<div class='col-lg-12' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records Found!</strong></h5></div>";
}
$("#folrow").html(stringx);  
}
});
}
});



  // var eachfolderid=$("#eachfolderid").val();
  // $('.foldercontent').hide();
  // if(element.value=="100"){
  //   $('.foldercontent').show();
  // }
  // $('.foldercontent .folStat').each(function(){
  //   var folderStatus=$(".folStat").val();
  //   if(element.value=="10"){
  //     $('.fc10').show();
  //   }
  //   else if(element.value=="11"){
  //     $('.fc11').show();
  //   }   
  // });


  $("#searchProcess").on("keyup",function(){
    var str = $(this).val();
    displayprocesstemp(str);
  });
//JQUERY DISPLAY
function displayfolders(word){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/display_folders",
    data: {
      word:word
    },
    cache: false,
    success: function (res) {
     var stringx = "";
     var stringlastid="";
     if(res!=0){
      var result = JSON.parse(res.trim());
      var lastid=0;
      $.each(result, function (key, item) {
       lastid=item.folder_ID;
       var foldername=item.folderName;
       var substringname="";
       if(foldername.length > 22){
        substringname = foldername.substring(0,22)+"...";
      }else{
        substringname = foldername.substring(0,22);
      }
      stringx +=  "<div class='col-lg-4 foldercontent' id='"+item.status_ID+"'>"; 
      stringx +=  "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
      stringx +=  "<div class='m-portlet__head border-accent rounded foldercolor'>";

      stringx +="<div class='row' style='margin-top:1em'>";
      stringx +="<div class='m-portlet__head-caption'>";
      stringx += "<div class='m-portlet__head-title'>";
      stringx += "<span class='m-portlet__head-icon'>";
      stringx += "<i class='fa fa-folder' id='"+item.folder_ID+"'></i>";
      stringx +="</span>";
      stringx +="<a data-toggle='tooltip' data-placement='bottom' title='"+item.folderName+"' style='color:#9ce4f5;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/subfolder')?>"+'/'+item.folder_ID+"' class='foldname'>"+substringname+"</a>";
      stringx += "<input type='hidden' value='"+item.folder_ID+"' id='eachfolderid'>";
      stringx += "<input type='hidden' class='folStat' value='"+item.status_ID+"'";
      stringx += "<input type='hidden' class='fetchval' id='fetchval'>" ;
      stringx += "</div>"; 
      stringx +="</div>";
      stringx += "</div>";
      stringx +="&nbsp;&nbsp;&nbsp;&nbsp;<span class='colorwhitesize createdby'>"+item.fname+"&nbsp;"+item.lname+"</span>";

      stringx +="<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
      stringx +="<div class='m-portlet__head-tools'>";
      stringx += "<ul class='m-portlet__nav'>";
      stringx += "<li class='m-portlet__nav-item'>";
      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
      stringx += "<i class='la la-cog'></i></a>";
      stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
      stringx +="<button class='dropdown-item' type='button' data-id='"+item.folder_ID+"' onclick='updateFetch(this)' data-toggle='modal' data-target='#updatemodal'>";
      stringx +="<i class='flaticon-file'></i>&nbsp;Change folder name</button>";
      stringx +="<button class='dropdown-item' type='button'>";
      stringx +="<i class='la la-archive'></i>";
      stringx +="&nbsp;Archive this folder</button>";
      stringx +="<button class='dropdown-item' type='button' data-id='"+item.folder_ID+"' onclick='deleteFolder(this)'>";
      stringx +="<i class='la la-trash'></i>&nbsp;Delete this folder</button>";
      stringx += "</div>";
      stringx += "</li>";

      stringx +="<li class='m-portlet__nav-item'>";
      stringx +="<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle'></i></a> ";
      stringx +="</li>"
      stringx +="<li class='m-portlet__nav-item'>";
      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'>";
      stringx += "<i class='la la-users'></i>";
      stringx +="</a>";
      stringx +="</li>";
      stringx +="<li class='m-portlet__nav-item colorwhiteicons'>";
      stringx +="<span>|</span>";
      stringx +="</li>";
      stringx +="<li class='m-portlet__nav-item'>";
      stringx +="<span class='m-menu__link-badge'>";
      stringx +="<span class='m-badge m-badge--metal' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Process Templates'>1</span>";
      stringx +="</span>";  
      stringx +="</li>";
      stringx += " <li class='m-portlet__nav-item'>";
      stringx +="<span class='m-menu__link-badge'>";
      stringx +="<span class='m-badge m-badge--success' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Checklists'>1</span>";
      stringx +="</span> ";
      stringx += "</li>";
      stringx +="<li class='m-portlet__nav-item'>";
      stringx +="<span class='m-menu__link-badge'>";
      stringx += "<span class='m-badge m-badge--danger' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Pending Checklists'>1</span>";
      stringx +="</span> "; 
      stringx += "</li>";

      stringx +="</ul>";
      stringx +="</div>";
      stringx +="</div>";

      stringx += "</div>";
      stringx += "</div>";
      stringx +="</div>";
    });
}else{
 stringx+="<div class='col-lg-12' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records Found!</strong></h5></div>";
}
$("#folrow").html(stringx);
stringlastid+= "<div class='load-more col-lg-12' lastID='"+lastid+"' style='display: none;'>";
stringlastid+= "<h5 style='text-align: center;'>";
stringlastid+= "<div class='m-loader m-loader--info' style='width: 30px; display: inline-block;'>";
stringlastid+= "</div> Loading more folder...</h5></div>";
$("#folrow").append(stringlastid);
}  
});
} 
function displayprocesstemp(word){
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/display_processtemplates",
  data: {
    word:word
  },
  cache: false,
  success: function (res) {
    var stringx = "";
    var stringlastid="";
    var lastid=0;
    if(res!=0){
      var result = JSON.parse(res.trim());
      $.each(result, function (key, item) {
        lastid=item.process_ID;
        var processname=item.processTitle;
        var substringprocess="";
        if(processname.length > 22){
         substringprocess = processname.substring(0,22)+"...";
       }else{
        substringprocess = processname.substring(0,22);
      }
      stringx +="<div class='col-lg-4 processcontent pr+processtatusid'>";
      stringx +="<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>"
      stringx +="<div class='m-portlet__head border-accent rounded colorlightblue'>";
      stringx +="<div class='row' style='margin-top:1em'>";
      stringx +="<div class='m-portlet__head-caption'>";
      stringx +="<div class='m-portlet__head-title'>";
      stringx +="<span class='m-portlet__head-icon'>";
      stringx +="<i class='fa fa-file-text-o colorblue'></i>";
      stringx +="</span>" ;

      if(item.processstatus_ID==10){
        stringx +="<a data-toggle='tooltip' data-placement='bottom' title='"+item.processTitle+"' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist')?>"+'/'+item.process_ID+"' class='m-portlet__head-text protitle'>"+substringprocess+"</a>";
      }else if(item.processstatus_ID==2){
       stringx +="<a data-toggle='tooltip' data-placement='bottom' title='"+item.processTitle+"' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist_pending')?>"+'/'+item.process_ID+"' class='m-portlet__head-text protitle'>"+substringprocess+"</a>";
     }   
     stringx +="</div>";
     stringx +="</div>";
     stringx +="</div>";
     stringx +="<span style='font-size: 0.9em;margin-left:1em'>";
     stringx +="<span>"+item.dateTimeCreated+"</span><br>";
     stringx +="</span>";
     stringx += "<span class='folname' style='color:#088594;font-size: 0.9em;margin-left:1em'><b>"+item.folderName+"</b></span>";
     stringx +="<input type='hidden' class='proStat' value='"+item.processstatus_ID+"'>";
     stringx +="<input type='hidden' class='eachprocessid' value="+item.process_ID+">";
     stringx +="<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
     stringx +="<div class='m-portlet__head-tools'>";
     stringx +="<ul class='m-portlet__nav'>";
     stringx +="<li class='m-portlet__nav-item'>";
     stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
     stringx +="<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";
     stringx +="<button class='dropdown-item' type='button' data-id='"+item.process_ID+"' onclick='updateProcessFetch(this)' data-toggle='modal' data-target='#updateProcessmodal'><i class='flaticon-file'></i>&nbsp;Edit Process Name</button>";
     stringx +="<button class='dropdown-item' type='button'><i class='flaticon-file'>";
     stringx +="</i>&nbsp;Assign this Process</button>";
     stringx +="</div> ";
     stringx +="</li>";

     stringx +="<li class='m-portlet__nav-item'>";
     stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle coloricons'></i></a> " ;
     stringx +="</li>";

     stringx +="<li class='m-portlet__nav-item'>";
     stringx +="<a href='#' data-portlet-tool='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='fa fa-trash-o coloricons'></i></a> "  ; 
     stringx +="</li> &nbsp;|&nbsp;";

     if(item.processstatus_ID==10){
      stringx += " <li class='m-portlet__nav-item'>";
      stringx +=" <span class='m-badge m-badge--info m-badge--wide' data-toggle='m-tooltip' data-placement='top' title data-original-title='Click process template to add, modify and run a checklist'>Run a Checklist</span>";
      stringx +="</li> ";
    }else if(item.processstatus_ID==2){
      stringx += "<li class='m-portlet__nav-item'>";
      stringx += " <span class='m-menu__link-badge'>";
      stringx += " <span class='m-badge m-badge--metal' data-toggle='m-tooltip' data-placement='top' title data-original-title='All Checklist'>2</span>";
      stringx +="</span>"  ; 
      stringx +="</li> " ;
      stringx +="<li class='m-portlet__nav-item'>";
      stringx +=" <span class='m-menu__link-badge'>";
      stringx +="<span class='m-badge m-badge--info' data-toggle='m-tooltip' data-placement='top' title data-original-title='Pending Checklist'>2</span>";
      stringx +="</span>"; 
      stringx +="</li>";
      stringx +="<li class='m-portlet__nav-item'>";
      stringx +="<span class='m-menu__link-badge'>";
      stringx +="<span class='m-badge m-badge--danger' data-toggle='m-tooltip' data-placement='top' title data-original-title='Overdue Checklist'>2</span>";
      stringx +="</span>";   
      stringx +="</li>";
      stringx +="<li class='m-portlet__nav-item'>";
      stringx +="<span class='m-menu__link-badge'>";
      stringx +="<span class='m-badge m-badge--success' data-toggle='m-tooltip' data-placement='top' title data-original-title='Completed Checklist'>2</span>";
      stringx +=" </span>" ;  
      stringx +="</li>";
    }
    stringx +="</ul>";
    stringx += "</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>"
    stringx +="</div>";
  });
}else{
 stringx+="<div class='col-lg-12' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records Found!</strong></h5></div>";
}
$("#prorow").html(stringx);
console.log(lastid);
stringlastid+= "<div class='processload-more col-lg-12' processlastID='"+lastid+"' style='display: none;'>";
stringlastid+= "<h5 style='text-align: center;'>";
stringlastid+= "<div class='m-loader m-loader--info' style='width: 30px; display: inline-block;'>";
stringlastid+= "</div> Loading more process template...</h5></div>";
$("#prorow").append(stringlastid);
}
});
}
//END OF JQUERY DISPLAY
$('#processTitle').keyup(function () {
  var max = 100;
  var len = $(this).val().length;
  if (len >= max) {
    $('#charNum').text('You have reached the limit');
  } else {
    var char = max - len;
    $('#charNum').text(char+'/100 characters remaining');
  }
});
function getprocessval(element)
{
  var eachprocessid=$("#eachprocessid").val();
  $('.processcontent').hide();
  if(element.value=="100"){
    $('.processcontent').show();
  }
  $('.processcontent .proStat').each(function(){
    var folderStatus=$(".proStat").val();
    if(element.value=="10"){
      $('.pr10').show();
    }
    else if(element.value=="2"){
      $('.pr2').show();
    } 
    else if(element.value=="11"){
      $('.pr11').show();
    }   
  });
}
$(document).ready(function () {
  $('#formadd').formValidation({
    message: 'This value is not valid',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      folderName: {
        message: 'Your folder name is not valid',
        validators: {
          notEmpty: {
            message: 'Your folder name is required and can\'t be empty'
          },
          regexp: {
            regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
            message: 'Folder name can only consist of letters and spaces'
          }
        }
      }
    }
  }).on('success.form.bv', function(e) {
    e.preventDefault();
  });
  $('#subformadd').formValidation({
    message: 'This value is not valid',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      subfolderName: {
        message: 'Your sub-folder name is not valid',
        validators: {
          notEmpty: {
            message: 'Your sub-folder name is required and can\'t be empty'
          },
          regexp: {
            regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
            message: 'Sub-folder name can only consist of letters and spaces'
          }
        }
      }
    }
  }).on('success.form.bv', function(e) {
    e.preventDefault();
  });
  $('#processadd').formValidation({
    message: 'This value is not valid',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      processTitle: {
        message: 'Your template/process name is not valid',
        validators: {
          notEmpty: {
            message: 'Your template/process name is required and can\'t be empty'
          },
          regexp: {
            regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
            message: 'template/process name can only consist of letters and spaces'
          }
        }
      }
    }
  }).on('success.form.bv', function(e) {
    e.preventDefault();
  });
});
function emptyField(){
  $(".errormessage").remove();  
  $("input.errorprompt").after("<span class='errormessage' style='color:red;font-size:0.8em'>*This field can't be empty</span>");
}
function PemptyField(){
  $(".pErrormessage").remove();  
  $("input.pErrorprompt").after("<span class='pErrormessage' style='color:red;font-size:0.8em'>*This field can't be empty</span>");
}
$("#btn-foldersave").click(function(e){
  if ($.trim($("#folderName").val()) === "") {
    emptyField();
    e.preventDefault();
  }else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_folder",
      data: {
        folderName: $("#folderName").val(),
      },
      cache: false,
      success: function (res) {
        window.location.href = '<?php echo base_url('process')?>';
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added Folder',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function (res){
        console.log(res);
      }
    });
  }
});
$("#btn-processsave").click(function(e){
  if ($.trim($("#processTitle,#folder_ID").val()) === "") {
    PemptyField();
    e.preventDefault();
  }else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_processfolder",
      data: {
        processTitle: $("#processTitle").val(),
        folder_ID: $("#folder_ID").val()
      },
      cache: false,
      success: function (res) {
        res = JSON.parse(res.trim());
        console.log(res);
        window.location.href = "<?php echo base_url('process/checklist')?>"+"/"+res;
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added Process',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function (res){
        console.log(res);
      }
    });
  }
});
$("#btn-subfoldersave").click(function(){
  if ($.trim($("#subfolderName,#rootfolder_ID").val()) === "") {
    e.preventDefault();
  }else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_subfolder",
      data: {
        folderName: $("#subfolderName").val(),
        folder_ID: $("#rootfolder_ID").val()
      },
      cache: false,
      success: function (res) {
        window.location.href = '<?php echo base_url('process')?>';
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added Sub folder',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function (res){
        console.log(res);
      }
    });
  }
});
function foldericon(element){
  var iconid=$(element).data('id');
  $("#foldericon"+iconid).toggleClass("fa fa-folder-open").toggleClass("fa fa-folder");
}
function deleteFolder(element){
  swal({
    title: 'Are you sure?',
    text: "All data under this folder will be deleted, too. Except for your subfolders with process template",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_folder",
        data: {
          folder_ID: $(element).data('id')
        },
        cache: false,
        success: function () {
          displayfolders();
          swal(
            'Deleted!',
            'Folder successfully deleted.',
            'success'
            )
        },
        error: function (res){
          swal(
            'Oops!',
            'You have subfolders with process template/s to be deleted first! Please Check!',
            'error'
            )
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deleting a folder has been cancelled',
        'error'
        )
    }
  });
  console.log($(element).data('id'));
}
function deleteProcess(element){
  swal({
    title: 'Are you sure?',
    text: "All data under this process will be deleted, too.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_process",
        data: {
          folder_ID: $(element).data('id')
        },
        cache: false,
        success: function () {
          window.location.href = '<?php echo base_url('process')?>';
          swal(
            'Deleted!',
            'Folder successfully deleted.',
            'success'
            )
        },
        error: function (res){
          swal(
            'Oops!',
            'You cannot delete this process ',
            'error'
            )
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deleting a process has been cancelled',
        'error'
        )
    }
  });
  console.log($(element).data('id'));
}
function updateFetch(element){
  console.log(element);
  console.log($(element).data('id'));
  var folderName = $("#updatefolderName").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_update",
    data: {
      folder_ID: $(element).data('id'),
      folderName:folderName
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      $("#updatefolderName").val(data.folderName);
      $("#btn-foldersavechanges").data('id',data.folder_ID);
    }
  });
}
function updateProcessFetch(element){
  console.log(element);
  console.log($(element).data('id'));
  var processTitle = $("#updateprocessName").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetchprocess_update",
    data: {
      process_ID: $(element).data('id'),
      processTitle:processTitle
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      $("#updateprocessName").val(data.processTitle);
      $("#btn-processsavechanges").data('id',data.process_ID);
    }
  });
}
function saveChangesFolder(element){
  console.log(element);
  console.log($(element).data('id'));
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_folder",
    data: {
      folder_ID: $(element).data('id'),
      folderName: $("#updatefolderName").val()
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'folder successfully updated',
        showConfirmButton: false,
        timer: 1500
      });
      $("#updatemodal").modal('hide');
      window.location.href = '<?php echo base_url('process')?>';
    }
  });
}
function saveChangesProcess(element){
  console.log(element);
  console.log($(element).data('id'));
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_process",
    data: {
      process_ID: $(element).data('id'),
      processTitle: $("#updateprocessName").val()
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'Process successfully updated',
        showConfirmButton: false,
        timer: 1500
      });
      $("#updateProcessmodal").modal('hide');
      window.location.href = '<?php echo base_url('process')?>';
    }
  });
}
</script>