<style type="text/css">
.colorgray{
    color:#797c88 !important;
}
.colorblue{
  color:#3297e4 !important;
}
.coloricons{
  color:#4371a2 !important;
}
</style> 
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto"> 
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>         
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                   <li class="m-nav__item m-nav__item--home">
                    <a href="<?php echo site_url('process');?>" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"><</li>
                <li class="m-nav__item">
                    <a href="http://localhost/su/process" class="m-nav__link">
                        <span class="m-nav__link-text">Folder</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
       <div class="col-lg-12">  
        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head portlet-header-report" style="background-color:#2c2e3eeb">
                <div class="m-portlet__head-caption">   
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon font-white">
                            <i class="flaticon-folder-2"></i>
                        </span>
                        <a href="<?php echo site_url('process');?>" class="m-portlet__head-text font-poppins"  style="color:white !important;font-size:16px">
                          <?php echo $folderinfo->folderName;?>  
                      </a>
                  </div>
              </div>
          </div>
          <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-12">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">

                        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i>&nbsp;
                            New 
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blankroot-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template</button>
                            <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template in subfolder</button>
                            <button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button>
                            <hr>
                            <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Sub folder</button>
                        </div>                 
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <div class="row">
                &nbsp;&nbsp;&nbsp;<h6 class="colorgray">Sub folders</h6><br><br>
                <?php if (!empty($subfolders)){ ?>
                    <?php foreach($subfolders as $subfolder){?>
                        <div class="col-lg-12">
                            <div class="m-portlet m-portlet--collapsed m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_7">
                               <div class="m-portlet__head border border-accent rounded" style="background-color:#69747d;border-width: 15px" >
                                   <div class="row" style="margin-top:1em">
                                    <!-- FIRST COl -->
                                    <div class="col-lg-6">
                                       <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <span class="m-portlet__head-icon">
                                                <i class="fa fa-folder iconchange"></i>
                                            </span>
                                            <div class="m-widget2__desc">
                                             <a href="<?php echo site_url('process/subfolder').'/'.$subfolder->folder_ID;?>" class="" style="color:#9ce4f5;display: table-cell;vertical-align: middle;font-size: 1.3rem;font-weight: 500;">
                                                <?php echo $subfolder->folderName?>
                                            </a><span style="color: #92e4ce">sub folder</span>
                                            <span style="font-size:0.9em;color:white" >|
                                              <?php 
                                              $datetime=$subfolder->dateTimeCreated;
                                              $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
                                              echo sprintf($date->format("M j, Y"))."&nbsp; at &nbsp;";
                                              echo sprintf($date->format("g:ia"));
                                              ?>
                                          </span><br>
                                          <span style="font-size:0.9em;color:white" ><?php echo $subfolder->fname;?>&nbsp;  <?php echo $subfolder->lname ?></span>
                                      </div>
                                  </div> 
                              </div>
                          </div>
                          <!-- SECOND COL -->
                          <div class="col-lg-6">
                            <div class="m-portlet__head-tools" style="float: right">
                             <ul class="m-portlet__nav" >
                                <li class="m-portlet__nav-item">
                                    <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon" id="dropdownOption" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-cog "></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownOption" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <button class="dropdown-item" type="button" data-id="<?php echo $subfolder->folder_ID; ?>" onclick="updateFetch(this)" data-toggle="modal" data-target="#updatemodal"><i class="flaticon-file"></i>&nbsp;Change folder name</button>
                                        <button class="dropdown-item" type="button"><i class="flaticon-file">
                                        </i>&nbsp;Assign this folder</button>
                                    </div> 
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a href="#" id="folderopen_close" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>  
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-id="<?php echo $subfolder->folder_ID; ?>" onclick="deleteFolder(this)" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-trash"></i></a> 
                                </li>
                            </ul>
                        </div><br><br>
                        <span class="m-widget2__user-name" style="float: right">
                            <span class="m-menu__link-badge">
                                <span class="m-badge m-badge--metal" data-toggle="m-tooltip" data-placement="top" title data-original-title="Number of Checklist">12</span>
                            </span>&nbsp;&nbsp;&nbsp;
                            <span class="m-menu__link-badge">
                                <span class="m-badge m-badge--success" data-toggle="m-tooltip" data-placement="top" title data-original-title="Pending Checklist">2</span>
                            </span>&nbsp;&nbsp;&nbsp;
                            <span class="m-menu__link-badge">
                                <span class="m-badge m-badge--danger" data-toggle="m-tooltip" data-placement="top" title data-original-title="Overdue Checklist">0</span>
                            </span>
                        </span><br><br>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body" style="background:#e3eff1;margin-top: 3em">
               <div class="row">
                  <div class="col-lg-4">
                    <div class="tab-pane active rounded" id="m_widget2_tab1_content" style="background-color:#d6d4ec">
                        <div class="m-widget2">
                            <div class="m-widget2__item">
                                <div class="m-widget2__checkbox">
                                </div>
                                <div class="m-widget2__desc">
                                    <span class="m-widget2__text">
                                     THIS PROCESS
                                 </span><br>
                                 <span class="m-widget2__user-name">
                                     <span class="m-menu__link-badge">
                                        <span class="m-badge m-badge--metal" data-toggle="m-tooltip" data-placement="top" title data-original-title="Number of Checklist">12</span>
                                    </span>
                                    <span class="m-menu__link-badge">
                                        <span class="m-badge m-badge--success" data-toggle="m-tooltip" data-placement="top" title data-original-title="Pending Checklist">2</span>
                                    </span>
                                    <span class="m-menu__link-badge">
                                        <span class="m-badge m-badge--danger" data-toggle="m-tooltip" data-placement="top" title data-original-title="Overdue Checklist">0</span>
                                    </span>     
                                </span><br>
                                <span class="m-widget2__user-name">
                                    <a href="#" class="m-widget2__link">
                                        Created by: John Doe
                                    </a>
                                </span><br>
                                <span class="m-widget2__user-name">
                                    <a href="#" class="m-widget2__link">
                                       December 5, 2018 13:10:11
                                   </a>
                               </span><br>

                           </div>
                           <div class="m-widget2__actions">
                            <div class="m-widget2__actions-nav">
                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover">
                                    <a href="#" class="m-dropdown__toggle">
                                        <i class="la la-ellipsis-h"></i>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                                <span class="m-nav__link-text">Activity</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                                <span class="m-nav__link-text">Messages</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                                <span class="m-nav__link-text">FAQ</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                <span class="m-nav__link-text">Support</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<?php }?>
<?php }else{?>
    <div class="col-lg-12" style="color:#7d7d7d"><h5 style="text-align: center;"><i style="color:#e4ac05"class="fa fa-exclamation-circle"></i><strong> No Record!</strong> You do not have subfolders yet.<br> Hit the button <i>+ New</i> and create one.</h5></div>
<?php }?>
</div>
 &nbsp;&nbsp;&nbsp;<h6 class="colorgray">Process Template</h6><br>
<div class="row">
   <?php if (!empty($processes)){ ?>
    <?php foreach ($processes as $process) { ?>
      <!--begin::Portlet-->
      <div class="col-lg-4">
          <?php 
          $countall=0;
          $countpen=0;
          $countoverdue=0;
          $countcomp=0;
          foreach ($checklists as $chs) {
            if($process->process_ID==$chs->process_ID){
                $countall++;
                if($chs->status_ID=="2"){
                    $countpen++;
                }
                if($chs->status_ID=="15"){
                    $countoverdue++;
                }
                if($chs->status_ID=="3"){
                   $countcomp++;
               }
           }
       }
       ?>  
       <div class="m-portlet m-portlet--collapsed m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_7">
        <div class="m-portlet__head border-accent rounded" style="background-color:#9cdfda">
           <!--  <div class="row" style="background-color: #86b4d8">hi</div> -->
           <div class="row" style="margin-top:1em">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="fa fa-file-text-o colorblue"></i>
                    </span>
                    <?php if($process->processstatus_ID=="10"){?>
                        <a href="<?php echo base_url('process/checklist').'/'.$process->process_ID;?>" class="m-portlet__head-text">
                          <?php echo $process->processTitle?>
                      </a>
                  <?php } else if($process->processstatus_ID=="2"){?>
                   <a href="<?php echo base_url('process/checklist_pending').'/'.$process->process_ID?>" class="m-portlet__head-text">
                    <?php echo $process->processTitle?>
                </a>
            <?php }?>
        </div>          
    </div>
</div>
<span style="font-size: 0.9em;margin-left:1em">
 <?php 
 $datetime=$process->dateTimeCreated;
 $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
 echo sprintf($date->format("M j, Y"))."&nbsp; at &nbsp;";
 echo sprintf($date->format("g:ia"));
 ?>
</span><br>
<span style="color:#088594;font-size: 0.9em;margin-left:1em"><b><?php echo $process->folderName?></b></span>
<div class="row" style="margin-top:1em;margin-bottom: 1em;">
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav" >
            <li class="m-portlet__nav-item">
              <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon" id="dropdownOption" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-cog coloricons"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownOption" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                <button class="dropdown-item" type="button" data-id="<?php echo $process->process_ID; ?>" onclick="updateProcessFetch(this)" data-toggle="modal" data-target="#updateProcessmodal"><i class="flaticon-file"></i>&nbsp;Edit process name</button>
                <button class="dropdown-item" type="button"><i class="flaticon-file">
                </i>&nbsp;Assign this Process</button>
            </div> 
        </li>
        <li class="m-portlet__nav-item">
            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-info-circle coloricons"></i></a>  
        </li>
        <li class="m-portlet__nav-item">
            <a href="#" data-portlet-tool="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="fa fa-trash-o coloricons"></i></a>    
        </li> &nbsp;|&nbsp;
        <?php if($process->processstatus_ID=="10"){?>
           <li class="m-portlet__nav-item">
            <span class="m-badge m-badge--info m-badge--wide" data-toggle="m-tooltip" data-placement="top" title data-original-title="Click the process name to add, modify and run a checklist">Run a Checklist</span>
        </li> 
    <?php }else if($process->processstatus_ID=="2"){?>         
        <li class="m-portlet__nav-item">
            <span class="m-menu__link-badge">
              <span class="m-badge m-badge--metal" data-toggle="m-tooltip" data-placement="top" title data-original-title="All Checklists"><?php echo $countall;?></span>
          </span>   
      </li>  
      <li class="m-portlet__nav-item">
        <span class="m-menu__link-badge">
          <span class="m-badge m-badge--info" data-toggle="m-tooltip" data-placement="top" title data-original-title="Pending Checklist"><?php echo $countpen;?></span>
      </span>   
  </li>
  <li class="m-portlet__nav-item">
    <span class="m-menu__link-badge">
      <span class="m-badge m-badge--danger" data-toggle="m-tooltip" data-placement="top" title data-original-title="Overdue Checklist"><?php echo $countoverdue;?></span>
  </span>   
</li>
<li class="m-portlet__nav-item">
    <span class="m-menu__link-badge">
      <span class="m-badge m-badge--success" data-toggle="m-tooltip" data-placement="top" title data-original-title="Completed Checklist"><?php echo $countcomp;?></span>
  </span>   
</li>
<?php }?>
</ul>
</div>
</div>
</div>
<div class="m-portlet__body" style="margin-top: 3em">
  <div class="col-md-12">
    <div class="m-input-icon m-input-icon--left">
        <input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
        <span class="m-input-icon__icon m-input-icon__icon--left">
            <span><i class="la la-search"></i></span>
        </span>
    </div>
</div>
<br> 
</div>
</div>  
</div>
<!--end::Portlet-->
<?php }?>
<?php }else{?>
   <div class="col-lg-12" style="color:#7d7d7d"><h5 style="text-align: center;"><i style="color:#e4ac05"class="fa fa-exclamation-circle"></i><strong> No Record!</strong> You do not have process template in this folder yet.<br> Hit the button <i>+ New</i> and create one.</h5></div>
<?php }?>
</div>
</div>
</div>  
<!--end::Portlet-->
</div>

<!--begin::Modal-->
<div class="modal fade" id="folder-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Sub folder</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form id="formadd" name="formadd" method="post">
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Sub Folder Name:</label>
            <input type="text" class="form-control" name="subfolderName" id="subfolderName">
            <input type="hidden" id="rootfolderID" name="rootfolderID" value="<?php echo $folderID ?>">
        </div> 
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" name="submit" id="btn-subfoldersave" class="btn btn-accent">Save</button>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Folder Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form id="formupdate" name="formupdate" method="post">
      <div class="form-group">
        <label for="recipient-name" class="form-control-label">Folder Name:</label>
        <input type="text" class="form-control" id="updatefolderName" name="updatefolderName" placeholder="Enter Folder Name">
    </div>
</form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" name="submit" id="btn-foldersavechanges" onclick="saveChangesFolder(this)" class="btn btn-accent">Save Changes</button>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="updateProcessmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Process Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form id="formupdate" name="formupdate" method="post">
      <div class="form-group">
        <label for="recipient-name" class="form-control-label">Process Name:</label>
        <input type="text" class="form-control" id="updateprocessName" name="updateprocessName" placeholder="Enter Process Name">
    </div>
</form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" name="submit" id="btn-processsavechanges" onclick="saveChangesProcess(this)" class="btn btn-accent">Save Changes</button>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="blank-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Blank Template</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form id="subformadd" name="subformadd" method="post">
      <div class="form-group">
         <label for="recipient-name" class="form-control-label">Template Name:</label>
         <input type="text" class="form-control" id="subprocessTitle" name="subprocessTitle">
     </div>
     <div class="form-group">
        <label>Choose folder:</label>
        <select class="form-control m-input" id="subprocessfolder" name="subprocessfolder">
            <?php foreach($subfolders as $subfolder){?>
                <option value='<?php echo $subfolder->folder_ID ?>'><?php echo $subfolder->folderName ?></option>
            <?php }?>
        </select>
        <p class="m-form__help text-center" id="leave_description" style="text-transform: capitalize;"></p>
    </div>
</form>
</div>
<div class="modal-footer">
 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
 <button type="submit" name="submit" id="btn-subprocesssave" class="btn btn-accent">Save</button>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="blankroot-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Blank Template</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form id="subformadd2" name="subformadd2" method="post">
      <div class="form-group">
        <label for="recipient-name" class="form-control-label">Template Name:</label>
        <input type="text" class="form-control" id="processTitle" name="processTitle">
        <input type="hidden" id="rootprocessID" name="rootprocessID" value="<?php echo $folderID ?>">
    </div>
</form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" name="submit" id="btn-processsave" class="btn btn-accent">Save</button>
</div>
</div>
</div>
</div>
<!--end::Modal-->
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
      $('#formadd').formValidation({
          message: 'This value is not valid',
          icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            subfolderName: {
              message: 'Your folder name is not valid',
              validators: {
                notEmpty: {
                  message: 'Your folder name is required and can\'t be empty'
              },
              regexp: {
                  regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
                  message: 'Folder name can only consist of letters and spaces'
              }
          }
      }
  }
}).on('success.form.bv', function(e) {
  e.preventDefault();
});
$('#subformadd').formValidation({
  message: 'This value is not valid',
  icon: {
    valid: 'glyphicon glyphicon-ok',
    invalid: 'glyphicon glyphicon-remove',
    validating: 'glyphicon glyphicon-refresh'
},
fields: {
    subprocessTitle: {
      message: 'Your template/process name is not valid',
      validators: {
        notEmpty: {
          message: 'Your template/process name is required and can\'t be empty'
      },
      regexp: {
          regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
          message: 'template/process name can only consist of letters and spaces'
      }
  }
}
}
}).on('success.form.bv', function(e) {
  e.preventDefault();
});
$('#subformadd2').formValidation({
  message: 'This value is not valid',
  icon: {
    valid: 'glyphicon glyphicon-ok',
    invalid: 'glyphicon glyphicon-remove',
    validating: 'glyphicon glyphicon-refresh'
},
fields: {
    processTitle: {
      message: 'Your template/process name is not valid',
      validators: {
        notEmpty: {
          message: 'Your template/process name is required and can\'t be empty'
      },
      regexp: {
          regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
          message: 'template/process name can only consist of letters and spaces'
      }
  }
}
}
}).on('success.form.bv', function(e) {
  e.preventDefault();
});
});
    $("#btn-subfoldersave").click(function(){
        if ($.trim($("#subfolderName,#rootfolderID").val()) === "") {
          e.preventDefault();
      }else{
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>process/add_subfolder",
            data: {
              folderName: $("#subfolderName").val(),
              folder_ID: $("#rootfolderID").val()
          },
          cache: false,
          success: function (res) {
             window.location.href = '<?php echo site_url('process/subfolder').'/'.$folderID; ?>';
             swal({
                position: 'center',
                type: 'success',
                title: 'Successfully Added Sub folder',
                showConfirmButton: false,
                timer: 1500
            });
             var result = JSON.parse(res.trim());
         },
         error: function (res){
          console.log(res);
      }
  });
      }
  });
    $("#btn-processsave").click(function(){
        if ($.trim($("#processTitle,#rootprocessID").val()) === "") {
          e.preventDefault();
      }else{
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>process/add_processfolder",
            data: {
              processTitle: $("#processTitle").val(),
              folder_ID: $("#rootprocessID").val()
          },
          cache: false,
          success: function (res) {
              res = JSON.parse(res.trim());
              console.log(res);
              window.location.href = "<?php echo base_url('process/checklist')?>"+"/"+res;
              swal({
                position: 'center',
                type: 'success',
                title: 'Successfully Added Process',
                showConfirmButton: false,
                timer: 1500
            });
              var result = JSON.parse(res.trim());
          },
          error: function (res){
              console.log(res);
          }
      });
      }
  });
    $("#btn-subprocesssave").click(function(){
        if ($.trim($("#subprocessTitle,#subprocessfolder").val()) === "") {
          e.preventDefault();
      }else{
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>process/add_processfolder",
            data: {
              processTitle: $("#subprocessTitle").val(),
              folder_ID: $("#subprocessfolder").val()
          },
          cache: false,
          success: function (res) {
              window.location.href = '<?php echo site_url('process/subfolder').'/'.$folderID; ?>';
              swal({
                position: 'center',
                type: 'success',
                title: 'Successfully Added Process',
                showConfirmButton: false,
                timer: 1500
            });
              var result = JSON.parse(res.trim());
          },
          error: function (res){
              console.log(res);
          }
      });
      }
  });
    function updateFetch(element){
      console.log(element);
      console.log($(element).data('id'));
      var folderName = $("#updatefolderName").val();

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/fetch_update",
        data: {
          folder_ID: $(element).data('id'),
          folderName:folderName
      },
      cache: false,
      success: function (res) {
          res = JSON.parse(res.trim());
          console.log(res);
          var data = res;
          $("#updatefolderName").val(data.folderName);
          $("#btn-foldersavechanges").data('id',data.folder_ID);
      }
  });
  }
  function updateProcessFetch(element){
      console.log(element);
      console.log($(element).data('id'));
      var processTitle = $("#updateprocessName").val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/fetchprocess_update",
        data: {
          process_ID: $(element).data('id'),
          processTitle:processTitle
      },
      cache: false,
      success: function (res) {
          res = JSON.parse(res.trim());
          console.log(res);
          var data = res;
          $("#updateprocessName").val(data.processTitle);
          $("#btn-processsavechanges").data('id',data.process_ID);
      }
  });
  }
  function saveChangesFolder(element){
      console.log(element);
      console.log($(element).data('id'));
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/update_folder",
        data: {
          folder_ID: $(element).data('id'),
          folderName: $("#updatefolderName").val()
      },
      cache: false,
      success: function (res) {
          swal({
            position: 'center',
            type: 'success',
            title: 'folder successfully updated',
            showConfirmButton: false,
            timer: 1500
        });
          $("#updatemodal").modal('hide');
          window.location.href = '<?php echo base_url('process/subfolder').'/'.$folderID?>';
      }
  });
  }
  function saveChangesProcess(element){
      console.log(element);
      console.log($(element).data('id'));
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/update_process",
        data: {
          process_ID: $(element).data('id'),
          processTitle: $("#updateprocessName").val()
      },
      cache: false,
      success: function (res) {
          swal({
            position: 'center',
            type: 'success',
            title: 'Process successfully updated',
            showConfirmButton: false,
            timer: 1500
        });
          $("#updateProcessmodal").modal('hide');
          window.location.href = '<?php echo base_url('process/subfolder').'/'.$folderID?>';
      }
  });
  }
  function deleteFolder(element){
      swal({
        title: 'Are you sure?',
        text: "All data under this folder will be deleted, too. Except for your subfolders with process template",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!'
    }).then(function(result) {
        if (result.value) {

          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>process/delete_folder",
            data: {
              folder_ID: $(element).data('id')
          },
          cache: false,
          success: function () {
            window.location.href = '<?php echo base_url('process/subfolder').'/'.$folderID?>';
            swal(
                'Deleted!',
                'Folder successfully deleted.',
                'success'
                )
        },
        error: function (res){
          swal(
            'Oops!',
            'You have subfolders with process template/s to be deleted first! Please Check!',
            'error'
            )
      }
  });
      }else if (result.dismiss === 'cancel') {
          swal(
            'Cancelled',
            'Deleting a folder has been cancelled',
            'error'
            )
      }
  });
    console.log($(element).data('id'));
}

</script>