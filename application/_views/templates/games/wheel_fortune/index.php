<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="utf-8" />
    <title>Wheel Of Fortune | SupportZebra</title>
    <meta name="description" content="Initialized via remote ajax json data">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src='<?php echo base_url();?>assets/src/custom/plugins/js_wheel_fortune/jquery/dist/jquery.min.js'></script>

    <script src='<?php echo base_url();?>assets/src/custom/plugins/js_wheel_fortune/TweenMax.min.js'></script>
    <script src='<?php echo base_url();?>assets/src/custom/plugins/js_wheel_fortune/underscore.js'></script>
    <script src='<?php echo base_url();?>assets/src/custom/plugins/javascript-winwheel-2.7.0/Winwheel.js'></script>
    <script src='<?php echo base_url();?>assets/src/custom/js/general.js'></script>
    <script src="<?php echo base_url(); ?>assets/js/app.js" type="text/javascript"></script>
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/src/custom/css/fonts/poppins/poppins.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/src/custom/css/fonts/roboto/roboto.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/src/custom/css/fonts/pacifico/pacifico.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-particles-burst-master/dist/jquery-particles-burst.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/img/favicon.ico" />
    <script src='<?php echo base_url();?>assets/src/custom/plugins/js_wheel_fortune/popin.js'></script>
    <script src='<?php echo base_url();?>assets/src/custom/plugins/jquery-particles-burst-master/dist/jquery-particles-burst.min.js'></script>

    <style>
    /* @import url(https: //fonts.googleapis.com/css?family=Roboto); */
        #canvasContainer {
            background-image: url(../assets/src/custom/plugins/javascript-winwheel-2.7.0/wheel_back.png);
            background-repeat: no-repeat;
            background-position: center;
            background-size: 505px 668px;
        }

.congrats {
	position: absolute;
    top: 138px;
	/* width: 550px;
	height: 100px; */
	padding: 20px 10px;
	text-align: center;
	margin: 0 auto;
	left: 0;
	right: 0;
}

#greetings, #prize{
	transform-origin: 50% 50%;
	font-size: 50px;
    /* font-family: 'Pacifico', cursive; */
	cursor: pointer;
	z-index: 10000;
	text-shadow: 2px 2px #000000;
	text-align: center;
	width: 100%;
    color: yellow;
   
}

.blob {
	height: 50px;
	width: 50px;
	color: #ffcc00;
	position: absolute;
	top: 45%;
	left: 45%;
	z-index: 1;
	font-size: 30px;
	display: none;	
}
.modal-backdrop
{
    opacity:0.7 !important;
}

    </style>
    <script>
        var options
        var theWheel
        var audio = new Audio(baseUrl+'/assets/media/games/wheel_fortune/sound/tick.mp3');

        function beforAnimation(){
            $('#spinBtn').fadeOut();
            $('.congrats').fadeOut();
        }

        function afterSpin(){
            $('#spinBtn').show();
        }

        function playSound() {
            // Stop and rewind the sound (stops it if already playing).
            audio.pause();
            audio.currentTime = 0;
            // Play the sound.
            audio.play();
        }



        function showCongrats(){
            //  $("#animatedModal").animatedModal();
            $('#prizeModal').modal('show');
            afterSpin();
            $('.emitter').pburst('burst_part', 20);
            setTimeout(function(){
                $('.emitter').pburst('burst_part', 20);
            }, 4000);
            var winningSegment = theWheel.getIndicatedSegment();
             $('#prize').text(winningSegment.text)
            $('.congrats').show();
                var numberOfStars = 50;
                for (var i = 0; i < numberOfStars; i++) {
                    $('.congrats').append('<div class="blob fa fa-star ' + i + '"></div>');
                }	
                animateText();
                // animateBlobs();
        }

        function alertPrize() {
            afterSpin();
            var winningSegment = theWheel.getIndicatedSegment();
            // alert("You have won " + winningSegment.text + "!");
            $('#prize').text(winningSegment.text)
            $('#prizeModal').modal('show');
        }

        function changePic() {
            $('#szebra').attr('src', "<?php echo base_url('assets/images/img/happy.gif'); ?>");
        }

        function initWheel(){
            $.when(fetchGetData('/raffle/get_sz_wheel_properties')).then(function (wheel) {
            var wheelObj = $.parseJSON(wheel.trim());
            var count = 0;
            var segment = [];
            // // console.log(wheelObj);
            $.each(wheelObj, function (index, values) {
            segment[count] = {
                'fillStyle': values.fillStyle,
                'text': values.text,
                'size': winwheelPercentToDegrees(values.percent)
            }
            count++;
            })
            // console.log(segment);
            console.log(wheelObj.length);

            var options = {
                'canvasId': 'myCanvas',
                'textFillStyle': '#FFFFFF',
                'textAlignment': 'outer',
                'textFontFamily': 'Verdana',
                'textFontSize': 11, // Set font size accordingly.
                // 'textOrientation' : 'vertical', // Set orientation. horizontal, vertical, curved.
                'outerRadius': 250, // Set radius to so wheel fits the background.
                'innerRadius': 80, // Set inner radius to make wheel hollow.
                'numSegments': 34,
                'textMargin': 10,
                'segments': segment,
                'animation': // Note animation properties passed in constructor parameters.
                {
                    'type': 'spinToStop', // Type of animation.
                    'duration': 15, // How long the animation is to take in seconds.
                    'spins': 8, // The number of complete 360 degree rotations the wheel is to do.
                    'callbackFinished': 'showCongrats()',
                    'callbackBefore': 'beforAnimation()',
                    'callbackSound': playSound,    // Specify function to call when sound is to be triggered.
                    'soundTrigger': 'pin' 
                },
                'pins':    // Specify pin parameters.
                {
                    'number': 74,
                    'outerRadius': 2,
                    // 'margin': 10,
                    'fillStyle': '#7734c3',
                    'strokeStyle': '#ffffff'
                }
            }
            theWheel = new Winwheel(options);
            });
        }

        $(function () {
            initWheel();
            $('.emitter').pburst({
                particle: baseUrl + '/assets/src/custom/plugins/jquery-particles-burst-master/dist/star.png',
                partoffset: 500,
                duration: 5000
            });
            // $("#animatedModal").animatedModal()
        });
    </script>
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-footer--push canvas-default">
    <!-- <video autoplay muted loop id="myVideo" style="position: fixed;top: -74px;width: 100%;">
        <source src="<?php echo base_url('assets/media/games/wheel_fortune/disco1.mp4'); ?>" type="video/mp4">
    </video> -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-image: url(<?php echo base_url("assets/bg.jpg");?>);background-size: cover;background-repeat: no-repeat;">
				<div class="row">
					 <div class="col-md-2" style="z-index: 999;">
                        
                        <img src="<?php echo base_url('assets/side.png'); ?>" style="width: 230%;float: left;">
                        <a href="#"onclick="theWheel.startAnimation();" id="spinBtn"><img  src="<?php echo base_url("assets/btn.png"); ?>"  style="margin-top: -136%;margin-left: 112%;width:80%"></a>	
                    </div>
                    <div class="col-md-8"  style="text-align:center;">
                        <!-- <div class="col-md-12 congrats" style="display:none">
                            <p id="greetings">
                            Congratulations!! You have Won</p>
                            <p id="prize" style="font-size:70px"></p>
                        </div> -->
                        <div id="canvasContainer">
                            <canvas id='myCanvas' width='600' height='600'>
                            </canvas>
                            <!-- <div class="centered">
                                                <img id="szebra" src="<?php echo base_url('assets/images/img/happy.png'); ?>" alt="" style="width: 163px;position: fixed;left: 953px;top: 294px;border-radius: 104px;border: 7px solid #000;height: 168px;background: #bfbfbf;">
                                            </div> -->
										
                        </div>
                    </div>
					 <div class="col-md-2" >
                       <img src="<?php echo base_url('assets/side2.png'); ?>" style="width: 230%;float: right;margin-top: 22px;margin-right: -15px;">
                    </div>
				</div>
				
                
            </div>
        </div>
    </div>
    <div class="modal fade" id="prizeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="congrats" style="display:none">
                        <!-- <img id="szebra" src="<?php echo base_url('assets/images/img/excited.gif'); ?>" alt="" style="width: 163px;"> -->
                        <div id="greetings">
                            <span style="font-family: 'Pacifico', cursive;">
                                    Congratulations!!
                            </span><br>
                            <span style="font-family: 'Poppins', sans-serif;color: #b5cfe6;">You have Won</span>

                        </div>
                        <div id="prize" style="font-size:70px;font-weight: bold;color: #e8e8e7;"></div>
                    <div class="emitter"></div>
                    <div class="emitter pull-right"></div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>