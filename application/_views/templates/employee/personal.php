<style type="text/css">
    .bg-green{
        background: #4CAF50 !important;
    }
    .btn-outline-green{
        color: #4CAF50 !important;
        background:white !important;
        border-color:#4CAF50 !important;
    }
    .btn-outline-green:hover{
        background: #4CAF50 !important;
        color:white !important;
        border-color:#4CAF50 !important;
    }
    .btn-green{
        background: #4CAF50;
        color:white !important;
    }
    .btn-green:hover{
        background:#2a862e !important;
    }
    .nav-pills .nav-link.active{
        background: #4CAF50 !important;
    }
    .nav.nav-pills .nav-link{
        padding:6px 5px !important;
        color:#aaa;
        border-bottom:1px solid #444;
        border-radius:0;
    }
    .font-green{
        color: #4CAF50 !important;
    }
    .font-white{
        color:white !Important;
    }
    .border-light-green{
        border-color: #4CAF507d !important;
    }
    .border-green{
        border-color: #4CAF50 !important;
    }
    .body{
        padding-right:0 !important;
    }
    .swal2-title{
        font-family:'Poppins' !important;
    }
    .font-poppins{
        font-family:'Poppins' !important;
    }
    .m-widget4 .m-widget4__item {
        padding:0 !important;
    }
    td.disabled{
        background:#eee !Important;
    }
    .m-content .m-stack__item{
        padding:0 !important;
        background:0 !important;
        border:0 !important;
    }

    .card-shadow{
        box-shadow: 0 1px 8px rgba(0,0,0,0.15);
    }
    .text-line-height-1{
        line-height:1;
    }
    .font-10{
        font-size:10px !Important;
    } 
    .font-11{
        font-size:11px !Important;
    } 
    .font-12{
        font-size:12px !Important;
    } 
    .font-13{
        font-size:13px !Important;
    }
    .font-14{
        font-size:14px !Important;
    }
    .font-15{
        font-size:15px !Important;
    }
    .font-16{
        font-size:16px !Important;
    }
    #profiles-directory p{
        line-height:1.3 !Important;
    }
    .fa{
        text-align:center !important;
    }
    .row-value > div:first-child{
        font-size:11px;
        font-weight:bolder;
    }

    .row-value > div:nth-child(2) >a, .row-value > .static{
        font-size:11px;
        color:#333 !important;
        font-weight:600;  
        text-decoration: none !important;
        cursor: default;
    }
    .the-legend {
        border-style: none;
        border-width: 0;
        font-size: 12px;
        line-height: 20px;
        margin-bottom: 0;
        width: auto;
        padding: 0 15px;
        border:1px solid #4CAF507d;
        background:white;
        color: #4CAF50  !important;
        font-weight:600;
    }
    .the-fieldset {
        margin-bottom:15px;
        border:1px solid  #4CAF507d;
        padding: 10px 15px;
    }
    .form-control{
        font-size:12px;
    }
    .popover{
        width:100% !important;
    }
    .popover select option {
        text-transform:capitalize;
    }
    a[id*='tbl_applicant'],a[id*='tbl_employee']{
        text-transform: capitalize;
    }
    .notcapital{
        text-transform: none !Important;
    }

    li span.current{
        background:#333 !important;
    }
    figure figcaption {
        position: absolute;
        bottom: 0px;
        color: #fff;
        width: 100%;
        text-shadow: 0 0 10px #000;
        /*background: rgb(0,0,0,0.5);*/
        background:#333;
        margin:5px 0px 0px -10px;
        text-align:center;
        padding: 5px 0;
    }
    @media  (max-width:1220px) and (min-width:932px) {
        figure img{
            width:80% !important;
            height:80% !important;
        }
    }
    .daterangepicker td, .daterangepicker th{
        width:30px !important;
        height:30px !important;
        font-size:13px !important;
    }

    #table-dtr tbody tr td{
        vertical-align: middle !important;
        text-align:center !Important;
        font-size:12px;
    }
    .m-quick-sidebar .mCSB_scrollTools {
        right:0 !Important;
    }
    .m-popover.m-popover--skin-dark.popover{
        background:#333 !important;
    }

    #editProfileModal label, #editProfileModalFamily label{
        font-size:12px;
        font-weight:bolder;
    }
    .d-xl-inline{
        margin-right:3px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper"  id="content">

    <div class="m-content">

        <div class="row">
            <div class="col-lg-3 col-md-4">
                <div class="card card-shadow ">
                    <div class="card-body p-0" style="background:#333;">
                        <div class="row no-gutters">
                            <div class="col-12 col-sm-6  col-md-12">
                                <div class="p-2">
                                    <img src="" class="gambar img-responsiv " id="item-img-output" style="background:white;width:100% !important;object-fit:cover;"/>

                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-12">
                                <ul class="nav nav-pills m-2" role="tablist">
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link  active show" data-toggle="tab" href="#tab-personal"><span class="la la-user d-lg-none d-xl-inline"></span> Personal  <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link" data-toggle="tab" href="#tab-emergency"><span class="la la-ambulance d-lg-none d-xl-inline"></span> Emergency <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link" data-toggle="tab" href="#tab-family"><span class="la la-group d-lg-none d-xl-inline"></span> Family <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link" data-toggle="tab" href="#tab-application"><span class="la la-clipboard d-lg-none d-xl-inline"></span> Application <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link" data-toggle="tab" href="#tab-work"><span class="la la-sitemap d-lg-none d-xl-inline"></span> Work <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link" data-toggle="tab" href="#tab-logs"><span class="la la-list d-lg-none d-xl-inline"></span> Logs <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link" data-toggle="tab" href="#tab-payroll"><span class="la la-money d-lg-none d-xl-inline"></span> Compensation <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-9  col-md-8">
                <div class="row no-gutters">
                    <div class="col-md-6 text-md-left text-center">
                        <p style="font-size:28px;color: #2a862e;" ><span  id="image-caption-fname" class="m--font-boldest"></span> <span  id="image-caption-lname" class="m--font-boldest"></span></p>
                    </div>
                    <div class="col-md-6  text-md-right text-center">
                        <button class="btn btn-outline-green m-btn m-btn--icon btn-sm" id='editBtn2' data-toggle="modal" data-target="#editProfileModal" style='display:none;padding:10px !important'>

                            <span>
                                <i class="fa fa-pencil-square-o"></i>
                                <span>Edit Profile</span>
                            </span>
                        </button>
                        <!--                        <button  class="btn btn-outline-green m-btn m-btn--icon btn-sm" id='editBtn-3' data-toggle="modal" data-target="#editProfileModalFamily" style='display:none;padding:10px !important'>
                                                    <span>
                                                        <i class="fa fa-group"></i>
                                                        <span>Edit Family</span>
                                                    </span>
                                                </button>-->
                    </div>
                </div>

                <div class="tab-content">
                    <div class="tab-pane active show" id="tab-personal" role="tabpanel">
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">BASIC INFORMATION</div>
                            <div class="card-footer p-3">
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">FULL NAME</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Last Name:</div>
                                                <div class="col-8"><a href="#"  id="tbl_applicant-lname" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-lg-2 py-2 py-md-2 py-lg-2">
                                                <div class="col-4">First Name:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-fname" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Middle Name:</div>
                                                <div class="col-8"><a href="#"  id="tbl_applicant-mname" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Extension:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-nameExt" data-type='select'  data-required="no"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Nickname:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-nickName" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">BIRTHDAY</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Birthdate:</div>
                                                <div class="col-8"><a href="#"  id="tbl_applicant-birthday" data-type='date' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Age:</div>
                                                <div class="col-8  static" id="personal-age"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Birthplace:</div>
                                                <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#"  id="tbl_applicant-birthplace" data-type='textarea' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">OTHER BASIC INFO</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Religion:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-religion" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Blood Type:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-bloodType" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Sex:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-gender" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Civil Status:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-civilStatus" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <br />
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">ADDRESS AND CONTACT INFORMATION</div>
                            <div class="card-footer p-3">
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">ADDRESSES</legend>

                                    <div class="row ">
                                        <div class="col-12">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Present:</div>
                                                <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_applicant-presentAddress" data-type='textarea' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Permanent:</div>
                                                <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_applicant-permanentAddress" data-type='textarea' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">CONTACT INFORMATION</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Telephone:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-tel" data-type="number"  data-required="no"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Mobile:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-cell" data-type="number"  data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Email:</div>
                                                <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_applicant-email" class="notcapital" data-type='email' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <br />
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">EDUCATION DETAILS</div>
                            <div class="card-footer p-3">
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">EDUCATION</legend>
                                    <div class="row ">
                                        <div class="col-12">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Educational Attainment:</div>
                                                <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_employee-educationalAttainment" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">High School Attended:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-highSchool" data-type='textarea' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Year Graduated:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-highSchoolYear" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Last School Attended:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-lastSchool" data-type='textarea' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Year Left:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-lastSchoolYear" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Course:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-course" data-type='textarea' data-required="no"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-emergency" role="tabpanel">
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">EMERGENCY DETAILS</div>
                            <div class="card-footer p-3">
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">EMERGENCY</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Last Name:</div>
                                                <div class="col-8"><a href="#"   id="tbl_employee-emergencyLname" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">First Name:</div>
                                                <div class="col-8"><a href="#"  id="tbl_employee-emergencyFname" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Middle Name:</div>
                                                <div class="col-8"><a href="#"   id="tbl_employee-emergencyMname" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Extension:</div>
                                                <div class="col-8"><a href="#"  id="tbl_employee-emergencyNameExt" data-type='select'  data-required="no"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Contact:</div>
                                                <div class="col-8"><a href="#"  id="tbl_employee-emergencyContact"data-required="yes" data-type="number"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Relationship:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-emergencyRelationship" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12">
                                            <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Address:</div>
                                                <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#"  id="tbl_employee-emergencyAddress" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-family" role="tabpanel">
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                FAMILY INFORMATION
                            </div>
                            <div class="card-footer p-3">
                                <fieldset class="the-fieldset bg-white" id="fieldset-spouse">
                                    <legend class="the-legend  m--font-bold m--font-brand">SPOUSE</legend>
                                    <div id="fieldset-spouse-div"></div>
                                </fieldset>
                                <fieldset class="the-fieldset bg-white"  id="fieldset-children">
                                    <legend class="the-legend  m--font-bold m--font-brand">CHILDREN</legend>
                                    <div id="fieldset-children-div"></div>
                                </fieldset>
                                <fieldset class="the-fieldset bg-white"  id="fieldset-parents">
                                    <legend class="the-legend  m--font-bold m--font-brand">PARENTS</legend>
                                    <div id="fieldset-parents-div"></div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-application" role="tabpanel">
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">APPLICATION DETAILS</div>
                            <div class="card-footer p-3">
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">EMPLOYMENT HISTORY</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">CC Experience:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-ccaExp" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Latest Employer:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-latestEmployer" data-type='textarea' data-required="no"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Last Position Held:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-lastPositionHeld" data-required="no"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Inclusive Dates:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-inclusiveDates" data-type='daterange' data-required="no"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Contact Number:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-lastEmployerContact" data-type="number" data-required="no"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Address:</div>
                                                <div class="col-8"><a href="#" id="tbl_applicant-lastEmployerAddress" data-type='textarea' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <br />
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                PREVIOUS ACCOUNTS
                            </div>
                            <div class="card-footer p-3">
                                <fieldset class="the-fieldset bg-white" id="fieldset-prev-accounts">
                                    <legend class="the-legend  m--font-bold m--font-brand">PREVIOUS ACCOUNTS</legend>

                                    <table class="table m-table table-bordered table-hover table-sm" id="table-previousAccounts">
                                        <thead>
                                        <th>Name</th>
                                        <th>Work Duration</th>
                                        <th></th>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-work" rol e="tabpanel">
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">USER DETAILS</div>
                            <div class="card-footer p-3">
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">ACCOUNT DETAILS</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Username:</div>
                                                <div class="col-8"><a href="#" id="tbl_user-username" class="notcapital" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Password:</div>
                                                <div class="col-8"><a href="#" id="tbl_user-password" data-type='password'  class="notcapital hide-text" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Department:</div>
                                                <div class="col-8 static" id="work-department"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Account:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-acc_id" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">ID Number:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-id_num" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Role:</div>
                                                <div class="col-8"><a href="#" id="tbl_user-role_id" data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">SUPERVISOR</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Name:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-Supervisor"  data-type='select' data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Email:</div>
                                                <div class="col-8 static"  id="work-supervisorEmail">jeffrey@supportzebra.com</div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">GOVERNMENT NUMBERS</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">SSS:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-sss" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Philhealth:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-philhealth" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Pag-ibig:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-pagibig" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">TIN/BIR:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-bir" data-required="yes"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                            </div>
                        </div>
                        <br />
                        <div class="card card-shadow">

                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                POSITIONS
                            </div>
                            <div class="card-footer p-3">

                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">CURRENT</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Code:</div>
                                                <div class="col-8 static" id='work-positioncode'> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Position:</div>
                                                <div class="col-8 static" id='work-positionname'></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Status:</div>
                                                <div class="col-8 static" id='work-status'></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Start Date:</div>
                                                <div class="col-8 static" id='work-dateFrom'></div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">HISTORY</legend>
                                    <div id='positionhistory-fieldset'></div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-payroll" role="tabpanel">
                        <div class="card card-shadow">

                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">COMPENSATION INFORMATION</div>
                            <div class="card-footer p-3">
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">RATES</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Monthly:</div>
                                                <div class="col-8 static"  id='payroll-rate-monthly'> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Semi-Monthly:</div>
                                                <div class="col-8 static" id='payroll-rate-semi'></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Daily:</div>
                                                <div class="col-8 static" id='payroll-rate-daily'></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Hourly:</div>
                                                <div class="col-8 static" id='payroll-rate-hourly'></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">PAYROLL</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Salary Mode:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-salarymode" data-type='select' data-required="yes"></a> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Work Days per Week:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-workDaysPerWeek" data-type='select' data-required="yes"></a> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">ATM MODE:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-isAtm" data-type='select' data-required="yes">ATM</a> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">ATM Account Number:</div>
                                                <div class="col-8"><a href="#" id="tbl_employee-atm_account_number" data-type="number" data-required="no"></a> </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">SEMI-MONTHLY GOVERNMENT PAYMENTS</legend>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">BIR Tax:</div>
                                                <div class="col-8 static"  id='payroll-tax'></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">SSS:</div>
                                                <div class="col-8 static" id='payroll-sss'></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Philhealth:</div>
                                                <div class="col-8 static" id='payroll-philhealth'></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Pag-ibig Basic:</div>
                                                <div class="col-8 static" id='payroll-pagibig'></div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row ">
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">Pag-ibig Additional: &#8369;</div>
                                                <div class="col-8"><a href="#" id="tbl_pagibig_add_contribution-amount" data-required="yes"></a> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6 m--hide">
                                            <div class="row row-value py-2 py-md-2 py-lg-2">
                                                <div class="col-4">BIR Category:</div>
                                                <div class="col-8"><a href="#" id="tbl_tax_emp-tax_desc_id" data-type='select' data-required="yes">S/ME</a> </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-logs" role="tabpanel">
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">DAILY TIME RECORD</div>
                            <div class="card-footer p-3">
                                <div class="row mb-2">
                                    <div class="col-md-6 col-sm-8 col-xs-12">
                                        <div class="m-input-icon m-input-icon--left" id="search-dates">
                                            <input type="text" class="form-control m-input" placeholder="Date Range" readonly style="background: #fff;">
                                            <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                        </div>
                                    </div>
                                </div>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">DTR LOGS</legend>
                                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="date-alert" style="display:none">
                                        <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                        <div class="m-alert__text"><strong>No Record Found! </strong> <br>Please adjust your search criteria and try again.
                                        </div>
                                    </div>
                                    <table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">
                                        <thead style="background: #555;color: white;">
                                        <th class="text-center">Schedule Date</th>
                                        <th class="text-center" colspan="2">Shift</th>
                                        <th class="text-center">Clock In</th>
                                        <th class="text-center">Clock Out</th>
                                        <th class="text-center">Breaks</th>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                    <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">
                                        <div class="m-stack__item m-stack__item--left m-stack__item--middle" >
                                            <ul id="dtr-pagination">
                                            </ul>
                                        </div>
                                        <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:12%">
                                            <select class="form-control m-input form-control-sm m-input--air" id="dtr-perpage">
                                                <option>5</option>
                                                <option>10</option>
                                                <option>20</option>
                                            </select>
                                        </div>
                                        <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="dtr-count"></span> of <span id="dtr-total"></span> records</div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <br />
                        <div class="card card-shadow">
                            <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">AUDIT TRAIL RECORDS</div>
                            <div class="card-footer p-3">
                                <div class="row mb-2">
                                    <div class="col-md-6 col-sm-8 col-xs-12">
                                        <div class="m-input-icon m-input-icon--left" id="search-dates-trail">
                                            <input type="text" class="form-control m-input" placeholder="Date Range" readonly style="background: #fff;">
                                            <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                        </div>
                                    </div>
                                </div>
                                <fieldset class="the-fieldset bg-white">
                                    <legend class="the-legend  m--font-bold m--font-brand">AUDIT TRAIL</legend>
                                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="date-alert-trail" style="display:none">
                                        <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                        <div class="m-alert__text"><strong>No Record Found! </strong> <br>Please adjust your search criteria and try again.
                                        </div>
                                    </div>
                                    <div style="overflow-y:scroll;max-height:250px;">
                                        <table class="table m-table table-hover table-sm table-bordered" id="table-trail" style="font-size:13px" >
                                            <thead style="background: #555;color: white;">
                                            <th>Remarks</th>
                                            <th>Logs</th>
                                            <th>IP Address</th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>

                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editProfileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel3">Edit Profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" data-scrollbar-shown="true" data-scrollable="true" data-max-height="430">
                <fieldset class="the-fieldset bg-white">
                    <legend class="the-legend  m--font-bold m--font-brand">PERSONAL</legend>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Last Name:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_applicant-lname-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">First Name:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_applicant-fname-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">Middle Name:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_applicant-mname-2" data-required="yes" />

                        </div>
                        <div class="col-md-3">
                            <label for="">Extension:</label> 
                            <select class="form-control form-control-sm m-input"  id="tbl_applicant-nameExt-2" data-required="no">
                                <option value="">--</option>
                                <option value="Jr">Jr</option>
                                <option value="Sr">Sr</option>
                                <option value="I">I</option>
                                <option value="II">II</option>
                            </select>

                        </div>
                    </div>
                    <hr class="border-light-green"/>
                    <div class="row">

                        <div class="col-md-3">
                            <label for="">Nick Name:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_applicant-nickName-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">Birthdate:</label>
                            <input type="text" class="form-control form-control-sm  custom-datepicker" readonly="" id="tbl_applicant-birthday-2"  data-required="yes"/>
                        </div>
                        <div class="col-md-6">
                            <label for="">Place of Birth:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_applicant-birthplace-2"  data-required="yes"/>
                        </div>
                    </div>
                    <hr class="border-light-green"/>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Religion:</label>
                            <select class="form-control form-control-sm m-input"  id="tbl_applicant-religion-2"  data-required="yes">
                                <option value="">--</option>
                                <option value="Roman Catholic">Roman Catholic</option>
                                <option value="Muslim">Muslim</option>
                                <option value="Protestant">Protestant</option>
                                <option value="Iglesia Ni Cristo">Iglesia Ni Cristo</option>
                                <option value="El Shaddai">El Shaddai</option>
                                <option value="Jehovahs Witnesses">Jehovahs Witnesses</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">Blood Type:</label>
                            <select class="form-control form-control-sm m-input"  id="tbl_applicant-bloodType-2" data-required="yes">
                                <option value="">--</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="O">O</option>
                                <option value="AB">AB</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">Sex:</label>
                            <select class="form-control form-control-sm m-input"  id="tbl_applicant-gender-2" data-required="yes">
                                <option value="">--</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">Civil Status:</label>
                            <select class="form-control form-control-sm m-input"  id="tbl_applicant-civilStatus-2" data-required="yes">
                                <option value="">--</option>
                                <option value="Single">Single</option>
                                <option value="Married">Married</option>
                                <option value="Legally Separated">Legally Separated</option>
                                <option value="Widower">Widower</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="the-fieldset bg-white">
                    <legend class="the-legend  m--font-bold m--font-brand">ADDRESS AND CONTACT INFORMATION</legend>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Present Address:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_applicant-presentAddress-2" data-required="yes"/>
                        </div>
                        <div class="col-md-6">
                            <label for="">Permament Address:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_applicant-permanentAddress-2" data-required="yes" />
                        </div>
                    </div>
                    <hr class="border-light-green"/>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">Telephone Number:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_applicant-tel-2" data-required="no" data-type="number"/>
                        </div>
                        <div class="col-md-4">
                            <label for="">Mobile Number:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_applicant-cell-2" data-required="yes" data-type="number"/>
                        </div>
                        <div class="col-md-4">
                            <label for="">Email Address:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_applicant-email-2" data-required="yes"/>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="the-fieldset bg-white">
                    <legend class="the-legend  m--font-bold m--font-brand">EDUCATION</legend>
                    <hr class="border-light-green"/>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Educational Attainment:</label>
                            <select class="form-control form-control-sm m-input"  id="tbl_employee-educationalAttainment-2" data-required="yes">
                                <option value="">--</option>
                                <option value="College Level">College Level</option>
                                <option value="College Graduate">College Graduate</option>
                                <option value="Graduate Level">Graduate Level</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="">High School Attended:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_employee-highSchool-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">Year Graduated:</label>
                            <select class="form-control form-control-sm m-input"  id="tbl_employee-highSchoolYear-2"  data-required="yes"/></select>
                        </div>
                    </div>
                    <hr class="border-light-green" />
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Course:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_employee-course-2" data-required="no"/>
                        </div>
                        <div class="col-md-6">
                            <label for="">Last School Attended:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_employee-lastSchool-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">Year Left:</label>
                            <select class="form-control form-control-sm m-input" id="tbl_employee-lastSchoolYear-2" data-required="yes"/></select>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="the-fieldset bg-white">
                    <legend class="the-legend  m--font-bold m--font-brand">GOVERNMENT NUMBERS</legend>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">SSS Number:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_employee-sss-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">Philhealth Number:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_employee-philhealth-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">Pag-ibig Number:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_employee-pagibig-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">TIN/BIR Number:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_employee-bir-2" data-required="yes"/>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="the-fieldset bg-white">
                    <legend class="the-legend  m--font-bold m--font-brand">EMERGENCY CONTACT PERSON</legend>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Last Name:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_employee-emergencyLname-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">First Name:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_employee-emergencyFname-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">Extension:</label>
                            <select class="form-control form-control-sm m-input"  id="tbl_employee-emergencyNameExt-2" data-required="no">
                                <option value="">--</option>
                                <option value="Jr">Jr</option>
                                <option value="Sr">Sr</option>
                                <option value="I">I</option>
                                <option value="II">II</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">Middle Name:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_employee-emergencyMname-2" data-required="yes"/>
                        </div>
                    </div>
                    <hr class="border-light-green"/>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Mobile Number:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_employee-emergencyContact-2" data-required="yes" data-type="number"/>
                        </div>
                        <div class="col-md-6">
                            <label for="">Address:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_employee-emergencyAddress-2" data-required="yes"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">Relationship:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_employee-emergencyRelationship-2" data-required="yes"/>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="the-fieldset bg-white">
                    <legend class="the-legend  m--font-bold m--font-brand">EMPLOYMENT HISTORY</legend>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">With CC Experience:</label>       
                            <select class="form-control form-control-sm m-input"  id="tbl_applicant-ccaExp-2" data-required="yes">
                                <option value="">--</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="">Latest Employer:</label>
                            <input type="text" class="form-control form-control-sm m-input" id="tbl_applicant-latestEmployer-2" data-required="no"/>
                        </div>
                        <div class="col-md-4">
                            <label for="">Last Position Held:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_applicant-lastPositionHeld-2" data-required="no"/>
                        </div>
                    </div>
                    <hr class="border-light-green"/>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Inclusive dates:</label>
                            <input type="text" class="form-control form-control-sm  custom-daterangepicker"  id="tbl_applicant-inclusiveDates-2" data-required="no">
                        </div>
                        <div class="col-md-6">
                            <label for="">Address:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_applicant-lastEmployerAddress-2" data-required="no"/>
                        </div>
                        <div class="col-md-3">
                            <label for="">Contact Number:</label>
                            <input type="text" class="form-control form-control-sm m-input"  id="tbl_applicant-lastEmployerContact-2" data-required="no" data-type="number"/>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="the-fieldset bg-white">
                    <legend class="the-legend  m--font-bold m--font-brand">FAMILY </legend>

                    <div id="modal-family"></div> 
                    <button class="btn btn-outline-metal btn-sm m-btn m-btn--icon mb-2" id="family-adder">
                        <span>
                            <i class="la la-user-plus"></i>
                            <span>New Record</span>
                        </span>
                    </button>
                </fieldset>

            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-green" id="btn-requestChanges">Request changes to HR</button>
            </div>
        </div>
    </div>
</div>

<!-- end::Quick Sidebar -->	
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">
        function tz_date(thedate, format = null) {
            var tz = moment.tz(new Date(thedate), "Asia/Manila");
            return (format === null) ? moment(tz) : moment(tz).format(format);
        }
        var setFamily = function (family, isActive) {
            $("#fieldset-children,#fieldset-spouse,#fieldset-parents").hide();
            $("#fieldset-spouse-div").html("");
            $("#fieldset-children-div").html("");
            $("#fieldset-parents-div").html("");
            $.each(family, function (x, fam) {
                if (fam.relation === 'Son' || fam.relation === 'Daughter') {
                    var str = '<div class="row ">'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">Last Name:</div>'
                            + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-lname-' + fam.employeeFamily_ID + '">' + fam.lname + '</a></div>'
                            + '</div>'
                            + '</div>'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">First Name:</div>'
                            + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-fname-' + fam.employeeFamily_ID + '">' + fam.fname + '</a></div>'
                            + '</div>'
                            + '</div>'
                            + '</div>'
                            + '<br />'
                            + '<div class="row ">'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">Middle Name:</div>'
                            + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-mname-' + fam.employeeFamily_ID + '">' + fam.mname + '</a></div>'
                            + '</div>'
                            + '</div>'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">Age:</div>'
                            + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-age-' + fam.employeeFamily_ID + '">' + fam.age + '</a></div>'
                            + '</div>'
                            + '</div>'
                            + '</div>'
                            + '<br />'
                            + '<div class="row ">'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">Relation:</div>'
                            + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-relation-' + fam.employeeFamily_ID + '" data-type="select">' + fam.relation + '</a></div>'
                            + '</div>'
                            + '</div>'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">School Attended:</div>'
                            + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-schoolAttended-' + fam.employeeFamily_ID + '" data-type="textarea">' + fam.schoolAttended + '</a></div>'
                            + '</div>'
                            + '</div>'
                            + '</div><hr />';
                    $("#fieldset-children-div").append(str);
                    $("#fieldset-children").show();
                } else {
                    var str = '<div class="row ">'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">';
                    if (fam.relation === 'Mother') {
                        str += '<div class="col-sm-4">' + fam.relation + '\'s Maiden Name:</div>';
                    } else {
                        str += '<div class="col-sm-4">' + fam.relation + '\'s Last Name:</div>';
                    }
                    str += '<div class="col-sm-8"> <a href = "#" id="tbl_employee_family-lname-' + fam.employeeFamily_ID + '">' + fam.lname + '</a></div > '
                            + '</div>'
                            + '</div>'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">First Name:</div>'
                            + '<div class="col-sm-8"> <a href = "#" id="tbl_employee_family-fname-' + fam.employeeFamily_ID + '">' + fam.fname + '</a></div > '
                            + '</div>'
                            + '</div>'
                            + '</div>'
                            + '<br />'
                            + '<div class="row ">'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">Extension:</div>'
                            + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-extension-' + fam.employeeFamily_ID + '" data-type="select">' + fam.extension + '</a></div>'
                            + '</div>'
                            + '</div>'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">Middle Name:</div>'
                            + '<div class="col-sm-8"> <a href = "#" id="tbl_employee_family-mname-' + fam.employeeFamily_ID + '">' + fam.mname + '</a></div >'
                            + '</div>'
                            + '</div>'
                            + '</div>'
                            + '<br />'
                            + '<div class="row ">'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">Contact Number:</div>'
                            + '<div class="col-sm-8"><a href="#"  id="tbl_employee_family-contactNumber-' + fam.employeeFamily_ID + '">' + fam.contactNumber + '</a></div>'
                            + '</div>'
                            + '</div>'
                            + '<div class="col-sm-6">'
                            + '<div class="row  row-value">'
                            + '<div class="col-sm-4">Relation:</div>'
                            + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-relation-' + fam.employeeFamily_ID + '" data-type="select">' + fam.relation + '</a></div>'
                            + '</div>'
                            + '</div>'
                            + '</div>';
                    if (fam.relation === 'Mother' || fam.relation === 'Father') {
                        str += '<br /><div class="row ">'
                                + '<div class="col-sm-6">'
                                + '<div class="row  row-value">'
                                + '<div class="col-sm-4">Address:</div>'
                                + '<div class="col-sm-8"><a href="#"  id="tbl_employee_family-address-' + fam.employeeFamily_ID + '"  data-type="textarea">' + fam.address + '</a></div>'
                                + '</div>'
                                + '</div>'
                                + '</div><hr />';
                        $("#fieldset-parents-div").append(str);
                        $("#fieldset-parents").show();
                    } else {
                        str += '<hr />';
                        $("#fieldset-spouse-div").append(str);
                        $("#fieldset-spouse").show();
                    }
                }
            });
            $('#fieldset-children-div').children('hr').last().remove();
            $('#fieldset-parents-div').children('hr').last().remove();
            $('#fieldset-spouse-div').children('hr').last().remove();
        };
        var setPositions = function () {

            var emp_id = "<?php echo $this->session->userdata('emp_id') ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Employee/employeePositionDetails",
                data: {
                    emp_id: emp_id
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    var position = res.position;
                    var positionHistory = res.positionHistory;
                    var payroll = res.payroll;
                    var employee = res.employee;
                    if (position !== null && payroll !== null) {

                        changeRates(employee.salarymode, position.rate, employee.workDaysPerWeek, payroll);
                    }
                    //POSITION CURRENT--------------------------------------------------
                    if (position !== null) {
                        $("#work-department").html(position.dep_name + " - " + position.dep_details);
                        $("#work-positioncode").html(position.pos_name);
                        $("#work-positionname").html(position.pos_details);
                        $("#work-status").html(position.status);
                        $("#work-dateFrom").html(moment(position.dateFrom).format('MMM DD, YYYY'));
                        $("#changePositionModal").data('datefrom', position.dateFrom);
                    } else {
                        $("#work-department").html('');
                        $("#work-positioncode").html('');
                        $("#work-positionname").html('');
                        $("#work-status").html('');
                        $("#work-dateFrom").html('');
                    }
                    //POSITION HISTORY--------------------------------------------------
                    var posHist_string = "";
                    var historycount = positionHistory.length;
                    if (positionHistory !== null) {
                        $.each(positionHistory, function (phkey, position) {
                            posHist_string += '<div class="row ">'
                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                    + '<div class="row row-value py-2 py-md-2 py-lg-2">'
                                    + '<div class="col-4">Code:</div>'
                                    + '<div class="col-8 static" id="work-history-positioncode-' + position.emp_promoteID + '">' + position.pos_name + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                    + '<div class="row row-value py-2 py-md-2 py-lg-2">'
                                    + '<div class="col-4">Position:</div>'
                                    + '<div class="col-8 static" id="work-history-position-' + position.emp_promoteID + '">' + position.pos_details + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>';
                            posHist_string += '<div class="row ">'
                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                    + '<div class="row row-value py-2 py-md-2 py-lg-2">'
                                    + '<div class="col-4">Status:</div>'
                                    + '<div class="col-8 static" id="work-history-status-' + position.emp_promoteID + '">' + position.status + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                    + '<div class="row row-value py-2 py-md-2 py-lg-2">'
                                    + '<div class="col-4">Start Date:</div>'
                                    + '<div class="col-8 static" id="work-history-dateFrom-' + position.emp_promoteID + '">' + moment(position.dateFrom).format('MMM DD, YYYY') + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>';
                            if (phkey !== historycount - 1) {
                                posHist_string += '<hr style="border-top: 1px solid #4CAF507d"/>';
                            }
                        });
                    }

                    $("#positionhistory-fieldset").html(posHist_string);
                }
            });
        };
        function addCommas(t) {
            return String(t).replace(/(\d)(?=(\d{3})+$)/g, "$1,");
        }
        function changeRates(salarymode, rate, workDaysPerWeek, payroll) {
            var monthly = 0, semi = 0, daily = 0, hourly = 0;
            //PAYROLL COMPUTATIONS
            if (salarymode !== '' || salarymode !== null) {
                if (salarymode.toString().toLowerCase() === 'daily') {
                    daily = rate;
                } else {
                    var divisor = (workDaysPerWeek === '6') ? 313 : 261;
                    monthly = rate;
                    semi = monthly / 2;
                    daily = (monthly * 12) / divisor;
                    hourly = daily / 8;
                }
            }
            var arr_monthly = (parseFloat(monthly).toFixed(2)).split(".");
            var arr_semi = (parseFloat(semi).toFixed(2)).split(".");
            var arr_daily = (parseFloat(daily).toFixed(2)).split(".");
            var arr_hourly = (parseFloat(hourly).toFixed(2)).split(".");
            $("#payroll-rate-monthly").html("&#8369; " + addCommas(arr_monthly[0]) + "." + arr_monthly[1]);
            $("#payroll-rate-semi").html("&#8369; " + addCommas(arr_semi[0]) + "." + arr_semi[1]);
            $("#payroll-rate-daily").html("&#8369; " + addCommas(arr_daily[0]) + "." + arr_daily[1]);
            $("#payroll-rate-hourly").html("&#8369; " + addCommas(arr_hourly[0]) + "." + arr_hourly[1]);
            var sss = (payroll.sss === null) ? 0 : payroll.sss;
            var pagibig = (payroll.pagibig === null) ? 0 : payroll.pagibig;
            var philhealth = (payroll.philhealth === null) ? 0 : payroll.philhealth;
            var tax = (payroll.tax === null) ? 0 : payroll.tax;
            var arr_sss = (parseFloat(sss).toFixed(2)).split(".");
            var arr_pagibig = (parseFloat(pagibig).toFixed(2)).split(".");
            var arr_philhealth = (parseFloat(philhealth).toFixed(2)).split(".");
            var arr_tax = (parseFloat(tax).toFixed(2)).split(".");
            $("#payroll-sss").html("&#8369; " + addCommas(arr_sss[0]) + "." + arr_sss[1]);
            $("#payroll-pagibig").html("&#8369; " + addCommas(arr_pagibig[0]) + "." + arr_pagibig[1]);
            $("#payroll-philhealth").html("&#8369; " + addCommas(arr_philhealth[0]) + "." + arr_philhealth[1]);
            $("#payroll-tax").html("&#8369; " + addCommas(arr_tax[0]) + "." + arr_tax[1]);
        }
        function setPrevAccounts() {
            var emp_id = "<?php echo $this->session->userdata('emp_id') ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Employee/setPrevAccounts",
                data: {
                    emp_id: emp_id
                },
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    $("#table-previousAccounts tbody").html('');
                    var str = "";
                    $.each(result.employees, function (iterator, emp) {
                        var separationDate = moment((emp.separationDate === 'null') ? 'Unknown' : emp.separationDate).format('MMM DD, YYYY');
                        var dateFrom = moment((emp.dateFrom === 'null') ? 'Unknown' : emp.dateFrom).format('MMM DD, YYYY');
                        var separationDateString = (separationDate === 'Invalid date') ? 'Unknown' : separationDate;
                        var dateFromString = (dateFrom === 'Invalid date') ? 'Unknown' : dateFrom;
                        str += '<tr>'
                                + '<td style="vertical-align:middle" class="font-12">' + emp.lname + ', ' + emp.fname + '</td>'
                                + '<td style="vertical-align:middle"  class="font-12">' + separationDateString + ' - ' + dateFromString + '</td>'
                                + '</tr>';
                    });
                    $("#table-previousAccounts tbody").html(str);
                }
            });
        }
        function getAuditTrail() {
            var datestart = $("#search-dates-trail").data('daterangepicker').startDate;
            var dateend = $("#search-dates-trail").data('daterangepicker').endDate;
            var emp_id = "<?php echo $this->session->userdata('emp_id') ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Employee/get_audit_trail",
                data: {
                    datestart: tz_date(moment(datestart), 'YYYY-MM-DD'),
                    dateend: tz_date(moment(dateend), 'YYYY-MM-DD'),
                    emp_id: emp_id
                },
                beforeSend: function () {
                    mApp.block('#table-trail', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    $("#table-trail tbody").html("");
                    var string = '';
                    $.each(res.trails, function (i, trail) {
                        if (trail.remark === 'I - Back from break' || trail.remark === 'O - Out for break ') {
                            var remarks = '<span class="m--font-warning m--font-bolder">' + trail.remark + '</span>';
                        } else if (trail.remark === 'I - DTR' || trail.remark === 'O - DTR') {
                            var remarks = '<span class="m--font-info m--font-bolder">' + trail.remark + '</span>';
                        } else {
                            var remarks = '<span class=" m--font-bold">' + trail.remark + '</span>';
                        }
                        string += '<tr>'
                                + '<td>' + remarks + '</td>'
                                + '<td>' + moment(trail.log).format('MMM DD, YYYY hh:mm:ss A') + '</td>'
                                + '<td>' + trail.ipaddress + '</td>'
                                + '</tr>';
                    });
                    if (string === '') {
                        $("#date-alert-trail").show();
                        $("#table-trail").hide();
                    } else {
                        $("#date-alert-trail").hide();
                        $("#table-trail").show();
                    }
                    $("#table-trail tbody").html(string);
                    mApp.unblock('#table-trail');
                }, error: function () {
                    swal("Error", "Please contact admin", "error");
                    mApp.unblock('#table-trail');
                }
            });
        }
        function getScheduleLogs(limiter, perPage, callback) {

            var datestart = $("#search-dates").data('daterangepicker').startDate;
            var dateend = $("#search-dates").data('daterangepicker').endDate;
            var emp_id = "<?php echo $this->session->userdata('emp_id') ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Employee/get_schedule_logs",
                data: {
                    limiter: limiter,
                    perPage: perPage,
                    datestart: tz_date(moment(datestart), 'YYYY-MM-DD'),
                    dateend: tz_date(moment(dateend), 'YYYY-MM-DD'),
                    emp_id: emp_id
                },
                beforeSend: function () {
                    mApp.block('#table-dtr', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    $("#dtr-total").html(res.total);
                    $("#dtr-total").data('count', res.total);
                    $("#table-dtr tbody").html("");
                    var string = '';
                    $.each(res.data, function (i, item) {
                        if (item.time_start === null) {
                            var shift = "";
                        } else {
                            var shift = item.time_start + " <br /> " + item.time_end;
                        }
                        if (shift === '') {
                            string += '<tr style="background:#f6f7f8">';
                        } else {
                            string += '<tr>';
                        }
                        string += '<td class="text-center">' + tz_date(moment(item.sched_date), 'MMM DD, YYYY') + '</td>';
                        if (shift === '') {
                            string += '<td class="text-center" colspan="2" style="color:' + item.style + ';font-weight:400">' + item.type + '</td>';
                        } else {
                            string += '<td class="text-center" style="color:' + item.style + ';font-weight:400">' + item.type + '</td>';
                            string += '<td class="text-center" style="color:' + item.style + ';font-weight:400">' + shift + '</td>';
                        }
                        if (item.clock_in === null) {
                            string += '<td>--</td>';
                        } else {
                            if (item.clock_in.log === null) {
                                string += '<td>---</td>';
                            } else {
                                string += '<td>' + moment(item.clock_in.log).format('MMM DD, YYYY ') + ' <span class="m--font-bolder">' + moment(item.clock_in.log).format(' hh:mm A') + '</span></td>';
                            }
                        }
                        if (item.clock_out === null) {
                            string += '<td>--</td>';
                        } else {
                            if (item.clock_out.log === null) {
                                string += '<td>---</td>';
                            } else {
                                string += '<td>' + moment(item.clock_out.log).format('MMM DD, YYYY ') + ' <span class="m--font-bolder">' + moment(item.clock_out.log).format(' hh:mm A') + '</span></td>';
                            }
                        }
                        if (item.breaklogs === null) {
                            string += '<td>--</td>';
                        } else {
                            var totaltime = '';
                            if (item.totalbreakminutes === 0) {
                                totaltime = item.totalbreakseconds + ' sec';
                            } else if (item.totalbreakseconds === 0) {
                                totaltime = item.totalbreakminutes + ' min';
                            } else {
                                totaltime = item.totalbreakminutes + ' min & <br /> ' + item.totalbreakseconds + ' sec';
                            }

                            var breaklogs = '<br /><p class=\'font-white\'><b>Break Logs</b></p>';
                            var counter = 1;
                            $.each(item.breaklogs, function (brkey, breaklog) {
                                breaklogs += '<small><b>Break #' + counter + '</b></small><br />';
                                breaklogs += '<small class=\'font-white\'>' + moment(breaklog.breakout.log).format('MMM DD, YYYY hh:mm:ss A') + '</small><br />';
                                breaklogs += '<small class=\'font-white\'>' + moment(breaklog.breakin.log).format('MMM DD, YYYY hh:mm:ss A') + '</small><hr />';
                                counter++;
                            });
                            string += '<td class="font-11 m--font-info m--font-bolder" data-toggle="m-tooltip" data-skin="dark" data-html="true" data-placement="left" title="" data-original-title="' + breaklogs + '" >' + totaltime + ' </td>';
                        }
                        string += '</tr>';
                    });
                    if (string === '') {
                        $("#date-alert").show();
                        $("#table-dtr").hide();
                    } else {
                        $("#date-alert").hide();
                        $("#table-dtr").show();
                    }
                    $("#table-dtr tbody").html(string);
                    mApp.initTooltips();
                    var count = $("#dtr-total").data('count');
                    var result = parseInt(limiter) + parseInt(perPage);
                    if (count === 0) {
                        $("#dtr-count").html(0 + ' - ' + 0);
                    } else if (count <= result) {
                        $("#dtr-count").html((limiter + 1) + ' - ' + count);
                    } else if (limiter === 0) {
                        $("#dtr-count").html(1 + ' - ' + perPage);
                    } else {
                        $("#dtr-count").html((limiter + 1) + ' - ' + result);
                    }
                    mApp.unblock('#table-dtr');
                    callback(res.total);
                }, error: function () {
                    swal("Error", "Please contact admin", "error");
                    mApp.unblock('#table-dtr');
                }
            });
        }
        var setUserUpdates = function (employee) {
            $("#tbl_applicant-inclusiveDates-2").daterangepicker({
                opens: "right",
                drops: "up",
                autoApply: true
            }, function (start, end, label) {
                console.log(start);
                console.log(end);
                console.log(label);
                $("#tbl_applicant-inclusiveDates-2").val(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
            });
            var startCount = 100;
            var year = parseInt(moment().year());
            $("#tbl_employee-lastSchoolYear-2,#tbl_employee-highSchoolYear-2").html('<option value="">--</option>');
            for (var x = startCount; x >= 0; x--) {
                $("#tbl_employee-lastSchoolYear-2,#tbl_employee-highSchoolYear-2").append('<option value="' + year.toString() + '">' + year.toString() + '</option>');
                year = year - 1;
            }

            $("#tbl_applicant-lname-2").data('orig_value', employee.lname);
            $("#tbl_applicant-mname-2").data('orig_value', employee.mname);
            $("#tbl_applicant-fname-2").data('orig_value', employee.fname);
            $("#tbl_applicant-nameExt-2").data('orig_value', employee.nameExt);
            $("#tbl_applicant-nickName-2").data('orig_value', employee.nickName);
            $("#tbl_applicant-birthday-2").data('orig_value', moment(employee.birthday).format('MMM DD, YYYY'));
            $("#tbl_applicant-birthplace-2").data('orig_value', employee.birthplace);
            $("#tbl_applicant-gender-2").data('orig_value', employee.gender);
            $("#tbl_applicant-religion-2").data('orig_value', employee.religion);
            $("#tbl_applicant-civilStatus-2").data('orig_value', employee.civilStatus);
            $("#tbl_applicant-bloodType-2").data('orig_value', employee.bloodType);
            $("#tbl_applicant-email-2").data('orig_value', employee.email);
            $("#tbl_applicant-tel-2").data('orig_value', employee.tel);
            $("#tbl_applicant-cell-2").data('orig_value', employee.cell);
            $("#tbl_applicant-permanentAddress-2").data('orig_value', employee.permanentAddress);
            $("#tbl_applicant-presentAddress-2").data('orig_value', employee.presentAddress);
            $("#tbl_employee-course-2").data('orig_value', employee.course);
            $("#tbl_employee-lastSchoolYear-2").data('orig_value', employee.lastSchoolYear);
            $("#tbl_employee-lastSchool-2").data('orig_value', employee.lastSchool);
            $("#tbl_employee-highSchoolYear-2").data('orig_value', employee.highSchoolYear);
            $("#tbl_employee-highSchool-2").data('orig_value', employee.highSchool);
            $("#tbl_employee-educationalAttainment-2").data('orig_value', employee.educationalAttainment);
            $("#tbl_employee-bir-2").data('orig_value', employee.bir);
            $("#tbl_employee-philhealth-2").data('orig_value', employee.philhealth);
            $("#tbl_employee-pagibig-2").data('orig_value', employee.pagibig);
            $("#tbl_employee-sss-2").data('orig_value', employee.sss);
            $("#tbl_employee-emergencyFname-2").data('orig_value', employee.emergencyFname);
            $("#tbl_employee-emergencyMname-2").data('orig_value', employee.emergencyMname);
            $("#tbl_employee-emergencyLname-2").data('orig_value', employee.emergencyLname);
            $("#tbl_employee-emergencyNameExt-2").data('orig_value', employee.emergencyNameExt);
            $("#tbl_employee-emergencyContact-2").data('orig_value', employee.emergencyContact);
            $("#tbl_employee-emergencyAddress-2").data('orig_value', employee.emergencyAddress);
            $("#tbl_employee-emergencyRelationship-2").data('orig_value', employee.emergencyRelationship);
            $("#tbl_applicant-ccaExp-2").data('orig_value', employee.ccaExp);
            $("#tbl_applicant-latestEmployer-2").data('orig_value', employee.latestEmployer);
            $("#tbl_applicant-lastPositionHeld-2").data('orig_value', employee.lastPositionHeld);
            $("#tbl_applicant-inclusiveDates-2").data('orig_value', employee.inclusiveDates);
            $("#tbl_applicant-lastEmployerAddress-2").data('orig_value', employee.lastEmployerAddress);
            $("#tbl_applicant-lastEmployerContact-2").data('orig_value', employee.lastEmployerContact);
            //FAMILY SETTINGS------------------------------------------------------------------------------



            //FAMILY SETTINGS------------------------------------------------------------------------------

            $("[id$='-2']").each(function (i, elem) {
                var val = $(elem).data('orig_value');
                if (val === null) {
                    console.log("NULL");
                    console.log($(elem).attr('id'));
                    $(elem).val('');
                } else if (val === undefined) {
                    console.log("UNDEFINED");
                    console.log($(elem).attr('id'));
                    $(elem).val('');
                } else {
                    $(elem).val(val.toString());
                }
            });
            $("[id$='-2']").each(function (i, elem) {
                if ($(elem).val() === null) {
                    $(elem).val('');
                }
            });
        };
        var setUserUpdatesFamily = function (family) {
            var familyInstance = 0;
            $("#modal-family").html("");
            var str = "<table><tbody>";
            $.each(family, function (i, item) {
                var relation = item.relation.toLowerCase();
                var general_relation = (relation === 'husband' || relation === 'wife') ? "spouse" : (relation === 'son' || relation === 'daughter') ? 'children' : relation;
                $.each(item, function (x, xitem) {
                    item[x] = (xitem === null) ? "" : xitem;
                });
                var relationOptions = '<option value="">--</option>'
                        + '<option value="Wife" ' + ((item.relation === "Wife") ? "selected" : "") + '>Wife</option>'
                        + '<option value="Husband" ' + ((item.relation === "Husband") ? "selected" : "") + '>Husband</option>'
                        + '<option value="Father" ' + ((item.relation === "Father") ? "selected" : "") + '>Father</option>'
                        + '<option value="Mother" ' + ((item.relation === "Mother") ? "selected" : "") + '>Mother</option>'
                        + '<option value="Son" ' + ((item.relation === "Son") ? "selected" : "") + '>Son</option>'
                        + '<option value="Daughter" ' + ((item.relation === "Daughter") ? "selected" : "") + '>Daughter</option>';
                familyInstance ++;
                str += '<div class="family-div" data-styler="update" data-familyinstance="' + familyInstance + '" data-employeefamilyid="' + item.employeeFamily_ID + '" style="background:#eee;padding:10px"><div class="row">'
                        + '<div class="col-md-3">'
                        + '<label for="">Last Name:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input customfamily" data-field="lname" value="' + item.lname + '"  data-originalvalue="' + item.lname + '" />'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<label for="">First Name:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input customfamily" data-field="fname" value="' + item.fname + '"  data-originalvalue="' + item.fname + '"/>'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<label for="">Middle Name:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input customfamily" data-field="mname" value="' + item.mname + '"  data-originalvalue="' + item.mname + '" />'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<label for="">Name Extension:</label>'
                        + '<select class="form-control form-control-sm m-input customfamily" data-field="extension" value="' + item.extension + '"  data-originalvalue="' + item.extension + '" >'
                        + '<option value="">--</option>'
                        + '<option value="Jr">Jr</option>'
                        + '<option value="Sr">Sr</option>'
                        + '<option value="I">I</option>'
                        + '<option value="II">II</option>'
                        + '</select>'
                        + '</div>'
                        + '</div><hr />'
                        + '<div class="row">'
                        + '<div class="col-md-3">'
                        + '<label for="">Contact Number:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input customfamily" data-field="contactNumber"  value="' + item.contactNumber + '"  data-originalvalue="' + item.contactNumber + '"/>'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<label for="">Relationship:</label>'
                        + '<select class="form-control form-control-sm m-input customfamily" data-field="relation" data-originalvalue="' + item.relation + '">'
                        + relationOptions
                        + '</select>'
                        + '</div>'
                        + '<div class="col-md-6">'
                        + '<label for="">Address:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input family-adult-form customfamily" data-field="address" value="' + item.address + '"  data-originalvalue="' + item.address + '" />'
                        + '</div>'
                        + '</div><hr />'
                        + '<div class="row">'
                        + '<div class="col-md-3">'
                        + '<label for="">Age:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input family-child-form customfamily" data-field="age"  value="' + item.age + '"  data-originalvalue="' + item.age + '"/>'
                        + '</div>'
                        + '<div class="col-md-6">'
                        + '<label for="">School Attended:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input family-child-form customfamily" data-field="schoolAttended" value="' + item.schoolAttended + '" data-originalvalue="' + item.schoolAttended + '"/>'
                        + '</div>'
                        + '</div></div>'
                        + '<hr class="border-light-green"/>';

            });

            str += '</tbody></table>';
            $("#modal-family").html(str);
            $("#modal-family").find('.customfamily[data-field="relation"]').each(function (x, xitem) {
                var val = $(xitem).data('originalvalue');
                if (val === 'Son' || val === 'Daughter') {
                    $(this).closest(".family-div").find(".family-child-form").closest('div').show();
                    $(this).closest(".family-div").find(".family-adult-form").closest('div').hide();
                } else {
                    $(this).closest(".family-div").find(".family-child-form").closest('div').hide();
                    $(this).closest(".family-div").find(".family-adult-form").closest('div').show();
                }
            });
            $("#modal-family").on('change', '.customfamily[data-field="relation"]', function () {
                console.log("TRIGGERED");
                var val = $(this).val();
                console.log(val);
                if (val === 'Son' || val === 'Daughter') {
                    $(this).closest(".family-div").find(".family-child-form").closest('div').show();
                    $(this).closest(".family-div").find(".family-adult-form").closest('div').hide();
                } else {
                    $(this).closest(".family-div").find(".family-child-form").closest('div').hide();
                    $(this).closest(".family-div").find(".family-adult-form").closest('div').show();
                }
            });
            $("#family-adder").click(function () {
                familyInstance ++;
                var newfamily_str = '<div class="family-div"  data-familyinstance="' + familyInstance + '"  data-styler="insert" data-employeefamilyid="" style="background:#eee;padding:10px"><div class="row">'
                        + '<div class="col-md-3">'
                        + '<label for="">Last Name:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input customfamily" data-field="lname" data-originalvalue=""/>'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<label for="">First Name:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input customfamily" data-field="fname" data-originalvalue=""/>'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<label for="">Middle Name:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input customfamily" data-field="mname" data-originalvalue=""/>'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<label for="">Name Extension:</label>'
                        + '<select class="form-control form-control-sm m-input customfamily" data-field="extension"  data-originalvalue="">'
                        + '<option value="">--</option>'
                        + '<option value="Jr">Jr</option>'
                        + '<option value="Sr">Sr</option>'
                        + '<option value="I">I</option>'
                        + '<option value="II">II</option>'
                        + '</select>'
                        + '</div>'
                        + '</div><hr />'
                        + '<div class="row">'
                        + '<div class="col-md-3">'
                        + '<label for="">Contact Number:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input customfamily" data-field="contactNumber" data-originalvalue=""/>'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<label for="">Relationship:</label>'
                        + '<select class="form-control form-control-sm m-input customfamily" data-field="relation" data-originalvalue="">'
                        + '<option value="">--</option>'
                        + '<option value="Wife">Wife</option>'
                        + '<option value="Husband">Husband</option>'
                        + '<option value="Father">Father</option>'
                        + '<option value="Mother">Mother</option>'
                        + '<option value="Son">Son</option>'
                        + '<option value="Daughter">Daughter</option>'
                        + '</select>'
                        + '</div>'
                        + '<div class="col-md-6">'
                        + '<label for="">Address:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input family-adult-form customfamily" data-field="address"  data-originalvalue=""/>'
                        + '</div>'
                        + '</div><hr />'
                        + '<div class="row">'
                        + '<div class="col-md-3">'
                        + '<label for="">Age:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input family-child-form customfamily" data-field="age"  data-originalvalue=""/>'
                        + '</div>'
                        + '<div class="col-md-6">'
                        + '<label for="">School Attended:</label>'
                        + '<input type="text" class="form-control form-control-sm m-input family-child-form customfamily" data-field="schoolAttended" data-originalvalue=""/>'
                        + '</div>'
                        + '</div></div>'
                        + '<hr class="border-light-green"/>';
                $("#modal-family").append(newfamily_str);
            });
        };
        var restrictArea = function (reference) {
            swal({
                title: 'CONFIDENTIAL INFORMATION',
                text: "'Enter Password:",
                input: 'password',
                type: 'warning',
                allowOutsideClick: false,
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonText: 'Access'
            }).then((result) => {
                if (result.value === '') {
                    swal('PASSWORD REQUIRED', 'Only allowed personnel can access this feature', 'error');
                    if (reference === 'tab-payroll') {
                        $('[href="#tab-personal"]').click();
                    }
                } else if (result.value) {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Employee/allowDirectoryAccess/personal",
                        data: {
                            password: result.value
                        }
                    }).done(function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === "Success") {
                            if (reference === 'tab-payroll') {
                                $("#tab-payroll").show();
                            }
                        } else {
                            swal('WRONG PASSWORD', 'Only allowed personnel can access this feature', 'error');
                            if (reference === 'tab-payroll') {
                                $('[href="#tab-personal"]').click();
                            }
                        }
                    }).fail(function (errordata) {
                        swal('WRONG PASSWORD', 'Only allowed personnel can access this feature', 'error');
                        if (reference === 'tab-payroll') {
                            $('[href="#tab-personal"]').click();
                        }
                    });
                } else if (result.dismiss) {
                    if (result.dismiss === 'cancel') {
                        if (reference === 'tab-payroll') {
                            $('[href="#tab-personal"]').click();
                        }
                    }
                }
            });
        };
        $(function () {
            var emp_id = "<?php echo $this->session->userdata('emp_id') ?>";
            var isActive = "yes";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Employee/employeeDetails",
                beforeSend: function () {
                    mApp.block($('#content'));
                },
                data: {
                    emp_id: emp_id
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    var employee = res.employee;
                    var family = res.family;
                    // Start upload preview image
                    $("#item-img-output").attr("src", "<?php echo base_url(); ?>" + employee.pic);
                    $('#item-img-output').on('error', function () {
                        $(this).attr('onerror', null);
                        $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                    });
                    $("#image-caption-lname").html(employee.lname);
                    $("#image-caption-fname").html(employee.fname);
                    $("#tbl_applicant-lname").html(employee.lname);
                    $("#tbl_applicant-mname").html(employee.mname);
                    $("#tbl_applicant-fname").html(employee.fname);
                    $("#tbl_applicant-nameExt").html(employee.nameExt);
                    $("#tbl_applicant-nickName").html(employee.nickName);
                    $("#tbl_applicant-birthday").html(moment(employee.birthday).format('MMM DD, YYYY'));
                    $("#tbl_applicant-birthplace").html(employee.birthplace);
                    $("#tbl_applicant-gender").html(employee.gender);
                    $("#tbl_applicant-religion").html(employee.religion);
                    $("#tbl_applicant-civilStatus").html(employee.civilStatus);
                    $("#tbl_applicant-bloodType").html(employee.bloodType);
                    $("#tbl_applicant-email").html(employee.email);
                    $("#tbl_applicant-tel").html(employee.tel);
                    $("#tbl_applicant-cell").html(employee.cell);
                    $("#tbl_applicant-permanentAddress").html(employee.permanentAddress);
                    $("#tbl_applicant-presentAddress").html(employee.presentAddress);
                    $("#tbl_employee-course").html(employee.course);
                    $("#tbl_employee-lastSchoolYear").html(employee.lastSchoolYear);
                    $("#tbl_employee-lastSchool").html(employee.lastSchool);
                    $("#tbl_employee-highSchoolYear").html(employee.highSchoolYear);
                    $("#tbl_employee-highSchool").html(employee.highSchool);
                    $("#tbl_employee-educationalAttainment").html(employee.educationalAttainment);
                    if (employee.birthday === '') {
                        $("#personal-age").html("No Birthday");
                    } else {
                        var bday = tz_date(moment()).diff(employee.birthday, 'years');
                        $("#personal-age").html(bday + " years old");
                    }
                    $("#work-supervisorEmail").html(employee.supervisor_email);
                    $("#tbl_employee-confidential").html(employee.confidential);
                    $("#tbl_employee-id_num").html(employee.id_num);
                    $("#tbl_employee-acc_id").html(employee.acc_name);
                    $("#tbl_employee-intellicare").html(employee.intellicare);
                    $("#tbl_employee-pioneer").html(employee.pioneer);
                    $("#tbl_employee-bir").html(employee.bir);
                    $("#tbl_employee-philhealth").html(employee.philhealth);
                    $("#tbl_employee-pagibig").html(employee.pagibig);
                    $("#tbl_employee-sss").html(employee.sss);
                    $("#tbl_employee-atm_account_number").html(employee.atm_account_number);
                    $("#tbl_employee-isAtm").html((employee.isAtm === '0') ? "Non-ATM" : (employee.isAtm === '1') ? "ATM" : "On Hold");
                    $("#tbl_employee-salarymode").html(employee.salarymode);
                    $("#tbl_employee-workDaysPerWeek").html(employee.workDaysPerWeek);
                    $("#tbl_employee-confidential").html(employee.confidential);
                    $("#tbl_employee-Supervisor").html(employee.supervisor_name);
                    $("#tbl_employee-emergencyFname").html(employee.emergencyFname);
                    $("#tbl_employee-emergencyMname").html(employee.emergencyMname);
                    $("#tbl_employee-emergencyLname").html(employee.emergencyLname);
                    $("#tbl_employee-emergencyNameExt").html(employee.emergencyNameExt);
                    $("#tbl_employee-emergencyContact").html(employee.emergencyContact);
                    $("#tbl_employee-emergencyAddress").html(employee.emergencyAddress);
                    $("#tbl_employee-emergencyRelationship").html(employee.emergencyRelationship);
                    $("#tbl_user-username").html(employee.username);
                    $("#tbl_user-password").html(employee.password.replace(/[^\s]/g, "*"));
                    $("#tbl_user-role_id").html(employee.role_description);
                    $("#tbl_applicant-cr1").html(employee.cr1);
                    $("#tbl_applicant-cr2").html(employee.cr2);
                    $("#tbl_applicant-cr3").html(employee.cr3);
                    $("#tbl_applicant-source").html(employee.source);
                    $("#tbl_applicant-ccaExp").html((employee.ccaExp === '1') ? "Yes" : "No");
                    $("#tbl_applicant-latestEmployer").html(employee.latestEmployer);
                    $("#tbl_applicant-lastPositionHeld").html(employee.lastPositionHeld);
                    $("#tbl_applicant-inclusiveDates").html(employee.inclusiveDates);
                    $("#tbl_applicant-lastEmployerAddress").html(employee.lastEmployerAddress);
                    $("#tbl_applicant-lastEmployerContact").html(employee.lastEmployerContact);
                    $("#tbl_applicant-failReason").html(employee.failReason);
                    $("#tbl_pagibig_add_contribution-amount").html(res.pagibig_additional);
                    setPositions();
                    setFamily(family, isActive);
                    setPrevAccounts();
                    $("a[id^='tbl_'],#work-supervisorEmail").each(function (id, elem) {
                        var val = $(elem).html().toLowerCase();
                        if (val === '' || val === 'null' || val === null) {
                            $(elem).html("<i class='m--font-metal'>Empty</i>");
                        }
                    });
                    mApp.unblock($('#content'));
                    //---------------------------------VIEW DTR LOGS------------------------------------------------------------------------------------
                    $("#table-dtr tbody,#table-trail tbody").html("");
                    $('#search-dates .form-control,#search-dates-trail .form-control').val(tz_date(moment(), 'MMM DD, YYYY') + ' - ' + tz_date(moment(), 'MMM DD, YYYY'));
                    $('#search-dates').daterangepicker({
                        startDate: tz_date(moment()),
                        endDate: tz_date(moment()),
                        opens: 'center',
                        drops: 'down',
                        autoUpdateInput: true
                    },
                            function (start, end, label) {
                                $('#search-dates .form-control').val(tz_date(moment(start), 'MMM DD, YYYY') + ' - ' + tz_date(moment(end), 'MMM DD, YYYY'));
                                $("#dtr-perpage").change();
                            });
                    $('#search-dates-trail').daterangepicker({
                        startDate: tz_date(moment()),
                        endDate: tz_date(moment()),
                        opens: 'center',
                        drops: 'up',
                        autoUpdateInput: true
                    },
                            function (start, end, label) {
                                $('#search-dates-trail .form-control').val(tz_date(moment(start), 'MMM DD, YYYY') + ' - ' + tz_date(moment(end), 'MMM DD, YYYY'));
                                getAuditTrail();
                            });
                    //enable this to automatically display logs upon viewing single profile
                    $("#dtr-perpage").change();
                    getAuditTrail();
                    if (employee.allowUserUpdate === '1') {
                        $('#editBtn2').show();
                        setUserUpdates(employee);
                        setUserUpdatesFamily(family);
                    } else {
                        $('#editBtn2').remove();
                    }

                }

            });
        });
        $("[href='#tab-payroll']").click(function () {
            $("#tab-payroll").hide();
            restrictArea('tab-payroll');
        });
        $("#dtr-perpage").change(function () {
            var perPage = $("#dtr-perpage").val();
            getScheduleLogs(0, perPage, function (total) {
                $("#dtr-pagination").pagination('destroy');
                $("#dtr-pagination").pagination({
                    items: total, //default
                    itemsOnPage: $("#dtr-perpage").val(),
                    hrefTextPrefix: "#",
                    cssStyle: 'light-theme',
                    displayedPages: 2,
                    onPageClick: function (pagenumber) {
                        var perPage = $("#dtr-perpage").val();
                        getScheduleLogs((pagenumber * perPage) - perPage, perPage);
                    }
                });
            });
        });
        $("#tbl_applicant-inclusiveDates-2").change(function () {
            if ($(this).val() === '') {
                $(this).val("");
            }
        });
        $("#btn-requestChanges").click(function () {
            var notifStringRequired = '<small class="notif-required m--font-danger m--font-bold mt-1" >Required*</small>';
            var notifStringNumber = '<small class="notif-number m--font-danger m--font-bold mt-1" >Not a valid number*</small>';
            var isOkay = true;
            $("[id$='-2']").each(function (i, elem) {

                $(elem).siblings().remove('.notif-required');
                $(elem).siblings().remove('.notif-number');
                var val = ($(elem).val() === null) ? null : $(elem).val().trim();
                if ($(elem).data('required') === 'yes') {
                    if (val === null || val === '') {
                        $(notifStringRequired).insertAfter(elem);
                        isOkay = false;
                    } else {
                        $(elem).siblings().remove('.notif-required');
                        if ($(elem).data('type') === 'number') {
                            if (!$.isNumeric(val)) {
                                $(notifStringNumber).insertAfter(elem);
                                isOkay = false;
                            } else {
                                $(elem).siblings().remove('.notif-number');
                            }
                        }
                    }
                } else if (val !== '') {
                    if ($(elem).data('type') === 'number') {
                        if (!$.isNumeric(val)) {
                            $(notifStringNumber).insertAfter(elem);
                            isOkay = false;
                        } else {
                            $(elem).siblings().remove('.notif-number');
                        }
                    }
                }
            });
            if (isOkay === true) {
                var final_array = [];
                $("[id$='-2']").each(function (i, elem) {
                    var original_value = $(elem).data('orig_value');
                    var value = $(elem).val();
//                    if (original_value === null || original_value === undefined) {
//
//                    } else {
                    var element_name = $(elem).attr('id');
                    var temp_name = element_name.split("-");
                    var table = temp_name[0];
                    var name = temp_name[1];
                    var type = 'normal';
                    if (original_value !== value) {
                        final_array.push({tableName: table, field: name, newValue: value, type: type, originalValue: original_value, style: 'update', status: 2});
                    }
//                    }

                });

                //get Family records updates -------------------------------------------------------------------------------
                var family_array = [];
                $(".family-div").each(function (x, family) {
                    var employeeFamily_ID = $(family).data('employeefamilyid');
                    var style = $(family).data('styler');
                    var myFamilyInstance = $(family).data('familyinstance');
                    var table = 'tbl_family';
                    var fname_check = $(family).find('.customfamily[data-field="fname"]').val();
                    var lname_check = $(family).find('.customfamily[data-field="lname"]').val();
                    var relation_check = $(family).find('.customfamily[data-field="relation"]').val();
                    if (fname_check === '' && lname_check === '' && relation_check === '') {
                        if (employeeFamily_ID !== '') {
                            $(family).find('.customfamily').each(function (i, input) {
                                var field = $(input).data('field');
                                var newVal = $(input).val();
                                var original = $(input).data('originalvalue');
                                if (newVal.toString() !== original.toString()) {
                                    family_array.push({tableName: table, field: field, newValue: newVal, type: 'family', originalValue: original, style: 'delete', status: 2,employeeFamily_ID: employeeFamily_ID, familyInstance: myFamilyInstance});
                                }

                            });
                        }
                    } else {
                        $(family).find('.customfamily').each(function (i, input) {
                            var field = $(input).data('field');
                            var newVal = $(input).val();
                            var original = $(input).data('originalvalue');
                            if (newVal.toString() !== original.toString()) {
                                family_array.push({tableName: table, field: field, newValue: newVal, type: 'family', originalValue: original, style: style, status: 2,employeeFamily_ID: employeeFamily_ID, familyInstance: myFamilyInstance});
                            }
                        });
                    }
                });
                if (final_array.length === 0 && family_array.length === 0) {
                    var text = 'You are only able to update once and no changes were made.';
                    var withChange = 0;

                } else {
                    var text = 'You are only able to update once.';
                    var withChange = 1;
                }
                swal({
                    title: 'Are you sure?',
                    text: text,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>Employee/saveEmployeeRequest",
                            data: {
                                final_array: final_array,
                                family_array: family_array,
                                withChange: withChange
                            }, beforeSend: function () {
                                mApp.block($('#content'));
                            },
                            cache: false,
                            success: function (res) {
                                res = JSON.parse(res.trim());
                                if (res.status === "Success") {
                                    $("#editProfileModal").modal('hide');
                                    $.notify({
                                        message: 'Successfully submitted request to HR.'
                                    }, {
                                        type: 'success',
                                        timer: 1000
                                    });
                                    $("#editBtn2").remove();
                                } else {
                                    $("#editProfileModal").modal('hide');
                                    $.notify({
                                        message: 'An error occured while processing your request'
                                    }, {
                                        type: 'danger',
                                        timer: 1000
                                    });
                                }
                                mApp.unblock($('#content'));
                            }
                        });
                    }
                });
            }else{
                swal("Missed validation","Please review the form","error");
            }

        });
</script>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     