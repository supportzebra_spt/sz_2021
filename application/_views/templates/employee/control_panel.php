<style type="text/css">
    .bg-green{
        background: #4CAF50 !important;
    }
    .btn-green{
        background: #4CAF50;
        color:white !important;
    }
    .btn-green:hover{
        background:#2a862e !important;
    } 
    .btn-outline-green{
        color: #4CAF50 !important;
        background:white !important;
        border-color:#4CAF50 !important;
    }
    .btn-outline-green:hover{
        background: #4CAF50 !important;
        color:white !important;
        border-color:#4CAF50 !important;
    }
    .font-green{
        color: #4CAF50 !important;
    }
    .border-light-green{
        border-color: #4CAF507d !important;
    }
    .card-shadow{
        box-shadow:  0 1px 15px 1px rgba(69,65,78,.08)
    }
    #bootstrap-duallistbox-nonselected-list_from option,#bootstrap-duallistbox-selected-list_from option {
        font-size:13px !important;
    }  
    .d-xl-inline{
        margin-right:3px;
    }

    .nav.nav-pills .nav-link{
        font-size:13px !important;
    }
    .font-10{
        font-size:10px !important;
    }
    .font-11{
        font-size:11px !important;
    }
    .font-12{
        font-size:12px !important;
    }
    .font-13{
        font-size:13px !important;
    }
    .font-16{
        font-size:16px !important;
    }
    #table-approval td{
        font-size:13px;
    }
    .m-accordion__item{
        border:none;
    }
    .m-accordion__item-mode{
        margin-left:5px;
        font-size:15px !important; 
        color: white !important;
    }
    .m-accordion__item-icon i{
        font-size:18px !important;  
    }
    .m-accordion__item-title{
        font-size:15px !important;  
    }
    .m-accordion__item-head{
        background:#4CAF50 !important;  
        color:white !important;  
        font-weight:bolder;
    } 
    .m-accordion__item-head:hover{
        background: #2a862e !important;  
    }
    .accordion-approve,.accordion-reject,.accordion-approve-insertfamily{
        width: 25px !important;
        height: 25px !important;
    }
    #table-loghistory{
        font-size:12px;
    }
    #table-loghistory .name{
        font-weight:400;
        color:#36a3f7;
    } 
    #table-loghistory .value{
        font-weight:400;
        color:#f4516c; 
    }

    .daterangepicker .daterangepicker_input .input-mini {
        display:none;
    }
    .m-accordion .m-accordion__item .m-accordion__item-head{
        padding:10px 20px !Important;
    }
    .nav-pills .nav-link{
        background:#eee;
    }
    .nav-pills .nav-link.active{
        background:#555 !important;
        color:white !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="content">

    <div class="m-content">
        <div class="row">
            <div class="col-md-4">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">CONTROL PANEL: <br /><small>Employee Management System</small></h3>   
            </div>
            <div class="col-md-8 text-right" style="margin-top:13px">
                <div class="btn-group m-btn-group " role="group" aria-label="...">
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url();?>Employee/dashboard'"><span><i class="fa fa-dashboard"></i><span>Dashboard</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url();?>Employee/profiles'"><span><i class="fa fa-list-alt"></i><span>Profiles</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url();?>Employee/birthday_list'"><span><i class="fa fa-birthday-cake"></i><span>Birthdays</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url();?>Employee/anniversary_list'"><span><i class="fa fa-gift"></i><span>Anniversaries</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-green" onclick="location.href = '<?php echo base_url();?>Employee/control_panel'"><span><i class="fa fa-gear"></i><span>Control Panel</span></span></button>
                </div>
            </div>
        </div>

        <br />
        <div class="card card-shadow">
            <div class="card-body">
                <ul class="nav nav-tabs mb-0" role="tablist" >
                    <li class="nav-item">
                        <a class="nav-link  active show" data-toggle="tab" href="#tab-approval"><span class="fa fa-thumbs-o-up d-lg-none d-xl-inline font-green"></span> Request Approval</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab-userupdate"><span class="fa fa-pencil d-lg-none d-xl-inline font-green"></span> User Update </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab-featureaccess"><span class="fa fa-lock d-lg-none d-xl-inline font-green"></span> Feature Access </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab-loghistory"><span class="fa fa-list d-lg-none d-xl-inline font-green"></span> Log History</a>
                    </li>
                </ul>
                <br />
                <div class="tab-content"> 
                    <div class="tab-pane active" id="tab-approval" role="tabpanel">
                        <div class="card">
                            <div class="card-header m--font-bolder font-green font-16">
                                REQUEST APPROVALS
                            </div>
                            <div class="card-body">
                                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand  fade show mt-2" role="alert" style="display:none" id="accordion-approval-alert">
                                    <div class="m-alert__icon">
                                        <i class="flaticon-exclamation-1"></i>
                                        <span></span>
                                    </div>
                                    <div class="m-alert__text">
                                        <strong>No Record!</strong> You do not have employee profile update request.	
                                    </div>				  	
                                </div>
                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-approval" role="tablist">                      

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-userupdate" role="tabpanel">
                        <div class="card">
                            <div class="card-header m--font-bolder font-green font-16">
                                <div class="row">
                                    <div class="col-md-12">USER UPDATES</div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="m-form__group form-group">
                                            <select name="from" id="dualListBox" size="8" multiple="multiple"></select>
                                        </div>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-green" id="btn-saveEnableUserUpdate">Save Changes</button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-featureaccess" role="tabpanel"> 
                        <div class="card">
                            <div class="card-header m--font-bolder font-green font-16">
                                <div class="row">
                                    <div class="col-md-10">FEATURE ACCESS</div>
                                    <div class="col-md-2 text-md-right"><button class="btn btn-outline-green btn-sm" id="btn-newfeatureaccess" data-toggle="modal" data-target="#newFeatureAccessModal" >New Record</button></div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand  fade show mt-2" role="alert" style="display:none" id="accordion-featureaccess-alert">
                                    <div class="m-alert__icon">
                                        <i class="flaticon-exclamation-1"></i>
                                        <span></span>
                                    </div>
                                    <div class="m-alert__text">
                                        <strong>No Record!</strong> You do not have employee profile update request.	
                                    </div>				  	
                                </div>
                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-featureaccess" role="tablist">                      

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-loghistory" role="tabpanel">
                        <div class="card">
                            <div class="card-header m--font-bolder font-green font-16">
                                <div class="row">
                                    <div class="col-md-8">Tracking / Log History</div>
                                    <div class="col-md-3">
                                        <div class="m-input-icon m-input-icon--left" id="loghistory-search-dates">
                                            <input type="text" class="form-control form-control-sm m-input" placeholder="Date Range" readonly style="background: #fff;font-size:12px">
                                            <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-md-right">
                                        <button class="btn btn-warning m-btn btn-sm m-btn--icon m-btn--icon-only" onclick="getLogHistory();">
                                            <i class="fa fa-refresh"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand  fade show mt-2" role="alert" style="display:none" id="loghistory-alert">
                                    <div class="m-alert__icon">
                                        <i class="flaticon-exclamation-1"></i>
                                        <span></span>
                                    </div>
                                    <div class="m-alert__text">
                                        <strong>No Record!</strong> Change your search criteria.	
                                    </div>				  	
                                </div>
                                <table class="table m-table table-bordered table-sm" id="table-loghistory">
                                    <thead>
                                    <th>Log Message</th>
                                    <th>Date time</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="newFeatureAccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel3">Employee Feature Access</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" data-scrollbar-shown="true" data-scrollable="true" data-max-height="430">
                <div class="row">
                    <div class="col-md-12">
                        <label for=""> Page User</label><select name="" id="select-feature-users" class="m-input form-control form-control-sm"></select>
                    </div>
                </div>
                <hr class="border-light-green"/>
                <div class="row">
                    <div class="col-md-6 feature-col" data-field="basic" data-type="1"><label for=""> Basic Information</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>
                    <div class="col-md-6 feature-col" data-field="family" data-type="1"><label for=""> Family</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>
                </div>

                <hr class="border-light-green"/>
                <div class="row">
                    <div class="col-md-6 feature-col" data-field="positions" data-type="1"><label for=""> Positions</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>
                    <div class="col-md-6 feature-col" data-field="dtr" data-type="2"><label for=""> Daily Time Record</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>

                </div>
                <hr class="border-light-green"/>
                <div class="row">
                    <div class="col-md-6 feature-col" data-field="auditTrail" data-type="2"><label for=""> Audit Trail</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>
                    <div class="col-md-6 feature-col" data-field="previousAccounts" data-type="1"><label for=""> Previous Accounts</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>
                </div>
                <hr class="border-light-green"/>
                <div class="row">
                    <div class="col-md-6 feature-col" data-field="rates" data-type="2"><label for=""> Rates</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>
                    <div class="col-md-6 feature-col" data-field="payrollSettings" data-type="1"><label for=""> Payroll Settings</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>

                </div>
                <hr class="border-light-green"/>
                <div class="row">
                    <div class="col-md-6 feature-col" data-field="government" data-type="1"><label for=""> Government Payments</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>
                    <div class="col-md-6 feature-col" data-field="confidentiality" data-type="1"><label for=""> Confidentiality</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>
                </div>
                <hr class="border-light-green"/>
                <div class="row">
                    <div class="col-md-6 feature-col" data-field="deactivation" data-type="3"><label for=""> Deactivation</label><select name="" class="m-input form-control form-control-sm feature-select"></select></div>

                </div>

            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-green" id="btn-createUserAccess">Create New Record</button>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/bootstrap-duallistbox.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.bootstrap-duallistbox.min.js" ></script>
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">
                                        function tz_date(thedate, format = null) {
                                            var tz = moment.tz(new Date(thedate), "Asia/Manila");
                                            return (format === null) ? moment(tz) : moment(tz).format(format);
                                        }
                                        function getLogHistory() {
                                            var datestart = $("#loghistory-search-dates").data('daterangepicker').startDate;
                                            var dateend = $("#loghistory-search-dates").data('daterangepicker').endDate;
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url(); ?>Employee/getLogHistory",
                                                data: {
                                                    datestart: tz_date(moment(datestart), 'YYYY-MM-DD HH:mm:ss'),
                                                    dateend: tz_date(moment(dateend), 'YYYY-MM-DD HH:mm:ss')
                                                },
                                                beforeSend: function () {
                                                    mApp.block('#table-loghistory', {
                                                        overlayColor: '#000000',
                                                        type: 'loader',
                                                        state: 'brand',
                                                        size: 'lg'
                                                    });
                                                },
                                                cache: false,
                                                success: function (res) {
                                                    res = JSON.parse(res.trim());
                                                    $("#table-loghistory tbody").html("");
                                                    var string = '';
                                                    $.each(res.history, function (i, hist) {
                                                        string += '<tr>'
                                                                + '<td>' + hist.text + '</td>'
                                                                + '<td>' + moment(hist.createdOn).format('MMM DD, YYYY hh:mm:ss A') + '</td>'
                                                                + '</tr>';
                                                    });
                                                    if (string === '') {
                                                        $("#loghistory-alert").show();
                                                        $("#table-loghistory").hide();
                                                    } else {
                                                        $("#loghistory-alert").hide();
                                                        $("#table-loghistory").show();
                                                    }
                                                    $("#table-loghistory tbody").html(string);
                                                    mApp.unblock('#table-loghistory');
                                                }, error: function () {
                                                    swal("Error", "Please contact admin", "error");
                                                    mApp.unblock('#table-loghistory');
                                                }
                                            });
                                        }
                                        $(function () {
                                            var dualListBox = $('#dualListBox').bootstrapDualListbox({
                                                nonSelectedListLabel: '<span class="m--font-bolder m--font-danger">Disabled User:</span>',
                                                selectedListLabel: '<span class="m--font-bolder font-green">Enabled User:</span>',
                                                selectorMinimalHeight: 150
                                            });
                                            var dualListContainer = dualListBox.bootstrapDualListbox('getContainer');
                                            dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
                                            dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
                                            dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
                                            dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');
                                            $('[href="#tab-approval"]').on('click', function (e) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/getEmployeeRequest",
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        $("#div-accordion-approval").html("");
                                                        var str = "";
                                                        $.each(res.requests, function (key, obj) {

                                                            str += '<div class="m-accordion__item">';
                                                            str += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-' + obj.emp_id + '" aria-expanded="  false">';
                                                            str += '<span class="m-accordion__item-icon"><i class="fa fa-user-circle"></i></span>';
                                                            str += '<span class="m-accordion__item-title">' + obj.lname + ', ' + obj.fname + '</span>';
                                                            str += '<span class="m-accordion__item-mode"></span>';
                                                            str += '</div>';
                                                            str += '<div class="m-accordion__item-body collapse" id="accordion-item-' + obj.emp_id + '" role="tabpanel" data-parent="#div-accordion-approval"> ';
                                                            str += '<div class="m-accordion__item-content">';

                                                            if (obj.normal !== undefined) {
                                                                str += '<div class="main-div-normal"><h5>Normal Updates</h5><hr class="border-light-green" style="border-width:3px"/><div style="overflow:auto;max-height:300px;" class="mb-4">';
                                                                str += '<table class="table m-table table-sm">';
                                                                str += '<thead style="background:#444;color:white"><th class="text-center">Date Requested</th><th>Field</th><th>Original Value</th><th>New Value</th><th class="text-center">Actions</th></thead><tbody>';
                                                                $.each(obj.normal, function (i, item) {
                                                                    var orig = (item.originalValue === '') ? '<i style="color:#aaa"><small>Empty</small></i>' : item.originalValue;
                                                                    var newValue = (item.newValue === '') ? '<i style="color:#aaa"><small>Empty</small></i>' : item.newValue;
                                                                    str += '<tr class="font-12">';
                                                                    str += '<td class="text-center">' + moment(obj.requestDate).format('lll') + '</td>';
                                                                    str += '<td class="text-uppercase m--font-bold">' + item.field + '</td>';
                                                                    str += '<td>' + orig + '</td>';
                                                                    str += '<td>' + newValue + '</td>';
                                                                    str += '<td class="text-center"><button class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only accordion-approve" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-check"></i></button> <button class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only accordion-reject" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-times"></i></button></td>';
                                                                    str += '</tr>';
                                                                });
                                                                str += '</tbody></table></div></div>';
                                                            }

                                                            if (obj.family !== undefined) {
                                                                str += '<div class="main-div-family"><h5>Family Changes</h5><hr class="border-light-green" style="border-width:3px"/><div style="overflow:auto;max-height:300px;">';
                                                                str += '<table class="table m-table table-sm">';
                                                                str += '<thead style="background:#444;color:white"><th class="text-center">Date Requested</th><th class="text-center">Type</th><th>Field</th><th>Original Value</th><th>New Value</th><th class="text-center">Actions</th></thead><tbody>';

                                                                $.each(obj.family, function (x, arraycount) {
                                                                    var counter = 1;
                                                                    $.each(arraycount, function (i, item) {
                                                                        var orig = (item.originalValue === '') ? '<i style="color:#aaa"><small>Empty</small></i>' : item.originalValue;
                                                                        var newValue = (item.newValue === '') ? '<i style="color:#aaa"><small>Empty</small></i>' : item.newValue;
                                                                        var background = (parseInt(item.familyInstance) % 2 === 0) ? "#fff" : "#eee";
                                                                        var styleColor = (item.style === 'update') ? 'm-badge--warning' : (item.style === 'delete') ? 'm-badge--danger' : 'm-badge--success';
                                                                        str += '<tr class="font-12" style="background:' + background + ' !important" data-familyinstance="' + item.familyInstance + '">';
                                                                        str += '<td class="text-center">' + moment(obj.requestDate).format('lll') + '</td>';
                                                                        str += '<td class="text-center"><span class="m-badge ' + styleColor + ' m-badge--wide m-badge--rounded font-10 m--font-bold">' + item.style + '</span></td>';
                                                                        str += '<td class="text-uppercase m--font-bold">' + item.field + '</td>';
                                                                        str += '<td>' + orig + '</td>';
                                                                        str += '<td>' + newValue + '</td>';
                                                                        if (item.style === 'delete' && counter === 1) {
                                                                            str += '<td class="text-center"><button class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only accordion-approve" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-check"></i></button> <button class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only accordion-reject" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-times"></i></button></td>';
                                                                        } else if (item.style === 'delete' && counter !== 1) {
                                                                            str += '<td></td>';
                                                                        } else if (item.style === 'insert' && counter === 1) {
                                                                            str += '<td class="text-center"><button class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only accordion-approve-insertfamily" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-check"></i></button> <button class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only accordion-reject" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-times"></i></button></td>';
                                                                        } else if (item.style === 'insert' && counter !== 1) {
                                                                            str += '<td><button class="accordion-approve-insertfamily-temp m--hide" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"></button></td>';
                                                                        } else {
                                                                            str += '<td class="text-center"><button class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only accordion-approve" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-check"></i></button> <button class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only accordion-reject" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-times"></i></button></td>';
                                                                        }
                                                                        counter++;
                                                                        str += '</tr>';
                                                                    });
                                                                });
                                                                str += '</tbody></table></div></div>';
                                                            }

                                                            str += '</div></div></div>';
                                                        });
                                                        if (str === "") {
                                                            $("#accordion-approval-alert").show();
                                                        } else {
                                                            $("#accordion-approval-alert").hide();
                                                        }
                                                        $("#div-accordion-approval").html(str);
                                                    }
                                                });
                                            });

                                            $("#tab-approval").on('click', '.accordion-approve', function () {
                                                var that = this;
                                                var employeeRequest_ID = $(that).data('emprequestid');
                                                var employeeRequestDetails_ID = $(that).data('emprequestdetailsid');
                                                var style = $(that).data('style');
                                                var type = $(that).data('type');
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/approveEmployeeRequest",
                                                    data: {
                                                        employeeRequest_ID: employeeRequest_ID,
                                                        employeeRequestDetails_ID: employeeRequestDetails_ID,
                                                        style: style,
                                                        type: type
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        if (res.status === "Success") {
                                                            $.notify({
                                                                message: 'Successfully approved employee request.'
                                                            }, {
                                                                type: 'success',
                                                                timer: 1000
                                                            });
                                                            var divstatus = res.remainingrequest;
                                                            if (divstatus === 'empty') {
                                                                $(that).closest('.m-accordion__item').remove();
                                                            } else if (divstatus === 'withoutnormal') {
                                                                $(that).closest('.main-div-normal').remove();
                                                            } else if (divstatus === 'withoutfamily') {
                                                                $(that).closest('.main-div-family').remove();
                                                            }

                                                            $(that).closest('tr').remove();
                                                            if ($("#div-accordion-approval").html() === '') {
                                                                $("#accordion-approval-alert").show();
                                                            } else {
                                                                $("#accordion-approval-alert").hide();
                                                            }
                                                        } else {
                                                            $.notify({
                                                                message: 'An error occured while processing your approval'
                                                            }, {
                                                                type: 'danger',
                                                                timer: 1000
                                                            });
                                                        }
                                                    }

                                                });
                                            });
                                            $("#tab-approval").on('click', '.accordion-reject', function () {
                                                var that = this;
                                                var employeeRequest_ID = $(that).data('emprequestid');
                                                var employeeRequestDetails_ID = $(that).data('emprequestdetailsid');
                                                var style = $(that).data('style');
                                                var type = $(that).data('type');
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/rejectEmployeeRequest",
                                                    data: {
                                                        employeeRequest_ID: employeeRequest_ID,
                                                        employeeRequestDetails_ID: employeeRequestDetails_ID,
                                                        style: style,
                                                        type: type
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        if (res.status === "Success") {
                                                            $.notify({
                                                                message: 'Successfully rejected employee request.'
                                                            }, {
                                                                type: 'success',
                                                                timer: 1000
                                                            });
                                                            var divstatus = res.remainingrequest;
                                                            if (divstatus === 'empty') {
                                                                $(that).closest('.m-accordion__item').remove();
                                                            } else if (divstatus === 'withoutnormal') {
                                                                $(that).closest('.main-div-normal').remove();
                                                            } else if (divstatus === 'withoutfamily') {
                                                                $(that).closest('.main-div-family').remove();
                                                            }

                                                            $(that).closest('tr').remove();
                                                            if ($("#div-accordion-approval").html() === '') {
                                                                $("#accordion-approval-alert").show();
                                                            } else {
                                                                $("#accordion-approval-alert").hide();
                                                            }
                                                        } else {
                                                            $.notify({
                                                                message: 'An error occured while processing your approval'
                                                            }, {
                                                                type: 'danger',
                                                                timer: 1000
                                                            });
                                                        }
                                                    }

                                                });
                                            });
                                            $("#tab-approval").on('click', '.accordion-approve-insertfamily', function () {
                                                var that = this;
                                                var employeeRequest_ID = $(that).data('emprequestid');
                                                var employeeRequestDetails_ID = $(that).data('emprequestdetailsid');
                                                var tr = $(that).closest('tr');
                                                var family_instance = tr.data('familyinstance');
                                                var employeeRequestDetails_IDs = [employeeRequestDetails_ID];
                                                $.each(tr.siblings(), function (i, item) {
                                                    var fam_instance = $(item).data('familyinstance');
                                                    if (fam_instance === family_instance) {
                                                        employeeRequestDetails_IDs.push($(item).find('.accordion-approve-insertfamily-temp').data('emprequestdetailsid'));
                                                    }
                                                });
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/approveEmployeeRequest_familyInsert",
                                                    data: {
                                                        employeeRequestDetails_IDs: employeeRequestDetails_IDs,
                                                        employeeRequest_ID: employeeRequest_ID
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        if (res.status === "Success") {
                                                            $.notify({
                                                                message: 'Successfully approved employee request.'
                                                            }, {
                                                                type: 'success',
                                                                timer: 1000
                                                            });
                                                            var divstatus = res.remainingrequest;
                                                            if (divstatus === 'empty') {
                                                                $(that).closest('.m-accordion__item').remove();
                                                            } else if (divstatus === 'withoutnormal') {
                                                                $(that).closest('.main-div-normal').remove();
                                                            } else if (divstatus === 'withoutfamily') {
                                                                $(that).closest('.main-div-family').remove();
                                                            }
                                                            tr.remove();
                                                            $.each(tr.siblings(), function (i, item) {
                                                                var fam_instance = $(item).data('familyinstance');
                                                                if (fam_instance === family_instance) {
                                                                    $(item).remove();
                                                                }
                                                            });
                                                            if ($("#div-accordion-approval").html() === '') {
                                                                $("#accordion-approval-alert").show();
                                                            } else {
                                                                $("#accordion-approval-alert").hide();
                                                            }
                                                        } else {
                                                            $.notify({
                                                                message: 'An error occured while processing your approval'
                                                            }, {
                                                                type: 'danger',
                                                                timer: 1000
                                                            });
                                                        }
                                                    }

                                                });
                                            });
                                            $('[href="#tab-userupdate"]').on('click', function (e) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/get_all_active_employees",
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        $("#dualListBox").html("");
                                                        var str = "";
                                                        $.each(res.activeEmployees, function (key, obj) {
                                                            if (obj.allowUserUpdate === '1') {
                                                                str += '<option value="' + obj.emp_id + '" selected>' + obj.lname + ' ' + obj.fname + '</option>';
                                                            } else {
                                                                str += '<option value="' + obj.emp_id + '">' + obj.lname + ' ' + obj.fname + '</option>';
                                                            }

                                                        });
                                                        $("#dualListBox").html(str);
                                                        $('#dualListBox').bootstrapDualListbox('refresh');
                                                    }
                                                });
                                            });
                                            $("#btn-saveEnableUserUpdate").click(function () {
                                                var users = $("#dualListBox").val();
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/setEnableUserUpdate",
                                                    data: {
                                                        users: users
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        $('#enableUserUpdateModal').modal('hide');
                                                        if (res.status === 'Success') {
                                                            swal("Good job!", "Successfully saved changes!", "success");
                                                        } else {
                                                            swal("Failed!", "Something happened while processing your request!", "error");
                                                        }
                                                    }

                                                });
                                            });
                                            $('[href="#tab-featureaccess"]').on('click', function (e) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/getEmployeeUserAccess",
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        var noRecords = res.norecords;
                                                        if (noRecords.length === 0) {
                                                            $("#btn-newfeatureaccess").hide();
                                                        } else {
                                                            $("#btn-newfeatureaccess").show();
                                                        }
                                                        $("#div-accordion-featureaccess").html("");
                                                        var str = "";

                                                        $.each(res.users, function (key, obj) {
                                                            var userAccess = obj.userAccess;
                                                            var select = '<select name="" class="m-input form-control form-control-sm feature-select" data-empid="' + obj.emp_id + '" data-empuseraccessid="' + userAccess.employeeUserAccess_ID + '">'
                                                                    + '<option value="hide" style="color:gray">Hide</option>'
                                                                    + '<option value="view" style="color:#60A6E0">View Only</option>'
                                                                    + '<option value="update" style="color:#4CAF50">Allow Updates</option></select>';
                                                            var select2 = '<select name="" class="m-input form-control form-control-sm feature-select" data-empid="' + obj.emp_id + '" data-empuseraccessid="' + userAccess.employeeUserAccess_ID + '">'
                                                                    + '<option value="hide" style="color:gray">Hide</option>'
                                                                    + '<option value="view" style="color:#60A6E0">View Only</option></select>';
                                                            var select3 = '<select name="" class="m-input form-control form-control-sm feature-select" data-empid="' + obj.emp_id + '" data-empuseraccessid="' + userAccess.employeeUserAccess_ID + '">'
                                                                    + '<option value="hide" style="color:gray">Hide</option>'
                                                                    + '<option value="update" style="color:#4CAF50">Allow Updates</option></select>';

                                                            str += '<div class="m-accordion__item">';
                                                            str += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-feature-' + obj.emp_id + '" aria-expanded="  false">';
                                                            str += '<span class="m-accordion__item-icon"><i class="fa fa-user-circle"></i></span>';
                                                            str += '<span class="m-accordion__item-title">' + obj.lname + ', ' + obj.fname + '</span>';
                                                            str += '<span class="m-accordion__item-mode"></span>';
                                                            str += '</div>';
                                                            str += '<div class="m-accordion__item-body collapse" id="accordion-item-feature-' + obj.emp_id + '" role="tabpanel" data-parent="#div-accordion-featureaccess"> ';
                                                            str += '<div class="m-accordion__item-content">';
                                                            str += '<div class="row">'
                                                                    + '<div class="col-md-3 feature-col" data-field="basic" data-value="' + userAccess.basic + '"><label for=""> Basic Information</label>' + select + '</div>'
                                                                    + '<div class="col-md-3 feature-col" data-field="family" data-value="' + userAccess.family + '"><label for=""> Family</label>' + select + '</div>'
                                                                    + '<div class="col-md-3 feature-col" data-field="positions" data-value="' + userAccess.positions + '"><label for=""> Positions</label>' + select + '</div>'
                                                                    + '<div class="col-md-3 feature-col" data-field="dtr" data-value="' + userAccess.dtr + '"><label for=""> Daily Time Record</label>' + select2 + '</div>'
                                                                    + '</div><hr class="border-light-green"/>';
                                                            str += '<div class="row">'
                                                                    + '<div class="col-md-3 feature-col" data-field="auditTrail" data-value="' + userAccess.auditTrail + '"><label for=""> Audit Trail</label>' + select2 + '</div>'
                                                                    + '<div class="col-md-3 feature-col" data-field="previousAccounts" data-value="' + userAccess.previousAccounts + '"><label for=""> Previous Accounts</label>' + select + '</div>'
                                                                    + '<div class="col-md-3 feature-col" data-field="rates" data-value="' + userAccess.rates + '"><label for=""> Rates</label>' + select2 + '</div>'
                                                                    + '<div class="col-md-3 feature-col" data-field="payrollSettings" data-value="' + userAccess.payrollSettings + '"><label for=""> Payroll Settings</label>' + select + '</div>'
                                                                    + '</div><hr class="border-light-green"/>';
                                                            str += '<div class="row">'
                                                                    + '<div class="col-md-3 feature-col" data-field="government" data-value="' + userAccess.government + '"><label for=""> Government Payments</label>' + select + '</div>'
                                                                    + '<div class="col-md-3 feature-col" data-field="confidentiality" data-value="' + userAccess.confidentiality + '"><label for=""> Confidentiality</label>' + select + '</div>'
                                                                    + '<div class="col-md-3 feature-col" data-field="deactivation" data-value="' + userAccess.deactivation + '"><label for=""> Deactivation</label>' + select3 + '</div>'
                                                                    + '</div>';

                                                            str += '</div></div></div>';
                                                        });
                                                        if (str === "") {
                                                            $("#accordion-featureaccess-alert").show();
                                                        } else {
                                                            $("#accordion-featureaccess-alert").hide();
                                                        }
                                                        $("#div-accordion-featureaccess").html(str);
                                                        $("#div-accordion-featureaccess").find('.feature-select').each(function (i, select) {
                                                            var value = $(select).closest('.feature-col').data('value');
                                                            $(select).val(value);
                                                            var color = $(select).find('option:selected').css('color');
                                                            $(select).css('color', color);
                                                        });

                                                        //-------------------------------NORECORDS-------------------------------------------------
                                                        var userOptions = "";
                                                        $("#select-feature-users").html("");
                                                        $.each(noRecords, function (i, item) {
                                                            userOptions += '<option value="' + item.emp_id + '">' + item.lname + ', ' + item.fname + '</option>';
                                                        });
                                                        $("#select-feature-users").html(userOptions);
                                                    }
                                                });
                                            });
                                            $("#div-accordion-featureaccess").on('change', '.feature-select', function () {
                                                var that = this;
                                                var value = $(that).val();
                                                var employeeUserAccess_ID = $(that).data('empuseraccessid');
                                                var emp_id = $(that).data('empid');
                                                var col = $(that).closest('.feature-col');
                                                var field = col.data('field');
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/updateEmployeeUserAccess",
                                                    data: {
                                                        value: value,
                                                        field: field,
                                                        employeeUserAccess_ID: employeeUserAccess_ID,
                                                        emp_id: emp_id
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        if (res.status === 'Success') {
                                                            $.notify({
                                                                message: 'Successfully changed user access'
                                                            }, {
                                                                type: 'success',
                                                                timer: 1000
                                                            });
                                                            var color = $(that).find('option:selected').css('color');
                                                            $(that).css('color', color);
                                                        } else {
                                                            $.notify({
                                                                message: 'An error occured while processing your approval'
                                                            }, {
                                                                type: 'danger',
                                                                timer: 1000
                                                            });
                                                        }
                                                    }

                                                });
                                            });
                                            $("#newFeatureAccessModal").on('show.bs.modal', function () {
                                                var select = '<option value="hide" style="color:gray" selected>Hide</option>'
                                                        + '<option value="view" style="color:#60A6E0">View Only</option>'
                                                        + '<option value="update" style="color:#4CAF50">Allow Updates</option>';
                                                var select2 = '<option value="hide" style="color:gray" selected>Hide</option>'
                                                        + '<option value="view" style="color:#60A6E0">View Only</option>';
                                                var select3 = '<option value="hide" style="color:gray" selected>Hide</option>'
                                                        + '<option value="update" style="color:#4CAF50">Allow Updates</option>';
                                                $(this).find('.feature-select').each(function (i, selectform) {
                                                    var type = $(selectform).closest('.feature-col').data('type');
                                                    if (type === 1) {
                                                        $(selectform).html(select);
                                                    } else if (type === 2) {

                                                        $(selectform).html(select2);
                                                    } else {
                                                        $(selectform).html(select3);
                                                    }
                                                    var color = $(selectform).find('option:selected').css('color');
                                                    $(selectform).css('color', color);
                                                });

                                            });
                                            $("#newFeatureAccessModal").on('change', '.feature-select', function () {
                                                var color = $(this).find('option:selected').css('color');
                                                $(this).css('color', color);
                                            });

                                            $("#btn-createUserAccess").click(function () {
                                                var emp_id = $("#select-feature-users").val();
                                                if(emp_id===null||emp_id===''){
                                                    swal("User Missing","No user is selected",'warning');
                                                }else{
                                                   var vals = {};
                                                vals['emp_id'] = emp_id;
                                                $("#newFeatureAccessModal").find('.feature-select').each(function (i, selectform) {
                                                    var field = $(selectform).closest('.feature-col').data('field');
                                                    var value = $(selectform).val();
                                                    vals[field] = value;
                                                });

                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/insertEmployeeUserAccess",
                                                    data: {
                                                        values: vals
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        if (res.status === 'Success') {
                                                            $.notify({
                                                                message: 'Successfully added a user access'
                                                            }, {
                                                                type: 'success',
                                                                timer: 1000
                                                            });
                                                            $('[href="#tab-approval"]').click();
                                                            $('[href="#tab-featureaccess"]').click();
                                                        } else {
                                                            $.notify({
                                                                message: 'An error occured while processing your actions'
                                                            }, {
                                                                type: 'danger',
                                                                timer: 1000
                                                            });
                                                        }
                                                        $("#newFeatureAccessModal").modal('hide');
                                                    }

                                                }); 
                                                }
                                                
                                            });


                                            $('[href="#tab-loghistory"]').on('click', function (e) {
                                                $('#loghistory-search-dates .form-control').val(tz_date(moment().subtract(5, 'days'), 'MMM DD, YYYY') + ' - ' + tz_date(moment().add(1, 'days'), 'MMM DD, YYYY'));
                                                $('#loghistory-search-dates').daterangepicker({
                                                    startDate: tz_date(moment().subtract(5, 'days').format('YYYY-MM-DD')),
                                                    endDate: tz_date(moment().add(1, 'days').format('YYYY-MM-DD')),
                                                    opens: 'left',
                                                    drops: 'down',
                                                    maxDate: moment().add(1, 'days'),
                                                    autoUpdateInput: true
                                                },
                                                        function (start, end, label) {
                                                            $('#loghistory-search-dates .form-control').val(tz_date(moment(start), 'MMM DD, YYYY') + ' - ' + tz_date(moment(end), 'MMM DD, YYYY'));
                                                            getLogHistory();
                                                        });
                                                getLogHistory();
                                            });

                                            $('[href="#tab-approval"]').click();

                                        });

</script>   
