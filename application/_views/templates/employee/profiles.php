<style type="text/css">
    a.disable_user_access{
        pointer-events: none;
        cursor: default;
        color: #333;
    }
    .btn-outline-green{
        color: #4CAF50 !important;
        background:white !important;
        border-color:#4CAF50 !important;
    }
    .btn-outline-green:hover{
        background: #4CAF50 !important;
        color:white !important;
        border-color:#4CAF50 !important;
    }
    .bg-green{
        background: #4CAF50 !important;
    }
    .btn-green{
        background: #4CAF50;
        color:white !important;
    }
    .btn-green:hover{
        background:#2a862e !important;
    }
    .font-green{
        color: #4CAF50 !important;
    }
    .font-white{
        color:white !Important;
    }
    .border-light-green{
        border-color: #4CAF507d !important;
    }
    .border-green{
        border-color: #4CAF50 !important;
    }
    .body{
        padding-right:0 !important;
    }
    .swal2-title{
        font-family:'Poppins' !important;
    }
    .font-poppins{
        font-family:'Poppins' !important;
    }
    .m-widget4 .m-widget4__item {
        padding:0 !important;
    }
    td.disabled{
        background:#eee !Important;
    }
    .two-line-ellipsis{
        display: inline-block;
        border: 0px;
        height: 18px;
        overflow: hidden;
        color:#777;
    }
    .one-line-ellipsis{
        line-height: 8px;
        height: 16px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        /*width: 80%;*/
        text-align:center !important;
    }
    .scaler:hover
    {
        box-shadow: 2px 2px 7px #777;
        z-index: 2;
        -webkit-transition: all 100ms ease-in;
        -webkit-transform: scale(1.01);
        -ms-transition: all 100ms ease-in;
        -ms-transform: scale(1.01);   
        -moz-transition: all 100ms ease-in;
        -moz-transform: scale(1.01);
        transition: all 100ms ease-in;
        transform: scale(1.01);
        cursor: pointer;
    }
    .m-content .m-stack__item{
        padding:0 !important;
        background:0 !important;
        border:0 !important;
    }

    .card-shadow{
        box-shadow: 0 1px 8px rgba(0,0,0,0.15);
    }
    .text-line-height-1{
        line-height:1;
    }
    .font-10{
        font-size:10px !Important;
    } 
    .font-11{
        font-size:11px !Important;
    } 
    .font-12{
        font-size:12px !Important;
    } 
    .font-13{
        font-size:13px !Important;
    }
    .font-14{
        font-size:14px !Important;
    }
    .font-15{
        font-size:15px !Important;
    }
    .font-16{
        font-size:16px !Important;
    }
    #profiles-directory p{
        line-height:1.3 !Important;
    }
    .fa{
        text-align:center !important;
    }
    .m-quick-sidebar .m-quick-sidebar__close:hover {
        color: #4CAF50 !important;
    }
    .nav.nav-pills .nav-link{
        padding:6px 5px !important;
        color:#aaa;
        border-bottom:1px solid #444;
        border-radius:0;
    }
    .nav-pills .nav-link.active{
        background-color: #4CAF50  !important;
    }
    .row-value > div:first-child:not(.static){
        font-size:11px;
        font-weight:bolder;
    }

    .row-value > div:nth-child(2):not(.static){
        font-size:11px;
        color:#1D98F6;
        font-weight:600;
        text-decoration: underline dotted #1D98F6;
    }
    .row-value > .static{
        font-size:11px;
        color:#333;
        font-weight:600;  
        text-decoration: none !important;
    }
    .the-legend {
        border-style: none;
        border-width: 0;
        font-size: 12px;
        line-height: 20px;
        margin-bottom: 0;
        width: auto;
        padding: 0 15px;
        border:1px solid #4CAF507d;
        background:white;
        color: #4CAF50  !important;
        font-weight:600;
    }
    .the-fieldset {
        margin-bottom:15px;
        border:1px solid  #4CAF507d;
        padding: 10px 15px;
    }
    .form-control{
        font-size:12px;
    }
    .popover{
        width:100% !important;
    }
    .popover select option {
        text-transform:capitalize;
    }
    a[id*='tbl_applicant'],a[id*='tbl_employee']{
        text-transform: capitalize;
    }
    .notcapital{
        text-transform: none !Important;
    }


    label.cabinet{
        display: block;
        cursor: pointer;
    }

    label.cabinet input.file{
        position: relative;
        height: 100%;
        width: auto;
        opacity: 0;
        -moz-opacity: 0;
        filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
        margin-top:-30px;
    }

    #upload-demo{
        width: 100%;
        height: 100%;
    }
    li span.current{
        background:#333 !important;
    }
    figure figcaption {
        position: absolute;
        bottom: 0px;
        color: #fff;
        width: 100%;
        text-shadow: 0 0 10px #000;
        /*background: rgb(0,0,0,0.5);*/
        background:#333;
        margin:5px 0px 0px -10px;
        text-align:center;
        padding: 5px 0;
    }
    @media  (max-width:1220px) and (min-width:932px) {
        figure img{
            width:80% !important;
            height:80% !important;
        }
    }
    .daterangepicker td, .daterangepicker th{
        width:30px !important;
        height:30px !important;
        font-size:13px !important;
    }

    #table-dtr tbody tr td{
        vertical-align: middle !important;
        text-align:center !Important;
        font-size:12px;
    }
    .m-quick-sidebar .mCSB_scrollTools {
        right:0 !Important;
    }

    .select2-selection__choice{
        font-size:12px !important;
        font-weight:400 !Important;
    }
    .select2-container,.m-select2,.select2-search__field,.select2-search{
        width:100% !important;
    }
    .select2-results__option{
        font-size:12px !important;
    }
    .m-popover.m-popover--skin-dark.popover{
        background:#333 !important;
    }
    .form-control[readonly]{
        background-color:#e9ecef !important;
    }   
    .d-xl-inline{
        margin-right:3px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper"  id="content">

    <div class="m-content">
        <div id="main-directory">
            <div class="row">
                <div class="col-md-4">
                    <h3 class="m-subheader__title m-subheader__title--separator font-poppins">PROFILES: <br /><small>Employee Management System</small></h3>   
                </div>
                <div class="col-md-8 text-right" style="margin-top:13px">
                    <div class="btn-group m-btn-group " role="group" aria-label="...">
                        <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url();?>Employee/dashboard'"><span><i class="fa fa-dashboard"></i><span>Dashboard</span></span></button>
                        <button type="button" class="m-btn btn m-btn m-btn--icon btn-green" onclick="location.href = '<?php echo base_url();?>Employee/profiles'"><span><i class="fa fa-list-alt"></i><span>Profiles</span></span></button>
                        <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url();?>Employee/birthday_list'"><span><i class="fa fa-birthday-cake"></i><span>Birthdays</span></span></button>
                        <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url();?>Employee/anniversary_list'"><span><i class="fa fa-gift"></i><span>Anniversaries</span></span></button>
                        <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url();?>Employee/control_panel'"><span><i class="fa fa-gear"></i><span>Control Panel</span></span></button>
                    </div>
                </div>
            </div>

            <br />
            <div class="card card-shadow">
                <div class="card-footer px-2 py-0" style="background:#333">
                    <div class="row no-gutters" id="filters">
                        <div class="col-md-2 col-sm-6 col-12 py-2 px-1">
                            <select name="" class="m-input form-control font-12 form-control-sm" id="search-class">
                                <option value="">All Class</option>
                                <option value="Agent">Ambassador</option>
                                <option value="Admin">SZ Team</option>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12 py-2 px-1">
                            <select name="" class="m-input form-control font-12 form-control-sm" id="search-accounts">
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12 py-2 px-1">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" class="form-control m-input font-12 form-control-sm" placeholder="Search Name..." id="search-name"/>
                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-search"></i></span></span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12 py-2 px-1">
                            <select name="" class="m-input form-control font-12 form-control-sm" id="search-active">
                                <option value="">All</option>
                                <option value="yes" selected>Active</option>
                                <option value="No">Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="card card-shadow">
                <div class="card-footer" style="padding: 15px 20px 0px 20px;background:white">
                    <div class="row"  id="card-container"></div>
                </div>
            </div>
            <br />
            <div class="card card-shadow px-2 ">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7 col-sm-12 p-2">
                            <ul id="pagination">
                            </ul>
                        </div>
                        <div class="col-md-2 col-sm-4 p-2">
                            <select class="form-control m-input form-control-sm m-input--air" id="perpage">
                                <option>12</option>
                                <option>30</option>
                                <option>60</option>
                                <option>100</option>
                                <option>250</option>
                                <option>500</option>
                                <option>1000</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-8 p-2 text-right">
                            Showing <span id="count"></span> of <span id="total"></span> records
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="single-profile" style="display:none">

            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="card card-shadow ">
                        <div class="card-body p-0" style="background:#333;">
                            <div class="row no-gutters">
                                <div class="col-12 col-sm-6  col-md-12">
                                    <div class="p-2">
                                        <label class="cabinet center-block mb-0">
                                            <figure style="margin:0 !important;text-align:center">
                                                <img src="" class="gambar img-responsiv " id="item-img-output" style="background:white;width:100%!important;object-fit:cover;"/>
                                            </figure>
                                            <input type="file" class="item-img file center-block m--hide" name="file_photo"/>
                                        </label> 
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-12">
                                    <ul class="nav nav-pills m-2" role="tablist">
                                        <li class="nav-item ml-0" style="width:100%">
                                            <a class="nav-link  active show" data-toggle="tab" href="#tab-personal"><span class="la la-user d-lg-none d-xl-inline"></span> Personal  <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                        </li>
                                        <li class="nav-item ml-0" style="width:100%">
                                            <a class="nav-link" data-toggle="tab" href="#tab-emergency"><span class="la la-ambulance d-lg-none d-xl-inline"></span> Emergency <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                        </li>
                                        <li class="nav-item ml-0" style="width:100%">
                                            <a class="nav-link" data-toggle="tab" href="#tab-family"><span class="la la-group d-lg-none d-xl-inline"></span> Family <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                        </li>
                                        <li class="nav-item ml-0" style="width:100%">
                                            <a class="nav-link" data-toggle="tab" href="#tab-application"><span class="la la-clipboard d-lg-none d-xl-inline"></span> Application <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                        </li>
                                        <li class="nav-item ml-0" style="width:100%">
                                            <a class="nav-link" data-toggle="tab" href="#tab-work"><span class="la la-sitemap d-lg-none d-xl-inline"></span> Work <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                        </li>
                                        <li class="nav-item ml-0" style="width:100%">
                                            <a class="nav-link" data-toggle="tab" href="#tab-logs"><span class="la la-list d-lg-none d-xl-inline"></span> Logs <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                        </li>
                                        <li class="nav-item ml-0" style="width:100%">
                                            <a class="nav-link" data-toggle="tab" href="#tab-payroll"><span class="la la-money d-lg-none d-xl-inline"></span> Compensation <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                        </li>
                                        <li class="nav-item ml-0" style="width:100%">
                                            <a class="nav-link" data-toggle="tab" href="#tab-others"><span class="la la-gear d-lg-none d-xl-inline"></span> Others <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="col-lg-9  col-md-8">
                    <div class="row">

                        <div class="col-lg-9 col-md-8">
                            <p style="font-size:28px;color: #2a862e;"><span  id="image-caption-fname" class="m--font-boldest text-capitalize"></span> <span  id="image-caption-lname" class="m--font-boldest text-capitalize"></span> <span id="image-caption-active"></span></p>
                        </div>
                        <div class="col-lg-3 col-md-4 text-md-right">
                            <button class="btn btn-outline-metal m-btn m-btn--icon" id="btn-backtodirectory" style="color:#333">
                                <span>
                                    <i class="fa fa-chevron-left"></i>
                                    <span>Employee List</span>
                                </span>
                            </button> 
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active show" id="tab-personal" role="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">BASIC INFORMATION</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">FULL NAME</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Last Name:</div>
                                                    <div class="col-8"><a href="#"  id="tbl_applicant-lname" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-lg-2 py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">First Name:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-fname" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Middle Name:</div>
                                                    <div class="col-8"><a href="#"  id="tbl_applicant-mname" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Extension:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-nameExt" data-type='select'  data-required="no"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Nickname:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-nickName" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">BIRTHDAY</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Birthdate:</div>
                                                    <div class="col-8"><a href="#"  id="tbl_applicant-birthday" data-type='date' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Age:</div>
                                                    <div class="col-8  static" id="personal-age"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Birthplace:</div>
                                                    <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#"  id="tbl_applicant-birthplace" data-type='textarea' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">OTHER BASIC INFO</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Religion:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-religion" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Blood Type:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-bloodType" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Sex:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-gender" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Civil Status:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-civilStatus" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <br />
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">ADDRESS AND CONTACT INFORMATION</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">ADDRESSES</legend>

                                        <div class="row ">
                                            <div class="col-12">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Present:</div>
                                                    <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_applicant-presentAddress" data-type='textarea' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Permanent:</div>
                                                    <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_applicant-permanentAddress" data-type='textarea' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">CONTACT INFORMATION</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Telephone:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-tel" data-type="number"  data-required="no"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Mobile:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-cell" data-type="number"  data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Email:</div>
                                                    <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_applicant-email" class="notcapital" data-type='email' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <br />
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">EDUCATION DETAILS</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">EDUCATION</legend>
                                        <div class="row ">
                                            <div class="col-12">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Educational Attainment:</div>
                                                    <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_employee-educationalAttainment" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">High School Attended:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-highSchool" data-type='textarea' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Year Graduated:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-highSchoolYear" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Last School Attended:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-lastSchool" data-type='textarea' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Year Left:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-lastSchoolYear" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Course:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-course" data-type='textarea' data-required="no"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-emergency" role="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">EMERGENCY DETAILS</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">EMERGENCY</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Last Name:</div>
                                                    <div class="col-8"><a href="#"   id="tbl_employee-emergencyLname" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">First Name:</div>
                                                    <div class="col-8"><a href="#"  id="tbl_employee-emergencyFname" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Middle Name:</div>
                                                    <div class="col-8"><a href="#"   id="tbl_employee-emergencyMname" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Extension:</div>
                                                    <div class="col-8"><a href="#"  id="tbl_employee-emergencyNameExt" data-type='select'  data-required="no"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Contact:</div>
                                                    <div class="col-8"><a href="#"  id="tbl_employee-emergencyContact"data-required="yes" data-type="number"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Relationship:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-emergencyRelationship" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Address:</div>
                                                    <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#"  id="tbl_employee-emergencyAddress" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-family" role="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                    <div class="row">
                                        <div class="col-md-6 mt-1"> FAMILY INFORMATION</div>
                                        <div class="col-md-6">
                                            <button class="btn btn-green m-btn m-btn--icon btn-sm pull-right" data-toggle="modal" data-target="#addFamilyModal">
                                                <span>
                                                    <i class="fa flaticon-user-add"></i>
                                                    <span>Add Family</span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_family" id="fieldset-spouse">
                                        <legend class="the-legend  m--font-bold m--font-brand">SPOUSE</legend>
                                        <div id="fieldset-spouse-div"></div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_family"  id="fieldset-children">
                                        <legend class="the-legend  m--font-bold m--font-brand">CHILDREN</legend>
                                        <div id="fieldset-children-div"></div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_family"  id="fieldset-parents">
                                        <legend class="the-legend  m--font-bold m--font-brand">PARENTS</legend>
                                        <div id="fieldset-parents-div"></div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-application" role="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">APPLICATION DETAILS</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">EMPLOYMENT HISTORY</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">CC Experience:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-ccaExp" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Latest Employer:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-latestEmployer" data-type='textarea' data-required="no"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Last Position Held:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-lastPositionHeld" data-required="no"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Inclusive Dates:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-inclusiveDates" data-type='daterange' data-required="no"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Contact Number:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-lastEmployerContact" data-type="number" data-required="no"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Address:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-lastEmployerAddress" data-type='textarea' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <br />
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                    <div class="row">
                                        <div class="col-md-6 mt-1"> PREVIOUS ACCOUNTS</div>
                                        <div class="col-md-6">
                                            <button class="btn btn-green m-btn m-btn--icon btn-sm pull-right" data-toggle="modal" data-target="#addPrevAccountsModal">
                                                <span>
                                                    <i class="fa flaticon-user-add"></i>
                                                    <span>Add Account</span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_previousAccounts" id="fieldset-prev-accounts">
                                        <legend class="the-legend  m--font-bold m--font-brand">PREVIOUS ACCOUNTS</legend>
                                        <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="previous_alert" style="display:none">
                                            <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                            <div class="m-alert__text"><strong>No Record Found! </strong> <br>Please adjust your search criteria and try again.
                                            </div>
                                        </div>
                                        <table class="table m-table table-bordered table-hover table-sm" id="table-previousAccounts">
                                            <thead>
                                            <th>Name</th>
                                            <th>Work Duration</th>
                                            <th></th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-work" rol e="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">USER DETAILS</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">ACCOUNT DETAILS</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Username:</div>
                                                    <div class="col-8"><a href="#" id="tbl_user-username" class="notcapital" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Password:</div>
                                                    <div class="col-8"><a href="#" id="tbl_user-password" data-type='password'  class="notcapital hide-text" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Department:</div>
                                                    <div class="col-8 static" id="work-department"></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Account:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-acc_id" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">ID Number:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-id_num" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Role:</div>
                                                    <div class="col-8"><a href="#" id="tbl_user-role_id" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">SUPERVISOR</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Name:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-Supervisor"  data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Email:</div>
                                                    <div class="col-8 static"  id="work-supervisorEmail">jeffrey@supportzebra.com</div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">GOVERNMENT NUMBERS</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">SSS:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-sss" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Philhealth:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-philhealth" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Pag-ibig:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-pagibig" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">TIN/BIR:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-bir" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                </div>
                            </div>
                            <br />
                            <div class="card card-shadow">

                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">POSITIONS</div>
                                        <div class="col-md-6">
                                            <button class="btn btn-green m-btn m-btn--icon btn-sm pull-right" id="btn-changePositionModal">
                                                <span>
                                                    <i class="fa fa-briefcase"></i>
                                                    <span>Change Position</span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer p-3">

                                    <fieldset class="the-fieldset bg-white for_user_access_positions">
                                        <legend class="the-legend  m--font-bold m--font-brand">CURRENT</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Code:</div>
                                                    <div class="col-8 static" id='work-positioncode'> </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Position:</div>
                                                    <div class="col-8 static" id='work-positionname'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Status:</div>
                                                    <div class="col-8 static" id='work-status'></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Start Date:</div>
                                                    <div class="col-8 static" id='work-dateFrom'></div>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_positions">
                                        <legend class="the-legend  m--font-bold m--font-brand">HISTORY</legend>
                                        <div id='positionhistory-fieldset'></div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-payroll" role="tabpanel">
                            <div class="card card-shadow">

                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">COMPENSATION INFORMATION</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_rates ">
                                        <legend class="the-legend  m--font-bold m--font-brand">RATES</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Monthly:</div>
                                                    <div class="col-8 static"  id='payroll-rate-monthly'> </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Semi-Monthly:</div>
                                                    <div class="col-8 static" id='payroll-rate-semi'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Daily:</div>
                                                    <div class="col-8 static" id='payroll-rate-daily'></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Hourly:</div>
                                                    <div class="col-8 static" id='payroll-rate-hourly'></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_payrollSettings">
                                        <legend class="the-legend  m--font-bold m--font-brand">PAYROLL</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Salary Mode:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-salarymode" data-type='select' data-required="yes"></a> </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Work Days per Week:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-workDaysPerWeek" data-type='select' data-required="yes"></a> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">ATM MODE:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-isAtm" data-type='select' data-required="yes">ATM</a> </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">ATM Account Number:</div>
                                                    <div class="col-8"><a href="#" id="tbl_employee-atm_account_number" data-type="number" data-required="no"></a> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_government">
                                        <legend class="the-legend  m--font-bold m--font-brand">SEMI-MONTHLY GOVERNMENT PAYMENTS</legend>
                                        <div class="row ">

                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">BIR Tax:</div>
                                                    <div class="col-8 static"  id='payroll-tax'></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">SSS:</div>
                                                    <div class="col-8 static" id='payroll-sss'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Philhealth:</div>
                                                    <div class="col-8 static" id='payroll-philhealth'></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Pag-ibig Basic:</div>
                                                    <div class="col-8 static" id='payroll-pagibig'></div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Pag-ibig Additional: &#8369;</div>
                                                    <div class="col-8"><a href="#" id="tbl_pagibig_add_contribution-amount" data-required="yes"></a> </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6 m--hide">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">BIR Category:</div>
                                                    <div class="col-8"><a href="#" id="tbl_tax_emp-tax_desc_id" data-type='select' data-required="yes">S/ME</a> </div>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-logs" role="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">DAILY TIME RECORD</div>
                                <div class="card-footer p-3">
                                    <div class="row mb-2">
                                        <div class="col-md-6 col-sm-8 col-xs-12">
                                            <div class="m-input-icon m-input-icon--left" id="search-dates">
                                                <input type="text" class="form-control m-input" placeholder="Date Range" readonly style="background: #fff;">
                                                <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <fieldset class="the-fieldset bg-white for_user_access_dtr">
                                        <legend class="the-legend  m--font-bold m--font-brand">DTR LOGS</legend>
                                        <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="date-alert" style="display:none">
                                            <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                            <div class="m-alert__text"><strong>No Record Found! </strong> <br>Please adjust your search criteria and try again.
                                            </div>
                                        </div>
                                        <table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">
                                            <thead style="background: #555;color: white;">
                                            <th class="text-center">Schedule Date</th>
                                            <th class="text-center" colspan="2">Shift</th>
                                            <th class="text-center">Clock In</th>
                                            <th class="text-center">Clock Out</th>
                                            <th class="text-center">Breaks</th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">
                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle" >
                                                <ul id="dtr-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:12%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="dtr-perpage">
                                                    <option>1</option>
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="dtr-count"></span> of <span id="dtr-total"></span> records</div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <br />
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">AUDIT TRAIL RECORDS</div>
                                <div class="card-footer p-3">
                                    <div class="row mb-2">
                                        <div class="col-md-6 col-sm-8 col-xs-12">
                                            <div class="m-input-icon m-input-icon--left" id="search-dates-trail">
                                                <input type="text" class="form-control m-input" placeholder="Date Range" readonly style="background: #fff;">
                                                <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <fieldset class="the-fieldset bg-white for_user_access_auditTrail">
                                        <legend class="the-legend  m--font-bold m--font-brand">AUDIT TRAIL</legend>

                                        <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="date-alert-trail" style="display:none">
                                            <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                            <div class="m-alert__text"><strong>No Record Found! </strong> <br>Please adjust your search criteria and try again.
                                            </div>
                                        </div>
                                        <div style="overflow-y:scroll;max-height:250px;">
                                            <table class="table m-table table-hover table-sm table-bordered" id="table-trail" style="font-size:13px" >
                                                <thead style="background: #555;color: white;">
                                                <th>Remarks</th>
                                                <th>Logs</th>
                                                <th>IP Address</th>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>

                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-others" role="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">SETTINGS</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_confidentiality">
                                        <legend class="the-legend  m--font-bold m--font-brand">CONFIDENTIALITY</legend>
                                        <div class="row ">
                                            <div class="col-12">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Confidential Employee:</div>
                                                    <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href=""   id='tbl_employee-confidential' data-type="select" data-required="yes"></a> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <br />
                                    <fieldset class="the-fieldset bg-white for_user_access_deactivation">
                                        <legend class="the-legend  m--font-bold m--font-brand">ACCOUNT DEACTIVATION</legend>
                                        <button href="#" class="btn btn-danger m-btn m-btn--icon"  data-toggle="modal" data-target="#deactivateModal">
                                            <span>
                                                <i class="fa fa-user-times"></i>
                                                <span>Deactivate Employee</span>
                                            </span>
                                        </button>
                                    </fieldset>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Change Profile Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-center">
                <div id="upload-demo"></div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-outline-warning m-btn m-btn--icon croppie-rotate" data-deg="-90"><i class="fa fa-rotate-right"></i> Right</button>
                <button class="btn btn-outline-warning m-btn m-btn--icon croppie-rotate" data-deg="90"><i class="fa fa-rotate-left"></i> Left</button>
                <button type="button" id="cropImageBtn" class="btn btn-green"><i class="fa fa-upload"></i> Crop and Upload</button>
            </div>
        </div>
    </div>
</div>	
<div class="modal fade" id="changePositionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">Change Position</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="alert alert-danger m-alert--outline mb-4" role="alert" id="changePosition-alert">
                    <strong>No Rate Set!</strong> Assign rate first before changing position.
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <label for="" class="m--font-bolder">Department: </label>
                        <select name="" class="m-input form-control font-12" id="changepos-departments">
                        </select>
                    </div>
                    <div class="col-md-5">
                        <label for="" class="m--font-bolder">Start Date: </label>
                        <input type="text" class="form-control m-input" readonly="" id="changepos-dateFrom">
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-md-12">
                        <label for="" class="m--font-bolder">Position: </label>
                        <select name="" class="m-input form-control font-12" id="changepos-positions">
                        </select>
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-md-12">
                        <label for="" class="m--font-bolder">Status: </label>
                        <select name="" class="m-input form-control font-12" id="changepos-status">
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-green" id="btn-saveChangePosition" disabled>Save Changes</button>
            </div>
        </div>
    </div>
</div>	
<div class="modal fade" id="addFamilyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel3">Add Family Member</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger m-alert--outline mb-4" role="alert" id="addFamily-alert">
                    <strong>Incomplete Fields!</strong> Fill all the input boxes to continue.
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="" class="m--font-bolder">Last Name: </label>
                        <input type="text" name="" class="m-input form-control font-12" id="addfamily-lname">
                    </div>
                    <div class="col-md-6">
                        <label for="" class="m--font-bolder">First Name: </label>
                        <input type="text" name="" class="m-input form-control font-12" id="addfamily-fname">
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-md-6">
                        <label for="" class="m--font-bolder">Middle Name: </label>
                        <input type="text" name="" class="m-input form-control font-12" id="addfamily-mname">
                    </div>
                    <div class="col-md-6">
                        <label for="" class="m--font-bolder">Relation: </label>
                        <select name="" class="m-input form-control font-12" id="addfamily-relation">
                            <option value="">--</option>
                            <option value="Father">Father</option>
                            <option value="Mother">Mother</option>
                            <option value="Husband">Husband</option>
                            <option value="Wife">Wife</option>
                            <option value="Son">Son</option>
                            <option value="Daughter">Daughter</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-green" id="btn-saveAddFamily">Save Changes</button>
            </div>
        </div>
    </div>
</div>	
<div class="modal fade" id="addPrevAccountsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel3">Add Previous Accounts</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger m-alert--outline mb-4" role="alert" id="addPrevAccounts-alert1" style="display:none">
                    <strong>No Search Name!</strong> Fill the input box to search employee.
                </div>
                <div class="alert alert-danger m-alert--outline mb-4" role="alert" id="addPrevAccounts-alert2" style="display:none">
                    <strong>No Result!</strong> We cannot find the employee you are searching for.
                </div>
                <label for="">Search for inactive employee accounts:</label>
                <input type="text" id="prev-searchname" class="form-control m-input" placeholder="Search for names..."/>
                <br />
                <div class="m-scrollable" data-scrollable="true" data-scrollbar-shown="true" data-max-height="250" data-mobile-max-height="200">
                    <table class="table m-table table-bordered table-hover table-sm" id="table-inactivelist">
                        <thead>
                        <th>Name</th>
                        <th>Work Duration</th>
                        <th>Action</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>	
<div class="modal fade" id="deactivateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel3">Deactivate Employee</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger m-alert--outline mb-4" role="alert" id="deactivate-alert" style="display:none">
                    <strong>Incomplete Fields!</strong> Fill all the input boxes to continue.
                </div>
                <div class="row">
                    <div class="col-lg-6" style="border-right:1px solid #eee">
                        <table class="table table-bordered ">
                            <tr>
                                <td class="font-12">Name:</td>
                                <td><span class="font-12 m--font-bolder" id="deactivate-name" >Nicca Gerodico Arique</span></td>
                            </tr>
                            <tr>
                                <td class="font-12">Interview Date:</td>
                                <td> <input type="text" class="form-control m-input form-control-sm" readonly="" id="deactivate-finalInterviewDate"></td>
                            </tr>
                            <tr>
                                <td class="font-12">Company ID No:</td>
                                <td><span class="font-12 m--font-bolder" id="deactivate-id_num" >123123</span></td>
                            </tr>
                            <tr>
                                <td class="font-12">Eligible for rehire:</td>
                                <td>
                                    <select name="" class="m-input form-control font-12  form-control-sm" id="deactivate-isRehire">
                                        <option value="">--</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-12">Team:</td>
                                <td><span class="font-12 m--font-bolder" id="deactivate-team" >Software Programming Team</span></td>
                            </tr>
                            <tr>
                                <td class="font-12">Supervisor:</td>
                                <td><span class="font-12 m--font-bolder" id="deactivate-supervisor" >Mark Jeffrey Magparoc</span></td>
                            </tr>
                            <tr>
                                <td class="font-12">Hire Date:</td>
                                <td><span class="font-12 m--font-bolder" id="deactivate-hireDate" >April 03, 2017</span></td>
                            </tr>
                            <tr>
                                <td class="font-12">Separation Date:</td>
                                <td> <input type="text" class="form-control m-input form-control-sm" readonly="" id="deactivate-separationDate"></td>
                            </tr>
                            <tr>
                                <td class="font-12">Starting Position:</td>
                                <td><span class="font-12 m--font-bolder" id="deactivate-startposition" >IT Staff / Programmer</span></td>
                            </tr>
                            <tr>
                                <td class="font-12">Ending Position:</td>
                                <td><span class="font-12 m--font-bolder" id="deactivate-endposition" >Junior Software Developer</span></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <label for="" class='font-12' style='margin-top:13px'>Reasons for Leaving:</label>
                        <select class="form-control m-input form-control-sm" id='deactivate-reasonForLeaving-select'>
                            <option value="">--</option>
                            <optgroup label="Unwanted">
                                <option value="Another employer">Another employer</option>
                                <option value="Personal reasons">Personal reasons</option>
                                <option value="Retirement">Retirement</option>
                                <option value="Relocation">Relocation</option>
                                <option value="Return to school">Return to school</option>
                            </optgroup>
                            <optgroup label="Wanted">
                                <option value="Attendance Issue">Attendance Issue</option>
                                <option value="Violation of Company Policy/ies">Violation of Company Policy/ies</option>
                                <option value="Performance Issue">Performance Issue</option>
                                <option value="Client's decision/Account removal">Client's decision/Account removal</option>
                                <option value="Termination of account">Termination of account</option>
                            </optgroup>
                        </select>
                        <br />
                        <div class="row">
                            <div class="col-md-4">
                                <label for="" class='font-12'>Other Specify:</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control m-input form-control-sm"  id='deactivate-reasonForLeaving-input'>
                            </div>
                        </div>
                        <br />
                        <label for="" class='font-12'>Employee Comments:</label>
                        <textarea name="" id="deactivate-employeeComments" cols="30" rows="4" class='form-control m-input' ></textarea>
                        <br />

                        <label for="" class='font-12'>Interviewer Comments:</label>
                        <textarea name="" id="deactivate-interviewerComments" cols="30" rows="4" class='form-control m-input'></textarea>
                        <hr />
                        <button type="button" class="btn btn-danger pull-right" id='btn-saveDeactivate'>Proceed with deactivation</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>	

<!-- begin::Quick Sidebar -->
<button id="m_quick_sidebar_toggle" class="m--hide"></button>
<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--skin-light p-0" style="overflow:hidden">
    <div class="m-quick-sidebar__content">
        <span id="m_quick_sidebar_close" class="m-quick-sidebar__close" style="z-index:2 !important;color:#666;margin: 15px;"><i class="fa fa-times" style="font-size:40px;"></i></span>
        <div id="profiles-directory"  >
            <div class="row">
                <div class="col-md-12">
                    <div style="background:#333;border-bottom:8px solid #4CAF50;padding:20px" >
                        <div class="m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:35%">
                                <img id="profile-single-image" data-src="" style="border-radius:50%;height:6rem;width:6rem !important;object-fit:cover;border:3px solid white;background:white" alt="NO IMAGE">
                            </div>
                            <div class="m-stack__item m-stack__item--left m-stack__item--middle p-0" style="width:65%">
                                <p id="profile-single-name" class="font-white m--font-bolder mb-0 text-capitalize" style="font-size:24px"></p>
                            </div>
                        </div>
                    </div>
                    <div class="px-5 py-2" class="m-scrollable" data-scrollable="true" data-max-height="540" data-scrollbar-shown="true">
                        <div class="card border border-0" style="background:#fff;">
                            <div class="card-body">
                                <p class="text-uppercase  m--font-bold font-13"><i class="fa fa-briefcase" style="width:20px;"></i> <span id="profile-single-position"></span> <span id="profile-single-position"></span> <small class="text-capitalize"  id="profile-single-status"></small></p>
                                <p class="text-capitalize font-13"><i class="fa fa-sitemap" style="width:20px;"></i>  <span id="profile-single-account"></span></p>
                                <p class="text-capitalize font-13 mb-0"><i class="fa fa-user-circle-o" style="width:20px;"></i> <span class="m--font-brand m--font-bold" id="profile-single-supervisor"></span></p>
                            </div>
                        </div>
                        <hr class="mt-0 border-light-green"/>
                        <div class="card border border-0 " style="background:#fff;">
                            <div class="card-body  py-0">
                                <p class="text-capitalize font-12"><i class="fa fa-map-marker" style="width:20px;font-size:18px"></i> <span id="profile-single-address" ></span> </p>
                                <p class="font-13"><i class="fa fa-phone-square" style="width:20px;font-size:15px"></i> <span class=" m--font-brand m--font-bold" id="profile-single-number"></span></p>
                                <p class="font-13" style="word-wrap: break-word;"><i class="fa fa-envelope" style="width:20px;font-size:13px;"></i> <span id="profile-single-email"></span></p>
                            </div>
                        </div>
                        <hr class="mt-0 border-light-green"/>
                        <div class="card border border-0" style="background:#f4f4f4;">
                            <div class="card-body font-12">
                                <p class="font-13 m--font-bolder"> <i class="fa fa-medkit" style="width:20px;"></i> Emergency Contact</p>
                                <p class="font-12"><i class="fa fa-circle font-10" style="width:20px;"></i> Name: <span class="m--font-danger m--font-bold font-12" id="profile-single-emerg-name"></span></p>
                                <p class="font-12 mb-0 "><i class="fa fa-circle font-10" style="width:20px;"></i> Contact Number: <span class="m--font-danger m--font-bold font-12"  id="profile-single-emerg-number"></span></p>
                            </div>
                        </div>
                        <hr class=" border-light-green"/>

                        <a href="#" class="m-link m-link--state font-green m--font-bolder text-center ml-2" id="btn-full-quicksidebar"><i class="fa fa-arrow-circle-right" style="font-size:20px"></i> View <span id="profile-single-firstname" class="text-capitalize"></span> Full Profile</a>

                        <div id="quickbar-references-link">

                        </div>
                        <div class="m--hide">
                            <hr class="border-light-green"/>
                            <p>QUICK LINKS:</p>
                            <a href="#" class="m-link m-link--state m-link--info m--font-bold font-12" id="btn-view-dtrCredentials" > DTR Credentials</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end::Quick Sidebar -->	
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/lazyload.min.js" ></script>
<link href="<?php echo base_url(); ?>assets/src/custom/css/croppie-2.6.2.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/croppie-2.6.2.js" ></script>
<script type="text/javascript">
                                function tz_date(thedate, format = null) {
                                    var tz = moment.tz(new Date(thedate), "Asia/Manila");
                                    return (format === null) ? moment(tz) : moment(tz).format(format);
                                }

                                function getInactiveEmployees(callback) {

                                    var searchname = $('#prev-searchname').val().trim();
                                    if (searchname.length === 0) {
                                        $("#addPrevAccounts-alert1").show();
                                    } else {
                                        $("#addPrevAccounts-alert1").hide();
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url(); ?>Employee/getInactiveEmployees",
                                            data: {
                                                searchname: searchname
                                            },
                                            beforeSend: function () {
                                                mApp.block('#addPrevAccountsModal', {
                                                    overlayColor: '#000000',
                                                    type: 'loader',
                                                    state: 'brand',
                                                    size: 'lg'
                                                });
                                            },

                                            cache: false,
                                            success: function (result) {
                                                result = JSON.parse(result.trim());
                                                if (result.length === '') {
                                                    $("#addPrevAccounts-alert2").show();
                                                } else {
                                                    $("#addPrevAccounts-alert2").hide();
                                                }
                                                mApp.unblock('#addPrevAccountsModal');
                                                callback(result);
                                            }
                                        });
                                    }

                                }
                                function setPrevAccounts() {
                                    var user_access_previousAccounts = $("#single-profile").data('previousAccounts');//hide,update,view
                                    var emp_id = $('#m_quick_sidebar').data('emp_id');
                                    if (user_access_previousAccounts === 'hide') {
                                        $('[data-target="#addPrevAccountsModal"]').hide();
                                        $(".for_user_access_previousAccounts").children().not('legend').hide();
                                        $(".for_user_access_previousAccounts").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                    } else {
                                        $(".for_user_access_previousAccounts").children().not('.alert').show();
                                        if (user_access_previousAccounts === 'update') {
                                            $('[data-target="#addPrevAccountsModal"]').show();
                                        } else {
                                            $('[data-target="#addPrevAccountsModal"]').hide();
                                        }
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url(); ?>Employee/setPrevAccounts",
                                            data: {
                                                emp_id: emp_id
                                            },
                                            cache: false,
                                            success: function (result) {
                                                result = JSON.parse(result.trim());
                                                $("#table-previousAccounts tbody").html('');
                                                var str = "";
                                                $.each(result.employees, function (iterator, emp) {
                                                    var separationDate = moment((emp.separationDate === 'null') ? 'Unknown' : emp.separationDate).format('MMM DD, YYYY');
                                                    var dateFrom = moment((emp.dateFrom === 'null') ? 'Unknown' : emp.dateFrom).format('MMM DD, YYYY');
                                                    var separationDateString = (separationDate === 'Invalid date') ? 'Unknown' : separationDate;
                                                    var dateFromString = (dateFrom === 'Invalid date') ? 'Unknown' : dateFrom;
                                                    str += '<tr>'
                                                            + '<td style="vertical-align:middle" class="font-12">' + emp.lname + ', ' + emp.fname + '</td>'
                                                            + '<td style="vertical-align:middle"  class="font-12">' + separationDateString + ' - ' + dateFromString + '</td>';
                                                    if (user_access_previousAccounts === 'update') {
                                                        str += '<td><button class="btn btn-danger m-btn m-btn--icon m-btn--icon-only btn-sm btn-deletePrevAccounts" data-empid="' + emp.emp_id + '"><i class="la la-trash"></i></button></td>';
                                                    } else {
                                                        str += '<td></td>';
                                                    }

                                                    str += '</tr>';
                                                });

                                                if (str === '') {
                                                    $("#previous_alert").show();
                                                    $("#table-previousAccounts").hide();
                                                } else {
                                                    $("#table-previousAccounts tbody").html(str);
                                                    $("#previous_alert").hide();
                                                    $("#table-previousAccounts").show();
                                                }
                                            }
                                        });

                                    }


                                }
                                function get_all_active_employees(callback) {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Employee/get_all_active_employees",
                                        cache: false,
                                        success: function (result) {
                                            result = JSON.parse(result.trim());
                                            callback(result);
                                        }
                                    });
                                }
                                ;
                                function get_all_accounts(callback) {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Employee/get_all_accounts",
                                        cache: false,
                                        success: function (result) {
                                            result = JSON.parse(result.trim());
                                            callback(result);
                                        }
                                    });
                                }
                                function get_all_departments(callback) {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Employee/get_all_departments",
                                        cache: false,
                                        success: function (result) {
                                            result = JSON.parse(result.trim());
                                            callback(result);
                                        }
                                    });
                                }
                                function get_all_user_roles(callback) {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Employee/get_all_user_roles",
                                        cache: false,
                                        success: function (result) {
                                            result = JSON.parse(result.trim());
                                            callback(result);
                                        }
                                    });
                                }
                                function get_all_tax_descriptions(callback) {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Employee/get_all_tax_descriptions",
                                        cache: false,
                                        success: function (result) {
                                            result = JSON.parse(result.trim());
                                            callback(result);
                                        }
                                    });
                                }
                                function heightChanger() {
                                    var height = 0;
                                    var $cards = $("#card-container").find(".inner-card");
                                    $($cards).each(function () {
                                        var tempheight = $(this).outerHeight(true);
                                        if (height < tempheight) {
                                            height = tempheight;
                                        }
                                    });
                                    $($cards).each(function () {
                                        $(this).outerHeight(height);
                                    });
                                }

                                var setFamily = function (family, isActive) {
                                    var user_access_family = $("#single-profile").data('family');//hide,update,view
                                    //FAMILY-----------------------------------------------------------------------------------
                                    var select_extension = [
                                        {value: 'jr', text: 'Jr'},
                                        {value: 'sr', text: 'Sr'},
                                        {value: 'i', text: 'I'},
                                        {value: 'ii', text: 'II'}
                                    ];

                                    var select_relation = [
                                        {value: 'Father', text: 'Father'},
                                        {value: 'Mother', text: 'Mother'},
                                        {value: 'Wife', text: 'Wife'},
                                        {value: 'Husband', text: 'Husband'},
                                        {value: 'Son', text: 'Son'},
                                        {value: 'Daughter', text: 'Daughter'}
                                    ];
                                    $("#fieldset-children,#fieldset-spouse,#fieldset-parents").hide();
                                    $("#fieldset-spouse-div").html("");
                                    $("#fieldset-children-div").html("");
                                    $("#fieldset-parents-div").html("");
                                    $.each(family, function (x, fam) {
                                        if (fam.relation === 'Son' || fam.relation === 'Daughter') {
                                            var str = '<div class="row ">'
                                                    + '<div class="col-sm-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-sm-4">Last Name:</div>'
                                                    + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-lname-' + fam.employeeFamily_ID + '" data-required="yes">' + fam.lname + '</a></div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="col-sm-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-sm-4">First Name:</div>'
                                                    + '<div class="col-sm-8"><a href="#" id="tbl_employee_family-fname-' + fam.employeeFamily_ID + '" data-required="yes">' + fam.fname + '</a></div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="row ">'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-4">Middle Name:</div>'
                                                    + '<div class="col-8"><a href="#" id="tbl_employee_family-mname-' + fam.employeeFamily_ID + '" data-required="yes">' + fam.mname + '</a></div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-4">Age:</div>'
                                                    + '<div class="col-8"><a href="#" id="tbl_employee_family-age-' + fam.employeeFamily_ID + '" data-required="yes">' + fam.age + '</a></div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="row ">'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-4">Relation:</div>'
                                                    + '<div class="col-8"><a href="#" id="tbl_employee_family-relation-' + fam.employeeFamily_ID + '" data-type="select" data-required="yes">' + fam.relation + '</a></div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-4">School Attended:</div>'
                                                    + '<div class="col-8"><a href="#" id="tbl_employee_family-schoolAttended-' + fam.employeeFamily_ID + '" data-type="textarea" data-required="yes">' + fam.schoolAttended + '</a></div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div><hr />';
                                            $("#fieldset-children-div").append(str);
                                            $("#fieldset-children").show();
                                        } else {
                                            var str = '<div class="row ">'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">';
                                            if (fam.relation === 'Mother') {
                                                str += '<div class="col-4">' + fam.relation + '\'s Maiden Name:</div>';
                                            } else {
                                                str += '<div class="col-4">' + fam.relation + '\'s Last Name:</div>';
                                            }
                                            str += '<div class="col-8"> <a href = "#" id="tbl_employee_family-lname-' + fam.employeeFamily_ID + '" data-required="yes">' + fam.lname + '</a></div > '
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-4">First Name:</div>'
                                                    + '<div class="col-8"> <a href = "#" id="tbl_employee_family-fname-' + fam.employeeFamily_ID + '" data-required="yes">' + fam.fname + '</a></div > '
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="row ">'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-4">Extension:</div>'
                                                    + '<div class="col-8"><a href="#" id="tbl_employee_family-extension-' + fam.employeeFamily_ID + '" data-type="select" data-required="yes">' + fam.extension + '</a></div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-4">Middle Name:</div>'
                                                    + '<div class="col-8"> <a href = "#" id="tbl_employee_family-mname-' + fam.employeeFamily_ID + '" data-required="yes">' + fam.mname + '</a></div >'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="row ">'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-4">Contact Number:</div>'
                                                    + '<div class="col-8"><a href="#"  id="tbl_employee_family-contactNumber-' + fam.employeeFamily_ID + '" data-required="yes">' + fam.contactNumber + '</a></div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                    + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                    + '<div class="col-4">Relation:</div>'
                                                    + '<div class="col-8"><a href="#" id="tbl_employee_family-relation-' + fam.employeeFamily_ID + '" data-type="select" data-required="yes">' + fam.relation + '</a></div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>';
                                            if (fam.relation === 'Mother' || fam.relation === 'Father') {
                                                str += '<div class="row ">'
                                                        + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                        + '<div class="row  row-value py-2 py-md-2 py-lg-2">'
                                                        + '<div class="col-4">Address:</div>'
                                                        + '<div class="col-8"><a href="#"  id="tbl_employee_family-address-' + fam.employeeFamily_ID + '"  data-type="textarea" data-required="yes">' + fam.address + '</a></div>'
                                                        + '</div>'
                                                        + '</div>'
                                                        + '</div><hr />';
                                                $("#fieldset-parents-div").append(str);
                                                $("#fieldset-parents").show();
                                            } else {
                                                str += '<hr />';
                                                $("#fieldset-spouse-div").append(str);
                                                $("#fieldset-spouse").show();
                                            }
                                        }
                                    });
                                    $('#fieldset-children-div').children('hr').last().remove();
                                    $('#fieldset-parents-div').children('hr').last().remove();
                                    $('#fieldset-spouse-div').children('hr').last().remove();
                                    var elements_ids2 = [];
                                    $("#fieldset-spouse,#fieldset-children,#fieldset-parents").find('a').each(function (x, elem) {
                                        var val = $(elem).html();
                                        if (val === '' || val === null || val === 'null') {
                                            $(elem).data('isempty', true);
                                        }
                                        elements_ids2.push("#" + $(elem).attr('id'));
                                    });

                                    if (isActive === 'yes') {
                                        $("[id^='tbl_employee_family-relation']").data('data', select_relation);
                                        $("[id^='tbl_employee_family-extension']").data('data', select_extension);

                                        $.each(elements_ids2, function (x, elem) {
                                            $(elem).css({"color": "", "font-style": ""});
                                            $(elem).parent().css({"text-decoration": ""});
                                        });
                                        customPopover(elements_ids2);
                                    } else {
                                        $.each(elements_ids2, function (x, elem) {
                                            $(elem).attr('style', 'color:#333 !Important;font-style:none !Important');
                                            $(elem).parent().attr('style', 'text-decoration:none !Important');
                                        });
                                    }

                                    //END OF FAMILY----------------------------------------------------------------------------

                                    if (user_access_family === 'hide') {
                                        $('[data-target="#addFamilyModal"]').hide();
                                        $(".for_user_access_family").children().not('legend').hide();
                                        $(".for_user_access_family").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                    } else {
                                        $(".for_user_access_family").children().not('.alert').show();
                                        if (user_access_family === 'update') {
                                            $('[data-target="#addFamilyModal"]').show();
                                            $(".for_user_access_family").find('a').each(function (i, item) {
                                                $(item).parent().removeClass('static');
                                                $(item).removeClass('disable_user_access');
                                            });
                                        } else {
                                            $('[data-target="#addFamilyModal"]').hide();
                                            $(".for_user_access_family").find('a').each(function (i, item) {
                                                $(item).parent().addClass('static');
                                                $(item).addClass('disable_user_access');
                                            });
                                        }
                                    }
                                };
                                var setPositions = function () {
                                    var user_access_positions = $("#single-profile").data('positions');//hide,update,view
                                    var emp_id = $('#m_quick_sidebar').data('emp_id');
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Employee/employeePositionDetails",
                                        data: {
                                            emp_id: emp_id
                                        },
                                        cache: false,
                                        success: function (res) {
                                            res = JSON.parse(res.trim());
                                            var position = res.position;
                                            var positionHistory = res.positionHistory;
                                            var payroll = res.payroll;
                                            var employee = res.employee;
                                            if (position !== null && payroll !== null) {

                                                changeRates(employee.salarymode, position.rate, employee.workDaysPerWeek, payroll);
                                            }

                                            if (user_access_positions === 'hide') {
                                                $("#btn-changePositionModal").hide();
                                                $(".for_user_access_positions").children().not('legend').hide();
                                                $(".for_user_access_positions").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');

                                            } else {
                                                if (user_access_positions === 'update') {
                                                    $("#btn-changePositionModal").show();
                                                } else {
                                                    $("#btn-changePositionModal").hide();
                                                }
                                                $(".for_user_access_positions").children().not('.alert').show();

                                                //POSITION CURRENT--------------------------------------------------
                                                if (position !== null) {
                                                    $("#work-department").html(position.dep_name + " - " + position.dep_details);
                                                    $("#work-positioncode").html(position.pos_name);
                                                    $("#work-positionname").html(position.pos_details);
                                                    $("#work-status").html(position.status);
                                                    $("#work-dateFrom").html(moment(position.dateFrom).format('MMM DD, YYYY'));
                                                    $("#changePositionModal").data('datefrom', position.dateFrom);
                                                } else {
                                                    $("#work-department").html('');
                                                    $("#work-positioncode").html('');
                                                    $("#work-positionname").html('');
                                                    $("#work-status").html('');
                                                    $("#work-dateFrom").html('');
                                                }
                                                //POSITION HISTORY--------------------------------------------------
                                                var posHist_string = "";
                                                var historycount = positionHistory.length;
                                                if (positionHistory !== null) {
                                                    $.each(positionHistory, function (phkey, position) {
                                                        posHist_string += '<div class="row ">'
                                                                + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                                + '<div class="row row-value py-2 py-md-2 py-lg-2">'
                                                                + '<div class="col-4">Code:</div>'
                                                                + '<div class="col-8 static" id="work-history-positioncode-' + position.emp_promoteID + '">' + position.pos_name + '</div>'
                                                                + '</div>'
                                                                + '</div>'
                                                                + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                                + '<div class="row row-value py-2 py-md-2 py-lg-2">'
                                                                + '<div class="col-4">Position:</div>'
                                                                + '<div class="col-8 static" id="work-history-position-' + position.emp_promoteID + '">' + position.pos_details + '</div>'
                                                                + '</div>'
                                                                + '</div>'
                                                                + '</div>';
                                                        posHist_string += '<div class="row ">'
                                                                + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                                + '<div class="row row-value py-2 py-md-2 py-lg-2">'
                                                                + '<div class="col-4">Status:</div>'
                                                                + '<div class="col-8 static" id="work-history-status-' + position.emp_promoteID + '">' + position.status + '</div>'
                                                                + '</div>'
                                                                + '</div>'
                                                                + '<div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">'
                                                                + '<div class="row row-value py-2 py-md-2 py-lg-2">'
                                                                + '<div class="col-4">Start Date:</div>'
                                                                + '<div class="col-8 static" id="work-history-dateFrom-' + position.emp_promoteID + '">' + moment(position.dateFrom).format('MMM DD, YYYY') + '</div>'
                                                                + '</div>'
                                                                + '</div>'
                                                                + '</div>';
                                                        if (phkey !== historycount - 1) {
                                                            posHist_string += '<hr style="border-top: 1px solid #4CAF507d"/>';
                                                        }
                                                    });
                                                }

                                                $("#positionhistory-fieldset").html(posHist_string);
                                            }

                                        }
                                    });

                                };
                                function getEmployeeDirectory(limiter, perPage, callback) {
                                    var searchname = $("#search-name").val();
                                    var searchclass = $("#search-class").val();
                                    var searchaccounts = $("#search-accounts").val();
                                    var searchactive = $("#search-active").val();
                                    $.each($("#single-profile").data(), function (i) {
                                        $("#single-profile").removeAttr("data-" + i);
                                    });
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Employee/getEmployeeDirectory",
                                        data: {
                                            limiter: limiter,
                                            perPage: perPage,
                                            searchname: searchname,
                                            searchclass: searchclass,
                                            searchaccounts: searchaccounts,
                                            searchactive: searchactive
                                        },
                                        beforeSend: function () {
                                            mApp.block('#content', {
                                                overlayColor: '#000000',
                                                type: 'loader',
                                                state: 'brand',
                                                size: 'lg'
                                            });
                                        },
                                        cache: false,
                                        success: function (res) {
                                            res = JSON.parse(res.trim());

                                            $("#total").html(res.total);
                                            $("#total").data('count', res.total);
                                            $("#card-container").html("");
                                            var string = "";
                                            $.each(res.employees, function (key, emp) {//#f1f1f1
                                                var background = (emp.isActive === 'yes') ? "#4CAF50" : "#f1f1f1";
                                                var text = (emp.isActive === 'yes') ? "#ffffff" : "#0000000";
                                                var border = (emp.isActive === 'yes') ? "#d9e8ea85" : "#d9e8ea85";
                                                var pic = emp.pic.split(".");
                                                var img = pic[0] + "_thumbnail.jpg?" + new Date().getTime();
                                                string += '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-3 px-2">';
                                                string += '<div class="card scaler" style="background:' + background + ';border:1px solid #549857" data-empid="' + emp.emp_id + '" data-isactive="' + emp.isActive + '">';
                                                string += '<div class="card-body" style="padding: 5px 10px;">';
                                                string += '<div class="m-widget4">';
                                                string += '<div class="m-widget4__item inner-card">';
                                                string += '<div class="m-widget4__img m-widget4__img--pic">';
                                                string += '<img class="employee-pic" data-src="<?php echo base_url(); ?>' + img + '" style="background:white;height:3.8rem;width:3.8rem !important;object-fit:cover;border:2px solid ' + border + '" alt="NO IMAGE">';
                                                string += '</div>';
                                                string += '<div class="m-widget4__info text-capitalize" style="line-height:1.2">';
                                                string += '<span class="m-widget4__title font-13" style="color:' + text + ' !important">' + emp.lname + ', ' + emp.fname + '';
                                                string += '</span><br />';
                                                string += '<span class="m-widget4__sub font-11" style="color:' + text + ' !important">' + emp.acc_name + '</span>';
                                                string += '</div>';
                                                string += '</div>';
                                                string += '</div>';
                                                string += '</div>';
                                                string += '</div>';
                                                string += '</div>';
                                            });
                                            $("#card-container").html(string);
                                            var myLazyLoad = new LazyLoad({
                                                elements_selector: ".employee-pic"
                                            });
                                            $('.employee-pic').on('error', function () {
                                                $(this).attr('onerror', null);
                                                $(this).attr('src', '<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>');
//                        $(this).attr('data-src', '<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>');
                                                heightChanger();
                                            });
                                            var count = $("#total").data('count');
                                            var result = parseInt(limiter) + parseInt(perPage);
                                            if (count === 0) {
                                                $("#count").html(0 + ' - ' + 0);
                                            } else if (count <= result) {
                                                $("#count").html((limiter + 1) + ' - ' + count);
                                            } else if (limiter === 0) {
                                                $("#count").html(1 + ' - ' + perPage);
                                            } else {
                                                $("#count").html((limiter + 1) + ' - ' + result);
                                            }

                                            mApp.unblock('#content');
                                            callback(res.total);
                                        }, error: function () {
                                            swal("Error", "Please contact admin", "error");
                                            mApp.unblock('#content');
                                        }
                                    });
                                }

                                function setClipboard(value) {
                                    var tempInput = document.createElement("input");
                                    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
                                    tempInput.value = value;
                                    document.body.appendChild(tempInput);
                                    tempInput.select();
                                    document.execCommand("copy");
                                    document.body.removeChild(tempInput);
                                    $.notify({
                                        message: 'Copied to Clipboard'
                                    }, {
                                        type: 'info',
                                        timer: 1000
                                    });
                                }
                                function addCommas(t) {
                                    return String(t).replace(/(\d)(?=(\d{3})+$)/g, "$1,");
                                }
                                function changeRates(salarymode, rate, workDaysPerWeek, payroll) {
                                    var user_access_rates = $("#single-profile").data('rates');//hide,view-------------------------
                                    var user_access_payrollSettings = $("#single-profile").data('payrollSettings');//hide,update,view
                                    var user_access_government = $("#single-profile").data('government');//hide,update,view
                                    var monthly = 0, semi = 0, daily = 0, hourly = 0;
                                    //PAYROLL COMPUTATIONS
                                    if (salarymode !== '' || salarymode !== null) {
                                        if (salarymode.toString().toLowerCase() === 'daily') {
                                            daily = rate;
                                        } else {
                                            var divisor = (workDaysPerWeek === '6') ? 313 : 261;
                                            monthly = rate;
                                            semi = monthly / 2;
                                            daily = (monthly * 12) / divisor;
                                            hourly = daily / 8;
                                        }
                                    }
                                    var arr_monthly = (parseFloat(monthly).toFixed(2)).split(".");
                                    var arr_semi = (parseFloat(semi).toFixed(2)).split(".");
                                    var arr_daily = (parseFloat(daily).toFixed(2)).split(".");
                                    var arr_hourly = (parseFloat(hourly).toFixed(2)).split(".");
                                    $("#payroll-rate-monthly").html("&#8369; " + addCommas(arr_monthly[0]) + "." + arr_monthly[1]);
                                    $("#payroll-rate-semi").html("&#8369; " + addCommas(arr_semi[0]) + "." + arr_semi[1]);
                                    $("#payroll-rate-daily").html("&#8369; " + addCommas(arr_daily[0]) + "." + arr_daily[1]);
                                    $("#payroll-rate-hourly").html("&#8369; " + addCommas(arr_hourly[0]) + "." + arr_hourly[1]);
                                    var sss = (payroll.sss === null) ? 0 : payroll.sss;
                                    var pagibig = (payroll.pagibig === null) ? 0 : payroll.pagibig;
                                    var philhealth = (payroll.philhealth === null) ? 0 : payroll.philhealth;
                                    var tax = (payroll.tax === null) ? 0 : payroll.tax;
                                    var arr_sss = (parseFloat(sss).toFixed(2)).split(".");
                                    var arr_pagibig = (parseFloat(pagibig).toFixed(2)).split(".");
                                    var arr_philhealth = (parseFloat(philhealth).toFixed(2)).split(".");
                                    var arr_tax = (parseFloat(tax).toFixed(2)).split(".");
                                    $("#payroll-sss").html("&#8369; " + addCommas(arr_sss[0]) + "." + arr_sss[1]);
                                    $("#payroll-pagibig").html("&#8369; " + addCommas(arr_pagibig[0]) + "." + arr_pagibig[1]);
                                    $("#payroll-philhealth").html("&#8369; " + addCommas(arr_philhealth[0]) + "." + arr_philhealth[1]);
                                    $("#payroll-tax").html("&#8369; " + addCommas(arr_tax[0]) + "." + arr_tax[1]);

                                    if (user_access_rates === 'hide') {
                                        $(".for_user_access_rates").children().not('legend').hide();
                                        $(".for_user_access_rates").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                    } else {
                                        $(".for_user_access_rates").children().not('.alert').show();
                                    }
                                    if (user_access_payrollSettings === 'hide') {
                                        $(".for_user_access_payrollSettings").children().not('legend').hide();
                                        $(".for_user_access_payrollSettings").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                    } else {
                                        $(".for_user_access_payrollSettings").children().not('.alert').show();
                                        if (user_access_payrollSettings === 'update') {
                                            $(".for_user_access_payrollSettings").find('a').each(function (i, item) {
                                                $(item).parent().removeClass('static');
                                                $(item).removeClass('disable_user_access');
                                            });
                                        } else {
                                            $(".for_user_access_payrollSettings").find('a').each(function (i, item) {
                                                $(item).parent().addClass('static');
                                                $(item).addClass('disable_user_access');
                                            });
                                        }

                                    }
                                    if (user_access_government === 'hide') {
                                        $(".for_user_access_government").children().not('legend').hide();
                                        $(".for_user_access_government").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                    } else {
                                        $(".for_user_access_government").children().not('.alert').show();
                                        if (user_access_government === 'update') {
                                            $(".for_user_access_government").find('a').each(function (i, item) {
                                                $(item).parent().removeClass('static');
                                                $(item).removeClass('disable_user_access');
                                            });
                                        } else {
                                            $(".for_user_access_government").find('a').each(function (i, item) {
                                                $(item).parent().addClass('static');
                                                $(item).addClass('disable_user_access');
                                            });
                                        }

                                    }
                                }
                                var customPopover = function (elements) {
                                    $.each(elements, function (key, elemx) {
                                        var elem = $(elemx);
                                        var id = elem.attr('id');
                                        var title = elem.data('title') ? elem.data('title') : '';
                                        var required = elem.data('required');
                                        var type = elem.data('type') ? elem.data('type') : 'text';
                                        var save = (elem.data('save') === 'insert') ? 'insert' : 'update';
                                        var isEmpty = elem.data('isempty');
                                        var value = (isEmpty === true) ? "" : elem.text();
                                        var data = elem.data('data') ? elem.data('data') : [];
                                        var notifString = '<div id="notif-required" class="m--font-danger m--font-boldest mt-1" style="display:none;">Required*</div><div id="notif-number" class="m--font-danger m--font-boldest mt-1" style="display:none;">Not a valid number*</div>';
                                        var buttons = '<button class="btn btn-outline-info btn-sm py-1 px-2 popover-approve" type="button" data-required="' + required + '"><i class="fa fa-check" style="font-size:20px;line-height: 1;"></i></button> <button class="btn btn-outline-danger btn-sm  py-1 px-2 popover-reject" type="button"><i class="fa 	fa-times"  style="font-size:20px;line-height: 1;"></i></button>';
                                        var contentText = '<input type="text" name="' + id + '" value="' + value + '"  data-value="' + value + '" class="form-control form-control-sm m-input"/>';
                                        var contentPassword = '<input type="password" name="' + id + '" value="' + value + '"  data-value="' + value + '" class="form-control form-control-sm m-input"/>';
                                        var contentEmail = '<input type="email" name="' + id + '" value="' + value + '"  data-value="' + value + '" class="form-control form-control-sm m-input"/>';
                                        var contentTextarea = '<textarea class="form-control m-input" name="' + id + '"  data-value="' + value + '" rows="3">' + value + '</textarea>';
                                        var contentDate = '<input type="text"  name="' + id + '"  data-value="' + value + '" class="form-control form-control-sm  custom-datepicker" readonly="" ' + value + '>';
                                        var contentDateRange = '<input type="text"  name="' + id + '"  data-value="' + value + '" class="form-control form-control-sm  custom-daterangepicker" readonly="" ' + value + '>';
                                        var contentNumber = '<input type="text" name="' + id + '" value="' + value + '"  data-value="' + value + '" class="form-control form-control-sm m-input"/>';
                                        if (isEmpty) {
                                            elem.html('Empty');
                                            elem.addClass('m--font-danger font-italic');
                                        }
                                        switch (type) {
                                            case 'text':
                                                var content = contentText;
                                                break;
                                            case 'number':
                                                var content = contentNumber;
                                                break;
                                            case 'password':
                                                var content = contentPassword;
                                                break;
                                            case 'email':
                                                var content = contentEmail;
                                                break;
                                            case 'date':
                                                var content = contentDate;
                                                break;
                                            case 'daterange':
                                                var content = contentDateRange;
                                                break;
                                            case 'select':
                                                var contentSelect = '<select name="' + id + '" class="form-control form-control-sm m-input">';
                                                contentSelect += '<option value="">--</option>';
                                                $.each(data, function (key, obj) {
                                                    if (obj.value.toString().toLowerCase() === value.toString().toLowerCase()) {
                                                        contentSelect += '<option value="' + obj.value + '" selected>' + obj.text + '</option>';
                                                        elem.html(obj.text);
                                                    } else {
                                                        contentSelect += '<option value="' + obj.value + '">' + obj.text + '</option>';
                                                    }

                                                });
                                                contentSelect += '</select>';
                                                var content = contentSelect;
                                                break;
                                            case 'textarea':
                                                var content = contentTextarea;
                                                break;
                                        }
                                        elem.popover({
                                            trigger: 'click',
                                            placement: 'bottom',
                                            title: title,
                                            html: true,
                                            template: '\
            <div class="m-popover m-popover--skin-dark popover" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body p-2"></div>\
            </div>',
                                            content: '<div class="row" data-save="' + save + '" data-type="' + type + '"><div class="col-md-8 col-xs-7">' + content + '</div><div class="col-md-4 col-xs-5 text-right">' + buttons + '</div></div>' + notifString
                                        }).on('click', function (e) {
                                            $('[data-original-title]').not(elem).popover('hide');
                                            e.preventDefault();
                                        }).on('inserted.bs.popover', function () {

                                            var popover_elem = $(this).data("bs.popover").tip;
                                            if (type === 'date') {
                                                var input_elem = $(popover_elem).find('.custom-datepicker');
                                                input_elem.datepicker({
                                                    orientation: "bottom left",
                                                    templates: {
                                                        leftArrow: '<i class="la la-angle-left"></i>',
                                                        rightArrow: '<i class="la la-angle-right"></i>'
                                                    },
                                                    format: 'yyyy-mm-dd'
                                                });

                                                if (!isEmpty) {
                                                    input_elem.datepicker('update', new Date(value));
                                                }

                                            }
                                            if (type === 'daterange') {
                                                var input_elem = $(popover_elem).find('.custom-daterangepicker');
                                                input_elem.daterangepicker({
                                                    opens: "left",
                                                    drops: "down",
                                                    autoApply: true
                                                }, function (start, end, label) {
                                                    if (!isEmpty) {
                                                        input_elem.val(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
                                                    }
                                                });
                                            } else if (type === 'select') {
                                                var input_elem = $(popover_elem).find('select');
                                                var realvalue = input_elem.val();
                                                input_elem.data('value', realvalue);
                                            }
                                        }).on("show.bs.popover", function () {
                                            var el = $(this).data("bs.popover").tip;
                                            $(el).css("max-width", "300px");
                                        });
                                    });
                                };
                                function getAuditTrail() {
                                    var datestart = $("#search-dates-trail").data('daterangepicker').startDate;
                                    var dateend = $("#search-dates-trail").data('daterangepicker').endDate;
                                    var emp_id = "<?php echo $this->session->userdata('emp_id') ?>";
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Employee/get_audit_trail",
                                        data: {
                                            datestart: tz_date(moment(datestart), 'YYYY-MM-DD'),
                                            dateend: tz_date(moment(dateend), 'YYYY-MM-DD'),
                                            emp_id: emp_id
                                        },
                                        beforeSend: function () {
                                            mApp.block('#table-trail', {
                                                overlayColor: '#000000',
                                                type: 'loader',
                                                state: 'brand',
                                                size: 'lg'
                                            });
                                        },
                                        cache: false,
                                        success: function (res) {
                                            res = JSON.parse(res.trim());
                                            $("#table-trail tbody").html("");
                                            var string = '';
                                            $.each(res.trails, function (i, trail) {
                                                if (trail.remark === 'I - Back from break' || trail.remark === 'O - Out for break ') {
                                                    var remarks = '<span class="m--font-warning m--font-bolder">' + trail.remark + '</span>';
                                                } else if (trail.remark === 'I - DTR' || trail.remark === 'O - DTR') {
                                                    var remarks = '<span class="m--font-info m--font-bolder">' + trail.remark + '</span>';
                                                } else {
                                                    var remarks = '<span class=" m--font-bold">' + trail.remark + '</span>';
                                                }
                                                string += '<tr>'
                                                        + '<td>' + remarks + '</td>'
                                                        + '<td>' + moment(trail.log).format('MMM DD,YYYY hh:mm:ss A') + '</td>'
                                                        + '<td>' + trail.ipaddress + '</td>'
                                                        + '</tr>';
                                            });

                                            if (string === '') {
                                                $("#date-alert-trail").show();
                                                $("#table-trail").hide();
                                            } else {
                                                $("#date-alert-trail").hide();
                                                $("#table-trail").show();
                                            }
                                            $("#table-trail tbody").html(string);
                                            mApp.unblock('#table-trail');
                                        }, error: function () {
                                            swal("Error", "Please contact admin", "error");
                                            mApp.unblock('#table-trail');
                                        }
                                    });
                                }
                                function getScheduleLogs(limiter, perPage, callback) {

                                    var datestart = $("#search-dates").data('daterangepicker').startDate;
                                    var dateend = $("#search-dates").data('daterangepicker').endDate;
                                    var emp_id = $('#m_quick_sidebar').data('emp_id');
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Employee/get_schedule_logs",
                                        data: {
                                            limiter: limiter,
                                            perPage: perPage,
                                            datestart: tz_date(moment(datestart), 'YYYY-MM-DD'),
                                            dateend: tz_date(moment(dateend), 'YYYY-MM-DD'),
                                            emp_id: emp_id
                                        },
                                        beforeSend: function () {
                                            mApp.block('#table-dtr', {
                                                overlayColor: '#000000',
                                                type: 'loader',
                                                state: 'brand',
                                                size: 'lg'
                                            });
                                        },
                                        cache: false,
                                        success: function (res) {
                                            res = JSON.parse(res.trim());
                                            $("#dtr-total").html(res.total);
                                            $("#dtr-total").data('count', res.total);
                                            $("#table-dtr tbody").html("");
                                            var string = '';
                                            $.each(res.data, function (i, item) {
                                                if (item.time_start === null) {
                                                    var shift = "";
                                                } else {
                                                    var shift = item.time_start + " <br /> " + item.time_end;
                                                }
                                                if (shift === '') {
                                                    string += '<tr style="background:#f6f7f8">';
                                                } else {
                                                    string += '<tr>';
                                                }
                                                string += '<td class="text-center">' + tz_date(moment(item.sched_date), 'MMM DD, YYYY') + '</td>';
                                                if (shift === '') {
                                                    string += '<td class="text-center" colspan="2" style="color:' + item.style + ';font-weight:400">' + item.type + '</td>';
                                                } else {
                                                    string += '<td class="text-center" style="color:' + item.style + ';font-weight:400">' + item.type + '</td>';
                                                    string += '<td class="text-center" style="color:' + item.style + ';font-weight:400">' + shift + '</td>';
                                                }
                                                if (item.clock_in === null) {
                                                    string += '<td>--</td>';
                                                } else {
                                                    if (item.clock_in.log === null) {
                                                        string += '<td>---</td>';
                                                    } else {
                                                        string += '<td>' + moment(item.clock_in.log).format('MMM DD, YYYY ') + ' <span class="m--font-bolder">' + moment(item.clock_in.log).format(' hh:mm A') + '</span></td>';
                                                    }
                                                }
                                                if (item.clock_out === null) {
                                                    string += '<td>--</td>';
                                                } else {
                                                    if (item.clock_out.log === null) {
                                                        string += '<td>---</td>';
                                                    } else {
                                                        string += '<td>' + moment(item.clock_out.log).format('MMM DD, YYYY ') + ' <span class="m--font-bolder">' + moment(item.clock_out.log).format(' hh:mm A') + '</span></td>';
                                                    }
                                                }
                                                if (item.breaklogs === null) {
                                                    string += '<td>--</td>';
                                                } else {
                                                    var totaltime = '';
                                                    if (item.totalbreakminutes === 0) {
                                                        totaltime = item.totalbreakseconds + ' sec';
                                                    } else if (item.totalbreakseconds === 0) {
                                                        totaltime = item.totalbreakminutes + ' min';
                                                    } else {
                                                        totaltime = item.totalbreakminutes + ' min & <br /> ' + item.totalbreakseconds + ' sec';
                                                    }

                                                    var breaklogs = '<br /><p class=\'font-white\'><b>Break Logs</b></p>';
                                                    var counter = 1;
                                                    $.each(item.breaklogs, function (brkey, breaklog) {
                                                        breaklogs += '<small><b>Break #' + counter + '</b></small><br />';
                                                        breaklogs += '<small class=\'font-white\'>' + moment(breaklog.breakout.log).format('MMM DD, YYYY hh:mm:ss A') + '</small><br />';
                                                        breaklogs += '<small class=\'font-white\'>' + moment(breaklog.breakin.log).format('MMM DD, YYYY hh:mm:ss A') + '</small><hr />';
                                                        counter++;
                                                    });
                                                    string += '<td class="font-11 m--font-info m--font-bolder" data-toggle="m-tooltip" data-skin="dark" data-html="true" data-placement="left" title="" data-original-title="' + breaklogs + '" >' + totaltime + ' </td>';
                                                }
                                                string += '</tr>';
                                            });
                                            if (string === '') {
                                                $("#date-alert").show();
                                                $("#table-dtr").hide();
                                            } else {
                                                $("#date-alert").hide();
                                                $("#table-dtr").show();
                                            }
                                            $("#table-dtr tbody").html(string);
                                            mApp.initTooltips();
                                            var count = $("#dtr-total").data('count');
                                            var result = parseInt(limiter) + parseInt(perPage);
                                            if (count === 0) {
                                                $("#dtr-count").html(0 + ' - ' + 0);
                                            } else if (count <= result) {
                                                $("#dtr-count").html((limiter + 1) + ' - ' + count);
                                            } else if (limiter === 0) {
                                                $("#dtr-count").html(1 + ' - ' + perPage);
                                            } else {
                                                $("#dtr-count").html((limiter + 1) + ' - ' + result);
                                            }
                                            mApp.unblock('#table-dtr');
                                            callback(res.total);
                                        }, error: function () {
                                            swal("Error", "Please contact admin", "error");
                                            mApp.unblock('#table-dtr');
                                        }
                                    });
                                }
                                var mQuickSidebar = function () {
                                    var topbarAside = $('#m_quick_sidebar');
                                    var topbarAsideClose = $('#m_quick_sidebar_close');
                                    var topbarAsideToggle = $('#m_quick_sidebar_toggle');
                                    var initOffcanvas = function () {
                                        topbarAside.mOffcanvas({
                                            class: 'm-quick-sidebar',
                                            overlay: true,
                                            close: topbarAsideClose,
                                            toggle: topbarAsideToggle
                                        });
                                    };
                                    return {
                                        init: function () {
                                            if (topbarAside.length === 0) {
                                                return;
                                            }
                                            initOffcanvas();
                                        }
                                    };
                                }();


                                $(function () {
                                    mQuickSidebar.init();
                                    var $uploadCrop, tempFilename, rawImg, imageId;
                                    $uploadCrop = $('#upload-demo').croppie({
                                        viewport: {
                                            width: 250,
                                            height: 250
                                        },
                                        boundary: {
                                            width: 300,
                                            height: 300
                                        },
                                        enforceBoundary: true,
                                        enableExif: true,
                                        enableOrientation: true
                                    });
                                    //---------------------------------EMPLOYEE UPDATE INITIALIZATIONS------------------------------------------------------------------------------
                                    var startCount = 100;
                                    var year = parseInt(moment().year());
                                    var select_previousYears = [];
                                    for (var x = startCount; x >= 0; x--) {
                                        select_previousYears.push({value: year.toString(), text: year.toString()});
                                        year = year - 1;
                                    }

                                    var select_departments = [];
                                    var select_accounts = [];
                                    var select_roles = [];
                                    var select_activeEmployees = [];
                                    get_all_departments(function (departmentArr) {
                                        get_all_accounts(function (accountArr) {
                                            get_all_user_roles(function (userRoleArr) {
                                                get_all_active_employees(function (activeArr) {
                                                    $.each(departmentArr.departments, function (key, dep) {
                                                        select_departments.push({value: dep.dep_id, text: dep.dep_details});
                                                    });
                                                    $.each(accountArr.accounts, function (key, acc) {
                                                        select_accounts.push({value: acc.acc_id, text: acc.acc_name});
                                                    });
                                                    $.each(userRoleArr.roles, function (key, role) {
                                                        select_roles.push({value: role.role_id, text: role.description});
                                                    });

                                                    $.each(activeArr.activeEmployees, function (key, activeEmp) {
                                                        select_activeEmployees.push({value: activeEmp.emp_id, text: activeEmp.lname + ", " + activeEmp.fname});
                                                    });


                                                });

                                            });
                                        });
                                    });
                                    var select_gender = [
                                        {value: 'male', text: 'Male'},
                                        {value: 'female', text: 'Female'}
                                    ];
                                    var select_yesno = [
                                        {value: '1', text: 'Yes'},
                                        {value: '0', text: 'No'}
                                    ];
                                    var select_civil = [
                                        {value: 'single', text: 'Single'},
                                        {value: 'married', text: 'Married'},
                                        {value: 'legally separated', text: 'Legally Separated'},
                                        {value: 'widower', text: 'Widower'}
                                    ];
                                    var select_extension = [
                                        {value: 'jr', text: 'Jr'},
                                        {value: 'sr', text: 'Sr'},
                                        {value: 'i', text: 'I'},
                                        {value: 'ii', text: 'II'}
                                    ];
                                    var select_bloodtype = [
                                        {value: 'a', text: 'A'},
                                        {value: 'b', text: 'B'},
                                        {value: 'o', text: 'O'},
                                        {value: 'ab', text: 'AB'}
                                    ];
                                    var select_attainment = [
                                        {value: 'college level', text: 'College Level'},
                                        {value: 'college graduate', text: 'College Graduate'},
                                        {value: 'graduate level', text: 'Graduate Level'}
                                    ];
                                    var select_religion = [
                                        {value: 'roman catholic', text: 'Roman Catholic'},
                                        {value: 'muslim', text: 'Muslim'},
                                        {value: 'protestant', text: 'Protestant'},
                                        {value: 'iglesia ni cristo', text: 'Iglesia Ni Cristo'},
                                        {value: 'el shaddai', text: 'El Shaddai'},
                                        {value: 'jehovahs witnesses', text: 'Jehovahs Witnesses'}
                                    ];
                                    var select_salarymode = [
                                        {value: 'Daily', text: 'Daily'},
                                        {value: 'Monthly', text: 'Monthly'}
                                    ];
                                    var select_atm = [
                                        {value: '0', text: 'Non-ATM'},
                                        {value: '1', text: 'ATM'},
                                        {value: '2', text: 'On Hold'}
                                    ];
                                    var select_workdays = [
                                        {value: '5', text: '5'},
                                        {value: '6', text: '6'}
                                    ];
                                    //---------------------------------------------------EMPLOYEE UPDATE INITIALIZATIONS--------------------------------------------------------
                                    get_all_accounts(function (res) {
                                        $("#search-accounts").html("<option value=''>All</option>");
                                        $.each(res.accounts, function (i, item) {
                                            $("#search-accounts").append("<option value='" + item.acc_id + "' data-class='" + item.acc_description + "'>" + item.acc_name + "</option>");
                                        });
                                    });
                                    $("#search-class").change(function () {
                                        var theclass = $(this).val();
                                        $("#search-accounts").children('option').css('display', '');
                                        $("#search-accounts").val('').change();
                                        if (theclass !== '') {
                                            $("#search-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css('display', 'none');
                                        }
                                        $("#perpage").change();
                                    });
                                    $("#search-accounts,#search-active").change(function () {
                                        $("#perpage").change();
                                    });
                                    $("#search-name").change(function () {
                                        $("#perpage").change();
                                    });
                                    $("#perpage").change(function () {
                                        var perPage = $("#perpage").val();
                                        getEmployeeDirectory(0, perPage, function (total) {
                                            $("#pagination").pagination('destroy');
                                            $("#pagination").pagination({
                                                items: total, //default
                                                itemsOnPage: $("#perpage").val(),
                                                hrefTextPrefix: "#",
                                                cssStyle: 'light-theme',
                                                displayedPages: 10,
                                                onPageClick: function (pagenumber) {
                                                    var perPage = $("#perpage").val();
                                                    getEmployeeDirectory((pagenumber * perPage) - perPage, perPage);
                                                }
                                            });
                                        });
                                    });


                                    $("#perpage").change();
                                    $("#card-container").on('click', '.scaler', function () {
                                        $('#m_quick_sidebar').data('emp_id', $(this).data('empid'));
                                        $('#m_quick_sidebar').data('isactive', $(this).data('isactive'));
                                        $("#m_quick_sidebar_toggle").click();
                                        var emp_id = $('#m_quick_sidebar').data('emp_id');
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url(); ?>Employee/profilePreview",
                                            beforeSend: function () {
                                                mApp.block($('#m_quick_sidebar'));
                                            },
                                            data: {
                                                emp_id: emp_id
                                            },
                                            cache: false,
                                            success: function (res) {
                                                res = JSON.parse(res.trim());

                                                //USER ACCESS ------------------------------------------------------------------------------------------------------------------

                                                var userAccess = res.user_access;
                                                $.each(userAccess, function (field, value) {
                                                    if (field === 'updatedOn' || field === 'isActive' || field === 'changedBy' || field === 'emp_id' || field === 'createdOn' || field === 'employeeUserAccess_ID') {
//                            console.log(field);
                                                    } else {
                                                        $("#single-profile").data(field, value);
                                                    }
                                                });
                                                if (userAccess === null) {
                                                    $("#btn-full-quicksidebar").hide();
                                                    $("#quickbar-references-link").addClass('m--hide');
                                                } else {
                                                    var userAccess_inProfile = $("#single-profile").data();
                                                    var willHideAll = true;
                                                    $.each(userAccess_inProfile, function (x, xx) {
                                                        if (xx !== 'hide') {
                                                            willHideAll = false;
                                                        }
                                                    });
                                                    if (willHideAll === true) {
                                                        $("#btn-full-quicksidebar").hide();
                                                        $("#quickbar-references-link").addClass('m--hide');
                                                    } else {
                                                        $("#quickbar-references-link").removeClass('m--hide');
                                                        $("#btn-full-quicksidebar").show();
                                                    }

                                                }

                                                //USER ACCESS ------------------------------------------------------------------------------------------------------------------
                                                var profile = res.profile;
                                                var address = profile.presentAddress;
                                                var addressArr = (address === null) ? null : jQuery.grep(address.split(" = "), function (n) {
                                                    return (n);
                                                });
                                                var addressArr = (addressArr === null) ? null : jQuery.grep(address.split("|"), function (n) {
                                                    return (n);
                                                });
                                                var addressString = (addressArr === null) ? "" : addressArr.join(", ");
                                                var emergency = (profile.emergencyFname === null) ? null : profile.emergencyFname + " " + profile.emergencyMname + " " + profile.emergencyLname;
                                                $("#profile-single-image").attr('src', "<?php echo base_url(); ?>" + profile.pic + "?" + new Date().getTime());
                                                $("#profile-single-name").html(profile.fname + " <span style='color:#9ef5a2'>" + profile.mname + "</span> " + profile.lname);
                                                $("#profile-single-position").html((profile.pos_details === null || profile.pos_details === '') ? "-" : profile.pos_details);
                                                $("#profile-single-status").html((profile.status === null || profile.status === '') ? "" : "(" + profile.status + ")");
                                                $("#profile-single-account").html(profile.acc_name);
                                                $("#profile-single-supervisor").html((profile.supervisor_name === null || profile.supervisor_name === '') ? "-" : profile.supervisor_name);
                                                $("#profile-single-address").html((addressString === null || addressString === '') ? "-" : addressString);
                                                $("#profile-single-number").html((profile.cell === null || profile.cell === '') ? "-" : profile.cell);
                                                $("#profile-single-email").html((profile.email === null || profile.email === '') ? "-" : profile.email);
                                                $("#profile-single-emerg-name").html((emergency === null || emergency === '') ? "-" : emergency);
                                                $("#profile-single-emerg-number").html((profile.emergencyContact === null || profile.emergencyContact === '') ? "-" : profile.emergencyContact);
                                                var withApostrophe = profile.fname.split(' ')[0];
                                                if (withApostrophe.substr(-1) === 's') {
                                                    withApostrophe += "'";
                                                } else {
                                                    withApostrophe += "'s";
                                                }
                                                $("#profile-single-firstname").html(withApostrophe);

                                                $('#profile-single-image').on('error', function () {
                                                    $(this).attr('onerror', null);
                                                    $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                                                    heightChanger();
                                                });

                                                $("#quickbar-references-link").html();
                                                if (profile.references === null) {
                                                    $("#quickbar-references-link").hide();
                                                } else {
                                                    var references = '<hr class="border-light-green"/><p>PREVIOUS ACCOUNTS:</p>';
                                                    $.each(profile.references, function (x, item) {
                                                        var dateFrom = (item === null) ? "Unknown" : moment(item.dateFrom).format('MMM YYYY');
                                                        var separationDate = (item === null) ? "Unknown" : moment(item.separationDate).format('MMM YYYY');
                                                        dateFrom = (dateFrom === 'Invalid date') ? "Unknown" : dateFrom;
                                                        separationDate = (separationDate === 'Invalid date') ? "Unknown" : separationDate;
                                                        references += '<a href="#" class="m-link m-link--state m-link--info m--font-bold font-12 quickbar-reference" data-referenceid="' + x + '">' + dateFrom + ' - ' + separationDate + '</a><br />';
                                                    });
                                                    $("#quickbar-references-link").html(references);

                                                    $("#quickbar-references-link").show();
                                                }
                                                $(".quickbar-reference").click(function () {
                                                    $('#m_quick_sidebar').data('emp_id', $(this).data('referenceid'));
                                                    $('#m_quick_sidebar').data('isactive', false);
                                                    $("#btn-full-quicksidebar").click();
                                                });
                                                mApp.unblock($('#m_quick_sidebar'));
                                            }
                                        });
                                    });
                                    $("#btn-full-quicksidebar").click(function (e) {
                                        e.preventDefault();
                                        var top = (window.pageYOffset || $(document).scrollTop) - ($(document).clientTop || 0);
                                        $("#main-directory").data('scroll', top);
                                        $("#main-directory").hide();
                                        $("#single-profile").show(500);
                                        $(".m-subheader").hide();
                                        $("#m_quick_sidebar_close").click();
                                        $('html').on('click', function (e) {
                                            if (typeof $(e.target).data('original-title') === 'undefined' && $(e.target).closest('.popover').length === 0) {
                                                $('[data-original-title]').popover('hide');
                                            }
                                        });
                                        $("#btn-changePositionModal ,[data-target='#deactivateModal'],[data-target='#addFamilyModal'],[data-target='#addPrevAccountsModal']").hide().prop('disabled', true);

                                        //USER ACCESS -------------------------------------------------------------------------------------------------------------
                                        var user_access_basic = $("#single-profile").data('basic');//hide,update,view


                                        var user_access_dtr = $("#single-profile").data('dtr');//hide,view-----------------------
                                        var user_access_auditTrail = $("#single-profile").data('auditTrail');//hide,view----------------------


                                        var user_access_confidentiality = $("#single-profile").data('confidentiality');//hide,update,view
                                        var user_access_deactivation = $("#single-profile").data('deactivation');//hide,update
                                        $(".alert_for_user_access").remove();
                                        //------------------------INSERT CODES FOR DISPLAY HERE-------------------------------------------------------------
                                        $('[href="#tab-personal"]').click();
                                        var emp_id = $('#m_quick_sidebar').data('emp_id');
                                        var isActive = $('#m_quick_sidebar').data('isactive');
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url(); ?>Employee/employeeDetails",
                                            beforeSend: function () {
                                                mApp.block($('#content'));
                                            },
                                            data: {
                                                emp_id: emp_id
                                            },
                                            cache: false,
                                            success: function (res) {
                                                res = JSON.parse(res.trim());
                                                var employee = res.employee;
                                                var family = res.family;
                                                $("#image-caption-lname").html(employee.lname);
                                                $("#image-caption-fname").html(employee.fname);

                                                $("#tbl_applicant-lname").html(employee.lname);
                                                $("#tbl_applicant-mname").html(employee.mname);
                                                $("#tbl_applicant-fname").html(employee.fname);
                                                $("#tbl_applicant-nameExt").html(employee.nameExt);
                                                $("#tbl_applicant-nickName").html(employee.nickName);
                                                $("#tbl_applicant-birthday").html(employee.birthday);
                                                $("#tbl_applicant-birthplace").html(employee.birthplace);
                                                $("#tbl_applicant-gender").html(employee.gender);
                                                $("#tbl_applicant-religion").html(employee.religion);
                                                $("#tbl_applicant-civilStatus").html(employee.civilStatus);
                                                $("#tbl_applicant-bloodType").html(employee.bloodType);
                                                $("#tbl_applicant-email").html(employee.email);
                                                $("#tbl_applicant-tel").html(employee.tel);
                                                $("#tbl_applicant-cell").html(employee.cell);
                                                $("#tbl_applicant-permanentAddress").html(employee.permanentAddress);
                                                $("#tbl_applicant-presentAddress").html(employee.presentAddress);
                                                $("#tbl_employee-course").html(employee.course);
                                                $("#tbl_employee-lastSchoolYear").html(employee.lastSchoolYear);
                                                $("#tbl_employee-lastSchool").html(employee.lastSchool);
                                                $("#tbl_employee-highSchoolYear").html(employee.highSchoolYear);
                                                $("#tbl_employee-highSchool").html(employee.highSchool);
                                                $("#tbl_employee-educationalAttainment").html(employee.educationalAttainment);
                                                if (employee.birthday === '') {
                                                    $("#personal-age").html("No Birthday");
                                                } else {
                                                    var bday = tz_date(moment()).diff(employee.birthday, 'years');
                                                    $("#personal-age").html(bday + " years old");
                                                }
                                                $("#work-supervisorEmail").html(employee.supervisor_email);

                                                $("#tbl_employee-confidential").html(employee.confidential);

                                                $("#tbl_employee-id_num").html(employee.id_num);
                                                $("#tbl_employee-acc_id").html(employee.acc_id);
                                                $("#tbl_employee-intellicare").html(employee.intellicare);
                                                $("#tbl_employee-pioneer").html(employee.pioneer);
                                                $("#tbl_employee-bir").html(employee.bir);
                                                $("#tbl_employee-philhealth").html(employee.philhealth);
                                                $("#tbl_employee-pagibig").html(employee.pagibig);
                                                $("#tbl_employee-sss").html(employee.sss);
                                                $("#tbl_employee-atm_account_number").html(employee.atm_account_number);
                                                $("#tbl_employee-isAtm").html(employee.isAtm);
                                                $("#tbl_employee-salarymode").html(employee.salarymode);
                                                $("#tbl_employee-workDaysPerWeek").html(employee.workDaysPerWeek);
                                                $("#tbl_employee-confidential").html(employee.confidential);
                                                $("#tbl_employee-Supervisor").html(employee.Supervisor);
                                                $("#tbl_employee-emergencyFname").html(employee.emergencyFname);
                                                $("#tbl_employee-emergencyMname").html(employee.emergencyMname);
                                                $("#tbl_employee-emergencyLname").html(employee.emergencyLname);
                                                $("#tbl_employee-emergencyNameExt").html(employee.emergencyNameExt);
                                                $("#tbl_employee-emergencyContact").html(employee.emergencyContact);
                                                $("#tbl_employee-emergencyAddress").html(employee.emergencyAddress);
                                                $("#tbl_employee-emergencyRelationship").html(employee.emergencyRelationship);
                                                $("#tbl_user-username").html(employee.username);
                                                $("#tbl_user-password").html(employee.password.replace(/[^\s]/g, "*"));
                                                $("#tbl_user-password").attr('realValue', employee.password);
                                                $("#tbl_user-role_id").html(employee.role_id);
                                                $("#tbl_applicant-cr1").html(employee.cr1);
                                                $("#tbl_applicant-cr2").html(employee.cr2);
                                                $("#tbl_applicant-cr3").html(employee.cr3);
                                                $("#tbl_applicant-source").html(employee.source);
                                                $("#tbl_applicant-ccaExp").html(employee.ccaExp);
                                                $("#tbl_applicant-latestEmployer").html(employee.latestEmployer);
                                                $("#tbl_applicant-lastPositionHeld").html(employee.lastPositionHeld);
                                                $("#tbl_applicant-inclusiveDates").html(employee.inclusiveDates);
                                                $("#tbl_applicant-lastEmployerAddress").html(employee.lastEmployerAddress);
                                                $("#tbl_applicant-lastEmployerContact").html(employee.lastEmployerContact);
                                                $("#tbl_applicant-failReason").html(employee.failReason);
                                                $("a[id^='tbl_applicant'],a[id^='tbl_employee'],a[id^='tbl_user'],a[id^='tbl_tax_emp'],a[id^='tbl_pagibig_add_contribution']").each(function (id, elem) {
                                                    var val = $(elem).html();
                                                    if (val === '') {
                                                        $(elem).data('isempty', true);
                                                    }
                                                });
                                                $("#tbl_pagibig_add_contribution-amount").html(res.pagibig_additional);
                                                if (res.pagibig_additional === '') {
                                                    $("#tbl_pagibig_add_contribution-amount").data('save', 'insert');
                                                }
                                                //--------------------------POPOVER INITIALIZATIONS----------------------------------------------------

                                                $("#tbl_employee-Supervisor").data('data', select_activeEmployees);
                                                $("#tbl_applicant-gender").data('data', select_gender);
                                                $("#tbl_applicant-civilStatus").data('data', select_civil);
                                                $("#tbl_applicant-nameExt").data('data', select_extension);
                                                $("#tbl_employee-emergencyNameExt").data('data', select_extension);
                                                $("#tbl_applicant-bloodType").data('data', select_bloodtype);
                                                $("#tbl_applicant-religion").data('data', select_religion);
                                                $("#tbl_applicant-ccaExp").data('data', select_yesno);
                                                $("#tbl_employee-lastSchoolYear,#tbl_employee-highSchoolYear").data('data', select_previousYears);
                                                $("#tbl_employee-educationalAttainment").data('data', select_attainment);
                                                $("#tbl_employee-acc_id").data('data', select_accounts);
                                                $("#tbl_user-role_id").data('data', select_roles);
                                                $("#tbl_employee-salarymode").data('data', select_salarymode);
                                                $("#tbl_employee-isAtm").data('data', select_atm);
                                                $("#tbl_employee-workDaysPerWeek").data('data', select_workdays);
                                                $("#tbl_employee-confidential").data('data', select_yesno);
                                                //------------------------INSERT CODES FOR XEDITABLE HERE ----------------------------------------------------------
                                                var element_ids = [
                                                    "#tbl_applicant-lname",
                                                    "#tbl_applicant-fname",
                                                    "#tbl_applicant-mname",
                                                    "#tbl_applicant-nickName",
                                                    "#tbl_applicant-nameExt",
                                                    "#tbl_applicant-birthday",
                                                    "#tbl_applicant-birthplace",
                                                    "#tbl_applicant-religion",
                                                    "#tbl_applicant-bloodType",
                                                    "#tbl_applicant-gender",
                                                    "#tbl_applicant-civilStatus",
                                                    "#tbl_applicant-presentAddress",
                                                    "#tbl_applicant-permanentAddress",
                                                    "#tbl_applicant-tel",
                                                    "#tbl_applicant-cell",
                                                    "#tbl_applicant-email",
                                                    "#tbl_employee-course",
                                                    "#tbl_employee-lastSchoolYear",
                                                    "#tbl_employee-lastSchool",
                                                    "#tbl_employee-highSchoolYear",
                                                    "#tbl_employee-highSchool",
                                                    "#tbl_employee-educationalAttainment",
                                                    "#tbl_employee-id_num",
                                                    "#tbl_employee-acc_id",
                                                    "#tbl_employee-intellicare",
                                                    "#tbl_employee-pioneer",
                                                    "#tbl_employee-bir",
                                                    "#tbl_employee-philhealth",
                                                    "#tbl_employee-pagibig",
                                                    "#tbl_employee-sss",
                                                    "#tbl_employee-atm_account_number",
                                                    "#tbl_employee-isAtm",
                                                    "#tbl_employee-salarymode",
                                                    "#tbl_employee-workDaysPerWeek",
                                                    "#tbl_employee-confidential",
                                                    "#tbl_employee-Supervisor",
                                                    "#tbl_employee-emergencyFname",
                                                    "#tbl_employee-emergencyMname",
                                                    "#tbl_employee-emergencyLname",
                                                    "#tbl_employee-emergencyNameExt",
                                                    "#tbl_employee-emergencyContact",
                                                    "#tbl_employee-emergencyAddress",
                                                    "#tbl_employee-emergencyRelationship",
                                                    "#tbl_applicant-cr1",
                                                    "#tbl_applicant-cr2",
                                                    "#tbl_applicant-cr3",
                                                    "#tbl_applicant-source",
                                                    "#tbl_applicant-ccaExp",
                                                    "#tbl_applicant-latestEmployer",
                                                    "#tbl_applicant-lastPositionHeld",
                                                    "#tbl_applicant-inclusiveDates",
                                                    "#tbl_applicant-lastEmployerAddress",
                                                    "#tbl_applicant-lastEmployerContact",
                                                    "#tbl_user-username",
                                                    "#tbl_user-password",
                                                    "#tbl_user-role_id",
                                                    "#tbl_tax_emp-tax_desc_id",
                                                    "#tbl_employee-confidential",
                                                    "#tbl_pagibig_add_contribution-amount"
                                                ];
                                                if (isActive === 'yes') {
                                                    $("#image-caption-active").removeClass();
                                                    $("#image-caption-active").addClass("m-badge m-badge--success m-badge--wide m-badge--rounded");
                                                    $("#image-caption-active").html("active");
                                                    customPopover(element_ids);
                                                    $.each(element_ids, function (x, elem) {
                                                        $(elem).css({"color": "", "font-style": ""});
                                                        $(elem).parent().css({"text-decoration": ""});
                                                    });
                                                    $("#btn-changePositionModal,[data-target='#deactivateModal'],[data-target='#addFamilyModal'],[data-target='#addPrevAccountsModal']").show().prop('disabled', false);

                                                } else {
                                                    $("#image-caption-active").removeClass();
                                                    $("#image-caption-active").addClass("m-badge m-badge--danger m-badge--wide m-badge--rounded");
                                                    $("#image-caption-active").html("inactive");
                                                    $.each(element_ids, function (x, elem) {
                                                        $(elem).attr('style', 'color:#333 !Important;font-style:none !Important');
                                                        $(elem).parent().attr('style', 'text-decoration:none !Important');
                                                    });
                                                }

                                                setPositions();
                                                setFamily(family, isActive);
                                                setPrevAccounts();
                                                // Start upload preview image
                                                $("#item-img-output").attr("src", "<?php echo base_url(); ?>" + employee.pic + "?" + new Date().getTime());
                                                $('#item-img-output').on('error', function () {
                                                    $(this).attr('onerror', null);
                                                    $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                                                });
                                                function readFile(input) {
                                                    if (input.files && input.files[0]) {
                                                        var file = input.files[0];
                                                        var fileType = file["type"];
                                                        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
                                                        if ($.inArray(fileType, ValidImageTypes) < 0) {
                                                            swal("File selected is not an image");
                                                        } else {
                                                            var reader = new FileReader();
                                                            reader.onload = function (e) {
                                                                $('.upload-demo').addClass('ready');
                                                                rawImg = e.target.result;
                                                            };
                                                            reader.readAsDataURL(input.files[0]);
                                                            reader.onloadend = function () {
                                                                $('#cropImagePop').modal('show');
                                                            };
                                                        }


                                                    } else {
                                                        swal("Sorry - you're browser doesn't support the FileReader API");
                                                    }
                                                }
                                                $('.item-img').on('change', function () {
                                                    imageId = $(this).data('id');
                                                    tempFilename = $(this).val();
                                                    $('#cancelCropBtn').data('id', imageId);
                                                    readFile(this);
                                                });
                                                mApp.unblock($('#content'));
                                                //---------------------------------VIEW DTR LOGS------------------------------------------------------------------------------------

                                                if (user_access_dtr === 'hide') {
                                                    $("#search-dates").hide();
                                                    $(".for_user_access_dtr").children().not('legend').hide();
                                                    $(".for_user_access_dtr").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                                } else {
                                                    $("#search-dates").show();
                                                    $(".for_user_access_dtr").children().not('.alert').show();
                                                    $("#table-dtr tbody").html("");
                                                    $('#search-dates .form-control').val(tz_date(moment(), 'MMM DD, YYYY') + ' - ' + tz_date(moment(), 'MMM DD, YYYY'));

                                                    $('#search-dates').daterangepicker({
                                                        startDate: tz_date(moment()),
                                                        endDate: tz_date(moment()),
                                                        opens: 'center',
                                                        drops: 'down',
                                                        autoUpdateInput: true
                                                    },
                                                            function (start, end, label) {
                                                                $('#search-dates .form-control').val(tz_date(moment(start), 'MMM DD, YYYY') + ' - ' + tz_date(moment(end), 'MMM DD, YYYY'));
                                                                $("#dtr-perpage").change();
                                                            });
                                                }
                                                if (user_access_auditTrail === 'hide') {
                                                    $("#search-dates-trail").hide();
                                                    $(".for_user_access_auditTrail").children().not('legend').hide();
                                                    $(".for_user_access_auditTrail").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                                } else {
                                                    $("#search-dates-trail").show();
                                                    $(".for_user_access_auditTrail").children().not('.alert').show();
                                                    $("#table-trail tbody").html("");
                                                    $('#search-dates-trail .form-control').val(tz_date(moment(), 'MMM DD, YYYY') + ' - ' + tz_date(moment(), 'MMM DD, YYYY'));
                                                    $('#search-dates-trail').daterangepicker({
                                                        startDate: tz_date(moment()),
                                                        endDate: tz_date(moment()),
                                                        opens: 'center',
                                                        drops: 'up',
                                                        autoUpdateInput: true
                                                    },
                                                            function (start, end, label) {
                                                                $('#search-dates-trail .form-control').val(tz_date(moment(start), 'MMM DD, YYYY') + ' - ' + tz_date(moment(end), 'MMM DD, YYYY'));
                                                                getAuditTrail();
                                                            });
                                                }

                                                if (user_access_confidentiality === 'hide') {
                                                    $(".for_user_access_confidentiality").children().not('legend').hide();
                                                    $(".for_user_access_confidentiality").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                                } else {
                                                    $(".for_user_access_confidentiality").children().not('.alert').show();
                                                    if (user_access_confidentiality === 'update') {
                                                        $(".for_user_access_confidentiality").find('a').each(function (i, item) {
                                                            $(item).parent().removeClass('static');
                                                            $(item).removeClass('disable_user_access');
                                                        });
                                                    } else {
                                                        $(".for_user_access_confidentiality").find('a').each(function (i, item) {
                                                            $(item).parent().addClass('static');
                                                            $(item).addClass('disable_user_access');
                                                        });
                                                    }
                                                }
                                                if (user_access_deactivation === 'hide') {
                                                    $('[data-target="#deactivateModal"]').hide();
                                                    $(".for_user_access_deactivation").children().not('legend').hide();
                                                    $(".for_user_access_deactivation").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                                } else {
                                                    $(".for_user_access_deactivation").children().not('.alert').show();
                                                    $('[data-target="#deactivateModal"]').show();
                                                }
                                                if (user_access_basic === 'hide') {
                                                    $(".for_user_access_basic").children().not('legend').hide();
                                                    $(".for_user_access_basic").append('<div class="alert alert-brand alert-dismissible fade show m-alert m-alert--square m-alert--air alert_for_user_access" role="alert"><strong>Access Limit!</strong> Ask admin to have access on this record.</div>');
                                                } else {
                                                    $(".for_user_access_basic").children().not('.alert').show();
                                                    if (user_access_basic === 'update') {
                                                        $(".for_user_access_basic").find('a').each(function (i, item) {
                                                            $(item).parent().removeClass('static');
                                                            $(item).removeClass('disable_user_access');
                                                        });
                                                        $("#item-img-output").parent().removeClass('disable_user_access');
                                                    } else {
                                                        $(".for_user_access_basic").find('a').each(function (i, item) {
                                                            $(item).parent().addClass('static');
                                                            $(item).addClass('disable_user_access');
                                                        });
                                                        $("#item-img-output").parent().addClass('disable_user_access');
                                                    }
                                                }
                                            }

                                        }
                                        );
                                    });
                                    $("#dtr-perpage").change(function () {
                                        var perPage = $("#dtr-perpage").val();
                                        getScheduleLogs(0, perPage, function (total) {
                                            $("#dtr-pagination").pagination('destroy');
                                            $("#dtr-pagination").pagination({
                                                items: total, //default
                                                itemsOnPage: $("#dtr-perpage").val(),
                                                hrefTextPrefix: "#",
                                                cssStyle: 'light-theme',
                                                displayedPages: 2,
                                                onPageClick: function (pagenumber) {
                                                    var perPage = $("#dtr-perpage").val();
                                                    getScheduleLogs((pagenumber * perPage) - perPage, perPage);
                                                }
                                            });
                                        });
                                    });
                                    $('#cropImagePop').on('shown.bs.modal', function () {
                                        $uploadCrop.croppie('bind', {
                                            url: rawImg
                                        }).then(function () {
                                            console.log('jQuery bind complete');
                                        });
                                        $(".croppie-rotate").click(function () {
                                            var that = this;
                                            $uploadCrop.croppie('rotate', parseInt($(that).data('deg')));
                                        });
                                    });
                                    $('#cropImageBtn').on('click', function (ev) {
                                        var emp_id = $('#m_quick_sidebar').data('emp_id');
                                        $uploadCrop.croppie('result', {
                                            type: 'canvas',
                                            format: 'jpeg',
                                            size: 'viewport'
                                        }).then(function (resp) {
                                            $('.item-img').val("");
                                            $('#item-img-output').attr('src', resp);
                                            $.ajax({
                                                url: "<?php echo base_url('Employee/uploadEmployeeImage'); ?>",
                                                type: "POST",
                                                data: {
                                                    image: resp,
                                                    emp_id: emp_id
                                                },
                                                success: function (res) {
                                                    res = JSON.parse(res.trim());
                                                    if (res.status === 'Success') {
                                                        $(".scaler[data-empid=" + emp_id + "]").find('img.employee-pic').attr('src', resp);
                                                        $.notify({
                                                            message: 'Successfully Updated'
                                                        }, {
                                                            type: 'success',
                                                            timer: 1000
                                                        });
                                                    } else {
                                                        $.notify({
                                                            message: 'An error occured while processing your request'
                                                        }, {
                                                            type: 'danger',
                                                            timer: 1000
                                                        });
                                                    }

                                                    $("#imageUploadModal").modal('hide');
                                                }
                                            });
                                            $('#cropImagePop').modal('hide');
                                        });
                                    });
                                    // End upload preview image
                                    $("#btn-backtodirectory").click(function (e) {
                                        e.preventDefault();
                                        $(".m-subheader").show();
                                        $("#main-directory").show(500, function () {
                                            $(document).scrollTop($(this).data('scroll'));
                                        });
                                        $("#single-profile").hide();
                                        $('[data-original-title]').popover('dispose');
                                    });
                                    $("html").on('click', '.popover-reject', function () {
                                        $(this).closest('.popover').popover('hide');
                                    });
                                    $("html").on('click', '.popover-approve', function () {
                                        var that = this;
                                        var elem = $(that).closest('.popover').find('.form-control');
                                        var save = $(that).closest('.popover').find('.row').data('save');
                                        var type = $(that).closest('.popover').find('.row').data('type');
                                        var original_value = $(elem).data('value');
                                        var value = ($(elem).val() === null) ? null : $(elem).val().trim();

                                        var element_name = $(elem).attr('name');
                                        var temp_name = element_name.split("-");
                                        var table = temp_name[0];
                                        var name = temp_name[1];
                                        var required = $(that).data('required');
                                        var isOkay = 'no';
                                        if (required === 'yes') {
                                            if (value === '' || value === null) {
                                                $("#notif-required").show();
                                                isOkay = 'no';
                                            } else {
                                                $("#notif-required").hide();
                                                if (type === 'number') {
                                                    if ($.isNumeric(value.trim())) {
                                                        $("#notif-number").hide();
                                                        isOkay = 'yes';
                                                    } else {
                                                        $("#notif-number").show();
                                                        isOkay = 'no';
                                                    }
                                                } else {
                                                    $("#notif-number").hide();
                                                    isOkay = 'yes';
                                                }
                                            }
                                        } else if (value !== '' && required !== 'yes') {
                                            $("#notif-required").hide();
                                            if (type === 'number') {
                                                if ($.isNumeric(value.trim())) {
                                                    $("#notif-number").hide();
                                                    isOkay = 'yes';
                                                } else {
                                                    $("#notif-number").show();
                                                    isOkay = 'no';
                                                }
                                            } else {
                                                $("#notif-number").hide();
                                                isOkay = 'yes';
                                            }
                                        } else {
                                            isOkay = 'yes';
                                        }

                                        if (isOkay === 'yes') {
                                            var emp_id = $('#m_quick_sidebar').data('emp_id');
                                            if (original_value.toString().toLowerCase() === value.toString().toLowerCase()) {
                                                $(that).closest('.popover').popover('hide');
                                            } else {
                                                if (table === 'tbl_employee_family') {

                                                    var employeeFamily_ID = temp_name[2];
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo base_url(); ?>Employee/updateEmployeeFamily",
                                                        data: {
                                                            name: name,
                                                            value: value.toString().toLowerCase(),
                                                            table: table,
                                                            employeeFamily_ID: employeeFamily_ID,
                                                            emp_id: emp_id,
                                                            original_value: original_value
                                                        },
                                                        cache: false,
                                                        success: function (res) {
                                                            res = JSON.parse(res.trim());
                                                            $(that).closest('.popover').popover('hide');
                                                            if (res.status === 'Success') {
                                                                $(that).closest('.popover').popover('dispose');
                                                                $.notify({
                                                                    message: 'Successfully Updated'
                                                                }, {
                                                                    type: 'success',
                                                                    timer: 1000
                                                                });
                                                                setFamily(res.family, res.isActive);
                                                            } else {
                                                                $.notify({
                                                                    message: 'An error occured while processing your request'
                                                                }, {
                                                                    type: 'danger',
                                                                    timer: 1000
                                                                });
                                                            }
                                                        }
                                                    });
                                                } else {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo base_url(); ?>Employee/updateEmployeeProfile",
                                                        data: {
                                                            save: save,
                                                            name: name,
                                                            value: value.toString().toLowerCase(),
                                                            table: table,
                                                            emp_id: emp_id,
                                                            original_value: original_value
                                                        },
                                                        cache: false,
                                                        success: function (res) {
                                                            res = JSON.parse(res.trim());
                                                            var employee = res.employee;
                                                            var position = res.position;
                                                            var payroll = res.payroll;
                                                            $(that).closest('.popover').popover('hide');
                                                            if (res.status === 'Success') {
                                                                $(that).closest('.popover').popover('dispose');
                                                                $.notify({
                                                                    message: 'Successfully Updated'
                                                                }, {
                                                                    type: 'success',
                                                                    timer: 1000
                                                                });
                                                                if (element_name === 'tbl_pagibig_add_contribution-amount') {
                                                                    $("#tbl_pagibig_add_contribution-amount").data('save', 'update');
                                                                } else if (element_name === 'tbl_applicant-birthday') {
                                                                    if (value === '') {
                                                                        $("#personal-age").html("No Birthday");
                                                                    } else {
                                                                        var bday = moment().diff(value, 'years');
                                                                        $("#personal-age").html(bday + " years old");
                                                                    }
                                                                } else if (element_name === 'tbl_employee-salarymode' || element_name === 'tbl_employee-workDaysPerWeek') {
                                                                    changeRates(employee.salarymode, position.rate, employee.workDaysPerWeek, payroll);
                                                                } else if (element_name === 'tbl_employee-Supervisor') {
                                                                    $("#work-supervisorEmail").html(employee.SupervisorEmail);
                                                                } else if (element_name === 'tbl_applicant-lname' || element_name === 'tbl_applicant-fname') {
                                                                    $("#image-caption-fname").html(employee.fname);
                                                                    $("#image-caption-lname").html(employee.lname);
                                                                    $(".scaler[data-empid=" + emp_id + "]").find('span.m-widget4__title').html(employee.lname + ', ' + employee.fname);
                                                                } else if (element_name === 'tbl_employee-acc_id') {
                                                                    $(".scaler[data-empid=" + emp_id + "]").find('span.m-widget4__sub').html(employee.acc_name);
                                                                }
                                                                if (value === '') {
                                                                    $("#" + element_name).html("Empty");
                                                                    $("#" + element_name).addClass('m--font-danger font-italic');
                                                                    $("#" + element_name).data('isempty', true);
                                                                    customPopover(["#" + element_name]);
                                                                } else {
                                                                    $("#" + element_name).html(value);
                                                                    $("#" + element_name).removeClass('m--font-danger font-italic');
                                                                    $("#" + element_name).data('isempty', false);
                                                                    customPopover(["#" + element_name]);
                                                                }
                                                            } else {
                                                                $.notify({
                                                                    message: 'An error occured while processing your request'
                                                                }, {
                                                                    type: 'danger',
                                                                    timer: 1000
                                                                });
                                                            }
                                                        }
                                                    });
                                                }

                                            }
                                        }
                                    });
                                    $("#btn-view-dtrCredentials").click(function (e) {
                                        e.preventDefault();
                                        swal({
                                            title: 'View DTR Credentials',
                                            text: "Enter Password",
                                            input: 'password',
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonText: 'Show'
                                        }).then((result) => {
                                            if (result.value) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/showDTRCredentials",
                                                    data: {
                                                        emp_id: $('#m_quick_sidebar').data('emp_id'),
                                                        password: result.value
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        if (res.status === "Success") {
                                                            swal({
                                                                title: "DTR Credentials",
                                                                html: '<table class="table m-table table-bordered text-left font-poppins" style="word-break: break-all;">' +
                                                                        '<tr>' +
                                                                        '<td style="width:115px"><i class="fa fa-user m--font-brand"></i> <span>Username:</span></td>' +
                                                                        '<td class="m--font-bolder">' + res.username + '</td>' +
                                                                        '<td style="width:10px" class=" text-right"><a class="m-link m--font-bolder m--font-metal"  href="#" id="clipboard-username"><i class="fa fa-clipboard"></i></a></td>' +
                                                                        '</tr>' +
                                                                        '<tr>' +
                                                                        '<td style="width:115px"><i class="fa fa-lock m--font-brand"></i> <span>Password:</span></td>' +
                                                                        '<td class="m--font-bolder">' + res.password + '</td>' +
                                                                        '<td style="width:10px" class=" text-right"><a class="m-link m--font-bolder m--font-metal"  href="#" id="clipboard-password"><i class="fa fa-clipboard"></i></a></td>' +
                                                                        '</tr></table><input type="text" style="display:none" id="clipboard"/>',
                                                                type: 'info'
                                                            });
                                                            $("#clipboard-username").click(function (e) {
                                                                e.preventDefault();
                                                                setClipboard(res.username);
                                                            });
                                                            $("#clipboard-password").click(function (e) {
                                                                e.preventDefault();
                                                                setClipboard(res.password);
                                                            });
                                                        } else {
                                                            swal('WRONG PASSWORD', 'This area is Restricted', 'error');
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    });
                                    $("#changePositionModal").on('hide.bs.modal', function (e) {
                                        if (e.namespace === 'bs.modal') {
                                            $("#changepos-dateFrom").datepicker('destroy');
                                            $("#changepos-dateFrom").val("");
                                        }
                                    });
                                    $("#btn-changePositionModal").click(function () {
                                        $("#changePositionModal").modal('show');
                                    })
                                    $("#changePositionModal").on('show.bs.modal', function (e) {
                                        if (e.namespace === 'bs.modal') {
                                            var dateFrom = $("#changePositionModal").data('datefrom');
                                            $("#changepos-dateFrom").val("");
                                            $("#changepos-dateFrom").datepicker({
                                                startDate: moment(dateFrom).toDate(),
                                                orientation: "bottom left",
                                                templates: {
                                                    leftArrow: '<i class="la la-angle-left"></i>',
                                                    rightArrow: '<i class="la la-angle-right"></i>'
                                                },
                                                format: 'yyyy-mm-dd'
                                            });
                                            $("#changePosition-alert").hide();
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url(); ?>Employee/get_all_emp_stat",
                                                data: {
                                                    dep_id: $(this).val()
                                                },
                                                cache: false,
                                                success: function (res) {
                                                    res = JSON.parse(res.trim());
                                                    $("#changepos-status").html("<option value=''>--</option>");
                                                    $.each(res.emp_stat, function (i, item) {
                                                        $("#changepos-status").append("<option value='" + item.empstat_id + "'>" + item.status + "</option>");
                                                    });
                                                }
                                            });
                                            get_all_departments(function (res) {
                                                $("#changepos-departments").html("<option value=''>All</option>");
                                                $.each(res.departments, function (i, item) {
                                                    $("#changepos-departments").append("<option value='" + item.dep_id + "'>" + item.dep_details + "</option>");
                                                });
                                                $("#changepos-departments").change();
                                            });
                                        }
                                    });
                                    $("#changepos-departments").change(function () {

                                        $("#btn-saveChangePosition").prop('disabled', true);
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url(); ?>Employee/getPositionPerDepartment",
                                            data: {
                                                dep_id: $(this).val()
                                            },
                                            cache: false,
                                            success: function (res) {
                                                res = JSON.parse(res.trim());
                                                $("#changepos-positions").html("<option value=''>--</option>");
                                                $.each(res.positions, function (i, item) {
                                                    $("#changepos-positions").append("<option value='" + item.pos_id + "'>" + item.pos_details + "</option>");
                                                });
                                            }
                                        });
                                    });
                                    $("#changepos-status,#changepos-positions,#changepos-dateFrom").change(function () {
                                        var status = $("#changepos-status").val();
                                        var position = $("#changepos-positions").val();
                                        var dateFrom = $("#changepos-dateFrom").val();
//                var statustext = $("#changepos-status option:selected").text();
//                var positiontext = $("#changepos-positions option:selected").text();
                                        $("#changePosition-alert").hide();
                                        if (status === '' || position === '' || dateFrom === '') {
                                            $("#changePosition-alert").hide();
                                            $("#btn-saveChangePosition").prop('disabled', true);
                                        } else {
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url(); ?>Employee/rateChecker",
                                                data: {
                                                    empstat_id: status,
                                                    pos_id: position
                                                },
                                                cache: false,
                                                success: function (res) {
                                                    res = JSON.parse(res.trim());
                                                    if (res.status === 'Success') {
                                                        $("#changePosition-alert").hide();
                                                        $("#btn-saveChangePosition").prop('disabled', false);
                                                    } else {
                                                        $("#changePosition-alert").show();
                                                        $("#btn-saveChangePosition").prop('disabled', true);
                                                    }
                                                }
                                            });
                                        }
                                    });
                                    $("#btn-saveChangePosition").click(function () {
                                        var status = $("#changepos-status").val();
                                        var position = $("#changepos-positions").val();
                                        var dateFrom = $("#changepos-dateFrom").val();
                                        var emp_id = $('#m_quick_sidebar').data('emp_id');
                                        swal({
                                            title: 'Are you sure?',
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonText: 'Yes',
                                            cancelButtonText: 'No'
                                        }).then((result) => {
                                            if (result.value) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/changePosition",
                                                    data: {
                                                        empstat_id: status,
                                                        pos_id: position,
                                                        dateFrom: dateFrom,
                                                        emp_id: emp_id
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        if (res.status === "Success") {
                                                            setPositions();
                                                            $("#changePositionModal").modal('hide');
                                                            $.notify({
                                                                message: 'Successfully changed position.'
                                                            }, {
                                                                type: 'success',
                                                                timer: 1000
                                                            });
                                                        } else {
                                                            $("#changePositionModal").modal('hide');
                                                            $.notify({
                                                                message: 'An error occured while processing your request'
                                                            }, {
                                                                type: 'danger',
                                                                timer: 1000
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    });
                                    $("#addFamilyModal").on('show.bs.modal', function (e) {
                                        $("#addFamily-alert").hide();
                                        $("#addfamily-fname").val('');
                                        $("#addfamily-lname").val('');
                                        $("#addfamily-mname").val('');
                                        $("#addfamily-relation").val('');
                                    });
                                    $("#btn-saveAddFamily").click(function () {
                                        var fname = $("#addfamily-fname").val();
                                        var lname = $("#addfamily-lname").val();
                                        var mname = $("#addfamily-mname").val();
                                        var relation = $("#addfamily-relation").val();
                                        if (fname === '' || lname === '' || mname === '' || relation === '') {
                                            $("#addFamily-alert").show();
                                        } else {
                                            $("#addFamily-alert").hide();
                                            var emp_id = $('#m_quick_sidebar').data('emp_id');
                                            swal({
                                                title: 'Are you sure?',
                                                type: 'warning',
                                                showCancelButton: true,
                                                confirmButtonText: 'Yes',
                                                cancelButtonText: 'No'
                                            }).then((result) => {
                                                if (result.value) {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo base_url(); ?>Employee/addFamily",
                                                        data: {
                                                            fname: fname,
                                                            lname: lname,
                                                            mname: mname,
                                                            relation: relation,
                                                            emp_id: emp_id
                                                        },
                                                        cache: false,
                                                        success: function (res) {
                                                            res = JSON.parse(res.trim());
                                                            if (res.status === "Success") {
                                                                setFamily(res.family, res.isActive);
                                                                $("#addFamilyModal").modal('hide');
                                                                $.notify({
                                                                    message: 'Successfully added family member.'
                                                                }, {
                                                                    type: 'success',
                                                                    timer: 1000
                                                                });
                                                            } else {
                                                                $("#addFamilyModal").modal('hide');
                                                                $.notify({
                                                                    message: 'An error occured while processing your request'
                                                                }, {
                                                                    type: 'danger',
                                                                    timer: 1000
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }

                                    });
                                    $("#deactivateModal").on('hide.bs.modal', function (e) {
                                        if (e.namespace === 'bs.modal') {
                                            $("#deactivate-finalInterviewDate").val('');
                                            $("#deactivate-isRehire").val('');
                                            $("#deactivate-separationDate").val('');
                                            $("#deactivate-reasonForLeaving-select").val('');
                                            $("#deactivate-reasonForLeaving-input").val('');
                                            $("#deactivate-employeeComments").val('');
                                            $("#deactivate-interviewerComments").val('');
                                        }
                                    });
                                    $("#deactivateModal").on('show.bs.modal', function (e) {
                                        if (e.namespace === 'bs.modal') {
                                            $("#deactivate-finalInterviewDate").datepicker({
                                                orientation: "bottom left",
                                                templates: {
                                                    leftArrow: '<i class="la la-angle-left"></i>',
                                                    rightArrow: '<i class="la la-angle-right"></i>'
                                                },
                                                format: 'yyyy-mm-dd'
                                            });
                                            $("#deactivate-separationDate").datepicker({
                                                orientation: "top left",
                                                templates: {
                                                    leftArrow: '<i class="la la-angle-left"></i>',
                                                    rightArrow: '<i class="la la-angle-right"></i>'
                                                },
                                                format: 'yyyy-mm-dd'
                                            });
                                            $("#deactivate-alert").hide();

                                            var emp_id = $('#m_quick_sidebar').data('emp_id');
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url(); ?>Employee/getDeactivateDetails",
                                                data: {
                                                    emp_id: emp_id
                                                },
                                                cache: false,
                                                success: function (res) {
                                                    res = JSON.parse(res.trim());
                                                    var details = res.deactivateDetails;
                                                    $("#deactivate-name").html(details.name);
                                                    $("#deactivate-id_num").html(details.id_num);
                                                    $("#deactivate-team").html(details.team);
                                                    $("#deactivate-supervisor").html(details.supervisor);
                                                    $("#deactivate-hireDate").html(moment(details.hireDate).format('MMM DD, YYYY'));
                                                    $("#deactivate-startposition").html(details.startposition);
                                                    $("#deactivate-endposition").html(details.endposition);
                                                }
                                            });
                                        }
                                    });
                                    $("#deactivate-reasonForLeaving-select").change(function () {
                                        $("#deactivate-reasonForLeaving-input").val("");
                                    });
                                    $("#deactivate-reasonForLeaving-input").change(function () {
                                        $("#deactivate-reasonForLeaving-select").val("");
                                    });
                                    $("#btn-saveDeactivate").click(function () {
                                        var finalInterviewDate = $("#deactivate-finalInterviewDate").val();
                                        var isRehire = $("#deactivate-isRehire").val();
                                        var separationDate = $("#deactivate-separationDate").val();
                                        var reasonForLeavingSelect = $("#deactivate-reasonForLeaving-select").val();
                                        var reasonForLeavingInput = $("#deactivate-reasonForLeaving-input").val();
                                        var reasonForLeaving = (reasonForLeavingSelect === '') ? reasonForLeavingInput : reasonForLeavingSelect;
                                        var employeeComments = $("#deactivate-employeeComments").val();
                                        var interviewerComments = $("#deactivate-interviewerComments").val();
                                        var emp_id = $('#m_quick_sidebar').data('emp_id');
                                        if (finalInterviewDate === '' || isRehire === '' || separationDate === '' || employeeComments === '' || interviewerComments === '') {
                                            $("#deactivate-alert").show();
                                        } else if (reasonForLeavingSelect === '' && reasonForLeavingInput === '') {
                                            $("#deactivate-alert").show();
                                        } else {
                                            $("#deactivate-alert").hide();
                                            swal({
                                                title: 'Are you sure?',
                                                type: 'warning',
                                                showCancelButton: true,
                                                confirmButtonText: 'Yes',
                                                cancelButtonText: 'No'
                                            }).then((result) => {
                                                if (result.value) {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo base_url(); ?>Employee/deactivateEmployee",
                                                        data: {
                                                            finalInterviewDate: finalInterviewDate,
                                                            isRehire: isRehire,
                                                            separationDate: separationDate,
                                                            reasonForLeaving: reasonForLeaving,
                                                            employeeComments: employeeComments,
                                                            interviewerComments: interviewerComments,
                                                            emp_id: emp_id
                                                        },
                                                        cache: false,
                                                        success: function (res) {
                                                            res = JSON.parse(res.trim());
                                                            if (res.status === "Success") {
                                                                $("#btn-backtodirectory").click();
                                                                $("#deactivateModal").modal('hide');
                                                                $.notify({
                                                                    message: 'Successfully deactivated employee.'
                                                                }, {
                                                                    type: 'success',
                                                                    timer: 1000
                                                                });
                                                                $("#perpage").change();
                                                            } else {
                                                                $("#deactivateModal").modal('hide');
                                                                $.notify({
                                                                    message: 'An error occured while processing your request'
                                                                }, {
                                                                    type: 'danger',
                                                                    timer: 1000
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });

                                    $("#addPrevAccountsModal").on('show.bs.modal', function (e) {
                                        if (e.namespace === 'bs.modal') {
                                            $("#table-inactivelist thead").hide();
                                            $("#table-inactivelist tbody").html("");
                                            $("#prev-searchname").val("");
                                        }
                                    });
                                    $("#prev-searchname").change(function () {
                                        $("#table-inactivelist tbody").html("");
                                        var options_string = "";
                                        getInactiveEmployees(function (emps) {
                                            $.each(emps.employees, function (iterator, inactiveemps) {
                                                var separationDate = moment((inactiveemps.separationDate === 'null') ? 'Unknown' : inactiveemps.separationDate).format('MMM DD, YYYY');
                                                var dateFrom = moment((inactiveemps.dateFrom === 'null') ? 'Unknown' : inactiveemps.dateFrom).format('MMM DD, YYYY');
                                                var separationDateString = (separationDate === 'Invalid date') ? 'Unknown' : separationDate;
                                                var dateFromString = (dateFrom === 'Invalid date') ? 'Unknown' : dateFrom;
                                                var checked = (inactiveemps.checked === 'yes') ? 'checked' : '';
                                                options_string += '<tr>'
                                                        + '<td class="font-12">' + inactiveemps.lname + ', ' + inactiveemps.fname + '</td>'
                                                        + '<td class="font-12">' + separationDateString + ' - ' + dateFromString + '</td>'
                                                        + '<td><button class="btn m-btn btn-green btn-sm btn-addPrevAccounts" data-empid="' + inactiveemps.emp_id + '">Add</button></td>'
                                                        + '</tr>';
                                            });
                                            $("#table-inactivelist tbody").html(options_string);
                                            $("#table-inactivelist thead").show();
                                        });
                                    });
                                    $("#table-inactivelist").on("click", ".btn-addPrevAccounts", function () {
                                        var that = this;
                                        var previousEmployee_ID = $(that).data('empid');
                                        var currentEmployee_ID = $('#m_quick_sidebar').data('emp_id');
                                        swal({
                                            title: 'Are you sure?',
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonText: 'Yes',
                                            cancelButtonText: 'No'
                                        }).then((result) => {
                                            if (result.value) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/addPreviousAccount",
                                                    data: {
                                                        previousEmployee_ID: previousEmployee_ID,
                                                        currentEmployee_ID: currentEmployee_ID
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        if (res.status === "Success") {
                                                            setPrevAccounts();
                                                            $.notify({
                                                                message: 'Successfully added account.'
                                                            }, {
                                                                type: 'success',
                                                                timer: 1000
                                                            });
                                                        } else {
                                                            $.notify({
                                                                message: 'An error occured while processing your request'
                                                            }, {
                                                                type: 'danger',
                                                                timer: 1000
                                                            });
                                                        }
                                                        $("#addPrevAccountsModal").modal('hide');
                                                    }
                                                });
                                            }
                                        });

                                    });
                                    $("#table-previousAccounts").on("click", ".btn-deletePrevAccounts", function () {
                                        var that = this;
                                        var previousEmployee_ID = $(that).data('empid');
                                        var currentEmployee_ID = $('#m_quick_sidebar').data('emp_id');
                                        swal({
                                            title: 'Are you sure?',
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonText: 'Yes',
                                            cancelButtonText: 'No'
                                        }).then((result) => {
                                            if (result.value) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Employee/deletePreviousAccount",
                                                    data: {
                                                        previousEmployee_ID: previousEmployee_ID,
                                                        currentEmployee_ID: currentEmployee_ID
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());
                                                        if (res.status === "Success") {
                                                            setPrevAccounts();
                                                            $.notify({
                                                                message: 'Successfully removed account.'
                                                            }, {
                                                                type: 'success',
                                                                timer: 1000
                                                            });
                                                        } else {
                                                            $.notify({
                                                                message: 'An error occured while processing your request'
                                                            }, {
                                                                type: 'danger',
                                                                timer: 1000
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    });
                                });
</script>

<script type="text/javascript">
        //$.ajax({
        //                                    type: "POST",
        //                                    url: "<?php echo base_url(); ?>Employee/allowDirectoryAccess",
        //                                    cache: false,
        //                                    success: function (res) {
        //                                        
        //                                    }
        //                                });
        //  swal({
        //                    title: 'ENTER PASSWORD',
        //                    input: 'password',
        //                    type: 'warning',
        //                    showCancelButton: true,
        //                    confirmButtonText: 'Show'
        //                }).then((result) => {
        //                    if (result.value) {
        //                        $.ajax({
        //                            type: "POST",
        //                            url: "<?php echo base_url(); ?>Employee/allowDirectoryAccess",
        //                            data: {
        //                                password: result.value
        //                            },
        //                            cache: false,
        //                            success: function (res) {
        //                                res = JSON.parse(res.trim());
        //                                if (res.status === "Success") {
        //                                    console.log("HEREEEE");
        ////                                    $("#profile-modal").modal("show");
        //                                    $("#m_quick_sidebar_toggle").click();
        //
        //                                } else {
        //                                    swal('WRONG PASSWORD', 'This area is Restricted', 'error');
        //                                }
        //                            }
        //                        });
        //                    }
        //                });
</script>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     