<style>
    .custom-txt-center{
        margin: 0 auto;
    }
    .pCelebNames .Male{
        font-size: 1.35em;
        color: #00aabd;
    }
    .pCelebNames .Female{
        font-size: 1.35em;
        color: #f4516c;
    }
    .pCelebNames {
        font-size: smaller;
    }
    .tooltip-inner {
        text-align: left;
        color: #fff !important;
        font-size: 1em !important;
    }
    .highcharts-exporting-group,.highcharts-credits{
        display:none
    }
    .m-portlet__head{
        height:50px !important;
    }
    .m-portlet__body{
        background:white !important;
    }
    .highcharts-title{
        font-family:'Poppins' !important;
    }
.bs-popover-auto[x-placement^=right] .arrow::after, .bs-popover-right .arrow::after {
border-right-color: #282A3C !important;
}
.bs-popover-auto[x-placement^=left] .arrow::after, .bs-popover-left .arrow::after {
border-left-color: #282A3C !important;
}
.popover-header{
background-color: rgb(52, 55, 78)!important;
border-bottom: 0px !important;
color: white;
max-width: 100% !important;
}
.popover-body{
background-color: #282A3C!important;
max-width: 100% !important;
}

.popover-body a {
    color: #a9aabf;
}
.popover-body a:hover {
    color: white;
    text-decoration: none;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <div class="m-content">
        <div class="row">
            <!-- IMPORTANT -->
            
            <!-- Alert Icons & Action Buttons -->
			<!----Start Block of Announcement----->
			<div class="col-lg-12">
                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show" style="" role="alert">
                    <div class="m-alert__icon" style="padding: 0.45rem 0.75rem;">
                        <i class="fa fa-info-circle" style="font-size: 40px;color: white;"></i>
                        <span></span>
                    </div>
                    <div class="m-alert__text" style="padding-bottom: 0px;">
                        <h2>Announcement!!!</h2>
						<p>We will be having a System Maintenance this  <b>February 24, 2018, Sunday </b>. With this, the system will be temporarily down and inaccessible from <b>1 PM - 10 PM </b>. For those who will be affected, we advise to file a TITO request later after the System will be up again.</p>
						<p>Thank you very much.</p>

 						<blockquote class="blockquote pull-right" style="width: 280px;">
                           <img src="<?php echo base_url("assets/sz-dance.gif"); ?>" style="width:15%;float:left"> <footer class="blockquote-footer" style="margin-top: 10px;"><cite title="Announcement From">Software Programming Team</cite></footer>
                       </blockquote>

                        <!-- ANOUNCEMENT HERE -->
                    </div>			  	
                </div>
            </div>
			<!----End Block of Announcement----->
            <div class="col-lg-12" id="approversDiv" style="display:none">
                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-warning fade show" style="color: #b77714;" role="alert" id="pendingAppr">
                    <div class="m-alert__icon" style="padding: 0.45rem 0.75rem;">
                        <i class="fa fa-info-circle" style="font-size: 40px;color: white;"></i>
                        <span></span>
                    </div>
                    <div class="m-alert__text">
                        </i>You currently have <strong id="pendingApprPopoverVal"></strong> pending request <span id="apprPendingWordForm"></span>.		
                    </div>
                    <div class="m-alert__actions" style="padding: 1.05rem 1.75rem 1.05rem 1.25rem !important;">
						<button type="button" id="pendingApprPopover" class="btn btn-sm m-btn--pill btn-outline-warning m-btn m-btn--custom m-btn--outline-2x">View Details</button>
					</div>				  	
                </div>
                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show" style="color: #b77714;" role="alert" id="deadTodayAppr">
                    <div class="m-alert__icon" style="padding: 0.45rem 0.75rem;">
                        <i class="fa fa-exclamation-circle" style="font-size: 40px;color: white;"></i>
                        <span></span>
                    </div>
                    <div class="m-alert__text">
                        You have <strong id="deadTodayApprPopoverVal"></strong> request <span id="apprDeadWordForm">approvals</span> that will be due by 11:59 PM today.
                    </div>
                    <div class="m-alert__actions" style="padding: 1.05rem 1.75rem 1.05rem 1.25rem !important;">
						<button type="button" id="deadTodayApprPopover" class="btn btn-sm m-btn--pill btn-outline-danger m-btn m-btn--custom m-btn--outline-2x">View Details</button>
					</div>
                </div>
                <!-- <div class="m-alert m-alert--outline m-alert--outline-2x alert alert-warning alert-dismissible fade show" role="alert" id="pendingAppr">
				  	<i class="fa fa-info-circle"></i>You currently have <a href="#" class="btn p-1" data-toggle="popover"  id="pendingApprPopover" >24</a> request approvals.
				</div>
                <div class="m-alert m-alert--outline m-alert--outline-2x alert alert-danger alert-dismissible fade show" role="alert" id="deadTodayAppr">
				  	<i class="fa fa-info-circle"></i>You have <a href="#" class="btn p-1" data-toggle="popover"  id="deadTodayApprPopover" >24</a> request approvals that will be due by 11:59 PM today.
				</div>                 -->
            </div>
            <!-- END OF IMPORTANT -->
            <!---- START OF FIRST DIV ----->
            <div class="col-lg-4">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit  ">
                    <div class="m-portlet__head"  style="background:#36A3F7">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa	fa-birthday-cake"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;" >
                                    Birthday Celebrants Today!
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="padding:5px 15px 0px 15px;background:white">	
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="225" id="divBday">
                                <div class="m-widget4">
                                    <?php
                                    if (isset($celebrants) && count($celebrants) > 0)
                                    {
                                        foreach ($celebrants as $row => $val)
                                        {
                                            $bday = date_format(date_create($val["birthday"]), "F d, Y");
                                            ?>
                                                                                                                                                                                                    <!--<p class="pCelebNames"> <i class="la la-info-circle <?php echo $val["gender"]; ?>" data-skin="dark" data-html="true" data-toggle="m-tooltip" data-placement="right" data-original-title="<div style='padding:10px 5px;!important'><span style='color:#ccc'>Fullname :</span><br /> <?php echo $val["fname"] . ' ' . $val["lname"]; ?> <br><br> <span style='color:#ccc'>Position :</span><br /> <small><?php echo $val["job"][0]->pos_details; ?></small> <br><br> <span style='color:#ccc'>Account :</span><br /> <small><?php echo $val["acc_name"]; ?></small> <br><br> <span style='color:#ccc'>Birthday :</span><br /> <small><?php echo $bday; ?></small></div>"></i><span class="m-topbar__userpic"> <img src="<?php echo base_url("/assets/images/" . $val["pic"]); ?>" class="m--img-rounded m--marginless m--img-centered header-userpic<?php echo $val["emp_id"] ?>" onerror="noImage(<?php echo $val["emp_id"]; ?>)"  alt=""></span> &nbsp;&nbsp;<?php echo $val["fname"] . ' ' . $val["lname"]; ?> </p>-->


                                            <div class="m-widget4__item pCelebNames">
                                                <div class="m-widget4__img m-widget4__img--logo">							 
                                                    <img src="<?php echo base_url("/assets/images/" . $val["pic"]); ?>" class="header-userpic<?php echo $val["emp_id"] ?>" alt="" onerror="noImage(<?php echo $val["emp_id"]; ?>)" style="width: 40px;">   
                                                </div>
                                                <div class="m-widget4__info">
                                                    <span class="m-widget4__title">
                                                        <?php echo $val["fname"] . ' ' . $val["lname"]; ?> 
                                                    </span>
                                                    </span>		 
                                                </div>
                                                <span class="m-widget4__ext">
                                                    <span class="m-widget4__number">
                                                        <i class="la la-info-circle <?php echo $val["gender"]; ?>" data-skin="dark" data-html="true" data-toggle="m-tooltip" data-placement="right" data-original-title="<div style='padding:10px 5px;!important'><span style='color:#ccc'>Fullname :</span><br /> <?php echo $val["fname"] . ' ' . $val["lname"]; ?> <br><br> <span style='color:#ccc'>Position :</span><br /> <small><?php echo $val["job"][0]->pos_details; ?></small> <br><br> <span style='color:#ccc'>Account :</span><br /> <small><?php echo $val["acc_name"]; ?></small> <br><br> <span style='color:#ccc'>Birthday :</span><br /> <small><?php echo $bday; ?></small></div>"></i>
                                                    </span>
                                                </span>	
                                            </div>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        ?>
    <!--<span style="font-size: x-large;margin: 0 auto;"><i class="socicon-stackoverflow"></i> No Celebrants Today!</span>-->
                                        <div class="alert m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert" style="border:2px solid #7B7E8A">
                                            <strong><i class="socicon-stackoverflow"></i></strong> No Celebrants Today!					  	
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit  ">
                    <div class="m-portlet__head"  style="background:#34BFA3">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa fa-thumbs-o-up"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;" >
                                    Kudos
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="padding:15px;background:white">	
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="225" >
                                <div class="alert m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert" style="border:2px solid #7B7E8A">
                                    <strong><i class="socicon-stackoverflow"></i></strong>  In Progress..						  	
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!---- END OF FIRST DIV ----->

            <!---- START OF 2nd DIV ----->
            <div class="col-lg-4">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit ">
                    <div class="m-portlet__head"style="background:#666">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa flaticon-time-2"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;" >
                                    My Daily Time Record 
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="padding:15px;background:white">	
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="225" >
                                <div class="alert m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert" style="border:2px solid #7B7E8A">
                                    <strong><i class="socicon-stackoverflow"></i></strong>  In Progress..					  	
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit  ">
                    <div class="m-portlet__head" style="background:#F4516C">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa fa-envelope-square"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;" >
                                    My Request
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="padding:15px;background:white" >	
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="225" >
                                <div class="alert m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert" style="border:2px solid #7B7E8A">
                                    <strong><i class="socicon-stackoverflow"></i></strong> In Progress..					  	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!---- END OF 2nd DIV ----->
            <!---- START OF 3rd DIV ----->
            <div class="col-lg-4">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__head" style="background:#716ACA">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa fa-user-circle-o"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;" >
                                    My Stuff
                                </h5>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body" style="padding:0px 15px 15px 0px;background:white">	
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="550">
                                <div id="highchart-credits" style="height:220px"></div>
                                <div style="padding-left:15px">
                                    <hr />
                                    <table class="table m-table m-table--head-separator-primary table-sm text-center" style="font-size:12px;" id="table-credits">
                                        <thead class="thead-inverse">
                                            <tr>
                                                <th class="text-left">Leave Name</th>
                                                <th>Total</th>
                                                <th>Taken</th>
                                                <th>Remaining</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <button type="button" class="btn btn-brand m-btn m-btn--custom m-btn--icon btn-block m--hide"><span class="pull-left">View More</span><i class="fa fa-arrow-right pull-right" style="margin-top: 8px;"></i></button>

                                    <hr />

                                    <div style="padding-left:15px">
                                        <!--MICMIC OVERTIME CODES-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!---- END OF 3rd DIV ----->
        </div>
    </div>
</div>
<script>

        $(function () {
            //GET LEAVE CREDITS---------------------------------------------------------------------------
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Dashboard/get_leave_credits",
                cache: false,
                beforeSend: function () {
                    console.log("HERE");
                },
                success: function (res) {
                    res = res.trim();
                    res = JSON.parse(res);
                    var leave_credits = res.leave_credits;
                    var series = [];
                    $.each(leave_credits, function (i, item) {
                        var value = item.remaining;
                        if (value === null) {
                            value = 0;
                        }
                        series.push({name: item.leaveType, y: parseFloat(value)});
                    });
                    Highcharts.chart('highchart-credits', {
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: "<?php echo date('Y'); ?> Leave Credits",
                            align: 'center',
                            verticalAlign: 'bottom',
                            margin: 50,
                            floating: true
                        },
                        subtitle: null,
                        plotOptions: {
                            pie: {
                                innerSize: 65,
                                depth: 40,
                                dataLabels: {
                                    distance: 5
                                }
                            }
                        },
                        colors: ["#ffc855", "#e6558b", "#4f74ee"],
                        series: [{
                                name: 'Remaining Credits',
                                data: series
                            }]
                    });
                    $("#table-credits tbody").html("");
                    var string = "";
                    $.each(leave_credits, function (i, item) {
                        var itemcredits = (item.credits === null) ? 'No Data' : item.credits;
                        var itemtaken = (item.taken === null) ? 'No Data' : item.taken;
                        var itemremaining = (item.remaining === null) ? 'No Data' : item.remaining;
                        string += "<tr>";
                        string += "<td class='text-left'>" + item.leaveType + " Leave</td>";
                        string += "<td>" + itemcredits + "</td>";
                        string += "<td>" + itemtaken + "</td>";
                        string += "<td>" + itemremaining + "</td>";
                        string += "</tr>";
                    });
                    $("#table-credits tbody").html(string);
                }
            });
            //END OF LEAVE CREDITS---------------------------------------------------------------------------

            //  GET PENDING APPROVALS ------------------------------------------------------------------------ 
            // var showApprovalNotice = 0;           
            getPendingAppr(function(pendingNoticeCount){
                getTodayDeadAppr(function(deadNoticeCount){
                    if((pendingNoticeCount > 0) || (deadNoticeCount > 0)){
                        $('#approversDiv').show();
                        var title = "Approval Request Notice";
                        var link = baseUrl+"/dashboard";
                        var icon = baseUrl+"/assets/images/img/happy.gif";
                        var appr = "Approval";
                        if(pendingNoticeCount > 1){
                            appr = "Approvals";
                        }
                        if(pendingNoticeCount == deadNoticeCount){
                            var body = "You currently have "+pendingNoticeCount+" Pending "+appr+" which will be due by 11:59 PM today.";
                        }else{
                            if((pendingNoticeCount > 0) && (deadNoticeCount == 0)){
                                var body = "You currently have "+pendingNoticeCount+" Pending "+appr+" as of the moment.";
                            }else{
                                var body = "You currently have "+pendingNoticeCount+" Pending "+appr+". "+deadNoticeCount+" out of it will by due by 11:59 PM today.";
                            }
                        }
                        showDeskopNotif(title, body, icon, link);
                    }else{
                        $('#approversDiv').hide();                       
                    }
                })
            })

            

            // END OF PENDING APPROVALS ---------------------------------------------------------------------
        });
</script>