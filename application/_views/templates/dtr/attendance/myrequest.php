<style>
.datepicker tbody tr>td.day {
		color: #000000;
		background: #dfdfdf;
	}
	.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
		color: #777 !important;
		cursor: default;
		background: #fff !important;
	}
	.datepicker tbody tr>td.day.active, .datepicker tbody tr>td.day.active:hover, .datepicker tbody tr>td.day.selected, .datepicker tbody tr>td.day.selected:hover {
		background: #cccccc;
		color: #000;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">My Time-in & Time-out Request</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">My Time-in & Time-out Request</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		 <div class="m-portlet m-portlet--tabs">
                    <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                                 <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i> Request</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i> Records</a>
                                </li>
								<a   data-toggle="modal" data-target="#m_datepicker_modal" >
								 <button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right" style="right: 0%;position: absolute;margin-top: 1%;"  onclick="loadSchedules(<?php echo $_SESSION["acc_id"]; ?>)"  data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="File Time-in Time-out request here.">
                                    <i class="fa fa-plus-circle"></i>
								</button> </a>
                            </ul>
                        </div>
                    </div>
            <div class="m-portlet__body">

                <div class="tab-content">
                    <div class="tab-pane active" id="m_tabs_6_1" role="tabpanel">
					 <div class="m-portlet__head-tools">
			  
						<div class="m-form m-form--label-align-right">
							<div class="m-input-icon m-input-icon--left">
								<input type="text" class="form-control m-input" placeholder="Type any keyword..." id="titoPendingSearch">
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-search"></i>
									</span>
								</span>
							</div>
						</div>
                            
                    </div> 
                        <div class="m_datatable_pending"></div>
                    </div>
                   
                    <div class="tab-pane" id="m_tabs_6_3" role="tabpanel">
					 <div class="m-portlet__head-tools">
                      
                                <div class="m-form m-form--label-align-right">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Type any keyword..." id="titoRecordSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            
                    </div>
                       <div class="m_datatable_record"></div>
                    </div>
                </div>  

               
 
            </div>
        </div>
		</div>
	</div>

<div id="showDetails" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	   <h4 class="modal-title" id="headerTitle"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body">
		  <div id="bodyShowDetails">
 		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div class="modal fade" id="m_datepicker_modal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelz" data-backdrop="static" data-keyboard="false" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"> <i class="flaticon-time-2"></i> Request Time-in & Time-out</h5>
				
			</div>
			<form class="m-form m-form--fit m-form--label-align-right form-horizontal" id="defaultForm" method="post" >
 				<div class="modal-body">
					 <div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Schedule Date</label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input" readonly  placeholder="Select date" id="dateReq" name="dateRequested" />
							</div>
							<span class="m-form__help"></span>
						</div>
						<div class="col-lg-6">
							Note:<br/> 
							<span id="showShift" data-totalrequestcheck="0">Please pick a date. The date allowed to be chosen is only between from the past week and a week ahead from the current date.</span>
						</div>
					</div>	
					<hr/>
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Date of Actual Work:</label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input datePick checkTimeSched" readonly  placeholder="Select date" id="dateActualWorked" value="<?php echo date("Y-m-d"); ?>"/>
								<div class="input-group-append">
									<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
							<span class="m-form__help">Please pick a date.</span>
						</div>
						<div class="col-lg-6">
							<label>Schedule:</label>
								<div class="input-group" >
									<select class="form-control m-select2 checkTimeSched" id="mySelectSched" name="listschedule"></select>
								</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Date Started:</label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input " id="reqDateStart" readonly  placeholder="Select date" id="dateActStart" style="padding: 0px;text-align: center;" value="<?php echo date("Y-m-d"); ?>"/>
								<div class="input-group-append">
									<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
 						</div>
						<div class="col-lg-3">
						<label>Time Started:</label>
							<div class='input-group'>
								<input type='text' class="form-control m-input timepicker " id="reqTimeStart"  readonly placeholder="Select time" type="text" onchange="checkTotalTimeStatus()"/>
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<label>Date Ended:</label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input" id="reqDateEnd" readonly  placeholder="Select date" id="dateActEnd" style="padding: 0px;text-align: center;" value="<?php echo date("Y-m-d"); ?>"/>
								<div class="input-group-append">
									<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
 						</div>
						<div class="col-lg-3">
						<label>Time Ended:</label>
							<div class='input-group'>
								<input type='text' class="form-control m-input timepicker" id="reqTimeEnd" readonly placeholder="Select time" type="text" onchange="checkTotalTimeStatus()"/>
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
								<label id="noteReqDateTime" data-timetotal=""> </label>
						</div>
					</div>
					<hr/>
					<div class="form-group m-form__group">
						<label for="exampleTextarea">Reason</label>
						<textarea class="form-control m-input" id="exampleTextarea" rows="3"  name="reqReason"></textarea>
					</div>
					 
					 
				</div>
				<div class="modal-footer">
					<button type="" class="btn btn-secondary m-btn" data-dismiss="modal" onclick="resetForm()">Close</button>
 					<button type="submit" class="btn btn-success m-btn" id="btnSubmit">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
 <script src="<?php echo base_url();?>assets/src/demo/default/custom/components/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>

 <script>

var UserAnswerDatatable = function () {
    var userAnswerSelector = function (data) {
	
        var user_id = <?php echo $_SESSION["uid"]; ?>;
         var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/dtr/listofmyrequest',
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
                        params: {
                            query: {
                                user_id: user_id,
                             },
                        },
                    }
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#titoRecordSearch'),
            },
            columns: [{
                field: "dtrRequest_ID",
                title: "TITO ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                     var dtrRequest_ID = row.dtrRequest_ID.padLeft(8);
                    var html = "<tr class='row'><td class = 'col-md-1'>" + dtrRequest_ID + "</td>";
                    return html;
                }
            },{
                field: "dateRequest",
                title: "DATE",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<tr class='row'><td class = 'col-md-12'>" +  row.dateRequest + "</td>";
                    return html;
                }
            },{
                field: "fname",
                title: "DATE & TIME REQUEST",
                width: 200,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<tr class='row'><td class = 'col-md-12'>" + row.startDateWorked+" "+row.startTimeWorked+"<br>"+row.endDateWorked+" "+row.endTimeWorked+ "</td>";
                    return html;
                }
            },{
                field: "timetotal",
                title: "Total",
                width: 50,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<tr class='row'><td class = 'col-md-12'>" + row.timetotal+ "</td>";
                    return html;
                }
            },{
                field: "description",
                title: "MESSAGE",
                width: 200,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
					if (parseInt(row.status_ID) == 12) {
							var badgeColor = 'btn-warning';
						} else if (parseInt(row.status_ID) == 2) {
							var badgeColor = 'btn-info';
						} else if (parseInt(row.status_ID) == 5) {
							var badgeColor = 'btn-success';
						} else if (parseInt(row.status_ID) == 6) {
							var badgeColor = 'btn-danger';
						}else if (parseInt(row.status_ID) == 7) {
							var badgeColor = 'btn-primary';
						}
						var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide' onclick=viewRecord('approver',"+row.dtrRequest_ID+")>" + row.description + "</span>";
						return html;
					
                }
            },{
                field: "action",
                title: "ACTION",
                width: 200,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = '<a href="#" class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" onclick=viewRecord("record",'+row.dtrRequest_ID+')><i class="fa fa-search"></i></a>';
                    return html;
                }
            },
			],
        };

        $('.m_datatable_record').mDatatable(options);

    };

    return {
        init: function (){
            userAnswerSelector();
        }
    };
}();
var pendingTitoRequest = function () {
    var userAnswerSelector = function (data) {
	
        var user_id = <?php echo $_SESSION["uid"]; ?>;
         var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/dtr/listofmypendingrequest',
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
                        params: {
                            query: {
                                user_id: user_id,
                             },
                        },
                    }
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#titoPendingSearch'),
            },
            columns: [{
                field: "dtrRequest_ID",
                title: "TITO ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                     var dtrRequest_ID = row.dtrRequest_ID.padLeft(8);
                    var html = "<tr class='row'><td class = 'col-md-1'>" + dtrRequest_ID + "</td>";
                    return html;
                }
            },{
                field: "dateRequest",
                title: "DATE",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<tr class='row'><td class = 'col-md-12'>" +  row.dateRequest + "</td>";
                    return html;
                }
            },{
                field: "fname",
                title: "DATE & TIME REQUEST",
                width: 200,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<tr class='row'><td class = 'col-md-12'>" + row.startDateWorked+" "+row.startTimeWorked+"<br>"+row.endDateWorked+" "+row.endTimeWorked+ "</td>";
                    return html;
                }
            },{
                field: "timetotal",
                title: "Total",
                width: 50,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<tr class='row'><td class = 'col-md-12'>" + row.timetotal+ "</td>";
                    return html;
                }
            },{
                field: "description",
                title: "MESSAGE",
                width: 200,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
						if (parseInt(row.status_ID) == 12) {
							var badgeColor = 'btn-warning';
						} else if (parseInt(row.status_ID) == 2) {
							var badgeColor = 'btn-info';
						} else if (parseInt(row.status_ID) == 5) {
							var badgeColor = 'btn-success';
						} else if (parseInt(row.status_ID) == 6) {
							var badgeColor = 'btn-danger';
						} else if (parseInt(row.status_ID) == 7) {
							var badgeColor = 'btn-primary';
						}
						var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide' onclick=viewRecord('approver',"+row.dtrRequest_ID+")>" + row.description + "</span>";
						return html;
					
                }
            },{
                field: "action",
                title: "ACTION",
                width: 200,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = '<a href="#" class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" onclick=viewRecord("record",'+row.dtrRequest_ID+')><i class="fa fa-search"></i></a> '+
								'<a href="#" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" onclick=deleterecord('+row.dtrRequest_ID+')><i class="fa fa-trash"></i></a>';
                     return html;
                }
            },
			],
        };

        $('.m_datatable_pending').mDatatable(options);

    };

    return {
        init: function (){
            userAnswerSelector();
        }
    };
}();
function deleterecord(id){
	 $.post("<?php echo base_url('dtr/checkIfRecordisOngoing'); ?>", {dtrRequest_ID:id}, function(result){
			   var rs = JSON.parse(result);
			   if(rs["cnt"]==0){
				   swal({
					  title: 'Cancel Request',
					  text: "Are you sure you want to cancel this request?",
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Delete',
					  closeOnConfirm: false,
					  closeOnCancel: false,
					  allowOutsideClick: false

					}).then((result) => {
					  if (result.value) {
							$.post("<?php echo base_url('dtr/deletetitorequest'); ?>", {dtrRequest_ID:id}, function(result){
								 $('.m_datatable_pending').mDatatable('destroy');
								  pendingTitoRequest.init();
							});
					  }else{
					  
					  }
					});
				   
			   }else{
				     swal("Cancel Restricted.", "The approval process is ongoing, you are not allowed to cancel this request!", "error");
				   
			   }
			
		}); 
		
}
function viewRecord(type,id){
	// alert(type+" "+id);
	if(type=="record"){
		viewModalRecord(id);
	}else{
		viewModalApprover(id);
	}
	
	
}
function viewModalRecord(id){
	var imagePath = baseUrl + '/assets/images/';
	 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/viewRecordTito'); ?>",
			data: {dtrRequest_ID:id},
			cache: false,
			success: function (output) {
				var rs = JSON.parse(output);
				 var str = "";
					str +='<div class="m-scrollable m-form m-form--fit m-form--label-align-right" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">';
					str += '<div class="m-card-user m-card-user--skin-dark">';
						str	+= '<div class="m-card-user__pic"><img src='+imagePath+rs["titoDetailsqry"][0]["pic"]+' onerror="noImage('+rs["titoDetailsqry"][0]["userId"]+')" class="m--img-rounded m--marginless header-userpic'+rs["titoDetailsqry"][0]["userId"]+'"></div>';
						str += '<div class="m-card-user__details"><span class="m-card-user__name m--font-weight-500 user-name" style="color: #000;">'+rs["titoDetailsqry"][0]["fname"]+' '+rs["titoDetailsqry"][0]["lname"]+' </span><span href="#" class="m-card-user__email m--font-weight-300 m-link user-role" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: darkcyan;">'+rs["empPosQry"][0]["pos_details"]+' - '+rs["empPosQry"][0]["status"]+'</span><span href="#" class="m-card-user__email" style="font-size: smaller;">'+moment(rs["titoDetailsqry"][0]["dateCreated"]).format('MMMM Do YYYY, h:mm:ss a')+'</span></div>';
 					str	+= '</div>';
					str +='<hr>';
					str +='<div class="form-form-group m-form__group">';
					str +='<table style="width: 100%;" id="tblTitoRqDetails">';
					str +='<tr><td>Date: </td><td>'+rs["titoDetailsqry"][0]["dateRequest"]+'</td></tr>';
					str +='<tr><td>Schedule: </td><td>'+rs["titoDetailsqry"][0]["time_start"]+" - "+rs["titoDetailsqry"][0]["time_end"]+'</td></tr>';
					str +='<tr><td>Start Date & Time: </td><td>'+rs["titoDetailsqry"][0]["startDateWorked"]+" "+rs["titoDetailsqry"][0]["startTimeWorked"]+'</td></tr>';
					str +='<tr><td>End Date & Time: </td><td>'+rs["titoDetailsqry"][0]["endDateWorked"]+" "+rs["titoDetailsqry"][0]["endTimeWorked"]+' </td></tr>';
					str +='<tr><td>Total hours: </td><td>'+rs["titoDetailsqry"][0]["timetotal"]+' hr.  </td></tr>';

					str +='<tr><td>Message: </td><td><blockquote  class="ahr_quote" style="padding:10px;"><i class="la la-quote-left"></i><span class="mb-0 text-capitalize" id="reasonAppr">'+rs["titoDetailsqry"][0]["message"]+'</span><i class="la la-quote-right"></i></blockquote></td></tr>';
					str +='</table>';
					str +='</div>';
					str +='</div>';
			$("#bodyShowDetails").html(str);
			$("#headerTitle").html("Request Detail");	
			}
		 });
	$("#showDetails").modal("show");
}
function viewModalApprover(id){
	 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/viewApproverStatus'); ?>",
			data: {dtrRequest_ID:id},
			cache: false,
			success: function (output) {
				 var rs = JSON.parse(output);
				var icon = "";
				var color = "";
				var animate = "";
				var animateI = "";
				var current = "";
				var stat = "";
				var str = "";
				 $.each(rs, function (index, value) {
					  if(value.remarks == null || value.remarks == " "){
							var note = "No Notes found.";
						}else{
							var note =  value.remarks;
						}
						if (parseInt(value.approvalStatus_ID) == 5) {
							icon = "fa fa-thumbs-o-up";
							color = "btn-success";
							stat = value.description;
						} else if (parseInt(value.approvalStatus_ID) == 6) {
							icon = "fa fa-thumbs-o-down";
							color = "btn-danger";
							stat = value.description;
						} else if ((parseInt(value.approvalStatus_ID) == 2) || (parseInt(value.approvalStatus_ID) == 4)) {
							icon = "fa fa-spinner";
							color = "btn-info";
							animate = "fa-spin";
							stat = "pending";
							if (value.approvalStatus_ID == 4){
								current = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink ml-2" style="min-height: 7px;min-width: 7px;"></span>';
							}else{
								current = '';
							}
						} else if (parseInt(value.approvalStatus_ID) == 12){
							icon = "fa fa-exclamation-circle";
							color = "btn-warning";
							animateI = "m-animate-blink";
							stat = value.description;
						}
						else if (parseInt(value.approvalStatus_ID) == 7){
							icon = "fa fa-close";
							color = "btn-primary";
							stat = value.description;
						}
					   str += '<div class="m-portlet bg-secondary">' +
						'<div class="col-md-12 pt-2 pb-2" style="background: #505a6b30;">' +
						'<div class="row">' +
						'<div class="col-7 col-sm-7 col-md-7 pt-2">APPROVER <span>' + value.approvalLevel + '</span>'+current+'</div>' +
						'<div class="col-5 col-sm-5 col-md-5">' +
						'<div class="btn pull-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;background: #505a6b;">' +
						'<div class="btn '+color+' m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air '+animate+'" style="width: 25px !important;height: 25px !important;">' +
						'<i class="' + icon + ' '+animateI+'" style="font-size: 17 px;"></i>' +
						'</div>' +
						'<span class="button-content text-light text-capitalize pl-2">' + stat + '</span>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>  ' +
						'<div class="m-portlet__body pt-3 pl-4 pb-3 pr-4">' +
						'<div class="row">' +
						'<div class="col-2 col-sm-2 col-md-2 d-none d-sm-block">' +
						'<img class="rounded-circle" src="<?php echo base_url(); ?>/assets/images/'+value.pic+'" width="58" alt="" class="mx-auto">' +
						'</div>' +
						'<div class="col-12 col-sm-10 col-md-10 pl-1">' +
						'<div class="col-md-12 font-weight-bold" style="font-size: 15px;">'+value.fname+' '+value.lname+'</div>' +
						'<div class="col-md-12" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: darkcyan;">'+value.position+'</div>' +
						'<span class="col-md-12" style="font-size: 12px;">'+moment(value.dateTimeStatus, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y h:mm A')+'</span>' +
						'<div class="col-md-12 mt-3" style="font-size: 12px;"><i> - '+note+'</i>' +
						'</div>' +
						'</div>' +
						'<div class="col-md-12 pt-3">' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>'; 
				 });
				
     
 			$("#bodyShowDetails").html(str);	
 			$("#headerTitle").html("Approver");	
			}
		 });
	$("#showDetails").modal("show");
}
function checkTotalTimeStatus(){
				var dateStart = $("#dateActualWorked").val();
				var d1 = dateStart;
				var d2 = moment(dateStart).add(1, 'days').format('YYYY-MM-DD');
				if($("#reqTimeStart").val()=="12:00 AM"){
					$("#reqDateEnd").val(d2);
					$("#reqDateStart").val(d1); 
						var reqFrom = d1+" "+$("#reqTimeStart").val();
						var reqTo = d2+" "+$("#reqTimeEnd").val();
						sumTotalWorkHours(reqFrom,reqTo);
				}else{
					var time1 = $("#reqTimeStart").val();
					var time2 = $("#reqTimeEnd").val();
						$.post("<?php echo base_url("dtr/checkTimeOfShift"); ?>", {timeStart: time1,timeEnd: time2}, function(result){
							var rs = result.split("|");
							if(rs[1]=="Tomorrow"){
								$("#reqDateEnd").val(d2);
								$("#reqDateStart").val(d1);
										var reqFrom = d1+" "+$("#reqTimeStart").val();
										var reqTo = d2+" "+$("#reqTimeEnd").val();
							}else{
								if(time1.trim() == "12:00:00 AM"){
									$("#reqDateEnd").val(d2);
									$("#reqDateStart").val(d1); 
										var reqFrom = d1+" "+$("#reqTimeStart").val();
										var reqTo = d2+" "+$("#reqTimeEnd").val();

								}else{
									$("#reqDateEnd").val(d1);
									$("#reqDateStart").val(d1);
										var reqFrom = d1+" "+$("#reqTimeStart").val();
										var reqTo = d1+" "+$("#reqTimeEnd").val();


								}
							
							}
							sumTotalWorkHours(reqFrom,reqTo);
						});
						
				}
			
		
		
}

function sumTotalWorkHours(reqFrom,reqTo){
	var txt ="";
		 $("#noteReqDateTime").html(txt);
		$.post("<?php echo base_url("dtr/checkTotalTime"); ?>", {timeStart: reqFrom,timeEnd: reqTo}, function(result){
			 if(result>=0){
					txt ="Note: You requested a total of "+result+" hrs ";
					$("#noteReqDateTime").data("timetotal",result);
			  }else{
				  $("#noteReqDateTime").data("timetotal",0);
				  
			  }
			 $("#noteReqDateTime").html(txt);
			 if($("#reqTimeStart").val()=="12:00 AM"){
				$("#noteReqDateTime").append("<br><small>(Our system is following the 24hour format time. Therefore, the 12MN shift you have selected will be on the end time of "+$("#reqDateStart").val()+" day.)</small>"); 

				}
		});
	
}

function loadSchedules(acc_id,acc_time_id=null){
		 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/loadSchedule'); ?>",
			data: {acc_id:acc_id},
			cache: false,
			success: function (json) {
			
			var rs = JSON.parse(json);
			var opt = "<option disabled selected> -- </option>";
			 $.each(rs, function (key, obj) {
				var selected = (acc_time_id!=null && acc_time_id==obj.acc_time_id) ? "selected" : ""; 
				opt += "<option value='" + obj.acc_time_id + "' "+selected+">" + obj.time_start+" - "+obj.time_end + "</option>";
			});
			
			$("#mySelectSched").html(opt);
			
			if(acc_time_id==null){
			$("#dateReq").val("");	
			}
			$("#mySelectSched").trigger("change");
			//$("#exampleTextarea").val("");
			}
		 });
	}
	function resetForm(){
		$("#defaultForm").formValidation('resetForm', true);	
	}
$(document).ready(function () {
    UserAnswerDatatable.init();
    pendingTitoRequest.init();
	 $('.timepicker').timepicker({minuteStep: 1});

	$('#mySelectSched').select2({
			placeholder: "Select your schedule.",
			width: '100%'
	 });
	 $(".checkTimeSched").change(function(){
		var time = ($("#mySelectSched").find("option:selected").text()).split("-");
		var dateStart =  $("#dateActualWorked").val();
		var dateEnd =  $("#reqDateEnd").val();
		if(time[0]!=" "){
		 $('#reqTimeStart').timepicker('setTime', time[0]);
		 $('#reqTimeEnd').timepicker('setTime', time[1]);
			 if(time[0].trim() == "12:00:00 AM"){
				$("#reqDateEnd").val(moment(dateStart).add(1, 'days').format('YYYY-MM-DD'));
				$("#reqDateStart").val(dateStart); 
			 }else{
			  $("#reqDateEnd").val(dateStart);
			  $("#reqDateStart").val(dateStart);
			 }
		}else{
		var current_time = new moment().format("HH:mm");

		 $('#reqTimeStart').timepicker('setTime', current_time);
		 $('#reqTimeEnd').timepicker('setTime', current_time);
		
		} 
		 $.post("<?php echo base_url("dtr/checkTimeOfShift"); ?>", {timeStart: time[0],timeEnd: time[1]}, function(result){
				var rs = result.split("|");
				if(rs[1]=="Tomorrow"){
					$("#reqDateEnd").val(moment(dateStart).add(1, 'days').format('YYYY-MM-DD'));
					 $("#reqDateStart").val(dateStart);
				}else{
				if(time[0].trim() == "12:00:00 AM"){
					$("#reqDateEnd").val(moment(dateStart).add(1, 'days').format('YYYY-MM-DD'));
					$("#reqDateStart").val(dateStart); 
				 }else{
				  $("#reqDateEnd").val(dateStart);
				  $("#reqDateStart").val(dateStart);
				 }
				
				}
				checkTotalTimeStatus();
 			});
	});
		 $('.checkTimeSched').datepicker({format: 'yyyy-mm-dd'});

	 $('.datePick').datepicker();
	 $('#dateReq').datepicker({
	    format: 'yyyy-mm-dd',
		startDate: '-8d',
		endDate: '+7d',
		}).on("changeDate", function (e) {
		var date = $(this).val()
		 $("#defaultForm").formValidation('revalidateField','dateRequested');
			 $.ajax({
				type: "POST",
				url: "<?php echo base_url("dtr/checkSched"); ?>",
				data: {
					date: date,
					emp_id: <?php echo $_SESSION["emp_id"]; ?>,
				},
				cache: false,
				success: function (res) { 
					var rs = JSON.parse(res);
					if(rs.result.length==0){
						$("#showShift").html("You don't have schedule set on the day you have selected.");
						loadSchedules(<?php echo $_SESSION["acc_id"]; ?>,"reset");
					}else if(rs.result.length==1){
						$("#showShift").html("You have a "+rs.result[0]["type"]+" schedule on this day.");
						if(rs.result[0]["type"]=="Normal" || rs.result[0]["type"]=="WORD" || rs.result[0]["type"]=="Double"){
							$("#showShift").append("<br>Your schedule is from <span style='color:"+rs.result[0]["style"]+"'>"+rs.result[0]["time_start"]+"</span> to <span style='color:"+rs.result[0]["style"]+"'>"+rs.result[0]["time_end"]+". </span>");
						}
						
						loadSchedules(<?php echo $_SESSION["acc_id"]; ?>,rs.result[0]["acc_time_id"]);
						
  					}else if(rs.result.length>1){
					
					}else{
					
					
					}
					$("#showShift").data("totalrequestcheck",rs.request_count[0].cnt);
					if(rs.request_count[0].cnt<2){
						$("#btnSubmit").prop("disabled",false);
					}else{
						$("#showShift").html("<span style='color:red;'>Permission to request denied! You have already filed a request couple of times on this date.</span>");
						$("#btnSubmit").prop("disabled",true);
					}
					
					
					
				}
			});
		});
		
		$('#defaultForm')
        // .bootstrapValidator({
        .formValidation({
            message: 'This value is not valid',
			live: 'enabled',
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'fa fa-refresh'
            },
            fields: {
                reqReason: {
                    message: 'The username is not valid',
                    validators: {
                        notEmpty: {
                            message: 'You need to state the reason to proceed.'
                        },
                        stringLength: {
                            min: 6,
                            max: 255,
                            message: 'The content must be more than 6 and less than 255 characters long'
                        },
                        /*remote: {
                            url: 'remote.php',
                            message: 'The username is not available'
                        },*/
                        regexp: {
                            regexp: /^[a-zA-Z0-9_!?\. ]+$/,
                            message: 'The content can only consist of alphabetical, number, dot and underscore'
                        }
                    }
                },
				listschedule: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'You need to select a schedule.'
                        }
                    }
                },
				dateRequested: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'This field is required.'
                        }
                    }
                },
            }
        })
        .on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

           
		var date= $('#dateReq').val();
		var schedule= $('#mySelectSched').val();
		var reason= $('#exampleTextarea').val();
		var actualDate= $('#dateActualWorked').val();
		var startDateWorked= $('#reqDateStart').val();
		var startTimeWorked= $('#reqTimeStart').val();
		var endDateWorked= $('#reqDateEnd').val();
		var endTimeWorked= $('#reqTimeEnd').val();
		var timetotal= $('#noteReqDateTime').data("timetotal");
		var cnt = $("#showShift").data("totalrequestcheck");
		if(cnt<2){
		swal({
			  title: 'Requesting Time-in and Time-out',
			  text: "Are you sure you want to proceed?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes',
			  closeOnConfirm: false,
			  closeOnCancel: false,
			  allowOutsideClick: false

			}).then((result) => {
			  if (result.value) {
					  $.ajax({
					type: "POST",
					url: "<?php echo base_url("dtr/requestTiTo"); ?>",
					data: {
						date: date,
						schedule: schedule,
						reason: reason,
						actualDate:actualDate,
						startDateWorked:startDateWorked,
						startTimeWorked:startTimeWorked,
						endDateWorked:endDateWorked,
						endTimeWorked:endTimeWorked,
						timetotal:timetotal,

					},
					cache: false,
					success: function (res) {
						var rs = JSON.parse(res.trim());
						if(res=="0"){
								alert("error");
						}else{
							 notification(rs.notif_id);
							 swal('Success',"Successfully filed request",'success');
							$("#m_datepicker_modal").modal("hide");
							resetForm();
						}

					}
				});  
				
				
			  }else{
				
			  }
			})
 		}else{
				swal('Oops!',"Only a maximum of two requests per date can be availed. You have already filed a request couple of times on this date. Please choose another date.",'error');

		}
        });
		
		
		
		
});
 
</script>