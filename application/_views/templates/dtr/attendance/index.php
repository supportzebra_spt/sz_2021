<style>
	#tblTopLog tr td{
		padding: 8px;
	}
	tr.footable-header {
		background: #337ab7;
		color: #fff;
		text-align:center;
	}
	form.form-inline {
		float: right;
	}
	tr:first-child > td > .fc-day-grid-event {
    margin-top: -19px !important;
	}
	.fc-content {
    padding: 1px !important;
	}
	.fc-unthemed .fc-event-dot.fc-start .fc-content:before, .fc-unthemed .fc-event.fc-start .fc-content:before {
   display:none;
	}
	.shiftNormal{
		background:#4CAF50 !important;
	}
	.shiftRestDay {
		background:#f6546a !important;
	}
	.shiftDouble {
		background:#662A79 !important;
	}
	.shiftWORD {
		background:#ff7f50 !important;
	}
	.shiftOfficialBusiness {
		background:#ED1808 !important;
	}
	.shiftVTO-WP {
		background:#2874A6 !important;
	}
	.shiftVTO-WoP {
		background:#8E44AD !important;
	}
	.shiftLeave-WP {
		background:#088da5 !important;
	}
	.shiftLeave-WoP {
		background:#088da5 !important;
	}
	.shiftSuspension {
		background:#943126 !important;
	}
	.shiftHolidayOff-WoP {
		background:#AF601A !important;
	}
	.shiftHolidayOff-WP {
		background:#87CEFA !important;
	}
	.shiftAbsent {
		background:#FF4935 !important;
	}
	.fc-unthemed .fc-event .fc-title, .fc-unthemed .fc-event-dot .fc-title{
		color:#fff !important;
	}
	.fc-scroller.fc-day-grid-container {
		height: 387px !important;
	}
	#tbkDTRlog th {
		text-align: center;
	}
	.datepicker tbody tr>td.day {
		color: #000000;
		background: #dfdfdf;
	}
	.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
		color: #777 !important;
		cursor: default;
		background: #fff !important;
	}
	.datepicker tbody tr>td.day.active, .datepicker tbody tr>td.day.active:hover, .datepicker tbody tr>td.day.selected, .datepicker tbody tr>td.day.selected:hover {
		background: #cccccc;
		color: #000;
	}
	.has-feedback .form-control-feedback {
		width: 34px;
		height: 34px;
		line-height: 34px;
		text-align: center;
		float: right;
		margin-top: -37px;
	}
	i.form-control-feedback.fa.fa-remove{
		color: #f4516c;
	}
	.custom-select.is-invalid, .form-control.is-invalid, .was-validated .custom-select:invalid, .was-validated .form-control:invalid {
    border-color: #dc3545;
	}
	.fc-unthemed .fc-day-grid td:not(.fc-axis).fc-event-container {
		padding: 0;
	}
	#tbldtrdetails td {
		padding: 7px;
	}
	th.m-datatable__cell--center.m-datatable__cell.m-datatable__cell--sort {
		background: #575962 !important;
	}
	th.m-datatable__cell--center.m-datatable__cell.m-datatable__cell--sort span {
		color: #fff !important;
	}
	.m-datatable>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell>span>i {
		color: #fff;
	}
	text.highcharts-credits {
		display: none;
	}
</style>
<!--FOOTABLES -->
 <link href="<?php echo base_url(); ?>assets/src/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/formValidation/dist/css/formValidation.min.css" />


<div class="m-grid__item m-grid__item--fluid m-wrapper">
<div class="m-content" style="width: 100%;">
<div class="row">
<div class="col-md-6">
	<!--begin::Portlet-->
		
	
	<div class="m-portlet m-portlet--tab">
		<div class="m-portlet__head" style="background: #575962;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
					<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #fff  !important;">
						<i class="la la-clock-o"></i>	  DAILY TIME RECORD
					</h3>
					
				</div>
				
			</div>
		
			<a   data-toggle="modal" data-target="#m_datepicker_modal" >
			 <button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right"   onclick="loadSchedules(<?php echo $_SESSION["acc_id"]; ?>)"  data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="File Time-in Time-out request here." style="margin-top: 30%;">
				<i class="fa fa-plus-circle"></i>
			</button> </a>
		</div>
				<div class="m-portlet__body"  style="height: 490px;background-image:url('<?php echo base_url()."assets/images/img/dtr-bg.png"; ?>');background-size: 73%;background-repeat: no-repeat;background-position: top;background-size: contain;">
			<!--begin::Section-->
			<div class="m-section" style="text-align: center;">
				<span class="m-section__sub">
				<?php 
				if(isset($emp->pic)){
					$pic = $emp->pic;
				}else{
					$pic = "";
				}
				?>
 				<img src="<?php echo base_url()."assets/images/".$pic; ?>" class="m--img-rounded m--marginless m--img-centered ifError" onerror='this.src="<?php echo base_url()."assets/img/sz.png"; ?>"' style="width: 200px;border: 5px solid #edeef4;background:#fff"> 
				</span>
				<div class="m-section__content">
					<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
						<div class="m-demo__preview" style="padding:25px;">
							<!--begin::Form-->
							<?php
								date_default_timezone_set('Asia/Manila');
									$dtrCutOff = strtotime(date("Y-m-d H:i:s",strtotime("+12 hours", strtotime($limit))));
									$current = strtotime(date("Y-m-d H:i:s"));
									// $dtrCutOff = strtotime("2018-07-10 22:15");
									// $current = strtotime(date("Y-m-d H:i"));
									$dtrCutStatus = ($dtrCutOff >= $current) ?  1 : 0;
										
									// echo count($emp_last_logs_DTR)."<br>";
									// echo $emp_last_logs_DTR[0]->entry."<br>";
									// echo $dtrCutStatus."<br>";
									// echo $dtrCutOff."<br>";
   								?>
							<div class="m-form">
							<?php 
								$isexcempt = $isexcempt[0]->cnt;	
								
							?>
							
								<div class="m-form__group form-group" id="schedIDdiv" data-id="<?php echo ($emp_last_logs_DTR !=0) ? $emp_last_logs_DTR[0]->sched_id : 0 ; ?>"> 
									<div class="m-checkbox-list" >
								<?php 
								
 								$isBreak=0;
								if($emp_last_logs_DTR !=0) {
										

									?>
								
										<?php if($emp_last_logs_DTR[0]->entry=='O'){ ?>
												<?php if($dtrCutStatus==0){ ?>
													<?php if($isexcempt>=1){ // if Managers and up -> No DTR policy  ?>
															<button class="btn btn-accent btn-lg" id="btnClockInExcempt"><i class="la la-clock-o"></i>  Clock-in</button>
													<?php }else{ ?>
															<button class="btn btn-accent btn-lg" id="btnClockIn"><i class="la la-clock-o"></i>  Clock-in</button>
													<?php } ?>

												<?php }else{ ?>
														
														<span  id="divInfoClock" data-acc_time_id="<?php echo $emp_last_logs_DTR[0]->acc_time_id; ?>" data-sched_id="<?php echo $emp_last_logs_DTR[0]->sched_id; ?>"></span>
														<?php if($isexcempt>=1){ // if Managers and up -> No DTR policy  ?>
																<button class="btn btn-danger btn-lg" id="btnClockOutExcempt"><i class="la la-clock-o"></i> Clock-out</button>
														<?php }else{ 
														$isBreak=1;
														?>
																<button class="btn btn-danger btn-lg" id="btnClockOut"><i class="la la-clock-o"></i> Clock-out</button>
														<?php } ?>
												
												<?php } ?>
										<?php }else{ ?>
										
													<?php if($dtrCutStatus==0){ ?>
													<?php if($isexcempt>=1){ // if Managers and up -> No DTR policy  ?>
															<button class="btn btn-accent btn-lg" id="btnClockInExcempt"><i class="la la-clock-o"></i>  Clock-in</button>
													<?php }else{ ?>
															<button class="btn btn-accent btn-lg" id="btnClockIn"><i class="la la-clock-o"></i>  Clock-in</button>
													<?php } ?>

												<?php }else{ ?>
														
														<span  id="divInfoClock" data-acc_time_id="<?php echo $emp_last_logs_DTR[0]->acc_time_id; ?>" data-sched_id="<?php echo $emp_last_logs_DTR[0]->sched_id; ?>"></span>
														<?php if($isexcempt>=1){ // if Managers and up -> No DTR policy  ?>
																<button class="btn btn-danger btn-lg" id="btnClockOutExcempt"><i class="la la-clock-o"></i> Clock-out</button>
														<?php }else{ 
														$isBreak=1;
														?>
																<button class="btn btn-danger btn-lg" id="btnClockOut"><i class="la la-clock-o"></i> Clock-out</button>
														<?php } ?>
												
												<?php } ?>
										<?php  } ?>
									<?php  }else{ ?>
											<?php if($isexcempt>=1){ // if Managers and up -> No DTR policy  ?>
													<button class="btn btn-accent btn-lg" id="btnClockInExcempt"><i class="la la-clock-o"></i>  Clock-in</button>
											<?php }else{ ?>
													<button class="btn btn-accent btn-lg" id="btnClockIn"><i class="la la-clock-o"></i>  Clock-in</button>
											<?php } ?>
									
									<?php  } ?>
									</div>
								</div>
								
							</div>
							<!--end::Form-->
							
						</div>
						
						<div class="m-demo__preview" style="padding:25px;background: #9e9e9e17;">
							<div class="m-checkbox-list">
									<?php
									// echo $isBreak;
 									if($isBreak==1){
										if(isset($get_break)){
										
  												$color = array("FIRST BREAK"=>"info","LUNCH"=>"warning","","LAST BREAK"=>"success");
												$icon = array("FIRST BREAK"=>"la-hotel","LUNCH"=>"la-coffee","LAST BREAK"=>"la-yelp");
											foreach($get_break as $row){
											$isTaken = (in_array($row->break_type,$get_taken_break)) ? "disabled" : "";
											$time = ($row->hour==1) ? (60+$row->min)." mins." :  ($row->hour+$row->min)." mins.";
											$isTakenComment = (in_array($row->break_type,$get_taken_break)) ? "<span style='color:red'>Taken</span>" : $time;
											
											
									?>
											<button class="btn btn-<?php echo $color[$row->break_type]; ?>" onclick="takeBreak(<?php echo $row->sbrk_id; ?>,'O','<?php echo $row->break_type; ?>')" <?php echo $isTaken; ?>>
												<i class="la <?php echo $icon[$row->break_type]; ?>"></i> <?php echo $row->break_type."<br><small>".$isTakenComment."</small>"; ?>
											</button>
									<?php
											}
										}
									}
									?>
							</div>
						</div>
						
					</div>
				</div>
				 
						
			</div>
			<!--end::Section-->
		</div>
	</div>
	<!--end::Portlet-->
</div>

<div class="col-md-6">
	<!--begin::Portlet-->
	<div class="m-portlet m-portlet--tab">
		<div class="m-portlet__head" style="background: #575962;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title" style="float: left;">
					<span class="m-portlet__head-icon m--hide">
					<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #fff !important; ">
						<i class="la la-calendar-check-o"></i> MY SCHEDULE
					</h3>
				</div>
				<div style="float: right;margin-top: 22px;">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
				<button type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left"><span class="fc-icon fc-icon-left-single-arrow"></span></button>
				<button type="button" class="fc-next-button fc-button fc-state-default fc-corner-right"><span class="fc-icon fc-icon-right-single-arrow"></span></button>

				</div>
			</div>
		</div>
		<div class="m-portlet__body" style="padding:0px;height: 480px;">
			<!--begin::Section--> 
			<div class="m-section" style="text-align: center;">
				<span class="m-section__sub">
				 
				</span>
				<div class="m-section__content">
					<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
						<div id="topDtrLogs">
				<button id="btnReload" hidden>reload</button>
				<div id="m_calendar" style="background: #fff;"></div>
		 
						</div>
					</div>
				</div>
			 
			</div>
			<!--end::Section-->
		</div>
	</div>
	<!--end::Portlet-->
</div>
</div>
<div class="row">
	<div class="col-md-12">
	<!--begin::Portlet-->
	<div class="m-portlet m-portlet--tab">
		<div class="m-portlet__head" style="background: #575962;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
					<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #fff  !important; ">
						<i class="la la-dashboard"></i>  DAILY TIME RECORD
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			   <div class="input-group col-md-8" style='float:right'>
					<div class="input-group-prepend">
						<button class="btn btn-brand" id="btnShwoDTRlogs" type="button">Show</button>
					</div>
					<input type="text" class="form-control" id="m_daterangepicker_1" readonly placeholder="Select time" type="text" style="color: #00c5dc;background: #fff;    font-weight: 700;text-align: center;">
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<!--begin::Section-->
			<div class="m-section" style="text-align: center;">
				 
				<!--	<div class="form-group m-form__group">
 						<div class="input-group col-md-4">
							<div class="input-group-prepend">
								<button class="btn btn-brand" id="btnShwoDTRlogs" type="button">Show</button>
							</div>
							<input type="text" class="form-control" id="m_daterangepicker_1" readonly placeholder="Select time" type="text">
						</div>
					</div>-->
					 <div class="m-portlet m-portlet--mobile">
						
							<div class="m_datatable_pending" ></div>
						
					</div>
			</div>
			<!--end::Section-->
		</div>
	</div>
	<!--end::Portlet-->
</div>

</div>
</div>
</div>
<div class="modal fade" id="m_datepicker_modal"  role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"> <i class="flaticon-time-2"></i> Request Time-in & Time-out</h5>
				
			</div>
			<form class="m-form m-form--fit m-form--label-align-right form-horizontal" id="defaultForm" method="post" >
 				<div class="modal-body">
					 <div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Schedule Date</label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input" readonly  placeholder="Select date" id="dateReq" name="dateRequested" />
							</div>
							<span class="m-form__help"></span>
						</div>
						<div class="col-lg-6">
							Note:<br/> 
							<span id="showShift" data-totalrequestcheck="0">Please pick a date. The date allowed to be chosen is only between from the past week and a week ahead from the current date.</span>
						</div>
					</div>	
					<hr/>
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Date of Actual Work:</label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input datePick checkTimeSched" readonly  placeholder="Select date" id="dateActualWorked" value="<?php echo date("Y-m-d"); ?>"/>
								<div class="input-group-append">
									<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
							<span class="m-form__help">Please pick a date.</span>
						</div>
						<div class="col-lg-6">
							<label>Schedule:</label>
								<div class="input-group" >
									<select class="form-control m-select2 checkTimeSched" id="mySelectSched" name="listschedule" onchange="checkTotalTimeStatus()"></select>
								</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Date Started:</label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input " id="reqDateStart" readonly  placeholder="Select date" id="dateActStart" style="padding: 0px;text-align: center;" value="<?php echo date("Y-m-d"); ?>"/>
								<div class="input-group-append">
									<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
 						</div>
						<div class="col-lg-3">
						<label>Time Started:</label>
							<div class='input-group'>
								<input type='text' class="form-control m-input timepicker " id="reqTimeStart"  readonly placeholder="Select time" type="text" onchange="checkTotalTimeStatus()"/>
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<label>Date Ended:</label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input" id="reqDateEnd" readonly  placeholder="Select date" id="dateActEnd" style="padding: 0px;text-align: center;" value="<?php echo date("Y-m-d"); ?>"/>
								<div class="input-group-append">
									<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
 						</div>
						<div class="col-lg-3">
						<label>Time Ended:</label>
							<div class='input-group'>
								<input type='text' class="form-control m-input timepicker" id="reqTimeEnd" readonly placeholder="Select time" type="text" onchange="checkTotalTimeStatus()"/>
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
								<label id="noteReqDateTime" data-timetotal=""> </label>
						</div>
					</div>
					<hr/>
					<div class="form-group m-form__group">
						<label for="exampleTextarea">Reason</label>
						<textarea class="form-control m-input" id="exampleTextarea" rows="3"  name="reqReason"></textarea>
					</div>
					 
					 
				</div>
				<div class="modal-footer">
					<button type="" class="btn btn-secondary m-btn" data-dismiss="modal" onclick="resetForm()">Close</button>
 					<button type="submit" class="btn btn-success m-btn" id="btnSubmit">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="modal fade" id="breakModal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" >
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"> <i class="flaticon-time-2"></i> My Daily Time Record Details.</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
			<form class="m-form m-form--fit m-form--label-align-right">
				<div class="modal-body">
					<div class="row">
 						<div class="col-lg-6">
								<div id="dtrdetails"></div>
						</div>
						<div class="col-lg-6">
							 <div id="dtrchartdetails"></div>
						</div>
 					</div>
					
				</div>
				 
			</form>
		</div>
	</div>
</div>

<?php

include 'popup_survey.php';
 
?>
 <script src="<?php echo base_url(); ?>assets/src/custom/js/formValidation.min.js" ></script>
 <script src="<?php echo base_url();?>assets/src/demo/default/custom/components/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/formValidation/dist/js/formValidation.js"></script>
    <script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/formValidation/dist/js/framework/bootstrap.min.js"></script>


  <script>
   function checkTotalTimeStatus(){
				var dateStart = $("#dateActualWorked").val();
				var d1 = dateStart;
				var d2 = moment(dateStart).add(1, 'days').format('YYYY-MM-DD');
				if($("#reqTimeStart").val()=="12:00 AM"){
					$("#reqDateEnd").val(d2);
					$("#reqDateStart").val(d1); 
						var reqFrom = d1+" "+$("#reqTimeStart").val();
						var reqTo = d2+" "+$("#reqTimeEnd").val();
						sumTotalWorkHours(reqFrom,reqTo);
				}else{
					var time1 = $("#reqTimeStart").val();
					var time2 = $("#reqTimeEnd").val();
						$.post("<?php echo base_url("dtr/checkTimeOfShift"); ?>", {timeStart: time1,timeEnd: time2}, function(result){
							var rs = result.split("|");
							if(rs[1]=="Tomorrow"){
								$("#reqDateEnd").val(d2);
								$("#reqDateStart").val(d1);
										var reqFrom = d1+" "+$("#reqTimeStart").val();
										var reqTo = d2+" "+$("#reqTimeEnd").val();
							}else{
								if(time1.trim() == "12:00:00 AM"){
									$("#reqDateEnd").val(d2);
									$("#reqDateStart").val(d1); 
										var reqFrom = d1+" "+$("#reqTimeStart").val();
										var reqTo = d2+" "+$("#reqTimeEnd").val();

								}else{
									$("#reqDateEnd").val(d1);
									$("#reqDateStart").val(d1);
										var reqFrom = d1+" "+$("#reqTimeStart").val();
										var reqTo = d1+" "+$("#reqTimeEnd").val();


								}
							
							}
							sumTotalWorkHours(reqFrom,reqTo);
						});
						
				}

}

function sumTotalWorkHours(reqFrom,reqTo){
	var txt ="";
		 $("#noteReqDateTime").html(txt);
		$.post("<?php echo base_url("dtr/checkTotalTime"); ?>", {timeStart: reqFrom,timeEnd: reqTo}, function(result){
			 if(result>=0){
					txt ="Note: You requested a total of "+result+" hrs ";
					$("#noteReqDateTime").data("timetotal",result);
			  }else{
				  $("#noteReqDateTime").data("timetotal",0);
				  
			  }
			 $("#noteReqDateTime").html(txt);
			 if($("#reqTimeStart").val()=="12:00 AM"){
				$("#noteReqDateTime").append("<br><small>(Our system is following the 24hour format time. Therefore, the 12MN shift you have selected will be on the end time of "+$("#reqDateStart").val()+" day.)</small>"); 

				}
		});
	
}
	function takeBreak(sbrk_id,type,break_type){
		swal({
			  title: break_type,
			  text: "Are you sure you want to take your break now?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes',
			  closeOnConfirm: false,
			  closeOnCancel: false,
			  allowOutsideClick: false

			}).then((result) => {
			  if (result.value) {
				 $.ajax({
					type: "POST",
					url: "<?php echo base_url('dtr/takeBreak'); ?>",
					data: {sbrk_id:sbrk_id,type:type,note:""},
					cache: false,
					success: function (output) {
						var rs = JSON.parse(output);
						if(rs.result>0){
							window.location.assign("<?php echo base_url();?>dtr/breakPage");
							
							socket.emit('live', {
									  emp_id: rs.personal[0].emp_id,
									  sup: rs.personal[0].sup,
									  fname: rs.personal[0].fname,
									  lname: rs.personal[0].lname,
									  pic: rs.personal[0].pic,
									  uid: rs.personal[0].uid,
									  position: rs.personal[0].position,
									  account: rs.personal[0].account,
									  dtr: rs.personal[0].dtr,
								  });
						}else{
							alert("error");
						}
					}
				 });
				
			  }else{
				
			  }
			})
		
		 
		
	}

	function loadSchedules(acc_id,acc_time_id=null){
		 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/loadSchedule'); ?>",
			data: {acc_id:acc_id},
			cache: false,
			success: function (json) {
			
			var rs = JSON.parse(json);
			var opt = "<option disabled selected> -- </option>";
			 $.each(rs, function (key, obj) {
				var selected = (acc_time_id!=null && acc_time_id==obj.acc_time_id) ? "selected" : ""; 
				opt += "<option value='" + obj.acc_time_id + "' "+selected+">" + obj.time_start+" - "+obj.time_end + "</option>";
			});
			
			$("#mySelectSched").html(opt);
			
			if(acc_time_id==null){
			$("#dateReq").val("");	
			}
			$("#mySelectSched").trigger("change");
			//$("#exampleTextarea").val("");
			}
		 });
	}
	function attendanceLogEntry(type,entry,acc_time_id,sched){
	
	}
	function viewDTR(sched_id){
 		 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/viewLogsDetails'); ?>",
			data: {sched_id:sched_id},
			cache: false,
			success: function (json) {
			
			var rs = JSON.parse(json);
			var str = "";
			str += "<table id='tbldtrdetails'>";
			str += "<tr><td><b>Date</b> </td><td>"+rs[0]["sched_date"]+"</td></tr>";
			str += "<tr><td><b>Shift</b> </td><td>"+rs[0]["shift"]+"</td></tr>";
			str += "<tr><td><b>Clock-in</b> </td><td>"+rs[0]["login_raw"]+"</td></tr>";
			if(rs[0]["firstbreak_total"]!=0){
				str += "<tr><td><b>First Break</b> </td><td>"+rs[0]["firstb"]+"</td></tr>";
			}
			if(rs[0]["lunchbreak_total"]!=0){
				str += "<tr><td><b>Lunch Break</b> </td><td>"+rs[0]["lunchb"]+"</td></tr>";
			}
			if(rs[0]["lastbreak_total"]!=0){
				str += "<tr><td><b>Last Break </b></td><td>"+rs[0]["lastb"]+"</td></tr>";
			}		
			str += "<tr><td><b>Clock-out</b> </td><td>"+rs[0]["logout_raw"]+"</td></tr>";
			
			str += "</table>";
			$("#dtrdetails").html(str);
			
			
			
			
			Highcharts.chart('dtrchartdetails', {
					chart: {
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false,
						type: 'pie'
					},
					title: {
						text: ''
					},
					tooltip: {
						pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: false,
								format: '{point.y}'
							},
							showInLegend: true
						}
					},
					series: [{
						name: 'Time (Hr.)',
						colorByPoint: true,
						data: [{
							name: 'Online',
							y: rs[0]["worked_total"]/60
						},{
							name: 'Idle',
							y: rs[0]["worked_total_idle"]/60
						},{
							name: 'First Break',
							y: rs[0]["firstbreak_total"]/60
						}, {
							name: 'Lunch',
							y: rs[0]["lunchbreak_total"]/60
						}, {
							name: 'Last Break',
							y: rs[0]["lastbreak_total"]/60
						} ]
					}]
				});
 			$("#breakModal").modal("show");
			}
		 });
		
	}
	function showDTR(date){
		
		var user_id = <?php echo $_SESSION["emp_id"]; ?>;
 		 $('.m_datatable_pending').mDatatable('destroy');
         var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/dtr/showMyLogs',
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
                        params: {
                            query: {
                                user_id: user_id,
                                date: date,
                             },
                        },
						 map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    }
                },
				saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: 'dashed-table',
                scroll: false,
                footer: false,
                header: true
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#titoPendingSearch'),
            },			
            columns: [{
                field: "sched_date",
                title: "DATE",
                width: 150,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function(data) {
                     var sched_id = data.sched_id.padLeft(8);
                    var html = "<div class = 'col-md-12'>" + data.sched_date + "</div>";
                    return html;
                }
            },{
                field: "shift",
                title: "SCHEDULE",
                width: 100,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                      var html = "<div class = 'col-md-12'>" +  data.shift + "</div>";
                    return html;
                }
            },{
                field: "login_raw",
                title: "LOGIN",
                width: 250,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                      var html = "<div class = 'col-md-12'>" +  data.login_raw + "</div>";
                    return html;
                }
            },{
                field: "logout_raw",
                title: "LOGOUT",
                width: 250,
				selector: false,
				sortable: false,
				textAlign: 'center',
                template: function(data) {
                      var html = "<div class = 'col-md-12'>" +  data.logout_raw + "</div>";
                    return html;
                }
            },{
                field: "lastb_total",
                title: "Consumed Break",
				selector: false,
                sortable: false,
				textAlign: 'center',
				overflow: 'invisible',
                width: 100,
                template: function (data) {
                    return '<span style="font-weight: 700;">' + data.lastb_total + '</span>';
                }
            },{
                field: "",
                title: "Action",
				selector: false,
                sortable: false,
				textAlign: 'center',
				overflow: 'invisible',
                width: 100,
                template: function (data) {
                     var html = '<span class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" onclick=viewDTR('+data.sched_id+')><i class="fa fa-search" ></i></span>';
                    return html;
                }
            },
			],
        };

        $('.m_datatable_pending').mDatatable(options);
		}
 
function getCalendar(date){
	
	$.ajax({
		type: "POST",
			url: "<?php echo base_url('dtr/mySched'); ?>",
			data: {date:date},
			cache: false,
			success: function(date){ 
              $('#m_calendar').fullCalendar('removeEvents');
              $('#m_calendar').fullCalendar('addEventSource',  JSON.parse(date));         
              $('#m_calendar').fullCalendar('rerenderEvents' );

 			  $('#m_calendar').fullCalendar({
                header: {
                    left: '',
                    center: 'title',
                    right: '  '
                },
				
                editable: true,
                events:  JSON.parse(date),
				eventRender: function(event, element)
				{ 
					element.find('.fc-title').append("<br/>" + event.description); 
				},
				 dayClick: function(date, jsEvent, view) {
					//alert('Clicked on: ' + date.format());
				},
				eventColor: '#fff',
				eventClick: function(calEvent, jsEvent, view, resourceObj) {
				$.ajax({
					type: "POST",
					url: "<?php echo base_url('dtr/mySchedLogs/'); ?>"+calEvent.id,
 					cache: false,
					success: function(res){ 
					var output = JSON.parse(res);
					if(res!=0){
						swal(
						  'Your Logs',
						  "In: "+output[0].dtr_in+"<br> Out: "+output[1].dtr_out,
						  'info'
						)
					}else{
						swal(
						  'NO Logs!',
						  "No DTR Logs detected!",
						  'error'
						)
					}
					}
				});
				 

				}
            });
			}
		});
	
}
function resetForm(){
$("#defaultForm").formValidation('resetForm', true);	
}
	 
	
	$(function(){
 	 getCalendar("");
	  $('#dateReq').datepicker({
	    format: 'yyyy-mm-dd',
		startDate: '-7d',
		endDate: '+7d',
		}).on("changeDate", function (e) {
		var date = $(this).val()
		 $("#defaultForm").formValidation('revalidateField','dateRequested');
			 $.ajax({
				type: "POST",
				url: "<?php echo base_url("dtr/checkSched"); ?>",
				data: {
					date: date,
					emp_id: <?php echo $_SESSION["emp_id"]; ?>,
				},
				cache: false,
				success: function (res) { 
					var rs = JSON.parse(res);
					if(rs.result.length==0){
						$("#showShift").html("You don't have schedule set on the day you have selected.");
						loadSchedules(<?php echo $emp->acc_id; ?>,"reset");
					}else if(rs.result.length==1){
						$("#showShift").html("You have a "+rs.result[0]["type"]+" schedule on this day.");
						if(rs.result[0]["type"]=="Normal" || rs.result[0]["type"]=="WORD" || rs.result[0]["type"]=="Double"){
							$("#showShift").append("<br>Your schedule is from <span style='color:"+rs.result[0]["style"]+"'>"+rs.result[0]["time_start"]+"</span> to <span style='color:"+rs.result[0]["style"]+"'>"+rs.result[0]["time_end"]+". </span>");
						}
						
						loadSchedules(<?php echo $emp->acc_id; ?>,rs.result[0]["acc_time_id"]);
						
  					}else if(rs.result.length>1){
					
					}else{
					
					
					}
					$("#showShift").data("totalrequestcheck",rs.request_count[0].cnt);
					if(rs.request_count[0].cnt<2){
						$("#btnSubmit").prop("disabled",false);
					}else{
						$("#showShift").html("<span style='color:red;'>Permission to request denied! You have already filed a request couple of times on this date.</span>");
						$("#btnSubmit").prop("disabled",true);
					}
					
					
					
				}
			});
		});
		
	 $('.checkTimeSched').datepicker({format: 'yyyy-mm-dd'});
	 
	 $('.timepicker').timepicker({minuteStep: 1});
	 socket.on('dtr violation2', function (details) {console.log(details.uid);});
	
	 $('.fc-prev-button').on("click",function(){
			var prevDate = $("#m_calendar").fullCalendar('getDate').toDate();
				prevDate.setMonth(prevDate.getMonth()-1);
				$("#m_calendar").fullCalendar('prev');
			getCalendar(prevDate); 
	});

	$('.fc-next-button').on("click",function(){		
			var nextMonthDate = $("#m_calendar").fullCalendar('getDate').toDate();
				nextMonthDate.setMonth(nextMonthDate.getMonth()+1);
				$("#m_calendar").fullCalendar('next');
			getCalendar($("#m_calendar").fullCalendar('getDate').toDate());
	});
	 $('#defaultForm')
        // .bootstrapValidator({
        .formValidation({
            message: 'This value is not valid',
			live: 'enabled',
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'fa fa-refresh'
            },
            fields: {
                reqReason: {
                    message: 'The username is not valid',
                    validators: {
                        notEmpty: {
                            message: 'You need to state the reason to proceed.'
                        },
                        stringLength: {
                            min: 6,
                            max: 255,
                            message: 'The content must be more than 6 and less than 255 characters long'
                        },
                        /*remote: {
                            url: 'remote.php',
                            message: 'The username is not available'
                        },*/
                        regexp: {
                            regexp: /^[a-zA-Z0-9_!?\. ]+$/,
                            message: 'The content can only consist of alphabetical, number, dot and underscore'
                        }
                    }
                },
				listschedule: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'You need to select a schedule.'
                        }
                    }
                },
				dateRequested: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'This field is required.'
                        }
                    }
                },
            }
        })
        .on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

           
		var date= $('#dateReq').val().trim();
		var schedule= $('#mySelectSched').val().trim();
		var reason= $('#exampleTextarea').val().trim();
		var actualDate= $('#dateActualWorked').val().trim();
		var startDateWorked= $('#reqDateStart').val().trim();
		var startTimeWorked= $('#reqTimeStart').val().trim();
		var endDateWorked= $('#reqDateEnd').val().trim();
		var endTimeWorked= $('#reqTimeEnd').val().trim();
		var timetotal= $('#noteReqDateTime').data("timetotal");
		var cnt = $("#showShift").data("totalrequestcheck");
		if(cnt<2){
		swal({
			  title: 'Requesting Time-in and Time-out',
			  text: "Are you sure you want to proceed?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes',
			  closeOnConfirm: false,
			  closeOnCancel: false,
			  allowOutsideClick: false

			}).then((result) => {
			  if (result.value) {
				  
			  
					  $.ajax({
					type: "POST",
					url: "<?php echo base_url("dtr/requestTiTo"); ?>",
					data: {
						date: date,
						schedule: schedule,
						reason: reason,
						actualDate:actualDate,
						startDateWorked:startDateWorked,
						startTimeWorked:startTimeWorked,
						endDateWorked:endDateWorked,
						endTimeWorked:endTimeWorked,
						timetotal:timetotal,

					},
					cache: false,
					success: function (res) {
						var rs = JSON.parse(res.trim());
						if(res=="0"){
								alert("error");
						}else{
							 notification(rs.notif_id);
							 swal('Success',"Successfully filed request",'success');
							$("#m_datepicker_modal").modal("hide");
							resetForm();
						}

					}
				});  
			 
				
			  }else{
				
			  }
			})
			
 		}else{
				swal('Oops!',"Only a maximum of two requests per date can be availed. You have already filed a request couple of times on this date. Please choose another date.",'error');

		}
        });
	$("#btnClockInExcempt").click(function(){
		 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/dtrInCheck'); ?>",
			cache: false,
			success: function (output) {
				if(output==0){
					swal(
					  'NO NEW SCHEDULE',
					  'Please approach your supervisor to upload schedule.',
					  'error'
					)
				 }else{
					var sched = JSON.parse(output);
					
						 if(sched.login_stat=="Late"){
							 var violate_ID=1;
						 }else{
							 var violate_ID=null;
						 }
						 // alert(violate_ID);
						 $.post("<?php echo base_url('dtr/dtrLogSave'); ?>", {type:"DTR",entry:"I",acc_time_id:sched.acc_time_id,sched_id:sched.sched_id,schedDate:sched.sched_date,dtrViolationType_ID:violate_ID,loginID:null}, function(result){
							var rs = JSON.parse(result);
							  
							if(rs["res"]==1){
								var ifLate= (sched.login_stat=="Late") ? "Successfully clocked in but you are late." : "Successfully clocked in";
 								  swal(
								  'Success',
								  ifLate,
								  'success'
								); 
								 $("#schedIDdiv").data("id",sched.sched_id);
								 var arr = rs["systemNotifID"];
								 systemNotification([arr]);
								 // showSurvey();
								 location.reload();
							}else{
								swal(
								  'Error',
								  "Please inform your supervisor about this.",
								  'error'
								)
							}
						});  
				 }
				
			}
		 });
		
	});
	$("#btnClockOutExcempt").click(function(){
		var acc_time_id= $("#divInfoClock").data("acc_time_id");
		var sched_id= $("#divInfoClock").data("sched_id");

		 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/dtrOutcheck'); ?>",
			data: {
 				acc_time_id: acc_time_id,
				sched_id: sched_id,
 			},
			cache: false,
			success: function (json) {
				var output = JSON.parse(json);
 				if(output.limit_ut=="UT"){
					swal({
					  title: 'UNDERTIME',
					  text: "Are you sure you want to proceed?",
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, I will go home early',
					  closeOnConfirm: false,
					  closeOnCancel: false,
					  allowOutsideClick: false

					}).then((result) => {
					  if (result.value) {
					 $.post("<?php echo base_url('dtr/dtrLogSave'); ?>", {type:"DTR",entry:"O",acc_time_id:acc_time_id,sched_id:sched_id,schedDate:output.schedDate,dtrViolationType_ID:2,loginID:output.emp_last_logs[0]["dtr_id"]}, function(result){
							   var rs = JSON.parse(result);
							  
									if(rs["res"]==1){
  								 swal(
								  'Success',
								  "Successfully clocked out but you are undertime.",
								  'success'
								)
								var arr = rs["systemNotifID"];
 								 systemNotification([arr]);
									location.reload();
							}else{
							
								swal(
								  'Error',
								  "Please inform your supervisor about this.",
								  'error'
								)
							}
						}); 
						
					  }else{
					 
					  }
					})
					
				}else{
					$.post("<?php echo base_url('dtr/dtrLogSave'); ?>", {type:"DTR",entry:"O",acc_time_id:acc_time_id,sched_id:sched_id,schedDate:output.schedDate,}, function(result){
									   var rs = JSON.parse(result);
									  
											if(rs["res"]==1){
											
												// socket.emit('dtr violation', {
												//     empid: rs["emp_id"],
												//     uid: <?php echo $_SESSION["uid"]; ?>,
												//     supervisor: rs["sup"],
												// });
												// alert("index page = "+rs.systemNotifID);
												 swal(
												  'Success',
												  "Successfully clocked out",
												  'success'
												);
												setTimeout(function(){
													location.reload();
												},2000);
											}else{
												swal(
												  'Error',
												  "Please inform your supervisor about this..",
												  'error'
												)
											}
										}); 
				}
			}
		});
		
	});
	$("#btnClockIn").click(function(){
		 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/dtrInCheck'); ?>",
			cache: false,
			success: function (json) {
 				 if(json==0){
					swal(
					  'NO NEW SCHEDULE',
					  'Please approach your supervisor to upload schedule.',
					  'error'
					)
				 }else{
				 var sched = JSON.parse(json);
					 if(sched.login_allowed=="Not"){
 						 swal(
						  'Opps! Too early.',
						  "You can only login after "+sched.login_allowed_time,
						  'error'
						)
					 }else{
						 if(sched.login_stat=="Late"){
							 var violate_ID=1;
						 }else{
							 var violate_ID=null;
						 }
						 // alert(violate_ID);
						 $.post("<?php echo base_url('dtr/dtrLogSave'); ?>", {type:"DTR",entry:"I",acc_time_id:sched.acc_time_id,sched_id:sched.sched_id,schedDate:sched.sched_date,dtrViolationType_ID:violate_ID,loginID:null}, function(result){
							var rs = JSON.parse(result);
							  
							if(rs["res"]==1){
								var ifLate= (sched.login_stat=="Late") ? "Successfully clocked in but you are late." : "Successfully clocked in";
 								  swal(
								  'Success',
								  ifLate,
								  'success'
								); 
								 $("#schedIDdiv").data("id",sched.sched_id);
								 var arr = rs["systemNotifID"];
								 systemNotification([arr]);
 
								 socket.emit('live', {
									  emp_id: rs.personal[0].emp_id,
									  sup: rs.personal[0].sup,
									  fname: rs.personal[0].fname,
									  lname: rs.personal[0].lname,
									  pic: rs.personal[0].pic,
									  uid: rs.personal[0].uid,
									  position: rs.personal[0].position,
									  account: rs.personal[0].account,
									  dtr: rs.personal[0].dtr,
								  });
								 // showSurvey();
								 location.reload();
							}else{
								swal(
								  'Error',
								  "Please inform your supervisor about this.",
								  'error'
								)
							}
						});  
					 }
				 
				 }
			}
		});
	});
	 
	$("#btnClockOut").click(function(){
		
		var acc_time_id= $("#divInfoClock").data("acc_time_id");
		var sched_id= $("#divInfoClock").data("sched_id");
		 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/dtrOutcheck'); ?>",
			data: {
 				acc_time_id: acc_time_id,
				sched_id: sched_id,
 			},
			cache: false,
			success: function (json) {
				var output = JSON.parse(json);
				
				if(output.limit_ut=="UT"){
					swal({
					  title: 'UNDERTIME',
					  text: "Are you sure you want to proceed?",
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, I will go home early',
					  closeOnConfirm: false,
					  closeOnCancel: false,
					  allowOutsideClick: false

					}).then((result) => {
					  if (result.value) {
					 $.post("<?php echo base_url('dtr/dtrLogSave'); ?>", {type:"DTR",entry:"O",acc_time_id:acc_time_id,sched_id:sched_id,schedDate:output.schedDate,dtrViolationType_ID:2,loginID:output.emp_last_logs[0]["dtr_id"]}, function(result){
							   var rs = JSON.parse(result);
							  
									if(rs["res"]==1){
  								 swal(
								  'Success',
								  "Successfully clocked out but you are undertime.",
								  'success'
								)
								var arr = rs["systemNotifID"];
								 socket.emit('live', {
									  emp_id: rs.personal[0].emp_id,
									  sup: rs.personal[0].sup,
									  fname: rs.personal[0].fname,
									  lname: rs.personal[0].lname,
									  pic: rs.personal[0].pic,
									  uid: rs.personal[0].uid,
									  position: rs.personal[0].position,
									  account: rs.personal[0].account,
									  dtr: rs.personal[0].dtr,
								  });
								  
								  		setTimeout(function(){
										location.reload();
									 },2000);

 								 // systemNotification([arr]);
									 setTimeout(function(){
										location.reload();
									 },2000);
							}else{
							
								swal(
								  'Error',
								  "Please inform your supervisor about this.",
								  'error'
								)
							}
						}); 
						
					  }else{
					 alert("error");
					  }
					})
					
				}else{
						if(output.limit_remarks=="Over"){
							swal({
							  title: 'TIMED OUT',
							  text: "You have just exceeded "+output.limit_hour+" given to logout. Please choose an action to procceed.",
							  type: 'error',
							  showCancelButton: true,
							  confirmButtonColor: '#3085d6',
							  cancelButtonColor: '#3085d6',
							  confirmButtonText: 'Proceed..',
							  closeOnConfirm: false,
							  closeOnCancel: false,
							  allowOutsideClick: false

							}).then((result) => {
							  if (result.value) {
								swal({
								  title: 'Please indicate the reason of your logging out late.',
								  input: 'text',
								  inputAttributes: {
									autocapitalize: 'off'
								  },
								  showCancelButton: true,
								  confirmButtonText: 'Submit',
								  showLoaderOnConfirm: true,
								  closeOnConfirm: false,
								  closeOnCancel: false,
								  allowOutsideClick: false,

								  preConfirm: (login) => {
									// alert(login)
								  },
								  allowOutsideClick: () => !swal.isLoading()
								}).then((result) => {
								
								
								  if (result.value){ //If OT -> send reason why late nag logout
									   $.post("<?php echo base_url('dtr/dtrLogSave'); ?>", {type:"DTR",entry:"O",acc_time_id:acc_time_id,sched_id:sched_id,schedDate:output.schedDate,dtrViolationType_ID:3,loginID:output.emp_last_logs[0]["dtr_id"]}, function(result){
									   var rs = JSON.parse(result);
									  
											if(rs["res"]==1){
											
												// socket.emit('dtr violation', {
												//     empid: rs["emp_id"],
												//     uid: <?php echo $_SESSION["uid"]; ?>,
												//     supervisor: rs["sup"],
												// });
												// alert("index page = "+rs.systemNotifID);
												 var arr = rs["systemNotifID"];
												 systemNotification([arr]);
												 swal(
												  'Success',
												  "Successfully clocked out",
												  'success'
												)
													setTimeout(function(){
														location.reload();
													 },2000);
											}else{
												swal(
												  'Error',
												  "Please inform your supervisor about this..",
												  'error'
												)
											}
										}); 
									 // swal("Thank you!",'You just have successfully sent your comment to your supervisor for review.'+result.value,'success')
								  }else{
									  
									  swal("Required",'Please state your reason.','error')
								  }
								})
							  }
							});
						}else{ 
							$.post("<?php echo base_url('dtr/dtrLogSave'); ?>", {type:"DTR",entry:"O",acc_time_id:acc_time_id,sched_id:sched_id,schedDate:output.schedDate,loginID:output.emp_last_logs[0]["dtr_id"]}, function(result){
									   var rs = JSON.parse(result);
									  
											if(rs["res"]==1){
											
												// socket.emit('dtr violation', {
												//     empid: rs["emp_id"],
												//     uid: <?php echo $_SESSION["uid"]; ?>,
												//     supervisor: rs["sup"],
												// });
												// alert("index page = "+rs.systemNotifID);
												 swal(
												  'Success',
												  "Successfully clocked out",
												  'success'
												);
												setTimeout(function(){
													location.reload();
												},2000);
											}else{
												swal(
												  'Error',
												  "Please inform your supervisor about this..",
												  'error'
												)
											}
										}); 
						
						
						}
				}
			}
		});
		
	});
 	
	$("#btnShwoDTRlogs").click(function(){
		var date= $('#m_daterangepicker_1').val();
		$("#noRecordShow").hide();
 		 showDTR(date); 
		
		
	});
	
	$(".checkTimeSched").change(function(){
		var time = ($("#mySelectSched").find("option:selected").text()).split("-");
		var dateStart =  $("#dateActualWorked").val();
		var dateEnd =  $("#reqDateEnd").val();
		if(time[0]!=" "){
		 $('#reqTimeStart').timepicker('setTime', time[0]);
		 $('#reqTimeEnd').timepicker('setTime', time[1]);
			 if(time[0].trim() == "12:00:00 AM"){
				$("#reqDateEnd").val(moment(dateStart).add(1, 'days').format('YYYY-MM-DD'));
				$("#reqDateStart").val(dateStart); 
			 }else{
			  $("#reqDateEnd").val(dateStart);
			  $("#reqDateStart").val(dateStart);
			 }
		}else{
		var current_time = new moment().format("HH:mm");

		 $('#reqTimeStart').timepicker('setTime', current_time);
		 $('#reqTimeEnd').timepicker('setTime', current_time);
		
		} 
		 $.post("<?php echo base_url("dtr/checkTimeOfShift"); ?>", {timeStart: time[0],timeEnd: time[1]}, function(result){
				var rs = result.split("|");
				if(rs[1]=="Tomorrow"){
					$("#reqDateEnd").val(moment(dateStart).add(1, 'days').format('YYYY-MM-DD'));
					 $("#reqDateStart").val(dateStart);
				}else{
				if(time[0].trim() == "12:00:00 AM"){
					$("#reqDateEnd").val(moment(dateStart).add(1, 'days').format('YYYY-MM-DD'));
					$("#reqDateStart").val(dateStart); 
				 }else{
				  $("#reqDateEnd").val(dateStart);
				  $("#reqDateStart").val(dateStart);
				 }
				
				}
				checkTotalTimeStatus();
 			});
	});
	$("#btnSubmitRequest").click(function(){
		var date= $('#dateReq').val();
		var schedule= $('#mySelectSched').val();
		var reason= $('#exampleTextarea').val();
		swal({
			  title: 'Requesting Time-in and Time-out',
			  text: "Are you sure you want to proceed?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes',
			  closeOnConfirm: false,
			  closeOnCancel: false,
			  allowOutsideClick: false

			}).then((result) => {
			  if (result.value) {
					/*  $.ajax({
					type: "POST",
					url: "<?php echo base_url("dtr/requestTiTo"); ?>",
					data: {
						date: date,
						schedule: schedule,
						reason: reason
					},
					cache: false,
					success: function (res) {
						var rs = JSON.parse(res.trim());
						if(res=="0"){
								alert("error");
						}else{
							 notification(rs.notif_id);
							 swal('Success',"Successfully clocked out",'success');
							$("#m_datepicker_modal").modal("hide");
						}

					}
				}); */
				alert("Yes");
				
			  }else{
				alert("No");
			  }
			})
 		
	});
	
	
	 $('#m_daterangepicker_1').daterangepicker({
		opens: 'right'
	}, function(start, end, label) {
		$("#noRecordShow").hide();
		  var date= start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY');
			showDTR(date);
	  });
  
});
</script>


<script>
	$(function(){
		var from = moment(<?php date('m/d/Y');?>).subtract(5, 'days').format('MM/DD/YYYY');
		var to = moment(<?php date('m/d/Y');?>).add(0, 'days').format('MM/DD/YYYY');
  	showDTR(from+" - "+to);
	console.log(<?php echo $dateOut; ?>);
	   // $('#popup_survey').modal("show"); 

		 $('#mySelectSched').select2({
                placeholder: "Select your schedule.",
				width: '100%'
         });
	});
</script>
 

