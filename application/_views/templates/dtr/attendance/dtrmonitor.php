<link rel="stylesheet" href="<?php echo base_url("assets/css/cards-style.css"); ?>" type="text/css" /> </head>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<style>
.btn-group.buttons {
    background: #575962;
}
button.btn.moveall.btn-outline-secondary {
    margin: 0;
}
button.btn.removeall.btn-outline-secondary {
    margin: 0;
}
#tblDtrShow thead{
     background: #34bfa3;
    color: #fff;
    font-size: initial;
}
#tblDtrShow thead th {
    padding: 6px;
}
.bootstrap-duallistbox-container {
    width: 100%;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Supervisor Attendance Monitoring</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Attendance Monitoring</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			 <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist" style="border: 0;">
                                 <!--<li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i> Live DTR</a>
                                </li>-->
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i> Raw Attendance Logs</a>
                                </li>
								<!--<button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right" id="toggleFilter" style="right: 36px;position: absolute;">
                                    <i class="fa fa-filter"></i>
								</button>-->
                            </ul>
                        </div>
                    </div>
			
            <div class="m-portlet__body pt-0">

                <div class="tab-content">
                   <!-- <div class="tab-pane " id="m_tabs_6_1" role="tabpanel">
						<div class="row" id="filters" style="display:none;">
							<div class="m-form m-form--fit m-form--label-align-right" style="width:100%">
								<div class="form-group m-form__group row" style="background: #e4e3e3;padding-top: 25px;">
									
										<label><b>Type:</b></label>
										<div class="col-lg-5">
										
											<select class="form-control m-select2" id="empType" onchange="live_log_record()">
												<option>ALL</option>
												<option value="Admin">Admin</option>
												<option value="Agent">Agent</option>
											</select>
										</div>
										<label><b>Account:</b></label>
										<div class="col-lg-5">
										
											<select class="form-control m-select2" id="empAccount" onchange="live_log_record()" >
												<option>ALL</option>
											</select>
										</div>
										
										
								</div>
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
									<label><b>Employees</b></label>
									<select multiple="multiple" class="form-control m-input" name="duallistbox_live[]" size="5" id="employeeList" >
									</select>
									
								</div>
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
								<label></label>
									<button type="button" class="btn m-btn--pill m-btn--air btn-info" onclick="live_log_record()">Show</button>
 								</div>
								<hr>
								
							</div>
								
						</div>
								<div class="container" style="margin-top: 5%;">
									<div class="row" id="div_live_log_monitoring">
										 <span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>
									</div>
								</div>
                    </div>-->
                   
                    <div class="tab-pane active" id="m_tabs_6_3" role="tabpanel">
					<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;">
								<div class="form-group m-form__group row" style="background: #e4e3e3;padding-top: 25px;">
									
										<label><b>Type:</b></label>
										<div class="col-lg-5">
											<select class="form-control m-select2" id="empTypeRecord"  onchange="record_log_record(1)" >
												<option disabled selected>--</option>
												<option>ALL</option>
												<option value="Admin">Admin</option>
												<option value="Agent">Agent</option>
											</select>
										</div>
										<label><b>Date:</b></label>
										<div class="col-lg-5">
										<input type="text" class="form-control m-input datePick" readonly  placeholder="Select date" id="dateReq" style="background:#fff"/>
										</div>
								</div>
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
									<label><b>Employees</b></label>
									<select multiple="multiple" class="form-control m-input"  size="5"  name="duallistbox_record[]" id="employeeListRecord" >
									</select>
 								</div>
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
								<label></label>
									<button type="button" class="btn m-btn--pill m-btn--air btn-info" id="btnShowLogs">Show</button>
 								</div>
								<hr>
								
							</div>
								
						</div>
								<div class="container" style="margin-top: 5%;overflow: scroll;text-align: center;">
								<div class="div_record_log_monitoring">
								</div>
									<div class="row" id="div_record_log_monitoring" style="width: 2000px;">
										 <span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>
									</div>
								</div>
                    </div>
                </div>  

               
 
            </div>
        </div>
		</div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

 <script>

 function record_log_record(id) {
		var class_type = $("#empTypeRecord").val();
		var account = $("#empAccountRecord").val();
		var employeeList = $("#employeeListRecord").val();
		$.ajax({
				type: "POST",
				url: "<?php echo base_url('dtr/dtr_current_status'); ?>",
				data: {
					class_type : class_type,
					account : account,
					emp : employeeList,
				},
				cache: false,
				beforeSend:function(data){
					$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");

				},
				success: function (json) {
					$("#div_record_log_monitoring").html("");
					var result = JSON.parse(json);
					var str_emp = "";

						$.each(result.dtr, function (x, item){
								if(class_type==item.acc_description || class_type=="ALL"){
 										str_emp += '<option  '+item.selected+' value='+item.emp_id+' >'+item.fname+" "+item.lname+'</option>';
									 
								}else{
									str_emp +="";
								}
						}); 
 						 $("#employeeListRecord").html(str_emp);
						 $("#employeeListRecord").bootstrapDualListbox('refresh');
				}
		});
 }
 function live_log_record() {
		var class_type = $("#empType").val();
		var account = $("#empAccount").val();
		var employeeList = $("#employeeList").val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('dtr/dtr_current_status'); ?>",
				data: {
					class_type : class_type,
					account : account,
					emp : employeeList,
				},
				cache: false,
				beforeSend:function(data){
					$("#div_live_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");

				},
				success: function (json) {
 					
					if (json === 'Empty') {
						$("#div_live_log_monitoring").html("<span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>");
						 var str_acc = "<option>ALL</option>";
						var str_emp = "<option></option>";
					
					  $("#empAccount").html(str_acc);
					  $("#employeeList").html(str_emp);
					  $("#employeeList").bootstrapDualListbox('refresh');
					}else{
						
					  var result = JSON.parse(json);
					  $("#div_live_log_monitoring").html("<span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>");
					  var str = '';
					  var str_emp = "";

					  var str_acc = "<option>ALL</option>";
					  $.each(result.account, function (x, item){
						  str_acc+="<option "+item+">"+x+"</option>";
					  });
					  $("#empAccount").html(str_acc);
					  $.each(result.dtr, function (x, item){
						  
							  if(item.dtr.status=="Online"){
								var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--success m-animate-blink" style="height: 13px;width: 13px;"></span>';
							 }else if(item.dtr.status=="Offline"){
								var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--danger " style="height: 13px;width: 13px;"></span>';
							 }else if(item.dtr.status=="FIRST BREAK"){
								var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--info "></span>';
							 }else if(item.dtr.status=="LUNCH"){
								var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--warning " style="height: 13px;width: 13px;"></span>';
							 }else if(item.dtr.status=="LAST BREAK"){
								var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--primary "></span>';
							 }else{
								var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--dark "></span>';
							 }
							 if(item.selected=="selected"){
								str += '<div class="col-md-4" id="emp'+item.uid+'">';
								str += '<div class="card card-profile">';
								str += '<div class="card-avatar" style="background: #fff;width: 26%;">';
								str += '<img class="img header-userpic'+item.emp_id+'" src='+baseUrl+"/assets/images/"+item.pic+' onerror="noImage('+item.emp_id+')">';
								str += '</div>';
								str += '<div class="table">';
								str += '<h4 class="card-caption">'+item.fname+" "+item.lname+'</h4>';
								str += '<h6 class="category text-muted" style="font-size: 90%;margin-top: 10px;font-weight: bold;color: #5b53c2 !important;width: 100%;white-space: nowrap;">'+item.position+'</h6>';
								str += '<h6 class="category text-muted" style="font-size: smaller;">'+item.account+'</h6><hr>';

								str += '<p class="card-description" style="font-size: larger;"> '+animate+' '+item.dtr.status+'</p>';
								str += '</div>';
								str += '</div>';
								str += '</div>';
								str += '</div>';
								$("#div_live_log_monitoring").html("");

							 }
								 
								if(class_type==item.acc_description || class_type=="ALL"){
									if(account==item.account || account=="ALL"){
										str_emp += '<option  '+item.selected+' value='+item.emp_id+' >'+item.fname+" "+item.lname+'</option>';
									}
								}else{
									str_emp +="";
								}
								
							  
						}); 
						 $("#div_live_log_monitoring").append(str);
						 $("#employeeList").html(str_emp);
						 $("#employeeList").bootstrapDualListbox('refresh');

					}
				}
			});
		
	}
	function live_log_record_show() {
		socket.on('log', function (details) {
		 console.log(details);
		 $("#emp"+details.uid).remove();
		
		  if(details.dtr.status=="Online"){
			var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--success m-animate-blink" style="height: 13px;width: 13px;"></span>';
		 }else if(details.dtr.status=="Offline"){
			var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--danger " style="height: 13px;width: 13px;"></span>';
		 }else if(details.dtr.status=="FIRST BREAK"){
			var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--info "></span>';
		 }else if(details.dtr.status=="LUNCH"){
			var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--warning " style="height: 13px;width: 13px;"></span>';
		 }else if(details.dtr.status=="LAST BREAK"){
			var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--primary "></span>';
		 }else{
			var animate = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-x-large m-badge--dark "></span>';
		 }
		 var str = '';
  			str += '<div class="col-md-4" id="emp'+details.uid+'">';
			str += '<div class="card card-profile">';
			str += '<div class="card-avatar" style="background: #fff;width: 26%;">';
			str += '<a href="#"> <img class="img header-userpic'+details.uid+'" src='+baseUrl+"/assets/images/"+details.pic+' onerror="noImage('+details.uid+')"> </a>';
			str += '</div>';
			str += '<div class="table">';
			str += '<h4 class="card-caption">'+details.fname+" "+details.lname+'</h4>';
			str += '<h6 class="category text-muted" style="font-size: 14px;margin-top: 10px;font-weight: bold;color: #5b53c2 !important;">'+details.position+'</h6>';
			str += '<h6 class="category text-muted" style="font-size: smaller;">'+details.account+'</h6><hr>';

			str += '<p class="card-description" style="font-size: larger;"> '+animate+' '+details.dtr.status+'</p>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '</div>';

 			
			$("#div_live_log_monitoring").append(str);
			
	 });
	}
 $(function(){ 
		
	 
	 live_log_record();
	 live_log_record_show();
		$('.m-select2').select2({
                placeholder: "Select your schedule.",
				width: '100%'
         });
$('#dateReq').daterangepicker({});
$("#btnShowLogs").click(function(){
 	var date = $("#dateReq").val();
	var emp = $("#employeeListRecord").val();
	$.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/dtr_view_logs'); ?>",
			data: {
				date : date,
				emp : emp,
			},
			cache: false,
			beforeSend:function(data){
				$(".div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
				$("#div_record_log_monitoring").html("");
				
			},
			success: function (json) {
				$(".div_record_log_monitoring").html("");
				
				var result = JSON.parse(json);
				console.log(result);
				var tbl = "<table style='width: 100%;' id='tblDtrShow'>";
				tbl+="<thead><tr><th>LAST NAME</th>"+
					 "<th>FIRST NAME</th>"+
					 "<th>DATE</th>"+
					 "<th>Type</th>"+
					 "<th>SCHEDULE</th>"+
					 "<th>ACTUAL IN</th>"+
					 "<th>ACTUAL OUT</th>"+
					 "<th>FIRST BREAK-OUT</th>"+
					 "<th>FIRST BREAK-IN</th>"+
					 "<th>LUNCH-OUT</th>"+
					 "<th>LUNCH-IN</th>"+
					 "<th>LAST BREAK-OUT</th>"+
					 "<th>LAST BREAK-IN</th>"+
					 "<th>TOTAL BREAK</th>"+
					 "</tr></thead>";
					 var i=1;
					
				$.each(result, function (x, item){
					 var bg = (i%2 == 0) ? "#fff" : "#c4c5d65e";
					 var color = (i%2 == 0) ? "#575962" : "#000";
					tbl+= "<tr><td colspan=5></td></tr>";
					$.each(item, function (y, item2){
						$.each(item2, function (z, item3){
							var fbo="",fbi="";
							var luo="",lui="";
							var lbo="",lbi="";
							var schedule = (item3.timeschedule!="--") ? item3.timeschedule[0].time_start+" to "+item3.timeschedule[0].time_end : "No Schedule";
							var login = (item3.actual_in!="") ?  moment(item3.actual_in[0].login).format('lll') : "<span class='text-danger'>No Login</span>";
 							var logout = (item3.actual_out!="") ? moment(item3.actual_out[0].logout).format('lll') : "<span class='text-danger'>No Logout</span>";
							tbl+="<tr style='background:"+bg+";color:"+color+"'><td>"+item3.lname+"</td>"+
								"<td>"+item3.fname+"</td>"+
								"<td>"+y+"</td>"+
								"<td><span style='color:"+item3.style+"'>"+item3.type+"</span></td>"+
								"<td>"+schedule+"</td>"+
								"<td>"+login+"</td>"+
								"<td>"+logout+"</td>";
								
								
								 $.each(item3.breakz, function (x, item){
									if(item.break_type=="FIRST BREAK"){
										if(item.entry=="O" && fbo==""){
										fbo=item.log;
										}
										if(item.entry=="I" && fbi==""){
										fbi=item.log;
										}
									}
									if(item.break_type=="LUNCH"){
										if(item.entry=="O" && luo==""){
											luo=item.log;
										}
										if(item.entry=="I" && lui==""){
											lui=item.log;
										}
									}
									if(item.break_type=="LAST BREAK"){
										if(item.entry=="O" && lbo==""){
										lbo=item.log;
										}
										if(item.entry=="I" && lbi==""){
										lbi=item.log;
										}
									}

								});  
								var totalfb = 0;
								var totallu = 0;
								var totallb = 0;
								if((fbo!="") && (fbi!="")){
									var f1 = moment(fbo).format();
									var f2 = moment(fbi).format();
									var duration = moment.duration(moment(f2).diff(f1));
									totalfb = duration.asHours();
								}
								if((luo!="") && (lui!="")){
									var lu1 = moment(luo).format();
									var lu2 = moment(lui).format();
									var duration = moment.duration(moment(lu2).diff(lu1));
									totallu =  duration.asHours();
								}
								if((lbo!="") && (lbi!="")){
									var l1 = moment(lbo).format();
									var l2 = moment(lbi).format();
									var duration = moment.duration(moment(l2).diff(l1));
									totallb =  duration.asHours();
 
								}
								var totalBreak = ((totalfb+totallu+totallb)*60);
								console.log(item3.fname+" "+y+" "+totalfb+" "+totallu+" "+totallb);
								tbl+="<td>"+((fbo!="") ? moment(fbo).format('lll') : "<span class='text-danger'>No Logs</span>")+"</td>";
								tbl+="<td>"+((fbi!="") ? moment(fbi).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
								tbl+="<td>"+((luo!="") ? moment(luo).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
								tbl+="<td>"+((lui!="") ? moment(lui).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
								tbl+="<td>"+((lbo!="") ? moment(lbo).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
								tbl+="<td>"+((lbi!="") ? moment(lbi).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
								tbl+="<td>"+totalBreak.toFixed(2)+" mins.</td>";
								
								tbl+="</tr>";
						}); 
					}); 
					i++;
				}); 
				tbl += "</table>";				
				$("#div_record_log_monitoring").html(tbl);
				 
			}
	});
});
$('select[name="duallistbox_live[]"]').bootstrapDualListbox();
$('select[name="duallistbox_record[]"]').bootstrapDualListbox();
	$('#toggleFilter').on('click', function () {
		$('#filters').toggle(1000);
	});
	 
 });
 </script>