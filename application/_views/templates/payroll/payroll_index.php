<style>
.h3, h3 {
	font-size: 1.2rem !important;
}
<?php if($site=="cebu") {?>
	.divHeaderStyle{
		background: #006666;
		text-align: center;
		padding: 5px;
	}
<?php }else{?>
	.divHeaderStyle{
		background: #cc6633;
		text-align: center;
		padding: 5px;
	}
<?php }?>
</style>
<?php
	$date = explode("-",$pcover[0]->daterange); 
	$data = date('F d', strtotime($date[0]))." - ".date('d , Y', strtotime($date[1]));
 ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("/payroll/coverage"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Coverage</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<?php echo " > ".$data;?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
	 
		<div class="row">
			<div class="col-xl-10">
				<u><h3 class="m-portlet__head-text"><i class="fa fa-user-circle"></i> <?php echo strtoupper($site); ?> - Call Center Ambassador</h3></u>
			</div>
		</div>
		<br>
		<div class="row">
				<div class="col-xl-3">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div class="divHeaderStyle">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											Trainee
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/general/cca/trainee/".$pcover[0]->coverage_id."/".$site); ?>">
							<div class="m-portlet__body">
								<div class="m-widget4__info" style="text-align:center">
									<img src="<?php echo base_url()."assets/img/sz-ambs-trainee.png"?>">
								</div>
							</div>
							</a>
					</div>
				</div>
				<div class="col-xl-3">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div class="divHeaderStyle">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											Probationary
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/general/cca/probationary/".$pcover[0]->coverage_id."/".$site); ?>">
							<div class="m-portlet__body">
									<div class="m-widget4__info" style="text-align:center">
										<img src="<?php echo base_url()."assets/img/probationary-cca.png"?>">
									</div>
							</div>
							</a>
					</div>
				</div>
				<div class="col-xl-3">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div class="divHeaderStyle">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											Regular
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/general/cca/regular/".$pcover[0]->coverage_id."/".$site); ?>">
							<div class="m-portlet__body">
										<div class="m-widget4__info" style="text-align:center">
											<img src="<?php echo base_url()."assets/img/reg-cca.png"?>">
										</div>
							</div>
							</a>
					</div>
				</div>
         </div>
		 <div class="row">
			<div class="col-xl-10">
				<u><h3 class="m-portlet__head-text"><i class="fa fa-user-circle-o"></i> <?php echo strtoupper($site); ?> -  SZ Team</h3></u>
			</div>
		</div>
		<br>
		<div class="row">
				<div class="col-xl-3">
					<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div class="divHeaderStyle">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text" style="color:#fff !important;">
										Trainee
									</h3>
								</div>
							</div>
							
						</div>
						<a href="<?php echo base_url("/payroll/general/admin/trainee/".$pcover[0]->coverage_id."/".$site); ?>">
						<div class="m-portlet__body">
									<div class="m-widget4__info" style="text-align:center">
										<img src="<?php echo base_url()."assets/img/sz-trainee.png"?>">
									</div>
						</div>
						</a>
				</div>
			</div>
				<div class="col-xl-3">
					<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div class="divHeaderStyle">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text" style="color:#fff !important;">
										Probationary & Regular
									</h3>
								</div>
							</div>
							
						</div>
						<a href="<?php echo base_url("/payroll/general/admin/probationary-regular/".$pcover[0]->coverage_id."/".$site); ?>">
						<div class="m-portlet__body">
									<div class="m-widget4__info" style="text-align:center">
										<img src="<?php echo base_url()."assets/img/sz-pr.png"?>">
									</div>
						</div>
						</a>
				</div>
			</div>
				<div class="col-xl-3">
					<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div class="divHeaderStyle">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text" style="color:#fff !important;">
										Confidential
									</h3>
								</div>
							</div>
							
						</div>
						<a href="<?php echo base_url("/payroll/general/admin/confidential/".$pcover[0]->coverage_id."/".$site); ?>">
						<div class="m-portlet__body">
									<div class="m-widget4__info" style="text-align:center">
										<img src="<?php echo base_url()."assets/img/sz-confi.jpg"?>">
									</div>
						</div>
						</a>
				</div>
			</div>
        </div>
	</div>
	</div>
 <script>
 
	$(function(){
	
	});
 
 </script>