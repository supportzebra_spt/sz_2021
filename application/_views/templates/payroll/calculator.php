<link rel="stylesheet" href="<?php echo base_url("assets/css/cards-style.css"); ?>" type="text/css" /> </head>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<style>
.btn-group.buttons {
    background: #575962;
}
button.btn.moveall.btn-outline-secondary {
    margin: 0;
}
button.btn.removeall.btn-outline-secondary {
    margin: 0;
}
#tblDtrShow thead{
     background: #34bfa3;
    color: #fff;
    font-size: initial;
}
#tblDtrShow thead th {
    padding: 6px;
}
.bootstrap-duallistbox-container {
    width: 100%;
}
thead {
    background: #575962;
}
thead td {
	padding: 10px;
    color: #FFEB3B;
    font-weight: 300;
}
tbody td {
    padding: 9px;
    border: 1px solid #bdc0cc;
    color: #000;
}
tbody tr:hover {
    background: #fff7b7;
	font-weight: 500;
}
.h3, h3 {
	font-size: .98rem !important;
}
td.allowanceStyle {
    background: #c7f1c9;
    border: 1px solid #7ec181;
}
td.govDeductStyle {
    background: #a6d6fd;
    border: 1px solid #65b7f9;
}
td.homePayStyle {
    background: #ffd885;
    font-size: larger;
    font-weight: 600;
}
td.earnStyle,td.deductStyle{
	text-decoration-line: underline;
    cursor: pointer;
    text-decoration-style: dashed;
    text-decoration-color: #8c8c8c;
    text-decoration-skip-ink: none;
    color: #1f9cff;
    font-weight: 600;
}
</style>
<?php
	$date = explode("-",$pcover[0]->daterange); 
	$data = date('F d', strtotime($date[0]))." - ".date('d , Y', strtotime($date[1]));
  ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator"><?php echo strtoupper($site); ?> Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("/payroll/coverage"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Coverage</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/emp/".$pcover[0]->coverage_id)."/".$site; ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > Base</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/general/".$type."/".$empstat."/".$pcover[0]->coverage_id."/".$site); ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > General</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<?php echo " > ".$data;?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			 <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist" style="border: 0;">
                                 <!--<li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i> Live DTR</a>
                                </li>-->
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i>Payroll Page</a>
                                </li>
								<!--<button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right" id="toggleFilter" style="right: 36px;position: absolute;">
                                    <i class="fa fa-filter"></i>
								</button>-->
                            </ul>
                        </div>
                    </div>
			
            <div class="m-portlet__body pt-0" style="background: #e4e3e3;">

                <div class="tab-content">
                  
                   
                    <div class="tab-pane active" id="m_tabs_6_3" role="tabpanel">
					<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;">
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
									<select multiple="multiple" class="form-control m-input"  size="5"  name="duallistbox_record[]" id="employeeListRecord" >
									
									</select>
 								</div>
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
								<label></label>
									<button type="button" class="btn m-btn--pill m-btn--air btn-info btn-sm" id="btnShowLogs">Show</button>
									<button type="button" class="btn m-btn--pill m-btn--air btn-success btn-sm" id="btnSavePayroll" style="display:none">Save</button>
 								</div>
								
								
							</div>
								
						</div>
								<div style="overflow: scroll;text-align: center;border: 1px solid #bdc0cc;max-height: 800px;">
								<div class="div_record_log_monitoring">
								</div>
									<div class="row" id="div_record_log_monitoring" style="background: #fff;" >
										 <span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>
									</div>
								</div>
                    </div>
                </div>  

               
 
            </div>
        </div>
		</div>
	</div>
<div class="modal fade" id="modalShow"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Add Adjustments</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		   <form class="m-form m-form--fit m-form--label-align-right" id="defaultForm">
		  <div class="modal-body">
		
				<div class="result-output">

					
				</div>

		  </div>
		  <div class="modal-footer">
				<button type="" class="btn btn-danger m-btn" data-dismiss="modal">Close</button>
		  </div>
		  </form>
		</div>
		
	  </div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

 <script>
 
  function emp_list(){
		var employeeList = $("#employeeListRecord").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/loadEmployee'); ?>",
		data: {
			class_type : '<?php echo $type; ?>',
			empstat :  '<?php echo $empstat; ?>',
			site :  '<?php echo $site; ?>',
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
 		},
		cache: false,
		beforeSend:function(data){
			$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
		},
		success: function (json) {
			$("#div_record_log_monitoring").html("");
			var result = JSON.parse(json);
			var str_emp = "";

				$.each(result.employee, function (x, item){
					var selected = (item.payroll_id >0) ? "selected=selected": "" ;
					str_emp += '<option   value='+item.emp_id+' '+selected+'>'+item.lname+", "+item.fname+'</option>';
						 
				}); 
				 $("#employeeListRecord").html(str_emp);
				 $("#employeeListRecord").bootstrapDualListbox('refresh');
		}
	});
}
 
 function getEarnings(txt,type){
	 
	 var data = (type==1) ? $("#"+txt).data("earn") : $("#"+txt).data("deduct");
	 var name = $("#"+txt).data("name");
	 if(type==1){
		$("#exampleModalLongTitle").html("Gross Salary Details of "+name);
	 }else{
		$("#exampleModalLongTitle").html("Deduction details of "+name);
	 }
	 $("#modalShow").modal("show");
	 var total = 0;

	 var rs = data.split(",");
	 var tbl = "<table style='width: 100%;'>";
	 tbl += "<thead><tr><td>Description</td><td>Amount</td></tr></thead>";
	for(var i=0;i<rs.length;i++){
		var x = rs[i].split("=");
		if(x[1]>0){
			tbl += "<tr><td>"+x[0]+"</td><td>"+x[1]+"</td></tr>";
			total +=Number(x[1]);

		}
	}
	 tbl += "<tr style='background: #d6d6d6;font-weight: 900;font-size: initial;'><td>Total</td><td>"+total.toFixed(2)+"</td></tr>";
	 tbl += "</table>";

	 $(".result-output").html(tbl);
 }
 
 $(function(){ 
		
	 
 		$('.m-select2').select2({
                placeholder: "Select your schedule.",
				width: '100%'
         });
$('#dateReq').daterangepicker({});
  
  
  
  
  $("#btnSavePayroll").click(function(){
			swal({
                title: 'Are you sure?',
                text: "Employees will be marked as final and payslips will be auto generated",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Save!'
            }).then(function(result) {
                if (result.value) {
					   var rowvalue = [];
					$("#tblShow  > tr").each(function(i, v) {
						rowvalue[i] = $('td', this).map(function() {
						if($(this).attr("id")!="notincluded"){
							return $(this).attr("id")+"---"+$(this).text();
						}
						}).get()

					});
					var dataSet = JSON.stringify(rowvalue);
					var coverage_id = "<?php echo $pcover[0]->coverage_id; ?>";
					$.ajax({
						url: '<?php echo base_url() ?>payroll/savePayroll',
						type: "POST",
						data: {
						dataArray: dataSet,
						coverage_id: coverage_id,
						site: '<?php echo $site; ?>',
						},
						success: function (result) {
							if(result==1){
								swal("Success","Payslips are already generated and available.", "success");
							}else{
								swal("Error","There was an error saving payroll.", "error");
							}
						}
					});  
				}
            });
		
		

});	
  
  
  
  
  
  
  
  
  
  
$("#btnShowLogs").click(function(){
	 var emp = $("#employeeListRecord").val();
 	 $.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/payroll_calculate'); ?>",
		data: {
			emp : emp,
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
			coverage_text :  '<?php echo $pcover[0]->daterange; ?>',
			empstat :  '<?php echo $empstat; ?>',
			class_type : '<?php echo $type; ?>',
			site :  '<?php echo $site; ?>',

 		},
		cache: false,
		beforeSend:function(data){
			$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
		},
		success: function (json) {
 			$("#div_record_log_monitoring").html("");
			var result = JSON.parse(json);
			
			var txt="<table class='tblRs' style='margin-left: 2%;background: #fff;'>";
			txt+="<thead>";
					txt+="<tr>";
						txt+="<td rowspan=2 >Fullname</td>";
						txt+="<td rowspan=2>Monthly</td>";
						txt+="<td rowspan=2>Quinsina</td>";
						txt+="<td rowspan=2>Daily</td>";
						txt+="<td rowspan=2>Hourly</td>";
						txt+="<td rowspan=2>Total Hours</td>";
						txt+="<td colspan=3 style='color: #8BC34A;font-weight: 700;font-size: larger;'>Allowances</td>";
						txt+="<td rowspan=2>Bonuses</td>";
						txt+="<td rowspan=2>AHR</td>";
						txt+="<td rowspan=2>ND</td>";
						txt+="<td rowspan=2>HP</td>";
						txt+="<td rowspan=2>HO</td>";
						txt+="<td colspan=4 style='color: #2dc3d6;font-weight: 700;'>Government Deduction</td>";
						txt+="<td rowspan=2>Adjust<i class='la la-plus-circle' style='color: #4CAF50;'><i></td>";
						txt+="<td rowspan=2>Adjust<i class='la la-minus-circle' style='color: #ff958d;'><i></td>";
						txt+="<td rowspan=2>Earnings</td>";
						txt+="<td rowspan=2>Deduction</td>";
						txt+="<td rowspan=2>Takehome Pay</td>";

					txt+="</tr>";
					
					txt+="<tr>";
						txt+="<td>Rice</td>";
						txt+="<td>Clothing</td>";
						txt+="<td>Laundry</td>";
						txt+="<td>SSS</td>";
						txt+="<td>PHIC</td>";
						txt+="<td>HDMF</td>";
						txt+="<td>BIR</td>";
					txt+="</tr>";
			txt+="</thead><tbody id='tblShow'>";

			var rice  = 0;
			var laundry  = 0;
			var clothing  = 0;
			$.each(result.payroll, function (x, item){
			var adjustPlus  = 0;
			var adjustMinus  = 0;
			var bonus  = 0;
			var earnings  = 0;
			var deduction  = 0;
			var ahr_cost  = 0;
			var monthly = parseFloat(item.Rate_monthly).toFixed(2);
			var quinsina = parseFloat(item.Rate_quinsina).toFixed(2);
			var daily = parseFloat(item.Rate_daily).toFixed(2);
			var hourly = parseFloat(item.Rate_hourly).toFixed(2);
			var ho =  parseFloat(hourly*item.holidaywork).toFixed(2);
			var nd = (item.night_diff_cost).toFixed(2);
			var hp = (item.hazard_pay_cost).toFixed(2);
			
			var txtEarn= new Array();
			var txtBonus= new Array();
			var txtAdjAdd= new Array();
			var txtAdjDeduct= new Array();
			var txtDeduct= new Array();
			var txtAhr= new Array();
			var txtLeave= new Array();
			
				txtEarn.push("Basic (Quinsina)="+quinsina);	
				txtEarn.push("Night Differential (ND)="+nd);	
				txtEarn.push("Hazard Pay (HP)="+hp);	
				txtEarn.push("Holiday Pay (HO)="+ho);
				
				txtDeduct.push("SSS="+item.contri_sss);	
				txtDeduct.push("PHIC="+item.contri_phic);	
				txtDeduct.push("HDMF="+item.contri_hdmf);	
				txtDeduct.push("BIR="+item.contri_bir);	
				
				if(item.logs_issue>0){
					txtDeduct.push("Late/Undertime/Absent ["+parseFloat(item.logs_issue).toFixed(2)+" hour(s)]="+parseFloat(Number(item.logs_issue*Number(hourly))).toFixed(2));	
				} 
				$.each(item.allowances, function (a, item_a){
					
					if(item_a.allowance_name=="Rice"){
						rice = item_a.value/2;
						txtEarn.push(item_a.allowance_name+"="+item_a.value/2);
					}
					if(item_a.allowance_name=="Clothing"){
						clothing = item_a.value/2; 
						txtEarn.push(item_a.allowance_name+"="+item_a.value/2);

					}
					if(item_a.allowance_name=="Laundry"){
						laundry = item_a.value/2;
						txtEarn.push(item_a.allowance_name+"="+item_a.value/2);
					}
				});
				$.each(item.adjAdd, function (b, item_b){
					adjustPlus+=Number(item_b.value);
					
					txtEarn.push(item_b.description+"|"+item_b.additionname+"="+item_b.value);
					txtAdjAdd.push(item_b.description+"|"+item_b.additionname+"="+item_b.value);

				});
				$.each(item.adjMinus, function (c, item_c){
					adjustMinus += Number(item_c.value);
					txtDeduct.push(item_c.deductionname+"="+item_c.value);
					txtAdjDeduct.push(item_c.deductionname+"="+item_c.value);

				});
				$.each(item.bonus, function (d, item_d){
					bonus += Number(item_d.amount)/2;
					txtEarn.push(item_d.bonus_name+"="+Number(item_d.amount)/2);
					txtBonus.push(item_d.bonus_name+"="+Number(item_d.amount)/2);

				});
				for(var i=0;i<(item.ahr).length;i++){
					txtAhr.push(item.ahr[i]);
				}
				$.each(item.leave, function (e, item_e){
					txtLeave.push(item_e.requestLeaveDetails_ID);
				});
			 
			 
				var ahr_amount = (item.ahr_val*hourly).toFixed(2);
				var ahr_amount_hour = (item.ahr_val).toFixed(2);
				txtEarn.push("Overtime (AHR)="+Number(ahr_amount));
				var absentLate = Number(item.logs_issue*Number(hourly));
				var absentLate_hours = Number(item.logs_issue);
				var earnings =  Number(quinsina) + Number(nd) + Number(hp) + Number(ho) + (Number(rice) + Number(clothing) + Number(laundry)) + Number(bonus) + Number(adjustPlus)+Number(ahr_amount);
					var earnings = parseFloat(earnings).toFixed(2);
					
				var deduction = Number(item.contri_sss) + Number(item.contri_phic) + Number(item.contri_hdmf) + Number(item.contri_bir) + Number(adjustMinus) + absentLate;
					var deduction = parseFloat(deduction).toFixed(2);
  				var finalpay = earnings - deduction;
					var finalpay = (finalpay>0) ? parseFloat(finalpay).toFixed(2) : "0.00";

			
					
 						txt+="<tr id='"+item.emp_id+"' data-name='"+item.fullname+"' data-earn='"+txtEarn+"' data-deduct='"+txtDeduct+"' ><td id='notincluded'>"+item.fullname+"</td>";
 						txt+="<td title='"+item.fullname+"' id='monthly'>"+monthly+"</td>";
 						txt+="<td title='"+item.fullname+"' id='quinsina'>"+quinsina+"</td>";
 						txt+="<td title='"+item.fullname+"' id='daily'>"+daily+"</td>";
 						txt+="<td title='"+item.fullname+"' id='hourly'>"+hourly+"</td>";
 						txt+="<td title='"+item.fullname+"' id='total_hours'>"+item.total_shift_work+"</td>";
 						txt+="<td class='allowanceStyle' title='"+item.fullname+"' id='rice'>"+rice+"</td>";
 						txt+="<td class='allowanceStyle' title='"+item.fullname+"' id='clothing'>"+clothing+"</td>";
 						txt+="<td class='allowanceStyle' title='"+item.fullname+"' id='laundry'>"+laundry+"</td>";
 						txt+="<td title='"+item.fullname+"' id='bonus'>"+bonus+"</td>";
 						txt+="<td title='"+item.fullname+"' id='ahr_amount'>"+ahr_amount+"</td>";
 						txt+="<td title='"+item.fullname+"' id='nd'>"+nd+"</td>";
 						txt+="<td title='"+item.fullname+"' id='hp'>"+hp+"</td>";
 						txt+="<td title='"+item.fullname+"' id='ho'>"+ho+"</td>";
 						txt+="<td class='govDeductStyle' title='"+item.fullname+"' id='sss_details'>"+item.contri_sss+"</td>";
 						txt+="<td class='govDeductStyle' title='"+item.fullname+"' id='phic_details'>"+item.contri_phic+"</td>";
 						txt+="<td class='govDeductStyle' title='"+item.fullname+"' id='hdmf_details'>"+item.contri_hdmf+"</td>";
 						txt+="<td class='govDeductStyle' title='"+item.fullname+"' id='bir_details'>"+item.contri_bir+"</td>";
 						txt+="<td title='"+item.fullname+"' id='adjust_add'>"+parseFloat(adjustPlus).toFixed(2)+"</td>";
 						txt+="<td title='"+item.fullname+"' id='adjust_minus'>"+parseFloat(adjustMinus).toFixed(2)+"</td>";
 						txt+="<td class='earnStyle' onclick='getEarnings("+item.emp_id+",1)'  title='"+item.fullname+"' id='total_earning'>"+earnings+"</td>";
 						txt+="<td class='deductStyle' onclick='getEarnings("+item.emp_id+",0)'  title='"+item.fullname+"' id='total_deduction'>"+deduction+"</td>";
  						txt+="<td class='homePayStyle'  title='"+item.fullname+"' id='final'>"+finalpay+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='emp_type'>"+item.emp_type+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='emp_promoteid'>"+item.emp_promoteid+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='account'>"+item.account+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='adjust_details'>"+((txtAdjAdd.length>0) ? txtAdjAdd : 0) +"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='deduct_details'>"+((txtAdjDeduct.length>0) ? txtAdjDeduct : 0 )+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='absentLate'>"+absentLate.toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='bonus_details'>"+((txtBonus.length>0) ? txtBonus : 0 )+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='isDaily'>"+item.isDaily+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hour_hp'>"+item.hour_hp+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hour_ho'>"+item.hour_ho+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hour_nd'>"+item.hour_nd+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='ahr_id'>"+txtAhr+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='leave_id'>"+txtLeave+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='absentLate_hours'>"+absentLate_hours.toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='ahr_hour'>"+ahr_amount_hour+"</td>";
						txt+="</tr>";
								 
					
				// txt+="<tr></tr>"; 
 
			 });
			txt+="</tbody></table>";
			if(emp.length>0){
				$("#div_record_log_monitoring").html(txt);
				$("#btnSavePayroll").css("display","block");
			}else{
				$("#btnSavePayroll").css("display","none");
				$("#div_record_log_monitoring").html("<h1 style='margin: 0 auto;padding: 5%;'>No Records Found</h1>");
			}
		}
	});
});

$('select[name="duallistbox_live[]"]').bootstrapDualListbox();
$('select[name="duallistbox_record[]"]').bootstrapDualListbox();
	$('#toggleFilter').on('click', function () {
		$('#filters').toggle(1000);
	});
emp_list();
 });
 </script>