<link rel="stylesheet" href="<?php echo base_url("assets/css/cards-style.css"); ?>" type="text/css" /> </head>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<style>
.btn-group.buttons {
    background: #575962;
}
button.btn.moveall.btn-outline-secondary {
    margin: 0;
}
button.btn.removeall.btn-outline-secondary {
    margin: 0;
}
#tblDtrShow thead{
     background: #34bfa3;
    color: #fff;
    font-size: initial;
}
#tblDtrShow thead th {
    padding: 6px;
}
.bootstrap-duallistbox-container {
    width: 100%;
}
thead {
    background: #575962;
}
thead td {
	padding: 10px;
    color: #FFEB3B;
    font-weight: 300;
}
tbody td {
    padding: 9px;
    border: 1px solid #bdc0cc;
    color: #000;
}
tbody tr:hover {
    background: #fff7b7;
}
tr.absent_class td {
    background: #ffd8de;
    font-style: italic;
    border-color: #e29fab !important;
}
</style>
<style>
.h3, h3 {
	font-size: .98rem !important;
}
</style>
<?php
	$date = explode("-",$pcover[0]->daterange); 
	$data = date('F d', strtotime($date[0]))." - ".date('d , Y', strtotime($date[1]));
  ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator"><?php echo strtoupper($site); ?> Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("/payroll/coverage"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Coverage</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/emp/".$pcover[0]->coverage_id)."/".$site; ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > Base</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/general/".$type."/".$empstat."/".$pcover[0]->coverage_id."/".$site); ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > General</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<?php echo " > ".$data;?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			 <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist" style="border: 0;">
                                 <!--<li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i> Live DTR</a>
                                </li>-->
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i> Raw Attendance Logs</a>
                                </li>
								<!--<button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right" id="toggleFilter" style="right: 36px;position: absolute;">
                                    <i class="fa fa-filter"></i>
								</button>-->
                            </ul>
                        </div>
                    </div>
			
            <div class="m-portlet__body pt-0" style="background: #e4e3e3;">

                <div class="tab-content">
                  
                   
                    <div class="tab-pane active" id="m_tabs_6_3" role="tabpanel">
					<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;">
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
									<select multiple="multiple" class="form-control m-input"  size="5"  name="duallistbox_record[]" id="employeeListRecord" >
									
									</select>
 								</div>
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
								<label></label>
									<button type="button" class="btn m-btn--pill m-btn--air btn-info btn-sm" id="btnShowLogs">Show</button>
 								</div>
								
								
							</div>
								
						</div>
								<div style="overflow: scroll;text-align: center;border: 1px solid #bdc0cc;">
								<div class="div_record_log_monitoring">
								</div>
									<div class="row" id="div_record_log_monitoring" style="background: #fff;" >
										 <span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>
									</div>
								</div>
                    </div>
                </div>  

               
 
            </div>
        </div>
		</div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

 <script>
 
  function emp_list(){
		var employeeList = $("#employeeListRecord").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/loadEmployee'); ?>",
		data: {
			class_type : '<?php echo $type; ?>',
			empstat :  '<?php echo $empstat; ?>',
			site :  '<?php echo $site; ?>',
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
 		},
		cache: false,
		beforeSend:function(data){
			$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
		},
		success: function (json) {
			$("#div_record_log_monitoring").html("");
			var result = JSON.parse(json);
			var str_emp = "";

				$.each(result.employee, function (x, item){
					var selected = (item.payroll_id >0) ? "selected=selected": "" ;
					str_emp += '<option   value='+item.emp_id+' '+selected+'>'+item.lname+", "+item.fname+'</option>';
						 
				}); 
				 $("#employeeListRecord").html(str_emp);
				 $("#employeeListRecord").bootstrapDualListbox('refresh');
		}
	});
}
 
 $(function(){ 
		
	 
 		$('.m-select2').select2({
                placeholder: "Select your schedule.",
				width: '100%'
         });
$('#dateReq').daterangepicker({});

$("#btnShowLogs").click(function(){
	 var emp = $("#employeeListRecord").val();
	 console.log(emp);
	 $.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/attendance_checker'); ?>",
		data: {
			emp : emp,
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
			coverage_text :  '<?php echo $pcover[0]->daterange; ?>',
 		},
		cache: false,
		beforeSend:function(data){
			$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
		},
		success: function (json) {
			$("#div_record_log_monitoring").html("");
			var result = JSON.parse(json);
			
			var txt="<table style='margin-left: 2%;background: #fff;'>";
			

			$.each(result.emp, function (x, item){
				txt+="<thead>";
					txt+="<tr>";
					txt+="<td>Lastname</td>";
					txt+="<td>Firstname</td>";
					txt+="<td>Date</td>";
					txt+="<td>Schedule</td>";
					txt+="<td>Login</td>";
					txt+="<td>Logout</td>";
					txt+="<td>Leave</td>";
					txt+="<td>Late/ND/Absent</td>";
					txt+="<td>ND</td>";
					txt+="<td>HP</td>";
					txt+="<td>HO</td>";
					txt+="<td>Total</td>";
					txt+="</tr>";
				txt+="</thead>";
  					$.each(item, function (y, item2){
						if(y!="absent_system"){
 						txt+="<tr><td>"+item2.lname+"</td>";
						txt+="<td>"+item2.fname+"</td>";
						txt+="<td>"+item2.sched_date+"</td>";
						txt+="<td>"+item2.time_start+" "+item2.time_end+"</td>";
						txt+="<td>"+item2.ActualIn+"</td>";
						txt+="<td>"+item2.ActualOut+"</td>";
						txt+="<td>"+item2.leaveDeduct+"</td>";
						txt+="<td>"+(item2.totalUTAbsent).toFixed(1)+" mins.</td>";
						txt+="<td>"+item2.ND+"</td>";
						txt+="<td>"+item2.ND+"</td>";
						txt+="<td>"+(item2.holidaywork).toFixed(2)+"</td>";
						txt+="<td>"+item2.total_shift_work+"</td>";
						txt+="</tr>";
						}
					 });
 						   $.each(item.absent_system["dates"], function (b, date){
							txt+="<tr class='absent_class'><td>"+item.absent_system["lname"]+"</td>";
							txt+="<td>"+item.absent_system["fname"]+"</td>";
							txt+="<td>"+date+"</td>";
							txt+="<td>No Schedule</td>";
							txt+="<td><span style='color: #F44336;'>No Logs</span></td>";
							txt+="<td><span style='color: #F44336;'>No Logs</span></td>";
							txt+="<td><span style='color: #F44336;'><b>Absent</b></span></td>";
 							txt+="<td>0</td>";
							txt+="<td>0</td>";
							txt+="<td>0.00</td>";
							txt+="<td>0</td>";
							txt+="</tr>";

						   });
 				txt+="<tr></tr>"; 
			 });
			 
			txt+="</table>";
			 $("#div_record_log_monitoring").html(txt);
		}
	});
});

$('select[name="duallistbox_live[]"]').bootstrapDualListbox();
$('select[name="duallistbox_record[]"]').bootstrapDualListbox();
	$('#toggleFilter').on('click', function () {
		$('#filters').toggle(1000);
	});
emp_list();
 });
 </script>