<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}

</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			
			
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
					<div class="col-lg-6">
						<label class="col-form-label col-lg-3 col-sm-12">Year</label>
							<select class="form-control m-select2" id="year" name="param">
								<option>2017</option>
								<option selected>2018</option>
								<option>2019</option>
								<option>2020</option>
							</select>
					</div>
					<div class="col-lg-6">						
						<label class="col-form-label col-lg-3 col-sm-12">Month</label>
							<select class="form-control m-select2" id="month" name="param">
								<option>All</option>
								<option>January</option>
								<option>February</option>
								<option>March</option>
								<option>April</option>
								<option>May</option>
								<option>June</option>
								<option>July</option>
								<option>August</option>
								<option>September</option>
								<option>October</option>
								<option>November</option>
								<option>December</option>
							</select>
						
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
					<button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnShow">Show</button>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
						<div class="m_datatable_record"></div>
					</div>
				</div>
               
 
            </div>
        </div>
		</div>
	</div>
	<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-location-arrow"></i> Please choose which site</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
      <div class="modal-body">
		  <div class="modal-site-div">
				
 		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
 <script>
	function coverage(){
		var year = $("#year").val();
		var month = $("#month").val();
		
		 $('.m_datatable_record').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
						url: "<?php echo base_url(); ?>payroll/payrollcoverage/"+year+"/"+month,
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        }
                    }
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#searchRecord'),
            },
            columns: [{
                field: "Year",
                title: "Year",
                width: 100,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<td class = 'col-md-12'>" +  row.year + "</td>";
                    return html;
                }
            },{
                field: "Month",
                title: "Month",
                width: 150,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                      var html = "<td class = 'col-md-12'>" +  row.month + "</td>";
                    return html;
                }
            },{
                field: "Coverage",
                title: "Coverage",
                width: 200,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                      var html = "<td class = 'col-md-12'>" +  row.daterange + "</td>";
                    return html;
                }
            },{
                field: "Status",
                title: "Status",
                width: 100,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                      var html = "<td class = 'col-md-12' onclick='clickme()'>" +  row.status + "</td>";
                    return html;
                }
            },{
                field: "Action",
                title: "Action",
                width: 100,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                      var html = "<td class = 'col-md-12'><span class='btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air' onclick='showModal("+row.coverage_id+")'><i class='fa fa-search'></i></span></td>";
                    return html;
					// href='<?php echo base_url("/payroll/emp/"); ?>"+row.coverage_id+"'
                }
            },
			],
        });
 
	}
	function showModal(coverage_id){
 		html = '<div class="m-widget1__item">'+
					'<div class="row m-row--no-padding align-items-center">'+
						'<div class="col">'+
							'<a href="<?php echo base_url("/payroll/emp/"); ?>'+coverage_id+'/cdo"><div class="siteDiv one" style="width: 85%;">'+
							'<img src="<?php echo base_url("assets/cdo.jpg"); ?>" style="width: 100%;">'+
							'</div></a>'+
						'</div>'+
						'<div class="col m--align-right">'+
							'<a href="<?php echo base_url("/payroll/emp/"); ?>'+coverage_id+'/cebu"><div class="siteDiv one" style="width: 85%;">'+
							'<img src="<?php echo base_url("assets/cebu.jpg"); ?>" style="width: 100%;">'+
							'</div></a>'+
						'</div>'+
					'</div>'+
				'</div>';
		$(".modal-site-div").html(html);
		$("#myModal").modal();
		
	}
	function clickme(){
		alert("asasasa");
	}
	$(function(){
			$('.m-select2').select2({
					placeholder: "Select your schedule.",
					width: '100%'
			 });
		 coverage();
		$("#btnShow").click(function(){
			 $('.m_datatable_record').mDatatable('destroy');
			coverage();
		});
	});
 
 </script>