<style>
.h3, h3 {
	font-size: .98rem !important;
}
</style>
<?php
	$date = explode("-",$pcover[0]->daterange); 
	$data = date('F d', strtotime($date[0]))." - ".date('d , Y', strtotime($date[1]));
 ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator"><?php echo strtoupper($site); ?> Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("/payroll/coverage"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Coverage</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/emp/".$pcover[0]->coverage_id)."/".$site; ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > Base </span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<?php echo " > ".$data;?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
	 <div class="row">
				<div class="col-xl-2">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											Attendance Review
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/attendance/".$type."/".$empstat."/".$pcover[0]->coverage_id."/".$site); ?>">
							<div class="m-portlet__body">
								<div class="m-widget4__info" style="text-align:center">
									<img src="<?php echo base_url()."assets/img/attendance.png"?>" style="width: 100%;"> 
								</div>
							</div>
							</a>
					</div>
				</div>
				<div class="col-xl-2">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											Adjustment (+)
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/adjustment/add/".$type."/".$empstat."/".$pcover[0]->coverage_id."/".$site); ?>">
							<div class="m-portlet__body">
									<div class="m-widget4__info" style="text-align:center">
										<img src="<?php echo base_url()."assets/img/adj-add.png"?>" style="width: 100%;">
									</div>
							</div>
							</a>
					</div>
				</div>
				<div class="col-xl-2">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											Adjustment (-)
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/adjustment/deduct/".$type."/".$empstat."/".$pcover[0]->coverage_id."/".$site); ?>">
							<div class="m-portlet__body">
										<div class="m-widget4__info" style="text-align:center">
											<img src="<?php echo base_url()."assets/img/adj-minus.png"?>" style="width: 100%;">
										</div>
							</div>
							</a>
					</div>
				</div>
				<div class="col-xl-2">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											Payroll
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/calculator/".$type."/".$empstat."/".$pcover[0]->coverage_id."/".$site); ?>">
							<div class="m-portlet__body" style="padding: 0;">
										<div class="m-widget4__info" style="text-align:center">
											<img src="<?php echo base_url()."assets/img/payroll.png"?>" style="width: 100%;">
										</div>
							</div>
							</a>
					</div>
				</div>
				<div class="col-xl-2">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											Payslip
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/general/".$type."/".$empstat."/".$pcover[0]->coverage_id."/".$site); ?>">
							<div class="m-portlet__body">
										<div class="m-widget4__info" style="text-align:center">
											<img src="<?php echo base_url()."assets/img/payslip.jpg"?>" style="width: 100%;">
										</div>
							</div>
							</a>
					</div>
				</div>
         </div>
		 
	</div>
	</div>
 <script>
 
	$(function(){
	
	});
 
 </script>