<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/markdown/markdown.js"></script>

<script type="text/javascript">
    $(function () {
        // $("textarea#wmd-input").pagedownBootstrap();
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>
<script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>
</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="<?php echo $setting['settings']; ?>">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
         <button class="btn btn-xs btn-success"  data-toggle="modal" data-target="#myModalAddMenu" id="AddMenu"> Add </button>
				<div id="myModalAddMenu" class="modal fade" role="dialog">
					<div class="modal-dialog" style="width: 400px;">

					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header" style="background: #00b19b; color: white;">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Post Announcement: </h4>
					  </div>
					  <div class="modal-body">

							<div id="AddUserOption">  
								 
								<div id="DivAddSingleUser"  >  
									<div class="example-box-wrapper">	 
									  <form class="form-horizontal bordered-row">

										<div class="form-group">
											<label class="col-sm-3 control-label">Subject</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" id="addAnnSubject">
											</div>
										</div> 
							 
										<div class="form-group">
											<label class="col-sm-3 control-label">Message: </label>
											<div class="col-sm-6">
													<textarea id="addAnnMessage" name="example-textarea-input" rows="9" class="form-control" placeholder="Content.."></textarea>
											</div>
										</div> 
									 
										</form>

										<button class="btn btn-xs btn-warning" id='BtnBackAddMenu' class="close" data-dismiss="modal"> Cancel</button>
										<button class="btn btn-xs btn-success" id='BtnSaveAddMessage'> Post </button>
									</div> 
											
									
									
									
								</div>
							</div>

					  </div>
					 
					</div>

				  </div>
				</div>           

 

<div id="page-title">
 <div class="example-box-wrapper">
<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
<thead>
<tr>
    <th>Subject</th>
    <th>Announcement</th>
    <th>Date Posted</th>
    <th> Update </th>
</tr>
</thead>

<tfoot>
<tr>
    <th>Subject</th>
    <th>Announcement</th>
	<th>Date Posted</th>
    <th > Update </th>
 
</tr>
</tfoot>

<tbody>
<?php foreach($userz as $k => $val){ ?>
<tr>
    <td><?php echo $val->subject; ?></td>
    <td><?php echo substr($val->details,0,50)."..."; ?></td>
	    <td><?php echo $val->datee; ?></td>

    <td> 
		<button class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModalUpdate<?php echo $val->anc_id; ?>">Update</button>
		
 	</td>
 

   
</tr>
 <div id="myModalUpdate<?php echo $val->anc_id; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #00b19b; color: white;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body"  >
		<div id="DivAddSingleUser"  >  
			<div class="example-box-wrapper">	 

					<div class="form-group">
						<label class="col-sm-3 control-label">Description</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="addSubject<?php echo $val->anc_id; ?>" value="<?php echo $val->subject; ?>">
						</div>
					</div> 
			</div> 
			<div class="example-box-wrapper">	 

					<div class="form-group">
						<label class="col-sm-3 control-label">Description</label>
						<div class="col-sm-12">
	<textarea   id="addmessage<?php echo $val->anc_id; ?>" name="example-textarea-input"  class="form-control" style="margin: 1px 0px;min-height: 400px;"   > <?php echo $val->	details; ?></textarea>
						</div>
					</div> 
			</div> 
			<div class="example-box-wrapper">	 
				<div class="form-group">
					<div class="col-sm-6">
						<button class="btn btn-xs btn-info"  id="myModalUpdates<?php echo $val->anc_id; ?>">Update</button>	
					</div>
				</div>
			</div> 		  
		</div>
      </div>
     
    </div>

  </div>
</div>
<script>
  $(function(){

 $('#myModalUpdates<?php echo $val->anc_id; ?>').click(function(){ 
var subject = $("#addSubject<?php echo $val->anc_id; ?>").val();
 var message = $("#addmessage<?php echo $val->anc_id; ?>").val(); 
 var anounceID = <?php echo $val->anc_id; ?>;
 dataString = "subject="+subject+"&message="+message+"&anounceID="+anounceID;
 $.ajax({
type: "POST",
url: "announcement/UpdateAnnounce",
data: dataString,
cache: false,
success: function(html)
{
  alert("Updated");

location.reload();
  }
  });	


});
});
</script>
<?php } ?>
 
</tbody>
</table>
</div>
 
    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
	$(function(){
		$.ajax({
			type: "POST",
			url:  "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
			cache: false,
			success: function(html)
			{

				$('#headerLeft').html(html);
		   //alert(html);
		}
	});	
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
			cache: false,
			success: function(html)
			{

				$('#sidebar-menu').html(html);
		 // alert(html);
		}
	});
	});	
</script>
<script>
  $(function(){
   
$('#BtnSaveAddMessage').click(function(){ 
var subject = $("#addAnnSubject").val();
var message = $("#addAnnMessage").val();
 
dataString = "subject="+subject+"&message="+message;

$.ajax({
type: "POST",
url: "announcement/addAnnounce",
data: dataString,
cache: false,
success: function(html)
{
  alert("Posted");
  location.reload();
  }
  });	

});
});
 
  </script>

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>