<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>SupportZebra Payslip</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<style>
table.paleBlueRows {
  font-family: "Times New Roman", Times, serif;
  border: 1px solid #FFFFFF;
  text-align: left;
  border-collapse: collapse;
}
table.paleBlueRows td, table.paleBlueRows th {
  border: 1px solid #0B6FA4;
  padding: 3px 2px;
}
table.paleBlueRows tbody td {
  font-size: 15px;
}
table.paleBlueRows tr:nth-child(even) {
  background: #D0E4F5;
}
table.paleBlueRows thead {
  background: #0B6FA4;
  border-bottom: 5px solid #0B6FA4;
}
table.paleBlueRows thead th {
  font-size: 21px;
  font-weight: bold;
  color: #FFFFFF;
  text-align: center;
  border-left: 2px solid #0B6FA4;
}
table.paleBlueRows thead th:first-child {
  border-left: none;
}

table.paleBlueRows tfoot td {
  font-size: 14px;
}
.myButton {
	-moz-box-shadow: 0px 10px 14px -7px #276873;
	-webkit-box-shadow: 0px 10px 14px -7px #276873;
	box-shadow: 0px 10px 14px -7px #276873;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
	background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
	background-color:#599bb3;
	-moz-border-radius:8px;
	-webkit-border-radius:8px;
	border-radius:8px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
 	font-weight:bold;
    padding: 2px 8px;
	text-decoration:none;
	text-shadow:0px 1px 0px #3d768a;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}
.myButton:active {
	position:relative;
	top:1px;
}

        
</style>
</head>
<body>
<div> 
	<div style="font-size:26px;font-weight:700;letter-spacing:-0.02em;line-height:32px;color:#41637e;font-family:sans-serif;text-align:center" align="center" id="m_-7518272001418123536emb-email-header">
   <img style="Margin-left:auto;Margin-right:auto" src="https://lh3.googleusercontent.com/NZNZzuQogtKpP32IPLfU7ycNwp-VO_a3pqnAB4Owr0sRkJdDEHENGxRulIG2eyEtLcWQdVR5NZOsStRx4zhQePBcFviP2V_F=w1366-h768-rw-no" class="CToWUd"></div>
   
   <br>
   <br>
<div>

	<?php
	$name = explode(",",$employee_text);
	?>
	Hi, <br><br>
	
	<?php echo ucwords($name[1])." ".ucwords($name[0]); ?> got a Kudos from <?php echo ucwords($clientname); ?>. <br><br>Please see below the details: <br><br>
</div>
<table class="paleBlueRows">
 
<thead>
<tr>
<th>Fields</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<tr><td>Account:</td><td><?php	echo $accounts_text; ?></td></tr>
<tr><td>Name:</td><td><?php	echo ucwords($name[1])." ".ucwords($name[0]); ?></td></tr>
<tr><td>Kudos by:</td><td><?php	echo $kudostype; ?></td></tr>
<tr><td>Preferred reward:</td><td><?php	echo $reward; ?></td></tr>
<tr><td>Client:</td><td><?php	echo $clientname; ?></td></tr>
<tr><td>Comment:</td><td><?php	echo $comment; ?></td></tr>
<tr><td>Date:</td><td><?php	echo $date; ?></td></tr>
 
</tbody>
</tr>
</table>
	<br>
	<br>
	
	Please request. Please use this  <a href="http://10.200.101.250/supportzebra/index.php/Kudos/kudos_request" class="myButton">link </a>.
	<br>
	<br>
	
	Thank you.
	
 </div>
</body>
</html> 