<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
	/* Loading Spinner */
	.spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
	#tblShowTR td{
		color: green;
	}
	table tr td{
		padding:2px;
	}
	#wrapper {
     width: 100%;
    border: 1px solid black;
    overflow: auto;
	}
	#first { 
		float: left;
		width: 534px;
		border: 1px solid black;
		padding:20px;
		background-image: url(http://10.200.101.250/supportzebra/assets/images/watermark2.png);
		background-size: cover;
		}
	#second {
	border: 1px solid black;
    margin: 0px 0 0 546px;
    padding: 12px;
    width: 465px;
	background-image: url(http://10.200.101.250/supportzebra/assets/images/watermark2.png);
	background-size: contain;
	}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  
    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>






    <script type="text/javascript">
        $(window).load(function(){
             setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'mm-dd-yyyy'
        });
    });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
		<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

 		<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>
 		<script src="<?php echo base_url();?>assets/multi-select/jquery.PrintArea.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.css">

</head>


    <body>
    <div id="sb-site">
   
 
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
       <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id='headerLeft'>
    </div>


 
</div>
        <div id="page-sidebar">
			<div class="scroll-sidebar">
        

		<ul id="sidebar-menu">
		</ul>
		</div>
		</div>
        <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

 

<div id="page-title">

	<div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
    
                <div class="example-box-wrapper">
                    <form class="form-horizontal bordered-row" role="form">
		
                         <div class="form-group" id="employzResult">
                    <div class="col-sm-12">
                 
          <form id="demoform" action="#" method="post">
		  		 
					 <div class="form-group">
                    <label class="col-sm-3 control-label">Payroll Coverage:</label>
                    <div class="col-sm-6">
                         <select name="" class="chosen-select" style="display: none;" id="payrollC">
								 
                                <option disabled="disabled" selected >--</option>
							<?php foreach($period as $row => $val){?>
							<optgroup label="<?php echo $val["month"]; ?>">
									<?php 
									$exp = explode(",",$val["daterange"]);
										for($i=0;$i<count($exp);$i++){									
									?>
										<option selected><?php echo $exp[$i]; ?></option>
									<?php } ?>
							</optgroup>
							<?php } ?>
                        </select>
	 
                    </div>

                </div>
          
		
            </form>

                    </div>
                </div>
		<input type="button" class="btn btn-xs btn-primary" id="btnShow" value="Show">
 		<!--<input type='button' class="btn btn-xs btn-info" id="print_button1" value='Print'>-->
                    </form>
						<div id="divNote"></div>
                </div>
            </div>
        </div>
		<a href="#" data-toggle="modal" data-target="#modalPDF"><input type='button' class="btn btn-xs btn-info" id="email_button2" value='PDF Send' style="display:none;"></a>

      
    </div>
<div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                
                <div class="example-box-wrapper">
						<!--<div id="loadingIMG" style="position: fixed; margin: -20% auto 0px; width: 100%; z-index: 1000; display: none;margin-left:20%;">
							 <img src="<?php echo base_url();?>assets/images/loading-gears-animation-3.gif"  > 
						</div>-->
                    <form class="form-horizontal bordered-row" role="form" id="tblShow2" >

                         
 						 <div id="tblShow">
                         </div>
                        
                    </form>
                </div>
            </div>
        </div>
   
       
    </div>
	

    <div id='Templatedesign'>
    </div>
</div>

 <div class="row" hidden>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Recent sales activity
                </h3>
                <div class="example-box-wrapper">
                    <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>

 
   

        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                Live server status
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator">
                        <i class="glyph-icon icon-question"></i>
                    </a>
                    <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                        <i class="glyph-icon icon-refresh"></i>
                    </a>
                    <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                        <i class="glyph-icon icon-times"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

    </div>
  
</div>
                    </div>

                

            </div>
        </div>
    </div>


     <!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.js"></script>

</div>
</body>
<script>
  $(function(){
 		$('#sidebar-menu').load('<?php echo base_url(); ?>/index.php/Templatedesign/sideBar')
		 $('#headerLeft').load('<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft')

  });	
</script> 
<script>
  $(function(){
	  	    // $("#loadingIMG").hide();

	  $("#btnShow").click(function(){
		  
		  var payrollC = $("#payrollC").val();
		  var employz = $("#employz").val();
		   var dataString = "payrollC="+payrollC+"&employz="+employz;
		   var ps = payrollC.split("-");
		   var ps1 = ps[0].split("/");
		   var ps2 = ps[1].split("/");
		   var totalEarnings=0;
		   var totalDeductions=0;
 		    $.ajax({
                    url: '<?php echo base_url() ?>index.php/payslip/emptype2',
                    type: 'POST',
                    data:  dataString,
					 // beforeSend: function(){
						// $('#loadingIMG').show();
						// $(".container").css("opacity",".5");
					// },
					// complete: function(){
						 // $('#loadingIMG').fadeOut("slow");
							// $(".container").css("opacity","1");

					// },
					success:function(res)
                    {
						var obj = jQuery.parseJSON(res);
						 var td = '';
  						$.each(obj,function(index, value){
						var bir = value["bir_details"].split("(");
/* 						var clothing = value["clothing"].split("(");
						var laundry = value["laundry"].split("(");
						var rice = value["rice"].split("(");
						
 */						var clothing = ((value["clothing"].trim()=="--") ? [0.00,0.00] : value["clothing"].split("("));
						var laundry = ((value["laundry"].trim()=="--") ? [0.00,0.00] : value["laundry"].split("("));
						var rice = ((value["rice"].trim()=="--") ? [0.00,0.00] : value["rice"].split("("));

  td+='<div id="wrapper"><div id="first"><table class="tableizer-table" >';
  td+='<thead><tr class="tableizer-firstrow"><th colspan="4" style="text-align: center;"><img src="<?php echo base_url(); ?>assets/images/logo2.png" style="width: 270px;"></th></tr></thead> <tbody>';
  td+='<tr><td colspan="4">PAY STATEMENT - EMPLOYEES COPY</td></tr>';
  td+='<tr><td>NAME:      </td><td>'+value["fullname"]+'</td><td>POSITION</td><td>'+value["pos_name"]+'</td></tr>';
  td+='<tr><td>Date:        </td><td>'+value["transact_date"]+'</td><td>TAX STATUS</td><td>'+bir[1].slice(0, -1)+'</td></tr>';
  td+='<tr><td>Period Covered:       </td><td>'+value["daterange"]+'</td><td> ATM </td><td>&nbsp;</td></tr>';
  td+='<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
  td+='<tr><td>EARNINGS:</td><td>SALARY</td><td colspan="2"> '+value["quinsina"]+' </td></tr>';
  td+='<tr><td>&nbsp;</td><td>CLOTHING ALLOWANCE</td><td colspan="2">'+clothing[0]+'</td></tr>';
  td+='<tr><td>&nbsp;</td><td>LAUNDRY ALLOWANCE</td><td colspan="2"> '+laundry[0]+' </td></tr>';
  td+='<tr><td>&nbsp;</td><td>RICE SUBSIDY</td><td colspan="2"> '+rice[0]+' </td></tr>';
  td+='<tr><td>&nbsp;</td><td>NIGHT DIFF</td><td  colspan="2"> '+value["nd"]+' </td></tr>';
  td+='<tr><td>&nbsp;</td><td>OVERTIME</td><td colspan="2"> '+value["ot_bct"]+' </td></tr>';
  td+='<tr><td>&nbsp;</td><td>HAZARD PAY</td><td colspan="2"> '+value["hp"]+' </td></tr>';
  td+='<tr><td>&nbsp;</td><td>HOLIDAY</td><td colspan="2">'+value["ho"]+'</td></tr>';
   if(value["adjust_details"]!=0){
	 var adjusted = value["adjust_details"].split(",");
	$.each(adjusted,function(yek,lav){
		ii = lav.split("=");	
		x = ii[0].split("|");	
		if(x[0].trim()!="Bonus"){
			if(ii[1].trim()!="0.00" && ii[1].trim()!="0"){

			td+='<tr><td>&nbsp;</td><td>'+x[1]+'</td><td  colspan="2">'+ii[1]+'</td></tr>';
			totalEarnings += parseFloat(ii[1]);
		}
		}
	});
	} 
	  totalEarnings+= parseFloat(value["quinsina"])+parseFloat(clothing[0])+parseFloat(laundry[0])+parseFloat(rice[0])+parseFloat(value["nd"])+parseFloat(value["ot_bct"])+parseFloat(value["hp"])+parseFloat(value["ho"]);

  td+='<tr><td>&nbsp;</td><td>Total Earnings</td><td colspan="2">'+parseFloat(totalEarnings).toFixed(2)+'</td></tr>';
  
  td+='<tr><td>DEDUCTIONS: </td><td>WITHHOLDING TAX</td><td colspan="2">'+bir[0]+'</td></tr>';
  td+='<tr><td>&nbsp;</td><td>SSS PREMIUM</td><td colspan="2"> '+value["sss_details"]+'</td></tr>';
   td+='<tr><td>&nbsp;</td><td>HDMF PREMIUM</td><td  colspan="2"> '+value["hdmf_details"]+' </td></tr>';
   td+='<tr><td>&nbsp;</td><td>PHILHEALTH</td><td  colspan="2"> '+value["phic_details"]+' </td></tr>';
  td+='<tr><td>&nbsp;</td><td>LEAVE/UNDERTIME/OB/TARDINESS</td><td  colspan="2"> '+value["absentLate"].trim()+' </td></tr>';
 
	if(value["deduct_details"]!=0){
	 var deducted = value["deduct_details"].split(",");
	$.each(deducted,function(yek,lav){
		ii = lav.split("=");	
		td+='<tr><td>&nbsp;</td><td>'+ii[0]+'</td><td  colspan="2"> '+ii[1].trim()+'</td></tr>';
		totalDeductions += parseFloat(ii[1]);
	});
	} 
  totalDeductions += parseFloat(bir[0]) + parseFloat(value["sss_details"]) + parseFloat(value["hdmf_details"]) + parseFloat(value["phic_details"]) + parseFloat(value["absentLate"]);
  td+='<tr><td>&nbsp;</td><td>Total Deductions</td><td  colspan="2"> '+parseFloat(totalDeductions).toFixed(2)+'</td></tr>';
  td+='<tr><td colspan=2>TAKE HOME PAY</td> <td  colspan="2"> '+parseFloat(totalEarnings-totalDeductions).toFixed(2)+' </td></tr>';
  td+='<tr><td>&nbsp;</td><td>&nbsp;</td><td  colspan="2">&nbsp;</td></tr>';
  td+='<tr><td>Signature:</td><td>&nbsp;</td><td>Date:</td><td>&nbsp;</td></tr>';
  td+='<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
  td+='<tr><td colspan="4">Note: Keep this statement permanently for Income Tax purposes.</td></tr>';
  td+='<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>';
  td+='</tbody>';
 td+='</table>  </div> ';
  td+='<div id="second"><table class="tableizer-table"  >';
  td+='<thead><tr class="tableizer-firstrow"><th colspan="4" style="text-align: center;"><img src="<?php echo base_url(); ?>assets/images/logo2.png" style="width: 270px;"></th></tr></thead> <tbody>';
  td+='<tr><td colspan="4">BONUS STATEMENT - EMPLOYEES COPY</td></tr>';
  td+='<tr><td>NAME:      </td><td>'+value["fullname"]+'</td><td>POSITION</td><td>'+value["pos_name"]+'</td></tr>';
  td+='<tr><td>Date:        </td><td>'+value["transact_date"]+'</td><td>TAX STATUS</td><td>'+bir[1].slice(0, -1)+'</td></tr>';
  td+='<tr><td>Period Covered:       </td><td>'+value["daterange"]+'</td><td> ATM </td><td>&nbsp;</td></tr>';
  td+='<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td> ';
     if(value["adjust_details"]!=0){
	 var adjusted = value["adjust_details"].split(",");
	$.each(adjusted,function(yek,lav){
		ii = lav.split("=");	
		x = ii[0].split("|");	
		console.log(value["fullname"]+" "+x[0]);
			if(x[0].trim()=="Bonus"){
			td+='<tr><td>&nbsp;</td><td>'+x[1]+'</td><td colspan="2"> P'+ii[1]+'</td></tr>';
			}
	});
	}
	if(value["bonus_details"]!=0){
	 var deducted = value["bonus_details"].split(",");
	$.each(deducted,function(yek,lav){
		ii = lav.split("=");	
		td+='<tr><td>&nbsp;</td><td>'+ii[0]+'</td><td  colspan="2"> P'+parseFloat(ii[1]).toFixed(2).trim()+'</td></tr>';
	});
	
	} else{
		td+='<tr><td>&nbsp;</td><td> No Bonus </td><td  colspan="2"> Available</td></tr>';

	
	}
  
   td+='<tr><td>&nbsp;</td><td>&nbsp;</td><td  colspan="2">&nbsp;</td></tr>';
  td+='<tr><td>Signature:</td><td>&nbsp;</td><td>Date:</td><td>&nbsp;</td></tr>';
  td+='<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
  td+='<tr><td colspan="4">Note: Keep this statement permanently for Income Tax purposes.</td></tr>';
  td+='<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>';
  td+='</tbody>';
 td+='</table></div></div>   ';
		    totalEarnings=0;
		   totalDeductions=0;

						});
	
swal({
  title: 'Password Required',
  html: 'Please enter your password to view your payslip.',
  input: 'password',
  showCancelButton: true,
  confirmButtonText: 'Submit',
  showLoaderOnConfirm: true,
  allowOutsideClick: false
}).then(function(email) {
   
		if(email=="<?php echo trim($user_id["user_id"]); ?>"){
					
					 $("#divNote").html("<h4><b style='color:red'>Note: </b>Please approach HRT to request a printed copy of your payslip.</h4>");
					 $("#tblShow").html(td);

		}else{
		 $("#tblShow").html("");
			swal({
			type: 'error',
			title: 'Oh, Snap!'
			}); 
		}
   
 
})
					 
						
						
                    }
                }); 
	  });
 
	});	
	$(function() {
        $(this).bind("contextmenu", function(e) {
            e.preventDefault();
        });
    }); 
</script>

 
<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
   
  <script>
            var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
            $("#demoform").submit(function() {
              alert($('[name="duallistbox_demo1[]"]').val());
              return false;
            });
          </script>
		   <script>
    $(document).ready(function(){
        $("#print_button1").click(function(){
            var mode = 'iframe'; // popup
            var close = mode == "popup";
            var options = { mode : mode, popClose : close};
            $("#tblShow").printArea( options );
        });
	 
	 
         
    });

  </script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>