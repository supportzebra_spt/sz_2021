<!DOCTYPE html> 

<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  body { padding-right: 0 !important }
  .editable-buttons{
    z-index: 100
  }
  .charref{
   text-indent: 50px;
   color:#777;
 }
</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>





<script type="text/javascript">
  $(window).load(function(){
   setTimeout(function() {
    $('#loading').fadeOut( 400, "linear" );
  }, 300);
 });
</script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'mm-dd-yyyy'
    });
  });

</script>

<!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

<!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/xeditable/xeditable.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable-demo.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/popover/popover.js"></script>

<!-- jQueryUI Tabs -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/tabs-ui/tabs.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs-ui/tabs.js"></script>
<script type="text/javascript">
  /* jQuery UI Tabs */

  $(function() { "use strict";
    $(".tabs").tabs();
  });

  $(function() { "use strict";
    $(".tabs-hover").tabs({
      event: "mouseover"
    });
  });
</script>
</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>



    </div>
    <div id="page-sidebar">
     <div class="scroll-sidebar">


      <ul id="sidebar-menu">
      </ul>
    </div>
  </div>
  <div id="page-content-wrapper">
    <div id="page-content">
      <h2>APPLICANT UPDATE</h2>
      <hr style='margin-bottom:15px'>
      <div class="container-fluid">
        <a class="btn btn-xs btn-warning" href="<?php echo base_url();?>index.php/Applicant" style='margin-bottom:5px'>
          <span class="glyph-icon icon-separator">
            <i class="glyph-icon icon-arrow-left"></i>
          </span>
          <span class="button-content">
            Back
          </span>
        </a>
        <div class="row">
          <div class="col-md-8">
            <div class="panel">
              <div class="panel-body" style='padding-bottom:0px'>
               <div class="tabs">
                <div class="row">
                  <div class="col-md-12">
                    <ul>
                      <li>
                        <a href="#tab1" title="Tab 1">
                         PERSONAL
                       </a>
                     </li>
                     <li>
                      <a href="#tab2" title="Tab 2">
                        APPLICATION
                      </a>
                    </li>
                    <li>
                      <a href="#tab3" title="Tab 2" id='tab3btn'>
                        EVALUATION
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="tab1" style='padding:10px 0px 0px 0px'>
                <div class="content-box">
                  <div class="content-box-wrapper">

                    <h4 class='font-bold'>PERSONAL DETAILS</h4><hr style='margin-bottom:0'>
                  </div>
                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">First Name:</label>
                      <div class="col-sm-8">
                        <a href="#" id="fname"><?php echo $details->fname; ?></a>
                      </div>
                    </div>
                  </div> 
                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Middle Name:</label>
                      <div class="col-sm-8">
                        <a href="#" id="mname"><?php echo $details->mname; ?></a>
                      </div>
                    </div>
                  </div>
                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Last Name:</label>
                      <div class="col-sm-8">
                        <a href="#" id="lname"><?php echo $details->lname; ?></a>
                      </div>
                    </div>
                  </div>

                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Birthday:</label>
                      <div class="col-sm-8">
                        <a href="#" id="birthday"><?php echo $details->birthday; ?></a>
                      </div>
                    </div>
                  </div>

                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Gender:</label>
                      <div class="col-sm-8">
                        <a href="#" id="gender"><?php echo $details->gender; ?></a>
                      </div>
                    </div>
                  </div>

                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Civil Status:</label>
                      <div class="col-sm-8">
                        <a href="#" id="civilStatus"><?php echo $details->civilStatus; ?></a>
                      </div>
                    </div>
                  </div>
                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Religion:</label>
                      <div class="col-sm-8">
                        <a href="#" id="religion"><?php echo $details->relijon; ?></a>
                      </div>
                    </div>
                  </div>
                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Cellphone Number:</label>
                      <div class="col-sm-8">
                        <a href="#" id="cell"><?php echo $details->cell; ?></a>
                      </div>
                    </div>
                  </div>
                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Alternative Number:</label>
                      <div class="col-sm-8">
                        <a href="#" id="tel"><?php echo $details->tel; ?></a>
                      </div>
                    </div>
                  </div>
                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Present Address:</label>
                      <div class="col-sm-8">
                        <a href="#" id="present">Edit</a>
                        <p><?php $date = explode("|",$details->presentAddrezs);echo $date[0]; ?></p>
                      </div>
                    </div>
                  </div>
                  <div class="content-box-wrapper">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Permanent Address:</label>
                      <div class="col-sm-8">
                        <a href="#" id="permanent">Edit</a>
                        <p><?php $date = explode("|",$details->permanentAddrezs);echo $date[0]; ?></p>
                      </div>
                    </div>
                  </div>
                  <br><br><br>
                </div>
              </div>
              <div id="tab2" style='padding:10px 0px 0px 0px'>
                <div class="content-box">
                 <div class="content-box-wrapper">

                  <h4 class='font-bold'>APPLICATION DETAILS</h4><hr style='margin-bottom:0'>
                </div>
                <div class="content-box-wrapper">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Position Applied:</label>
                    <div class="col-sm-8">
                      <a href="#" id="position"><?php echo $details->pos_details; ?></a>
                    </div>
                  </div>
                </div> 

                <div class="content-box-wrapper">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Source:</label>
                    <div class="col-sm-8">
                      <a href="#" id="source"><?php echo $details->Source; ?></a>
                    </div>
                  </div>
                </div> 

                <div class="content-box-wrapper">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Call Center Experience:</label>
                    <div class="col-sm-8">
                      <a href="#" id="ccaExp"><?php echo $details->ccaExp; ?></a>
                    </div>
                  </div>
                </div>

                <div class="content-box-wrapper">
                  <label class="col-sm-12 control-label">Character Reference 1:</label>
                </div>
                <?php $cr1 = explode('|',$details->cr1); $cr1name = explode('$',$cr1[0]);?>
                <div class="content-box-wrapper">
                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">First Name:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr1fname"><?php echo $cr1name[0]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">
                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Middle Name:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr1mname"><?php echo $cr1name[1]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">

                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Last Name:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr1lname"><?php echo $cr1name[2]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">

                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Mobile Number:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr1cell"><?php echo $cr1[1]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">

                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Occupation:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr1job"><?php echo $cr1[2]; ?></a>
                    </div>
                  </div>
                </div>
                <!--Char Ref 2-->                 
                <div class="content-box-wrapper">
                  <label class="col-sm-12 control-label">Character Reference 2:</label>
                </div>
                <?php $cr2 = explode('|',$details->cr2); $cr2name = explode('$',$cr2[0]);?>

                <div class="content-box-wrapper">
                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">First Name:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr2fname"><?php echo $cr2name[0]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">
                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Middle Name:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr2mname"><?php echo $cr2name[1]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">

                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Last Name:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr2lname"><?php echo $cr2name[2]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">

                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Mobile Number:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr2cell"><?php echo $cr2[1]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">

                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Occupation:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr2job"><?php echo $cr2[2]; ?></a>
                    </div>
                  </div>
                </div>
                <!--Char Ref 3-->
                <div class="content-box-wrapper">
                  <label class="col-sm-12 control-label">Character Reference 3:</label>
                </div>
                <?php $cr3 = explode('|',$details->cr3); $cr3name = explode('$',$cr3[0]);?>
                <div class="content-box-wrapper">
                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">First Name:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr3fname"><?php echo $cr3name[0]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">
                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Middle Name:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr3mname"><?php echo $cr3name[1]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">

                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Last Name:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr3lname"><?php echo $cr3name[2]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">

                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Mobile Number:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr3cell"><?php echo $cr3[1]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="content-box-wrapper">

                  <div class="form-group">
                    <label class="col-sm-4 control-label charref">Occupation:</label>
                    <div class="col-sm-8">
                      <a href="#" id="cr3job"><?php echo $cr3[2]; ?></a>
                    </div>
                  </div>
                </div>
                <br>
              </div>
            </div>
            <div id="tab3" style='padding:10px 0px 0px 0px'>
             <div class="panel">
              <div class="panel-body">
                <h4 class='font-bold'>EVALUATION RESULT</h4><hr>
                <div class='text-left'>
                  <button class="btn btn-success" id='pass'>
                    <span class="glyph-icon icon-separator">
                      <i class="glyph-icon icon-thumbs-up"></i>
                    </span>
                    <span class="button-content">
                      Pass
                    </span>
                  </button>
                  <button class="btn btn-danger" id='fail'>
                    <span class="glyph-icon icon-separator">
                      <i class="glyph-icon icon-thumbs-down"></i>
                    </span>
                    <span class="button-content">
                      Fail
                    </span>
                  </button>
                </div>
                <br><br>
                <div class="alert alert-warning text-center" id='inf_pool' style='display:none'>
                 <h5><b style='color:red'>NOTE:</b> Please update applied position to continue. Currently <b>POOL</b>.</h5>
               </div>
               <div class="alert alert-warning text-center" id='inf_year' style='display:none;'>
                <h5><b style='color:red'>NOTE:</b> Please indicate years of Call Center Experience.<br> This is required to all <b>Customer Care Ambassador</b> applicants.</h5>
              </div>
              <div id='passdiv' style='display:none;'>
                <div class="content-box" style='padding:0;margin:0'>
                  <h3 class="content-box-header bg-gray">
                    <i class="glyph-icon icon-cog"></i>
                    Employment Details Form
                  </h3>
                  <div class="content-box-wrapper" style='padding-bottom:0'>
                    <div class="form-group">
                      <label class="control-label" >Employment Position:</label><br>
                      <a href="#" id="positionApplied"></a>
                    </div>
                  </div> 

                  <div class="content-box-wrapper" style='padding-bottom:0'>
                    <div class="form-group">
                      <label class="control-label">Effectivity Date:</label><br>
                      <a href="#" id="effec_date"></a>
                    </div>
                  </div> 
                  <input type="hidden" name="" id='positionvalue'>
                  <input type="hidden" name="" id='effecdatevalue'>
                  <div class="button-pane text-right  bg-gray">
                    <a class="btn btn-sm btn-info" id='createrecord'>
                      <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-plus"></i>
                      </span>
                      <span class="button-content">
                        Employ
                      </span>
                    </a>
                    <a class="btn btn-sm" data-dismiss='modal'>
                      <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-times"></i>
                      </span>
                      <span class="button-content">
                        Cancel
                      </span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-4">
  <div class="panel">
    <div class="panel-body">
      <img src="<?php echo base_url()."assets/images/$details->pic"; ?>"  alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';"  class='img-polaroid' style='border:2px solid #ddd;width:100%'/>
      <hr>
      <h4 class='font-bold text-center'><?php echo "$details->lname, $details->fname $details->mname";?></h4>
      <p class="text-center font-size-11">FULL NAME</p>
      <br>
      <h5 class="font-bold text-center"><?php echo "$details->pos_details";?></h5>
      <p class="text-center font-size-11">POSITION APPLIED</p>
      <?php if($details->class=='Agent' && $details->ccaExp=='Yes'):?>
       <hr>
       <?php if($details->cc_yrs!=NULL){
        $cc=explode('|',$details->cc_yrs);
        $cc_yrs=$cc[0];
        $cc_mnt=$cc[1];}?>
        <div class="content-box">
          <div class="content-box-wrapper">
            <div class="form-group">
              <label class="control-label col-sm-12 text-center">YEARS OF CCA EXPERIENCE</label>
            </div>
          </div>
          <div class="content-box-wrapper">
            <div class="form-group">
              <label class="col-sm-4 control-label text-right">Year:</label>
              <div class="col-sm-8">
                <a href="#" id="cc_yrs" data-value="<?php if($details->cc_yrs!=NULL){ echo $cc_yrs;}?>">
                  <?php if($details->cc_yrs!=NULL){ echo $cc_yrs;}?>
                </a>
              </div>
            </div>
          </div>
          <div class="content-box-wrapper">
            <div class="form-group">
              <label class="col-sm-4 control-label text-right">Month:</label>
              <div class="col-sm-8">
                <a href="#" id="cc_mnt" data-value="<?php if($details->cc_yrs!=NULL){ echo $cc_mnt;}?>">
                  <?php if($details->cc_yrs!=NULL){ echo $cc_mnt;}?>
                </a>
              </div>
            </div>
          </div>
        </div>
      <?php endif;?>
    </div>
  </div>
</div>
</div>
</div>
<div class="row" hidden>
  <div class="col-md-8">
    <div class="panel">
      <div class="panel-body">
        <h3 class="title-hero">
          Recent sales activity
        </h3>
        <div class="example-box-wrapper">
          <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
        </div>
      </div>
    </div>
    <div class="content-box">
      <h3 class="content-box-header bg-default">
        <i class="glyph-icon icon-cog"></i>
        Live server status
        <span class="header-buttons-separator">
          <a href="#" class="icon-separator">
            <i class="glyph-icon icon-question"></i>
          </a>
          <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
            <i class="glyph-icon icon-refresh"></i>
          </a>
          <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
            <i class="glyph-icon icon-times"></i>
          </a>
        </span>
      </h3>
      <div class="content-box-wrapper">
        <div id="data-example-3" style="width: 100%; height: 250px;"></div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Templatedesign/headerLeft",
    cache: false,
    success: function(html)
    {
      $('#headerLeft').html(html);
		   //alert(html);
     }
   });	
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Templatedesign/sideBar",
    cache: false,
    success: function(html)
    {

      $('#sidebar-menu').html(html);
		 // alert(html);
		}
  });
 });	
</script>
<script type="text/javascript">
  $(function(){
    var list1 = [];
    for (var i = 0; i <= 30; i++) {
      list1.push({value:i,text:i});
    }
    var yrs = JSON.stringify(list1);
    var list2 = [];
    for (var j = 0; j <= 11; j++) {
      list2.push({value:j,text:j});
    }
    var mnt = JSON.stringify(list2);

    $("#cc_yrs").editable({
      mode:"inline",
      name:"cc_yrs",
      type:"select",
      source: yrs
    });
    $("#cc_mnt").editable({
      mode:"inline",
      name:"cc_mnt",
      type:"select",
      source: mnt
    });

    $("#fname").editable({type:"text",pk:1,name:"fname",title:"Enter First Name",mode:"inline"});
    $("#mname").editable({type:"text",pk:1,name:"mname",title:"Enter Middle Name",mode:"inline"});
    $("#lname").editable({type:"text",pk:1,name:"lname",title:"Enter Last Name",mode:"inline"});    
    $("#birthday").editable({type:"date",pk:1,name:"birthday",title:"Enter Birth Day",mode:"inline",combodate: {minYear: <?php echo date('Y', strtotime('-100 years'));?>,maxYear: <?php echo date("Y"); ?>}});
    $("#gender").editable({
      mode:"inline",
      name:"gender",
      type:"select",
      value:"<?php echo trim($details->gender); ?>",
      source:
      [{value: 'Male', text: 'Male'},
      {value: 'Female', text: 'Female'}]
    });
    $("#civilStatus").editable({
      mode:"inline",
      name:"civilStatus",
      type:"select",
      value:"<?php echo trim($details->civilStatus); ?>",
      source:
      [{value: 'Single', text: 'Single'},
      {value: 'Married', text: 'Married'},
      {value: 'Widowed', text: 'Widowed'},
      {value: 'Divorced', text: 'Divorced'},
      {value: 'Separated', text: 'Separated'}]
    });
    $("#religion").editable({
      mode:"inline",
      name:"religion",
      type:"select",
      value:"<?php echo trim($details->relijon); ?>",
      source:
      [
      {value: 'Roman Catholic', text: 'Roman Catholic'},
      {value: 'Muslim', text: 'Muslim'},
      {value: 'Protestant', text: 'Protestant'},
      {value: 'Iglesia ni Cristo', text: 'Iglesia ni Cristo'},
      {value: 'El Shaddai', text: 'El Shaddai'},
      {value: 'Jehovahs Witnesses', text: 'Jehovah’s Witnesses'}
      ]
    });
    $("#cell").editable({type:"text",pk:1,name:"cell",title:"Enter Cellphone Number",mode:"inline"});
    $("#tel").editable({type:"text",pk:1,name:"tel",title:"Enter Alternative Number",mode:"inline"});
    $("#position").editable({
      mode:"inline",
      name:"position",
      type:"select",
      tpl: "<select type='text' style='width: 300px'>",
      value:"<?php echo trim($details->pos_id); ?>",
      source: <?php echo trim($positions);?>
    });
    $("#source").editable({type:"text",pk:1,name:"source",title:"Enter Source",mode:"inline"});
    $("#ccaExp").editable({
      mode:"inline",
      name:"ccaExp",
      type:"select",
      value:"<?php echo trim($details->ccaExp); ?>",
      source:
      [{value: 'Yes', text: 'Yes'},
      {value: 'None', text: 'None'}]
    });

    $("#cr1fname").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 1 First Name",mode:"inline"});
    $("#cr1mname").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 1 Middle Name",mode:"inline"});
    $("#cr1lname").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 1 Last Name",mode:"inline"});
    $("#cr1job").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 1 Occupation",mode:"inline"});
    $("#cr1cell").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 1 Mobile Number",mode:"inline"});

    $("#cr2fname").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 2 First Name",mode:"inline"});
    $("#cr2mname").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 2 Middle Name",mode:"inline"});
    $("#cr2lname").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 2 Last Name",mode:"inline"});
    $("#cr2job").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 2 Occupation",mode:"inline"});
    $("#cr2cell").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 2 Mobile Number",mode:"inline"});

    $("#cr3fname").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 3 First Name",mode:"inline"});
    $("#cr3mname").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 3 Middle Name",mode:"inline"});
    $("#cr3lname").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 3 Last Name",mode:"inline"});
    $("#cr3job").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 3 Occupation",mode:"inline"});
    $("#cr3cell").editable({type:"text",pk:1,name:"tel",title:"Enter Character Reference 3 Mobile Number",mode:"inline"});


    $("#effec_date").editable({type:"date", pk:1,name:"effec_date",title:"Enter Effectivity Date",mode:"inline",combodate:{minYear:moment().format('YYYY'),maxYear:moment().add(2,'years').format('YYYY')}});
    //-------------------------------------------------------------------------------------------
    var updater = function(name,value){
      if(name=='birthday'){
        value = moment(value).format('YYYY-MM-DD');
      }
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/Applicant/updateApplicantValue",
        data: {
          apid: '<?php echo $apid;?>',
          name: name,
          value: value
        },
        cache: false,
        success: function(res)
        {
          res = $.trim(res);
          if(res=='Success'){
           $.jGrowl( "Successfully Updated.",{sticky:!1,position:"top-right",theme:"bg-blue-alt"});
         }else{
           $.jGrowl( "Failed.",{sticky:!1,position:"top-right",theme:"bg-red"});
         }

       }
     });
    }

    var charrefupdater = function(name,index,value){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/Applicant/updateCharRef",
        data: {
          apid: '<?php echo $apid;?>',
          name: name,
          index: index,
          value: value
        },
        cache: false,
        success: function(res)
        {
          res = $.trim(res);
          if(res=='Success'){
           $.jGrowl( "Successfully Updated.",{sticky:!1,position:"top-right",theme:"bg-blue-alt"});
         }else{
           $.jGrowl( "Failed.",{sticky:!1,position:"top-right",theme:"bg-red"});
         }

       }
     });
    }

    var ccayrsupdater = function(index,value){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/Applicant/ccayrsupdater",
        data: {
          apid: '<?php echo $apid;?>',
          index: index,
          value: value,
          data: "<?php echo $details->cc_yrs;?>"
        },
        cache: false,
        success: function(res)
        {
          res = $.trim(res);
          if(res=='Success'){
           $.jGrowl( "Successfully Updated.",{sticky:!1,position:"top-right",theme:"bg-blue-alt"});
         }else{
           $.jGrowl( "Failed.",{sticky:!1,position:"top-right",theme:"bg-red"});
         }

       }
     });
    }

    $("#fname").on('save',function(e,params){updater('fname',params.newValue);location.reload();});
    $("#mname").on('save',function(e,params){updater('mname',params.newValue);location.reload();});
    $("#lname").on('save',function(e,params){updater('lname',params.newValue);location.reload();});
    $("#birthday").on('save',function(e,params){updater('birthday',params.newValue);});
    $("#gender").on('save',function(e,params){updater('gender',params.newValue);});
    $("#civilStatus").on('save',function(e,params){updater('civilStatus',params.newValue);});
    $("#religion").on('save',function(e,params){updater('relijon',params.newValue);});
    $("#cell").on('save',function(e,params){updater('cell',params.newValue);});
    $("#tel").on('save',function(e,params){updater('tel',params.newValue);});
    $("#position").on('save',function(e,params){updater('pos_id',params.newValue);location.reload();});
    $("#source").on('save',function(e,params){updater('Source',params.newValue);});
    $("#ccaExp").on('save',function(e,params){updater('ccaExp',params.newValue);updater('cc_yrs',null);location.reload();});

    $("#cr1fname").on('save',function(e,params){charrefupdater('cr1', 'fname', params.newValue);});
    $("#cr1mname").on('save',function(e,params){charrefupdater('cr1', 'mname', params.newValue);});
    $("#cr1lname").on('save',function(e,params){charrefupdater('cr1', 'lname', params.newValue);});
    $("#cr1job").on('save',function(e,params){charrefupdater('cr1', 'cell', params.newValue);});
    $("#cr1cell").on('save',function(e,params){charrefupdater('cr1', 'job', params.newValue);});

    $("#cr2fname").on('save',function(e,params){charrefupdater('cr2', 'fname', params.newValue);});
    $("#cr2mname").on('save',function(e,params){charrefupdater('cr2', 'mname', params.newValue);});
    $("#cr2lname").on('save',function(e,params){charrefupdater('cr2', 'lname', params.newValue);});
    $("#cr2job").on('save',function(e,params){charrefupdater('cr2', 'cell', params.newValue);});
    $("#cr2cell").on('save',function(e,params){charrefupdater('cr2', 'job', params.newValue);});

    $("#cr3fname").on('save',function(e,params){charrefupdater('cr3', 'fname', params.newValue);});
    $("#cr3mname").on('save',function(e,params){charrefupdater('cr3', 'mname', params.newValue);});
    $("#cr3lname").on('save',function(e,params){charrefupdater('cr3', 'lname', params.newValue);});
    $("#cr3job").on('save',function(e,params){charrefupdater('cr3', 'cell', params.newValue);});
    $("#cr3cell").on('save',function(e,params){charrefupdater('cr3', 'job', params.newValue);});

    $("#cc_yrs").on('save',function(e,params){ccayrsupdater('cc_yrs', params.newValue);});
    $("#cc_mnt").on('save',function(e,params){ccayrsupdater('cc_mnt', params.newValue);});
  })
</script>

<script type="text/javascript">
  $("#pass").click(function(){
    var apid = "<?php echo $apid;?>";
    var cc_yrs = "<?php echo $details->cc_yrs;?>";
    var ccaExp = "<?php echo $details->ccaExp;?>";
    var aclass = "<?php echo $details->class;?>";
    var pos = "<?php echo $details->pos_details;?>";
    var pos_id = "<?php echo $details->pos_id;?>";

    // alert(cc_yrs +" - "+aclass+" - "+ccaExp);
    if(cc_yrs=="" && aclass=='Agent' && ccaExp=='Yes'){
      $("#inf_year").show();
      // alert(cc_yrs +" - "+aclass);
    }else if(pos=='Pool'){
     $("#inf_pool").show();
   }else{
     $("#inf_pool").hide();
     $("#inf_year").hide();
     if(aclass!='Agent'){
       $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Applicant/getDepartmentPosition",
        data:{
          pos_id:pos_id
        },
        cache: false,
        success: function(res)
        {
         res = res.trim();
         // alert(res);
         // res = JSON.parse(res);
         $("#positionApplied").editable({
          mode:"inline",
          name:"position",
          type:"select",
          source: res
        }); 
         $('#positionApplied').editable('option', 'source', res);         
       }
     });
     }else{
       $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Applicant/getCCAPosition",
        cache: false,
        success: function(res)
        {
         res = res.trim();
         // res = JSON.parse(res);
         $("#positionApplied").editable({
          mode:"inline",
          name:"position",
          type:"select",
          source: res
        });  
         $('#positionApplied').editable('option', 'source', res);          
       }
     });
     }
     $("#passdiv").show(1000);
   }

 });
  $("#fail").click(function(){
    $("#passdiv").hide(500);
    var apid = "<?php echo $apid;?>";
    swal({
      title: 'Applicant Evaluation',
      type: 'error',
      html:
      'Are you sure to <b>FAIL</b> this applicant?',
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText:
      '<i class="glyph-icon icon-check"></i> Confirm',
      cancelButtonText:
      '<i class="glyph-icon icon-times"></i>'
    }).then(function () {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Applicant/failApplicant",
        data:{
          apid:apid
        },
        cache: false,
        success: function(res)
        {
          if(res.trim()=='Success'){
           swal(
            'Applicant Has Failed!',
            'Applicant Evaluation have been approved and confirmed.',
            'warning'
            )
         }else{
          swal(
            'Error Occured!',
            'Please try again later.',
            'error'
            )
        }
        window.setTimeout(function(){  
          window.location = "<?php echo base_url();?>index.php/Applicant";
        } ,2000);
      }
    });

    })
  });
</script>
<script type="text/javascript">
  $("#createrecord").click(function(){
    var apid = "<?php echo $apid;?>";
    var pos = $("#positionvalue").val();
    var date = $("#effecdatevalue").val();
    if(pos==''||pos==null){
      $.jGrowl( "Please choose employment position",{sticky:!1,position:"top-right",theme:"bg-orange"});
    }else if(date==''||date==null){
      $.jGrowl( "Please choose effectivity date",{sticky:!1,position:"top-right",theme:"bg-orange"});
    } else{
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/Applicant/createemployeerecord",
        data:{
          apid:apid,
          posempstat_id:pos,
          effectivedate:date
        },
        cache: false,
        success: function(res)
        {
          if(res.trim()=='Success'){
           swal(
            'Applicant is Employed!',
            'Applicant Evaluation have been approved and confirmed.',
            'success'
            );
         }else{
          swal(
            'Error Occured!',
            'Please try again later.',
            'error'
            );
        }
        window.setTimeout(function(){  
          window.location = "<?php echo base_url();?>index.php/Applicant";
        } ,2000);
      }
    });
    }
  });
</script>
<script type="text/javascript">
  $("#positionApplied").on('save',function(e,params){$("#positionvalue").val(params.newValue)});
  $("#effec_date").on('save',function(e,params){$("#effecdatevalue").val(moment(params.newValue).format('YYYY-MM-DD'));});
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>