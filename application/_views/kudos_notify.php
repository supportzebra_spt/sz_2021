<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  th{
    background:#666 !important;
    color:white !important;
    padding-top:10px;
    padding-bottom:10px;
}
.table-striped>tbody>tr:nth-child(odd)>td, 
.table-striped>tbody>tr:nth-child(odd)>th {
    background-color: #eee;
    color:#555;
    font-size:13px;
}
.table-striped>tbody>tr:nth-child(even)>td, 
.table-striped>tbody>tr:nth-child(even)>th {
    background-color: white;
    color:#777;
    font-size:13px;
}
body { padding-right: 0 !important }
</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript">
    $(window).load(function(){
       setTimeout(function() {
          $('#loading').fadeOut( 400, "linear" );
      }, 300);
       $("#t3").click();
       $("#t2").click();
       $("#t1").click();
   });

</script>
<!-- jQueryUI Tabs -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/tabs-ui/tabs.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs-ui/tabs.js"></script>
<script type="text/javascript">
    /* jQuery UI Tabs */

    $(function() { "use strict";
        $(".tabs").tabs();
    });

    $(function() { "use strict";
        $(".tabs-hover").tabs({
            event: "mouseover"
        });
    });
</script>

<!-- Boostrap Tabs -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs/tabs.js"></script>

<!-- Tabdrop Responsive -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tabs/tabs-responsive.js"></script>
<script type="text/javascript">
    /* Responsive tabs */
    $(function() { "use strict";
        $('.nav-responsive').tabdrop();
    });
</script>
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>
<script type="text/javascript">
  /* Input switch */

  $(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
});
</script>
</head>

<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<div id="page-wrapper">
  <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
      <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
      <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
  </div>
  <div id="header-logo" class="logo-bg">
   <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
   <a id="close-sidebar" href="#" title="Close sidebar">
      <i class="glyph-icon icon-angle-left"></i>
  </a>
</div>
<div id='headerLeft'>
</div>

</div>
<div id="page-sidebar">
  <div class="scroll-sidebar">


    <ul id="sidebar-menu">
    </ul>
</div>
</div>
<div id="page-content-wrapper">
  <div id="page-content">

    <div id="page-title">


        <div class="row">
            <div class="col-md-8">
                <h2>Kudos Users</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="#" class="btn btn-sm bg-blue" data-toggle="modal" data-target="#myModal">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-plus"></i>
                    </span>
                    <span class="button-content">
                        Add New User
                    </span>
                </a>
            </div>    
        </div>
        
        <hr>
        <div class="example-box-wrapper">
            <div class="content-box tabs">
                <h3 class="content-box-header bg-black">
                    <span class="icon-separator">
                        <i class="glyph-icon icon-cog"></i>
                    </span>
                    <span>Kudos Persons to Notify</span>

                    <ul>
                        <li>
                            <a href="#tabs1" title="Tab 1" id='t1'>
                                Requestors
                            </a>
                        </li>
                        <li>
                            <a href="#tabs2" title="Tab 2" id='t2'>
                                Approvers
                            </a>
                        </li>
                        <li>
                            <a href="#tabs3" title="Tab 3" id='t3'>
                                Releasers
                            </a>
                        </li>
                    </ul>
                </h3>
                <div id="tabs1">
                   <table class="table table-striped table-bordered responsive no-wrap table-condensed">
                    <thead class='font-bold'>
                        <tr>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Department</th>
                            <th>Position</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot class='font-bold'>
                        <tr>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Department</th>
                            <th>Position</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody id='requesttbody'>
                    </tbody>
                </table>
            </div>
            <div id="tabs2">
             <table class="table table-striped table-bordered responsive no-wrap table-condensed">
                <thead class='font-bold'>
                    <tr>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Department</th>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot class='font-bold'>
                    <tr>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Department</th>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody id='approvetbody'>
                </tbody>
            </table>
        </div>
        <div id="tabs3">
         <table class="table table-striped table-bordered responsive no-wrap table-condensed">
            <thead class='font-bold'>
                <tr>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Department</th>
                    <th>Position</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot class='font-bold'>
                <tr>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Department</th>
                    <th>Position</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody id='releasetbody'>
            </tbody>
        </table>
    </div>
</div>                    
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header pad20A" style='color:white;background:#333'>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Assign Employee as User<h4>
                </div>
                <div class="modal-body">
                 <div class="row">

                    <div class="col-md-4">
                     <label class="control-label">Role</label>
                     <select class='form-control' id='role'>
                        <option value=''>--</option>
                        <option value='Request'>Request</option>
                        <option value='Approve'>Approve</option>
                        <option value='Release'>Release</option>
                    </select>
                </div>
                <div class="col-md-8">
                    <label class="control-label">Department</label>
                    <select class='form-control' id='department'>
                        <option value=''>--</option>
                        <?php foreach($department as $dept):?>
                            <option value='<?php echo $dept->dep_id;?>'><?php echo $dept->dep_details;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="col-md-12"><br>
                 <label class="control-label">Employee</label>
                 <select class='form-control' id='users'></select>
             </div>
         </div>
     </div>
     <div class="modal-footer"  style='color:white;background:#333'>
        <a href="#" class="btn btn-default" data-dismiss="modal">
            <span class="glyph-icon icon-separator">
                <i class="glyph-icon icon-times"></i>
            </span>
            <span class="button-content">
                Close
            </span>
        </a>
        <a href="#" class="btn btn-info" id='submit'>
            <span class="glyph-icon icon-separator">
                <i class="glyph-icon icon-plus"></i>
            </span>
            <span class="button-content">
                Create
            </span>
        </a>
    </div>
</div>
</div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
       }
   });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
     }
 });
});   
</script>
<script type="text/javascript">
    $("#t1").click(function(){
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/getrequestors",
          cache: false,
          success: function(res)
          {
            res = res.trim();
            res = JSON.parse(res);
            $("#requesttbody").html("");
            $.each(res,function(i,item){
                var tbody = "";
                tbody += "<tr>";
                tbody += "<td>"+item.lname+", "+item.fname+"</td>";
                tbody += "<td>"+item.role+"</td>";
                tbody += "<td>"+item.dep_name+" - "+item.dep_details+"</td>";
                tbody += "<td>"+item.pos_details+"</td>";
                tbody += "<td>"+item.status+"</td>";
                tbody += "<td><input type='checkbox' data-on-color='info' data-off-color='danger' class='input-switch' checked data-size='small' data-on-text='A' data-off-text='IA' id='id"+item.ksetting_id+"' onchange='setActiveness("+item.ksetting_id+")'></td>";
                tbody += "</tr>";
                $("#requesttbody").append(tbody);
            })

            $('.input-switch').bootstrapSwitch();
        }
    });
  })
</script>
<script type="text/javascript">
    $("#t2").click(function(){
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/getapprovers",
          cache: false,
          success: function(res)
          {
            res = res.trim();
            res = JSON.parse(res);
            $("#approvetbody").html("");
            $.each(res,function(i,item){
                var tbody = "";
                tbody += "<tr>";
                tbody += "<td>"+item.lname+", "+item.fname+"</td>";
                tbody += "<td>"+item.role+"</td>";
                tbody += "<td>"+item.dep_name+" - "+item.dep_details+"</td>";
                tbody += "<td>"+item.pos_details+"</td>";
                tbody += "<td>"+item.status+"</td>";
                tbody += "<td><input type='checkbox' data-on-color='info' data-off-color='danger' class='input-switch' checked data-size='small' data-on-text='A' data-off-text='IA' id='id"+item.ksetting_id+"' onchange='setActiveness("+item.ksetting_id+")'></td>";
                tbody += "</tr>";
                $("#approvetbody").append(tbody);
            })

            $('.input-switch').bootstrapSwitch();
        }
    });
  })
</script>
<script type="text/javascript">
    $("#t3").click(function(){
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/getreleasers",
          cache: false,
          success: function(res)
          {
            res = res.trim();
            res = JSON.parse(res);
            $("#releasetbody").html("");
            $.each(res,function(i,item){
                var tbody = "";
                tbody += "<tr>";
                tbody += "<td>"+item.lname+", "+item.fname+"</td>";
                tbody += "<td>"+item.role+"</td>";
                tbody += "<td>"+item.dep_name+" - "+item.dep_details+"</td>";
                tbody += "<td>"+item.pos_details+"</td>";
                tbody += "<td>"+item.status+"</td>";
                tbody += "<td><input type='checkbox' data-on-color='info' data-off-color='danger' class='input-switch' checked data-size='small' data-on-text='A' data-off-text='IA' id='id"+item.ksetting_id+"' onchange='setActiveness("+item.ksetting_id+")'></td>";
                tbody += "</tr>";
                $("#releasetbody").append(tbody);
            })

            $('.input-switch').bootstrapSwitch();
        }
    });
  })
</script>
<script type="text/javascript">
  function setActiveness(id){

    if ($('#id'+id).is(":checked")){
      // alert("Yes - "+id);
      var active = 1;
  }else{
      // alert("No - "+id);
      var active = 0;
  }

  $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Kudos/updateActiveNotify",
      data: {
        ksetting_id: id,
        activevalue: active
    },
    cache: false,
    success: function(res)
    {
       if(res.trim()=='Success'){
           $.jGrowl("Successfully Updated",{sticky:!1,position:"top-right",theme:"bg-blue-alt"});
       }else{
           $.jGrowl("Failed",{sticky:!1,position:"top-right",theme:"bg-red"});
       }
   }
});
}
</script>
<script type="text/javascript">

    $('#department').on('change', function(){
        var dep_id = $(this).val();
        var role = $("#role").val();  
        // alert("yes");
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/getEmpInDept",
          data:{
            dep_id:dep_id,
            role:role
        },
        cache: false,
        success: function(res)
        {
            res = res.trim();
            res = JSON.parse(res);
            $('#users').html('');
            $('#users').append('<option value="">--</option>');
            $.each(res,function (i,elem){
             $('#users').append('<option value="'+elem.uid+'">'+elem.lname+", "+elem.fname+'</option>');
         })
        }
    });
    });  
</script>
<script type="text/javascript">
    $("#role").on('change',function(){
        $("#users").html('');
        $("#department").val('');
    });
</script>
<script type="text/javascript">
    $("#submit").click(function(){
        var uid = $('#users').val();
        var role = $("#role").val();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/updatekudosnotify",
          data:{
            uid:uid,
            role:role
        },
        cache: false,
        success: function(res)
        {
            res = res.trim();
            if(res=='Success'){
                swal('Success','Successfully assigned user to kudos','success');
            }else{
                swal('Error','Failed to assign user to kudos','error');
            }
            if(role=='Request'){
                $("#t1").click();
            }else if(role=='Approve'){

                $("#t2").click();
            }else if(role=='Release'){

                $("#t3").click();
            }

            $("#role").val('').change();
        }
    });
    });
</script>
</html>