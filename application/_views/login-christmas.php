<html>
<?php 
 $dir = base_url()."assets_login/";
 $dir2 = base_url()."assets/";
 ?>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SupportZebra | Daily Time Record</title>
         <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<?php echo $dir; ?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $dir; ?>font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $dir; ?>css/form-elements.css">
        <link rel="stylesheet" href="<?php echo $dir; ?>css/style.css">
    <link href="<?php echo $dir2; ?>css/toastr.min.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="<?php echo $dir; ?>ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $dir; ?>ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $dir; ?>ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $dir; ?>ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo $dir; ?>ico/apple-touch-icon-57-precomposed.png">
<style>
.inner-bg {
    padding: 60px 0 10px 0;
}

button.btn {
    background: #4c4c4c; 
}
button.btn:focus {
    outline: 0;
    opacity: 0.6;
    background: #000000;
    color: #fff;
}
::selection {
    background: #ffffff;
    color: #fff;
    text-shadow: none;
}
html{
	background: #AACAD5;
}
body { 
			background: #453633;
			margin: 0px auto;
 			
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#0E708B', endColorstr='#AACAD5'); 
			background: -webkit-gradient(linear, left top, left bottom, from(#0E708B), to(#AACAD5)); 
			background: -moz-linear-gradient(top,  #0E708B,  #AACAD5);
			overflow:hidden;
background-repeat: no-repeat;
		} 
 canvas {
       margin-top: -47%;
    height: 100%;
    width: 100%;
 }
.form-top{
	background-image: url(http://10.200.101.250/supportzebra/assets/images/header.png);
    height: 280px;
	    box-shadow: 10px 10px 5px #609fb199;
	border-top-right-radius: 61px;
	border-top-left-radius: 61px;
}
.form-bottom{
    background: rgb(214, 231, 237);
	    box-shadow: 10px 10px 5px #609fb199;
	border-bottom-left-radius: 61px;
	border-bottom-right-radius: 61px;

}
button.btn {
    background: #227c94;
}
button.btn:focus {
    background: #227c94;

}
.form-box {
    margin-top: -15px;
}
</style>
	
	
</head>

<body class="gray-bg">
<div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
					<!--<img src='<?php echo base_url()."assets/images/sz2.png"; ?>'>-->
                    <div class="row">

                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
 
                            </div>
                            <div class="form-bottom" style="text-align: center;">
								<span style="font-size: larger;font-weight: bold;color: #227c94;text-align: center;"> EMPLOYEE LOGIN </span>
			                    <form role="form" action="#" method="post" class="login-form" onsubmit="return submitdata();">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input type="text" name="form-username" id="uname" placeholder="Username..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="form-password" id="pw" placeholder="Password..." class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn"id="BtnLog">Get me in!</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                   <!-- <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 social-login">
                        	<h3>...or login with:</h3>
                        	<div class="social-login-buttons">
	                        	<a class="btn btn-link-2" href="#">
	                        		<i class="fa fa-facebook"></i> Facebook
	                        	</a>
	                        	<a class="btn btn-link-2" href="#">
	                        		<i class="fa fa-twitter"></i> Twitter
	                        	</a>
	                        	<a class="btn btn-link-2" href="#">
	                        		<i class="fa fa-google-plus"></i> Google Plus
	                        	</a>
                        	</div>
                        </div>
                    </div>-->
                </div>
            </div>
            
        </div>

 
        <!-- Javascript -->
        <script src="<?php echo $dir; ?>js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo $dir; ?>bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo $dir; ?>js/jquery.backstretch.min.js"></script>
        <script src="<?php echo $dir; ?>js/scripts.js"></script>
		    <script src="<?php echo $dir2; ?>js/toastr.min.js"></script>
					<script type="text/javascript" src="<?php echo $dir2; ?>christmas/Three.js"></script>
				<script type="text/javascript" src="<?php echo $dir2; ?>christmas/Snow.js"></script>
				<script type="text/javascript" src="<?php echo $dir2; ?>christmas/SnowInit.js"></script>

</body>
<script>
function submitdata(){
	
		
		var checkuname =  $("#uname").hasClass("input-error");
		var checkpw =  $("#pw").hasClass("input-error");
	 	if(checkuname && checkpw){
		setTimeout(function(){ },1000);	 
}
			
 return false;
}
$(function(){
 	$("#BtnLog").click(function(){
		var uname = $("#uname").val();
		var pw = $("#pw").val();
	if(uname!="" && pw!=""){
		$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>index.php/login/loginVerify",
				data: {uname:uname,pw:pw},
				cache: false,
				success: function(html)
				{
					if(html>0){
						  window.location.href="<?php echo base_url(); ?>index.php/home";
 					}else{
						// $("#loginRes").html('<center><font color="red"> Username/Password mismatch! </font></center>');
						                toastr.error('Try to log back in.','Username/Password Mismatch!')

					}
				}
		});
	}else{
			toastr.error('input fields cannot be empty','Empty Fields')

	}
		
	});
	
});
</script>

 </html>
