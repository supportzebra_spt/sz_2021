<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  .modal-open{padding:0px !important;}
</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">
<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

<script type="text/javascript">
  $(window).load(function(){
   setTimeout(function() {
    $('#loading').fadeOut( 400, "linear" );
  }, 300);
 });
</script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker/datepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
  /* Datepicker bootstrap */

  $(function() { "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
      format: 'yyyy-mm-dd'
    });
  });

</script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/xeditable/xeditable.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/xeditable/xeditable-demo.js"></script> -->

<!-- Bootstrap Summernote WYSIWYG editor -->

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/tooltip/tooltip.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/popover/popover.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div class="container">

          <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>

          <script type="text/javascript">
            /* Input masks */

            $(function() { "use strict";
              $(".input-mask").inputmask();
            });

          </script>

          <div id="page-title">

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header bg-black">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">New Employee Position History</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-md-2">
                        <h5>Employee Name</h5>
                      </div>
                      <div class="col-md-10">
                        <p class='font-size-14 font-bold' id='fullname'></p>
                      </div>
                    </div>
                    <br>
                    <input type="hidden" name="" id="empid">
                    <div class="row">
                      <div class="col-md-2">
                        <h5>Department</h5>
                      </div>
                      <div class="col-md-10">
                        <span class="text-danger" id='dn' style='font-size:8px;' hidden>* (Required)</span>
                        <select class='form-control' id='department'>
                          <option value=''>--</option>
                          <?php foreach($departments as $d):?>
                            <option value='<?php echo $d->dep_id;?>'><?php echo $d->dep_details;?></option>
                          <?php endforeach;?>
                        </select>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-2">
                        <h5>Position</h5>
                      </div>
                      <div class="col-md-10">
                        <span class="text-danger" id='pn' style='font-size:8px;' hidden>* (Required)</span>
                        <select class='form-control' id='position'>
                          <option value=''>--</option>

                        </select>
                      </div>
                    </div>

                    <br>
                    <div class="row">
                      <div class="col-md-2">
                        <h5>Date</h5>
                      </div>
                      <div class="col-md-10">
                        <span class="text-danger" id='en' style='font-size:8px;' hidden>* (Required)</span>

                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon">
                            <i class="glyph-icon icon-calendar"></i>
                          </span>
                          <input type="text" id='effecdate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
                        </div>
                      </div>
                    </div>

                    <br>
                    <div class="row">
                      <div class="col-md-2">
                        <h5>Status</h5>
                      </div>
                      <div class="col-md-10">
                        <span class="text-danger" id='sn' style='font-size:8px;' hidden>* (Required)</span>
                        <select class='form-control' id='status'>
                          <option value=''>--</option>
                          <option value='1'>Trainee</option>
                          <option value='2'>Probationary</option>
                          <option value='3'>Regular</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id='submitmymodal'>Save</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-9">
                <h3>EMPLOYEE HISTORY</h3>
                <hr>
              </div>
              <div class="form-group col-md-3 text-right">
                <label>SEARCH</label>
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search Here ..." class='form-control'>
              </div>
            </div>
            <div class="row">
              <?php foreach($values as $emp):?>
                <div class="col-md-12  style_prevu_kit" data-fullname='<?php echo $emp->lname.', '.$emp->fname;?>'>
                  <div class="panel">
                    <div class="panel-heading" style='background:#333;'> 
                     <div class="row">
                      <div class="text-left col-md-10">
                        <h4 style='color:white'><?php echo $emp->lname.', '.$emp->fname;?></h4>
                      </div>
                      <div class="col-md-2 text-right">
                        <button class='btn btn-xs btn-info' data-empid="<?php echo $emp->emp_id;?>" data-fullname='<?php echo $emp->lname.', '.$emp->fname;?>' data-toggle="modal" data-target="#myModal"><i class="glyph-icon icon-plus"></i> Position</button>
                      </div>
                    </div>
                  </div>
                  <div class="panel-body" style="background: #d6d6d6;">
				   <div class="row font-size-12 font-bold">
                      <div class="col-md-1"> </div>
                      <div class="col-md-4">POSITION</div>
                      <div class="col-md-2">STATUS</div>
                      <div class="col-md-1">CLASS</div>
                      <div class="col-md-4">DATE</div>
                    </div>
                  
                  </div>
                  <div class="panel-footer">
                   
                   	   <?php foreach($emp->history as $key => $hist): $color = ($key % 2 === 0) ? 'bg-gray-alt' : 'bg-gray';?>
                      <?php if($hist->isActive==1):?>
                       <div class="row" style="background: #f3f3ae;padding: 12px  0px 12px 0px;">
                        <div class="col-md-1 font-size-12 font-bold"><i>CURRENT</i></div>
                        <div class="col-md-4 font-bold"><?php echo $hist->pos_details;?></div>
                        <div class="col-md-2 font-bold"><?php echo $hist->status;?></div>
                        <div class="col-md-1 font-bold"><?php echo $hist->class;?></div>
                        <div class="col-md-4 font-bold"><a href="#" class='historydates' data-type="date" data-pk="1" data-value="<?php echo  $hist->date;?>" data-emppromoteid='<?php echo $hist->emp_promoteID;?>' data-title=""></a></div>
                      </div>
                    <?php endif;?>
                  <?php endforeach;?>
				  <br>
                    <?php foreach($emp->history as $key => $hist): $color = ($key % 2 === 0) ? 'bg-gray-alt' : 'bg-gray';?>
                      <?php if($hist->isActive==0):?>
                        <div class="row" style='margin-bottom:5px;padding: 12px  0px 12px 0px;   background: #d8ecf9;'>
                          <div class="col-md-1"><button data-id='<?php echo $hist->emp_promoteID;?>' class='btn btn-xs btn-danger btn-deleter'><i class="glyph-icon icon-times"></i></button></div>
                          <div class="col-md-4"><?php echo $hist->pos_details;?></div>
                          <div class="col-md-2"><?php echo $hist->status;?></div>
                          <div class="col-md-1"><?php echo $hist->class;?></div>
                          <div class="col-md-4"><a href="#" class='historydates' data-type="date" data-pk="1" data-value="<?php echo  $hist->date; ?>" data-emppromoteid='<?php echo  $hist->emp_promoteID;?>' data-title=""></a> </div>
                        </div>
                      <?php endif;?>
                    <?php endforeach;?>
                </div>
              </div>
            </div>
          <?php endforeach;?>
        </div>
      </div>

      <div class="row" hidden>
        <div class="col-md-8">
          <div class="panel">
            <div class="panel-body">
              <h3 class="title-hero">
                Recent sales activity
              </h3>
              <div class="example-box-wrapper">
                <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
          </div>




          <div class="content-box">
            <h3 class="content-box-header bg-default">
              <i class="glyph-icon icon-cog"></i>
              Live server status
              <span class="header-buttons-separator">
                <a href="#" class="icon-separator">
                  <i class="glyph-icon icon-question"></i>
                </a>
                <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                  <i class="glyph-icon icon-refresh"></i>
                </a>
                <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                  <i class="glyph-icon icon-times"></i>
                </a>
              </span>
            </h3>
            <div class="content-box-wrapper">
              <div id="data-example-3" style="width: 100%; height: 250px;"></div>
            </div>
          </div>

        </div>

      </div>
    </div>



  </div>
</div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script>
  $(function(){
    $(".historydates").editable({
      mode:"inline",
      combodate: {minYear: moment().subtract(100, "years").format("YYYY"),maxYear: moment().format("YYYY")}
    }).on('save',function(e,params){
      var value = params.newValue;
      if(value==''||value==null){

        $.jGrowl( "Error Updating.",{sticky:!1,position:"top-right",theme:"bg-red"});
      }else{
        var dater = value.format('YYYY-MM-DD');
        emp_promoteid = $(this).data('emppromoteid');
        console.log(value);
        // alert(emp_promoteid);
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/PositionRates/updateemppromotehist",
          data: {
            dater: dater,
            emp_promoteid: emp_promoteid
          },
          cache: false,
          success: function(result)
          {
            if(result=='Success'){
             $.jGrowl("Successfully Updated.",{sticky:!1,position:"top-right",theme:"bg-blue-alt"});
           }else{

            $.jGrowl( "Error Updating.",{sticky:!1,position:"top-right",theme:"bg-red"});
          }
        }
      });
      }
    })

  });
</script>
<script type="text/javascript">
 function myFunction(){
  var dom = document.querySelectorAll('.style_prevu_kit');
  var filter = $("#myInput").val();
  filter = filter.toUpperCase();
  for(var i=0;i<dom.length;i++){
    var a = dom[i].getAttribute('data-fullname');
    if (a.toUpperCase().indexOf(filter) > -1) {
      dom[i].style.display = "";
    } else {
      dom[i].style.display = "none";
    }
  }
}
</script>
<script type="text/javascript">
  $('#department').on('change', function(){
   var department_id = $('#department').val();
   $('#position').html('');
   $('#position').append('<option value="">--</option>');
   $.each(<?php echo json_encode($positions);?>,function (i,elem){
    if(elem.dep_id==department_id){
      // console.log(elem);
      var position = elem.pos_details;
      var pos_id = elem.pos_id;
      $('#position').append('<option value="'+pos_id+'">'+position+'</option>');
    }
  })
 });
</script>
<script type="text/javascript">
  $('#myModal').on('shown.bs.modal', function(e){
    var invoker = $(e.relatedTarget);
    $emp_id = invoker.data('empid');
    $fullname = invoker.data('fullname');
    $("#empid").val($emp_id);
    $("#fullname").html($fullname);
    $("#department").val('');
    $("#position").val('');
    $("#status").val('');
    $("#effecdate").val('');
  });
</script>
<script type="text/javascript">
  $("#submitmymodal").click(function(){
    var empid = $("#empid").val();
    var position = $("#position").val();
    var status = $("#status").val();
    var date = $("#effecdate").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/PositionRates/newpositionhistory",
      data: {
        empid:empid,
        position:position,
        status:status,
        date:date
      },
      cache: false,
      success: function(result)
      {
        result = result.trim();
        if(result=='Success'){
          $.jGrowl("Successfully Updated.",{sticky:!1,position:"top-right",theme:"bg-blue-alt"}); 
          location.reload(1000);
        }else if(result=='Failed2'){
         $.jGrowl( "Cannot Update... Position chosen has no RATE.",{sticky:!1,position:"top-right",theme:"bg-red"});
       }else{
        $.jGrowl( "Error Updating.",{sticky:!1,position:"top-right",theme:"bg-red"});
      }         

    }
  });  
  });
</script>
<script type="text/javascript">
  $(".btn-deleter").click(function(){
    var emp_promoteid = $(this).data('id');
    swal({
      title: 'Are you sure?',
      text: "It will not be part of employee's employment history.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/index.php/PositionRates/disableHistory",
        data: {
          emp_promoteid:emp_promoteid
        },
        cache: false,
        success: function(result)
        {
          result = result.trim();
          if(result=='Success'){
           swal(
            'Deleted!',
            'Record has been deleted.',
            'success'
            );
           location.reload(1000);
         }else{
          swal(
            'Error!',
            'Failed to delete record.',
            'error'
            );
          location.reload(1000);
        }         

      }
    }); 

    })
  });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>