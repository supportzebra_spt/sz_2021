<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
  /* Loading Spinner */
  .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
  body { padding-right: 0 !important }
  .ms-list{max-height:150px;}
  .bootstrap-timepicker-widget.dropdown-menu.open {
    display: inline-block;
    z-index: 10000;
  }
</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript">
  $(window).load(function(){
    setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
  });

</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
 $(function(){
  "use strict";
  var m =  moment().subtract(1,'year').format("MM/DD/YYYY");
  var end = moment().format("MM/DD/YYYY");
  $("#daterangepicker-example").daterangepicker({ minDate: m,startDate: end,maxDate : end});
  $("[name=daterangepicker_start]").attr('readonly',true);
  $("[name=daterangepicker_end]").attr('readonly',true);
});
</script>
<!-- Multi select -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
<script type="text/javascript">
  /* Multiselect inputs */

  $(function() { "use strict";
    $(".multi-select").multiSelect();
    $(".ms-selectable").append('<i class="glyph-icon icon-exchange"></i>');
  });
</script>
<!-- Bootstrap Timepicker -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/timepicker/timepicker.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/timepicker/timepicker.js"></script>
<script type="text/javascript">

  /* Timepicker */

  $(function() { "use strict";
    $('.timepicker-example').timepicker();
  });
</script>
</head>

<body>
  <div id="sb-site">
    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div id="page-title">
          <h2>Adjust Attendance Logs</h2><hr>
          <div class="panel">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-2">
                  <div class="form-group" id='acc_descriptiondiv'>
                    <label class='control-label'>Account Type</label>
                    <select class='form-control' id='acc_description'>
                      <option value='' id='removeme'>--</option>
                      <option value='All'>All</option>
                      <option value='Admin'>Admin</option>
                      <option value='Agent'>Agent</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group" id='acc_namediv'>
                    <label class='control-label'>Account Name</label>
                    <select class='form-control' id='acc_name'></select>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="col-md-12">
                    <label class='control-label'>Date Range</label>
                    <div class="input-prepend input-group">
                      <span class="add-on input-group-addon">
                        <i class="glyph-icon icon-calendar"></i>
                      </span>
                      <input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control" >
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <label class='control-label'>Employee List</label>
                  <select multiple name="" id="employeelist"  class="multi-select"></select>
                </div>
                <div class="col-md-4">
                  <br>
                  <button id="select" class='btn btn-info'>Select all</button>
                  <button id="deselect" class='btn btn-info'>Deselect all</button>
                </div>
              </div>
            </div>
            <div class="panel-footer text-right">
              <button class="btn btn-primary" id='generateschedule'>Display Schedule</button>
            </div>
          </div>

          <div class="loading-spinner" id="loadingspinner" style="display:none">
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
            <i class="bg-primary"></i>
          </div>

          <div class="content-box" id="scheduleview" style="display:none;">
            <h3 class="content-box-header bg-blue-alt"> 
              <i class="glyph-icon icon-list"></i>
              SCHEDULE LIST
            </h3>
            <div class="content-box-wrapper">

              <table class="table table-striped table-condensed">
                <thead>
                  <th>Name</th>
                  <th>Schedule</th>
                  <th colspan=2>Log In</th>
                  <th colspan=2>Log Out</th>
                </thead>
                <tbody id="tbody" style='color:#666 !important'></tbody>
              </table>
            </div>
          </div>

        </div>
      </div> 
    </div>
  </div>
</div>
</div>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $(function(){

   $("#employeelist").prop('disabled',true);
   $("#employeelist").multiSelect('refresh');
   $("#select").prop('disabled',true);
   $("#deselect").prop('disabled',true);
   $("#acc_name").prop('disabled',true);
   $("#acc_description").on('change',function(){
    var desc = $(this).val();
    $("#removeme").remove();
    if(desc=='All'){

     $("#employeelist").prop('disabled',true);
     $("#employeelist").multiSelect('refresh');
     $("#select").prop('disabled',true);
     $("#deselect").prop('disabled',true);
     $("#acc_name").prop('disabled',true);
   }else{

     $("#acc_name").prop('disabled',false);
     $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/getAccounts",
      data: {
        desc: desc
      },
      cache: false,
      success: function(res)
      {
        if(res!='Empty'){
          $("#acc_name").html("<option value='All'>All</option>");
          $.each(JSON.parse(res),function (i,elem){
              // alert(elem.rate);
              $("#acc_name").append("<option value='"+elem.acc_id+"'>"+elem.acc_name+"</option>");
            });
        }
      }
    });
   }
 });
 })
</script>
<script type="text/javascript">
  $(function(){
    $("#acc_name").on('change',function(){
      var acc_id = $(this).val();
      if(acc_id=='All'){
       $("#employeelist").prop('disabled',true);
       $("#employeelist").multiSelect('refresh');
       $("#select").prop('disabled',true);
       $("#deselect").prop('disabled',true);
     }else{
      $("#employeelist").prop('disabled',false);
      $("#employeelist").multiSelect('refresh');
      $("#select").prop('disabled',false);
      $("#deselect").prop('disabled',false);
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/getAccountEmployee",
        data: {
          acc_id: acc_id
        },
        cache: false,
        success: function(res)
        {
          if(res!='Empty'){
            $("#employeelist").html("");
            $.each(JSON.parse(res),function (i,elem){
              // alert(elem.rate);
              // $("#employeelist").append("<option value=''>Arique</option>");
              $('#employeelist').multiSelect('addOption', { value: elem.emp_id, text: elem.lname+", "+elem.fname, index: 0 });              

            });
            $("#employeelist").multiSelect('refresh');

          }else{

            $("#employeelist").html("");
            $("#employeelist").multiSelect('refresh');
          }
        }
      });
    }
  });
  })
</script>
<script type="text/javascript">
  $(function(){
    $("#acc_description").on('change',function(){
      $("#employeelist").html("");
      $("#employeelist").multiSelect('refresh');
    })
  })
</script>

<script type="text/javascript">
  $(function(){
    $('#select').click(function(){
      $("#employeelist").multiSelect('select_all');
    });
  })
</script>
<script type="text/javascript">
  $(function(){

    $('#deselect').click(function(){
      $("#employeelist").multiSelect('deselect_all');
    });
  })
</script>
<script type="text/javascript">

  $("#generateschedule").click(function(){

    $("#scheduleview").hide();
    var date = $("#daterangepicker-example").val();
    if(date==null||date==''){
      alert("date empty");
    }else{

      var dater = date.split(" - ");
      var fromDateRaw = dater[0];
      var fromDateRaw2 = fromDateRaw.split("/");
      var fromDate = fromDateRaw2[2]+"-"+fromDateRaw2[0]+"-"+fromDateRaw2[1];
      var toDateRaw = dater[1];
      var toDateRaw2 = toDateRaw.split("/");
      var toDate = toDateRaw2[2]+"-"+toDateRaw2[0]+"-"+toDateRaw2[1];
      var acc_description = $("#acc_description").val();
      var acc_name = $("#acc_name").val();
      var employeelist = $("#employeelist").val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/generatedate",
        data: {
          fromDate : fromDate,
          toDate : toDate
        },
        cache: false,
        success: function(sched)
        { 

          if(acc_description==''){
            alert("Please Choose Account Type");
          }else if(acc_description=='All'){
            $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>index.php/Schedule/getLogsAllDTR",
              data: {
                fromDate : fromDate,
                toDate : toDate
              },
              cache: false,
              beforeSend: function(){
                $("#loadingspinner").show();
              },
              success: function(ress)
              { 
                $("#loadingspinner").hide();

                $("#tbody").html("");
                $("#scheduleview").show();
                var res = JSON.parse(ress);
                var body = "";
                for(var i=0;i<res.length;i++){
                  body += "<tr>";
                  body += "<td class='font-bold'>"+res[i].lname+", "+res[i].fname+"</td>";
                  if(res[i].sched_time==null){
                    body += "<td class='font-red'>No Schedule</td>";
                  }else{
                    body += "<td>"+res[i].sched_time+"</td>";
                  }
                  body += "<td>"+res[i].log_in_date+"</td>";
                  var html = '<div class="bootstrap-timepicker dropdown col-md-12"><div class="input-group"><input class="timepicker-example form-control" type="text" value="'+res[i].log_in_time+'"><span class="input-group-btn"><button data-id="'+res[i].dtr_id_in+'" class="savetime btn btn-danger" type="button">Update</button></span></div></div>';
                  body += "<td><a href='#' data-html='true' class='popover-button-default   font-bold'   data-content='"+html+"' title='Change DTR logs' data-trigger='click' data-placement='left'>"+res[i].log_in_time+"</a></td>";
                  if(res[i].log_out_date==null){
                    body += "<td colspan=2 class='font-orange'>Not Logged Out</td>";
                  }else{
                    body += "<td>"+res[i].log_out_date+"</td>";
                    var html2 = '<div class="bootstrap-timepicker dropdown col-md-12"><div class="input-group"><input class="timepicker-example form-control" type="text" value="'+res[i].log_out_time+'"><span class="input-group-btn"><button data-id="'+res[i].dtr_id_out+'" class="savetime btn btn-danger" type="button">Update</button></span></div></div>';
                    body += "<td><a href='#'  data-html='true' class='popover-button-default   font-bold'   data-content='"+html2+"' title='Change DTR logs' data-trigger='click' data-placement='left'>"+res[i].log_out_time+"</a></td>"; }

                  body += "</tr>";

                }
                $("#tbody").append(body); 

                $('[data-trigger="click"]').popover();
                $('[data-trigger="click"]').on('click', function(e) {
                  $('.timepicker-example').timepicker({showSeconds:true,minuteStep:1});    
                  $('[data-trigger="click"]').not(this).popover('hide');
                  $(".savetime").on('click',function(e){
                    var newtime = $(this).parent().siblings("input").val();
                    var dtr_id = $(this).data('id');
                    swal({
                      title: 'Enter Password',
                      input: 'password',
                      showCancelButton: true,
                      confirmButtonText: 'Continue',  
                      preConfirm: function (password) {
                        return new Promise(function (resolve, reject) {
                          $.ajax({
                            type: "POST",
                            url: "<?php echo base_url();?>index.php/Employee/matchEmployeeCode",
                            data: {
                              password: password
                            },
                            cache: false,
                            success: function(res)
                            {
                             if(res=='Exists'){
                              resolve();
                            }else{
                              reject('Code Mismatch.');
                            }
                          }
                        });
                        })
                      },
                      allowOutsideClick: false
                    }).then(function () {
                     $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>index.php/Schedule/updateDtrLogs",
                      data: {
                        newtime: newtime,
                        dtr_id: dtr_id
                      },
                      cache: false,
                      success: function(res)
                      {
                        res = res.trim();
                        if(res=='Success'){
                         swal("Success!!","Successfully updated DTR logs",'success');
                       }else{
                         swal("Error!","Error updating",'error');
                       }
                       $("#generateschedule").click();
                     }
                   });
                   })
                  });
                  e.preventDefault(); 
                  return true;
                });
              }
            });
}else{
  if(acc_name=='All'){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Schedule/getLogsPerTypeDTR",
    data: {
      fromDate : fromDate,
      toDate : toDate,
      acc_description : acc_description
    },
    cache: false,
    beforeSend: function(){
      $("#loadingspinner").show();
    },
    success: function(ress)
    { 
      $("#loadingspinner").hide();

      $("#tbody").html("");
      $("#scheduleview").show();
      var res = JSON.parse(ress);
      var body = "";
      for(var i=0;i<res.length;i++){
        body += "<tr>";
        body += "<td class='font-bold'>"+res[i].lname+", "+res[i].fname+"</td>";
        if(res[i].sched_time==null){
          body += "<td class='font-red'>No Schedule</td>";
        }else{
          body += "<td>"+res[i].sched_time+"</td>";
        }
        body += "<td>"+res[i].log_in_date+"</td>";
        var html = '<div class="bootstrap-timepicker dropdown col-md-12"><div class="input-group"><input class="timepicker-example form-control" type="text" value="'+res[i].log_in_time+'"><span class="input-group-btn"><button data-id="'+res[i].dtr_id_in+'" class="savetime btn btn-danger" type="button">Update</button></span></div></div>';
        body += "<td><a href='#' data-html='true' class='popover-button-default    font-bold'   data-content='"+html+"' title='Change DTR logs' data-trigger='click' data-placement='left'>"+res[i].log_in_time+"</a></td>";
        if(res[i].log_out_date==null){
          body += "<td colspan=2 class='font-orange'>Not Logged Out</td>";
        }else{
          body += "<td>"+res[i].log_out_date+"</td>";
          var html2 = '<div class="bootstrap-timepicker dropdown col-md-12"><div class="input-group"><input class="timepicker-example form-control" type="text" value="'+res[i].log_out_time+'"><span class="input-group-btn"><button data-id="'+res[i].dtr_id_out+'" class="savetime btn btn-danger" type="button">Update</button></span></div></div>';
          body += "<td><a href='#' data-html='true' class='popover-button-default   font-bold'  data-content='"+html2+"' title='Change DTR logs' data-trigger='click' data-placement='left'>"+res[i].log_out_time+"</a></td>";
        }

        body += "</tr>";

      }
      $("#tbody").append(body); 

      $('[data-trigger="click"]').popover();
      $('[data-trigger="click"]').on('click', function(e) {
        $('.timepicker-example').timepicker({showSeconds:true,minuteStep:1});    
        $('[data-trigger="click"]').not(this).popover('hide');
        $(".savetime").on('click',function(e){
          var newtime = $(this).parent().siblings("input").val();
          var dtr_id = $(this).data('id');
          swal({
            title: 'Enter Password',
            input: 'password',
            showCancelButton: true,
            confirmButtonText: 'Continue',  
            preConfirm: function (password) {
              return new Promise(function (resolve, reject) {
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/Employee/matchEmployeeCode",
                  data: {
                    password: password
                  },
                  cache: false,
                  success: function(res)
                  {
                   if(res=='Exists'){
                    resolve();
                  }else{
                    reject('Code Mismatch.');
                  }
                }
              });
              })
            },
            allowOutsideClick: false
          }).then(function () {
           $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Schedule/updateDtrLogs",
            data: {
              newtime: newtime,
              dtr_id: dtr_id
            },
            cache: false,
            success: function(res)
            {
              res = res.trim();
              if(res=='Success'){
               swal("Success!!","Successfully updated DTR logs",'success');
             }else{
               swal("Error!","Error updating",'error');
             }
             $("#generateschedule").click();
           }
         });
         })
        });
        e.preventDefault(); 
        return true;
      });
    }
  });
}else{
  var emplist = employeelist.toString().split(",");
  employeelist = emplist.join('-');

  $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>index.php/Schedule/getLogsPerAccountEmployeeDTR",
    data: {
      fromDate : fromDate,
      toDate : toDate,
      employeelist : employeelist
    },
    cache: false,
    beforeSend: function(){
      $("#loadingspinner").show();
    },
    success: function(ress)
    { 
      $("#loadingspinner").hide();

      $("#tbody").html("");
      $("#scheduleview").show();
      var res = JSON.parse(ress);
      var body = "";
      for(var i=0;i<res.length;i++){
        body += "<tr>";
        body += "<td class='font-bold'>"+res[i].lname+", "+res[i].fname+"</td>";
        if(res[i].sched_time==null){
          body += "<td class='font-red'>No Schedule</td>";
        }else{
          body += "<td>"+res[i].sched_time+"</td>";
        }
        body += "<td>"+res[i].log_in_date+"</td>";
        var html = '<div class="bootstrap-timepicker dropdown col-md-12"><div class="input-group"><input class="timepicker-example form-control" type="text" value="'+res[i].log_in_time+'"><span class="input-group-btn"><button data-id="'+res[i].dtr_id_in+'" class="savetime btn btn-danger" type="button">Update</button></span></div></div>';
        body += "<td><a href='#' data-html='true' class='popover-button-default   font-bold'   data-content='"+html+"' title='Change DTR logs' data-trigger='click' data-placement='left'>"+res[i].log_in_time+"</a></td>";
        if(res[i].log_out_date==null){
          body += "<td colspan=2 class='font-orange'>Not Logged Out</td>";
        }else{
          body += "<td>"+res[i].log_out_date+"</td>";
          var html2 = '<div class="bootstrap-timepicker dropdown col-md-12"><div class="input-group"><input class="timepicker-example form-control" type="text" value="'+res[i].log_out_time+'"><span class="input-group-btn"><button data-id="'+res[i].dtr_id_out+'" class="savetime btn btn-danger" type="button">Update</button></span></div></div>';
          body += "<td><a href='#' data-html='true' class='popover-button-default   font-bold'  data-content='"+html2+"' title='Change DTR logs' data-trigger='click' data-placement='left'>"+res[i].log_out_time+"</a></td>";
        }

        body += "</tr>";

      }
      $("#tbody").append(body); 

      $('[data-trigger="click"]').popover();
      $('[data-trigger="click"]').on('click', function(e) {
        $('.timepicker-example').timepicker({showSeconds:true,minuteStep:1});    
        $('[data-trigger="click"]').not(this).popover('hide');
        $(".savetime").on('click',function(e){
          var newtime = $(this).parent().siblings("input").val();
          var dtr_id = $(this).data('id');
          swal({
            title: 'Enter Password',
            input: 'password',
            showCancelButton: true,
            confirmButtonText: 'Continue',  
            preConfirm: function (password) {
              return new Promise(function (resolve, reject) {
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/Employee/matchEmployeeCode",
                  data: {
                    password: password
                  },
                  cache: false,
                  success: function(res)
                  {
                   if(res=='Exists'){
                    resolve();
                  }else{
                    reject('Code Mismatch.');
                  }
                }
              });
              })
            },
            allowOutsideClick: false
          }).then(function () {
           $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Schedule/updateDtrLogs",
            data: {
              newtime: newtime,
              dtr_id: dtr_id
            },
            cache: false,
            success: function(res)
            {
              res = res.trim();
              if(res=='Success'){
               swal("Success!!","Successfully updated DTR logs",'success');
             }else{
               swal("Error!","Error updating",'error');
             }
             $("#generateschedule").click();
           }
         });
         })
        });
        e.preventDefault(); 
        return true;
      });
    }
  });

}
}

}
});

}

})
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>