<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

	<style>
		/* Loading Spinner */
		.spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
		.hidden{
			visibility: hidden;
		}
		.seen{
			visibility: visible;
		}
		td{
			font-size:14px;
		}
	</style>


	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- jQueryUI Autocomplete -->



	<!-- Favicons -->

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

	<!--<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>-->





	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

	<!-- JS Core -->

	<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>


	<!-- FORM MASKS-->
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-mask/inputmask.js"></script>
	<script type="text/javascript">
		/* Input masks */

		$(function() { "use strict";
			$(".input-mask").inputmask();
		});

	</script>


	<script type="text/javascript">
		$(window).load(function(){
			setTimeout(function() {
				$('#loading').fadeOut( 400, "linear" );
			}, 300);
		});
	</script>



	<!-- jQueryUI Datepicker -->

	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.css">-->
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

	<!-- Bootstrap Daterangepicker -->

	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.css">-->
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-responsive.js"></script>

	<!-- JQUERY CONFIRM-->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.js"></script>

	<!-- Parsley -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>
	<script type="text/javascript">

		/* Datatables responsive */

		$(document).ready(function() {
			$('#datatable-responsive').DataTable( {
				responsive: true,
				"order": [[ 5, "desc" ]]
			} );
		} );

		$(document).ready(function() {
			$('.dataTables_filter input').attr("placeholder", "Search...");
		});

	</script>
	<script type="text/javascript">

	</script>

</head>


<body>
	<div id="sb-site">


		<div id="loading">
			<div class="spinner">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>

		<div id="page-wrapper">
			<div id="page-header" class="bg-black">
				<div id="mobile-navigation">
					<button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
					<a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
				</div>
				<div id="header-logo" class="logo-bg">
					<img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
					<a id="close-sidebar" href="#" title="Close sidebar">
						<i class="glyph-icon icon-angle-left"></i>
					</a>
				</div>
				<div id='headerLeft'>
				</div>



			</div>
			<div id="page-sidebar">
				<div class="scroll-sidebar">


					<ul id="sidebar-menu">
					</ul>
				</div>
			</div>
			<div id="page-content-wrapper">
				<div id="page-content">

					<div class="container">



						<div id="page-title">
							<h2>List of Kudos</h2><hr>
							<div class="panel">
								<div class="panel-body">
									<div class="example-box-wrapper"> 
										<button class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#myModalAddMenu" onclick="viewButtons()">
											Add Kudos
										</button>
										<a href="<?php echo base_url('index.php/Kudos/kudoslistexcel');?>" class="btn btn-sm btn-info">Export</a>
										
										<br>
										<div id="myModalAddMenu" class="modal fade" role="dialog">
											<div class="modal-dialog" style="width: 500px; height: auto !important;">

												<div class="modal-content">
													<div class="modal-header" style="background: #1f2e2e; color: white;">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title float-left"><img src="<?php echo base_url();?>assets/images/add-user.png" width="40" height="40"> &nbsp; &nbsp;Choose Kudos Add Type</h4>
													</div>
													<div class="modal-body">
														<div id="twobuttons" class='text-center'>
															<button class="btn btn-s btn-primary " onclick="isSingle()" <?php $time=date("H");
															$t=date("H", strtotime($time));
															date_default_timezone_set('Asia/Manila');
															if((date("j")==15 && $t>=9) || (date("j")==16 && $t<9))
															{
																echo 'disabled';
															}?> 
															<?php date_default_timezone_set('Asia/Manila');
															$time=date("H");
															$t=date("H", strtotime($time));
															$first=new DateTime('first day of this month');
															$last=new DateTime('last day of this month');
															if((date("j")==$last && $t>=9) || (date("j")==$first && $t<9))
															{
																echo 'disabled';
															}?>> Add Single Record</button>
															<a class="btn btn-s btn-primary"  id="AddMenuMultiple" href="<?php echo base_url();?>index.php/Kudos/addMultipleView/" 
																<?php $time=date("H");
																$t=date("H", strtotime($time));
																date_default_timezone_set('Asia/Manila');
																if((date("j")==15 && $t>=9) || (date("j")==16 && $t<9))
																{
																	echo 'disabled';
																}?> 
																<?php date_default_timezone_set('Asia/Manila');
																$time=date("H");
																$t=date("H", strtotime($time));
																$first=new DateTime('first day of this month');
																$last=new DateTime('last day of this month');
																if((date("j")==$last && $t>=9) || (date("j")==$first && $t<9))
																{
																	echo 'disabled';
																}?>> Add Multiple Records</a>
															</div>
															<div id="AddUserOption">  

																<div id="DivAddSingleUser"  >  
																	<div class="example-box-wrapper">    
																		<form class="form-horizontal bordered-row" data-parsley-validate id="addForm" data-parsley-excluded="[disabled=disabled]">
																			<div class="form-group" style="height: auto; overflow: auto;">
																				<label class="col-sm-3 control-label float-left">Campaign  *</label>
																				<div class="col-sm-9">
																					<select class="form-control" id="addCampaign" required="true" data-trigger="change">
																						<option selected="selected" value="">-- Select Campaign --</option>
																						<?php 
																						foreach($acc as $row)
																						{
																							echo '<option value="'.$row->acc_id.'">'.$row->account.'</option>';
																						}
																						?>
																					</select>
																				</div>
																			</div>

																			<div class="form-group" style="height: auto; overflow: auto;">
																				<label class="col-sm-3 control-label float-left">Ambassador Name  *</label>
																				<div class="col-sm-9">
																					<select class="form-control" id="agentNames" required="true" data-trigger="change">
																						<option selected="selected" value="">-- Select Ambassador Name --</option>
																					</select>
																				</div>
																			</div>

																			<div class="form-group" style="height: auto; overflow: auto;">
																				<label class="col-sm-3 control-label float-left">Preferred Reward *</label>
																				<div class="col-sm-9">
																					<select class="form-control" id="addPrefReward" required="true" data-trigger="change">
																						<option selected="selected" value="">-- Select Preferred Reward --</option>
																						<option>Cash</option>
																						<option>Credit</option>
																					</select>
																				</div>
																			</div>

																			<div class="form-group" style="height: auto; overflow: auto;">
																				<label class="col-sm-3 control-label float-left">Kudos Type *</label>
																				<div class="col-sm-9">
																					<select class="form-control" id="addKudosType" required="true" data-trigger="change" style="text-align: center;">
																						<option selected="selected" value="">-- Select Kudos Type --</option>
																						<option value="Call">Call</option>
																						<option value="Email">Email</option>
																					</select>
																				</div>
																			</div>

																			<div class="form-group" style="display: none;height: auto; overflow: auto;"">
																				<label class="col-sm-3 control-label float-left">Supervisor *</label>
																				<div class="col-sm-9">
																					<input type="text" value="<?php echo $_SESSION['uid']; ?>" class="form-control" id="addSupervisor" >
																				</div>
																			</div>

																			<div id="append" style="display: none;">
																				<div class="form-group" style="height: auto; overflow: auto;">
																					<label class="col-sm-3 control-label float-left">Client's Name *</label>
																					<div class="col-sm-9">
																						<input type="text" class="form-control" id="addCustomerName" placeholder="Enter Client's Name" required="" data-parsley-range="[2,50]">
																					</div>
																				</div>
																				<div class="form-group" style="height: auto; overflow: auto;" id="PhoneNumberAdd">
																					<label class="col-sm-3 control-label float-left">Client's Phone Number *</label>
																					<div class="col-sm-9">
																						<input type="text" class="input-mask form-control" id="addPhoneNumber" placeholder="Enter Client's Phone Number" required="" min="10" data-inputmask="'mask':'9999999999'" >
																					</div>
																				</div>
																				<div class="form-group" style="height: auto; overflow: auto;" id="AddEmailAdd">
																					<label class="col-sm-3 control-label float-left">Client's Email Address *</label>
																					<div class="col-sm-9">
																						<input type="email" class="form-control" id="addEmailAdd" placeholder="Enter Client's Email Address" required="" >
																					</div>
																				</div>
																				<div class="form-group" style="height: auto; overflow: auto;" id="CustomerCommentAdd">
																					<label class="col-sm-3 control-label float-left">Client's Comments *</label>
																					<div class="col-sm-9">
																						<textarea rows="4" type="text" class="form-control" id="addCustomerComment" placeholder="Enter Client's Comment" style="margin: 1px 0px;" required="" data-parsley-range="[10,250]"></textarea>
																					</div>
																				</div>

																				<div class="form-group" style="height: auto; overflow: auto;" id="FileAdd">
																					<label class="col-sm-3 control-label float-left">Screenshot</label>
																					<div class="col-sm-9">

																						<input type="file" class="form-control" onchange="readURL(this);" id="addFile" name="addFile" required="true">
																					</div>
																				</div>    

																				<div class="form-group" style="height: auto; overflow: auto;" id="Picture">
																					<label class="col-sm-3 control-label float-left"></label>
																					<div class="col-sm-9">
																						<img width="350px" height="150px" src="<?php echo base_url();?>assets/images/screenshot.jpg" id="picture" name="picture">
																					</div>
																				</div>


																				<div class="form-group" style="height: auto; overflow: auto; display: none;">
																					<label class="col-sm-3 control-label float-left">Proofreading *</label>
																					<div class="col-sm-9">
																						<select class="form-control" id="addProofreading" required="true" data-trigger="change">
																							<option selected="selected">Pending</option>
																							<option>Done</option>
																						</select>
																					</div>
																				</div>
																				<div class="form-group" style="height: auto; overflow: auto; display: none;">
																					<label class="col-sm-3 control-label float-left">Kudos Card *</label>
																					<div class="col-sm-9">
																						<select class="form-control" id="addKudosCard" required="true" data-trigger="change">
																							<option selected="selected">Pending</option>
																							<option>Done</option>
																						</select>
																					</div>
																				</div>
																				<div class="form-group" style="height: auto; overflow: auto; display: none;">
																					<label class="col-sm-3 control-label float-left">Reward Status *</label>
																					<div class="col-sm-9">
																						<select class="form-control" id="addRewardStatus" required="true" data-trigger="change">
																							<option value="">-- Select Reward Status --</option>
																							<option selected="selected">Pending</option>
																							<option>Done</option>
																						</select>
																					</div>
																				</div>
																			</div>

																			<div>
																				<div align="center">
																					<button class="btn btn-s btn-warning" id='BtnBackAddMenu' class="close" data-dismiss="modal" type="reset"> Cancel</button>
																					<button class="btn btn-s btn-success" id='BtnSaveAddMenu' name="addButton" type="submit"> Save</button>
																				</div>
																			</div>
																		</form>
																	</div> 




																</div>
															</div>

														</div>

													</div>

												</div>
											</div>
											<div id="page-title">            
												<div class="example-box-wrapper">
													<table id="datatable-responsive" class="table table-striped table-bordered table-condensed responsive no-wrap" cellspacing="0" width="100%" style="width: 100%;">
														<thead>
															<tr>
																<th>Ambassador Name</th>
																<th>Campaign</th>
																<!--<th>Customer's Name</th>-->
																<!--<th><b>Description</b></th>-->
																<th>Kudos Type</th>
																<!--<th>Proofreading</Ath>-->
																<th>Kudos Card</th>
																<th>Reward Status</th>
																<th>Date Added</th>
																<th>Action</th>

															</tr>
														</thead>

														<tfoot>
															<tr>

																<th><b>Ambassador Name</b></th>
																<th><b>Campaign</b></th>
																<!--<th><b>Customer's Name</b></th>-->
																<!--<th><b>Description</b></th>-->
																<th><b>Kudos Type</b></th>
																<!--<th><b>Proofreading</b></th>-->
																<th><b>Kudos Card</b></th>
																<th><b>Reward Status</b></th>
																<th><b>Date Added</b></th>
																<th><b>Action</b></th>

															</tr>
														</tfoot>

														<tbody>
															<?php foreach($kudos as $k => $val){ ?>
															<tr>
																<td><?php echo $val->ambassador; ?></td>
																<td><?php echo $val->campaign; ?></td>
																<!--<td><?php echo $val->client_name; ?></td>-->
																<!--<td><?php echo $val->comment; ?></td>-->
																<td><?php echo $val->kudos_type; ?></td>
                                                    <!--<td><?php if($val->proofreading=='Pending'){ ?>
                                                        <p style="color: red;""><?php echo $val->proofreading; ?></p>
                                                        <?php }else{?>
                                                        <p style="color: green;""><?php echo $val->proofreading; ?></p>
                                                        <?php }?>
                                                    </td>-->
                                                    <td><?php if($val->kudos_card=='Pending'){ ?>
                                                    	<p style="color: red;""><?php echo $val->kudos_card; ?></p>
                                                    	<?php }?>
                                                    	<?php if($val->kudos_card=='In Progress'){ ?>
                                                    	<p style="color: #f29341;""><?php echo $val->kudos_card; ?></p>
                                                    	<?php }?>
                                                    	<?php if($val->kudos_card=='Done'){ ?>
                                                    	<p style="color: green;""><?php echo $val->kudos_card; ?></p>
                                                    	<?php }?>
                                                    </td>
                                                    <td><?php if($val->reward_status=='Pending'){ ?>
                                                    	<p style="color: red;""><?php echo $val->reward_status; ?></p>
                                                    	<?php }?>
                                                    	<?php if($val->reward_status=='Requested'){ ?>
                                                    	<p style="color: #f29341;""><?php echo $val->reward_status; ?></p>
                                                    	<?php }?>
                                                    	<?php if($val->reward_status=='Released'){ ?>
                                                    	<p style="color: green;""><?php echo $val->reward_status; ?></p>
                                                    	<?php }?>
                                                    	<?php if($val->reward_status=='Approved'){ ?>
                                                    	<p style="color: #41b2f4;""><?php echo $val->reward_status; ?></p>
                                                    	<?php }?>
                                                    </td>
                                                    <td><?php echo $val->date_given; ?></td>
                                                    <td style="text-align: center;">
                                                    	<button style="width: 6em;" id="updateButton" class="btn btn-sm btn-info" data-toggle="modal" data-target="#myModalUpdate<?php echo $val->kudos_id; ?>">Update</button>
                                                    </td>

                                                </tr>
                                                <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/widgets/pretty-photo/prettyphoto.css">
                                                <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/pretty-photo/prettyphoto.js"></script>
                                                <script type="text/javascript">
                                                	/* PrettyPhoto */

                                                	$(document).ready(function() {
                                                		$(".prettyphoto").prettyPhoto();
                                                	});
                                                </script>
                                                <div id="myModalUpdate<?php echo $val->kudos_id; ?>" class="modal fade" role="dialog" style="height: auto !important;">
                                                	<div class="modal-dialog" style="width: 500px; height: auto !important;">
                                                		<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>
                                                		<div class="modal-content">
                                                			<div class="modal-header" style="background: #1f2e2e; color: white;">
                                                				<button type="button" class="close" data-dismiss="modal">&times;</button>
                                                				<h4 class="modal-title float-left"><img src="<?php echo base_url();?>assets/images/update-user.png" width="40" height="40"> &nbsp; &nbsp;Update Kudos</h4>
                                                			</div>
                                                			<div class="modal-body">

                                                				<div id="AddUserOption">  

                                                					<div id="DivAddSingleUser"  >
                                                						<form class="form-horizontal" id="updateForm<?php echo $val->kudos_id; ?>"> 
                                                							<div class="example-box-wrapper">



                                                								<div class="form-group" style="height: auto; overflow: auto;">
                                                									<label class="col-sm-3 control-label float-left">Campaign</label>
                                                									<div class="col-sm-9">
                                                										<select class="form-control" id="addCampaign<?php echo $val->kudos_id; ?>" >
                                                											<option selected="selected" value="">-- Select Campaign --</option>
                                                											<?php 
                                                											foreach($acc as $row)
                                                											{
                                                												echo '<option value="'.$row->acc_id.'"
                                                												';if($val->campaign==$row->account){
                                                													echo'selected="selected"';
                                                												}
                                                												echo '>'; echo $row->account; echo'</option>';
                                                											}
                                                											?>
                                                										</select>
                                                										<span id="span_campaign<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>
                                                								</div>

                                                								<div class="form-group" style="height: auto; overflow: auto;">
                                                									<label class="col-sm-3 control-label float-left">Ambassador Name</label>
                                                									<div class="col-sm-9">
                                                										<select class="form-control" id="agentNames<?php echo $val->kudos_id; ?>" >
                                                											<option selected="" value="">-- Select Ambassador Name --</option>
                                                											<option selected="selected" value="<?php echo $val->emp_id;?>"><?php echo $val->ambassador;?></option>
                                                											<?php
                                                											foreach($name as $n){
                                                												if($val->acc_id==$n->acc_id){
                                                													?>
                                                													<option value="<?php echo $n->emp_id;?>"
                                                														<?php if($val->ambassador==$n->name){
                                                															echo 'style="display:none;"';
                                                														}?>
                                                														><?php echo $n->name;?>
                                                													</option>
                                                													<?php
                                                												}
                                                											}
                                                											?>
                                                										</select>
                                                										<span id="span_ambassadorName<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>
                                                								</div>
                                                								<div class="form-group" style="height: auto; overflow: auto;">
                                                									<label class="col-sm-3 control-label float-left">Preferred Reward</label>
                                                									<div class="col-sm-9">
                                                										<select class="form-control" id="addPrefReward<?php echo $val->kudos_id; ?>" >
                                                											<option value="">-- Select Preferred Reward --</option>
                                                											<option
                                                											<?php
                                                											if ($val->reward_type=='Cash'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Cash</option>
                                                											<option
                                                											<?php
                                                											if ($val->reward_type=='Credit'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Credit</option>
                                                										</select>
                                                										<span id="span_prefReward<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>
                                                								</div>
                                                								<div class="form-group" style="height: auto; overflow: auto;">
                                                									<label class="col-sm-3 control-label float-left">Kudos Type</label>
                                                									<div class="col-sm-9">
                                                										<select class="form-control" id="addKudosType<?php echo $val->kudos_id; ?>" >
                                                											<option value="">-- Select Preferred Reward --</option>
                                                											<option
                                                											<?php
                                                											if ($val->kudos_type=='Call'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Call</option>
                                                											<option
                                                											<?php
                                                											if ($val->kudos_type=='Email'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Email</option>
                                                										</select>
                                                										<span id="span_kudosType<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>

                                                								</div>

                                                								<div class="form-group" style="display: none; height: auto; overflow: auto;">
                                                									<label class="col-sm-3 control-label float-left">Supervisor</label>
                                                									<div class="col-sm-9">
                                                										<input type="text" value="<?php echo $val->uid; ?>" class="form-control" id="addSupervisor<?php echo $val->kudos_id; ?>" >
                                                										<span id="span_supervisor<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>
                                                								</div>



                                                								<div class="form-group" style="height: auto; overflow: auto;">
                                                									<label class="col-sm-3 control-label float-left">Client's Name</label>
                                                									<div class="col-sm-9">
                                                										<input type="text" class="form-control" id="addCustomerName<?php echo $val->kudos_id; ?>" value="<?php echo $val->client_name; ?>"  placeholder="Enter Client's Name" >
                                                										<span id="span_customerName<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>
                                                								</div>
                                                								<div class="form-group" style="height: auto; overflow: auto; <?php if($val->kudos_type=='Email'){echo 'display: none;';}?>" id="PhoneNumberAdd<?php echo $val->kudos_id; ?>">
                                                									<label class="col-sm-3 control-label float-left" >Client's Phone Number</label>
                                                									<div class="col-sm-9">
                                                										<input class="input-mask form-control" type="text" value="<?php echo $val->phone_number; ?>" class="form-control" id="addPhoneNumber<?php echo $val->kudos_id; ?>" data-inputmask="'mask':'9999999999'">
                                                										<span id="span_customerNumber<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>
                                                								</div>
                                                								<div class="form-group" style="height: auto; overflow: auto; <?php if($val->kudos_type=='Email'){echo 'display: none;';}?>" id="CustomerCommentAdd<?php echo $val->kudos_id; ?>">
                                                									<label class="col-sm-3 control-label float-left">Client's Comments</label>
                                                									<div class="col-sm-9">
                                                										<textarea rows="4" cols="50" type="text" class="form-control" id="addCustomerComment<?php echo $val->kudos_id; ?>" style="margin: 1px 0px;"  placeholder="Enter Client's Comment" ><?php echo $val->comment; ?></textarea>
                                                										<span id="span_customerComment<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>
                                                								</div>
                                                								<div class="form-group" style="height: auto; overflow: auto; <?php if($val->kudos_type=='Call'){echo 'display: none;';}?>" id="AddEmailAdd<?php echo $val->kudos_id; ?>">
                                                									<label class="col-sm-3 control-label float-left">Client's Email</label>
                                                									<div class="col-sm-9">
                                                										<input type="email" class="form-control" id="addEmailAdd<?php echo $val->kudos_id; ?>" value="<?php echo $val->client_email; ?>" placeholder="Enter Client's Email" >
                                                										<span id="span_customerEmail<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>
                                                								</div>

                                                								<div class="form-group" style="height: auto; overflow: auto; display: none;">
                                                									<label class="col-sm-3 control-label float-left">Proofreading</label>
                                                									<div class="col-sm-9">
                                                										<select class="form-control" id="addProofreading<?php echo $val->kudos_id; ?>">
                                                											<option value="">-- Select Proofreading Status --</option>
                                                											<option
                                                											<?php
                                                											if ($val->proofreading=='Pending'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Pending</option>
                                                											<option
                                                											<?php
                                                											if ($val->proofreading=='In Progress'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>In Progress</option>
                                                											<option
                                                											<?php
                                                											if ($val->proofreading=='Done'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Done</option>
                                                										</select>
                                                									</div>
                                                								</div>
                                                								<div class="form-group" style="height: auto; overflow: auto; display: none;">
                                                									<label class="col-sm-3 control-label float-left">Kudos Card</label>
                                                									<div class="col-sm-9">
                                                										<select class="form-control" id="addKudosCard<?php echo $val->kudos_id; ?>">
                                                											<option value="">-- Select Kudos Card Status --</option>
                                                											<option
                                                											<?php
                                                											if ($val->kudos_card=='Pending'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Pending</option>
                                                											<option
                                                											<?php
                                                											if ($val->kudos_card=='In Progress'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>In Progress</option>
                                                											<option
                                                											<?php
                                                											if ($val->kudos_card=='Done'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Done</option>
                                                										</select>
                                                									</div>
                                                								</div>
                                                								<div class="form-group" style="height: auto; overflow: auto; display: none;">
                                                									<label class="col-sm-3 control-label float-left">Reward Status</label>
                                                									<div class="col-sm-9">
                                                										<select class="form-control" id="addRewardStatus<?php echo $val->kudos_id; ?>">
                                                											<option value="">-- Select Reward Status --</option>
                                                											<option
                                                											<?php
                                                											if ($val->reward_status=='Pending'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Pending</option>
                                                											<option
                                                											<?php
                                                											if ($val->reward_status=='In Progress'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>In Progress</option>
                                                											<option
                                                											<?php
                                                											if ($val->reward_status=='Done'){
                                                												echo 'selected="selected"';
                                                											} 
                                                											?>
                                                											>Done</option>
                                                										</select>
                                                									</div>
                                                								</div>
                                                								<div class="form-group" style="height: auto; overflow: auto; <?php if($val->kudos_type=='Call'){echo 'display: none;';}?>" id="FileAdd<?php echo $val->kudos_id; ?>">
                                                									<label class="col-sm-3 control-label float-left">File</label>
                                                									<div class="col-sm-9">
                                                										<input type="file" class="form-control" id="addFile<?php echo $val->kudos_id; ?>" value="<?php echo $val->file; ?>" >
                                                										<span id="span_customerFile<?php echo $val->kudos_id; ?>" style="color: red;"></span>
                                                									</div>
                                                								</div>
                                                								<?php if($val->file) {?>
                                                								<div class="form-group" style="height: auto; overflow: auto;" id="screenshot<?php echo $val->kudos_id; ?>">
                                                									<label class="col-sm-3 control-label float-left">Screenshot</label>
                                                									<div class="col-sm-9">
                                                										<a href="<?php echo base_url();?><?php echo $val->file;?>" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="">
                                                											<img src="<?php echo base_url();?><?php echo $val->file;?>" style="width: 325px; height: 125px;">
                                                										</a>
                                                									</div>
                                                								</div>
                                                								<?php }?>


                                                								<div align="center">
                                                									<button class="btn btn-s btn-warning" id='BtnBackAddMenu2' class="close" data-dismiss="modal"> Cancel</button>

                                                									<button class="btn btn-s btn-success" id="myModalUpdatesCall<?php echo $val->kudos_id; ?>" style="display:none;">Update</button>

                                                									<button class="btn btn-s btn-success" id="myModalUpdatesEmail<?php echo $val->kudos_id; ?>"" style="display:none;">Update</button>


                                                								</div>

                                                							</div>
                                                						</form>
                                                					</div> 




                                                				</div>

                                                			</div>

                                                		</div>

                                                	</div>
                                                </div>
                                                <script>
                                                	$(function(){
                                                		$("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false,});
                                                		$('input[type="file"]').attr("disabled", false);
                                                		$('input[type="email"]').attr("disabled", false);
                                                    /*if($('#addKudosType<?php echo $val->kudos_id; ?>').val()=='Call'){
                                                        $('#c_email').hide();
                                                        $('#c_file').hide();
                                                        //$('#c_pnumber').show();
                                                        //$('#c_comments').show();
                                                    }
                                                    if($('#addKudosType<?php echo $val->kudos_id; ?>').val()=='Email'){
                                                        //$('#c_pnumber').hide();
                                                        //$('#c_comments').hide();
                                                        //$('#c_email').show();
                                                        //$('#c_file').show();
                                                    }*/

                                                    //Hides the Email Button if it's a call type
                                                    if($("#addKudosType<?php echo $val->kudos_id; ?>").val()=="Call"){
                                                    	$("#myModalUpdatesCall<?php echo $val->kudos_id; ?>").show()
                                                    	$("#myModalUpdatesEmail<?php echo $val->kudos_id; ?>").hide()
                                                    }

                                                    //Hides the Call Button if it's an email type
                                                    if($("#addKudosType<?php echo $val->kudos_id; ?>").val()=="Email"){
                                                    	$("#myModalUpdatesCall<?php echo $val->kudos_id; ?>").hide()
                                                    	$("#myModalUpdatesEmail<?php echo $val->kudos_id; ?>").show()
                                                    }

                                                    
                                                    //Email Type validation and ajax for sending data
                                                    $("#myModalUpdatesEmail<?php echo $val->kudos_id; ?>").click(function(event){
                                                        //Checks if one field is empty on the Email type
                                                        if($("#agentNames<?php echo $val->kudos_id; ?>").val()=="" || $("#addCampaign<?php echo $val->kudos_id; ?>").val()=="" || $("#addCustomerName<?php echo $val->kudos_id; ?>").val()=="" ||  $("#addEmailAdd<?php echo $val->kudos_id; ?>").val()=="" || $("#addPrefReward<?php echo $val->kudos_id; ?>").val()=="" || $("#addKudosType<?php echo $val->kudos_id; ?>").val()==""){

                                                        	if($("#addCampaign<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addCampaign<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_campaign<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	if($("#agentNames<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#agentNames<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_ambassadorName<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	if($("#addPrefReward<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addPrefReward<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_prefReward<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	if($("#addKudosType<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addKudosType<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_kudosType").text("This is required.");
                                                        	}

                                                        	if($("#addCustomerName<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addCustomerName<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_customerName<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	if($("#addEmailAdd<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addEmailAdd<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_customerEmail<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	event.preventDefault();

                                                        } else {
                                                        	var data = new FormData();
                                                        	$.each($("#addFile<?php echo $val->kudos_id; ?>")[0].files,function(i,file){
                                                        		data.append("addFile<?php echo $val->kudos_id; ?>",file);
                                                        	});
                                                        	data.append("emp_id",$('#agentNames<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("campaign",$('#addCampaign<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("k_type",$('#addKudosType<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("c_name",$('#addCustomerName<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("p_number",$('#addPhoneNumber<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("e_add",$('#addEmailAdd<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("comment",$('#addCustomerComment<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("supervisor",$('#addSupervisor<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("file","<?php echo $val->file; ?>");
                                                        	data.append("p_reward",$('#addPrefReward<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("pfrd",$('#addProofreading<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("k_card",$('#addKudosCard<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("r_status",$('#addRewardStatus<?php echo $val->kudos_id; ?>').val());
                                                        	data.append("k_id", <?php echo $val->kudos_id; ?>)



                                                        	$.ajax({
                                                        		type: "POST",
                                                        		url: "<?php echo base_url(); ?>index.php/Kudos/updateKudosOPT_withPic/",
                                                        		data: data,
                                                        		cache: false,
                                                        		processData:false,
                                                        		contentType:false,
                                                        		success: function(response)
                                                        		{
                                                                    //alert($("#addFile").val())
                                                                    if(response=="no"){
                                                                    	alert("File type not allowed.");
                                                                    } else {
                                                                    	alert("Successfully updated.");
                                                                    	location.reload();
                                                                    }
                                                                }
                                                            }); 
                                                            /*
                                                            var emp_id = $("#agentNames<?php echo $val->kudos_id; ?>").val();
                                                            var campaign = $("#addCampaign<?php echo $val->kudos_id; ?>").val();
                                                            var c_name = $("#addCustomerName<?php echo $val->kudos_id; ?>").val();
                                                            var p_number = $("#addPhoneNumber<?php echo $val->kudos_id; ?>").val();
                                                            var e_add = $("#addEmailAdd<?php echo $val->kudos_id; ?>").val();
                                                            var comment = $("#addCustomerComment<?php echo $val->kudos_id; ?>").val();
                                                            var supervisor = $("#addSupervisor<?php echo $val->kudos_id; ?>").val();
                                                            var k_type = $("#addKudosType<?php echo $val->kudos_id; ?>").val();
                                                            var file = $("#addFile<?php echo $val->kudos_id; ?>").val();
                                                            var p_reward = $("#addPrefReward<?php echo $val->kudos_id; ?>").val();
                                                            var pfrd = $("#addProofreading<?php echo $val->kudos_id; ?>").val();
                                                            var k_card = $("#addKudosCard<?php echo $val->kudos_id; ?>").val();
                                                            var r_status = $("#addRewardStatus<?php echo $val->kudos_id; ?>").val();
                                                            var k_id = <?php echo $val->kudos_id; ?>;

                                                            dataString = "emp_id="+emp_id+"&campaign="+campaign+"&c_name="+c_name+"&p_number="+p_number+"&e_add="+e_add+"&comment="+comment+"&supervisor="+supervisor+"&k_type="+k_type+"&file="+file+"&p_reward="+p_reward+"&pfrd="+pfrd+"&k_card="+k_card+"&r_status="+r_status+"&k_id="+k_id;

                                                            $.ajax({
                                                                type: "POST",
                                                                url: "<?php echo base_url(); ?>index.php/Kudos/updateKudos/",
                                                                data: dataString,
                                                                cache: false,
                                                                success: function(html)
                                                                {

                                                                    alert("Succesfully Updated!");
                                                                    location.reload();
                                                                }
                                                            });*/
                                                            
                                                            
                                                            
                                                        }
                                                    }); 

                                                    //Call Type validation and ajax for sending data
                                                    $('#myModalUpdatesCall<?php echo $val->kudos_id; ?>').click(function(event){
                                                        //Checks if one field is empty on the Email type
                                                        if($("#agentNames<?php echo $val->kudos_id; ?>").val()=="" || $("#addCampaign<?php echo $val->kudos_id; ?>").val()=="" || $("#addCustomerName<?php echo $val->kudos_id; ?>").val()=="" ||  $("#addPhoneNumber<?php echo $val->kudos_id; ?>").val()=="" || $("#addCustomerComment<?php echo $val->kudos_id; ?>").val()=="" || $("#addPrefReward<?php echo $val->kudos_id; ?>").val()=="" || $("#addKudosType<?php echo $val->kudos_id; ?>").val()==""){

                                                        	if($("#addCampaign<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addCampaign<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_campaign<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	if($("#agentNames<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#agentNames<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_ambassadorName<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	if($("#addPrefReward<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addPrefReward<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_prefReward<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	if($("#addKudosType<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addKudosType<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_kudosType").text("This is required.");
                                                        	}

                                                        	if($("#addCustomerName<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addCustomerName<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_customerName<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	if($("#addPhoneNumber<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addPhoneNumber<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_customerNumber<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}

                                                        	if($("#addCustomerComment<?php echo $val->kudos_id; ?>").val()==""){
                                                        		$("#addCustomerComment<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        		$("#span_customerComment<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	}
                                                        	event.preventDefault();

                                                        } else {
                                                        	var emp_id = $("#agentNames<?php echo $val->kudos_id; ?>").val();
                                                        	var campaign = $("#addCampaign<?php echo $val->kudos_id; ?>").val();
                                                        	var c_name = $("#addCustomerName<?php echo $val->kudos_id; ?>").val();
                                                        	var p_number = $("#addPhoneNumber<?php echo $val->kudos_id; ?>").val();
                                                        	var e_add = $("#addEmailAdd<?php echo $val->kudos_id; ?>").val();
                                                        	var comment = $("#addCustomerComment<?php echo $val->kudos_id; ?>").val();
                                                        	var supervisor = $("#addSupervisor<?php echo $val->kudos_id; ?>").val();
                                                        	var k_type = $("#addKudosType<?php echo $val->kudos_id; ?>").val();
                                                        	var file = $("#addFile<?php echo $val->kudos_id; ?>").val();
                                                        	var p_reward = $("#addPrefReward<?php echo $val->kudos_id; ?>").val();
                                                        	var pfrd = $("#addProofreading<?php echo $val->kudos_id; ?>").val();
                                                        	var k_card = $("#addKudosCard<?php echo $val->kudos_id; ?>").val();
                                                        	var r_status = $("#addRewardStatus<?php echo $val->kudos_id; ?>").val();
                                                        	var k_id = <?php echo $val->kudos_id; ?>;

                                                        	dataString = "emp_id="+emp_id+"&campaign="+campaign+"&c_name="+c_name+"&p_number="+p_number+"&e_add="+e_add+"&comment="+comment+"&supervisor="+supervisor+"&k_type="+k_type+"&file="+file+"&p_reward="+p_reward+"&pfrd="+pfrd+"&k_card="+k_card+"&r_status="+r_status+"&k_id="+k_id;

                                                        	$.ajax({
                                                        		type: "POST",
                                                        		url: "<?php echo base_url(); ?>index.php/Kudos/updateKudosNoPic/",
                                                        		data: dataString,
                                                        		cache: false,
                                                        		success: function(html)
                                                        		{

                                                        			alert("Succesfully Updated!");
                                                        			location.reload();
                                                        		}
                                                        	});



                                                        }
                                                    }); 

                                                    //Campaign dropdown
                                                    $('#addCampaign<?php echo $val->kudos_id; ?>').on('change', function(){
                                                        //Populate ambassador names dropdown
                                                        $.ajax({
                                                        	type : 'POST',
                                                        	data : 'campaign_id='+ $('#addCampaign<?php echo $val->kudos_id; ?>').val(),
                                                        	url : "<?php echo base_url(); ?>index.php/Kudos/getAgents/",
                                                        	success : function(data){
                                                        		$('#agentNames<?php echo $val->kudos_id; ?>').html(data);
                                                        	}
                                                        });
                                                        if($("#addCampaign<?php echo $val->kudos_id; ?>").val()==""){
                                                        	$("#addCampaign<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        	$("#span_campaign<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        	$("#agentNames<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                        	$("#span_ambassadorName<?php echo $val->kudos_id; ?>").text("This is required.");
                                                        } else {
                                                        	$("#addCampaign<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");   
                                                        	$("#span_campaign<?php echo $val->kudos_id; ?>").text("");
                                                        }
                                                    });

                                                    //Agent Names dropdown
                                                    $('#agentNames<?php echo $val->kudos_id; ?>').on('change', function(){
                                                    	if($("#agentNames<?php echo $val->kudos_id; ?>").val()==""){
                                                    		$("#agentNames<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                    		$("#span_ambassadorName<?php echo $val->kudos_id; ?>").text("This is required.");
                                                    	} else {
                                                    		$("#agentNames<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
                                                    		$("#span_ambassadorName<?php echo $val->kudos_id; ?>").text("");
                                                    	}
                                                    });

                                                    //Preferred Reward dropdown
                                                    $('#addPrefReward<?php echo $val->kudos_id; ?>').on('change', function(){
                                                    	if($("#addPrefReward<?php echo $val->kudos_id; ?>").val()==""){
                                                    		$("#addPrefReward<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                    		$("#span_prefReward<?php echo $val->kudos_id; ?>").text("This is required.");
                                                    	} else {
                                                    		$("#addPrefReward<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
                                                    		$("#span_prefReward<?php echo $val->kudos_id; ?>").text("");
                                                    	}
                                                    });


                                                    $('#addKudosType<?php echo $val->kudos_id; ?>').on('change', function(){
                                                    	if(this.value=='Call'){
                                                    		$('#CustomerCommentAdd<?php echo $val->kudos_id; ?>').show();
                                                    		$('#PhoneNumberAdd<?php echo $val->kudos_id; ?>').show();
                                                    		$('#AddEmailAdd<?php echo $val->kudos_id; ?>').hide();
                                                    		$('#FileAdd<?php echo $val->kudos_id; ?>').hide();
                                                    		$('#addCustomerComment<?php echo $val->kudos_id; ?>').removeClass('hidden');
                                                    		$('#CustomerCommentAdd<?php echo $val->kudos_id; ?>').removeClass('hidden');
                                                    		$('#addCustomerComment<?php echo $val->kudos_id; ?>').addClass('seen');
                                                    		$('#CustomerCommentAdd<?php echo $val->kudos_id; ?>').addClass('seen');
                                                    		$('input[type="email"]').attr("disabled", true);
                                                    		$('#addPhoneNumber<?php echo $val->kudos_id; ?>').removeClass('hidden');
                                                    		$('#PhoneNumberAdd<?php echo $val->kudos_id; ?>').removeClass('hidden');
                                                    		$('#addPhoneNumber<?php echo $val->kudos_id; ?>').addClass('seen');
                                                    		$('#PhoneNumberAdd<?php echo $val->kudos_id; ?>').addClass('seen');
                                                    		$('#addFile<?php echo $val->kudos_id; ?>').val("");
                                                    		$('#addEmailAdd<?php echo $val->kudos_id; ?>').val("");
                                                    		$("#addKudosType<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
                                                    		$("#span_kudosType<?php echo $val->kudos_id; ?>").text("");
                                                    		$("#myModalUpdatesCall<?php echo $val->kudos_id; ?>").show();
                                                    		$("#myModalUpdatesEmail<?php echo $val->kudos_id; ?>").hide();
                                                    	}
                                                    	if(this.value=='Email'){
                                                    		$('#CustomerCommentAdd<?php echo $val->kudos_id; ?>').hide();
                                                    		$('#PhoneNumberAdd<?php echo $val->kudos_id; ?>').hide();
                                                    		$('#AddEmailAdd<?php echo $val->kudos_id; ?>').show();
                                                    		$('#FileAdd<?php echo $val->kudos_id; ?>').show();
                                                    		$('#addFile<?php echo $val->kudos_id; ?>').removeClass('hidden');
                                                    		$('#FileAdd<?php echo $val->kudos_id; ?>').removeClass('hidden');
                                                    		$('#addFile<?php echo $val->kudos_id; ?>').addClass('seen');
                                                    		$('#FileAdd<?php echo $val->kudos_id; ?>').addClass('seen');
                                                    		$('input[type="email"]').attr("disabled", false);
                                                    		$('#addEmailAdd<?php echo $val->kudos_id; ?>').removeClass('hidden');
                                                    		$('#AddEmailAdd<?php echo $val->kudos_id; ?>').removeClass('hidden');
                                                    		$('#addEmailAdd<?php echo $val->kudos_id; ?>').addClass('seen');
                                                    		$('#AddEmailAdd<?php echo $val->kudos_id; ?>').addClass('seen');
                                                    		$('#addPhoneNumber<?php echo $val->kudos_id; ?>').val("");
                                                    		$('#addCustomerComment<?php echo $val->kudos_id; ?>').val("");
                                                    		$("#addKudosType<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
                                                    		$("#span_kudosType<?php echo $val->kudos_id; ?>").text("");
                                                    		$("#myModalUpdatesCall<?php echo $val->kudos_id; ?>").hide();
                                                    		$("#myModalUpdatesEmail<?php echo $val->kudos_id; ?>").show();

                                                    	}
                                                    	if($("#addKudosType<?php echo $val->kudos_id; ?>").val()==""){
                                                    		$("#addKudosType<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
                                                    		$("#span_kudosType<?php echo $val->kudos_id; ?>").text("This is required.");
                                                    	} 
                                                    });
$('#addCustomerName<?php echo $val->kudos_id; ?>').on('keyup', function() {
	if($("#addCustomerName<?php echo $val->kudos_id; ?>").val()==""){
		$("#addCustomerName<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
		$("#span_customerName<?php echo $val->kudos_id; ?>").text("This is required.");
	} else {
		$("#addCustomerName<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
		$("#span_customerName<?php echo $val->kudos_id; ?>").text("");
	}
});
$('#addPhoneNumber<?php echo $val->kudos_id; ?>').on('keyup', function() {
	if($("#addPhoneNumber<?php echo $val->kudos_id; ?>").val()==""){
		$("#addPhoneNumber<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
		$("#span_customerNumber<?php echo $val->kudos_id; ?>").text("This is required.");
	} else {
		$("#addPhoneNumber<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
		$("#span_customerNumber<?php echo $val->kudos_id; ?>").text("");
	}
});

$('#addCustomerComment<?php echo $val->kudos_id; ?>').on('keyup', function() {
	if($("#addCustomerComment<?php echo $val->kudos_id; ?>").val()==""){
		$("#addCustomerComment<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
		$("#span_customerComment<?php echo $val->kudos_id; ?>").text("This is required.");
	} else {
		$("#addCustomerComment<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
		$("#span_customerComment<?php echo $val->kudos_id; ?>").text("");
	}
});

$('#addEmailAdd<?php echo $val->kudos_id; ?>').on('keyup', function() {
	if($("#addEmailAdd<?php echo $val->kudos_id; ?>").val()==""){
		$("#addEmailAdd<?php echo $val->kudos_id; ?>").css("border", "1px solid red");
		$("#span_customerEmail<?php echo $val->kudos_id; ?>").text("This is required.");
	} else {
		$("#addEmailAdd<?php echo $val->kudos_id; ?>").css("border", "1px solid #66ff66");
		$("#span_customerEmail<?php echo $val->kudos_id; ?>").text("");
	}
});



});
</script>
<?php }?>

</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



</div>
</div>
</div>
</body>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/autocomplete/menu.js"></script>
<script>
	$(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
			cache: false,
			success: function(html)
			{

				$('#headerLeft').html(html);
           //alert(html);
       }
   });   
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
			cache: false,
			success: function(html)
			{

				$('#sidebar-menu').html(html);
         // alert(html);
     }
 });
	});   
</script>
<script>

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#picture').attr('src', e.target.result)
                        //.height('60%');
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
            $(function(){



            	$('#addKudosType').on('change', function(){
            		if(this.value=='Call'){
            			$('#append').show();
                //hide file and email add
                $('#addFile').addClass('hidden');
                $('#FileAdd').addClass('hidden');
                $('#addEmailAdd').addClass('hidden');
                $('#AddEmailAdd').addClass('hidden');
                //disable file and email add
                $('input[type="file"]').attr("disabled", true);
                $('input[type="email"]').attr("disabled", true);
                //remove comment class hidden and add seen
                $('#addCustomerComment').removeClass('hidden');
                $('#CustomerCommentAdd').removeClass('hidden');
                $('#addCustomerComment').addClass('seen');
                $('#CustomerCommentAdd').addClass('seen');
                //remove disabled attr for comment
                $('#addCustomerComment').attr("disabled", false);
                //remove hidden class in phone number and add seen
                $('#addPhoneNumber').removeClass('hidden');
                $('#PhoneNumberAdd').removeClass('hidden');
                $('#addPhoneNumber').addClass('seen');
                $('#PhoneNumberAdd').addClass('seen');
                $('#addPhoneNumber').attr("disabled", false);
                $('#Picture').removeClass('seen');
                $('#picture').removeClass('seen');
                $('#Picture').addClass('hidden');
                $('#picture').addClass('hidden');
            }
            if(this.value=='Email'){
            	$('#append').show();
            	$('input[type="file"]').attr("disabled", false);
            	$('#addFile').removeClass('hidden');
            	$('#FileAdd').removeClass('hidden');
            	$('#addFile').addClass('seen');
            	$('#FileAdd').addClass('seen');
            	$('input[type="email"]').attr("disabled", false);
            	$('#addEmailAdd').removeClass('hidden');
            	$('#AddEmailAdd').removeClass('hidden');
            	$('#addEmailAdd').addClass('seen');
            	$('#AddEmailAdd').addClass('seen');
            	$('#addCustomerComment').attr("disabled", "disabled");
            	$('#addCustomerComment').addClass('hidden');
            	$('#CustomerCommentAdd').addClass('hidden');
            	$('#addPhoneNumber').attr("disabled", true);
            	$('#addPhoneNumber').addClass('hidden');
            	$('#PhoneNumberAdd').addClass('hidden');
            	$('#Picture').removeClass('hidden');
            	$('#picture').removeClass('hidden');
            	$('#Picture').addClass('seen');
            	$('#picture').addClass('seen');

            }
            if(this.value==''){
            	$('#append').hide();
            }
        });

            	$('#addCampaign').on('change', function(){
            		$.ajax({
            			type : 'POST',
            			data : 'campaign_id='+ $('#addCampaign').val(),
            			url : "<?php echo base_url(); ?>index.php/Kudos/getAgents/",
                //dataType: 'json',
                success : function(data){
                	$('#agentNames').html(data);
                    //JSON.stringify(data);
                  /*  $.each(data,function(key,value){
                        alert(value.emp_id);
                        $('#agentNames').append('<option value="'+value.emp_id+'">'+value.fullname+'</option>');
                    });*/

                }
            });
            	});


            	$('#addForm').submit(function(event){ 
            		$.confirm({
            			title: 'ADD KUDOS!',
            			content: 'Add Kudos?',
            			buttons: {
            				confirm: function () {
            					var data = new FormData();
            					$.each($("#addFile")[0].files,function(i,file){
            						data.append("addFile",file);
            					});
                        //data.append('file', $('#addFile')[0].files[0]);
                        data.append("emp_id",$('#agentNames').val());
                        data.append("campaign",$('#addCampaign').val());
                        data.append("k_type",$('#addKudosType').val());
                        data.append("c_name",$('#addCustomerName').val());
                        data.append("p_number",$('#addPhoneNumber').val());
                        data.append("e_add",$('#addEmailAdd').val());
                        data.append("comment",$('#addCustomerComment').val());
                        data.append("supervisor",$('#addSupervisor').val());
                        //data.append("file",$('#addKudosType').val());
                        data.append("p_reward",$('#addPrefReward').val());
                        data.append("pfrd",$('#addProofreading').val());
                        data.append("k_card",$('#addKudosCard').val());
                        data.append("r_status",$('#addRewardStatus').val());

                        

                        $.ajax({
                        	type: "POST",
                        	url: "<?php echo base_url(); ?>index.php/Kudos/addKudos/",
                        	data: data,
                        	cache: false,
                        	processData:false,
                        	contentType:false,
                        	success: function(response)
                        	{
                                //alert($("#addFile").val())
                                if(response=="File type is not allowed."){
                                	$.alert("File type is not allowed.");
                                }
                                if(response=="Successfully added!"){
                                	$('#addForm').find(':submit').attr('disabled','disabled');
                                	$.confirm({
                                		title: 'SUCCESS!',
                                		content: 'Successfully added!',
                                		buttons: {
                                			ok: function(){
                                				$('#addForm').find(':submit').attr('disabled','disabled');
                                				
                                			}
                                		}
                                	});
                                    /*
                                    alert("Successfully added!");
                                    $('#addForm').find(':submit').attr('disabled','disabled');
                                    location.reload();
                                    */

                                }
                                window.location = "<?php echo base_url();?>index.php/Kudos";
                            }
                        });  
                    },
                    cancel: function () {
                    },
                }
            });

            		event.preventDefault();
            	});



            	$('#BtnBackAddMenu').click(function(){   
            		$("#addAmbassadorName").val("");
            		$("#addCampaign").val("");
            		$("#addCustomerName").val("");
            		$("#addCustomerComment").val("");
            		$("#addFile").val("");
            		$("#addPrefReward").val("");
            		$("#addProofreading").val("");
            		$("#addKudosCard").val("");
            		$("#addRewardStatus").val("");

            	});






            });
        </script>
        <script>
        	$(function(){
        		"use strict";
        		var obj = JSON.parse(<?php echo "'".json_encode($name)."'"; ?>);
        		var all = obj.map(function (value) {
        			return value.name;
        		});
        		$("#addAmbassadorName2").autocomplete({source: all});
        	});
        </script>
        <script type="text/javascript">
        	function viewButtons(){
        		$('#twobuttons').show();
        		$('#AddUserOption').hide();
        	}
        	function isSingle(){
        		$('#AddUserOption').fadeIn(800);	
        		// $('#twobuttons').hide();
        		$('#twobuttons').fadeOut(500);
        	}
        </script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>

        <!-- Parsley -->
        <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/parsley/parsley.js"></script>

        <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
        </html>