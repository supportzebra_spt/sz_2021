<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>SupportZebra Payslip</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
   <div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header">
   
   
   </div>
   
 <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
 <?php 
  $timeTotal = 0;
 $timeTotalOTBCT = 0;
 $ndRatexBCT2 = 0;
 $HPval2 = 0;
 $SSS = 0;
 $PhilHealthCompute = 0;
 $PagIbig = 0;
 $daily = 0;
 $hourly = 0;
 $TotalAmountHoliday = 0;
 		$HPComputeHolidayLogin =0;
		$NDComputeHolidayLogin  =0;
		$NDComputeHolidayLogout =0;
		$HPComputeHolidayLogout =0;

  //echo json_encode(($employee));
   end($employee); 
   $ID = key($employee); 
 
 foreach($employee as $key => $val){
	if($ID == $key){
	foreach($val as $key1 => $val1){
					$Mrate = $val1['rate'];
					$daily = (($Mrate*12)/261);
					$hourly = $daily/8; 
		$timeTotalBCT = ($val1['bct']=='Q') ? 0.21 : 0.00;
		 
			if($val1['nd']!=null){
				 $ndExplode = explode("-",$val1['nd']);
				$ndRate = $ndExplode[0]; 
				$ndRate += $ndExplode[1]/60; 
				$ndRatexBCT = ($ndRate ==7)? $ndRate : $ndRate+$timeTotalBCT; 
				$HPval = $ndRate;
			 }else{
				$ndRatexBCT = 0.00;
				$HPval = 0.00;
			 }
			 $NDvalBCT = ($val1['ndRemark']=='Y' && $timeTotalBCT != 0.00) ?  "0.21" : "0.00" ;
			 
			 $timeTotalVal = ($val1['total']>=8.0) ? "8.0" : $val1['total']-(1.0);
 	$timeTotal +=$timeTotalVal;
	 // $timeTotalOTBCT += $timeTotalBCT;
	  // $timeTotalOTBCT += ($val1['HolidayType1'] === 'Regular') ?  (($timeTotalBCT)*2) : (($timeTotalBCT)*.30)+ $timeTotalBCT; 
	  if(($val1['HolidayType1'] === 'Regular')){
		 $timeTotalOTBCT +=  (($timeTotalBCT)*2);
	  }elseif($val1['HolidayType1'] === 'Special'){
		  $timeTotalOTBCT += (($timeTotalBCT)*.30)+ $timeTotalBCT;
	  }else{
		  $timeTotalOTBCT += $timeTotalBCT;
	  }
	//Total number of time for BCT 0.21 + (Holiday then it will be times two)
 
 
 //echo $timeTotalOTBCT."<br>";
	 // $ndRatexBCT2 += $ndRatexBCT;
	 // $HPval2 += $HPval;
		$ndRatexBCT2+= ($ndRatexBCT)+ ($val1['NDComputeHolidayLogin'])+ ($val1['NDComputeHolidayLogout']); // Night Diff + ( [Special/Regular] Holiday ND)
		$HPval2 += $HPval+ $val1['HPComputeHolidayLogin']+ $val1['HPComputeHolidayLogout']; // Hazard Pay + ( [Special/Regular] Holiday HP)
		// echo $HPval."<br>";
	 $SSS = $val1['SSS']/2;
	$PhilHealthCompute = $val1['PhilHealthCompute']/2;
	$PagIbig = ($val1['rate']*.02)/2;
	$pos_name =$val1['pos_name'];
	$hpRate =$val1['hpRate'];
	$fname =$val1['fname'];
	$email =$val1['email'];
	$adjustSalary = ($val1['adjustSalary']!=null) ? explode(",",$val1['adjustSalary']) : 0;
	$deductSalary = ($val1['deductSalary']!=null) ? explode(",",$val1['deductSalary']) : 0;
	$BIRCompute =$val1['BIRCompute'];
	$percent =$val1['BIRPercent'];
	$salarybase =$val1['BIRSalaryBase'];
	$BIRDescription =$val1['BIRDescription'];
	 $TotalAmountHoliday +=$val1['TotalAmountHoliday'];
	$BIRTotalCompute = (($val1['rate2']-$salarybase)*$percent) + ($BIRCompute);
		$NDComputeHolidayLogin += ($val1['NDComputeHolidayLogin'])*($hourly*.15);
		$NDComputeHolidayLogout += ($val1['NDComputeHolidayLogout'])*($hourly*.15);
		// $HPComputeHolidayLogout += ($val1['HPComputeHolidayLogout'])*($hourly*$hpRate);
		// $HPComputeHolidayLogin += ($val1['HPComputeHolidayLogin'])*($hourly*$hpRate);
		$HPComputeHolidayLogout += ($val1['HPComputeHolidayLogout']);
		$HPComputeHolidayLogin += ($val1['HPComputeHolidayLogin']) ;

	}
	}
 }
	$TotalBCTxHourly = $timeTotalOTBCT * $hourly;
 	$TotalNet =  ($SSS)+($PhilHealthCompute)+($PagIbig)+($BIRTotalCompute);
 	$NightDiff = ($ndRatexBCT2*($hourly*.15));
   //echo $email;
	$ssTotal =0;
	$netTotal =0;
	$finaltotalgross =0;
	 $TotalNetPay =0;
	  $HPHoliday=0;
	$NDHoliday=0;
	$HPHoliday=($HPComputeHolidayLogin+$HPComputeHolidayLogout)*($hourly*$hpRate);
	// echo "(".$HPComputeHolidayLogin."+".$HPComputeHolidayLogout.")*(".$hourly."*".$hpRate.")";
	$NDHoliday=$NDComputeHolidayLogin+$NDComputeHolidayLogout;
	$TotalHazardPayFinal=0;
	$TotalHazardPayFinal=(($HPval2 * ($hourly*$hpRate))+$HPHoliday);
	// echo $HPval2 ."*(". $hourly."*".$hpRate."))+".$HPHoliday;
	$TotalNighDiffFinal=0;
	$TotalNighDiffFinal=(($ndRatexBCT2*($hourly*.15))+$NDHoliday);
   echo "Hi ".$fname.", <br> <br>"; 
   echo "Kindly see your Payslip below: <br><br>"; 
   
 echo "<table  style='width: 50%;display: inline-block;'> ";
 echo "<tr><td colspan=2><img src='https://lh3.googleusercontent.com/NZNZzuQogtKpP32IPLfU7ycNwp-VO_a3pqnAB4Owr0sRkJdDEHENGxRulIG2eyEtLcWQdVR5NZOsStRx4zhQePBcFviP2V_F=w1366-h768-rw-no'></td>";
 //echo	"<td colspan=2></b>Period Covered: </b>"+ps1[0]+"/"+ps1[1]+"-"+ps2[1]+"/"+ps1[2]+"</td></tr><tr>"+
	echo"<tr><td colspan=2></b>Earnings</b> </td></tr>";
	echo "<tr><td>Salary </td><td>P".number_format(($Mrate/2),2)."</td></tr>";
	echo"<tr><td>Overtime </td> <td><b>P".number_format($TotalBCTxHourly,2)."</b></td></tr>";
	echo"<tr><td>Night Diff </td><td><b>P".number_format($TotalNighDiffFinal,2)."</b></td></tr>";
	echo"<tr><td>Holiday </td><td><b>P".number_format($TotalAmountHoliday,2)."</b></td></tr>";
	echo"<tr><td>Hazard Pay</td><td> <b>P". number_format($TotalHazardPayFinal,2)."</b></td></tr>";
	if($adjustSalary>0){
		foreach($adjustSalary as $kk => $vv){
			$ss = explode("=",$vv);
			
			echo"<tr><td>".$ss[0]."</td><td> <b>P".$ss[1]."</b></td></tr>";
			$ssTotal+=$ss[1];
		}
	}else{
		$ssTotal = 0;
	}

									 // TotalGross =  parseFloat(TotalBCTxHourly)+parseFloat(TotalNighDiffFinal)+parseFloat(TotalHazardPayFinal);
	$TotalGross =  ($TotalBCTxHourly)+($TotalNighDiffFinal)+($TotalHazardPayFinal);
	$finaltotalgross= (($Mrate/2) +$TotalGross+ $ssTotal+ $TotalAmountHoliday);

	echo"<tr><td>Total Earnings </td><td><b>P".number_format($finaltotalgross,2)." </td></tr>";
	echo"<tr><td colspan=2><b>Deductions</b></td></tr>";
	echo"<tr><td>Witholding Tax </td><td><b>P".number_format($BIRTotalCompute,2)."</b></td></tr>";
	echo"<tr><td>SSS Premium</td><td> <b>P".number_format($SSS,2)."</b></td></tr>";
	echo"<tr><td>Philhealth</td><td> <b>P".number_format($PhilHealthCompute,2)."</b></td></tr>";
	echo"<tr><td>HDMF Premium </td><td><b>P".number_format($PagIbig,2)."</b></td></tr>";
	if($deductSalary>0){
		foreach($deductSalary as $kk => $vv){
			$ss = explode("=",$vv);
			
			echo"<tr><td>".$ss[0]."</td><td> <b>P".$ss[1]."</b></td></tr>";
			$netTotal+=$ss[1];
		}
	}else{
		$netTotal = 0;
	}
	$TotalNetPay = $TotalNet+$netTotal;
	$TotalPay =  $finaltotalgross - $TotalNetPay ;
	echo"<tr><td>Total Deductions</td><td> <b>P".number_format($TotalNetPay ,2)." </td></tr>";
	echo"<tr><td style='color:green'>TakeHome Pay</td><td> <b>P".number_format($TotalPay,2)." </td> </tr>";
	echo " </table>";
	echo " <br>";
	echo " <br>";
	echo " Please approach Human Resources Team if you have concerns. Thank you.";

 $timeTotal = 0;
 $timeTotalOTBCT = 0;
 $ndRatexBCT2 = 0;
 $HPval2 = 0;
$HPComputeHolidayLogin=0;
$NDComputeHolidayLogin=0;
$NDComputeHolidayLogout=0;
$HPComputeHolidayLogout=0;
$TotalAmountHoliday=0;	
 ?>
 </div>
</body>
</html> 