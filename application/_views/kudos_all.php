<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    #datatable-tabletools th{
        background:#666 !important;
        color:white !important;
        padding-top:10px;
        padding-bottom:10px;
    }
    #datatable-tabletools .table-striped>tbody>tr:nth-child(odd)>td, 
    #datatable-tabletools .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #eee;
        color:#555;
        font-size:13px;
    }
    #datatable-tabletools .table-striped>tbody>tr:nth-child(even)>td, 
    #datatable-tabletools .table-striped>tbody>tr:nth-child(even)>th {
        background-color: white;
        color:#777;
        font-size:13px;
    }
    body { padding-right: 0 !important }
</style>


<meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

<!-- JS Core -->

<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

<script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
  }, 300);
 });

</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-tabletools.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/datatable/datatable-reorder.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
        var table = $('#datatable-tabletools').DataTable();

        $('.dataTables_filter input').attr("placeholder", "Search...");

    } );


</script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lightbox/css/lightbox.min.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/lightbox/js/lightbox.min.js"></script>


<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    $(function(){"use strict";$("#daterangepicker-example").daterangepicker({startDate:moment().subtract("days",29),endDate:moment(),minDate:"01/01/2017",maxDate:new Date(),dateLimit:{days:60},showDropdowns:!0,showWeekNumbers:!0,timePicker:!1,timePickerIncrement:1,timePicker12Hour:!0,ranges:{Today:[moment(),moment()],Yesterday:[moment().subtract("days",1),moment().subtract("days",1)],"Last 7 Days":[moment().subtract("days",6),moment()],"Last 30 Days":[moment().subtract("days",29),moment()],"This Month":[moment().startOf("month"),moment().endOf("month")],"Last Month":[moment().subtract("month",1).startOf("month"),moment().subtract("month",1).endOf("month")]},opens:"left",buttonClasses:["btn btn-default"],applyClass:"small bg-green",cancelClass:"small ui-state-default",format:"MM/DD/YYYY",separator:" - ",locale:{applyLabel:"Apply",fromLabel:"From",toLabel:"To",customRangeLabel:"Custom Range",daysOfWeek:["Su","Mo","Tu","We","Th","Fr","Sa"],monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],firstDay:1}},function(a,b){console.log("Callback has been called!"),$("#daterangepicker-custom span").html(a.format("MMMM D, YYYY")+" - "+b.format("MMMM D, YYYY"))}),$("#daterangepicker-custom span").html(moment().subtract("days",29).format("MMMM D, YYYY")+" - "+moment().format("MMMM D, YYYY"))})

</script> 
</head>

<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<div id="page-wrapper">
  <div id="page-header" class="bg-black">
    <div id="mobile-navigation">
      <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
      <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
  </div>
  <div id="header-logo" class="logo-bg">
     <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
     <a id="close-sidebar" href="#" title="Close sidebar">
      <i class="glyph-icon icon-angle-left"></i>
  </a>
</div>
<div id='headerLeft'>
</div>

</div>
<div id="page-sidebar">
  <div class="scroll-sidebar">


    <ul id="sidebar-menu">
    </ul>
</div>
</div>
<div id="page-content-wrapper">
  <div id="page-content">

    <div id="page-title">
        <h2>Kudos View All</h2>
        <hr>
        <div class="content-box">
            <h3 class="content-box-header bg-black">
                <i class="glyph-icon icon-gift"></i>
                Kudos Lists
            </h3>
            <div class="content-box-wrapper">
                <div class="form-group btn text-left">
                    <label for="" class="col-sm-1 col-sm-offset-2 control-label text-right" >Date</label>
                    <div class="col-sm-4">
                        <div class="input-prepend input-group">
                            <span class="add-on input-group-addon">
                                <i class="glyph-icon icon-calendar"></i>
                            </span>
                            <input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a href='#' id='export' class="btn btn-primary">
                            <span class="glyph-icon icon-separator">
                                <i class="glyph-icon icon-download"></i>
                            </span>
                            <span class="button-content">
                                Export Excel
                            </span>
                        </a>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <table id="datatable-tabletools" class="table table-striped table-bordered responsive no-wrap table-condensed">
                            <thead class='font-bold'>
                                <tr>
                                    <th>Name</th>
                                    <th>Campaign</th>
                                    <th>Type</th>
                                    <th>Proofreading</th>
                                    <th>Card</th>
                                    <th>Reward</th>
                                    <th>Date Added</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot class='font-bold'>
                                <tr>
                                    <th>Name</th>
                                    <th>Campaign</th>
                                    <th>Type</th>
                                    <th>Proofreading</th>
                                    <th>Card</th>
                                    <th>Reward</th>
                                    <th>Date Added</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody class=" font-size-14" id='tbody'>
                                <?php foreach($kudos as $k => $val): ?>
                                    <tr>
                                        <td class='font-bold'><?php echo $val->ambassador; ?></td>
                                        <td><?php echo $val->campaign; ?></td>
                                        <td><?php echo $val->kudos_type; ?></td>
                                        <?php if($val->proofreading=='Pending'){ ?>
                                        <td style="color: red;"><?php echo $val->proofreading; ?></td>
                                        <?php }?>
                                        <?php if($val->proofreading=='Done'){ ?>
                                        <td style="color: green;"><?php echo $val->proofreading; ?></td>
                                        <?php }?>
                                        <?php if($val->proofreading=='In Progress'){ ?>
                                        <td style="color: #f29341;"><?php echo $val->proofreading; ?></td>
                                        <?php }?>
                                        
                                        <?php if($val->kudos_card=='Pending'){ ?>
                                        <td style="color: red;"><?php echo $val->kudos_card; ?></td>
                                        <?php }?>
                                        <?php if($val->kudos_card=='Done'){ ?>
                                        <td style="color: green;"><?php echo $val->kudos_card; ?></td>
                                        <?php }?>
                                        <?php if($val->kudos_card=='In Progress'){ ?>
                                        <td style="color: #f29341;"><?php echo $val->kudos_card; ?></td>
                                        <?php }?>
                                        
                                        <?php if($val->reward_status=='Pending'){ ?>
                                        <td style="color: red;"><?php echo $val->reward_status; ?></td>
                                        <?php }?>
                                        <?php if($val->reward_status=='Released'){ ?>
                                        <td style="color: green;"><?php echo $val->reward_status; ?></td>
                                        <?php }?>
                                        <?php if($val->reward_status=='Requested'){ ?>
                                        <td style="color: #f29341;"><?php echo $val->reward_status; ?></td>
                                        <?php }?>
                                        <?php if($val->reward_status=='On Hold'){ ?>
                                        <td style="color: purple;"><?php echo $val->reward_status; ?></td>
                                        <?php }?>
                                        <?php if($val->reward_status=='Approved'){ ?>
                                        <td style="color: #41b2f4;"><?php echo $val->reward_status; ?></td>
                                        <?php }?>
                                        
                                        <td><?php echo $val->date_given; ?></td>
                                        <td style="text-align: center;">
                                            <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModalUpdate" data-kudosid="<?php echo $val->kudos_id; ?>">
                                                <span class="glyph-icon icon-separator">
                                                    <i class="glyph-icon icon-eye"></i>
                                                </span>
                                                <span class="button-content">
                                                    View
                                                </span>
                                            </a>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div> 
</div>
</div>
</div>
</div>
<div class="modal fade" id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style='width:40%'>
        <div class="modal-content">
            <div class="modal-header pad20A" style='color:white;background:#333'>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <h4 class="modal-title font-bold"><i id='kudos_icon'></i> <span id='kudos_type'></span> <span class='pull-right' id='date_added'></span></h4>
            </div>
            <div class="modal-body">
                <div id="call" style='display:none'>
                    <div class="row">
                        <div class="col-md-3 text-center">
                            <img src=""  alt="Image not found" id='cemployeepic' class='img-circle' style='border:2px solid #ddd;width:70%' />
                        </div>
                        <div class="col-md-9">
                            <h3 id='cambassador' class='font-bold' style='margin-bottom:5px'></h3> 
                            <h5 id='cposition'></h5>
                            <h5 id='cstatus'></h5>
                            <h5 id='ccampaign' class='font-bold font-size-13' style='margin-top:5px'></h5>
                        </div>
                    </div>
                    <hr>

                    <blockquote style='margin:0;' >
                        <p id='ccomment'></p>
                        <footer id='cfooter' class='font-size-12'></footer>
                    </blockquote>
                    <br>
                    <blockquote style='margin:0;' >
                        <p id='crevised'></p>
                    </blockquote>
                    <h5 class='text-center'><small>Edited by: <span id='ceditedby'></span> on <span id='ceditedon'></span></small></h5>
                    <hr>  
                    <div class="row text-center">
                        <div class="col-md-4">
                            <h5 class='font-bold'>Reward Type</h5>
                            <h5 id='creward'></h5>
                        </div>
                        <div class="col-md-4">
                            <h5 class='font-bold'>Proof Reading</h5>
                            <h5 id='cproofreading'></h5>
                        </div>
                        <div class="col-md-4">
                            <h5 class='font-bold'>Card Status</h5>
                            <h5 id='ccardstatus'></h5>
                        </div>
                    </div> 
                    <hr>  
                </div>
                <div id='email' style='display:none'>
                   <div class="row">
                    <div class="col-md-3 text-center">
                        <img src=""  alt="Image not found" id='eemployeepic' class='img-circle' style='border:2px solid #ddd;width:70%' />
                    </div>
                    <div class="col-md-9">
                        <h3 id='eambassador' class='font-bold' style='margin-bottom:5px'></h3> 
                        <h5 id='eposition'></h5>
                        <h5 id='estatus'></h5>
                        <h5 id='ecampaign' class='font-bold font-size-13' style='margin-top:5px'></h5>
                    </div>
                </div>
                <hr>
                <div class="row text-center">
                    <div class="col-md-12">
                        <h5 class='font-bold'>Email Screenshot</h5><br>
                        <a href="images/image-1.jpg" id='escreenshot' data-lightbox="image-2" data-title="Email Screenshot"><img style='height:150px' src="" class='img-thumbnail' id='escreenshot2'></a>

                        <p> - <small id='efooter'></small></p>
                    </div>
                </div>
                <hr>
                <blockquote style='margin:0;' >
                    <p id='erevised'></p>
                </blockquote>
                <h5 class='text-center'><small>Edited by: <span id='eeditedby'></span> on <span id='eeditedon'></span></small></h5>
                <hr>  
                <div class="row text-center">
                    <div class="col-md-4">
                        <h5 class='font-bold'>Reward Type</h5>
                        <h5 id='ereward'></h5>
                    </div>
                    <div class="col-md-4">
                        <h5 class='font-bold'>Proof Reading</h5>
                        <h5 id='eproofreading'></h5>
                    </div>
                    <div class="col-md-4">
                        <h5 class='font-bold'>Card Status</h5>
                        <h5 id='ecardstatus'></h5>
                    </div>
                </div> 
                <hr>  
            </div>
            <div class="row text-center" id='card'>
                <div class="col-md-12">
                    <h5 class='font-bold'>Card Picture</h5><br>
                    <a href="images/image-1.jpg" id='cardpicture' data-lightbox="image-1" data-title="Kudos Card"><img style='height:150px' src="" id='cardpicture2' class='img-thumbnail'></a>
                </div>
            </div>
        </div>
        <div class="modal-footer" style='color:white;background:#333'>
            <a href="#" class="btn btn-default" data-dismiss="modal">
                <span class="glyph-icon icon-separator">
                    <i class="glyph-icon icon-times"></i>
                </span>
                <span class="button-content">
                    Close
                </span>
            </a>
        </div>
    </div>
</div>
</div>
<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
       }
   });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
     }
 });
});   
</script>
<script type="text/javascript">
    $('#myModalUpdate').on('show.bs.modal', function(e) {
        var kudosid = $(e.relatedTarget).data('kudosid');
        $("#call").hide();
        $("#email").hide();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Kudos/getKudos",
            data: {
                kudosid:kudosid
            },
            cache: false,
            success: function(res)
            {
                res = res.trim();
                res = JSON.parse(res);
                console.log(res);
                $("#submit").attr('data-kudosid',kudosid);
                $("#kudos_type").html(res.kudos_type);
                $("#date_added").html(res.date_given);

                var img2 = new Image();
                img2.src = "<?php echo base_url()?>"+res.file;
                if(img2.height != 0){

                    $("#cardpicture").attr('href',"<?php echo base_url()?>"+res.kudos_card_pic);
                    $("#cardpicture2").attr('src',"<?php echo base_url()?>"+res.kudos_card_pic);
                }else{
                    $("#cardpicture").attr('href',"<?php echo base_url('assets/images/emoji/image-not-found.png')?>");
                    $("#cardpicture2").attr('src',"<?php echo base_url('assets/images/emoji/image-not-found.png')?>");
                }
                
                if(res.kudos_type=='Call'){
                    $("#kudos_icon").html("<i class='glyph-icon icon-phone'></i>");
                    $("#ccomment").html('"'+res.comment+'"');
                    $("#crevised").html('"'+res.revision+'"');
                    $("#cfooter").html(res.client_name+", "+res.phone_number);
                    $("#ceditedby").html(res.revised_by);
                    $("#ceditedon").html(res.date_revised);

                    $("#ccampaign").html(res.acc_name);
                    $("#cambassador").html(res.lname+", "+res.fname);
                    $("#creward").html(res.reward_type);
                    $("#cproofreading").html(res.proofreading);
                    $("#ccardstatus").html(res.reward_status);
                    var img = new Image();
                    img.src = "<?php echo base_url()?>assets/images/"+res.pic;
                    if(img.height != 0){
                        $("#cemployeepic").attr('src',"<?php echo base_url()?>assets/images/"+res.pic);
                    }else{
                        $("#cemployeepic").attr('src',"<?php echo base_url('assets/images/sz.png'); ?>");
                    }
                    $("#cposition").html(res.pos_details);
                    $("#cstatus").html(res.status);

                    $("#call").show();
                }else{
                    $("#kudos_icon").html("<i class='glyph-icon icon-envelope'></i>");
                    $("#erevised").html('"'+res.revision+'"');
                    $("#efooter").html(res.client_name+", "+res.client_email);
                    $("#eeditedby").html(res.revised_by);
                    $("#eeditedon").html(res.date_revised);

                    $("#ecampaign").html(res.acc_name);
                    $("#eambassador").html(res.lname+", "+res.fname);
                    $("#ereward").html(res.reward_type);
                    $("#eproofreading").html(res.proofreading);
                    $("#ecardstatus").html(res.reward_status);
                    var img = new Image();
                    img.src = "<?php echo base_url()?>assets/images/"+res.pic;
                    if(img.height != 0){
                        $("#eemployeepic").attr('src',"<?php echo base_url()?>assets/images/"+res.pic);
                    }else{
                        $("#eemployeepic").attr('src',"<?php echo base_url('assets/images/sz.png'); ?>");
                    }
                    $("#eposition").html(res.pos_details);
                    $("#estatus").html(res.status);

                    $("#escreenshot").attr('href',"<?php echo base_url()?>"+res.file);
                    $("#escreenshot2").attr('src',"<?php echo base_url()?>"+res.file);
                    $("#email").show();
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $("#export").click(function(){
        var date = $("#daterangepicker-example").val();
        if(date==''){
                 swal(
                    'Kudos!',
                    'Please select date range.',
                    'warning'
                    )
        }else{
            date = date.split(" - ");
            start = date[0];
            end = date[1];
            startdate = start.replace(/\//g,'-');
            enddate = end.replace(/\//g,'-');
            window.location = "<?php echo base_url();?>index.php/Kudos/kudoslistexcel/"+startdate+"/"+enddate;
        }
    })
</script>
<script type="text/javascript">
    $('#daterangepicker-example').on('apply.daterangepicker', function(ev, picker) {
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Kudos/getKudosAll",
          data:{
            startdate:picker.startDate.format('YYYY-MM-DD'),
            enddate:picker.endDate.format('YYYY-MM-DD')
        },
        cache: false,
        success: function(res)
        {
            
            $('#datatable-tabletools').DataTable().destroy();
            res = res.trim();
            res = JSON.parse(res);
            $("#tbody").html('');
            $.each(res, function(i, item) {
                var body = "";
                body += "<tr>";
                body += "<td>"+item.lname+", "+item.fname+"</td>";
                body += "<td>"+item.campaign+"</td>";
                body += "<td>"+item.kudos_type+"</td>";
                if(item.proofreading=='In Progress'){
                    body += "<td style='color:#f29341'>"+item.proofreading+"</td>";
                }else if(item.proofreading=='Done'){
                    body += "<td style='color:green'>"+item.proofreading+"</td>";
                }else{
                    body += "<td style='color:red'>"+item.proofreading+"</td>";
                }
                if(item.kudos_card=='In Progress'){
                    body += "<td style='color:#f29341'>"+item.kudos_card+"</td>";
                }else if(item.kudos_card=='Done'){
                    body += "<td style='color:green'>"+item.kudos_card+"</td>";
                }else{
                    body += "<td style='color:red'>"+item.kudos_card+"</td>";
                }
                if(item.reward_status=='Requested'){
                    body += "<td style='color:#f29341'>"+item.reward_status+"</td>";
                }else if(item.reward_status=='Done'){
                    body += "<td style='color:green'>"+item.reward_status+"</td>";
                }else{
                    body += "<td style='color:red'>"+item.reward_status+"</td>";
                }
                body += "<td>"+item.date_given+"</td>";
                body += "<td><a href='#' class='btn btn-info btn-sm' data-toggle='modal' data-target='#myModalUpdate' data-kudosid='"+item.kudos_id+"'><span class='glyph-icon icon-separator'><i class='glyph-icon icon-eye'></i></span><span class='button-content'>View</span></a></td>";
                body += "</tr>";
                $("#tbody").append(body);
            });
            $('#datatable-tabletools').DataTable().draw();
        }
    });
  });
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>