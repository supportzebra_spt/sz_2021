<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

  <style>
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    .chosen-container-single .chosen-single{
      line-height:25px !important;
      height:25px !important;
      font-size:12px !important;
    }
    .chosen-results .active-result{

      font-size:12px !important;
    }
  </style>

  <meta charset="UTF-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Favicons -->

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">



  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/fontawesome/fontawesome.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icons/typicons/typicons.css">

  <!-- JS Core -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>

  <script type="text/javascript">
    $(window).load(function(){
     setTimeout(function() {
      $('#loading').fadeOut( 400, "linear" );
    }, 300);
   });

 </script><!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/chosen/chosen.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/chosen/chosen-demo.js"></script>
 <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/daterangepicker/daterangepicker.css">-->
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/moment.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/widgets/daterangepicker/daterangepicker.js"></script>
 <script type="text/javascript">
   $(function(){
    "use strict";
    var end = moment().format("MM/DD/YYYY");
    var m = moment().subtract(1,'year').format("MM/DD/YYYY");
    $("#daterangepicker-example").daterangepicker({ minDate: m,startDate: end,maxDate : end});
    $("[name=daterangepicker_start]").attr('readonly',true);
    $("[name=daterangepicker_end]").attr('readonly',true);
  });
</script>
<!-- Input switch -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/input-switch/inputswitch.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>
<script type="text/javascript">
  /* Input switch */

  $(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
  });
</script>
<!-- Multi select -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/multi-select/multiselect.js"></script>
<script type="text/javascript">
  /* Multiselect inputs */

  $(function() { "use strict";
    $("#employeelist").multiSelect();
    $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
  });
</script>
</head>


<body>
  <div id="sb-site">


    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-black">
        <div id="mobile-navigation">
          <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
          <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">
         <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
         <a id="close-sidebar" href="#" title="Close sidebar">
          <i class="glyph-icon icon-angle-left"></i>
        </a>
      </div>
      <div id='headerLeft'>
      </div>

    </div>
    <div id="page-sidebar">
      <div class="scroll-sidebar">


        <ul id="sidebar-menu">
        </ul>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div id="page-content">

        <div id="page-title">
          <h3>DTR Schedule</h3>
          <hr>
          <div class="example-box-wrapper">

            <a href="<?php echo base_url();?>index.php/Schedule" class='btn btn-warning btn-xs'>Back</a>
            <div class="row">
              <div class="col-md-12">
                <div class="content-box">
                  <h3 class="content-box-header bg-primary">
                    <i class="glyph-icon icon-search"></i>
                    Search for Schedules
                  </h3>
                  <div class="content-box-wrapper">

                    <div class="row">
                     <div class="col-md-4">
                       <div class="row">
                        <div class="col-md-12">
                         <div class="form-group" id='acc_descriptiondiv'>
                          <label class='control-label'>Account Type</label>
                          <select class='form-control' id='acc_description'>
                            <option value='' id='removeme'>--</option>
                            <option value='All'>All</option>
                            <option value='Admin'>Admin</option>
                            <option value='Agent'>Agent</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <label class='control-label'>Date Range</label>
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon">
                            <i class="glyph-icon icon-calendar"></i>
                          </span>
                          <input type="text" name="daterangepicker-example" id="daterangepicker-example" class="form-control">
                        </div>
                      </div>                    
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-12"  id='employeelistdiv'>
                      <div class="row">
                        <div class="col-md-10">
                          <label class='control-label'>Employee List</label>

                          <select multiple name="" id="employeelist"  class="multi-select"></select>
                        </div>

                        <div class="col-md-2"><br>
                          <div class="btn-group-vertical">
                            <button id="select" class='btn btn-primary btn-xs'>Select all</button>
                            <button id="deselect" class='btn btn-primary btn-xs'>Deselect all</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="button-pane text-right">
                <button class="btn btn-primary" id='generateschedule'>Display Schedule</button>

              </div>
            </div>

          </div>
        </div>
      </div>

    </div> 
    <div class="loading-spinner" id="loadingspinner" style="display:none">
      <i class="bg-primary"></i>
      <i class="bg-primary"></i>
      <i class="bg-primary"></i>
      <i class="bg-primary"></i>
      <i class="bg-primary"></i>
      <i class="bg-primary"></i>
    </div>
    <div class="container" id="page-title">

      <div class="content-box" id="scheduleview" style="display:none;">
        <h3 class="content-box-header bg-blue-alt"> 
          <i class="glyph-icon icon-list"></i>
          SCHEDULE LIST
        </h3>
        <div class="content-box-wrapper">

          <table class="table table-striped table-hover table-condensed">
            <thead>
              <th>Name</th>
              <th>Log In</th>
              <th>Log Out</th>
              <th colspan='3'>Schedule</th>
            </thead>
            <tbody id="tbody"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
  $(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
      cache: false,
      success: function(html)
      {

        $('#headerLeft').html(html);
           //alert(html);
         }
       });   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
      cache: false,
      success: function(html)
      {

        $('#sidebar-menu').html(html);
         // alert(html);
       }
     });
  });   
</script>
<script type="text/javascript">
  $(function(){

   $("#employeelist").prop('disabled',true);
   $("#employeelist").multiSelect('refresh');
   $("#select").prop('disabled',true);
   $("#deselect").prop('disabled',true);
   $("#generateschedule").prop('disabled',true);
   $("#acc_description").on('change',function(){
    var desc = $(this).val();
    $("#removeme").remove();
    if(desc=='All'){
     $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/getSupervisoryAllEmp",
      cache: false,
      success: function(res)
      { 
        if(res=="Empty"){
         $("#employeelist").prop('disabled',true);
         $("#employeelist").multiSelect('refresh');
         $("#select").prop('disabled',true);
         $("#deselect").prop('disabled',true);
         $("#generateschedule").prop('disabled',true);
       }else{
        $("#employeelist").html('');
        $.each(JSON.parse(res),function (i,elem){
          $('#employeelist').multiSelect('addOption', { value: elem.emp_id, text: elem.lname+", "+elem.fname, index: 0 });  
        });

        $("#employeelist").multiSelect('refresh');
        $("#generateschedule").prop('disabled',false);
      }
    }
  });
   }else if(desc=='Admin'){
     $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/getSupervisoryAdminEmp",
      cache: false,
      success: function(res)
      { 
        if(res=="Empty"){
         $("#employeelist").prop('disabled',true);
         $("#employeelist").multiSelect('refresh');
         $("#select").prop('disabled',true);
         $("#deselect").prop('disabled',true);
         $("#generateschedule").prop('disabled',true);
       }else{
        $("#employeelist").html('');
        $.each(JSON.parse(res),function (i,elem){
          $('#employeelist').multiSelect('addOption', { value: elem.emp_id, text: elem.lname+", "+elem.fname, index: 0 });  
        });

        $("#employeelist").multiSelect('refresh');
        $("#generateschedule").prop('disabled',false);
      }
    }
  });
   }else if(desc=='Agent'){
     $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/getSupervisoryAgentEmp",
      cache: false,
      success: function(res)
      { 
        if(res=="Empty"){
         $("#employeelist").prop('disabled',true);
         $("#employeelist").multiSelect('refresh');
         $("#select").prop('disabled',true);
         $("#deselect").prop('disabled',true);
         $("#generateschedule").prop('disabled',true);
       }else{
        $("#employeelist").html('');
        $.each(JSON.parse(res),function (i,elem){
          $('#employeelist').multiSelect('addOption', { value: elem.emp_id, text: elem.lname+", "+elem.fname, index: 0 });  
        });

        $("#employeelist").multiSelect('refresh');
        $("#generateschedule").prop('disabled',false);
      }

    }
  });
   }else{
    alert('ambot nganu');
  }

  $("#employeelist").prop('disabled',false);
  $("#employeelist").multiSelect('refresh');
  $("#select").prop('disabled',false);
  $("#deselect").prop('disabled',false);
});
 })
</script>
<script type="text/javascript">
  $(function(){
    $("#acc_description").on('change',function(){
      $("#employeelist").html("");
      $("#employeelist").multiSelect('refresh');
    })
  })
</script>

<script type="text/javascript">
  $(function(){
    $('#select').click(function(){

      $("#employeelist").multiSelect('select_all');
    });
  })
</script>
<script type="text/javascript">
  $(function(){

    $('#deselect').click(function(){

      $("#employeelist").multiSelect('deselect_all');
    });
  })
</script>
<script type="text/javascript">
  function schedupdater(dtr_id_in,sched_id,dtr_id_out){
    if(sched_id==''){
      
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/schedupdaterdtr2",
      data: {
        dtr_id_in : dtr_id_in,
        dtr_id_out : dtr_id_out
      },
      cache: false,
      success: function(res)
      { 
        // alert(res);
        if(res=='Success'||res=='Success2'){
          $.jGrowl("Successfully Updated Schedule.",{sticky:!1,position:"top-right",theme:"bg-blue-alt"});
        }else{ 
          $.jGrowl("Problem Updating Schedule.",{sticky:!1,position:"top-right",theme:"bg-red"});
        }
      }
    });
    }else{
      $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/Schedule/schedupdaterdtr",
      data: {
        sched_id : sched_id,
        dtr_id_in : dtr_id_in,
        dtr_id_out : dtr_id_out
      },
      cache: false,
      success: function(res)
      { 
        // alert(res);
        if(res=='Success'||res=='Success2'){
          $.jGrowl("Successfully Updated Schedule.",{sticky:!1,position:"top-right",theme:"bg-blue-alt"});
        }else{ 
          $.jGrowl("Problem Updating Schedule.",{sticky:!1,position:"top-right",theme:"bg-red"});
        }
      }
    });
    }
    
  }
</script>
<script type="text/javascript">
  $("#generateschedule").click(function(){

    $("#scheduleview").hide();
    var date = $("#daterangepicker-example").val();
    if(date==null||date==''){
      alert("date empty");
    }else{

      var dater = date.split(" - ");
      var fromDateRaw = dater[0];
      var fromDateRaw2 = fromDateRaw.split("/");
      var fromDate = fromDateRaw2[2]+"-"+fromDateRaw2[0]+"-"+fromDateRaw2[1];
      var toDateRaw = dater[1];
      var toDateRaw2 = toDateRaw.split("/");
      var toDate = toDateRaw2[2]+"-"+toDateRaw2[0]+"-"+toDateRaw2[1];
      var acc_description = $("#acc_description").val();
      var employeelist = $("#employeelist").val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Schedule/generatedate",
        data: {
          fromDate : fromDate,
          toDate : toDate
        },
        cache: false,
        success: function(sched)
        { 
          var emplist = employeelist.toString().split(",");
          employeelist = emplist.join('-');

          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Schedule/getSchedPerAccountEmployeeDTR",
            data: {
              fromDate : fromDate,
              toDate : toDate,
              employeelist : employeelist
            },
            cache: false,
            beforeSend: function(){
              $("#loadingspinner").show();
            },
            success: function(ress)
            { 
              $("#loadingspinner").hide();

              $("#tbody").html("");
              $("#scheduleview").show();
              var res = JSON.parse(ress);                
              var body = "";
              $.each(res, function(index, val) {

              body += "<tr>";
              body += "<td>"+val.lname+", "+val.fname+"</td>";
              body += "<td>"+val.log_in+"</td>";
              if(val.log_out==null){
                body += "<td>Not Logged Out</td>";
              }else{
                body += "<td>"+val.log_out+"</td>";
              }
              body += "<td colspan='3'>";
              if(val.availablesched!=null){
                body += "<select name='' class='chosen-select input-sm' onchange='schedupdater("+val.dtr_id_in+",this.value,"+val.dtr_id_out+")'>";

                body += "<option value=''>--</option>";
                val.availablesched.forEach( function( item ) {

                  if(item.type=='Normal'){
                    body += "<option value='"+item.sched_id+"'"+" "+(val.sched_id == item.sched_id ? "selected" : "")+">"+item.schedule_date+" "+item.time_start+" - "+item.time_end+"</option>";
                  }else if(item.type=='WORD'){
                    body += "<option value='"+item.sched_id+"'"+" "+(val.sched_id == item.sched_id ? "selected" : "")+">"+item.schedule_date+" "+item.time_start+" - "+item.time_end+" "+item.type+"</option>";
                  }else{
                    body += "<option value='"+item.sched_id+"'"+" "+(val.sched_id == item.sched_id ? "selected" : "")+">"+item.schedule_date+" "+item.type+"</option>";
                  }

                });

                body += "</select>";
              }else{
                body += "No Schedule Available";
              }

              body += "</td>";
              body += "</tr>";
            });
              $("#tbody").append(body);            
              $(".chosen-select").chosen();
              $(".chosen-search").append('<i class="glyph-icon icon-search"></i>');
              $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
            }

          });
        }

      });

    }

  })
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>