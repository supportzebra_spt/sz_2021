<!DOCTYPE html> 
<html  lang="en">

<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
        .hidden{
            visibility: hidden;
        }
        .seen{
            visibility: visible;
        }

    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- jQueryUI Autocomplete -->



    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/favicon.png">

    <!--<script type="text/javascript" src="<?php echo base_url();?>assets/widgets/input-switch/inputswitch.js"></script>-->

    <!-- High Charts-->

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/themes/dark-unica.js"></script>




    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets-minified/admin-all-demo.css">

    <!-- JS Core -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/js-core.js"></script>



    <script type="text/javascript">
        $(window).load(function(){
         setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
     });
 </script>

</head>


<body>
    <div id="sb-site">


        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div id="page-wrapper">
            <div id="page-header" class="bg-black">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                    <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                </div>
                <div id="header-logo" class="logo-bg">
                   <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                   <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id='headerLeft'>
            </div>



        </div>
        <div id="page-sidebar">
            <div class="scroll-sidebar">


                <ul id="sidebar-menu">
                </ul>
            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content">

                <div class="container">


                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div id="chartx1"></div>
                            </div>
                            <div class="col-md-6">
                                <div id="chartx2"></div>
                            </div>
                        </div>
                    </div> 

                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div id="charter5"></div>
                            </div>
                            <div class="col-md-6">
                                <div id="charter6"></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div id="charter7"></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>



</div>
</div>
</div>
</body>
<script>
    $(function(){
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
          cache: false,
          success: function(html)
          {

            $('#headerLeft').html(html);
     //alert(html);
 }
}); 
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
          cache: false,
          success: function(html)
          {

            $('#sidebar-menu').html(html);
   // alert(html);
}
});
    }); 
</script>


<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>
<script type="text/javascript">
    Highcharts.setOptions(Highcharts.theme);
    var chart = Highcharts.chart('chartx1', {
        chart: {
            inverted: true,
            polar: false,
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Kudos Count of <?php date_default_timezone_set('Asia/Manila'); echo date("Y");?>'
        },

        xAxis: {
            type: 'category'
        },

        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            name: 'Monthly Kudos Count',
            colorByPoint: true,
            data: [
            <?php foreach($cntkudos as $kudos):?>

            {
                name: '<?php echo $kudos->monthyear;?>',
                y: <?php echo $kudos->cnt;?>,
                drilldown: '<?php echo $kudos->monthyear;?>'
            },
        <?php endforeach;?>
        ]
    }],

    drilldown: {
        series: [

        <?php foreach($cntkudos as $kudos):?>
        {
            type: 'column',
            name: '<?php echo $kudos->monthyear;?>',
            id: '<?php echo $kudos->monthyear;?>',
            data: [
            <?php foreach($ambassadorkudos as $a):?>
            <?php if($a->monthyear==$kudos->monthyear):?>
            ['<?php echo $a->lname.', '.$a->fname.' '.$a->mname;?>', <?php echo $a->counter;?>],
        <?php endif;?>
    <?php endforeach;?>


    ]
},
<?php endforeach;?>

]
}


});
</script>

<script type="text/javascript">
    Highcharts.setOptions(Highcharts.theme);
    var chart = Highcharts.chart('chartx2', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'KUDOS COUNT OF <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
        },
        xAxis: {
            categories: [
            <?php foreach($currentmonthkudos as $kudos):?>
            '<?php  echo $kudos->lname.', '.$kudos->fname?>',
        <?php endforeach;?>]
    },
    yAxis: {
        min: 0,
        title: {
            text: 'VALUES'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [{

        colorByPoint: true,
        name: '<?php echo date("F Y");?> KUDOS',
        data: [
        <?php foreach($currentmonthkudos as $kudos):?>
        <?php  echo $kudos->counter;?>,
    <?php endforeach;?>


    ]
}]
});
</script>

<script type="text/javascript">
    Highcharts.setOptions(Highcharts.theme);
    var chart = Highcharts.chart('charter5', {
        chart: {
            inverted: true,
            polar: false,
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Kudos Count By Campaigns of <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
        },

        xAxis: {
            type: 'category'
        },

        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            name: 'Monthly Kudos Count',
            colorByPoint: true,
            data: [
            <?php foreach($allcampaign as $kudos):?>

            {
                name: '<?php echo $kudos->name;?>',
                y: <?php echo $kudos->counter;?>,
                drilldown: '<?php echo $kudos->name;?>'
            },
        <?php endforeach;?>
        ]
    }],

    drilldown: {
        series: [

        <?php foreach($allcampaign as $kudos):?>
        {
            type: 'column',
            name: '<?php echo $kudos->name;?>',
            id: '<?php echo $kudos->name;?>',
            data: [
            <?php foreach($campaignambassador as $a):?>
            <?php if($a->monthyear==$kudos->monthyear && $a->campaignname==$kudos->name):?>
            ['<?php echo $a->lname.', '.$a->fname?>', <?php echo $a->counter;?>],
        <?php endif;?>
    <?php endforeach;?>


    ]
},
<?php endforeach;?>

]
}


});
</script>
<script type="text/javascript">
    Highcharts.setOptions(Highcharts.theme);
    Highcharts.chart('charter6', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Top 5 Campaigns for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
            }
        },
        series: [{
            id: 'toplevel',
            name: 'Kudos Counts',
            data: [
            <?php $x = 1; foreach($newtopcampaign as $newtc):?>
            {name: 'Top <?php echo $x;?>', y:<?php echo $newtc->scores;?>, drilldown: 'Top<?php echo $x;?>'},
            <?php $x++; endforeach;?>
            ]
        }],
        drilldown: {
            series: [ 

            <?php $x = 1; foreach($newtopcampaign as $newtc):?>
            { 
                id:'Top<?php echo $x;?>',
                name: 'Top <?php echo $x;?>',
                data: [
                <?php foreach($allcampaign as $kudos):?>
                <?php if($newtc->monthyear==$kudos->monthyear && $newtc->scores==$kudos->counter):?>
                {name: '<?php echo $kudos->name;?>', y: <?php echo $kudos->counter;?>, drilldown: '<?php echo $kudos->name;?>'},
                <?php array_push($camp, $kudos); endif;?>
            <?php endforeach;?>
            ] 
        },
        <?php $x++; endforeach;?>


        <?php foreach($camp as $c):?>

        {
            id: '<?php echo $c->name;?>',
            name: '<?php echo $c->name;?>',
            data: [
            <?php foreach ($campaignambassador as $tca):?>
            <?php if($c->monthyear==$tca->monthyear && $c->name==$tca->campaignname):?>
            ['<?php echo $tca->lname.", <br>".$tca->fname;?>', <?php echo $tca->counter;?>],
        <?php endif;?>
    <?php endforeach;?>
    ]
},

<?php endforeach;?>
]
}

});
</script>


<script type="text/javascript">
    Highcharts.setOptions(Highcharts.theme);
    Highcharts.chart('charter7', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Top 5 Ambassador for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                cursor: 'pointer',
                depth: 30,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Kudos Count',
            data: [

            <?php $x=1; foreach($newtopambassador as $t): ?>
            {
                name: 'Top <?php echo $x;?>',
                y: <?php echo $t->scores;?>,
                drilldown: 'Top<?php echo $x;?>'
            },
            <?php $x++; endforeach;?>
            ]
        }],
        drilldown: {

            series: [
            <?php $x=1; foreach($newtopambassador as $t): ?>
            {
                type: 'pie',
                name: 'Top <?php echo $x;?>',
                id: 'Top<?php echo $x;?>',
                data: [ 
                <?php foreach ($campaignambassador as $tca):?>
                <?php if($t->monthyear==$tca->monthyear && $t->scores==$tca->counter):?>
                ['<?php echo $tca->lname.", <br>".$tca->fname;?>', <?php echo $tca->counter;?>],
            <?php endif;?>
        <?php endforeach;?>
        ]
    },
    <?php  $x++; endforeach;?>
    ]
}
});
</script>


<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>