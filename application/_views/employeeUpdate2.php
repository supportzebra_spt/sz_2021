<!DOCTYPE html> 
<html  lang="en"  ng-app="app">

    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
            .editable-radiolist label {
                display: block;
            }
        </style>
        <style type="text/css">
            .blurry{
                -webkit-filter: blur(5px);
                -moz-filter: blur(5px);
                -ms-filter: blur(5px);
                -o-filter: blur(5px);
                filter: blur(5px);
            }
            body { padding-right: 0 !important }
        </style>
        <style type="text/css">
            .radio{
                margin-top:5px;
            }
            textarea {
                resize: none;
            }
			.img-container, .img-preview {
			overflow: hidden;
			text-align: center;
			width: 100%;
		}
        </style>

        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">
		<link href="<?php echo base_url(); ?>assets/cropper/cropper.min.css" rel="stylesheet">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">

        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>
		<script src="<?php echo base_url(); ?>assets/cropper/cropper.min.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });
        </script>



        <!-- jQueryUI Datepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/datepicker-ui/datepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker-ui/datepicker-demo.js"></script>

        <!-- Bootstrap Daterangepicker -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/daterangepicker/daterangepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/daterangepicker.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/daterangepicker-demo.js"></script>-->
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/datepicker/datepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
                /* Datepicker bootstrap */

                $(function () {
                    "use strict";
                    $('.bootstrap-datepicker').bsdatepicker({
                        format: 'yyyy-mm-dd'
                    });
                });

        </script>
        <!-- Bootstrap Wizard -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/wizard/wizard.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/wizard/wizard.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/wizard/wizard-demo.js"></script>

        <!-- Boostrap Tabs -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/tabs/tabs.js"></script>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/xeditable/xeditable.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/xeditable/xeditable.js"></script>
        <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/xeditable/xeditable-demo.js"></script> -->

        <!-- Bootstrap Summernote WYSIWYG editor -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/input-switch/inputswitch.js"></script>
        <script type="text/javascript">
                /* Input switch */

                $(function () {
                    "use strict";
                    $('.input-switch').bootstrapSwitch();
                });
        </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/tooltip/tooltip.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/popover/popover.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/moment.js"></script>
        <!--<script src="<?php echo base_url(); ?>assets/js/jquery-dateFormat.min.js"></script>-->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/jgrowl-notifications/jgrowl.js"></script>
        <!-- jQueryUI Autocomplete -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/autocomplete/autocomplete.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/autocomplete/menu.js"></script>


        <script>
                $(function () {
                    "use strict";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/employee/listofSupervisor",
                        cache: false,
                        success: function (response) {
                            var a = jQuery.parseJSON(response);
                            $("#supName").autocomplete({
                                source: a,
                                focus: function (event, ui) {
                                    $("#supName").val(ui.item.label);
                                    return false;
                                },
                                select: function (event, ui) {
                                    $("#supName").val(ui.item.label);
                                    $("#supEmail").val(ui.item.email);
                                    $("#supID").val(ui.item.emp_id);
                                    return false;
                                }
                            }).autocomplete("instance")._renderItem = function (ul, item) {
                                return $("<li>").append("<div>" + item.label + "<br>" + item.email + "</div>").appendTo(ul);
                            };
                        }
                    });
                });
        </script>
        <!-- Touchspin -->
        <!-- jQueryUI Spinner -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/spinner/spinner.js"></script>
        <script type="text/javascript">
                /* jQuery UI Spinner */

                $(function () {
                    "use strict";
                    $(".spinner-input").spinner();
                });
        </script>

        <!-- jQueryUI Autocomplete -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/autocomplete/autocomplete.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/autocomplete/menu.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/autocomplete/autocomplete-demo.js"></script>


        <!-- Input switch -->



        <!-- Uniform -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/widgets/uniform/uniform.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/uniform/uniform.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/uniform/uniform-demo.js"></script>

    </head>

    <body>
        <div id="sb-site">
            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
            <div id="page-wrapper">
                <div id="page-header" class="<?php echo $setting['settings']; ?>">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>
                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">
                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">
                        <h2>EMPLOYEE UPDATE</h2>
                        <hr style='margin-bottom:15px'>
                        <a class="btn btn-xs btn-warning" href="<?php echo base_url(); ?>index.php/Employee" style='margin-bottom:5px'>
                            <span class="glyph-icon icon-separator">
                                <i class="glyph-icon icon-arrow-left"></i>
                            </span>
                            <span class="button-content">
                                Back
                            </span>
                        </a>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="panel" style='padding-bottom:10px'>
                                    <div class="panel-body">
                                        <div class="example-box-wrapper">
                                            <div id="form-wizard-1">
                                                <ul style='margin:0;padding:0'>
                                                    <li><a href="#tab1" data-toggle="tab">EMPLOYEE INFORMATION</a></li>
                                                    <li><a href="#tab2" data-toggle="tab">EMPLOYMENT DETAILS</a></li>
                                                    <li><a href="#tab4" data-toggle="tab">PAYROLL DETAILS</a></li>
                                                </ul>
                                                <hr style='margin:0 0 5px 0'>
                                                <div class="tab-content"  style='margin:0 !important'>
                                                    <div class="tab-pane" id="tab1">
                                                        <div class="content-box">
                                                            <h3 class="content-box-header bg-primary">
                                                                Personal Information
                                                            </h3>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">First Name:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="fname"><?php echo $emp['fname']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Middle Name:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="mname"><?php echo $emp['mname']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Last Name:</label>
                                                                    <div class="col-sm-8">

                                                                        <a href="#" id="lname"><?php echo $emp['lname']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Nick Name:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="nikname"><?php echo $emp['nikname']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Birthday:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="bday"><?php echo $emp['birthday']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Gender:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="gender" data-type="select" data-value="<?php echo $emp['gender']; ?>" data-pk="1" data-title="Select Tax Category"><?php echo $emp['gender']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Civil:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="civil" data-type="select" data-pk="1" data-value="<?php echo $emp['civil']; ?>" data-title="Select Tax Category"><?php echo $emp['civil']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Contact #:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="cell"><?php echo $emp['cell']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Alt Contact #:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="tel"><?php echo $emp['tel']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div> 

                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Email:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="email"><?php echo $emp['email']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Blood Type:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="bloodtype" data-type="select" data-value="<?php echo $emp['blood']; ?>" data-pk="1" data-title="Select Blood Type"><?php echo $emp['blood']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Religion:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="religion" data-type="select" data-value="<?php echo $emp['relijon']; ?>" data-pk="1" data-title="Select Religion"><?php echo $emp['relijon']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="content-box">
                                                            <h3 class="content-box-header bg-primary">
                                                                EDUCATIONAL ATTAINMENT

                                                            </h3>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">School Name:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="school_name"><?php echo $emp['school_name']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Course:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="course"><?php echo $emp['course']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">School Note:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="school_note"><?php echo $emp['school_note']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="content-box" style='margin-bottom:0'>
                                                            <h3 class="content-box-header bg-primary">
                                                                Emergency Contact Person
                                                            </h3>

                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">First Name:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="emergency_fname"><?php echo $emp['emergency_fname']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Middle Name:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="emergency_mname"><?php echo $emp['emergency_mname']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Last Name:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="emergency_lname"><?php echo $emp['emergency_lname']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Contact Number:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="emergency_contact"><?php echo $emp['emergency_contact']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab2">
                                                        <div class="content-box">
                                                            <h3 class="content-box-header bg-primary" >
                                                                Employment history
                                                            </h3>

                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">CC Experience:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="ccaExp"><?php echo $emp['ccaExp']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Latest Employer:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="other_latestEmp"><?php echo $emp['other_latestEmp']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Last Held Position:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="other_lastPosition"><?php echo $emp['other_lastPosition']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Year Left:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="other_inclusiveDate"><?php echo $emp['other_inclusiveDate']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="content-box">
                                                            <h3 class="content-box-header bg-primary" >
                                                                Employment Anniversary
                                                            </h3>

                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Date:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="anniversary"><?php echo $emp['anniversary']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="content-box">
                                                            <h3 class="content-box-header bg-primary">
                                                                Supervisor
                                                            </h3>
                                                            <div class="content-box-wrapper">
                                                                <label class="col-sm-3 control-label">Full name</label>
                                                                <div >
                                                                    <input type="text" name="" placeholder="Start typing to see results..." class="form-control autocomplete-input" style="width: 400px;" id='supName' value="<?php echo $Supname; ?>"/>  
                                                                </div>
                                                                <label class="col-sm-3 control-label"> Email Address </label>
                                                                <div ng-controller="Ctrl">
                                                                    <input type="text" name="" placeholder="Email address" class="form-control" id='supEmail' style="width: 400px;" value="<?php echo $emp['supEmails']; ?>" disabled/>  
                                                                    <input type="hidden" name="" placeholder="Email address" class="form-control" id='supID' style="width: 400px;"  value="<?php echo $emp['Supervisor']; ?>"  />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="content-box" style='margin-bottom:0'>
                                                            <h3 class="content-box-header bg-primary">
                                                                Benefits / Insurance

                                                            </h3>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Health Care Insurance:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="intellicare"><?php echo $emp['intellicare']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Other Insurance:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="pioneer"><?php echo $emp['pioneer']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">TIN/BIR Number:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="BIR"><?php echo $emp['BIR']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Philhealth Number:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="philhealth"><?php echo $emp['philhealth']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Pag-ibig Number:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="pagibig"><?php echo $emp['pagibig']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">SSS Number:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="sss"><?php echo $emp['sss']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab4">
                                                        <div class="content-box">
                                                            <h3 class="content-box-header bg-primary"
                                                                >
                                                                DTR Login details:
                                                            </h3>

                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Account:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="accountx" data-type="select" data-pk="1" data-value='<?php echo $account->acc_id; ?>'><?php
                                                                            if (!empty($account)) {
                                                                                echo $account->acc_name;
                                                                            }
                                                                            ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Username:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="dtr_uname"><?php echo $emp['username']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                            <div class="content-box-wrapper">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Password:</label>
                                                                    <div class="col-sm-8">
                                                                        <a href="#" id="dtr_password"><?php echo $emp['password']; ?></a>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <?php if ($pospay != '' || $pospay != NULL): ?>
                                                                <div class="content-box-wrapper">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label">Role: </label>
                                                                        <div class="col-sm-8">
                                                                            <a href="#" id="rolex" data-type="select" data-pk="1" data-value="<?php
                                                                            if ($emprole == NULL || $emprole == '') {
                                                                                echo '';
                                                                            } else {
                                                                                echo $emprole->role_id;
                                                                            }
                                                                            ?>" data-title="Select Role"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                        <?php if ($pospay != '' || $pospay != NULL): ?>
                                                            <div class="content-box">
                                                                <h3 class="content-box-header bg-primary">
                                                                    POSITION
                                                                </h3>
                                                                <div class="content-box-wrapper">
                                                                    <div class="example-box-wrapper">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <b>Code:</b><br><?php echo $pospay->pos_name; ?>
                                                                            </div>  
                                                                            <div class="col-md-6 text-left">
                                                                                <b> Description</b>:<br><?php echo $pospay->pos_details; ?>
                                                                            </div>  
                                                                            <div class="col-md-3 text-left">
                                                                                <b> Status</b>:<br><?php echo $pospay->status; ?>
                                                                            </div>         
                                                                        </div>                                                
                                                                    </div>

                                                                </div>

                                                                <div class="button-pane text-right">
                                                                    <?php if ($pospay->status == 'Trainee') { ?>
                                                                        <a href="#" class="btn btn-sm btn-info" id='trainee' data-toggle="modal" data-target="#traineemodal">
                                                                            <span class="glyph-icon icon-separator">
                                                                                <i class="glyph-icon icon-edit"></i>
                                                                            </span>
                                                                            <span class="button-content">
                                                                                Update Trainee
                                                                            </span>
                                                                        </a>
                                                                    <?php } else if ($pospay->status == 'Probationary') { ?>
                                                                        <a href="#" class="btn btn-sm btn-info" id='probationary' data-toggle="modal" data-target="#probationarymodal">
                                                                            <span class="glyph-icon icon-separator">
                                                                                <i class="glyph-icon icon-edit"></i>
                                                                            </span>
                                                                            <span class="button-content">
                                                                                Update Probationary
                                                                            </span>
                                                                        </a>
                                                                    <?php } else if ($pospay->status == 'Regular') { ?>
                                                                        <a href="#" class="btn btn-sm btn-info" id='regular' data-toggle="modal" data-target="#regularmodal">
                                                                            <span class="glyph-icon icon-separator">
                                                                                <i class="glyph-icon icon-edit"></i>
                                                                            </span>
                                                                            <span class="button-content">
                                                                                Promote Regular
                                                                            </span>
                                                                        </a>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class="content-box">
                                                                <h3 class="content-box-header bg-primary">
                                                                    Rates
                                                                </h3>
                                                                <div class="content-box-wrapper" id='ratelist'>

                                                                    <div class="example-box-wrapper">
                                                                        <div class='row'>

                                                                            <div class="col-md-3 text-center">
                                                                                <b> Monthly:</b><br> 
                                                                                <?php
                                                                                if ($userrole != NULL || $userrole != '') {
                                                                                    if ($userrole->description == "Administrator") {
                                                                                        echo "PHP " . $pospay->monthly;
                                                                                    } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
                                                                                        echo "PHP " . $pospay->monthly;
                                                                                    } else {
                                                                                        echo "--";
                                                                                    }
                                                                                } else {
                                                                                    echo '-';
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                            <div class="col-md-3 text-center">
                                                                                <b> Semi-Monthly:</b><br> 
                                                                                <?php
                                                                                if ($userrole != NULL || $userrole != '') {
                                                                                    if ($userrole->description == "Administrator") {
                                                                                        echo "PHP " . $pospay->quinsina;
                                                                                    } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
                                                                                        echo "PHP " . $pospay->quinsina;
                                                                                    } else {
                                                                                        echo "--";
                                                                                    }
                                                                                } else {
                                                                                    echo '-';
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                            <div class="col-md-3 text-center">
                                                                                <b> Daily:</b><br> 
                                                                                <?php
                                                                                if ($userrole != NULL || $userrole != '') {
                                                                                    if ($userrole->description == "Administrator") {
                                                                                        echo "PHP " . $pospay->daily;
                                                                                    } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
                                                                                        echo "PHP " . $pospay->daily;
                                                                                    } else {
                                                                                        echo "--";
                                                                                    }
                                                                                } else {
                                                                                    echo '-';
                                                                                }
                                                                                ?>

                                                                            </div>
                                                                            <div class="col-md-3 text-center">
                                                                                <b> Hourly:</b><br> 
                                                                                <?php
                                                                                if ($userrole != NULL || $userrole != '') {
                                                                                    if ($userrole->description == "Administrator") {
                                                                                        echo "PHP " . $pospay->hourly;
                                                                                    } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
                                                                                        echo "PHP " . $pospay->hourly;
                                                                                    } else {
                                                                                        echo "--";
                                                                                    }
                                                                                } else {
                                                                                    echo '-';
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="content-box">
                                                                <h3 class="content-box-header bg-primary">
                                                                    Salary Mode
                                                                </h3>
                                                                <div class="content-box-wrapper">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label">Salary Mode:</label>
                                                                        <div class="col-sm-8">
                                                                            <a href="#" id="salarymode" data-type="select" data-value="<?php echo $emp['salarymode']; ?>" data-pk="1" data-title="Select Salary Mode"><?php echo $emp['salarymode']; ?></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="content-box">
                                                                <h3 class="content-box-header bg-primary">
                                                                    Benefits and Taxes
                                                                    <span class='pull-right'>Semi-Monthly</span>
                                                                </h3>
                                                                <div class="content-box-wrapper" id='benefitlist'>
                                                                    <div class="form-group">
                                                                        <label class="col-md-5 control-label">Bureau of Internal Revenue</label>
                                                                        <div class="col-md-4">
                                                                            <a href="#" id="selecttax" data-type="select" data-pk="1" data-value="<?php
                                                                            if (!empty($taxemp)) {
                                                                                echo $taxemp->tax_desc_id;
                                                                            }
                                                                            ?>" data-title="Select Tax Category"></a>    
                                                                        </div>

                                                                        <?php if (!empty($pospay) && !empty($sss) && !empty($philhealth) && !empty($taxemp) && !empty($pagibig)) { ?>
                                                                            <div id='taxdisplay' class='col-md-3 text-right text-warning' style='font-weight: bold;'></div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <div class="content-box-wrapper">
                                                                    <div class="form-group">
                                                                        <label class="col-md-5 control-label">Social Security System</label>
                                                                        <?php if (!empty($pospay) && !empty($sss) && !empty($philhealth) && !empty($taxemp) && !empty($pagibig)) { ?>
                                                                            <div class='col-md-7 text-right text-warning' style='font-weight: bold;'> 
                                                                                <?php
                                                                                if ($userrole != NULL || $userrole != '') {
                                                                                    if ($userrole->description == "Administrator") {
                                                                                        if (empty($sss)) {
                                                                                            echo '--';
                                                                                        } else {
                                                                                            echo "PHP " . number_format($sss->sss_employee / 2, 2);
                                                                                        }
                                                                                    } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
                                                                                        if (empty($sss)) {
                                                                                            echo '--';
                                                                                        } else {
                                                                                            echo "PHP " . number_format($sss->sss_employee / 2, 2);
                                                                                        }
                                                                                    } else {
                                                                                        echo "--";
                                                                                    }
                                                                                } else {
                                                                                    echo '-';
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>

                                                                <div class="content-box-wrapper">
                                                                    <div class="form-group">
                                                                        <label class="col-md-5 control-label">Philhealth</label>
                                                                        <?php if (!empty($pospay) && !empty($sss) && !empty($philhealth) && !empty($taxemp) && !empty($pagibig)) { ?>
                                                                            <div class='col-md-7 text-right text-warning' style='font-weight: bold;'> 
                                                                                <?php
                                                                                if ($userrole != NULL || $userrole != '') {
                                                                                    if ($userrole->description == "Administrator") {
                                                                                        echo "PHP " . number_format($philhealth->employeeshare / 2, 2);
                                                                                    } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
                                                                                        echo "PHP " . number_format($philhealth->employeeshare / 2, 2);
                                                                                    } else {
                                                                                        echo "--";
                                                                                    }
                                                                                } else {
                                                                                    echo '-';
                                                                                }
                                                                                ?>

                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <div class="content-box-wrapper" style="padding:10px 25px">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Home Development Mutual Fund</label><br>
                                                                        <?php if (!empty($pospay) && !empty($sss) && !empty($philhealth) && !empty($taxemp) && !empty($pagibig)) { ?>
                                                                            <div class="row">
                                                                                <div class="col-md-8">
                                                                                    <h5 class="font-size-12 font-bold" style="color:#777">Basic Contribution</h5>
                                                                                    <p class="font-purple  font-size-12 font-bold" style="margin-bottom:10px">
                                                                                        <?php
                                                                                        if ($userrole != NULL || $userrole != '') {
                                                                                            if ($userrole->description == "Administrator") {
                                                                                                echo "PHP " . number_format(($pagibig->employeeshare / 2), 2);
                                                                                            } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
                                                                                                echo "PHP " . number_format(($pagibig->employeeshare / 2), 2);
                                                                                            } else {
                                                                                                echo "--";
                                                                                            }
                                                                                        } else {
                                                                                            echo '-';
                                                                                        }
                                                                                        ?>
                                                                                    </p>
                                                                                    <h5 class="font-size-12 font-bold" style="color:#777">Additional Contribution</h5>
                                                                                    <p class="font-purple font-size-12 font-bold">
                                                                                        <?php if ($userrole != NULL || $userrole != '') { ?>
                                                                                            <?php if ($userrole->description == "Administrator"): ?>
                                                                                                PHP <a href="#" id="addContrib"><?php echo ($pagibig->addContrib == NULL) ? '' : $pagibig->addContrib->amount; ?></a>
                                                                                            <?php elseif ($userrole->description == "Manager" && $emp['confidential'] == 0): ?>
                                                                                                PHP <a href="#" id="addContrib"><?php echo ($pagibig->addContrib == NULL) ? '' : $pagibig->addContrib->amount; ?></a>
                                                                                            <?php else: ?>
                                                                                                --
                                                                                            <?php endif; ?>

                                                                                            <?php
                                                                                        }else {
                                                                                            echo '-';
                                                                                        }
                                                                                        ?>
                                                                                    </p>

                                                                                </div>
                                                                                <div class="col-md-4 text-right text-warning font-bold" id="pagibigdisplay"><br>
                                                                                    <?php
                                                                                    if ($userrole != NULL || $userrole != '') {
                                                                                        if ($userrole->description == "Administrator") {
                                                                                            echo "PHP " . number_format(($pagibig->employeeshare / 2) + (($pagibig->addContrib == NULL) ? 0 : $pagibig->addContrib->amount), 2);
                                                                                        } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
                                                                                            echo "PHP " . number_format(($pagibig->employeeshare / 2) + (($pagibig->addContrib == NULL) ? 0 : $pagibig->addContrib->amount), 2);
                                                                                        } else {
                                                                                            echo "--";
                                                                                        }
                                                                                    } else {
                                                                                        echo '-';
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        <?php else: ?>
                                                            <hr>
                                                            <h5 class="text-danger text-center"><b>EMPLOYEE DATA INCOMPLETE.</b></h5>
                                                        <?php endif; ?>



                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div id="myModal" class="modal fade" role="dialog">
									<div class="modal-dialog modal-lg" style="width:60%">
										<div class="modal-content animated bounceInRight">
											<div class="modal-header" style="background: #2F4050;color: #fff;">
												<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
												 <h4 class="modal-title">Upload an Image</h4>
											 </div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-6">
													<h4>Uploaded Image</h4>
														<div class="image-crop">
														
																  <img alt='image' id="profilePic" class='img-responsive' width='100%' src='<?php echo base_url()."assets/images/".$emp["pic"]; ?>'>

															
														</div>
													</div>
													<div class="col-md-6">
														<h4>Preview image</h4>
														<div class="img-preview img-preview-sm img11" style="width: 350px;height: 350px;"></div>
													</div>
												</div>  
											</div>
											<div class="modal-footer" style="text-align: center;">
											   <div class="btn-group" >
													<label   id="" class="btn btn-danger" data-dismiss="modal">Cancel</label>
													<label title="Upload image file" for="inputImage" class="btn btn-warning">
														<input type="file" accept="image/*" name="file" id="inputImage" class="hide">
														Upload image
													</label>
													<label title="Donload image" id="download" class="btn btn btn-success" data-style="slide-up">Crop & Upload</label>
												</div>
											</div>
										</div>
									</div>
								</div>

                            <div class="col-md-4">
                                <div class='panel'>
                                    <div class="panel-body text-center">

                                        <img src="<?php echo base_url() . "assets/images/" . $emp['pic']; ?>" alt="Image not found" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/sz.png'); ?>';" class='img-thumbnail' style='border:2px solid #ddd;width:100%' " data-toggle="modal" data-target="#myModal"/>
										
                                        <hr>
                                        <div class="panel">
                                            <div class="panel-body">
                                                <h5 class="font-bold font-size-15"><?php echo $emp['lname']; ?>, <?php echo $emp['fname']; ?> <?php echo $emp['mname']; ?> <i> <?php
                                                        if ($emp['nikname'] != '') {
                                                            echo '"' . $emp['nikname'] . '"';
                                                        }
                                                        ?></i></h5>
                                                <p>Employee Name</p>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-body">
                                                <h5 class="font-bold font-size-15"><a href="#" id="idnumber"><?php echo $emp['id_num']; ?></a></h5>
                                                <p style='margin-top:5px'>ID Number</p>
                                            </div>
                                        </div>

                                        <div class="panel">
                                            <div class="panel-body">

                                                <h3 class="title-hero">Settings</h3>
                                                <button class="btn btn-alt btn-hover btn-danger" data-toggle="modal" data-target="#confimodal" value="Settings" 
                                                <?php
                                                if ($userrole != NULL || $userrole != '') {
                                                    if ($userrole->description != "Administrator") {
                                                        echo "style='display:none'";
                                                    }
                                                } else {
                                                    echo "style='display:none'";
                                                }
                                                ?>>
                                                    <span>Configure</span>
                                                    <i class="glyph-icon icon-arrow-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-body">
                                                <h3 class="title-hero">Application Details</h3>

                                                <button class="btn btn-alt btn-hover btn-info" data-toggle="modal" data-target="#viewApply" disabled>
                                                    <span>View</span>
                                                    <i class="glyph-icon icon-arrow-right"></i>
                                                </button>
                                                <button class="btn btn-alt btn-hover btn-warning" data-toggle="modal" data-target="#deactivatemodal" value="Settings">
                                                    <span>Deactivate</span>
                                                    <i class="glyph-icon icon-arrow-right"></i>
                                                </button>
                                            </div>
                                        </div>        

                                        <div class="panel">
                                            <div class="panel-body">
                                                <h3 class="title-hero">Incident Report Summary</h3>
                                                <button class="btn btn-alt btn-hover btn-primary" data-toggle="modal" data-target="#addIR" disabled>
                                                    <span>View Details</span>
                                                    <i class="glyph-icon icon-arrow-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="viewApply" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                            </div>
                            <div class="modal-body">
                                <p>Some text in the modal.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="deactivatemodal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">DEACTIVATE EMPLOYEE</h4>
                            </div>
                            <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-4 text-right font-bold">FULL NAME</div>
                                    <div class="col-md-8"><?php echo $emp['lname']; ?>, <?php echo $emp['fname']; ?> <?php echo $emp['mname']; ?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 text-right font-bold">CAMPAIGN/TEAM</div>
                                    <div class="col-md-8"><?php
                                        if (!empty($account)) {
                                            echo $account->acc_name;
                                        }
                                        ?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 text-right font-bold">TYPE OF RESIGNATION <br><span class='font-red font-bold font-size-11  ' id='notif_resign' style='display:none'>Required*</span></div>
                                    <div class="col-md-8 font-bold">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title font-size-13">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                            INVOLUNTARY
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class=" radio-danger">
                                                            <label> 
                                                                <input type="radio" id="radio1" name="example-radio" class="custom-radio" data-name='Terminated due to Attendance'>
                                                                Terminated due to Attendance
                                                            </label>
                                                        </div>
                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio2" name="example-radio" class="custom-radio" data-name='Terminated due to Performance'>
                                                                Terminated due to Performance
                                                            </label>
                                                        </div>
                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio3" name="example-radio" class="custom-radio" data-name='Client Decision due to Performance'>
                                                                Client Decision due to Performance
                                                            </label>
                                                        </div>
                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio4" name="example-radio" class="custom-radio" data-name='Client Decision due to Attendance'>
                                                                Client Decision due to Attendance
                                                            </label>
                                                        </div>
                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio5" name="example-radio" class="custom-radio" data-name='End of Training'>
                                                                End of Training
                                                            </label>
                                                        </div>
                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio6" name="example-radio" class="custom-radio" data-name='End of Contract'>
                                                                End of Contract
                                                            </label>
                                                        </div>
                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio7" name="example-radio" class="custom-radio" data-name='Failed Classroom Training'>
                                                                Failed Classroom Training
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title font-size-13">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                            VOLUNTARY
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio8" name="example-radio" class="custom-radio" data-name='Resigned Immediately'>
                                                                Resigned Immediately
                                                            </label>
                                                        </div>

                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio9" name="example-radio" class="custom-radio" data-name='Resigned Gracefully'>
                                                                Resigned Gracefully
                                                            </label>
                                                        </div>

                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio10" name="example-radio" class="custom-radio" data-name='Classroom Training No Show'>
                                                                Classroom Training No Show
                                                            </label>
                                                        </div>

                                                        <div class=" radio-danger">
                                                            <label>
                                                                <input type="radio" id="radio11" name="example-radio" class="custom-radio" data-name='Did not continue Classroom Training'>
                                                                Did not continue Classroom Training
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 text-right font-bold">RESIGNATION DETAILS <br>  <span class='font-red font-bold font-size-11 ' id='notif_details' style='display:none'>Required*</span></div>
                                    <div class="col-md-8">
                                        <textarea class='form-control' id='resigndetails'></textarea>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 text-right font-bold">SEPARATION DATE <br><span class='font-red font-bold font-size-11 ' id='notif_separation'  style='display:none'>Required*</span></div>
                                    <div class="col-md-8">
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon">
                                                <i class="glyph-icon icon-calendar"></i>
                                            </span>
                                            <input type="text" class="bootstrap-datepicker form-control" id='separationdate'>
                                        </div>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 text-right font-bold">PROCESS CLEARANCE <br><span class='font-red font-bold font-size-11 ' id='notif_clearance' style='display:none'>Required*</span></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class=" radio-info">
                                                    <label>
                                                        <input type="radio" id="radio12" name="clearance-radio" class="custom-radio" data-name='Yes'>
                                                        Yes
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class=" radio-danger">
                                                    <label>
                                                        <input type="radio" id="radio13" name="clearance-radio" class="custom-radio" data-name='No'>
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 text-right font-bold">ELIGIBLE FOR REHIRE <br><span class='font-red font-bold font-size-11 ' id='notif_rehire' style='display:none'>Required*</span></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class=" radio-info">
                                                    <label>
                                                        <input type="radio" id="radio14" name="rehire-radio" class="custom-radio" data-name='Yes'>
                                                        Yes
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class=" radio-danger">
                                                    <label>
                                                        <input type="radio" id="radio15" name="rehire-radio" class="custom-radio" data-name='No'>
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info" id='btn-deactivate'>Deactivate</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="confimodal" class="modal fade" role="dialog">
                    <div class="modal-dialog" style="width:350px">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">CONFIDENTIALITY SETTINGS</h4>
                            </div>
                            <div class="modal-body text-center">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading"><h5 class='font-bold font-italic'>Is this a confidential employee ?</h5></div>
                                            <div class="panel-body">
                                                <label class="control-label">

                                                </label>
                                                <input type="checkbox" data-on-color="info" data-off-color="danger" name="" class="input-switch" onchange='showPass()' data-size="small" data-on-text="Yes" data-off-text="No" id='confichecker' <?php
                                                if ($emp['confidential'] == 1) {
                                                    echo 'checked';
                                                }
                                                ?> >
                                            </div>
                                        </div>
                                    </div>

                                    <div id='confipassdiv' class='col-md-12' style='display:none;'>
                                        <label class="control-label">
                                            <h5>Enter Security Code:</h5>
                                        </label>
                                        <div class="input-group">
                                            <input class="form-control input-sm" id='confipass' type='text'></input>

                                            <div class="input-group-btn">
                                                <button id="confipasssubmit" type="submit" class="btn btn-warning btn-sm">Continue</button>
                                            </div>
                                        </div>
                                        <p id='confipassn' class='text-danger' >Required*.*</p><br>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div id="addIR" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                            </div>
                            <div class="modal-body">
                                <p>Some text in the modal.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div id='Templatedesign'>
                </div>
            </div>

            <div class="row hidden" >
                <div class="col-md-8">
                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="title-hero">
                                Recent sales activity
                            </h3>
                            <div class="example-box-wrapper">
                                <div id="data-example-1" class="mrg20B" style="width: 100%; height: 300px;"></div>
                            </div>
                        </div>
                    </div>




                    <div class="content-box " >
                        <h3 class="content-box-header bg-default">
                            <i class="glyph-icon icon-cog"></i>
                            Live server status
                            <span class="header-buttons-separator">
                                <a href="#" class="icon-separator">
                                    <i class="glyph-icon icon-question"></i>
                                </a>
                                <a href="#" class="icon-separator refresh-button" data-style="dark" data-theme="bg-white" data-opacity="40">
                                    <i class="glyph-icon icon-refresh"></i>
                                </a>
                                <a href="#" class="icon-separator remove-button" data-animation="flipOutX">
                                    <i class="glyph-icon icon-times"></i>
                                </a>
                            </span>
                        </h3>
                        <div class="content-box-wrapper">
                            <div id="data-example-3" style="width: 100%; height: 250px;"></div>
                        </div>
                    </div>

                </div>

            </div>
        </div>



    </div>
</div>
</div>

<div class="modal fade " id='traineemodal' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-="true">
    <div class="modal-dialog" style="width:35%;">
        <div class="modal-content">
            <div class="modal-header" style="background: #1f2e2e; color: white;">
                <button type="button" class="close" data-dismiss="modal" aria-="true">&times;</button>
                <h3 class="modal-title">Trainee Update</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">Action</label>
                    </div>
                    <div class="col-md-9">
                        <select class='form-control' id='traineeselect'>
                            <option value=''>--</option>
                            <option value='traineeprobi'>Probationary</option>
                            <option value='traineeextend'>Extend</option>
                            <option value='traineefail'>Fail</option>
                        </select>
                    </div>
                </div><hr>
                <div id='traineeprobibody' >
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Start Date</label>
                            <input type="text" id='traineestartdate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
                        </div>
                    </div>
                </div>
                <div id='traineeextendbody' >
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Extended Date</label>
                            <input type="text" id='traineeextenddate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
                        </div>
                        <div class="col-md-12">
                            <label class="control-label">Reason</label>
                            <textarea class='form-control' id='traineeextendreason'></textarea>
                        </div>
                    </div>
                </div>
                <div id='traineefailbody' >
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Date</label> 
                            <input type="text" id='traineefaildate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
                        </div>
                        <div class="col-md-12">
                            <label class="control-label">Reason</label>
                            <textarea class='form-control' id='traineefailreason'></textarea>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="modal-footer" id='footer'>
                <a href="#" class="btn bg-default"  data-dismiss="modal">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-times"></i>
                    </span>
                    <span class="button-content">
                        Close
                    </span>
                </a>
                <a href="#" class="btn btn-info" id='traineeprobiupdate' style="display:none;">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-thumbs-up"></i>
                    </span>
                    <span class="button-content">
                        Promote
                    </span>
                </a>
                <a href="#" class="btn btn-warning" id='traineeextendupdate' style="display:none;">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-sort"></i>
                    </span>
                    <span class="button-content">
                        Extend
                    </span>
                </a>
                <a href="#" class="btn btn-danger" id='traineefailupdate' style="display:none;">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-thumbs-down"></i>
                    </span>
                    <span class="button-content">
                        Fail
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id='probationarymodal' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-="true">
    <div class="modal-dialog" style="width:35%;">
        <div class="modal-content">
            <div class="modal-header" style="background: #1f2e2e; color: white;">
                <button type="button" class="close" data-dismiss="modal" aria-="true">&times;</button>
                <h3 class="modal-title">Probationary Update</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">Action</label>
                    </div>
                    <div class="col-md-9">
                        <select class='form-control' id='probiselect'>
                            <option value=''>--</option>
                            <option value='probiregular'>Regular</option>
                            <option value='probiextend'>Extend</option>
                            <option value='probifail'>Fail</option>
                        </select>
                    </div>
                </div><hr>
                <div id='probiregularbody' >
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Start Date</label>
                            <input type="text" id='probistartdate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
                        </div>
                    </div>
                </div>
                <div id='probiextendbody' >
                    <div class="row">
                        <div class="col-md-6"> 
                            <label class="control-label">Extended Date</label>
                            <input type="text" id='probiextenddate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
                        </div>
                        <div class="col-md-12">
                            <label class="control-label">Reason</label>
                            <textarea class='form-control' id='probiextendreason'></textarea>
                        </div>
                    </div>
                </div>
                <div id='probifailbody' >
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Date</label> 
                            <input type="text" id='probifaildate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
                        </div>
                        <div class="col-md-12">
                            <label class="control-label">Reason</label>
                            <textarea class='form-control' id='probifailreason'></textarea>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="modal-footer" id='footer'>
                <a href="#" class="btn bg-default"  data-dismiss="modal">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-times"></i>
                    </span>
                    <span class="button-content">
                        Close
                    </span>
                </a>
                <a href="#" class="btn btn-info" id='probiregularupdate' style="display:none;">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-thumbs-up"></i>
                    </span>
                    <span class="button-content">
                        Promote
                    </span>
                </a>
                <a href="#" class="btn btn-warning" id='probiextendupdate' style="display:none;">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-sort"></i>
                    </span>
                    <span class="button-content">
                        Extend
                    </span>
                </a>
                <a href="#" class="btn btn-danger" id='probifailupdate' style="display:none;">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-thumbs-down"></i>
                    </span>
                    <span class="button-content">
                        Fail
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id='regularmodal' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-="true">
    <div class="modal-dialog" style="width:35%;">
        <div class="modal-content">
            <div class="modal-header" style="background: #1f2e2e; color: white;">
                <button type="button" class="close" data-dismiss="modal" aria-="true">&times;</button>
                <h3 class="modal-title">Regular Promote</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <label class="control-label">Departments</label>
                        <select class='form-control' id='regulardept'>
                            <option value=''>--</option>
                            <?php foreach ($departments as $d): ?>
                                <option value='<?php echo $d->dep_id; ?>'><?php echo $d->dep_details; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Effectivity Date</label>
                        <input type="text" id='regulardate' class="bootstrap-datepicker form-control" data-date-format="mm/dd/yy" readonly="readonly">
                    </div>
                    <div class="col-md-12">
                        <label class="control-label">Position</label>
                        <select class='form-control' id='regularposition'></select>
                    </div>

                </div>
            </div>    
            <div class="modal-footer" id='footer'>
                <a href="#" class="btn bg-default"  data-dismiss="modal">
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-times"></i>
                    </span>
                    <span class="button-content">
                        Close
                    </span>
                </a>
                <a href="#" class="btn btn-info" id='regularupdate'>
                    <span class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-thumbs-up"></i>
                    </span>
                    <span class="button-content">
                        Promote
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<p class="growl1" ></p>
<p class="growl2" ></p>
<p class="growl3" ></p>

<!-- JS Demo -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
</div>
</body>
<script>
	$(function () {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
			cache: false,
			success: function (html) {

				$('#headerLeft').html(html);
				//alert(html);
			}
		});
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
			cache: false,
			success: function (html) {

				$('#sidebar-menu').html(html);
				// alert(html);
			}
		});
		//Start Image upload cover	
			
	});
</script>
<script type="text/javascript">
        $(function () {
		var image1 = $("#profilePic");
            $(image1).cropper({
                aspectRatio: 1,
                preview: ".img11",
                done: function(data) {
                    // Output the result data for cropping image.
                }
			});
		
            var inputImage = $("#inputImage");
            if (window.FileReader) {
                inputImage.change(function() {
                    var fileReader = new FileReader(),
                            files = this.files,
                            file;

                    if (!files.length) {
                        return;
                    }

                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                           inputImage.val("");
                            image1.cropper("reset", true).cropper("replace", this.result);
                        };
                    } else {
                        showMessage("Please choose an image file.");
                    }
                });
            } else {
                inputImage.addClass("hide");
            }
			//End image upload cover
				$("#download").click(function() {
                // window.open(image1.cropper("getDataURL"));
						    $.ajax({
							type: "POST",
							url: "<?php echo base_url(); ?>index.php/employee/uploadPic",
							data: {
							  employee_ID: <?php echo $emp['apid']; ?>,
							  img: $("#profilePic").cropper("getDataURL"),
							 },
							cache: false,
							success: function(html)
							{
								 location.reload();
								 
							}
						  });   
						  
				
				});	
		
		
<?php if (!empty($pospay) && !empty($sss) && !empty($philhealth) && !empty($taxemp) && !empty($pagibig)): ?>
                var salary = parseFloat('<?php echo $pospay->quinsina; ?>');
                salary = salary - parseFloat('<?php echo $sss->sss_employee / 2; ?>') - parseFloat('<?php echo $philhealth->employeeshare / 2; ?>')
                        - parseFloat('<?php echo $pagibig->employeeshare / 2; ?>');
                if (<?php echo $taxemp->tax_desc_id; ?> == '' ||<?php echo $taxemp->tax_desc_id; ?> == 'NULL') {

                    $("#taxdisplay").html("");
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Employee/getTaxEmployeeShare/" +<?php echo $taxemp->tax_desc_id; ?>,
                        data: {
                            salary: salary
                        },
                        cache: false,
                        success: function (res) {
    <?php
    if ($userrole != NULL || $userrole != '') {
        if ($userrole->description == "Administrator") {
            ?>
                                    $("#taxdisplay").html("PHP " + parseFloat(res).toFixed(2));
        <?php } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
            ?>
                                    $("#taxdisplay").html("PHP " + parseFloat(res).toFixed(2));
        <?php } else { ?>
                                    $("#taxdisplay").html("--");

            <?php
        }
    } else {
        ?>
                                $("#taxdisplay").html("-");
    <?php } ?>
                        }
                    })
                }
<?php endif; ?>
        });

</script>
<script>
        $(function () {
            $("#addContrib").editable({type: "text",
                pk: 1,
                name: "addContrib",
                title: "Enter Additional Contribution",
                mode: "inline",
                tpl: "<input type='text' style='width: 100px'>",
                validate: function (value) {
                    if ($.isNumeric(value) === '') {
                        return 'Only numbers are allowed';
                    }
                }}),
                    $("#idnumber").editable({type: "text", pk: 1, name: "idnumber", title: "Enter ID Number", mode: "inline", tpl: "<input type='text' style='width: 100px'>"}),
                    $("#fname").editable({type: "text", pk: 1, name: "fname", title: "Enter First Name", mode: "inline"}),
                    $("#mname").editable({type: "text", pk: 1, name: "mname", title: "Enter Middle Name", mode: "inline"}),
                    $("#lname").editable({type: "text", pk: 1, name: "lname", title: "Enter Last Name", mode: "inline"}),
                    $("#nikname").editable({type: "text", pk: 1, name: "nikname", title: "Enter Nick Name", mode: "inline"}),
                    $("#bday").editable({type: "date", pk: 1, name: "birthday", title: "Enter Birth Day", mode: "inline", combodate: {minYear: <?php echo date('Y', strtotime('-100 years')); ?>, maxYear: <?php echo date("Y"); ?>}}),
                    $("#cell").editable({type: "text", pk: 1, name: "cell", title: "Enter Contact #", mode: "inline"}),
                    $("#tel").editable({type: "text", pk: 1, name: "tel", title: "Enter Alt Contact #", mode: "inline"}),
                    $("#email").editable({type: "text", pk: 1, name: "email", title: "Enter Email", mode: "inline"}),
                    $("#school_name").editable({type: "text", pk: 1, name: "school_name", title: "Enter School Name", mode: "inline"}),
                    $("#course").editable({type: "text", pk: 1, name: "course", title: "Enter Course", mode: "inline"}),
                    $("#school_note").editable({type: "text", pk: 1, name: "school_note", title: "Enter Remarks", mode: "inline"}),
                    $("#emergency_fname").editable({type: "text", pk: 1, name: "emergency_fname", title: "Enter First Name", mode: "inline"}),
                    $("#emergency_mname").editable({type: "text", pk: 1, name: "emergency_mname", title: "Enter Middle Name", mode: "inline"}),
                    $("#emergency_lname").editable({type: "text", pk: 1, name: "emergency_lname", title: "Enter Last Name", mode: "inline"}),
                    $("#emergency_contact").editable({type: "text", pk: 1, name: "emergency_contact", title: "Enter Contact", mode: "inline"}),
                    $("#ccaExp").editable({type: "text", pk: 1, name: "ccaExp", title: "Enter CC Experience", mode: "inline"}),
                    $("#other_latestEmp").editable({type: "text", pk: 1, name: "other_latestEmp", title: "Enter Latest Employer", mode: "inline"}),
                    $("#other_lastPosition").editable({type: "text", pk: 1, name: "other_lastPosition", title: "Enter Last Held Position", mode: "inline"}),
                    $("#other_inclusiveDate").editable({type: "text", pk: 1, name: "other_inclusiveDate", title: "Enter Year Left", mode: "inline"}),
                    $("#intellicare").editable({type: "text", pk: 1, name: "intellicare", title: "Enter Health Care Insurance", mode: "inline"}),
                    $("#pioneer").editable({type: "text", pk: 1, name: "pioneer", title: "Enter Other Insurance", mode: "inline"}),
                    $("#BIR").editable({type: "text", pk: 1, name: "BIR", title: "Enter TIN/BIR Number", mode: "inline"}),
                    $("#philhealth").editable({type: "text", pk: 1, name: "philhealth", title: "Enter Philhealth Number", mode: "inline"}),
                    $("#pagibig").editable({type: "text", pk: 1, name: "pagibig", title: "Enter Pag-ibig Number", mode: "inline"}),
                    $("#sss").editable({type: "text", pk: 1, name: "sss", title: "Enter SSS Number", mode: "inline"}),
                    $("#dtr_uname").editable({type: "text", pk: 1, name: "dtr_uname", title: "Enter Username", mode: "inline"}),
                    $("#dtr_password").editable({type: "text", pk: 1, name: "dtr_password", title: "Enter Password", mode: "inline"}),
                    $("#anniversary").editable({type: "date", pk: 1, name: "anniversary", title: "Enter Anniversary Day", mode: "inline", combodate: {minYear: <?php echo date('Y', strtotime('-100 years')); ?>, maxYear: <?php echo date("Y"); ?>}})
        });

</script>
<script type="text/javascript">
        $(function () {
            $("#civil").editable({
                mode: "inline", name: "civil",
                source:
                        [
                            {value: 'Single', text: 'Single'},
                            {value: 'Married', text: 'Married'},
                            {value: 'Legally Separated', text: 'Legally Separated'},
                            {value: 'Widower', text: 'Widower'}
                        ]

            })
                    .on('save', function (e, params) {
                        updater('tbl_applicant', 'civilStatus', params.newValue);
                    });
            $("#gender").editable({
                mode: "inline", name: "gender",
                source:
                        [
                            {value: 'Male', text: 'Male'},
                            {value: 'Female', text: 'Female'}
                        ]

            })
                    .on('save', function (e, params) {
                        updater('tbl_applicant', 'gender', params.newValue);
                    });
            $("#bloodtype").editable({
                mode: "inline", name: "gender",
                source:
                        [
                            {value: 'O', text: 'O'},
                            {value: 'A', text: 'A'},
                            {value: 'B', text: 'B'},
                            {value: 'AB', text: 'AB'}
                        ]

            })
                    .on('save', function (e, params) {
                        updater('tbl_employee', 'blood', params.newValue);
                    });
            $("#religion").editable({
                mode: "inline", name: "religion",
                source:
                        [
                            {value: 'Roman Catholic', text: 'Roman Catholic'},
                            {value: 'Muslim', text: 'Muslim'},
                            {value: 'Protestant', text: 'Protestant'},
                            {value: 'Iglesia ni Cristo', text: 'Iglesia ni Cristo'},
                            {value: 'El Shaddai', text: 'El Shaddai'},
                            {value: 'Jehovahs Witnesses', text: 'Jehovah’s Witnesses'}
                        ]

            })
                    .on('save', function (e, params) {
                        updater('tbl_applicant', 'relijon', params.newValue);
                    });
            $("#salarymode").editable({
                mode: "inline", name: "salarymode",
                source:
                        [
                            {value: 'Monthly', text: 'Monthly'},
                            {value: 'Daily', text: 'Daily'}
                        ]

            })
                    .on('save', function (e, params) {
                        updater('tbl_employee', 'salarymode', params.newValue);
                        location.reload();
                    });

            $("#accountx").editable({
                mode: "inline", name: "accountx",
                source:<?php echo $accountlist; ?>
            })
                    .on('save', function (e, params) {
                        updater('tbl_employee', 'acc_id', params.newValue);
                    });



        })
</script>
<script type="text/javascript">
        $(function () {
            $("#rolex").editable({
                mode: "inline",
                source: <?php echo $roles; ?>
            }).on('save', function (e, params) {
                unamepass('role_id', params.newValue);
            });
        });
</script>
<script>
        $(function () {
            $("#selecttax").editable({
                mode: "inline",
                source:
                        // {value:1,text:"Male"},
                        // {value:2,text:"Female"}
<?php echo $taxdesc; ?>

            }).on('save', function (e, params) {

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/Employee/insertNewTaxValue",
                    data: {
                        taxid: params.newValue,
                        emp_id: "<?php echo $emp['emp_id']; ?>"
                    },
                    cache: false,
                    success: function (result) {
                        if (result == 'Success') {
                            $(".growl1").trigger('select');
                        } else {
                            $(".growl2").trigger('select');
                        }
                    }
                });
<?php if (!empty($pospay) && !empty($sss) && !empty($philhealth) && !empty($taxemp) && !empty($pagibig)): ?>
                    var salary = <?php echo $pospay->quinsina; ?>;
                    salary = salary - parseFloat(<?php echo $sss->sss_employee / 2; ?>) - parseFloat(<?php echo $philhealth->employeeshare / 2; ?>)
                            - parseFloat(<?php echo $pagibig->employeeshare / 2; ?>);
                    // alert(salary);
                    if (params.newValue == null || params.newValue == '') {
                        $("#taxdisplay").html("");
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>index.php/Employee/getTaxEmployeeShare/" + params.newValue,
                            data: {
                                salary: salary
                            },
                            cache: false,
                            success: function (res) {
    <?php
    if ($userrole != NULL || $userrole != '') {
        if ($userrole->description == "Administrator") {
            ?>
                                        $("#taxdisplay").html("PHP " + parseFloat(res).toFixed(2));
        <?php } else if ($userrole->description == "Manager" && $emp['confidential'] == 0) {
            ?>
                                        $("#taxdisplay").html("PHP " + parseFloat(res).toFixed(2));
        <?php } else { ?>
                                        $("#taxdisplay").html("--");

            <?php
        }
    } else {
        ?>
                                    $("#taxdisplay").html("-");
    <?php } ?>

                            }
                        })
                    }
                    ;
<?php endif; ?>
            })

        });
</script>
<script type="text/javascript">
        $(".growl1").select(function () {
            $.jGrowl(
                    "Successfully Updated.",
                    {sticky: !1, position: "top-right", theme: "bg-blue-alt"}
            )
        });
        $(".growl2").select(function () {
            $.jGrowl(
                    "Error Updating.",
                    {sticky: !1, position: "top-right", theme: "bg-red"}
            )
        });
        $(".growl3").select(function () {
            $.jGrowl(
                    "Security Code Mismatch.",
                    {sticky: !1, position: "top-right", theme: "bg-red"}
            )
        });
        function updater(table, name, value) {

            if (table == 'tbl_employee') {
                var id = "<?php echo $emp['emp_id']; ?>";
                var idname = 'emp_id';
            } else if (table == 'tbl_applicant') {
                var id = "<?php echo $emp['apid']; ?>";
                var idname = 'apid';
            }
            if (name == 'birthday' || name == 'anniversary') {
                value = moment(value).format('YYYY-MM-DD');
                alert(value);
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Employee/UpdateEmployeeValue",
                data: {
                    id: id,
                    idname: idname,
                    table: table,
                    name: name,
                    value: value
                },
                cache: false,
                success: function (res) {
                    res = $.trim(res);
                    if (res == 'Success') {
                        $(".growl1").trigger('select');
                    } else {
                        $(".growl2").trigger('select');
                    }

                }
            });


        }
        function unamepass(name, value) {
            var empid = "<?php echo $emp['emp_id']; ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Employee/intoTblUser",
                data: {
                    empid: empid,
                    name: name,
                    value: value
                },
                cache: false,
                success: function (res) {
                    if (res == 'Success') {
                        $(".growl1").trigger('select');
                    } else {
                        $(".growl2").trigger('select');
                    }
                }
            });

        }
</script>
<script type="text/javascript">
        $("#addContrib").on('save', function (e, params) {
            var emp_id = "<?php echo $emp['emp_id']; ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Employee/updatePagibigContrib",
                data: {
                    amount: params.newValue,
                    emp_id: emp_id
                },
                cache: false,
                success: function (result) {
                    result = result.trim();
                    if (result.status == 'Failed') {
                        $(".growl2").trigger('select');
                    } else {
                        result = JSON.parse(result);
                        $(".growl1").trigger('select');
                        var pagibigshare = parseFloat("<?php echo $pagibig->employeeshare / 2; ?>");
                        var total = pagibigshare + parseFloat(result.amount);
                        alert(result.amount);
                        alert(total);
                        $("#pagibigdisplay").html("<br> PHP " + parseFloat(total).toFixed(2));
//                        echo "PHP ".number_format(($pagibig - > employeeshare / 2) + (($pagibig - > addContrib == NULL) ? 0 : $pagibig - > addContrib - > amount), 2);
                    }
                }
            });
        });
</script>
<script type="text/javascript">

        $("#idnumber").on('save', function (e, params) {
            updater('tbl_employee', 'id_num', params.newValue);
        }),
                $("#fname").on('save', function (e, params) {
            updater('tbl_applicant', 'fname', params.newValue);
        }),
                $("#mname").on('save', function (e, params) {
            updater('tbl_applicant', 'mname', params.newValue);
        }),
                $("#lname").on('save', function (e, params) {
            updater('tbl_applicant', 'lname', params.newValue);
        }),
                $("#nikname").on('save', function (e, params) {
            updater('tbl_employee', 'nikname', params.newValue);
        }),
                $("#bday").on('save', function (e, params) {
            updater('tbl_applicant', 'birthday', params.newValue);
        }),
                $("#cell").on('save', function (e, params) {
            updater('tbl_applicant', 'cell', params.newValue);
        }),
                $("#tel").on('save', function (e, params) {
            updater('tbl_applicant', 'tel', params.newValue);
        }),
                $("#email").on('save', function (e, params) {
            updater('tbl_employee', 'email', params.newValue);
        }),
                $("#school_name").on('save', function (e, params) {
            updater('tbl_employee', 'school_name', params.newValue);
        }),
                $("#course").on('save', function (e, params) {
            updater('tbl_employee', 'course', params.newValue);
        }),
                $("#school_note").on('save', function (e, params) {
            updater('tbl_employee', 'school_note', params.newValue);
        }),
                $("#emergency_fname").on('save', function (e, params) {
            updater('tbl_employee', 'emergency_fname', params.newValue);
        }),
                $("#emergency_mname").on('save', function (e, params) {
            updater('tbl_employee', 'emergency_mname', params.newValue);
        }),
                $("#emergency_lname").on('save', function (e, params) {
            updater('tbl_employee', 'emergency_lname', params.newValue);
        }),
                $("#emergency_contact").on('save', function (e, params) {
            updater('tbl_employee', 'emergency_contact', params.newValue);
        }),
                $("#ccaExp").on('save', function (e, params) {
            updater('tbl_applicant', 'ccaExp', params.newValue);
        }),
                $("#other_latestEmp").on('save', function (e, params) {
            updater('tbl_employee', 'other_latestEmp', params.newValue);
        }),
                $("#other_lastPosition").on('save', function (e, params) {
            updater('tbl_employee', 'other_lastPosition', params.newValue);
        }),
                $("#other_inclusiveDate").on('save', function (e, params) {
            updater('tbl_employee', 'other_inclusiveDate', params.newValue);
        }),
                $("#intellicare").on('save', function (e, params) {
            updater('tbl_employee', 'intellicare', params.newValue);
        }),
                $("#pioneer").on('save', function (e, params) {
            updater('tbl_employee', 'pioneer', params.newValue);
        }),
                $("#BIR").on('save', function (e, params) {
            updater('tbl_employee', 'BIR', params.newValue);
        }),
                $("#philhealth").on('save', function (e, params) {
            updater('tbl_employee', 'philhealth', params.newValue);
        }),
                $("#pagibig").on('save', function (e, params) {
            updater('tbl_employee', 'pagibig', params.newValue);
        }),
                $("#sss").on('save', function (e, params) {
            updater('tbl_employee', 'sss', params.newValue);
        }),
                $("#dtr_uname").on('save', function (e, params) {
            updater('tbl_employee', 'dtr_uname', params.newValue);
            unamepass('username', params.newValue);
        }),
                $("#dtr_password").on('save', function (e, params) {
            updater('tbl_employee', 'dtr_password', params.newValue);
            unamepass('password', params.newValue);
        }),
                $("#anniversary").on('save', function (e, params) {
            updater('tbl_employee', 'anniversary', params.newValue);
        })

</script>
<script type="text/javascript">
        $("#supName").on('change', function () {
            var supemail = $("#supEmail").val();
            var supid = $("#supID").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Employee/UpdateEmployeeSupervisor",
                data: {
                    emp_id: "<?php echo $emp['emp_id']; ?>",
                    email: supemail,
                    supervisor: supid
                },
                cache: false,
                success: function (res) {
                    // alert(res);
                    if (res == 'Success') {
                        $(".growl1").trigger('select');
                    } else {
                        $(".growl2").trigger('select');
                    }

                }
            });
        });
</script>
<script type="text/javascript">
        $(document).ready(function () {
            $("#traineeselect").on('change', function () {

                var select = $(this).val();
                // alert(select);
                if (select == 'traineeprobi') {
                    $("#traineeprobibody").show();
                    $("#traineeextendbody").hide();
                    $("#traineefailbody").hide();

                    $("#traineeprobiupdate").show();
                    $("#traineeextendupdate").hide();
                    $("#traineefailupdate").hide();
                } else if (select == 'traineeextend') {
                    $("#traineeprobibody").hide();
                    $("#traineeextendbody").show();
                    $("#traineefailbody").hide();


                    $("#traineeprobiupdate").hide();
                    $("#traineeextendupdate").show();
                    $("#traineefailupdate").hide();
                } else if (select == 'traineefail') {
                    $("#traineeprobibody").hide();
                    $("#traineeextendbody").hide();
                    $("#traineefailbody").show();

                    $("#traineeprobiupdate").hide();
                    $("#traineeextendupdate").hide();
                    $("#traineefailupdate").show();
                } else {
                    $("#traineeprobibody").hide();
                    $("#traineeextendbody").hide();
                    $("#traineefailbody").hide();

                    $("#traineeprobiupdate").hide();
                    $("#traineeextendupdate").hide();
                    $("#traineefailupdate").hide();
                }

            });

            $("#traineeprobiupdate").click(function () {
                var startdate = $("#traineestartdate").val();
                if (startdate == '') {
                    alert('StartDate Empty');
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Employee/traineeprobiupdate",
                        data: {
                            startdate: startdate,
                            emp_id: "<?php echo $emp['emp_id']; ?>",
                            pos_id: "<?php echo $pospay->pos_id; ?>",
                            class: "<?php echo $pospay->class; ?>"
                        },
                        cache: false,
                        success: function (res) {
                            if (res == 'Success') {
                                location.reload();
                            } else if (res == 'NoRate') {
                                swal("Oops!", "Please assign <b>RATE</b> to next position to continue", "error");
                            } else {
                                swal("Oops!", "An Error Occured while processing data.", "error");
                            }
                        }
                    })
                }
            });

            $("#traineeextendupdate").click(function () {
                var extenddate = $("#traineeextenddate").val();
                var reason = $("#traineeextendreason").val();
                if (extenddate == '') {
                    alert('Date Empty');
                }
                if (reason == '') {
                    alert('Reason Empty');
                }
                if (extenddate != '' && reason != '') {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Employee/extendupdate",
                        data: {
                            extenddate: extenddate,
                            reason: reason,
                            emp_id: "<?php echo $emp['emp_id']; ?>",
                            pos_id: "<?php echo $pospay->pos_id; ?>"
                        },
                        cache: false,
                        success: function (res) {
                            if (res == 'Success') {
                                location.reload();
                            } else if (res == 'NoRate') {
                                swal("Oops!", "Please assign <b>RATE</b> to next position to continue", "error");
                            } else {
                                swal("Oops!", "An Error Occured while processing data.", "error");
                            }
                        }
                    })
                }
            });

            $("#traineefailupdate").click(function () {
                var faildate = $("#traineefaildate").val();
                var reason = $("#traineefailreason").val();
                if (faildate == '') {
                    alert('Date Empty');
                }
                if (reason == '') {
                    alert('Reason Empty');
                }
                if (faildate != '' && reason != '') {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Employee/failupdate",
                        data: {
                            faildate: faildate,
                            reason: reason,
                            emp_id: "<?php echo $emp['emp_id']; ?>",
                            pos_id: "<?php echo $pospay->pos_id; ?>"
                        },
                        cache: false,
                        success: function (res) {
                            if (res == 'Success') {
                                window.location = "<?php echo base_url(); ?>index.php/Employee";
                            } else if (res == 'NoRate') {
                                swal("Oops!", "Please assign <b>RATE</b> to next position to continue", "error");
                            } else {
                                swal("Oops!", "An Error Occured while processing data.", "error");
                            }
                        }
                    })
                }
            });
        });
</script>

<script type="text/javascript">
        $(document).ready(function () {
            $("#probiselect").on('change', function () {

                var select = $(this).val();
                // alert(select);
                if (select == 'probiregular') {
                    $("#probiregularbody").show();
                    $("#probiextendbody").hide();
                    $("#probifailbody").hide();

                    $("#probiregularupdate").show();
                    $("#probiextendupdate").hide();
                    $("#probifailupdate").hide();
                } else if (select == 'probiextend') {
                    $("#probiregularbody").hide();
                    $("#probiextendbody").show();
                    $("#probifailbody").hide();


                    $("#probiregularupdate").hide();
                    $("#probiextendupdate").show();
                    $("#probifailupdate").hide();
                } else if (select == 'probifail') {
                    $("#probiregularbody").hide();
                    $("#probiextendbody").hide();
                    $("#probifailbody").show();

                    $("#probiregularupdate").hide();
                    $("#probiextendupdate").hide();
                    $("#probifailupdate").show();
                } else {
                    $("#probiregularbody").hide();
                    $("#probiextendbody").hide();
                    $("#probifailbody").hide();

                    $("#probiregularupdate").hide();
                    $("#probiextendupdate").hide();
                    $("#probifailupdate").hide();
                }

            });

            $("#probiregularupdate").click(function () {
                var startdate = $("#probistartdate").val();

                if (startdate == '') {
                    alert('StartDate Empty');
                } else {
                    if ("<?php echo $pospay->class; ?>" == 'Agent') {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>index.php/Employee/probiregularupdatecca",
                            data: {
                                startdate: startdate,
                                emp_id: "<?php echo $emp['emp_id']; ?>"
                            },
                            cache: false,
                            success: function (res) {
                                if (res == 'Success') {
                                    location.reload();
                                } else if (res == 'NoRate') {
                                    swal("Oops!", "Please assign <b>RATE</b> to next position to continue", "error");
                                } else {
                                    swal("Oops!", "An Error Occured while processing data.", "error");
                                }
                            }
                        })
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>index.php/Employee/probiregularupdate",
                            data: {
                                startdate: startdate,
                                emp_id: "<?php echo $emp['emp_id']; ?>",
                                pos_id: "<?php echo $pospay->pos_id; ?>"
                            },
                            cache: false,
                            success: function (res) {
                                if (res == 'Success') {
                                    location.reload();
                                } else if (res == 'NoRate') {
                                    swal("Oops!", "Please assign <b>RATE</b> to next position to continue", "error");
                                } else {
                                    swal("Oops!", "An Error Occured while processing data.", "error");
                                }
                            }
                        })
                    }

                }
            });

            $("#probiextendupdate").click(function () {
                var extenddate = $("#probiextenddate").val();
                var reason = $("#probiextendreason").val();
                if (extenddate == '') {
                    alert('Date Empty');
                }
                if (reason == '') {
                    alert('Reason Empty');
                }
                if (extenddate != '' && reason != '') {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Employee/extendupdate",
                        data: {
                            extenddate: extenddate,
                            reason: reason,
                            emp_id: "<?php echo $emp['emp_id']; ?>",
                            pos_id: "<?php echo $pospay->pos_id; ?>"
                        },
                        cache: false,
                        success: function (res) {
                            if (res == 'Success') {
                                location.reload();
                            } else if (res == 'NoRate') {
                                swal("Oops!", "Please assign <b>RATE</b> to next position to continue", "error");
                            } else {
                                swal("Oops!", "An Error Occured while processing data.", "error");
                            }
                        }
                    })
                }
            });

            $("#probifailupdate").click(function () {
                var faildate = $("#probifaildate").val();
                var reason = $("#probifailreason").val();
                if (faildate == '') {
                    alert('Date Empty');
                }
                if (reason == '') {
                    alert('Reason Empty');
                }
                if (faildate != '' && reason != '') {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Employee/failupdate",
                        data: {
                            faildate: faildate,
                            reason: reason,
                            emp_id: "<?php echo $emp['emp_id']; ?>",
                            pos_id: "<?php echo $pospay->pos_id; ?>"
                        },
                        cache: false,
                        success: function (res) {
                            if (res == 'Success') {
                                window.location = "<?php echo base_url(); ?>index.php/Employee";
                            } else if (res == 'NoRate') {
                                swal("Oops!", "Please assign <b>RATE</b> to next position to continue", "error");
                            } else {
                                swal("Oops!", "An Error Occured while processing data.", "error");
                            }
                        }
                    })
                }
            });
        });
</script>
<script type="text/javascript">
        $(document).ready(function () {
            // console.log("pangit:"+positions);
            $("#regulardept").on('change', function () {
                var deptid = $(this).val();
                $('#regularposition').html("<option value=''>--</option>");

                var positions = <?php echo json_encode($positions); ?>;
                $.each(positions, function (i, elem) {
                    // console.log("ID:"+elem);
                    if (elem.dep_id == deptid) {
                        $('#regularposition').append('<option value="' + elem.pos_id + '">' + elem.pos_details + '</option>');

                    }
                })

            })
            $("#regularupdate").click(function () {
                var posid = $("#regularposition").val();
                var date = $("#regulardate").val();
                if (posid == '') {
                    alert('Position Empty');
                }
                if (date == '') {
                    alert('Date Empty');
                }
                if (posid != '' && date != '') {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Employee/regularupdate",
                        data: {
                            posid: posid,
                            date: date,
                            empid: "<?php echo $emp['emp_id']; ?>"
                        },
                        cache: false,
                        success: function (res) {
                            if (res == 'Success') {
                                location.reload();
                            } else if (res == 'NoRate') {
                                swal("Oops!", "Please assign <b>RATE</b> to next position to continue", "error");
                            } else {
                                swal("Oops!", "An Error Occured while processing data.", "error");
                            }
                        }
                    })
                }
            })
        })

</script>
<script type="text/javascript">

        function setActiveness(id) {
            if ($("#confichecker").is(":checked")) {
                var active = 1;
                // alert('active');
            } else {
                var active = 0;
                // alert('inactive');
            }

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Employee/updateConfidentiality",
                data: {
                    emp_id: id,
                    activevalue: active
                },
                cache: false,
                success: function (res) {
                    if (res == 'Success') {
                        $(".growl1").trigger('select');
                    } else {
                        $(".growl2").trigger('select');
                    }
                }
            });
        }
</script>
<script type="text/javascript">
        function showPass() {
            $("#confipassdiv").show();
            // if ($("#confichecker").is(":checked")){
            //     var active = 1;
            //     alert('active');
            // }else{
            //     var active = 0;
            //     alert('inactive');
            // }
            // $("#confi").val(active);
        }
        $("#confipasssubmit").click(function () {
            var password = $("#confipass").val();
            if (password != '') {

                $("#confipassn").hide();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/Employee/matchUserCode",
                    data: {
                        password: password
                    },
                    cache: false,
                    success: function (res) {
                        if (res == 'Exists') {
                            setActiveness("<?php echo $emp['emp_id']; ?>");
                            // $('#confimodal').modal('toggle');
                            $("#confipass").val('');
                            location.reload();
                        } else {
                            $(".growl3").trigger('select');
                        }
                    }
                });

            } else {
                $("#confipassn").show();
            }
        })

</script>
<script type="text/javascript">
        $("#deactivatemodal").on('show.bs.modal', function () {
            $('[id^=uniform]').css('margin-top', '5px');
        });
</script>
<script type="text/javascript">
        $("#btn-deactivate").click(function () {
            var resigntype = $('input[name=example-radio]:checked').data('name');
            var rehire = $('input[name=rehire-radio]:checked').data('name');
            var clearance = $('input[name=clearance-radio]:checked').data('name');
            var separationdate = $("#separationdate").val();
            var resigndetails = $("#resigndetails").val();
            if (resigntype == undefined) {
                $("#notif_resign").show();
            } else {
                $("#notif_resign").hide();
            }
            if (rehire == undefined) {
                $("#notif_rehire").show();
            } else {
                $("#notif_rehire").hide();
            }
            if (clearance == undefined) {
                $("#notif_clearance").show();
            } else {
                $("#notif_clearance").hide();
            }
            if (separationdate == '') {
                $("#notif_separation").show();
            } else {
                $("#notif_separation").hide();
            }
            if (resigndetails == '') {
                $("#notif_details").show();
            } else {
                $("#notif_details").hide();
            }
            if (resigntype != undefined && rehire != undefined && clearance != undefined && separationdate != '' && resigndetails != '') {
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to restore the account!",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Deactivate Employee!',
                    cancelButtonText: 'No, Cancel',
                    confirmButtonClass: 'btn btn-danger',
                    cancelButtonClass: 'btn btn-default',
                    buttonsStyling: false
                }).then(function () {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Employee/deactivateEmployee",
                        data: {
                            empid: "<?php echo $emp['emp_id']; ?>",
                            resigntype: resigntype,
                            rehire: rehire,
                            clearance: clearance,
                            separationdate: separationdate,
                            resigndetails: resigndetails
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            if (res == 'Success') {
                                swal('Success!!', 'Successfully Deactivated Employee.', 'success');
                                setTimeout(function () {
                                    location.href = "<?php echo base_url(); ?>index.php/Employee";
                                }, 2000);
                            } else {

                                swal('Error!', 'Failed Deactivating Employee.', 'error');
                                setTimeout(function () {
                                    location.reload();
                                }, 2000);
                            }
                        }
                    });
                }, function (dismiss) {
                    // dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                    if (dismiss === 'cancel') {
                        swal(
                                'Cancelled',
                                'Deactivation  was Cancelled',
                                'error'
                                )
                    }
                })
            }

        });
		
</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->
</html>