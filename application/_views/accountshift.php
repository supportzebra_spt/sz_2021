<!DOCTYPE html> 
<html  lang="en">

    <!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:00 GMT -->
    <head>

        <style>
            /* Loading Spinner */
            .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
            body { padding-right: 0 !important }
        </style>


        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title> Monarch UI - Bootstrap Frontend &amp; Admin Template </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicons -->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icons/favicon.png">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/fontawesome/fontawesome.css"> -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icons/typicons/typicons.css">

        <!-- JS Core -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/js-core.js"></script>

        <script type="text/javascript">
                $(window).load(function () {
                    setTimeout(function () {
                        $('#loading').fadeOut(400, "linear");
                    }, 300);
                });
        </script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/daterangepicker/moment.js"></script>
        <!-- Bootstrap Timepicker -->

        <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/timepicker/timepicker.css">-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/widgets/timepicker/timepicker.js"></script>
        <script type="text/javascript">

                /* Timepicker */

                $(function () {
                    "use strict";
                    $('.timepicker-example').timepicker({
                        defaultTime: '12:00 AM'
                    });
                });
        </script>
    </head>

    <body>
        <div id="sb-site">
            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <div id="page-header" class="bg-black">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="index-2.html" class="logo-content-small" title="MonarchUI"></a>
                    </div>
                    <div id="header-logo" class="logo-bg">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" style='width:90%'>
                        <a id="close-sidebar" href="#" title="Close sidebar">
                            <i class="glyph-icon icon-angle-left"></i>
                        </a>
                    </div>
                    <div id='headerLeft'>
                    </div>

                </div>
                <div id="page-sidebar">
                    <div class="scroll-sidebar">


                        <ul id="sidebar-menu">
                        </ul>
                    </div>
                </div>
                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div id="page-title">
                            <h2>Account Shift</h2><hr>
                            <div class="row text-left bordered-row">
                                <div class="col-md-9">
                                    <div class="panel">
                                        <div class="panel-body row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" style="margin-top: 8px;">TYPE: </label>
                                                    <div class="col-sm-9">
                                                        <select class='form-control' id='type'>
                                                            <option value=''>--</option>
                                                            <option value='Admin'>Admin</option>
                                                            <option value='Agent'>Agent</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" style="margin-top: 8px;">ACCOUNT: </label>
                                                    <div class="col-sm-9">
                                                        <select class='form-control' id='account'></select> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="panel">
                                        <div class="panel-body text-center">
                                            <button class="btn btn-info" id='addbtn' data-toggle="modal" data-target="#shiftmodal">
                                                <span class="glyph-icon icon-separator">
                                                    <i class="glyph-icon icon-plus"></i>
                                                </span>
                                                <span class="button-content">
                                                    Create Shift
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading font-bold font-blue">LIST OF SHIFTS</div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class='glyph-icon icon-search'></i></span>
                                                        <input disabled type="text" id="search1" onkeyup="myFunction1()" class="form-control" placeholder="Search for shifts...">  
                                                    </div>
                                                </div>
                                            </div><br>

                                            <table class='table table-hover table-bordered table-condensed' id='table1'>
                                                <thead>
                                                    <tr>
                                                        <th class='text-center'>SHIFT</th>
                                                        <th class='text-center'>FIRST BREAK</th>
                                                        <th class='text-center'>LUNCH BREAK</th>
                                                        <th class='text-center'>LAST BREAK</th>
                                                        <th class='text-center'>ACTIONS</th>
                                                    </tr>
                                                </thead>
                                                <tbody id='shiftlist'></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="shiftmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">CREATE SHIFT</h4>
                </div>
                <div class="modal-body">
                    <div class="panel">
                        <div class="panel-heading">
                            SHIFT TIME
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label" style='margin-top: 8px;'>START TIME:</label>
                                        <div class="col-sm-7">
                                            <div class="bootstrap-timepicker dropdown">
                                                <input class="timepicker-example form-control" type="text" id='time_start'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label" style='margin-top: 8px;'>END TIME:</label>
                                        <div class="col-sm-7">
                                            <div class="bootstrap-timepicker dropdown">
                                                <input class="timepicker-example form-control" type="text" id='time_end'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading">
                            DEFAULT SHIFT BREAK
                        </div>
                        <div class="panel-body">
                            <table class='table table-bordered'>

                                <thead>
                                <th>First Break</th>
                                <th>Lunch Break</th>
                                <th>Last Break</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><select name="firstbreak" id="ifirstbreak" class='form-control'></select></td>
                                        <td><select name="lunchbreak" id="ilunchbreak" class='form-control'></select></td>
                                        <td><select name="lastbreak" id="ilastbreak" class='form-control'></select></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id='insertbtn'>Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">UPDATE BREAKS</h4>
                </div>
                <div class="modal-body">
                    <h3 id='displayshift' class='text-center'></h3>
                    <br />
                    <table class='table table-bordered'>

                        <thead>
                        <th>First Break</th>
                        <th>Lunch Break</th>
                        <th>Last Break</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td><select name="firstbreak" id="firstbreak" class='form-control'></select></td>
                                <td><select name="lunchbreak" id="lunchbreak" class='form-control'></select></td>
                                <td><select name="lastbreak" id="lastbreak" class='form-control'></select></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-warning" id='updatebtn'>Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- JS Demo -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets-minified/admin-all-demo.js"></script>
</body>
<script>
</script>    
<script>
        $(function () {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/index.php/Templatedesign/headerLeft",
                cache: false,
                success: function (html) {

                    $('#headerLeft').html(html);
                    //alert(html);
                }
            });
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/index.php/Templatedesign/sideBar",
                cache: false,
                success: function (html) {

                    $('#sidebar-menu').html(html);
                    // alert(html);
                }
            });
        });</script>
<script type="text/javascript">
        $(function () {
            $("#type").on('change', function () {
                $("#shiftlist").html("");
                $("#addbreakbtn").prop('disabled', true);
                $("#search1").prop('disabled', true);
                if ($(this).val() === 'Admin') {
                    var account = <?php echo $admins; ?>;
                    $("#account").html('');
                    var options = "<option value=''>--</option>";
                    $.each(account, function (i, item) {
                        options += "<option value='" + item.acc_id + "'>" + item.acc_name + "</option>";
                    });
                    $("#account").html(options);
                } else if ($(this).val() === 'Agent') {
                    var account = <?php echo $agents; ?>;
                    $("#account").html('');
                    var options = "<option value=''>--</option>";
                    $.each(account, function (i, item) {
                        options += "<option value='" + item.acc_id + "'>" + item.acc_name + "</option>";
                    });
                    $("#account").html(options);
                } else {
                    $("#account").html('');
                }
            });
        });</script>
<script type="text/javascript">
        $(function () {
            $("#account").on('change', function () {

                $("#shiftlist").html("");
                $("#breaklist").html("");
                var acc_id = $(this).val();
                if (acc_id === '') {
                    $("#addbreakbtn").prop('disabled', true);
                    $("#search1").prop('disabled', true);
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Schedule/getAccShifts",
                        data: {
                            acc_id: acc_id
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            res = JSON.parse(res);
                            var options = "";
                            $.each(res, function (i, item) {
                                options += "<tr>";
                                options += "<td  class='text-center'>" + item.time_start + " - " + item.time_end + "</td>";
                                if (item.firstbreak === '' || item.firstbreak === null) {
                                    options += "<td></td>";
                                } else {
                                    options += "<td class='text-center'>" + item.firstbreak.min + " minutes</td>";
                                }
                                if (item.lunchbreak === '' || item.lunchbreak === null) {
                                    options += "<td></td>";
                                } else {
                                    options += "<td class='text-center'>" + parseInt(parseInt(item.lunchbreak.min) + parseInt(item.lunchbreak.hour * 60)) + " minutes</td>";
                                }
                                if (item.lastbreak === '' || item.lastbreak === null) {
                                    options += "<td></td>";
                                } else {
                                    options += "<td class='text-center'>" + item.lastbreak.min + " minutes</td>";
                                }

                                options += "<td class='text-center'><a href='' data-toggle='modal' data-target='#updatemodal' data-acc_time_id='" + item.acc_time_id + "' data-shift='" + item.time_start + " - " + item.time_end + "' class='btn btn-xs btn-warning'><span class='glyph-icon icon-separator'><i class='glyph-icon icon-pencil'></i></span><span class='button-content'></span>Update</a></td>";
                                options += "</tr>";
                            });
                            $("#shiftlist").html(options);
                            $("#savebtn").data('accid', acc_id);
                            $("#search1").prop('disabled', false);
                        }
                    });
                }
            });
        });</script>
<script>
        function myFunction1() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("search1");
            filter = input.value.toUpperCase();
            table = document.getElementById("table1");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
</script>
<script type="text/javascript">
        $("#updatemodal").on('show.bs.modal', function (e) {
            var acc_time_id = $(e.relatedTarget).data('acc_time_id');
            var shift = $(e.relatedTarget).data('shift');
            $("#updatebtn").data('acc_time_id', acc_time_id);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/index.php/Schedule/getAvailableAccTimeBreak",
                cache: false,
                success: function (res2) {
                    res2 = res2.trim();
                    res2 = JSON.parse(res2);
                    var fb = "<option value=''>--</option>";
                    var l = "<option value=''>--</option>";
                    var lb = "<option value=''>--</option>";
                    $("#firstbreak").html("");
                    $("#lunchbreak").html("");
                    $("#lastbreak").html("");
                    if (res2.firstbreak !== null) {
                        $.each(res2.firstbreak, function (x, xitem) {
                            fb += "<option value='" + xitem.bta_id + "'>" + xitem.min + " mins</option>";
                        });
                    }
                    if (res2.lunchbreak !== null) {
                        $.each(res2.lunchbreak, function (x, xitem) {
                            l += "<option value='" + xitem.bta_id + "'>" + parseInt(parseInt(xitem.min) + parseInt(xitem.hour * 60)) + " mins</option>";
                        });
                    }
                    if (res2.lastbreak !== null) {
                        $.each(res2.lastbreak, function (x, xitem) {
                            lb += "<option value='" + xitem.bta_id + "'>" + xitem.min + " mins</option>";
                        });
                    }
                    $("#firstbreak").html(fb);
                    $("#lunchbreak").html(l);
                    $("#lastbreak").html(lb);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Schedule/getAccTimeBreak",
                        data: {
                            acc_time_id: acc_time_id
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            res = JSON.parse(res);
                            if (res.firstbreak !== null) {
                                $("#firstbreak").val(res.firstbreak.bta_id);
                            }
                            if (res.lunchbreak !== null) {
                                $("#lunchbreak").val(res.lunchbreak.bta_id);
                            }
                            if (res.lastbreak !== null) {
                                $("#lastbreak").val(res.lastbreak.bta_id);
                            }
                            $("#displayshift").html(shift);
                        }
                    });
                }
            });
        });</script>
<script type="text/javascript">
        $("#updatebtn").click(function () {
            var acc_time_id = $(this).data('acc_time_id');
            var fb = $("#firstbreak").val();
            var l = $("#lunchbreak").val();
            var lb = $("#lastbreak").val();
            if (fb === '' && l === '' && lb === '') {
                swal({
                    title: 'NO BREAKS',
                    text: "Are you sure this shift has no breaks?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, I am sure'
                }).then(() => {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Schedule/updateAccTimeBreak",
                        data: {
                            fb: fb,
                            l: l,
                            lb: lb,
                            acc_time_id: acc_time_id
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            if (res === 'Success') {
                                swal("Success", 'Successfully updated shift breaks', 'success');
                            } else {
                                swal("Failed", 'Failed to update shift breaks', 'error');
                            }
                            $("#account").change();
                            $("#updatemodal .close").click();
                        }
                    });
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>/index.php/Schedule/updateAccTimeBreak",
                    data: {
                        fb: fb,
                        l: l,
                        lb: lb,
                        acc_time_id: acc_time_id
                    },
                    cache: false,
                    success: function (res) {
                        res = res.trim();
                        if (res === 'Success') {
                            swal("Success", 'Successfully updated shift breaks', 'success');
                        } else {
                            swal("Failed", 'Failed to update shift breaks', 'error');
                        }
                        $("#account").change();
                        $("#updatemodal .close").click();
                    }
                });
            }
        });</script>
<script type="text/javascript">
        $("#shiftmodal").on('show.bs.modal', function (e) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/index.php/Schedule/getAvailableAccTimeBreak",
                cache: false,
                success: function (res2) {
                    res2 = res2.trim();
                    res2 = JSON.parse(res2);
                    var fb = "<option value=''>--</option>";
                    var l = "<option value=''>--</option>";
                    var lb = "<option value=''>--</option>";
                    $("#ifirstbreak").html("");
                    $("#ilunchbreak").html("");
                    $("#ilastbreak").html("");
                    if (res2.firstbreak !== null) {
                        $.each(res2.firstbreak, function (x, xitem) {
                            fb += "<option value='" + xitem.bta_id + "'>" + xitem.min + " mins</option>";
                        });
                    }
                    if (res2.lunchbreak !== null) {
                        $.each(res2.lunchbreak, function (x, xitem) {
                            l += "<option value='" + xitem.bta_id + "'>" + parseInt(parseInt(xitem.min) + parseInt(xitem.hour * 60)) + " mins</option>";
                        });
                    }
                    if (res2.lastbreak !== null) {
                        $.each(res2.lastbreak, function (x, xitem) {
                            lb += "<option value='" + xitem.bta_id + "'>" + xitem.min + " mins</option>";
                        });
                    }
                    $("#ifirstbreak").html(fb);
                    $("#ilunchbreak").html(l);
                    $("#ilastbreak").html(lb);
                }
            });
        });</script>
<script type="text/javascript">
        $("#insertbtn").click(function () {
            var fb = $("#ifirstbreak").val();
            var l = $("#ilunchbreak").val();
            var lb = $("#ilastbreak").val();
            var time_start = $("#time_start").val();
            var time_end = $("#time_end").val();
            var isExists = $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Schedule/checkTimeExists",
                data: {
                    time_start: time_start,
                    time_end: time_end
                },
                cache: false,
                global: false,
                async: false,
                success: function (isExists) {
                    return isExists;
                }
            }).responseText;
            if(isExists){
                swal('Shift Exists', 'Cannot create shift.', 'error');
            }else if (time_start === time_end) {
                swal('24 hour shift', 'Cannot create a 24 hours shift.', 'error');
            } else {
                var ts = time_start.split(" ");
                var te = time_end.split(" ");
                var datestart = moment().format('L');

                ///---------------------
                var st = moment(new Date(datestart + " " + time_start));
                var en = moment(new Date(datestart + " " + time_end));
                if (en - st < 0) {
                    var dateend = moment().add(1, 'days').format('L');
//                    alert("Next Day");
                } else {
                    var dateend = moment().format('L');
//                    alert("Same Day");
                }
                //-----------------------


                var datestart = new Date(datestart + " " + time_start);
                var dateend = new Date(dateend + " " + time_end);
                var difference = moment(dateend).valueOf() - moment(datestart).valueOf();
                difference = difference / (60 * 60 * 1000);
                var hrs = parseInt(Number(difference));
                var min = Math.round((Number(difference) - hrs) * 60);
                if (min === 0) {
                    var disp = hrs + " hrs";
                } else if (hrs === 0) {
                    var disp = min + " mins";
                } else {
                    var disp = hrs + ' hrs & ' + min + " mins";
                }

                if (difference > 9) {
                    swal({
                        title: 'Shift is greater than 9 hours',
                        html: "Shift covers <b>" + disp + "</b> of work. <br>Continue to create shift?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, continue'
                    }).then(() => {
                        saver();
                    });

                } else if (difference < 3) {
                    swal({
                        title: 'Shift is lesser than 3 hours',
                        html: "Shift covers <b>" + disp + "</b> of work.  <br>Continue to create shift?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, continue'
                    }).then(() => {
                        saver();
                    });
                } else {
                    saver();
                }
            }

            function saver() {
                if (fb === '' && l === '' && lb === '') {
                    swal({
                        title: 'NO BREAKS',
                        text: "Are you sure that this shift has no default breaks?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, I am sure'
                    }).then(() => {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>/index.php/Schedule/generateAccTimeDetails",
                            data: {
                                fb: fb,
                                l: l,
                                lb: lb,
                                time_start: time_start,
                                time_end: time_end
                            },
                            cache: false,
                            success: function (res) {
                                res = res.trim();
                                if (res === 'Success') {
                                    swal("Success", 'Successfully Created Shift in all accounts', 'success');
                                } else {
                                    swal("Failed", 'Failed to create shift to all accounts', 'error');
                                }
                                $("#account").val('').change();
                                $("#shiftmodal .close").click();
                            }
                        });
                    });
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>/index.php/Schedule/generateAccTimeDetails",
                        data: {
                            fb: fb,
                            l: l,
                            lb: lb,
                            time_start: time_start,
                            time_end: time_end
                        },
                        cache: false,
                        success: function (res) {
                            res = res.trim();
                            if (res === 'Success') {
                                swal("Success", 'Successfully Created Shift in all accounts', 'success');
                            } else {
                                swal("Failed", 'Failed to create shift to all accounts', 'error');
                            }
                            $("#account").val('').change();
                            $("#shiftmodal .close").click();
                        }
                    });
                }
            }
        });</script>
<!-- Mirrored from agileui.com/demo/monarch/demo/admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Aug 2016 15:19:47 GMT -->

</html>