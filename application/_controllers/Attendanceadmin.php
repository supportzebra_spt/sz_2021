<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AttendanceAdmin extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	
	public function index(){
				// $query = $this->db->query("select b.apid,fname,lname,emp_id from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes'");
				// $query = $this->db->query("select distinct(b.emp_id),b.apid,fname,lname,pos_name from tbl_applicant a,tbl_employee b,tbl_position d  where a.apid=b.apid  and a.pos_id = d.pos_id and b.isActive='yes' and d.class='Agent' and b.isActive='yes' order by lname");
				// $query = $this->db->query("select b.emp_id,b.apid,fname,lname,pos_name from tbl_applicant a,tbl_employee b,tbl_position d  where a.apid=b.apid  and a.pos_id = d.pos_id and b.isActive='yes' and d.class='Agent' and b.isActive='yes' order by lname");
				  // $query = $this->db->query("select b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and d.isActive=1 and status='Probationary' or status='regular' order by lname ");
				 $query = $this->db->query("select f.rate_id,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and d.isActive=1 and e.class='Admin'  and b.isActive='yes' and f.isactive=1 order by lname");
		foreach ($query->result() as $row)
			{
				$arr['employee'][$row->apid] = array(
				"apid" => $row->apid,
 				"fname" => $row->fname,
 				"lname" => $row->lname,
 				"emp_id" => $row->emp_id,
  				);
			}
	if($this->session->userdata('uid')){
 		 
  					 
					$this->load->view('attendance_daily',$arr);
				 
			 
		}else{
					redirect('index.php/login');
		}
	
	
		}
		public function emptype(){
		 $payrollC = $this->input->post('payrollC');
		 $employz = $this->input->post('employz');
		  $date = explode("-",$payrollC);
		   	  
		  $query = $this->db->query("select b.apid,fname,lname,emp_id from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes' and b.emp_id in (".$employz.")");
 		  foreach ($query->result() as $row){
			$DateRangeShift = $this->DateRangeShift($date);
				foreach($DateRangeShift as $row1 => $data){
 					$shift = $this->empshift($row->emp_id,$data);
					foreach($shift as $row2 => $data2){
						$in = $this->empshiftin($row->emp_id,$data2->sched_id);
						$out = $this->empshiftout($row->emp_id,$data2->sched_id);
						$emplogsShift2 = $this->emplogsShift($data2->sched_id,$row->emp_id);
						$empRate2 = $this->empRate($row->emp_id);
						
						$timeschedule = $this->timeschedule($data2->sched_id);
							$NDbegin = "10:10:00 PM";
							$NDend   = "6:00:00 AM";
							
							$HPbegin = "12:00:00 AM";
							$HPend   = "3:00:00 AM";
							
							$HPbegin2 = "2:00:00 PM";
							$HPend2   = "11:59:59 PM";
							
							$HPbegin3 = "4:00:00 AM";
							$HPend3   = "5:00:00 AM";
							 $ndRemark = "N";
							
						if($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double"){
							if(count($in)>0){
							$shiftBreak=  $this->shiftBrk($in[0]->acc_time_id);
							
							$to_time= strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login)));
							$from_time = strtotime($data." ".$timeschedule[0]->time_start);
								$NDdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
								$NDdate2 = DateTime::createFromFormat('H:i a', $NDbegin);
								$NDdate3 = DateTime::createFromFormat('H:i a', $NDend);

								$HPdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
								$HPdate2 = DateTime::createFromFormat('H:i a', $HPbegin);
								$HPdate3 = DateTime::createFromFormat('H:i a', $HPend);
								
								$HPdate4 = DateTime::createFromFormat('H:i a', $HPbegin2);
								$HPdate5 = DateTime::createFromFormat('H:i a', $HPend2);
								
								$HPdate6 = DateTime::createFromFormat('H:i a', $HPbegin3);
								$HPdate7 = DateTime::createFromFormat('H:i a', $HPend3);
								if ($NDdate1 >= $NDdate2 or $NDdate1 <= $NDdate3){
								   $ndRemark = "Y";
								}else{
								   $ndRemark= "N";
								}
								if (($HPdate1 >= $HPdate4 and $HPdate1 <= $HPdate5) or ($HPdate1 >= $HPdate6 and $HPdate1 <= $HPdate7) ){
											$hpRemark = "Y";
											$hpRate = '0.08';
								}else if(($HPdate1 >= $HPdate2) and ($HPdate1 <= $HPdate3)){
										   $hpRemark = "Y";
											$hpRate = '0.10';
								}else{
											$hpRemark= "N";
											 $hpRate = '';	
								}
								$time_startNEWW  = ($timeschedule[0]->time_start  == "12:00:00 AM" ) ? "11:59:59 PM" : $timeschedule[0]->time_start;
								$ndTimeStarted = $data." ".$time_startNEWW;
								if($timeschedule[0]->time_start == "12:00:00 AM"){
									 // if(date('h:i:s a', strtotime($in[0]->login))>date('h:i:s a', strtotime("12:00:00 am"))){
									if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) >= strtotime(date('Y-m-d H:i:s', strtotime($data." 24:00:00")) )){

														 // $data= date('Y-m-d', strtotime('-1 day', strtotime($data)));
														 $from_time = strtotime($data." 24:00:00");
														$timediff = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
															$late = (round(abs($from_time - $to_time) / 60,2));
															$latemins = (round(abs($from_time - $to_time) / 60,2));

												 }else{
														 
														$from_time = strtotime($data." 24:00:00");
														$timediff = round(abs($from_time - $to_time) / 60,2)." mins. early";
															$latemins = 0;
															$late = 0;
												 }												 
											 }else{
											if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) >= strtotime(date('Y-m-d H:i:s', strtotime($data." ".$timeschedule[0]->time_start))) ){
										 
											$timediff = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
											$late=(round(abs($from_time - $to_time) / 60,2));
											$latemins = (round(abs($from_time - $to_time) / 60,2));
										}else{
											$timediff = round(abs($from_time - $to_time) / 60,2)." mins. early";
											$late=0;
											$latemins=0;
										}
											 }
								
								
								$BCTSched = date('H:i:00', strtotime('-10 mins', strtotime($timeschedule[0]->time_start)));
								$bct = ($data." ".$BCTSched.":00" > date('Y-m-d H:i:00', strtotime($in[0]->login)) ) ? "Q" : "DQ";
							}else{
								$latemins=0;
								$late=0;
								$timediff = "<span style='color:red'><b>Absent</b></span>";
								$bct = "<span style='color:red'>DQ</span>";
								$hpRemark= "N";
								$hpRate = '';	
							}
							if((count($emplogsShift2)>0)){
								
								$finalND = explode("-",$emplogsShift2[0]->nd); 
								// $finalND2 = (count($emplogsShift2[0]->nd)>0) ? $finalND[0]+($finalND[1]/60) : 0; 
								$finalND2 = (count($emplogsShift2[0]->nd)>0 and !empty($emplogsShift2[0]->nd)) ? $finalND[0]+($finalND[1]/60) : 0; 

							}else{
								$finalND2=0;
							}
							$totalShiftBreaks = (count($in) >0) ? $shiftBreak[0]->hr + $shiftBreak[0]->min  : 0;

						}else{
								$latemins=0;
								$late=0;
								$time_startNEWW=0;
								$ndTimeStarted=0;
								$finalND2=0;
								$timediff = "<span style='color:red'>--</span>";
								$hpRemark= "N";
								$hpRate = '';	
								$bct  = "DQ";
								$totalShiftBreaks = 0;
						}
 
							// $logoutt =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? $out[0]->logout : $data." 00:00:00" : $data." 00:00:00";
							$logouttTotalHours =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? date_format(date_create($out[0]->logout),"Y-m-d")." ".$timeschedule[0]->time_end : $data." 00:00:00" : $data." 00:00:00";
							$logouttDTRID =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? $out[0]->dtr_id : $data." 00:00:00" : $data." 00:00:00";
 							  // $loginn =  (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? $data." ".date_format(date_create($in[0]->login),"H:i:00") : $data." 00:00:00" : $data." 00:00:00"; 
 
							$logouttTotalHours =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date_format(date_create($out[0]->logout),"Y-m-d")." ".$timeschedule[0]->time_end : $data." 00:00:00" : $data." 00:00:00";
						    $logoutt =  (count($out)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $out[0]->logout : $data." 00:00:00") : $data." 00:00:00";
						
				
						 $logouttDTRID =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $out[0]->dtr_id : $data." 00:00:00" : $data." 00:00:00";
 						 // $loginn =  (count($in)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? (($latemins>0) ? $in[0]->login: $data." ".$time_startNEWW) : $data." 00:00:00") : $data." 00:00:00";
 						 $loginn =  (count($in)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? (($latemins>0) ? $in[0]->login : date('Y-m-d h:i:00 a', strtotime($data." ".$time_startNEWW))) : $data." 00:00:00") : $data." 00:00:00";

							$loginnTotalHours =  (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? $data." ".$time_startNEWW  : $data." 00:00:00" : $data." 00:00:00";
 
							$loginnNew =  (count($in)>0) ? (($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? ((strtotime($in[0]->login)> strtotime($loginnTotalHours) ) ?  date('Y-m-d H:i:00 ', strtotime($in[0]->login))  : $loginnTotalHours) : $data." 00:00:00") : $data." 00:00:00";
 
						$loginnDTRID =  (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $in[0]->dtr_id : $data." 00:00:00" : $data." 00:00:00";
						$logoutt2 =  ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_end : "00:00:00";
						$loginn2 =  ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_start : "00:00:00";
 							$loginn2_timeID =  ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_id : "0";
							$breaks = $this->breakIdentify($loginnDTRID,$logouttDTRID,$row->emp_id);
							$loginn2timeID= $this->loginn2timeID($loginn2_timeID);
							$loginnNdtimeID= $this->loginnNdtimeID($loginn2_timeID);
							$shiftTotalHr= $this->shiftTotalHr($loginn2_timeID);
							$from_timeLog1 = strtotime($loginn); 
							$to_timeLog1 = strtotime($logoutt);
							$resLog1 = round(abs(($to_timeLog1-$from_timeLog1)/3600),2);
							$shiftTotalHours = (count($shiftTotalHr)>0) ? $shiftTotalHr[0]->ho_end_time + $shiftTotalHr[0]->ho_start_time : 0;
							$logouttNew =  (count($out)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? (strtotime($out[0]->logout)>=(strtotime($logouttTotalHours )) ? $logouttTotalHours :  $out[0]->logout) : $data." 00:00:00") : $data." 00:00:00";


							$isLogoutHoliday2 = $this->isLogoutHoliday($logoutt,$logoutt2);
							$isLoginHoliday2 = $this->isLoginHoliday($loginn,$loginn2);
							 $TotalAmountHoliday1=0;
								$TotalAmountHoliday2=0;	
								$hrly=0;	
								$HolidayND=0;	
								$HolidayHP=0;	
								$NDComputeHolidayLogout=0;	
								$HPComputeHolidayLogout=0;	
								$NDComputeHolidayLogin=0;	
								$HPComputeHolidayLogin=0;	
								$HolidayType1 =0;
								$HolidayType2=0;
								$time_startNEWW=0;


					if(count($isLogoutHoliday2[0]->date)>0){
							// $HolidayType = ($isLogoutHoliday2[0]->type=='Regular') ? 1 : .30;
							$HolidayType =$isLogoutHoliday2[0]->calc;
						if(count($loginn2timeID)>0){
							if(count($in)>0){
							$isLogoutHoliday1  =  number_format(($loginn2timeID[0]->ho_end_time*$HolidayType),2);
								// $TotalAmountHoliday1 = (((($empRate2[0]->rate*12)/261)/8)*$HolidayType)*$isLogoutHoliday1;  
							$NDComputeHolidayLogout =  (isset($loginnNdtimeID[0]->nd_time_end)) ? number_format(($loginnNdtimeID[0]->nd_time_end*$HolidayType),2) : 0;
							$HPComputeHolidayLogout =  (isset($loginnNdtimeID[0]->nd_time_end)) ? number_format(($loginnNdtimeID[0]->nd_time_end*$HolidayType),2) : 0;
							$HolidayType2 =  $isLogoutHoliday2[0]->type;
							}else{
							$isLogoutHoliday1  =  0 ;
							}
						}else{
							$isLogoutHoliday1  =  0 ;
						}
					}else{
					 $isLogoutHoliday1  =  0;
					}
					if(count($isLoginHoliday2[0]->date)>0){
						// $HolidayType = ($isLoginHoliday2[0]->type=='Regular') ? 1 : .30;
						$HolidayType = $isLoginHoliday2[0]->calc;

					if(count($loginn2timeID)>0){
						if(count($in)>0){
						$isLoginHoliday1  =  number_format(($loginn2timeID[0]->ho_start_time*$HolidayType),2);
							//$TotalAmountHoliday2 = (((($empRate2[0]->rate*12)/261)/8)*$HolidayType)*$isLoginHoliday1;  
						 $NDComputeHolidayLogin = (isset($loginnNdtimeID[0]->nd_time_start)) ? number_format(($loginnNdtimeID[0]->nd_time_start*$HolidayType),2)  : 0;
						$HPComputeHolidayLogin = (isset($loginnNdtimeID[0]->nd_time_start)) ?number_format(($loginnNdtimeID[0]->nd_time_start*$HolidayType),2) : 0;
						$HolidayType1 =  $isLoginHoliday2[0]->type;
						}else{
						$isLoginHoliday1  =  0 ;
						}
					}else{
						$isLoginHoliday1  =  0 ;
					}
				  }else{
					 $isLoginHoliday1  =  0;
				  }
				$shiftTotalHours = (count($shiftTotalHr)>0) ? $shiftTotalHr[0]->ho_end_time + $shiftTotalHr[0]->ho_start_time : 0;
				$shiftTotalHoursWithBreaks = (count($shiftTotalHr)>0) ? $shiftTotalHr[0]->ho_end_time + $shiftTotalHr[0]->ho_start_time+($totalShiftBreaks/60) : 0;
					$from_timeLog1 = strtotime($loginnNew);
					$to_timeLog1 = strtotime($logouttNew);
					$from_timeLog = strtotime($loginnTotalHours);
					$to_timeLog = strtotime($logouttTotalHours);
					$from_timeLog_NEW = strtotime($loginnNew); 
					$to_timeLogNEW = strtotime($logouttNew);
				$resLog1 = round(abs(($to_timeLog1-$from_timeLog1)/3600),2); //totalwork
				$resLog = round(abs(($to_timeLog-$from_timeLog)/3600),2);//total
				$resLogNEW = round(abs(($to_timeLogNEW-$from_timeLog_NEW)/3600),2); //resLogNEW
				
				if($finalND2!=0){
					if ($resLog1<$shiftTotalHours){
							$start = strtotime($ndTimeStarted);
							$end = strtotime($logouttNew);
							$brk = (count($breaks)>0) ? (($breaks[0]->hr >=1) ? 30 : $breaks[0]->hr*60)+$breaks[0]->min : 0;
							$NDuT = round(abs(($end-$start)/3600),2); //ND if UT
							$NDuT -= $brk/60; //ND if UT
						}elseif($resLog1==$shiftTotalHours){
							$NDuT = $finalND2; 

						}else{
							$NDuT = $finalND2;
						}
				}else{
						$NDuT = 0;
				}
						$arr["employee"][$row->emp_id][$data2->sched_id."-".$data2->sched_date]= array(
							"fname" => $row->lname.", ".$row->fname,
							"date" => $data,
							"shift_type" => $data2->type,
							"shift_style" => $data2->style,
							"login" =>(count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? date('Y-m-d h:i:00 a', strtotime($in[0]->login)) : $data2->type : "No Log IN",
							"loginnTotalHours" =>$loginnTotalHours,
							"logout" => (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? date('Y-m-d h:i:s a', strtotime($out[0]->logout)) : $data2->type : "No Log OUT",
							"shiftStart" => ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? $timeschedule[0]->time_start : "--",
							"shiftEnd" => ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? $timeschedule[0]->time_end : " ",
							"late" => $timediff,
							"latemins" => $latemins/60,
							"latecalc" => ceil($late/60), 
							"bct" => $bct,
 							"ndRemark" =>   $ndRemark,
							"hpRemark" =>   $hpRemark,
							"hpRate" =>   $hpRate,
							"nd" =>  (count($emplogsShift2)>0) ? $NDuT : "0",
							"isHoliday" =>    $isLogoutHoliday1 + $isLoginHoliday1,
							"isLoginHoliday" =>    $isLoginHoliday1,
							"isLogoutHoliday" =>    $isLogoutHoliday1,
 							"HPComputeHolidayLogin" =>   $HPComputeHolidayLogin,
							"NDComputeHolidayLogin" =>   $NDComputeHolidayLogin,
							"NDComputeHolidayLogout" =>   $NDComputeHolidayLogout,
							"HPComputeHolidayLogout" =>   $HPComputeHolidayLogout,
							"HolidayType1" =>   $HolidayType1,
							"HolidayType2" =>   $HolidayType2,
							"logouttDTRID" =>   $logouttDTRID,
							"loginnDTRID" =>   $loginnDTRID,
							"breaks" =>   (count($breaks)>0) ? (($breaks[0]->hr >=1) ? 30 : $breaks[0]->hr*60)+$breaks[0]->min : 0,
							"total" =>   ($resLog>0) ? ( ($resLog<=$shiftTotalHoursWithBreaks) ? $resLog : $shiftTotalHoursWithBreaks ): 0,
							"totalwork" =>   ($resLog1>0) ? ( ($resLog1<=$shiftTotalHoursWithBreaks) ? $resLog1 : $shiftTotalHoursWithBreaks)  : 0,
							"isUndertime" =>   ($resLog1<=$shiftTotalHours) ? "UT" : "No",
							"resLog1" =>   $resLog1,
							"resLog" =>   $resLog,
							"calcHoliday1" =>    (isset($isLoginHoliday2[0]->calc)) ? $isLoginHoliday2[0]->calc : 0,
							"calcHoliday2" =>   (isset($isLogoutHoliday2[0]->calc)) ? $isLogoutHoliday2[0]->calc : 0,
							"logoutt2" =>   $isLogoutHoliday1,
							"loginn2" =>   $isLoginHoliday1,
							"logouttNew" =>   $logouttNew,
							"loginnNew" =>   $loginnNew,
							"resLogNEW" =>   ($resLogNEW>0) ? ( ($resLogNEW<=$shiftTotalHoursWithBreaks) ? $resLogNEW : $shiftTotalHoursWithBreaks ): 0,
 							"shiftTotalHours" =>   $shiftTotalHours,
 							"resLogNEW22" =>   $resLogNEW,
 							"logoutt" =>   $logoutt,
 							"loginn" =>   $loginn,
 							"to_timeLogNEW" =>   $to_timeLogNEW,
							"ho_end_time" =>  count($shiftTotalHr)>0 ? $shiftTotalHr[0]->ho_end_time : 0,
							"ho_start_time" =>  count($shiftTotalHr)>0 ? $shiftTotalHr[0]->ho_start_time  : 0,
							"NDuT" =>   $NDuT,
							"finalND2" =>   $finalND2,
 							"account" =>   (count($timeschedule) >0) ?  $timeschedule[0]->acc_id : 0,
							"shiftBreak" =>  ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? ((count($in) >0) ? $shiftBreak[0]->hr + $shiftBreak[0]->min  : 0) : 0,   
							"shiftTotalHoursWithBreaks" =>    $shiftTotalHoursWithBreaks,
 
							
						);
					}
				}
			}
 			echo json_encode($arr);
 		}
		public function emptyperesult(){
			$Emptype = $this->input->post('Emptype');
			
		$query = $this->db->query("select b.apid,fname,lname,b.emp_id,pos_name,e.class from tbl_applicant a,tbl_employee b,tbl_emp_promote c,tbl_pos_emp_stat d,tbl_position e where a.apid=b.apid and b.emp_id=c.emp_id and d.posempstat_id=c.posempstat_id and d.pos_id=e.pos_id and b.isActive='yes' and c.isActive=1 and e.class='".$Emptype."'");
		foreach ($query->result() as $row)
			{
				$arr['emp'][$row->emp_id] = array(
					"fname" => $row->lname.", ".$row->fname,
					"emp_id" => $row->emp_id 
					
				);
			}
		
			    echo json_encode($arr);

		}
		 public function breakIdentify($login,$logout,$empid){
		 		$query = $this->db->query("SELECT sum(hour) as hr,sum(min) as min FROM `tbl_dtr_logs` a,tbl_acc_time b,tbl_time c, tbl_break_time_account d, tbl_break e,tbl_break_time f where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and c.bta_id=d.bta_id and d.break_id=e.break_id and d.btime_id=f.btime_id and dtr_id between '".$login."' and '".$logout."' and emp_id=".$empid." and a.type='Break' and entry='O'");
			  return $query->result();

		 }
		 public function shiftBrk($acc_time_id){
		$query = $this->db->query("SELECT sum(hour*60) as hr, sum(min) as min FROM `tbl_shift_break` a,tbl_break_time_account b,tbl_break_time c where a.bta_id=b.bta_id and b.btime_id = c.btime_id and a.acc_time_id =$acc_time_id and a.isActive=1");
			  return $query->result();
		 }
		 public function empshift($empid,$date){
		$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and a.isActive=1 and sched_date = '".$date."' and emp_id=".$empid);
			  return $query->result();
		 }
		public function timeschedule($timeschedule){
			$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_acc_time b,tbl_time c  where a.acc_time_id = b.acc_time_id and b.time_id = c.time_id and sched_id=".$timeschedule);
			  return $query->result();
		 }
		 public function DateRangeShift($date){
		$strDateFrom = date("Y-m-d", strtotime($date[0]));
		$strDateTo = date("Y-m-d", strtotime($date[1]));
		$aryRange=array();
		$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
			if($iDateTo>=$iDateFrom){
				array_push($aryRange,date('Y-m-d',$iDateFrom));
				 while($iDateFrom<$iDateTo){
					$iDateFrom+=86400; // add 24 hours
						array_push($aryRange,date('Y-m-d',$iDateFrom));
					}
			}
			 return $aryRange;
			 		
		}
		 
		  public function empshiftin($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and sched_id=".$shift." and emp_id=".$empid." order by dtr_id ASC limit 1");
			  return $query->result();
 		}
		  public function empshiftout($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as logout,acc_time_id from tbl_dtr_logs where entry='O' and type='DTR' and sched_id=".$shift." and emp_id=".$empid." order by dtr_id ASC limit 1");
			  return $query->result();
 		}
		  public function emplogs($date,$empid){
		// public function emplogs(){
				// $date = '2017-04-05';
				// $empid = 350;
			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and log like '%".$date."%' and emp_id=".$empid."");
				
			  return $query->result();
			// echo json_encode($query->result());
 			 
 		}
		public function emplogsOut($dtrid,$empid){
 
			$query = $this->db->query("select dtr_id,emp_id,log as logout from tbl_dtr_logs where entry='O' and type='DTR' and note=".$dtrid." and emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function emplogsShift($sched_id,$empid){
 
			// $query = $this->db->query("select time_start,time_end from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.acc_time_id=".$acc_time_id." and emp_id=".$empid."");
			// $query = $this->db->query("select distinct(c.time_id),time_start,time_end,(select group_concat(concat(hour,'-',minutes))  from tbl_payroll_nd as s where s.time_id=c.time_id) as nd from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.sched_id=".$sched_id."  and emp_id=".$empid."");
			$query = $this->db->query("select distinct(c.time_id),time_start,time_end,(select group_concat(concat(hour,'-',minutes))  from tbl_payroll_nd as s where s.time_id=c.time_id) as nd from tbl_dtr_logs a,tbl_acc_time b,tbl_time c,tbl_schedule d where b.acc_time_id=d.acc_time_id and b.time_id=c.time_id and a.sched_id=d.sched_id and a.sched_id=".$sched_id."  and a.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function empRate($empid){
 
			$query = $this->db->query("select b.apid,b.emp_id,fname,lname,pos_name,g.status,rate from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 and b.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function NightDiff($nd){
 			$query = $this->db->query("select time_start,time_end,hour,minutes from tbl_payroll_nd a,tbl_time b where a.time_id=b.time_id and a.isActive=1 and b.time_start ='".$nd."'");
				
			 return $query->result();
  		}
		public function SSSCompute($empid){
 			// $query = $this->db->query("select sss_employee from (select rate from tbl_rate a,tbl_position b,tbl_employee c,tbl_applicant d where a.pos_id=b.pos_id and c.apid=d.apid and b.pos_id =d.pos_id and a.isActive=1 and c.isActive='yes' and c.emp_id=".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
 			  $query = $this->db->query("select sss_employee from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
				
			 return $query->result();
  		}
		public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		}
			public function shiftTotalHr($time_id){
		 			$query = $this->db->query("SELECT * FROM tbl_time where time_id=".$time_id);
			 return $query->result();

		}
		public function loginn2timeID($time_id){
		 			$query = $this->db->query("SELECT * FROM tbl_time where time_id=".$time_id);
			 return $query->result();

		}
		public function loginnNdtimeID($time_id){
		 			$query = $this->db->query("SELECT b.* FROM `tbl_time` a,tbl_payroll_nd b  where a.time_id=b.time_id and a.time_id=".$time_id);
			 return $query->result();

		}
		public function isLoginHoliday($login,$timeStart){
			$year = date_format(date_create($login),"Y");
			  $date = date_format(date_create($login),"Y-m-d");
			  $timeStart = date_format(date_create($timeStart),"h:i:s");
 			
			$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");

			return $query->result();
  		}
		public function isLogoutHoliday($logout,$timeEnd){
 	/* 	public function isLogoutHoliday(){
		$logout="2017-08-17 06:00:37";
		$timeEnd="6:00:00 AM"; */
 		  $year = date_format(date_create($logout),"Y");
			  $date = date_format(date_create($logout),"Y-m-d");
			  $timeEnd = date_format(date_create($timeEnd),"h:i:s");
 			
		$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
			// echo json_encode($query->result());
			 return $query->result();
  		}
		 
		/* public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare from (select rate from tbl_rate a,tbl_position b,tbl_employee c,tbl_applicant d where a.pos_id=b.pos_id and c.apid=d.apid and b.pos_id =d.pos_id and a.isActive=1 and c.isActive='yes' and c.emp_id=".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		} */
	
}