<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement extends CI_Controller {

  public function __construct(){
        parent::__construct();
		$this->load->model("AnnounceModel");

			}
	public function index(){
		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='announcement' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$this->data['setting'] = array("settings" => $row->settings);
			}
	if($this->session->userdata('uid')){
			$this->data['userz'] = $this->AnnounceModel->fetchAnnouncement();
			$this->load->view('announce',$this->data);
 
		}else{
			redirect('index.php/login');
		}
		}
	
	public function addAnnounce(){
	 			$subject= $this->input->post('subject');
	 			$message= $this->input->post('message');
 				$this->AnnounceModel->addAnnouncement($subject,$message);	


	}
	public function UpdateAnnounce(){
 
	 			$subject= $this->input->post('subject');
	 			$message= $this->input->post('message');
	 			$anounceID= $this->input->post('anounceID');
	 		 
				$this->AnnounceModel->updateAnnounce($subject,$message,$anounceID);	


	}
}