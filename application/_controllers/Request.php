
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

	public function __construct(){
		parent::__construct(); 
		$this->load->model('RequestModel');
		$this->load->model('ScheduleModel');
	}
	public function view_leave_main(){
		$data['leaves'] = $this->RequestModel->getLeaveList();
		$this->load->view('request_leave_main',$data);
	}
	public function view_leave_personal(){
		$data['leaves'] = $this->RequestModel->getLeaveList();
		$this->load->view('request_leave_personal',$data);
	}
	public function getLeaveRequestedToMe2(){
		$status = $this->input->post('status');
		$uid = $this->session->userdata('uid');
		$requestedToMe = $this->RequestModel->getLeaveRequestedToMe2($uid,$status);
		foreach($requestedToMe as $rtm){
			$values = $this->RequestModel->getEmployeeDetails($rtm->requested_by);
			$rtm->lname = $values->lname;
			$rtm->fname = $values->fname;
			$rtm->pic = $values->pic;
			$rtm->requested_on = date("F d, Y h:i a",strtotime($rtm->requested_on));
			$rtm->start_date = date("F d, Y",strtotime($rtm->start_date));
			$rtm->end_date = date("F d, Y",strtotime($rtm->end_date));
		}
		if(empty($requestedToMe)){
			echo 'Empty';
		}else{
			echo json_encode($requestedToMe);
		}
	}
	public function getLeaveMyRequests2(){
		$status = $this->input->post('status');
		$uid = $this->session->userdata('uid');
		$myRequest = $this->RequestModel->getLeaveMyRequests2($uid,$status);
		foreach($myRequest as $mr){
			$values = $this->RequestModel->getEmployeeDetails($mr->responded_by);
			$mr->lname = $values->lname;
			$mr->fname = $values->fname;
			$mr->pic = $values->pic;
			$mr->requested_on = date("F d, Y h:i a",strtotime($mr->requested_on));
			$mr->start_date = date("F d, Y",strtotime($mr->start_date));
			$mr->end_date = date("F d, Y",strtotime($mr->end_date));
		}
		
		if(empty($myRequest)){
			echo 'Empty';
		}else{
			echo json_encode($myRequest);
		}
	}
	public function request_leave(){
		$data['leaves'] = $this->RequestModel->getLeaveList();
		$this->load->view('request_leave',$data);
	}
	public function checkleavedaterange(){
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
		$uid = $this->session->userdata('uid');
		$period = new DatePeriod(
			new DateTime("$startdate"),
			new DateInterval('P1D'),
			new DateTime("$enddate +1 day ")
		);
		$dateexists = 'false';
		foreach ($period as $date) {
			$r = $this->RequestModel->checkdateexistrequest($date->format("Y-m-d"),$uid);
			if(!empty($r)){
				$dateexists = 'true';
			}
		}
		if($dateexists=='true'){
			echo 'Exists';
		}else{
			$data = $this->RequestModel->checkleavedaterange($startdate,$enddate,$uid);
			if(empty($data)){
				echo 'Empty';
			}else{
				echo json_encode($data);
			}
		}

	}
	public function sendrequestleave(){
		$requesttype = $this->input->post('requesttype');
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
		$leavetype = $this->input->post('leavetype');
		$reason = $this->input->post('reason');
		$paidleaves = $this->input->post('paidleaves');
		$unpaidleaves = $this->input->post('unpaidleaves');
		// $leavetype = 1;
		// $paidleaves = 2;
		// $startdate = '2017-12-25';
		// $enddate = '2018-01-25';
		$uid = $this->session->userdata('uid');
		$data = $this->RequestModel->getSupervisor($uid);
		$period = new DatePeriod(
			new DateTime("$startdate"),
			new DateInterval('P1D'),
			new DateTime("$enddate +1 day ")
		);
		$dateexists = 'false';
		foreach ($period as $date) {
			$r = $this->RequestModel->checkdateexistrequest($date->format("Y-m-d"),$uid);
			if(!empty($r)){
				$dateexists = 'true';
			}
		}
		if($dateexists=='true'){
			echo 'Exists';
		}else{
			$startyear = date_format(date_create($startdate),"Y");
			$endyear = date_format(date_create($enddate),"Y");
			if($startyear!=$endyear){
				$startdates = array();
				$enddates = array();
				$dates = array();
				foreach ($period as $date) {
					$dateyear = $date->format("Y");
					if($startyear==$dateyear){
						$startdates[] = $date->format("Y-m-d");
					}else{
						$enddates[] = $date->format("Y-m-d");
					}
				}
				$start1 = current($startdates);
				$end1 = end($startdates);
				$cnt1 = count($startdates);
				$start2 = current($enddates);
				$end2 = end($enddates);
				$cnt2 = count($enddates);
				$token = strtotime("now");
				$ptrack1 = $paidleaves-count($startdates);
				if($ptrack1>0){
					$leave_paidtrack1=count($startdates)."-0";
				}else{
					$leave_paidtrack1="$paidleaves-".abs($ptrack1);
				}
				$user = $this->RequestModel->getUser($uid);
				$promote = $this->RequestModel->getPromoteVals($user->emp_id);
				$leave_ids[] = $leavetype;
				$leaveidsSTR = "'".implode("','",$leave_ids)."'";
				$leavecredits = $this->RequestModel->getLeaveCredits($leaveidsSTR,$promote->posempstat_id);
				$nextcredit =  $leavecredits[0]->credit;
				$ptrack2 = $nextcredit-count($enddates);
				if($ptrack2>0){
					$leave_paidtrack2=count($enddates)."-0";
				}else{
					$leave_paidtrack2="$nextcredit-".abs($ptrack2);
				}
			// echo "PAID LEAVES:".$paidleaves.'<br>';
			// echo "MAX CREDITS:".$nextcredit.'<br>';
			// echo "2017:".$leave_paidtrack1.'<br>';
			// echo "2018:".$leave_paidtrack2.'<br>';
				$values = array(array(
					'details' => $leavetype,
					'type' => $requesttype,
					'reason' => $reason,
					'start_date' => $start1,
					'end_date' => $end1,
					'requested_by' => $data->emp_id,
					'requested_on' => date("Y-m-d h:i:s A"),
					'responded_by' => $data->Supervisor,
					'status' => 'Pending',
					'leave_paidtrack' => "$leave_paidtrack1",
					'token' => $token
				),
				array(
					'details' => $leavetype,
					'type' => $requesttype,
					'reason' => $reason,
					'start_date' => $start2,
					'end_date' => $end2,
					'requested_by' => $data->emp_id,
					'requested_on' => date("Y-m-d h:i:s A"),
					'responded_by' => $data->Supervisor,
					'status' => 'Pending',
					'leave_paidtrack' => "$leave_paidtrack2",
					'token' => $token
				));
				$rows = $this->RequestModel->sendBatchRequest($values);
				if($rows>0){
					echo 'Successful';
				}else{
					echo 'Failed';
				}
			}else{
				$values = array(
					'details' => $leavetype,
					'type' => $requesttype,
					'reason' => $reason,
					'start_date' => $startdate,
					'end_date' => $enddate,
					'requested_by' => $data->emp_id,
					'requested_on' => date("Y-m-d h:i:s A"),
					'responded_by' => $data->Supervisor,
					'status' => 'Pending',
					'leave_paidtrack' => "$paidleaves-$unpaidleaves",
					'token' => strtotime("now")
				);
				$rows = $this->RequestModel->sendrequestleave($values);
				if($rows>0){
					echo 'Successful';
				}else{
					echo 'Failed';
				}
			}
		}	
	}
	public function getRequestDetails(){
		$token = $this->input->post('token');
		$rtm = $this->RequestModel->getRequestDetails($token);
		$values = $this->RequestModel->getEmployeeDetails($rtm->requested_by);
		$rtm->lname = $values->lname;
		$rtm->fname = $values->fname;
		$rtm->pic = $values->pic;
		echo json_encode($rtm);
	}
	public function leaverequestAccepted(){
		// $request_id = 1;
		$token = $this->input->post('token');
		$responded_on = date("Y-m-d h:i:s A");
		$rows = $this->RequestModel->leaverequestAccepted($token,$responded_on);
		if($rows>0){
			$requestdetails = $this->RequestModel->getRequestDetails($token);
			$period = new DatePeriod(
				new DateTime("$requestdetails->start_date"),
				new DateInterval('P1D'),
				new DateTime("$requestdetails->end_date +1 day ")
			);
			foreach ($period as $date) {
				$dates[] = $date->format("Y-m-d");
			}
			foreach($dates as $d){
				$res = $this->RequestModel->checkExistSchedule($requestdetails->requested_by,$d);
				if(empty($res)){
					//No Schedule
					$row = $this->RequestModel->requestNewSchedule($requestdetails->requested_by,$d,$requestdetails->details);
				}else{
					//With Schedule
					$row = $this->RequestModel->requestUpdateSchedule($requestdetails->requested_by,$d,$requestdetails->responded_by,$responded_on,$requestdetails->details);
				}

			}
			echo 'Successful';
		}else{
			echo 'Failed';
		}
	}
	public function leaverequestRejected(){
		$reason_reject = $this->input->post('reason_reject');
		$token = $this->input->post('token');
		$responded_on = date("Y-m-d h:i:s A");
		$rows = $this->RequestModel->leaverequestRejected($token,$reason_reject,$responded_on);
		if($rows>0){
			echo 'Successful';
		}else{
			echo 'Failed';
		}
	}
	public function getScheduleDetails(){
		$schedids = $this->input->post('schedids');
		// echo $schedids;
		$schedids = explode('|',$schedids);
		$schedule = $this->RequestModel->getScheduleDetails($schedids);
		$shift = $this->ScheduleModel->getTimeSched($schedule{0}->emp_id);
		$blankshift = new stdClass();
		$blankshift->acc_time_id = '';
		$blankshift->time_start = '';
		$blankshift->time_end = '';
		array_unshift($shift , $blankshift);
		foreach($schedule as $sched){
			$sched->shift = $shift;
		}
		echo json_encode($schedule);
	}
	public function checkIfSchedLeave(){
		$empid = $this->input->post('empid');
		$start_date = $this->input->post('startdate');
		$end_date = $this->input->post('enddate');
		$token = $this->input->post('token');

		$period = new DatePeriod(
			new DateTime("$start_date"),
			new DateInterval('P1D'),
			new DateTime("$end_date +1 day ")
		);
		$dates = array();
		foreach ($period as $date) {
			$dates[] = $date->format("Y-m-d");
		}
		$leavedates = array();
		foreach($dates as $d){
			$res = $this->RequestModel->checkIfSchedLeave($d,$empid);
			if(!empty($res)){
				$notleavedates[] = $res->sched_id;
			}
		}
		if(empty($notleavedates )){
			$row = $this->RequestModel->updateRequestDecline($token,'Already leave dates');
			if($row>0){
				echo 'Success';
			}else{
				echo 'Failed';
			}
		}else{
			echo implode("|",$notleavedates);
		}
	}
	public function updateLeaveSchedule(){
		$acc_time_id = $this->input->post('acc_time_id');
		$isRD = $this->input->post('isRD');
		$schedid = $this->input->post('schedid');
		if($isRD=='true'&&empty($acc_time_id)){
			$schedtype_id = 2 ;
		}else if($isRD=='true'&&!empty($acc_time_id)){
			$schedtype_id = 4;
		}else if($isRD=='false'&&!empty($acc_time_id)){
			$schedtype_id = 1;
		}else if($isRD=='false'&&empty($acc_time_id)){
			$schedtype_id = 3;
		}else{
			$schedtype_id = 1;
		}
		$row = $this->RequestModel->updatesched($schedid,$acc_time_id,$schedtype_id);
		if($row>0){
			echo 'Success';
		}else{
			echo 'Failed';
		}

	}
	public function updateRequestStatusDecline(){
		$thereasonreject = $this->input->post('thereasonreject');
		$token = $this->input->post('token');
		$row = $this->RequestModel->updateRequestDecline($token,$thereasonreject);
		if($row>0){
			echo 'Success';
		}else{
			echo 'Failed';
		}
	}
	public function getBirthdate(){
		$uid = $this->session->userdata('uid');
		$data = $this->RequestModel->getBirthdate($uid);
		if($data->birthday=='0000-00-00'||$data->birthday==NULL){
			echo 'Empty';
		}else{
			echo $data->birthday;
		}
	}

	//NEW CODES----------------------------------------------------------------------------------

	public function leavecredits(){
			$uid = $this->session->userdata('uid');
		if($uid){
			  	$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Request/leavecredits' and is_assign=1 order by lname");   

		 	if ($qry->num_rows() > 0){
			$data['leavelistWOVS'] = $this->RequestModel->getLeaveListWOVS();
			$data['leavelist'] = $this->RequestModel->getLeaveList();
			$this->load->view('leavecredits',$data);
		}else{
			redirect('index.php/home/error');

		}
		}else{
			redirect('index.php/login');

		}
	}
	public function getLeaveCredits(){
		$leave_ids = $this->input->post('leavetype');
		$class = $this->input->post('class');
		$status = $this->input->post('status');
		$classes = array();
		$statuses = array();
		if($class=='Both'){
			$classes[] = 'Admin';
			$classes[] = 'Agent';
		}else{
			$classes[] = $class;
		}
		if($status=='Both'){
			$statuses[] = 'Probationary';
			$statuses[] = 'Regular';
		}else{
			$statuses[] = $status;
		}
		$classesSTR = "'".implode("','",$classes)."'";
		$statusesSTR = "'".implode("','",$statuses)."'";
		$leaveidsSTR = "'".implode("','",$leave_ids)."'";
		// echo $classesSTR;
		// echo $statusesSTR;
		$positions = $this->RequestModel->getListOfPositions($classesSTR,$statusesSTR);

		$leavelist = $this->RequestModel->getLeaveListSpecific($leaveidsSTR);
		foreach($positions as $pos){
			$pos->leaveCredits = $this->RequestModel->getLeaveCredits($leaveidsSTR,$pos->posempstat_id);
		}
		echo json_encode($positions);
		// echo "<pre>";
		// var_dump($positions);
		// echo "</pre>";
	}
	public function getLeaveHeader(){
		$leave_ids = $this->input->post('leavetype');
		$leaveidsSTR = "'".implode("','",$leave_ids)."'";
		$leavelist = $this->RequestModel->getLeaveListSpecific($leaveidsSTR);
		// echo $this->db->last_query();
		echo json_encode($leavelist);
	}
	public function IUleavecredit(){
		$posempstat_id = $this->input->post('posempstat_id');
		$leave_id = $this->input->post('leave_id');
		$value = $this->input->post('value');
		$lc = $this->RequestModel->getLeaveCreditId($posempstat_id,$leave_id);
		if(empty($lc)){
			$row = $this->RequestModel->insertleavecredit($posempstat_id,$leave_id,$value);
		}else{
			$row = $this->RequestModel->updateleavecredit($lc->leavecredit_id,$value);
		}
		if($row>0){
			echo "Success";
		}else{
			echo "Failed";
		}
	}
	public function IUSingleTrans(){
		$leavetype = $this->input->post('leavetype');
		$class = $this->input->post('class');
		$status = $this->input->post('status');
		$credits = $this->input->post('credits');
		$classes = array();
		$statuses = array();
		if($class=='Both'){
			$classes[] = 'Admin';
			$classes[] = 'Agent';
		}else{
			$classes[] = $class;
		}
		if($status=='Both'){
			$statuses[] = 'Probationary';
			$statuses[] = 'Regular';
		}else{
			$statuses[] = $status;
		}

		$classesSTR = "'".implode("','",$classes)."'";
		$statusesSTR = "'".implode("','",$statuses)."'";
		$positions = $this->RequestModel->getListOfPositions($classesSTR,$statusesSTR);
		foreach($positions as $pos){
			$lc = $this->RequestModel->getLeaveCreditId($pos->posempstat_id,$leavetype);
			if(empty($lc)){
				$row = $this->RequestModel->insertleavecredit($pos->posempstat_id,$leavetype,$credits);
			}else{
				// $row = $this->RequestModel->updateleavecredit($lc->leavecredit_id,$credits);
			}
		}
	}
	public function getRemainingCredits(){
		$uid = $this->session->userdata('uid');
		$leavetype = $this->input->post('leavetype');
		// $leavetype = 1;
		$user = $this->RequestModel->getUser($uid);
		$value = $this->RequestModel->getLeaveRequestofUser($user->emp_id,$leavetype);
		
		$paidleaves = array();
		foreach($value as $val){
			$paidtrack = explode('-',$val->leave_paidtrack);
			$paidleaves[] = $paidtrack[0];
		}
		$leave_ids[] = $leavetype;
		$leaveidsSTR = "'".implode("','",$leave_ids)."'";
		$promote = $this->RequestModel->getPromoteVals($user->emp_id);
		$leavecredits = $this->RequestModel->getLeaveCredits($leaveidsSTR,$promote->posempstat_id);
		if(empty($leavecredits)){
			echo "Not Set";
		}else{
			echo $leavecredits[0]->credit - array_sum($paidleaves);
		}
		
	}

	public function getPaidUnpaidLeaves(){
		$creditsavailable = $this->input->post('creditsavailable');
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
		$dates = array();
		$period = new DatePeriod(
			new DateTime("$startdate"),
			new DateInterval('P1D'),
			new DateTime("$enddate +1 day ")
		);
		foreach ($period as $date) {
			$dateyear = date('Y');
			if($date->format('Y')==$dateyear){
				$dates[] = $date->format("Y-m-d");
			}
		}
		$value = $creditsavailable - count($dates);
		if($value>0){
			echo json_encode(array('paid'=>count($dates),'unpaid'=>0));
		}else{
			echo json_encode(array('paid'=>$creditsavailable,'unpaid'=>abs($value)));
		}
	}
	//--------------NEW CODES----------------------------
	public function request_leave_remaining(){
		// if($this->session->userdata('uid')){

		// 	$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='<LINK>' and is_assign=1 order by lname");   

		// 	if ($qry->num_rows() > 0){
		$uid = $this->session->userdata('uid');
		$position = $this->RequestModel->getEmployeePosition($uid);
	 
		if($position->status=='Regular'){
			$regular = $this->RequestModel->getRegularizationDate($uid);
			$regyear = date('Y',strtotime($regular->dateFrom));
			$currentyear = date('Y');
			if($regyear==$currentyear){

			}else{
				//MAXIMUM
			}
		}
		// $this->load->view('request_leave_remaining');

		// 	}else{
		// 		redirect('index.php/home/error');
		// 	}

		// }

	}
}
