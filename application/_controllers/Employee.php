<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('EmployeeModel');
    }

    public function index($class = NULL) {

        $arr['class'] = $class;
        if ($class == 'Agents') {
            $query = $this->db->query("select fname, lname, pic, b.apid, b.emp_id, id_num, birthday, birthplace, intellicare, pioneer, BIR, philhealth, pagibig, sss, TIMESTAMPDIFF(YEAR,birthday,now()) as age, a.pos_id, c.pos_name, c.pos_details, e.status, cell,presentAddress as address,email,a.cell from tbl_applicant a,tbl_employee b,tbl_position c,tbl_pos_emp_stat d,tbl_emp_stat e,tbl_emp_promote f where a.apid=b.apid and b.emp_id=f.emp_id and f.posempstat_id=d.posempstat_id and d.empstat_id=e.empstat_id and d.pos_id=c.pos_id and d.isActive=1 and f.isActive=1 and b.isActive='yes'  and c.class='Agent' order by lname ASC");
        } else if ($class == 'Admin') {
            $query = $this->db->query("select fname, lname, pic, b.apid, b.emp_id, id_num, birthday, birthplace, intellicare, pioneer, BIR, philhealth, pagibig, sss, TIMESTAMPDIFF(YEAR,birthday,now()) as age, a.pos_id, c.pos_name, c.pos_details, e.status, cell,presentAddress as address,email,a.cell from tbl_applicant a,tbl_employee b,tbl_position c,tbl_pos_emp_stat d,tbl_emp_stat e,tbl_emp_promote f where a.apid=b.apid and b.emp_id=f.emp_id and f.posempstat_id=d.posempstat_id and d.empstat_id=e.empstat_id and d.pos_id=c.pos_id and d.isActive=1 and f.isActive=1 and b.isActive='yes'  and c.class='Admin' order by lname ASC");
        } else {
            $query = $this->db->query("select fname, lname, pic, b.apid, b.emp_id, id_num, birthday, birthplace, intellicare, pioneer, BIR, philhealth, pagibig, sss, TIMESTAMPDIFF(YEAR,birthday,now()) as age, a.pos_id, c.pos_name, c.pos_details, e.status, cell,presentAddress as address,email,a.cell from tbl_applicant a,tbl_employee b,tbl_position c,tbl_pos_emp_stat d,tbl_emp_stat e,tbl_emp_promote f where a.apid=b.apid and b.emp_id=f.emp_id and f.posempstat_id=d.posempstat_id and d.empstat_id=e.empstat_id and d.pos_id=c.pos_id and d.isActive=1 and f.isActive=1 and b.isActive='yes' order by lname ASC");
        }

        foreach ($query->result() as $row) {
            $arr['data'][$row->apid] = array(
                "apid" => $row->apid,
                "emp_id" => $row->emp_id,
                "fname" => $row->fname,
                "lname" => $row->lname,
                "pic" => $row->pic,
                "id_num" => $row->id_num,
                "birthday" => $row->birthday,
                "age" => $row->age,
                "placebday" => $row->birthplace,
                "intellicare" => $row->intellicare,
                "pioneer" => $row->pioneer,
                "BIR" => $row->BIR,
                "philhealth" => $row->philhealth,
                "pagibig" => $row->pagibig,
                "sss" => $row->sss,
                "pos_id" => $row->pos_id,
                "pos_name" => $row->pos_name,
                "pos_details" => $row->pos_details,
                "status" => $row->status,
                "cell" => $row->cell,
                "address" => $row->address,
                "email" => $row->email,
                "cell" => $row->cell
            );
        }
        if ($this->session->userdata('uid')) {
            $qry = $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=" . $this->session->userdata('uid') . " and item_link='employee' and is_assign=1");
            foreach ($qry->result() as $row) {
                $arr['setting'] = array("settings" => $row->settings);
            }
            if ($qry->num_rows() > 0) {
                $this->load->view('employees', $arr);
            } else {
                redirect('index.php/home/error');
            }
        } else {
            redirect('index.php/login');
        }
    }

    public function updateEmployee() {
        $empid = $this->input->post('emp_id');
        // var_dump($empid);
//        $empid = 554;
        if ($empid != null) {
            $query = $this->db->query("select 
				a.fname,
				a.mname,
				a.lname,
				nikname,
				ccaExp,
				gender,
				civilStatus,
				a.pic,
				b.apid,
				b.emp_id,
				id_num,
				birthday,
				placebday,
				intellicare,
				pioneer,
				BIR,
				philhealth,
				pagibig,
				sss,
				TIMESTAMPDIFF(YEAR,birthday,now()) as age,
				a.pos_id,
				c.pos_name,
				b.emp_stat,
				cell,
				tel,
				email,
				blood,
				relijon,
				school_name,
				course,
				school_note,
				emergency_fname,
				emergency_mname,
				emergency_lname,
				emergency_extname,
				emergency_contact,
				other_latestEmp,
				other_lastPosition,
				other_inclusiveDate,
				intellicare,
				pioneer,
				BIR,
				philhealth,
				pagibig,
				sss,
				d.username,
				d.password,
				Supervisor,
				supEmail,
				confidential,
				anniversary,
				salarymode
				from tbl_applicant a,tbl_employee b,tbl_position c,tbl_user d where d.emp_id=b.emp_id AND c.pos_id=a.pos_id and a.apid=b.apid and b.isActive='yes' and b.emp_id= " . $empid . " order by lname ASC");
            foreach ($query->result() as $row) {

                $arr['emp'] = array(
                    "apid" => $row->apid,
                    "emp_id" => $row->emp_id,
                    "fname" => $row->fname,
                    "lname" => $row->lname,
                    "mname" => $row->mname,
                    "nikname" => $row->nikname,
                    "gender" => $row->gender,
                    "civil" => $row->civilStatus,
                    "pic" => $row->pic,
                    "id_num" => $row->id_num,
                    "birthday" => $row->birthday,
                    "age" => $row->age,
                    "placebday" => $row->placebday,
                    "intellicare" => $row->intellicare,
                    "pioneer" => $row->pioneer,
                    "BIR" => $row->BIR,
                    "philhealth" => $row->philhealth,
                    "pagibig" => $row->pagibig,
                    "sss" => $row->sss,
                    "pos_id" => $row->pos_id,
                    "pos_name" => $row->pos_name,
                    "emp_stat" => $row->emp_stat,
                    "cell" => $row->cell,
                    "tel" => $row->tel,
                    "email" => $row->email,
                    "blood" => $row->blood,
                    "relijon" => $row->relijon,
                    "school_name" => $row->school_name,
                    "course" => $row->course,
                    "school_note" => $row->school_note,
                    "emergency_fname" => $row->emergency_fname,
                    "emergency_mname" => $row->emergency_mname,
                    "emergency_lname" => $row->emergency_lname,
                    "emergency_extname" => $row->emergency_extname,
                    "emergency_contact" => $row->emergency_contact,
                    "ccaExp" => $row->ccaExp,
                    "other_latestEmp" => $row->other_latestEmp,
                    "other_lastPosition" => $row->other_lastPosition,
                    "other_inclusiveDate" => $row->other_inclusiveDate,
                    "intellicare" => $row->intellicare,
                    "pioneer" => $row->pioneer,
                    "BIR" => $row->BIR,
                    "philhealth" => $row->philhealth,
                    "pagibig" => $row->pagibig,
                    "sss" => $row->sss,
                    "username" => $row->username,
                    "password" => $row->password,
                    "Supervisor" => $row->Supervisor,
                    "supEmails" => $row->supEmail,
                    "confidential" => $row->confidential,
                    "anniversary" => $row->anniversary,
                    "salarymode" => $row->salarymode
                );
            }

            if ($this->session->userdata('uid')) {
                if ($arr['emp']['Supervisor'] != null) {
                    $query2 = $this->db->query("select fname, mname, lname from tbl_applicant a,tbl_employee b,tbl_position c where c.pos_id=a.pos_id and a.apid=b.apid and b.isActive='yes' and emp_id= " . $arr['emp']['Supervisor'] . " order by lname ASC");
                    foreach ($query2->result() as $row) {
                        $arr['Supname'] = $row->fname . " " . $row->lname;
                    }
                } else {
                    $arr['Supname'] = " ";
                }
                $qry = $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=" . $this->session->userdata('uid') . " and item_link='Applicant' and is_assign=1");
                foreach ($qry->result() as $row) {
                    $arr['setting'] = array("settings" => $row->settings);
                }

                //----------------NICCA_------------------------------------------------------------------------------------------------
                // $nquery = $this->db->query("SELECT pos_name,pos_details,rate AS monthly, round(rate/2,2) AS quinsina,round((rate*12)/261,2) as daily,round(((rate*12)/261)/8,2) as hourly FROM tbl_applicant a,tbl_employee b,tbl_position c,tbl_rate d WHERE a.apid=b.apid AND a.pos_id=c.pos_id AND c.pos_id = d.pos_id AND b.isActive='yes' AND b.emp_id=".$empid);

                $arr['emprole'] = $this->db->query("SELECT role_id FROM tbl_user WHERE emp_id=" . $empid . " AND isActive=1")->row();
                $arr['account'] = $this->db->query("SELECT a.acc_id,a.acc_name,a.acc_description,b.emp_id FROM tbl_account a,tbl_employee b WHERE a.acc_id=b.acc_id AND emp_id=$empid")->row();
                $arr['accountlist'] = json_encode($this->EmployeeModel->getAccountLists());
                $arr['userrole'] = $this->db->query("SELECT b.description FROM tbl_user as a,tbl_user_role as b WHERE a.role_id=b.role_id AND a.uid =" . $this->session->userdata('uid'))->row();
                $nquery = $this->db->query("select e.pos_id,g.status,class,pos_name,pos_details,rate AS monthly, round(rate/2,2) AS quinsina,round((rate*12)/261,2) as daily,round(((rate*12)/261)/8,2) as hourly from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 AND f.isActive=1 and c.isActive=1 and b.emp_id=" . $empid);
                $pospay = $nquery->row();
                $arr['pospay'] = $nquery->row();
                if ($pospay != '' || $pospay != NULL) {
                    $taxdesc = $this->db->query("SELECT tax_desc_id as value,description as text FROM tbl_tax_description WHERE isActive=1");
                    $arr['taxdesc'] = json_encode($taxdesc->result());
                    $roles = $this->db->query("SELECT role_id as value,description as text FROM tbl_user_role WHERE is_active=1");
                    $arr['roles'] = json_encode($roles->result());
                    $sss = $this->db->query("SELECT a.* FROM tbl_sss as a,tbl_sss_effectivity as b WHERE a.min_range<='" . $pospay->monthly . "' AND a.max_range>='" . $pospay->monthly . "' AND a.sss_effec_id=b.sss_effec_id AND b.isActive=1");
                    $arr['sss'] = $sss->row();
//                    $philhealth = $this->db->query("SELECT a.* FROM tbl_philhealth as a,tbl_philhealth_effectivity as b WHERE a.minrange<='" . $pospay->monthly . "' AND a.maxrange>='" . $pospay->monthly . "' AND a.p_effec_id=b.p_effec_id AND b.isActive=1");
//                    $arr['philhealth'] = $philhealth->row();
                    if ($pospay->monthly <= 10000) {
                        $arr['philhealth'] = (object) array('employeeshare' => 137.50);
                    } else if ($pospay->monthly > 10000 && $pospay->monthly < 39999.99) {
                        $philhealthvalue = ($pospay->monthly * 0.0275) / 2;
                        $arr['philhealth'] = (object) array('employeeshare' => $philhealthvalue);
                    } else {
                        $arr['philhealth'] = (object) array('employeeshare' => 550);
                    }
                    $pagibig = $this->db->query("SELECT pagibig_id,(employeeshare*'" . $pospay->monthly . "') as employeeshare,employershare FROM tbl_pagibig as a WHERE a.isActive=1")->row();
                    $pagibig->addContrib = $this->db->query("SELECT * FROM tbl_pagibig_add_contribution WHERE emp_id = $empid AND isActive=1")->row();
                    $arr['pagibig'] = $pagibig;

                    $taxemp = $this->db->query("SELECT tax_desc_id FROM tbl_tax_emp WHERE emp_id=" . $empid . " AND isActive=1");
                    $arr['taxemp'] = $taxemp->row();
                    // var_dump($arr['taxemp']);
                    if ($arr['pospay']->status == 'Regular') {
                        $arr['positions'] = $this->EmployeeModel->getAllPositions();
                        $arr['departments'] = $this->EmployeeModel->getAllDepartments();
                    } else {
                        $arr['positions'] = NULL;
                        $arr['departments'] = NULL;
                    }
                } else {
                    $arr['positions'] = NULL;
                    $arr['departments'] = NULL;
                }


                $qry = $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=" . $this->session->userdata('uid') . " and item_link='employee' and is_assign=1");
                foreach ($qry->result() as $row) {
                    $arr['setting'] = array("settings" => $row->settings);
                }

                // var_dump($arr['positions']);
                // -----------------Nicca--------------------------------------------------------------------------------------------------
                // var_dump($arr['userrole']);
                // echo $this->session->userdata('uid');
//                echo "<pre>";
//                var_dump($arr);
//                echo "</pre>";
                $this->load->view('employeeUpdate2', $arr);
                // var_dump($arr['c']);
            } else {
                redirect('index.php/login');
            }
        } else {
            redirect('index.php/Employee');
        }
    }

    public function listofSupervisor() {
        $query = $this->db->query("select b.apid,emp_id,fname,lname,a.status,b.email from tbl_applicant as a,tbl_employee as b,tbl_account c where a.apid=b.apid and c.acc_id = b.acc_id and a.status='employ' and email!='' and c.acc_description='Admin' and b.isActive='yes'");

        foreach ($query->result() as $row) {
            $arr[] = array(
                "emp_id" => $row->emp_id,
                "label" => $row->fname . " " . $row->lname,
                "email" => $row->email
            );
        }
        echo json_encode($arr);
    }

    //----------------------------------------------NICCA-----------------------------------------------
    public function getTaxEmployeeShare($taxid) {
        $salary = $this->input->post('salary'); //deducted already with gov payments semi-monthly
//        $salary = 20014.35;
//change-----------------------------------------------
        $salary = $salary * 2;
        $annualbasic = $salary * 12;
        if ($annualbasic <= 250000) {
            $subtotal = 0;
        } else if ($annualbasic <= 400000) {
            $diff = $salary * 12;
            $excess = $diff - 250000;
            $percentdeduct = $excess * .20;
            $monthlytax = $percentdeduct / 12;
            $subtotal = $monthlytax / 2;
        } else if ($annualbasic <= 800000) {
            $diff = $salary * 12;
            $excess = $diff - 400000;
            $percentdeduct = ($excess * .25) + 30000;
            $monthlytax = $percentdeduct / 12;
            $subtotal = $monthlytax / 2;
        } else if ($annualbasic <= 2000000) {
            $diff = $salary * 12;
            $excess = $diff - 800000;
            $percentdeduct = ($excess * .30) + 130000;
            $monthlytax = $percentdeduct / 12;
            $subtotal = $monthlytax / 2;
        } else if ($annualbasic <= 8000000) {
            $diff = $salary * 12;
            $excess = $diff - 2000000;
            $percentdeduct = ($excess * .32) + 490000;
            $monthlytax = $percentdeduct / 12;
            $subtotal = $monthlytax / 2;
        } else if ($annualbasic > 8000000) {
            $diff = $salary * 12;
            $excess = $diff - 8000000;
            $percentdeduct = ($excess * .35) + 2410000;
            $monthlytax = $percentdeduct / 12;
            $subtotal = $monthlytax / 2;
        } else {
            $subtotal = 0;
        }
//change-----------------------------------------------
//        $query = $this->db->query("SELECT salarybase,deduction_id from tbl_tax as a,tbl_tax_description as b WHERE salarybase<='" . $salary . "' AND a.tax_desc_id=b.tax_desc_id AND b.tax_desc_id='" . $taxid . "' ORDER BY salarybase DESc LIMIT 1")->row();
//        $subtotal = $salary - $query->salarybase;
//        $query2 = $this->db->query("SELECT deduction,percent FROM tbl_tax_deduction as a,tbl_tax_percent as b WHERE a.percent_id=b.percent_id AND a.deduction_id=" . $query->deduction_id)->row();
//        $subtotal = ($subtotal * $query2->percent) + $query2->deduction;
//        echo $diff.'<br>';
        echo $subtotal;
    }

    public function UpdateEmployeeValue() {

        $id = $this->input->post('id');
        $idname = $this->input->post('idname');
        $table = $this->input->post('table');
        $name = $this->input->post('name');
        $value = $this->input->post('value');
        $data = array($name => $value);
        // var_dump($data);
        $this->db->where($idname, $id);
        $this->db->update($table, $data);
        if ($this->db->affected_rows() > 0) {
            echo 'Success';
        } else {
            echo 'Fail';
        }
    }

    public function UpdateEmployeeSupervisor() {

        $emp_id = $this->input->post('emp_id');
        $email = $this->input->post('email');
        $supervisor = $this->input->post('supervisor');
        $data = array('Supervisor' => $supervisor, 'supEmail' => $email);
        // var_dump($data);
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_employee', $data);
        if ($this->db->affected_rows() > 0) {
            echo 'Success';
        } else {
            echo 'Fail';
        }
    }

    public function insertNewTaxValue() {

        $taxid = $this->input->post('taxid');
        $emp_id = $this->input->post('emp_id');

        $query = $this->db->query("SELECT taxemp_id FROM tbl_tax_emp where emp_id='" . $emp_id . "' AND isActive=1");
        $result = $query->result();
        if ($result != NULL) {
            foreach ($result as $r) {
                $this->db->where('taxemp_id', $r->taxemp_id);
                $this->db->update('tbl_tax_emp', array('isActive' => '0'));
            }
        }
        $data = array('tax_desc_id' => $taxid, 'emp_id' => $emp_id, 'isActive' => '1');
        // var_dump($data);
        $this->db->insert('tbl_tax_emp', $data);
        if ($this->db->affected_rows() > 0) {
            echo 'Success';
        } else {
            echo 'Fail';
        }
    }

    //----------------------------------------------NICCA-----------------------------------------------
    //----------------------PROMOTE FUNCTIONALITY-------------------------------------------------------
    public function traineeprobiupdate() {
        $empid = $this->input->post('emp_id');
        $posid = $this->input->post('pos_id');
        $class = $this->input->post('class');
        $datestart = $this->input->post('startdate');
        $datestart = explode('-', $datestart);
        $datestart = $datestart[2] . '-' . $datestart[0] . '-' . $datestart[1];
        $dateend = strtotime("+6 months", strtotime($datestart));
        $dateend = strftime('%Y-%m-%d', $dateend);
        $posempstat = $this->EmployeeModel->getpromoteid($empid);

        if ($class == 'Agent') {
            $newposempstat = $this->db->query("SELECT b.posempstat_id as id FROM tbl_position a,tbl_pos_emp_stat b,tbl_emp_stat c,tbl_rate d WHERE a.pos_id=b.pos_id AND c.empstat_id=b.empstat_id AND a.class='Agent' AND c.status='Probationary' AND d.posempstat_id=b.posempstat_id AND d.isActive=1")->row();
        } else {
            $newposempstat = $this->db->query("SELECT posempstat_id as id FROM tbl_pos_emp_stat WHERe pos_id='" . $posid . "' AND empstat_id=2")->row();
        }
        if (empty($newposempstat)) {
            echo 'NoRate';
        } else {
            $data = array(
                'posempstat_id' => $newposempstat->id,
                'emp_id' => $empid,
                'dateFrom' => $datestart,
                'dateTo' => $dateend,
                'isActive' => 1
            );
            $this->db->insert('tbl_emp_promote', $data);
            if ($this->db->affected_rows() > 0) {
                $this->db->where('emp_promoteID', $posempstat->pid);
                $this->db->update('tbl_emp_promote', array('isActive' => 0));
                echo 'Success';
            } else {
                echo 'Failed';
            }
        }
    }

    public function probiregularupdate() {
        $empid = $this->input->post('emp_id');
        $posid = $this->input->post('pos_id');
        $datestart = $this->input->post('startdate');
        $datestart = explode('-', $datestart);
        $datestart = $datestart[2] . '-' . $datestart[0] . '-' . $datestart[1];
        $posempstat = $this->EmployeeModel->getpromoteid($empid);
        $newposempstat = $this->db->query("SELECT posempstat_id as id FROM tbl_pos_emp_stat WHERe pos_id='" . $posid . "' AND empstat_id=3")->row();
        if (empty($newposempstat)) {
            echo 'NoRate';
        } else {
            $data = array(
                'posempstat_id' => $newposempstat->id,
                'emp_id' => $empid,
                'dateFrom' => $datestart,
                'isActive' => 1
            );
            $this->db->insert('tbl_emp_promote', $data);
            if ($this->db->affected_rows() > 0) {

                $this->db->where('emp_promoteID', $posempstat->pid);
                $this->db->update('tbl_emp_promote', array('isActive' => 0));
                echo 'Success';
            } else {
                echo 'Failed';
            }
        }
    }

    public function probiregularupdatecca() {
        $empid = $this->input->post('emp_id');
        $datestart = $this->input->post('startdate');
        $datestart = explode('-', $datestart);
        $datestart = $datestart[2] . '-' . $datestart[0] . '-' . $datestart[1];
        $cca1 = $this->EmployeeModel->getCCAid();
        $posid = $cca1->pos_id;
        $posempstat = $this->EmployeeModel->getpromoteid($empid);
// var_dump($posempstat);
        $newposempstat = $this->db->query("SELECT posempstat_id as id FROM tbl_pos_emp_stat WHERe pos_id='" . $posid . "' AND empstat_id=3")->row();
        if (empty($newposempstat)) {
            echo 'NoRate';
        } else {
            $data = array(
                'posempstat_id' => $newposempstat->id,
                'emp_id' => $empid,
                'dateFrom' => $datestart,
                'isActive' => 1
            );
            $this->db->insert('tbl_emp_promote', $data);
            if ($this->db->affected_rows() > 0) {

                $this->db->where('emp_promoteID', $posempstat->pid);
                $this->db->update('tbl_emp_promote', array('isActive' => 0));
                echo 'Success';
            } else {
                echo 'Failed';
            }
        }
    }

    public function extendupdate() {
        $empid = $this->input->post('emp_id');
        $reason = $this->input->post('reason');
        $posid = $this->input->post('pos_id');
        $enddate = $this->input->post('extenddate');
        $enddate = explode('-', $enddate);
        $enddate = $enddate[2] . '-' . $enddate[0] . '-' . $enddate[1];
        $posempstat = $this->EmployeeModel->getpromoteid($empid);


        $data = array(
            'posempstat_id' => $posempstat->id,
            'emp_id' => $empid,
            'dateFrom' => $posempstat->dateTo,
            'dateTo' => $enddate,
            'eva_note' => $reason,
            'isActive' => 1
        );
        $this->db->insert('tbl_emp_promote', $data);
        if ($this->db->affected_rows() > 0) {

            $this->db->where('emp_promoteID', $posempstat->pid);
            $this->db->update('tbl_emp_promote', array('isActive' => 0));
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    public function failupdate() {
        $empid = $this->input->post('emp_id');
        $reason = $this->input->post('reason');
        $posid = $this->input->post('pos_id');
        $faildate = $this->input->post('faildate');
        $faildate = explode('-', $faildate);
        $faildate = $faildate[2] . '-' . $faildate[0] . '-' . $faildate[1];
        $posempstat = $this->EmployeeModel->getpromoteid($empid);

        $row = $this->EmployeeModel->employeefail($empid, $reason, $faildate);
        if ($row > 0) {

            $this->db->where('emp_promoteID', $posempstat->pid);
            $this->db->update('tbl_emp_promote', array('isActive' => 0));
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    public function regularupdate() {

        $empid = $this->input->post('empid');
        $date = $this->input->post('date');
        $posid = $this->input->post('posid');

        $posempstat = $this->EmployeeModel->getpromoteid($empid);
        $newposempstat = $this->EmployeeModel->getposempstat($posid, 3);
        if (!empty($newposempstat)) {
            $row = $this->EmployeeModel->promoteregular($empid, $newposempstat->id, $date);
            if ($row > 0){

                $this->db->where('emp_promoteID', $posempstat->pid);
                $this->db->update('tbl_emp_promote', array('isActive' => 0));
                echo 'Success';
            }else{
                echo 'Failed';
            }
        } else {
            echo "NoRate";
        }
    }

    //--------------------------------------------------FOR TBL_USER----------------------------------------
    public function intoTblUser() {
        $empid = $this->input->post('empid');
        $empname = $this->input->post('name');
        $value = $this->input->post('value');
        $res = $this->EmployeeModel->getTblUserData($empid);
        // var_dump($res);
        // echo $name;
        if ($res == NULL || $res == '') {
            $name = $this->EmployeeModel->getName($empid);
            if ($name == NULL || $name == '') {
                echo 'Failed1';
            } else {
                if ($empname == 'username') {
                    $rows = $this->EmployeeModel->insertTblUserUname($empid, $value, $name->fname, $name->lname);
                } else if ($empname == 'password') {
                    $rows = $this->EmployeeModel->insertTblUserPass($empid, $value, $name->fname, $name->lname);
                } else if ($empname == 'role_id') {
                    $dep = $this->EmployeeModel->getEmpDept($empid);
                    $roledesc = $this->EmployeeModel->getRoleDesc($value);
                    $roler = $roledesc->description . '|' . $dep->dep_name;
                    $rows = $this->EmployeeModel->insertTblUserRole($empid, $value, $name->fname, $name->lname, $roler);
                } else {
                    $rows = -1;
                }
                if ($rows > 0) {
                    echo 'Success';
                } else {
                    echo 'Failed2';
                }
            }
        } else {
            if ($empname == 'username') {
                $rows = $this->EmployeeModel->updatetTblUserUname($empid, $value);
            } else if ($empname == 'password') {
                $rows = $this->EmployeeModel->updatetTblUserPass($empid, $value);
            } else if ($empname == 'role_id') {
                $dep = $this->EmployeeModel->getEmpDept($empid);
                $roledesc = $this->EmployeeModel->getRoleDesc($value);
                $roler = $roledesc->description . '|' . $dep->dep_name;
                $rows = $this->EmployeeModel->updatetTblUserRole($empid, $value, $roler);
            } else {
                $rows = -1;
            }
            if ($rows > 0) {
                echo 'Success';
            } else {
                echo 'Failed3';
            }
        }
    }

    //-------------------------------ONESHOT---------------------------------------

    public function sync() {
        $employees = $this->EmployeeModel->getAllActiveEmployees();
        foreach ($employees as $emp) {
            $res = $this->EmployeeModel->getTblUserData($emp->emp_id);
            if ($res == NULL || $res == '') {
                $rows = $this->EmployeeModel->insertTblUser($emp->emp_id, $emp->dtr_uname, $emp->dtr_password, $emp->fname, $emp->lname, $emp->dep_name);
                if ($rows > 0) {
                    echo 'Success <br>';
                } else {
                    echo 'Failed <br>';
                }
                // echo 'EMPTY <br>';
            } else {
                $role = $res->role;
                $role = explode('|', $role);
                $rows = $this->EmployeeModel->updatetTblUser($emp->emp_id, $emp->dtr_uname, $emp->dtr_password, $emp->fname, $emp->lname, "$role[0]|$emp->dep_name");
                if ($rows > 0) {
                    echo 'Success2 <br>';
                } else {
                    echo 'Failed2 <br>';
                }
                // echo 'NAA <br>';
            }
        }
    }

    //------------------------SECURITY--------------------------
    public function updateConfidentiality() {
        $emp_id = $this->input->post('emp_id');
        $activevalue = $this->input->post('activevalue');
        // $emp_id = 350;
        // $activevalue = 0;
        $row = $this->EmployeeModel->updateConfidentiality($emp_id, $activevalue);
        if ($row > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    public function matchUserCode() {
        $user = $this->session->userdata('uid');
        $code = $this->input->post('password');
        $data = $this->EmployeeModel->matchUserCode($user, $code);
        if ($data == '' || $data == NULL) {
            echo 'Empty';
        } else {
            echo 'Exists';
        }
    }

    public function matchUserCodeWithReturn() {
        $user = $this->session->userdata('uid');
        $code = $this->input->post('password');
        $data = $this->EmployeeModel->matchUserCode($user, $code);
        if ($data == '' || $data == NULL) {
            echo 'Empty';
        } else {
            echo json_encode($data);
        }
    }

    public function matchEmployeeCode() {
        // $user = $this->input->post('empid');

        $user = $this->session->userdata('uid');
        $code = $this->input->post('password');
        // $code = "xx";
        $data = $this->EmployeeModel->matchEmployeeCode($user, $code);
        if ($data == '' || $data == NULL) {
            echo 'Empty';
        } else {
            echo 'Exists';
        }
    }

// EMPLOYEE EMAIL ADD
    public function employeeEmail() {
        $data['empemail'] = $this->EmployeeModel->getEmpEmail();
        $this->load->view('employeeEmail', $data);
    }

    public function employeeAnniversary() {
        $data['empanniv'] = $this->EmployeeModel->getEmpAnniv();
        $this->load->view('employeeAnniversary', $data);
    }

    // FOR VIEW IN EMPLOYEE

    public function getEmployeePreview() {
        $emp_id = $this->input->post('emp_id');
        $values = $this->EmployeeModel->getEmpPreview($emp_id);
        echo json_encode($values);
    }

    public function deactivateEmployee() {
        $empid = $this->input->post('empid');
        $resigntype = $this->input->post('resigntype');
        $clearance = $this->input->post('clearance');
        $rehire = $this->input->post('rehire');
        $separation = $this->input->post('separationdate');
        $resigndetails = $this->input->post('resigndetails');
        $row = $this->EmployeeModel->deactivateEmployee($empid, $resigntype, $clearance, $rehire, $separation, $resigndetails);
        if ($row > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    public function updatePagibigContrib() {
        $emp_id = $this->input->post('emp_id');
        $amount = $this->input->post('amount');
        $this->EmployeeModel->resetAllPagibigContrib($emp_id);
        $pagibigContrib = $this->EmployeeModel->getPagibigContrib($emp_id, $amount);
        if ($pagibigContrib == NULL || $pagibigContrib == '') {
            $row = $this->EmployeeModel->insertPagibigContrib($emp_id, $amount);
        } else {
            $row = $this->EmployeeModel->reactivatePagibigContrib($pagibigContrib->pibig_add_id);
        }
        $result = array();
        if ($row > 0) {
            $result['status'] = 'Success';
            $result['amount'] = $amount;
        } else {
            $result['status'] = 'Failed';
        }
        echo json_encode($result);
    }
    public function uploadPic(){ 
    		$employee_ID= $this->input->post("employee_ID");
		/*-------------------------------*/
		
		$data= $this->input->post("img");
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);
		$newfilename = round(microtime(true)).'.png';
		$target_dir = "./assets/images/img/";
		file_put_contents($target_dir.$newfilename, $data);
		/*-------------------------------*/
		
		$query =  $this->db->query("update tbl_applicant set pic='img/".$newfilename."' where apid=".$employee_ID);
		if($query){
			 
			echo "ok";
		}else{
			echo "fail";
		}
	}
}
