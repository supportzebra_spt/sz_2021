<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Latetracking extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
			public function __construct(){
				parent::__construct();
			}
			
			public function get_account(){
				$accType = $this->input->post('accType');
				$query = $this->db->query("SELECT * FROM tbl_account WHERE acc_description = '$accType' order by acc_name");
					if ($query->num_rows() > 0){
						foreach ($query->result() as $row){
							echo "<option value=".$row->acc_id."> ".$row->acc_name."</option>";
						}
					}			
			}
			/* public function index(){
				redirect('index.php/home/error');
			}   */ 
		  public function index(){ //Down for the moment..

		if($this->session->userdata('uid')){
				$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='latetracking' and is_assign=1");
				foreach ($qry->result() as $row)
				{
						$arr['setting'] = array("settings" => $row->settings);
				}
				if ($qry->num_rows() > 0){
						$this->load->view('late_track',$arr);
				}else{
					redirect('index.php/home/error');
				}
				}else{
							redirect('index.php/login');
				}
		} 
			public function get_emp_dtr_details(){
		             $accType = $this->input->post('accType');
		             $account = $this->input->post('account');
		             $dated = $this->input->post('date');
					 $data = [];
					 $date = explode("-",$dated);
					 $emp = $this->emp_list_per_account($account);
					 if( $accType == 'Admin'){ // If Employee is Admin
					 $data['headers'] = ["Date","Shift","Log In","Log Out","Total"];
						foreach($emp as $k => $value){
							$data['user'][$value->emp_id] = ['emp_name'=> $value->fname." ".$value->lname];
							$array_emp_id[] = $value->emp_id;
						}
					$dateRange =  $this->DateRangeShift($date);
						foreach($dateRange as $key => $valz){
 
							$emp_logs1 =$this->get_logs($valz,$array_emp_id);
 
								foreach($emp_logs1 as $susi => $knob){
									$time_one = new DateTime($knob->LOG_IN);
									$time_two = new DateTime($knob->LOG_OUT);
									$difference = $time_one->diff($time_two);
									
									
									$knob->LOG_OUT = ($knob->LOG_OUT == null) ? $knob->LOG_OUT :  date('Y/m/d h:i a ', strtotime($knob->LOG_OUT));
									 $totalTime = ($knob->LOG_OUT == null) ? "--" : $difference->format('%h hours %i minutes');
									$loginz = date('Y/m/d h:i a', strtotime($knob->LOG_IN));
									$data['user'][$knob->emp_id][$valz] = [
										'schedule' => date('h:i a', strtotime($knob->time_start))."-".date('h:i a', strtotime($knob->time_end)),
										'log_in' => $loginz,
										'log_out'=> $knob->LOG_OUT,
										'total'=> $totalTime
										];
								}
	
					 }}else{ //Agentz
    $data['headers'] = ["Date","Shift","Log In","First Break","Lunch Break","Last Break","Log Out","Total Break","Total Time"];
						foreach($emp as $k => $value){
							$data['user'][$value->emp_id] = ['emp_name'=> $value->fname." ".$value->lname];
							$array_emp_id[] = $value->emp_id;
						}
					$dateRange =  $this->DateRangeShift($date);
	foreach($dateRange as $key => $valz){
 
							$emp_logs1 =$this->get_logs($valz,$array_emp_id);
 								foreach($emp_logs1 as $susi => $knob){
									$day = date('Y-m-d', strtotime($valz));
						
						$emp_break = $knob->emp_id."|".$day."|".$knob->LOG_IN."|".$knob->LOG_OUT;
 						$break_lunch_logs = $this->get_break_lunch_logs($emp_break);
						// var_dump($break_lunch_logs);
 							$time_one = new DateTime($knob->LOG_IN);
							$time_two = new DateTime($knob->LOG_OUT);
							$difference = $time_one->diff($time_two);
							$totalTime = ($knob->LOG_OUT == null) ? "--" : $difference->format('%h hours %i minutes');

						  $data['user'][$knob->emp_id][$day] = [
                    'schedule' => date('h:i a', strtotime($knob->time_start))."<br>".date('h:i a', strtotime($knob->time_end)),
                      'log_in' => date('Y/m/d h:i a ', strtotime($knob->LOG_IN)),
                      'first_break' => "<span style = 'color:red;'>NO FIRST BREAK</span>",
                      'lunch' => "<span style = 'color:red;'>NO LUNCH</span>",
                      'last_break' => "<span style = 'color:red;'>NO LAST BREAK</span>",
                      'log_out'=>  $knob->LOG_OUT, 
                      'over_break'=> " -- " ,
                      'Total'=> $totalTime
                    ];
					$fb_time=0;
					$lb_time=0;
					$lb2_time=0;
									  // $fb = "<span style = 'color:red;'>NO FIRST BREAK</span>";
									  // $lb = "<span style = 'color:red;'>NO LUNCH</span>";
									  // $lb2 = "<span style = 'color:red;'>NO LUNCH</span>";
									
									   $fb = " ";
									  $lb = " ";
									  $lb2 = " ";
						foreach($break_lunch_logs as $bkey => $bval){
							if($bval->break_type=="FIRST BREAK"){
								if(!empty($bval->break_type) ){ 
									$fb = date('Y/m/d h:i a ', strtotime($bval->BREAK_OUT))." ".date('Y/m/d h:i a ', strtotime($bval->BREAK_IN));
									$fb_time   = round(abs(strtotime($bval->BREAK_OUT) - strtotime($bval->BREAK_IN)) / 60);
								}else{
									$fb = " ";
								}
							}
							if($bval->break_type=="LUNCH"){
								if(!empty($bval->break_type) ){
									// $lb = $bval->BREAK_OUT." ".$bval->BREAK_IN;
									$lb = date('Y/m/d h:i a ', strtotime($bval->BREAK_OUT))." ".date('Y/m/d h:i a ', strtotime($bval->BREAK_IN));
									$lb_time   = round(abs(strtotime($bval->BREAK_OUT) - strtotime($bval->BREAK_IN)) / 60);
								}else{
									$lb = " ";
								}
							}
							if($bval->break_type=="LAST BREAK"){
								if(!empty($bval->break_type) ){
									// $lb2 = $bval->BREAK_OUT." ".$bval->BREAK_IN;
									$lb2 = date('Y/m/d h:i a ', strtotime($bval->BREAK_OUT))." ".date('Y/m/d h:i a ', strtotime($bval->BREAK_IN));
									$lb2_time   = round(abs(strtotime($bval->BREAK_OUT) - strtotime($bval->BREAK_IN)) / 60);
 								}else{
									$lb2 = " ";
								}
							}
							$totalmis = $fb_time+$lb_time+$lb2_time;
							$time_one = new DateTime($knob->LOG_IN);
							$time_two = new DateTime($knob->LOG_OUT);
							$difference = $time_one->diff($time_two);
							$totalTime = ($knob->LOG_OUT == null) ? "--" : $difference->format('%h hours %i minutes');

							$knob->LOG_OUT = ($knob->LOG_OUT == null) ? $knob->LOG_OUT :  date('Y/m/d h:i a ', strtotime($knob->LOG_OUT));

									$data['user'][$knob->emp_id][$day] = [
                    'schedule' => date('h:i a', strtotime($knob->time_start))."<br>".date('h:i a', strtotime($knob->time_end)),
                      'log_in' => date('Y/m/d h:i a ', strtotime($knob->LOG_IN)),
                      'first_break' => $fb,
                      'lunch' =>  $lb,
                      'last_break' => $lb2,
                      'log_out'=> $knob->LOG_OUT, 
                      'Totalbreak'=> $totalmis." mins.",
                      'Total'=> $totalTime
                    ];
						
											}
											}
	
					 }
					 }
	echo json_encode($data);
	return  $data;
	die();
  }
	public function emp_list_per_account($account){
		// $emplist = $this->db->query("SELECT b.emp_id,a.fname,a.lname FROM tbl_applicant a LEFT JOIN tbl_employee b ON a.apid = b.apid WHERE b.acc_id = $account and b.isActive = 'yes'");
		$emplist = $this->db->query("SELECT b.emp_id,a.fname,a.lname FROM tbl_applicant a LEFT JOIN tbl_employee b ON a.apid = b.apid WHERE b.isActive='Yes' and b.acc_id = $account ");
		
 		return $emplist->result();
	}
	 public function DateRangeShift($date){
    $strDateFrom = date("Y-m-d", strtotime($date[0]));
    $strDateTo = date("Y-m-d", strtotime($date[1]));
    $aryRange=array();
    $iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));

	
	if($iDateTo>=$iDateFrom){
		array_push($aryRange,date('Y-m-d',$iDateFrom));
		
		 while($iDateFrom<$iDateTo){
            $iDateFrom+=86400; // add 24 hours

                          array_push($aryRange,date('Y-m-d',$iDateFrom));

            }
	}
return $aryRange;	
  }
   public function get_logs($date,$arrayEmployees){
		$emp_id = implode(',', $arrayEmployees);
		$date_from = $date." 00:00:00";
		$date_to = $date." 24:59:59";
$query = "	SELECT a.dtr_id,
            a.emp_id,
            a.entry,
            a.log as 'LOG_IN',
            (SELECT Log from tbl_dtr_logs d where d.note = a.dtr_id limit 1) as 'LOG_OUT',
            a.type,
            c.time_id,
            c.time_start,
            c.time_end 
            FROM `tbl_dtr_logs` a 
            LEFT JOIN tbl_acc_time b 
            on a.acc_time_id = b.acc_time_id 
            LEFT JOIN tbl_time c on b.time_id = c.time_id
            where emp_id in ($emp_id)  and a.entry = 'I' and a.type = 'DTR' and log >='$date_from' and log <= '$date_to'";
   $emp_logs =  $this->db->query($query);
    return $emp_logs->result();

 }
 public function get_break_lunch_logs($data)
{
  $explode_data = explode("|", $data);
  $query = "SELECT a.dtr_id,
            a.log as 'BREAK_OUT',
            (SELECT Log from tbl_dtr_logs d where d.note = a.dtr_id limit 1) as 'BREAK_IN',
            f.break_type
            FROM `tbl_dtr_logs` a 
            LEFT JOIN tbl_acc_time b 
            on a.acc_time_id = b.acc_time_id 
            LEFT JOIN tbl_time c on b.time_id = c.time_id
            LEFT JOIN tbl_break_time_account e on c.bta_id = e.bta_id
            LEFT JOIN tbl_break f on e.break_id = f.break_id
            where emp_id in ($explode_data[0])  and a.entry = 'O' and a.type = 'BREAK' and log >' $explode_data[2]' and log < ' $explode_data[3]'";        
		  // echo $query."<br>";
   $emp_logs =  $this->db->query($query);
    return $emp_logs->result();
}
		public function export()
		{
	

		             $accType = $this->input->post('accType');
		             $account = $this->input->post('account');
		             $dated = $this->input->post('date');
					 $data = [];
					 $date = explode("-",$dated);
					 $emp = $this->emp_list_per_account($account);
					 if( $accType == 'Admin'){
					 $data['headers'] = ["Date","Shift","Log In","Log Out"];
						foreach($emp as $k => $value){
							$data['user'][$value->emp_id] = ['emp_name'=> $value->fname." ".$value->lname];
							$array_emp_id[] = $value->emp_id;
						}
					$dateRange =  $this->DateRangeShift($date);
						foreach($dateRange as $key => $valz){
 
							$emp_logs1 =$this->get_logs($valz,$array_emp_id);
 
								foreach($emp_logs1 as $susi => $knob){
									$knob->LOG_OUT = ($knob->LOG_OUT == null) ? $knob->LOG_OUT :  date('Y/m/d h:i a ', strtotime($knob->LOG_OUT));
									$data['user'][$knob->emp_id][$valz] = [
										'schedule' =>date('h:i a', strtotime($knob->time_start))."-".date('h:i a', strtotime($knob->time_end)),
										'log_in' => date('Y/m/d h:i a ', strtotime($knob->LOG_IN)),
										'log_out'=>$knob->LOG_OUT 
										];
								}
	
					 }}else{ //Agentz
    $data['headers'] = ["Date","Shift","Log In","First Break","Lunch Break","Last Break","Log Out","Total Break"];
						foreach($emp as $k => $value){
							$data['user'][$value->emp_id] = ['emp_name'=> $value->fname." ".$value->lname];
							$array_emp_id[] = $value->emp_id;
						}
					$dateRange =  $this->DateRangeShift($date);
	foreach($dateRange as $key => $valz){
 
							$emp_logs1 =$this->get_logs($valz,$array_emp_id);
 								foreach($emp_logs1 as $susi => $knob){
									$day = date('Y-m-d', strtotime($valz));
						
						$emp_break = $knob->emp_id."|".$day."|".$knob->LOG_IN."|".$knob->LOG_OUT;
 						$break_lunch_logs = $this->get_break_lunch_logs($emp_break);
						// var_dump($break_lunch_logs);
			$knob->LOG_OUT = ($knob->LOG_OUT == null) ? $knob->LOG_OUT :  date('Y/m/d h:i a ', strtotime($knob->LOG_OUT));

						  $data['user'][$knob->emp_id][$day] = [
                    'schedule' => date('h:i a', strtotime($knob->time_start))."<br>".date('h:i a', strtotime($knob->time_end)),
                      'log_in' => date('Y/m/d h:i a ', strtotime($knob->LOG_IN)),
                      'first_break' => "NO FIRST BREAK",
                      'lunch' => "NO LUNCH",
                      'last_break' => "NO LAST BREAK",
                      'log_out'=> $knob->LOG_OUT, 
                      'over_break'=> " " 
                    ];
					$fb_time=0;
					$lb_time=0;
					$lb2_time=0;
									$fb = "NO FIRST BREAK";
									$lb = "NO LUNCH";
									$lb2 = "NO LAST BREAK";
						foreach($break_lunch_logs as $bkey => $bval){
							if($bval->break_type=="FIRST BREAK"){
								if(!empty($bval->break_type) ){
									// $fb = $bval->BREAK_OUT." - ".$bval->BREAK_IN;
									$fb = date('Y/m/d h:i a ', strtotime($bval->BREAK_OUT))." ".date('Y/m/d h:i a ', strtotime($bval->BREAK_IN));
									$fb_time   = round(abs(strtotime($bval->BREAK_OUT) - strtotime($bval->BREAK_IN)) / 60);
								}else{
									$fb = " ";
								}
							}
							if($bval->break_type=="LUNCH"){
								if(!empty($bval->break_type) ){
									// $lb = $bval->BREAK_OUT." - ".$bval->BREAK_IN;
									$lb = date('Y/m/d h:i a ', strtotime($bval->BREAK_OUT))." ".date('Y/m/d h:i a ', strtotime($bval->BREAK_IN));
									$lb_time   = round(abs(strtotime($bval->BREAK_OUT) - strtotime($bval->BREAK_IN)) / 60);
								}else{
									$lb = " ";
								}
							}
							if($bval->break_type=="LAST BREAK"){
								if(!empty($bval->break_type) ){
									// $lb2 = $bval->BREAK_OUT." - ".$bval->BREAK_IN;
									$lb2 = date('Y/m/d h:i a ', strtotime($bval->BREAK_OUT))." ".date('Y/m/d h:i a ', strtotime($bval->BREAK_IN));
									$lb2_time   = round(abs(strtotime($bval->BREAK_OUT) - strtotime($bval->BREAK_IN)) / 60);
 								}else{
									$lb2 = " ";
								}
							}
							$totalmis = $fb_time+$lb_time+$lb2_time;
				$knob->LOG_OUT = ($knob->LOG_OUT == null) ? $knob->LOG_OUT :  date('Y/m/d h:i a ', strtotime($knob->LOG_OUT));

									$data['user'][$knob->emp_id][$day] = [
                    'schedule' => date('h:i a', strtotime($knob->time_start))." - ".date('h:i a', strtotime($knob->time_end)),
                      'log_in' => date('Y/m/d h:i a ', strtotime($knob->LOG_IN)),
                      'first_break' => $fb,
                      'lunch' =>  $lb,
                      'last_break' => $lb2,
                      'log_out'=>$knob->LOG_OUT, 
                      'Totalbreak'=> $totalmis." mins."
                    ];
						
											}
											}
	
					 }
					 }
	 $file =  $this->export_excel($data);
	 // $file =  $this->export_excel2($data);
	
 
//	return  $data;
 }
 public function export_excel2($data){
  	 foreach($data as $k => $v){
 
		 if($k=="user"){
			foreach($v as $key => $val){
				  
				  foreach($val as $susi => $knobb){
					  
							if($susi =="emp_name"){
								
								echo json_encode($knobb);
								
							}
							}
				  
				  
				}
		 }

	}
 }
 public function export_excel($data){
	$objPHPExcel = $this->phpexcel;
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setName('Logo');
	$objDrawing->setDescription('Logo');
	$objDrawing->setPath('./assets/images/logo2.png');
	$objDrawing->setHeight(100);
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
	$objPHPExcel->getActiveSheet()->getStyle("A1:D5")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
	$objPHPExcel->getActiveSheet()->getStyle("D1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('000000');
	$objPHPExcel->getActiveSheet()->getStyle("D3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('000000');
	$objPHPExcel->getActiveSheet()->setCellValue('D1','Downloaded by:')->getStyle('D1')->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
	$objPHPExcel->getActiveSheet()->setCellValue('D2',$this->session->userdata('fname')." ".$this->session->userdata('lname'))->getStyle('D1')->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
	$objPHPExcel->getActiveSheet()->setCellValue('D3','Downloaded on:')->getStyle('D3')->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
	$objPHPExcel->getActiveSheet()->mergeCells("D4:D5")->setCellValue('D4',date("Y-m-d")." ".date("h:i a"))->getStyle('D3')->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
 
	$col = 0;
      $row = 6;
      $freeze ="";
 $border_style = array(
 'font' => array(
'bold' => true,
),
'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
'color' => array('argb' => '000000'),

),
),
);
 $border_style2 = array(
 
'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
'color' => array('argb' => '000000'),

),
),
);

	foreach($data as $k => $v){
		
		 if($k=="headers"){
			foreach($v as $key => $val){        
			$al = $this->toNum($key);           

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$val);
		$objPHPExcel->getActiveSheet()->getStyle($al."6:".$al."6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('58d3a2');
		$objPHPExcel->getActiveSheet()->getStyle($al."6:".$al."6")->applyFromArray($border_style);

		$objPHPExcel->getActiveSheet()->getColumnDimension($al)->setAutoSize(true);
		$col++;

 			}
		 }

	}
	$okss = $col-1;
	 
	$col=0;
	$row++;
	      // $objPHPExcel->getActiveSheet()->freezePane('C7');
$last = $this->toNum($okss);  
	 foreach($data as $k => $v){
		
		 if($k=="user"){
			foreach($v as $key => $val){
				  
				  foreach($val as $susi => $knobb){
					  
							if($susi =="emp_name"){
 							  $col = 0;
 	$objPHPExcel->getActiveSheet()->mergeCells("A".$row.":".$last."".$row);
								$objPHPExcel->getActiveSheet()->getStyle("A".$row.":".$last."".$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('b5ffe1');
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$knobb);
									$objPHPExcel->getActiveSheet()->getStyle("A".$row.":".$last."".$row)->applyFromArray($border_style);

 								$row++;
							}else{
							$col = 0;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$susi);
							$col++;	
								foreach($knobb as $other_k => $other_v){	 
								 
									if($other_v=="NO FIRST BREAK" ||$other_v=="NO LUNCH" || $other_v=="NO LAST BREAK" ){
							$al = $this->toNum($col);           

							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$other_v);
 
							$col++;	

									}else{
										if($other_v==null){
											$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row," -- ");

										}else{
											$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$other_v);

										}
								
							$col++;	
									}
						

									
								}
							$objPHPExcel->getActiveSheet()->getStyle("A".$row.":".$last."".$row)->applyFromArray($border_style2);

							$row++;

							}
							}
				  
				  
				}
		 }

	}
		$file = 'DTR_LOGS.xlsx';
      $path= "files/".$file;
 		$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
      $objPHPExcel->getActiveSheet()->getProtection()->setPassword('pazzword');

	  header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");;
      header("Content-Disposition: attachment; filename=$file");
      header("Content-Transfer-Encoding: binary ");
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007'); 
	  
       $objWriter->save($path);
	   //$objWriter->save('php://output');

      ob_clean();
      return $path;
 }
 
     public function exportExcel() {
		
		$file = 'DTR_LOGS.xlsx';
      $path= "files/".$file;
	  header("Content-Description: File Transfer");
        header("Content-Type: application/octet-stream");
        header('Content-Disposition: attachment; filename="'.basename($path).'"');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header("Content-Type: application/force-download");
        header("Content-Type: application/download");
        header("Content-Length: ".filesize($path));
		// header("Location: ".$path);
	        readfile($path);
 
	}
    public function toNum($data) {
    $alphabet = array( 'a', 'b', 'c', 'd', 'e',
                       'f', 'g', 'h', 'i', 'j',
                       'k', 'l', 'm', 'n', 'o',
                       'p', 'q', 'r', 's', 't',
                       'u', 'v', 'w', 'x', 'y',
                       'z'
                       );
    
    return $alphabet[$data];
}
 
 }
