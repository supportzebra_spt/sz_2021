
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey extends CI_Controller {

	public function __construct(){
		parent::__construct(); 
		$this->load->model('SurveyModel');
	}
	public function index(){
		$this->load->view('surveymain');
	}
	public function change(){
		$data['survey'] = $this->SurveyModel->getAllSurvey();
		$this->load->view('surveychange',$data);
	}
	public function getSequenceQuestion(){
		$sequence = $this->input->post('sequence');
		$data = $this->SurveyModel->getSequenceQuestion($sequence);
		if(empty($data)){
			echo "How can I be of help?";
		}else{
			echo $data->question;
		}
	}
	public function getHeaderQuestion(){
		$data = $this->SurveyModel->getHeaderQuestion();
		if(empty($data)){
			echo "How are you today?";
		}else{
			echo $data->headerquestion;
		}
	}
	public function createnewsurvey(){
		$uid = $this->session->userdata('uid'); 
		$date = date('Y-m-d H:i:s');
		$headerquestion = $this->input->post('headerquestion');
		$level1 = $this->input->post('level1');
		$level2 = $this->input->post('level2');
		$level3 = $this->input->post('level3');
		$level4 = $this->input->post('level4');
		$level5 = $this->input->post('level5');
		$survey_id = $this->SurveyModel->createnewsurvey($headerquestion,$uid,$date);
		if(empty($survey_id)){
			echo 'Failed';
		}else{
			for($x=1;$x<=5;$x++){
				$this->SurveyModel->createnewsurveyans(${'level' . $x},$uid,$date,$survey_id,$x);
			}
		}
	}
	public function setActiveSurvey(){
		$survey_id = $this->input->post('survey_id');
		$this->SurveyModel->deactivateAllSurvey();
		$row = $this->SurveyModel->setActiveSurvey($survey_id);
		if($row>0){
			echo "Success";
		}else{
			echo "Failed";
		}
	}

	//////
	public function campaignnotification(){
		$uid = $this->session->userdata('uid'); 
		$acc = $this->SurveyModel->getUserAcc_id($uid);
		$data['values'] = $this->SurveyModel->getCampNotif($acc->acc_id);
		$this->load->view('campaignnotification',$data);
	}
	public function managecampaign(){
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Survey/managecampaign' and is_assign=1 order by lname");
			if ($qry->num_rows() > 0){
				$data['campaigns'] = $this->SurveyModel->getAgentCampaigns();
				$this->load->view('managecampaign',$data);
			}else{
				redirect('index.php/home/error');
			}
		}
	}

	public function createnewcampnotif(){
		$title = $this->input->post('title');
		$message = $this->input->post('message');
		$acc_id = $this->input->post('acc_id');
		$row = $this->SurveyModel->createnewcampnotif($title,$message,$acc_id);
		if($row>0){
			echo "Success";
		}else{
			echo "Failed";
		}
	}
	function cmp($a, $b)
	{
		return strcmp($a->acc_name, $b->acc_name);
	}
	//CODES FOR OPENING THE NOTIFICATION//

	// <script type="text/javascript">
	// 	window.open("index.php/Survey/campaignnotification",'nicca', 'width=600,height=370,menubar=0,resizable=0,status=0,titlebar=0,toolbar=0');
	// </script>

	//END OF CODES//KULANG OG BASE_URL NGA CODE
	public function getNotificationPerCampaign($acc_id){
		$data = $this->SurveyModel->getNotificationPerCampaign($acc_id);
		if($data==NULL || $data==''){
			echo "Empty";
		}else{
			echo json_encode($data);
		}
	}

	public function getAccNotificationList($acc_id){
		echo json_encode($this->SurveyModel->getAccNotificationList($acc_id));
	}
	public function updatecampnotifActive(){
		$acc_id = $this->input->post('acc_id');
		$cn_id = $this->input->post('cn_id');
		$r = $this->SurveyModel->updatecampnotifActive(1,$cn_id);
		if($r>0){
			$row = $this->SurveyModel->deactivateAllNotifyAcc($acc_id,$cn_id);
			echo "Success";
		}else{
			echo "Failed";
		}
	}
	public function updatecampnotifInactive(){
		$cn_id = $this->input->post('cn_id');
		$r = $this->SurveyModel->updatecampnotifActive(0,$cn_id);
		if($r>0){
			echo "Success";
		}else{
			echo "Failed";
		}
	}
	public function getNotifdetails($cn_id){
		echo json_encode($this->SurveyModel->getNotifdetails($cn_id));
	}
	public function updatenotifdetails(){
		$cn_id = $this->input->post('cn_id');
		$message = $this->input->post('message');
		$title = $this->input->post('title');
		$row = $this->SurveyModel->updatenotifdetails($cn_id,$title,$message);
		if($row>0){
			echo "Success";
		}else{
			echo "Failed";
		}
	}
	public function getActiveNotification(){
		$uid = $this->session->userdata('uid'); 
		$acc = $this->SurveyModel->getUserAcc_id($uid);
		echo json_encode($this->SurveyModel->getCampNotif($acc->acc_id));
	}
}
