<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dtr extends CI_Controller {

  public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Manila");
			}
	
	public function index(){ 
				$arr= array();
			if($this->session->userdata('uid')){
 					// if(in_array($this->session->userdata('uid'),$empz)){
					// if( $_SESSION["acc_description"]=="Admin"){
					if(true){
						// redirect('index.php/dtr/forbidden');
						$data['page'] = "dtrpage";

					// $this->load->view('multiplesched', $data);
						$this->load->view('forbidden', $data);

						
					}else{
 				// $dtr =  $this->db->query("select dtr_id,a.emp_id,entry,log,type,sched_date,b.acc_time_id from tbl_dtr_logs a, tbl_schedule b where a.sched_id = b.sched_id and a.emp_id=".$this->session->userdata('emp_id')." and type='DTR' order by dtr_id DESC limit 1");
 				// $dtr =  $this->db->query("select dtr_id,a.emp_id,entry,abs(time_to_sec(timediff(log,now()))/3600) as diff,log,type,sched_date,b.sched_id,b.acc_time_id,(select concat(time_start,' - ',time_end) from tbl_acc_time x,tbl_time y where x.time_id=y.time_id and x.acc_time_id = b.acc_time_id) as shift from tbl_dtr_logs a, tbl_schedule b where a.sched_id = b.sched_id and a.emp_id=".$this->session->userdata('emp_id')." and type='DTR' order by dtr_id DESC limit 1");
 				$dtr =  $this->db->query("select dtr_id,a.emp_id,entry,sched_id,abs(time_to_sec(timediff(log,now()))/3600) as diff,log,type,a.acc_time_id,(select concat(time_start,' - ',time_end) from tbl_acc_time x,tbl_time y where x.time_id=y.time_id and x.acc_time_id = a.acc_time_id) as shift from tbl_dtr_logs a where a.emp_id=".$this->session->userdata('emp_id')." and type='DTR' order by log DESC limit 1");
				$defaultSched =  $this->db->query("SELECT acc_time_id,concat(time_start,' - ',time_end) as shift  FROM `tbl_employee` a, tbl_account b, tbl_acc_time c,tbl_time d where a.acc_id= b.acc_id and a.acc_id=c.acc_id and c.time_id = d.time_id and emp_id=".$this->session->userdata('emp_id')." and c.isDefault=1 limit 1");
							$arr['defaultSched'] =  $defaultSched->result()[0]->acc_time_id;
							$arr['defaultshift'] =  $defaultSched->result()[0]->shift;
				$schedTypes = $this->db->query("SELECT type as typ FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id=b.schedtype_id and emp_id=".$this->session->userdata('emp_id')." and sched_date=curdate()");
 					if(count($dtr->result())>0){
					
							$arr['dtr_log'] = array("entry" => $dtr->result()[0]->entry);
							$arr['fb'] = array("break" => $dtr->result()[0]->dtr_id);
							$krb = $this->breakAvailable();
 							$lastDTRlog = $this->lastDTRlog($dtr->result()[0]->emp_id);
							 // $arr['schedTypes'] =  $schedTypes->result()[0]->typ;
							 $arr['schedTypes'] =  ($schedTypes->num_rows()>0) ?  $schedTypes->result()[0]->typ : "none" ;

							$fbb = $this->firstBreak($dtr->result()[0]->dtr_id,$dtr->result()[0]->emp_id);
							$lunchb = $this->lunchBreak($dtr->result()[0]->dtr_id,$dtr->result()[0]->emp_id);
							$lbb = $this->lastBreak($dtr->result()[0]->dtr_id,$dtr->result()[0]->emp_id);
							$arr['breakz'] = array("break" => (count($krb)>0) ? $krb[0]->brk : 0 );
							if($dtr->result()[0]->entry=='I'){
							$arr['fbs'] = array("break" => (count($fbb)>0) ? $fbb[0]->diff : 0 );
							$arr['lunchs'] = array("break" => (count($lunchb)>0) ? $lunchb[0]->diff : 0 );
							$arr['lbs'] = array("break" => (count($lbb)>0) ? $lbb[0]->diff : 0 );
							
							$arr['fbs_details'] = array("break" => (count($fbb)>0) ? $fbb[0]->outt." <br> I: ".$fbb[0]->innn  : 0 );
							$arr['lunchs_details'] = array("break" => (count($lunchb)>0) ? $lunchb[0]->outt." <br> I: ".$lunchb[0]->innn : 0 );
							$arr['lbs_details'] = array("break" => (count($lbb)>0) ? $lbb[0]->outt." <br> I: ".$lbb[0]->innn : 0 );

								foreach ($dtr->result() as $row){
								$arr['schedOut'] = array(
								"dtr_id" => $row->dtr_id,
								"emp_id" => $row->emp_id,
								"entry" => $row->entry,
								"log" => $row->log,
								"type" => $row->type,
								"sched_date" => (!empty($row->sched_date)) ? $row->sched_date : date("Y-m-d"),
								"sched_id" => (!empty($row->sched_id)) ? $row->sched_id : 0,
								"acc_time_id" => $row->acc_time_id,
								"shift" => $row->shift,
								"diff" => $row->diff,
								);
							}  
							}else{
							$dtrOut =  $this->db->query("select a.emp_id,sched_date,a.sched_id,a.acc_time_id,(select concat(time_start,' - ',time_end) from tbl_acc_time x,tbl_time y where x.time_id=y.time_id and x.acc_time_id = a.acc_time_id) as shift from tbl_schedule a where a.isActive=1 and a.emp_id=".$this->session->userdata('emp_id')." and curdate()=sched_date");
							$arr['fbs'] = 0;
							$arr['lunchs'] = 0;
							$arr['lbs'] = 0;
								 foreach ($dtrOut->result() as $row){
								$arr['schedIn'] = array(
								"shift" => $row->shift,
								"acc_time_id" => $row->acc_time_id,
								"sched_id" => $row->sched_id,
								);
							}  
							}
					}else{
					$arr['dtr_log'] = array("entry" => 'O');
					
						 $dtrOut =  $this->db->query("select a.emp_id,sched_date,a.sched_id,a.acc_time_id,(select concat(time_start,' - ',time_end) from tbl_acc_time x,tbl_time y where x.time_id=y.time_id and x.acc_time_id = a.acc_time_id) as shift from tbl_schedule a where a.emp_id=".$this->session->userdata('emp_id')." and curdate()=sched_date");
						if(count($dtrOut->result())>0){
						 $dtrChk =  $this->db->query("select * from tbl_dtr_logs where sched_id= ".$dtrOut->result()[0]->sched_id);
						if($dtrChk->num_rows()>0){
							foreach ($dtr->result() as $row){
								$arr['schedOut'] = array(
								"dtr_id" => $row->dtr_id,
								"emp_id" => $row->emp_id,
								"entry" => $row->entry,
								"log" => $row->log,
								"type" => $row->type,
								"sched_date" => $row->sched_date,
								"sched_id" => $row->sched_id,
								"acc_time_id" => $row->acc_time_id,
								"shift" => $row->shift,
							);
							}
						}else{
 							foreach ($dtrOut->result() as $row){
								$arr['schedIn'] = array(
								"shift" => $row->shift,
								"acc_time_id" => $row->acc_time_id,
								"sched_id" => $row->sched_id,
								);
							}
						}
						}else{
							$arr['schedIn'] =  array(
								"shift" => 0,
 								);
						}
						}
					 

 
					  // print_r($arr); 
					  if(!empty($lastDTRlog)){
							if($lastDTRlog[0]->type=="Break" && $lastDTRlog[0]->entry=="O"){
								redirect("index.php/dtr/break_log");
							}else{
								 $this->load->view('dtr',$arr);
   							}
						}else{

							$this->load->view('dtr',$arr);
						}
					}
					   
			}else{
					redirect('index.php/login');
			}
 		}
		 public function forbidden(){
			$this->load->view('forbidden');
		}
		public function running_time_break(){
			if($this->session->userdata('uid')){
			$qry = $this->db->query("SELECT TIMEDIFF(now(),log) as diff FROM `tbl_dtr_logs` WHERE emp_id=".$this->session->userdata('emp_id')."  and type='Break' order by dtr_id desc limit 1");
				$res = explode(":",$qry->result()[0]->diff);
				echo '<span class="time"><ul> <li class="timeDetails">'.$res[0].'</li><li class="note"> hour </li></ul> </span>:<span class="time"><ul> <li class="timeDetails">'.$res[1].'</li><li class="note"> min </li></ul></span>:<span class="time"><ul> <li class="timeDetails">'.$res[2].'</li><li class="note"> sec </li></ul></span>';
			}else{
					echo "<span style='color:red'>Session Timeout <br><small style='font-size: 18px;color:red'>please refresh your page</small></span>";


			}
		}
		public function break_log(){
		if($this->session->userdata('uid')){
		$dtr =  $this->db->query("select dtr_id,a.emp_id,entry,TIMESTAMPDIFF(HOUR,log,now()) as diff,log,type,a.acc_time_id from tbl_dtr_logs a  where   a.emp_id=".$this->session->userdata('emp_id')." and type='Break' order by dtr_id DESC limit 1");
		
		foreach ($dtr->result() as $row){
		
			$arr['schedOut'] = array(
			"dtr_id" => $row->dtr_id,
			"emp_id" => $row->emp_id,
			"entry" => $row->entry,
			"log" => $row->log,
			"type" => $row->type,
 			"acc_time_id" => $row->acc_time_id,
 			"diff" => $row->diff,
			);
		} 
			if($arr['schedOut']['entry']=='O'){
			$this->load->view('break',$arr);
			}else{
				redirect('index.php/dtr/');
			}
  		}else{
			redirect('index.php/dtr/');
		}
  		}
		public function break_back(){
 			$pass =  $this->input->post("pass");
 			$acc_time_id =  $this->input->post("acc_time_id");
			$dtr_id =  $this->input->post("dtr_id");
			$qry = $this->db->query("SELECT * from tbl_user where password='".$pass."'and uid=".$this->session->userdata('uid'));
			$uid2 =  $this->db->query("select fname,lname,uid,b.description from tbl_user a,tbl_user_role b where a.role_id = b.role_id and uid=".$this->session->userdata('uid')."");

				if($qry->num_rows()>0){
						$uid = $uid2->result();
						$qry222 =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$uid[0]->uid.",now(),'I - Back from break','".$uid[0]->fname." ".$uid[0]->lname."')");
						if($this->db->affected_rows()>0){
							echo 1;
							$qryBack = $this->db->query("insert into tbl_dtr_logs(emp_id,acc_time_id,entry,log,type,note) values(".$this->session->userdata('emp_id').",".$acc_time_id.",'I',now(),'Break',".$dtr_id.")");

						}else{
							echo 0;
						}
				}else{
					echo 0;
				}
   		}
		public function breaks(){
			$acc_time_id =  $this->input->post("acc_time_id");
  			$qry = $this->db->query("insert into tbl_dtr_logs(emp_id,acc_time_id,entry,log,type) values(".$this->session->userdata('emp_id').",".$acc_time_id.",'O',now(),'Break')");
			if($this->db->affected_rows()>0){
			$uid2 =  $this->db->query("select fname,lname,uid,b.description from tbl_user a,tbl_user_role b where a.role_id = b.role_id and uid=".$this->session->userdata('uid')."");
			$uid = $uid2->result();
			$qry222 =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$uid[0]->uid.",now(),'O - Out for break','".$uid[0]->fname." ".$uid[0]->lname."')");
			echo 1;
			}else{
				echo 0;
			}
		
		}
		public function getTime(){
				date_default_timezone_set("Asia/Manila");
				echo  "<h2>".date("h:i:s a")."</h2>";
				echo  "<small>".date("l, F j Y")."</small>";
  		}
		public function getTime2(){
			$dtr =  $this->db->query("select dtr_id,a.emp_id,entry,TIMEDIFF(now(),log) as diff,log,type, (select concat(time_start,' - ',time_end) from tbl_acc_time x,tbl_time y where x.time_id=y.time_id and x.acc_time_id = a.acc_time_id) as shift from tbl_dtr_logs a where a.emp_id=".$this->session->userdata('emp_id')." and type='DTR' order by dtr_id DESC limit 1");
		 
					$res = explode(":",$dtr->result()[0]->diff);
	echo '<span class="time"><ul> <li class="timeDetails">'.$res[0].'</li><li class="note"> hour </li></ul> </span>:<span class="time"><ul> <li class="timeDetails">'.$res[1].'</li><li class="note"> min </li></ul></span>:<span class="time"><ul> <li class="timeDetails">'.$res[2].'</li><li class="note"> sec </li></ul></span>';
 		}
		public function login_wo_sched(){
			$acc_time_id =  trim($this->input->post("acc_time_id"));
			if(!empty($acc_time_id)){
			$now = date("Y-m-d");
			$uid2 =  $this->db->query("select fname,lname,uid,b.description from tbl_user a,tbl_user_role b where a.role_id = b.role_id and uid=".$this->session->userdata('uid')."");
			$insertLogin = $this->db->query("insert into tbl_dtr_logs(emp_id,acc_time_id,entry,log,type) values(".$this->session->userdata('emp_id').",".$acc_time_id.",'I',now(),'DTR')");
			$uid = $uid2->result();

				if($this->db->affected_rows()>0){
					echo 1;
					$qry222 =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$uid[0]->uid.",now(),'I - DTR (w/o sched)','".$uid[0]->fname." ".$uid[0]->lname."')");

				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
		} 
		public function login(){
				$acc_time_id =  $this->input->post("acc_time_id");
				$sched_id = $this->input->post("sched_id");
			$dtr =  $this->db->query("select dtr_id,a.emp_id,entry,log,type,sched_date,b.sched_id,b.acc_time_id,(select concat(time_start,' - ',time_end) from tbl_acc_time x,tbl_time y where x.time_id=y.time_id and x.acc_time_id = b.acc_time_id) as shift from tbl_dtr_logs a, tbl_schedule b where a.sched_id = b.sched_id and a.emp_id=".$this->session->userdata('emp_id')." and type='DTR' order by dtr_id DESC limit 1");
			$uid2 =  $this->db->query("select fname,lname,uid,b.description from tbl_user a,tbl_user_role b,tbl_employee c,tbl_applicant d where c.emp_id=a.emp_id and d.apid=c.apid and a.role_id = b.role_id and uid=".$this->session->userdata('uid')."");
			if(count($dtr->result())>0){ 
						$dtrIN =  $this->db->query("SELECT * FROM `tbl_schedule` a, tbl_acc_time b where a.acc_time_id = b.acc_time_id and emp_id = ".$this->session->userdata('emp_id')." and sched_id>".$dtr->result()[0]->sched_id." limit 1");
						$insertLogin = $this->db->query("insert into tbl_dtr_logs(emp_id,acc_time_id,entry,log,type,sched_id) values(".$this->session->userdata('emp_id').",".$acc_time_id.",'I',now(),'DTR',".$sched_id.")");
						$uid = $uid2->result();

				if($this->db->affected_rows()>0){
						echo 0;
						$qry222 =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$uid[0]->uid.",now(),'I - DTR','".$uid[0]->fname." ".$uid[0]->lname."')");

					}else{
						echo 1;
					}
			}else{
				$dtrIN =  $this->db->query("SELECT * FROM `tbl_schedule` a, tbl_acc_time b where a.acc_time_id = b.acc_time_id and emp_id = ".$this->session->userdata('emp_id')." and curdate()=sched_date limit 1");
 				$insertLogin = $this->db->query("insert into tbl_dtr_logs(emp_id,acc_time_id,entry,log,type,sched_id) values(".$this->session->userdata('emp_id').",".$acc_time_id.",'I',now(),'DTR',".$dtrIN->result()[0]->sched_id.")");
				$uid = $uid2->result();

				if($this->db->affected_rows()>0){
						echo 0;
						$qry222 =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$uid[0]->uid.",now(),'I - DTR','".$uid[0]->fname." ".$uid[0]->lname."')");


					}else{
						echo 1;
					}
			}
 		}
		public function logout(){
				$acc_time_id =  $this->input->post("acc_time_id");
				if(!empty($acc_time_id)){
				$sched_id = $this->input->post("sched_id");
				$dtr_id = $this->input->post("dtr_id");
				$qry = $this->db->query("insert into tbl_dtr_logs(emp_id,acc_time_id,entry,log,type,sched_id,note) values(".$this->session->userdata('emp_id').",".$acc_time_id.",'O',now(),'DTR',".$sched_id.",".$dtr_id.")");

					if($this->db->affected_rows()>0){
			$uid2 =  $this->db->query("select fname,lname,uid,b.description from tbl_user a,tbl_user_role b,tbl_employee c,tbl_applicant d where c.emp_id=a.emp_id and d.apid=c.apid and a.role_id = b.role_id and uid=".$this->session->userdata('uid')."");
							$uid = $uid2->result();
							$qry222 =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$uid[0]->uid.",now(),'O - DTR','".$uid[0]->fname." ".$uid[0]->lname."')");
						echo 1;
					}else{
						echo 0;
					}
				}else{
				
				echo 0;
				}

 		}
		public function viewLogsDetailsofNoSched(){
			$datee = explode("-",$this->input->post("datee"));
 $qry = $this->db->query("select * from tbl_dtr_logs where emp_id=".$this->session->userdata('emp_id')." and entry ='I' and type='DTR' and sched_id is null and log >= '".date_format(date_create($datee[0]),"Y-m-d")." 00:00:00' and log<='".date_format(date_create($datee[1]),"Y-m-d")." 23:59:59' order by dtr_id");
 
 			// $qry = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b  where a.schedtype_id =b.schedtype_id and emp_id = ".$this->session->userdata('emp_id')."  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' order by a.sched_date ASC");
			$arr = array();
 			foreach($qry->result() as $row){
				$login = $this->getLoginNoSched($row->dtr_id);
				$login1 = (count($login)>0) ? $login[0]->log : 0;
				$login2 = (count($login)>0) ? $login[0]->dtr_id : 0;
				$loginentry1 = (count($login)>0) ? $login[0]->entry : 0;
				$logout = $this->getLogoutNoSched($row->dtr_id);
				$logout1 = (count($logout)>0) ? $logout[0]->log : 0;
				$logout2 = (count($logout)>0) ? $logout[0]->dtr_id : 0;
				$logout3 = (count($logout)>0) ? $logout[0]->dtr_id : ($login2*1000);
				$logoutentry1 = (count($logout)>0) ? $logout[0]->entry : 0;
				
					$llbreak = $this->getLunchBreak1($row->emp_id,$login2,$logout3);
				$llbreak1 = (count($llbreak)) ? $this->getLunchBreak1($row->emp_id,$login2,$logout3) : 0;
				$llbreak2 = (count($this->getLunchBreak2($row->emp_id,$login2,$logout3))) ? $this->getLunchBreak2($row->emp_id,$login2,$logout3) : 0;
 					// $llbreak1 = (count($llbreak)>0) ? explode(",",$llbreak[0]->break) : 0;
				$ffbreak = $this->getFirstBreak($row->emp_id,$login2,$logout3);
					$ffbreak1 = (count($ffbreak)>0) ? explode(",",$ffbreak[0]->break) : 0;
				$llastbreak = $this->getLastBreak($row->emp_id,$login2,$logout3);
					$llastbreak1 = (count($llastbreak)>0) ? explode(",",$llastbreak[0]->break) : 0;
 					$arr['sched_date'][$row->dtr_id] = array(
 					"login" => ($login1 != 0) ? date_format(date_create($login1),"M d, Y h:i:s A ") : $login1,
					"login_raw" => $login1,
					"login_id" => $login2,
					"logout_id" => $logout2,
					"login_entry" => $loginentry1,
					"logout" => ($logout1 != 0) ? date_format(date_create($logout1),"M d, Y h:i:s A") : $logout1,
					"logout_raw" => $logout1,
					"logout_entry" => $logoutentry1,
					"type" => $row->type,
 					"emp_id" => $row->emp_id,
					"sched_id" => $row->sched_id,
					"lunchb" =>  (count($llbreak)>0) ? "Out: ".$llbreak1[0]->break."<br> In: ".$llbreak2[0]->break : "<i style='color:#ff93a6'>No Logs</i>",
					"firstb" => (count($ffbreak1)>1) ? "Out: ".$ffbreak1[0]."<br> In: ".$ffbreak1[1] :  "--",
					"lastb" => (count($llastbreak1)>1) ? "Out: ".$llastbreak1[0]."<br> In: ".$llastbreak1[1] :  "--",
 				);
				
			}
 			  echo json_encode($arr);
			  // echo "SELECT * FROM `tbl_schedule` a,tbl_schedule_type b  where a.schedtype_id =b.schedtype_id and emp_id = ".$this->session->userdata('emp_id')."  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' order by sched_date ASC";
 		}
		public function getLoginNoSched($schedID){
			$qry = $this->db->query("SELECT dtr_id,log,entry FROM `tbl_dtr_logs` where type='DTR' and   entry='I' and dtr_id = ".$schedID."");
			return $qry->result();
		}
		public function getLogoutNoSched($schedID){
			$qry = $this->db->query("SELECT dtr_id,log,entry FROM `tbl_dtr_logs` where type='DTR' and   entry='O' and note = ".$schedID."");
			return $qry->result();
		}
		public function viewLogsDetails(){
			$datee = explode("-",$this->input->post("datee"));
 			  // $qry = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b,tbl_acc_time c,tbl_time d  where a.acc_time_id = c.acc_time_id and c.time_id=d.time_id and a.schedtype_id =b.schedtype_id and emp_id = ".$this->session->userdata('emp_id')."  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' order by a.sched_date ASC");
 			  $qry = $this->db->query("SELECT a.sched_id,a.emp_id,a.sched_date,b.type,b.style,(select concat(time_start,' to ',time_end)  from tbl_acc_time as x,tbl_time as y where x.time_id=y.time_id and x.acc_time_id=a.acc_time_id) as times FROM tbl_schedule a,tbl_schedule_type b where a.schedtype_id =b.schedtype_id and a.isActive=1 and emp_id = ".$this->session->userdata('emp_id')."  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' order by a.sched_date ASC");
 			  //$qry = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b  where a.schedtype_id =b.schedtype_id and emp_id = ".$this->session->userdata('emp_id')."  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' order by a.sched_date ASC");
			$arr = array();
			foreach($qry->result() as $row){
 				$login = $this->getLogin($row->sched_id);
				$login1 = (count($login)>0) ? $login[0]->log : 0;
				$login2 = (count($login)>0) ? $login[0]->dtr_id : 0;
				$loginentry1 = (count($login)>0) ? $login[0]->entry : 0;
				$logout = $this->getLogout($row->sched_id);
				$logout1 = (count($logout)>0) ? $logout[0]->log : 0;
				$logout2 = (count($logout)>0) ? $logout[0]->dtr_id : 0;
				$logout3 = (count($logout)>0) ? $logout[0]->dtr_id : ($login2*1000);
				$logoutentry1 = (count($logout)>0) ? $logout[0]->entry : 0;
			
					$llbreak = $this->getLunchBreak1($row->emp_id,$login2,$logout3);
				$llbreak1 = (count($llbreak)) ? $this->getLunchBreak1($row->emp_id,$login2,$logout3) : 0;
				$llbreak2 = (count($this->getLunchBreak2($row->emp_id,$login2,$logout3))) ? $this->getLunchBreak2($row->emp_id,$login2,$logout3) : 0;
 					// $llbreak1 = (count($llbreak)>0) ? explode(",",$llbreak[0]->break) : 0;
				$ffbreak = $this->getFirstBreak($row->emp_id,$login2,$logout3);
					$ffbreak1 = (count($ffbreak)>0) ? explode(",",$ffbreak[0]->break) : 0;
				$llastbreak = $this->getLastBreak($row->emp_id,$login2,$logout3);
					$llastbreak1 = (count($llastbreak)>0) ? explode(",",$llastbreak[0]->break) : 0;
 				$arr['sched_date'][$row->sched_id."-".$row->sched_date] = array(
					"sched" => date_format(date_create($row->sched_date), "M d, Y <br>l "),
					"login" => ($login1 != 0) ? date_format(date_create($login1),"h:i:s A") : $login1,
					"login_raw" => $login1,
					"sched_date" => $row->sched_date,
					"login_id" => $login2,
					"logout_id" => $logout2,
					"login_entry" => $loginentry1,
					"logout" => ($logout1 != 0) ? date_format(date_create($logout1),"h:i:s A") : $logout1,
					"logout_raw" => $logout1,
					"logout_entry" => $logoutentry1,
					"type" => $row->type,
					"style" => $row->style,
					"emp_id" => $row->emp_id,
					"emp_id2" => count($llbreak),
					"shift" =>  ($row->times) ? $row->times : $row->type,
					"sched_id" => $row->sched_id,
					"lunchb" =>  (count($llbreak)>0) ? "Out: ".$llbreak1[0]->break."<br> In: ".$llbreak2[0]->break : "<i style='color:#ff93a6'>No Logs</i>",
					"firstb" => (count($ffbreak1)>1) ? "Out: ".$ffbreak1[0]."<br> In: ".$ffbreak1[1] :  "<i style='color:#ff93a6'>No Logs</i>",
					"lastb" => (count($llastbreak1)>1) ? "Out: ".$llastbreak1[0]."<br> In: ".$llastbreak1[1] :  "<i style='color:#ff93a6'>No Logs</i>",
 				);
				
			}
 			  echo json_encode($arr);
			  // echo "SELECT * FROM `tbl_schedule` a,tbl_schedule_type b  where a.schedtype_id =b.schedtype_id and emp_id = ".$this->session->userdata('emp_id')."  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' order by sched_date ASC";
 		}
		public function viewLogs(){
			$datee = explode("-",$this->input->post("datee"));
 			$qry = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b  where a.schedtype_id =b.schedtype_id  and a.isActive=1 and emp_id = ".$this->session->userdata('emp_id')."  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' order by a.sched_date ASC");
			$arr = array();
			foreach($qry->result() as $row){
				$login = $this->getLogin($row->sched_id);
				$login1 = (count($login)>0) ? $login[0]->log : 0;
				$loginentry1 = (count($login)>0) ? $login[0]->entry : 0;
				
				$logout = $this->getLogout($row->sched_id);
				$logout1 = (count($logout)>0) ? $logout[0]->log : 0;
				$logoutentry1 = (count($logout)>0) ? $logout[0]->entry : 0;
				$arr['sched_date'][$row->sched_date] = array(
					"sched" => date_format(date_create($row->sched_date), "M d, Y <br>l "),
					"login" => ($login1 != 0) ? date_format(date_create($login1),"h:i:s A") : $login1,
					"login_raw" => $login1,
					"login_entry" => $loginentry1,
					"logout" => ($logout1 != 0) ? date_format(date_create($logout1),"h:i:s A") : $logout1,
					"logout_raw" => $logout1,
					"logout_entry" => $logoutentry1,
					"type" => $row->type,
					"style" => $row->style,
					"sched_id" => $row->sched_id,
				
 				);
				
			}
 			  echo json_encode($arr);
			  // echo "SELECT * FROM `tbl_schedule` a,tbl_schedule_type b  where a.schedtype_id =b.schedtype_id and emp_id = ".$this->session->userdata('emp_id')."  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' order by sched_date ASC";
 		}
		public function getLunchBreak1($emp_id,$login,$logout){
			// $qry = $this->db->query("select log from tbl_dtr_logs where emp_id=".$emp_id." and dtr_id> ".$login." and dtr_id<".$logout."");
			$qry = $this->db->query("select log as break,entry from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='Lunch' and dtr_id>  ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." and entry='O' order by log asc limit 1");
			// $qry = $this->db->query("select group_concat(log) as break from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='Lunch' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id."");
			return $qry->result();
		}public function getLunchBreak2($emp_id,$login,$logout){
			// $qry = $this->db->query("select log from tbl_dtr_logs where emp_id=".$emp_id." and dtr_id> ".$login." and dtr_id<".$logout."");
			$qry = $this->db->query("select log as break,entry from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='Lunch' and dtr_id>  ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." and entry='I' order by log asc limit 1");
			return $qry->result();
		}
		public function getFirstBreak($emp_id,$login,$logout){
			// $qry = $this->db->query("select log from tbl_dtr_logs where emp_id=".$emp_id." and dtr_id> ".$login." and dtr_id<".$logout."");
			// $qry = $this->db->query("select group_concat(log) as break from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='First Break' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id."");
			$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='First Break' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." limit 1 ");
			return $qry->result();
		}
		public function getLastBreak($emp_id,$login,$logout){
			// $qry = $this->db->query("select log from tbl_dtr_logs where emp_id=".$emp_id." and dtr_id> ".$login." and dtr_id<".$logout."");
			// $qry = $this->db->query("select  group_concat(log )  as break from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='Last Break' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." limit 1 ");
			$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='Last Break' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." limit 1 ");
			return $qry->result();
		}
		public function getLogin($schedID){
			$qry = $this->db->query("SELECT dtr_id,log,entry FROM `tbl_dtr_logs` where type='DTR' and   entry='I' and sched_id = ".$schedID."");
			return $qry->result();
		}
		public function getLogout($schedID){
			$qry = $this->db->query("SELECT dtr_id,log,entry FROM `tbl_dtr_logs` where type='DTR' and   entry='O' and sched_id = ".$schedID."");
			return $qry->result();
		}
		public function lastDTRlog($id){
				$qry = $this->db->query("select type,entry from tbl_dtr_logs where emp_id=".$id." order by dtr_id DESC limit 1");
				return $qry->result();

		}
		 
		public function breakAvailable(){
			$qry = $this->db->query("select group_concat(DISTINCT(concat(acc_time_id,'|',break_type))) as brk from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and a.acc_id= (select acc_id from tbl_employee where emp_id=".$this->session->userdata('emp_id').")and note='break'");
			 
			return $qry->result();
		}
		public function lastBreak($id,$emp){
			$qry = $this->db->query("select abs(time_to_sec(timediff(log,(select log from tbl_dtr_logs where note=g.dtr_id limit 1)))/3600) as diff,(select log from tbl_dtr_logs where note=g.dtr_id limit 1) as innn,log as outt from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and dtr_id>=$id and break_type='Last Break' and entry='O' and emp_id=$emp");
			// $qry = $this->db->query("select TIMESTAMPDIFF(minute,log,(select log from tbl_dtr_logs where note=g.dtr_id)) as diff from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and sched_id=$id and break_type='Last Break' and entry='O'");
			return $qry->result();
		}
		public function firstBreak($id,$emp){
			// $qry = $this->db->query("select abs(time_to_sec(timediff(log,(select log from tbl_dtr_logs where note=g.dtr_id limit 1)))/3600) as diff ,(select log from tbl_dtr_logs where note=g.dtr_id limit 1) as innn,log as outt from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and dtr_id>=$id  and break_type='First Break' and entry='O' and emp_id=$emp");
			$qry = $this->db->query("select abs(time_to_sec(timediff(log,(select log from tbl_dtr_logs where dtr_id<g.dtr_id and emp_id=$emp   and entry='O' order by dtr_id desc limit 1)))/3600) as diff,(select log from tbl_dtr_logs where dtr_id<g.dtr_id and emp_id=$emp   and entry='O' order by dtr_id desc limit 1) as outt,log as innn from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and dtr_id>=$id and break_type='First Break' and entry='I' and emp_id=$emp  limit 1");
			return $qry->result();
		}
		public function lunchBreak($id,$emp){
			 $qry = $this->db->query("select abs(time_to_sec(timediff(log,(select log from tbl_dtr_logs where note=g.dtr_id limit 1)))/3600) as diff,(select log from tbl_dtr_logs where note=g.dtr_id limit 1) as innn,log as outt from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and dtr_id>=$id and break_type='Lunch' and entry='O' and emp_id=$emp limit 1");
			return $qry->result();
		}
		public function TAP(){
			$qry = $this->db->query("select distinct(lname) as lname from tbl_applicant where dateCreated >= '2017-04-19' order by dateCreated;");
			 echo "<table>";
			foreach($qry->result() as $row){
				$getRecords = $this->getRecordsALLTAP($row->lname);	
				echo "<tr><td>".$getRecords[0]->apid."</td><td>".$getRecords[0]->lname."</td><td>".$getRecords[0]->fname."</td><td>".$getRecords[0]->source."</td><td>".$getRecords[0]->gender."</td><td>".$getRecords[0]->cell."</td><td>".$getRecords[0]->birthday."</td><td>".$getRecords[0]->permanentAddrezs."</td><td>".$getRecords[0]->presentAddrezs."</td><td>".$getRecords[0]->relijon."</td><td>".$getRecords[0]->source."</td><td>".$getRecords[0]->dateCreated."</td><td>".$getRecords[0]->pos_details."</td></tr>";
				 
			}
			 echo "</table>";
		}
		public function getRecordsALLTAP($lname){
			$qry = $this->db->query("select apid,lname,fname,gender,cell,birthday,permanentAddrezs,presentAddrezs,relijon,source,dateCreated,pos_details from tbl_applicant a,tbl_position b where a.pos_id=b.pos_id and lname='".$lname."'");
			return $qry->result();
		}
		
}