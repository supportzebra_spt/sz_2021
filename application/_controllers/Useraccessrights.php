<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Useraccessrights extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	public function index(){
			if($this->session->userdata('uid')){
				$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Applicant' and is_assign=1");
				foreach ($qry->result() as $row)
				{
						$arr['setting'] = array("settings" => $row->settings);
				}
			if ($qry->num_rows() > 0){
	 					$this->load->view('userrights',$arr);
			}else{
				redirect('index.php/home/error');
			}

			}else{
					redirect('index.php/login');
			}
		}
 
 	public function userlistaccess(){
		$users= [];
		$qry =  $this->db->query("select * from tbl_user");
		if ($qry->num_rows() > 0){
			foreach ($qry->result() as $row){
			$users[] = $row->uid."|".$row->fname."|".$row->lname;
 			}
		}
		echo json_encode($users);
		return $users;
		
	}
	public function usertabs(){
		$uid = $this->input->post('userliz'); 
		$users= [];
		// $qry =  $this->db->query("select distinct(c.tab_id),tab_name from tbl_user a,tbl_user_access b,tbl_menu_items c,tbl_menu_tab_item d where a.uid = b.user_id and b.menu_item_id=c.menu_item_id and  c.tab_id=d.tab_id and  uid=".$uid.";");
		$qry =  $this->db->query(" select b.menu_item_id,tab_name,item_title,(select is_assign from tbl_user_access c where c.menu_item_id = b.menu_item_id and c.user_id=".$uid.") as is_assign from tbl_menu_tab_item a,tbl_menu_items b where a.tab_id=b.tab_id order by a.tab_name ASC");
		if ($qry->num_rows() > 0){
			foreach($qry->result() as $row){
				// $menuID= $this->usertabsMenu2($row->tab_id);
				 
				 
					  $users[] = [
                     'menu_item_id' => $row->menu_item_id,
                    'item_title' => $row->item_title,
                    'tab_name' => $row->tab_name,
                    'is_selected' =>$row->is_assign
                    ];
					  
				 
 			}
		}
		   echo json_encode($users);
		 // print_r($users);
		return $users;
		
	}
 
	public function usertabsNo(){ //This is for the Modal Menu
		$uid = $this->input->post('userliz'); 
		
		$menu_item_idz = $this->input->post('menu_item_idz'); 
		$arr = explode(",",$menu_item_idz);
		$qry22 =  $this->db->query("update tbl_user_access set is_assign = 0 where user_id=".$uid."");
		foreach($arr as $k => $val){
			$qry =  $this->db->query("select * from tbl_user_access where menu_item_id=".$val." and user_id=".$uid."");
 				if ($qry->num_rows() > 0){
					$qry2 =  $this->db->query("update tbl_user_access set is_assign = 1 where user_id=".$uid." and menu_item_id=".$val."");

				}else{
					$qry2 =  $this->db->query("insert into tbl_user_access(user_id, menu_item_id, is_assign) values(".$uid.",".$val.",1)");

				}

		}
   
   
 	}
	public function usertabsAdd(){ 
		$uid = $this->input->post('userliz'); 
		$users= [];
		$qry =  $this->db->query("select tab_id,tab_name from tbl_menu_tab_item where tab_id not in (select distinct(c.tab_id) from tbl_user a,tbl_user_access b,tbl_menu_items c,tbl_menu_tab_item d where a.uid = b.user_id and b.menu_item_id=c.menu_item_id and  c.tab_id=d.tab_id and uid=".$uid.");");
		if ($qry->num_rows() > 0){
			foreach ($qry->result() as $row){
			$users[] = $row->tab_id."|".$row->tab_name;
 			}
		}
		echo json_encode($users);
		return $users;
		
	}
 

}