<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermenu extends CI_Controller {

  public function __construct(){
        parent::__construct();
		$this->load->model("MenuModel");
			}
	public function index(){
	if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Applicant' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$this->data['setting'] = array("settings" => $row->settings);
			}
		if ($qry->num_rows() > 0){
				$this->data['menu'] = $this->MenuModel->fetchMenu();
			$this->load->view('usermenus',$this->data);		
		}else{
			redirect('index.php/home/error');
		}
		
			
		}else{
			redirect('index.php/login');
		}
	}
	
	public function getTabAssign(){
			$ok = $this->MenuModel->fetchMenuTabs();
			echo json_encode($ok);
	}
 
	public function addMenuTabz(){
	 			$desc= $this->input->post('desc');
	 			$link= $this->input->post('link');
	 			$tab= $this->input->post('tab');
				$this->MenuModel->addMenu($desc,$link,$tab);	


	}
	public function UpdateMenuTabz(){
 
	 			$desc= $this->input->post('desc');
	 			$link= $this->input->post('link');
	 			$tab= $this->input->post('tab');
	 			$isactib= $this->input->post('isactib');
	 			$menuID= $this->input->post('menuID');
				if($isactib!="1"){
					$isactib = "0";
				}else{
				$isactib = "1";	
				}
				$this->MenuModel->updateMenu($desc,$link,$tab,$isactib,$menuID);	


	}
}