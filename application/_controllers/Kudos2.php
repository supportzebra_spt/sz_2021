<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kudos extends CI_Controller {

	/* public $file = 'C:\inetpub\wwwroot\nicca\sz3\reports\uploaded.xlsx';  for Windows
	public $logo= 'C:\inetpub\wwwroot\nicca\sz3\assets\images\logo2.png';
	public $savefile = "C:\\inetpub\\wwwroot\\nicca\\sz3\\reports\\"; */
	
	public $file = '/usr/local/www/apache24/data/supportzebra/reports/uploaded.xlsx';   //for linux
	public $logo= '/usr/local/www/apache24/data/supportzebra/assets/images/logo2.png';
	public $savefile = "//usr//local//www//apache24//data//supportzebra//reports//"; 
	
/* 	public $file= "C:\\xampp\\htdocs\\supportzebra\\reports\\uploaded.xlsx";
	public $logo= "C:\\xampp\\htdocs\\supportzebra\\assets\\images\\logo2.png";
	public $savefile= "C:\\xampp\\htdocs\\supportzebra\\reports\\";
 */	public function __construct()
	{
		parent::__construct();
		$this->load->model("KudosModel");
	}

	public function index()
	{
		if($this->session->userdata('uid'))
		{
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Kudos/' and is_assign=1");
			if ($qry->num_rows() > 0){
					//$this->load->view('employees',$arr);
				$role = explode('|',$this->session->userdata('role'));
				if($role[1]=='HRT' || $role[1]=='AET'){
					$data['kudos'] = $this->KudosModel->get_kudos();
					$data['acc'] = $this->KudosModel->get_accounts();
					$data['name'] = $this->KudosModel->get_names();
					$data['name2'] = $this->KudosModel->another_getName();
					$data['k_count'] = $this->KudosModel->get_kudos_count();
					$data['top'] = $this->KudosModel->get_kudos_top();
					$data['all'] = $this->KudosModel->get_kudos_all_agents();
					$data['top_campaigns'] = $this->KudosModel->get_kudos_top_campaigns();
					$data['drilldown'] = $this->KudosModel->get_kudos_drilldown();
					$data['campaign_kcount'] = $this->KudosModel->get_kudos_by_campaigns();
					$this->load->view('kudoslist',$data);
				}
				if($role[1]=='OPT' || $role[1]=='SET'){
					$data['kudos'] = $this->KudosModel->get_kudos();
					$data['acc'] = $this->KudosModel->get_accounts();
					$data['name'] = $this->KudosModel->get_names();
					$data['name2'] = $this->KudosModel->another_getName();
					$this->load->view('kudos_opt',$data);
				}
			}else{
				redirect('index.php/home/error');
			}
		}
		else
		{
			redirect('index.php/login');
		}
	}

	public function hr_dashboard()
	{
		if($this->session->userdata('uid'))
		{
			$data['kudos'] = $this->KudosModel->get_kudos();
			$data['acc'] = $this->KudosModel->get_accounts();
			$data['name'] = $this->KudosModel->get_names();
			$data['name2'] = $this->KudosModel->another_getName();
			$data['k_count'] = $this->KudosModel->get_kudos_count();
			$data['top'] = $this->KudosModel->get_kudos_top();
			$data['all'] = $this->KudosModel->get_kudos_all_agents();
			$data['top_campaigns'] = $this->KudosModel->get_kudos_top_campaigns();
			$data['drilldown'] = $this->KudosModel->get_kudos_drilldown();
			$data['drilldown2'] = $this->KudosModel->get_kudos_drilldown2();
			$data['drilldown3'] = $this->KudosModel->get_kudos_drilldown3();
			$data['campaign_kcount'] = $this->KudosModel->get_kudos_by_campaigns();
			$this->load->view('kudos_hr_dashboard',$data);
		}
	}

	public function opt_dashboard()
	{
		if($this->session->userdata('uid'))
		{
			$data['kudos'] = $this->KudosModel->get_kudos();
			$data['acc'] = $this->KudosModel->get_accounts();
			$data['name'] = $this->KudosModel->get_names();
			$data['name2'] = $this->KudosModel->another_getName();
			$data['k_count'] = $this->KudosModel->get_kudos_count();
			$data['top'] = $this->KudosModel->get_kudos_top();
			$data['all'] = $this->KudosModel->get_kudos_all_agents();
			$data['top_campaigns'] = $this->KudosModel->get_kudos_top_campaigns();
			$data['drilldown'] = $this->KudosModel->get_kudos_drilldown();
			$data['drilldown2'] = $this->KudosModel->get_kudos_drilldown2();
			$data['drilldown3'] = $this->KudosModel->get_kudos_drilldown3();
			$data['campaign_kcount'] = $this->KudosModel->get_kudos_by_campaigns();
			$this->load->view('kudos_opt_dashboard',$data);
		}
	}

	public function report1View()
	{
		if($this->session->userdata('uid'))
		{
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Kudos/report3View' and is_assign=1");
			if ($qry->num_rows() > 0){
				$data['kudos'] = $this->KudosModel->get_kudos();
				$data['acc'] = $this->KudosModel->get_accounts();
				$data['name'] = $this->KudosModel->get_names();
				$data['name2'] = $this->KudosModel->another_getName();
				$data['k_count'] = $this->KudosModel->get_kudos_count();
				$data['top'] = $this->KudosModel->get_kudos_top();
				$data['all'] = $this->KudosModel->get_kudos_all_agents();
				$data['top_campaigns'] = $this->KudosModel->get_kudos_top_campaigns();
				$data['drilldown'] = $this->KudosModel->get_kudos_drilldown();
				$data['drilldown2'] = $this->KudosModel->get_kudos_drilldown2();
				$data['campaign_kcount'] = $this->KudosModel->get_kudos_by_campaigns();
				$this->load->view('report1',$data);
			}else{
				redirect('index.php/home/error');

			}
		}
		else {
			redirect('index.php/login');
		}
	}

	public function addMultipleView()
	{
		$role = explode('|',$this->session->userdata('role'));
		if($role[1]=='OPT'){
			$data['kudos'] = $this->KudosModel->get_kudos();
			$data['acc'] = $this->KudosModel->get_accounts();
			$data['name'] = $this->KudosModel->get_names();
			$data['name2'] = $this->KudosModel->another_getName();
			$data['k_count'] = $this->KudosModel->get_kudos_count();
			$data['top'] = $this->KudosModel->get_kudos_top();
			$data['all'] = $this->KudosModel->get_kudos_all_agents();
			$data['top_campaigns'] = $this->KudosModel->get_kudos_top_campaigns();
			$data['drilldown'] = $this->KudosModel->get_kudos_drilldown();
			$data['campaign_kcount'] = $this->KudosModel->get_kudos_by_campaigns();
			$this->load->view('addMultiple',$data);
		} else {
			redirect('index.php/login');
		}
	}

	public function report2View()
	{
		if($this->session->userdata('uid'))
		{
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Kudos/report2View' and is_assign=1");
			if ($qry->num_rows() > 0){
				$data['kudos'] = $this->KudosModel->get_kudos();
				$data['acc'] = $this->KudosModel->get_accounts();
				$data['name'] = $this->KudosModel->get_names();
				$data['name2'] = $this->KudosModel->another_getName();
				$data['k_count'] = $this->KudosModel->get_kudos_count();
				$data['top'] = $this->KudosModel->get_kudos_top();
				$data['all'] = $this->KudosModel->get_kudos_all_agents();
				$data['top_campaigns'] = $this->KudosModel->get_kudos_top_campaigns();
				$data['drilldown'] = $this->KudosModel->get_kudos_drilldown();
				$data['campaign_kcount'] = $this->KudosModel->get_kudos_by_campaigns();
				$this->load->view('report2',$data);
			}else{
				redirect('index.php/home/error');

			}
		}
		else {
			redirect('index.php/login');
		}
		


	}

	public function report3View()
	{

		if($this->session->userdata('uid'))
		{	
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Kudos/report3View' and is_assign=1");
			if ($qry->num_rows() > 0){

				$data['kudos'] = $this->KudosModel->get_kudos();
				$data['acc'] = $this->KudosModel->get_accounts();
				$data['name'] = $this->KudosModel->get_names();
				$data['name2'] = $this->KudosModel->another_getName();
				$data['k_count'] = $this->KudosModel->get_kudos_count();
				$data['top'] = $this->KudosModel->get_kudos_top();
				$data['all'] = $this->KudosModel->get_kudos_all_agents();
				$data['top_campaigns'] = $this->KudosModel->get_kudos_top_campaigns();
				$data['drilldown'] = $this->KudosModel->get_kudos_drilldown();
				$data['drilldown3'] = $this->KudosModel->get_kudos_drilldown3();
				$data['campaign_kcount'] = $this->KudosModel->get_kudos_by_campaigns();
				$this->load->view('report3',$data);
			}else{
				redirect('index.php/home/error');

			}
		}
		else {
			redirect('index.php/login');
		}

	}

	public function proofreadView()
	{

		$data['kudos'] = $this->KudosModel->get_kudos_pf();
		$data['acc'] = $this->KudosModel->get_accounts();
		$data['name'] = $this->KudosModel->get_names();
		$data['name2'] = $this->KudosModel->another_getName();
		$this->load->view('kudos_proofread',$data);

	}

	public function proofreadingUpdate()
	{
		$pfrd= $this->input->post('pfrd');
		$comment= $this->input->post('comment');
		$revision= $this->input->post('revision');
		$r_by= $this->input->post('r_by');
		$k_id= $this->input->post('k_id');
		$row = $this->KudosModel->update_proofreading($pfrd,$comment,$revision,$r_by,$k_id);
		if($row>0){
			echo 'Success';
		}else{
			echo 'Failed';
		}
	}

	public function requestView()
	{
		$data['kudos'] = $this->KudosModel->get_requested();
		$data['acc'] = $this->KudosModel->get_accounts();
		$data['name'] = $this->KudosModel->get_names();
		$data['name2'] = $this->KudosModel->another_getName();
		$this->load->view('kudos_requestApprove',$data);
	}

	public function approveCashRequest()
	{
		$k_id= $this->input->post('k_id');
		$r_status= $this->input->post('r_status');
		$a_by =$this->input->post('a_by');
		$this->KudosModel->approveCash($k_id,$r_status,$a_by);

	}

	public function releaseCashRequest()
	{
		$k_id= $this->input->post('k_id');
		$r_status= $this->input->post('r_status');
		$this->KudosModel->releaseCash($k_id,$r_status);

	}

	public function releaseCashView()
	{
		$data['kudos'] = $this->KudosModel->get_approved();
		$data['acc'] = $this->KudosModel->get_accounts();
		$data['name'] = $this->KudosModel->get_names();
		$data['name2'] = $this->KudosModel->another_getName();
		$this->load->view('kudos_releaseCash',$data);
	}

	public function addKudos()
	{
		$path = $_FILES["addFile"]["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Manila');
		$file_sc = 'uploads/screenshots/'.date("Ymdhms.").$extension;
		

		$new_name = date("Ymdhms").".".$extension;
		$target_dir = "./uploads/screenshots/";
		$target_file = $target_dir.$new_name;

		$type = $_FILES["addFile"]["type"];
		$allowed = array('image/jpeg', 'image/png', 'image/gif');
		
		if($_FILES["addFile"]["name"]){
			if(!in_array($type,$allowed)){
				echo 'File type is not allowed.';
			} else {

				move_uploaded_file($_FILES["addFile"]["tmp_name"],$target_file);
				echo 'Successfully added!';
				$emp_id= $this->input->post('emp_id');
				$campaign= $this->input->post('campaign');
				$k_type= $this->input->post('k_type');
				$c_name= $this->input->post('c_name');
				$p_number= $this->input->post('p_number');
				$e_add= $this->input->post('e_add');
				$comment= $this->input->post('comment');
				$supervisor= $this->input->post('supervisor');
					//$file= $this->input->post('file');
				$p_reward= $this->input->post('p_reward');
				$pfrd= $this->input->post('pfrd');
				$k_card= $this->input->post('k_card');
				$r_status= $this->input->post('r_status');

				$this->KudosModel->add_kudos($emp_id,$campaign,$k_type,$c_name,$p_number,$e_add,$comment,$supervisor,$p_reward,$pfrd,$k_card,$r_status,$file_sc);
			}
		} else {
			echo 'Successfully added!';
			$file_sc = '';
			$emp_id= $this->input->post('emp_id');
			$campaign= $this->input->post('campaign');
			$k_type= $this->input->post('k_type');
			$c_name= $this->input->post('c_name');
			$p_number= $this->input->post('p_number');
			$e_add= $this->input->post('e_add');
			$comment= $this->input->post('comment');
			$supervisor= $this->input->post('supervisor');
			//$file= $this->input->post('file');
			$p_reward= $this->input->post('p_reward');
			$pfrd= $this->input->post('pfrd');
			$k_card= $this->input->post('k_card');
			$r_status= $this->input->post('r_status');

			$this->KudosModel->add_kudos($emp_id,$campaign,$k_type,$c_name,$p_number,$e_add,$comment,$supervisor,$p_reward,$pfrd,$k_card,$r_status,$file_sc);
		}

	}

	public function updateKudos()
	{
		$k_id= $this->input->post('k_id');
		$path = $_FILES["kudosCardPicture".$k_id]["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Manila');
		$file_sc = 'uploads/kudos_cards/'.date("Ymdhms.").$extension;
		

		$new_name = date("Ymdhms").".".$extension;
		$target_dir = "./uploads/kudos_cards/";
		$target_file = $target_dir.$new_name;

		$type = $_FILES["kudosCardPicture".$k_id]["type"];
		$allowed = array('image/jpeg', 'image/png', 'image/gif');

		if($_FILES["kudosCardPicture".$k_id]["name"]){
			if(!in_array($type,$allowed)){
				echo 'no';
			} else {
				echo 'yes';
				move_uploaded_file($_FILES["kudosCardPicture".$k_id]["tmp_name"],$target_file);
				$emp_id= $this->input->post('emp_id');
				$campaign= $this->input->post('campaign');
				$k_type= $this->input->post('k_type');
				$c_name= $this->input->post('c_name');
				$p_number= $this->input->post('p_number');
				$e_add= $this->input->post('e_add');
				$comment= $this->input->post('comment');
				$supervisor= $this->input->post('supervisor');
			 	//$file= $this->input->post('file');
				$p_reward= $this->input->post('p_reward');
				$pfrd= $this->input->post('pfrd');
				$k_card= $this->input->post('k_card');
				$r_status= $this->input->post('r_status');
				$r_note= $this->input->post('r_note');
				$r_by= $this->input->post('r_by');
				$this->KudosModel->save_update($k_id,$emp_id,$campaign,$c_name,$p_number,$e_add,$comment,$supervisor,$k_type,$file_sc,$p_reward,$pfrd,$k_card,$r_status,$r_note,$r_by);
			}
		}

	}

	public function updateKudosOPT_withPic()
	{
		$k_id= $this->input->post('k_id');
		$path = $_FILES["addFile".$k_id]["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Manila');
		$file_sc = 'uploads/screenshots/'.date("Ymdhms.").$extension;
		

		$new_name = date("Ymdhms").".".$extension;
		$target_dir = "./uploads/screenshots/";
		$target_file = $target_dir.$new_name;

		$type = $_FILES["addFile".$k_id]["type"];
		$allowed = array('image/jpeg', 'image/png', 'image/gif');

		if($_FILES["addFile".$k_id]["name"]){
			if(!in_array($type,$allowed)){
				echo 'no';
			} else {
				echo 'yes';
				move_uploaded_file($_FILES["addFile".$k_id]["tmp_name"],$target_file);
				$emp_id= $this->input->post('emp_id');
				$campaign= $this->input->post('campaign');
				$k_type= $this->input->post('k_type');
				$c_name= $this->input->post('c_name');
				$p_number= $this->input->post('p_number');
				$e_add= $this->input->post('e_add');
				$comment= $this->input->post('comment');
				$supervisor= $this->input->post('supervisor');
			 	//$file= $this->input->post('file');
				$p_reward= $this->input->post('p_reward');
				$pfrd= $this->input->post('pfrd');
				$k_card= $this->input->post('k_card');
				$r_status= $this->input->post('r_status');
				$r_note= $this->input->post('r_note');
				$r_by= $this->input->post('r_by');
				$this->KudosModel->save_update_opt($k_id,$emp_id,$campaign,$c_name,$p_number,$e_add,$comment,$supervisor,$k_type,$file_sc,$p_reward,$pfrd,$k_card,$r_status,$r_note,$r_by);
			}
		} else {
			echo "yes";
			$emp_id= $this->input->post('emp_id');
			$campaign= $this->input->post('campaign');
			$k_type= $this->input->post('k_type');
			$c_name= $this->input->post('c_name');
			$p_number= $this->input->post('p_number');
			$e_add= $this->input->post('e_add');
			$comment= $this->input->post('comment');
			$supervisor= $this->input->post('supervisor');
			$file_sc= $this->input->post('file');
			$p_reward= $this->input->post('p_reward');
			$pfrd= $this->input->post('pfrd');
			$k_card= $this->input->post('k_card');
			$r_status= $this->input->post('r_status');
			$this->KudosModel->save_update_opt($k_id,$emp_id,$campaign,$c_name,$p_number,$e_add,$comment,$supervisor,$k_type,$file_sc,$p_reward,$pfrd,$k_card,$r_status);
		}
	}

	public function updateKudosNoPic()
	{
		$k_id= $this->input->post('k_id');
		$emp_id= $this->input->post('emp_id');
		$campaign= $this->input->post('campaign');
		$k_type= $this->input->post('k_type');
		$c_name= $this->input->post('c_name');
		$p_number= $this->input->post('p_number');
		$e_add= $this->input->post('e_add');
		$comment= $this->input->post('comment');
		$supervisor= $this->input->post('supervisor');
		$file_sc= $this->input->post('file_sc');
		$p_reward= $this->input->post('p_reward');
		$pfrd= $this->input->post('pfrd');
		$k_card= $this->input->post('k_card');
		$r_status= $this->input->post('r_status');
		$r_note= $this->input->post('r_note');
		$r_by= $this->input->post('r_by');
		$this->KudosModel->save_update_opt2($k_id,$emp_id,$campaign,$c_name,$p_number,$e_add,$comment,$supervisor,$k_type,$file_sc,$p_reward,$pfrd,$k_card,$r_status,$r_note,$r_by);
	}

	public function uploadExcel()
	{
		$path = $_FILES["uploadFile"]["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Manila');	

		$target_dir = "./reports/";
		$new_name = "uploaded";
		$target_file = $target_dir.$new_name.'.'.$extension;

		$allowed = array('xls', 'xlsx', 'csv');
		
		if($_FILES["uploadFile"]["name"]){
			if(!in_array($extension,$allowed)){
				echo 'File type is not allowed.';
			} else {
				move_uploaded_file($_FILES["uploadFile"]["tmp_name"],$target_file);
				echo 'Successfully Uploaded.';
			}

		}
	}

	public function getDate()
	{
		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$data = $this->KudosModel->getDateRange($startDate,$endDate);
		header('Content-type: application/json'); 
		echo json_encode($data);
	}

	public function getDate2()
	{
		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$campaign = $this->input->post('campaign');
		$data = $this->KudosModel->getDateRange2($startDate,$endDate,$campaign);
		header('Content-type: application/json'); 
		echo json_encode($data);
	}

	public function getDate3()
	{
		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$data = $this->KudosModel->getDateRange3($startDate,$endDate);
		header('Content-type: application/json'); 
		echo json_encode($data);
	}

	public function getDate4()
	{
		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$campaign = $this->input->post('campaign');
		$data = $this->KudosModel->getDateRange2($startDate,$endDate,$campaign);

		
		foreach($data as $d)
		{
			echo "<tr>";
			echo "<td>".$d->month."</td>";
			echo "<td>".$d->campaign."</td>";
			echo "<td>".$d->kudos_count."</td>";
			echo "</tr>";
		}
		//header('Content-type: application/json'); 
		//echo json_encode($data);
	}

	public function getDate5()
	{
		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$data = $this->KudosModel->getDateRange($startDate,$endDate);

		foreach($data as $d)
		{
			echo "<tr>";
			echo "<td>".$d->month."</td>";
			echo "<td>".$d->kudos_count."</td>";
			echo "</tr>";
		}
		echo "</tbody>";

	}

	public function getDate6()
	{
		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$data = $this->KudosModel->getDateRange3($startDate,$endDate);

		foreach($data as $d)
		{
			echo "<tr>";
			echo "<td>".$d->ambassador."</td>";
			echo "<td>".$d->campaign."</td>";
			echo "<td>".$d->kudos_count."</td>";
			echo "</tr>";
		}
		echo "</tbody>";

	}

	public function getAgents()
	{
		$campaign_id = $this->input->post('campaign_id');
		$data = $this->KudosModel->get_agents($campaign_id);
	   //echo $data;
		echo "<option value=''>-- Select Ambassador Name --</option>";
		foreach($data as $a)
		{
			echo "<option value='".$a->emp_id."'>".$a->fullname."</option>";
		}
	   //header('Content-Type', 'application/json');
	}

	public function excel_report1()
	{
		//$this->load->library('PHPExcel');
		$this->load->library('PHPExcel', NULL, 'excel');

		$this->excel->setActiveSheetIndex(0);

		$this->excel->getActiveSheet()->setTitle('Users list');

		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$data = $this->KudosModel->getDateRange($startDate,$endDate);

		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 15,
				));

		$styleArray2 = array(
			'font'  => array(
				'size'  => 15,
				));

		$styleArray3 = array(
			'font'  => array(
				'size'  => 15,
				'color' => array('rgb' => 'FFFFFF'),
				));

		$styleArray6 = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);

		$this->excel->getActiveSheet()->getStyle('A1:B2')->applyFromArray($styleArray6);
		
		$this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($styleArray6);
		

		$this->excel->getActiveSheet()->mergeCells('A3:B3');
		$this->excel->getActiveSheet()->setCellValue('A3', 'TOTAL KUDOS COUNT ('.$startDate.' - '.$endDate.')');
		$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
		$this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($styleArray3);
		$this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($styleArray);

		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
		$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
		$this->excel->getActiveSheet()->mergeCells('A1:B2');

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($this->logo);
      	$objDrawing->setOffsetX(40);    // setOffsetX works properly
		$objDrawing->setOffsetY(10);  //setOffsetY has no effect
		$objDrawing->setCoordinates('A1');
		$objDrawing->setHeight(85); // logo height
		$objDrawing->setWorksheet($this->excel->getActiveSheet());

		$this->excel->getActiveSheet()->getStyle('A4:B4')->applyFromArray($styleArray6);
		

		$this->excel->getActiveSheet()->getStyle('A4:D4')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);

		$this->excel->getActiveSheet()->setCellValue('A4', 'Month and Year');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Kudos Count');



		$row = 5;
		foreach($data as $r){
			$this->excel->getActiveSheet()->fromArray(array($r->month, $r->kudos_count), null, 'A'.$row);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray2);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray6);
			$row++;

			
		}

		unset($styleArray6);


		

		ob_end_clean();
		date_default_timezone_set("Asia/Manila");
		$timestamp=date("Y-m-d-His");
		$filename='KudosCount.xls'; 

		header('Content-Type: application/vnd.ms-excel'); 

		header('Content-Disposition: attachment;filename="'.$filename.'"'); 

		header('Cache-Control: max-age=0'); 

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save($this->savefile.''.$filename);
        //$objWriter->save($filename);
		exit();
	}

	public function excel_report2()
	{
		//$this->load->library('PHPExcel');
		$this->load->library('PHPExcel', NULL, 'excel');

		$this->excel->setActiveSheetIndex(0);

		$this->excel->getActiveSheet()->setTitle('Users list');

		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$data = $this->KudosModel->getDateRange3($startDate,$endDate);

		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 15,
				));

		$styleArray2 = array(
			'font'  => array(
				'size'  => 15,
				));

		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 15,
				'color' => array('rgb' => 'FFFFFF'),
				));

		$styleArray6 = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($this->logo);
      	$objDrawing->setOffsetX(220);    // setOffsetX works properly
		$objDrawing->setOffsetY(10);  //setOffsetY has no effect
		$objDrawing->setCoordinates('A1');
		$objDrawing->setHeight(85); // logo height
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
		$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
		$this->excel->getActiveSheet()->mergeCells('A1:C2');

		$this->excel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray6);
		$this->excel->getActiveSheet()->getStyle('A2:C2')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->mergeCells('A3:C3');
		$this->excel->getActiveSheet()->setCellValue('A3', 'TOTAL KUDOS COUNT BY AMBASSADOR ('.$startDate.' - '.$endDate.')');
		$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
		$this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray3);

		$this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

		$this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->setCellValue('A4', 'Ambassador');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Campaign');
		$this->excel->getActiveSheet()->setCellValue('C4', 'Kudos Count');



		$row = 5;
		foreach($data as $r){
			$this->excel->getActiveSheet()->fromArray(array($r->ambassador, $r->campaign, $r->kudos_count), null, 'A'.$row);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray2);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray6);
			$row++;
		}

		unset($styleArray6);

		ob_end_clean();
		date_default_timezone_set("Asia/Manila");
		$timestamp=date("Y-m-d-His");
		$filename='KudosCountAmbassadors.xls'; 

		header('Content-Type: application/vnd.ms-excel'); 

		header('Content-Disposition: attachment;filename="'.$filename.'"'); 

		header('Cache-Control: max-age=0'); 

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save($this->savefile.''.$filename);
        //$objWriter->save($filename);
		exit();
	}



	public function excel_report3()
	{
		//$this->load->library('PHPExcel');
		$this->load->library('PHPExcel', NULL, 'excel');

		$this->excel->setActiveSheetIndex(0);

		$this->excel->getActiveSheet()->setTitle('Users list');

		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$campaign = $this->input->post('campaign');
		$data = $this->KudosModel->getDateRange2($startDate,$endDate,$campaign);

		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 15,
				));

		$styleArray2 = array(
			'font'  => array(
				'size'  => 15,
				));

		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 15,
				'color' => array('rgb' => 'FFFFFF'),
				));

		$styleArray6 = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($this->logo);
      	$objDrawing->setOffsetX(220);    // setOffsetX works properly
		$objDrawing->setOffsetY(10);  //setOffsetY has no effect
		$objDrawing->setCoordinates('A1');
		$objDrawing->setHeight(85); // logo height
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
		$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
		$this->excel->getActiveSheet()->mergeCells('A1:C2');

		$this->excel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray6);
		$this->excel->getActiveSheet()->getStyle('A2:C2')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->mergeCells('A3:C3');
		$this->excel->getActiveSheet()->setCellValue('A3', 'TOTAL KUDOS COUNT BY CAMPAIGN ('.$startDate.' - '.$endDate.')');
		$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
		$this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray3);

		$this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray6);
		$this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);

		$this->excel->getActiveSheet()->setCellValue('A4', 'Month and Year');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Campaign');
		$this->excel->getActiveSheet()->setCellValue('C4', 'Kudos Count');



		$row = 5;
		foreach($data as $r){
			$this->excel->getActiveSheet()->fromArray(array($r->month, $r->campaign, $r->kudos_count), null, 'A'.$row);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray2);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray6);
			$row++;
		}

		unset($styleArray6);

		ob_end_clean();
		date_default_timezone_set("Asia/Manila");
		$timestamp=date("Y-m-d-His");
		$filename='KudosCountCampaign.xls'; 

		header('Content-Type: application/vnd.ms-excel'); 

		header('Content-Disposition: attachment;filename="'.$filename.'"'); 

		header('Cache-Control: max-age=0'); 

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save($this->savefile.''.$filename);
        //$objWriter->save($filename);
		exit();
	}

	public function viewAddMultiple()
	{
		$this->load->library('PHPExcel', NULL, 'excel');
		// $file = base_url().'reports/uploaded.xlsx';
		$this->excel = PHPExcel_IOFactory::load($this->file);
		//get only the Cell Collection
		$cell_collection = $this->excel->getActiveSheet()->getCellCollection();

		for($rows=4;$rows<=18;$rows++){

			$empidd= $this->excel->getActiveSheet()->getCell('A'.$rows)->getValue();

			if($empidd=="" || empty($empidd) || $empidd==null){
				// echo "Nicca";
			} else{
				// echo $empidd;
				$datasss = $this->KudosModel->getEmployee($empidd);
				foreach ($datasss as $key => $value) {
					echo "<tr>";
					echo "<td>".$value->fname." ".$value->lname."</td>";
					echo "<td>".$value->acc_name."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('B'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('C'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('D'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('E'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('F'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('G'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('H'.$rows)->getFormattedValue()."</td>";
					echo "</tr>";
				}
			}
		} 
	}

	public function addAddMultiple()
	{
		$this->load->library('PHPExcel', NULL, 'excel');

		$this->excel = PHPExcel_IOFactory::load($this->file);
		//get only the Cell Collection
		$cell_collection = $this->excel->getActiveSheet()->getCellCollection();

		for($rows=4;$rows<=18;$rows++){

			$empidd= $this->excel->getActiveSheet()->getCell('A'.$rows)->getValue();
			if($empidd!="" || !empty($empidd) || $empidd!=null){

				$datasss = $this->KudosModel->getEmployee($empidd);


				foreach ($datasss as $key => $value) {

					$emp_id = $value->emp_id;
					$acc_id = $value->acc_id;
					$k_type = $this->excel->getActiveSheet()->getCell('B'.$rows)->getValue();
					$r_type = $this->excel->getActiveSheet()->getCell('C'.$rows)->getValue();
					$c_name = $this->excel->getActiveSheet()->getCell('D'.$rows)->getValue();
					$c_pnumber = $this->excel->getActiveSheet()->getCell('E'.$rows)->getValue();
					$c_email = $this->excel->getActiveSheet()->getCell('F'.$rows)->getValue();
					$c_comment = $this->excel->getActiveSheet()->getCell('G'.$rows)->getValue();
					$d_given = $this->excel->getActiveSheet()->getCell('H'.$rows)->getFormattedValue();
					$uid = $this->session->userdata('uid');
					$this->KudosModel->insertExcel($emp_id,$acc_id,$k_type,$r_type,$c_name,$c_pnumber,$c_email,$c_comment,$uid,$d_given);

				}
			} 
		} 
	}
	public function sample()
	{
		$data = $this->KudosModel->getDateRangeDrilldown2();
		print_r($data);
	}


	//----------------------------------NICCACODES-----------------------------------

	public function kudoslistexcel(){
		$this->load->library('PHPExcel', NULL, 'excel');

		//load our new PHPExcel library
		// $this->load->library('excel');
//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Nicca WorkSheet');



//-------------------------------------------------------------------
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($this->logo);
      	$objDrawing->setOffsetX(0);    // setOffsetX works properly
		$objDrawing->setOffsetY(0);  //setOffsetY has no effect
		$objDrawing->setCoordinates('B1');
		$objDrawing->setHeight(100); // logo height
		// $objDrawing->setWidth(320); // logo width
		// $objDrawing->setWidthAndHeight(200,400);
		$objDrawing->setResizeProportional(true);
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
//----------------------------------------------------------------------
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38);
		$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(38);


		$this->excel->getActiveSheet()->setCellValue('F1', "Date Downloaded:");
		$this->excel->getActiveSheet()->setCellValue('F2', DATE('F d, Y h:i a'));
		$this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setWrapText(true); 
//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A3', 'Ambassador Name');
		$this->excel->getActiveSheet()->setCellValue('B3', 'Campaign');
		$this->excel->getActiveSheet()->setCellValue('C3', 'Kudos Type');
		$this->excel->getActiveSheet()->setCellValue('D3', 'Kudos Card');
		$this->excel->getActiveSheet()->setCellValue('E3', 'Reward Status');
		$this->excel->getActiveSheet()->setCellValue('F3', 'Date Added');
//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:E2');
//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A1:E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('F1:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('F2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

		$this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
		$this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'ffffff'))));
		$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => '000000'))));


		$this->excel->getActiveSheet()->setAutoFilter('A3:F3');
		$this->excel->getActiveSheet()->getProtection()->setSort(true);


		// $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(28);

		$kudoses = $this->KudosModel->get_kudos();
		$x = 4;$bool = TRUE;
		foreach($kudoses as $k){
			if($bool==TRUE){
				$this->excel->getActiveSheet()->getStyle('A'.$x.':F'.$x)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'dddddd'))));
				$bool=FALSE;
			}else{
				$this->excel->getActiveSheet()->getStyle('A'.$x.':F'.$x)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'eeeeee'))));
				$bool=TRUE;
			}
			$this->excel->getActiveSheet()->setCellValue('A'.$x, $k->ambassador);
			$this->excel->getActiveSheet()->setCellValue('B'.$x, $k->campaign);
			$this->excel->getActiveSheet()->setCellValue('C'.$x, $k->kudos_type);
			$this->excel->getActiveSheet()->setCellValue('D'.$x, $k->kudos_card);
			$this->excel->getActiveSheet()->setCellValue('E'.$x, $k->reward_status);
			$this->excel->getActiveSheet()->setCellValue('F'.$x, $k->date_given);
			if($k->kudos_card=='Done'){
				$this->excel->getActiveSheet()->getStyle('D'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => '00CC00'))));
			}else{
				$this->excel->getActiveSheet()->getStyle('D'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => 'FF0000'))));
			}
			if($k->reward_status=='Released'){
				$this->excel->getActiveSheet()->getStyle('E'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => '00CC00'))));
			}else{
				$this->excel->getActiveSheet()->getStyle('E'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => 'FF0000'))));
			}

			$x++;
		}
		$x--;
		$this->excel->getActiveSheet()->getStyle('A1:F'.$x)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
		$filename='KudosList.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');

	}


	//----------------------------------NICCACODES-----------------------------------
}
