<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once dirname(__FILE__) . "/General.php";

class Additional_hour extends General
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *     - or -
     *         http://example.com/index.php/welcome/index
     *     - or -
     */
    public function request()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Personal AHR',
        ];
        $this->load_template_view('templates/additional_hour_request/personal_ahr', $data);
    }

    public function approval_ahr()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Approval AHR',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/additional_hour_request/approval_ahr', $data);
        }
    }

    public function ahr_settings()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'AHR Settings',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/additional_hour_request/ahr_settings', $data);
        }
    }

    public function ahr_monitoring()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'AHR Monitoring',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/additional_hour_request/ahr_monitoring', $data);
        }
    }

    public function ahr_reporting()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'AHR Reporting',
        ];
        if($this->check_access()){
        $this->load_template_view('templates/additional_hour_request/ahr_reporting', $data);
        }
    }

    // public function ahr_monthly_report()
    // {
    //     $data = [
    //         'uri_segment' => $this->uri->segment_array(),
    //         'title' => 'AHR Monthly Report',
    //     ];
    //     $this->load_template_view('templates/additional_hour_request/ahr_monthly_report', $data);
    // }

    // CREATE ---------------------------------------------------------------------------------

    public function set_word_request($data)
    {
        return $insertStat = $this->general_model->insert_vals($data, 'tbl_word_request');
    }

    public function request_ahr()
    {
        if ($this->input->post("formType") == 1) {
            $data['requestType'] = "rendered";
        } else {
            $data['requestType'] = "unrendered";

        }
        $data['userId'] = $this->session->userdata('uid');
        $data['additionalHourTypeId'] = $this->input->post("ahrType");
        $data['schedId'] = $this->input->post("schedId");
        $data['otStartDate'] = $this->input->post("otStartDate");
        $data['otStartTime'] = $this->input->post("otStartTime");
        $data['otEndDate'] = $this->input->post("otEndDate");
        $data['otEndTime'] = $this->input->post("otEndTime");
        $data['totalOtHours'] = $this->input->post("otHours");
        $data['reason'] = $this->input->post("reason");

        $insert_stat['set_sched'] = $this->update_sched_lock($this->input->post("schedId"), "ahr", 1, 2);
        $ahr_id = $this->general_model->insert_vals_last_inserted_id($data, "tbl_additional_hour_request");
        // SET WORD REQUEST FOR UNRENDERED FILING
        if ($this->input->post("formType") == 2) {
            if ($this->input->post("ahrType") == 3) {
                $word['accTimeId'] = $this->input->post("accTimeId");
                $word['additionalHourRequestId'] = $ahr_id;
                $insert_stat['insert_word_request'] = $this->set_word_request($word);
            }
        }

        $insert_stat['request_stat'] = $ahr_id;
        $date_filed = $this->get_date_filed($ahr_id);
        // SET APPROVERS
        $approvers = $this->set_approvers($date_filed, $this->session->userdata('uid'), 1, 'additionalHourRequest_ID', $ahr_id);
        $insert_stat['insert_approvers'] = $this->general_model->batch_insert($approvers, 'tbl_additional_hour_request_approval');

        if ($this->input->post("ahrType") == 3) {
            $ahrType = 'a WORD type of';
            $ahrTypeAlone = 'WORD';
        } else if ($this->input->post("ahrType") == 2) {
            $ahrType = 'a PRE shift';
            $ahrTypeAlone = 'PRE';
        } else {
            $ahrType = 'an AFTER shift';
            $ahrTypeAlone = 'AFTER';
        }

        // SET NOTIFICATION
        $notif_mssg = "has sent you " . $ahrType . " Additional Hour request.  <br><small><b>AHR-ID</b>: " . str_pad($ahr_id, 8, '0', STR_PAD_LEFT) . " </small>";
        $link = "additional_hour/approval_ahr";
        $insert_stat['set_notif_details'] = $this->set_notif($this->session->userdata('uid'), $notif_mssg, $link, $approvers);

        echo json_encode($insert_stat);
    }

    // READ -----------------------------------------------------------------------------------

    private function get_date_filed($ahr_id)
    {
        $fields = "dateTimeFiled";
        $where = "additionalHourRequestId = $ahr_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_additional_hour_request");
        return $record->dateTimeFiled;
    }
    private function get_user_details($userId)
    {
        $fields = "emp_id";
        $where = "uid = $userId";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_user");
    }

    public function check_if_date($value)
    {
        $date_bool = 0;
        // echo ucfirst($value);
        $formats = ['M d, Y', 'Y-m-d'];
        foreach ($formats as $f) {
            $d = DateTime::createFromFormat($f, ucfirst($value));
            $is_date = $d && $d->format($f) === ucfirst($value);
            if (true == $is_date) {
                $date_bool = 1;
            }
        }
        return $date_bool;
    }

    public function convert_date($dateVal)
    {
        $date = date_create($dateVal);
        return date_format($date, "Y-m-d");
    }

    // public function check_date(){
    //     $date_bool = $this->check_if_date("Mar 05, 2018");
    //     if((int)$date_bool){
    //         echo $this->convert_date("Mar 05, 2018");
    //     }else{
    //         echo "not a date";
    //     }
    // }

    public function get_personal_pending_ahr_request()
    {
        $datatable = $this->input->post('datatable');
        $user_id = $this->session->userdata('uid');
        $status_id = $datatable['query']['requestStat'];
        if ($status_id == 0) {
            $status = "AND request.approvalStatus_ID IN (2, 12)";
        } else {
            $status = "AND request.approvalStatus_ID = $status_id";
        }
        $query['query'] = "SELECT request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as typesDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedid, sched.sched_date as sched_date, reason, dateTimeFiled, approvalStatus_ID, statuses.description as statusDescription, requestType FROM tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched WHERE sched.sched_id = request.schedId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND request.userId = $user_id " . $status;
        if ($datatable['query']['personalPendingAhrSearch'] != '') {
            $keyword = $datatable['query']['personalPendingAhrSearch'];
            $date_bool = $this->check_if_date($keyword);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "sched_date = '$date' OR dateTimeFiled LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "additionalHourRequestId LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "additionalHourRequestId = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_personal_record_ahr_request()
    {
        $datatable = $this->input->post('datatable');
        $user_id = $this->session->userdata('uid');
        $status_id = $datatable['query']['requestStat'];
        $rangeStart = $datatable['query']['rangeStart'];
        $rangeEnd = $datatable['query']['rangeEnd'];
        $query['query'] = "SELECT request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as typesDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedid, sched.sched_date as sched_date, reason, dateTimeFiled, statuses.description as statusDescription, request.approvalStatus_ID FROM tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched WHERE sched.sched_id = request.schedId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND request.userId = $user_id AND DATE(sched_date) >= '$rangeStart' AND DATE(sched_date) <= '$rangeEnd' AND request.approvalStatus_ID = $status_id";
        if (($datatable['query']['personalAhrRecordSearch']) != '') {
            $keyword = $datatable['query']['personalAhrRecordSearch'];
            $date_bool = $this->check_if_date($keyword);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "sched_date = '$date' OR dateTimeFiled LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "additionalHourRequestId LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "additionalHourRequestId = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_approval_pending_ahr_request()
    {
        $datatable = $this->input->post('datatable');
        $user_id = $this->session->userdata("uid");
        $requestor_id = $datatable['query']['user'];
        if ($requestor_id == 0) {
            $user = "";
        } else {
            $user = "AND ahrRequest.userId = $requestor_id";
        }
        $query['query'] = "SELECT approval.additionalHourRequestApproval_ID, approval.approvalLevel, ahrRequest.additionalHourRequestId, ahrRequest.dateTimeFiled, ahrRequest.otStartDate, ahrRequest.otStartTime, ahrRequest.otEndDate, ahrRequest.otEndTime, ahrRequest.totalOtHours, ahrRequest.approvalStatus_ID, ahrRequest.reason, ahrRequest.requestType, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahrRequest.userId, app.pic, app.fname, app.lname, app.mname, app.nameExt, schedType.type, sched_date, dead.deadline FROM tbl_status statuses,tbl_additional_hour_request_approval approval, tbl_additional_hour_type ahrType, tbl_additional_hour_request ahrRequest, tbl_user users, tbl_schedule sched, tbl_schedule_type schedType, tbl_deadline dead, tbl_employee emp, tbl_applicant app WHERE schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = ahrRequest.schedId AND ahrType.additionalHourTypeId = ahrRequest.additionalHourTypeId AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND dead.userId = approval.userId AND dead.request_ID = approval.additionalHourRequest_ID AND dead.module_ID = 1 AND users.uid = ahrRequest.userId AND statuses.status_ID = ahrRequest.approvalStatus_ID AND ahrRequest.additionalHourRequestId = approval.additionalHourRequest_ID AND approval.userId = $user_id AND approval.approvalStatus_ID = 4 " . $user;
        if ($datatable['query']['approvalPendingAhrSearch'] != '') {
        $keyword = $datatable['query']['approvalPendingAhrSearch'];
            $date_bool = $this->check_if_date($keyword);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "sched_date = '$date' OR dead.deadline LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "ahrRequest.additionalHourRequestId LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "ahrRequest.additionalHourRequestId = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_approval_ahr_records()
    {
        $datatable = $this->input->post('datatable');
        $user_id = $this->session->userdata("uid");
        $status_id = $datatable['query']['status'];
        $requestor_id = $datatable['query']['user'];
        $rangeStart = $datatable['query']['rangeStart'];
        $rangeEnd = $datatable['query']['rangeEnd'];
        if ($requestor_id == 0) {
            $user = "";
        } else {
            $user = "AND ahrRequest.userId = $requestor_id";
        }
        if ($status_id == 0) {
            $status = "AND approval.approvalStatus_ID NOT IN (2, 4, 7)";
        } else {
            $status = "AND approval.approvalStatus_ID = $status_id";
        }
        $query['query'] = "SELECT approval.additionalHourRequestApproval_ID, approval.approvalLevel, ahrRequest.additionalHourRequestId, ahrRequest.dateTimeFiled, ahrRequest.otStartDate, ahrRequest.otStartTime, ahrRequest.otEndDate, ahrRequest.otEndTime, ahrRequest.totalOtHours, ahrRequest.approvalStatus_ID, ahrRequest.reason, ahrRequest.requestType, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahrRequest.userId, app.pic, app.fname, app.lname, app.mname, app.nameExt, schedType.type, sched_date, approval.approvalStatus_ID as decisionStat FROM tbl_status statuses,tbl_additional_hour_request_approval approval, tbl_additional_hour_type ahrType, tbl_additional_hour_request ahrRequest, tbl_user users, tbl_schedule sched, tbl_schedule_type schedType, tbl_employee emp, tbl_applicant app WHERE schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = ahrRequest.schedId AND ahrType.additionalHourTypeId = ahrRequest.additionalHourTypeId AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = ahrRequest.userId AND statuses.status_ID = approval.approvalStatus_ID AND ahrRequest.additionalHourRequestId = approval.additionalHourRequest_ID AND DATE(sched_date) >= '$rangeStart' AND DATE(sched_date) <= '$rangeEnd' AND approval.userId = $user_id " . $user . " $status";
        if ($datatable['query']['approvalRecordSearch'] != '') {
            $keyword = $datatable['query']['approvalRecordSearch'];
            $date_bool = $this->check_if_date($keyword);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "sched_date = '$date'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "ahrRequest.additionalHourRequestId LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "ahrRequest.additionalHourRequestId = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_reporting_ahr_records()
    {
        $datatable = $this->input->post('datatable');
        $status_id = $datatable['query']['status'];
        $requestor_id = $datatable['query']['user'];
        $rangeStart = $datatable['query']['rangeStart'];
        $rangeEnd = $datatable['query']['rangeEnd'];
        if ($requestor_id == 0) {
            $user = "";
        } else {
            $user = "AND ahrRequest.userId = $requestor_id";
        }
        if ($status_id == 0) {
            $status = "AND ahrRequest.approvalStatus_ID NOT IN (2, 4)";
        } else {
            $status = "AND ahrRequest.approvalStatus_ID = $status_id";
        }
        $query['query'] = "SELECT ahrRequest.additionalHourRequestId, ahrRequest.dateTimeFiled, ahrRequest.otStartDate, ahrRequest.otStartTime, ahrRequest.otEndDate, ahrRequest.otEndTime, ahrRequest.totalOtHours, ahrRequest.approvalStatus_ID, ahrRequest.reason, ahrRequest.requestType, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahrRequest.userId, app.fname, app.lname, app.mname, schedType.type, sched_date FROM tbl_status statuses, tbl_additional_hour_type ahrType, tbl_additional_hour_request ahrRequest, tbl_user users, tbl_schedule sched, tbl_schedule_type schedType, tbl_applicant app WHERE schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = ahrRequest.schedId AND ahrType.additionalHourTypeId = ahrRequest.additionalHourTypeId AND app.apid = emp.apid AND emp.emp_id = users_emp_id AND users.uid = ahrRequest.userId AND statuses.status_ID = ahrRequest.approvalStatus_ID AND DATE(ahrRequest.otStartDate) >= '$rangeStart' AND DATE(ahrRequest.otStartDate) <= '$rangeEnd' " . $user . " $status";
        if (isset($datatable['query']['personalAhrRecordSearch'])) {
            $keyword = $datatable['query']['personalAhrRecordSearch'];
            // $query['search']['append'] = " AND additionalHourRequestId LIKE '%" . $keyword . "%' OR otStartDate LIKE '%" . $keyword . "%' OR otStartTime LIKE '%" . $keyword . "%' OR otEndDate LIKE '%" . $keyword . "%' OR otEndTime LIKE '%" . $keyword . "%' OR totalOtHours LIKE '%" . $keyword . "%' OR sched_date LIKE '%" . $keyword . "%' OR time_start LIKE '%" . $keyword . "%' OR time_end LIKE '%" . $keyword . "%' OR reason LIKE '%" . $keyword . "%' OR dateTimeFiled LIKE '%" . $keyword . "%' OR reason LIKE '%" . $keyword . "%' OR dateTimeFiled LIKE '%" . $keyword . "%'";
            // $query['search']['total'] = $query['query'] . " AND additionalHourRequestId LIKE '%" . $keyword . "%' OR otStartDate LIKE '%" . $keyword . "%' OR otStartTime LIKE '%" . $keyword . "%' OR otEndDate LIKE '%" . $keyword . "%' OR otEndTime LIKE '%" . $keyword . "%' OR totalOtHours LIKE '%" . $keyword . "%' OR sched_date LIKE '%" . $keyword . "%' OR time_start LIKE '%" . $keyword . "%' OR time_end LIKE '%" . $keyword . "%' OR reason LIKE '%" . $keyword . "%' OR dateTimeFiled LIKE '%" . $keyword . "%' OR reason LIKE '%" . $keyword . "%' OR dateTimeFiled LIKE '%" . $keyword . "%'";
            $query['search']['append'] = " AND (additionalHourRequestId LIKE '%" . $keyword . "%' OR otStartDate LIKE '%" . $keyword . "%' OR otEndDate LIKE '%" . $keyword . "%' OR otStartTime LIKE '%" . $keyword . "%' OR otEndTime LIKE '%" . $keyword . "%' OR totalOtHours LIKE '%" . $keyword . "%' OR sched_date LIKE '%" . $keyword . "%' OR reason LIKE '%" . $keyword . "%' OR dateTimeFiled LIKE '%" . $keyword . "%' OR reason LIKE '%" . $keyword . "%' OR dateTimeFiled LIKE '%" . $keyword . "%')";
            $query['search']['total'] = " AND (additionalHourRequestId LIKE '%" . $keyword . "%' OR otStartDate LIKE '%" . $keyword . "%' OR otEndDate LIKE '%" . $keyword . "%' OR otStartTime LIKE '%" . $keyword . "%' OR otEndTime LIKE '%" . $keyword . "%' OR totalOtHours LIKE '%" . $keyword . "%' OR sched_date LIKE '%" . $keyword . "%' OR reason LIKE '%" . $keyword . "%' OR dateTimeFiled LIKE '%" . $keyword . "%' OR reason LIKE '%" . $keyword . "%' OR dateTimeFiled LIKE '%" . $keyword . "%')";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_report_specific_emp()
    {
        $status_id = $this->input->post('status');
        $rangeStart = $this->input->post('rangeStart');
        $rangeEnd = $this->input->post('rangeEnd');
        $account = $this->input->post('account');
        $emp_type = $this->input->post('empType');
        $ahr_type = $this->input->post('ahrType');
        $paymentStat = $this->input->post('paymentStat');

        if ($emp_type == 'Admin') {
            $employee_type = "emp.acc_id = acc.acc_id AND acc.dep_id = $account";
            $tbl_account = "tbl_account acc,";
            $posClass = "pos.class='Admin'";
        } else if ($emp_type == 'Ambs') {
            $tbl_account = "";
            $employee_type = "emp.acc_id = $account";
            $posClass = "pos.class='Agent'";
        }

        if ($ahr_type == 0) {
            $ahr_type_col = "";
        } else {
            $ahr_type_col = "AND ahr.additionalHourTypeId = $ahr_type";
        }

        $released = "";
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }

        // SELECT ahr.additionalHourRequestId, ahr.dateTimeFiled, ahr.otStartDate, ahr.otStartTime, ahr.otEndDate, ahr.otEndTime, ahr.totalOtHours, ahr.approvalStatus_ID, ahr.reason, ahr.requestType, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahr.userId, users.fname, app.lname, schedType.type, sched_date FROM tbl_additional_hour_request ahr, tbl_additional_hour_type ahrType, tbl_schedule sched, tbl_schedule_type schedType, tbl_user users, $tbl_account tbl_employee emp, tbl_status statuses WHERE schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = ahr.schedId AND ahrType.additionalHourTypeId = ahr.additionalHourTypeId AND users.uid = ahr.userId AND statuses.status_ID = ahr.approvalStatus_ID AND users.emp_id = emp.emp_id AND $employee_type AND users.uid = ahr.userId AND Date(ahr.otStartDate)>= '$rangeStart' AND Date(ahr.otStartDate)<= '$rangeEnd' AND ahr.approvalStatus_ID = $status_id ".$user
        $fields = "DISTINCT(ahr.userId) as userId, app.fname, app.lname, app.mname";
        $where = "$posClass AND pos.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND sched.sched_id = ahr.schedId AND users.uid = ahr.userId AND app.apid = emp.apid AND users.emp_id = emp.emp_id AND $employee_type AND users.uid = ahr.userId AND Date(sched_date)>= '$rangeStart' AND Date(sched_date)<= '$rangeEnd' AND ahr.approvalStatus_ID = $status_id " . $ahr_type_col . " $released";
        $table = "tbl_schedule sched, tbl_user users, tbl_additional_hour_request ahr, $tbl_account tbl_employee emp, tbl_position pos, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'app.lname', 'ASC');
        echo json_encode($record);
    }

    public function get_drilldown_emp_record()
    {
        $datatable = $this->input->post('datatable');
        $status_id = $datatable['query']['status'];
        $requestor_id = $datatable['query']['user'];
        $rangeStart = $datatable['query']['rangeStart'];
        $rangeEnd = $datatable['query']['rangeEnd'];
        $account = $datatable['query']['account'];
        $emp_type = $datatable['query']['empType'];
        $ahr_type = $datatable['query']['ahrType'];
        $paymentStat = $datatable['query']['paymentStat'];
        $tbl_account = "";
        $pos_class = "";
        $employee_type = "";
        $released = "";
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }

        if ($requestor_id == 0) {
            $user = "";
        } else {
            $user = "AND ahr.userId = $requestor_id";
        }

        if ($ahr_type == 0) {
            $ahr_type_col = "";
        } else {
            $ahr_type_col = "AND ahr.additionalHourTypeId = $ahr_type";
        }

        if ($emp_type == 'Admin') {
            $employee_type = "AND emp.acc_id = acc.acc_id AND acc.dep_id = $account";
            $pos_class = "AND pos.class='Admin'";
            $tbl_account = "tbl_account acc,";
        } else if ($emp_type == 'Ambs') {
            $tbl_account = "";
            $pos_class = "AND pos.class='Agent'";
            $employee_type = "AND emp.acc_id = $account";
        }

        $query['query'] = "SELECT ahr.additionalHourRequestId, ahr.dateTimeFiled, ahr.otStartDate, ahr.otStartTime, ahr.otEndDate, ahr.otEndTime, ahr.totalOtHours, ahr.approvalStatus_ID, ahr.reason, ahr.requestType, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahr.userId, app.pic, app.fname, app.lname, app.mname, schedType.type, sched_date FROM tbl_additional_hour_request ahr, tbl_additional_hour_type ahrType, tbl_schedule sched, tbl_schedule_type schedType, tbl_user users, tbl_position pos, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote, $tbl_account tbl_employee emp, tbl_status statuses, tbl_applicant app WHERE schedType.schedtype_id = sched.schedtype_id $pos_class AND pos.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND sched.sched_id = ahr.schedId AND ahrType.additionalHourTypeId = ahr.additionalHourTypeId AND users.uid = ahr.userId AND statuses.status_ID = ahr.approvalStatus_ID AND app.apid = emp.apid AND users.emp_id = emp.emp_id $employee_type AND users.uid = ahr.userId AND Date(sched_date)>= '$rangeStart' AND Date(sched_date)<= '$rangeEnd' $ahr_type_col AND ahr.approvalStatus_ID = $status_id " . $user . " $released";
        if ($datatable['query']['employeeAhrRecordSearch'] != '') {
            $keyword = $datatable['query']['employeeAhrRecordSearch'];
            $date_bool = $this->check_if_date($keyword);

            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "sched_date = '$date' OR ahr.dateTimeFiled LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "ahr.additionalHourRequestId LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "ahr.additionalHourRequestId = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_approval_info()
    {
        $request_id = $this->input->post('ahrId');
        $fields = "app.pic, app.fname, app.lname, app.mname, empStat.status as empStatus, approval.additionalHourRequestApproval_ID,  approval.approvalLevel, approval.approvalStatus_ID, approval.approvalStatus_ID, stat.description, approval.dateTimeStatus, approval.remarks, positions.pos_details positionDescription, dead.deadline";
        $where = "stat.status_ID = approvalStatus_ID AND empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id =users.emp_id AND dead.module_ID = 1 AND dead.request_ID = approval.additionalHourRequest_ID AND dead.userId = approval.userId  AND users.uid = approval.userId AND approval.additionalHourRequest_ID = $request_id";
        $table = "tbl_deadline dead,tbl_additional_hour_request_approval approval, tbl_user users, tbl_status stat, tbl_account accounts, tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        echo json_encode($record);
    }

    public function get_ahr_requests()
    {
        $datatable = $this->input->post('datatable');
        $user_id = $this->session->userdata('uid');
        $status_id = $datatable['query']['status'];
        $user_id = $datatable['query']['user'];
        if ($user_id == 0) {
            $user = "";
        } else {
            $user = "AND request.userId = $user_id";
        }
        if ($status_id == 0) {
            $status = "AND request.approvalStatus_ID IN (2, 12)";
        } else {
            $status = "AND request.approvalStatus_ID = $status_id";
        }
        // $query['query'] = "SELECT request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as typesDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedid, sched.sched_date as sched_date, time_start, time_end, reason, dateTimeFiled, statuses.description as statusDescription, approval.approvalLevel FROM tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched, tbl_acc_time accTime, tbl_time times, tbl_additional_hour_request_approval approval WHERE times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND sched.sched_id = request.schedId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND approval.approvalStatus_ID = 4 AND approval.additionalHourRequest_ID = request.additionalHourRequestId AND request.userId = $user_id AND request.approvalStatus_ID = $status_id";
        $query['query'] = "SELECT request.additionalHourRequestId additionalHourRequestId, app.pic, app.fname, app.lname, app.mname, app.nameExt, types.additionalHourTypeId, types.description as typesDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, requestType, schedid, sched.sched_date as sched_date, reason, dateTimeFiled, request.approvalStatus_ID,  statuses.description as statusDescription FROM tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type as types, tbl_schedule sched, tbl_user users,tbl_applicant app, tbl_employee emp  WHERE app.apid = emp.apid AND emp.emp_id =users.emp_id AND users.uid = request.userId AND sched.sched_id = request.schedId AND statuses.status_ID = request.approvalStatus_ID AND types.additionalHourTypeId = request.additionalHourTypeId $status " . $user;
        if ($datatable['query']['requestAhrSearch'] != '') {
            $keyword = $datatable['query']['requestAhrSearch'];
            $date_bool = $this->check_if_date($keyword);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "sched_date = '$date'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "request.additionalHourRequestId LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "request.additionalHourRequestId = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    // Monitoring Count
    private function get_ahr_request_status_count($status)
    {
        $fields = "count(additionalHourRequestId) as ahrCount";
        $where = "approvalStatus_ID = $status";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_additional_hour_request");
        return $record[0]->ahrCount;
    }

    public function get_ahr_requests_pending()
    {
        $count = $this->get_ahr_request_status_count(2);
        echo json_encode($count);
    }

    public function get_ahr_requests_due()
    {
        $count = $this->get_ahr_request_status_count(12);
        echo json_encode($count);
    }

    public function get_ahr_request_details($ahr_id){
        $fields = "additionalHourTypeId, userId, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedId, reason, dateTimeFiled, approvalStatus_ID, datedStatus, requestType, isReleased, dateReleased";
        $where = "additionalHourRequestId = $ahr_id";
        // return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_additional_hour_request");
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_additional_hour_request");      
    }

    // personal count ORIGINAL
    // private function get_ahr_request_status_count_personal($status)
    // {
    //     $user_id = $this->session->userdata('uid');
    //     if ($status == 0) {
    //         $stat = "approvalStatus_ID IN(2,12)";
    //     } else {
    //         $stat = "approvalStatus_ID = $status";
    //     }
    //     $fields = "count(additionalHourRequestId) as ahrCount";
    //     $where = "$stat AND userId = $user_id";
    //     $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_additional_hour_request");
    //     return $record[0]->ahrCount;
    // }

    // TO RESOLVE ISSUE WITH SCHED
    private function get_ahr_request_status_count_personal($status)
    {
        $user_id = $this->session->userdata('uid');
        if ($status == 0) {
            $stat = "ahr.approvalStatus_ID IN(2,12)";
        } else {
            $stat = "ahr.approvalStatus_ID = $status";
        }
        $fields = "count(ahr.additionalHourRequestId) as ahrCount";
        $where = "$stat AND ahr.userId = $user_id AND sched.sched_id = ahr.schedId";
        $table = "tbl_additional_hour_request ahr, tbl_schedule sched";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return $record[0]->ahrCount;
    }

    public function get_ahr_personal_count()
    {
        $stat['request'] = $this->get_ahr_request_status_count_personal(0);
        $stat['pending'] = $this->get_ahr_request_status_count_personal(2);
        $stat['missed'] = $this->get_ahr_request_status_count_personal(12);
        echo json_encode($stat);
    }

    public function get_ahr_approval_count($status, $start, $end)
    {
        // SELECT approval.additionalHourRequestApproval_ID, approval.approvalLevel, ahrRequest.additionalHourRequestId, ahrRequest.dateTimeFiled, ahrRequest.otStartDate, ahrRequest.otStartTime, ahrRequest.otEndDate, ahrRequest.otEndTime, ahrRequest.totalOtHours, ahrRequest.approvalStatus_ID, ahrRequest.reason, ahrRequest.requestType, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahrRequest.userId, users.fname, app.lname, schedType.type, sched_date, dead.deadline FROM tbl_status statuses,tbl_additional_hour_request_approval approval, tbl_additional_hour_type ahrType, tbl_additional_hour_request ahrRequest, tbl_user users, tbl_schedule sched, tbl_schedule_type schedType, tbl_deadline dead WHERE schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = ahrRequest.schedId AND ahrType.additionalHourTypeId = ahrRequest.additionalHourTypeId AND dead.userId = approval.userId AND dead.request_ID = approval.additionalHourRequest_ID AND users.uid = ahrRequest.userId AND statuses.status_ID = ahrRequest.approvalStatus_ID AND ahrRequest.additionalHourRequestId = approval.additionalHourRequest_ID AND approval.userId = $user_id AND approval.approvalStatus_ID = 4 ".$user
        if ($status == 0) {
            $stat = "AND approval.approvalStatus_ID IN(5,12,6)";
        } else {
            $stat = "AND approval.approvalStatus_ID = $status";
        }
        $user_id = $this->session->userdata('uid');
        $fields = "count(ahr.additionalHourRequestId) as ahrCount";
        $where = "sched.sched_id = ahr.schedId AND ahr.additionalHourRequestId = approval.additionalHourRequest_ID $stat AND approval.userId = $user_id AND DATE(sched_date) >= '$start' AND DATE(sched_date) <='$end'";
        $table = "tbl_schedule sched, tbl_additional_hour_request_approval approval, tbl_additional_hour_request ahr";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return $record[0]->ahrCount;
    }

    public function get_approval_status_count()
    {
        $stat_count['record'] = $this->get_ahr_approval_count(0, $this->input->post('from'), $this->input->post('to'));
        $stat_count['approved'] = $this->get_ahr_approval_count(5, $this->input->post('from'), $this->input->post('to'));
        $stat_count['disapproved'] = $this->get_ahr_approval_count(6, $this->input->post('from'), $this->input->post('to'));
        $stat_count['missed'] = $this->get_ahr_approval_count(12, $this->input->post('from'), $this->input->post('to'));
        echo json_encode($stat_count);
    }
// ORIGINAL MONITORING COUNT
    // public function get_monitoring_request_count($status)
    // {
    //     if ($status == 0) {
    //         $stat = "ahr.approvalStatus_ID IN(2,12)";
    //     } else {
    //         $stat = "ahr.approvalStatus_ID = $status";
    //     }
    //     $user_id = $this->session->userdata('uid');
    //     $fields = "count(ahr.additionalHourRequestId) as ahrCount";
    //     $where = "$stat";
    //     $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_additional_hour_request ahr");
    //     return $record[0]->ahrCount;
    // }

    // // COUNT TO RESOLVED SCHED ISSUE
    public function get_monitoring_request_count($status)
    {
        if ($status == 0) {
            $stat = "ahr.approvalStatus_ID IN(2,12)";
        } else {
            $stat = "ahr.approvalStatus_ID = $status";
        }
        $user_id = $this->session->userdata('uid');
        $fields = "count(ahr.additionalHourRequestId) as ahrCount";
        $where = "$stat AND sched.sched_id = ahr.schedId";
        $table  = "tbl_additional_hour_request ahr, tbl_schedule sched";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return $record[0]->ahrCount;
    }

    public function get_monitoring_request_stat_count()
    {
        $stat_count['request'] = $this->get_monitoring_request_count(0);
        $stat_count['pending'] = $this->get_monitoring_request_count(2);
        $stat_count['missed'] = $this->get_monitoring_request_count(12);
        echo json_encode($stat_count);
    }

    public function get_monitoring_record_count($status, $start, $end)
    {
        if ($status == 0) {
            $stat = "ahr.approvalStatus_ID IN(5,6)";
        } else {
            $stat = "ahr.approvalStatus_ID = $status";
        }
        $user_id = $this->session->userdata('uid');
        $fields = "count(ahr.additionalHourRequestId) as ahrCount";
        $where = "sched.sched_id = ahr.schedId AND $stat AND Date(sched_date)>= '$start' AND Date(sched_date)<= '$end'";
        $table = "tbl_schedule sched, tbl_additional_hour_request ahr";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return $record[0]->ahrCount;
    }

    public function get_monitoring_record_stat_count()
    {
        $stat_count['record'] = $this->get_monitoring_record_count(0, $this->input->post('startDate'), $this->input->post('endDate'));
        $stat_count['approved'] = $this->get_monitoring_record_count(5, $this->input->post('startDate'), $this->input->post('endDate'));
        $stat_count['disapproved'] = $this->get_monitoring_record_count(6, $this->input->post('startDate'), $this->input->post('endDate'));
        echo json_encode($stat_count);
    }

    public function get_employee_names()
    {
        $status_id = $this->input->post('status');
        if ($status_id == 0) {
            $status = "AND ahr.approvalStatus_ID IN (2, 12)";
            // $status = "";
        } else {
            $status = "AND ahr.approvalStatus_ID = $status_id";
        }
        $fields = "DISTINCT(ahr.userId) as userId, app.fname, app.lname, app.mname";
        $where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = ahr.userId $status AND sched.sched_id = ahr.schedId";
        $table = "tbl_user users, tbl_additional_hour_request ahr, tbl_schedule sched, tbl_employee emp, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'app.lname', 'ASC');
        echo json_encode($record);
    }

    public function get_requestor_names()
    {
        $status_id = $this->input->post('status');
        $rangeStart = $this->input->post('rangeStart');
        $rangeEnd = $this->input->post('rangeEnd');
        $approver_id = $this->session->userdata('uid');
        if ($this->input->post('selectedTab') == 'request') {
            $status = "AND approval.approvalStatus_ID = 4";
            $fields = "DISTINCT(request.userId), app.fname, app.lname, app.mname";
            $where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = request.userId AND request.additionalHourRequestId = approval.additionalHourRequest_ID AND approval.userId = $approver_id " . $status;
            $table = "tbl_additional_hour_request_approval approval, tbl_additional_hour_request request, tbl_user users, tbl_employee emp, tbl_applicant app";
        } else {
            if ($status_id == 0) {
                $status = "AND approval.approvalStatus_ID NOT IN (2, 4, 7)";
            } else {
                $status = "AND approval.approvalStatus_ID = $status_id";
            }
            // SELECT approval.additionalHourRequestApproval_ID, approval.approvalLevel, ahrRequest.additionalHourRequestId, ahrRequest.dateTimeFiled, ahrRequest.otStartDate, ahrRequest.otStartTime, ahrRequest.otEndDate, ahrRequest.otEndTime, ahrRequest.totalOtHours, ahrRequest.approvalStatus_ID, ahrRequest.reason, ahrRequest.requestType, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahrRequest.userId, users.fname, app.lname, schedType.type, sched_date, approval.approvalStatus_ID as decisionStat FROM tbl_status statuses,tbl_additional_hour_request_approval approval, tbl_additional_hour_type ahrType, tbl_additional_hour_request ahrRequest, tbl_user users, tbl_schedule sched, tbl_schedule_type schedType WHERE schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = ahrRequest.schedId
            $fields = "DISTINCT(request.userId), app.fname, app.lname, app.mname";
            $where = "sched.sched_id = request.schedId AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = request.userId AND request.additionalHourRequestId = approval.additionalHourRequest_ID AND DATE(sched_date) >= '$rangeStart' AND DATE(sched_date) <= '$rangeEnd' AND approval.userId = $approver_id " . $status;
            $table = "tbl_schedule sched, tbl_additional_hour_request_approval approval, tbl_additional_hour_request request, tbl_user users, tbl_employee emp, tbl_applicant app";
        }

        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'app.lname', 'ASC');
        echo json_encode($record);
    }

    public function get_requestor_names_monitoring()
    {
        $status_id = $this->input->post('status');
        $rangeStart = $this->input->post('rangeStart');
        $rangeEnd = $this->input->post('rangeEnd');

        if ($status_id == 0) {
            $status = "AND request.approvalStatus_ID NOT IN (2, 12)";
            $status = "";
        } else {
            $status = "AND request.approvalStatus_ID = $status_id";
        }
        $fields = "DISTINCT(request.userId), app.fname, app.lname, app.mname";
        $where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = request.userId AND request.additionalHourRequestId = approval.additionalHourRequest_ID " . $status;
        $table = "tbl_additional_hour_request_approval approval, tbl_additional_hour_request request, tbl_user users, tbl_employee emp, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'app.lname', 'ASC');
        echo json_encode($record);
    }

    public function get_requestor_names_monitoring_record()
    {
        $status_id = $this->input->post('status');
        $rangeStart = $this->input->post('rangeStart');
        $rangeEnd = $this->input->post('rangeEnd');

        if ($status_id == 0) {
            $status = "request.approvalStatus_ID IN (5, 6)";
            // $status = "";
        } else {
            $status = "request.approvalStatus_ID = $status_id";
        }
        $fields = "DISTINCT(request.userId), app.fname, app.lname, app.mname";
        $where = "sched.sched_id = request.schedId AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = request.userId AND $status AND DATE(sched_date) >= '$rangeStart' AND DATE(sched_date) <= '$rangeEnd'";
        $table = "tbl_schedule sched, tbl_additional_hour_request request, tbl_user users, tbl_employee emp, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'app.lname', 'ASC');
        echo json_encode($record);
    }

    // public function get_personal_pending_ahr_request()
    // {
    //     $status_id = 2;
    //     $record = [];
    //     $count = 0;
    //     $user_id = $this->session->userdata('uid');
    //     $fields = "request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as typesDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedid, sched.sched_date as sched_date, time_start, time_end, reason, dateTimeFiled, statuses.description as statusDescription, approval.approvalLevel";
    //     $where = "times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND sched.sched_id = request.schedId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND approval.approvalStatus_ID = 4 AND approval.additionalHourRequest_ID = request.additionalHourRequestId AND request.userId = $user_id AND request.approvalStatus_ID = $status_id";
    //     $tables = "tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched, tbl_acc_time accTime, tbl_time times, tbl_additional_hour_request_approval approval";
    //     $record = $this->general_model->fetch_specific_vals($fields,$where,$tables);
    //     echo json_encode($record);
    // }

    public function get_monitoring_records()
    {
        $datatable = $this->input->post('datatable');
        $user_id = $this->session->userdata("uid");
        $status_id = $datatable['query']['status'];
        $requestor_id = $datatable['query']['user'];
        $rangeStart = $datatable['query']['rangeStart'];
        $rangeEnd = $datatable['query']['rangeEnd'];
        if ($requestor_id == 0) {
            $user = "";
        } else {
            $user = "AND ahrRequest.userId = $requestor_id";
        }
        if ($status_id == 0) {
            $status = "ahrRequest.approvalStatus_ID IN (5, 6)";
            // $status = "";
        } else {
            $status = "ahrRequest.approvalStatus_ID = $status_id";
        }
        $query['query'] = "SELECT ahrRequest.additionalHourRequestId, ahrRequest.dateTimeFiled, ahrRequest.otStartDate, ahrRequest.otStartTime, ahrRequest.otEndDate, ahrRequest.otEndTime, ahrRequest.totalOtHours, ahrRequest.approvalStatus_ID, ahrRequest.reason, ahrRequest.requestType, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahrRequest.userId, app.pic, app.fname, app.lname, app.mname, app.nameExt, schedType.type, sched_date FROM tbl_status statuses, tbl_additional_hour_type ahrType, tbl_additional_hour_request ahrRequest, tbl_user users, tbl_schedule sched, tbl_schedule_type schedType, tbl_applicant app, tbl_employee emp WHERE schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = ahrRequest.schedId AND ahrType.additionalHourTypeId = ahrRequest.additionalHourTypeId AND app.apid = emp.apid AND emp.emp_id =users.emp_id AND users.uid = ahrRequest.userId AND statuses.status_ID = ahrRequest.approvalStatus_ID AND DATE(sched_date) >= '$rangeStart' AND DATE(sched_date) <= '$rangeEnd' $user AND " . $status;
        if ($datatable['query']['monitoringRecordSearch'] != '') {
            $keyword = $datatable['query']['monitoringRecordSearch'];

            $date_bool = $this->check_if_date($keyword);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "sched_date = '$date'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "ahrRequest.additionalHourRequestId LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "ahrRequest.additionalHourRequestId = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";

            // $query['search']['append'] = " AND (ahrRequest.additionalHourRequestId LIKE '%" . $keyword . "%')";
            // $query['search']['total'] = " AND (ahrRequest.additionalHourRequestId LIKE '%" . $keyword . "%')";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_ahr_record()
    {
        $user_id = $this->session->userdata('uid');
        $status = 1;
        ($status == 1) ? $status_id = 5 : $status_id = 6;
        $date_from = '2018-1-18';
        $date_to = '2018-5-31';
        $fields = "request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as typesDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedid, sched.sched_date as sched_date, time_start, time_end, reason, dateTimeFiled, statuses.description as statusDescription";
        $where = "times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND sched.sched_id = request.schedId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND request.userId = $userId AND request.otStartDate BETWEEN '$date_from' AND '$date_to' AND request.approvalStatus_ID = $status_id";
        $tables = "tbl_status statuses ,tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched, tbl_acc_time accTime, tbl_time times";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        echo json_encode($record);
    }

    public function get_ahr_type()
    {
        $fields = "additionalHourTypeId, description";
        $where = "description IS NOT NULL";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_additional_hour_type");
        return $record;
    }

    public function get_ahr_type_details()
    {
        $record = $this->get_ahr_type();
        echo json_encode($record);
    }

    public function get_server_date_time()
    {
        $date_time = $this->get_current_date_time();
        // var_dump($date_time);
        echo json_encode($date_time);
    }

    public function get_shift()
    {
        $user_id = $this->session->userdata('uid');
        $date = date_format(date_create($this->input->post("scheduleDate")), "Y-m-d");
        // echo $date;
        $fields = "schedType.schedtype_id as schedTypeId, type, sched.sched_id as schedId, users.emp_id empId, users.uid userId, time_start, time_end, bta_id, accTime.acc_id as accountId";
        $where = "schedType.schedtype_id = sched.schedtype_id AND accTime.acc_time_id = sched.acc_time_id AND times.time_id = accTime.time_id AND sched.sched_date = '$date' AND sched.emp_id = users.emp_id AND users.uid = $user_id";
        $tables = "tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users";
        $shift = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        ($shift) ? $record = $shift : $record = 0;
        echo json_encode($record);
    }

    public function get_sched($date, $user_id)
    {
        $fields = "sched.sched_id, sched.schedtype_id";
        $where = "sched.sched_date = '$date' AND sched.emp_id = users.emp_id AND users.uid = $user_id";
        $tables = "tbl_schedule sched, tbl_user users";
        $sched = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        ($sched) ? $record = $sched : $record = 0;
        return $record;
    }

    public function get_form_sched()
    {
        $user_id = $this->session->userdata('uid');
        $date = date_format(date_create($this->input->post("scheduleDate")), "Y-m-d");
        $record = $this->get_sched($date, $user_id);
        echo json_encode($record);
    }

    private function get_dtr_log($empId, $schedId)
    {
        $fields = "dtr_id, entry, log, note, type, sched_id as schedId";
        $where['emp_id'] = $empId;
        $where['sched_id'] = $schedId;
        return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_dtr_logs");
    }

    private function check_attendance_issue($shift, $dtr_log, $issue_type)
    {
        $shift_str = strtotime($shift);
        $dtr_log_str = strtotime($dtr_log);
        if ($issue_type === "late") {
            if ($dtr_log_str > $shift_str) {
                $duration = $this->get_human_time_format($shift_str, $dtr_log_str, 0);
            } else {
                $duration = 0;
            }
        } else if ($issue_type === "ut") {
            if ($dtr_log_str < $shift_str) {
                $duration = $this->get_human_time_format($dtr_log_str, $shift_str, 0);
            } else {
                $duration = 0;
            }
        }
        return $duration;
    }

    private function get_bct_shift_diff($late, $shift_start, $login, $bct)
    {
        $bct_min_to_hr = $bct / 60;
        $shift_start_str = strtotime($shift_start);
        $login_str = strtotime($login);
        $remaining_time = ($shift_start_str - $login_str) / 60;
        if ($late != 0) {
            $actual_bct = 0;
        } else {
            if ($remaining_time < $bct) {
                $actual_bct = 0;
            } else {
                $actual_bct = round($bct_min_to_hr, 2);
            }
        }
        return $actual_bct;
    }

    private function get_sched_unrendered_rest_day($user_id, $date)
    {
        $fields = "sched.sched_id, sched.schedtype_id";
        $where = "sched.emp_id = users.emp_id AND users.uid =$user_id AND sched.sched_date = '$date' AND sched.schedtype_id = 2";
        $table = "tbl_schedule sched, tbl_user users";
        return $record = $this->general_model->fetch_specific_val($fields, $where, $table);

    }

    public function check_if_personal_sched_rest_day()
    {
        $record = $this->get_sched_unrendered_rest_day($this->session->userdata('uid'), $this->input->post('scheduleDate'));
        // $record = $this->get_sched_unrendered_rest_day(177, '2017-11-18');
        if (count($record) == 0) {
            $rest_day['bool'] = 0;
            $rest_day['sched_id'] = 0;
        } else {
            $rest_day['bool'] = 1;
            $rest_day['sched_id'] = $record->sched_id;

        }

        echo json_encode($rest_day);
    }

    public function get_account_shifts()
    {
        $emp_account_details = $this->get_emp_account_details($this->session->userdata('emp_id'));
        $acc_id = $emp_account_details->acc_id;

        $fields = "accTime.acc_time_id, times.time_start, times.time_end";
        $where = "times.time_id = accTime.time_id AND accTime.acc_id = $acc_id AND accTime.note='DTR' ORDER BY STR_TO_DATE(times.time_start, '%l:%i:%s %p')";
        $table = "tbl_acc_time accTime, tbl_time times";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        echo json_encode($record);
    }

    public function get_account_shift_break()
    {
        $acc_time_id = $this->input->post('accShiftId');
        $fields = "sbrk_id,a.bta_id,break_type,hour,min,b.break_id,a.acc_time_id";
        $where = "a.bta_id=b.bta_id AND b.break_id=c.break_id AND b.btime_id=d.btime_id AND isActive=1 and acc_time_id= $acc_time_id";
        $table = "tbl_shift_break a,tbl_break_time_account b,tbl_break c,tbl_break_time d";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        $hours = 0;
        $minutes = 0;
        foreach ($record as $break_val) {
            $hours += $break_val->hour;
            $minutes += $break_val->min;
        }
        $hours_to_min = $hours * 60;
        $minutes += $hours_to_min;
        $break['breakData'] = $record;
        $break['minutes'] = $minutes;
        echo json_encode($break);
    }

    public function check_if_already_filed()
    {
        $user_id = $this->session->userdata('uid');
        $ahr_type = $this->input->post("ahrType");
        $sched_id = $this->input->post("schedId");
        $fields = "additionalHourRequestId";
        $where = "additionalHourTypeId = $ahr_type AND schedId = $sched_id AND userId = $user_id AND approvalStatus_ID != 6";
        $if_exist = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request');
        if ($if_exist) {
            $record['exist'] = 1;
        } else {
            $record['exist'] = 0;
        }
        echo json_encode($record);
    }

    public function check_if_filed_no_sched()
    {
        $user_id = $this->session->userdata('uid');
        $ahr_type = $this->input->post("ahrType");
        $date = $this->input->post("dateSched");
        $fields = "additionalHourRequestId";
        $where = "additionalHourTypeId = $ahr_type AND otStartDate = '$date' AND userId = $user_id AND approvalStatus_ID != 6";
        $if_exist = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request');
        if ($if_exist) {
            $record['exist'] = 1;
        } else {
            $record['exist'] = 0;
        }
        echo json_encode($record);
    }

    public function get_ot_details()
    {
        $shift_start_date_time = new DateTime($this->input->post("scheduleDate") . " " . $this->input->post("shiftStart"));
        $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
        $shift_end_date_time = new DateTime($this->input->post("scheduleDate") . " " . $this->input->post("shiftEnd"));
        $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
        $dated_shift_start = $formatted_shift_start;
        $dated_shift_end = $formatted_shift_end;
        // echo $dated_shift_start;
        // echo $dated_shift_end;
        $dtr_log = $this->get_dtr_log($this->input->post("empId"), $this->input->post("schedId")); // get DTR logs
        $ot_details['break'] = $this->get_allowable_break($this->input->post("accountId"), $this->input->post("schedId")); //get breaks
        $emp_account_details = $this->get_emp_account_details($this->input->post("empId"));

        if (empty($dtr_log) || $dtr_log === null) //if there are no logs
        {
            $ot_details = 0;
        } else {
            foreach ($dtr_log as $dtr_log_val) // separate login and logout logs
            {
                if ($dtr_log_val->entry == "I") {
                    $login_date_time = new DateTime($dtr_log_val->log);
                    $login = $login_date_time->format("Y-m-d H:i");
                } else {
                    $logout_date_time = new DateTime($dtr_log_val->log);
                    $logout = $logout_date_time->format("Y-m-d H:i");
                }
            }
            $new_shifts = $this->check_start_end_shift($dated_shift_start, $dated_shift_end);
            $ot_details['datedShifts'] = $new_shifts;
            if (isset($login)) {
                $ot_details['late'] = $this->check_attendance_issue($new_shifts['start'], $login, "late");
                $ot_details['actual_bct'] = $this->get_bct_shift_diff($ot_details['late'], $new_shifts['start'], $login, $emp_account_details->beforeCallingTime);
            } else {
                $ot_details['late'] = 0;
                $ot_details['actual_bct'] = 0;
            }
            if (isset($logout)) {
                $ot_details['ut'] = $this->check_attendance_issue($new_shifts['end'], $logout, "ut");
            } else {
                $ot_details['ut'] = 0;
            }
            $ot_details['dtrLog'] = $dtr_log;
            $ot_details['account_details'] = $emp_account_details;
        }
        echo json_encode($ot_details);
    }

    public function get_bct_unrendered()
    {
        $emp_account_details = $this->get_emp_account_details($this->input->post("empId"));
        $bct_min_to_hr = $emp_account_details->beforeCallingTime / 60;
        $data['bct'] = round($bct_min_to_hr, 2);
        echo json_encode($data);
    }

    public function get_days_available_to_file()
    {
        $fields = "daysAvailable, changedBy, dateChanged";
        $where = "daysAvailable IS NOT NULL";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request_settings');
        echo json_encode($record);
    }

    public function get_breaks_for_unrendered_filing()
    {
        $user_details = $this->get_user_details($this->session->userdata('uid'));
        $emp_account_details = $this->get_emp_account_details($user_details->emp_id);
        $data['totalBreak'] = $this->get_allowable_break($emp_account_details->acc_id, $this->input->post("schedId"));
        $data['bct'] = $emp_account_details->beforeCallingTime;
        echo json_encode($data);
    }
    public function check_if_rendered_enabled()
    {
        $fields = "acc.ahr_rendered";
        $where = "acc.acc_id = emp.acc_id AND emp_id =" . $this->session->userdata('emp_id');
        $table = "tbl_employee emp, tbl_account acc";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        echo json_encode($record->ahr_rendered);
    }

    // --- AHR SETTINGS
    public function get_ahr_rendered_settings_admin()
    {
        $datatable = $this->input->post('datatable');
        $accountType = "Admin";

        $query['query'] = "SELECT acc_id, acc_name, ahr_rendered FROM tbl_account WHERE status_ID = 1 AND acc_description = '$accountType'";
        if (isset($datatable['query']['assignRenderedAhrSearchAdmin'])) {
            $keyword = $datatable['query']['assignRenderedAhrSearchAdmin'];
            $query['search']['append'] = " AND acc_name LIKE '%" . $keyword . "%'";

            $query['search']['total'] = " AND acc_name LIKE '%" . $keyword . "%'";
        }

        $data = $this->set_datatable_query($datatable, $query);

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_ahr_rendered_settings_agent()
    {
        $datatable = $this->input->post('datatable');
        $accountType = 'Agent';

        $query['query'] = "SELECT acc_id, acc_name, ahr_rendered FROM tbl_account WHERE status_ID = 1 AND acc_description = '$accountType'";
        if (isset($datatable['query']['assignRenderedAhrSearchAgent'])) {
            $keyword = $datatable['query']['assignRenderedAhrSearchAgent'];
            $query['search']['append'] = " AND acc_name LIKE '%" . $keyword . "%'";

            $query['search']['total'] = " AND acc_name LIKE '%" . $keyword . "%'";
        }

        $data = $this->set_datatable_query($datatable, $query);

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    // APPROVAL
    private function get_request_info($request_id)
    {
        $fields = "request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as description, request.userId, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedId, request.requestType, reason, dateTimeFiled, statuses.description as statusDescription";
        $where = "types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND request.additionalHourRequestId = $request_id";
        $tables = "tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types";
        $record = $this->general_model->fetch_specific_val($fields, $where, $tables);
        return $record;
    }

    public function get_request_details()
    {
        $request_id = $this->input->post('ahr_id');
        $fields = "request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as description, request.userId, app.pic, app.fname, app.lname, app.mname, app.nameExt, users.emp_id, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedId, request.requestType as requestType, sched.sched_date as sched_date, schedType.type as schedType, time_start, time_end, reason, dateTimeFiled, statuses.description as statusDescription, empStat.status as empStatus";
        $where = "times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = request.schedId AND empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id =users.emp_id  AND users.uid = request.userId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND request.additionalHourRequestId = $request_id";
        $tables = "tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users, tbl_account accounts, tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app ";
        $record = $this->general_model->fetch_specific_val($fields, $where, $tables);
        echo json_encode($record);
    }

    public function request_details_no_time()
    {
        $request_id = $this->input->post('ahr_id');
        $fields = "request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as description, request.userId, app.pic, app.fname, app.lname, app.mname, app.nameExt, users.emp_id, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedId, request.requestType as requestType, sched.sched_date as sched_date, schedType.type as schedType, reason, dateTimeFiled, statuses.description as statusDescription, empStat.status as empStatus";
        $where = "schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = request.schedId AND empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id =users.emp_id AND users.uid = request.userId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND request.additionalHourRequestId = $request_id";
        $tables = "tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched, tbl_schedule_type schedType, tbl_user users, tbl_account accounts, tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app";
        $record = $this->general_model->fetch_specific_val($fields, $where, $tables);
        echo json_encode($record);
    }

    private function get_ahr_word_request($request_id)
    {
        $fields = "wordRequest_ID, accTimeId";
        $where = "additionalHourRequestId = $request_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_word_request");
    }

    public function get_word_request_details()
    {
        $request_id = $this->input->post('ahr_id');
        $record = $this->get_ahr_word_request($request_id);
        echo json_encode($record);
    }

    public function get_requestor_account_shifts()
    {
        $emp_account_details = $this->get_emp_account_details($this->input->post('emp_id'));
        $acc_id = $emp_account_details->acc_id;

        $fields = "accTime.acc_time_id, times.time_start, times.time_end";
        $where = "times.time_id = accTime.time_id AND accTime.acc_id = $acc_id AND accTime.note='DTR' ORDER BY STR_TO_DATE(times.time_start, '%l:%i:%s %p')";
        $table = "tbl_acc_time accTime, tbl_time times";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        echo json_encode($record);
    }

    public function get_requestor_shift()
    {
        $user_id = $this->input->post('uid');
        $date = date_format(date_create($this->input->post("scheduleDate")), "Y-m-d");
        // echo $date;
        $fields = "schedType.schedtype_id as schedTypeId, type, sched.sched_id as schedId, users.emp_id empId, users.uid userId, time_start, time_end, bta_id, accTime.acc_id as accountId";
        $where = "schedType.schedtype_id = sched.schedtype_id AND accTime.acc_time_id = sched.acc_time_id AND times.time_id = accTime.time_id AND sched.sched_date = '$date' AND sched.emp_id = users.emp_id AND users.uid = $user_id";
        $tables = "tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users";
        $shift = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        ($shift) ? $record = $shift : $record = 0;
        echo json_encode($record);
    }

    public function get_specific_shift_request()
    {
        $acc_time_id = $this->input->post('accTimeId');
        $fields = "accTime.acc_time_id, times.time_start, times.time_end";
        $where = "times.time_id = accTime.time_id AND accTime.acc_time_id = $acc_time_id";
        $tables = "tbl_acc_time accTime, tbl_time times";
        $shift = $this->general_model->fetch_specific_val($fields, $where, $tables);
        echo json_encode($shift);
    }

    public function check_if_request_still_exist()
    {
        $request_id = $this->input->post('ahrId');
        $fields = "additionalHourRequestId";
        $where = "additionalHourRequestId = $request_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request');
        if (count($record) > 0) {
            $exist = 1;
        } else {
            $exist = 0;
        }
        echo json_encode($exist);
    }

    private function get_specific_ahr($request_id)
    {
        $fields = "otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours";
        $where = "additionalHourRequestId = $request_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request');
    }

    private function get_ahr_details_for_word($request_id)
    {
        $fields = "schedId, requestType";
        $where = "additionalHourRequestId = $request_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request');
    }

    private function get_requestor_details($request_id)
    {
        $fields = "users.uid as userId, app.fname, app.lname, app.mname, app.nameExt, app.gender";
        $where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = request.userId AND additionalHourRequestId = $request_id";
        $tables = "tbl_user users, tbl_additional_hour_request request, tbl_employee emp, tbl_applicant app";
        return $requestor = $this->general_model->fetch_specific_val($fields, $where, $tables);
    }

    public function get_approvers_details($request_id)
    {
        $fields = "a.uid as userId, d.fname, d.lname";
        $where = "a.uid = b.userId AND approvalStatus_ID IN (5, 6, 4, 12) AND b.additionalHourRequest_ID = $request_id AND a.emp_id = c.emp_id AND c.apid=d.apid";
        $tables = "tbl_user a, tbl_additional_hour_request_approval b, tbl_employee c,tbl_applicant d";
        $approvers = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        return $approvers;
    }

    private function get_max_approval_level($request_id)
    {
        $fields = "MAX(approvalLevel) as approvalLevel";
        $where = "additionalHourRequest_ID = $request_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request_approval');
    }

    private function get_current_approval_level($ahr_approval_id)
    {
        $fields = "approvalLevel";
        $where = "additionalHourRequestApproval_ID = $ahr_approval_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request_approval');
    }

    private function get_next_approver($request_id, $approval_level)
    {
        $fields = "additionalHourRequestApproval_ID, userId";
        $where = "additionalHourRequest_ID = $request_id AND approvalLevel = $approval_level";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request_approval');
    }

    private function get_preceeding_approvers($current_approval_level, $request_id)
    {
        $fields = "additionalHourRequestApproval_ID, userId";
        $where = "additionalHourRequest_ID = $request_id AND approvalLevel < $current_approval_level";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_additional_hour_request_approval');
    }

    // UPDATE -----------------------------------------------------------------------------------

    // AHR SETTINGS
    public function set_available_days_to_file()
    {
        $data['changedBy'] = $this->session->userdata('uid');
        $data['daysAvailable'] = $this->input->post('daysAvailable');
        $where = "daysAvailable IS NOT NULL";
        $update_stat = $this->general_model->update_vals($data, $where, 'tbl_additional_hour_request_settings');
        echo json_encode($update_stat);
    }

    public function set_rendered_ahr_account()
    {
        $data['changedBy'] = $this->session->userdata('uid');
        if ($this->input->post('renderedAhr') === 'true') {
            $render_ahr_val = 1;
        } else {
            $render_ahr_val = 0;
        }
        $data['ahr_rendered'] = $render_ahr_val;
        $acc_id = $this->input->post('accountId');
        $where = "acc_id = $acc_id";
        $update_stat = $this->general_model->update_vals($data, $where, 'tbl_account');
        echo json_encode($update_stat);
    }

    // COMPLEX (APPROVAL)

    // public function set_approvers_ahr($ahr_id, $date_filed, $module_id, $approval_type)
    public function set_approvers_ahr()
    {
        $date_filed = '2018-11-21';
        $user_id = 14;
        $module_id = 1;
        $approval_type = 1; // 1- Sequential, 2 - Parallel
        $ahr_id = 2;
        $approvers = $this->set_approvers($date_filed, $user_id, $module_id, 'additionalHourRequest_ID', $ahr_id);
        $this->set_deadline($approvers, $module_id, $ahr_id, 'additionalHourRequest_ID');
        // var_dump($approvers);
        // for($loop = 0; $loop<count($approvers); $loop++){
        //     unset($approvers[$loop]['approvalStatus_ID']);
        //     unset($approvers[$loop]['additionalHourRequest_ID']);
        //     $approvers[$loop]["module_ID"] = $module_id;
        //     $approvers[$loop]["request_ID"] = 1;
        //     $approvers[$loop]["status_ID"] = 2;
        // }
        // var_dump($approvers);

        // $approval_type = 2; // 1- Sequential, 2 - Parallel
        // if($approval_type === 1)
        // {
        //      $recipients[] = $approvers[0];
        // }
        // else
        // {
        //      $recipients = $approvers;
        // }
        // // var_dump($recipients);
        // // $recipient[] = $approvers[0];
        // $this->send_notif_test(1, $recipients);
        // $insert_status = $this->general_model->batch_insert($approvers, 'tbl_additional_hour_request_approval');
        // var_dump($approvers);
    }

    // APPROVAL PROCESS

    private function set_next_approver_as_current($ahr_approval_id)
    {
        $data['approvalStatus_ID'] = 4;
        $where = "additionalHourRequestApproval_ID = $ahr_approval_id";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_additional_hour_request_approval');
    }

    private function set_proceeding_approvers_cancelled($request_id, $current_approver_id, $current_approval_level)
    {
        $data['approvalStatus_ID'] = 7;
        $where = "additionalHourRequest_ID = $request_id AND userId != $current_approver_id AND approvalLevel > $current_approval_level";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_additional_hour_request_approval');
    }

    private function set_proceeding_approvers_deadline_stat($request_id, $current_approver_id, $current_approval_level)
    {
        $data['status_ID'] = 7;
        $where = "module_ID = 1 AND request_ID = $request_id AND userId != $current_approver_id AND approvalLevel > $current_approval_level";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_deadline');
    }

    private function set_sched_rendered_word($sched_id, $schedtype_id)
    {
        $data['schedtype_id'] = $schedtype_id;
        $where = "sched_id = $sched_id";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_schedule');
    }

    private function set_sched_unrendered_word($sched_id, $request_id, $schedtype_id)
    {
        $word_request = $this->get_ahr_word_request($request_id);
        $data['schedtype_id'] = $schedtype_id;
        $data['acc_time_id'] = $word_request->accTimeId;
        $where = "sched_id = $sched_id";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_schedule');
    }

    private function set_ahr_status($status, $request_id)
    {
        $data['approvalStatus_ID'] = $status;
        $where = "additionalHourRequestId = $request_id";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_additional_hour_request');
    }

    private function proceed_approval($ahr_approval_id, $request_id, $ahr_type, $decision, $current_approver_id)
    {
        $request_details = $this->get_ahr_request_details($this->input->post('ahrId'));
        $requestor = $this->get_requestor_details($request_id);
        $requestors[0] = ['userId' => $requestor->userId];
        $max_approval_level = $this->get_max_approval_level($request_id);
        $current_approval_level = $this->get_current_approval_level($ahr_approval_id);
        $ahr_type_text = "";
        $notif_mssg = "";
        if ($ahr_type == 3) {
            $ahr_type_text = 'the WORD';
            $ahr_type_text_alone = 'WORD';
        } else if ($ahr_type == 2) {
            $ahr_type_text = 'the PRE shift additional hour';
            $ahr_type_text_alone = 'PRE shift additional hour';
        } else if ($ahr_type == 1) {
            $ahr_type_text = 'the AFTER shift additional hour';
            $ahr_type_text_alone = 'AFTER shift additional hour';
        }
        if ($decision == 5) {
            $approvalStat = 'APPROVED';
        } else {
            $approvalStat = 'DISAPPROVED';
        }

        if ($decision == 6) {
            //system notification
            if ($current_approval_level->approvalLevel < $max_approval_level->approvalLevel) {
                $proceed_appr['set_proceeding_cancelled_stat'] = $this->set_proceeding_approvers_cancelled($request_id, $current_approver_id, $current_approval_level->approvalLevel);
                $proceed_appr['set_proceeding_deadline_stat'] = $this->set_proceeding_approvers_deadline_stat($request_id, $current_approver_id, $current_approval_level->approvalLevel);
            }
            $notif_mssg = "Sorry, your $ahr_type_text_alone request was not approved.<br><small><b>AHR-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "additional_hour/request";
            $proceed_appr['set_notif_details'] = $this->set_system_notif($notif_mssg, $link, $requestors);
            // request notif
            $notif_mssg = "disapproved your $ahr_type_text_alone request.<br><small><b>AHR-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "additional_hour/request";
            $proceed_appr['finalized'] = 1;
            //set ahr status
            $proceed_appr['set_ahr_status'] = $this->set_ahr_status($decision, $request_id);
            $proceed_appr['set_sched'] = $this->update_sched_lock($request_details->schedId, "Excel", "", 0);
            //set word
            if ($ahr_type == 3) {
                $ahr_details_word = $this->get_ahr_details_for_word($request_id);
                if ($ahr_details_word->requestType == 'rendered') {
                    $proceed_appr['set_sched_to_word'] = $this->set_sched_rendered_word($ahr_details_word->schedId, 1);
                } else {
                    $proceed_appr['set_sched_to_word'] = $this->set_sched_unrendered_word($ahr_details_word->schedId, $request_id, 1);
                }
            }
        } else if ($decision == 5) {
            if ($current_approval_level->approvalLevel == $max_approval_level->approvalLevel) {
                //system notification
                $notif_mssg = "Your $ahr_type_text_alone request was successfully approved.<br><small><b>AHR-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
                $link = "additional_hour/request";
                $proceed_appr['set_notif_details'] = $this->set_system_notif($notif_mssg, $link, $requestors);
                // request notification
                $notif_mssg = "approved your $ahr_type_text_alone request.<br><small><b>AHR-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
                $link = "additional_hour/request";
                $proceed_appr['finalized'] = 1;
                //set ahr status
                $proceed_appr['set_ahr_status'] = $this->set_ahr_status($decision, $request_id);
                $proceed_appr['set_sched'] = $this->update_sched_lock($request_details->schedId, "ahr", 1, 14);
                //set word
                if ($ahr_type == 3) {
                    $ahr_details_word = $this->get_ahr_details_for_word($request_id);
                    if ($ahr_details_word->requestType == 'rendered') {
                        $proceed_appr['set_sched_to_word'] = $this->set_sched_rendered_word($ahr_details_word->schedId, 4);
                    } else {
                        $proceed_appr['set_sched_to_word'] = $this->set_sched_unrendered_word($ahr_details_word->schedId, $request_id, 4);
                    }
                }
            } else {
                $next_approval_level = $current_approval_level->approvalLevel + 1;
                $next_approver = $this->get_next_approver($request_id, $next_approval_level);
                $approvers[0] = ['userId' => $next_approver->userId];
                // SET NEXT APPROVER STATUS AS CURRENT
                $proceed_appr['set_next_current_stat'] = $this->set_next_approver_as_current($next_approver->additionalHourRequestApproval_ID);
                // SET NOTIFICATION TO NEXT APPROVER
                $approval_level_ordinal = $this->ordinal($next_approval_level);
                $notif_mssg = "forwarded to you $ahr_type_text request of <strong>" . $requestor->fname . " " . $requestor->lname . "</strong> for $approval_level_ordinal Approval.<br><small><b>AHR-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
                $link = "additional_hour/approval_ahr";
                $proceed_appr['set_notif_details'] = $this->set_notif($this->session->userdata('uid'), $notif_mssg, $link, $approvers);
                // SET NOTIFICATION TO REQUESTOR
                $notif_mssg = "approved your $ahr_type_text_alone request.<br><small><b>AHR-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
                $link = "additional_hour/request";
                $proceed_appr['finalized'] = 0;
            }
        }
        $data['approvalStatus_ID'] = $this->input->post('approval');
        $data['remarks'] = $this->input->post('remarks');
        $where = "additionalHourRequestApproval_ID = $ahr_approval_id";
        $approval['set_current_approver_stat'] = $this->general_model->update_vals($data, $where, 'tbl_additional_hour_request_approval');
        // Notify Proceeding
        if ($current_approval_level->approvalLevel > 1) {
            $preceeding_approvers = $this->get_preceeding_approvers($current_approval_level->approvalLevel, $request_id);
            for ($loop = 0; $loop < count($preceeding_approvers); $loop++) {
                $recipient[$loop] = [
                    "userId" => $preceeding_approvers[$loop]->userId,
                ];
            }
            $notif_mssg_proceeding = $approvalStat . " $ahr_type_text request of <strong>" . $requestor->fname . " " . $requestor->lname . "</strong>.<br><small><b>AHR-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
            $link_proceeding = "additional_hour/approval_ahr";
            $proceed_appr['set_notif_details_preceeding'] = $this->set_notif_preceeding($this->session->userdata('uid'), $notif_mssg_proceeding, $link_proceeding, $recipient);
            $proceed_appr['preceeding'] = 1;
        } else {
            $proceed_appr['preceeding'] = 0;
        }
        $proceed_appr['set_notif_details_requestor'] = $this->set_notif($this->session->userdata('uid'), $notif_mssg, $link, $requestors);
        return $proceed_appr;
    }

    public function set_aproval_decision_no_change()
    {
        $ahr_approval_id = $this->input->post('approvalId');
        $ahr_type = $this->input->post('ahrType');
        $data['approvalStatus_ID'] = $this->input->post('approval');
        $data['remarks'] = $this->input->post('remarks');
        $where = "additionalHourRequestApproval_ID = $ahr_approval_id";
        $approval['set_current_approver_stat'] = $this->general_model->update_vals($data, $where, 'tbl_additional_hour_request_approval');
        $approval['update_deadline'] = $this->update_ahr_deadline_stat($this->input->post('ahrId'));
        $approval['proceed_approval'] = $this->proceed_approval($ahr_approval_id, $this->input->post('ahrId'), $ahr_type, $this->input->post('approval'), $this->session->userdata('uid'));
        echo json_encode($approval);
    }

    private function back_up_ahr($request_id, $ahr_approval_id)
    {
        $ahr_details = $this->get_specific_ahr($request_id);
        $data['additionalHourRequest_ID'] = $request_id;
        $data['additionalHourRequestApproval_ID'] = $ahr_approval_id;
        $data['otStartDate'] = $ahr_details->otStartDate;
        $data['otStartTime'] = $ahr_details->otStartTime;
        $data['otEndDate'] = $ahr_details->otEndDate;
        $data['otEndTime'] = $ahr_details->otEndTime;
        $data['previousOtHours'] = $ahr_details->totalOtHours;
        return $insert_stat = $this->general_model->insert_vals($data, 'tbl_additional_hour_request_change');
    }

    private function change_ahr($request, $request_id)
    {
        $where = "additionalHourRequestId = $request_id";
        return $update_stat = $this->general_model->update_vals($request, $where, 'tbl_additional_hour_request');
    }

    private function change_word_request($request_id, $acc_time_id)
    {
        $data['accTimeId'] = $acc_time_id;
        $where = "additionalHourRequestId = $request_id";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_word_request');
    }

    private function update_ahr_deadline_stat($request_id)
    {
        $data['status_ID'] = 3;
        $user_id = $this->session->userdata('uid');
        $where = "module_ID = 1 AND request_ID = $request_id AND userId = $user_id";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_deadline');
    }

    public function set_approval_decision_with_change()
    {
        $ahr_type = $this->input->post('ahrType');
        $ahr_approval_id = $this->input->post('approvalId');
        $ahr_details = $this->get_specific_ahr($this->input->post('ahrId'));
        //back up
        $ahr_approval['modify_ahr'] = $this->back_up_ahr($this->input->post('ahrId'), $ahr_approval_id);

        if ($ahr_type == 1) {
            $request['otStartDate'] = $ahr_details->otStartDate;
            $request['otStartTime'] = $ahr_details->otStartTime;
            $request['otEndDate'] = $this->input->post('dateEnd');
            $request['otEndTime'] = $this->input->post('timeEnd');
        } else if ($ahr_type == 2) {
            $request['otStartDate'] = $this->input->post('dateStart');
            $request['otStartTime'] = $this->input->post('timeStart');
            $request['otEndDate'] = $ahr_details->otEndDate;
            $request['otEndTime'] = $ahr_details->otEndTime;
        } else if ($ahr_type == 3) {
            $request['otStartDate'] = $this->input->post('dateStart');
            $request['otStartTime'] = $this->input->post('timeStart');
            $request['otEndDate'] = $this->input->post('dateEnd');
            $request['otEndTime'] = $this->input->post('timeEnd');
            $ahr_approval['modify_word_request'] = $this->change_word_request($this->input->post('ahrId'), $this->input->post('accTimeId'));
        }
        $request['totalOtHours'] = $this->input->post('otHours');
        // update change
        $ahr_approval['modify_ahr'] = $this->change_ahr($request, $this->input->post('ahrId'));
        $ahr_approval['update_deadline'] = $this->update_ahr_deadline_stat($this->input->post('ahrId'));

        $approval['approvalStatus_ID'] = $this->input->post('approval');
        $approval['remarks'] = $this->input->post('remarks');

        // proceed approval (notif, status)
        $where = "additionalHourRequestApproval_ID = $ahr_approval_id";
        $ahr_approval['set_current_approver_stat'] = $this->general_model->update_vals($approval, $where, 'tbl_additional_hour_request_approval');
        $ahr_approval['proceed_approval'] = $this->proceed_approval($this->input->post('approvalId'), $this->input->post('ahrId'), $ahr_type, 5, $this->session->userdata('uid'));
        echo json_encode($ahr_approval);
    }

    // DEFAULT APPROVER

    private function hr_approval_notif()
    {
        //notify preceding approvers
        //notify requestor
        //notify default approver
    }

    private function get_ahr_details($request_id)
    {
        $fields = "additionalHourRequestId, additionalHourTypeId, userId";
        $where = "additionalHourRequestId = $request_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_additional_hour_request');
    }

    private function set_hr_approver($request_id, $status_id, $remarks)
    {
        $max_approval_level = $this->get_max_approval_level($request_id);
        $data['additionalHourRequest_ID'] = $request_id;
        $data['approvalStatus_ID'] = $status_id;
        $data['approvalLevel'] = $max_approval_level->approvalLevel + 1;
        $data['userId'] = $this->session->userdata('uid');
        $data['remarks'] = $remarks;
        return $record = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_additional_hour_request_approval');
    }

    private function notify_default_preceding_approvers($approver_id, $request_id, $status)
    {
        $ahr_details = $this->get_ahr_details($request_id);
        $requestor = $this->get_user_details_due($ahr_details->userId);
        $approver = $this->get_user_details_due($approver_id);
        $max_approval_level = $this->get_max_approval_level($request_id);
        $preceeding_approvers = $this->get_preceding_approvers_due($max_approval_level->approvalLevel, $request_id, 1);
        for ($loop = 0; $loop < count($preceeding_approvers); $loop++) {
            $recipients[$loop] = [
                "userId" => $preceeding_approvers[$loop]->userId,
            ];
        }
        if ($status = 5) {
            $stat = 'Approved';
        } else if ($status = 6) {
            $stat = 'Disapproved';
        }
        $notif_mssg = "<i class='fa fa-info-circle'></i> " . $approver->fname . " $approver->lname (HR Default Approver) " . $stat . " the Additional Hour Request of " . $requestor->fname . " " . $requestor->lname . " .<br><small>AHR-ID: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
        return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, 'additional_hour/approval_ahr', $recipients);
    }

    private function notify_default_requestor($approver_id, $request_id, $status)
    {
        $ahr_details = $this->get_ahr_details($request_id);
        $requestor = $this->get_user_details_due($ahr_details->userId);
        $approver = $this->get_user_details_due($approver_id);
        $recipients[0] = ["userId" => $ahr_details->userId];

        if ($status = 5) {
            $stat = 'Approved';
        } else if ($status = 6) {
            $stat = 'Disapproved';
        }
        $notif_mssg = "<i class='fa fa-info-circle'></i> " . $approver->fname . " $approver->lname (HR Default Approver) " . $stat . " your Additional Hour Request.<br><small>AHR-ID: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
        return $notif_stat = $this->set_system_notif($notif_mssg, 'additional_hour/request', $recipients);
    }

    public function set_aproval_decision_no_change_monitor()
    {
        $request_details = $this->get_ahr_request_details($this->input->post('ahrId'));
        if($this->input->post('approval') == 5){
            $delete_stat['set_sched'] = $this->update_sched_lock($request_details->schedId, "ahr", 1, 14);
        }else if ($this->input->post('approval') == 6){
            $delete_stat['set_sched'] = $this->update_sched_lock($request_details->schedId, "Excel", "", 0);
        }
        $ahr_approval['set_current_approver_stat'] = $this->set_hr_approver($this->input->post('ahrId'), $this->input->post('approval'), $this->input->post('remarks'));
        $request['approvalStatus_ID'] = $this->input->post('approval');
        $ahr_approval['modify_ahr'] = $this->change_ahr($request, $this->input->post('ahrId'));
        $ahr_type = $this->input->post('ahrType');
        $ahr_approval['notify_preceding'] = $this->notify_default_preceding_approvers($this->session->userdata('uid'), $this->input->post('ahrId'), $this->input->post('approval'));
        $ahr_approval['notify_requestor'] = $this->notify_default_requestor($this->session->userdata('uid'), $this->input->post('ahrId'), $this->input->post('approval'));
        echo json_encode($ahr_approval);
    }

    public function set_approval_decision_with_change_monitor()
    {
        $request_details = $this->get_ahr_request_details($this->input->post('ahrId'));
        if($this->input->post('approval') == 5){
            $delete_stat['set_sched'] = $this->update_sched_lock($request_details->schedId, "ahr", 1, 14);
        }else if ($this->input->post('approval') == 6){
            $delete_stat['set_sched'] = $this->update_sched_lock($request_details->schedId, "Excel", "", 0);
        }
        $ahr_approval['set_current_approver_stat'] = $this->set_hr_approver($this->input->post('ahrId'), $this->input->post('approval'), $this->input->post('remarks'));

        $ahr_type = $this->input->post('ahrType');
        $ahr_details = $this->get_specific_ahr($this->input->post('ahrId'));
        //back up
        $ahr_approval['modify_ahr'] = $this->back_up_ahr($this->input->post('ahrId'), $ahr_approval['set_current_approver_stat']);

        if ($ahr_type == 1) {
            $request['otStartDate'] = $ahr_details->otStartDate;
            $request['otStartTime'] = $ahr_details->otStartTime;
            $request['otEndDate'] = $this->input->post('dateEnd');
            $request['otEndTime'] = $this->input->post('timeEnd');
        } else if ($ahr_type == 2) {

            $request['otStartDate'] = $this->input->post('dateStart');
            $request['otStartTime'] = $this->input->post('timeStart');
            $request['otEndDate'] = $ahr_details->otEndDate;
            $request['otEndTime'] = $ahr_details->otEndTime;
        } else if ($ahr_type == 3) {
            $request['otStartDate'] = $this->input->post('dateStart');
            $request['otStartTime'] = $this->input->post('timeStart');
            $request['otEndDate'] = $this->input->post('dateEnd');
            $request['otEndTime'] = $this->input->post('timeEnd');
            $ahr_approval['modify_word_request'] = $this->change_word_request($this->input->post('ahrId'), $this->input->post('accTimeId'));
        }
        $request['totalOtHours'] = $this->input->post('otHours');
        $request['approvalStatus_ID'] = $this->input->post('approval');
        // update change
        $ahr_approval['modify_ahr'] = $this->change_ahr($request, $this->input->post('ahrId'));
        $ahr_approval['notify_preceding'] = $this->notify_default_preceding_approvers($this->session->userdata('uid'), $this->input->post('ahrId'), $this->input->post('approval'));
        $ahr_approval['notify_requestor'] = $this->notify_default_requestor($this->session->userdata('uid'), $this->input->post('ahrId'), $this->input->post('approval'));
        // // $ahr_approval['proceed_approval'] = $this->proceed_approval($this->input->post('approvalId'), $this->input->post('ahrId'), $ahr_type, 5, $this->session->userdata('uid'));

        // // proceed approval (notif, status)
        echo json_encode($ahr_approval);
    }

    //REMOVE REQUEST
    private function check_approval_ongoing($ahr_id)
    {
        $fields = "additionalHourRequestApproval_ID";
        $where = "additionalHourRequest_ID = $ahr_id AND approvalStatus_ID NOT IN (2,4, 12)";
        $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_additional_hour_request_approval');
        if (count($record) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    private function remove_ahr_approvers($ahr_id)
    {
        $where['additionalHourRequest_ID'] = $ahr_id;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_additional_hour_request_approval');
    }

    private function remove_ahr($ahr_id)
    {
        $where['additionalHourRequestId'] = $ahr_id;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_additional_hour_request');
    }

    private function remove_word_request($ahr_id)
    {
        $where['additionalHourRequestId'] = $ahr_id;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_word_request');
    }

    public function check_approval_stat()
    {
        $approval_ongoing = $this->check_approval_ongoing($this->input->post('ahrId'));
        echo json_encode($approval_ongoing);
    }

    private function notify_approvers_remove($request_id)
    {
        $approvers = $this->get_approvers_details($request_id);
        $requestor = $this->get_requestor_details($request_id);
        $request_info = $this->get_request_info($request_id);
        $pro_noun = "";
        $recipient = [];
        for ($loop = 0; $loop < count($approvers); $loop++) {
            $recipient[$loop] = [
                "userId" => $approvers[$loop]->userId,
            ];
        }
        if ($requestor->gender == 'Male') {
            $pro_noun = "his";
        } else if ($requestor->gender == 'Female') {
            $pro_noun = "her";
        }
        $notif_mssg = "cancelled $pro_noun $request_info->description shift Additional hour request.<br><small><b>AHR-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
        $link = "additional_hour/approval_ahr";
        return $notif = $this->set_notif_preceeding($this->session->userdata('uid'), $notif_mssg, $link, $recipient);
    }

    public function remove_request()
    {
        $approval_ongoing = $this->check_approval_ongoing($this->input->post('ahrId'));
        if ($approval_ongoing) {
            $delete_stat['approval_ongoing'] = $approval_ongoing;
        } else {
            $delete_stat['approval_ongoing'] = $approval_ongoing;
            $request_details = $this->get_ahr_request_details($this->input->post('ahrId'));
            $delete_stat['set_sched'] = $this->update_sched_lock($request_details->schedId, "Excel", NULL, 0);
            $delete_stat['notify_approvers'] = $this->notify_approvers_remove($this->input->post('ahrId'));
            $delete_stat['delete_deadline_stat'] = $this->remove_deadlines(1, $this->input->post('ahrId'));
            $delete_stat['delete_approvers_stat'] = $this->remove_ahr_approvers($this->input->post('ahrId'));
            $delete_stat['delete_word_request'] = $this->remove_word_request($this->input->post('ahrId'));
            $delete_stat['delete_ahr_stat'] = $this->remove_ahr($this->input->post('ahrId'));
        }
        echo json_encode($delete_stat);
    }

    // REPORTING

    private function get_current_months($currentYear)
    {
        $fields = "DISTINCT MONTH(DATE_FORMAT(ahr.otStartDate, '%Y-%m-01')) months";
        $where = "account.acc_id = emp.acc_id AND emp.emp_id = users.emp_id AND users.uid = ahr.userId AND DATE_FORMAT(ahr.otStartDate, '%Y') = '$currentYear' GROUP BY MONTH(DATE_FORMAT(ahr.otStartDate, '%Y-%m-01')), account.acc_description";
        $tables = "tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_account account";
        $distinct_months = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        return $distinct_months;
    }

    private function get_emp_request_count($year, $month, $quantity_type)
    {
        if ($quantity_type == 1) {
            $fields = "COUNT(ahr.additionalHourRequestId) requestCount, account.acc_description";
        } else {
            $fields = "SUM(ahr.totalOtHours) requestCount, account.acc_description";
        }
        $where = "account.acc_id = emp.acc_id AND emp.emp_id = users.emp_id AND users.uid = ahr.userId AND ahr.approvalStatus_ID = 5 AND DATE_FORMAT(ahr.otStartDate, '%Y') = '$year' AND MONTH(DATE_FORMAT(ahr.otStartDate, '%Y-%m-01')) = '$month' GROUP BY account.acc_description";
        $tables = "tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_account account";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        if (count($record) == 0) {
            $ahr_monthly_count['Agent'] = 0;
            $ahr_monthly_count['Admin'] = 0;
        } else if (count($record) == 1) {
            foreach ($record as $rows) {
                if ($rows->acc_description == 'Agent') {
                    $ahr_monthly_count['Agent'] = floatval($rows->requestCount);
                    $ahr_monthly_count['Admin'] = 0;
                } else {
                    $ahr_monthly_count['Agent'] = 0;
                    $ahr_monthly_count['Admin'] = floatval($rows->requestCount);
                }
            }
        } else {
            foreach ($record as $rows) {
                if ($rows->acc_description == 'Agent') {
                    $ahr_monthly_count['Agent'] = floatval($rows->requestCount);
                } else {
                    $ahr_monthly_count['Admin'] = floatval($rows->requestCount);
                }
            }
        }
        return $ahr_monthly_count;
    }

    private function get_emp_request_count_per_type($year, $month, $quantity_type, $ahr_type)
    {
        if ($quantity_type == 1) {
            $fields = "COUNT(ahr.additionalHourRequestId) requestCount, account.acc_description";
        } else {
            $fields = "SUM(ahr.totalOtHours) requestCount, account.acc_description";
        }
        $where = "account.acc_id = emp.acc_id AND emp.emp_id = users.emp_id AND users.uid = ahr.userId AND ahr.additionalHourTypeId = $ahr_type AND ahr.approvalStatus_ID = 5 AND DATE_FORMAT(ahr.otStartDate, '%Y') = '$year' AND MONTH(DATE_FORMAT(ahr.otStartDate, '%Y-%m-01')) = '$month' GROUP BY account.acc_description";
        $tables = "tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_account account";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        if (count($record) == 0) {
            $ahr_monthly_count['Agent'] = 0;
            $ahr_monthly_count['Admin'] = 0;
        } else if (count($record) == 1) {
            foreach ($record as $rows) {
                if ($rows->acc_description == 'Agent') {
                    $ahr_monthly_count['Agent'] = floatval($rows->requestCount);
                    $ahr_monthly_count['Admin'] = 0;

                } else {
                    $ahr_monthly_count['Agent'] = 0;
                    $ahr_monthly_count['Admin'] = floatval($rows->requestCount);
                }
            }
        } else {
            foreach ($record as $rows) {
                if ($rows->acc_description == 'Agent') {
                    $ahr_monthly_count['Agent'] = floatval($rows->requestCount);
                } else {
                    $ahr_monthly_count['Admin'] = floatval($rows->requestCount);
                }
            }
        }
        return $ahr_monthly_count;
    }

    public function get_passed_months($date_selected)
    {
        $OrigDateTime = new DateTime($date_selected);
        $dateTime = new DateTime($date_selected);
        $prev_year = $dateTime->modify('-1 year');
        $count = 0;
        while ($OrigDateTime->format('Y') != $prev_year->format('Y')) {
            $month[$count] = $OrigDateTime->format('m');
            $OrigDateTime->modify('-1 month');
            $count++;
        };
        $monthCount = count($month) - 1;
        $count_2 = 0;
        for ($loop = $monthCount; $loop > 0; $loop--) {
            $asc_months[$count_2] = $month[$loop];
            $count_2++;
        }
        return $asc_months;
    }

    // public function get_general_chart_data()
    // {
    //     $date_selected = $this->input->post("dateSelected");
    //     $current_months = $this->get_passed_months($date_selected);
    //     $year_selected = new DateTime($date_selected);
    //     $count = 0;
    //     $admin = [];
    //     $agent = [];
    //     $months = [];
    //     for($loop1 = 0; $loop1 < count($current_months); $loop1++){
    //         $ahr_counts[$loop1] = $this->get_emp_request_count($year_selected->format('Y'), $current_months[$loop1], $this->input->post("quantityType"));
    //         $date_obj = DateTime::createFromFormat('!m', $current_months[$loop1]);
    //         $months[$loop1] = $date_obj->format('M');
    //         $count++;
    //     }
    //     for($loop = 0; $loop < count($ahr_counts); $loop++){
    //         $agent[] =  $ahr_counts[$loop]['Agent'];
    //         $admin[] =  $ahr_counts[$loop]['Admin'];
    //     }
    //     $ahr_data['months'] = $months;
    //     $ahr_data['agent'] = $agent;
    //     $ahr_data['admin'] = $admin;
    //     echo json_encode($ahr_data);
    // }

    public function get_gen_chart_drilldown()
    {
        $monthStart = $this->input->post('monthStart');
        $monthEnd = $this->input->post('monthEnd');
        $emp_type = $this->input->post('empType');
        $unit = $this->input->post('unit');
        $name = $this->input->post('name');
        $month = $this->input->post('month');
        $ahrType = $this->input->post('ahrType');
        $count = 0;
        if ($emp_type == 'admin') {
            // echo "admin";
            $accountCount = $this->get_ahr_count_admin_acc($monthStart, $monthEnd, $ahrType, 5, $unit);
        } else if ($emp_type == 'agent') {
            // echo "agent";
            $accountCount = $this->get_ahr_count_agent_acc($monthStart, $monthEnd, $ahrType, 5, $unit);
        }
        foreach ($accountCount as $rows) {
            $category[$count] = $rows->name;
            $data[$count] = [
                'name' => $rows->name,
                'y' => floatval($rows->y),
            ];
            $count++;
        }
        $drilldown[$name] = [
            "name" => $name,
            "data" => $data,
        ];
        $report_details['drilldown'] = $drilldown;
        $report_details['category'] = $category;
        echo json_encode($report_details);
    }

    public function get_general_chart_data()
    {
        $date_selected = $this->input->post("dateSelected");
        $year_selected = new DateTime($date_selected);
        $current_months = $this->get_passed_months($date_selected);
        $record = $this->get_ahr_type();
        $count = 0;
        for ($loop1 = 0; $loop1 < count($current_months); $loop1++) {
            $date_obj = DateTime::createFromFormat('!m', $current_months[$loop1]);
            $months[$loop1] = $date_obj->format('M');
        }
        foreach ($record as $vals) {
            for ($loop1 = 0; $loop1 < count($current_months); $loop1++) {
                $ahr_counts = $this->get_emp_request_count_per_type($year_selected->format('Y'), $current_months[$loop1], $this->input->post("quantityType"), $vals->additionalHourTypeId);
                $date_obj = DateTime::createFromFormat('!m', $current_months[$loop1]);
                $agent[$loop1] = [
                    "y" => $ahr_counts['Agent'],
                    "name" => $date_obj->format('M'),
                    "drilldown" => $current_months[$loop1],
                    "empType" => 'agent',
                ];
                $admin[$loop1] = [
                    "y" => $ahr_counts['Admin'],
                    "name" => $date_obj->format('M'),
                    "drilldown" => $current_months[$loop1],
                    "empType" => 'admin',
                ];
            }
            $ahrTypeAdmin[$count] = [
                'name' => $vals->description,
                'typeId' => $vals->additionalHourTypeId,
                'data' => $admin,
                'stack' => 'admin',
            ];
            $ahrTypeAgent[$count] = [
                'name' => $vals->description,
                'typeId' => $vals->additionalHourTypeId,
                'data' => $agent,
                'stack' => 'agent',
            ];
            $count++;
        }

        $combinedData = array_merge($ahrTypeAdmin, $ahrTypeAgent);
        $ahr_data['combinedData'] = $combinedData;
        $ahr_data['months'] = $months;
        echo json_encode($ahr_data);

    }

    public function get_monthly_chart()
    {
        $date_selected = $this->input->post("dateSelected");
        $emp_type = $this->input->post("empType");
        $year_selected = new DateTime($date_selected);
        $current_months = $this->get_passed_months($date_selected);
        $record = $this->get_ahr_type();
        $count = 0;
        for ($loop1 = 0; $loop1 < count($current_months); $loop1++) {
            $date_obj = DateTime::createFromFormat('!m', $current_months[$loop1]);
            $months[$loop1] = $date_obj->format('M');
        }
        foreach ($record as $vals) {
            for ($loop1 = 0; $loop1 < count($current_months); $loop1++) {
                $ahr_counts = $this->get_emp_request_count_per_type($year_selected->format('Y'), $current_months[$loop1], $this->input->post("quantityType"), $vals->additionalHourTypeId);
                $date_obj = DateTime::createFromFormat('!m', $current_months[$loop1]);
                $agent[$loop1] = [
                    "y" => $ahr_counts['Agent'],
                    "name" => $date_obj->format('M'),
                    "drilldown" => $current_months[$loop1],
                    "empType" => 'agent',
                ];
                $admin[$loop1] = [
                    "y" => $ahr_counts['Admin'],
                    "name" => $date_obj->format('M'),
                    "drilldown" => $current_months[$loop1],
                    "empType" => 'admin',
                ];
            }
            $ahrTypeAdmin[$count] = [
                'name' => $vals->description,
                'data' => $admin,
                // 'stack' => 'admin'
            ];
            $ahrTypeAgent[$count] = [
                'name' => $vals->description,
                'data' => $agent,
                // 'stack' => 'agent'
            ];
            $count++;
        }
        if ($emp_type == "Admin") {
            $ahr_data['seriesData'] = $ahrTypeAdmin;
        } else if ($emp_type == "Agent") {
            $ahr_data['seriesData'] = $ahrTypeAgent;
        }
        $ahr_data['months'] = $months;
        // var_dump($ahr_data);
        echo json_encode($ahr_data);
    }

    public function get_ahr_count_admin_acc($start_date, $end_date, $ahr_type_id, $status_id, $quantity_type, $paymentStat)
    {
        $released = "";
        if ($quantity_type == 1) {
            $fields = "COUNT(ahr.additionalHourRequestId) y, dep.dep_details name, dep.dep_id drilldown";
        } else {
            $fields = "SUM(ahr.totalOtHours) y, dep.dep_id, dep.dep_details name, dep.dep_id drilldown";
        }
        if ($ahr_type_id == 0) {
            $ahrType = "";
        } else {
            $ahrType = "AND additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }
        $where = "pos.class='Admin' AND pos.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND sched.sched_id = ahr.schedId AND dep.dep_id = account.dep_id AND account.acc_id = emp.acc_id AND emp.emp_id = users.emp_id AND users.uid = ahr.userId AND Date(sched_date)>= '$start_date' AND Date(sched_date)<= '$end_date' $ahrType AND approvalStatus_ID = $status_id " . $released . " AND account.acc_description = 'Admin' GROUP BY dep.dep_details ORDER BY y DESC";
        $tables = "tbl_schedule sched, tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_account account, tbl_department dep, tbl_position pos, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        if (count($record) > 0) {
            return $record;
        } else {
            return 0;
        }
    }

    public function get_ahr_count_agent_acc($start_date, $end_date, $ahr_type_id, $status_id, $quantity_type, $paymentStat)
    {
        $released = "";
        if ($quantity_type == 1) {
            $fields = "COUNT(ahr.additionalHourRequestId) y, acc.acc_id, acc.acc_name name, acc.acc_id drilldown";
        } else {
            $fields = "SUM(ahr.totalOtHours) y, acc.acc_id, acc.acc_name name, acc.acc_id drilldown";
        }
        if ($ahr_type_id == 0) {
            $ahrType = "";
        } else {
            $ahrType = "AND additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }
        $where = "sched.sched_id = ahr.schedId AND acc.acc_id = emp.acc_id AND acc.acc_description = 'Agent' AND emp.emp_id = users.emp_id AND users.uid = ahr.userId AND Date(sched_date)>= '$start_date' AND Date(sched_date)<= '$end_date' $ahrType " . $released . " AND approvalStatus_ID = '$status_id' GROUP BY acc.acc_name ORDER BY y DESC";
        $tables = "tbl_schedule sched, tbl_additional_hour_request ahr, tbl_account acc, tbl_employee emp, tbl_user users";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        if (count($record) > 0) {
            return $record;
        } else {
            return 0;
        }
    }

    public function get_admin_drilldown($date_start, $date_end, $ahr_type_id, $status_id, $depId, $quantity_type, $paymentStat)
    {
        $released = "";
        if ($quantity_type == 1) {
            $fields = "COUNT(ahr.additionalHourRequestId) ahrCount, ahr.userId, app.fname, app.lname, app.mname";
        } else {
            $fields = "SUM(ahr.totalOtHours) ahrCount, ahr.userId, app.fname, app.lname, app.mname";
        }
        if ($ahr_type_id == 0) {
            $ahrType = "";
        } else {
            $ahrType = "AND additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }
        $where = "pos.class='Admin' AND pos.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND sched.sched_id = ahr.schedId AND app.apid = emp.apid AND users.emp_id = emp.emp_id AND emp.acc_id = acc.acc_id AND acc.dep_id = $depId AND users.uid = ahr.userId AND Date(sched_date)>= '$date_start' AND Date(sched_date)<= '$date_end' $ahrType " . $released . " AND approvalStatus_ID = '$status_id' GROUP BY ahr.userId ORDER BY ahrCount DESC";
        $tables = "tbl_schedule sched, tbl_additional_hour_request ahr, tbl_user users, tbl_account acc, tbl_employee emp, tbl_position pos, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        $data = [];
        $count = 0;
        if (count($record) > 0) {
            foreach ($record as $rows) {
                $data[$count] = [$rows->fname . " " . $rows->lname, floatval($rows->ahrCount)];
                $count++;
            }
            return $data;
        } else {
            return 0;
        }
    }

    // public function get_admin_record(){

    // }

    public function get_agent_drillDown($date_start, $date_end, $ahr_type_id, $status_id, $accId, $quantity_type, $paymentStat)
    {
        $released = "";
        if ($quantity_type == 1) {
            $fields = "COUNT(ahr.additionalHourRequestId) ahrCount, ahr.userId, app.fname, app.lname, app.mname";
        } else {
            $fields = "SUM(ahr.totalOtHours) ahrCount, ahr.userId, app.fname, app.lname, app.mname";
        }
        if ($ahr_type_id == 0) {
            $ahrType = "";
        } else {
            $ahrType = "AND additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }
        $where = "users.uid = ahr.userId AND app.apid = emp.apid AND users.emp_id = emp.emp_id AND emp.acc_id = $accId AND Date(ahr.dateTimeFiled)>= '$date_start' AND Date(ahr.dateTimeFiled)<= '$date_end' $ahrType " . $released . " AND approvalStatus_ID = '$status_id' GROUP BY users.uid ORDER BY ahrCount DESC";
        $tables = "tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        $data = [];
        $count = 0;
        if (count($record) > 0) {
            foreach ($record as $rows) {
                $data[$count] = [$rows->fname . " " . $rows->lname, floatval($rows->ahrCount)];
                $count++;
            }
            return $data;
        } else {
            return 0;
        }
    }

    public function get_drilldown()
    {
        $date_start = $this->input->post("dateStart");
        $date_end = $this->input->post("dateEnd");
        $ahr_type_id = $this->input->post("ahrTypeId");
        $status_id = $this->input->post("statusId");
        $emp_type = $this->input->post("employeeType");
        $drill_id = $this->input->post("drill_id");
        $drill_name = $this->input->post("drill_name");
        $paymentStat = (int) $this->input->post("paymentStat");
        $drilldown = [];
        $count = 0;
        if ($emp_type == "Admin") {
            $data = $this->get_admin_drilldown($date_start, $date_end, $ahr_type_id, $status_id, $drill_id, $this->input->post('quantityType'), $paymentStat);
        } else {
            $data = $this->get_agent_drillDown($date_start, $date_end, $ahr_type_id, $status_id, $drill_id, $this->input->post('quantityType'), $paymentStat);

        }
        $drilldown[$drill_name . " - " . $emp_type] = [
            "name" => $drill_name . " - " . $emp_type,
            "id" => $drill_id,
            "data" => $data,
        ];
        $report_details['drilldown'] = $drilldown;
        echo json_encode($report_details);
    }

    public function get_filtered_report()
    {
        $date_start = $this->input->post("dateStart");
        $date_end = $this->input->post("dateEnd");
        $ahr_type_id = $this->input->post("ahrTypeId");
        $status_id = $this->input->post("statusId");
        $emp_type = $this->input->post("employeeType");
        $paymentStat = (int) $this->input->post("paymentStat");
        $drilldown = [];
        $count = 0;
        $sum = [];
        if ($emp_type == '0') {
            $per_admin_account = $this->get_ahr_count_admin_acc($date_start, $date_end, $ahr_type_id, $status_id, $this->input->post('quantityType'), $paymentStat);
            $per_agent_account = $this->get_ahr_count_agent_acc($date_start, $date_end, $ahr_type_id, $status_id, $this->input->post('quantityType'), $paymentStat);
            if ($per_admin_account == 0) {
                $accounts[$count] = 0;
                $drilldown[$count] = 0;
            } else {
                foreach ($per_admin_account as $rows) {
                    $accounts[$count] = [
                        "y" => floatval($rows->y),
                        "name" => $rows->name . " - Admin",
                        "drilldown" => $rows->drilldown,
                    ];
                    $count++;
                }
            }
            if ($per_agent_account == 0) {
                $accounts[$count] = 0;
            } else {
                foreach ($per_agent_account as $rows) {
                    $accounts[$count] = [
                        "y" => floatval($rows->y),
                        "name" => $rows->name . " - Ambs",
                        "drilldown" => $rows->drilldown,
                    ];
                    $count++;
                }
            }
        } else if ($emp_type == "Admin") {
            $per_admin_account = $this->get_ahr_count_admin_acc($date_start, $date_end, $ahr_type_id, $status_id, $this->input->post('quantityType'), $paymentStat);
            if ($per_admin_account == 0) {
                $accounts[$count] = 0;
                $drilldown[$count] = 0;
            } else {
                foreach ($per_admin_account as $rows) {
                    $accounts[$count] = [
                        "y" => floatval($rows->y),
                        "name" => $rows->name . " - Admin",
                        "drilldown" => $rows->drilldown,
                    ];
                    $count++;
                }
            }
        } else {
            $per_agent_account = $this->get_ahr_count_agent_acc($date_start, $date_end, $ahr_type_id, $status_id, $this->input->post('quantityType'), $paymentStat);
            if ($per_agent_account == 0) {
                $accounts[$count] = 0;
            } else {
                foreach ($per_agent_account as $rows) {
                    $accounts[$count] = [
                        "y" => floatval($rows->y),
                        "name" => $rows->name . " - Ambs",
                        "drilldown" => $rows->drilldown,
                    ];
                    $count++;
                }
            }
        }
        foreach ($accounts as $key => $row) {
            $sum[$key] = $row['y'];
        }
        array_multisort($sum, SORT_DESC, $accounts);
        $report_details['accounts'] = $accounts;
        echo json_encode($report_details);
    }

    //  public function get_ahr_count_admin_acc($start_date, $end_date, $ahr_type_id, $status_id, $quantity_type, $paymentStat){
    //     $released = "";
    //     if($quantity_type == 1){
    //         $fields = "COUNT(ahr.additionalHourRequestId) y, dep.dep_details name, dep.dep_id drilldown";
    //     }else{
    //         $fields = "SUM(ahr.totalOtHours) y, dep.dep_id, dep.dep_details name, dep.dep_id drilldown";
    //     }
    //     if($ahr_type_id == 0){
    //         $ahrType = "";
    //     }else{
    //         $ahrType = "AND additionalHourTypeId = $ahr_type_id";
    //     }
    //     if($paymentStat != 2){
    //         $released = "AND ahr.isReleased = ".$paymentStat;
    //     }
    //     $where = "pos.class='Admin' AND pos.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND sched.sched_id = ahr.schedId AND dep.dep_id = account.dep_id AND account.acc_id = emp.acc_id AND emp.emp_id = users.emp_id AND users.uid = ahr.userId AND Date(sched_date)>= '$start_date' AND Date(sched_date)<= '$end_date' $ahrType AND approvalStatus_ID = $status_id ".$released." AND account.acc_description = 'Admin' GROUP BY dep.dep_details ORDER BY y DESC";
    //     $tables = "tbl_schedule sched, tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_account account, tbl_department dep, tbl_position pos, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote";
    //     $record = $this->general_model->fetch_specific_vals($fields,$where,$tables);
    //     if(count($record)>0){
    //         return $record;
    //     }else{
    //         return 0;
    //     }
    // }

    public function get_ahr_record_admin($date_start, $date_end, $ahr_type_id, $status_id, $paymentStat)
    {
        $released = "";
        $fields = "ahr.additionalHourRequestId, ahr.additionalHourRequestId, ahr.dateTimeFiled, ahr.otStartDate, ahr.otStartTime, ahr.otEndDate, ahr.otEndTime, ahr.totalOtHours, ahr.approvalStatus_ID, ahr.reason, ahr.requestType, ahr.isReleased, dep.dep_details as name, dep.dep_id drilldown, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahr.userId, app.pic, app.fname, app.lname, app.mname, empStat.status as empStatus, pos.pos_details positionDescription, schedType.type, sched_date";
        if ($ahr_type_id == 0) {
            $ahrType = "";
        } else {
            $ahrType = "AND ahr.additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }
        $where = "pos.class='Admin' AND empPromote.posempstat_id = posEmpStat.posempstat_id AND posEmpStat.pos_id = pos.pos_id AND posEmpStat.empstat_id AND posEmpStat.empstat_id = empStat.empstat_id AND empPromote.ishistory = 1 AND empPromote.isActive = 1 AND empPromote.emp_id = emp.emp_id AND schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = ahr.schedId AND ahrType.additionalHourTypeId = ahr.additionalHourTypeId AND dep.dep_id = account.dep_id AND account.acc_id = emp.acc_id AND emp.emp_id = users.emp_id AND users.uid = ahr.userId $released AND statuses.status_ID = ahr.approvalStatus_ID $ahrType AND app.apid = emp.apid AND Date(sched_date)>= '$date_start' AND Date(sched_date)<= '$date_end' AND approvalStatus_ID = $status_id AND account.acc_description = 'Admin'";
        $tables = "tbl_schedule sched,  tbl_schedule_type schedType, tbl_additional_hour_type ahrType, tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_account account, tbl_department dep, tbl_position pos, tbl_emp_stat empStat, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote, tbl_status statuses, tbl_applicant app";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);

        // if(count($record)>0){
        //     return $record;
        // }else{
        //     return 0;
        // }
    }

    public function get_ahr_record_agent($date_start, $date_end, $ahr_type_id, $status_id, $paymentStat)
    {
        $released = "";
        $fields = "ahr.additionalHourRequestId, ahr.additionalHourRequestId, ahr.dateTimeFiled, ahr.otStartDate, ahr.otStartTime, ahr.otEndDate, ahr.otEndTime, ahr.totalOtHours, ahr.approvalStatus_ID, ahr.reason, ahr.requestType, ahr.isReleased, acc.acc_name as name, acc.acc_id drilldown, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahr.userId, app.pic, app.fname, app.lname, app.mname, empStat.status as empStatus, pos.pos_details positionDescription, schedType.type, sched_date";
        if ($ahr_type_id == 0) {
            $ahrType = "";
        } else {
            $ahrType = "AND ahr.additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }
        $where = "empPromote.posempstat_id = posEmpStat.posempstat_id AND posEmpStat.pos_id = pos.pos_id AND posEmpStat.empstat_id AND posEmpStat.empstat_id = empStat.empstat_id AND empPromote.ishistory = 1 AND empPromote.isActive = 1 AND empPromote.emp_id = emp.emp_id AND schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = ahr.schedId AND ahrType.additionalHourTypeId = ahr.additionalHourTypeId AND acc.acc_id = emp.acc_id AND acc.acc_description = 'Agent' AND emp.emp_id = users.emp_id AND users.uid = ahr.userId $released AND statuses.status_ID = ahr.approvalStatus_ID $ahrType AND app.apid = emp.apid AND Date(sched_date)>= '$date_start' AND Date(sched_date)<= '$date_end' AND approvalStatus_ID = $status_id";
        $tables = "tbl_schedule sched, tbl_schedule_type schedType, tbl_additional_hour_type ahrType, tbl_additional_hour_request ahr, tbl_account acc, tbl_employee emp, tbl_user users, tbl_position pos, tbl_emp_stat empStat, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote, tbl_status statuses, tbl_applicant app";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
    }

    public function get_detailed_report_record($date_start, $date_end, $ahr_type_id, $status_id, $emp_type, $paymentStat)
    {
        // $date_start = '2017-08-19';
        // $date_end = '2018-09-17';
        // $ahr_type_id = 0;
        // $status_id = 5;
        // $emp_type = 0;
        // $paymentStat = 0;
        $count = 0;

        if ($emp_type == '0') {
            $admin_bool = 0;
            $agent_bool = 0;
            $get_ahr_record_admin = $this->get_ahr_record_admin($date_start, $date_end, $ahr_type_id, $status_id, $paymentStat);
            $get_ahr_record_agent = $this->get_ahr_record_agent($date_start, $date_end, $ahr_type_id, $status_id, $paymentStat);
            if (count($get_ahr_record_admin) == 0) {
                $admin_bool = 0;
            } else {
                $admin_bool = 1;
            }
            if (count($get_ahr_record_agent) == 0) {
                $agent_bool = 0;
            } else {
                $agent_bool = 1;
            }

            if (($admin_bool == 1) && ($agent_bool == 0)) {
                $detailed_record = $get_ahr_record_admin;
            } else if (($admin_bool == 0) && ($agent_bool == 1)) {
                $detailed_record = $get_ahr_record_agent;
            } else if (($admin_bool == 1) && ($agent_bool == 1)) {
                $detailed_record = array_merge($get_ahr_record_admin, $get_ahr_record_agent);
            }
        } else if ($emp_type == "Admin") {
            $get_ahr_record_admin = $this->get_ahr_record_admin($date_start, $date_end, $ahr_type_id, $status_id, $paymentStat);
            if (count($get_ahr_record_admin) == 0) {
                $detailed_record = 0;
            } else {
                $detailed_record = $get_ahr_record_admin;
            }
        } else {
            $get_ahr_record_agent = $this->get_ahr_record_agent($date_start, $date_end, $ahr_type_id, $status_id, $paymentStat);
            if (count($get_ahr_record_agent) == 0) {
                $detailed_record = 0;
            } else {
                $detailed_record = $get_ahr_record_agent;
            }
        }
        $index_count = 0;
        foreach ($detailed_record as $rows) {
            $sum[$index_count]['additionalHourRequestId'] = $rows->additionalHourRequestId;
            $index_count++;
        }
        array_multisort($sum, SORT_ASC, $detailed_record);
        return $detailed_record;
    }

    // EXPORT RECORD
    public function get_employee_ahr_records()
    {
        $status_id = 5;
        $requestor_id = 0;
        $rangeStart = "2017-08-16";
        $rangeEnd = "2018-09-14";
        $account = 23;
        $emp_type = 0;
        $ahr_type = 0;
        $paymentStat = 0;
        $tbl_account = "";
        $pos_class = "";
        $employee_type = "";
        $released = "";
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }

        if ($requestor_id == 0) {
            $user = "";
        } else {
            $user = "AND ahr.userId = $requestor_id";
        }

        if ($ahr_type == 0) {
            $ahr_type_col = "";
        } else {
            $ahr_type_col = "AND ahr.additionalHourTypeId = $ahr_type";
        }

        if ($emp_type == 'Admin') {
            $employee_type = "AND emp.acc_id = acc.acc_id AND acc.dep_id = $account";
            $pos_class = "AND pos.class='Admin'";
            $tbl_account = "tbl_account acc,";
        } else if ($emp_type == 'Ambs') {
            $tbl_account = "";
            $pos_class = "AND pos.class='Agent'";
            $employee_type = "AND emp.acc_id = $account";
        }
        $fields = "ahr.additionalHourRequestId, ahr.dateTimeFiled, ahr.otStartDate, ahr.otStartTime, ahr.otEndDate, ahr.otEndTime, ahr.totalOtHours, ahr.approvalStatus_ID, ahr.reason, ahr.requestType, statuses.description as statusDescription, ahrType.additionalHourTypeId, ahrType.description as ahrTypeDescription, ahr.userId, app.pic, app.fname, app.lname, app.mname, , pos.pos_details positionDescription, empStat.status as empStatus, schedType.type, sched_date";
        $where = "schedType.schedtype_id = sched.schedtype_id $pos_class AND empStat.empstat_id = posEmpStat.empstat_id AND pos.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND sched.sched_id = ahr.schedId AND ahrType.additionalHourTypeId = ahr.additionalHourTypeId AND users.uid = ahr.userId AND statuses.status_ID = ahr.approvalStatus_ID AND app.apid = emp.apid AND emp.emp_id = users.emp_id $employee_type AND users.uid = ahr.userId AND Date(sched_date)>= '$rangeStart' AND Date(sched_date)<= '$rangeEnd' $ahr_type_col AND ahr.approvalStatus_ID = $status_id " . $user . " $released";
        $tables = "tbl_additional_hour_request ahr, tbl_additional_hour_type ahrType, tbl_schedule sched, tbl_schedule_type schedType, tbl_user users, tbl_position pos, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, $tbl_account tbl_employee emp, tbl_status statuses, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        var_dump($record);
        // if ($datatable['query']['employeeAhrRecordSearch'] != '') {
        //     $keyword = $datatable['query']['employeeAhrRecordSearch'];
        //     $date_bool = $this->check_if_date($keyword);

        //     if((int)$date_bool == 1){
        //         $date = $this->convert_date($keyword);
        //         $where = "sched_date = '$date' OR ahr.dateTimeFiled LIKE '%".$date."%'";
        //     }else{
        //         if(is_numeric($keyword)){
        //             $where = "ahr.additionalHourRequestId LIKE '%" . (int)$keyword . "%'";
        //         }else{
        //             $where = "ahr.additionalHourRequestId = $keyword";
        //         }
        //     }
        //     $query['search']['append'] = " AND ($where)";
        //     $query['search']['total'] = " AND ($where)";
        // }
        // $data = $this->set_datatable_query($datatable, $query);
        // echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function ahr_count_per_ahr_type_admin_emp($start_date, $end_date, $ahr_type_id, $status_id, $paymentStat, $user_id)
    {
        $released = "";
        $ahrType = "";
        $user_col_id = "";
        if ($user_id != 0) {
            $user_col_id = "AND users.uid = $user_id";
        }
        $fields = "COUNT(ahr.additionalHourRequestId) requestCount, SUM(ahr.totalOtHours) sumHours, dep.dep_details as name, account.acc_description teamType, dep.dep_id drilldown, users.uid, app.pic, app.fname, app.lname, app.mname, empStat.status as empStatus, pos.pos_details positionDescription";
        if ($ahr_type_id != 0) {
            $ahrType = "AND additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = $paymentStat" ;
        }
        $where = "pos.class='Admin' AND empPromote.posempstat_id = posEmpStat.posempstat_id AND posEmpStat.pos_id = pos.pos_id AND posEmpStat.empstat_id AND posEmpStat.empstat_id = empStat.empstat_id AND empPromote.ishistory = 1 AND empPromote.isActive = 1 AND empPromote.emp_id = emp.emp_id AND sched.sched_id = ahr.schedId AND dep.dep_id = account.dep_id AND account.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = ahr.userId ".$released." AND Date(sched_date)>= '$start_date' AND Date(sched_date)<= '$end_date' ".$ahrType." AND approvalStatus_ID = $status_id ".$user_col_id." AND account.acc_description = 'Admin' GROUP BY users.uid ORDER BY sumHours DESC";
        $tables = "tbl_schedule sched, tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_account account, tbl_department dep, tbl_position pos, tbl_emp_stat empStat, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        if (count($record) > 0) {
            return $record;
        } else {
            return 0;
        }
    }

    public function ahr_count_per_ahr_type_agent_emp($start_date, $end_date, $ahr_type_id, $status_id, $paymentStat, $user_id)
    {
        $released = "";
        $ahrType = "";
        $user_col_id = "";
        if ($user_id != 0) {
            $user_col_id = "AND users.uid = $user_id";
        }
        $fields = "COUNT(ahr.additionalHourRequestId) requestCount, ROUND(SUM(ahr.totalOtHours),2) sumHours, account.acc_name as name, account.acc_description teamType, account.acc_id drilldown, users.uid, app.pic, app.fname, app.lname, app.mname, empStat.status as empStatus, pos.pos_details positionDescription";
        if ($ahr_type_id != 0) {
            $ahrType = "AND additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }
        $where = "pos.class='Agent' AND empPromote.posempstat_id = posEmpStat.posempstat_id AND posEmpStat.pos_id = pos.pos_id AND posEmpStat.empstat_id AND posEmpStat.empstat_id = empStat.empstat_id AND empPromote.ishistory = 1 AND empPromote.isActive = 1 AND empPromote.emp_id = emp.emp_id AND sched.sched_id = ahr.schedId AND account.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = ahr.userId ".$released." AND Date(sched_date)>= '$start_date' AND Date(sched_date)<= '$end_date' ".$ahrType." AND approvalStatus_ID = 5 ".$user_col_id." AND account.acc_description = 'Agent' GROUP BY users.uid ORDER BY sumHours DESC";
        $tables = "tbl_schedule sched, tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_account account, tbl_position pos, tbl_emp_stat empStat, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        // var_dump($record);
        // echo $where;
        if (count($record) > 0) {
            return $record;
        } else {
            return 0;
        }
    }

    public function individual_record_count($start_date, $end_date, $ahr_type_id, $status_id, $emp_type, $paymentStat)
    {
        // echo $start_date." - ".$end_date;
        // $start_date = '2017-08-19';
        // $end_date = '2018-09-17';
        // $ahr_type_id = 0;
        // $status_id = 5;
        // $emp_type = 0;
        // $paymentStat = 0;
        $count = 0;
        $admin_bool = 0;
        $agent_bool = 0;
        $admin_user = [];
        $amb_user = [];
        $admin_count = 0;
        $amb_count = 0;
        $ahr_type_id_arr = [
            "after" => 1,
            "pre" => 2,
            "word" => 3,
            "total" => 0,
        ];
        $ahr_type_keys = array_keys($ahr_type_id_arr);
        $admin_emp = $this->ahr_count_per_ahr_type_admin_emp($start_date, $end_date, $ahr_type_id, $status_id, $paymentStat, 0);
        $agent_emp = $this->ahr_count_per_ahr_type_agent_emp($start_date, $end_date, $ahr_type_id, $status_id, $paymentStat, 0);
        // var_dump($admin_emp);
        // var_dump($agent_emp);
        if ($emp_type == '0') {
            foreach ($admin_emp as $admin_rows) {
                $m_name = "";
                if ($admin_rows->mname != "") {
                    $m_name = $admin_rows->mname;
                }
                $admin_user[$admin_count]['lname'] = $admin_rows->lname;
                $admin_user[$admin_count]['fname'] = $admin_rows->fname;
                $admin_user[$admin_count]['mname'] = $m_name;
                $admin_user[$admin_count]['type'] = $admin_rows->teamType;
                $admin_user[$admin_count]['position'] = $admin_rows->positionDescription;
                $admin_user[$admin_count]['emp_stat'] = $admin_rows->empStatus;
                $admin_user[$admin_count]['team'] = $admin_rows->name;
                for ($admin_loop = 0; $admin_loop < count($ahr_type_id_arr); $admin_loop++) {
                    $admin_obj = $this->ahr_count_per_ahr_type_admin_emp($start_date, $end_date, $ahr_type_id_arr[$ahr_type_keys[$admin_loop]], $status_id, $paymentStat, $admin_rows->uid);
                    if ($admin_obj == 0) {
                        $admin_user[$admin_count][$ahr_type_keys[$admin_loop] . "_count"] = 0;
                        $admin_user[$admin_count][$ahr_type_keys[$admin_loop] . "_hours"] = 0;
                    } else {
                        foreach ($admin_obj as $admin_obj_rows) {
                            $admin_user[$admin_count][$ahr_type_keys[$admin_loop] . "_count"] = $admin_obj_rows->requestCount;
                            $admin_user[$admin_count][$ahr_type_keys[$admin_loop] . "_hours"] = $admin_obj_rows->sumHours;
                        }
                    }
                }
                $admin_count++;
            }
            foreach ($agent_emp as $agent_rows) {
                // var_dump($agent_rows);
                $m_name = "";
                if ($agent_rows->mname != "") {
                    $m_name = $agent_rows->mname;
                }
                $amb_user[$amb_count]['lname'] = $agent_rows->lname;
                $amb_user[$amb_count]['fname'] = $agent_rows->fname;
                $amb_user[$amb_count]['mname'] = $m_name;
                $amb_user[$amb_count]['type'] = $agent_rows->teamType;
                $amb_user[$amb_count]['position'] = $agent_rows->positionDescription;
                $amb_user[$amb_count]['emp_stat'] = $agent_rows->empStatus;
                $amb_user[$amb_count]['team'] = $agent_rows->name;
                for ($agent_loop = 0; $agent_loop < count($ahr_type_id_arr); $agent_loop++) {
                    $agent_obj = $this->ahr_count_per_ahr_type_agent_emp($start_date, $end_date, $ahr_type_id_arr[$ahr_type_keys[$agent_loop]], $status_id, $paymentStat, $agent_rows->uid);
                    // var_dump($agent_obj);
                    if ($agent_obj == 0) {
                        $amb_user[$amb_count][$ahr_type_keys[$agent_loop] . "_count"] = 0;
                        $amb_user[$amb_count][$ahr_type_keys[$agent_loop] . "_hours"] = 0;
                    } else {
                        foreach ($agent_obj as $agent_obj_rows) {
                            $amb_user[$amb_count][$ahr_type_keys[$agent_loop] . "_count"] = $agent_obj_rows->requestCount;
                            $amb_user[$amb_count][$ahr_type_keys[$agent_loop] . "_hours"] = $agent_obj_rows->sumHours;
                        }
                    }
                }
                $amb_count++;
            }
            if (count($admin_emp) == 0) {
                $admin_bool = 0;
            } else {
                $admin_bool = 1;
            }
            if (count($agent_emp) == 0) {
                $agent_bool = 0;
            } else {
                $agent_bool = 1;
            }
            if (($admin_bool == 1) && ($agent_bool == 0)) {
                $individual_record = $admin_user;
            } else if (($admin_bool == 0) && ($agent_bool == 1)) {
                $individual_record = $amb_user;
            } else if (($admin_bool == 1) && ($agent_bool == 1)) {
                $individual_record = array_merge($admin_user, $amb_user);
            }
        } else if ($emp_type == "Admin") {
            if (count($admin_emp) == 0) {
                $individual_record = 0;
            } else {
                foreach ($admin_emp as $admin_rows) {
                    $m_name = "";
                    if ($admin_rows->mname != "") {
                        $m_name = $admin_rows->mname;
                    }
                    $admin_user[$admin_count]['lname'] = $admin_rows->lname;
                    $admin_user[$admin_count]['fname'] = $admin_rows->fname;
                    $admin_user[$admin_count]['mname'] = $m_name;
                    $admin_user[$admin_count]['type'] = $admin_rows->teamType;
                    $admin_user[$admin_count]['position'] = $admin_rows->positionDescription;
                    $admin_user[$admin_count]['emp_stat'] = $admin_rows->empStatus;
                    $admin_user[$admin_count]['team'] = $admin_rows->name;

                    for ($admin_loop = 0; $admin_loop < count($ahr_type_id_arr); $admin_loop++) {
                        $admin_obj = $this->ahr_count_per_ahr_type_admin_emp($start_date, $end_date, $ahr_type_id_arr[$ahr_type_keys[$admin_loop]], $status_id, $paymentStat, $admin_rows->uid);
                        if ($admin_obj == 0) {
                            $admin_user[$admin_count][$ahr_type_keys[$admin_loop] . "_count"] = 0;
                            $admin_user[$admin_count][$ahr_type_keys[$admin_loop] . "_hours"] = 0;
                        } else {
                            foreach ($admin_obj as $admin_obj_rows) {
                                $admin_user[$admin_count][$ahr_type_keys[$admin_loop] . "_count"] = $admin_obj_rows->requestCount;
                                $admin_user[$admin_count][$ahr_type_keys[$admin_loop] . "_hours"] = $admin_obj_rows->sumHours;
                            }
                        }
                    }
                    $admin_count++;
                }
                $individual_record = $admin_user;
            }
        } else {
            if (count($agent_emp) == 0) {
                $individual_record = 0;
            } else {
                foreach ($agent_emp as $agent_rows) {
                    $m_name = "";
                    if ($agent_rows->mname != "") {
                        $m_name = $agent_rows->mname;
                    }
                    $amb_user[$amb_count]['lname'] = $agent_rows->lname;
                    $amb_user[$amb_count]['fname'] = $agent_rows->fname;
                    $amb_user[$amb_count]['mname'] = $m_name;
                    $amb_user[$amb_count]['type'] = $agent_rows->teamType;
                    $amb_user[$amb_count]['position'] = $agent_rows->positionDescription;
                    $amb_user[$amb_count]['emp_stat'] = $agent_rows->empStatus;
                    $amb_user[$amb_count]['team'] = $agent_rows->name;
                    for ($agent_loop = 0; $agent_loop < count($ahr_type_id_arr); $agent_loop++) {
                        $agent_obj = $this->ahr_count_per_ahr_type_agent_emp($start_date, $end_date, $ahr_type_id_arr[$ahr_type_keys[$agent_loop]], $status_id, $paymentStat, $agent_rows->uid);
                        if ($agent_obj == 0) {
                            $amb_user[$amb_count][$ahr_type_keys[$agent_loop] . "_count"] = 0;
                            $amb_user[$amb_count][$ahr_type_keys[$agent_loop] . "_hours"] = 0;
                        } else {
                            foreach ($agent_obj as $agent_obj_rows) {
                                $amb_user[$amb_count][$ahr_type_keys[$agent_loop] . "_count"] = $agent_obj_rows->requestCount;
                                $amb_user[$amb_count][$ahr_type_keys[$agent_loop] . "_hours"] = $agent_obj_rows->sumHours;
                            }
                        }
                    }
                    $amb_count++;
                }
                $individual_record = $amb_user;
            }
        }
        $index_count = 0;
        foreach ($individual_record as $rows) {
            $sum[$index_count]['total_hours'] = $rows['total_hours'];
            $index_count++;
        }
        array_multisort($sum, SORT_DESC, $individual_record);
        return $individual_record;
    }

    public function ahr_count_per_ahr_type_admin($start_date, $end_date, $ahr_type_id, $status_id, $paymentStat, $dep_id)
    {
        $released = "";
        if ($dep_id == 0) {
            $dep_col_id = "";
        } else {
            $dep_col_id = "AND dep.dep_id = $dep_id";
        }
        $fields = "COUNT(ahr.additionalHourRequestId) requestCount, SUM(ahr.totalOtHours) sumHours, dep.dep_details name, account.acc_description teamType, dep.dep_id drilldown";
        if ($ahr_type_id == 0) {
            $ahrType = "";
        } else {
            $ahrType = "AND additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }
        $where = "pos.class='Admin' AND pos.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND sched.sched_id = ahr.schedId AND dep.dep_id = account.dep_id AND account.acc_id = emp.acc_id AND emp.emp_id = users.emp_id AND users.uid = ahr.userId AND Date(sched_date)>= '$start_date' AND Date(sched_date)<= '$end_date' $ahrType AND approvalStatus_ID = $status_id " . $released . " $dep_col_id AND account.acc_description = 'Admin' GROUP BY dep.dep_details ORDER BY sumHours DESC";
        $tables = "tbl_schedule sched, tbl_additional_hour_request ahr, tbl_user users, tbl_employee emp, tbl_account account, tbl_department dep, tbl_position pos, tbl_pos_emp_stat posEmpStat, tbl_emp_promote empPromote";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        if (count($record) > 0) {
            return $record;
        } else {
            return 0;
        }
    }

    public function ahr_count_per_ahr_type_agent($start_date, $end_date, $ahr_type_id, $status_id, $paymentStat, $acc_id)
    {
        $released = "";
        if ($acc_id == 0) {
            $acc_col_id = "";
        } else {
            $acc_col_id = "AND acc.acc_id = $acc_id";
        }
        $fields = "COUNT(ahr.additionalHourRequestId) requestCount, SUM(ahr.totalOtHours) sumHours,  acc.acc_name name, acc.acc_description teamType, acc.acc_id drilldown";
        if ($ahr_type_id == 0) {
            $ahrType = "";
        } else {
            $ahrType = "AND additionalHourTypeId = $ahr_type_id";
        }
        if ($paymentStat != 0) {
            $released = "AND ahr.isReleased = " . $paymentStat;
        }
        $where = "sched.sched_id = ahr.schedId AND acc.acc_id = emp.acc_id AND acc.acc_description = 'Agent' AND emp.emp_id = users.emp_id AND users.uid = ahr.userId AND Date(sched_date)>= '$start_date' AND Date(sched_date)<= '$end_date' $ahrType " . $released . " " . $acc_col_id . " AND approvalStatus_ID = '$status_id' GROUP BY acc.acc_name ORDER BY sumHours DESC";
        $tables = "tbl_schedule sched, tbl_additional_hour_request ahr, tbl_account acc, tbl_employee emp, tbl_user users";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        if (count($record) > 0) {
            return $record;
        } else {
            return 0;
        }
    }

    public function team_count_record($start_date, $end_date, $ahr_type_id, $status_id, $emp_type, $paymentStat)
    {
        // $start_date = '2017-08-19';
        // $end_date = '2018-09-17';
        // $ahr_type_id = 0;
        // $status_id = 5;
        // $emp_type = 0;
        // $paymentStat = 0;
        $count = 0;
        $admin_bool = 0;
        $agent_bool = 0;
        $admin_team = [];
        $amb_team = [];
        $admin_count = 0;
        $amb_count = 0;
        $ahr_type_id_arr = [
            "after" => 1,
            "pre" => 2,
            "word" => 3,
            "total" => 0,
        ];
        $ahr_type_keys = array_keys($ahr_type_id_arr);
        $dep_admin = $this->ahr_count_per_ahr_type_admin($start_date, $end_date, $ahr_type_id, $status_id, $paymentStat, 0);
        $acc_agent = $this->ahr_count_per_ahr_type_agent($start_date, $end_date, $ahr_type_id, $status_id, $paymentStat, 0);
        if ($emp_type == '0') {
            foreach ($dep_admin as $admin_rows) {
                $admin_team[$admin_count]['team'] = $admin_rows->name;
                $admin_team[$admin_count]['type'] = $admin_rows->teamType;
                for ($admin_loop = 0; $admin_loop < count($ahr_type_id_arr); $admin_loop++) {
                    $admin_obj = $this->ahr_count_per_ahr_type_admin($start_date, $end_date, $ahr_type_id_arr[$ahr_type_keys[$admin_loop]], $status_id, $paymentStat, $admin_rows->drilldown);
                    if ($admin_obj == 0) {
                        $admin_team[$admin_count][$ahr_type_keys[$admin_loop] . "_count"] = 0;
                        $admin_team[$admin_count][$ahr_type_keys[$admin_loop] . "_hours"] = 0;
                    } else {
                        foreach ($admin_obj as $admin_obj_rows) {
                            $admin_team[$admin_count][$ahr_type_keys[$admin_loop] . "_count"] = $admin_obj_rows->requestCount;
                            $admin_team[$admin_count][$ahr_type_keys[$admin_loop] . "_hours"] = $admin_obj_rows->sumHours;
                        }
                    }
                }
                $admin_count++;
            }
            foreach ($acc_agent as $agent_rows) {
                $amb_team[$amb_count]['team'] = $agent_rows->name;
                $amb_team[$amb_count]['type'] = $agent_rows->teamType;
                for ($agent_loop = 0; $agent_loop < count($ahr_type_id_arr); $agent_loop++) {
                    $agent_obj = $this->ahr_count_per_ahr_type_agent($start_date, $end_date, $ahr_type_id_arr[$ahr_type_keys[$agent_loop]], $status_id, $paymentStat, $agent_rows->drilldown);
                    if ($agent_obj == 0) {
                        $amb_team[$amb_count][$ahr_type_keys[$agent_loop] . "_count"] = 0;
                        $amb_team[$amb_count][$ahr_type_keys[$agent_loop] . "_hours"] = 0;
                    } else {
                        foreach ($agent_obj as $agent_obj_rows) {
                            $amb_team[$amb_count][$ahr_type_keys[$agent_loop] . "_count"] = $agent_obj_rows->requestCount;
                            $amb_team[$amb_count][$ahr_type_keys[$agent_loop] . "_hours"] = $agent_obj_rows->sumHours;
                        }
                    }
                }
                $amb_count++;
            }
            if (count($dep_admin) == 0) {
                $admin_bool = 0;
            } else {
                $admin_bool = 1;
            }
            if (count($acc_agent) == 0) {
                $agent_bool = 0;
            } else {
                $agent_bool = 1;
            }
            if (($admin_bool == 1) && ($agent_bool == 0)) {
                $team_record = $admin_team;
            } else if (($admin_bool == 0) && ($agent_bool == 1)) {
                $team_record = $amb_team;
            } else if (($admin_bool == 1) && ($agent_bool == 1)) {
                $team_record = array_merge($admin_team, $amb_team);
            }
        } else if ($emp_type == "Admin") {
            if (count($dep_admin) == 0) {
                $team_record = 0;
            } else {
                foreach ($dep_admin as $admin_rows) {
                    $admin_team[$admin_count]['team'] = $admin_rows->name;
                    $admin_team[$admin_count]['type'] = $admin_rows->teamType;
                    for ($admin_loop = 0; $admin_loop < count($ahr_type_id_arr); $admin_loop++) {
                        $admin_obj = $this->ahr_count_per_ahr_type_admin($start_date, $end_date, $ahr_type_id_arr[$ahr_type_keys[$admin_loop]], $status_id, $paymentStat, $admin_rows->drilldown);
                        if ($admin_obj == 0) {
                            $admin_team[$admin_count][$ahr_type_keys[$admin_loop] . "_count"] = 0;
                            $admin_team[$admin_count][$ahr_type_keys[$admin_loop] . "_hours"] = 0;
                        } else {
                            foreach ($admin_obj as $admin_obj_rows) {
                                $admin_team[$admin_count][$ahr_type_keys[$admin_loop] . "_count"] = $admin_obj_rows->requestCount;
                                $admin_team[$admin_count][$ahr_type_keys[$admin_loop] . "_hours"] = $admin_obj_rows->sumHours;
                            }
                        }
                    }
                    $admin_count++;
                }
                $team_record = $admin_team;
            }
        } else {
            if (count($acc_agent) == 0) {
                $team_record = 0;
            } else {
                foreach ($acc_agent as $agent_rows) {
                    $amb_team[$amb_count]['team'] = $agent_rows->name;
                    $amb_team[$amb_count]['type'] = $agent_rows->teamType;
                    for ($agent_loop = 0; $agent_loop < count($ahr_type_id_arr); $agent_loop++) {
                        $agent_obj = $this->ahr_count_per_ahr_type_agent($start_date, $end_date, $ahr_type_id_arr[$ahr_type_keys[$agent_loop]], $status_id, $paymentStat, $agent_rows->drilldown);
                        if ($agent_obj == 0) {
                            $amb_team[$amb_count][$ahr_type_keys[$agent_loop] . "_count"] = 0;
                            $amb_team[$amb_count][$ahr_type_keys[$agent_loop] . "_hours"] = 0;
                        } else {
                            foreach ($agent_obj as $agent_obj_rows) {
                                $amb_team[$amb_count][$ahr_type_keys[$agent_loop] . "_count"] = $agent_obj_rows->requestCount;
                                $amb_team[$amb_count][$ahr_type_keys[$agent_loop] . "_hours"] = $agent_obj_rows->sumHours;
                            }
                        }
                    }
                    $amb_count++;
                }
                $team_record = $amb_team;
            }
        }
        $index_count = 0;
        foreach ($team_record as $rows) {
            $sum[$index_count]['total_hours'] = $rows['total_hours'];
            $index_count++;
        }
        array_multisort($sum, SORT_DESC, $team_record);
        return $team_record;
    }
    public function download_ahr_report($start_date, $end_date, $ahr_type_id, $status_id, $emp_type, $paymentStat)
    {
    //     $start_date = "2017-09-06";
    //     $end_date ="2018-09-06";
    //     $ahr_type_id = 1;
    //     $status_id = 5;
    //     $emp_type = 'Agent';
    //     $paymentStat = 0;

    // $team_ahr_count = $this->team_count_record($start_date, $end_date, $ahr_type_id, $status_id, $emp_type, $paymentStat);
    // $individual_ahr_count = $this->individual_record_count($start_date, $end_date, $ahr_type_id, $status_id, $emp_type, $paymentStat);
    // $detailed_ahr_record = $this->get_detailed_report_record($start_date, $end_date, $ahr_type_id, $status_id, $emp_type, $paymentStat);

    // var_dump($team_ahr_count);
    // var_dump($individual_ahr_count);
    // var_dump($detailed_ahr_record);

        $type_ahr = "ALL";
        $type_emp = "ALL";
        $stat_payment = "ALL";


        if($ahr_type_id == 1){
            $type_ahr = "AFTER SHIFT";
        }else if($ahr_type_id == 2){
            $type_ahr = "PRE SHIFT";
        }else if($ahr_type_id == 3){
            $type_ahr = "WORD";
        }

        if($status_id == 5){
            $appr_stat = "APPROVED";
        }else if($status_id == 6){
            $appr_stat = "DISAPPROVED";
        }

        if ($emp_type == "Admin") {
            $type_emp = "SZ TEAM";
        } else if ($emp_type == "Agent") {
            $type_emp = "Ambassadors";
        }

        // if ($paymentStat == 1) {
        //     $stat_payment = "RELEASED";
        // } else if ($emp_type == "Agent") {
        //     $stat_payment = "UNRELEASED";
        // }

        // $logo = 'C:\wamp64\www\sz\assets\images\img\logo2.png';
        $logo = $this->dir . '/assets/images/img/logo2.png';

        $this->load->library('PHPExcel', null, 'excel');

        for($sheet_loop = 0; $sheet_loop < 2;  $sheet_loop++){
            $this->excel->createSheet($sheet_loop);
        }
        // ----------------------------------- TEAM COUNT

        $this->excel->setActiveSheetIndex(1);
        $this->excel->getActiveSheet()->setTitle('Team AHR Count');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($logo);
        $objDrawing->setOffsetX(10); // setOffsetX works properly
        $objDrawing->setOffsetY(10); //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(85);// logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->setShowGridlines(false);

        $cells = [
            ['col' => '', 'id' => 'D2', 'title' => 'Additional Hour Count Per Team'],
            ['col' => '', 'id' => 'D3', 'title' => 'Duration: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
            ['col' => '', 'id' => 'D4', 'title' => 'Status: '.$appr_stat],
            ['col' => '', 'id' => 'G3', 'title' => 'Employee Type: '.$type_emp],
            ['col' => '', 'id' => 'G4', 'title' => 'AHR Type: '.$type_ahr],
            ['col' => 'A', 'id' => 'A7', 'title' => 'ENTRY #'],
            ['col' => 'B', 'id' => 'B7', 'title' => 'TEAM'],
            ['col' => 'C', 'id' => 'C7', 'title' => 'TYPE'],
            ['col' => 'D', 'id' => 'D7', 'title' => 'Request #'],
            ['col' => 'E', 'id' => 'E7', 'title' => 'Hours'],
            ['col' => 'F', 'id' => 'F7', 'title' => 'Request #'],
            ['col' => 'G', 'id' => 'G7', 'title' => 'Hours'],
            ['col' => 'H', 'id' => 'H7', 'title' => 'Request #'],
            ['col' => 'I', 'id' => 'I7', 'title' => 'Hours'],
            ['col' => 'J', 'id' => 'J7', 'title' => 'Request #'],
            ['col' => 'K', 'id' => 'K7', 'title' => 'Hours'],
        ];

        $duplicate3 = $cells;

        unset($duplicate3[0], $duplicate3[1], $duplicate3[2], $duplicate3[3], $duplicate3[4]);
        


        $merge_cells = [
            ['id1'=>'D6', 'id2'=>'E6', 'title'=>'PRE-SHIFT'],
            ['id1'=>'F6', 'id2'=>'G6', 'title'=>'AFTER-SHIFT'],
            ['id1'=>'H6', 'id2'=>'I6', 'title'=>'WORD'],
            ['id1'=>'J6', 'id2'=>'K6', 'title'=>'TOTAL'],
        ];


        foreach ($cells as $cell) {
            $this->excel->getActiveSheet()->setCellValue($cell['id'], $cell['title']);
            $this->excel->getActiveSheet()->getStyle($cell['id'])->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle($cell['id'])->getFont()->getColor()->setRGB('FFFFFF');
            $this->excel->getActiveSheet()->getStyle($cell['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $this->excel->getActiveSheet()->getStyle('D2:D2')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('D2:D4')->getFont()->getColor()->setRGB('000000');
        $this->excel->getActiveSheet()->getStyle('G3:G4')->getFont()->getColor()->setRGB('000000');
        $this->excel->getActiveSheet()->getStyle('D2:D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->getStyle('G3:G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


        foreach($merge_cells as $merge_titles){
            $this->excel->getActiveSheet()->mergeCells($merge_titles['id1'].':'.$merge_titles['id2']);
            $this->excel->getActiveSheet()->getCell($merge_titles['id1'])->setValue($merge_titles['title']);
            $this->excel->getActiveSheet()->getStyle($merge_titles['id1'])->getFont()->getColor()->setRGB('FFFFFF');
            $this->excel->getActiveSheet()->getStyle($merge_titles['id1'])->getFont()->setSize(14);
            $this->excel->getActiveSheet()->getStyle($merge_titles['id1'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $this->excel->getActiveSheet()->getStyle('J6')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('J6')->getFont()->getColor()->setRGB('000000');
        $this->excel->getActiveSheet()->getStyle('D7:K7')->getFont()->getColor()->setRGB('000000');

        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(8);

        $this->excel->getActiveSheet()->getStyle('D6:E6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('31869B');
        $this->excel->getActiveSheet()->getStyle('F6:G6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('76933C');
        $this->excel->getActiveSheet()->getStyle('H6:I6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F26800');
        $this->excel->getActiveSheet()->getStyle('J6:K6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC000');
        $this->excel->getActiveSheet()->getStyle('A7:C7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
        $this->excel->getActiveSheet()->getStyle('D7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92CDDC');
        $this->excel->getActiveSheet()->getStyle('E7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DAEEF3');
        $this->excel->getActiveSheet()->getStyle('F7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C4D79B');
        $this->excel->getActiveSheet()->getStyle('G7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D8E4BC');
        $this->excel->getActiveSheet()->getStyle('H7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF9F57');
        $this->excel->getActiveSheet()->getStyle('I7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FCD5B4');
        $this->excel->getActiveSheet()->getStyle('J7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FAE862');
        $this->excel->getActiveSheet()->getStyle('K7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ECFD99');

        $rowNum = 8;
        $entry = 1;
        
        $team_ahr_count = $this->team_count_record($start_date, $end_date, $ahr_type_id, $status_id, $emp_type, $paymentStat);
        $last_row = (count($team_ahr_count) + $rowNum)-1;

        foreach ($team_ahr_count as $rows) {
            $this->excel->getActiveSheet()->setCellValue('A' . $rowNum, $entry);
            $this->excel->getActiveSheet()->setCellValue('B' . $rowNum, $rows['team']);
            $this->excel->getActiveSheet()->setCellValue('C' . $rowNum, $rows['type']);
            $this->excel->getActiveSheet()->setCellValue('D' . $rowNum, $rows['pre_count']);
            $this->excel->getActiveSheet()->setCellValue('E' . $rowNum, $rows['pre_hours']);
            $this->excel->getActiveSheet()->setCellValue('F' . $rowNum, $rows['after_count']);
            $this->excel->getActiveSheet()->setCellValue('G' . $rowNum, $rows['after_hours']);
            $this->excel->getActiveSheet()->setCellValue('H' . $rowNum, $rows['word_count']);
            $this->excel->getActiveSheet()->setCellValue('I' . $rowNum, $rows['word_hours']);
            $this->excel->getActiveSheet()->setCellValue('J' . $rowNum, $rows['total_count']);
            $this->excel->getActiveSheet()->setCellValue('K' . $rowNum, $rows['total_hours']);
            $rowNum++;
            $entry++;
        }

        $this->excel->getActiveSheet()->getStyle('A7:A' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('C7:C' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('D7:D' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E7:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('F7:F' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('G7:G' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('H7:H' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I7:I' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('J7:J' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('K7:K' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B7:B' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('J7:J' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('K7:K' . $last_row)->getFont()->setBold(true);

        $this->excel->getActiveSheet()->freezePane('D8');
        $this->excel->getActiveSheet()->getStyle('A7:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'DDDDDD'),
                    ),
                ),
            )
        );
        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);

        // ------------------------------ INDIVIDUAL COUNT

        $this->excel->setActiveSheetIndex(2);
        $this->excel->getActiveSheet()->setTitle('Individual AHR Count');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($logo);
        $objDrawing->setOffsetX(10); // setOffsetX works properly
        $objDrawing->setOffsetY(10); //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(85); // logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->setShowGridlines(false);

        $cells = [
            ['col' => 'E', 'id' => 'E2', 'title' => 'Additional Hour Count Per Employee'],
            ['col' => 'E', 'id' => 'E3', 'title' => 'Duration: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
            ['col' => 'E', 'id' => 'E4', 'title' => 'Status: '.$appr_stat],
            ['col' => 'G', 'id' => 'G3', 'title' => 'Employee Type: '.$type_emp],
            ['col' => 'G', 'id' => 'G4', 'title' => 'AHR Type: '.$type_ahr],
            ['col' => 'A', 'id' => 'A7', 'title' => 'ENTRY #'],
            ['col' => 'B', 'id' => 'B7', 'title' => 'LASTNAME'],
            ['col' => 'B', 'id' => 'C7', 'title' => 'FIRSTNAME'],
            ['col' => 'B', 'id' => 'D7', 'title' => 'MIDDLENAME'],
            ['col' => 'C', 'id' => 'E7', 'title' => 'TYPE'],
            ['col' => 'D', 'id' => 'F7', 'title' => 'POSITION'],
            ['col' => 'E', 'id' => 'G7', 'title' => 'STATUS'],
            ['col' => 'F', 'id' => 'H7', 'title' => 'TEAM'],
            ['col' => 'G', 'id' => 'I7', 'title' => 'Request #'],
            ['col' => 'H', 'id' => 'J7', 'title' => 'Hours'],
            ['col' => 'I', 'id' => 'K7', 'title' => 'Request #'],
            ['col' => 'J', 'id' => 'L7', 'title' => 'Hours'],
            ['col' => 'K', 'id' => 'M7', 'title' => 'Request #'],
            ['col' => 'L', 'id' => 'N7', 'title' => 'Hours'],
            ['col' => 'M', 'id' => 'O7', 'title' => 'Request #'],
            ['col' => 'N', 'id' => 'P7', 'title' => 'Hours'],
        ];
        
        $duplicate2 = $cells;

        unset($duplicate2[0], $duplicate2[1], $duplicate2[2], $duplicate2[3], $duplicate2[4]);

        $merge_cells = [
            ['id1'=>'I6', 'id2'=>'J6', 'title'=>'PRE-SHIFT'],
            ['id1'=>'K6', 'id2'=>'L6', 'title'=>'AFTER-SHIFT'],
            ['id1'=>'M6', 'id2'=>'N6', 'title'=>'WORD'],
            ['id1'=>'O6', 'id2'=>'P6', 'title'=>'TOTAL'],
        ];

        // $duplicate = $cells;
        // unset($duplicate[0], $duplicate[1], $duplicate[2]);

        foreach ($cells as $cell) {
            $this->excel->getActiveSheet()->setCellValue($cell['id'], $cell['title']);
            $this->excel->getActiveSheet()->getStyle($cell['id'])->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle($cell['id'])->getFont()->getColor()->setRGB('FFFFFF');
            $this->excel->getActiveSheet()->getStyle($cell['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $this->excel->getActiveSheet()->getStyle('E2:E2')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('E2:E4')->getFont()->getColor()->setRGB('000000');
        $this->excel->getActiveSheet()->getStyle('G3:G4')->getFont()->getColor()->setRGB('000000');
        
        $this->excel->getActiveSheet()->getStyle('E2:E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->getStyle('G3:G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


        foreach($merge_cells as $merge_titles){
            $this->excel->getActiveSheet()->mergeCells($merge_titles['id1'].':'.$merge_titles['id2']);
            $this->excel->getActiveSheet()->getCell($merge_titles['id1'])->setValue($merge_titles['title']);
            $this->excel->getActiveSheet()->getStyle($merge_titles['id1'])->getFont()->getColor()->setRGB('FFFFFF');
            $this->excel->getActiveSheet()->getStyle($merge_titles['id1'])->getFont()->setSize(14);
            $this->excel->getActiveSheet()->getStyle($merge_titles['id1'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $this->excel->getActiveSheet()->getStyle('O6')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('O6')->getFont()->getColor()->setRGB('000000');
        $this->excel->getActiveSheet()->getStyle('I7:P7')->getFont()->getColor()->setRGB('000000');

        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(8);

        $this->excel->getActiveSheet()->getStyle('I6:J6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('31869B');
        $this->excel->getActiveSheet()->getStyle('K6:L6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('76933C');
        $this->excel->getActiveSheet()->getStyle('M6:N6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F26800');
        $this->excel->getActiveSheet()->getStyle('O6:P6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC000');
        $this->excel->getActiveSheet()->getStyle('A7:H7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
        $this->excel->getActiveSheet()->getStyle('I7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92CDDC');
        $this->excel->getActiveSheet()->getStyle('J7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DAEEF3');
        $this->excel->getActiveSheet()->getStyle('K7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C4D79B');
        $this->excel->getActiveSheet()->getStyle('L7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D8E4BC');
        $this->excel->getActiveSheet()->getStyle('M7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF9F57');
        $this->excel->getActiveSheet()->getStyle('N7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FCD5B4');
        $this->excel->getActiveSheet()->getStyle('O7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FAE862');
        $this->excel->getActiveSheet()->getStyle('P7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ECFD99');

        $rowNum = 8;
        $entry = 1;

        $individual_ahr_count = $this->individual_record_count($start_date, $end_date, $ahr_type_id, $status_id, $emp_type, $paymentStat);
        $last_row = (count($individual_ahr_count) + $rowNum)-1;

        foreach ($individual_ahr_count as $rows) {
            $this->excel->getActiveSheet()->setCellValue('A' . $rowNum, $entry);
            $this->excel->getActiveSheet()->setCellValue('B' . $rowNum, $rows['lname']);
            $this->excel->getActiveSheet()->setCellValue('C' . $rowNum, $rows['fname']);
            $this->excel->getActiveSheet()->setCellValue('D' . $rowNum, $rows['mname']);
            $this->excel->getActiveSheet()->setCellValue('E' . $rowNum, $rows['type']);
            $this->excel->getActiveSheet()->setCellValue('F' . $rowNum, $rows['position']);
            $this->excel->getActiveSheet()->setCellValue('G' . $rowNum, $rows['emp_stat']);
            $this->excel->getActiveSheet()->setCellValue('H' . $rowNum, $rows['team']);
            $this->excel->getActiveSheet()->setCellValue('I' . $rowNum, $rows['pre_count']);
            $this->excel->getActiveSheet()->setCellValue('J' . $rowNum, $rows['pre_hours']);
            $this->excel->getActiveSheet()->setCellValue('K' . $rowNum, $rows['after_count']);
            $this->excel->getActiveSheet()->setCellValue('L' . $rowNum, $rows['after_hours']);
            $this->excel->getActiveSheet()->setCellValue('M' . $rowNum, $rows['word_count']);
            $this->excel->getActiveSheet()->setCellValue('N' . $rowNum, $rows['word_hours']);
            $this->excel->getActiveSheet()->setCellValue('O' . $rowNum, $rows['total_count']);
            $this->excel->getActiveSheet()->setCellValue('P' . $rowNum, $rows['total_hours']);
            $rowNum++;
            $entry++;
        }
        $this->excel->getActiveSheet()->getStyle('A7:A' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E7:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('G7:G' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I7:I' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('J7:J' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('K7:K' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('L7:L' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('M7:M' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // // $this->excel->getActiveSheet()->getStyle('7:' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('N7:N' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('O7:O' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P7:P' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B7:B' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C7:C' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D7:D' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('O7:O' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P7:P' . $last_row)->getFont()->setBold(true);

        $this->excel->getActiveSheet()->getStyle('A7:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'DDDDDD'),
                    ),
                ),
            )
        );

        $this->excel->getActiveSheet()->freezePane('E8');
        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);

        // ------------------------- DETAILED RECORD

        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Detailed AHR Record');

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($logo);
        $objDrawing->setOffsetX(10); // setOffsetX works properly
        $objDrawing->setOffsetY(10); //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(70); // logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $this->excel->getActiveSheet()->setShowGridlines(false);
       

        $cells = [
            ['col' => '', 'id' => 'D2', 'title' => 'DETAILED AHR RECORD'],
            ['col' => '', 'id' => 'D3', 'title' => 'Duration: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
            ['col' => '', 'id' => 'D4', 'title' => 'Status: '.$appr_stat],
            ['col' => '', 'id' => 'F3', 'title' => 'Employee Type: '.$type_emp],
            ['col' => '', 'id' => 'F4', 'title' => 'AHR Type: '.$type_ahr],
            // ['col' => '', 'id' => 'F5', 'title' => 'Payment Status: '.$stat_payment],
            ['col' => 'A', 'id' => 'A6', 'title' => 'ENTRY #'],
            ['col' => 'B', 'id' => 'B6', 'title' => 'AHR ID'],
            ['col' => 'C', 'id' => 'C6', 'title' => 'DATE & TIME FILED'],
            ['col' => 'D', 'id' => 'D6', 'title' => 'SCHEDULE DATE'],
            ['col' => 'E', 'id' => 'E6', 'title' => 'LASTNAME'],
            ['col' => 'E', 'id' => 'F6', 'title' => 'FIRSTNAME'],
            ['col' => 'E', 'id' => 'G6', 'title' => 'MIDDLENAME'],
            ['col' => 'E', 'id' => 'H6', 'title' => 'TEAM'],
            ['col' => 'F', 'id' => 'I6', 'title' => 'POSITION'],
            ['col' => 'G', 'id' => 'J6', 'title' => 'STATUS'],
            ['col' => 'H', 'id' => 'K6', 'title' => 'COVERAGE START'],
            ['col' => 'H', 'id' => 'L6', 'title' => 'COVERAGE END'],
            ['col' => 'I', 'id' => 'M6', 'title' => '# OF HOURS'],
            ['col' => 'J', 'id' => 'N6', 'title' => 'AHR TYPE'],
            ['col' => 'K', 'id' => 'O6', 'title' => 'FILING TYPE'],
            ['col' => 'M', 'id' => 'P6', 'title' => 'PAYROLL STATUS'],
            ['col' => 'L', 'id' => 'Q6', 'title' => 'REASON'],
        ];

        $duplicate = $cells;

        unset($duplicate[0], $duplicate[1], $duplicate[2], $duplicate[3], $duplicate[4]);
        $this->excel->getActiveSheet()->getStyle('D2:D2')->getFont()->setSize(20);

        foreach ($cells as $cell) {
            $this->excel->getActiveSheet()->setCellValue($cell['id'], $cell['title']);
            $this->excel->getActiveSheet()->getStyle($cell['id'])->getFont()->setBold(true);
        }

        foreach ($duplicate as $dup) {
            $this->excel->getActiveSheet()->getStyle($dup['id'])->getFont()->getColor()->setRGB('FFFFFF');
            $this->excel->getActiveSheet()->getStyle($dup['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $rowNum = 7;
        $entry = 1;

        $detailed_ahr_record = $this->get_detailed_report_record($start_date, $end_date, $ahr_type_id, $status_id, $emp_type, $paymentStat);
        $last_row = (count($detailed_ahr_record) + $rowNum)-1;
        foreach ($detailed_ahr_record as $rows) {
            $m_name = "";
            $ot_type = "After-Shift OT";
            $release_stat = "Unreleased";
            if($rows->mname != ""){
                $m_name = $rows->mname;
            }

            if((int)$rows->additionalHourTypeId == 2){
                $ot_type = "Pre-Shift OT";
            }else if((int)$rows->additionalHourTypeId == 3){
                $ot_type = $rows->ahrTypeDescription;
            }

            if((int)$rows->isReleased == 3){
                $release_stat = "Released";
            }
            $filed = new DateTime($rows->dateTimeFiled);
            $start = new DateTime($rows->otStartDate." ".$rows->otStartTime);
            $end = new DateTime($rows->otEndDate." ".$rows->otEndTime);
            $this->excel->getActiveSheet()->setCellValue('A' . $rowNum, $entry);
            $this->excel->getActiveSheet()->setCellValue('B' . $rowNum, str_pad($rows->additionalHourRequestId, 8, '0', STR_PAD_LEFT));
            $this->excel->getActiveSheet()->setCellValue('C' . $rowNum, $filed->format('d/m/Y g:i A'));
            $this->excel->getActiveSheet()->setCellValue('D' . $rowNum, date_format(date_create($rows->sched_date), "m/d/Y"));
            $this->excel->getActiveSheet()->setCellValue('E' . $rowNum, $rows->lname);
            $this->excel->getActiveSheet()->setCellValue('F' . $rowNum, $rows->fname);
            $this->excel->getActiveSheet()->setCellValue('G' . $rowNum, $m_name);
            $this->excel->getActiveSheet()->setCellValue('H' . $rowNum, $rows->name);
            $this->excel->getActiveSheet()->setCellValue('I' . $rowNum, $rows->positionDescription);
            $this->excel->getActiveSheet()->setCellValue('J' . $rowNum, $rows->empStatus);
            $this->excel->getActiveSheet()->setCellValue('K' . $rowNum, $start->format('d/m/Y  g:i A'));
            $this->excel->getActiveSheet()->setCellValue('L' . $rowNum, $end->format('d/m/Y  g:i A'));
            $this->excel->getActiveSheet()->setCellValue('M' . $rowNum, $rows->totalOtHours);
            $this->excel->getActiveSheet()->setCellValue('N' . $rowNum, $ot_type);
            $this->excel->getActiveSheet()->setCellValue('O' . $rowNum, $rows->requestType);
            $this->excel->getActiveSheet()->setCellValue('P' . $rowNum, $release_stat);
            $this->excel->getActiveSheet()->setCellValue('Q' . $rowNum, $rows->reason);
            $rowNum++;
            $entry++;
        }

        $this->excel->getActiveSheet()->getStyle('A7:A' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B7:B' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('C7:C' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('D7:D' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I7:I' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('J7:J' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('K7:K' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('L7:L' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('M7:M' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('N7:N' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('O7:O' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P7:P' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B7:B' . $last_row)->getFont()->setBold(true);
        
        $this->excel->getActiveSheet()->getStyle('E7:E' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('F7:F' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('G7:G' . $last_row)->getFont()->setBold(true);
        
        $this->excel->getActiveSheet()->getStyle('M7:M' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('M7:M' . $last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFE389');

        $this->excel->getActiveSheet()->getStyle('A6:A' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('B6:B' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('C6:C' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('D6:D' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('E6:E' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('F6:F' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('G6:G' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('H6:H' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('I6:I' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('J6:J' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('K6:K' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('L6:L' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('M6:M' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('N6:N' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('O6:O' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P6:P' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('Q6:Q' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(false);

        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(80);
        $this->excel->getActiveSheet()->getRowDimension('6')->setRowHeight(15);
        $this->excel->getActiveSheet()->getStyle('A6:Q6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');

        $this->excel->getActiveSheet()->freezePane('H7');
        $this->excel->getActiveSheet()->getStyle('A6:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'DDDDDD'),
                    ),
                ),
            )
        );

        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);

        $filename = 'AHR '.date_format(date_create($start_date), "Y-m-d").' to '.date_format(date_create($end_date), "Y-m-d").' '.$appr_stat.'.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');

    }

    // public function get_filtered_report(){
    //     $date_start = $this->input->post("dateStart");
    //     $date_end = $this->input->post("dateEnd");
    //     $ahr_type_id = $this->input->post("ahrTypeId");
    //     $status_id = $this->input->post("statusId");
    //     $emp_type = $this->input->post("employeeType");
    //     $drilldown = [];
    //     $count = 0;
    //     if($emp_type == "Admin"){
    //         $per_admin_account = $this->get_ahr_count_admin_acc($date_start, $date_end, $ahr_type_id, $status_id, $this->input->post('quantityType'));
    //         if($per_admin_account == 0){
    //             $accounts[$count] = 0;
    //             $drilldown[$count] = 0;
    //         }else{
    //             foreach($per_admin_account as $rows){
    //                 $data = $this->get_admin_drilldown($date_start, $date_end, $ahr_type_id, $status_id, $rows->drilldown, $this->input->post('quantityType'));
    //                 $accounts[$count] = [
    //                     "y" => floatval($rows->y),
    //                     "name"=>$rows->name,
    //                     "drilldown"=>$rows->drilldown,
    //                 ];
    //                 $drilldown[$count] = [
    //                     "name"=>$rows->name,
    //                     "id"=>$rows->drilldown,
    //                     "data"=>$data
    //                 ];
    //                 $count++;
    //             }
    //         }
    //     }else{
    //         $per_agent_account = $this->get_ahr_count_agent_acc($date_start, $date_end, $ahr_type_id, $status_id, $this->input->post('quantityType'));
    //         if($per_agent_account == 0){
    //             $accounts[$count] = 0;
    //             $drilldown[$count] = 0;
    //         }else{
    //             foreach($per_agent_account as $rows){
    //                 $data = $this->get_agent_drillDown($date_start, $date_end, $ahr_type_id, $status_id, $rows->drilldown, $this->input->post('quantityType'));
    //                 $accounts[$count] = [
    //                     "y" => floatval($rows->y),
    //                     "name"=>$rows->name,
    //                     "drilldown"=>$rows->drilldown,
    //                 ];
    //                 $drilldown[$count] = [
    //                     "name"=>$rows->name,
    //                     "id"=>$rows->drilldown,
    //                     "data"=>$data
    //                 ];
    //                 $count++;
    //             }
    //         }
    //     }
    //     $report_details['accounts'] = $accounts;
    //     $report_details['drilldown'] = $drilldown;
    //     echo json_encode($report_details);
    // }

    // NOTIFICATION

    // DELETE ---------------------------------------------------------------------------------

}
