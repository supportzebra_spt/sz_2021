<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PositionRates extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('PositionRatesModel');
    }

    public function index() {
        if ($this->session->userdata('uid')) {
            $qry = $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=" . $this->session->userdata('uid') . " and item_link='PositionRates/' and is_assign=1");
            if ($qry->num_rows() > 0) {
                $data['values'] = $this->PositionRatesModel->getPosRates();
                $data['positions'] = $this->PositionRatesModel->getPosStatRates();
                $data['departments'] = $this->PositionRatesModel->getAllDept();
                // $data['days'] = array('1','2','3','4','5','16','17','18','19','20');
                $data['days'] = array();
                $this->load->view('positionrates', $data);
            } else {
                redirect('index.php/home/error');
            }
        } else {
            redirect('index.php/login');
        }
    }

    public function newRate() {
        $sessionuser = $this->session->userdata('uid');
        $position_id = $this->input->post('position_id');
        $status_id = $this->input->post('status_id');
        $rate = $this->input->post('rate');
        $effecdate = $this->input->post('effecdate');
        $d = explode('-', $effecdate);
        $date_effect = $d[2] . '-' . $d[0] . '-' . $d[1];
        $date = DATE('Y-m-d');

        $posempstat_id = $this->PositionRatesModel->getPosEmpStatID($position_id, $status_id);
        if ($posempstat_id != '') {
            $posempstat_id = $posempstat_id->posempstat_id;
        } else {

            $data = array(
                'pos_id' => $position_id,
                'empstat_id' => $status_id,
                'date' => $date,
                'isActive' => 1
            );
            $posempstat_id = $this->PositionRatesModel->insertPosEmpStat($data);
        }

        $data2 = array(
            'posempstat_id' => $posempstat_id,
            'rate' => $rate,
            'date_effect' => $date_effect,
            'isActive' => 1,
            'date_added' => $date,
            'added_by' => $sessionuser
        );

        $rows = $this->PositionRatesModel->insertNewRate($data2);

        if ($rows > 0) {
            echo 'Success';
            $this->session->set_flashdata('success', 'Successfully Added New Rate');
        } else {
            echo 'Failed';
        }
    }

    public function updateActiveRate() {
        $rate_id = $this->input->post('rate_id');
        $activevalue = $this->input->post('activevalue');
        $rows = $this->PositionRatesModel->updateActiveRate($rate_id, $activevalue);
        if ($rows > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    public function getSimilarRateValue() {
        $status_id = $this->input->post('status_id');
        $position_id = $this->input->post('position_id');
        $ratevalue = $this->input->post('ratevalue');
        $posempstat_id = $this->PositionRatesModel->getPosEmpStatID($position_id, $status_id);
        if ($posempstat_id != '' || $posempstat_id != NULL) {
            $rates = $this->PositionRatesModel->getRates($posempstat_id->posempstat_id, $ratevalue);
            echo json_encode($rates);
            // echo $posempstat_id->posempstat_id;
        } else {
            echo 'Empty';
        }
        // echo $position_id.' '.$status_id.' '.$ratevalue;
    }

    public function activateExistingRate() {
        $position_id = $this->input->post('position_id');
        $status_id = $this->input->post('status_id');
        $rate = $this->input->post('rate');
        $rate_id = $this->PositionRatesModel->getRateId($status_id, $position_id, $rate);
        $rows = $this->PositionRatesModel->updateActiveRate($rate_id->rate_id, 1);
        if ($rows > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    //--------------------------------START OF NEW CODES-------------------------------------------------------------------------------
//--------------------------------START OF NEW CODES-------------------------------------------------------------------------------
    public function positionAllowance() {

        $data['allowance'] = $this->PositionRatesModel->getPositionAllowance();
        $data['positions'] = $this->PositionRatesModel->getPosStatAllowance();
        $data['departments'] = $this->PositionRatesModel->getAllDept();
        $this->load->view('positionAllowance', $data);
    }

    public function updatePositionAllowance() {
        $value = $this->input->post('value');
        $allowanceid = $this->input->post('allowanceid');
        $allowancename = $this->input->post('allowancename');
        $posempstatid = $this->input->post('posempstatid');

        $rows = $this->PositionRatesModel->updateActiveAllowance($allowanceid, 0);
        if ($rows > 0) {
            $rows = $this->PositionRatesModel->insertNewAllowance($allowancename, $posempstatid, $value);
            if ($rows > 0) {
                echo 'Success';
            } else {
                $rows = $this->PositionRatesModel->updateActiveAllowance($allowanceid, 1);
                echo 'Failed';
            }
        } else {
            echo 'Failed2';
        }
    }

    public function insertNewAllowance() {

        $position = $this->input->post('position');
        $status = $this->input->post('status');
        $clothing = $this->input->post('clothing');
        $laundry = $this->input->post('laundry');
        $rice = $this->input->post('rice');

        $posempstatid = $this->PositionRatesModel->getPosEmpStatID($position, $status);
        if ($posempstatid != NULL || $posempstatid != '') {
            $id = $posempstatid->posempstat_id;
            $this->db->where('posempstat_id', $id);
            $this->db->update('tbl_allowance', array('isActive' => 0));
        } else {
            $id = $this->PositionRatesModel->insertNewPosEmpStat($position, $status);
        }

        $a1 = $this->PositionRatesModel->insertNewAllowance('Clothing', $id, $clothing);
        $a2 = $this->PositionRatesModel->insertNewAllowance('Laundry', $id, $laundry);
        $a3 = $this->PositionRatesModel->insertNewAllowance('Rice', $id, $rice);
        if ($a1 > 0 && $a2 > 0 && $a3 > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    public function employeepositions() {

        $data['positions'] = $this->PositionRatesModel->getPositions();
        $data['values'] = $this->PositionRatesModel->getEmpPos();
        $this->load->view('employeepositions', $data);
    }

    public function insertNewEmpPosition() {

        $posempstat_id = $this->input->post('posempstat_id');
        $emp_id = $this->input->post('emp_id');
        $query = $this->db->query("SELECT emp_promoteID FROM tbl_emp_promote where emp_id='" . $emp_id . "' AND isActive=1");
        $result = $query->result();
        if ($this->db->affected_rows() > 0) {
            foreach ($result as $r) {
                $this->db->where('emp_promoteID', $r->emp_promoteID);
                $this->db->update('tbl_emp_promote', array('isActive' => '0'));
            }
        }
        $data = array('posempstat_id' => $posempstat_id, 'emp_id' => $emp_id, 'isActive' => '1');
        $this->db->insert('tbl_emp_promote', $data);
        // echo $this->db->last_query();
        if ($this->db->affected_rows() > 0) {
            echo 'Success';
        } else {
            echo 'Fail';
        }
    }

    public function bonus() {
        $data['values'] = $this->PositionRatesModel->getAllBonus();
        $this->load->view('bonus', $data);
    }

    public function newBonus() {
        $bonusname = $this->input->post('bonusname');
        $description = $this->input->post('description');
        $data = array(
            'bonus_name' => $bonusname,
            'description' => $description,
            'isActive' => 1
        );
        $rows = $this->PositionRatesModel->newBonus($data);
        if ($rows > 0) {
            echo 'Success';
            $this->session->set_flashdata('success', 'Successfully Added New Bonus');
        } else {
            echo 'Failed';
        }
    }

    public function updateActiveBonus() {
        $bonus_id = $this->input->post('bonus_id');
        $activevalue = $this->input->post('activevalue');
        $rows = $this->PositionRatesModel->updateActiveBonus($bonus_id, $activevalue);
        if ($rows > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    public function positionbonus() {
        if ($this->session->userdata('uid')) {
            $qry = $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=" . $this->session->userdata('uid') . " and item_link='PositionRates/positionbonus'");
            if ($qry->num_rows() > 0) {
                $data['count'] = $this->PositionRatesModel->countBonus();
                $data['bonus'] = $this->PositionRatesModel->getPositionBonus();
                $data['positions'] = $this->PositionRatesModel->getPosStatBonus();
                $data['departments'] = $this->PositionRatesModel->getAllDept();
                $data['bonuslist'] = $this->PositionRatesModel->getBonusList();
                $this->load->view('positionbonus', $data);
            } else {
                redirect('index.php/home/error');
            }
        } else {
            redirect('index.php/login');
        }
    }

    public function insertNewBonus() {
        $position = $this->input->post('position');
        $status = $this->input->post('status');
        $bonusid = $this->input->post('bonusid');
        $amount = $this->input->post('amount');

        $posempstat = $this->PositionRatesModel->getPosEmpStatID($position, $status);
        $data = array(
            'posempstat_id' => $posempstat->posempstat_id,
            'bonus_id' => $bonusid,
            'amount' => $amount,
            'isActive' => 1
        );
        $rows = $this->PositionRatesModel->insertPosBonus($data);
        if ($rows > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    public function updatePositionBonus() {
        $posempstatid = $this->input->post('posempstatid');
        $pbonusid = $this->input->post('pbonusid');
        $bonusid = $this->input->post('bonusid');
        $amount = $this->input->post('amount');

        $rows = $this->PositionRatesModel->deactivatePositionBonus($pbonusid);
        if ($rows > 0) {
            $rows2 = $this->PositionRatesModel->updatePositionBonus($posempstatid, $bonusid, $amount);
            if ($rows2 > 0) {
                echo 'Success';
            } else {
                echo 'Failed2';
            }
        } else {
            echo 'Failed';
        }
    }

    public function getPosAvailableBonus() {
        $posempstatid = $this->input->post('posempstatid');
        $bonusids = $this->input->post('bonusids');
        $ids = explode('|', $bonusids);
        $data = $this->PositionRatesModel->getPosAvailableBonus($posempstatid, $ids);
        if ($data == NULL || $data == '') {
            echo 'Empty';
        } else {
            echo json_encode($data);
        }
    }

    public function insertNewBonus2() {
        $posempstatid = $this->input->post('posempstatid');
        $bonusid = $this->input->post('bonusid');
        $amount = $this->input->post('amount');
        $data = array(
            'posempstat_id' => $posempstatid,
            'bonus_id' => $bonusid,
            'amount' => $amount,
            'isActive' => 1
        );
        $rows = $this->PositionRatesModel->insertPosBonus($data);
        if ($rows > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    //NEW CODES------------------------------------------------------
    public function employeepositionshistory() {
        if ($this->session->userdata('uid')) {
            $employees = $this->PositionRatesModel->getEmployeelist();
            foreach ($employees as $emp) {
                $emp->history = $this->PositionRatesModel->getEmpPosHistory($emp->emp_id);
            }
            $data['values'] = $employees;
            $data['departments'] = $this->PositionRatesModel->getAllDept();
            $data['positions'] = $this->PositionRatesModel->getPosStatAll();
            $this->load->view('employeepositionshistory', $data);
        } else {
            redirect('index.php/login');
        }
    }

    public function updateemppromotehist() {
        $date = $this->input->post('dater');
        $emp_promoteid = $this->input->post('emp_promoteid');
        $row = $this->PositionRatesModel->updateemppromotehist($date, $emp_promoteid);
        if ($row > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }

    public function newpositionhistory() {
        $empid = $this->input->post('empid');
        $position = $this->input->post('position');
        $status = $this->input->post('status');
        $date = $this->input->post('date');
        $posempstat = $this->PositionRatesModel->getPosEmpStatID($position, $status);
        if (empty($posempstat)) {
            echo 'Failed2';
            // echo $position.' '.$status;
        } else {
            $row = $this->PositionRatesModel->insertnewpositionhistory($empid, $posempstat->posempstat_id, $date);
            if ($row > 0) {
                echo 'Success';
            } else {
                echo 'Failed';
            }
        }
    }

    public function employeeATM() {
        $data['values'] = $this->PositionRatesModel->getEmployeeATM();
        $this->load->view('employeeATM', $data);
    }

    public function updateATM() {
        $emp_id = $this->input->post('emp_id');
        $activevalue = $this->input->post('activevalue');
        $row = $this->PositionRatesModel->updateATM($emp_id, $activevalue);
        if ($row > 0) {
            echo "Success";
        } else {
            echo "Failed";
        }
    }
	public function updateATMdetails() { // MArk
        $emp_id = $this->input->post('emp_id');
        $value = $this->input->post('value');
        $row = $this->PositionRatesModel->updateATMdetails($emp_id, $value);
		// echo $row;
		
        if ($row > 0) {
            echo "Success";
        } else {
            echo "Failed";
        }
    }

    public function disableHistory() {
        $emp_promoteid = $this->input->post('emp_promoteid');
        $row = $this->PositionRatesModel->disableHistory($emp_promoteid);
        if ($row > 0) {
            echo "Success";
        } else {
            echo "Failed";
        }
    }

    public function positions() {
        if ($this->session->userdata('uid')) {
            $qry = $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=" . $this->session->userdata('uid') . " and item_link='PositionRates/positions' and is_assign=1 ");
            if ($qry->num_rows() > 0) {
                $data['departments'] = $this->PositionRatesModel->getAllDept();
                $data['positions'] = $this->PositionRatesModel->getAllPositionWithDept();
                $this->load->view('positions', $data);
            } else {
                redirect('index.php/home/error');
            }
        }
    }

    public function createnewposition() {
        $code = $this->input->post('code');
        $position = $this->input->post('position');
        $classification = $this->input->post('classification');
        $isHiring = $this->input->post('isHiring');
        $dep_id = $this->input->post('department');
        $pos_id = $this->PositionRatesModel->createnewposition($code, $position, $classification, $isHiring);
        if ($pos_id != NULL) {
            $row = $this->PositionRatesModel->createPosDep($dep_id, $pos_id);
            if ($row > 0) {
                echo "Success";
            } else {
                echo "Failed";
            }
        } else {
            echo "Failed";
        }
    }

    public function updateposition() {
        $pos_id = $this->input->post('pos_id');
        $code = $this->input->post('code');
        $position = $this->input->post('position');
        $classification = $this->input->post('classification');
        $isHiring = $this->input->post('isHiring');
        $dep_id = $this->input->post('department');
        $row = $this->PositionRatesModel->updateposition($pos_id, $code, $position, $classification, $isHiring);
        if ($row > 0) {
            $okay = true;
        } else {
            $okay = false;
        }
        $pos_dep_id = $this->PositionRatesModel->getPosDep($pos_id);
        $row2 = $this->PositionRatesModel->updatePosDep($pos_dep_id->pos_dep_id, $dep_id);
        if ($row2 > 0) {
            $okay2 = true;
        } else {
            $okay2 = false;
        }
        if ($okay || $okay2) {
            echo "Success";
        } else {
            echo "Failed";
        }
    }

    public function deactivateposition() {
        $pos_id = $this->input->post('pos_id');
        $row = $this->PositionRatesModel->deactivateposition($pos_id);
        if ($row > 0) {
            echo "Success";
        } else {
            echo "Failed";
        }
    }

}
