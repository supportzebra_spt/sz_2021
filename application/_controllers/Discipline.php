<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Discipline extends General
{
    public function cod_settings(){
         $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'COD Settings',
        ];
        // if($this->check_access()){
            $this->load_template_view('templates/dms/cod_settings', $data);
        // }        
    }
       
    // CREATE -----------------------------------------------------------------------------------
    public function add_disciplinary_action(){
        $data['action'] = $this->input->post("action");
        $data['abbreviation'] = $this->input->post("abbr");
        $data['periodMonth'] = $this->input->post("period");
        $data['changedBy'] = $this->session->userdata('emp_id');
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_disciplinary_action');
        echo json_encode($insert_stat);
    }

    public function add_category(){
        $data['category'] = strtolower($this->input->post("category"));
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_discipline_category');
        echo json_encode($insert_stat);
    }
    
    public function add_offense_level(){
        $disciplinary_action_id = $this->input->post("disciplinaryActionId");
        $max_level = 0;
        $max_offense_level = $this->get_max_offense_level($this->input->post("categoryId"));        
        if($max_offense_level->level !== NULL){
            $max_level = $max_offense_level->level;
        }
        $data['disciplineCategory_ID'] = $this->input->post("categoryId");
        $data['disciplinaryAction_ID'] = $this->input->post("disciplinaryActionId");
        $data['level'] = $max_level+1;
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_disciplinary_action_category');
        echo json_encode($insert_stat);
    }

    public function add_offense_category(){
        $data['letter'] = strtolower($this->input->post("categoryLetter"));
        $data['offenseType'] = $this->input->post("offenseCategory");
        $data['changedBy'] = $this->session->userdata('emp_id');
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_offense_type');
        echo json_encode($insert_stat);
    }

    public function add_offense(){
        if($this->check_if_order_num_exist($this->input->post("offenseNum"), $this->input->post("offenseCategoryId"))){
            $this->insert_num_order($this->input->post("offenseNum")-1 , $this->input->post("offenseCategoryId"));
        }
        $data['orderNum'] = $this->input->post("offenseNum");
        $data['offenseType_ID'] = $this->input->post("offenseCategoryId");
        // $data['disciplineCategory_ID'] = $this->input->post("disciplineCategoryId");
        $data['offense'] = $this->input->post("offense");
        $data['multiCategory'] = $this->input->post("multiCateg");
        $data['multiCategoryLetters'] = $this->input->post("disciplineCategoryLetter");
        $data['changedBy'] = $this->session->userdata('emp_id');
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_offense');
        echo json_encode($insert_stat);
    }

    public function add_default_violation_settings($dtr_violation_type_id){
        $offense = $this->get_offense_for_dtr_vio_add_settings();
        // if(count($offense) > 0){
        //     $data['offense_ID'] = $offense->offense_ID;
        // }
        $data['dtrViolationType_ID'] = $dtr_violation_type_id;
        $data['changedBy'] = $this->session->userdata('emp_id');
        return $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dtr_violation_type_settings');
    }

    public function add_default_ir_deadline_settings(){
        $data['deadlineToExplain'] = 1;
        $data['deadlineToExplainOption'] = 1;
        $data['deadlineToRecommend'] = 1;
        $data['deadlineToRecommendOption'] = 1;
        $data['deadlineLastRecommendation'] = 1;
        $data['deadlineFinalDecision'] = 1;
        $data['deadlineUploadDocs'] = 1;
        $data['changedBy'] = $this->session->userdata('emp_id');
        return $this->general_model->insert_vals($data, 'tbl_incident_report_deadline_settings');
    }

    public function assign_qip(){
        $qip_emp_list = $this->input->post('qipEmpList');
        for($loop = 0; $loop < count($qip_emp_list); $loop++){
            $data[$loop] = [
                'emp_id' => $qip_emp_list[$loop],
                'createdBy' => $this->session->userdata('emp_id')
            ];
        }
        $insert_stat = $this->general_model->batch_insert($data, 'tbl_qip_assignment');
        echo json_encode($insert_stat);
    }

    public function assign_super_ero($emp_id){
        $assigned_with_sup = $this->check_if_assigned_with_sup($emp_id);
        if(count($assigned_with_sup)>0){
            $assign_sup = 2;
        }else{
            $user_details = $this->get_user_by_emp_id($emp_id);
            if(count($user_details) > 0){
                $sup_user_details = $this->get_direct_supervisor_emp($user_details->uid);
                $data['employeeRelationsSupervisor'] = $sup_user_details->Supervisor;
                $data['emp_id'] = $emp_id;
                $assign_sup = $this->general_model->insert_vals($data, 'tbl_employee_relations_supervisor');
            }else{
                $assign_sup = 0;
            }
        }
    }

    public function assign_ero(){
        $account_ids = $this->input->post('accountIds');
        $site_id = $this->input->post('siteId');
        $emp_id = $this->input->post('empId');
        $insert_stat['assign_ero_sup'] = $this->assign_super_ero($emp_id);
        for($loop = 0; $loop < count($account_ids); $loop++){
            $data[$loop] = [
                'acc_id' => $account_ids[$loop],
                'site_ID' => $site_id,
                'emp_ID' => $emp_id,
                'createdBy' => $this->session->userdata('emp_id'),
            ];
        }
        $insert_stat['assign_ero'] = $this->general_model->batch_insert($data, 'tbl_employee_relations_officer');
        echo json_encode($insert_stat);
    }

    public function assign_offense_type(){
        $acc_id = $this->input->post('accId');
        $offense_type_id = $this->input->post('offenseTypeId');
        for($loop = 0; $loop < count($offense_type_id); $loop++){
            $data[$loop] = [
                'offenseType_ID' => $offense_type_id[$loop],
                'acc_id' => $acc_id,
                'createdBy' => $this->session->userdata('emp_id'),
            ];
        }
        $insert_stat['assign_offense_type'] = $this->general_model->batch_insert($data, 'tbl_offense_type_assignment');
        echo json_encode($insert_stat);
    }

    private function add_user_dtr_violation_details($user_dtr_violation_details){
        return $this->general_model->batch_insert($user_dtr_violation_details, 'tbl_user_dtr_violation_details');
    }

    private function add_user_dtr_violation_related_details($user_dtr_violation_related_details){
        return $this->general_model->batch_insert($user_dtr_violation_related_details, 'tbl_user_dtr_violation_related_details');
    }

    private function add_qualified_user_dtr_violation($data){
        return $this->general_model->insert_vals_last_inserted_id($data, 'tbl_qualified_user_dtr_violation');
    }

    // public function test_time(){
    //     $shift_in= strtotime("2019-02-2 12:00 AM");
    //     $log_in = strtotime("2019-02-1 11:30 PM");
    //     echo  ($shift_in - $log_in)/3600;

    // }
    // END OF CREATE -----------------------------------------------------------------------------------

    // READ -----------------------------------------------------------------------------------
    public function get_disciplinary_action(){
        $fields = "disciplinaryAction_ID, action, periodMonth, abbreviation";
        $where = "disciplinaryAction_ID IS NOT NULL";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_disciplinary_action");
        echo json_encode($record);
    }

    public function get_specific_diciplinary_action(){
        $disciplinary_action_id = $this->input->post('disciplinaryActionId');
        $fields = "disciplinaryAction_ID, action, periodMonth, abbreviation";
        $where = "disciplinaryAction_ID = $disciplinary_action_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_disciplinary_action");
        if(count($record) < 0){
            echo json_encode(0);
        }else{
            echo json_encode($record);
        }
    }

    private function get_discipline_category(){
        $fields = "disciplineCategory_ID, category";
        $where = "disciplineCategory_ID IS NOT NULL";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_discipline_category");
    }

    private function get_specific_discipline_category($discipline_category_id){
        $fields = "disciplineCategory_ID, category";
        $where = "disciplineCategory_ID = $discipline_category_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_discipline_category");
    }

    private function get_offense_per_category($discipline_category_id){
        $fields = "discActionCateg.disciplinaryActionCategory_ID, discCateg.category, discAction.action, discActionCateg.level";
        $where = "discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discActionCateg.disciplineCategory_ID = $discipline_category_id";
        $table = "tbl_disciplinary_action_category discActionCateg, tbl_discipline_category discCateg, tbl_disciplinary_action discAction";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "level", "asc");
    }

    public function get_offense_category(){
        $discipline_category = $this->get_discipline_category();
        $count = 0;
        foreach($discipline_category as $rows){
            $offense_per_category = $this->get_offense_per_category($rows->disciplineCategory_ID);
            if(count($offense_per_category)>0){
                $offense_category[$count] = [
                    "disciplineCategory_ID" =>$rows->disciplineCategory_ID,
                    "category"=> $rows->category,
                    "offense_per_category" => $this->get_offense_per_category($rows->disciplineCategory_ID),
                    "offense_per_category_count" => count($offense_per_category)
                ];
                
            }else{
                $offense_category[$count] = [
                    "disciplineCategory_ID" =>$rows->disciplineCategory_ID,
                    "category"=> $rows->category,
                    "offense_per_category" => '',
                    "offense_per_category_count" => count($offense_per_category)
                ];
            }
            $count++;
        }
        if(count($offense_category)>0){
            echo json_encode($offense_category);
        }else{
            echo json_encode([]);
        }
    }

    public function get_max_offense_level($category_id){
        $fields = "MAX(level) level";
        $where = "disciplineCategory_ID = $category_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_disciplinary_action_category");
    }

    public function get_unused_disciplinary_action($category_id){
        $query = "SELECT disciplinaryAction_ID, action, abbreviation FROM tbl_disciplinary_action WHERE disciplinaryAction_ID NOT IN (SELECT disciplinaryAction_ID FROM tbl_disciplinary_action_category WHERE disciplineCategory_ID = $category_id)";
        return $record = $this->general_model->custom_query($query);
    }

    public function get_add_offense_level_form_details(){
        $category_id = $this->input->post('categoryId');
        $unused_disc_action = $this->get_unused_disciplinary_action($category_id);
        $max_offense_level = $this->get_max_offense_level($category_id);
        if(count($unused_disc_action) > 0){
            $disciplinary_action['record'] = $unused_disc_action;
        }else{
            $disciplinary_action['record'] = 0;
        }
        if($max_offense_level->level === NULL){
            $disciplinary_action['max_level'] = 0;
        }else{
            $disciplinary_action['max_level'] = $max_offense_level->level;
        }
        echo json_encode($disciplinary_action); 
    }

    public function get_max_category(){
        $fields = "disciplineCategory_ID, category";
        $where = "disciplineCategory_ID IS NOT NULL";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_discipline_category", "category", "desc");
        if(count($record)>0){
            $max['exist'] = 1;
            $max['record'] = $record;
        }else{
            $max['exist'] = 0;
            $max['record'] = 0;
        }
        echo json_encode($max);
    }

    public function get_max_offense_category(){
        $fields = "offenseType_ID, letter";
        $where = "offenseType_ID IS NOT NULL";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_offense_type", "letter", "desc");
        if(count($record) > 0){
            $max['exist'] = 1;
            $max['record'] = $record;
        }else{
            $max['exist'] = 0;
            $max['record'] = 0;
        }
        echo json_encode($max);
    }

    public function check_if_category_exist(){
        $category = strtolower($this->input->post('category'));
        $fields = "disciplineCategory_ID";
        $where = "category = '$category'";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_discipline_category");
        if (count($record) > 0) {
            $validation['valid'] = false;
        } else {
            $validation['valid'] = true;
        }
        echo json_encode($validation);
    }

    public function check_if_offense_category_exist(){
        $category = strtolower($this->input->post('categoryLetter'));
        $fields = "offenseType_ID";
        $where = "letter = '$category'";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_offense_type");
        if (count($record) > 0) {
            $validation['valid'] = false;
        } else {
            $validation['valid'] = true;
        }
        echo json_encode($validation);
    }

    private function check_if_category_exist_offense($category_id){
        $fields = "offense_ID";
        $where = "multiCategory LIKE '%$category_id%'";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_offense");
    }

    private function get_offense_with_spec_category($category_id){
        $fields = "offense_ID, multiCategory, multiCategoryLetters";
        $where = "multiCategory LIKE '%$category_id%'";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_offense");
    }

    public function check_if_category_has_offenses(){
        $category_id = $this->input->post('categoryId');
        $record = $this->get_offense_with_spec_category($category_id);
        if(count($record) > 0){
            $exist['exist'] = 1; 
            $exist['offense_count'] = count($record); 
        }else{
            $exist['exist'] = 0;
            $exist['offense_count'] = count($record);
        }
        echo json_encode($exist);
    }

    private function get_offenses_under_offense_type($offense_category_id){
        $fields = "offense_ID";
        $where = "offenseType_ID = $offense_category_id";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_offense");
    }

    public function check_if_category_type_has_offenses(){
        $offense_category_id = $this->input->post('offenseCategoryId');
        $record = $this->get_offenses_under_offense_type($offense_category_id);
        if(count($record) > 0){
            $exist['exist'] = 1; 
            $exist['offense_count'] = count($record); 
        }else{
            $exist['exist'] = 0;
            $exist['offense_count'] = count($record);
        }
        echo json_encode($exist);
    }

    public function check_if_disc_action_has_category(){
        $disciplinary_action_id = $this->input->post('disciplinaryActionId');
        $fields = "disciplinaryActionCategory_ID";
        $where = "disciplinaryAction_ID = $disciplinary_action_id";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_disciplinary_action_category");
        if(count($record) > 0){
            $exist['exist'] = 1; 
            $exist['disciplinary_action_count'] = count($record); 
        }else{
            $exist['exist'] = 0;
            $exist['disciplinary_action_count'] = count($record);
        }
        echo json_encode($exist);
    }

    private function check_if_assigned_with_sup($emp_id){
        $fields = "employeeRelationSupervisor_ID";
        $where = "emp_id = $emp_id";
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_employee_relations_supervisor");
    }

    public function get_specific_offense_type(){
        $fields = "DISTINCT(offenseCateg.offenseType_ID) offenseType_ID, offenseCateg.letter, offenseCateg.offenseType";
        $where = "offenseCateg.offenseType_ID = offense.offenseType_ID";
        $table = "tbl_offense offense, tbl_offense_type offenseCateg";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "offenseCateg.letter", "ASC");
        if(count($record) > 0){
            $exist['exist'] = 1; 
            $exist['record'] = $record; 
        }else{
            $exist['exist'] = 0;
            $exist['record'] = 0;
        }
        echo json_encode($exist);
    }

    public function get_default_offense(){
        $offense_category_id = $this->input->post("offenseCategoryId");
        if((int)$offense_category_id > 0){
            $where = "offenseType_ID = $offense_category_id";
        }else{
            $where = "offenseType_ID IS NOT NULL";
        }
        $fields = "offense_ID, offense, offenseType_ID, multiCategory, multiCategoryLetters";;
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_offense", 'offenseType_ID', 'orderNum ASC');
        if(count($record) > 0){
            $exist['exist'] = 1; 
            $exist['record'] = $record; 
        }else{
            $exist['exist'] = 0;
            $exist['record'] = 0;
        }
        echo json_encode($exist);
        // var_dump($exist);
    }

    public function get_all_specific_offense_category(){
        $search_offense_category = $this->input->post('searchOffenseCategory');
        $offense_categ_description = "";
        if(!empty($search_offense_category)){
            $offense_categ_description = "AND (offenseType LIKE '%".$search_offense_category."%' OR letter LIKE '%".$search_offense_category."%')";
        }
        $fields = "offenseType_ID, letter, offenseType";;
        $where = "offenseType_ID IS NOT NULL $offense_categ_description";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_offense_type", 'letter', 'ASC');
        if(count($record) > 0){
            $exist['exist'] = 1; 
            $exist['spec_offense_category'] = $record; 
        }else{
            $exist['exist'] = 0;
            $exist['spec_offense_category'] = 0;
        }
        echo json_encode($exist);
    }

    public function get_one_specific_offense_category(){
        $offense_category_id = $this->input->post('offenseCategoryId');
        $fields = "offenseType_ID, letter, offenseType";
        $where = "offenseType_ID = $offense_category_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_offense_type");
        if(count($record) > 0){
            $exist['exist'] = 1; 
            $exist['spec_offense_category'] = $record; 
        }else{
            $exist['exist'] = 0;
            $exist['spec_offense_category'] = 0;
        }
        echo json_encode($exist);
    }

    public function get_all_offense_category(){
        $fields = "DISTINCT(discActionCateg.disciplineCategory_ID), discCateg.category";
        $where = "discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID";
        $table = "tbl_discipline_category discCateg, tbl_disciplinary_action_category discActionCateg";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'discCateg.category', 'ASC');
        if(count($record) > 0){
            $exist['exist'] = 1; 
            $exist['spec_offenses'] = $record; 
        }else{
            $exist['exist'] = 0;
            $exist['spec_offenses'] = 0;
        }
        echo json_encode($exist);
    }

    public function get_max_offense_order(){
        $offense_category_id = $this->input->post('offenseCategoryId');
        $fields = "MAX(orderNum) orderNum";
        $where = "offenseType_ID = $offense_category_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_offense");
        if($record->orderNum != NULL){
            $max_order['max_order'] = $record->orderNum + 1; 
        }else{
            $max_order['max_order'] = 1;
        }
        echo json_encode($max_order);
    }
    private function get_specific_offense_category($offense_category_id){
        $fields = "letter, offenseType, dateCreated, dateChanged, changedBy";
        $where = "offenseType_ID = $offense_category_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_offense_type");
    }

    private function get_specific_offenses($offense_category_id){
        $fields = "offense_ID, offense, offenseType_ID, orderNum";
        $where = "offenseType_ID = $offense_category_id";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_offense");
        if(count($record) > 0){
            $offense['exist'] = 1; 
            $offense['record'] = $record; 
        }else{
            $offense['exist'] = 0;
            $offense['record'] = 0;
        }
        return $offense;
    }
    private function get_offense($offense_id){
        $fields = "offense_ID, offense, multiCategory, offenseType_ID, orderNum, dateCreated, dateChanged, changedBy";
        $where = "offense_ID = $offense_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_offense");
    }

    public function get_specific_offense(){
        $offense_id = $this->input->post('offenseId');
        $record = $this->get_offense($offense_id);
        if(count($record) > 0){
            $offense['exist'] = 1; 
            $offense['record'] = $record; 
        }else{
            $offense['exist'] = 0;
            $offense['record'] = 0;
        }
        echo json_encode($offense);
    }

    public function get_selected_offense_category_details(){
        $offense_category_id = $this->input->post('offenseCategoryId');
        $details['category'] = $this->get_specific_offense_category($offense_category_id);
        $details['offenses'] = $this->get_specific_offenses($offense_category_id);
        echo json_encode($details);
    }

    public function distinct_offense_category(){
        $offense_category_id = $this->input->post('offenseCategoryId');
        $fields = "DISTINCT(off.disciplineCategory_ID), discCateg.category";
        $where = "discCateg.disciplineCategory_ID = off.disciplineCategory_ID AND off.offenseType_ID = $offense_category_id";
        $table = "tbl_offense off, tbl_discipline_category discCateg";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'discCateg.category', 'ASC');
        if(count($record) > 0){
            $offense['exist'] = 1; 
            $offense['record'] = $record; 
        }else{
            $offense['exist'] = 0;
            $offense['record'] = 0;
        }
        echo json_encode($offense);
    }

    public function get_offenses_datatable(){
        $datatable = $this->input->post('datatable');
        $offense_category_id = $datatable['query']['offenseCategoryId'];
        // $discipline_category_id = $datatable['query']['disciplineCategoryId'];
        $disc_where = "";
        // if((int) $discipline_category_id !== 0){
        //     $disc_where = "AND off.disciplineCategory_ID = $discipline_category_id";
        // }
        $query['query'] = "SELECT off.offense_ID, off.orderNum, off.offense, off.offenseType_ID, off.multiCategory, off.multiCategoryLetters FROM tbl_offense off WHERE off.offenseType_ID = $offense_category_id";
        if ($datatable['query']['offenseSearchField'] != '') {
            $keyword = $datatable['query']['offenseSearchField'];
            $query['search']['append'] = " AND off.offense LIKE '%" . $keyword . "%'";
            $query['search']['total'] = " AND off.offense LIKE '%" . $keyword . "%'";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    private function check_if_order_num_exist($order_num, $offense_category_id){
        $fields = "offense_ID";
        $where = "orderNum = $order_num AND offenseType_ID = $offense_category_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_offense');
        if(count($record)){
            return 1;
        }else{
            return 0;
        }
    }

    private function get_dtr_violation_settings($dtr_violation_type_id){
        $fields = "dtrVioSettings.dtrViolationTypeSettings_ID, dtrVioSettings.dtrViolationType_ID, dtrVioSettings.oneTimeStat, dtrVioSettings.oneTimeVal, dtrVioSettings.consecutiveStat, dtrVioSettings.consecutiveVal, dtrVioSettings.nonConsecutiveStat, dtrVioSettings.nonConsecutiveVal, dtrVioSettings.durationType, dtrVioSettings.durationVal, dtrVioSettings.durationUnit, dtrVioSettings.offense_ID, dtrVioSettings.daysDeadline, dtrVioSettings.deadlineOption";
        $where = "dtrVioSettings.dtrViolationType_ID = $dtr_violation_type_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dtr_violation_type_settings dtrVioSettings");       
    }

    public function get_previous_user_dtr_violation($user_dtr_violation_id, $user_id, $violation_type){
        $fields = "userDtrViolation_ID, user_ID, supervisor_ID, dtrViolationType_ID, sched_ID, month, year, sched_date, occurence, dateRecord";
        $where = "userDtrViolation_ID < $user_dtr_violation_id AND user_ID = $user_id AND dtrViolationType_ID = $violation_type";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_user_dtr_violation", "userDtrViolation_ID", "DESC");
    }

    private function get_user_dtr_violation($user_dtr_violation_id){
        $fields = "userDtrViolation_ID, user_ID, supervisor_ID, dtrViolationType_ID, sched_ID, month, year, sched_date, occurence, dateRecord";
        $where = "userDtrViolation_ID = $user_dtr_violation_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_user_dtr_violation");
    }

    private function get_shedule_shift($sched_id, $user_id){
        $fields = "schedType.schedtype_id as schedTypeId, type, sched.sched_id as schedId, sched.sched_date schedDate, users.emp_id empId, users.uid userId, time_start, time_end, bta_id, accTime.acc_id as accountId";
        $where = "schedType.schedtype_id = sched.schedtype_id AND accTime.acc_time_id = sched.acc_time_id AND times.time_id = accTime.time_id AND sched.sched_id = $sched_id AND sched.emp_id = users.emp_id AND users.uid = $user_id";
        $tables = "tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users";
        return $this->general_model->fetch_specific_val($fields, $where, $tables);
    }

    private function get_user_dtr_log($sched_id, $entry_type){
        $fields = "dtr_id, emp_id, acc_time_id, entry, log, note, type, sched_id";
        $where = "sched_id = $sched_id AND type='DTR' AND entry = '$entry_type'";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dtr_logs");
    }

    private function get_ir_deadline_settings($field){
        $fields = "deadlineToExplain, deadlineToExplainOption, deadlineToRecommend, deadlineToRecommendOption, deadlineLastRecommendation, deadlineFinalDecision, deadlineUploadDocs";
        $where = "deadlineToExplain IS NOT NULL";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_incident_report_deadline_settings");       
    }

    public function get_ir_deadline_settings_details(){
        // $field = $this->input->post('field');
        $field = "deadlineToExplain";
        $ir_deadline_settings = $this->get_ir_deadline_settings($field);
        if(count($ir_deadline_settings)> 0){
            $deadline_settings['record'] = $ir_deadline_settings;
        }else{
            $add_default_stat = $this->add_default_ir_deadline_settings();
            if($add_default_stat){
                $deadline_settings['record'] = $this->get_ir_deadline_settings($field);
            }else{
                $deadline_settings['record'] = 0;
            }
        }
        echo json_encode($deadline_settings);
    }

    private function get_offense_with_type($offense_id){
        $fields = "offense.offense_ID, offense.offense, offense.multiCategory, offense.multiCategoryLetters, offense.offenseType_ID, offenseCateg.offenseType, offenseCateg.letter";
        $where = "offenseCateg.offenseType_ID = offense.offenseType_ID AND offense.offense_ID = $offense_id";
        $table = "tbl_offense offense, tbl_offense_type offenseCateg";
        return $record = $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function get_dtr_violation_settings_details(){
        $dtr_violation_type_id = $this->input->post('dtrViolationTypeId');
        $dtr_violation_settings = $this->get_dtr_violation_settings($dtr_violation_type_id);
        if(count($dtr_violation_settings)>0){
            $dtr_vio_settings['record'] = $dtr_violation_settings;
            $dtr_vio_settings['offense'] = $this->get_offense_with_type($dtr_violation_settings->offense_ID);
        }else{
            $default_violation_settings_id = $this->add_default_violation_settings($dtr_violation_type_id);
            $dtr_violation_settings2 = $this->get_dtr_violation_settings($dtr_violation_type_id);
            $dtr_vio_settings['record'] = $dtr_violation_settings2;
            $dtr_vio_settings['offense'] = $this->get_offense_with_type($dtr_violation_settings2->offense_ID);
        }
        echo json_encode($dtr_vio_settings);
    }

    public function get_offense_for_dtr_vio_add_settings(){
        $fields = "offense_ID, offense, multiCategory, offenseType_ID, orderNum, dateCreated, dateChanged, changedBy";
        $where = "offense_ID IS NOT NULL";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_offense");
    }

    private function get_account_by_class($class){
        $where = "acc_id IS NOT NULL";
        if($class !== '0'){
            $where = "acc_description = '$class'";
        }
        $fields = "acc_id, acc_name";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_account", "acc_name", 'ASC');
    }

    private function get_emp_by_acc_class($class, $acc_id, $ass_type){
        $where_class = "";
        $where_acc = "";
        $where_qip = "";
        if($class !== '0'){
            $where_class = "AND acc.acc_description = '$class'";
        }
        if((int)$acc_id !== 0){
            $where_acc = "AND acc.acc_id = $acc_id";
        }
        if($ass_type == "qip"){
            $where_qip = " AND emp.emp_id NOT IN (SELECT emp_id FROM tbl_qip_assignment) ";
        }

        $where = "app.apid = emp.apid AND emp.acc_id = acc.acc_id AND emp.isActive = 'yes' ".$where_class." ".$where_acc." ".$where_qip;
        $fields = "emp.emp_id, app.fname, app.mname, app.lname";
        $table = "tbl_applicant app, tbl_employee emp, tbl_account acc";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "lname", 'ASC');
    }


    public function get_account(){
        $class = $this->input->post('class');
        $record = $this->get_account_by_class($class);
        if(count($record) > 0){
            $data['record'] = $record;
        }else{
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_site(){
        $fields = "site_ID, code, siteName";
        $where = "site_ID IS NOT NULL";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_site", "code", 'ASC');
        if(count($record) > 0){
            $data['record'] = $record;
        }else{
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_offense_type_select(){
        $acc_id = $this->input->post('accId');
        if($acc_id !== ""){
            $fields = "offenseType_ID, letter, offenseType";
            $where = "offenseType_ID NOT IN (SELECT offenseType_ID FROM tbl_offense_type_assignment WHERE acc_id = $acc_id)";
            $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_offense_type", "letter", 'ASC');
        }else{
            $record = 0;
        }
        if(count($record) > 0){
            $data['record'] = $record;
            $data['exist'] = 1;
        }else{
            $data['record'] = 0;
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_assignable_account(){
        $emp_id = $this->input->post('empId');
        $site_id = $this->input->post('siteId');
        if($emp_id !== "" && $site_id !== ""){
            $fields = "acc.acc_id, acc.acc_name";
            $where = "acc.acc_id NOT IN (SELECT ero.acc_id FROM tbl_employee_relations_officer ero WHERE site_ID = $site_id)";
            $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_account acc", 'acc.acc_name', 'ASC');
        }else{
            $record = 0;
        }
        if(count($record) > 0){
            $data['record'] = $record;
            $data['exist'] = 1;
        }else{
            $data['record'] = 0;
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_qip(){
        $qip_id = $this->input->post('qipId');
        $fields = "qipAssignment_ID, emp_id";
        $where = "qipAssignment_ID = $qip_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_qip_assignment");
        if(count($record) > 0){
            $data['exist'] = 1;
        }else{
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    private function get_specific_ero_accounts_record($ero_id){
        $fields = "employeeRelationsOfficer_ID, emp_id";
        $where = "employeeRelationsOfficer_ID = $ero_id";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_employee_relations_officer");
    }

    public function get_specific_ero_account(){
        $ero_id = $this->input->post('eroId');
        $record = $this->get_specific_ero_accounts_record($ero_id);
        if(count($record) > 0){
            $data['exist'] = 1;
        }else{
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_ero_account_by_emp(){
        $emp_id = $this->input->post('empId');
        // $emp_id = 26;
        $fields = "COUNT(employeeRelationsOfficer_ID) eroCount";
        $where = "emp_id = $emp_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_employee_relations_officer");
        // var_dump($record);
        if(count($record) > 0){
            $data['exist'] = $record->eroCount;
        }else{
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_assigned_offense_type(){
        $off_type_ass_id = $this->input->post('offTypeAssId');
        // $off_type_ass_id = 1;
        $fields = "offenseTypeAssignment_ID, offenseType_ID, acc_id";
        $where = "offenseTypeAssignment_ID = $off_type_ass_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_offense_type_assignment");
        // var_dump($record);
        if(count($record) > 0){
            $data['exist'] = $record->offenseTypeAssignment_ID;
        }else{
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_assigned_offense_type_by_acc_id(){
        $acc_id = $this->input->post('accId');
        $fields = "offenseTypeAssignment_ID, offenseType_ID";
        $where = "acc_ID = $acc_id";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_offense_type_assignment");
        if(count($record) > 0){
            $data['exist'] = count($record);
        }else{
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_emp(){
        $class = $this->input->post('class');
        $acc_id = $this->input->post('acc_id');
        $ass_type = $this->input->post('ass_type');
        // $class = 'Admin';
        // $acc_id = 0;
        $record = $this->get_emp_by_acc_class($class, $acc_id, $ass_type);
        if(count($record) > 0){
            $data['record'] = $record;
        }else{
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_qip_assignment_datatable(){
        $datatable = $this->input->post('datatable');
        // $class = $datatable['query']['class'];
        // $acc_id = $datatable['query']['accId'];
        $where_class = "";
        $where_acc = "";
        // if($class !== '0'){
        //     $where_class = "AND acc.acc_description = '$class'";
        // }
        // if((int)$acc_id !== 0){
        //     $where_acc = "AND acc.acc_id = $acc_id";
        // }
        $query['query'] = "SELECT qip.qipAssignment_ID, app.fname, app.mname, app.lname FROM tbl_qip_assignment qip, tbl_employee emp, tbl_applicant app, tbl_account acc WHERE acc.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = qip.emp_id ".$where_acc." ".$where_class ;
        if ($datatable['query']['qipSearchField'] != '') {
            $keyword = $datatable['query']['qipSearchField'];
            $query['search']['append'] = " AND (app.fname LIKE '%" . $keyword . "%' OR app.mname LIKE '%" . $keyword . "%' OR app.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname ,app.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname, app.mname ,app.lname) LIKE '%" . $keyword . "%')";
            $query['search']['total'] = " AND (app.fname LIKE '%" . $keyword . "%' OR app.mname LIKE '%" . $keyword . "%' OR app.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname ,app.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname, app.mname ,app.lname) LIKE '%" . $keyword . "%')";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_ero_supervisor_datatable(){
        $datatable = $this->input->post('datatable');
        $ero_sup_id = $datatable['query']['eroSupId'];
        $ero_id = $datatable['query']['eroId'];
        $where_ero_sup = "";
        $where_ero = "";
        if((int)$where_ero_sup !== 0){
            $where_ero_sup = " AND eroSup.employeeRelationsSupervisor = $ero_sup_id";
        }
        if((int)$where_ero !== 0){
            $where_ero = " AND eroSup.emp_id = $ero_id";
        }
        $query['query'] = "SELECT eroSup.employeeRelationSupervisor_ID, empEro.emp_id empEroEmpId, appEro.fname appEroFname, appEro.mname appEroMname, appEro.lname appEroLname, empEroSup.emp_id empEroSupEmp_id, appEroSup.fname appEroSupFname, appEroSup.mname appEroSupMname, appEroSup.lname appEroSupLname FROM tbl_employee_relations_supervisor eroSup, tbl_employee empEro, tbl_employee empEroSup, tbl_applicant appEro, tbl_applicant appEroSup WHERE appEro.apid = empEro.apid AND appEroSup.apid = empEroSup.apid AND empEro.emp_id = eroSup.emp_id AND empEroSup.emp_id = eroSup.employeeRelationsSupervisor" ;

        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function get_ero_assignment_datatable(){
        $datatable = $this->input->post('datatable');
        
        if ($datatable['query']['eroSearchField'] != '') {
            $query['query'] = "SELECT DISTINCT(ero.emp_id) emp_id, ero.employeeRelationsOfficer_ID, appEro.fname appEroFname, appEro.lname appEroLname, appEro.mname appEroMname, appEroSup.fname appEroSupFname, appEroSup.mname appEroSupMname, appEroSup.lname appEroSupLname, COUNT(ero.acc_id) acc_id FROM tbl_employee_relations_officer ero, tbl_employee_relations_supervisor eroSup, tbl_employee empEro, tbl_employee empEroSup, tbl_applicant appEro, tbl_applicant appEroSup WHERE appEro.apid = empEro.apid AND appEroSup.apid = empEroSup.apid AND empEroSup.emp_id = eroSup.employeeRelationsSupervisor AND eroSup.emp_id = ero.emp_id AND empEro.emp_id = ero.emp_id" ;
            $keyword = $datatable['query']['eroSearchField'];
            $query['search']['append'] = " AND (appEro.fname LIKE '%" . $keyword . "%' OR appEro.mname LIKE '%" . $keyword . "%' OR appEro.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',appEro.fname ,appEro.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',appEro.fname, appEro.mname ,appEro.lname) LIKE '%" . $keyword . "%' OR appEroSup.fname LIKE '%" . $keyword . "%' OR appEroSup.mname LIKE '%" . $keyword . "%' OR appEroSup.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',appEroSup.fname ,appEroSup.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',appEroSup.fname, appEroSup.mname ,appEroSup.lname) LIKE '%" . $keyword . "%') GROUP BY ero.emp_id";
            $query['search']['total'] = " AND (appEro.fname LIKE '%" . $keyword . "%' OR appEro.mname LIKE '%" . $keyword . "%' OR appEro.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',appEro.fname ,appEro.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',appEro.fname, appEro.mname ,appEro.lname) LIKE '%" . $keyword . "%' OR appEroSup.fname LIKE '%" . $keyword . "%' OR appEroSup.mname LIKE '%" . $keyword . "%' OR appEroSup.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',appEroSup.fname ,appEroSup.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',appEroSup.fname, appEroSup.mname ,appEroSup.lname) LIKE '%" . $keyword . "%') GROUP BY ero.emp_id";
        }else{
            $query['query'] = "SELECT DISTINCT(ero.emp_id) emp_id, ero.employeeRelationsOfficer_ID, appEro.fname appEroFname, appEro.lname appEroLname, appEro.mname appEroMname, appEroSup.fname appEroSupFname, appEroSup.mname appEroSupMname, appEroSup.lname appEroSupLname, COUNT(ero.acc_id) acc_id FROM tbl_employee_relations_officer ero, tbl_employee_relations_supervisor eroSup, tbl_employee empEro, tbl_employee empEroSup, tbl_applicant appEro, tbl_applicant appEroSup WHERE appEro.apid = empEro.apid AND appEroSup.apid = empEroSup.apid AND empEroSup.emp_id = eroSup.employeeRelationsSupervisor AND eroSup.emp_id = ero.emp_id AND empEro.emp_id = ero.emp_id GROUP BY ero.emp_id" ;
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_offense_type_assignment_datatable(){
        $datatable = $this->input->post('datatable'); 
        $acc_id = $datatable['query']['accId'];
        $when_acc_id = "";
        if((int)$acc_id !== 0){
            $when_acc_id = " AND offTypeAss.acc_id = $acc_id";
        }
        $query['query'] = "SELECT DISTINCT(acc.acc_id) accId, acc.acc_name, acc.acc_description, COUNT(offTypeAss.offenseType_ID) offenseTypeCount FROM tbl_account acc, tbl_offense_type_assignment offTypeAss WHERE acc.acc_id = offTypeAss.acc_id ".$when_acc_id." GROUP BY acc.acc_id" ;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_assigned_offenses_datatable(){
        $datatable = $this->input->post('datatable');
        $acc_id = $datatable['query']['accId'];
        if ($datatable['query']['assignedOffenseTypeSearch'] != '') {
            $query['query'] = "SELECT offTypeAss.offenseTypeAssignment_ID, offType.offenseType_ID, offType.letter, offType.offenseType FROM tbl_offense_type_assignment offTypeAss, tbl_offense_type offType WHERE offType.offenseType_ID = offTypeAss.offenseType_ID AND offTypeAss.acc_id = $acc_id" ;
            $keyword = $datatable['query']['assignedOffenseTypeSearch'];
            $query['search']['append'] = " AND (offType.letter LIKE '%" . $keyword . "%' OR offType.offenseType LIKE '%" . $keyword . "%') GROUP BY offType.letter";
            $query['search']['total'] = " AND (offType.letter LIKE '%" . $keyword . "%' OR offType.offenseType LIKE '%" . $keyword . "%') GROUP BY offType.letter";
        }else{
            $query['query'] = "SELECT offTypeAss.offenseTypeAssignment_ID, offType.offenseType_ID, offType.letter, offType.offenseType FROM tbl_offense_type_assignment offTypeAss, tbl_offense_type offType WHERE offType.offenseType_ID = offTypeAss.offenseType_ID AND offTypeAss.acc_id = $acc_id GROUP BY offType.letter" ;
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    private function get_user_by_emp_id($emp_id){
        $fields = "uid";
        $where = "emp_id = $emp_id";
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_user");
    }

    private function get_emp_details($emp_id){
        $fields = "app.pic, app.fname, app.lname, app.mname, app.nameExt, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, empStat.status as empStatus";
        $where = "empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = $emp_id";
        $table = "tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app, tbl_account accounts";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    private function get_user_emp_details($user_id){
        $fields = "emp.emp_id, app.pic, app.fname, app.lname, app.mname, app.nameExt, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, empStat.status as empStatus";
        $where = "empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = $user_id";
        $table = "tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app, tbl_account accounts, tbl_user users";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function get_acc_details(){
        $acc_id = $this->input->post('accId');
        $fields = "acc.acc_id, acc.acc_name, acc.acc_description, dep.dep_details";
        $where = "dep.dep_id = acc.dep_id AND acc.acc_id = $acc_id";
        $table = "tbl_account acc, tbl_department dep";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        echo json_encode($record);
    }

    public function get_ero_account_datatable(){
        $datatable = $this->input->post('datatable');
        $class = $datatable['query']['class'];
        $site_id = $datatable['query']['siteId'];
        $emp_id = $datatable['query']['empId'];
        $where_class = "";
        $where_site = "";
        if($class !== "0"){
            $where_class = "AND account.acc_description = '$class'";
        }
        if($site_id !== ""){
            $where_site = "AND site.site_ID = $site_id";
        }
        // $fields = "ero.employeeRelationsOfficer_ID, account.acc_name, site.code";
        // $where = "account.acc_id = ero.acc_id AND site.site_ID = ero.site_ID AND ero.emp_id =  557 ".$where_class."  ".$where_site;
        // $table = "tbl_employee_relations_officer ero, tbl_account account, tbl_site site";
        // return $this->general_model->fetch_specific_val($fields, $where, $table);
        $query['query'] = "SELECT ero.employeeRelationsOfficer_ID, account.acc_name, site.code FROM tbl_employee_relations_officer ero, tbl_account account, tbl_site site WHERE account.acc_id = ero.acc_id AND site.site_ID = ero.site_ID AND ero.emp_id = $emp_id ".$where_class."  ".$where_site ;
        if ($datatable['query']['eroAccountSearchField'] != '') {
            $keyword = $datatable['query']['eroAccountSearchField'];
            $query['search']['append'] = " AND (account.acc_name LIKE '%" . $keyword . "%' OR site.code LIKE '%" . $keyword . "%')";
            $query['search']['total'] = " AND (account.acc_name LIKE '%" . $keyword . "%' OR site.code LIKE '%" . $keyword . "%')";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_specific_ero_supervisor($emp_id){
        $fields = "eroSup.employeeRelationSupervisor_ID, eroSup.employeeRelationsSupervisor, app.fname, app.mname, app.lname";
        $where = "app.apid = emp.apid AND emp.emp_id = eroSup.employeeRelationsSupervisor AND eroSup.emp_id = $emp_id";
        $table = "tbl_applicant app, tbl_employee emp, tbl_employee_relations_supervisor eroSup";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function get_ero_details(){
        $emp_id = $this->input->post('empId');
        $record = $this->get_emp_details($emp_id);
        if(count($record) > 0){
            $data['record_exist'] = 1;
            $data['record'] = $record;
            $ero_sup = $this->get_specific_ero_supervisor($emp_id);
            if(count($ero_sup) > 0){
                $data['ero_sup'] = $ero_sup;
                $data['ero_exist'] = 1;
            }else{
                $insert_ero_sup_stat = $this->assign_super_ero($emp_id);
                if($insert_ero_sup_stat){
                    $ero_sup2 = $this->get_specific_ero_supervisor($emp_id);
                    $data['ero_sup'] = $ero_sup2;
                    $data['ero_exist'] = 1;
                }else{
                    $data['ero_sup'] = 0;
                    $data['ero_exist'] = 0;
                }
            }
        }else{
            $data['record_exist'] = 0;
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    //   private function get_existing_ero(){
    //     $fields = "offense_ID, offense, multiCategory, offenseType_ID, orderNum, dateCreated, dateChanged, changedBy";
    //     $where = "offense_ID = $offense_id";
    //     return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_offense");
    // }

    public function get_assigned_accounts(){
        $query = "SELECT DISTINCT(acc.acc_id) accId, acc.acc_name, acc.acc_description, COUNT(offTypeAss.offenseType_ID) offenseTypeCount FROM tbl_account acc, tbl_offense_type_assignment offTypeAss WHERE acc.acc_id = offTypeAss.acc_id GROUP BY acc.acc_id";
        $record = $this->general_model->custom_query($query);
        if(count($record) > 0){
            $data['exist'] = 1;
            $data['record'] = $record;
        }else{
            $data['exist'] = 0;
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function check_if_qip_record_exist(){
        $fields = "qipAssignment_ID";
        $record =  $this->general_model->fetch_all($fields, 'tbl_qip_assignment');
        if(count($record) > 0){
            $data['exist'] = 1;
        }else{
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function check_if_ero_record_exist(){
        $fields = "employeeRelationsOfficer_ID";
        $record =  $this->general_model->fetch_all($fields, 'tbl_employee_relations_officer');
        if(count($record) > 0){
            $data['exist'] = 1;
        }else{
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function check_if_off_type_ass_record_exist(){
        $fields = "offenseTypeAssignment_ID";
        $record =  $this->general_model->fetch_all($fields, 'tbl_offense_type_assignment');
        if(count($record) > 0){
            $data['exist'] = 1;
        }else{
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    

    private function get_disciplinary_action_details_prev_occurence($discipline_categ_id, $level){
        $fields = "discAct.disciplinaryAction_ID, discAct.action, discAct.periodMonth, discAct.abbreviation, discAct.dateCreated, discAct.dateChanged, discAct.changedBy";
        $where = "discAct.disciplinaryAction_ID = discActCateg.disciplinaryAction_ID AND discActCateg.disciplineCategory_ID = $discipline_categ_id AND discActCateg.level = $level";
        $table = "tbl_disciplinary_action discAct, tbl_disciplinary_action_category discActCateg";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    private function get_user_dtr_violation_by_range($from, $to, $user_id, $dtr_vio_type){
        $fields = "userDtrViolation_ID, sched_date, occurence, duration";
        $where = "DATE(sched_date) >= '$from' AND DATE(sched_date) <= '$to' AND user_ID = $user_id AND dtrViolationType_ID = $dtr_vio_type AND status_ID = 0";
        $table = "tbl_user_dtr_violation";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, 'sched_date', 'ASC');
    }

    private function get_first_level_disciplinary_action($offense_id, $level){
        $offense_with_type = $this->get_offense_with_type($offense_id);
        $multiCateg = explode(",", $offense_with_type->multiCategory);
        $disciplinary_category = $multiCateg[0];
        $fields = "discAction.disciplinaryAction_ID, discAction.action, discAction.periodMonth, discAction.abbreviation, discActionCateg.disciplineCategory_ID, discActionCateg.disciplinaryActionCategory_ID,discActionCateg.level";
        $where = "discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discActionCateg.disciplineCategory_ID = $disciplinary_category AND discActionCateg.level = $level";
        $table = "tbl_disciplinary_action discAction, tbl_disciplinary_action_category discActionCateg";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    // END OF READ -----------------------------------------------------------------------------------
    
    // UPDATE -----------------------------------------------------------------------------------
    public function update_disciplinary_action(){
        $disciplinary_action_id = $this->input->post('disciplinaryActionId');
        $data['action']= $this->input->post('action');
        $data['abbreviation']= $this->input->post('abbr');
        $data['periodMonth'] = $this->input->post('period');
        $data['changedBy'] = $this->session->userdata('emp_id');
        $where = "disciplinaryAction_ID = $disciplinary_action_id";
        $update_stat = $this->general_model->update_vals($data, $where, 'tbl_disciplinary_action');
        echo json_encode($update_stat);
    }

    public function update_specific_offense_category(){
        $offense_category_id = $this->input->post('specificOffenseCategoryId');
        $data['offenseType']= $this->input->post('offenseCategoryVal');
        $data['changedBy'] = $this->session->userdata('emp_id');
        $where = "offenseType_ID = $offense_category_id";
        $update_stat = $this->general_model->update_vals($data, $where, 'tbl_offense_type');
        echo json_encode($update_stat);
    }

    public function update_offense(){
        if($this->input->post('origOffenseNum') < $this->input->post('offenseNum')){
            $this->decrement_order_num($this->input->post('offenseNum'), $this->input->post('origOffenseNum'), $this->input->post('offenseCategoryId'));
            $data['orderNum']= $this->input->post('offenseNum');
        }else if($this->input->post('origOffenseNum') > $this->input->post('offenseNum')){
            $this->increment_order_num($this->input->post('offenseNum'), $this->input->post('origOffenseNum'), $this->input->post('offenseCategoryId'));
            $data['orderNum']= $this->input->post('offenseNum');
        }
        $offense_id = $this->input->post('offenseId');
        // $data['disciplineCategory_ID']= $this->input->post('disciplineCategoryId');
        $data['offense']= $this->input->post('offense');
        $data['multiCategory']= ''.$this->input->post('multiCateg');
        $data['multiCategoryLetters'] = $this->input->post("disciplineCategoryLetter");
        $data['changedBy'] = $this->session->userdata('emp_id');
        $where = "offense_ID = $offense_id";
        $update_stat = $this->general_model->update_vals($data, $where, 'tbl_offense');
        echo json_encode($update_stat);
    }

    private function decrement_order_num($num, $orig_num, $offense_category_id){
        $query = "UPDATE tbl_offense SET orderNum = orderNum-1 WHERE offenseType_ID = $offense_category_id AND orderNum > $orig_num AND  orderNum < $num+1";
        return $record = $this->general_model->custom_query_no_return($query);
    }

    private function increment_order_num($num, $orig_num, $offense_category_id){
        $query = "UPDATE tbl_offense SET orderNum = orderNum+1 WHERE offenseType_ID = $offense_category_id AND orderNum < $orig_num AND  orderNum > $num-1";
        return $record = $this->general_model->custom_query_no_return($query);
    }
    private function set_new_level($new_level, $disc_action_category_id){
        $data['level'] = $new_level;
        $where = "disciplinaryActionCategory_ID = $disc_action_category_id";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_disciplinary_action_category');
    }

    private function transfer_level($exchange_level, $new_level, $category_id){
        $data['level'] = $exchange_level;
        $where = "disciplineCategory_ID =  $category_id AND level = $new_level";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_disciplinary_action_category');
    }
    
    public function rearrange_level(){
        $disc_action_category_id = $this->input->post('disciplinaryActionCategoryId');
        $new_level = $this->input->post('newLevel');
        $exchange_level = $this->input->post('exchangeLevel');
        $category_id = $this->input->post('categoryId');
        $update['transfer_level'] = $this->transfer_level($exchange_level, $new_level, $category_id);
        $update['set_new_level'] = $this->set_new_level($new_level, $disc_action_category_id);
        echo json_encode($update);
    }

    private function move_level($category_id, $level){
        $query = "UPDATE tbl_disciplinary_action_category SET level = level-1 WHERE disciplineCategory_ID = $category_id AND level > $level";
        return $record = $this->general_model->custom_query_no_return($query);
    }

    private function insert_num_order($num, $offense_category_id){
        $query = "UPDATE tbl_offense SET orderNum = orderNum+1 WHERE offenseType_ID = $offense_category_id AND orderNum > $num";
        return $record = $this->general_model->custom_query_no_return($query);
    }

    private function remove_num_order($num, $offense_category_id){
        $query = "UPDATE tbl_offense SET orderNum = orderNum-1 WHERE offenseType_ID = $offense_category_id AND orderNum > $num";
        return $record = $this->general_model->custom_query_no_return($query);
    }

    public function update_offense_multi_category($category_id){
        $spec_category = $this->get_offense_with_spec_category($category_id);
        $category_details = $this->get_specific_discipline_category($category_id);
        $loop_count = 0;
        if(count($spec_category) > 0){
             foreach($spec_category as $rows){
                $categ_id_arr = explode(',', $rows->multiCategory);
                $categ_letter_arr = explode(',', $rows->multiCategoryLetters);
                $categ_id_key = array_search($category_id."", $categ_id_arr);
                $categ_letter_key = array_search($category_details->category."", $categ_letter_arr);
                if(count($categ_id_arr) > 1){
                    if (false !== $categ_id_key) {
                        unset($categ_id_arr[$categ_id_key]);
                    }
                    if (false !== $categ_letter_key) {
                        unset($categ_letter_arr[$categ_letter_key]);
                    }
                }else{
                    if (false !== $categ_id_key) {
                        $categ_id_arr[$categ_id_key] = '0';
                    }
                    if (false !== $categ_letter_key) {
                        $categ_letter_arr[$categ_letter_key] = '0';
                    }
                }
                $updated_multicateg[$loop_count] = [
                    'offense_ID' => $rows->offense_ID,
                    'multiCategory' => implode(",",array_values($categ_id_arr)),
                    'multiCategoryLetters' => implode(",",array_values($categ_letter_arr)),
                ];
                $loop_count++;
            }
            return $this->general_model->batch_update($updated_multicateg, 'offense_ID', 'tbl_offense');
        }else{
            return 2;
        }
    }

    public function update_dtr_violation_settings(){
        $violation_id = $this->input->post('violationId');
        $field_1 = $this->input->post('field1');
        $value_1 = $this->input->post('value1');
        $field_2 = $this->input->post('field2');
        $value_2 = $this->input->post('value2');
        if($field_2 == 'durationUnit'){
            $data[$field_2] = $value_2;
        }
        $data[$field_1] = $value_1;
        $data['changedBy'] = $this->session->userdata('emp_id');
        $where = "dtrViolationType_ID = $violation_id";
        $update_stat = $this->general_model->update_vals($data, $where, 'tbl_dtr_violation_type_settings');
        echo json_encode($update_stat);
    }

    public function update_ir_deadline_settings(){
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $exist = 0;
        $ir_deadline_settings = $this->get_ir_deadline_settings($field);
        if(count($ir_deadline_settings)> 0){
            $exist = 1;
        }else{
            $add_default_stat = $this->add_default_ir_deadline_settings();
            if($add_default_stat){
                $exist = 1;
            }else{
                $exist = 0;
            }
        }
        if($exist){
            $data[$field] = $value;
            $data['changedBy'] = $this->session->userdata('emp_id');
            $where = "$field IS NOT NULL";
            $update_stat = $this->general_model->update_vals($data, $where, 'tbl_incident_report_deadline_settings');
        }else{
            $update_stat = 0;
        }
        echo json_encode($update_stat);
    }

    public function update_ero_supervisor(){
        $ero_sup_id = $this->input->post('supId');
        $emp_id = $this->input->post('empId');
        $data['employeeRelationsSupervisor'] = $ero_sup_id;
        $where = "emp_id = $emp_id";
        $update_stat = $this->general_model->update_vals($data, $where, 'tbl_employee_relations_supervisor');
        echo json_encode($update_stat);
    }

    public function update_dtr_vio_settings_offense($offense_list){
        $data['offense_ID'] = NULL;
        $where = "offense_ID IN ($offense_list)";
        return $this->general_model->update_vals($data, $where, 'tbl_dtr_violation_type_settings');
    }

    public function update_user_dtr_violation($user_dtr_vio_id, $data){
        $where = "userDtrViolation_ID = $user_dtr_vio_id";
        return $this->general_model->update_vals($data, $where, 'tbl_user_dtr_violation');
    }
    // END OF UPDATE -----------------------------------------------------------------------------------
    
    // DELETE -----------------------------------------------------------------------------------
    public function delete_disciplinary_action(){
        $where['disciplinaryAction_ID'] = $this->input->post('disciplinaryActionId');
        $delete_stat = $this->general_model->delete_vals($where, 'tbl_disciplinary_action');
        echo json_encode($delete_stat);
    }

    public function delete_offense_level($disc_action_category_id){
        $where['disciplinaryActionCategory_ID'] = $disc_action_category_id;
        $delete_stat = $this->general_model->delete_vals($where, 'tbl_disciplinary_action_category');
        return json_encode($delete_stat);
    }
    public function remove_offense_disciplinary_action(){
        $disc_action_category_id = $this->input->post('disciplinaryActionCategoryId');
        $category_id = $this->input->post('categoryId');
        $level = $this->input->post('level');
        $remove['delete_offense_level']= $this->delete_offense_level($disc_action_category_id);
        $remove['move_level']= $this->move_level($category_id, $level);
        echo json_encode($remove);
    }

    public function remove_category_levels($category_id){
        $where['disciplineCategory_ID'] = $category_id;
        $delete_stat = $this->general_model->delete_vals($where, 'tbl_disciplinary_action_category');
        return json_encode($delete_stat);
    }

    public function remove_category_in_offense($category_id){
        $where['disciplineCategory_ID'] = $category_id;
        $delete_stat = $this->general_model->delete_vals($where, 'tbl_disciplinary_action_category');
        return json_encode($delete_stat);
    }

    public function delete_offense_category($category_id){
        $where['disciplineCategory_ID'] = $category_id;
        $delete_stat = $this->general_model->delete_vals($where, 'tbl_discipline_category');
        return json_encode($delete_stat);
    }

    public function delete_specific_category(){
        $category_id = $this->input->post("categoryId");
        $delete_category['remove_category_levels'] = $this->remove_category_levels($category_id);
        $delete_category['remove_category_on_offenses'] = $this->update_offense_multi_category($category_id);
        $delete_category['delete_offense_category'] = $this->delete_offense_category($category_id);
        echo json_encode($delete_category);
    }

    private function remove_offense_via_type($offense_category_id){
        $where['offenseType_ID'] = $offense_category_id;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_offense');
    }

    private function delete_offense_category_type($offense_category_id){
        $where['offenseType_ID'] = $offense_category_id;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_offense_type');
    }

    public function remove_assigned_offenses_via_type($offense_category_id){
        $offenses = $this->get_offenses_under_offense_type($offense_category_id);
        $offense_list = [];
        $count = 0;
        foreach($offenses as $rows){
            $offense_list[$count] = $rows->offense_ID;
            $count++;
        }
        return $this->update_dtr_vio_settings_offense(implode(',',$offense_list));
    }

    public function remove_assigned_offense_type($offense_category_id){
        $where['offenseType_ID'] = $offense_category_id;
        return $this->general_model->delete_vals($where, 'tbl_offense_type_assignment');
    }

    public function remove_specific_offense_type(){
        $offense_category_id = $this->input->post('offenseCategoryId');
        $del_offense_type['del_assigned_offenses_types'] = $this->remove_assigned_offense_type($offense_category_id);
        $del_offense_type['del_assigned_offenses'] = $this->remove_assigned_offenses_via_type($offense_category_id);
        $del_offense_type['del_offense_stat'] = $this->remove_offense_via_type($offense_category_id);
        $del_offense_type['del_offense_type_stat'] = $this->delete_offense_category_type($offense_category_id);
        echo json_encode($del_offense_type);
    }

    private function delete_offense($offense_id){
        $where['offense_ID'] = $offense_id;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_offense');
    }


    public function remove_offense(){
        $offense_id = $this->input->post("offenseId");
        $record = $this->get_offense($offense_id);
        $remove['reorder_stat'] = $this->remove_num_order($record->orderNum, $record->offenseType_ID);
        $remove['remove_dtr_vio_set_offense'] = $this->update_dtr_vio_settings_offense($offense_id);
        $remove['remove_stat'] = $this->delete_offense($offense_id);
        echo json_encode($remove);
    }

    public function remove_qip_assignment(){
        $qip_id = $this->input->post("qipId");
        $where['qipAssignment_ID'] = $qip_id;
        $delete_stat['remove_stat'] = $this->general_model->delete_vals($where, 'tbl_qip_assignment');
        echo json_encode($delete_stat);
    }

    
    public function remove_ero_account(){
        $ero_id = $this->input->post("eroId");
        $emp_id = $this->input->post("empId");
        $where['employeeRelationsOfficer_ID'] = $ero_id;
        $delete_stat['remove_stat'] = $this->general_model->delete_vals($where, 'tbl_employee_relations_officer');
        $ero_records = $this->get_specific_ero_accounts_record($ero_id);
        if(count($ero_records) == 0){
            $delete_stat['remove_sup_stat'] = $this->remove_ero_supervisor($emp_id);
        }
        echo json_encode($delete_stat);
    }

    public function remove_offense_type_by_offense_type_ass_id(){
        $off_type_ass_id = $this->input->post("offTypeAssId");
        $where['offenseTypeAssignment_ID'] = $off_type_ass_id;
        $delete_stat['remove_stat'] = $this->general_model->delete_vals($where, 'tbl_offense_type_assignment');
        echo json_encode($delete_stat);
    }

    public function remove_offense_type_by_acc_id(){
        $acc_id = $this->input->post("accId");
        $where['acc_id'] = $acc_id;
        $delete_stat['remove_stat'] = $this->general_model->delete_vals($where, 'tbl_offense_type_assignment');
        echo json_encode($delete_stat);
    }

    public function remove_ero_supervisor($ero_id){
        $where['emp_id'] = $ero_id;
        return $this->general_model->delete_vals($where, 'tbl_employee_relations_supervisor');
    }

    public function remove_employee_relations_officer(){
        $where['emp_id'] = $this->input->post('empId');
        $delete_stat['remove_sup_stat'] = $this->remove_ero_supervisor($this->input->post('empId'));
        $delete_stat['remove_ero_stat'] = $this->general_model->delete_vals($where, 'tbl_employee_relations_officer');
        echo json_encode($delete_stat);
    }

    // END OF DELETE -----------------------------------------------------------------------------------

    // TEST FUNCTIONS ----

    private function get_violation_related_details($user_dtr_violation_obj){
        $data['error'] = 0;
        $data['error_details'] = "";
        $data['valid'] = 0;
        $sched = $this->get_shedule_shift((int)$user_dtr_violation_obj->sched_ID, (int)$user_dtr_violation_obj->user_ID);
        if(count($sched) > 0){
            if((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //LATE
            {
                // DTR logs
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $login = new DateTime($login_raw->log);
                $login_formatted = $login->format("Y-m-d H:i");
                if(count($login_raw) > 0){
                    $login_str = strtotime($login_formatted);
                    // DATED SCHED
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                    $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
                    $shift_start_str = strtotime($new_formatted_shift_start);
                    // Compute late duration
                    $duration = $this->get_human_time_format($shift_start_str, $login_str, 0);
                    $hrs = $duration['hoursTotal'];
                    $hrsToMin = $duration['hoursTotal']*60;
                    if($login_str > $shift_start_str){
                        $data['valid'] = 1;
                        $shift_start_end = $sched->time_start." - ".$sched->time_end;
                        $violation_details = [
                            "sched_date" => $sched->schedDate,
                            "shift" => $shift_start_end,
                            "in_id" => $login_raw->dtr_id,
                            "in_log" => $login_formatted,
                            "shift_in" => $new_formatted_shift_start,
                            "duration" => $hrsToMin,
                            "human_time_format" => $duration
                        ];
                    }else{
                        $data['valid'] = 0;
                    }
                    $user_dtr_violation_details[0] = [
                        'userDtrViolation_ID' => $user_dtr_violation_obj->dtrViolationType_ID,
                        'shift' => $shift_start_end,
                        'dtr_id' => $login_raw->dtr_id,
                        'log' => $login_formatted,
                        'type' => "DTR",
                        'entry' => "I",
                        'minDuration' => 0
                    ];
                    $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                    $data['violation_details'] = $violation_details;
                    $user_data_vio['duration'] = $hrsToMin;
                    $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
                }else{
                    $data['error_details'] = [
                        'error' => "No login",
                        'origin' => 'Related Violation details function - (get_violation_related_details)',
                        'process' => 'getting of login and logout for late details'
                    ];
                }
            }
            else if((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) // UT
            {
                // DTR logs
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
                
                if(count($login_raw) > 0 && count($logout_raw) > 0){
                    $login = new DateTime($login_raw->log);
                    $login_formatted = $login->format("Y-m-d H:i");
                    $logout = new DateTime($logout_raw->log);
                    $logout_formatted = $logout->format("Y-m-d H:i");

                    $login_str = strtotime($login_formatted);
                    $logout_str = strtotime($logout_formatted);
                    // DATED SCHED
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                    $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);

                    $shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
                    $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
                    $new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
                    $new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);
                    $shift_out_str = strtotime($new_formatted_shift_end);
                    
                    
                    $duration = $this->get_human_time_format($logout_str, $shift_out_str, 0);
                    $hrs = $duration['hoursTotal'];
                    $hrsToMin = $duration['hoursTotal']*60;

                    if($shift_out_str > $logout_str){
                        $data['valid'] = 1;
                        $shift_start_end = $sched->time_start." - ".$sched->time_end;
                        $violation_details = [
                            "sched_date" => $sched->schedDate,
                            "shift" => $shift_start_end,
                            "out_id" => $logout_raw->dtr_id,
                            "out_log" => $logout_formatted,
                            "shif_out" => $formatted_shift_end,
                            "duration" => $hrsToMin,
                            "human_time_format" => $duration
                        ];
                    }else{
                        $data['valid'] = 0;
                    }
                    $user_dtr_violation_details[0] = [
                        'userDtrViolation_ID' => $user_dtr_violation_obj->dtrViolationType_ID,
                        'shift' => $shift_start_end,
                        'dtr_id' => $logout_raw->dtr_id,
                        'log' => $logout_formatted,
                        'type' => "DTR",
                        'entry' => "O",
                        'minDuration' => $hrsToMin
                    ];
                    $data['violation_details'] = $violation_details;
                    $user_data_vio['duration'] = $hrsToMin;
                    $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                    $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
                }else{
                    $data['error'] = 1;
                    $data['error_details'] = [
                        'error' => "Incomplete Log",
                        'origin' => 'Related Violation details function - (get_violation_related_details)',
                        'process' => 'getting of login and logout for forgot to undertime details'
                    ];
                }
            }
            else if((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) //FORGOT TO LOGOUT
            {
                // DTR logs
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
                
                if(count($login_raw) > 0 && count($logout_raw) > 0){
                    $login = new DateTime($login_raw->log);
                    $login_formatted = $login->format("Y-m-d H:i");
                    $logout = new DateTime($logout_raw->log);
                    $logout_formatted = $logout->format("Y-m-d H:i");
                    $login_str = strtotime($login_formatted);
                    $logout_str = strtotime($logout_formatted);

                    // DATED SCHED
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");

                    $shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
                    $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
                    $new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
                    $new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);
                    $shift_out_str = strtotime($new_formatted_shift_end);

                    $duration = $this->get_human_time_format($shift_out_str, $logout_str, 0);
                    $hrsToMin = $duration['hoursTotal']*60;
                    if($logout_str > $shift_out_str){
                        $data['valid'] = 1;
                        $shift_start_end = $sched->time_start." - ".$sched->time_end;
                        $violation_details = [
                            "sched_date" => $sched->schedDate,
                            "shift" => $shift_start_end,
                            "out_id" => $logout_raw->dtr_id,
                            "out_log" => $logout_formatted,
                            "shif_out" => $formatted_shift_end,
                            "duration" => $hrsToMin,
                            "human_time_format" => $duration
                        ];
                    }
                    $user_dtr_violation_details[0] = [
                        'userDtrViolation_ID' => $user_dtr_violation_obj->dtrViolationType_ID,
                        'shift' => $shift_start_end,
                        'dtr_id' => $logout_raw->dtr_id,
                        'log' => $logout_formatted,
                        'type' => "DTR",
                        'entry' => "O",
                        'minDuration' => $hrsToMin
                    ];
                    $data['violation_details'] = $violation_details;
                    $user_data_vio['duration'] = $hrsToMin;
                    $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                    $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
                }else{
                    $data['error'] = 1;
                    $data['error_details'] = [
                        'error' => "Incomplete Log",
                        'origin' => 'Related Violation details function - (get_violation_related_details)',
                        'process' => 'getting of login and logout for forgot to logout details'
                    ];
                }
            }
            else if((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
            {
                // DTR logs
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
                if(count($login_raw) > 0 && count($logout_raw) > 0){
                    // DATED SCHED
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                    $shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
                    $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
                     // ALLOWABLE BREAKS
                    $allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
                    $emp_account_details = $this->get_emp_account_details($sched->empId);
                    if($allowable_breaks['breakData'] > 0){
                        // GET BREAK LOGS
                        $break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
                        $break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');
                        // BREAK COVERED
                        $covered_breaks = [];
                        $covered_breaks_count = 0;
                        $total_covered_break = 0;
                        $user_dtr_vio_details_count = 0;
                        if(count($break_out_logs)> 0){
                            foreach($break_out_logs as $break_out_row){
                                $searchedValue = $break_out_row->dtr_id;
                                $neededObject = array_filter($break_in_logs,
                                    function ($e) use ($searchedValue) {
                                        return $e->note == $searchedValue;
                                    }
                                );
                                $shift_start_end = $sched->time_start." - ".$sched->time_end;
                                $break_out_log = new DateTime($break_out_row->log);
                                $formatted_break_out_log = $break_out_log->format("Y-m-d H:i");

                                $user_dtr_violation_details[$user_dtr_vio_details_count] = [
                                   'userDtrViolation_ID' => $user_dtr_violation_obj->dtrViolationType_ID,
                                   'shift' => $shift_start_end,
                                   'dtr_id' => $break_out_row->dtr_id,
                                   'log' => $formatted_break_out_log,
                                   'type' => "Break",
                                   'entry' => "O",
                                   'minDuration' => 0
                                ];
                                $user_dtr_vio_details_count++;
                                if(count($neededObject[0])> 0){
                                    $break_in_log = new DateTime($neededObject[0]->log);
                                    $formatted_break_in_log = $break_in_log->format("Y-m-d H:i");

                                    $break_out_str = strtotime($formatted_break_out_log);
                                    $break_in_str = strtotime($formatted_break_in_log);

                                    $duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
                                    $break_hrs_to_min = $duration['hoursTotal']*60;
                                    $total_covered_break += $break_hrs_to_min;
                                    
                                    
                                    $covered_breaks[$covered_breaks_count] = [
                                        "sched_date" => $sched->schedDate,
                                        "shift" => $shift_start_end,
                                        "break_type" => $break_out_row->break_type,
                                        "out_id" => $break_out_row->dtr_id,
                                        "out_log" => $formatted_break_out_log,
                                        "in_id" => $neededObject[0]->dtr_id,
                                        "in_log" => $formatted_break_in_log,
                                        "minutes" => $break_hrs_to_min,
                                        "human_time_format" => $duration
                                    ];                                    
                                    $user_dtr_violation_details[$user_dtr_vio_details_count] = [
                                        'userDtrViolation_ID' => $user_dtr_violation_obj->dtrViolationType_ID,
                                        'shift' => $shift_start_end,
                                        'dtr_id' => $neededObject[0]->dtr_id,
                                        'log' => $formatted_break_in_log,
                                        'type' => "Break",
                                        'entry' => "I",
                                        'minDuration' => $break_hrs_to_min
                                    ];
                                    $user_dtr_vio_details_count++;
                                    $covered_breaks_count++;
                                }
                            }
                            $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                            if($total_covered_break > $allowable_breaks['minutes']){
                                $data['valid'] = 1;
                                $data['total_covered'] = $total_covered_break;
                                $data['break_coverage_info'] = $covered_breaks;
                                $data['allowable_break'] = $allowable_breaks;
                            }
                            $user_data_vio['duration'] = $total_covered_break;
                            $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
                        }else{
                            $data['error'] = 1;
                            $data['error_details'] = [
                                'error' => "Incomplete Break logs",
                                'origin' => 'Related Violation details function - (get_violation_related_details)',
                                'process' => 'getting out for break logs'
                            ];
                        }
                    }else{
                        $data['error'] = 1;
                        $data['error_details'] = [
                            'error' => "Assigned Breaks not found",
                            'origin' => 'Related Violation details function - (get_violation_related_details)',
                            'process' => 'getting of allowable break details'
                        ];
                    }
                }else{
                    $data['error'] = 1;
                    $data['error_details'] = [
                        'error' => "Incomplete Log",
                        'origin' => 'Related Violation details function - (get_violation_related_details)',
                        'process' => 'getting of login and logout for over break details'
                    ];
                }
            }
            else if((int) $user_dtr_violation_obj->dtrViolationType_ID == 6) // INC BREAK
            {
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
                if(count($login_raw) > 0 && count($logout_raw) > 0){
                    // DATED SCHED
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                    $shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
                    $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
                     // ALLOWABLE BREAKS
                    $allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
                    $required_break_logs = count($allowable_breaks['breakData']) * 2;
                    $emp_account_details = $this->get_emp_account_details($sched->empId);
                    if(count($allowable_breaks['breakData']) > 0){
                        // GET BREAK LOGS
                        $break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
                        $break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');
                        // var_dump($break_out_logs);
                        // var_dump($break_in_logs);
                        $user_dtr_vio_details_count = 0;
                        if(count($break_out_logs)> 0){
                            foreach($break_out_logs as $break_out_row){
                                $searchedValue = $break_out_row->dtr_id;
                                $neededObject = array_filter($break_in_logs,
                                    function ($e) use ($searchedValue) {
                                        return $e->note == $searchedValue;
                                    }
                                );
                                $shift_start_end = $sched->time_start." - ".$sched->time_end;
                                $break_out_log = new DateTime($break_out_row->log);
                                $formatted_break_out_log = $break_out_log->format("Y-m-d H:i");

                                $user_dtr_violation_details[$user_dtr_vio_details_count] = [
                                   'userDtrViolation_ID' => $user_dtr_violation_obj->dtrViolationType_ID,
                                   'shift' => $shift_start_end,
                                   'dtr_id' => $break_out_row->dtr_id,
                                   'log' => $formatted_break_out_log,
                                   'type' => "Break",
                                   'entry' => "O",
                                   'minDuration' => 0
                                ];
                                $user_dtr_vio_details_count++;
                                if(count($neededObject[0])> 0){
                                    $break_in_log = new DateTime($neededObject[0]->log);
                                    $formatted_break_in_log = $break_in_log->format("Y-m-d H:i");

                                    $break_out_str = strtotime($formatted_break_out_log);
                                    $break_in_str = strtotime($formatted_break_in_log);

                                    $duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
                                    $break_hrs_to_min = $duration['hoursTotal']*60;
                                
                                    $user_dtr_violation_details[$user_dtr_vio_details_count] = [
                                        'userDtrViolation_ID' => $user_dtr_violation_obj->dtrViolationType_ID,
                                        'shift' => $shift_start_end,
                                        'dtr_id' => $neededObject[0]->dtr_id,
                                        'log' => $formatted_break_in_log,
                                        'type' => "Break",
                                        'entry' => "I",
                                        'minDuration' => $break_hrs_to_min
                                    ];
                                    $user_dtr_vio_details_count++;
                                }
                            }
                            $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                        }else{
                            $data['error'] = 1;
                            $data['error_details'] = [
                                'error' => "Incomplete Break logs",
                                'origin' => 'Related Violation details function - (get_violation_related_details)',
                                'process' => 'getting out for break logs'
                            ];
                        }
                        $recorded_break_logs = count($break_out_logs) + count($break_in_logs);
                        if($recorded_break_logs != $required_break_logs){
                            $data['valid'] = 1;
                            $data['allowable_break'] = $allowable_breaks;
                            $data['break_out_logs'] = $break_out_logs;
                            $data['break_in_logs'] = $break_in_logs;
                        }
                    }else{
                        $data['error'] = 1;
                        $data['error_details'] = [
                            'error' => "Assigned Breaks not found",
                            'origin' => 'Related Violation details function - (get_violation_related_details)',
                            'process' => 'getting of allowable break details'
                        ];
                    }
                }else{
                    $data['error'] = 1;
                    $data['error_details'] = [
                        'error' => "Incomplete Log",
                        'origin' => 'Related Violation details function - (get_violation_related_details)',
                        'process' => 'getting of login and logout for over break details'
                    ];
                }
            }
            else if((int) $user_dtr_violation_obj->dtrViolationType_ID == 7) //INC DTR Logs
            {
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
                if(count($login_raw) > 0 && count($logout_raw) > 0){
                    // "COMPLETE DTR LOGS";
                }else{
                    $login = new DateTime($login_raw->log);
                    $login_formatted = $login->format("Y-m-d H:i");
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                    $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
                    $shift_start_end = $sched->time_start." - ".$sched->time_end;

                    $data['valid'] = 1;
                    $violation_details = [
                        "sched_date" => $sched->schedDate,
                        "shift" => $shift_start_end,
                        "in_id" => $login_raw->dtr_id,
                        "in_log" => $login_formatted,
                        "shift_in" => $new_formatted_shift_start,
                    ];
                    $data['violation_details'] = $violation_details;
                }
            }
            else if((int) $user_dtr_violation_obj->dtrViolationType_ID == 5)// ABSENT
            {
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
                $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
                $shift_start_end = $sched->time_start." - ".$sched->time_end;
                if(count($login_raw) == 0 && count($logout_raw) == 0){
                    // "COMPLETE DTR LOGS";
                    $data['valid'] = 1;
                    $violation_details = [
                        "sched_date" => $sched->schedDate,
                        "shift" => $shift_start_end,
                    ];
                    $data['violation_details'] = $violation_details;
                }
            }
        }
        else
        {
            $data['error'] = 0;
            $data['error_details'] = [
                'error' => "No Sched found",
                'origin' => 'Related Violation details function - (get_violation_related_details)',
                'process' => 'getting of schedule'
            ];
        }
        return $data;
    }

    public function check_one_time_rules($user_dtr_violation_obj, $violation_rules_obj, $violation_details_obj){
        $data['error'] = 0;
        $data['error_details'] = "";
        $data['valid'] = 0;
        if((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //late
        {
            if($violation_details_obj['violation_details']['duration'] >= $violation_rules_obj->oneTimeVal){
                $data['valid'] = 1;
            }
        }
        else if((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) //UT
        {
            if($violation_details_obj['violation_details']['duration'] >= $violation_rules_obj->oneTimeVal){
                $data['valid'] = 1;
            }
        }
        else if((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) // forgot to logout
        {
            if($violation_details_obj['violation_details']['duration'] >= $violation_rules_obj->oneTimeVal){
                $data['valid'] = 1;
            }
        }
        else if((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
        {
            if($violation_details_obj['violation_details']['total_covered'] > $violation_rules_obj->oneTimeVal){
                $data['valid'] = 1;
            }
        }
        return $data;  
    }

    public function check_if_consec_rules_covered($user_dtr_violation_obj, $violation_rules_obj, $first_date){
        $unit = $violation_rules_obj->durationUnit;
        $presc_period = $violation_rules_obj->durationVal;
        $first_date_coverage = strtotime($first_date);
        $covered = 0;
        if($violation_rules_obj->durationType == 1){
            $fixed_rule = $presc_period-1;
            $start_end_coverage = $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date, $fixed_rule, $unit);
            $consec_coverage_from = strtotime($start_end_coverage['start']);
            $consec_coverage_to = strtotime($start_end_coverage['end']);
            if(($consec_coverage_from <= $first_date_coverage) && ($consec_coverage_to >= $first_date_coverage)){
                $covered = 1;
            }else{
                $covered = 0;
            }
        }else if($violation_rules_obj->durationType == 2){
            $custom_rule = $presc_period;
            $consec_coverage_from = strtotime($user_dtr_violation_obj->sched_date . '-'.$custom_rule.' days');
            $consec_coverage_to = strtotime($user_dtr_violation_obj->sched_date);
            if(($consec_coverage_from <= $first_date_coverage) && ($consec_coverage_to >= $first_date_coverage)){
                $covered = 1;
            }else{
                $covered = 0;
            }
        }else if($violation_rules_obj->durationType == 3){
            $prev_categ = 1; //get categ from prev valid IR
            $prev_level = 1; //get level from prev valid IR
            $disc_action_details = $this->get_disciplinary_action_details_prev_occurence($prev_categ, $prev_level);
            $cod_presc_period = $disc_action_details->periodMonth - 1;
            $start_end_coverage = $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date, $cod_presc_period, 'month');
            $consec_coverage_to = strtotime($start_end_coverage['end']);
            $consec_coverage_from = strtotime($start_end_coverage['start']);
            if(($consec_coverage_from <= $first_date_coverage) && ($consec_coverage_to >= $first_date_coverage)){
                $covered = 1;
            }else{
                $covered = 0;
            }
        }
        return $covered;
    }

    private function check_if_consecutive_dates($dtr_consecutive_dates){
        $consec = 1;
        $previous = new DateTime($dtr_consecutive_dates[0]->sched_date); // Set the "previous" value
        unset($dtr_consecutive_dates[0]); // Unset the value we just set to $previous, so we don't loop it twice
        foreach ($dtr_consecutive_dates as $row) { // Loop the object
            $current = new DateTime($row->sched_date);
            $diff = $current->diff($previous);
            // If the difference is exactly 1 day, it's continuous 
            if ($diff->days == 1) 
            {
                $previous =  new DateTime($row->sched_date);
            } 
            else
            {
                $consec = 0;
            }
        }
        return $consec;
    }

    public function compute_accumulated($violation_type, $fetch_accumulated_obj, $violation_rules_obj){
        $accumulated_time = 0;
        $occurence = 0;
        $accumulated_violations = [];
        $accumulated_count = 0;
        foreach($fetch_accumulated_obj as $row){
            $accumulated_time += $row->duration; 
            if(($violation_type == 1) || ($violation_type == 4) || ($violation_type == 2))
            {
                if($row->duration != 0){
                    $accumulated_violations[$accumulated_count] = [
                        'user_dtr_vio_id' => $row->userDtrViolation_ID,
                        'sched_date' => $row->sched_date,
                        'occurence' => $row->occurence,
                        'duration' => $row->duration
                    ];
                    $accumulated_count++;
                }
            }
            else
            {
                $accumulated_violations[$accumulated_count] = [
                    'user_dtr_vio_id' => $row->userDtrViolation_ID,
                    'sched_date' => $row->sched_date,
                    'occurence' => $row->occurence,
                    'duration' => $row->duration
                ];
                $accumulated_count++;
            }
        }
        $occurence = count($fetch_accumulated_obj);
        $data['details'] = $accumulated_violations;

        if(($violation_type == 1) || ($violation_type == 4) || ($violation_type == 2))
        {
            $data['accumulated'] = $accumulated_time;
        }
        else
        {
            $data['accumulated'] = $occurence;
        }
        return $data;
    }

    public function check_non_consecutive_rules($user_dtr_violation_obj, $violation_rules_obj)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $data['valid'] = 0;
        $unit = $violation_rules_obj->durationUnit;
        $presc_period = $violation_rules_obj->durationVal;
        $covered = 0;
        if($violation_rules_obj->durationType == 1)
        {
            // FIXED
            $fixed_rule = $presc_period-1;
            $start_end_coverage = $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date, $fixed_rule, 'month');
            $coverage_to = $start_end_coverage['end'];
            $coverage_from = $start_end_coverage['start'];
        }
        else if($violation_rules_obj->durationType == 2)
        {
            // CUSTOM
            $custom_rule = $presc_period;
            $coverage_from = date("Y-m-d",strtotime($user_dtr_violation_obj->sched_date . '-'.$custom_rule.' days'));
            $coverage_to = $user_dtr_violation_obj->sched_date;
        }
        else if($violation_rules_obj->durationType == 3)
        {
            // COD
            $prev_categ = 1; //get categ from prev valid IR
            $prev_level = 1; //get level from prev valid IR
            $disc_action_details = $this->get_disciplinary_action_details_prev_occurence($prev_categ, $prev_level);
            $cod_presc_period = $disc_action_details->periodMonth - 1;
            $start_end_coverage = $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date, $cod_presc_period, 'month');
            $coverage_to = $start_end_coverage['end'];
            $coverage_from = $start_end_coverage['start'];
        }
        $accumulated = $this->get_user_dtr_violation_by_range($coverage_from, $coverage_to, $user_dtr_violation_obj->user_ID, $user_dtr_violation_obj->dtrViolationType_ID);
        if(count($accumulated) > 0)
        {
            $compute_accumulated = $this->compute_accumulated($user_dtr_violation_obj->dtrViolationType_ID, $accumulated, $violation_rules_obj);
            if($compute_accumulated['accumulated'] >= $violation_rules_obj->nonConsecutiveVal)
            {
                $non_consec_count = 0;

                foreach($compute_accumulated['details'] as $rows){
                    $non_consec_dates[$non_consec_count] = [
                        'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                        'relatedUserDtrViolation_ID' => $rows['user_dtr_vio_id']
                    ];
                    $non_consec_count++;
                }
                var_dump($non_consec_dates);
                $data['add_user_dtr_violation_related_details'] = $this->add_user_dtr_violation_related_details($non_consec_dates);
                $data['valid'] = 1;
                $data['violation_details'] = $compute_accumulated;
            }
        }
        else
        {
            $data['violation_details'] = "no record found";
        }
        return $data;
    }

    public function get_start_and_end_date_coverage($sched_date, $rule, $unit){
        // $sched_date = '2019-02-10';
        // $fixed_rule = 4;
        // $unit = 'week';
        if($unit == 'month')
        {
            $coverage_from_month = Date("Y-m-d", strtotime($sched_date . '-'.$rule.' months'));
            $start = new DateTime($coverage_from_month);
            $date['start'] =  $start->format( 'Y-m')."-01"; 
            $end = new DateTime($sched_date);
            $date['end'] =  $end->format('Y-m-t'); 
        }
        else
        {
            $start_date = new DateTime(Date("Y-m-d", strtotime($sched_date . '-'.$rule.' weeks')));
            $start_year = (int) Date("Y", strtotime($sched_date . '-'.$rule.' weeks'));
            $start_week = (int) Date("W", strtotime($sched_date . '-'.$rule.' weeks'));
            $date['start'] = $start_date->setISODate($start_year, $start_week)->format('Y-m-d');
            $end_year = (int) Date("Y", strtotime($sched_date));
            $end_week = (int) Date("W", strtotime($sched_date));
            $end_date = new DateTime($sched_date);
            $end_date->setISODate($end_year, $end_week)->format('Y-m-d');
            $date['end'] = $end_date->modify('+6 days')->format('Y-m-d');
        }
        return $date;
    }

    public function check_consecutive_rules($user_dtr_violation_obj, $violation_rules_obj){
        $data['error'] = 0;
        $data['error_details'] = "";
        $data['valid'] = 0;
        $days = $violation_rules_obj->consecutiveVal - 1;
        $to = $user_dtr_violation_obj->sched_date;
        $from = Date("Y-m-d ", strtotime($user_dtr_violation_obj->sched_date . '-'.$days.' days'));
        
        $day = $violation_rules_obj->consecutiveVal;
        $dtr_consecutive_dates = $this->get_user_dtr_violation_by_range($from, $to, $user_dtr_violation_obj->user_ID, $user_dtr_violation_obj->dtrViolationType_ID);
        if(count($dtr_consecutive_dates) >= (int) $day)
        {
            $consec = $this->check_if_consecutive_dates($dtr_consecutive_dates);
            if($consec)
            {
                $covered = $this->check_if_consec_rules_covered($user_dtr_violation_obj, $violation_rules_obj, $from);
                if($covered)
                {
                    $consec_count = 0;
                    foreach($dtr_consecutive_dates as $rows){
                        $consec_dates[$consec_count] = [
                            'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                            'relatedUserDtrViolation_ID' => $rows->userDtrViolation_ID
                        ];
                        $consec_count++;
                    }
                    $data['add_user_dtr_violation_related_details'] = $this->add_user_dtr_violation_related_details($consec_dates);
                    $data['valid'] = 1;
                    $data['violation_details'] = $dtr_consecutive_dates;
                }else{
                    $data['violation_details'] = "beyond coverage";
                }
            }else{
                $data['violation_details'] = "dates not consecutive";
            }
        }
        else
        {
            $data['valid'] = 0;
            $data['violation_details'] = "count does not qualify";
        }
        // var_dump($data);
        return $data;
    }

    // public function monthly_check(){
    //     $dateTime = $this->get_current_date_time();
    //     $date = new DateTime($dateTime['date']);
    //     $month = $date->format("m");
    //     echo "Monthnummer: $month";
    // }

    public function weekly_check(){
        $ddate = "2019-02-17";
        $date = new DateTime($ddate);
        $week = $date->format("W");
        echo "Weeknummer: $week";
    }

    // MITE BE USEFUL LATER FOR PRESCRIPTIVE PERIOD CHECKING
    //  INSTEAD OF USER DTR VIOLAION. PREVIOUSE RECORD SHOULD BE THE IR
    public function check_dtr_vio_prescription(){
        $prev_violation = $this->get_previous_user_dtr_violation(1680, 17, 3);
        var_dump($prev_violation);
        
        // if($violation_rules_obj->durationType == 1){
        //     $prev_violation = $this->get_previous_user_dtr_violation(1680, 17, 3);
        //     $prev_str = (int) strtotime($prev_violation->sched_date);
        //     if($prev_str == 0){
        //         echo "reset";
        //     }else{
        //         // $fixed_pres = 1;
        //         $fixed_pres = $violation_rules->durationVal;
        //         // $prev_sched = "2019-01-01";
        //         $prev_sched = $prev_violation->sched_date;
        //         // $current_sched = "2019-01-3";
        //         $current_sched = $user_dtr_violation_obj->sched_date;
        //         // $unit = "month";
        //         // $unit = "week";
        //         $unit = $violation_rules->durationUnit;
        //         if($unit == "month")
        //         {
        //             $current = date("n",strtotime($current_sched));
        //             $from = date("n",strtotime($prev_sched));
        //         }else
        //         {
        //             $current = (int) date("W",strtotime($current_sched));
        //             $from = (int) date("W",strtotime($prev_sched));
        //         }
        //         $to = $from + ($fixed_pres - 1);

        //         echo $from."<br>";
        //         echo $current."<br>";
        //         echo $to."<br>";

        //         if(($from <= $current) && ($to >= $current)){
        //             echo "escalate";
        //         }else{
        //             echo "reset";
        //         }
        //     }
        // }else if($violation_rules_obj->durationType == 2){
            // $custom_presc = 15;
            // $prev_sched = "2019-01-01";
            // $current_sched = "2019-01-30";
            // $prev_str = strtotime($prev_sched);
            // $current_str = strtotime($current_sched);
            // $between_str = $current_str - $prev_str;
            // $days = $between_str/86400;
            // if($days <= $custom_presc)
            // {
            //     echo "escalate";
            // }
            // else
            // {
            //     echo "reset";
            // }
        // }else if($violation_rules_obj->durationType == 3){
                $prev_sched = "2019-01-01";
                $current_sched = "2019-02-28";
                $prev_str = strtotime($prev_sched);
                $current_str = strtotime($current_sched);
                $prev_occurence_offense_id = 1;
                $prev_occurence_num = 1;
                if($prev_occurence_num == 0){
                    $prev_occurence_num = 1;
                }
                // get valid ir details for category
                $prev_categ = 1; //get categ from prev valid IR
                $prev_level = 1; //get level from prev valid IR
                $disc_action_details = $this->get_disciplinary_action_details_prev_occurence($prev_categ, $prev_level);
                var_dump($disc_action_details);

                $from = date("n",strtotime($prev_sched));
                $current = date("n",strtotime($current_sched));
                $to = $from + ($disc_action_details->periodMonth - 1);

                if(($from <= $current) && ($to >= $current)){
                    echo "escalate";
                }else{
                    echo "reset";
                }
                // $offense_details = $this->get_offense_with_type($prev_occurence_offense_id);
                // var_dump($offense_details);
                // $multi_categ_arr = explode(',', $offense_details->multiCategory);
                // $default_categ = $multi_categ_arr['0'];

                // $default_offense
                
        // }
    }

    private function check_if_previous_ir_exist(){
        // get_if previous_ir_exist
        // get_ir_with_current_stat
        // get_ir_with_same_offense and same category
        // identify if curren IR prescription end date still covers the date of currently commited violation
        
        $data['exist'] = 0;
        $data['ir_details'] = "";
        return $data;

    }

    public function check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules){
        $qualified  = 0;
        $qualify['valid'] = 0;
        $qualify['error'] = 0;
            // var_dump($violation_rules);
        if(count($violation_rules) > 0)
        {
            if((int) $violation_rules->oneTimeStat)
            {
                // one time
                if($qualified == 0)
                {
                    $rule_checking_details = $this->check_one_time_rules($user_dtr_violation, $violation_rules, $violation_details);
                    // var_dump($rule_checking_details);
                    if($rule_checking_details['error'] == 0)
                    {
                        $qualify['error'] = 0;
                        if($rule_checking_details['valid'] == 1)
                        {
                            $qualified = 1;
                            $qualify['valid'] = 1;
                            $qualify['occurence_rule_num'] = $qualified;
                            $qualify['occurence_rule'] = "one time";
                            $qualify['related_details'] = $violation_details['violation_details'];
                        }
                    }
                    else
                    {
                        $qualify['error'] = 1;
                        $qualify['error_details'] = $rule_checking_details['error_details'];
                    }
                }
            }
            if((int) $violation_rules->consecutiveStat)
            {
                if($qualified == 0)
                {
                    $rule_checking_details = $this->check_consecutive_rules($user_dtr_violation, $violation_rules);
                    if($rule_checking_details['valid'] == 1)
                    {
                        $qualified = 2;
                        $qualify['valid'] = 1;
                        $qualify['occurence_rule_num'] = $qualified;
                        $qualify['occurence_rule'] = "consecutive";
                        $qualify['related_details'] = $violation_details['violation_details'];
                        $qualify['supporting_details'] = $rule_checking_details['violation_details'];
                    }
                }
            }
            if((int) $violation_rules->nonConsecutiveStat)
            {
                if($qualified == 0)
                {
                    $rule_checking_details = $this->check_non_consecutive_rules($user_dtr_violation, $violation_rules);
                    if($rule_checking_details['valid'] == 1)
                    {
                        $qualified = 3;
                        $qualify['valid'] = 1;
                        $qualify['occurence_rule_num'] = $qualified;
                        $qualify['occurence_rule'] = "non consecutive";
                        $qualify['related_details'] = $violation_details['violation_details'];
                        $qualify['supporting_details'] = $rule_checking_details['violation_details'];
                    }
                }
            }
        }
        else
        {
            $qualify['error'] = 1;
            $qualify['error_details'] = "no violation rules set";
        }
        return $qualify;
    }


    public function set_deadline_to_file_ir(){
        
    }

    public function set_notify_ir_direct_sup(){

    }

    public function auto_notify_ir_direct_sup($user_dtr_violation, $user_emp_details, $auto_ir_notif_rules, $qualified_user_dtr_violation_id){
        var_dump($user_dtr_violation);
        var_dump($user_emp_details);
        var_dump($auto_ir_notif_rules);
        var_dump($qualified_user_dtr_violation_id);

        $direct_sup = $this->get_direct_supervisor_emp($user_dtr_violation->user_ID);
        var_dump($direct_sup);
        // set deadline 
        // set notify ir direct sup
    }

    public function auto_ir_notification_process(){
        $qualified  = 0;
        $qualify['valid'] = 0;
        $qualify['error'] = 0;
        $valid = 0;
        $user_dtr_violation_id = 1687;
        // GET USER DTR VIOLATION
        $user_dtr_violation = $this->get_user_dtr_violation($user_dtr_violation_id);
        if(count($user_dtr_violation) > 0)
        {
            $violation_details = $this->get_violation_related_details($user_dtr_violation);
            if($violation_details['valid']) //check if incident is valid
            {
                $violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID); //Get violation rules  and prescription (violation settings)
                $ir_exist = $this->check_if_previous_ir_exist($user_dtr_violation); // Check if IR of the same violation exist
                if($ir_exist['exist']){
                    // Escalate
                    $valid = 1;
                }else{
                    $auto_ir_notif_rules = $this->check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules);
                    if($auto_ir_notif_rules['valid']){
                        $first_level_disc_action = $this->get_first_level_disciplinary_action($violation_rules->offense_ID, 1);
                        // var_dump($first_level_disc_action);
                        if(count($first_level_disc_action) > 0)
                        {
                            $user_emp_details = $this->get_user_emp_details($user_dtr_violation->user_ID);
                            $valid = 1;
                            $data['occurenceRuleNum'] = $auto_ir_notif_rules['occurence_rule_num'];
                            $data['userDtrViolation_ID'] = $user_dtr_violation_id;
                            $data['disciplinaryAction_ID'] = $first_level_disc_action->disciplinaryAction_ID;
                            $data['offense_ID'] = $violation_rules->offense_ID;
                            $data['emp_id'] = $user_emp_details->emp_id;
                            $data['level'] = 1;
                            $data['status_ID'] = 2;
                            //save IR Details
                            $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($data);
                            $qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
                            //auto_notify_ir_direct_sup
                            $this->auto_notify_ir_direct_sup($user_dtr_violation, $user_emp_details, $auto_ir_notif_rules, $qualified_user_dtr_violation_id);
                        }
                        else
                        {
                            $qualify['error'] = 1;
                            $qualify['error_details'] = "first level disciplinary action not found";
                        }
                        // get 1st level disciplinary action;
                    }else{
                        // check if violation type is ABSENT
                    }
                }
            }
            else
            {
                echo "not valid";
            }
        }
        else
        {
            $qualify['error'] = 1;
            $qualify['error_details'] = "user dtr violation not found";
        }        
        // var_dump($qualify);
    }


    public function check_occurence_rules(){
        $qualified  = 0;
        $qualify['valid'] = 0;
        $qualify['error'] = 0;
        $user_dtr_violation_id = 1685;
        // GET USER DTR VIOLATION
        $user_dtr_violation = $this->get_user_dtr_violation($user_dtr_violation_id);
        if(count($user_dtr_violation) > 0)
        {
            $violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID);
            // var_dump($violation_rules);
            if(count($violation_rules) > 0)
            {
                $prev_ir_exist = $this->check_if_previous_ir_exist($user_dtr_violation, $violation_rules);
                if($prev_ir_exist)
                {
                    // automatically escalate
                }
                else
                {
                    if((int) $violation_rules->oneTimeStat)
                    {
                        // one time
                        if($qualified == 0)
                        {
                            $violation_details = $this->check_one_time_rules($user_dtr_violation, $violation_rules);
                            var_dump($violation_details);
                            if($violation_details['error'] == 0)
                            {
                                $qualify['error'] = 0;
                                if($violation_details['valid'] == 1)
                                {
                                    $qualified = 1;
                                    $qualify['valid'] = 1;
                                    $qualify['occurence_rule_num'] = $qualified;
                                    $qualify['occurence_rule'] = "one time";
                                    $qualify['supporting_details'] = $violation_details['violation_details'];
                                }
                            }
                            else
                            {
                                $qualify['error'] = 1;
                                $qualify['error_details'] = $violation_details['error_details'];
                            }
                        }
                    }
                    if((int) $violation_rules->consecutiveStat)
                    {
                        if($qualified == 0)
                        {
                            $violation_details = $this->check_consecutive_rules($user_dtr_violation, $violation_rules);
                            if($violation_details['valid'] == 1)
                            {
                                $qualified = 2;
                                $qualify['valid'] = 1;
                                $qualify['occurence_rule_num'] = $qualified;
                                $qualify['occurence_rule'] = "consecutive";
                                $qualify['supporting_details'] = $violation_details['violation_details'];
                            }
                        }
                    }
                    if((int) $violation_rules->nonConsecutiveStat)
                    {
                        if($qualified == 0)
                        {
                            $violation_details = $this->check_non_consecutive_rules($user_dtr_violation, $violation_rules);
                            if($violation_details['valid'] == 1)
                            {
                                $qualified = 3;
                                $qualify['valid'] = 1;
                                $qualify['occurence_rule_num'] = $qualified;
                                $qualify['occurence_rule'] = "non consecutive";
                                $qualify['supporting_details'] = $violation_details['violation_details'];
                            }
                        }
                    }
                }
            }
            else
            {
                $qualify['error'] = 1;
                $qualify['error_details'] = "no violation rules set";
            }
        }
        else
        {
            $qualify['error'] = 1;
            $qualify['error_details'] = "user dtr violation not found";
        }        
        var_dump($qualify);
    }

    public function check_irable_rules(){
        $violation_type = 1;
        var_dump($violation_rules); 
        
        if(($violation_type == 1) || ($violation_type == 2) || ($violation_type == 4)){
        // late, undertime, overbreak
        }else if($violation_type == 3){
        // forgot to logout
        }else if(($violation_type == 6) || ($violation_type == 7)){
        // incomplete break
        }
    }

    public function user_dtr_vio_generator($sched_id, $sched_date){
        $data['user_ID'] = 275;
        $data['supervisor_ID'] = 26;
        $data['dtrViolationType_ID'] = 1;
        $data['sched_ID'] = $sched_id;
        $data['sched_date'] = $sched_date;
        $data['month'] = date("F",strtotime($sched_date));        
        $data['year'] = date("Y",strtotime($sched_date));
        return $this->general_model->insert_vals_last_inserted_id($data, 'tbl_user_dtr_violation');        
    }

    public function log_generator($log, $acc_time_id, $entry, $type, $sched_id = NULL, $note = NULL){
        // LOGIN LOG
        $login['emp_id'] = 557;
        $login['acc_time_id'] = $acc_time_id;
        $login['entry'] = $entry;
        $login['type'] = $type;
        $login['log'] = $log;
        $login['sched_id'] = $sched_id;
        $login['note'] = $note;
        return $this->general_model->insert_vals_last_inserted_id($login, 'tbl_dtr_logs');
    }

    public function sched_generator(){
        $sched_date = "2019-02-20";

        $sched['emp_id'] = 557;
        $sched['sched_date'] = $sched_date;
        $sched['schedtype_id'] = 1;
        $sched['acc_time_id'] = 1833;
        $sched['remarks'] = 'Excel';
        $sched['updated_by'] = 0;
        $sched['isActive'] = 1;
        $sched_id = $this->general_model->insert_vals_last_inserted_id($sched, 'tbl_schedule');
        var_dump($sched_id);
    }

    public function dtr_transaction(){
        // 161343, 161345, 161346
        $sched_date = '2019-02-20';

        // LOGIN
        $log = "2019-02-20 21:01:50";
        $acc_time_id = 1833;
        $entry = 'I';
        $type = "DTR";
        $sched_id = 161346;
        $note = NULL;

        // // BREAK IN
        // $log = "2019-02-19 01:20:00";
        // $acc_time_id = 4614;
        // $entry = 'I';
        // $type = "Break";
        // $sched_id = NULL;

        // // BREAK OUT
        // $log = "2019-02-19 02:20:00";
        // $acc_time_id = 4614;
        // $entry = 'O';
        // $type = "Break";
        // $sched_id = NULL;
        // $note = '';

        // // LOGOUT
        // $log = "2019-02-19 06:50:20";
        // $acc_time_id = 1833;
        // $entry = 'O';
        // $type = "DTR";
        // $sched_id = '';
        // $note = '';

        $log_gen = $this->log_generator($log, $acc_time_id, $entry, $type, $sched_id, $note);
        var_dump($log_gen);
        $user_data_vio = $this->user_dtr_vio_generator($sched_id, $sched_date);
        var_dump($user_data_vio);
    }




}
