<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Dashboard extends General {

    protected $title = 'Dashboard';

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => $this->title,
        ];
        $fields = "a.apid,fname,lname,pic,emp_id,gender,acc_name,birthday";
        $where = "a.apid=b.apid and c.acc_id=b.acc_id and month(birthday)=" . date("m") . " and day(birthday)=" . date("d") . " and b.isActive='yes'";
        $result = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_applicant a,tbl_employee b,tbl_account c');
        foreach ($result as $row)
        {
            $job = $this->getCurrentPosition($row->emp_id);
            $data["celebrants"][$row->emp_id] = array(
                "emp_id" => $row->emp_id,
                "apid" => $row->apid,
                "fname" => $row->fname,
                "lname" => $row->lname,
                "pic" => $row->pic,
                "gender" => $row->gender,
                "acc_name" => $row->acc_name,
                "birthday" => $row->birthday,
                "job" => $job,
            );
        }

        $data['highfives'] = $this->_show_top_high_five_stats();

        $this->load_template_view('templates/dashboard/index', $data);
    }

    public function get_leave_credits()
    {
        $leaveType_IDS = array(5, 6, 2); //BL,SL,VL
        $leave_credits = array();
        foreach ($leaveType_IDS as $leaveType_ID)
        {
            $credits = $this->get_yearly_leave_credits($leaveType_ID);
            $credits['leaveType'] = $this->general_model->fetch_specific_val("leaveType", "leaveType_ID=$leaveType_ID", "tbl_leave_type")->leaveType;
            $leave_credits[] = $credits;
        }
        echo json_encode(array('leave_credits' => $leave_credits));
    }

    private function _show_top_high_five_stats()
    {
        $img = base_url() . "assets/images/";
        $uid = $this->session->userdata('uid');

        $user_stat = $this->general_model->fetch_specific_val('count(*) as count', ['senderId' => $uid], 'tbl_szfive_high_fives');
        $user_receive_count = $this->general_model->fetch_specific_val('count(*) as count', ['recipientId' => $uid], 'tbl_szfive_high_five_details');
        
        $user_stat->sent_count = $user_stat->count;
        $user_stat->receive_count = $user_receive_count->count;
        $highfives['my_stats'][] = $user_stat;

        $rdata = $this->_get_top_highfive(['img' => $img, 'id' => 'recipientId', 'table' => 'tbl_szfive_high_five_details']);
        if (!empty($rdata)) {
            $rdata[0]->hi5_user = 'receiver';
            $rdata[0]->title = 'Most Appreciated';
            $rdata[0]->header_color = '#00a2d4';
            $highfives['top_highfives'][] = $rdata[0];
        }

        $sdata = $this->_get_top_highfive(['img' => $img, 'id' => 'senderId', 'table' => 'tbl_szfive_high_fives']);
        if (!empty($sdata)) {
            $sdata[0]->hi5_user = 'sender';
            $sdata[0]->title = 'Top High Fiver';
            $sdata[0]->header_color = '#118ecc';
            $highfives['top_highfives'][] = $sdata[0];
        }

        return $highfives;
    }
    
    private function _get_top_highfive($criteria)
    {
        $img = $criteria['img'];
        $id = $criteria['id'];
        $table = $criteria['table'];

        $query = "SELECT t.uid, d.fname,d.lname, u.$id, CONCAT('$img', d.pic) as pic, j.pos_details, c.acc_description, c.acc_name, count(*) as count  FROM $table as u JOIN tbl_user as t ON t.uid = u.$id JOIN tbl_employee as b ON b.emp_id = t.emp_id JOIN tbl_account as c ON c.acc_id = b.acc_id JOIN tbl_applicant as d ON d.apid = b.apid LEFT JOIN tbl_emp_promote as h ON (h.emp_id = b.emp_id AND h.isActive = 1) LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = h.posempstat_id) LEFT JOIN tbl_position as j ON (j.pos_id = i.pos_id) ";

        if (isset($criteria['uid'])) {
            $query .= " WHERE u.$id = " . $criteria['uid'];
        }

        $query .= " GROUP BY $id ORDER BY count(*) DESC LIMIT 1";

        return $this->general_model->custom_query($query);
    }
}
