<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AttndMonitoring extends CI_Controller {

	public function __construct(){
		parent::__construct(); 
		$this->load->model('AttndMonitoringModel');
	}
	public function index(){if($this->session->userdata('uid')){
		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='AttndMonitoring' and is_assign=1");
		foreach ($qry->result() as $row)
		{
			$arr['setting'] = array("settings" => $row->settings);
		}
		if ($qry->num_rows() > 0){
					// $this->load->view('attendancelist');

			$this->load->view('attendancelist',$arr);
		}else{
			redirect('index.php/home/error');
		}
	}else{
		redirect('index.php/login');
	}
}
public function getAttendancelist(){
	$fromDate = $this->input->post('fromDate');
	$toDate = $this->input->post('toDate');
	$employeelist = $this->input->post('employeelist');	
	// $fromDate = "2017-11-21";
	// $toDate = "2017-11-21";
		// $employeelist = "109-436-137-294-26-66-341-557";
	// $employeelist = "378";
	$employeelist = explode('-',$employeelist);

	$employees = $this->AttndMonitoringModel->getEmployeeDetails($employeelist);
	$period = new DatePeriod(
		new DateTime("$fromDate"),
		new DateInterval('P1D'),
		new DateTime("$toDate +1 day ")
	);
	foreach ($period as $date) {
		$dates[] = $date->format("Y-m-d");
	}

	foreach($employees as $emp){
		$dtrin = $this->AttndMonitoringModel->getDTRIns($emp->emp_id,reset($dates),end($dates));

		//--------------------------------TESTING AREA -------------------------------------------------------------------------
		// $x = new stdClass();
		// foreach($dtrin as $din){
		// 	$logindate = date('Y-m-d', strtotime($din->log_in));

		// 	// echo "<pre>";
		// 	// var_dump($datetest);
		// 	// echo "</pre>";
		// }
		$sortedData = array();
		foreach ($dtrin as $din) {
			$timestamp = strtotime($din->log_in);
		    $date = date("Y-m-d", $timestamp); //truncate hours:minutes:seconds
		    if ( ! isSet($sortedData[$date]) ) { //first entry of that day
		    	$sortedData[$date] = array($din);
		    } else { //just push current element onto existing array
		    	$sortedData[$date][] = $din;
		    }
		    
		}
		// $keys = array();
		foreach ($sortedData as $key => $value) { //2
			$withSched = array();
			$withSchedkey = array();
			for($xx=0;$xx<count($sortedData[$key]);$xx++){
				if($sortedData[$key][$xx]->sched_id!=NULL){
					$withSched[] = $sortedData[$key][$xx]->sched_id;
					$withSchedkey[] = $xx;
				}
			}
			if(empty($withSched)){
				for($xx=1;$xx<count($sortedData[$key]);$xx++){
					unset($sortedData[$key][$xx]);
				}
			}else if(count($withSched)==count($sortedData[$key])){
				for($xx=1;$xx<count($sortedData[$key]);$xx++){
					unset($sortedData[$key][$xx]);
				}
			}else{
				for($xx=0;$xx<count($sortedData[$key]);$xx++){
					if($xx!=reset($withSchedkey)){
						unset($sortedData[$key][$xx]);
					}
					
				}
			}
		}
		$dtrin = array();
		foreach($sortedData as $value){
			foreach($value as $val){
				$dtrin[] = $val;
			}
		}

		// //--------------------------------TESTING AREA -------------------------------------------------------------------------
		$dtrout = $this->AttndMonitoringModel->getDTROuts($emp->emp_id,reset($dates),end($dates));
		foreach($dtrin as $din){
			$naa=false;
			foreach($dtrout as $dout){
				if($din->dtr_id_in==$dout->note){
					$din->dtr_id_out = $dout->dtr_id_out;
					$din->log_out = $dout->log_out;
					$din->log_out_view = $dout->log_out_view;
					$naa=true;

				}
			}

			if($naa==false){
				$din->dtr_id_out = null;
				$din->log_out = null;
			}
		}
		$daters = (object)$dates;
		$datex = array();	
		foreach($daters as $date){
			$val = $this->AttndMonitoringModel->getEmpSchedule($emp->emp_id,$date);
			if(!empty($val)||$val!=NULL){
				if($val->leavenote==NULL||$val->leavenote==0){
					$val->leavenote = NULL;
				}else{
					$leavename = $this->AttndMonitoringModel->getLeaveNote($val->leavenote);
					$val->leavenote = $leavename->leave_name;
				}
				$schedule = $val;
			}else{
				$schedule = NULL;
			}
			$schedStat = '-';
			$naa=false;
			foreach($dtrin as $din){
				if($din->date==$date){
					$logs = $din;
					$naa=true;
					if($din->dtr_id_out==NULL){
						$fb = $this->getFirstBreak($emp->emp_id,$din->dtr_id_in);
						$lunchb = $this->getLunchBreak($emp->emp_id,$din->dtr_id_in);
						$lb = $this->getLastBreak($emp->emp_id,$din->dtr_id_in);
					}else{
						$fb = $this->getFirstBreak($emp->emp_id,$din->dtr_id_in,$din->dtr_id_out);
						$lunchb = $this->getLunchBreak($emp->emp_id,$din->dtr_id_in,$din->dtr_id_out);
						$lb = $this->getLastBreak($emp->emp_id,$din->dtr_id_in,$din->dtr_id_out);
					}
					if($din->sched_id==NULL){
						$schedStat = '<span class="font-red">Not Tagged</span>';
					}else{
						$schedStat = '<span class="font-green">Tagged</span>';
					}
				}
			}
			if($naa==false){
				$logs = NULL;
				$fb = NULL;
				$lunchb = NULL;
				$lb = NULL;
			}


			$totalbreaks = 0;
//---------------------------------CHANGES---------------------------------------------
			if(empty($fb)){
				$totalhours = '-';
			}else if(count($fb)==1){
				$totalhours = '-';
			}else{
				$datetime1 = strtotime($fb[0]->log);
				$datetime2 = strtotime($fb[1]->log);
				$interval  = abs($datetime2 - $datetime1);
				$minutes   = round($interval / 60);
				$totalbreaks += $minutes;
			}
			if(empty($lunchb)){
				$totalhours = '-';
			}else if(count($lunchb)==1){
				$totalhours = '-';
			}else{
				$datetime1 = strtotime($lunchb[0]->log);
				$datetime2 = strtotime($lunchb[1]->log);
				$interval  = abs($datetime2 - $datetime1);
				$minutes   = round($interval / 60);
				$totalbreaks += $minutes;
			}
			if(empty($lb)){
				$totalhours = '-';
			}else if(count($lb)==1){
				$totalhours = '-';
			}else{
				$datetime1 = strtotime($lb[0]->log);
				$datetime2 = strtotime($lb[1]->log);
				$interval  = abs($datetime2 - $datetime1);
				$minutes   = round($interval / 60);
				$totalbreaks += $minutes;
			}


//-----------------------------------------CHANGES--------------------------------------------
			//NEW CHANGE--------------------------------------------------------------------

			if(empty($fb)){
				$fbAdd = 0;
			}else{
				$res = $this->AttndMonitoringModel->getBreakTime('FIRST BREAK',$emp->acc_id);
				if(empty($res)){
					$fbAdd = 0;
				}else{
					$hour = $res->hour;
					$min = $res->min;
					$hourtomin = $hour * 60;
					$fbAdd = $hourtomin+$min;
				}
			}
			if(empty($lunchb)){
				$lunchAdd = 0;
			}else{
				$res = $this->AttndMonitoringModel->getBreakTime('LUNCH',$emp->acc_id);
				if(empty($res)){
					$lunchAdd = 0;
				}else{
					$hour = $res->hour;
					$min = $res->min;
					$hourtomin = $hour * 60;
					$lunchAdd = $hourtomin+$min;
				}
			}
			if(empty($lb)){
				$lbAdd = 0;
			}else{
				$res = $this->AttndMonitoringModel->getBreakTime('LAST BREAK',$emp->acc_id);
				if(empty($res)){
					$lbAdd = 0;
				}else{
					$hour = $res->hour;
					$min = $res->min;
					$hourtomin = $hour * 60;
					$lbAdd = $hourtomin+$min;
				}
			}
			$totalbreaktimes = $fbAdd+$lunchAdd+$lbAdd;

			//END OF NEW CHANGE-------------------------------------------------------------------

			$status = "<td width='80px'>None</td>";
			if($schedule==NULL){
				$totalhours = 0;
			}else if($logs==NULL){

				$totalhours = 0;
			}else if($logs->log_out==NULL){
				$totalhours = '-';
			}else if($schedule->type=='Normal' || $schedule->type=='WORD'){
					//---------------------------------------------------------

				if($schedule->time_start=='12:00:00 AM'){
					$schedindt = strtotime("+1 day", strtotime($date.' '.$schedule->time_start));
				}else{
					$schedindt = strtotime($date.' '.$schedule->time_start);
				}

				//changes
					$ts = strtotime($schedule->time_start);
					$te = strtotime($schedule->time_end);
					// $ttend = abs($te - $ts) / 60;
					if($te<$ts){
						$schedoutdt = strtotime("+1 day", strtotime($date.' '.$schedule->time_end));
					}else{
						$schedoutdt = strtotime($date.' '.$schedule->time_end);
					}
				//end changes
				
				// $schedoutdt = strtotime($date.' '.$schedule->time_end);
				$logindt = strtotime($logs->log_in);
				$logoutdt = strtotime($logs->log_out);
				$BCTdt = strtotime('-10 minutes',$schedindt);
				if($logindt<$schedindt){
					$starttouse = $schedindt;
					// echo 'early';
				}else{
					$starttouse = $logindt;
					// echo 'late';
				}
				if($logoutdt<$schedoutdt){
					$endtouse = $logoutdt;
					// echo 'mins undertime';
				}else if($logoutdt<$schedindt){
					// echo "No TIME";
					$endtouse = 0;
					$starttouse = 0;
				}else{
					// echo 'normal';
					$endtouse = $schedoutdt;
				}
				if ($endtouse < $starttouse) {
					$endtouse += 86400;
					// $schedoutdt += 86400;
					// echo "Next Day";
				}
				// echo $starttouse.' '.$endtouse;

				$overbreak = false;
		//TOTALHOURS WITH BREAK BASE----------------------------------------------------------------------------------------
				// echo '<br>'.$totalbreaktimes.'<br>';
				// echo  $starttouse.' upto '.$endtouse.' => ';
				$totalhours = $endtouse-$starttouse-($totalbreaktimes*60);
				// echo $totalhours.' => ';
				$h = $totalhours / 3600 % 24;
				$m = $totalhours / 60 % 60; 
				if($h<0 || $m<0){
					$totalhours = "<span class='font-warning'>0 hrs & 0 mins</span>";
				}else{
					$totalhours = "<span>$h hrs & $m mins</span>";
				}
				
		//END -------------------- TOTALHOURS WITH BREAK BASE-----------------------------------------------------------------
				// echo date("g-i",$totalhours).'<br>';
				if($logindt<$BCTdt){
					$interval  = strtotime("+10 minutes",abs($logindt - $BCTdt));
					if($logoutdt<$schedindt){
						$status   = '<td width="80px"><span class="text-danger">Logs are not between shift</span>';
					}else if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-success">'.floor($interval / 60) .' mins early</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-success">'.floor($interval / 60) .' mins early</span></td>';
					}
					// echo 'early';
				}else if($logindt==$BCTdt){
					$status   = '<td width="80px" class="text-success">On Time</td>';
					if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-success">On Time</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-success">On Time</span></td>';
					}
				}else if($logindt<$schedindt){
					$interval  = abs($logindt - $schedindt);
					if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-warning">'.abs(floor(($interval / 60)-10)) .' mins late BCT</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-warning">'.abs(floor(($interval / 60)-10)) .' mins late BCT</span></td>';
					}
						//echo 'bct late'
				}else if($logindt==$schedindt){
					if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-warning">10 mins late BCT</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-warning">10 mins late BCT</span></td>';
					}
				}else{
					$interval  = abs($logindt - $schedindt);
					if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-danger">'.floor($interval / 60) .' mins late ACT</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-danger">'.floor($interval / 60) .' mins late ACT</span></td>';
					}
						// echo 'act late';
				}


			}else{
				$totalhours = 0;
			}

			if($emp->acc_id==1){
				if($totalbreaks<=90){
					$totalbreaks = $totalbreaks.' mins / '.floor($totalbreaktimes).' mins';
				}else{
					$totalbreaks = "<span class='font-red'>$totalbreaks mins</span> / ".floor($totalbreaktimes)." mins";
				}
			}else{
				if($totalbreaks<=60){
					$totalbreaks = $totalbreaks.' mins / '.floor($totalbreaktimes).' mins';
				}else{
					$totalbreaks = "<span class='font-red'>$totalbreaks mins</span> / ".floor($totalbreaktimes)." mins";
				}
			}

			$datex[] = array('date'=>$date,'schedule'=>$schedule,'logs'=>$logs,'firstbreak'=>$fb,'lunchbreak'=>$lunchb,'lastbreak'=>$lb,'totalhours'=>$totalhours,'totalbreaks'=>$totalbreaks,'status'=>$status,'schedStat'=>$schedStat);
		}
		$emp->details = $datex;
	}

	// echo "<pre>";
	// var_dump($employees);
	// echo "</pre>";
	echo json_encode($employees);
}

public function getFirstBreak($emp_id,$dtr_id_in,$dtr_id_out=NULL){
	if($dtr_id_out==NULL){
		$dtr_id_out = $dtr_id_in + 10000;
	}
	$fb = $this->AttndMonitoringModel->getBreaks(2,$emp_id,$dtr_id_in,$dtr_id_out);
	return $fb ;
}
public function getLunchBreak($emp_id,$dtr_id_in,$dtr_id_out=NULL){
	if($dtr_id_out==NULL){
		$dtr_id_out = $dtr_id_in + 10000;
	}
	$lb = $this->AttndMonitoringModel->getBreaks(3,$emp_id,$dtr_id_in,$dtr_id_out);
	return $lb;
}
public function getLastBreak($emp_id,$dtr_id_in,$dtr_id_out=NULL){
	if($dtr_id_out==NULL){
		$dtr_id_out = $dtr_id_in + 10000;
	}
	$lb = $this->AttndMonitoringModel->getBreaks(4,$emp_id,$dtr_id_in,$dtr_id_out);
	return $lb;
}
}
