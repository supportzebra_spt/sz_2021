<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $savefile = "/usr/local/www/apache24/data/supportzebra/reports/payroll/"; 
 
	public function index()
	{
		$this->load->view('welcome_message');

	}
	
	public function pdffs()
	{
	
	$savefile2 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/mark3/"; 
	$query = $this->db->query("SELECT b.emp_id, fname, lname,month,year,period,(select password from tbl_user x where x.emp_id=b.emp_id) as password, a.* FROM `tbl_payroll` a, tbl_emp_promote b, tbl_employee c, tbl_applicant d,tbl_payroll_coverage e where e.coverage_id=16 and a.emp_promoteID = b.emp_promoteID and b.emp_id = c.emp_id and c.apid= d.apid and e.coverage_id=a.coverage_id");
 
	$this->load->library('Pdf');
foreach($query->result() as $row){
// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetProtection(array('print', 'copy','modify'), $row->password, $row->password, 0, null);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('SupportZebra Payslip - MJM');
$pdf->SetTitle('Payslip');
$pdf->SetSubject('Payslip');
$pdf->SetKeywords('Payslip, Payslip, Payslip, Payslip, Payslip');

$PDF_HEADER_LOGO = "logo2.png";//any image file. check correct path.
$PDF_HEADER_LOGO_WIDTH = "50";
$PDF_HEADER_TITLE = "SupportZebra Payslip";
$PDF_HEADER_STRING = " Tel: +63(88)8551519 \n Mob: +63(17)7007656\n Email: recruitement@supportzebra.com\n Website: www.supportzebra.com \n\n";
$pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE,$PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}


// add a page
$pdf->AddPage();

 
$pdf->SetFont('helvetica', '', 8);

 $pdf->Multicell(0,2," \n \n "); 

// NON-BREAKING TABLE (nobr="true")
$tax = explode("(",$row->bir_details);
$tax_type = substr_replace($tax[1] ,"",-1);
$clothing=explode("(",$row->clothing);
$laundry=explode("(",$row->laundry);
$rice=explode("(",$row->rice);
$tbl = '
<style type="text/css">
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
	} 
	.tableizer-table td {
		padding: 4px;
		margin: 3px;
		border: 1px solid #CCC;
	}
	.tableizer-firstrow th {
		padding:17px;
	}
	.tableizer-table th {
		background-color: #104E8B; 
		color: #FFF;
		font-weight: bold;
		text-align:center
	}
</style>
 
<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th colspan="4">PAY STATEMENT - EMPLOYEES COPY</th></tr></thead><tbody>
  <tr><td>NAME:      </td><td>'.$row->lname.', '.$row->fname.'</td><td>POSITION</td><td>CCA1</td></tr>
 <tr><td>Date:        </td><td>September 16-30, 2017</td><td>TAX STATUS</td><td>'.$tax_type.'</td></tr>
 <tr><td>Period Covered:       </td><td>October 5, 2017</td><td> ATM </td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>EARNINGS:</td><td>SALARY</td><td colspan="2"> '.$row->quinsina.' </td></tr>
 <tr><td>&nbsp;</td><td>CLOTHING ALLOWANCE</td><td colspan="2">'.$clothing[0].'</td></tr>
 <tr><td>&nbsp;</td><td>LAUNDRY ALLOWANCE</td><td colspan="2"> '.$laundry[0].' </td></tr>
 <tr><td>&nbsp;</td><td>RICE SUBSIDY</td><td colspan="2"> '.$rice[0].' </td></tr>
 <tr><td>&nbsp;</td><td>NIGHT DIFF</td><td  colspan="2"> '.$row->nd.' </td></tr>
 <tr><td>&nbsp;</td><td>OVERTIME</td><td colspan="2"> '.$row->ot_bct.' </td></tr>
 <tr><td>&nbsp;</td><td>HAZARD PAY</td><td colspan="2"> '.$row->hp.' </td></tr>
 <tr><td>&nbsp;</td><td>HOLIDAY</td><td colspan="2"> '.$row->ho.'</td></tr>
 <tr><td>&nbsp;</td><td>SALARY ADJUSTMENT</td><td colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>Total Earnings</td><td colspan="2"> -- </td></tr>
 <tr><td>DEDUCTIONS: </td><td>WITHHOLDING TAX</td><td colspan="2"> '.$tax[0].'</td></tr>
 <tr><td>&nbsp;</td><td>SSS PREMIUM</td><td colspan="2"> '.$row->sss_details.'</td></tr>
 <tr><td>&nbsp;</td><td>SSS LOAN</td><td colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>HDMF PREMIUM</td><td  colspan="2"> '.$row->hdmf_details.' </td></tr>
 <tr><td>&nbsp;</td><td>HDMF LOAN</td><td  colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>PHILHEALTH</td><td  colspan="2"> '.$row->phic_details.' </td></tr>
 <tr><td>&nbsp;</td><td>LEAVE/UNDERTIME/OB/TARDINESS</td><td  colspan="2"> --  </td></tr>
 <tr><td>&nbsp;</td><td>EMPLOYMENT BOND</td><td colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>CANTEEN</td><td  colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>CASH ADVANCE</td><td  colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>INTELLICARE</td><td  colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>GYM MEMBERSHIP</td><td colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>GARAGE/SZ TSHIRT</td><td  colspan="2">175.00 </td></tr>
 <tr><td>&nbsp;</td><td>Total Deductions</td><td  colspan="2"> -- </td></tr>
 <tr><td>TAKE HOME PAY</td><td>&nbsp;</td><td  colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td  colspan="2">&nbsp;</td></tr>
 <tr><td>Signature:</td><td>&nbsp;</td><td>Date:</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td colspan="4">Note: Keep this statement permanently for Income Tax purposes.</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
</tbody></table>';

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

 
// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output($savefile2.'Payslip_'.$row->lname.'_'.$row->month.'_'.$row->period.'_'.$row->emp_id.'.pdf', 'F');

	}
	}
}
