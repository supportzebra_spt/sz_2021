<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kudos extends CI_Controller {

	/* public $file = 'C:\inetpub\wwwroot\nicca\sz3\reports\uploaded.xlsx';  for Windows
	public $logo= 'C:\inetpub\wwwroot\nicca\sz3\assets\images\logo2.png';
	public $savefile = "C:\\inetpub\\wwwroot\\nicca\\sz3\\reports\\"; */
	
	public $file = '/usr/local/www/apache24/data/supportzebra/reports/uploaded.xlsx';   //for linux
	public $logo= '/usr/local/www/apache24/data/supportzebra/assets/images/logo2.png';
	public $savefile = "//usr//local//www//apache24//data//supportzebra//reports//"; 
	
	// public $file= "C:\\xampp\\htdocs\\supportzebra\\reports\\uploaded.xlsx";
	// public $logo= "C:\\xampp\\htdocs\\supportzebra\\assets\\images\\logo2.png";
	// public $savefile= "C:\\xampp\\htdocs\\supportzebra\\reports\\";
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Kudos2Model");
	}
	public function uploadExcel()
	{
		$path = $_FILES["uploadFile"]["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Manila');	

		$target_dir = "./reports/";
		$new_name = "uploaded";
		$target_file = $target_dir.$new_name.'.'.$extension;

		$allowed = array('xls', 'xlsx', 'csv');

		if($_FILES["uploadFile"]["name"]){
			if(!in_array($extension,$allowed)){
				echo 'File type is not allowed.';
			} else {
				move_uploaded_file($_FILES["uploadFile"]["tmp_name"],$target_file);
				echo 'Successfully Uploaded.';
			}

		}
	}

	public function viewAddMultiple()
	{
		$this->load->library('PHPExcel', NULL, 'excel');
		// $file = base_url().'reports/uploaded.xlsx';
		$this->excel = PHPExcel_IOFactory::load($this->file);
		//get only the Cell Collection
		$cell_collection = $this->excel->getActiveSheet()->getCellCollection();

		for($rows=4;$rows<=18;$rows++){

			$empidd= $this->excel->getActiveSheet()->getCell('A'.$rows)->getValue();

			if($empidd=="" || empty($empidd) || $empidd==null){
				// echo "Nicca";
			} else{
				// echo $empidd;
				$datasss = $this->Kudos2Model->getEmployeeAgent($empidd);
				foreach ($datasss as $key => $value) {
					echo "<tr>";
					echo "<td>".$value->fname." ".$value->lname."</td>";
					echo "<td>".$value->acc_name."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('B'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('C'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('D'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('E'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('F'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('G'.$rows)->getValue()."</td>";
					echo "<td>".$this->excel->getActiveSheet()->getCell('H'.$rows)->getFormattedValue()."</td>";
					echo "</tr>";
				}
			}
		} 
	}

	public function addAddMultiple()
	{
		$this->load->library('PHPExcel', NULL, 'excel');

		$this->excel = PHPExcel_IOFactory::load($this->file);
		//get only the Cell Collection
		$cell_collection = $this->excel->getActiveSheet()->getCellCollection();

		for($rows=4;$rows<=18;$rows++){
			$empidd= $this->excel->getActiveSheet()->getCell('A'.$rows)->getValue();
			if($empidd!="" || !empty($empidd) || $empidd!=null){
				$datasss = $this->Kudos2Model->getEmployeeAgent($empidd);
				foreach ($datasss as $key => $value) {
					$emp_id = $value->emp_id;
					$acc_id = $value->acc_id;
					$k_type = $this->excel->getActiveSheet()->getCell('B'.$rows)->getValue();
					$r_type = $this->excel->getActiveSheet()->getCell('C'.$rows)->getValue();
					$c_name = $this->excel->getActiveSheet()->getCell('D'.$rows)->getValue();
					$c_pnumber = $this->excel->getActiveSheet()->getCell('E'.$rows)->getValue();
					$c_email = $this->excel->getActiveSheet()->getCell('F'.$rows)->getValue();
					$c_comment = $this->excel->getActiveSheet()->getCell('G'.$rows)->getValue();
					$d_given = $this->excel->getActiveSheet()->getCell('H'.$rows)->getFormattedValue();
					$uid = $this->session->userdata('uid');
					$this->Kudos2Model->createkudos($emp_id,$acc_id,$k_type,$r_type,$c_name,$c_pnumber,$c_email,$c_comment,$uid,$d_given,'Excel');
				}
			} 
		} 
	}
	//----------------------------------NICCACODES-----------------------------------

	public function kudoslistexcel($startdate=NULL,$enddate=NULL){
		// $startdate = '2017-09-12';$enddate = '2017-09-12';
		$startdate=date_create_from_format("m-d-Y",$startdate);
		$enddate=date_create_from_format("m-d-Y",$enddate);
		$startdate = date_format($startdate,"Y-m-d");
		$enddate = date_format($enddate,"Y-m-d");
		$this->load->library('PHPExcel', NULL, 'excel');

		//load our new PHPExcel library
		// $this->load->library('excel');
//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Nicca WorkSheet');



//-------------------------------------------------------------------
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($this->logo);
      	$objDrawing->setOffsetX(0);    // setOffsetX works properly
		$objDrawing->setOffsetY(0);  //setOffsetY has no effect
		$objDrawing->setCoordinates('B1');
		$objDrawing->setHeight(100); // logo height
		// $objDrawing->setWidth(320); // logo width
		// $objDrawing->setWidthAndHeight(200,400);
		$objDrawing->setResizeProportional(true);
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
//----------------------------------------------------------------------
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38);
		$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(38);


		$this->excel->getActiveSheet()->setCellValue('F1', "Date Downloaded:");
		$this->excel->getActiveSheet()->setCellValue('F2', DATE('F d, Y h:i a'));
		$this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setWrapText(true); 
//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A3', 'Ambassador Name');
		$this->excel->getActiveSheet()->setCellValue('B3', 'Campaign');
		$this->excel->getActiveSheet()->setCellValue('C3', 'Kudos Type');
		$this->excel->getActiveSheet()->setCellValue('D3', 'Kudos Card');
		$this->excel->getActiveSheet()->setCellValue('E3', 'Reward Status');
		$this->excel->getActiveSheet()->setCellValue('F3', 'Date Added');
//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:E2');
//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A1:E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('F1:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('F2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

		$this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
		$this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'ffffff'))));
		$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => '000000'))));


		$this->excel->getActiveSheet()->setAutoFilter('A3:F3');
		$this->excel->getActiveSheet()->getProtection()->setSort(true);


		// $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		// $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(28);

		$kudoses = $this->Kudos2Model->getKudosCustom($startdate,$enddate);
		$x = 4;$bool = TRUE;
		foreach($kudoses as $k){
			if($bool==TRUE){
				$this->excel->getActiveSheet()->getStyle('A'.$x.':F'.$x)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'dddddd'))));
				$bool=FALSE;
			}else{
				$this->excel->getActiveSheet()->getStyle('A'.$x.':F'.$x)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'eeeeee'))));
				$bool=TRUE;
			}
			$this->excel->getActiveSheet()->setCellValue('A'.$x, $k->ambassador);
			$this->excel->getActiveSheet()->setCellValue('B'.$x, $k->campaign);
			$this->excel->getActiveSheet()->setCellValue('C'.$x, $k->kudos_type);
			$this->excel->getActiveSheet()->setCellValue('D'.$x, $k->kudos_card);
			$this->excel->getActiveSheet()->setCellValue('E'.$x, $k->reward_status);
			$this->excel->getActiveSheet()->setCellValue('F'.$x, $k->date_given);
			if($k->kudos_card=='Done'){
				$this->excel->getActiveSheet()->getStyle('D'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => '00CC00'))));
			}else{
				$this->excel->getActiveSheet()->getStyle('D'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => 'FF0000'))));
			}
			if($k->reward_status=='Released'){
				$this->excel->getActiveSheet()->getStyle('E'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => '00CC00'))));
			}else{
				$this->excel->getActiveSheet()->getStyle('E'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => 'FF0000'))));
			}

			$x++;
		}
		$x--;
		$this->excel->getActiveSheet()->getStyle('A1:F'.$x)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
		$filename='KudosList.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');

	}


	//----------------------------------NICCACODES-----------------------------------

	public function kudos_card(){
		$data['kudos'] = $this->Kudos2Model->getKudosCard();
		$this->load->view('kudos_card',$data);
	}
	public function kudos_request(){
		if($this->session->userdata('uid')){
			$data['kudos'] = $this->Kudos2Model->getKudosRequest();
			$this->load->view('kudos_request',$data);
		}else{
			redirect('index.php/login');
		}
	}
	public function getKudos(){
		$kudos_id = $this->input->post('kudosid');
		$value = $this->Kudos2Model->getKudos($kudos_id);
		echo json_encode($value);
	}
	public function submitrequest(){
		$note = $this->input->post('note');
		$kudos_id = $this->input->post('kudosid');
		$uid = $this->session->userdata('uid');
		$employee = $this->Kudos2Model->getEmployeeDetails($uid);
		$row = $this->Kudos2Model->submitrequest($kudos_id,$note,$employee->fname.' '.$employee->lname);
		// echo $this->db->last_query();
		if($row>0){
			
			//----------------------------INSERT REQUEST EMAILER CODES-----------------------------------
			$qry = $this->db->query("SELECT d.email FROM `tbl_kudos_notify` a,tbl_user b,tbl_employee c, tbl_applicant d where c.apid = d.apid AND a.uid=b.uid and b.emp_id=c.emp_id and a.role='Approve' and a.isActive=1");
			if($qry->num_rows()>0){
				foreach($qry->result() as $row){
					
					
					$this->email->initialize(array(
						'protocol' => 'smtp',
						'smtp_host' => 'smtp.gmail.com',
						'smtp_user' => 'szdummysz@gmail.com',
						'smtp_pass' => 'szdummy123',
						'smtp_port' => 587,
						'smtp_crypto' => 'tls',
						'mailtype	' => 'html',
						'validate' => TRUE,
						'crlf' => "\r\n",
						'charset' => "iso-8859-1",
						'newline' => "\r\n"
					));
					$data = array(
						'requested_by' => $employee->fname.' '.$employee->lname,
						'note' => $note,
					);
					$this->email->from('IT_HELPDESK@supportzebra.com','SupportZebra');
					$this->email->to($row->email);
					$this->email->subject('KUDOS | CASH Request on '.date("m.d.y").'');
					$body = $this->load->view('kudos_email_HRT.php',$data,TRUE);
					$this->email->message($body);
					$this->email->send();
				//echo $this->email->print_debugger();	
				}
			}	
			
			//----------------------------INSERT REQUEST EMAILER CODES-----------------------------------
			echo "Success";
		}else{
			echo "Failed";
		}
	}
	public function uploadkudoscard(){
		$kudosid= $this->input->post('kudosid');
		$path = $_FILES['imagefile']["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		$file_sc = 'uploads/kudos_cards/'.date("Ymdhms.").$extension;
		$new_name = date("Ymdhms").".".$extension;
		$target_dir = "./uploads/kudos_cards/";
		$target_file = $target_dir.$new_name;

		$type = $_FILES['imagefile']["type"];
		$allowed = array('image/jpeg', 'image/png', 'image/gif');

		if($path){
			if(!in_array($type,$allowed)){
				echo 'Failed';
			} else {
				if(move_uploaded_file($_FILES['imagefile']["tmp_name"],$target_file)){
					$row = $this->Kudos2Model->uploadkudoscard($file_sc,$kudosid);
					if($row>0){
						echo "Success";
					}else{
						echo 'Failed';
					}
				}else{
					echo 'Failed';
				}
			}
		}
	}
	public function uploadscreenshot(){
		$kudosid= $this->input->post('kudosid');
		$path = $_FILES['imagefile']["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		$file_sc = 'uploads/screenshots/'.date("Ymdhms.").$extension;
		$new_name = date("Ymdhms").".".$extension;
		$target_dir = "./uploads/screenshots/";
		$target_file = $target_dir.$new_name;

		$type = $_FILES['imagefile']["type"];
		$allowed = array('image/jpeg', 'image/png', 'image/gif');

		if($path){
			if(!in_array($type,$allowed)){
				echo 'Failed';
			} else {
				if(move_uploaded_file($_FILES['imagefile']["tmp_name"],$target_file)){
					$row = $this->Kudos2Model->uploadscreenshot($file_sc,$kudosid);
					if($row>0){
						echo "Success";
					}else{
						echo 'Failed';
					}
				}else{
					echo 'Failed';
				}
			}
		}
	}
	public function kudos_add(){

		if($this->session->userdata('uid')){

			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Kudos/kudos_add' and is_assign=1");   

			if ($qry->num_rows() > 0){

				$data['kudos'] = $this->Kudos2Model->getKudosAdd();		
				$data['accounts'] = $this->Kudos2Model->getAllAccounts();
				$this->load->view('kudos_add',$data);
			}else{
				redirect('index.php/home/error');
			}

		}
	}
	public function getEmpInAccounts(){
		$acc_id = $this->input->post('acc_id');
		$value = $this->Kudos2Model->getEmpInAccounts($acc_id);
		echo json_encode($value);
	}
	public function kudos_addmultiple(){
		$this->load->view('kudos_addmultiple');
	}

	public function createkudos(){
		$accounts = $this->input->post('accounts');
		$accounts_text = $this->input->post('accounts_text');
		$employee = $this->input->post('employee');
		$employee_text = $this->input->post('employee_text');
		$reward = $this->input->post('reward');
		$kudostype = $this->input->post('kudostype');
		$clientname = $this->input->post('clientname');
		$clientemail = $this->input->post('clientemail');
		$clientnumber = $this->input->post('clientnumber');
		$comment = $this->input->post('comment');
		$uid = $this->session->userdata('uid');
		$date = date("Y-m-d H:i:s");
		$id = $this->Kudos2Model->createkudos($employee,$accounts,$kudostype,$reward,$clientname,$clientnumber,$clientemail,$comment,$uid,$date,'Manual');
		if($id==NULL){
			echo 'Failed';
		}else{
			 //----------------------------INSERT REQUEST EMAILER CODES  (OPT t0 Requestor) -----------------------------------

			$qry = $this->db->query("SELECT d.email,d.fname,lname FROM `tbl_kudos_notify` a,tbl_user b,tbl_employee c, tbl_applicant d where c.apid = d.apid AND a.uid=b.uid and b.emp_id=c.emp_id and a.role='Request' and a.isActive=1");
			if($qry->num_rows()>0){
				foreach($qry->result() as $row){
					
					
					$this->email->initialize(array(
						'protocol' => 'smtp',
						'smtp_host' => 'smtp.gmail.com',
						'smtp_user' => 'szdummysz@gmail.com',
						'smtp_pass' => 'szdummy123',
						'smtp_port' => 587,
						'smtp_crypto' => 'tls',
						'mailtype	' => 'html',
						'validate' => TRUE,
						'crlf' => "\r\n",
						'charset' => "iso-8859-1",
						'newline' => "\r\n"
					));
					$data = array(
						'emp_id' => $employee,
						'employee_text' => $employee_text,
						'acc_id' => $accounts,
						'accounts_text' => $accounts_text,
						'kudostype' => $kudostype,
						'reward' => $reward,
						'clientname' => $clientname,
						'clientnumber' => $clientnumber,
						'comment' => $comment,
						'uid' => $uid,
						'date' => $date,
					);
					$this->email->from('IT_HELPDESK@supportzebra.com','SupportZebra');
					$this->email->to($row->email);
					$this->email->subject('KUDOS | Kudos Entry for '.date("m.d.y").'');
					$body = $this->load->view('kudos_email_OPT.php',$data,TRUE);
					$this->email->message($body);
					$this->email->send();
				// echo $this->email->print_debugger();	
				}
			}
			//----------------------------INSERT REQUEST EMAILER CODES (OPT t0 Requestor) -----------------------------------

			echo $id;
		}
	}
	public function viewEmail(){
		$qry = $this->db->query("SELECT d.email FROM `tbl_kudos_notify` a,tbl_user b,tbl_employee c, tbl_applicant d where c.apid = d.apid AND a.uid=b.uid and b.emp_id=c.emp_id and a.role='Request'");
		foreach($qry->result() as $row){
			echo $row->email;

		}
	}
	public function updatekudos(){
		$kudosid = $this->input->post('kudosid');
		$accounts = $this->input->post('accounts');
		$employee = $this->input->post('employee');
		$reward = $this->input->post('reward');
		$kudostype = $this->input->post('kudostype');
		$clientname = $this->input->post('clientname');
		$clientemail = $this->input->post('clientemail');
		$clientnumber = $this->input->post('clientnumber');
		$comment = $this->input->post('comment');
		$uid = $this->session->userdata('uid');
		$date = date("Y-m-d H:i:s");
		$row = $this->Kudos2Model->updatekudos($kudosid,$employee,$accounts,$kudostype,$reward,$clientname,$clientnumber,$clientemail,$comment,$uid,$date);
		if($row>0){
			echo 'Success';
		}else{
			echo "Failed";
		}
	}
	

	public function kudos_proofread(){
		$data['kudos'] = $this->Kudos2Model->getKudosProofread();
		$this->load->view('kudos_proofreading',$data);
	}
	public function submitrevision(){
		$revision = $this->input->post('revision');
		$kudos_id = $this->input->post('kudosid');
		$uid = $this->session->userdata('uid');
		$employee = $this->Kudos2Model->getEmployeeDetails($uid);
		$row = $this->Kudos2Model->submitrevision($kudos_id,$revision,$employee->fname.' '.$employee->lname);
		if($row>0){

			echo "Success";
		}else{
			echo "Failed";
		}
	}

	public function kudos_release(){
		if($this->session->userdata('uid')){
			$data['kudos'] = $this->Kudos2Model->getKudosRelease();
			$this->load->view('kudos_release',$data);
		}
	}
	public function approvekudos(){
		$kudos_id = $this->input->post('kudosid');
		$uid = $this->session->userdata('uid');
		$employee = $this->Kudos2Model->getEmployeeDetails($uid);
		$row = $this->Kudos2Model->approvekudos($kudos_id,$employee->fname.' '.$employee->lname);
		if($row>0){
				//----------------------------INSERT REQUEST EMAILER CODES (OPT t0 Requestor) -----------------------------------
			$qry = $this->db->query("SELECT d.email FROM `tbl_kudos_notify` a,tbl_user b,tbl_employee c, tbl_applicant d where c.apid = d.apid AND a.uid=b.uid and b.emp_id=c.emp_id and a.role='Release' and a.isActive=1");
			$approved_by = $this->db->query("SELECT approved_by from tbl_kudos where kudos_id=".$kudos_id);

			if($qry->num_rows()>0){
				foreach($qry->result() as $row){
					
					if($row->email!=null){
					$this->email->initialize(array(
						'protocol' => 'smtp',
						'smtp_host' => 'smtp.gmail.com',
						'smtp_user' => 'szdummysz@gmail.com',
						'smtp_pass' => 'szdummy123',
						'smtp_port' => 587,
						'smtp_crypto' => 'tls',
						'mailtype	' => 'html',
						'validate' => TRUE,
						'crlf' => "\r\n",
						'charset' => "iso-8859-1",
						'newline' => "\r\n"
					));
					$data = array('approved_by' => $approved_by->result()[0]->approved_by);
				// print_r($approved_by);		
					$this->email->from('it_helpdesk@supportzebra.com','SupportZebra');
					$this->email->to($row->email);
					$this->email->subject('KUDOS | CASH FOR RELEASE on '.date("m.d.y").'');
					$body = $this->load->view('kudos_email_CASH_APPROVER.php',$data,TRUE);
					$this->email->message($body);
					$this->email->send();
				//echo $this->email->print_debugger();	
				}}
			}

		//----------------------------INSERT REQUEST EMAILER CODES (OPT t0 Requestor) -----------------------------------

			echo "Success";
		}else{
			echo 'Failed';
		}
	}
	public function rejectkudos(){
		$kudos_id = $this->input->post('kudosid');
		$rejectnote = $this->input->post('rejectnote');
		$uid = $this->session->userdata('uid');
		$employee = $this->Kudos2Model->getEmployeeDetails($uid);
		$row = $this->Kudos2Model->rejectkudos($kudos_id,$rejectnote,$employee->fname.' '.$employee->lname);
		if($row>0){
			echo "Success";
		}else{
			echo 'Failed';
		}
	}
	public function kudos_all(){
		$startdate = date('Y-m-01');
		$enddate  = date('Y-m-t');
		$data['kudos'] = $this->Kudos2Model->getKudosCustom($startdate,$enddate);
		$this->load->view('kudos_all',$data);
	}
	public function getKudosAll(){
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
		$val = $this->Kudos2Model->getKudosCustom($startdate,$enddate);
		echo json_encode($val);
	}
	public function kudos_approve(){
		if($this->session->userdata('uid')){
			$data['kudos'] = $this->Kudos2Model->getKudosApprove();
			$this->load->view('kudos_approve',$data);
		}
	}
	public function releaseupdatekudos(){
		$status = $this->input->post('status');
		$kudos_id = $this->input->post('kudosid');
		$row = $this->Kudos2Model->releaseupdatekudos($kudos_id,$status);
 		// echo $this->db->last_query();
		if($row>0){
		/* 
//----------------------------INSERT REQUEST EMAILER CODES (OPT t0 Requestor) -----------------------------------
		$qry = $this->db->query("SELECT email FROM `tbl_kudos_notify` a,tbl_user b,tbl_employee c where a.uid=b.uid and b.emp_id=c.emp_id and a.role='Request' and a.isActive=1");
		$approved_by = $this->db->query("SELECT released_by from tbl_kudos where kudos_id=".$kudos_id);
		
				if($qry->num_rows()>0){
				foreach($qry->result() as $row){
					
					
 				$this->email->initialize(array(
				  'protocol' => 'smtp',
				  'smtp_host' => 'smtp.gmail.com',
				  'smtp_user' => 'szdummysz@gmail.com',
				  'smtp_pass' => 'szdummy123',
				  'smtp_port' => 587,
				  'smtp_crypto' => 'tls',
				  'mailtype	' => 'html',
				  'validate' => TRUE,
				  'crlf' => "\r\n",
				  'charset' => "iso-8859-1",
				  'newline' => "\r\n"
				));
					 $data = array('approved_by' => $approved_by->result()[0]->approved_by);
				// print_r($approved_by);		
 				$this->email->from('IT_HELPDESK@supportzebra.com','SupportZebra');
				$this->email->to($row->email);
 				$this->email->subject('KUDOS | CASH REQUEST RELEASED on '.date("m.d.y").'');
				$body = $this->load->view('kudos_email_CASH_result.php',$data,TRUE);
  				$this->email->message($body);
				$this->email->send();
				//echo $this->email->print_debugger();	
					}
					}

		//----------------------------INSERT REQUEST EMAILER CODES (OPT t0 Requestor) -----------------------------------
 */
					echo "Success";
				}else{
					echo 'Failed';
				}
			}
			public function report1View()
			{
				if($this->session->userdata('uid'))
				{
					$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Kudos/report3View' and is_assign=1");
					if ($qry->num_rows() > 0){
						$data['kudos'] = $this->Kudos2Model->get_kudos();
						$data['acc'] = $this->Kudos2Model->get_accounts();
						$data['name'] = $this->Kudos2Model->get_names();
						$data['name2'] = $this->Kudos2Model->another_getName();
						$data['k_count'] = $this->Kudos2Model->get_kudos_count();
						$data['top'] = $this->Kudos2Model->get_kudos_top();
						$data['all'] = $this->Kudos2Model->get_kudos_all_agents();
						$data['top_campaigns'] = $this->Kudos2Model->get_kudos_top_campaigns();
						$data['drilldown'] = $this->Kudos2Model->get_kudos_drilldown();
						$data['drilldown2'] = $this->Kudos2Model->get_kudos_drilldown2();
						$data['campaign_kcount'] = $this->Kudos2Model->get_kudos_by_campaigns();
						$this->load->view('report1',$data);
					}else{
						redirect('index.php/home/error');

					}
				}
				else {
					redirect('index.php/login');
				}
			}

			public function addMultipleView()
			{
				$role = explode('|',$this->session->userdata('role'));
				if($role[1]=='OPT'){
					$data['kudos'] = $this->Kudos2Model->get_kudos();
					$data['acc'] = $this->Kudos2Model->get_accounts();
					$data['name'] = $this->Kudos2Model->get_names();
					$data['name2'] = $this->Kudos2Model->another_getName();
					$data['k_count'] = $this->Kudos2Model->get_kudos_count();
					$data['top'] = $this->Kudos2Model->get_kudos_top();
					$data['all'] = $this->Kudos2Model->get_kudos_all_agents();
					$data['top_campaigns'] = $this->Kudos2Model->get_kudos_top_campaigns();
					$data['drilldown'] = $this->Kudos2Model->get_kudos_drilldown();
					$data['campaign_kcount'] = $this->Kudos2Model->get_kudos_by_campaigns();
					$this->load->view('addMultiple',$data);
				} else {
					redirect('index.php/login');
				}
			}

			public function report2View()
			{
				if($this->session->userdata('uid'))
				{
					$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Kudos/report2View' and is_assign=1");
					if ($qry->num_rows() > 0){
						$data['kudos'] = $this->Kudos2Model->get_kudos();
						$data['acc'] = $this->Kudos2Model->get_accounts();
						$data['name'] = $this->Kudos2Model->get_names();
						$data['name2'] = $this->Kudos2Model->another_getName();
						$data['k_count'] = $this->Kudos2Model->get_kudos_count();
						$data['top'] = $this->Kudos2Model->get_kudos_top();
						$data['all'] = $this->Kudos2Model->get_kudos_all_agents();
						$data['top_campaigns'] = $this->Kudos2Model->get_kudos_top_campaigns();
						$data['drilldown'] = $this->Kudos2Model->get_kudos_drilldown();
						$data['campaign_kcount'] = $this->Kudos2Model->get_kudos_by_campaigns();
						$this->load->view('report2',$data);
					}else{
						redirect('index.php/home/error');

					}
				}
				else {
					redirect('index.php/login');
				}



			}

			public function report3View()
			{

				if($this->session->userdata('uid'))
				{	
					$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Kudos/report3View' and is_assign=1");
					if ($qry->num_rows() > 0){

						$data['kudos'] = $this->Kudos2Model->get_kudos();
						$data['acc'] = $this->Kudos2Model->get_accounts();
						$data['name'] = $this->Kudos2Model->get_names();
						$data['name2'] = $this->Kudos2Model->another_getName();
						$data['k_count'] = $this->Kudos2Model->get_kudos_count();
						$data['top'] = $this->Kudos2Model->get_kudos_top();
						$data['all'] = $this->Kudos2Model->get_kudos_all_agents();
						$data['top_campaigns'] = $this->Kudos2Model->get_kudos_top_campaigns();
						$data['drilldown'] = $this->Kudos2Model->get_kudos_drilldown();
						$data['drilldown3'] = $this->Kudos2Model->get_kudos_drilldown3();
						$data['campaign_kcount'] = $this->Kudos2Model->get_kudos_by_campaigns();
						$this->load->view('report3',$data);
					}else{
						redirect('index.php/home/error');

					}
				}
				else {
					redirect('index.php/login');
				}

			}
			public function getDate()
			{
				$startDate = $this->input->post('fromDate');
				$endDate = $this->input->post('toDate');
				$data = $this->Kudos2Model->getDateRange($startDate,$endDate);
				header('Content-type: application/json'); 
				echo json_encode($data);
			}
			public function getDate2()
			{
				$startDate = $this->input->post('fromDate');
				$endDate = $this->input->post('toDate');
				$campaign = $this->input->post('campaign');
				$data = $this->Kudos2Model->getDateRange2($startDate,$endDate,$campaign);
				header('Content-type: application/json'); 
				echo json_encode($data);
			}

			public function getDate3()
			{
				$startDate = $this->input->post('fromDate');
				$endDate = $this->input->post('toDate');
				$data = $this->Kudos2Model->getDateRange3($startDate,$endDate);
				header('Content-type: application/json'); 
				echo json_encode($data);
			}

			public function getDate4()
			{
				$startDate = $this->input->post('fromDate');
				$endDate = $this->input->post('toDate');
				$campaign = $this->input->post('campaign');
				$data = $this->Kudos2Model->getDateRange2($startDate,$endDate,$campaign);


				foreach($data as $d)
				{
					echo "<tr>";
					echo "<td>".$d->month."</td>";
					echo "<td>".$d->campaign."</td>";
					echo "<td>".$d->kudos_count."</td>";
					echo "</tr>";
				}
		//header('Content-type: application/json'); 
		//echo json_encode($data);
			}
			public function getDate5()
			{
				$startDate = $this->input->post('fromDate');
				$endDate = $this->input->post('toDate');
				$data = $this->Kudos2Model->getDateRange($startDate,$endDate);

				foreach($data as $d)
				{
					echo "<tr>";
					echo "<td>".$d->month."</td>";
					echo "<td>".$d->kudos_count."</td>";
					echo "</tr>";
				}
				echo "</tbody>";

			}


			public function getDate6()
			{
				$startDate = $this->input->post('fromDate');
				$endDate = $this->input->post('toDate');
				$data = $this->Kudos2Model->getDateRange3($startDate,$endDate);

				foreach($data as $d)
				{
					echo "<tr>";
					echo "<td>".$d->ambassador."</td>";
					echo "<td>".$d->campaign."</td>";
					echo "<td>".$d->kudos_count."</td>";
					echo "</tr>";
				}
				echo "</tbody>";

			}
			public function reports(){
				$this->load->view('kudosreports');

			}
			public function excel_report1()
			{
		//$this->load->library('PHPExcel');
				$this->load->library('PHPExcel', NULL, 'excel');

				$this->excel->setActiveSheetIndex(0);

				$this->excel->getActiveSheet()->setTitle('Users list');

				$startDate = $this->input->post('fromDate');
				$endDate = $this->input->post('toDate');
				$data = $this->Kudos2Model->getDateRange($startDate,$endDate);

				$styleArray = array(
					'font'  => array(
						'bold'  => true,
						'size'  => 15,
					));

				$styleArray2 = array(
					'font'  => array(
						'size'  => 15,
					));

				$styleArray3 = array(
					'font'  => array(
						'size'  => 15,
						'color' => array('rgb' => 'FFFFFF'),
					));

				$styleArray6 = array(
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					)
				);

				$this->excel->getActiveSheet()->getStyle('A1:B2')->applyFromArray($styleArray6);

				$this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($styleArray6);


				$this->excel->getActiveSheet()->mergeCells('A3:B3');
				$this->excel->getActiveSheet()->setCellValue('A3', 'TOTAL KUDOS COUNT ('.$startDate.' - '.$endDate.')');
				$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('A3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
				$this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($styleArray3);
				$this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($styleArray);

				$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
				$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
				$this->excel->getActiveSheet()->mergeCells('A1:B2');

				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Logo');
				$objDrawing->setPath($this->logo);
      	$objDrawing->setOffsetX(40);    // setOffsetX works properly
		$objDrawing->setOffsetY(10);  //setOffsetY has no effect
		$objDrawing->setCoordinates('A1');
		$objDrawing->setHeight(85); // logo height
		$objDrawing->setWorksheet($this->excel->getActiveSheet());

		$this->excel->getActiveSheet()->getStyle('A4:B4')->applyFromArray($styleArray6);
		

		$this->excel->getActiveSheet()->getStyle('A4:D4')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);

		$this->excel->getActiveSheet()->setCellValue('A4', 'Month and Year');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Kudos Count');



		$row = 5;
		foreach($data as $r){
			$this->excel->getActiveSheet()->fromArray(array($r->month, $r->kudos_count), null, 'A'.$row);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray2);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray6);
			$row++;

			
		}

		unset($styleArray6);


		

		ob_end_clean();
		date_default_timezone_set("Asia/Manila");
		$timestamp=date("Y-m-d-His");
		$filename='KudosCount.xls'; 

		header('Content-Type: application/vnd.ms-excel'); 

		header('Content-Disposition: attachment;filename="'.$filename.'"'); 

		header('Cache-Control: max-age=0'); 

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save($this->savefile.''.$filename);
        //$objWriter->save($filename);
		exit();
	}
	public function excel_report2()
	{
		//$this->load->library('PHPExcel');
		$this->load->library('PHPExcel', NULL, 'excel');

		$this->excel->setActiveSheetIndex(0);

		$this->excel->getActiveSheet()->setTitle('Users list');

		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$data = $this->Kudos2Model->getDateRange3($startDate,$endDate);

		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 15,
			));

		$styleArray2 = array(
			'font'  => array(
				'size'  => 15,
			));

		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 15,
				'color' => array('rgb' => 'FFFFFF'),
			));

		$styleArray6 = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($this->logo);
      	$objDrawing->setOffsetX(220);    // setOffsetX works properly
		$objDrawing->setOffsetY(10);  //setOffsetY has no effect
		$objDrawing->setCoordinates('A1');
		$objDrawing->setHeight(85); // logo height
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
		$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
		$this->excel->getActiveSheet()->mergeCells('A1:C2');

		$this->excel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray6);
		$this->excel->getActiveSheet()->getStyle('A2:C2')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->mergeCells('A3:C3');
		$this->excel->getActiveSheet()->setCellValue('A3', 'TOTAL KUDOS COUNT BY AMBASSADOR ('.$startDate.' - '.$endDate.')');
		$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
		$this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray3);

		$this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

		$this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->setCellValue('A4', 'Ambassador');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Campaign');
		$this->excel->getActiveSheet()->setCellValue('C4', 'Kudos Count');



		$row = 5;
		foreach($data as $r){
			$this->excel->getActiveSheet()->fromArray(array($r->ambassador, $r->campaign, $r->kudos_count), null, 'A'.$row);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray2);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray6);
			$row++;
		}

		unset($styleArray6);

		ob_end_clean();
		date_default_timezone_set("Asia/Manila");
		$timestamp=date("Y-m-d-His");
		$filename='KudosCountAmbassadors.xls'; 

		header('Content-Type: application/vnd.ms-excel'); 

		header('Content-Disposition: attachment;filename="'.$filename.'"'); 

		header('Cache-Control: max-age=0'); 

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save($this->savefile.''.$filename);
        //$objWriter->save($filename);
		exit();
	}



	public function excel_report3()
	{
		//$this->load->library('PHPExcel');
		$this->load->library('PHPExcel', NULL, 'excel');

		$this->excel->setActiveSheetIndex(0);

		$this->excel->getActiveSheet()->setTitle('Users list');

		$startDate = $this->input->post('fromDate');
		$endDate = $this->input->post('toDate');
		$campaign = $this->input->post('campaign');
		$data = $this->Kudos2Model->getDateRange2($startDate,$endDate,$campaign);

		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 15,
			));

		$styleArray2 = array(
			'font'  => array(
				'size'  => 15,
			));

		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 15,
				'color' => array('rgb' => 'FFFFFF'),
			));

		$styleArray6 = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($this->logo);
      	$objDrawing->setOffsetX(220);    // setOffsetX works properly
		$objDrawing->setOffsetY(10);  //setOffsetY has no effect
		$objDrawing->setCoordinates('A1');
		$objDrawing->setHeight(85); // logo height
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
		$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
		$this->excel->getActiveSheet()->mergeCells('A1:C2');

		$this->excel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray6);
		$this->excel->getActiveSheet()->getStyle('A2:C2')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->mergeCells('A3:C3');
		$this->excel->getActiveSheet()->setCellValue('A3', 'TOTAL KUDOS COUNT BY CAMPAIGN ('.$startDate.' - '.$endDate.')');
		$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
		$this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray3);

		$this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray6);
		$this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray6);

		$this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);

		$this->excel->getActiveSheet()->setCellValue('A4', 'Month and Year');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Campaign');
		$this->excel->getActiveSheet()->setCellValue('C4', 'Kudos Count');



		$row = 5;
		foreach($data as $r){
			$this->excel->getActiveSheet()->fromArray(array($r->month, $r->campaign, $r->kudos_count), null, 'A'.$row);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray2);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray6);
			$row++;
		}

		unset($styleArray6);

		ob_end_clean();
		date_default_timezone_set("Asia/Manila");
		$timestamp=date("Y-m-d-His");
		$filename='KudosCountCampaign.xls'; 

		header('Content-Type: application/vnd.ms-excel'); 

		header('Content-Disposition: attachment;filename="'.$filename.'"'); 

		header('Cache-Control: max-age=0'); 

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save($this->savefile.''.$filename);
        //$objWriter->save($filename);
		exit();
	}
	public function kudos_notify(){
		$data['department'] = $this->Kudos2Model->getDepartments();
		$this->load->view('kudos_notify',$data);
	}

	public function getrequestors(){
		$val = $this->Kudos2Model->getrequestors();
		echo json_encode($val);
	}
	public function getreleasers(){
		$val = $this->Kudos2Model->getreleasers();
		echo json_encode($val);
	}
	public function getapprovers(){
		$val = $this->Kudos2Model->getapprovers();
		echo json_encode($val);
	}
	public function updateActiveNotify(){
		$ksetting_id = $this->input->post('ksetting_id');
		$active = $this->input->post('activevalue');
		$row = $this->Kudos2Model->updateActiveNotify($ksetting_id,$active);
		if($row>0){
			echo "Success";
		}else{
			echo 'Failed';
		}
	}
	public function getEmpInDept(){
		$dep_id = $this->input->post('dep_id');
		$role = $this->input->post('role');
		$value = $this->Kudos2Model->getEmpInDept($dep_id);
		$notify = $this->Kudos2Model->getNotifyPerRole($role);
		foreach($value as $valkey => &$val){
			foreach($notify as $n){
				if($val->uid==$n->uid){
					unset($value[$valkey]);
				}
			}
		}
		echo json_encode($value);
	}
	public function updatekudosnotify(){
		$uid = $this->input->post('uid');
		$role = $this->input->post('role');
		$notify = $this->Kudos2Model->getKudosNotify($uid,$role);
		if(empty($notify)){
			$row = $this->Kudos2Model->insertKudosNotify($uid,$role);
		}else{
			$row = $this->Kudos2Model->updateKudosNotify($notify->ksetting_id);
		}
		if($row>0){
			echo "Success";
		}else{
			echo 'Failed';
		}
	}
}