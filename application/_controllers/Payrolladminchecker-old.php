<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payrolladminchecker extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	
	public function index($stat=null,$pid=null){
				// $query = $this->db->query("select b.apid,fname,lname,emp_id from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes'");
				// $query = $this->db->query("select distinct(b.emp_id),b.apid,fname,lname,pos_name from tbl_applicant a,tbl_employee b,tbl_position d  where a.apid=b.apid  and a.pos_id = d.pos_id and b.isActive='yes' and d.class='Agent' and b.isActive='yes' order by lname");
				// $query = $this->db->query("select b.emp_id,b.apid,fname,lname,pos_name from tbl_applicant a,tbl_employee b,tbl_position d  where a.apid=b.apid  and a.pos_id = d.pos_id and b.isActive='yes' and d.class='Agent' and b.isActive='yes' order by lname");
				  // $query = $this->db->query("select b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and d.isActive=1 order by lname");
				  // $query = $this->db->query(" select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and (g.status='Probationary' or g.status='Regular') and e.class='Agent' and d.isActive=1 order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId");
				  $query = $this->db->query(" select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and (g.status='Probationary' or g.status='Regular') and e.class='Admin' and d.isActive=1 and b.salarymode='Monthly' order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$pid);
 
		foreach ($query->result() as $row)
			{
				$arr['employee'][$row->apid] = array(
				"apid" => $row->apid,
 				"fname" => $row->fname,
 				"lname" => $row->lname,
 				"emp_id" => $row->emp_id,
 				"payroll_id" => $row->payroll_id,
 				"emp_promoteId" => $row->emp_promoteId,
  				);
			}
	if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='attendance2' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
					if($pid>0){
						$per_id =  $this->db->query("select coverage_id,daterange,month from tbl_payroll_coverage where coverage_id=".$pid."");
					foreach ($per_id->result() as $row)
					{
							$arr['period'] = array(
							"daterange" => $row->daterange,
							"coverage_id" => $row->coverage_id,
							"month" => $row->month,
							
							);
					}
													$arr["stat"] = array("status" => $stat);

					$this->load->view('payrolladmincheck',$arr);
				}else{
					
				}
			}else{
				redirect('index.php/home/error');
			}
		}else{
					redirect('index.php/login');
		}
	
	
		}
		
		public function emptype(){
		 $payrollC = $this->input->post('payrollC');
		 $employz = $this->input->post('employz');
		  $date = explode("-",$payrollC);
		  	  $HolidateSEnd = array(
			 '2:00:00 PM' => '0',
			 '3:00:00 PM' => '0',
			 '4:00:00 PM' => '0',
			 '5:00:00 PM' => '0',
			 '6:00:00 PM' => '0',
			 '7:00:00 PM' => '0',
			 '8:00:00 PM' => '0',
			 '9:00:00 PM' => '0',
			 '9:30:00 PM' => '0',
			 '10:00:00 PM' => '0',
			 '10:15:00 PM' => '0',
			 '10:30:00 PM' => '0',
			 '11:00:00 PM' => '0',
			 '12:00:00 AM' => '0',
			 '1:00:00 AM' => '1',
			 '2:00:00 AM' => '2',
			 '3:00:00 AM' => '2.75',
			 '4:00:00 AM' => '3.25',
			 '5:00:00 AM' => '4.25',
			 '6:00:00 AM' => '5.25',
			 '6:30:00 AM' => '5.75',
			 '7:00:00 AM' => '6',
			 '8:00:00 AM' => '7',
			 '9:00:00 AM' => '0',
			 '9:30:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
		  );
		  $HolidateStart = array(
			 '2:00:00 PM' => '8',
			 '3:00:00 PM' => '8',
			 '4:00:00 PM' => '7',
			 '5:00:00 PM' => '6',
			 '6:00:00 PM' => '5.25',
			 '7:00:00 PM' => '4.75',
			 '8:00:00 PM' => '3.75',
			 '9:00:00 PM' => '2.75',
			 '9:30:00 PM' => '2.25',
			 '10:00:00 PM' => '2',
			 '10:15:00 PM' => '1.75',
			 '10:30:00 PM' => '1.5',
			 '11:00:00 PM' => '1',
			 '12:00:00 AM' => '8',
			 '1:00:00 AM' => '8',
			 '2:00:00 AM' => '8',
			 '3:00:00 AM' => '8',
			 '4:00:00 AM' => '8',
			 '5:00:00 AM' => '8',
			 '6:00:00 AM' => '8',
			 '7:00:00 AM' => '8',
			 '8:00:00 AM' => '8',
			 '9:00:00 AM' => '8',
			 '9:30:00 AM' => '8',
			 '10:00:00 AM' => '8',
			 '11:00:00 AM' => '8',
			 '12:00:00 PM' => '8',
			 '1:00:00 PM' => '8',
		  );
		    $NDHolidateStart= array(
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
			 '2:00:00 PM' => '1',
			 '3:00:00 PM' => '2',
			 '4:00:00 PM' => '1.25',
			 '5:00:00 PM' => '1.25',
			 '5:30:00 PM' => '2',
			 '6:00:00 PM' => '1.5',
			 '7:00:00 PM' => '1.5',
			 '8:00:00 PM' => '1.75',
			 '9:00:00 PM' => '2',
			 '9:30:00 PM' => '2',
			 '10:00:00 PM' => '2',
			 '10:15:00 PM' => '1.75',
			 '10:30:00 PM' => '1.5',
			 '11:00:00 PM' => '1',
			 '11:30:00 PM' => '0.5',
			 '12:00:00 AM' => '5.25',
			 '12:30:00 AM' => '0',
			 '1:00:00 AM' => '4.25',
			 '2:00:00 AM' => '3.75',
			 '3:00:00 AM' => '2.75',
			 '4:00:00 AM' => '2',
			 '5:00:00 AM' => '1',
			 '6:00:00 AM' => '0',
			 '6:30:00 AM' => '0',
			 '7:00:00 AM' => '0',
			 '8:00:00 AM' => '0',
			 '9:00:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
			
		  );
		  	$NDHolidateEnd= array(
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
			 '2:00:00 PM' => '0',
			 '3:00:00 PM' => '0',
			 '4:00:00 PM' => '0',
			 '5:00:00 PM' => '0',
			 '6:00:00 PM' => '0',
			 '7:00:00 PM' => '0',
			 '8:00:00 PM' => '0',
			 '9:00:00 PM' => '0',
			 '9:30:00 PM' => '0',
			 '10:00:00 PM' => '0',
			 '10:15:00 PM' => '0',
			 '10:30:00 PM' => '0',
			 '11:00:00 PM' => '0',
			 '12:00:00 AM' => '0',
			 '1:00:00 AM' => '1',
			 '2:00:00 AM' => '2',
			 '2:30:00 AM' => '2.25',
			 '3:00:00 AM' => '2.75',
			 '4:00:00 AM' => '3.75',
			 '5:00:00 AM' => '4.25',
			 '6:00:00 AM' => '5',
			 '6:30:00 AM' => '5.25',
			 '7:00:00 AM' => '5',
			 '7:15:00 AM' => '5.25',
			 '7:30:00 AM' => '5.25',
			 '8:00:00 AM' => '5.25',
			 '8:30:00 AM' => '5.25',
			 '9:00:00 AM' => '5.25',
			 '9:30:00 AM' => '4.75',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
		  );
		$query = $this->db->query("select b.apid,fname,lname,emp_id,(select concat(acc_id,'|',acc_name) from tbl_account where acc_id = b.acc_id) as account from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes' and b.emp_id in (".$employz.") order by lname ASC");
		  foreach ($query->result() as $row){
			$DateRangeShift = $this->DateRangeShift($date);
			$empRate2 = $this->empRate($row->emp_id);
			$empDeductAdjustment2 = $this->empDeductAdjustment($row->emp_id);


				foreach($DateRangeShift as $row1 => $data){
					$shift = $this->empshift($row->emp_id,$data);
						 
						$SSSCompute2 = $this->SSSCompute($row->emp_id);
						$PhilHealthCompute2 = $this->PhilHealthCompute($row->emp_id);
						$adjustSalary2 = $this->adjustSalary($row->emp_id,$payrollC);
						$deductSalary2 = $this->deductSalary($row->emp_id,$payrollC);
					foreach($shift as $row2 => $data2){
						$in = $this->empshiftin($row->emp_id,$data2->sched_id);
						$out = $this->empshiftout($row->emp_id,$data2->sched_id);
						$emplogsShift2 = $this->emplogsShift($data2->sched_id,$row->emp_id);
						$empRate2 = $this->empRate($row->emp_id);
							$empPosAllowance2 = $this->empPosAllowance($empRate2[0]->posempstat_id);
							$empPosAllowance1 = (count($empPosAllowance2)>0) ? $empPosAllowance2[0]->allowance : 0;
							$empPosBonus2 = $this->empPosBonus($empRate2[0]->posempstat_id);
							$empPosBonus1 = (count($empPosBonus2)>0) ? $empPosBonus2[0]->bonus : 0;

						$timeschedule = $this->timeschedule($data2->sched_id);
						$NDbegin = "10:10:00 PM";
							$NDend   = "6:00:00 AM";
							
							$HPbegin = "12:00:00 AM";
							$HPend   = "3:00:00 AM";
							
							$HPbegin2 = "2:00:00 PM";
							$HPend2   = "11:59:59 PM";
							
							$HPbegin3 = "4:00:00 AM";
							$HPend3   = "5:00:00 AM";
							
							
						if($data2->type=="Normal" || $data2->type=="WORD"){
						 
							if(count($in)>0){
								   $shiftFrom = date("H:i:s", strtotime($timeschedule[0]->time_start));
									 
									$NDbegin = "10:10:00 pm";
									$NDend   = "6:00:00 am";
									
									$HPbegin = "12:00:00 am";
									$HPend   = "3:00:00 am";
									
									$HPbegin2 = "2:00:00 pm";
									$HPend2   = "11:59:59 pm";
									
									$HPbegin3 = "4:00:00 am";
									$HPend3   = "5:00:00 am";
									 
									$NDdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
									$NDdate2 = DateTime::createFromFormat('H:i a', $NDbegin);
									$NDdate3 = DateTime::createFromFormat('H:i a', $NDend);

									$HPdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
									$HPdate2 = DateTime::createFromFormat('H:i a', $HPbegin);
									$HPdate3 = DateTime::createFromFormat('H:i a', $HPend);
									
									$HPdate4 = DateTime::createFromFormat('H:i a', $HPbegin2);
									$HPdate5 = DateTime::createFromFormat('H:i a', $HPend2);
									
									$HPdate6 = DateTime::createFromFormat('H:i a', $HPbegin3);
									$HPdate7 = DateTime::createFromFormat('H:i a', $HPend3);
									if ($NDdate1 >= $NDdate2 or $NDdate1 <= $NDdate3){
									   $ndRemark = "Y";
									}else{
									   $ndRemark= "N";
									}
									/* if ($HPdate1 >= $HPdate2 and $HPdate1 <= $HPdate3){
									   $hpRemark = "Y";
									   $hpRate = '0.10';
									}else{
									   $hpRemark= "N";
									   $hpRate = '';
									} */
								if (($HPdate1 >= $HPdate4 and $HPdate1 <= $HPdate5) or ($HPdate1 >= $HPdate6 and $HPdate1 <= $HPdate7) ){
												$hpRemark = "Y";
												$hpRate = '0.08';
												
								}else if(($HPdate1 >= $HPdate2) and ($HPdate1 <= $HPdate3)){
											   $hpRemark = "Y";
												$hpRate = '0.10';
								}else{
												$hpRemark= "N";
												 $hpRate = '0.0';
								}
									 
									 
								// $to_time = strtotime($in[0]->login);
								$to_time= strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login)));

								$BCTSched = date('H:i:s', strtotime('-10 mins', strtotime($timeschedule[0]->time_start)));
								
							 if($timeschedule[0]->time_start == "12:00:00 AM"){
								if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) >= strtotime(date('Y-m-d H:i:00', strtotime($data." 24:00:00")) )){
										 $data= date('Y-m-d', strtotime('-1 day', strtotime($data)));
										 $from_time = strtotime($data." 24:00:00");
										$res = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
										$late= (round(abs($from_time - $to_time) / 60,2));
										$latemins = (round(abs($from_time - $to_time) / 60,2));
								 }else{
										 
										$from_time = strtotime($data." 24:00:00");
										$res = round(abs($from_time - $to_time) / 60,2)." mins. early";
										$late=0;
										$latemins = 0;

								 }												 
							 }else{
								if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) > strtotime(date('Y-m-d H:i:00', strtotime($data." ".$timeschedule[0]->time_start))) ){
									$from_time = strtotime($data." ".$shiftFrom);
										$res = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
										$late= (round(abs($from_time - $to_time) / 60,2));
										$latemins=(round(abs($from_time - $to_time) / 60,2));

								  }else{
										$from_time = strtotime($data." ".$shiftFrom);
										$res = round(abs($from_time - $to_time) / 60,2)." mins. early";
										$late=0;
										$latemins = 0;
								  }
										 
							 }
								$bct = ($data." ".$BCTSched.":00" > $in[0]->login ) ? "Q" : "<span style='color:red'>DQ</span>";

							}else{
							$timediff = "<span style='color:red'><b>Absent</b></span>";
							$bct = "<span style='color:red'>DQ</span>";
														 $hpRemark= "N";
							    $hpRate = '0.0';	
							  	$empDeductAdjustmentFinal = 0;
								$empDeductAdjustmentFinal_ID = 0;
								$totalSSSPiPh =0;
								$ndRemark = "N";
								$late=0;
								$latemins=0;
							}
							
							$totalSSSPiPh=	$PhilHealthCompute2[0]->employeeshare + $SSSCompute2[0]->sss_employee;
							
								
								
						}else{
							 $timediff = "<span style='color:red'>".$data2->type."</span>";
							 $hpRemark= "N";
							    $hpRate = '0.0';	
							  	$empDeductAdjustmentFinal = 0;
							 $empDeductAdjustmentFinal_ID = 0;
							 $bct  = "DQ";
							 $totalSSSPiPh =0;
							$ndRemark = "N";
							$late=0;
							$latemins=0;
						}
								$BIRCompute2 = $this->BIRCompute($row->emp_id,$empRate2[0]->rate,$totalSSSPiPh);
								$BIRCompute3 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->deduction : '0';
								$BIRCompute4 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->percent : '0';
								$BIRCompute5 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->salarybase : '0';
								$BIRCompute6 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->description : 'not set';
								$adjustSalary1 = (count($adjustSalary2)>0) ? $adjustSalary2[0]->adjust : 0;
								$deductSalary1 = (count($deductSalary2)>0) ? $deductSalary2[0]->deduct : 0;
 								$empDeductAdjustmentFinal = (count($empDeductAdjustment2)>0) ? $empDeductAdjustment2[0]->adj_amount : 0;
								$empDeductAdjustmentFinal_ID = (count($empDeductAdjustment2)>0) ? $empDeductAdjustment2[0]->pemp_adjust_id : 0;
						$logoutt =  (count($out)>0) ? (($data2->type=="Normal" || $data2->type=="WORD") ? $out[0]->logout : $data." 00:00:00") : $data." 00:00:00";
						$loginn =  (count($in)>0) ? (($data2->type=="Normal" || $data2->type=="WORD") ? $in[0]->login : $data." 00:00:00") : $data." 00:00:00";
						$logoutt2 =  ($data2->type=="Normal" || $data2->type=="WORD") ? $timeschedule[0]->time_end : "00:00:00";
						$loginn2 =  ($data2->type=="Normal" || $data2->type=="WORD") ? $timeschedule[0]->time_start : "00:00:00";
									
							$isLogoutHoliday2 = $this->isLogoutHoliday($logoutt,$logoutt2);
							$isLoginHoliday2 = $this->isLoginHoliday($loginn,$loginn2);
							 $TotalAmountHoliday1=0;
								$TotalAmountHoliday2=0;	
								$hrly=0;	
								$HolidayND=0;	
								$HolidayHP=0;	
								$NDComputeHolidayLogout=0;	
								$HPComputeHolidayLogout=0;	
								$NDComputeHolidayLogin=0;	
								$HPComputeHolidayLogin=0;	
								$HolidayType1 =0;
								$HolidayType2=0;
								
   if(count($isLogoutHoliday2)>0){
   if($data2->type=="Normal" || $data2->type=="WORD"){
   if(count($in)>0){
		// $HolidayType = ($isLogoutHoliday2[0]->type=='Regular') ? 1 : .30;
		$HolidayType =$isLogoutHoliday2[0]->calc;
 			if(array_key_exists($logoutt2,$HolidateSEnd)){
				$isLogoutHoliday1  =  $HolidateSEnd[$logoutt2];
				$isLogoutHolidayND1  =  $NDHolidateEnd[$logoutt2];
				$TotalAmountHoliday1 = (((($empRate2[0]->rate*12)/261)/8)*$HolidayType)*$isLogoutHoliday1; 
					$hrly =$HolidayType;
				$NDComputeHolidayLogout =  ($isLogoutHolidayND1 *  $HolidayType);
				$HPComputeHolidayLogout =  ($isLogoutHolidayND1 * $HolidayType);
				$HolidayType2 =  $isLogoutHoliday2[0]->type;
			}else{
				$isLogoutHoliday1  =  0 ;
			}
	}else{
			$isLogoutHoliday1  =  0;
			}
			}else{
			$isLogoutHoliday1  =  0;
			}
	}else{
			$isLogoutHoliday1  =  0;
	}
if(count($isLoginHoliday2 )>0){
	if($data2->type=="Normal" || $data2->type=="WORD"){
	if(count($in)>0){
		// $HolidayType = ($isLoginHoliday2[0]->type=='Regular') ? 1 : .30;
		$HolidayType = $isLoginHoliday2[0]->calc;
		if(array_key_exists($loginn2,$HolidateStart)){
			$isLoginHoliday1  =  $HolidateStart[$loginn2];
			$isLoginHolidayND1  =  $NDHolidateStart[$loginn2];
			$TotalAmountHoliday2 = (((($empRate2[0]->rate*12)/261)/8)*$HolidayType)*$isLoginHoliday1;  
			 $NDComputeHolidayLogin = ($isLoginHolidayND1 * $HolidayType);
			$HPComputeHolidayLogin =  ($isLoginHolidayND1 * $HolidayType);
			$HolidayType1 =  $isLoginHoliday2[0]->type;

		}else{
			$isLoginHoliday1  =  0 ;
		}
	}else{
		$isLoginHoliday1  =  0 ;
	}}else{
		$isLoginHoliday1  =  0 ;
	}
	 
}else{
		$isLoginHoliday1  =  0;

}
				$from_timeLog = strtotime($loginn);
				$to_timeLog = strtotime($logoutt);
				$resLog = round(abs(($to_timeLog-$from_timeLog)/3600),2);
 						$arr["employee"][$row->emp_id][$data]= array(
							"fname" => $row->lname.", ".$row->fname,
							"date" => $data,
							"shift_type" => $data2->type,
							"login" =>(count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD") ? date('Y-m-d h:i:s a', strtotime($in[0]->login)) : $data2->type : "No Log IN",
							"logout" => (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD") ? date('Y-m-d h:i:s a', strtotime($out[0]->logout)) : $data2->type : "No Log OUT",
							"shiftStart" => ($data2->type=="Normal" || $data2->type=="WORD") ? $timeschedule[0]->time_start : $data2->type,
							"shiftEnd" => ($data2->type=="Normal" || $data2->type=="WORD") ? $timeschedule[0]->time_end : $data2->type,
							// "late" => $timediff,
							"latecalc" => (isset($late)) ? (($late>120) ? 0 : $late/60 ) : 0,
 							"latemins" => (isset($latemins)) ? $latemins/60 : 0,
							"bct" => $bct,
							"ndRemark" =>  (isset($ndRemark)) ? $ndRemark: "N"  ,
							"hpRemark" =>   (isset($hpRemark)) ? $hpRemark: "N" ,
							"hpRate" =>   (isset($hpRate)) ? $hpRate : 0,
							"nd" =>  (count($emplogsShift2)>0) ? (($data2->type=="Normal" || $data2->type=="WORD") ? $emplogsShift2[0]->nd : "0.00") : "0-0",
 							"empPosAllowance" =>   $empPosAllowance1,
							"empPosBonus" =>   $empPosBonus1,
							"isHoliday" =>    $isLogoutHoliday1+ $isLoginHoliday1,
							"isHoliday1" =>    $isLoginHoliday1,
							"isHoliday2" =>    $isLogoutHoliday1,
							"loginn" =>    $loginn,
							"logoutt" =>    $logoutt,
							"calcHoliday1" =>    (isset($isLoginHoliday2[0]->calc)) ? $isLoginHoliday2[0]->calc : 0,
							"calcHoliday2" =>   (isset($isLogoutHoliday2[0]->calc)) ? $isLogoutHoliday2[0]->calc : 0,
							"TotalAmountHoliday" =>   number_format($TotalAmountHoliday1+$TotalAmountHoliday2,2, '.', ''),
							"TotalAmountHoliday1" =>   number_format($TotalAmountHoliday1,2, '.', ''),
							"TotalAmountHoliday2" =>   number_format($TotalAmountHoliday2,2, '.', ''),
							"HPComputeHolidayLogin" =>   $HPComputeHolidayLogin,
							"NDComputeHolidayLogin" =>   $NDComputeHolidayLogin,
							"NDComputeHolidayLogout" =>   $NDComputeHolidayLogout,
							"HPComputeHolidayLogout" =>   $HPComputeHolidayLogout,
							"HolidayType1" =>   $HolidayType1,
							"HolidayType2" =>   $HolidayType2,
							"total" =>   ($resLog>0) ? $resLog : 1,
							"hrly" =>   $hrly,
							"rate" =>   $empRate2[0]->rate,
							"emp_promoteId" =>  $empRate2[0]->emp_promoteId,
							"rate2" =>   (($empRate2[0]->rate/2)-($totalSSSPiPh/2)-(($empRate2[0]->rate/2)*.02)),
							"pos_name" =>   $empRate2[0]->pos_name,
							"payroll_adjDeduct_amount" =>  $empDeductAdjustmentFinal ,
							"pemp_adjust_id" =>  $empDeductAdjustmentFinal_ID ,
							"sss_id" =>   $SSSCompute2[0]->sss_id,
							"SSS" =>   $SSSCompute2[0]->sss_employee,
							"philhealth_id" =>   $PhilHealthCompute2[0]->philhealth_id,
							"PhilHealthCompute" =>   $PhilHealthCompute2[0]->employeeshare,
							"tax_id" =>   (count($BIRCompute2) > 1) ? $BIRCompute2[0]->tax_id : 2,
							"BIRCompute" =>  $BIRCompute3,
							"BIRPercent" =>  $BIRCompute4,
							"BIRSalaryBase" =>  $BIRCompute5,
							"BIRDescription" =>  $BIRCompute6,
							"adjustSalary" =>  $adjustSalary1,
							"deductSalary" =>  $deductSalary1,
							"account" =>   $row->account,

						);
					}
				}
				$late=0;
			}
			echo json_encode($arr);
 		}
		 public function empshift($empid,$date){
		$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and sched_date = '".$date."' and emp_id=".$empid);
			  return $query->result();
		 }
		  
		  public function empshiftin($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and sched_id=".$shift." and emp_id=".$empid);
			  return $query->result();
 		}
		  public function empshiftout($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as logout,acc_time_id from tbl_dtr_logs where entry='O' and type='DTR' and sched_id=".$shift." and emp_id=".$empid);
			  return $query->result();
 		}
		public function timeschedule($timeschedule){
			$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_acc_time b,tbl_time c  where a.acc_time_id = b.acc_time_id and b.time_id = c.time_id and sched_id=".$timeschedule);
			return $query->result();
		 }
		 public function DateRangeShift($date){
		$strDateFrom = date("Y-m-d", strtotime($date[0]));
		$strDateTo = date("Y-m-d", strtotime($date[1]));
		$aryRange=array();
		$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
			if($iDateTo>=$iDateFrom){
				array_push($aryRange,date('Y-m-d',$iDateFrom));
				 while($iDateFrom<$iDateTo){
					$iDateFrom+=86400; // add 24 hours
						array_push($aryRange,date('Y-m-d',$iDateFrom));
					}
			}
			 return $aryRange;
			 		
		}
	 
		 public function emplogs($date,$empid){
		$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and sched_date = '".$date."' and emp_id=".$empid);
			  return $query->result();
		 }
		public function empDeductAdjustment($empid){
 
			$query = $this->db->query("select pemp_adjust_id,adj_amount from tbl_payroll_emp_adjustment where emp_id= ".$empid." and isActive=1");
				
			 return $query->result();
 			 
 		}
		public function emplogsOut($dtrid,$empid){
 
			$query = $this->db->query("select dtr_id,emp_id,log as logout from tbl_dtr_logs where entry='O' and type='DTR' and note=".$dtrid." and emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
			public function emplogsShift($sched_id,$empid){
 
			// $query = $this->db->query("select time_start,time_end from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.acc_time_id=".$acc_time_id." and emp_id=".$empid."");
			$query = $this->db->query("select distinct(c.time_id),time_start,time_end,(select group_concat(concat(hour,'-',minutes))  from tbl_payroll_nd as s where s.time_id=c.time_id) as nd from tbl_dtr_logs a,tbl_acc_time b,tbl_time c,tbl_schedule d where b.acc_time_id=d.acc_time_id and b.time_id=c.time_id and a.sched_id=d.sched_id and a.sched_id=".$sched_id."  and a.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function empRate($empid){
 
			$query = $this->db->query("select d.emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,c.posempstat_id from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 and f.isActive=1 and b.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function NightDiff($nd){
 			$query = $this->db->query("select time_start,time_end,hour,minutes from tbl_payroll_nd a,tbl_time b where a.time_id=b.time_id and a.isActive=1 and b.time_start ='".$nd."'");
				
			 return $query->result();
  		}
		public function SSSCompute($empid){
 			// $query = $this->db->query("select sss_employee from (select rate from tbl_rate a,tbl_position b,tbl_employee c,tbl_applicant d where a.pos_id=b.pos_id and c.apid=d.apid and b.pos_id =d.pos_id and a.isActive=1 and c.isActive='yes' and c.emp_id=".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
 			  $query = $this->db->query("select sss_employee,sss_id from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
				
			 return $query->result();
  		}
		public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare,philhealth_id from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		}
		public function empPosAllowance($posempstatid){
 
			$query = $this->db->query("select pos_name,d.status,group_concat(concat(allowance_name,'=',value/2)) as allowance from tbl_allowance a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id  and b.empstat_id=d.empstat_id and b.posempstat_id =".$posempstatid." and a.isActive=1 group by b.posempstat_id");
				
			 return $query->result();
 			 
 		}
		public function empPosBonus($posempstatid){
 
			$query = $this->db->query("select pos_name,d.status,group_concat(concat(bonus_name,'=',amount/2)) as bonus from tbl_pos_bonus a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_bonus e where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and e.bonus_id = a.bonus_id and b.posempstat_id =".$posempstatid." group by b.posempstat_id ");
				
			 return $query->result();
 			 
 		}
		   public function BIRCompute($empid,$rate,$totalSSSPiPh){
		  /*  public function BIRCompute(){
			   $empid=46;
			   $rate=10500;
			   $totalSSSPiPh=253.25; */
  			   $query = $this->db->query("select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".(($rate/2)-$totalSSSPiPh-(($rate/2)*.02))." order by salarybase DESC limit 1 ");
			  return $query->result();
			 // echo json_encode($query->result());
  		} 
		 
/* 		 public function BIRCompute2(){
			   $empid = 33;
			  $rate = 5000;
  			   $query = $this->db->query("select g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase <=".($rate/2)." order by salarybase DESC limit 1");
			 var_dump($query->result_array());
			 
  		}  */
		 
 	public function adjustSalary($empid,$payrollCover){
			$query = $this->db->query("select emp_id,group_concat(concat(additionname,'=',value)) as adjust from tbl_payroll_addition a,tbl_payroll_emp_addition b,tbl_payroll_coverage c where a.paddition_id=b.paddition_id and c.coverage_id=b.coverage_id and daterange ='".$payrollCover."' and emp_id=".$empid);
			 return $query->result();			  
  		}
		public function deductSalary($empid,$payrollCover){
			$query = $this->db->query("select emp_id,group_concat(concat(deductionname,'=',value)) as deduct from tbl_payroll_deductions a,tbl_payroll_coverage b, tbl_payroll_emp_deduction c where a.pdeduct_id=c.pdeduct_id and b.coverage_id=c.coverage_id and daterange ='".$payrollCover."' and c.emp_id= ".$empid);
			 return $query->result();			  
  		}
		 	// public function isLoginHoliday($login,$timeStart){
				public function isLoginHoliday222222(){
				$login = "2017-08-21 16:42:50";
				$timeStart = "05:00:00 PM";
			$year = date_format(date_create($login),"Y");
			  $date = date_format(date_create($login),"Y-m-d");
			  $timeStart = date_format(date_create($timeStart),"h:i:s");
 			
	$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
	echo "select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')";
  			  return $query->result();
  		}
		public function isLoginHoliday($login,$timeStart){
				// public function isLoginHoliday(){
				// $login = "2017-08-28 08:34:23";
				// $timeStart = "9:00:00 AM";
			$year = date_format(date_create($login),"Y");
			$date = date_format(date_create($login),"Y-m-d");
			$timeStart = date_format(date_create($timeStart),"h:i:s");
 			
			$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
			return $query->result();
		}
	
		public function isLogoutHoliday($logout,$timeEnd){
			  $year = date_format(date_create($logout),"Y");
			  $date = date_format(date_create($logout),"Y-m-d");
			  $timeEnd = date_format(date_create($timeEnd),"h:i:s");
 			
	// $query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
		$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");


			return $query->result();
  		}
			public function savePayroll(){
				$coverage_id = $this->input->post("coverage_id");
				$type = $this->input->post("emp");

				   $query = $this->db->query("delete from tbl_payroll where emp_type='".$type."' and coverage_id=".$coverage_id );
				$data = json_decode($this->input->post("dataArray"));
				// print_r($data);
				 foreach($data as $key => $val){
					foreach($val as $k => $v){
						//echo  $val[26]." =";
					}
					// echo  "**".$val[21]." =";
					  print_r($val);
					
 // $query = $this->db->query("insert into tbl_payroll(coverage_id,emp_promoteId,pagibig_id,sss_id,tax_id,philhealth_id,monthly,quinsina,clothing,laundry,rice,ot_bct,nd,hp,ho,sss_details,phic_details,hdmf_details,bir_details,adjust_add,adjust_minus,total_earning,total_deduction,final,total_hours,isActive,pemp_adjust_id,hour_ho,hour_hp,hour_nd) values(".$coverage_id.",'$val[0]',1,'$val[1]','$val[3]' ,'$val[2]' ,'$val[6]' ,'$val[7]' ,'$val[11]' ,'$val[12]' ,'$val[13]' ,'$val[14]' ,'$val[15]' ,'$val[16]' ,'$val[17]' ,'$val[18]' ,'$val[19]' ,'$val[20]' ,'$val[21]' ,'$val[22]' ,'$val[23]' ,'$val[24]' ,'$val[25]' ,'$val[26]' ,'$val[10]',1,'$val[27]','$val[28]','$val[29]','$val[30]')");
  $query = $this->db->query("insert into tbl_payroll(coverage_id,emp_promoteId,pagibig_id,sss_id,tax_id,philhealth_id,monthly,quinsina,clothing,laundry,rice,ot_bct,nd,hp,ho,sss_details,phic_details,hdmf_details,bir_details,adjust_add,adjust_minus,total_earning,total_deduction,final,total_hours,isActive,pemp_adjust_id,hour_ho,hour_hp,hour_nd,bonus,emp_type,account) values(".$coverage_id.",'$val[0]',1,'$val[1]','$val[3]' ,'$val[2]' ,'$val[6]' ,'$val[7]' ,'$val[11]' ,'$val[12]' ,'$val[13]' ,'$val[15]' ,'$val[16]' ,'$val[17]' ,'$val[18]' ,'$val[19]' ,'$val[20]' ,'$val[21]' ,'$val[22]' ,'$val[23]' ,'$val[24]' ,'$val[25]' ,'$val[26]' ,'$val[27]' ,'$val[10]',1,'$val[28]','$val[29]','$val[30]','$val[31]','$val[14]','$type','$val[32]')");
 
				 }
				
				
		}
}