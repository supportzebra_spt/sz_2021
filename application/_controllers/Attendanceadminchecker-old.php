<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendanceadminchecker extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	
	public function index($stat=null,$pid=null){
	$status = explode("-",$stat);
			$st1 = ($status[0]=="agent") ? "Agent" : "Admin";
			$st2 = ($status[1]=="trainee") ? "Daily" : "Monthly";

				// $query = $this->db->query("select b.apid,fname,lname,emp_id from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes'");
				// $query = $this->db->query("select distinct(b.emp_id),b.apid,fname,lname,pos_name from tbl_applicant a,tbl_employee b,tbl_position d  where a.apid=b.apid  and a.pos_id = d.pos_id and b.isActive='yes' and d.class='Agent' and b.isActive='yes' order by lname");
				// $query = $this->db->query("select b.emp_id,b.apid,fname,lname,pos_name from tbl_applicant a,tbl_employee b,tbl_position d  where a.apid=b.apid  and a.pos_id = d.pos_id and b.isActive='yes' and d.class='Agent' and b.isActive='yes' order by lname");
				  // $query = $this->db->query("select b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and d.isActive=1 and status='Probationary' or status='regular' order by lname ");
				 // $query = $this->db->query("select f.rate_id,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and d.isActive=1 and (g.status='Probationary' or g.status='Regular') and b.salarymode='Monthly' and e.class='Admin' and confidential=0 and f.isactive=1 and b.isactive='Yes' order by lname");
				$query = $this->db->query(" select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='Admin' and d.isActive=1 and b.salarymode='$st2' order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$pid);

		foreach ($query->result() as $row)
			{
				$arr['employee'][$row->apid] = array(
				"apid" => $row->apid,
 				"fname" => $row->fname,
 				"lname" => $row->lname,
 				"emp_id" => $row->emp_id,
  				);
			}
	if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='attendance' and is_assign=1 order by lname ");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
				if($pid>0){
						$per_id =  $this->db->query("select coverage_id,daterange,month from tbl_payroll_coverage where coverage_id=".$pid."");
					foreach ($per_id->result() as $row)
					{
							$arr['period'] = array(
							"daterange" => $row->daterange,
							"coverage_id" => $row->coverage_id,
							"month" => $row->month,
							
							);
					}
					$arr["stat"] = array("status" => $stat);
					$this->load->view('attendance_admin_sub',$arr);
				}else{
					
				}
			}else{
				redirect('index.php/home/error');
			}
		}else{
					redirect('index.php/login');
		}
	
	
		}
		public function emptype(){
		 $payrollC = $this->input->post('payrollC');
		 $employz = $this->input->post('employz');
		  $date = explode("-",$payrollC);
		$HolidateSEnd = array(
			 '2:00:00 PM' => '0',
			 '3:00:00 PM' => '0',
			 '4:00:00 PM' => '0',
			 '5:00:00 PM' => '0',
			 '6:00:00 PM' => '0',
			 '7:00:00 PM' => '0',
			 '8:00:00 PM' => '0',
			 '9:00:00 PM' => '0',
			 '9:30:00 PM' => '0',
			 '10:00:00 PM' => '0',
			 '10:15:00 PM' => '0',
			 '10:30:00 PM' => '0',
			 '11:00:00 PM' => '0',
			 '12:00:00 AM' => '0',
			 '1:00:00 AM' => '1',
			 '2:00:00 AM' => '2',
			 '3:00:00 AM' => '2.75',
			 '4:00:00 AM' => '3.25',
			 '5:00:00 AM' => '4.25',
			 '6:00:00 AM' => '5.25',
			 '6:30:00 AM' => '6.5',
			 '7:00:00 AM' => '6',
			 '8:00:00 AM' => '7',
			 '9:00:00 AM' => '0',
			 '9:30:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
		  );
		  $HolidateStart = array(
			 '2:00:00 PM' => '8',
			 '3:00:00 PM' => '8',
			 '4:00:00 PM' => '7',
			 '5:00:00 PM' => '6',
			 '6:00:00 PM' => '5.25',
			 '7:00:00 PM' => '4.75',
			 '8:00:00 PM' => '3.75',
			 '9:00:00 PM' => '2.75',
			 '9:30:00 PM' => '2.5',
			 '10:00:00 PM' => '2',
			 '10:15:00 PM' => '1.75',
			 '10:30:00 PM' => '1.5',
			 '11:00:00 PM' => '1',
			 '12:00:00 AM' => '8',
			 '1:00:00 AM' => '8',
			 '2:00:00 AM' => '8',
			 '3:00:00 AM' => '8',
			 '4:00:00 AM' => '8',
			 '5:00:00 AM' => '8',
			 '6:00:00 AM' => '8',
			 '7:00:00 AM' => '8',
			 '8:00:00 AM' => '8',
			 '9:00:00 AM' => '8',
			 '9:30:00 AM' => '8',
			 '10:00:00 AM' => '8',
			 '11:00:00 AM' => '8',
			 '12:00:00 PM' => '8',
			 '1:00:00 PM' => '8',
		  );
		   $NDHolidateStart= array(
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
			 '2:00:00 PM' => '1',
			 '3:00:00 PM' => '2',
			 '4:00:00 PM' => '1.25',
			 '5:00:00 PM' => '1.25',
			 '6:00:00 PM' => '1.5',
			 '7:00:00 PM' => '1.5',
			 '8:00:00 PM' => '1.75',
			 '9:00:00 PM' => '2',
			 '9:30:00 PM' => '2',
			 '10:00:00 PM' => '2',
			 '10:15:00 PM' => '1.75',
			 '10:30:00 PM' => '1.5',
			 '11:00:00 PM' => '1',
			 '12:00:00 AM' => '5.25',
			 '1:00:00 AM' => '4.25',
			 '2:00:00 AM' => '3.75',
			 '3:00:00 AM' => '2.75',
			 '4:00:00 AM' => '2',
			 '5:00:00 AM' => '1',
			 '6:00:00 AM' => '0',
			 '6:30:00 AM' => '0',
			 '7:00:00 AM' => '0',
			 '8:00:00 AM' => '0',
			 '9:00:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
			
		  );
		  	$NDHolidateEnd= array(
			 '9:00:00 PM' => '0',  //12:00:00 PM
			 '10:00:00 PM' => '0',  //1:00:00 PM
			 '11:00:00 PM' => '0', //2:00:00 PM
			 '12:00:00 AM' => '0', //3:00:00 PM
			 '1:00:00 AM' => '1', //4:00:00 PM
			 '2:00:00 AM' => '2', //5:00:00 PM
			 '3:00:00 AM' => '2.75', //6:00:00 PM
			 '4:00:00 AM' => '3.75', //7:00:00 PM
			 '5:00:00 AM' => '4.25', //8:00:00 PM
			 '6:00:00 AM' => '5.25', //9:00:00 PM
			 '6:30:00 AM' => '5.25', //9:30:00 PM
			 '7:00:00 AM' => '5.25', //10:00:00 PM
			 '7:15:00 AM' => '5.25', //10:15:00 PM
			 '7:30:00 AM' => '5.25', //10:30:00 PM
			 '8:00:00 AM' => '5.25', //11:00:00 PM
			 '9:00:00 AM' => '5.25', //12:00:00 AM
			 '10:00:00 AM' => '0', //1:00:00 AM
			 '11:00:00 AM' => '0', //2:00:00 AM
			 '12:00:00 PM' => '0', //3:00:00 AM
			 '1:00:00 PM' => '0', // 4:00:00 AM
			 '2:00:00 PM' => '0', //5:00:00 AM
			 '3:00:00 PM' => '0', //6:00:00 AM
			 '3:30:00 PM' => '0', //6:30:00 AM
			 '4:00:00 PM' => '0', //7:00:00 AM
			 '5:00:00 PM' => '0', //8:00:00 AM
			 '6:00:00 PM' => '0', //9:00:00 AM
			 '7:00:00 PM' => '0', //10:00:00 AM
			 '8:00:00 PM' => '0', //11:00:00 AM
		  );
		  $query = $this->db->query("select b.apid,fname,lname,emp_id from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes' and b.emp_id in (".$employz.")");
		  foreach ($query->result() as $row){
			$DateRangeShift = $this->DateRangeShift($date);
			
				foreach($DateRangeShift as $row1 => $data){
					$shift = $this->empshift($row->emp_id,$data);
					foreach($shift as $row2 => $data2){
						$in = $this->empshiftin($row->emp_id,$data2->sched_id);
						$out = $this->empshiftout($row->emp_id,$data2->sched_id);
						$emplogsShift2 = $this->emplogsShift($data2->sched_id,$row->emp_id);
						$empRate2 = $this->empRate($row->emp_id);

							$timeschedule = $this->timeschedule($data2->sched_id);
							$NDbegin = "10:10:00 PM";
							$NDend   = "6:00:00 AM";
							
							$HPbegin = "12:00:00 AM";
							$HPend   = "3:00:00 AM";
							
							$HPbegin2 = "2:00:00 PM";
							$HPend2   = "11:59:59 PM";
							
							$HPbegin3 = "4:00:00 AM";
							$HPend3   = "5:00:00 AM";
							 $ndRemark = "N";
							
						if($data2->type=="Normal" || $data2->type=="WORD"){
							if(count($in)>0){
							// $to_time= strtotime($in[0]->login);
							$to_time= strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login)));
							$from_time = strtotime($data." ".$timeschedule[0]->time_start);
								$NDdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
								$NDdate2 = DateTime::createFromFormat('H:i a', $NDbegin);
								$NDdate3 = DateTime::createFromFormat('H:i a', $NDend);

								$HPdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
								$HPdate2 = DateTime::createFromFormat('H:i a', $HPbegin);
								$HPdate3 = DateTime::createFromFormat('H:i a', $HPend);
								
								$HPdate4 = DateTime::createFromFormat('H:i a', $HPbegin2);
								$HPdate5 = DateTime::createFromFormat('H:i a', $HPend2);
								
								$HPdate6 = DateTime::createFromFormat('H:i a', $HPbegin3);
								$HPdate7 = DateTime::createFromFormat('H:i a', $HPend3);
								if ($NDdate1 >= $NDdate2 or $NDdate1 <= $NDdate3){
								   $ndRemark = "Y";
								}else{
								   $ndRemark= "N";
								}
								if (($HPdate1 >= $HPdate4 and $HPdate1 <= $HPdate5) or ($HPdate1 >= $HPdate6 and $HPdate1 <= $HPdate7) ){
											$hpRemark = "Y";
											$hpRate = '0.08';
								}else if(($HPdate1 >= $HPdate2) and ($HPdate1 <= $HPdate3)){
										   $hpRemark = "Y";
											$hpRate = '0.10';
								}else{
											$hpRemark= "N";
											 $hpRate = '';	
								}
								
								if($timeschedule[0]->time_start == "12:00:00 AM"){
												 // if(date('h:i:s a', strtotime($in[0]->login))>date('h:i:s a', strtotime("12:00:00 am"))){
												if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) >= strtotime(date('Y-m-d H:i:00', strtotime($data." 24:00:00")) )){

														 // $data= date('Y-m-d', strtotime('-1 day', strtotime($data)));
														 $from_time = strtotime($data." 24:00:00");
														$timediff = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
															$latemins = (round(abs($from_time - $to_time) / 60,2));

												 }else{
														 
														$from_time = strtotime($data." 24:00:00");
														$timediff = round(abs($from_time - $to_time) / 60,2)." mins. early";
															$latemins = 0;
												 }												 
											 }else{
												if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) > strtotime(date('Y-m-d H:i:00', strtotime($data." ".$timeschedule[0]->time_start))) ){
								 
													$timediff = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
													$latemins = (round(abs($from_time - $to_time) / 60,2));
												}else{
													$timediff = round(abs($from_time - $to_time) / 60,2)." mins. early";
													$latemins = 0;
												}
											 }
								
							$BCTSched = date('H:i:s', strtotime('-10 mins', strtotime($timeschedule[0]->time_start)));
							$bct = ($data." ".$BCTSched.":00" > $in[0]->login ) ? "Q" : "<span style='color:red'>DQ</span>";
							}else{
							$timediff = "<span style='color:red'><b>Absent</b></span>";
							$latemins =0;
							$bct = "<span style='color:red'>DQ</span>";
							$hpRemark= "N";
							$hpRate = '';	
											 }
						}else{
							 $timediff = "<span style='color:red'>".$data2->type."</span>";
							 $latemins =0;
							 $hpRemark= "N";
							 $hpRate = '';	
							 $bct  = "DQ";
						}
						$logoutt =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD") ? $out[0]->logout : $data." 00:00:00" : $data." 00:00:00";
						$loginn =  (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD") ? $in[0]->login : $data." 00:00:00" : $data." 00:00:00";
						$logoutt2 =  ($data2->type=="Normal" || $data2->type=="WORD") ? $timeschedule[0]->time_end : "00:00:00";
						$loginn2 =  ($data2->type=="Normal" || $data2->type=="WORD") ? $timeschedule[0]->time_start : "00:00:00";
									
							$isLogoutHoliday2 = $this->isLogoutHoliday($logoutt,$logoutt2);
							$isLoginHoliday2 = $this->isLoginHoliday($loginn,$loginn2);
							$TotalAmountHoliday1=0;
							$TotalAmountHoliday2=0;	
							$NDComputeHolidayLogout=0;	
							$HPComputeHolidayLogout=0;	
							$NDComputeHolidayLogin=0;	
							$HPComputeHolidayLogin=0;	
							$HolidayType1 =0;
							$HolidayType2=0;
							  if(count($isLogoutHoliday2[0]->date)>0){
								  
							$HolidayType =$isLogoutHoliday2[0]->calc;
						if(array_key_exists($logoutt2,$HolidateSEnd)){
							$isLogoutHoliday1  =  $HolidateSEnd[$logoutt2];
							$isLogoutHolidayND1  =  $NDHolidateEnd[$logoutt2];
							$TotalAmountHoliday1 = (((($empRate2[0]->rate*12)/261)/8)*$HolidayType)*$isLogoutHoliday1;  
							$NDComputeHolidayLogout =  ($isLogoutHolidayND1 *  $HolidayType);
							$HPComputeHolidayLogout =  ($isLogoutHolidayND1 * $HolidayType);
							$HolidayType2 =  $isLogoutHoliday2[0]->type;
						}else{
							$isLogoutHoliday1  =  0 ;
						}

					}else{
					 $isLogoutHoliday1  =  0;
					  
					}
					if(count($isLoginHoliday2[0]->date)>0){
						$HolidayType = $isLoginHoliday2[0]->calc;
					if(array_key_exists($loginn2,$HolidateStart)){
						$isLoginHoliday1  =  $HolidateStart[$loginn2];
						$isLoginHolidayND1  =  $NDHolidateStart[$loginn2];

						$TotalAmountHoliday2 = (((($empRate2[0]->rate*12)/261)/8)*$HolidayType)*$isLoginHoliday1;  
						 $NDComputeHolidayLogin = ($isLoginHolidayND1 * $HolidayType);
						$HPComputeHolidayLogin =  ($isLoginHolidayND1 * $HolidayType);
						$HolidayType1 =  $isLoginHoliday2[0]->type;
					}else{
						$isLoginHoliday1  =  0 ;
					}
				  }else{
					 $isLoginHoliday1  =  0;
				  }
				$from_timeLog = strtotime($loginn);
				$to_timeLog = strtotime($logoutt);
				$resLog = round(abs(($to_timeLog-$from_timeLog)/3600),2);

						$arr["employee"][$row->emp_id][$data]= array(
							"fname" => $row->lname.", ".$row->fname,
							"date" => $data,
							"shift_type" => $data2->type,
							"login" =>(count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD") ? date('Y-m-d h:i:s a', strtotime($in[0]->login)) : $data2->type : "No Log IN",
							"logout" => (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD") ? date('Y-m-d h:i:s a', strtotime($out[0]->logout)) : $data2->type : "No Log OUT",
							"shiftStart" => ($data2->type=="Normal" || $data2->type=="WORD") ? $timeschedule[0]->time_start : $data2->type,
							"shiftEnd" => ($data2->type=="Normal" || $data2->type=="WORD") ? $timeschedule[0]->time_end : $data2->type,
							"late" => $timediff,
							"latemins" => $latemins/60,
 							"bct" => $bct,
							"ndRemark" =>   $ndRemark,
							"hpRemark" =>   $hpRemark,
							"hpRate" =>   $hpRate,
							"nd" =>  (count($emplogsShift2)>0) ? ($data2->type=="Normal" || $data2->type=="WORD") ? $emplogsShift2[0]->nd : "0.00" : "0-0",
							"isHoliday" =>    $isLogoutHoliday1+ $isLoginHoliday1,
							"isLoginHoliday" =>    $isLoginHoliday1,
							"isLogoutHoliday" =>    $isLogoutHoliday1,
							"HPComputeHolidayLogin" =>   $HPComputeHolidayLogin,
							"NDComputeHolidayLogin" =>   $NDComputeHolidayLogin,
							"NDComputeHolidayLogout" =>   $NDComputeHolidayLogout,
							"HPComputeHolidayLogout" =>   $HPComputeHolidayLogout,
							"HolidayType1" =>   $HolidayType1,
							"HolidayType2" =>   $HolidayType2,
							"total" =>   ($resLog>0) ? $resLog : 0,
 							"loginMilitary" =>(count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD") ? date('Y-m-d H:i:s', strtotime($in[0]->login)) : $data2->type : "No Log IN",
							"logoutMilitary" => (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD") ? date('Y-m-d H:i:s', strtotime($out[0]->logout)) : $data2->type : "No Log OUT",
							"timess" => date('Y-m-d G:i:s', strtotime($data." 12:00:00 am")),
						);
					}
				}
			}
			echo json_encode($arr);
 		}
		public function emptyperesult(){
			$Emptype = $this->input->post('Emptype');
			
		$query = $this->db->query("select b.apid,fname,lname,b.emp_id,pos_name,e.class from tbl_applicant a,tbl_employee b,tbl_emp_promote c,tbl_pos_emp_stat d,tbl_position e where a.apid=b.apid and b.emp_id=c.emp_id and d.posempstat_id=c.posempstat_id and d.pos_id=e.pos_id and b.isActive='yes' and c.isActive=1 and e.class='".$Emptype."'");
		foreach ($query->result() as $row)
			{
				$arr['emp'][$row->emp_id] = array(
					"fname" => $row->lname.", ".$row->fname,
					"emp_id" => $row->emp_id 
					
				);
			}
		
			    echo json_encode($arr);

		}
		 public function empshift($empid,$date){
		$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and sched_date = '".$date."' and emp_id=".$empid);
			  return $query->result();
		 }
		public function timeschedule($timeschedule){
$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_acc_time b,tbl_time c  where a.acc_time_id = b.acc_time_id and b.time_id = c.time_id and sched_id=".$timeschedule);
			  return $query->result();
		 }
		 public function DateRangeShift($date){
		$strDateFrom = date("Y-m-d", strtotime($date[0]));
		$strDateTo = date("Y-m-d", strtotime($date[1]));
		$aryRange=array();
		$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
			if($iDateTo>=$iDateFrom){
				array_push($aryRange,date('Y-m-d',$iDateFrom));
				 while($iDateFrom<$iDateTo){
					$iDateFrom+=86400; // add 24 hours
						array_push($aryRange,date('Y-m-d',$iDateFrom));
					}
			}
			 return $aryRange;
			 		
		}
		 
		  public function empshiftin($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and sched_id=".$shift." and emp_id=".$empid);
			  return $query->result();
 		}
		  public function empshiftout($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as logout,acc_time_id from tbl_dtr_logs where entry='O' and type='DTR' and sched_id=".$shift." and emp_id=".$empid);
			  return $query->result();
 		}
		  public function emplogs($date,$empid){
		// public function emplogs(){
				// $date = '2017-04-05';
				// $empid = 350;
			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and log like '%".$date."%' and emp_id=".$empid."");
				
			  return $query->result();
			// echo json_encode($query->result());
 			 
 		}
		public function emplogsOut($dtrid,$empid){
 
			$query = $this->db->query("select dtr_id,emp_id,log as logout from tbl_dtr_logs where entry='O' and type='DTR' and note=".$dtrid." and emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function emplogsShift($sched_id,$empid){
 
			// $query = $this->db->query("select time_start,time_end from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.acc_time_id=".$acc_time_id." and emp_id=".$empid."");
			$query = $this->db->query("select distinct(c.time_id),time_start,time_end,(select group_concat(concat(hour,'-',minutes))  from tbl_payroll_nd as s where s.time_id=c.time_id) as nd from tbl_dtr_logs a,tbl_acc_time b,tbl_time c,tbl_schedule d where b.acc_time_id=d.acc_time_id and b.time_id=c.time_id and a.sched_id=d.sched_id and a.sched_id=".$sched_id."  and a.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function empRate($empid){
 
			$query = $this->db->query("select b.apid,b.emp_id,fname,lname,pos_name,g.status,rate from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 and b.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function NightDiff($nd){
 			$query = $this->db->query("select time_start,time_end,hour,minutes from tbl_payroll_nd a,tbl_time b where a.time_id=b.time_id and a.isActive=1 and b.time_start ='".$nd."'");
				
			 return $query->result();
  		}
		public function SSSCompute($empid){
 			// $query = $this->db->query("select sss_employee from (select rate from tbl_rate a,tbl_position b,tbl_employee c,tbl_applicant d where a.pos_id=b.pos_id and c.apid=d.apid and b.pos_id =d.pos_id and a.isActive=1 and c.isActive='yes' and c.emp_id=".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
 			  $query = $this->db->query("select sss_employee from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
				
			 return $query->result();
  		}
		public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		}
		public function isLoginHoliday($login,$timeStart){
			$year = date_format(date_create($login),"Y");
			  $date = date_format(date_create($login),"Y-m-d");
			  $timeStart = date_format(date_create($timeStart),"h:i:s");
 			
			$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");

			return $query->result();
  		}
		public function isLogoutHoliday($logout,$timeEnd){
			  $year = date_format(date_create($logout),"Y");
			  $date = date_format(date_create($logout),"Y-m-d");
			  $timeEnd = date_format(date_create($timeEnd),"h:i:s");
 			
		$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");

			return $query->result();
  		}
		 
		/* public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare from (select rate from tbl_rate a,tbl_position b,tbl_employee c,tbl_applicant d where a.pos_id=b.pos_id and c.apid=d.apid and b.pos_id =d.pos_id and a.isActive=1 and c.isActive='yes' and c.emp_id=".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		} */
	
 
}