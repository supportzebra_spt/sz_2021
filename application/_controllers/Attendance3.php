<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance3 extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	
	public function index($stat=null,$pid=null,$site=null){
						$status = explode("-",$stat);
				$append = ($site!=null) ? " and pos_details like '%".$site."%'" : " and pos_details not like '%cebu%'";

				if($status[0]=="agent"){
							$query = $this->db->query(" select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and (g.status='".ucfirst($status[1])."')  and e.class='Agent' and d.isActive=1 ".$append." order by lname) as x, tbl_payroll y,tbl_payroll_coverage z where x.emp_promoteId = y.emp_promoteId and z.coverage_id=y.coverage_id and y.coverage_id=".$pid);
				}else{
 					if($status[1]=="trainee"){
							$query = $this->db->query("Select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='Admin' and (g.status!='Part-time') and confidential=0 and b.isActive='yes' and d.isActive=1 and b.salarymode='Daily' and e.pos_details!='SMT Vogmsi' order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$pid);

					}else{
						$conf = ($status[2]=="nonconfi") ? 0 : 1;
							$query = $this->db->query(" select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and  e.class='Admin'  and confidential=$conf and d.isActive=1 order by lname) as x, tbl_payroll y,tbl_payroll_coverage z where x.emp_promoteId = y.emp_promoteId and z.coverage_id=y.coverage_id and y.coverage_id=".$pid);

						}
					
				
				}
				  
 		foreach ($query->result() as $row)
			{
				$arr['employee'][$row->apid] = array(
				"apid" => $row->apid,
 				"fname" => $row->fname,
 				"lname" => $row->lname,
 				"emp_id" => $row->emp_id,
 				"emp_promoteId" => $row->emp_promoteId,
  				);
			}
	if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='GeneratePayroll/$stat' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
					if($pid>0){
						$per_id =  $this->db->query("select coverage_id,daterange,month,period,year from tbl_payroll_coverage where coverage_id=".$pid."");
					foreach ($per_id->result() as $row)
					{
							$arr['period'] = array(
							"daterange" => $row->daterange,
							"coverage_id" => $row->coverage_id,
							"month" => $row->month,
							"year" => $row->year,
							"periods" => $row->period,
							
							);
					}
					$arr["stat"] = array("status" => $stat);
					$arr["site"] = array("loc" => $site);

					$this->load->view('attendance3',$arr);
				}else{
					
				}
			}else{
				redirect('index.php/home/error');
			}
		}else{
					redirect('index.php/login');
		}
	
	
		}
	 public function sendPDFEmail(){
		 $payrollC = $this->input->post('payrollC');
		 $employz = $this->input->post('employz');
		 $emp_type = $this->input->post('emp_type');
		 $query = $this->db->query("SELECT fname,lname,email,c.emp_id,e.daterange,e.transact_date,g.pos_name,g.pos_details,h.status,d.*,(select password from tbl_user x where x.emp_id=c.emp_id) as password from tbl_applicant a,tbl_employee b, tbl_emp_promote c,tbl_payroll d,tbl_payroll_coverage e,tbl_pos_emp_stat f,tbl_position g,tbl_emp_stat h where a.apid=b.apid and b.emp_id=c.emp_id and c.emp_promoteId = d.emp_promoteId and f.posempstat_id = c.posempstat_id and f.pos_id =g.pos_id and h.empstat_id = f.empstat_id and d.coverage_id= e.coverage_id and c.emp_promoteID in (".$employz.") and e.coverage_id=".$payrollC."");
 
			$coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$payrollC);
		 	$year= $coverage->result()[0]->year;
		 	$month= $coverage->result()[0]->month; 
		 	$period= $coverage->result()[0]->period;
		 	$type= "agent";

			foreach ($query->result() as $row)
			{
				$fname = 'mark';
				$mname = 'roowts';
				$lname = 'asas';
					$this->email->initialize(array(
					  'protocol' => 'smtp',
					  'smtp_host' => 'smtp.gmail.com',
					  'smtp_user' => 'notification@supportzebra.com',
					  'smtp_pass' => 'spt@2018!',
					  'smtp_port' => 587,
					  'smtp_crypto' => 'tls',
					  'mailtype	' => 'html',
					  'validate' => TRUE,
					  'crlf' => "\r\n",
					  'charset' => "iso-8859-1",
					  'newline' => "\r\n"
					));
						$data = array(
						 'Name'=> $fname." ".$mname." ".$lname,
							 );
					$savefile2 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/$year/$month/$period/$type/SZ_Payslip_".strtoupper($row->lname)."_".strtoupper($month)."_".$period."_".$row->emp_id.".pdf"; 
					$this->email->attach($savefile2);

					$this->email->from('jeffrey@supportzebra.com', 'jeffrey');
					$this->email->to($row->email);
					$this->email->subject('SupportZebra - Payslip_'.$year.'_'.$month.'_Payroll_Period_'.$period);
					$this->email->message("Hi, <br><br> Please see attached file of your payslip. <br><br> <br><br>  --<br> Human Resources Team  ");
					$this->email->send();

					echo $this->email->print_debugger();			 

			}
	 }
	 public function emptype3(){
		 $payrollC = $this->input->post('payrollC');
		 $employz = $this->input->post('employz');
		 $emp_type = $this->input->post('emp_type');
		 $query = $this->db->query("SELECT fname,lname,email,c.emp_id,e.daterange,e.transact_date,g.pos_name,g.pos_details,h.status,d.*,(select password from tbl_user x where x.emp_id=c.emp_id) as password from tbl_applicant a,tbl_employee b, tbl_emp_promote c,tbl_payroll d,tbl_payroll_coverage e,tbl_pos_emp_stat f,tbl_position g,tbl_emp_stat h where a.apid=b.apid and b.emp_id=c.emp_id and c.emp_promoteId = d.emp_promoteId and f.posempstat_id = c.posempstat_id and f.pos_id =g.pos_id and h.empstat_id = f.empstat_id and d.coverage_id= e.coverage_id and c.emp_promoteID in (".$employz.") and e.coverage_id=".$payrollC."");
		 $coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$payrollC);
		 	$year= $coverage->result()[0]->year;
		 	$month= $coverage->result()[0]->month; 
		 	$period= $coverage->result()[0]->period;
		 	$type= "agent";
			
		$savefile2 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/$year/$month/$period/$type/"; 
  
	$this->load->library('Pdf');

		 $arr = array();
			foreach ($query->result() as $row)
			{ 
  
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetProtection(array('print', 'copy','modify'), $row->password, $row->password, 0, null);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('SupportZebra Payslip - MJM');
$pdf->SetTitle('Payslip');
$pdf->SetSubject('Payslip');
$pdf->SetKeywords('Payslip, Payslip, Payslip, Payslip, Payslip');

$PDF_HEADER_LOGO = "logo2.png";//any image file. check correct path.
$PDF_HEADER_LOGO_WIDTH = "50";
$PDF_HEADER_TITLE = "SupportZebra Payslip";
$PDF_HEADER_STRING = " Tel: +63(88)8551519 \n Mob: +63(17)7007656\n Email: recruitement@supportzebra.com\n Website: www.supportzebra.com \n\n";
$pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE,$PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}


// add a page
$pdf->AddPage();

 
$pdf->SetFont('helvetica', '', 8);

 $pdf->Multicell(0,2," \n \n "); 

// NON-BREAKING TABLE (nobr="true")
$tax = explode("(",$row->bir_details);
$tax_type = substr_replace($tax[1] ,"",-1);
$clothing=explode("(",$row->clothing);
$laundry=explode("(",$row->laundry);
$rice=explode("(",$row->rice);
$totalEarning  = 0;
$totalDeduction  = 0;
$tbl = '
<style type="text/css">
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
	} 
	.tableizer-table td {
		padding: 4px;
		margin: 3px;
		border: 1px solid #CCC;
	}
	.tableizer-firstrow th {
		padding:17px;
	}
	.tableizer-table th {
		background-color: #104E8B; 
		color: #FFF;
		font-weight: bold;
		text-align:center
	}
</style> <table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th colspan="4">PAY STATEMENT - EMPLOYEES COPY</th></tr></thead><tbody>
  <tr><td>NAME:      </td><td>'.$row->lname.', '.$row->fname.'</td><td>POSITION</td><td>'.$row->pos_name.'</td></tr>
 <tr><td>Date:        </td><td>September 16-30, 2017</td><td>TAX STATUS</td><td>'.$tax_type.'</td></tr>
 <tr><td>Period Covered:       </td><td>October 5, 2017</td><td> ATM </td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>EARNINGS:</td><td>SALARY</td><td colspan="2"> '.$row->quinsina.' </td></tr>
 <tr><td>&nbsp;</td><td>CLOTHING ALLOWANCE</td><td colspan="2">'.$clothing[0].'</td></tr>
 <tr><td>&nbsp;</td><td>LAUNDRY ALLOWANCE</td><td colspan="2"> '.$laundry[0].' </td></tr>
 <tr><td>&nbsp;</td><td>RICE SUBSIDY</td><td colspan="2"> '.$rice[0].' </td></tr>
 <tr><td>&nbsp;</td><td>NIGHT DIFF</td><td  colspan="2"> '.$row->nd.' </td></tr>
 <tr><td>&nbsp;</td><td>OVERTIME</td><td colspan="2"> '.$row->ot_bct.' </td></tr>
 <tr><td>&nbsp;</td><td>HAZARD PAY</td><td colspan="2"> '.$row->hp.' </td></tr>
 <tr><td>&nbsp;</td><td>HOLIDAY</td><td colspan="2"> '.$row->ho.'</td></tr>';
 
		if(trim($row->adjust_details)!="0"){
				$adjust_details = explode(",",$row->adjust_details);
				foreach($adjust_details as $roww){
					$dd = explode("=",$roww);
					$x = explode("|",$dd[0]);
					
					 if(trim($x[0])!="Bonus"){
					$tbl .='<tr><td>&nbsp;</td><td>'.trim($x[1]).'</td><td> P'.$dd[1].'</td></tr>';
					$totalEarning += $dd[1];
					 }
					
 				}
			} 
			$totalEarning += $row->quinsina + $clothing[0] +  $laundry[0]+ $rice[0]+ $row->nd+ $row->ot_bct + $row->hp +  $row->ho; 
$tbl.='<tr><td>&nbsp;</td><td>Total Earnings</td><td colspan="2"> P'.$totalEarning.' </td></tr>
 <tr><td>DEDUCTIONS: </td><td>WITHHOLDING TAX</td><td colspan="2"> '.$tax[0].'</td></tr>
 <tr><td>&nbsp;</td><td>SSS PREMIUM</td><td colspan="2"> '.$row->sss_details.'</td></tr>
 <tr><td>&nbsp;</td><td>SSS LOAN</td><td colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>HDMF PREMIUM</td><td  colspan="2"> '.$row->hdmf_details.' </td></tr>
 <tr><td>&nbsp;</td><td>HDMF LOAN</td><td  colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>PHILHEALTH</td><td  colspan="2"> '.$row->phic_details.' </td></tr>
 <tr><td>&nbsp;</td><td>LEAVE/UNDERTIME/OB/TARDINESS</td><td  colspan="2"> P'.trim($row->absentLate).' </td></tr>';
			if(trim($row->deduct_details)!="0"){
				$deduct_details = explode(",",$row->deduct_details);
				foreach($deduct_details as $roww){
					$dd = explode("=",$roww);
					 
					 
					$tbl .='<tr><td>&nbsp;</td><td>'.trim($dd[0]).'</td><td> P'.$dd[1].'</td></tr>';
					$totalDeduction+= $dd[1];
 				}
			} 
			$totalDeduction += $tax[0] + $row->sss_details + $row->hdmf_details + $row->phic_details + $row->absentLate;
 $tbl.='<tr><td>&nbsp;</td><td>Total Deductions</td><td  colspan="2">P'.$totalDeduction .' </td></tr>
 <tr><td>TAKE HOME PAY</td><td>&nbsp;</td><td  colspan="2"> P'.($totalEarning- $totalDeduction) .' </td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td  colspan="2">&nbsp;</td></tr>
 <tr><td>Signature:</td><td>&nbsp;</td><td>Date:</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td colspan="4">Note: Keep this statement permanently for Income Tax purposes.</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
</tbody></table> ';
 $pdf->writeHTML($tbl, true, false, false, false, '');

 $pdf->AddPage();
 
$pdf->SetFont('helvetica', '', 8);

 $pdf->Multicell(0,2," \n \n "); 

$tbl='<style type="text/css">
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
	} 
	.tableizer-table td {
		padding: 4px;
		margin: 3px;
		border: 1px solid #CCC;
	}
	.tableizer-firstrow th {
		padding:17px;
	}
	.tableizer-table th {
		background-color: #104E8B; 
		color: #FFF;
		font-weight: bold;
		text-align:center
	}
</style>
<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th colspan="4">Bonus STATEMENT - EMPLOYEES COPY</th></tr></thead><tbody>
  <tr><td>NAME:      </td><td>'.$row->lname.', '.$row->fname.'</td><td>POSITION</td><td>'.$row->pos_name.'</td></tr>
 <tr><td>Date:        </td><td>September 16-30, 2017</td><td>TAX STATUS</td><td>'.$tax_type.'</td></tr>
 <tr><td>Period Covered:       </td><td>October 5, 2017</td><td> ATM </td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Bonuses:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
 if(trim($row->adjust_details)!="0"){
				$adjust_details = explode(",",$row->adjust_details);
				foreach($adjust_details as $roww){
					$dd = explode("=",$roww);
					$x = explode("|",$dd[0]);
					
					 if(trim($x[0])=="Bonus"){
					$tbl .='<tr><td>&nbsp;</td><td> '.trim($x[1]).'</td><td> P'.$dd[1].'</td></tr>';
 					 }
					
 				}
			} 
 if($row->bonus_details!="0"){
		$bonuss = explode(",",$row->bonus_details);
			foreach($bonuss as $roww){
				$bns = explode("=",$roww);
				if(count($bns)>1){
				  
				if((int)substr($bns[1], 1)>0){
				$tbl .='<tr><td>&nbsp;</td><td>'.$bns[0].'</td><td> P'.number_format((int)substr($bns[1], 1), 2, '.', '').'</td></tr>';
				}
			}
		}
	}
 $tbl .='<tr><td>TAKE HOME PAY</td><td>&nbsp;</td><td  colspan="2"> -- </td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td  colspan="2">&nbsp;</td></tr>
 <tr><td>Signature:</td><td>&nbsp;</td><td>Date:</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td colspan="4">Note: Keep this statement permanently for Income Tax purposes.</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
</tbody></table>';

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

 
// -----------------------------------------------------------------------------

//Close and output PDF document
	$pdfDirectory = "reports/payroll/$year/$month/$period/$type/"; 

$pdf->Output($savefile2.'SZ_Payslip_'.strtoupper($row->lname).'_'.strtoupper($month).'_'.$period.'_'.$row->emp_id.'.pdf', 'F');
$arr["payslipPDF"][$row->emp_id] = array(
	"TCPDF" => $row->lname.", ".$row->fname.'|'.$pdfDirectory.'SZ_Payslip_'.strtoupper($row->lname).'_'.strtoupper($month).'_'.$period.'_'.$row->emp_id.'.pdf',
);
$totalEarning = 0;
$totalDeduction = 0;
	}
	echo json_encode($arr);
	 }
	 public function emptype2(){
		 $payrollC = $this->input->post('payrollC');
		 $employz = $this->input->post('employz');
		 $query = $this->db->query("select x.*,y.*,z.daterange,z.transact_date,deduct_details,adjust_details from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and d.isActive=1 order by lname) as x,tbl_payroll y,tbl_payroll_coverage z where x.emp_promoteId = y.emp_promoteId and z.coverage_id=y.coverage_id and z.daterange='".$payrollC."' and y.emp_promoteId in (".$employz.")");
		 
			$arr = array(); 
			foreach ($query->result() as $row)
			{
				$arr[$row->payroll_id] = array(
					"payroll_id" => $row->payroll_id,
					"emp_promoteId" => $row->emp_promoteId,
					"daterange" => $row->daterange,
					"transact_date" => $row->transact_date,
					"fullname" => $row->lname.", ".$row->fname,
					"pos_name" => $row->pos_name,
					"status" => $row->status,
					"rate" => $row->rate,
					"allowance" => $row->allowance,
					"monthly" => $row->monthly,
					"quinsina" => $row->quinsina,
					"clothing" => $row->clothing,
					"laundry" => $row->laundry,
					"rice" => $row->rice,
					"ot_bct" => $row->ot_bct,
					"nd" => $row->nd,
					"hp" => $row->hp,
					"ho" => $row->ho,
					"sss_details" => $row->sss_details,
					"phic_details" => $row->phic_details,
					"hdmf_details" => $row->hdmf_details,
					"bir_details" => $row->bir_details,
					"adjust_add" => $row->adjust_add,
					"adjust_minus" => $row->adjust_minus,
					"total_earning" => $row->total_earning,
					"final" => $row->final,
					"isActive" => $row->isActive,
					"deduct_details" => $row->deduct_details,
					"adjust_details" => $row->adjust_details,
					"total_deduction" => $row->total_deduction,
					"absentLate" => $row->absentLate,
					"bonus_details" => $row->bonus_details					
 
				);
			}
			echo json_encode($arr);
 	 }
	 
	
	public function emptype(){
		 $payrollC = $this->input->post('payrollC');
		 $employz = $this->input->post('employz');
		  $date = explode("-",$payrollC);
	  $arr= Array();
	 	 	  		$HolidateSEnd = array(
			 '2:00:00 PM' => '0',
			 '3:00:00 PM' => '0',
			 '4:00:00 PM' => '1',
			 '5:00:00 PM' => '2',
			 '6:00:00 PM' => '2.75',
			 '7:00:00 PM' => '3.25',
			 '8:00:00 PM' => '4.25',
			 '9:00:00 PM' => '5',
			 '9:30:00 PM' => '5.5',
			 '10:00:00 PM' => '6',
			 '10:15:00 PM' => '6.25',
			 '10:30:00 PM' => '6.5',
			 '11:00:00 PM' => '7',
			 '12:00:00 AM' => '0',
			 '1:00:00 AM' => '0',
			 '2:00:00 AM' => '0',
			 '3:00:00 AM' => '0',
			 '4:00:00 AM' => '0',
			 '5:00:00 AM' => '0',
			 '6:00:00 AM' => '0',
			 '6:30:00 AM' => '0',
			 '7:00:00 AM' => '0',
			 '8:00:00 AM' => '0',
			 '9:00:00 AM' => '0',
			 '9:30:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
		  );
		  $HolidateStart = array(
			 '2:00:00 PM' => '8',
			 '3:00:00 PM' => '8',
			 '4:00:00 PM' => '7',
			 '5:00:00 PM' => '6',
			 '6:00:00 PM' => '5.25',
			 '7:00:00 PM' => '4.75',
			 '8:00:00 PM' => '3.75',
			 '9:00:00 PM' => '3',
			 '9:30:00 PM' => '2.5',
			 '10:00:00 PM' => '2',
			 '10:15:00 PM' => '1.75',
			 '10:30:00 PM' => '1.5',
			 '11:00:00 PM' => '1',
			 '12:00:00 AM' => '8',
			 '01:00:00 AM' => '8',
			 '02:00:00 AM' => '8',
			 '03:00:00 AM' => '8',
			 '04:00:00 AM' => '8',
			 '05:00:00 AM' => '8',
			 '06:00:00 AM' => '8',
			 '07:00:00 AM' => '8',
			 '08:00:00 AM' => '8',
			 '09:00:00 AM' => '8',
			 '09:30:00 AM' => '8',
			 '10:00:00 AM' => '8',
			 '11:00:00 AM' => '8',
			 '12:00:00 PM' => '8',
			 '01:00:00 PM' => '8',
		  );
		   $NDHolidateStart= array(
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
			 '2:00:00 PM' => '1',
			 '3:00:00 PM' => '2',
			 '4:00:00 PM' => '1.75',
			 '5:00:00 PM' => '1.75',
			 '6:00:00 PM' => '1.5',
			 '7:00:00 PM' => '1.5',
			 '8:00:00 PM' => '1.75',
			 '9:00:00 PM' => '1.75',
			 '9:30:00 PM' => '1.75',
			 '10:00:00 PM' => '2',
			 '10:15:00 PM' => '1.75',
			 '10:30:00 PM' => '1.5',
			 '11:00:00 PM' => '1',
			 '12:00:00 AM' => '0',
			 '1:00:00 AM' => '0',
			 '2:00:00 AM' => '0',
			 '3:00:00 AM' => '0',
			 '4:00:00 AM' => '0',
			 '5:00:00 AM' => '0',
			 '6:00:00 AM' => '0',
			 '6:30:00 AM' => '0',
			 '7:00:00 AM' => '0',
			 '8:00:00 AM' => '0',
			 '9:00:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
			
		  );
		  	$NDHolidateEnd= array(
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
			 '2:00:00 PM' => '0',
			 '3:00:00 PM' => '0',
			 '4:00:00 PM' => '1',
			 '5:00:00 PM' => '2',
			 '6:00:00 PM' => '2.75',
			 '7:00:00 PM' => '3.75',
			 '8:00:00 PM' => '4.25',
			 '9:00:00 PM' => '5.25',
			 '9:30:00 PM' => '5.25',
			 '10:00:00 PM' => '5',
			 '10:15:00 PM' => '5',
			 '10:30:00 PM' => '5',
			 '11:00:00 PM' => '5',
			 '12:00:00 AM' => '5.25',
			 '1:00:00 AM' => '4.25',
			 '2:00:00 AM' => '3.75',
			 '3:00:00 AM' => '2.75',
			 '4:00:00 AM' => '2',
			 '5:00:00 AM' => '0',
			 '6:00:00 AM' => '0',
			 '6:30:00 AM' => '0',
			 '7:00:00 AM' => '0',
			 '8:00:00 AM' => '0',
			 '9:00:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
		  );  
		// $query = $this->db->query("select b.apid,fname,lname,emp_id from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes' and b.emp_id in (".$employz.")");
		// $query = $this->db->query("select b.apid,fname,lname,emp_id from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes' and b.emp_id in (select group_concat(emp_id) from tbl_emp_promote where emp_promoteID in (".$employz."))");
		// $query = $this->db->query("select b.apid,fname,lname,b.emp_id from tbl_applicant a,tbl_employee b,tbl_emp_promote c where a.apid=b.apid and b.isActive='yes' and b.emp_id =c.emp_id and c.emp_promoteID in (".$employz.")");
		$query = $this->db->query("select b.apid,fname,lname,b.emp_id,daterange,transact_date from tbl_applicant a,tbl_employee b,tbl_emp_promote c,tbl_payroll d,tbl_payroll_coverage e where a.apid=b.apid and d.emp_promoteId = c.emp_promoteId and e.coverage_id =d.coverage_id and b.isActive='yes' and b.emp_id =c.emp_id and c.emp_promoteID in (".$employz.")");
			foreach ($query->result() as $row)
			{
				$DateRangeShift = $this->DateRangeShift($date);
				$empDeductAdjustment2 = $this->empDeductAdjustment($row->emp_id);
 					foreach($DateRangeShift as $row1 => $data){
 							$empLogs2 = $this->emplogs($data,$row->emp_id);
							
							 foreach($empLogs2 as $row2 => $data2){	
									$emplogsOut2 = $this->emplogsOut($data2->dtr_id,$row->emp_id);
									$emplogsShift2 = $this->emplogsShift($data2->acc_time_id,$row->emp_id);
									$empRate2 = $this->empRate($row->emp_id);
									$SSSCompute2 = $this->SSSCompute($row->emp_id);
									$PhilHealthCompute2 = $this->PhilHealthCompute($row->emp_id);
									$adjustSalary2 = $this->adjustSalary($row->emp_id,$payrollC);
									$deductSalary2 = $this->deductSalary($row->emp_id,$payrollC);
										// $NightDiff2 = $this->NightDiff($data4->time_start);
									foreach($emplogsOut2 as $row3 => $data3){	
										foreach($emplogsShift2 as $row4 => $data4){
												   $shiftFrom = date("H:i:s", strtotime($data4->time_start));
													$NDbegin = "10:10:00 pm";
													$NDend   = "6:00:00 am";

													$HPbegin = "12:00:00 am";
													$HPend   = "3:00:00 am";
													
													$HPbegin2 = "2:00:00 pm";
													$HPend2   = "11:59:59 pm";
													
													$HPbegin3 = "4:00:00 am";
													$HPend3   = "5:00:00 am";
													 
													$NDdate1 = DateTime::createFromFormat('H:i a', $data4->time_start);
													$NDdate2 = DateTime::createFromFormat('H:i a', $NDbegin);
													$NDdate3 = DateTime::createFromFormat('H:i a', $NDend);

													$HPdate1 = DateTime::createFromFormat('H:i a', $data4->time_start);
													$HPdate2 = DateTime::createFromFormat('H:i a', $HPbegin);
													$HPdate3 = DateTime::createFromFormat('H:i a', $HPend);
													
													$HPdate4 = DateTime::createFromFormat('H:i a', $HPbegin2);
													$HPdate5 = DateTime::createFromFormat('H:i a', $HPend2);
													
													$HPdate6 = DateTime::createFromFormat('H:i a', $HPbegin3);
													$HPdate7 = DateTime::createFromFormat('H:i a', $HPend3);
													if ($NDdate1 >= $NDdate2 or $NDdate1 <= $NDdate3){
													   $ndRemark = "Y";
													}else{
													   $ndRemark= "N";
													}
													/* if ($HPdate1 >= $HPdate2 and $HPdate1 <= $HPdate3){
													   $hpRemark = "Y";
													   $hpRate = '0.10';
													}else{
													   $hpRemark= "N";
													   $hpRate = '';
													} */
												if (($HPdate1 >= $HPdate4 and $HPdate1 <= $HPdate5) or ($HPdate1 >= $HPdate6 and $HPdate1 <= $HPdate7) ){
																$hpRemark = "Y";
																$hpRate = '0.08';
												}else if(($HPdate1 >= $HPdate2) and ($HPdate1 <= $HPdate3)){
															   $hpRemark = "Y";
																$hpRate = '0.10';
												}else{
																$hpRemark= "N";
																 $hpRate = '';	
												}
													 
													 
											$to_time= strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login)));
												$BCTSched = date('H:i:s', strtotime('-10 mins', strtotime($data4->time_start)));
												
 											 if($data4->time_start == "12:00:00 AM"){
												 if(date('h:i:s a', strtotime($data2->login))>date('h:i:s a', strtotime("12:00:00 am"))){
														 $data= date('Y-m-d', strtotime('-1 day', strtotime($data)));
														 $from_time = strtotime($data." 24:00:00");
														$res = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
												 }else{
														$from_time = strtotime($data." 24:00:00");
														$res = round(abs($from_time - $to_time) / 60,2)." mins. early";
												 }												 
											 }else{
												  if(strtotime(date('h:i:s a', strtotime($data2->login)))>strtotime(date('h:i:s a', strtotime($data4->time_start)))){
													$from_time = strtotime($data." ".$shiftFrom);
														$res = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
												  }else{
														$from_time = strtotime($data." ".$shiftFrom);
														$res = round(abs($from_time - $to_time) / 60,2)." mins. early";

												  }
														 
											 }
												$bct = ($data." ".$BCTSched.":00" > $data2->login ) ? "Q" : "DQ";
												$from_timeLog = strtotime($data2->login);
 												$to_timeLog = strtotime($data3->logout);
												$resLog = round(abs(($to_timeLog-$from_timeLog)/3600),2);
												

												foreach($empRate2 as $row5 => $data5){
												$totalSSSPiPh =0;
												$empPosAllowance2 = $this->empPosAllowance($data5->posempstat_id);
												$empPosBonus2 = $this->empPosBonus($data5->posempstat_id);
												$totalSSSPiPh=	$PhilHealthCompute2[0]->employeeshare +$SSSCompute2[0]->sss_employee;
												$BIRCompute2 = $this->BIRCompute($row->emp_id,$data5->rate,$totalSSSPiPh);
												$BIRCompute3 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->deduction : '0';
												$BIRCompute4 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->percent : '0';
												$BIRCompute5 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->salarybase : '0';
												$BIRCompute6 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->description : 'not set';
												$adjustSalary1 = (count($adjustSalary2)>0) ? $adjustSalary2[0]->adjust : 0;
												$deductSalary1 = (count($deductSalary2)>0) ? $deductSalary2[0]->deduct : 0;
												$empPosAllowance1 = (count($empPosAllowance2)>0) ? $empPosAllowance2[0]->allowance : 0;
												$empPosBonus1 = (count($empPosBonus2)>0) ? $empPosBonus2[0]->bonus : 0;
												$empDeductAdjustmentFinal = (count($empDeductAdjustment2)>0) ? $empDeductAdjustment2[0]->adj_amount : 0;
												$empDeductAdjustmentFinal_ID = (count($empDeductAdjustment2)>0) ? $empDeductAdjustment2[0]->pemp_adjust_id : 0;
												$deductionname = (count($empDeductAdjustment2)>0) ? $empDeductAdjustment2[0]->deductionname : "";

											$isLogoutHoliday2 = $this->isLogoutHoliday($data3->logout,$data4->time_end);
												$isLoginHoliday2 = $this->isLoginHoliday($data2->login,$data4->time_start);
												  $TotalAmountHoliday1=0;
												  $TotalAmountHoliday2=0;	
												  $hrly=0;	
												  $HolidayND=0;	
												  $HolidayHP=0;	
												  	  $NDComputeHolidayLogout=0;	
		  $HPComputeHolidayLogout=0;	
		  $NDComputeHolidayLogin=0;	
		  $HPComputeHolidayLogin=0;	
		  $HolidayType1 =0;
		  $HolidayType2=0;
				   if(count($isLogoutHoliday2 )>0){
					  
						$HolidayType = ($isLogoutHoliday2[0]->type=='Regular') ? 1 : .30;
						if(array_key_exists($data4->time_start,$HolidateSEnd)){
							$isLogoutHoliday1  =  $HolidateSEnd[$data4->time_start];
							$isLogoutHolidayND1  =  $NDHolidateEnd[$data4->time_start];
							$TotalAmountHoliday1 = (((($data5->rate*12)/261)/8)*$HolidayType)*$isLogoutHoliday1; 
								$hrly =$HolidayType;
							$NDComputeHolidayLogout = ($isLogoutHoliday2[0]->type=='Regular') ? $isLogoutHolidayND1 : ($isLogoutHolidayND1 *.30);
							$HPComputeHolidayLogout = ($isLogoutHoliday2[0]->type=='Regular') ? $isLogoutHolidayND1 : ($isLogoutHolidayND1 *.30);
							$HolidayType2 =  $isLogoutHoliday2[0]->type;
 
								
						}else{
							$isLogoutHoliday1  =  0 ;
							
						}

				}else{
						$isLogoutHoliday1  =  0;
		  
				}
				if(count($isLoginHoliday2 )>0){
						$HolidayType = ($isLoginHoliday2[0]->type=='Regular') ? 1 : .30;
					if(array_key_exists($data4->time_start,$HolidateStart)){
						$isLoginHoliday1  =  $HolidateStart[$data4->time_start];
						$isLoginHolidayND1  =  $NDHolidateStart[$data4->time_start];
						$TotalAmountHoliday2 = (((($data5->rate*12)/261)/8)*$HolidayType)*$isLoginHoliday1;  
						 $NDComputeHolidayLogin = ($isLoginHoliday2[0]->type=='Regular') ? $isLoginHolidayND1 : ($isLoginHolidayND1 *.30);
						$HPComputeHolidayLogin = ($isLoginHoliday2[0]->type=='Regular') ? $isLoginHolidayND1 : ($isLoginHolidayND1 * .30);
						$HolidayType1 =  $isLoginHoliday2[0]->type;

					}else{
						$isLoginHoliday1  =  0 ;
			
					}
				}else{
						$isLoginHoliday1  =  0;
		  
				}	 
 

													
																$arr['employee'][$row->emp_id][$data] = array(
																	"fname" => $row->lname.", ".$row->fname,
																	"date" => $data,
																	"shiftStart" => $data4->time_start,
																	"shiftEnd" => $data4->time_end,
																	"login" => date('Y-m-d h:i:s a', strtotime($data2->login)),
																	"nd" => $data4->nd,
																	"logout" =>  date('Y-m-d h:i:s a', strtotime($data3->logout)),
																	"late" =>  $res ,
																	"emp_promoteId" =>  $data5->emp_promoteId,
																	"bct" =>   $bct,
																	"total" =>   $resLog,
																	"rate" =>   $data5->rate,
																	"rate2" =>   (($data5->rate/2)-($totalSSSPiPh/2)-(($data5->rate/2)*.02)),
																	"pos_name" =>   $data5->pos_name,
																	"ndRemark" =>   $ndRemark,
																	"hpRemark" =>   $hpRemark,
																	"hpRate" =>   $hpRate,
																	"empPosAllowance" =>   $empPosAllowance1,
																	"empPosBonus" =>   $empPosBonus1,
																	"sss_id" =>   $SSSCompute2[0]->sss_id,
																	"deductionname" =>  $deductionname,
																	"payroll_adjDeduct_amount" =>   $empDeductAdjustmentFinal,
																	"pemp_adjust_id" =>   $empDeductAdjustmentFinal_ID,
																	"SSS" =>   $SSSCompute2[0]->sss_employee,
																	"philhealth_id" =>   $PhilHealthCompute2[0]->philhealth_id,
																	"PhilHealthCompute" =>   $PhilHealthCompute2[0]->employeeshare,
																	"tax_id" =>   $BIRCompute2[0]->tax_id,
																	"BIRCompute" =>  $BIRCompute3,
																	"BIRPercent" =>  $BIRCompute4,
																	"BIRSalaryBase" =>  $BIRCompute5,
																	"BIRDescription" =>  $BIRCompute6,
																	"adjustSalary" =>  $adjustSalary1,
																	"deductSalary" =>  $deductSalary1,
																	"isHoliday" =>    $isLogoutHoliday1+ $isLoginHoliday1,
																	"isHoliday1" =>    $isLogoutHoliday1,
																	"isHoliday2" =>   $isLoginHoliday1,
																	"hrly" =>   $hrly,
																	"TotalAmountHoliday" =>   number_format($TotalAmountHoliday1+$TotalAmountHoliday2,2),
																	"TotalAmountHoliday1" =>   number_format($TotalAmountHoliday1,2),
																	"TotalAmountHoliday2" =>   number_format($TotalAmountHoliday2,2),
																   "HPComputeHolidayLogin" =>   $HPComputeHolidayLogin,
																  "NDComputeHolidayLogin" =>   $NDComputeHolidayLogin,
																  "NDComputeHolidayLogout" =>   $NDComputeHolidayLogout,
																  "HPComputeHolidayLogout" =>   $HPComputeHolidayLogout,
																  "HolidayType1" =>   $HolidayType1,
																  "HolidayType2" =>   $HolidayType2,
																  "daterange" =>    $row->daterange,
																  "transact_date" =>   $row->transact_date,
																);
															 
												  
												$totalSSSPiPh =0;
											}
										}
									}
								}
						}
				} 
				if(count($arr)>0){
					echo json_encode($arr);
				 }else{
					 $arr = 0;
					echo json_encode($arr);

				 }
  			     
  			    // echo json_encode($arr);
 		}
		 public function DateRangeShift($date){
		$strDateFrom = date("Y-m-d", strtotime($date[0]));
		$strDateTo = date("Y-m-d", strtotime($date[1]));
		$aryRange=array();
		$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
			if($iDateTo>=$iDateFrom){
				array_push($aryRange,date('Y-m-d',$iDateFrom));
				 while($iDateFrom<$iDateTo){
					$iDateFrom+=86400; // add 24 hours
						array_push($aryRange,date('Y-m-d',$iDateFrom));
					}
			}
			 return $aryRange;
			 		
		}
		 
		  public function emplogs($date,$empid){
		// public function emplogs(){
				// $date = '2017-04-05';
				// $empid = 350;
			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and log like '%".$date."%' and emp_id=".$empid."");
				
			  return $query->result();
			// echo json_encode($query->result());
 			 
 		}
		public function emplogsOut($dtrid,$empid){
 
			$query = $this->db->query("select dtr_id,emp_id,log as logout from tbl_dtr_logs where entry='O' and type='DTR' and note=".$dtrid." and emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function emplogsShift($acc_time_id,$empid){
 
			// $query = $this->db->query("select time_start,time_end from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.acc_time_id=".$acc_time_id." and emp_id=".$empid."");
			$query = $this->db->query("select distinct(c.time_id),time_start,time_end,(select group_concat(concat(hour,'-',minutes))  from tbl_payroll_nd as s where s.time_id=c.time_id) as nd from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.acc_time_id=".$acc_time_id."  and emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function empRate($empid){
 
			$query = $this->db->query("select b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,c.posempstat_id,emp_promoteId from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 and b.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function empDeductAdjustment($empid){
 
			$query = $this->db->query("select pemp_adjust_id,adj_amount,deductionname from tbl_payroll_emp_adjustment a,tbl_payroll_deductions b where a.pdeduct_id=b.pdeduct_id and emp_id= ".$empid." and a.isActive=1");
				
			 return $query->result();
 			 
 		}
	 
		public function empPosAllowance($posempstatid){
 
			$query = $this->db->query("select pos_name,d.status,group_concat(concat(allowance_name,'=',value/2)) as allowance from tbl_allowance a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id  and b.empstat_id=d.empstat_id and b.posempstat_id =".$posempstatid." group by b.posempstat_id");
				
			 return $query->result();
 			 
 		}
		public function empPosBonus($posempstatid){
 
			$query = $this->db->query("select pos_name,d.status,group_concat(concat(bonus_name,'=',amount/2)) as bonus from tbl_pos_bonus a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_bonus e where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and e.bonus_id = a.bonus_id and b.posempstat_id =".$posempstatid." group by b.posempstat_id ");
				
			 return $query->result();
 			 
 		}
		public function NightDiff($nd){
 			$query = $this->db->query("select time_start,time_end,hour,minutes from tbl_payroll_nd a,tbl_time b where a.time_id=b.time_id and a.isActive=1 and b.time_start ='".$nd."'");
				
			 return $query->result();
  		}
		public function SSSCompute($empid){
 			// $query = $this->db->query("select sss_employee from (select rate from tbl_rate a,tbl_position b,tbl_employee c,tbl_applicant d where a.pos_id=b.pos_id and c.apid=d.apid and b.pos_id =d.pos_id and a.isActive=1 and c.isActive='yes' and c.emp_id=".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
 			  $query = $this->db->query("select sss_employee,sss_id from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
				
			 return $query->result();
  		}
		public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare,philhealth_id from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		}
		   public function BIRCompute($empid,$rate,$totalSSSPiPh){
		  /*  public function BIRCompute(){
			   $empid=46;
			   $rate=10500;
			   $totalSSSPiPh=253.25; */
  			   $query = $this->db->query("select g.emp_id,tax_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".(($rate/2)-$totalSSSPiPh-(($rate/2)*.02))." order by salarybase DESC limit 1 ");
			  return $query->result();
			 // echo json_encode($query->result());
  			 
  		} 
		 
		 
		 public function BIRCompute2(){
			   $empid = 33;
			  $rate = 5000;
  			   $query = $this->db->query("select g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase <=".($rate/2)." order by salarybase DESC limit 1");
			 var_dump($query->result_array());
			 
  		} 
		
		 public function sendEmail(){
		 $config=array();
  $config['protocol']    = 'smtp';
  $config['smtp_host']    = 'smtp.gmail.com';
  $config['smtp_port'] = 587;
  $config['smtp_user']    = 'szdummysz@gmail.com';
  $config['smtp_pass']    = 'szdummy123';
  $config['charset']    = 'iso-8859-1';
  $config['smtp_crypto']    = 'tls';
  $config['mailtype']    = 'html';
  $config['validate']    = TRUE;
  $config['crlf']    = "\r\n";
  
  $this->email->initialize($config);
    $this->email->set_newline("\r\n");
   $this->email->from('szdummysz@gmail.com', 'Dummy');
  $this->email->to('nicca.arique@supportzebra.com');
  $this->email->cc('jeffrey@supportzebra.com');

  $this->email->subject('Kudos');
  $this->email->message('You still have pending kudos.');

  $this->email->send();
  $this->email->print_debugger([$include = array('headers', 'subject', 'body')]);
 }
		public function emailPayslip(){
			$fname = 'mark';
			$mname = 'roowts';
			$lname = 'asas';
				$this->email->initialize(array(
				  'protocol' => 'smtp',
				  'smtp_host' => 'smtp.gmail.com',
				  'smtp_user' => 'jeffrey@supportzebra.com',
				  'smtp_pass' => 'rakenzel2124',
				  'smtp_port' => 587,
				  'smtp_crypto' => 'tls',
				  'mailtype	' => 'html',
				  'validate' => TRUE,
				  'crlf' => "\r\n",
				  'charset' => "iso-8859-1",
				  'newline' => "\r\n"
				));
					$data = array(
					 'Name'=> $fname." ".$mname." ".$lname,
 						 );
				$attched_file= $_SERVER["DOCUMENT_ROOT"]."/supportzebra/reports/payroll/2017/October/2/agent/Payslip_Abao_October_2_484.pdf";
				$this->email->attach($attched_file);

				$this->email->from('jeffrey@supportzebra.com', 'jeffrey');
				$this->email->to('jeffrey@supportzebra.com');
 				$this->email->subject('SupportZebra - Payslip');
   				$this->email->message("Hi, <br> Please see attached file of your payslip. ");
				$this->email->send();

				echo $this->email->print_debugger();			 
  		}
		public function adjustSalary($empid,$payrollCover){
			$query = $this->db->query("select emp_id,group_concat(concat(additionname,'=',value)) as adjust from tbl_payroll_addition a,tbl_payroll_emp_addition b,tbl_payroll_coverage c where a.paddition_id=b.paddition_id and c.coverage_id=b.coverage_id and daterange ='".$payrollCover."' and emp_id=".$empid);
			 return $query->result();			  
  		}
		public function deductSalary($empid,$payrollCover){
			$query = $this->db->query("select emp_id,group_concat(concat(deductionname,'=',value)) as deduct from tbl_payroll_deductions a,tbl_payroll_coverage b, tbl_payroll_emp_deduction c where a.pdeduct_id=c.pdeduct_id and b.coverage_id=c.coverage_id and daterange ='".$payrollCover."' and c.emp_id= ".$empid);
			 return $query->result();			  
  		} 
		 	public function isLoginHoliday($login,$timeStart){
			$year = date_format(date_create($login),"Y");
			  $date = date_format(date_create($login),"Y-m-d");
			  $timeStart = date_format(date_create($timeStart),"h:i:s");
 			
	$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");

			return $query->result();
  		}
		public function isLogoutHoliday($logout,$timeEnd){
			  $year = date_format(date_create($logout),"Y");
			  $date = date_format(date_create($logout),"Y-m-d");
			  $timeEnd = date_format(date_create($timeEnd),"h:i:s");
 			
	$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
			return $query->result();
  		}
		
		public function exportexcel(){
		$objPHPExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$payrollC = $this->input->post('payrollC');
		$employz = $this->input->post('employz');
		$emp_type = $this->input->post('emp_type');
		$query = $this->db->query("SELECT fname,lname,email,c.emp_id,e.daterange,e.transact_date,g.pos_name,g.pos_details,h.status,d.*,b.isATM,(select password from tbl_user x where x.emp_id=c.emp_id) as password from tbl_applicant a,tbl_employee b, tbl_emp_promote c,tbl_payroll d,tbl_payroll_coverage e,tbl_pos_emp_stat f,tbl_position g,tbl_emp_stat h where a.apid=b.apid and b.emp_id=c.emp_id and c.emp_promoteId = d.emp_promoteId and f.posempstat_id = c.posempstat_id and f.pos_id =g.pos_id and h.empstat_id = f.empstat_id and d.coverage_id= e.coverage_id and c.emp_promoteID in (".$employz.") and e.coverage_id=".$payrollC."");
		$coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$payrollC);
		$year= $coverage->result()[0]->year;
		$month= $coverage->result()[0]->month; 
		$period= $coverage->result()[0]->period;
		$type= "agent";
				
			$sheet = $objPHPExcel->getActiveSheet(0);

			$j=0;
			$i=0;
			$jb=0;
			$ib=0;
		// for($i=0; $i<=count($arr); $i++){
						
				$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];

			$sheet->setShowGridlines(false);
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payslip");

			foreach($query->result() as $row){

				$sheet->getStyle($this->columnLetter($j+1))->applyFromArray($styleArray);
				$sheet->getStyle($this->columnLetter($j+4))->applyFromArray($styleArray);
  				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo'.$j);
				$objDrawing->setDescription('Logo'.$j);
				$objDrawing->setPath('./assets/images/logo2.png');
				$objDrawing->setHeight(50);
				$objDrawing->setCoordinates($this->columnLetter($j+2)."3");
				$objDrawing->setOffsetX(50);
				$objDrawing->setWorksheet($sheet);

				
 
				$clothing = explode("(",$row->clothing);
				$laundry = explode("(",$row->laundry);
				$rice = explode("(",$row->rice);
				$bir = explode("(",$row->bir_details);
				$total = 0;
				$deduct = 0;
				$takehome = 0;
				
				$sheet->setCellValueByColumnAndRow($j,7, "PAY STATEMENT - EMPLOYEES COPY");
				$sheet->setCellValueByColumnAndRow($j,8, "NAME");
				$sheet->setCellValueByColumnAndRow($j+1,8, $row->lname.", ".$row->fname);
				$sheet->setCellValueByColumnAndRow($j+2,8, "");
				$sheet->setCellValueByColumnAndRow($j+3,8, "POSITION");
				$sheet->setCellValueByColumnAndRow($j+4,8, $row->pos_name);
				
				$sheet->setCellValueByColumnAndRow($j,9, "DATE");
				$sheet->setCellValueByColumnAndRow($j+1,9, $row->transact_date);
				$sheet->setCellValueByColumnAndRow($j+2,9, "");
				$sheet->setCellValueByColumnAndRow($j+3,9, "TAX STATUS");
				$sheet->setCellValueByColumnAndRow($j+4,9, "--");

				$sheet->setCellValueByColumnAndRow($j,10, "PERIOD COVERED");
				$sheet->setCellValueByColumnAndRow($j+1,10, $row->daterange);
				$sheet->setCellValueByColumnAndRow($j+2,10, "");
				$sheet->setCellValueByColumnAndRow($j+3,10, "ATM");
				$sheet->setCellValueByColumnAndRow($j+4,10, ($row->isATM==1) ? "YES" : "NO");
				
				$sheet->setCellValueByColumnAndRow($j,12, "EARNINGS");
				$sheet->setCellValueByColumnAndRow($j+1,12, "SALARY");
				$sheet->setCellValueByColumnAndRow($j+2,12, " ");
				$sheet->setCellValueByColumnAndRow($j+3,12, trim($row->quinsina));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).'12:'.$this->columnLetter($j+3).'12');
				$total+= $row->quinsina;
				
				$sheet->setCellValueByColumnAndRow($j+1,13, "CLOTHING ALLOWANCE");
				$sheet->setCellValueByColumnAndRow($j+2,13, " ");
				$sheet->setCellValueByColumnAndRow($j+3,13, trim($clothing[0]));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).'13:'.$this->columnLetter($j+3).'13');
				$total+= $clothing[0];
				
				$sheet->setCellValueByColumnAndRow($j+1,14, "LAUNDRY ALLOWANCE");
				$sheet->setCellValueByColumnAndRow($j+2,14, " ");
				$sheet->setCellValueByColumnAndRow($j+3,14, trim($laundry[0]));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).'14:'.$this->columnLetter($j+3).'14');
				$total+= $laundry[0];
				
				$sheet->setCellValueByColumnAndRow($j+1,15, "RICE SUBSIDY");
				$sheet->setCellValueByColumnAndRow($j+2,15, " ");
				$sheet->setCellValueByColumnAndRow($j+3,15, trim($rice[0]));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).'15:'.$this->columnLetter($j+3).'15');
				$total+= $rice[0];
				
				$sheet->setCellValueByColumnAndRow($j+1,16, "NIGHT DIFFERENTIAL");
				$sheet->setCellValueByColumnAndRow($j+2,16, " ");
				$sheet->setCellValueByColumnAndRow($j+3,16, trim($row->nd));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).'16:'.$this->columnLetter($j+3).'16');
				$total+= $row->nd;

				$sheet->setCellValueByColumnAndRow($j+1,17, "OVERTIME");
				$sheet->setCellValueByColumnAndRow($j+2,17, " ");
				$sheet->setCellValueByColumnAndRow($j+3,17, trim($row->ot_bct));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).'17:'.$this->columnLetter($j+3).'17');
				$total+= $row->ot_bct;

				$sheet->setCellValueByColumnAndRow($j+1,18, "HAZARD PAY");
				$sheet->setCellValueByColumnAndRow($j+2,18, " ");
				$sheet->setCellValueByColumnAndRow($j+3,18, trim($row->hp));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).'18:'.$this->columnLetter($j+3).'18');
				$total+= $row->hp;

				$sheet->setCellValueByColumnAndRow($j+1,19, "HOLIDAY");
				$sheet->setCellValueByColumnAndRow($j+2,19, " ");
				$sheet->setCellValueByColumnAndRow($j+3,19, trim($row->ho));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).'19:'.$this->columnLetter($j+3).'19');
				$total+= $row->ho;


				  if(!empty($row->adjust_details)){
					 $adjusted = explode(",",$row->adjust_details);
					 $inc=20;

						for($i = 0; $i<count($adjusted); $i++){
								$ii = explode("|",$adjusted[$i]);
								$iii = explode("=",$ii[1]);

								if(trim($ii[0])!="Bonus"){
									$sheet->setCellValueByColumnAndRow($j+1,$inc,strtoupper($iii[0]));
									$sheet->setCellValueByColumnAndRow($j+2,$inc, " ");
									$sheet->setCellValueByColumnAndRow($j+3,$inc, trim($iii[1]));
									$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).''.$inc.':'.$this->columnLetter($j+3).''.$inc);
									$inc++;
									$total +=  $iii[1];

								}
							
							}
					} 
					
				$sheet->setCellValueByColumnAndRow($j+1,$inc, "TOTAL EARNINGS");
				$sheet->setCellValueByColumnAndRow($j+2,$inc, " ");
				$sheet->setCellValueByColumnAndRow($j+3,$inc, $total);
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).''.$inc.':'.$this->columnLetter($j+3).''.$inc);

				$sheet->getColumnDimension($this->columnLetter($j+2))->setAutoSize(true);
				
				$sheet->setCellValueByColumnAndRow($j,$inc, "DEDUCTIONS");
				$sheet->setCellValueByColumnAndRow($j+1,$inc, "WITHHOLDING TAX");
				$sheet->setCellValueByColumnAndRow($j+2,$inc, " ");
				$sheet->setCellValueByColumnAndRow($j+3,$inc, trim($bir[0]));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).''.$inc.':'.$this->columnLetter($j+3).''.$inc);
				$deduct+=$bir[0];

				$sheet->setCellValueByColumnAndRow($j+1,($inc+1), "SSS PREMIUM");
				$sheet->setCellValueByColumnAndRow($j+2,($inc+1), " ");
				$sheet->setCellValueByColumnAndRow($j+3,($inc+1), trim($row->sss_details));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).''.($inc+1).':'.$this->columnLetter($j+3).''.($inc+1));
				$deduct+=$row->sss_details;

				$sheet->setCellValueByColumnAndRow($j+1,($inc+2), "HDMF PREMIUM	");
				$sheet->setCellValueByColumnAndRow($j+2,($inc+2), " ");
				$sheet->setCellValueByColumnAndRow($j+3,($inc+2), trim($row->hdmf_details));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).''.($inc+2).':'.$this->columnLetter($j+3).''.($inc+2));
				$deduct+=$row->hdmf_details;
				
				$sheet->setCellValueByColumnAndRow($j+1,($inc+3), "PHILHEALTH");
				$sheet->setCellValueByColumnAndRow($j+2,($inc+3), " ");
				$sheet->setCellValueByColumnAndRow($j+3,($inc+3), trim($row->phic_details));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).''.($inc+3).':'.$this->columnLetter($j+3).''.($inc+3));
				$deduct+=$row->phic_details;

				$sheet->setCellValueByColumnAndRow($j+1,($inc+4), "LEAVE/UNDERTIME/OB/TARDINESS");
				$sheet->setCellValueByColumnAndRow($j+2,($inc+4), " ");
				$sheet->setCellValueByColumnAndRow($j+3,($inc+4), trim($row->absentLate));
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).''.($inc+4).':'.$this->columnLetter($j+3).''.($inc+4));
				$deduct+=$row->absentLate;

				  if(!empty($row->deduct_details)){
					 $deduct_details = explode(",",$row->deduct_details);
					 $inc2=$inc+5;

						for($i = 0; $i<count($deduct_details); $i++){
								$ii = explode("=",$deduct_details[$i]);
								$iii = explode("|",$ii[0]);

									$sheet->setCellValueByColumnAndRow($j+1,$inc2,strtoupper($iii[0]));
									$sheet->setCellValueByColumnAndRow($j+2,$inc2, " ");
									$sheet->setCellValueByColumnAndRow($j+3,$inc2, trim($ii[1]));
									$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).''.$inc2.':'.$this->columnLetter($j+3).''.$inc2);
									$inc2++;
									$deduct +=  $ii[1];

								 
							
							}
					}
				$sheet->setCellValueByColumnAndRow($j+1,$inc2, "Total Deductions");
				$sheet->setCellValueByColumnAndRow($j+2,$inc2, " ");
				$sheet->setCellValueByColumnAndRow($j+3,$inc2, $deduct);
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+2).''.$inc2.':'.$this->columnLetter($j+3).''.$inc2);
				
				$takehome =$total - $deduct;
				$sheet->setCellValueByColumnAndRow($j,($inc2+1), "TAKE HOME PAY	");
 				$sheet->setCellValueByColumnAndRow($j+3,($inc2+1), $takehome);
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+1).''.($inc2+1).':'.$this->columnLetter($j+3).''.($inc2+1));

				$sheet->setCellValueByColumnAndRow($j,($inc2+2), "SIGNATURE");
 				$sheet->setCellValueByColumnAndRow($j+3,($inc2+2), "DATE");
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+1).''.($inc2+2).':'.$this->columnLetter($j+3).''.($inc2+2));

				$sheet->setCellValueByColumnAndRow($j,($inc2+3), "Note: Keep this statement permanently for Income Tax purposes.");
 				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($j+1).''.($inc2+3).':'.$this->columnLetter($j+4).''.($inc2+3));

				$sheet->getColumnDimension($this->columnLetter($j+2))->setAutoSize(true);
				$sheet->getStyle($this->columnLetter($j+1).''.($inc2+3).':'.$this->columnLetter($j+4).''.($inc2+3))->applyFromArray($fontStyle);

				$total =0;
				$deduct =0;
				$takehome =0;
				$inc =0;
				$inc2 =0;

				$j+=6;
				$i++;
			 }
			 /*****START BONUS*****/
			 $objPHPExcel->createSheet();
			 $objPHPExcel->setActiveSheetIndex(1)->setTitle("Bonus");
			$sheet1 = $objPHPExcel->getActiveSheet();
			$sheet1->setShowGridlines(false);

			foreach($query->result() as $row){
				

				$sheet1->getStyle($this->columnLetter($jb+1))->applyFromArray($styleArray);
				$sheet1->getStyle($this->columnLetter($jb+4))->applyFromArray($styleArray);
  				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo'.$jb);
				$objDrawing->setDescription('Logo'.$jb);
				$objDrawing->setPath('./assets/images/logo2.png');
				$objDrawing->setHeight(50);
				$objDrawing->setCoordinates($this->columnLetter($jb+2)."3");
				$objDrawing->setOffsetX(50);
				$objDrawing->setWorksheet($sheet1);
 				
				$sheet1->setCellValueByColumnAndRow($jb,7, "BONUS STATEMENT - EMPLOYEES COPY");
				$sheet1->setCellValueByColumnAndRow($jb,8, "NAME");
				$sheet1->setCellValueByColumnAndRow($jb+1,8, $row->lname.", ".$row->fname);
				$sheet1->setCellValueByColumnAndRow($jb+2,8, "");
				$sheet1->setCellValueByColumnAndRow($jb+3,8, "POSITION");
				$sheet1->setCellValueByColumnAndRow($jb+4,8, $row->pos_name);
				
				$sheet1->setCellValueByColumnAndRow($jb,9, "DATE");
				$sheet1->setCellValueByColumnAndRow($jb+1,9, $row->transact_date);
				$sheet1->setCellValueByColumnAndRow($jb+2,9, "");
				$sheet1->setCellValueByColumnAndRow($jb+3,9, "TAX STATUS");
				$sheet1->setCellValueByColumnAndRow($jb+4,9, "--");

				$sheet1->setCellValueByColumnAndRow($jb,10, "PERIOD COVERED");
				$sheet1->setCellValueByColumnAndRow($jb+1,10, $row->daterange);
				$sheet1->setCellValueByColumnAndRow($jb+2,10, "");
				$sheet1->setCellValueByColumnAndRow($jb+3,10, "ATM");
				$sheet1->setCellValueByColumnAndRow($jb+4,10, ($row->isATM==1) ? "YES" : "NO");
				$inc=12;

				
					if(!empty($row->adjust_details)){
					$adjusted = explode(",",$row->adjust_details);
						for($i = 0; $i<count($adjusted); $i++){
						$ii = explode("|",$adjusted[$i]);
						$iii = explode("=",$ii[1]);

							if(trim($ii[0])=="Bonus"){
							$sheet1->setCellValueByColumnAndRow($jb+1,$inc,strtoupper($iii[0]));
							$sheet1->setCellValueByColumnAndRow($jb+2,$inc, " ");
							$sheet1->setCellValueByColumnAndRow($jb+3,$inc, trim($iii[1]));
							$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($jb+2).''.$inc.':'.$this->columnLetter($jb+3).''.$inc);
							$inc++;
		 
							}
						}
					}
					

					if(!empty($row->bonus_details)){
						// $sheet1->setCellValueByColumnAndRow($jb+1,$inc,$row->bonus_details);

 						$bonus_details = explode(",",$row->bonus_details);
						for($v = 0; $v<count($bonus_details); $v++){
						$vv = explode("=",$bonus_details[$v]);
 
						$sheet1->setCellValueByColumnAndRow($jb+1,$inc,strtoupper($vv[0]));
						$sheet1->setCellValueByColumnAndRow($jb+2,$inc, " ");
						$sheet1->setCellValueByColumnAndRow($jb+3,$inc, trim($vv[1]));
						$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($jb+2).''.$inc.':'.$this->columnLetter($jb+3).''.$inc);
						$inc++;
						}
					} 

				$sheet1->setCellValueByColumnAndRow($jb,($inc+2), "SIGNATURE");
 				$sheet1->setCellValueByColumnAndRow($jb+3,($inc+2), "DATE");
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($jb+1).''.($inc+2).':'.$this->columnLetter($jb+3).''.($inc+2));

				$sheet1->setCellValueByColumnAndRow($jb,($inc+3), "Note: Keep this statement permanently for Income Tax Purposes.");
 				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($jb+1).''.($inc+3).':'.$this->columnLetter($jb+4).''.($inc+3));

				$sheet1->getColumnDimension($this->columnLetter($jb+2))->setAutoSize(true);
				$sheet1->getStyle($this->columnLetter($jb+1).''.($inc+3).':'.$this->columnLetter($jb+4).''.($inc+3))->applyFromArray($fontStyle);

				$jb+=6;
				$ib++;
				 }
			 /*****END BONUS*****/

		// }
	  $file = 'Payslip_Report_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_u'.$this->session->userdata('uid').'.xlsx';
      $path= "reports/payroll/$year/$month/$period/report/".$file;
 
/* 	  $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
      $objPHPExcel->getActiveSheet()->getProtection()->setPassword('pazzword');
 */
	  header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");;
      header("Content-Disposition: attachment; filename=$file");
      header("Content-Transfer-Encoding: binary ");
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007'); 
	  $objWriter->save($path);
      ob_clean();
      return $path;
	  
		}
		/* public function exportexcel(){
		$payrollC = $this->input->post('payrollC');
		$employz = $this->input->post('employz');
		$emp_type = $this->input->post('emp_type');
		$query = $this->db->query("SELECT fname,lname,email,c.emp_id,e.daterange,e.transact_date,g.pos_name,g.pos_details,h.status,d.*,(select password from tbl_user x where x.emp_id=c.emp_id) as password from tbl_applicant a,tbl_employee b, tbl_emp_promote c,tbl_payroll d,tbl_payroll_coverage e,tbl_pos_emp_stat f,tbl_position g,tbl_emp_stat h where a.apid=b.apid and b.emp_id=c.emp_id and c.emp_promoteId = d.emp_promoteId and f.posempstat_id = c.posempstat_id and f.pos_id =g.pos_id and h.empstat_id = f.empstat_id and d.coverage_id= e.coverage_id and c.emp_promoteID in (".$employz.") and e.coverage_id=".$payrollC."");
		$coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$payrollC);
		$year= $coverage->result()[0]->year;
		$month= $coverage->result()[0]->month; 
		$period= $coverage->result()[0]->period;
		$type= "agent";


	$objPHPExcel = $this->phpexcel;
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setName('Logo');
	$objDrawing->setDescription('Logo');
	$objDrawing->setPath('./assets/images/logo2.png');
	$objDrawing->setHeight(100);
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
	$objPHPExcel->getActiveSheet()->getStyle("A1:D5")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
	$objPHPExcel->getActiveSheet()->getStyle("D1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('000000');
	$objPHPExcel->getActiveSheet()->getStyle("D3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('000000');
	$objPHPExcel->getActiveSheet()->setCellValue('D1','Downloaded by:')->getStyle('D1')->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
	$objPHPExcel->getActiveSheet()->setCellValue('D2',$this->session->userdata('fname')." ".$this->session->userdata('lname'))->getStyle('D1')->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
	$objPHPExcel->getActiveSheet()->setCellValue('D3','Downloaded on:')->getStyle('D3')->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
	$objPHPExcel->getActiveSheet()->mergeCells("D4:D5")->setCellValue('D4',date("Y-m-d")." ".date("h:i a"))->getStyle('D3')->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
   
	  $file = 'Payslip_Report_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_u'.$this->session->userdata('uid').'.xlsx';
      $path= "reports/payroll/$year/$month/$period/report/".$file;
 
	  $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
      $objPHPExcel->getActiveSheet()->getProtection()->setPassword('pazzword');

	  header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");;
      header("Content-Disposition: attachment; filename=$file");
      header("Content-Transfer-Encoding: binary ");
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007'); 
	  $objWriter->save($path);
      ob_clean();
      return $path;
	} */
function columnLetter($c){

    $c = intval($c);
    if ($c <= 0) return '';

    $letter = '';
             
    while($c != 0){
       $p = ($c - 1) % 26;
       $c = intval(($c - $p) / 26);
       $letter = chr(65 + $p) . $letter;
    }
    
    return $letter;
        
}
 
}