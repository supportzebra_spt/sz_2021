<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule_new extends CI_Controller {

   // public $logo = "C:\\wamp64\\www\\supportzebra2\\assets\\images\\logo2.png";
    public $logo = "/usr/local/www/apache24/data/supportzebra/assets/images/logo2.png";
    // public $file= "C:\\xampp\\htdocs\\supportzebra\\reports\\schedule.xlsx";
    //public $file= "/usr/local/www/apache24/data/supportzebra/reports/schedule.xlsx";
    public $file = "";

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
			if ($this->session->userdata('uid')) {
				$this->load->model('ScheduleModel');
				//  $this->file = "C:\\wamp64\\www\\supportzebra2\\reports\\" . $this->session->userdata('emp_id') . "\\schedule.xlsx";
					$this->file = "/usr/local/www/apache24/data/supportzebra/reports/". $this->session->userdata('emp_id') . "/schedule.xlsx";
			} else {
				redirect('index.php/login');
			}
       
    }
    
	public function insertsched() { 
        
        $emp_id = $this->input->post('emp_id');
       
        $leaveType_id = $this->input->post('leaveType');
        $schedtype_id = $this->input->post('schedType');
		$acc_time_id = ($schedtype_id==1 || $schedtype_id==4 || $schedtype_id==5) ? $this->input->post('acc_time_id') : null;	
			$scheddate = $this->input->post('scheddate');
			$date = date_create_from_format("l - M d, Y", $scheddate);
			$scheddate = date_format($date, "Y-m-d");
        $row = $this->ScheduleModel->insertsched($leaveType_id, $acc_time_id, $schedtype_id, $scheddate, $emp_id);
        // echo $row;
        if ($row > 0) {
            echo json_encode($this->ScheduleModel->getSchedofEmp($scheddate, $scheddate, $emp_id));
        } else {
            echo 'Failed';
        }
    }
	public function updatesched() {
        $schedid = $this->input->post('schedid');
        $schedtype_id = $this->input->post('schedtypid');
		$acc_time_id = ($schedtype_id==1 || $schedtype_id==4 || $schedtype_id==5) ? $this->input->post('acc_time_id') : null;	
        $emp_id = $this->input->post('emp_id');
         $leave_id = $this->input->post('leavetypeid');
         $row = $this->ScheduleModel->updatesched($leave_id, $schedid, $acc_time_id, $schedtype_id, $emp_id, $this->session->userdata('uid'));
        if ($row > 0) {
            echo json_encode($this->ScheduleModel->getSchedofEmp($scheddate, $scheddate, $emp_id));
        } else {
			 echo 'Failed';
			//echo $row;
			// echo $leave_id." <==> ".$schedid." <==> ".$acc_time_id." <==> ".$schedtype_id." <==> ".$emp_id;

        }
    }
}
