<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payrolladmintrainee extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	
	public function index($stat=null,$pid=null){
		$status = explode("-",$stat);
			$st1 = ($status[0]=="agent") ? "Agent" : "Admin";
			$st2 = ($status[1]=="trainee") ? "Daily" : "Monthly";
 				/* $query = $this->db->query(" select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='Admin' and d.isActive=1 and b.salarymode='$st2'  and e.pos_name!='NR' and e.pos_details not like '%Vogmsi%' order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$pid); */
				$query = $this->db->query("Select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='Admin' and (g.status!='Part-time') and confidential=0 and b.isActive='yes' and d.isActive=1 and b.salarymode='$st2' and e.pos_details!='SMT Vogmsi' order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$pid);

 
		foreach ($query->result() as $row)
			{
				$arr['employee'][$row->apid] = array(
				"apid" => $row->apid,
 				"fname" => $row->fname,
 				"lname" => $row->lname,
 				"emp_id" => $row->emp_id,
 				"payroll_id" => $row->payroll_id,
 				"emp_promoteId" => $row->emp_promoteId,
  				);
			}
	if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='GeneratePayroll/admin-trainee' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
					if($pid>0){
						$per_id =  $this->db->query("select coverage_id,daterange,month,year,period as period2 from tbl_payroll_coverage where coverage_id=".$pid."");
					foreach ($per_id->result() as $row)
					{
							$arr['period'] = array(
							"daterange" => $row->daterange,
							"coverage_id" => $row->coverage_id,
							"year" => $row->year,
							"month" => $row->month,
							"period" => $row->period2,
							
							);
					}
								$arr["stat"] = array("status" => $stat);					$arr['pid'] = $pid;

					$this->load->view('payrolladmintrainees',$arr);
				}else{
					
				}
			}else{
				redirect('index.php/home/error');
			}
		}else{
					redirect('index.php/login');
		}
	
	
		}
		
	public function emptype($type){
		if($type==1){
			$payrollC = $this->input->post('payrollC');
			$employz = $this->input->post('employz');

		}else{
		$payrollC = $this->input->post('pc');
 		 $employz = $this->input->post('emp_id');

		}
		 $pid = $this->input->post('pid');
		  $date = explode("-",$payrollC);
  		$query = $this->db->query("select b.apid,fname,lname,salarymode,emp_id,(select concat(acc_id,'|',acc_name,'|',acc_description) from tbl_account where acc_id = b.acc_id) as account from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes' and b.emp_id in (".$employz.") order by lname ASC");
		  foreach ($query->result() as $row){
			$DateRangeShift = $this->DateRangeShift($date);
			$empRate2 = $this->empRate($row->emp_id);
			$empDeductAdjustment2 = $this->empDeductAdjustment($row->emp_id,$pid);
			$rate = ($empRate2[0]->dep=='SMT') ? (($empRate2[0]->rate*313)/12) : (($empRate2[0]->rate*261)/12);
			$rateDaily = $empRate2[0]->rate;

				foreach($DateRangeShift as $row1 => $data){
 					$shift = $this->empshift($row->emp_id,$data);
						 
						$SSSCompute2 = $this->SSSCompute($row->emp_id,$empRate2[0]->dep);
						$PhilHealthCompute2 = $this->PhilHealthCompute($row->emp_id);
						$adjustSalary2 = $this->adjustSalary($row->emp_id,$payrollC);
						$deductSalary2 = $this->deductSalary($row->emp_id,$payrollC);
							$totalSSSPiPh =	(($PhilHealthCompute2[0]->employeeshare/2) + ($SSSCompute2[0]->sss_employee/2) + (($rate/2)*.02));
								$BIRCompute2 = $this->BIRCompute($row->emp_id,$rate,$totalSSSPiPh);
									$BIRCompute3 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->deduction : '0';
									$BIRCompute4 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->percent: '0';
									$BIRCompute5 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->salarybase : '0';
									$BIRCompute6 = (count($BIRCompute2)>0) ? $BIRCompute2[0]->description : 'not set';
					foreach($shift as $row2 => $data2){
						$in = $this->empshiftin($row->emp_id,$data2->sched_id);
						$out = $this->empshiftout($row->emp_id,$data2->sched_id);
						$emplogsShift2 = $this->emplogsShift($data2->sched_id,$row->emp_id);
 							$empPosAllowance2 = $this->empPosAllowance($empRate2[0]->posempstat_id);
							$empPosAllowance1 = (count($empPosAllowance2)>0) ? $empPosAllowance2[0]->allowance : 0;
							$empPosBonus2 = $this->empPosBonus($empRate2[0]->posempstat_id);
							$empPosBonus1 = (count($empPosBonus2)>0) ? $empPosBonus2[0]->bonus : 0;

						$timeschedule = $this->timeschedule($data2->sched_id);
						$NDbegin = "10:10:00 PM";
							$NDend   = "6:00:00 AM";
							
							$HPbegin = "12:00:00 AM";
							$HPend   = "3:00:00 AM";
							
							$HPbegin2 = "2:00:00 PM";
							$HPend2   = "11:59:59 PM";
							
							$HPbegin3 = "4:00:00 AM";
							$HPend3   = "5:00:00 AM";
							
							
						if($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double" || $data2->type=="Double"){
						 
							if(count($in)>0){
									$shiftBreak=  $this->shiftBrk($timeschedule[0]->acc_time_id);

								   $shiftFrom = date("H:i:s", strtotime($timeschedule[0]->time_start));
							 
									 
									$NDdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
									$NDdate2 = DateTime::createFromFormat('H:i a', $NDbegin);
									$NDdate3 = DateTime::createFromFormat('H:i a', $NDend);

									$HPdate1 = DateTime::createFromFormat('H:i a', $timeschedule[0]->time_start);
									$HPdate2 = DateTime::createFromFormat('H:i a', $HPbegin);
									$HPdate3 = DateTime::createFromFormat('H:i a', $HPend);
									
									$HPdate4 = DateTime::createFromFormat('H:i a', $HPbegin2);
									$HPdate5 = DateTime::createFromFormat('H:i a', $HPend2);
									
									$HPdate6 = DateTime::createFromFormat('H:i a', $HPbegin3);
									$HPdate7 = DateTime::createFromFormat('H:i a', $HPend3);
									if ($NDdate1 >= $NDdate2 or $NDdate1 <= $NDdate3){
									   $ndRemark = "Y";
									}else{
									   $ndRemark= "N";
									}
 								if (($HPdate1 >= $HPdate4 and $HPdate1 <= $HPdate5) or ($HPdate1 >= $HPdate6 and $HPdate1 <= $HPdate7) ){
												$hpRemark = "Y";
												$hpRate = '0.08';
												
								}else if(($HPdate1 >= $HPdate2) and ($HPdate1 <= $HPdate3)){
											   $hpRemark = "Y";
												$hpRate = '0.10';
								}else{
												$hpRemark= "N";
												 $hpRate = '0.0';
								}
									 
									 
 								$to_time= strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login)));

								$BCTSched = date('H:i:s', strtotime('-10 mins', strtotime($timeschedule[0]->time_start)));
								$time_startNEWW  = ($timeschedule[0]->time_start  == "12:00:00 AM" ) ? "11:59:59 PM" : $timeschedule[0]->time_start;
								$ndTimeStarted = $data." ".$time_startNEWW;
	
							 if($timeschedule[0]->time_start == "12:00:00 AM"){
								if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) >= strtotime(date('Y-m-d H:i:00', strtotime($data." 24:00:00")) )){
 										 $from_time = strtotime($data." 24:00:00");
										 
										$res = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
										$late=(round(abs($from_time - $to_time) / 60,2));
 										$latemins = (round(abs($from_time - $to_time) / 60,2));
								 }else{
										$from_time = strtotime($data." 24:00:00");
										$res = round(abs($from_time - $to_time) / 60,2)." mins. early";
										$late=0;
										$latemins = 0;
								 }												 
							 }else{ 
								if(strtotime(date('Y-m-d H:i:00', strtotime($in[0]->login))) > strtotime(date('Y-m-d H:i:00', strtotime($data." ".$timeschedule[0]->time_start))) ){
									$from_time = strtotime($data." ".$shiftFrom);
										$res = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
										$late=(round(abs($from_time - $to_time) / 60,2));
										$latemins=(round(abs($from_time - $to_time) / 60,2));

								  }else{
										$from_time = strtotime($data." ".$shiftFrom);
										$res = round(abs($from_time - $to_time) / 60,2)." mins. early";
										$late=0;
										$latemins = 0;
								  }
										 
							 }
 								$bct = ($data." ".$BCTSched.":00" > date('Y-m-d H:i:00', strtotime($in[0]->login)) ) ? "Q" : "DQ";
								$absent = 0;
							}else{
								$timediff = "<span style='color:red'><b>Absent</b></span>";
								$bct = "DQ";
								$absent = 1;
								$latemins=0;
								$time_startNEWW=0;
								$ndTimeStarted=0;
 								$ndRemark = "N";
								$hpRemark= "N";
								$hpRate = '0.0';
							}
							if((count($emplogsShift2)>0)){
								$finalND = explode("-",$emplogsShift2[0]->nd); 
								$finalND2 = (count($emplogsShift2[0]->nd)>0 and !empty($emplogsShift2[0]->nd)) ? $finalND[0]+($finalND[1]/60) : 0; 
								// print_r($emplogsShift2);
							}else{
								$finalND2=0;
							}
						}else{
							 $timediff = "<span style='color:red'>".$data2->type."</span>";
							 $hpRemark= "N";
							    $hpRate = '0.0';	
							  	$empDeductAdjustmentFinal = 0;
							 $empDeductAdjustmentFinal_ID = 0;
							 $bct  = "DQ";
							 // $totalSSSPiPh =0;
							$ndRemark = "N";
							$late=0; 
							$latemins=0;
								$time_startNEWW=0;
								$ndTimeStarted=0;

							$finalND2=0;
							  if($empRate2[0]->status == "Probationary"){
									$absent = ($data2->type=="Leave") ? 1 : 0 ;
							  }else{
									$absent = 0;
								 }
						}
							
								$adjustSalary1 = (count($adjustSalary2)>0) ? $adjustSalary2[0]->adjust : 0;
								$deductSalary1 = (count($deductSalary2)>0) ? $deductSalary2[0]->deduct : 0;
 								$empDeductAdjustmentFinal = (count($empDeductAdjustment2)>0) ? $empDeductAdjustment2[0]->adj_amount : 0;
								$empDeductAdjustmentFinal_ID = (count($empDeductAdjustment2)>0) ? $empDeductAdjustment2[0]->pemp_adjust_id : 0;
 						$logouttTotalHours =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date_format(date_create($out[0]->logout),"Y-m-d")." ".$timeschedule[0]->time_end : $data." 00:00:00" : $data." 00:00:00";
						$logoutt =  (count($out)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $out[0]->logout : $data." 00:00:00") : $data." 00:00:00";
						
						$logouttNew =  (count($out)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? ((strtotime($out[0]->logout)> strtotime($logouttTotalHours) ) ? $logouttTotalHours :  $out[0]->logout) : $data." 00:00:00") : $data." 00:00:00";
 
						$logouttDTRID =  (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $out[0]->dtr_id : $data." 00:00:00" : $data." 00:00:00";
						// $loginn =  (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date_format(date_create($in[0]->login),"Y-m-d")." ".$timeschedule[0]->time_start : $data." 00:00:00" : $data." 00:00:00";
						 $loginn =  (count($in)>0) ? (($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $in[0]->login : $data." 00:00:00") : $data." 00:00:00";
						
							$loginnTotalHours =  (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? $data." ".$time_startNEWW  : $data." 00:00:00" : $data." 00:00:00";
 
							$loginnNew =  (count($in)>0) ? (($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? ((strtotime($in[0]->login)> strtotime($loginnTotalHours) ) ?  date('Y-m-d H:i:00 ', strtotime($in[0]->login))  : $loginnTotalHours) : $data." 00:00:00") : $data." 00:00:00";

						$loginnDTRID =  (count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $in[0]->dtr_id : $data." 00:00:00" : $data." 00:00:00";
						$logoutt2 =  ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_end : "00:00:00";
						$loginn2 =  ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_start : "00:00:00";
						$loginn2_timeID =  ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_id : "0";
							$breaks = $this->breakIdentify($loginnDTRID,$logouttDTRID,$row->emp_id);
							$loginn2timeID= $this->loginn2timeID($loginn2_timeID);
							$loginnNdtimeID= $this->loginnNdtimeID($loginn2_timeID);
							$shiftTotalHr= $this->shiftTotalHr($loginn2_timeID);

							$isLogoutHoliday2 = $this->isLogoutHoliday($logoutt,$logoutt2);
							$isLoginHoliday2 = $this->isLoginHoliday($loginn,$loginn2);
							 $TotalAmountHoliday1=0;
								$TotalAmountHoliday2=0;	
								$hrly=0;	
								$HolidayND=0;	
								$HolidayHP=0;	
								$NDComputeHolidayLogout=0;	
								$HPComputeHolidayLogout=0;	
								$NDComputeHolidayLogin=0;	
								$HPComputeHolidayLogin=0;	
								$HolidayType1 =0;
								$HolidayType2=0;
								$time_startNEWW=0;

					if(count($isLogoutHoliday2[0]->date)>0){
							// $HolidayType = ($isLogoutHoliday2[0]->type=='Regular') ? 1 : .30;
							$HolidayType =$isLogoutHoliday2[0]->calc;
						if(count($loginn2timeID)>0){
							if(count($in)>0){
							$isLogoutHoliday1  =  number_format(($loginn2timeID[0]->ho_end_time*$HolidayType),2);
  								 
							
							 $TotalAmountHoliday1 = ((($rate*12)/261)/8)*$isLogoutHoliday1;  
							$NDComputeHolidayLogout =  (isset($loginnNdtimeID[0]->nd_time_end)) ? number_format(($loginnNdtimeID[0]->nd_time_end*$HolidayType),2) : 0;
							$HPComputeHolidayLogout =  (isset($loginnNdtimeID[0]->nd_time_end)) ? number_format(($loginnNdtimeID[0]->nd_time_end*$HolidayType),2) : 0;
							$HolidayType2 =  $isLogoutHoliday2[0]->type;
							}else{
							$isLogoutHoliday1  =  0 ;
							}
						}else{
							$isLogoutHoliday1  =  0 ;
						}
					}else{
					 $isLogoutHoliday1  =  0; 
					}
					if(count($isLoginHoliday2[0]->date)>0){
						// $HolidayType = ($isLoginHoliday2[0]->type=='Regular') ? 1 : .30;
						$HolidayType = $isLoginHoliday2[0]->calc;

					if(count($loginn2timeID)>0){
						if(count($in)>0){
						$isLoginHoliday1  =  number_format(($loginn2timeID[0]->ho_start_time*$HolidayType),2);
						 $TotalAmountHoliday2 = ((($rate*12)/261)/8)*$isLoginHoliday1;  

						 $NDComputeHolidayLogin = (isset($loginnNdtimeID[0]->nd_time_start)) ? number_format(($loginnNdtimeID[0]->nd_time_start*$HolidayType),2)  : 0;
						$HPComputeHolidayLogin = (isset($loginnNdtimeID[0]->nd_time_start)) ? number_format(($loginnNdtimeID[0]->nd_time_start*$HolidayType),2) : 0;
						$HolidayType1 =  $isLoginHoliday2[0]->type;
						}else{
							$isLoginHoliday1  =  0;

						}
					}else{
						$isLoginHoliday1  =  0 ;
					}
				  }else{
					 $isLoginHoliday1  =  0;
				  }
				$shiftTotalHours = (count($shiftTotalHr)>0) ? $shiftTotalHr[0]->ho_end_time + $shiftTotalHr[0]->ho_start_time : 0;
					$from_timeLog1 = strtotime($loginn);
					$to_timeLog1 = strtotime($logoutt);
					$from_timeLog = strtotime($loginnTotalHours);
					$to_timeLog = strtotime($logouttTotalHours);
					$from_timeLog_NEW = strtotime($loginnNew);
					$to_timeLogNEW = strtotime($logouttNew);
				$resLog1 = round(abs(($to_timeLog1-$from_timeLog1)/3600),2);
				$resLog = round(abs(($to_timeLog-$from_timeLog)/3600),2);
				$resLogNEW = round(abs(($to_timeLogNEW-$from_timeLog_NEW)/3600),2);

					if($finalND2!=0){
					if ($resLog1<$shiftTotalHours){
						$start = strtotime($ndTimeStarted);
						$end = strtotime($logouttNew);
						$brk = (count($breaks)>0) ? (($breaks[0]->hr >=1) ? 30 : $breaks[0]->hr*60)+$breaks[0]->min : 0;
						$NDuT = round(abs(($end-$start)/3600),2); //ND if UT
						$NDuT -= $brk/60; //ND if UT
					}elseif($resLog1==$shiftTotalHours){
						$NDuT = $finalND2;

					}else{
						$NDuT = $finalND2;
					}
					}else{
							$NDuT = 0;
					}
						$arr["employee"][$row->emp_id][$data2->sched_id."-".$data2->sched_date]= array(
							"emp_id" => $row->emp_id,
							"salarymode" => $row->salarymode,
							"fname" => $row->lname.", ".$row->fname,
							"date" => $data,
							"shift_type" => $data2->type,
							"login" =>(count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date('Y-m-d h:i:s a', strtotime($in[0]->login)) : $data2->type : "No Log IN",
							"logout" => (count($out)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? date('Y-m-d h:i:s a', strtotime($out[0]->logout)) : $data2->type : "No Log OUT",
							"shiftStart" => ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_start : $data2->type,
							"shiftEnd" => ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ? $timeschedule[0]->time_end : $data2->type,
							// "late" => $timediff,
							"latecalc" => (isset($late)) ? ceil($late/60) : 0,
							"latecalcTEST" => (isset($late)) ? $late : 0,
							"latemins22" =>(count($in)>0) ? ($data2->type=="Normal" || $data2->type=="WORD" || $data2->type=="Double") ?  date('Y-m-d H:i:00', strtotime($in[0]->login))."  >= ".date('Y-m-d H:i:00', strtotime($data." 24:00:00")) : $data2->type : "No Log IN",
							"latemins" => $latemins/60,
							"bct" => $bct,
							"ndRemark" =>   $ndRemark, 
							"hpRemark" =>   $hpRemark,
							"hpRate" =>   $hpRate,
							"dayType" => $absent,
							"nd" =>  (count($emplogsShift2)>0) ? $NDuT : "0", 
 							"empPosAllowance" =>   $empPosAllowance1,
							"empPosBonus" =>   $empPosBonus1,
							"isHoliday" =>    $isLogoutHoliday1+ $isLoginHoliday1,
							"isHoliday1" =>    $isLoginHoliday1,
							"isHoliday2" =>    $isLogoutHoliday1,
							"calcHoliday1" =>    (isset($isLoginHoliday2[0]->calc)) ? $isLoginHoliday2[0]->calc : 0,
							"calcHoliday2" =>   (isset($isLogoutHoliday2[0]->calc)) ? $isLogoutHoliday2[0]->calc : 0,
							"TotalAmountHoliday" =>   number_format($TotalAmountHoliday1+$TotalAmountHoliday2,2, '.', ''),
							"TotalAmountHoliday1" =>   number_format($TotalAmountHoliday1,2, '.', ''),
							"TotalAmountHoliday2" =>   number_format($TotalAmountHoliday2,2, '.', ''),
							"HPComputeHolidayLogin" =>   $HPComputeHolidayLogin,
							"NDComputeHolidayLogin" =>   $NDComputeHolidayLogin,
							"NDComputeHolidayLogout" =>   $NDComputeHolidayLogout,
							"HPComputeHolidayLogout" =>   $HPComputeHolidayLogout,
							"HolidayType1" =>   $HolidayType1,
							"HolidayType2" =>   $HolidayType2,
							// "total" =>   ($resLog>0) ? $resLog : 1,
							"hrly" =>   $hrly,
							"rate" =>   $rate,
							"rateDaily" =>   $rateDaily,
							"emp_promoteId" =>  $empRate2[0]->emp_promoteId,
							"rate2" =>   (($rate/2)-($totalSSSPiPh)),
							"totalSSSPiPh" =>   $totalSSSPiPh,
							"pos_name" =>   $empRate2[0]->pos_name,
							"payroll_adjDeduct_amount" =>  $empDeductAdjustmentFinal ,
							"pemp_adjust_id" =>  $empDeductAdjustmentFinal_ID ,
							"sss_id" =>   $SSSCompute2[0]->sss_id,
							"SSS" =>   $SSSCompute2[0]->sss_employee,
							"philhealth_id" =>   $PhilHealthCompute2[0]->philhealth_id,
							"PhilHealthCompute" =>   $PhilHealthCompute2[0]->employeeshare,
							"tax_id" =>   (count($BIRCompute2) > 1) ? $BIRCompute2[0]->tax_id : 2,
							"BIRCompute" =>  $BIRCompute3,
							"BIRPercent" =>  $BIRCompute4,
							"BIRSalaryBase" =>  $BIRCompute5,
							"BIRDescription" =>  $BIRCompute6,
							"adjustSalary" =>  $adjustSalary1,
							"deductSalary" =>  $deductSalary1,
							"total" =>   ($resLog>0) ? $resLog : 0,
							"totalwork" =>   ($resLog1>0) ? $resLog1 : 0,
							"totalwork2" =>   ($resLog1>0) ? floor($resLog1) : 0,
							"breaks" =>   (count($breaks)>0) ? $breaks[0]->hr+$breaks[0]->min : 0,
							"account" =>   $row->account,
							"shiftTotalHours" =>   isset($shiftTotalHours) ? (($shiftTotalHours<0) ? 0:$shiftTotalHours ) : 0,
							"logouttNew" =>   $logouttNew,
							"loginnNew" =>   $loginnNew,
							"resLogNEW" =>   $resLogNEW,
							"shiftBreak" =>  ($data2->type=="Normal" || $data2->type=="WORD"  || $data2->type=="Double") ? ((count($in) >0) ? $shiftBreak[0]->hr + $shiftBreak[0]->min  : 0) : 0,   
 
						);
					}
				}
				$late=0;
			}
			echo json_encode($arr);
 		}
		public function empshift($empid,$date){
		$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and sched_date = '".$date."' and emp_id=".$empid);
			  return $query->result();
		 }
		 	 public function shiftBrk($acc_time_id){
		$query = $this->db->query("SELECT sum(hour*60) as hr, sum(min) as min FROM `tbl_shift_break` a,tbl_break_time_account b,tbl_break_time c where a.bta_id=b.bta_id and b.btime_id = c.btime_id and a.acc_time_id =$acc_time_id and a.isActive=1");
			  return $query->result();
		 }
		  public function breakIdentify($login,$logout,$empid){
		 		$query = $this->db->query("SELECT sum(hour) as hr,sum(min) as min FROM `tbl_dtr_logs` a,tbl_acc_time b,tbl_time c, tbl_break_time_account d, tbl_break e,tbl_break_time f where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and c.bta_id=d.bta_id and d.break_id=e.break_id and d.btime_id=f.btime_id and dtr_id between '".$login."' and '".$logout."' and emp_id=".$empid." and a.type='Break' and entry='O'");
			  return $query->result();

		 }
		  
		  public function empshiftin($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and sched_id=".$shift." and emp_id=".$empid);
			  return $query->result();
 		}
		  public function empshiftout($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as logout,acc_time_id from tbl_dtr_logs where entry='O' and type='DTR' and sched_id=".$shift." and emp_id=".$empid);
			  return $query->result();
 		}
		public function timeschedule($timeschedule){
			$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_acc_time b,tbl_time c  where a.acc_time_id = b.acc_time_id and b.time_id = c.time_id and sched_id=".$timeschedule);
			return $query->result();
		 }
		 public function DateRangeShift($date){
		$strDateFrom = date("Y-m-d", strtotime($date[0]));
		$strDateTo = date("Y-m-d", strtotime($date[1]));
		$aryRange=array();
		$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
			if($iDateTo>=$iDateFrom){
				array_push($aryRange,date('Y-m-d',$iDateFrom));
				 while($iDateFrom<$iDateTo){
					$iDateFrom+=86400; // add 24 hours
						array_push($aryRange,date('Y-m-d',$iDateFrom));
					}
			}
			 return $aryRange;
			 		
		}
	 
		 public function loan($id){
 
			$query = $this->db->query("select b.emp_id,a.fname,a.lname from tbl_applicant a,tbl_employee b,tbl_user c,tbl_emp_promote d where a.apid=b.apid and b.emp_id=d.emp_id and b.emp_id = c.emp_id and b.isActive='Yes' and c.isActive=1 and d.isActive=1");
				foreach ($query->result() as $row){
					$loanz = $this->loanList($row->emp_id);
					foreach ($loanz as $row1){
 						$loanz2 = $this->loanList2($row1->pemp_adjust_id);
						/*foreach ($loanz2 as $row12){*/
						if($loanz2[0]->cnt < $row1->count){
							$insert = $this->db->query("insert into tbl_payroll_deduct_child(coverage_id,emp_id,pemp_adjust_id,date_given) values($id,$row->emp_id,$row1->pemp_adjust_id,now())");

						}else{
							$update = $this->db->query("update tbl_payroll_emp_adjustment set isActive=0 where pemp_adjust_id=".$row1->pemp_adjust_id);

						}
					 
						// }
					}
				}
 
		 
		 }
		 
		 public function loanList($empid){
			$query = $this->db->query("SELECT * FROM `tbl_payroll_emp_adjustment` where isActive=1 and emp_id =".$empid);
			 return $query->result();

		 }
		 public function loanList2($pemp_adjust_id){
			$query = $this->db->query("SELECT count(*) as cnt FROM `tbl_payroll_deduct_child` where pemp_adjust_id =".$pemp_adjust_id);
			 return $query->result();

		 }
		 public function emplogs($date,$empid){
		$query = $this->db->query("SELECT * FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and sched_date = '".$date."' and emp_id=".$empid);
			  return $query->result();
		 }
 
		public function addDeductLonNew(){
				$emp_id = $this->input->post('emp_id');
				$pid = $this->input->post('pid');
				$nd = $this->input->post('nd');
			$query = $this->db->query("insert into tbl_payroll_deduct_child(coverage_id,emp_id,pemp_adjust_id,date_given) values($pid,$emp_id,$nd,now())");
			if($query){
				echo 1;
			}else{
				echo 0;
			}
		}
		public function addDeductNew(){
				$emp_id = $this->input->post('emp_id');
				$pid = $this->input->post('pid');
				$nd = $this->input->post('nd');
				$DeductVal = floatval($this->input->post('deductVal'));
			$query = $this->db->query("insert into tbl_payroll_emp_deduction(coverage_id,emp_id,pdeduct_id,value) values($pid,$emp_id,$nd,$DeductVal)");
			if($query){
				echo 1;
			}else{
				echo 0;
			}
		}
		public function addLoan(){
				$emp_id = $this->input->post('emp_id');
				$pid = $this->input->post('pid');
				$value = $this->input->post('value');
			// $query = $this->db->query("Select pemp_adjust_id,deductionname from tbl_payroll_emp_adjustment where pemp_adjust_id not  in (SELECT pemp_adjust_id FROM `tbl_payroll_deduct_child` where  coverage_id=$pid ) and  emp_id=$emp_id and isActive=1");
			$query = $this->db->query("Select pemp_adjust_id,deductionname,adj_amount from tbl_payroll_emp_adjustment a, tbl_payroll_deductions b where a.pdeduct_id=b.pdeduct_id and pemp_adjust_id not  in (SELECT pemp_adjust_id FROM `tbl_payroll_deduct_child` where coverage_id=$pid ) and  emp_id=$emp_id and a.isActive=1");
				echo "<option selected disabled>--</option>";
			foreach($query->result() as $row){
				echo "<option value=".$row->pemp_adjust_id.">".$row->deductionname." = P".($row->adj_amount/2)."</option>";
			}	
				
			}
		public function addOtherDeduct(){
  			$emp_id = $this->input->post('emp_id');
			$pid = $this->input->post('pid');
			$value = $this->input->post('value');
			$query = $this->db->query("select  pdeduct_id,deductionname from tbl_payroll_deductions where  pdeduct_id not in (select pdeduct_id from tbl_payroll_emp_deduction where coverage_id =$pid and emp_id=$emp_id)");
				echo "<option selected disabled>--</option>";

			foreach($query->result() as $row){
				echo "<option value=".$row->pdeduct_id.">".$row->deductionname."</option>";
			}
		}
		public function empDeductAdjustment($empid,$pid){
			// $query = $this->db->query("select pemp_adjust_id,adj_amount from tbl_payroll_emp_adjustment where emp_id= ".$empid." and isActive=1");
			 // $query = $this->db->query("select GROUP_CONCAT(pemp_adjust_id) as pemp_adjust_id ,GROUP_CONCAT(deductionname) as deductName,sum(adj_amount) as adj_amount from tbl_payroll_emp_adjustment a,tbl_payroll_deductions b where a.pdeduct_id=b.pdeduct_id and  emp_id= ".$empid."  and isActive=1");
			 $query = $this->db->query("select GROUP_CONCAT(a.pemp_adjust_id) as pemp_adjust_id ,GROUP_CONCAT(concat(deductionname,'-',adj_amount)) as deductName,sum(adj_amount) as adj_amount from tbl_payroll_emp_adjustment a,tbl_payroll_deductions b,tbl_payroll_deduct_child c where a.pdeduct_id=b.pdeduct_id and a.pemp_adjust_id = c.pemp_adjust_id and a.emp_id= ".$empid." and c.coverage_id=".$pid." and a.isActive=1");
			 return $query->result();
 		}
		public function emplogsOut($dtrid,$empid){
			$query = $this->db->query("select dtr_id,emp_id,log as logout from tbl_dtr_logs where entry='O' and type='DTR' and note=".$dtrid." and emp_id=".$empid."");
			 return $query->result();
 		}
			public function emplogsShift($sched_id,$empid){
 
			// $query = $this->db->query("select time_start,time_end from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.acc_time_id=".$acc_time_id." and emp_id=".$empid."");
			$query = $this->db->query("select distinct(c.time_id),time_start,time_end,(select group_concat(concat(hour,'-',minutes))  from tbl_payroll_nd as s where s.time_id=c.time_id) as nd from tbl_dtr_logs a,tbl_acc_time b,tbl_time c,tbl_schedule d where b.acc_time_id=d.acc_time_id and b.time_id=c.time_id and a.sched_id=d.sched_id and a.sched_id=".$sched_id."  and a.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function empRate($empid){
 
			$query = $this->db->query("select d.emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,c.posempstat_id,(select dep_name from tbl_pos_dep a,tbl_department b where a.dep_id=b.dep_id and pos_id=e.pos_id) as dep from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 and f.isActive=1 and b.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function NightDiff($nd){
 			$query = $this->db->query("select time_start,time_end,hour,minutes from tbl_payroll_nd a,tbl_time b where a.time_id=b.time_id and a.isActive=1 and b.time_start ='".$nd."'");
				
			 return $query->result();
  		}
		public function SSSCompute($empid,$dep){
			$com = ($dep=='SMT') ? 313 : 261 ;
 			  $query = $this->db->query("select sss_employee,sss_id from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and ((rate*$com)/12)  <=max_range and ((rate*$com)/12) >=min_range and c.isActive=1");
				
			 return $query->result();
  		}
		public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare,philhealth_id from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		}
		public function empPosAllowance($posempstatid){
 
			$query = $this->db->query("select pos_name,d.status,group_concat(concat(allowance_name,'=',value/2) order by allowance_name) as allowance from tbl_allowance a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id  and b.empstat_id=d.empstat_id and b.posempstat_id =".$posempstatid." and a.isActive=1 group by b.posempstat_id");
				
			 return $query->result();
 			 
 		}
		public function empPosBonus($posempstatid){
 
			$query = $this->db->query("select pos_name,d.status,group_concat(concat(bonus_name,'=',amount/2)) as bonus from tbl_pos_bonus a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_bonus e where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and e.bonus_id = a.bonus_id and b.posempstat_id =".$posempstatid." and a.isActive=1 and amount!=0 group by b.posempstat_id ");
				
			 return $query->result();
 			 
 		}
		     public function BIRCompute($empid,$rate,$totalSSSPiPh){
		 /*  public function BIRCompute(){
			   $empid=51;
			   $rate=10800;
			   $totalSSSPiPh=370.35; */
			  
			   
				if($totalSSSPiPh<=300){
					$base= (($rate/2)-500);
				}else{
					$base =  (($rate/2)-$totalSSSPiPh);
				}
   			   $query = $this->db->query("select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".$base." order by salarybase DESC limit 1 ");
			  return $query->result();
 			  //echo json_encode($query->result());
  		} 
 		  public function BIRComputeTest(){ //test Only
		 
			   $empid=100;
				$base = 5029.65;
				 
   			   $query = $this->db->query("select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".$base." order by salarybase DESC limit 1 ");
 
				echo "select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".$base." order by salarybase DESC limit 1 ";
 			 echo json_encode($query->result());
  		} 
 	  public function BIRCompute2(){
		 
			   $empid=484;
			   $rate=7500;
			   $totalSSSPiPh=186.25; 
  			   $tots = $totalSSSPiPh+(($rate/2)*.02);
  			   $query = $this->db->query("select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".(($rate/2)-$tots)." order by salarybase DESC limit 1 ");
			    echo "select tax_id,g.emp_id,salarybase,f.description,percent,deduction from tbl_tax_deduction a,tbl_tax_percent b,tbl_tax_type c,tbl_tax_effectivity d,tbl_tax e,tbl_tax_description f,tbl_tax_emp g where a.percent_id = b.percent_id and a.type_id=c.type_id and d.t_effec_id = a.t_effec_id and e.deduction_id = a.deduction_id and e.tax_desc_id = f.tax_desc_id and f.tax_desc_id=g.tax_desc_id and d.isActive=1 and g.emp_id=".$empid." and g.isActive=1 and salarybase<=".(($rate/2)-$tots)." order by salarybase DESC limit 1 ";
				echo "<br>";
			 var_dump($query->result_array());
			 
  		}   
		 
 	public function adjustSalary($empid,$payrollCover){
			// $query = $this->db->query("select emp_id,group_concat(concat(additionname,'=',value)) as adjust from tbl_payroll_addition a,tbl_payroll_emp_addition b,tbl_payroll_coverage c where a.paddition_id=b.paddition_id and c.coverage_id=b.coverage_id and daterange ='".$payrollCover."' and emp_id=".$empid);
			$query = $this->db->query("select emp_id,group_concat(concat(description,'|',additionname,'=',value)) as adjust from tbl_payroll_addition a,tbl_payroll_emp_addition b,tbl_payroll_coverage c where a.paddition_id=b.paddition_id and c.coverage_id=b.coverage_id and daterange ='".$payrollCover."' and emp_id=".$empid);
			 return $query->result();			  
  		}
		public function deductSalary($empid,$payrollCover){
			$query = $this->db->query("select emp_id,group_concat(concat(deductionname,'=',value)) as deduct from tbl_payroll_deductions a,tbl_payroll_coverage b, tbl_payroll_emp_deduction c where a.pdeduct_id=c.pdeduct_id and b.coverage_id=c.coverage_id and value>0 and daterange ='".$payrollCover."' and c.emp_id= ".$empid);
			 return $query->result();			  
  		}
	/* 	 	public function isLoginHoliday($login,$timeStart){
			$year = date_format(date_create($login),"Y");
			  $date = date_format(date_create($login),"Y-m-d");
			  $timeStart = date_format(date_create($timeStart),"h:i:s");
 			
	$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");

			return $query->result();
  		} */
			 public function isLoginHoliday($login,$timeStart){
				/*  public function isLoginHoliday(){
				  $login = "2017-08-28 08:13:23";
				 $timeStart = "9:30:00 AM"; */
			$year = date_format(date_create($login),"Y");
			$date = date_format(date_create($login),"Y-m-d");
			$timeStart = date_format(date_create($timeStart),"h:i:00");
 			
			$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
 			return $query->result();
		}
		public function loginn2timeID($time_id){
		 			$query = $this->db->query("SELECT * FROM tbl_time where time_id=".$time_id);
			 return $query->result();

		}
		public function shiftTotalHr($time_id){
		 			$query = $this->db->query("SELECT * FROM tbl_time where time_id=".$time_id);
			 return $query->result();

		}
		public function loginnNdtimeID($time_id){
		 			$query = $this->db->query("SELECT b.* FROM `tbl_time` a,tbl_payroll_nd b  where a.time_id=b.time_id and a.time_id=".$time_id);
			 return $query->result();

		}
		public function isLogoutHoliday($logout,$timeEnd){
			  $year = date_format(date_create($logout),"Y");
			  $date = date_format(date_create($logout),"Y-m-d");
			  $timeEnd = date_format(date_create($timeEnd),"h:i:s");
 			
	// $query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");
		$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type,sum(calc) as calc from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type,calc from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");


			return $query->result();
  		}
			public function savePayroll(){
				$coverage_id = $this->input->post("coverage_id");
				$type = $this->input->post("emp");
				   $query = $this->db->query("delete from tbl_payroll where emp_type='".$type."' and coverage_id=".$coverage_id );
				$data = json_decode($this->input->post("dataArray"));
				// print_r($data);
				 foreach($data as $key => $val){
					foreach($val as $k => $v){
						//echo  $val[26]." =";
					}
					  // echo  "**".$val[21]." =";
					  // echo  json_encode($val);
  // $query = $this->db->query("insert into tbl_payroll(coverage_id,emp_promoteId,pagibig_id,sss_id,tax_id,philhealth_id,monthly,quinsina,clothing,laundry,rice,ot_bct,nd,hp,ho,sss_details,phic_details,hdmf_details,bir_details,adjust_add,adjust_minus,total_earning,total_deduction,final,total_hours,isActive,pemp_adjust_id,hour_ho,hour_hp,hour_nd) values(".$coverage_id.",'$val[0]',1,'$val[1]','$val[3]' ,'$val[2]' ,'$val[6]' ,'$val[7]' ,'$val[11]' ,'$val[12]' ,'$val[13]' ,'$val[14]' ,'$val[15]' ,'$val[16]' ,'$val[17]' ,'$val[18]' ,'$val[19]' ,'$val[20]' ,'$val[21]' ,'$val[22]' ,'$val[23]' ,'$val[24]' ,'$val[25]' ,'$val[26]' ,'$val[10]',1,'$val[27]','$val[28]','$val[29]','$val[30]')");
  if(count($data)>0){
  $query = $this->db->query("insert into tbl_payroll(coverage_id,emp_promoteId,pagibig_id,sss_id,tax_id,philhealth_id,monthly,quinsina,clothing,laundry,rice,ot_bct,nd,hp,ho,sss_details,phic_details,hdmf_details,bir_details,adjust_add,adjust_minus,total_earning,total_deduction,final,total_hours,isActive,pemp_adjust_id,hour_ho,hour_hp,hour_nd,bonus,emp_type,account,adjust_details,deduct_details,absentLate,bonus_details) values(".$coverage_id.",'$val[0]',1,'$val[1]','$val[3]' ,'$val[2]' ,'$val[6]' ,'$val[7]' ,'$val[11]' ,'$val[12]' ,'$val[13]' ,'$val[15]' ,'$val[16]' ,'$val[17]' ,'$val[18]' ,'$val[19]' ,'$val[20]' ,'$val[21]' ,'$val[22]' ,'$val[23]' ,'$val[24]' ,'$val[25]' ,'$val[26]' ,'$val[27]' ,'$val[10]',1,'$val[28]','$val[29]','$val[30]','$val[31]','$val[14]','$type','$val[32]','$val[33]','$val[34]','$val[35]','$val[36]')");
  }
				 }
		}
}