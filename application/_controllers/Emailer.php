<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Emailer extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model("KudosModel");
	}
	public function sendEmail(){

		$cnt = $this->KudosModel->getKudosPending();

		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'smtp.gmail.com';
		$config['smtp_port'] = 587;
		$config['smtp_user']    = 'szdummysz@gmail.com';
		$config['smtp_pass']    = 'szdummy123';
		$config['charset']    = 'utf-8';
		
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from('szdummysz@gmail.com', 'Dummy');
		$this->email->to('nicca.arique@supportzebra.com');
        $this->email->cc('jeffrey@supportzebra.com');

		$this->email->subject('Kudos');
		$this->email->message('You still have '.$cnt.' pending kudos.');

		$this->email->send();
		$this->email->print_debugger([$include = array('headers', 'subject', 'body')]);
       // redirect('Home');
	}

}
