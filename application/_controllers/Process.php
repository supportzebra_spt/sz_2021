<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Process extends General {
	protected $title = 'Process';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('process_model');
	}
	public function index()
	{
		$data=$this->user_infos();
		$data['processes']=$this->process_model->get_allprocess();
		$data['folders']= $this->general_model->fetch_specific_vals("folder.*, app.*","folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1'","tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee");
		/*$this->general_model->fetch_specific_vals("process.*, user.*,folder.*","process.createdBy=user.uid AND process.folder_ID=folder.folder_ID","tbl_processST_process process, tbl_user user, tbl_processST_folder folder","process.processTitle ASC");*/
		$data['checklists']=$this->general_model->fetch_all("*","tbl_processST_checklist","checklistTitle ASC");
		$this->load_template_view('templates/process/process', $data);
	}

//JQUERY DISPLAY


	function display_folders(){
		$word = $this->input->post("word");
		$word  =  ($word!=null or !empty($word) or $word!="") ? $word : null;
		$data=$this->process_model->get_allfolders($word);
/*		$data['processes']=$this->process_model->get_allprocess();
$data['checklists']=$this->general_model->fetch_all("*","tbl_processST_checklist","checklistTitle ASC");*/
echo (count($data)>0) ? json_encode($data) : 0;
}
function display_processtemplates(){
	$word = $this->input->post("word");
	$word =($word!=null or !empty($word) or $word!="") ? $word : null;
	$data=$this->process_model->get_allprocess($word);
	echo (count($data)>0) ? json_encode($data) : 0;
}
function display_morefolders(){
	$lastid = $_POST['folder_ID'];
	$str = ($_POST['str']=="") ? "" : $_POST['str'];
	$data=$this->process_model->get_nextallfolders($lastid,$str);
	echo json_encode($data);
}
function display_moreprocess(){
	$lastid = $_POST['process_ID'];
	$str = ($_POST['str']=="") ? "" : $_POST['str'];
	$data=$this->process_model->get_nextallprocesses($lastid,$str);
	echo json_encode($data);
}
function display_tasks(){
	$process_ID = $_POST['process_ID'];
	$data=$this->general_model->fetch_specific_vals("task.*,process.*","task.process_ID=$process_ID AND task.process_ID=process.process_ID","tbl_processST_task task, tbl_processST_process process");
	echo json_encode($data);
}
function display_tasks_update(){
	$process_ID = $_POST['process_ID'];
	$data=$this->general_model->fetch_specific_vals("task.*,process.*","task.process_ID=$process_ID AND task.process_ID=process.process_ID","tbl_processST_task task, tbl_processST_process process");
	echo json_encode($data);
}
function display_tasks_answerable(){
	$checklist_ID = $_POST['checklist_ID'];
	$data=$this->general_model->fetch_specific_vals("task.*,checkStatus.*","task.task_ID=checkStatus.task_ID AND checkStatus.checklist_ID=$checklist_ID","tbl_processST_task task, tbl_processST_checklistStatus checkStatus");
	echo json_encode($data);
}
//END OF JQUERY DISPLAY
function folderload_more()
{
	$lastid = $_POST['folder_ID'];
	$data['foldernum']=$this->process_model->count_folders($lastid);
	$data['folders']=$this->process_model->get_nextallfolders($lastid);
	$data['processes']=$this->general_model->fetch_specific_vals("process.*, app.*,folder.*","process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID","tbl_processST_process process, tbl_user user, tbl_processST_folder folder, tbl_applicant app, tbl_employee employee","process.processTitle ASC");
	$data['checklists']=$this->general_model->fetch_all("*","tbl_processST_checklist","checklistTitle ASC");
	$data['folderlimit']=3;
	$this->load->view('templates/process/folder_loadmore', $data);
}
function processload_more()
{
	$lastid = $_POST['process_ID'];
	$data['folders']=$this->process_model->get_allfolders();
	$data['processnum']=$this->process_model->count_processtemp($lastid);
	$data['processes']=$this->process_model->get_nextallprocesses($lastid);
	$data['checklists']=$this->general_model->fetch_all("*","tbl_processST_checklist","checklistTitle ASC");
	$this->load->view('templates/process/process_loadmore', $data);
}
public function getID($folder_ID)
{
	$data=$folder_ID;
}
public function user_infos()  
{
	$data = [
		'uri_segment' => $this->uri->segment_array(),
		'title' => $this->title,
	];
	$fields = "a.apid,fname,lname,pic,emp_id,gender,acc_name,birthday";
	$where = "a.apid=b.apid and c.acc_id=b.acc_id and month(birthday)=" . date("m") . " and day(birthday)=" . date("d") . " and b.isActive='yes'";
	$result = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_applicant a,tbl_employee b,tbl_account c');
	foreach ($result as $row)
	{
		$job = $this->getCurrentPosition($row->emp_id);
		$data["celebrants"][$row->emp_id] = array(
			"emp_id" => $row->emp_id,
			"apid" => $row->apid,
			"fname" => $row->fname,
			"lname" => $row->lname,
			"pic" => $row->pic,
			"gender" => $row->gender,
			"acc_name" => $row->acc_name,
			"birthday" => $row->birthday,
			"job" => $job,
		);
	}
	return $data;
}
public function subfolders(){
	$id = $_POST['folder_ID'];
	$data['subfolders']=$this->general_model->fetch_specific_vals("folder.*, app.*","folder.referenceFolder=$id AND folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid","tbl_processST_folder folder,tbl_user user,tbl_applicant app, tbl_employee employee","folder.folderName ASC");
		// $this->process_model->get_subfolders($id);
	$data['eachprocess']=$this->general_model->fetch_specific_vals("process.*,folder.*","process.folder_ID=$id AND folder.folder_ID=$id","tbl_processST_process process, tbl_processST_folder folder","process.processTitle ASC");
		// $this->process_model->get_process($id);
	echo json_encode($data);
}
public function subfolder($folder_ID){
	$data=$this->user_infos();
	$data['folderID']=$folder_ID;
	$data['processes']=$this->general_model->fetch_specific_vals("process.*,folder.*","process.folder_ID=$folder_ID AND folder.folder_ID=$folder_ID","tbl_processST_process process, tbl_processST_folder folder","process.processTitle ASC");
	$data['subfolders']=$this->general_model->fetch_specific_vals("folder.*, app.*","folder.referenceFolder=$folder_ID AND folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid","tbl_processST_folder folder,tbl_user user,tbl_applicant app, tbl_employee employee","folder.folderName ASC");
		// $this->process_model->get_subfolders($folder_ID);
	$data['folderinfo']=$this->general_model->fetch_specific_val("*","folder_ID=$folder_ID","tbl_processST_folder","folderName ASC");
		// $this->process_model->get_singlefolderinfo($folder_ID);
	$data['checklists']=$this->general_model->fetch_all("*","tbl_processST_checklist","checklistTitle ASC");
	$this->load_template_view('templates/process/all_processes', $data);
}
public function checklist($process_ID){
	$data=$this->user_infos();
	$userid=$this->session->userdata('uid');
	$data['user_info']=$this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname","user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid","tbl_applicant applicant, tbl_employee employee, tbl_user user"); //GET FNAME AND LNAME
		// $this->process_model->get_userinfo($userid);
	$data['processID']=$process_ID;
	$data['checklist']=$this->general_model->fetch_specific_vals("checklist.*","checklist.process_ID=$process_ID","tbl_processST_checklist checklist");
		// $this->process_model->get_checklist($process_ID);
	$data['task']=$this->general_model->fetch_specific_vals("task.*,process.*","task.process_ID=$process_ID AND task.process_ID=process.process_ID","tbl_processST_task task, tbl_processST_process process");
		// $this->process_model->get_task($process_ID);
	$data['processinfo']=$this->general_model->fetch_specific_val("*","process_ID=$process_ID","tbl_processST_process","processTitle ASC");
		// $this->process_model->get_singleprocessinfo($process_ID);
	$this->load_template_view('templates/process/checklist', $data);
}
public function checklist_update($checklist_ID,$process_ID){
	$data=$this->user_infos();
	$userid=$this->session->userdata('uid');
	$data['user_info']=$this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname","user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid","tbl_applicant applicant, tbl_employee employee, tbl_user user");
	$data['processID']=$process_ID;
	$data['checklist']=$this->general_model->fetch_specific_val("*","checklist_ID=$checklist_ID","tbl_processST_checklist","checklistTitle ASC");
		// $this->process_model->get_singlechecklistinfo($checklist_ID);
	$data['task']=$this->general_model->fetch_specific_vals("task.*,process.*","task.process_ID=$process_ID AND task.process_ID=process.process_ID","tbl_processST_task task, tbl_processST_process process");
	$data['processinfo']=$this->general_model->fetch_specific_val("*","process_ID=$process_ID","tbl_processST_process","processTitle ASC");
	$this->load_template_view('templates/process/checklist_update', $data);
}
public function checklist_pending($process_ID){
	$data=$this->user_infos();
	$userid=$this->session->userdata('uid');
	$data['user_info']=$this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname","user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid","tbl_applicant applicant, tbl_employee employee, tbl_user user");
	$data['processID']=$process_ID;
	$data['checklist']=$this->process_model->get_checklist($process_ID);
		// $this->general_model->fetch_specific_vals("checklist.*","checklist.process_ID=$process_ID","tbl_processST_checklist checklist");
	$data['checklist_status']=$this->general_model->fetch_all("*","tbl_processST_checklistStatus");
		// $this->process_model->get_checklistStat();
	$data['processinfo']=$this->general_model->fetch_specific_val("*","process_ID=$process_ID","tbl_processST_process","processTitle ASC");
	$this->load_template_view('templates/process/checklist_pending', $data);
}
public function checklistload_more()
{	
	$data=$this->user_infos();
	$userid=$this->session->userdata('uid');
	$lastid = $_POST['checklist_ID'];
	$process_ID=$_POST['process_ID'];
	$data['processID']=$process_ID;
	$data['user_info']=$this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname","user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid","tbl_applicant applicant, tbl_employee employee, tbl_user user");
	$data['checklistnum']=$this->process_model->count_checklist($lastid);
	$data['checklist']=$this->process_model->get_nextallchecklist($process_ID,$lastid);
	$data['checklist_status']=$this->general_model->fetch_all("*","tbl_processST_checklistStatus");
	$data['processinfo']=$this->general_model->fetch_specific_val("*","process_ID=$process_ID","tbl_processST_process","processTitle ASC");
	$this->load->view('templates/process/checklist_loadmore', $data);
}
public function checklist_answerable($process_ID,$checklist_ID)
{
	$data=$this->user_infos();
	$userid=$this->session->userdata('uid');
	$data['user_info']=$this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname","user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid","tbl_applicant applicant, tbl_employee employee, tbl_user user");
	$data['processID']=$process_ID;
	$data['checklist']=$this->general_model->fetch_specific_val("*","checklist_ID=$checklist_ID","tbl_processST_checklist","checklistTitle ASC");
	$data['task']=$this->general_model->fetch_specific_vals("task.*,checkStatus.*","task.task_ID=checkStatus.task_ID AND checkStatus.checklist_ID=$checklist_ID","tbl_processST_task task, tbl_processST_checklistStatus checkStatus");
		// $this->process_model->get_answerabletask($checklist_ID);
	$data['processinfo']=$this->general_model->fetch_specific_val("*","process_ID=$process_ID","tbl_processST_process","processTitle ASC");
		// $this->process_model->get_singleprocessinfo($process_ID);
	$this->load_template_view('templates/process/checklist_answerable', $data);
}
public function add_folder(){
	$userid=$this->session->userdata('uid');
	$data['folderName'] = $this->input->post('folderName');
	$data['referenceFolder']='1';
	$data['createdBy']=$userid;
	$data['status_ID']='10';
	$this->general_model->insert_vals($data,"tbl_processST_folder");
}
public function add_subfolder(){
	$userid=$this->session->userdata('uid');
	$data['folderName'] = $this->input->post('folderName');
	$data['referenceFolder']= $this->input->post('folder_ID');;
	$data['createdBy']=$userid;
	$data['status_ID']='10';
	$this->general_model->insert_vals($data,"tbl_processST_folder");
}
public function add_processfolder(){
	$userid=$this->session->userdata('uid');
	$data['processTitle'] = $this->input->post('processTitle');
	$data['processType_ID']='1';
	$data['processstatus_ID']='10';
	$data['folder_ID']= $this->input->post('folder_ID');;
	$data['createdBy']=$userid;
	$processid=$this->process_model->add_processfolderM($data);
	$this->add_checklist($processid);
	$this->add_task($processid);
	echo json_encode($processid);
}
public function add_checklist($process_ID){
	$userid=$this->session->userdata('uid');
	$data['process_ID']=$process_ID;
	$data['status_ID']='10';
	$data['createdBy']=$userid;
	$this->general_model->insert_vals($data,"tbl_processST_checklist");
}
public function add_anotherchecklist(){	
	$userid=$this->session->userdata('uid');
	$processid= $_POST['process_ID'];
	$checkTitle=$_POST['checklistTitle'];
	$data['process_ID']=$processid;
	$data['checklistTitle']=$checkTitle;
	$data['status_ID']='2';
	$data['createdBy']=$userid;
	$cid=$this->general_model->insert_vals_last_inserted_id($data,"tbl_processST_checklist");
		// $this->process_model->add_anotherchecklist($data);
	$alltask=$this->process_model->get_task($processid);
	$arTask=[];
	$taskNum = 0;
	foreach($alltask as $alltaskrow){
		$arTask[$taskNum]=[
			"checklist_ID" => $cid,
			"task_ID" => $alltaskrow->task_ID,
			"isCompleted" => 2,
		];
		$taskNum++;
	}
	$this->process_model->addbatch_checkstatus($arTask,'tbl_processST_checklistStatus');
	echo json_encode($data);
}
public function add_subtask(){
	$data['component_ID']= $_POST['component_ID'];
	$data['task_ID']= $_POST['task_ID'];
	$data['sequence']= $_POST['sequence'];
	$subtask['subTask_ID']=$this->general_model->insert_vals_last_inserted_id($data,"tbl_processST_subtask");
	$this->general_model->insert_vals($subtask,"tbl_processST_component_subtask");
	echo json_encode($subtask);
}
public function add_subtaskcomponent(){
	$data['subTask_ID']= $_POST['subTask_ID'];
	$subtaskid=$_POST['subTask_ID'];
	$this->general_model->insert_vals($data,"tbl_processST_component_subtask");
	$subtaskcomp=$this->general_model->fetch_specific_val("*","subTask_ID=$subtaskid","tbl_processST_subtask");
	echo json_encode($subtaskcomp);
}
public function add_task($process_ID){
	$userid=$this->session->userdata('uid');
	$data['process_ID']=$process_ID;
	$data['status_ID']='2';
	$data['createdBy']=$userid;
	$this->process_model->add_task($data);
}
public function add_onetask(){
	$userid=$this->session->userdata('uid');
	$processid= $_POST['process_ID'];
	$data['process_ID']=$processid;
	$data['status_ID']='2';
	$data['createdBy']=$userid;
	$this->process_model->add_task($data);
	echo json_encode($data);
}
public function add_onetaskStatus(){
	$userid=$this->session->userdata('uid');
	$processid= $_POST['process_ID'];
	$checkid= $_POST['checklist_ID'];
	$data['process_ID']=$processid;
	$data['status_ID']='2';
	$data['createdBy']=$userid;
	$taskid=$this->process_model->add_anothertask($data);
	$allchecklist=$this->process_model->get_checklist($processid);
	$arCheck=[];
	$checkNum = 0;
	foreach($allchecklist as $allcheckrow){
		$arCheck[$checkNum]=[
			"checklist_ID" => $allcheckrow->checklist_ID,
			"task_ID" => $taskid,
			"isCompleted" => 2,
		];
		$checkNum++;
	}
	$this->process_model->addbatch_checkstatus($arCheck,'tbl_processST_checklistStatus');
	echo json_encode($data);
}
public function add_taskdetails(){
	$userid=$this->session->userdata('uid');
	$id = $_POST['task_ID'];
	$taskTitle= $_POST['taskTitle'];
	$data['taskTitle']=$taskTitle;
	$data['updatedBy']=$userid;
	$this->process_model->update_taskdetails($id,$data);
	echo json_encode($data);
}
public function add_subtaskdetails(){
	$id= $_POST['subTask_ID'];
	$data['complabel']= $_POST['complabel'];
	$this->process_model->update_subtaskdetails($id,$data);
	echo json_encode($data);
}
public function add_answer(){
	$empid=$this->user_details();
	$answeredby=$empid->emp_id;
	$subtask=$_POST['subtask_ID'];
	$checklist=$_POST['checklistid'];

	$data['answer']= $_POST['answer'];
	$data['answeredBy']=$answeredby;

	$data['subtask_ID']= $subtask;
	$data['checklist_ID']= $checklist;
	$result=$this->process_model->check_answerexist($subtask,$checklist);
	if($result==0){
		$this->general_model->insert_vals($data,"tbl_processST_answer");
	}else{
		$id=$this->process_model->get_answerid($subtask,$checklist)->row();
		$ansid=$id->answer_ID;
		$this->process_model->update_answerdetails($ansid,$data);
	}
	echo json_encode($data);
}
public function add_multiple_answer(){
	$empid=$this->user_details();
	$answeredby=$empid->emp_id;
	$subtask=$_POST['subtask_ID'];
	$checklist=$_POST['checklistid'];
	$data['answer']= $_POST['answer'];
	$data['answeredBy']=$answeredby;
	$data['subtask_ID']= $subtask;
	$data['checklist_ID']= $checklist;
	$this->general_model->insert_vals($data,"tbl_processST_answer");
	echo json_encode($data);
}
public function add_optiondetails(){
	$id= $_POST['componentSubtask_ID'];
	$data['compcontent']= $_POST['compcontent'];
	$this->process_model->update_compsubtaskdetails($id,$data);
	echo json_encode($data);
}
public function add_sublabeldetails(){
	$id= $_POST['subTask_ID'];
	$data['sublabel']= $_POST['sublabel'];
	$this->process_model->update_subtaskdetails($id,$data);
	echo json_encode($data);
}
public function add_checklistdetails(){
	$userid=$this->session->userdata('uid');
	$id = $_POST['checklist_ID'];
	$pid = $_POST['process_ID'];
	$checkTitle= $_POST['checklistTitle'];
	$data['checklistTitle']=$checkTitle;
	$data['status_ID']='2';
	$data['updatedBy']=$userid;
	$pdata['processstatus_ID']='2';
	$this->process_model->update_checklistdetails($id,$data);
	$this->process_model->update_process($pid,$pdata);
	$alltask=$this->process_model->get_task($pid);
	$arTask=[];
	$taskNum = 0;
	foreach($alltask as $alltaskrow){
		$arTask[$taskNum]=[
			"checklist_ID" => $id,
			"task_ID" => $alltaskrow->task_ID,
			"isCompleted" => 2,
		];
		$taskNum++;
	}
	$this->process_model->addbatch_checkstatus($arTask,'tbl_processST_checklistStatus');
	echo json_encode($data);
}
public function delete_folder(){
	$id = $_POST['folder_ID'];
	$this->process_model->delete_process($id);
	$this->process_model->delete_subfolder($id);
	$this->process_model->delete_folder($id);
}
public function delete_task(){
	$id = $_POST['task_ID'];
	$this->process_model->delete_task($id);
}
public function delete_process(){
	$id = $_POST['folder_ID'];
	$this->process_model->delete_checklist($id);
	$this->process_model->delete_process($id);
}
public function delete_component(){
	$id = $_POST['subTask_ID'];
	$this->process_model->delete_compsubtask($id);
	$this->process_model->delete_subtask($id);
}
public function delete_option(){
	$id = $_POST['componentSubtask_ID'];
	$this->process_model->delete_option($id);
}
public function update_folder(){
	$id = $_POST['folder_ID'];
	$data['folderName'] = $this->input->post('folderName');
	$this->process_model->updatefolder_process($id,$data);
}
public function update_process(){
	$id = $_POST['process_ID'];
	$data['processTitle'] = $this->input->post('processTitle');
	$this->process_model->update_process($id,$data);
}
public function update_checklist(){
	$id = $_POST['checklist_ID'];
	$data['checklistTitle'] = $this->input->post('checklistTitle');
	$this->process_model->update_checklist($id,$data);
}
public function update_checklistStatus(){
	$id = $_POST['checklist_ID'];
	$data['status_ID']=$_POST['status_ID'];
	$this->process_model->update_checklist($id,$data);
	echo json_encode($data);
}
public function update_checklistDuedate(){
	$id = $_POST['checklist_ID'];
	$data['dueDate'] = $_POST['dueDate'];
	$this->process_model->update_checklist($id,$data);
	echo json_encode($data);
}
public function update_taskDuedate(){
	$checklist_ID = $_POST['checklist_ID'];
	$task_ID = $_POST['task_ID'];;
	$data['taskdueDate'] = $_POST['taskdueDate'];
	$id=$this->process_model->get_checklistStatusID($checklist_ID,$task_ID)->row();
	$checkstatid=$id->checklistStatus_ID;
	$this->process_model->update_taskduedate($checkstatid,$data);
	echo json_encode($data);
}
public function update_taskStatus(){
	$id = $_POST['checklistStatus_ID'];
	$data['isCompleted'] = $_POST['isCompleted'];
	$this->process_model->update_taskStatus($id,$data);
	echo json_encode($data);
}
public function update_taskAllStatus(){
	$id = $_POST['checklist_ID'];
	$data['isCompleted'] = $_POST['isCompleted'];
	$this->process_model->update_taskAllStatus($id,$data);
	echo json_encode($data);
}
public function update_inputformdetails(){
	$id= $_POST['subTask_ID'];
	$data['complabel']= $_POST['complabel'];
	$data['sublabel']= $_POST['sublabel'];
	$data['placeholder']= $_POST['placeholder'];
	$data['required']= $_POST['required'];
	$data['min']= $_POST['min'];
	$data['max']= $_POST['max'];
	$this->process_model->update_inputformdetails($id,$data);
	echo json_encode($data);
}
public function update_headingtextdetails(){
	$id= $_POST['subTask_ID'];
	$data['complabel']= $_POST['complabel'];
	$data['sublabel']= $_POST['sublabel'];
	$data['text_alignment']= $_POST['text_alignment'];
	$data['text_size']= $_POST['text_size'];
	$this->process_model->update_inputformdetails($id,$data);
	echo json_encode($data);
}
public function update_optiondetails(){
	$id= $_POST['subTask_ID'];
	$data['complabel']= $_POST['complabel'];
	$data['sublabel']= $_POST['sublabel'];
	$data['required']= $_POST['required'];
	$this->process_model->update_inputformdetails($id,$data);
	echo json_encode($data);
}
public function update_datetimedetails(){
	$id= $_POST['subTask_ID'];
	$data['complabel']= $_POST['complabel'];
	$data['sublabel']= $_POST['sublabel'];
	$data['placeholder']= $_POST['placeholder'];
	$data['required']= $_POST['required'];
	$this->process_model->update_inputformdetails($id,$data);
	echo json_encode($data);
}
public function get_dropdownfolinfo(){
	$stat = $this->input->post("status_ID");
	$res=$this->process_model->get_dropdowninfo($stat);
	echo json_encode($res);
}
public function get_dropdownproinfo(){
	$stat = $this->input->post("status_ID");
	$res=$this->process_model->get_dropdownproinfo($stat);
	echo json_encode($res);
}
public function fetch_update(){
	$id = $_POST['folder_ID'];
	$res= $this->process_model->fetch_update($id);
	echo json_encode($res);
}
public function fetchprocess_update(){
	$id = $_POST['process_ID'];
	$res= $this->process_model->fetchprocess_update($id);
	echo json_encode($res);
}
public function fetchtask_update(){
	$id = $_POST['task_ID'];
	$res= $this->process_model->fetchtask_update($id);
	echo json_encode($res);
}
public function fetchchecklist_update(){
	$id = $_POST['checklist_ID'];
	$res= $this->process_model->fetchchecklist_update($id);
	echo json_encode($res);
}
public function fetch_duedate(){
	$task_ID = $_POST['task_ID'];
	$checklist_ID= $_POST['check_ID'];
	$id=$this->process_model->get_checklistStatusID($checklist_ID,$task_ID)->row();
	$checkstatid=$id->checklistStatus_ID;
	$info=$this->process_model->get_singleCheckstatinfo($checkstatid);
	echo json_encode($info);
}
public function fetch_subtask(){
	$subtask_ID = $_POST['subTask_ID'];
	$data=$this->general_model->fetch_specific_val("*","subTask_ID=$subtask_ID","tbl_processST_subtask");
	echo json_encode($data);
}
public function fetch_componentsubtask(){
	$subtask_ID = $_POST['subTask_ID'];
	$data=$this->general_model->fetch_specific_vals("*","subTask_ID=$subtask_ID","tbl_processST_component_subtask");
	echo json_encode($data);
}
public function fetch_allsubtask(){
	$task_ID = $_POST['task_ID'];
	$data=$this->general_model->fetch_specific_vals("*","task_ID=$task_ID","tbl_processST_subtask");
	echo json_encode($data);
}
public function fetch_allanswers(){
	$task_ID = $_POST['task_ID'];
	$checklist_ID = $_POST['checklist_ID'];
	$data=$this->process_model->get_answers($task_ID,$checklist_ID);
	echo json_encode($data);
}
public function fetch_formdetails(){
	$subTask_ID = $_POST['subTask_ID'];
	$data=$this->general_model->fetch_specific_val("*","subTask_ID=$subTask_ID","tbl_processST_subtask");
	echo json_encode($data);
}
public function find_folder(){
	$data= $_POST['input'];
	$res=$this->process_model->search_folder($data);
	echo json_encode($res);
}
public function countallforms(){
	$id = $_POST['task_ID'];
	$data=$this->process_model->countallforms($id);
	echo json_encode($data);
}
public function user_details(){
	$userid=$this->session->userdata('uid');
	$answeredby=$this->process_model->get_empid($userid)->row();
	return $answeredby;
}
}
