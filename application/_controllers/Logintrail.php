<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logintrail extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('LoginTrailModel');
    }

    public function index() {

        if ($this->session->userdata('uid')) {

            $qry = $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=" . $this->session->userdata('uid') . " and item_link='logintrail' and is_assign=1 order by lname");

            if ($qry->num_rows() > 0) {
                $data['users'] = $this->LoginTrailModel->getAllUsers();
                $this->load->view('audittrail', $data);
            } else {
                redirect('index.php/home/error');
            }
        }
    }

    public function auditLog() {
        $dateFrom = $this->input->post('dateFrom');
        $timeFrom = date("H:i:00", strtotime($this->input->post('timeFrom')));
        $dateTo = $this->input->post('dateTo');
        $timeTo = date("H:i:00", strtotime($this->input->post('timeTo')));
        $from = $dateFrom . " " . $timeFrom;
        $to = $dateTo . " " . $timeTo;

        $uid = $this->input->post('employee');
        if ($uid == 'All') {
            $result = $this->LoginTrailModel->getAuditTrailLogsAll($from, $to);
        } else {
            $result = $this->LoginTrailModel->getAuditTrailLogsUser($from, $to, $uid);
        }
        if (empty($result)) {
            echo "Empty";
        } else {
            echo json_encode($result);
        }
    }

}
