<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Settings extends General {

    protected $title = array('title' => 'Settings');

    public function __construct()
    {
        parent::__construct();
    }

    public function user_access_rights()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
//        if ($this->check_access())
//        {
        $this->load_template_view('templates/settings/user_access_rights', $data);
//        }
    }

    public function getMenuTabs()
    {
        $data = $this->general_model->fetch_all("tab_id,tab_name,icon", "tbl_menu_tab_item", "tab_name ASC");
        echo json_encode(array('tabs' => $data));
    }

    public function getMenuItems()
    {

        $searchLinkType = $this->input->post('searchLinkType');
        $searchString = "";
        if ($searchLinkType !== '')
        {
            $searchString = " WHERE a.tab_id = $searchLinkType";
        }
        $menu_items = $this->general_model->custom_query("SELECT * FROM tbl_menu_tab_item a INNER JOIN tbl_menu_items b ON a.tab_id=b.tab_id $searchString ORDER BY tab_name ASC,item_title ASC");
        echo json_encode($menu_items);
    }

    public function setUserAccess()
    {

        $menu_item_id = $this->input->post('menu_item_id');
        $uid = $this->input->post('uid');
        $status = $this->input->post('status');
        if ($status == 'true')
        {//ADD TO DB
            $res = $this->general_model->insert_vals(array('user_id' => $uid, 'menu_item_id' => $menu_item_id, 'is_assign' => 1, 'created_by' => $this->session->userdata('uid'), 'updated_by' => $this->session->userdata('uid')), 'tbl_user_access');
        }
        else
        {//DELETE TO DB
            $res = $this->general_model->delete_vals("user_id=$uid AND menu_item_id=$menu_item_id", 'tbl_user_access');
        }
        echo json_encode(array('status' => $res));
    }

    public function getUserAccess()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $searchClass = $this->input->post('searchClass');
        $searchName = $this->input->post('searchName');
        $search = array();
        $searchString = "";
        if ($searchClass !== '')
        {
            $search[] = "d.acc_description = '$searchClass'";
        }
        if ($searchName !== '')
        {
            $search[] = "(a.fname LIKE \"%$searchName%\" OR a.lname LIKE \"%$searchName%\")";
        }
        if (!empty($search))
        {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT a.fname,a.lname,a.mname,a.apid,b.emp_id,c.uid FROM tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d WHERE b.acc_id=d.acc_id AND a.apid=b.apid AND b.emp_id=c.emp_id AND b.isActive='yes' $searchString ORDER BY a.lname ASC";
        $employees = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");

        foreach ($employees as $emp)
        {
            $user_access = $this->general_model->fetch_specific_vals("useraccess_id,menu_item_id,is_assign", "is_assign=1 AND user_id=" . $emp->uid, "tbl_user_access");
            $emp->data = $user_access;
        }
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $employees, 'total' => $count));
    }

}
