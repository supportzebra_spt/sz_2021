<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	public function index(){
	if($this->session->userdata('uid')){
			redirect('index.php/home');
		}else{
			  $this->load->view('login'); //Default Design
			 // $this->load->view('login-christmas'); //For Christmas Design
			  // $this->load->view('login-valentines'); //For Valentines Design
			// $this->load->view('login-santa'); //For santa cruzan Design
		}
	}
	public function logout(){
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select fname,lname,uid,b.description from tbl_user a,tbl_user_role b,tbl_employee c,tbl_applicant d where a.role_id = b.role_id and c.emp_id=a.emp_id and d.apid=c.apid and uid=".$this->session->userdata('uid')."");
			if ($qry->num_rows() > 0){
				$result = $qry->result();
				$qry =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$result[0]->uid.",now(),'Logout','".$result[0]->fname." ".$result[0]->lname."')");
					$this->session->unset_userdata('uid');
					redirect('index.php/login');
			}
		}else{
			$this->load->view('login');
		}
	}
	public function loginVerify(){
             $uname = htmlspecialchars($this->input->post('uname'));	
             $pw = htmlspecialchars($this->input->post('pw'));
			 // $qry =  $this->db->query("select fname,lname,uid,b.description,role,settings,emp_id,(select class from tbl_emp_promote x,tbl_pos_emp_stat y, tbl_position z where x.posempstat_id = y.posempstat_id and y.pos_id = z.pos_id and x.emp_id=a.emp_id and x.isActive=1) as class from tbl_user a,tbl_user_role b where a.role_id = b.role_id and username='".$uname."' and password='".$pw."' ");
			 // $qry =  $this->db->query("select fname,lname,uid,b.description,role,settings,a.emp_id,(select class from tbl_emp_promote x,tbl_pos_emp_stat y, tbl_position z where x.posempstat_id = y.posempstat_id and y.pos_id = z.pos_id and x.emp_id=a.emp_id and x.isActive=1) as class from tbl_user a,tbl_user_role b,tbl_employee c where a.emp_id=c.emp_id and a.role_id = b.role_id and username='".$uname."' and password='".$pw."'  and c.isActive='Yes'");
			 $qry =  $this->db->query("select a.username,d.birthday,d.cell,d.email,d.fname,d.lname,uid,b.description,role,settings,a.emp_id,d.pic,(select class from tbl_emp_promote x,tbl_pos_emp_stat y, tbl_position z where x.posempstat_id = y.posempstat_id and y.pos_id = z.pos_id and x.emp_id=a.emp_id and x.isActive=1) as class,(select acc_name from tbl_account p where p.acc_id=c.acc_id) as acc_name,(select acc_description from tbl_account p where p.acc_id=c.acc_id) as acc_description,c.acc_id,a.role_id as roleID from tbl_user a,tbl_user_role b,tbl_employee c,tbl_applicant d where a.emp_id=c.emp_id and a.role_id = b.role_id and c.apid=d.apid and username='".$uname."' and password='".$pw."'  and c.isActive='yes'");
			 // echo "select fname,lname,uid,b.description,role,settings,emp_id,(select class from tbl_emp_promote x,tbl_pos_emp_stat y, tbl_position z where x.posempstat_id = y.posempstat_id and y.pos_id = z.pos_id and x.emp_id=a.emp_id and x.isActive=1) as class from tbl_user a,tbl_user_role b where a.role_id = b.role_id and username='".$uname."' and password='".$pw."'  limit 1";
	if ($qry->num_rows() > 0){
		$result = $qry->result();
		$qry =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$result[0]->uid.",now(),'Login','".$result[0]->fname." ".$result[0]->lname."')");

   foreach ($result as $row)
   {
 		 $pos = $this->emp($row->emp_id);

		echo "1 ";
		$this->session->set_userdata(array(
			'uid'      		=> $row->uid,
			'uname'      		=> $row->username,
			'fname'   	    => $row->fname,
			'lname'    		=> $row->lname,
			'description'	=> $row->description,
			'settings'	=> $row->settings,
			'class'	=> 	$row->class,
			'role'	=> 	$row->role,
			'role_id'	=> 	$row->roleID,
			'acc_name'	=> 	$row->acc_name,
			'acc_description'	=> 	$row->acc_description,
			'acc_id'	=> 	$row->acc_id,
			'emp_id'	=> $row->emp_id,
			'birthday'	=> (count($row->birthday)>0) ? $row->birthday : " -- ",
			'cell'	=> (count($row->cell)>0) ? $row->cell : " -- ",
			'email'	=> (count($row->email)>0) ? $row->email : " -- ",
			'status'        => TRUE,
			'pos'        => (count($pos)>0) ? $pos[0]->pos_details : " -- ",
			'pic'        => (count($row->pic)>0) ? $row->pic : "SZ_logo.png"
			));
	}
			}else{
				echo 0;
				
			}
	}
	public function loginval(){
		if($this->session->userdata('uid')){
			redirect('index.php/home');
		}else{
			// $this->load->view('login-valentines');
			$this->load->view('login-santa'); 
		}
	}
	public function emp($emp_id){
		$qry =  $this->db->query("SELECT pos_details FROM `tbl_emp_promote` a,tbl_pos_emp_stat b,tbl_position c where a.posempstat_id = b.posempstat_id and c.pos_id=b.pos_id and a.isActive=1 and emp_id = ".$emp_id);
		
		return $qry->result();

	}
}