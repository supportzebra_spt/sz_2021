<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct(){
        parent::__construct();
				$this->load->model("KudosModel");

			}
	public function index(){
	if($this->session->userdata('uid')){
			$query = $this->db->query("select b.role_id,b.description,uid,username,role,a.isActive,acc_name,acc_description from tbl_user a,tbl_user_role b,tbl_employee c,tbl_account d where a.emp_id=c.emp_id and c.acc_id=d.acc_id and a.role_id = b.role_id and a.isActive=1 and uid=".$this->session->userdata('uid')." ");
			$bdayQry = $this->db->query("SELECT emp_id,pic,fname,lname,birthday FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid and DAY(birthday) = DAY(curdate()) AND MONTH(birthday) = MONTH(curdate())");
			$applyQry = $this->db->query("SELECT apid,pic,fname,lname,pos_name from tbl_applicant a,tbl_position b where a.pos_id=b.pos_id and isActive='yes'");
			$accountQry = $this->db->query("SELECT acc_id,acc_name,acc_description from tbl_account where acc_description='Admin'");
			$accountQry2 = $this->db->query("SELECT acc_id,acc_name,acc_description from tbl_account where acc_description='Agent'");
			 $arr['userRole']  = array();
				foreach ($accountQry->result() as $row)
				{
					// echo $row->acc_id." ".$row->acc_name." ".$row->acc_description."<br>";
					$res = $this->emp_account($row->acc_id);
					foreach ($res->result() as $row2 => $val){
						if($val->cnt>0){
 						$arr['accAdmin'][$row->acc_id] = array(
							"cnt" => "['".$row->acc_name."',".$val->cnt."]",
							"cntAdmin" => $val->cnt

						);
  						}
					}
				}
				foreach ($accountQry2->result() as $row)
				{
					// echo $row->acc_id." ".$row->acc_name." ".$row->acc_description."<br>";
					$res = $this->emp_account($row->acc_id);
					foreach ($res->result() as $row2 => $val){
						if($val->cnt>0){
 						$arr['accAgent'][$row->acc_id] = array(
							"cnt2" => "['".$row->acc_name."',".$val->cnt."]",
							"cntAgent" => $val->cnt
						);
  						}
					}
				}
					//print_r($arr);
				
					foreach ($query->result() as $row)
				{
					$arr['userRole'] = array(
					"role_id" => $row->role_id,
					"description" => $row->description,
					"uid" => $row->uid,
					"username" => $row->username,
					"role" => $row->role,
					"isActive" => $row->isActive,
					"acc_name" => $row->acc_name,
					"acc_description" => $row->acc_description,
					);
				}
				foreach ($bdayQry->result() as $row)
				{
					$arr['Bdate'][$row->emp_id] = array(
					"emp_id" => $row->emp_id,
					"pic" => $row->pic,
					"fname" => $row->fname,
					"lname" => $row->lname,
					"lname" => $row->lname,
					"bday" => $row->birthday,
				 
					);
				}
				 
				foreach ($applyQry->result() as $row)
				{
					$arr['applicant'][$row->apid] = array(
					"apid" => $row->apid,
					"pic" => $row->pic,
					"fname" => $row->fname,
					"lname" => $row->lname,
					"pos_name" => $row->pos_name,
 				 
					);
				}
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')."  and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
			 if(count($arr['userRole'])>0){
					if($arr['userRole']['acc_name']=="Human Resource Team"){ //for Human Resource Team
						$arr['cntkudos'] = $this->KudosModel->getkudosthisyear();
						$arr['ambassadorkudos'] = $this->KudosModel->getkudosmonthlythisyear();
						$arr['currentmonthkudos'] = $this->KudosModel->getkudoscurrentmonth();
						// $arr['top5'] = $this->KudosModel->getkudostop5();
						// $arr['topcampaign'] = $this->KudosModel->getkudoscampaigntop();
						$arr['campaignambassador'] = $this->KudosModel->getcampaignamabassadors();
						$arr['allcampaign'] = $this->KudosModel->getkudoscampaign();
						$arr['newtopcampaign'] = $this->KudosModel->getnewcampaigntopscore();
						$arr['newtopambassador'] = $this->KudosModel->getnewambassadortopscore();
						$arr['camp'] = array();
						 $this->load->view('home',$arr);
					}elseif($arr['userRole']['acc_name']=="SupportZebra Executive Team"){ // for EO or SupportZebra Executive Team
						 $this->load->view('home_EO',$arr);
					}elseif($arr['userRole']['acc_name']=="Operations Team"){ // for EO or SupportZebra Executive Team
		 
						 $arr['cntkudos'] = $this->KudosModel->getkudosthisyear();
						$arr['ambassadorkudos'] = $this->KudosModel->getkudosmonthlythisyear();
						$arr['currentmonthkudos'] = $this->KudosModel->getkudoscurrentmonth();
						// $arr['top5'] = $this->KudosModel->getkudostop5();
						// $arr['topcampaign'] = $this->KudosModel->getkudoscampaigntop();
						$arr['campaignambassador'] = $this->KudosModel->getcampaignamabassadors();
						$arr['allcampaign'] = $this->KudosModel->getkudoscampaign();
						$arr['newtopcampaign'] = $this->KudosModel->getnewcampaigntopscore();
						$arr['newtopambassador'] = $this->KudosModel->getnewambassadortopscore();
						$arr['camp'] = array();
						 $this->load->view('kudos_opt_dashboard',$arr);			
					}else{
						  $this->load->view('home_EO',$arr);
					}
			}else{
				$this->load->view('home_EO',$arr);
			}
		}else{
					redirect('index.php/login');
		}
		}
		public function emp_account($accid){
			$emp = $this->db->query("Select count(*) as cnt from tbl_employee where isActive='Yes' and acc_id=".$accid."");
			return $emp;
		}
		public function error(){
			$this->load->view('error');		
		}
 	public function password(){
			$cp = trim($this->input->post("CurrentPass"));
			$np= trim($this->input->post("NewPass"));
			$nrp =  trim($this->input->post("NewRPass"));
			$emp = $this->db->query("Select * from tbl_user where isActive='1' and uid=".$this->session->userdata('uid')." and password='".$cp."'");
 			if($emp->num_rows()>0){
 			if(!empty($np)){
					if($np==$nrp){
						echo 1;
							$emp = $this->db->query("update tbl_user set password='".$np."' where uid=".$this->session->userdata('uid'));
					}else{
						echo 2;
					}
			}else{
						echo 3;
				}
			}else{
				echo 0;
			}
	}
	public function emails2(){
		$data = array(
             'userName'=> 'Anil Kumar Panigrahi'
                 );
		$this->load->view('testEmail.php',$data);
	}
	public function emails(){
 
		$this->email->initialize(array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'smtp.gmail.com',
		  'smtp_user' => 'recruitment@supportzebra.com',
		  'smtp_pass' => 'hrdepartment2014',
		  'smtp_port' => 587,
		  'smtp_crypto' => 'tls',
		  'mailtype	' => 'html',
		  'validate' => TRUE,
		  'crlf' => "\r\n",
		  'charset' => "iso-8859-1",
		  'newline' => "\r\n"
		));
			$data = array(
             'userName'=> 'Anil Kumar Panigrahi'
                 );
		$this->email->from('recruitment@supportzebra.com', 'HR - RECRUITMENT');
		$this->email->to('jeffrey@supportzebra.com');
		$this->email->cc('jeffrey@supportzebra.com');
 		$this->email->subject('Email Test');
		$body = $this->load->view('testEmail.php',$data,TRUE);

		$this->email->message($body);
		$this->email->send();

		echo $this->email->print_debugger();

	}
}