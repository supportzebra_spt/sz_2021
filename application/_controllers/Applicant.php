<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Applicant extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Applicant2Model");
	}
	public function index(){
		if($this->session->userdata('uid')){
			$data['data'] = $this->Applicant2Model->getAllApplicants();
			$this->load->view('applicant2_main',$data);
		}else{
			redirect('index.php/login');
		}

	}
	public function getApplicantPreview(){
		$apid = $this->input->post('apid');
		$details = $this->Applicant2Model->getApplicantDetails($apid);
		echo json_encode($details);

	}
	public function applicantUpdate($apid=NULL){
		if($this->session->userdata('uid')){
			if($apid==NULL){
				redirect('index.php/Applicant');
			}else{
				$details = $this->Applicant2Model->getApplicantDetails($apid);
				if(empty($details)){
					redirect('index.php/Applicant');
				}else{
					$data['details'] = $details;
					$data['positions'] = json_encode($this->Applicant2Model->getPositionApplied());
					$data['apid'] = $apid;
					$this->load->view('applicant2_update',$data);
				}
			}
		}else{
			redirect('index.php/login');
		}

	}
	public function updateApplicantValue(){
		$apid = $this->input->post('apid');
		$name = $this->input->post('name');
		$value = $this->input->post('value');
		if($name=="cc_yrs"){
			$value=NULL;
		}
		$row = $this->Applicant2Model->updateApplicantValue($apid,$name,$value);
		if($row>0){
			echo 'Success';
		}else{
			echo 'Fail';
		}
	}
	public function updateCharRef(){
		$apid = $this->input->post('apid');
		$name = $this->input->post('name');
		$index = $this->input->post('index');
		$value = $this->input->post('value');
		$cr = $this->Applicant2Model->getCharRef($apid,$name);
		if(empty($cr)){
			echo "Failed";
		}else{
			$cr = explode('|',$cr->$name);
			switch ($index) {
				case "fname":
				$newname = explode('$',$cr[0]);
				$newname[0] = $value;
				$cr[0] = implode("$",$newname);
				break;
				case "mname":
				$newname = explode('$',$cr[0]);
				$newname[1] = $value;
				$cr[0] = implode("$",$newname);
				break;
				case "lname":
				$newname = explode('$',$cr[0]);
				$newname[2] = $value;
				$cr[0] = implode("$",$newname);
				break;
				case "cell":
				$cr[1] = $value;
				break;
				case "job":
				$cr[2] = $value;
				break;
			}
			$cr = implode("|",$cr);
			$row = $this->Applicant2Model->updateApplicantValue($apid,$name,$cr);
			if($row>0){
				echo 'Success';
			}else{
				echo 'Fail';
			}
		}
	}

	public function ccayrsupdater(){
		$apid = $this->input->post('apid');
		$index = $this->input->post('index');
		$value = $this->input->post('value');
		$data = $this->input->post('data');
		if($data==NULL){
			if($index=='cc_yrs'){
				$exp = "$value|0";
			}else if($index=='cc_mnt'){
				$exp = "0|$value";
			}
		}else{
			$data = explode("|",$data);
			if($index=='cc_yrs'){
				$exp = "$value|$data[1]";
			}else if($index=='cc_mnt'){
				$exp = "$data[0]|$value";
			}
		}

		$row = $this->Applicant2Model->updateApplicantValue($apid,"cc_yrs",$exp);
		if($row>0){
			echo 'Success';
		}else{
			echo 'Fail';
		}
		
	}

	// public function passApplicant(){
	// 	$apid = $this->input->post('apid');
	// 	$details = $this->Applicant2Model->getApplicantDetails($apid);
	// 	if($details->class=='Agent'){
	// 		$cc_yrs = explode("|",$details->cc_yrs);
	// 		$years = $cc_yrs[0]*12;
	// 		$months = $cc_yrs[1] + $years;		
	// 	}else{

	// 	}
	// } 
	public function failApplicant(){
		$apid = $this->input->post('apid');
		$row = $this->Applicant2Model->failApplicant($apid);
		// echo $this->db->last_query();
		if($row>0){
			echo 'Success';
		}else{
			echo 'Fail';
		}
	}
	public function getDepartmentPosition(){
		$pos_id = $this->input->post('pos_id');
		$department = $this->Applicant2Model->getDepartment($pos_id);
		$position = $this->Applicant2Model->getPOsInDep($department->dep_id);
		echo json_encode($position);
	}
	public function getCCAPosition(){
		$position = $this->Applicant2Model->getCCAPosition();
		echo json_encode($position);
	}
	public function createemployeerecord(){
		$apid = $this->input->post('apid');
		$posempstat_id = $this->input->post('posempstat_id');
		$effectivedate = $this->input->post('effectivedate');
		$emp_id = $this->Applicant2Model->createEmployee($apid,$effectivedate);
		if(empty($emp_id)){
			echo "Failed";
		}else{
			$row = $this->Applicant2Model->createEmpPromote($emp_id,$posempstat_id,$effectivedate);
			if($row>0){
				$data = $this->Applicant2Model->getEmpDetailsWithDep($emp_id);
				if(empty($data)){
					echo 'Failed2';
				}else{
					$row2 = $this->Applicant2Model->createUser($emp_id,$data->fname,$data->lname,$data->dep_name);
					if($row2>0){
						$row3 = $this->Applicant2Model->employApplicant($apid);
						if($row3>0){
							echo "Success";
						}else{
							echo "Failed";
						}
					}else{
						echo "Failed";
					}
				}
			}else{
				echo 'Failed';
			}
		}
		
	}

}