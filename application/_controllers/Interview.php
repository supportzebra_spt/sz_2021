<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interview extends CI_Controller {

		public function __construct(){
        parent::__construct();
		$this->load->model("InterviewModel");

			}
		
		public function index(){
			$data = array();
		if($this->session->userdata('uid')){
				$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='interview' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$data['setting'] = array("settings" => $row->settings);
			}
			
			if ($qry->num_rows() > 0){
					$data['interview'] = $this->InterviewModel->ListInterview();
					$data['category'] = $this->InterviewModel->ListCategory();
					$data['department'] = $this->InterviewModel->ListDep();
  				   	 $this->load->view('interviewlist',$data);
			}else{
				redirect('index.php/home/error');
			}
		}else{
					redirect('index.php/login');
		}
		
		
		}
		
		 
 
}