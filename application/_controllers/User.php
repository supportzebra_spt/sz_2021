<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Templatedesign.php");

class User extends Templatedesign {

  public function __construct(){
        parent::__construct();
		$this->load->database();
		$this->load->library('upload');
		$this->load->model("UserModel");

	}
	
	public function sideBars(){
		$this->sideBar();
	}
	public function headerLefts()
	{
		$this->HeaderLeft();
		
	}
	public function index(){
		if($this->session->userdata('uid')){
				$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Applicant' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$this->data['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
				$this->data['userz'] = $this->UserModel->fetchUSers();
				$this->load->view('userview',$this->data);
			}else{
				redirect('index.php/home/error');
			}
		}else{
					redirect('index.php/login');
		}
		
		
		}
 	public function addSingleUser(){  

 			$fname= $this->input->post('fname');
			$lname= $this->input->post('lname');
			$role= $this->input->post('role');
			$uname= $this->input->post('uname');
			$pword= $this->input->post('pword');
		 $this->UserModel->addUsers($fname,$lname,$role,$uname,$pword);	
	}
	public function updateUserNow(){
			$uid= trim($this->input->post('uid'));
			$fname= trim($this->input->post('fname'));
			$lname= trim($this->input->post('lname'));
			// $role= $this->input->post('role');
			$uname= trim($this->input->post('uname'));
			$pword= trim($this->input->post('pw'));
			
	 $query = $this->db->query("update tbl_user set username='".$uname."', password='".$pword."', fname='".$fname."', lname='".$lname."' where uid=".$uid."");
 	}
	public function updateUser(){
	 $uid['uid']= $this->input->post('uid');
	 $query = $this->db->query("select uid,username,password,fname,lname,isActive from tbl_user a where  uid=".$this->input->post('uid')."");
	// $query = $this->db->query("select uid,username,password,fname,lname,isActive from tbl_user a where uid=9");
			$rez = $query->result();
foreach($rez as $key => $val){
	foreach($val as $k => $v){
		$arr['user'][$k] = $v;
		
	}
}
	  	 
		  $this->load->view('userupdate',$arr);
	}
	public function addmultipleusers(){  

  			$this->load->view('useraddmulitipleusers');

	
	
	}
	public function sideBar()
	{
				// $this->load->view('TemplateSideBar',$query);
		$data = array();
		if($this->session->userdata('uid')){
			$query = $this->db->query("select  distinct(a.tab_id),tab_name from tbl_menu_tab_item a,tbl_menu_items b,tbl_user_access c,tbl_user d where a.tab_id =b.tab_id and b.menu_item_id=c.menu_item_id and d.uid=c.user_id and d.uid='".$this->session->userdata('uid')."'  and c.is_assign=1");
			$rez = $query->result();
			  
			foreach($rez as $key =>$v){
				 	//	 $data[$key] = $v->tab_id;
				 		  $data [$v->tab_name] = $this->sideBarMenu($v->tab_id);
						
 		}
			 						 // $data['items'] = $this->sideBarMenu($data);

			//echo json_encode($data);
   	$this->load->view('TemplateSideBar',array('res'=>$data));
		
		}else{
					redirect('login');
		}
	}
		public function sideBarMenu($rez)
	{
 
  $query = $this->db->query("select item_link,item_title from tbl_menu_tab_item a,tbl_menu_items b,tbl_user_access c,tbl_user d where a.tab_id =b.tab_id and b.menu_item_id=c.menu_item_id and d.uid=c.user_id and b.tab_id =".$rez."  and uid='".$this->session->userdata('uid')."'  and c.is_assign=1");
  
     return $query->result();

 }
	
	public function do_upload(){
	  $config =  array(
                  'upload_path'     => "./files/",
                  'allowed_types'   => "gif|jpg|png|jpeg|xlsx",
                  'overwrite'       => TRUE,
                  'max_size'        => "2048000",  // Can be set to particular file size
                  
                ); 
         $this->load->library('upload', $config);
	     $this->upload->initialize($config);

         if ( ! $this->upload->do_upload('userFile')) {
            $error = array('error' => $this->upload->display_errors()); 			 
           }
			
         else { 
            $data = array('upload_data' => $this->upload->data()); 
			 
          } 
	}
	
	

	public function userUploadxls()
	{
		$tmpName= "files/Template-User.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpName);
		$excelObj = $excelReader->load($tmpName);
		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();
		$lastCol  = $worksheet->getHighestColumn();
		$arr=[];
	 
		for($row=7;$row<=$lastRow;$row++){
		 
			$arr[] = array(
			'fname'=>$worksheet->getCell('A'.$row)->getValue(),
			'lname'=>$worksheet->getCell('B'.$row)->getValue(),
			'role'=>$worksheet->getCell('C'.$row)->getValue(),
			'uname'=>$worksheet->getCell('D'.$row)->getValue(),
			'pw'=>$worksheet->getCell('E'.$row)->getValue()
			);
		}
	 
	 echo json_encode($arr);
	}
	public function SaveuserUploadxls(){
		$users=   $this->input->post('selected_value');
			$listUsers = explode(",",$users);
			foreach($listUsers as $key => $val){
				$ls = explode("|",$val);

						 echo "fname: ".$ls[0]." lname: ".$ls[1]." role: ".$ls[2]." uname: ".$ls[3]." pw: ".$ls[4]."\n";
					 
				 
			}
			 
	}
	
}