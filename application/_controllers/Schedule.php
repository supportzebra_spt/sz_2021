<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends CI_Controller {

     //public $logo = "C:\\wamp64\\www\\sztest\\assets\\images\\logo2.png";
  public $logo = "/usr/local/www/apache24/data/supportzebra/assets/images/logo2.png";
     // public $file= "C:\\xampp\\htdocs\\supportzebra\\reports\\schedule.xlsx";
     //public $file= "/usr/local/www/apache24/data/supportzebra/reports/schedule.xlsx";
  public $file = "";

  public function __construct() {
    parent::__construct();
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    if ($this->session->userdata('uid')) {
        $this->load->model('ScheduleModel');
				// $this->file = "C:\\wamp64\\www\\sztest\\reports\\" . $this->session->userdata('emp_id') . "\\schedule.xlsx";
        $this->file = "/usr/local/www/apache24/data/supportzebra/reports/" . $this->session->userdata('emp_id') . "/schedule.xlsx";
    } else {
        redirect('index.php/login');
    }

}

public function index() {
	
    $roles = explode('|', $this->session->userdata('role'));
    $role = $roles[0];
        // $data['leave'] = $this->ScheduleModel->getLeaveTypes(); // Changed 7/26/2018
    $data['leave'] = $this->ScheduleModel->getAllLeaveTypes();
		// echo $role;
	$data['page'] = "schedule/manual";

    if ($role == 'Administrator') {
            // $this->load->view('sched_admin', $data);
			// $this->load->view('sched_admin_new', $data);  #Disabled as of 11-06-2018
        $this->load->view('forbidden', $data);
    }
		/* else if ($role == 'User Level 1') {
            // $this->load->view('sched_team', $data);
            $this->load->view('sched_admin_new', $data);
        }  else if ($role == 'Manager') {
		
            $this->load->view('sched_manager', $data);
        } */
        else {
            // $this->load->view('sched_notadmin', $data);  #Disabled as of 11-06-2018
             $this->load->view('forbidden', $data);

        }
    }

    public function calendar() {
        $data['uid'] = $this->session->userdata('uid');
        $data['legends'] = $this->ScheduleModel->getCalendarLegend2();
        $this->load->view('sched_calendar', $data);
    }

    public function multiplesched() {
//        $roles = explode('|', $this->session->userdata('role'));
//        $role = $roles[0];
//        if ($role == 'Administrator') {
//            $this->load->view('multiplesched_admin');
//        } else if ($role == 'User Level 1') {
//            $this->load->view('multiplesched_team');
//        } else {
//            $this->load->view('multiplesched_notadmin');
//        }
        $roles = explode('|', $this->session->userdata('role'));
        $role = $roles[0];
        $data['role'] = $role;
       $data['page'] = "schedule/excel";

        // $this->load->view('multiplesched', $data);
               $this->load->view('forbidden', $data);

    }

    public function getAccounts() {
        $desc = $this->input->post('desc');
        $data = $this->ScheduleModel->getAccount($desc);
        if ($data == NULL || $data == '') {
            echo 'Empty';
        } else {
            echo json_encode($data);
        }
    }

    public function getAccountEmployee() {
        $acc_id = $this->input->post('acc_id');
        $data = $this->ScheduleModel->getAccountEmployee($acc_id);
        if ($data == NULL || $data == '') {
            echo 'Empty';
        } else {
            echo json_encode($data);
        }
    }

    public function downloadSched($fromDate, $toDate, $acc_description = NULL, $employeelist = NULL) {
        // $fromDate = explode('/',$fromDate);
        // $fromDate = $fromDate[2]."-".$fromDate[0]."-".$fromDate[1];
        // $toDate = explode('/',$toDate);
        // $toDate = $toDate[2]."-".$toDate[0]."-".$toDate[1];
        $period = new DatePeriod(
            new DateTime("$fromDate"), new DateInterval('P1D'), new DateTime("$toDate +1 day ")
        );
        foreach ($period as $date) {
            $dates[] = $date->format("l - M d, Y");
        }
        if ($acc_description == 0 && $employeelist != NULL) {
            $employeelist = explode('-', $employeelist);
            $employees = $this->ScheduleModel->getAllEmployeesInList($employeelist);
        } else if ($acc_description != NULL && $employeelist == NULL) {
            $employees = $this->ScheduleModel->getAllEmployeesPerType($acc_description);
        } else {
            $employees = $this->ScheduleModel->getAllEmployees();
        }
        $schedtype = $this->ScheduleModel->getschedtypenotnormal();
        if ($this->session->userdata('uid')) {
            $this->load->library('PHPExcel', NULL, 'excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            $this->excel->createSheet(1);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Schedule');
            $this->excel->getActiveSheet()->setShowGridlines(false);
            //------------------------INSERT LOGO-------------------------------------------
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($this->logo);
            $objDrawing->setOffsetX(0);    // setOffsetX works properly
            $objDrawing->setOffsetY(0);  //setOffsetY has no effect
            $objDrawing->setCoordinates('B1');
            $objDrawing->setHeight(100); // logo height
            // $objDrawing->setWidth(320); // logo width
            // $objDrawing->setWidthAndHeight(200,400);
            $objDrawing->setResizeProportional(true);
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            //----------------------------------------------------------------------
            $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38);
            $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(38);

            //set cell A1 content with some text
            $headerrow = 3;
            $headercol = 2;
            $letters = array();
            $this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(FALSE);
            $this->excel->getActiveSheet()->setCellValue('A1', 'CODE-SZScheduleTemplate');
            $this->excel->getActiveSheet()->setCellValue('A3', 'Profile Code');
            $this->excel->getActiveSheet()->setCellValue('B3', 'ID Number');
            $this->excel->getActiveSheet()->setCellValue('C3', 'Full Name');
            foreach ($dates as $d) {
                $headercol++;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headercol, $headerrow, $d);
                $this->excel->getActiveSheet()->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($headercol))->setWidth(23);
                array_push($letters, array('letter' => PHPExcel_Cell::stringFromColumnIndex($headercol), 'date' => $d));
            }
            $emprow = 4;
            foreach ($employees as $emp) {
                $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $emp->emp_id);
                $this->excel->getActiveSheet()->setCellValue('B' . $emprow, $emp->id_num);
                $this->excel->getActiveSheet()->setCellValue('C' . $emprow, $emp->lname . ', ' . $emp->fname);
                $list = explode(',', $emp->shift);
                $this->excel->setActiveSheetIndex(1);
                $this->excel->getActiveSheet()->setTitle('Reference');
                $cnt = 0;
                foreach ($list as $li) {
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($cnt, $emprow, $li);
                    $cnt++;
                }
                foreach ($schedtype as $sched) {

                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($cnt, $emprow, $sched->type);
                    $cnt++;
                }
                $emprow++;

                $this->excel->setActiveSheetIndex(0);
            }
            $emprow = 4;

            $column = $this->excel->getActiveSheet()->getHighestColumn();
            $row = $this->excel->getActiveSheet()->getHighestRow();
            $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
            $this->excel->getActiveSheet()->getProtection()->setSheet(true);
            $this->excel->getActiveSheet()->getStyle('D4:' . $column . '' . $row)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
            $this->excel->getActiveSheet()->freezePane('D4');

            foreach ($employees as $emp) {
                $start = 0;
                foreach ($letters as $l) {
                    if ($start == 0) {

                        $objValidation = $this->excel->getActiveSheet()->getCell($l['letter'] . '' . $emprow)->getDataValidation();
                        $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                        $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                        $objValidation->setAllowBlank(false);
                        $objValidation->setShowInputMessage(true);
                        $objValidation->setShowErrorMessage(true);
                        $objValidation->setShowDropDown(true);
                        $objValidation->setErrorTitle('Input error');
                        $objValidation->setError('Value is not in list.');
                        $objValidation->setPromptTitle('Pick from list');
                        $objValidation->setPrompt('Please pick a value from the drop-down list.');
                        $this->excel->setActiveSheetIndex(1);
                        $col = $this->excel->getActiveSheet()->getHighestDataColumn($emprow);
                        // $colletter = PHPExcel_Cell::stringFromColumnIndex($col);
                        // echo $col;
                        $this->excel->setActiveSheetIndex(0);
                        $objValidation->setFormula1('=\'Reference\'!$A$' . $emprow . ':$' . $col . '$' . $emprow);
                    } else {

                        $this->excel->getActiveSheet()->getCell($l['letter'] . '' . $emprow)->setDataValidation(clone $objValidation);
                    }
                    $formatteddate = date_create_from_format("l - M d, Y", $l['date']);
                    $schedule_data = $this->ScheduleModel->checkifExistSchedule2($emp->emp_id, $formatteddate->format('Y-m-d'));
                    // echo "<pre>";
                    // var_dump($formatteddate->format('Y-m-d'));
                    // echo "</pre>";
                    // echo $this->db->last_query().'<br><br>';
                    if (!empty($schedule_data)) {

                        if ($schedule_data->type == 'Normal') {

                            $this->excel->getActiveSheet()->setCellValue($l['letter'] . '' . $emprow, $schedule_data->time_start . ' - ' . $schedule_data->time_end);
                        } else if ($schedule_data->type == 'Leave-WoP' || $schedule_data->type == 'Leave-WP' ) {
                            $this->excel->getActiveSheet()->setCellValue($l['letter'] . '' . $emprow, $schedule_data->type);
                            $this->excel->getActiveSheet()->getStyle($l['letter'] . '' . $emprow)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                        } else {

                            $this->excel->getActiveSheet()->setCellValue($l['letter'] . '' . $emprow, $schedule_data->type);
                        }
                    }
                    $start++;
                }
                $emprow++;
            }

            //merge cell A1 until D1
            $this->excel->getActiveSheet()->mergeCells('B1:D2');
            //set aligment to center for that merged cell (A1 to D1)

            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(45);

            $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));

            $this->excel->getSheetByName('Reference')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
            /* echo "<pre>";
              var_dump($employees);
              echo "</pre>";
             */$filename = 'ScheduleTemplate.xlsx'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
//force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        } else {
            redirect('index.php/login');
        }
    }

    public function checkAll() {
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        $employees = $this->ScheduleModel->checkAll($fromDate, $toDate);
        $cnt = COUNT($employees);
        if ($cnt > 0) {
            echo json_encode($employees);
        } else {
            echo "Okay";
        }
    }

    public function checkPerType() {
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        $acc_description = $this->input->post('acc_description');
        $employees = $this->ScheduleModel->checkPerType($fromDate, $toDate, $acc_description);
        $cnt = COUNT($employees);
        if ($cnt > 0) {
            echo json_encode($employees);
        } else {
            echo "Okay";
        }
    }

    public function checkPerAccountEmployee() {
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        $employeelist = $this->input->post('employeelist');

        $employeelist = explode('-', $employeelist);
        $employees = $this->ScheduleModel->checkPerAccountEmployee($fromDate, $toDate, $employeelist);
        $cnt = COUNT($employees);
        if ($cnt > 0) {
            echo json_encode($employees);
        } else {
            echo "Okay";
        }
    }

    public function uploadsched() {

        $path = $_FILES["uploadFile"]["name"];
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        date_default_timezone_set('Asia/Manila');
        // to see if directory exists 

        $dir = "./reports/" . $this->session->userdata('emp_id') . "/";
        if (file_exists($dir) && is_dir($dir)) {
            // print $dir . ' is a directory!'; 
        } else {
            // print $dir . ' is not directory'; 
            mkdir($dir);
        }
        $new_name = "schedule";
        $target_file = $dir . $new_name . '.' . $extension;

        $allowed = array('xls', 'xlsx', 'csv');

        if ($_FILES["uploadFile"]["name"]) {
            if (!in_array($extension, $allowed)) {
                echo 'File type is not allowed.';
            } else {
                if (move_uploaded_file($_FILES["uploadFile"]["tmp_name"], $this->file)) {
                    echo 'Success';
                } else {
                    echo 'Error';
                }
            }
        }
    }

    public function viewuploadedschedules() {
        if ($this->session->userdata('uid')) {
            $this->load->library('PHPExcel', NULL, 'excel');

            $thisfile = $this->file;
            $objPHPExcel = PHPExcel_IOFactory::load($thisfile);
            $code = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
            if ($code == 'CODE-SZScheduleTemplate') {
                $maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
                $data = $objPHPExcel->getActiveSheet()->rangeToArray('C3:' . $maxCell['column'] . $maxCell['row']);

                if (empty($data)) {
                    echo json_encode(array('status' => 'Empty'));
                } else {
                    $arr = array('status' => "Success", 'data' => $data);
                    echo json_encode($arr);
                }
            } else {
//                    unlink($thisfile);
                echo json_encode(array('status' => 'Mismatch'));
            }
        } else {
            redirect('index.php/login');
        }
    }

    public function savemultiplesched() {
        $uid = $this->session->userdata('uid');
        if ($this->session->userdata('uid')) {
            $this->load->library('PHPExcel', NULL, 'excel');
            $thisfile = $this->file;
            $objPHPExcel = PHPExcel_IOFactory::load($thisfile);
            $maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
            $colnumber = PHPExcel_Cell::columnIndexFromString($maxCell['column']);
            $data = $objPHPExcel->getActiveSheet()->rangeToArray('A3:' . $maxCell['column'] . $maxCell['row']);

            $schedtype = $this->ScheduleModel->getschedtypenotnormal();

            for ($row = 1; $row < count($data); $row++) {
                for ($col = 3; $col < $colnumber; $col++) {
                    $schedchoice = $data[$row][$col];
                    if ($schedchoice == '' || $schedchoice == NULL) {

                    } else {
                        $where = "";
                        $date = date_create_from_format('l - M d, Y', $data[0][$col]);
                        $sched_date = date_format($date, 'Y-m-d');
                        $emp_id = $data[$row][0];
                        $schedtype_id = 1;
                        foreach ($schedtype as $type) {
                            if ($type->type == $schedchoice) {
                                $schedtype_id = $type->schedtype_id;
                            }
                        }
                        if ($schedtype_id == 4) { //WORD
                            $result = $this->ScheduleModel->getAccTimeIdWORD($emp_id);

                            $values = array(
                                'emp_id' => $emp_id,
                                'sched_date' => $sched_date,
                                'schedtype_id' => $schedtype_id,
                                'acc_time_id' => $result->acc_time_id,
                                'remarks' => "Excel",
                                'updated_by' => $uid,
                                'updated_on' => date('Y-m-d H:i:s')
                            );
//                            $where = "acc_time_id != $result->acc_time_id";
                        } else if ($schedtype_id != 1) { //NOT NORMAL NOT WORD
                            $values = array(
                                'emp_id' => $emp_id,
                                'sched_date' => $sched_date,
                                'schedtype_id' => $schedtype_id,
                                'acc_time_id' => NULL,
                                'remarks' => "Excel",
                                'updated_by' => $uid,
                                'updated_on' => date('Y-m-d H:i:s')
                            );
                        } else {    //NORMAL
                            $schedchoices = explode(' - ', $schedchoice);
                            $result = $this->ScheduleModel->getAccTimeId($emp_id, $schedchoices[0], $schedchoices[1]);
                            if (empty($result)) {
                                $values = array();
                                echo 'Not Shift';
                            } else {

                                $values = array(
                                    'emp_id' => $emp_id,
                                    'sched_date' => $sched_date,
                                    'schedtype_id' => $schedtype_id,
                                    'acc_time_id' => $result->acc_time_id,
                                    'remarks' => "Excel",
                                    'updated_by' => $uid,
                                    'updated_on' => date('Y-m-d H:i:s')
                                );

//                                $where = "acc_time_id != $result->acc_time_id";
                            }
                        }
                        if (empty($values)) {
                            echo " of Employee";
                        } else {
                            $schedule_data = $this->ScheduleModel->checkifExistSchedule($emp_id, $sched_date);
                            if (empty($schedule_data)) {
                                $this->ScheduleModel->savemultiplesched($values);
                                echo 'save';
                            } else {
                                $this->ScheduleModel->updatemultiplesched($values, $schedule_data->sched_id, $where);
//                                echo 'update';
//                                echo $this->db->affected_rows
                                echo $this->db->last_query();
                            }
                        }
                    }
                }
            }
        } else {
            redirect('index.php/login');
        }
    }

    public function getSchedAll() {
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        // $fromDate = '2017-06-30';$toDate = '2017-07-12';
        $employees = $this->ScheduleModel->getSchedAllNames();
        foreach ($employees as $emp) {
            $emp->schedule = $this->ScheduleModel->getSchedofEmp($fromDate, $toDate, $emp->emp_id);
            // $emp->times = $this->ScheduleModel->getSchedTimeEmp($emp->emp_id);
        }

        echo json_encode($employees);
    }

    public function getSchedPerType() {
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        $acc_description = $this->input->post('acc_description');

        $employees = $this->ScheduleModel->getSchedPerTypeNames($acc_description);

        foreach ($employees as $emp) {
            $emp->schedule = $this->ScheduleModel->getSchedofEmp($fromDate, $toDate, $emp->emp_id);
            // $emp->times = $this->ScheduleModel->getSchedTimeEmp($emp->emp_id);
        }
        echo json_encode($employees);
    }

    public function getSchedPerAccountEmployee() {
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        $employeelist = $this->input->post('employeelist');
        $employeelist = explode('-', $employeelist);
        $employees = $this->ScheduleModel->getSchedPerAccountNames($employeelist);

        foreach ($employees as $emp) {
            $emp->schedule = $this->ScheduleModel->getSchedofEmp($fromDate, $toDate, $emp->emp_id);

            // $emp->times = $this->ScheduleModel->getSchedTimeEmp($emp->emp_id);
        }
        echo json_encode($employees);
        // echo "<pre>";
        // var_dump($employees);
        // echo "</pre>";
    }

    public function generatedate() {
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        $period = new DatePeriod(
            new DateTime("$fromDate"), new DateInterval('P1D'), new DateTime("$toDate +1 day ")
        );
        foreach ($period as $date) {
            $dates[] = array('dates' => $date->format("l - M d, Y"));
        }
        echo json_encode($dates);
    }

    public function getCalendarEmpSched() {
        $uid = $this->input->post('uid');
        $result = $this->ScheduleModel->getCalendarEmpSched($uid);
        $calendarsched = array();
        foreach ($result as $res) {
            $startdate = $res->sched_date . ' ' . $res->time_start;
            $enddate = $res->sched_date . ' ' . $res->time_end;

            $sdate = date(DATE_ISO8601, strtotime($startdate));
            // $edate = date(DATE_ISO8601, strtotime($enddate));
            // array_push($calendarsched,array('start'=>$sdate,'end'=>$edate,'title'=>$startdate.' - '.$enddate));
            if ($res->time_start == '' || $res->time_end == null) {
                $type = explode("-",$res->type);
                $leave = ($type[0]=="Leave") ? "( ".$res->leaveType." )" : "";
                if ($res->leaveType == NULL) {
                    array_push($calendarsched, array('start' => $sdate, 'title' => "$type[0]", 'backgroundColor' => $res->style));
                } else {
                    array_push($calendarsched, array('start' => $sdate, 'title' => "$type[0]  $leave", 'backgroundColor' => $res->style));
                }
            } else {
                array_push($calendarsched, array('start' => $sdate, 'title' => $res->time_start . " - " . $res->time_end, 'backgroundColor' => $res->style));
            }
        }
        // echo "<pre>";
        // var_dump($calendarsched);
        // echo "</pre>";
        echo json_encode($calendarsched);
    }

    public function getTimeSched() {
        $emp_id = $this->input->post('emp_id');
        $result["shift"] = $this->ScheduleModel->getTimeSched($emp_id);
        $result["schedType"] = $this->ScheduleModel->getCalendarLegend();
        echo json_encode($result);
    }

    public function getTimeSched2() {
        $emp_id = $this->input->post('emp_id');
        $result = $this->ScheduleModel->getTimeSched2($emp_id);
        echo json_encode($result);
    }

    public function addDoubleShiftSched() {
        $emp_id = $this->input->post('emp_id');
        $shift = $this->input->post('shift');
        $switchstate = $this->input->post('switchstate');
        if ($switchstate == "true") {
            $ss = 4;
        } else {
            $ss = 5;
        }
        $date = date_create($this->input->post('date'));
        $date = date_format($date, "Y-m-d");

        $result = $this->ScheduleModel->addDoubleShiftSched($emp_id, $shift, $date, $ss);
          // echo  $emp_id." ".$shift." ".$sched_id." ".$ss." ".$switchstate." ".$date;

        if ($result > 0) {
            echo $date;
        } else {
            echo "0";
        }
    }

    public function updateDoubleShiftSched() {
        $emp_id = $this->input->post('emp_id');
        $acc_time_id = $this->input->post('shift');
        $sched_id = $this->input->post('sched_id');
        $switchstate = $this->input->post('switchstate');
        if ($switchstate == "true") {
            $ss = 4;
        } else {
            $ss = 5;
        }

        $result = $this->ScheduleModel->updateDoubleShiftSched($emp_id, $acc_time_id, $sched_id, $ss);
        if ($result > 0) {
            echo "1";
        } else {
            echo "0";
            // echo  $emp_id." ".$acc_time_id." ".$sched_id." ".$ss." ".$switchstate;
        }
    }

    public function deleteDoubleshift() {
        $val = $this->input->post('val');

        $shift = $this->input->post('shift');
        if (!empty($shift)) {
            $result = $this->ScheduleModel->checkdeleteDoubleshift($shift);
            if (count($result) > 0) {
                echo "1";
            } else {
                $result2 = $this->ScheduleModel->deleteDoubleshift($shift, $val);
                echo "0";
            }
        } else {
            $result2 = $this->ScheduleModel->deleteDoubleshift($shift, $val);
            echo "0";
        }
    }

    public function confirmdoubleshift() {
        $emp_id = $this->input->post('emp_id');
        $schedule = explode("-", $this->input->post('schedule'));
        $date = date_create($schedule[1]);
        $date = date_format($date, "Y-m-d ");
        $result["doubleSched"] = $this->ScheduleModel->confirmdoubleshift($emp_id, $date);
        $result["shift"] = $this->ScheduleModel->getTimeSched($emp_id);

        echo json_encode($result);
    }

    public function updatesched() {
        $schedid = $this->input->post('schedid');
        $schedtype_id = $this->input->post('switchstate');
        $acc_time_id = $this->input->post('acc_time_id');
        $emp_id = $this->input->post('emp_id');
        $scheddate = $this->input->post('scheddate');
        $leave_id = $this->input->post('leave_id');
        $date = date_create_from_format("l - M d, Y", $scheddate);
        $scheddate = date_format($date, "Y-m-d");
        $row = $this->ScheduleModel->updatesched($leave_id, $schedid, $acc_time_id, $schedtype_id, $scheddate, $emp_id, $this->session->userdata('uid'));
        if ($row > 0) {
            echo json_encode($this->ScheduleModel->getSchedofEmp($scheddate, $scheddate, $emp_id));
        } else {
            echo 'Failed';
        }
    }

    public function insertsched() {
        $schedtype_id = $this->input->post('switchstate');
        $acc_time_id = $this->input->post('acc_time_id');
        $emp_id = $this->input->post('emp_id');
        $scheddate = $this->input->post('scheddate');
        $leave_id = $this->input->post('leave_id');
        // echo $schedtype_id.' | '.$acc_time_id.' | '.$emp_id.' | '.$scheddate.' | '.$leave_id;
        $date = date_create_from_format("l - M d, Y", $scheddate);
        $scheddate = date_format($date, "Y-m-d");
        $row = $this->ScheduleModel->insertsched($leave_id, $acc_time_id, $schedtype_id, $scheddate, $emp_id);
        // echo $row;
        if ($row > 0) {
            echo json_encode($this->ScheduleModel->getSchedofEmp($scheddate, $scheddate, $emp_id));
        } else {
            echo 'Failed';
        }
    }

    public function getSupervisoryAllEmp() {
        $uid = $this->session->userdata('uid');
        $empid = $this->session->userdata('emp_id');
        $description = $this->session->userdata('description');
        $level1 = $this->ScheduleModel->getSupervisoryEmpLevel1($uid);

        $manager = $this->ScheduleModel->insertManagerSched($empid);
        if (empty($level1)) {
            $results = array();
            if ($description == "Manager") {
                $results = array_merge($results, $manager);
                echo json_encode($results);
            }else{
             echo 'Empty';
         }
     } else {
        $results = array();
        $results = array_merge($results, $level1);
        foreach ($level1 as $l) {
            $emps[] = $l->emp_id;
        }

        $data = $this->ScheduleModel->getSupervisoryEmpNext($emps);
        $results = array_merge($results, $data);
        $data2 = $data;
        if (!empty($data)) {
            jumper:
            foreach ($data2 as $d2) {
                $emps2[] = $d2->emp_id;
            }
            $data2 = $this->ScheduleModel->getSupervisoryEmpNext($emps2);
            if (empty($data2)) {
                    // echo 'Recurring Empty';
            } else {
                $results = array_merge($results, $data2);
                unset($emps2);
                goto jumper;
            }
        } else {
                // echo 'Empty2';
        }


        if ($description == "Manager") {
            $results = array_merge($results, $manager);
        }
        usort($results, array($this, "cmp"));
        $results = array_reverse($results);
        echo json_encode($results);
    }
        // echo "<pre>";
        // var_dump($results);
        // echo "</pre>";
}

public function getSupervisoryAdminEmp() {
    $uid = $this->session->userdata('uid');
    $description = $this->session->userdata('description');
    $empid = $this->session->userdata('emp_id');
    $level1 = $this->ScheduleModel->getSupervisoryEmpLevel1($uid);
    $manager = $this->ScheduleModel->insertManagerSched($empid);

    if (empty($level1)) {
        $results = array();
        if ($description == "Manager") {
            $results = array_merge($results, $manager);
            echo json_encode($results);
        }else{
         echo 'Empty';
     }

 } else {
    foreach ($level1 as $tempd0) {
        if ($tempd0->acc_description == 'Admin') {
            $temp0[] = $tempd0;
        }
    }
    $results = array();
    if (!empty($temp0)) {
        $results = array_merge($results, $temp0);
    }
    foreach ($level1 as $l) {
        $emps[] = $l->emp_id;
    }

    $data = $this->ScheduleModel->getSupervisoryEmpNext($emps);
    foreach ($data as $tempd) {
        if ($tempd->acc_description == 'Admin') {
            $temp[] = $tempd;
        }
    }
    if (!empty($temp)) {

        $results = array_merge($results, $temp);
    }
    $data2 = $data;
    if (!empty($data)) {
        jumper:
        foreach ($data2 as $d2) {
            $emps2[] = $d2->emp_id;
        }
        $data2 = $this->ScheduleModel->getSupervisoryEmpNext($emps2);

        if (empty($data2)) {
                    // echo 'Recurring Empty';
        } else {
            foreach ($data2 as $tempd2) {
                if ($tempd2->acc_description == 'Admin') {
                    $temp2[] = $tempd2;
                }
            }
            if (!empty($temp2)) {

                $results = array_merge($results, $temp2);
            }
            unset($emps2);
            goto jumper;
        }
    } else {
                // echo 'Empty2';
    }

    if ($description == "Manager") {
        $results = array_merge($results, $manager);
    }


    usort($results, array($this, "cmp"));
    $results = array_reverse($results);
    echo json_encode($results);
}
        // echo "<pre>";
        // var_dump($results);
        // echo "</pre>";
}

public function getSupervisoryAgentEmp() {
	ini_set('max_execution_time', 300);
    $uid = $this->session->userdata('uid');
    $role = explode("|", $this->session->userdata('role'));
    if ($role[0] == "User Level 1") {
        $level1 = $this->ScheduleModel->getSupervisoryEmpLevel1AgentULevel1($uid);
    } else {
        $level1 = $this->ScheduleModel->getSupervisoryEmpLevel1($uid);
    }

    if (empty($level1)) {
        echo 'Empty';
    } else {
        foreach ($level1 as $tempd0) {
            if ($tempd0->acc_description == 'Agent') {
                $temp0[] = $tempd0;
            }
        }
        $results = array();
        if (!empty($temp0)) {
            $results = array_merge($results, $temp0);
        }
        foreach ($level1 as $l) {
            $emps[] = $l->emp_id;
        }

        $data = $this->ScheduleModel->getSupervisoryEmpNext($emps);
        foreach ($data as $tempd) {
            if ($tempd->acc_description == 'Agent') {
                $temp[] = $tempd;
            }
        }
        if (!empty($temp)) {

            $results = array_merge($results, $temp);
        }
        $data2 = $data;
        if (!empty($data)) {
            jumper:
            foreach ($data2 as $d2) {
                $emps2[] = $d2->emp_id;
            }
            $data2 = $this->ScheduleModel->getSupervisoryEmpNext($emps2);

            if (empty($data2)) {
                    // echo 'Recurring Empty';
            } else {
                foreach ($data2 as $tempd2) {
                    if ($tempd2->acc_description == 'Agent') {
                        $temp2[] = $tempd2;
                    }
                }
                if (!empty($temp2)) {

                    $results = array_merge($results, $temp2);
                }
                unset($emps2);
                goto jumper;
            }
        } else {
                // echo 'Empty2';
        }
        usort($results, array($this, "cmp"));
        $results = array_reverse($results);
        echo json_encode($results);
    }
        // echo "<pre>";
        // var_dump($results);
        // echo "</pre>";
}

function cmp($a, $b) {
    return strcmp($a->lname, $b->lname);
}

function cmp2($a, $b) {
    if ($a->lname == $b->lname) {
        return $a->dtr_id_in - $b->dtr_id_in;
    }
    return strcmp($a->lname, $b->lname);
}

public function schedule_dtr($key=null) {
    $roles = explode('|', $this->session->userdata('role'));
    $role = $roles[0];
    $data['role'] = $role;

    if($key=="key"){
	   $data['page'] = "schedule/schedule_logs";
       $this->load->view('schedule_dtr', $data);
   }else{ 
       $data['page'] = "schedule/schedule_logs";
       $this->load->view('forbidden', $data);
   }


}

public function getSchedAllDTR() {
    $fromDate = $this->input->post('fromDate');
    $toDate = $this->input->post('toDate');
        // $fromDate = "2017-07-01";
        // $toDate = "2017-07-09";
    $dtrin = $this->ScheduleModel->getAllDTRin($fromDate, $toDate);
    $dtrout = $this->ScheduleModel->getAllDTRout($fromDate, $toDate);
    foreach ($dtrin as $din) {

        $naa = false;
        foreach ($dtrout as $dout) {
            if ($din->dtr_id_in == $dout->note) {
                $din->dtr_id_out = $dout->dtr_id_out;
                $din->sched_id_out = $dout->sched_id_out;
                $din->log_out = $dout->log_out;
                $naa = true;
            }
        }
        if ($naa == false) {
            $din->dtr_id_out = null;
            $din->log_out = null;
        }
        $name = $this->ScheduleModel->getEmployeeName($din->emp_id);
        $din->lname = $name->lname;
        $din->fname = $name->fname;
    }
    foreach ($dtrin as $din) {
        $startdate = strtotime("-5 day", strtotime($din->log_in));
        $enddate = strtotime("+5 day", strtotime($din->log_in));
        $startdate = date("Y-m-d", $startdate);
        $enddate = date("Y-m-d", $enddate);
        $values = $this->ScheduleModel->getSchedofEmp2($startdate, $enddate, $din->emp_id);
        if (empty($values)) {
            $din->availablesched = NULL;
        } else {
            $din->availablesched = $values;
        }
    }

    usort($dtrin, array($this, "cmp2"));
    echo json_encode($dtrin);
}

public function getSchedPerTypeDTR() {
    $fromDate = $this->input->post('fromDate');
    $toDate = $this->input->post('toDate');
    $acc_description = $this->input->post('acc_description');
        // $fromDate = "2017-07-01";
        // $toDate = "2017-07-09";
        // $acc_description = 'Admin';
    $dtrin = $this->ScheduleModel->getAllDTRin($fromDate, $toDate);
    $dtrout = $this->ScheduleModel->getAllDTRout($fromDate, $toDate);
    $employees = $this->ScheduleModel->getAllEmpIdPerType($acc_description);
    foreach ($dtrin as $key => $din) {
        $naa = false;
        foreach ($employees as $emp) {
            if ($din->emp_id == $emp->emp_id) {
                $naa = true;
            }
        }
        if ($naa == false) {
            unset($dtrin[$key]);
        }
    }
    foreach ($dtrin as $din) {

        $naa = false;
        foreach ($dtrout as $dout) {
            if ($din->dtr_id_in == $dout->note) {
                $din->dtr_id_out = $dout->dtr_id_out;
                $din->sched_id_out = $dout->sched_id_out;
                $din->log_out = $dout->log_out;
                $naa = true;
            }
        }
        if ($naa == false) {
            $din->dtr_id_out = null;
            $din->log_out = null;
        }
        $name = $this->ScheduleModel->getEmployeeName($din->emp_id);
        $din->lname = $name->lname;
        $din->fname = $name->fname;
        ;
    }

    foreach ($dtrin as $din) {
        $startdate = strtotime("-5 day", strtotime($din->log_in));
        $enddate = strtotime("+5 day", strtotime($din->log_in));
        $startdate = date("Y-m-d", $startdate);
        $enddate = date("Y-m-d", $enddate);
        $values = $this->ScheduleModel->getSchedofEmp2($startdate, $enddate, $din->emp_id);
        if (empty($values)) {
            $din->availablesched = NULL;
        } else {
            $din->availablesched = $values;
        }
    }

    usort($dtrin, array($this, "cmp2"));
    echo json_encode($dtrin);
}

public function getSchedPerAccountEmployeeDTR() {
    $fromDate = $this->input->post('fromDate');
    $toDate = $this->input->post('toDate');
    $employeelist = $this->input->post('employeelist');
    $employeelist = explode('-', $employeelist);
    $dtrin = $this->ScheduleModel->getAllDTRin($fromDate, $toDate);
    $dtrout = $this->ScheduleModel->getAllDTRout($fromDate, $toDate);
    $employees = $this->ScheduleModel->getAllEmpIdPerAccountEmp($employeelist);
    foreach ($dtrin as $key => $din) {
        $naa = false;
        foreach ($employees as $emp) {
            if ($din->emp_id == $emp->emp_id) {
                $naa = true;
            }
        }
        if ($naa == false) {
            unset($dtrin[$key]);
        }
    }
    foreach ($dtrin as $din) {

        $naa = false;
        foreach ($dtrout as $dout) {
            if ($din->dtr_id_in == $dout->note) {
                $din->dtr_id_out = $dout->dtr_id_out;
                $din->sched_id_out = $dout->sched_id_out;
                $din->log_out = $dout->log_out;
                $naa = true;
            }
        }
        if ($naa == false) {
            $din->dtr_id_out = null;
            $din->log_out = null;
        }
        $name = $this->ScheduleModel->getEmployeeName($din->emp_id);
        $din->lname = $name->lname;
        $din->fname = $name->fname;
        ;
    }

    foreach ($dtrin as $din) {
        $startdate = strtotime("-5 day", strtotime($din->log_in));
        $enddate = strtotime("+5 day", strtotime($din->log_in));
        $startdate = date("Y-m-d", $startdate);
        $enddate = date("Y-m-d", $enddate);
        $values = $this->ScheduleModel->getSchedofEmp2($startdate, $enddate, $din->emp_id);
        if (empty($values)) {
            $din->availablesched = NULL;
        } else {
            $din->availablesched = $values;
        }
    }
    usort($dtrin, array($this, "cmp2"));
    echo json_encode($dtrin);
}

public function schedupdaterdtr() {
    $sched_id = $this->input->post('sched_id');
    $dtr_id = $this->input->post('dtr_id');
    $res = $this->ScheduleModel->getAccTimeIdFromSched($sched_id);
    $acc_time_id = $res->acc_time_id;
    $row = $this->ScheduleModel->schedupdaterdtr($sched_id, $dtr_id, $acc_time_id);
    if ($row > 0) {
        echo 'Success';
    } else {
        echo 'Failed';
    }
}

public function schedupdaterdtr2() {
    $dtr_id_in = $this->input->post('dtr_id');
    $row = $this->ScheduleModel->schedupdaterdtr(NULL, $dtr_id_in, NULL);
    if ($row > 0) {
        echo 'Success';
    } else {
        echo 'Failed';
    }
}

public function defaultaccounts() {
    $this->load->view('defaultaccounts');
}

public function getAccountsSched() {
    $acc_description = $this->input->post('acc_description');
    $account = $this->ScheduleModel->getAccountsList($acc_description);
    foreach ($account as $acc) {
        $acc->shifts = $this->ScheduleModel->getAccountSched($acc->acc_id);
    }
    echo json_encode($account);
}

public function changeaccountdefault() {
    $acc_time_id = $this->input->post('acc_time_id');
    $row = $this->ScheduleModel->changeaccountdefault($acc_time_id);
    if ($row > 0) {
        echo 'Success';
    } else {
        echo 'Failed';
    }
}

public function account() {
    $data['accounts'] = $this->ScheduleModel->getAccountList();
    $data['departments'] = $this->ScheduleModel->getDepartments();
    $data['schedules'] = $this->ScheduleModel->getScheduleList();
    $this->load->view('account', $data);
}

public function getAccountList() {
    $data = $this->ScheduleModel->getAccountList();
    echo json_encode($data);
}

public function createAccount() {
    $acc_name = $this->input->post('acc_name');
    $acc_description = $this->input->post('acc_description');
    $dep_id = $this->input->post('dep_id');
    $time_id = $this->input->post('time_id');
    $uacc_level = $this->input->post('uacc_level');
    $uacc_bct = $this->input->post('uacc_bct');
    $uacc_reference = $this->input->post('uacc_reference');
    $acc_id = $this->ScheduleModel->createAccount($acc_name, $acc_description, $dep_id,$uacc_level,$uacc_bct,$uacc_reference);

    if (empty($acc_id) || $acc_id == NULL) {
        echo "Failed";
    } else {
        $isokay = true;
        $times = $this->ScheduleModel->getAllTime();
        foreach ($times as $time) {
            $acc_time_id = $this->ScheduleModel->createAccTime($acc_id, $time->time_id);
            if ($acc_time_id !== '' || $acc_time_id !== NULL) {
                if ($acc_description == 'Admin') {
                    $rowl = $this->ScheduleModel->insertShiftBreak($acc_time_id, 5);
                    if ($rowl > 0) {
//                        $isokay = true;
                    } else {
                        $isokay = false;
                    }
                } else {
                    $rowfb = $this->ScheduleModel->insertShiftBreak($acc_time_id, 3);
                    $rowl = $this->ScheduleModel->insertShiftBreak($acc_time_id, 4);
                    $rowlb = $this->ScheduleModel->insertShiftBreak($acc_time_id, 2);
                    if ($rowfb > 0 || $rowl > 0 || $rowlb > 0) {
//                        $isokay = true;
                    } else {
                        $isokay = false;
                    }
                }
            } else {
                $isokay = false;
            }
        }
        $row = $this->ScheduleModel->assignDefaultSched($time_id, $acc_id);
        if ($row > 0 && $isokay == true) {
            echo "Success";
        } else if ($isokay == false && $row > 0) {
            echo "Check";
        } else {
            echo "Failed";
        }
    }
}


public function getAccountSpecific() {
    $acc_id = $this->input->post('acc_id');
    $data = $this->ScheduleModel->getAccountSpecific($acc_id);
    echo json_encode($data);
} 
public function getAllAccount() {
    $data = $this->ScheduleModel->getAllAccount();
    echo json_encode($data);
}

public function updateAccount() {
    $acc_name = $this->input->post('acc_name');
    $acc_description = $this->input->post('acc_description');
    $dep_id = $this->input->post('dep_id');
    $time_id = $this->input->post('time_id');
    $uacc_level = $this->input->post('uacc_level');
    $uacc_bct = $this->input->post('uacc_bct');
    $uacc_reference = $this->input->post('uacc_reference');
    $acc_id = $this->input->post('acc_id');
    $row = $this->ScheduleModel->updateAccount($acc_id, $acc_name, $acc_description, $dep_id,$uacc_level,$uacc_bct,$uacc_reference);
    $details = $this->ScheduleModel->getAccountSpecific($acc_id);
    if ($details->time_id != $time_id) {
        $values = $this->ScheduleModel->getAccTimeDetails($acc_id, $time_id);
        if (empty($values)) {
            $this->ScheduleModel->updateDefaultSched($details->acc_time_id);
            $row2 = $this->ScheduleModel->assignDefaultSched($time_id, $acc_id);
        } else {
            $this->ScheduleModel->updateDefaultSched($details->acc_time_id);
            $row2 = $this->ScheduleModel->reassignDefaultSched($values->acc_time_id);
        }
        if ($row > 0 || $row2 > 0) {
            echo "Success";
        } else {
            echo "Failed";
        }
    } else {
        if ($row > 0) {
            echo "Success";
        } else {
            echo "Failed";
        }
    }
}

    //-----------------------------NICCA CODES-------------------------------------------------------------------------------

public function adjustAttendancelogs() {
    $this->load->view('adjust_attendance_logs');
}

public function getLogsAllDTR() {
    $fromDate = $this->input->post('fromDate');
    $toDate = $this->input->post('toDate');
        // $fromDate = "2017-10-01";
        // $toDate = "2017-10-08";
    $dtrin = $this->ScheduleModel->getAllDTRinLogs($fromDate, $toDate);
    $dtrout = $this->ScheduleModel->getAllDTRoutLogs($fromDate, $toDate);
    foreach ($dtrin as $din) {

        $naa = false;
        foreach ($dtrout as $dout) {
            if ($din->dtr_id_in == $dout->note) {
                $din->dtr_id_out = $dout->dtr_id_out;
                $din->log_out_date = $dout->log_out_date;
                $din->log_out_time = $dout->log_out_time;
                $naa = true;
            }
        }
        if ($naa == false) {
            $din->dtr_id_out = null;
            $din->log_out_date = null;
            $din->log_out_time = null;
        }
        $name = $this->ScheduleModel->getEmployeeName($din->emp_id);
        $din->lname = $name->lname;
        $din->fname = $name->fname;
        if ($din->sched_id == NULL || $din->sched_id == '') {
            $din->sched_time = NULL;
        } else {
            $values = $this->ScheduleModel->getSched($din->sched_id);
            if ($values == '') {
                $din->sched_time = NULL;
            } else {
                $din->sched_time = $values->sched_time;
            }
        }
    }
    usort($dtrin, array($this, "cmp2"));
    echo json_encode($dtrin);
}

public function getLogsPerTypeDTR() {
    $fromDate = $this->input->post('fromDate');
    $toDate = $this->input->post('toDate');
    $acc_description = $this->input->post('acc_description');
        // $fromDate = "2017-10-01";
        // $toDate = "2017-10-09";
        // $acc_description = 'Admin';
    $dtrin = $this->ScheduleModel->getAllDTRinLogs($fromDate, $toDate);
    $dtrout = $this->ScheduleModel->getAllDTRoutLogs($fromDate, $toDate);
    $employees = $this->ScheduleModel->getAllEmpIdPerType($acc_description);
    foreach ($dtrin as $key => $din) {
        $naa = false;
        foreach ($employees as $emp) {
            if ($din->emp_id == $emp->emp_id) {
                $naa = true;
            }
        }
        if ($naa == false) {
            unset($dtrin[$key]);
        }
    }
    foreach ($dtrin as $din) {

        $naa = false;
        foreach ($dtrout as $dout) {
            if ($din->dtr_id_in == $dout->note) {
                $din->dtr_id_out = $dout->dtr_id_out;
                $din->log_out_date = $dout->log_out_date;
                $din->log_out_time = $dout->log_out_time;
                $naa = true;
            }
        }
        if ($naa == false) {
            $din->dtr_id_out = null;
            $din->log_out_date = null;
            $din->log_out_time = null;
        }
        $name = $this->ScheduleModel->getEmployeeName($din->emp_id);
        $din->lname = $name->lname;
        $din->fname = $name->fname;
        if ($din->sched_id == NULL || $din->sched_id == '') {
            $din->sched_time = NULL;
        } else {
            $values = $this->ScheduleModel->getSched($din->sched_id);
            if ($values == '') {
                $din->sched_time = NULL;
            } else {
                $din->sched_time = $values->sched_time;
            }
        }
    }

    usort($dtrin, array($this, "cmp2"));
    echo json_encode($dtrin);
}

public function getLogsPerAccountEmployeeDTR() {
    $fromDate = $this->input->post('fromDate');
    $toDate = $this->input->post('toDate');
    $employeelist = $this->input->post('employeelist');
    $employeelist = explode('-', $employeelist);
    $dtrin = $this->ScheduleModel->getAllDTRinLogs($fromDate, $toDate);
    $dtrout = $this->ScheduleModel->getAllDTRoutLogs($fromDate, $toDate);
    $employees = $this->ScheduleModel->getAllEmpIdPerAccountEmp($employeelist);
    foreach ($dtrin as $key => $din) {
        $naa = false;
        foreach ($employees as $emp) {
            if ($din->emp_id == $emp->emp_id) {
                $naa = true;
            }
        }
        if ($naa == false) {
            unset($dtrin[$key]);
        }
    }
    foreach ($dtrin as $din) {

        $naa = false;
        foreach ($dtrout as $dout) {
            if ($din->dtr_id_in == $dout->note) {
                $din->dtr_id_out = $dout->dtr_id_out;
                $din->log_out_date = $dout->log_out_date;
                $din->log_out_time = $dout->log_out_time;
                $naa = true;
            }
        }
        if ($naa == false) {
            $din->dtr_id_out = null;
            $din->log_out_date = null;
            $din->log_out_time = null;
        }
        $name = $this->ScheduleModel->getEmployeeName($din->emp_id);
        $din->lname = $name->lname;
        $din->fname = $name->fname;
        if ($din->sched_id == NULL || $din->sched_id == '') {
            $din->sched_time = NULL;
        } else {
            $values = $this->ScheduleModel->getSched($din->sched_id);
            if ($values == '') {
                $din->sched_time = NULL;
            } else {
                $din->sched_time = $values->sched_time;
            }
        }
    }
    usort($dtrin, array($this, "cmp2"));
    echo json_encode($dtrin);
}

public function updateDtrLogs() {
    $uid = $this->session->userdata('uid');
    $newtime = $this->input->post('newtime');
    $dtr_id = $this->input->post('dtr_id');
    $currentdate = date('Y-m-d H:i:s');
    $newtime = date("H:i:s", strtotime($newtime));
    $log = $this->ScheduleModel->getDTRLog($dtr_id);
    if (empty($log)) {
        echo "Failed";
    } else {
        $row = $this->ScheduleModel->insertDtrHistory($log->log, $dtr_id, $currentdate, $uid);
        if ($row > 0) {
            $row2 = $this->ScheduleModel->updateDtrLogs($newtime, $dtr_id);
            if ($row2 > 0) {
                echo "Success";
            } else {
                echo "Failed";
            }
        } else {
            echo "Failed";
        }
    }
}

//TEAM SCHEDULE CODES -----------------------------------------------

public function getAllTeam() {
    $uid = $this->session->userdata('uid');
    $acc = $this->ScheduleModel->getAccountUser($uid);
    echo json_encode($this->ScheduleModel->getTeamAll($acc->acc_id));
}

public function getTeamAdminEmp() {
    $uid = $this->session->userdata('uid');
    $acc = $this->ScheduleModel->getAccountUser($uid);
    echo json_encode($this->ScheduleModel->getTeamDesc($acc->acc_id, 'Admin'));
}

public function getTeamAgentEmp() {
    $uid = $this->session->userdata('uid');
    $acc = $this->ScheduleModel->getAccountUser($uid);
    echo json_encode($this->ScheduleModel->getTeamDesc($acc->acc_id, 'Agent'));
}

//-----------------------------------START OF SHIFTS--------------------------------------------------------------------------
public function accountshift() {
    $data['admins'] = json_encode($this->ScheduleModel->getAdminAccounts());
    $data['agents'] = json_encode($this->ScheduleModel->getAgentAccounts());
    $this->load->view('accountshift', $data);
}

public function getAccShifts() {
    $acc_id = $this->input->post('acc_id');
    $data = $this->ScheduleModel->getAccShifts($acc_id);
    foreach ($data as $d) {
        $d->firstbreak = $this->ScheduleModel->getAccTimeBreak($d->acc_time_id, 'FIRST BREAK');
        $d->lunchbreak = $this->ScheduleModel->getAccTimeBreak($d->acc_time_id, 'LUNCH');
        $d->lastbreak = $this->ScheduleModel->getAccTimeBreak($d->acc_time_id, 'LAST BREAK');
    }
    echo json_encode($data);
}

public function getAccTimeBreak() {
    $acc_time_id = $this->input->post('acc_time_id');
    $fb = $this->ScheduleModel->getAccTimeBreak($acc_time_id, 'FIRST BREAK');
    $l = $this->ScheduleModel->getAccTimeBreak($acc_time_id, 'LUNCH');
    $lb = $this->ScheduleModel->getAccTimeBreak($acc_time_id, 'LAST BREAK');
    $data = array('firstbreak' => $fb, 'lunchbreak' => $l, 'lastbreak' => $lb);
    echo json_encode($data);
}

public function getAvailableAccTimeBreak() {
    $fb = $this->ScheduleModel->getAvailableAccTimeBreak('FIRST BREAK');
    $l = $this->ScheduleModel->getAvailableAccTimeBreak('LUNCH');
    $lb = $this->ScheduleModel->getAvailableAccTimeBreak('LAST BREAK');
    $data = array('firstbreak' => $fb, 'lunchbreak' => $l, 'lastbreak' => $lb);
    echo json_encode($data);
}

public function updateAccTimeBreak() {
    $fb_bta = $this->input->post('fb');
    $l_bta = $this->input->post('l');
    $lb_bta = $this->input->post('lb');
    $acc_time_id = $this->input->post('acc_time_id');
    if ($fb_bta == '' && $l_bta == '' && $lb_bta == '') {
        $row = $this->ScheduleModel->resetShiftBreak($acc_time_id);
        if ($row > 0) {
            echo "Success";
        } else {
            echo "Failed";
        }
    } else {
        $this->ScheduleModel->resetShiftBreak($acc_time_id);
        $isfb = $this->ScheduleModel->getShiftBreak($fb_bta, $acc_time_id);
        $isl = $this->ScheduleModel->getShiftBreak($l_bta, $acc_time_id);
        $islb = $this->ScheduleModel->getShiftBreak($lb_bta, $acc_time_id);

        if ($isfb == NULL || $isfb == '') {
            $rowfb = $this->ScheduleModel->insertShiftBreak($acc_time_id, $fb_bta);
        } else {
            $rowfb = $this->ScheduleModel->updateShiftBreak($isfb->sbrk_id);
        }
        if ($isl == NULL || $isl == '') {
            $rowl = $this->ScheduleModel->insertShiftBreak($acc_time_id, $l_bta);
        } else {
            $rowl = $this->ScheduleModel->updateShiftBreak($isl->sbrk_id);
        }
        if ($islb == NULL || $islb == '') {
            $rowlb = $this->ScheduleModel->insertShiftBreak($acc_time_id, $lb_bta);
        } else {
            $rowlb = $this->ScheduleModel->updateShiftBreak($islb->sbrk_id);
        }

        if ($rowfb > 0 || $rowl > 0 || $rowlb > 0) {
            echo "Success";
        } else {
            echo "Failed";
        }
    }
}

public function generateAccTimeDetails() {
    $fb_bta = $this->input->post('fb');
    $l_bta = $this->input->post('l');
    $lb_bta = $this->input->post('lb');
    $time_start = $this->input->post('time_start');
    $time_end = $this->input->post('time_end');

    $ts = explode(" ", $time_start);
    $en = explode(" ", $time_end);
    $time_start = "$ts[0]:00 $ts[1]";
    $time_end = "$en[0]:00 $en[1]";

    $time_id = $this->ScheduleModel->createTime($time_start, $time_end);
    if ($time_id !== '' || $time_id !== NULL) {
        $accounts = $this->ScheduleModel->getAllAccount();
        $isokay = true;
        foreach ($accounts as $acc) {
            $acc_time_id = $this->ScheduleModel->createAccTime($acc->acc_id, $time_id);
            if ($acc_time_id !== '' || $acc_time_id !== NULL) {
                $rowfb = $this->ScheduleModel->insertShiftBreak($acc_time_id, $fb_bta);
                $rowl = $this->ScheduleModel->insertShiftBreak($acc_time_id, $l_bta);
                $rowlb = $this->ScheduleModel->insertShiftBreak($acc_time_id, $lb_bta);
                if ($rowfb > 0 || $rowl > 0 || $rowlb > 0) {
//                        $isokay = true;
                } else {
                    $isokay = false;
                }
            } else {
                $isokay = false;
            }
        }
        if ($isokay) {
            echo "Success";
        } else {
            echo "Failed";
        }
    } else {
        echo "Failed";
    }
}

public function checkTimeExists() {
    $time_start = $this->input->post('time_start');
    $time_end = $this->input->post('time_end');
    $ts = explode(" ", $time_start);
    $en = explode(" ", $time_end);
    $time_start = "$ts[0]:00 $ts[1]";
    $time_end = "$en[0]:00 $en[1]";
    $row = $this->ScheduleModel->checkTimeExists($time_end, $time_start);
    if ($row > 0) {
        echo true;
    } else {
        echo false;
    }
}

//-----------------------------------END OF SHIFTS--------------------------------------------------------------------------
    //------------------OLD ACCOUNT SHIFT------------------------------------


public function accountshift_old() {
    $data['admins'] = json_encode($this->ScheduleModel->getAdminAccounts());
    $data['agents'] = json_encode($this->ScheduleModel->getAgentAccounts());
    $this->load->view('accountshift_old', $data);
}

public function getAccShifts_old() {
    $acc_id = $this->input->post('acc_id');
    $data = $this->ScheduleModel->getAccShifts_old($acc_id);
    echo json_encode($data);
}

public function getAccBreaks_old() {
    $acc_id = $this->input->post('acc_id');
    $data = $this->ScheduleModel->getAccBreaks_old($acc_id);
    echo json_encode($data);
}

public function getNotShifts_old() {
    $acc_id = $this->input->post('acc_id');
        // $acc_id=1;
    $values = $this->ScheduleModel->getAccShifts_old($acc_id);
    foreach ($values as $v) {
        $timeids[] = $v->time_id;
    }
    $data = $this->ScheduleModel->getNotShifts_old($timeids);
    echo json_encode($data);
}

public function assignShift_old() {
    $acc_id = $this->input->post('acc_id');
    $time_id = $this->input->post('time_id');
    $row = $this->ScheduleModel->createAccTime_old($acc_id, $time_id);
    if ($row > 0) {
        echo 'Success';
    } else {
        echo 'Failed';
    }
}

public function getNotBreaks_old() {
    $acc_id = $this->input->post('acc_id');
    $values = $this->ScheduleModel->getAccBreaks_old($acc_id);
    $btaids = array();
    $where = "";
    foreach ($values as $v) {
        $btaids[] = $v->bta_id;
    }
    if (!empty($btaids)) {
        $where = "AND a.bta_id NOT IN (" . implode(',', $btaids) . ") ";
    }
    $data = $this->ScheduleModel->getNotBreaks_old($where);
    echo json_encode($data);
}

public function assignBreak_old() {
    $acc_id = $this->input->post('acc_id');
    $bta_id = $this->input->post('bta_id');
    $data = $this->ScheduleModel->getTimebta_old($bta_id);
    if (empty($data)) {
        echo 'Failed';
    } else {
        $row = $this->ScheduleModel->createAccTimeBreak_old($acc_id, $data->time_id);
        if ($row > 0) {
            echo 'Success';
        } else {
            echo 'Failed';
        }
    }
}

    //----------------------END OF OLD ACCOUNT SHIFT
}
