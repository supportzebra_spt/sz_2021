<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		// $query = $this->db->query("select 
		// 	fname,
		// 	lname,
		// 	pic,
		// 	b.apid,
		// 	b.emp_id,
		// 	id_num,
		// 	bday,
		// 	placebday,
		// 	intellicare,
		// 	pioneer,
		// 	BIR,
		// 	philhealth,
		// 	pagibig,
		// 	sss,
		// 	TIMESTAMPDIFF(YEAR,bday,now()) as age,
		// 	a.pos_id,
		// 	c.pos_name,
		// 	b.emp_stat,
		// 	cell
		// 	from tbl_applicant a,tbl_employee b,tbl_position c where c.pos_id=a.pos_id and a.apid=b.apid and b.isActive='yes' order by lname ASC");
		$query = $this->db->query("select fname, lname, pic, b.apid, b.emp_id, id_num, bday, placebday, intellicare, pioneer, BIR, philhealth, pagibig, sss, TIMESTAMPDIFF(YEAR,bday,now()) as age, a.pos_id, c.pos_name, e.status, cell from tbl_applicant a,tbl_employee b,tbl_position c,tbl_pos_emp_stat d,tbl_emp_stat e,tbl_emp_promote f where a.apid=b.apid and b.emp_id=f.emp_id and f.posempstat_id=d.posempstat_id and d.empstat_id=e.empstat_id and d.pos_id=c.pos_id and d.isActive=1 and f.isActive=1 and b.isActive='yes' order by lname ASC");
		foreach ($query->result() as $row)
		{
			$arr['data'][$row->apid] = array(
				"apid" => $row->apid,
				"emp_id" => $row->emp_id,
				"fname" => $row->fname,
				"lname" => $row->lname,
				"pic" => $row->pic,
				"id_num" => $row->id_num,
				"bday" => $row->bday,
				"age" => $row->age,
				"placebday" => $row->placebday,
				"intellicare" => $row->intellicare,
				"pioneer" => $row->pioneer,
				"BIR" => $row->BIR,
				"philhealth" => $row->philhealth,
				"pagibig" => $row->pagibig,
				"sss" => $row->sss,
				"pos_id" => $row->pos_id,
				"pos_name" => $row->pos_name,
				"status" => $row->status,
				"cell" => $row->cell,
				);
		}
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='employee' and is_assign=1");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
				$this->load->view('employees',$arr);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}


	}


	public function updateEmployee(){
		$empid = $this->input->post('emp_id');
		// var_dump($empid);
		if($empid!=null){
			$query = $this->db->query("select 
				fname,
				mname,
				lname,
				nikname,
				ccaExp,
				gender,
				civil,
				pic,
				b.apid,
				b.emp_id,
				id_num,
				bday,
				placebday,
				intellicare,
				pioneer,
				BIR,
				philhealth,
				pagibig,
				sss,
				TIMESTAMPDIFF(YEAR,bday,now()) as age,
				a.pos_id,
				c.pos_name,
				b.emp_stat,
				cell,
				tel,
				email,
				blood,
				relijon,
				school_name,
				course,
				school_note,
				emergency_fname,
				emergency_mname,
				emergency_lname,
				emergency_extname,
				emergency_contact,
				other_latestEmp,
				other_lastPosition,
				other_inclusiveDate,
				intellicare,
				pioneer,
				BIR,
				philhealth,
				pagibig,
				sss,
				dtr_uname,
				dtr_password,
				Supervisor,
				supEmail
				from tbl_applicant a,tbl_employee b,tbl_position c where c.pos_id=a.pos_id and a.apid=b.apid and b.isActive='yes' and emp_id= ".$empid." order by lname ASC");
			foreach ($query->result() as $row)
			{
				
				$arr['emp'] = array(
					"apid" => $row->apid,
					"emp_id" => $row->emp_id,
					"fname" => $row->fname,
					"lname" => $row->lname,
					"mname" => $row->mname,
					"nikname" => $row->nikname,
					"gender" => $row->gender,
					"civil" => $row->civil,
					"pic" => $row->pic,
					"id_num" => $row->id_num,
					"bday" => $row->bday,
					"age" => $row->age,
					"placebday" => $row->placebday,
					"intellicare" => $row->intellicare,
					"pioneer" => $row->pioneer,
					"BIR" => $row->BIR,
					"philhealth" => $row->philhealth,
					"pagibig" => $row->pagibig,
					"sss" => $row->sss,
					"pos_id" => $row->pos_id,
					"pos_name" => $row->pos_name,
					"emp_stat" => $row->emp_stat,
					"cell" => $row->cell,
					"tel" => $row->tel,
					"email" => $row->email,
					"blood" => $row->blood,
					"relijon" => $row->relijon,
					"school_name" => $row->school_name,
					"course" => $row->course,
					"school_note" => $row->school_note,
					"emergency_fname" => $row->emergency_fname,
					"emergency_mname" => $row->emergency_mname,
					"emergency_lname" => $row->emergency_lname,
					"emergency_extname" => $row->emergency_extname,
					"emergency_contact" => $row->emergency_contact,
					"ccaExp" => $row->ccaExp,
					"other_latestEmp" => $row->other_latestEmp,
					"other_lastPosition" => $row->other_lastPosition,
					"other_inclusiveDate" => $row->other_inclusiveDate,
					"intellicare" => $row->intellicare,
					"pioneer" => $row->pioneer,
					"BIR" => $row->BIR,
					"philhealth" => $row->philhealth,
					"pagibig" => $row->pagibig,
					"sss" => $row->sss,
					"dtr_uname" => $row->dtr_uname,
					"dtr_password" => $row->dtr_password,
					"Supervisor" => $row->Supervisor,
					"supEmails" => $row->supEmail
					);
			} 

			if($this->session->userdata('uid')){
				if($arr['emp']['Supervisor'] != null){
					$query2 = $this->db->query("select fname, mname, lname from tbl_applicant a,tbl_employee b,tbl_position c where c.pos_id=a.pos_id and a.apid=b.apid and b.isActive='yes' and emp_id= ".$arr['emp']['Supervisor']." order by lname ASC");
					foreach ($query2->result() as $row){
						$arr['Supname'] = $row->fname." ".$row->lname;
					}
				}else{
					$arr['Supname'] = " ";
				}
				$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Applicant' and is_assign=1");
				foreach ($qry->result() as $row)
				{
					$arr['setting'] = array("settings" => $row->settings);
				}

				//----------------NICCA_------------------------------------------------------------------------------------------------
				// $nquery = $this->db->query("SELECT pos_name,pos_details,rate AS monthly, round(rate/2,2) AS quinsina,round((rate*12)/261,2) as daily,round(((rate*12)/261)/8,2) as hourly FROM tbl_applicant a,tbl_employee b,tbl_position c,tbl_rate d WHERE a.apid=b.apid AND a.pos_id=c.pos_id AND c.pos_id = d.pos_id AND b.isActive='yes' AND b.emp_id=".$empid);
				$nquery = $this->db->query("select g.status,pos_name,pos_details,rate AS monthly, round(rate/2,2) AS quinsina,round((rate*12)/261,2) as daily,round(((rate*12)/261)/8,2) as hourly from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 AND f.isActive=1 and c.isActive=1 and b.emp_id=".$empid);
				$pospay = $nquery->row();
				$arr['pospay']  = $nquery->row();
				$taxdesc = $this->db->query("SELECT tax_desc_id as value,description as text FROM tbl_tax_description WHERE isActive=1");
				$arr['taxdesc']  = json_encode($taxdesc->result());
				$sss = $this->db->query("SELECT a.* FROM tbl_sss as a,tbl_sss_effectivity as b WHERE a.min_range<='".$pospay->monthly."' AND a.max_range>='".$pospay->monthly."' AND a.sss_effec_id=b.sss_effec_id AND b.isActive=1");
				$arr['sss'] = $sss->row();
				$philhealth = $this->db->query("SELECT a.* FROM tbl_philhealth as a,tbl_philhealth_effectivity as b WHERE a.minrange<='".$pospay->monthly."' AND a.maxrange>='".$pospay->monthly."' AND a.p_effec_id=b.p_effec_id AND b.isActive=1");
				$arr['philhealth'] = $philhealth->row();
				$pagibig = $this->db->query("SELECT pagibig_id,(employeeshare*'".$pospay->monthly."') as employeeshare,employershare FROM tbl_pagibig as a WHERE a.isActive=1");
				$arr['pagibig'] = $pagibig->row();

				$taxemp = $this->db->query("SELECT tax_desc_id FROM tbl_tax_emp WHERE emp_id=".$empid." AND isActive=1");
				$arr['taxemp'] = $taxemp->row();
				// var_dump($arr['taxemp']);
				//-----------------Nicca--------------------------------------------------------------------------------------------------

				$this->load->view('employeeUpdate',$arr);
				// var_dump($arr['c']);

			}else{
				redirect('index.php/login');
			}

		}else{
			redirect('index.php/Employee');
		}
	}
	public function listofSupervisor(){
		$query = $this->db->query("select b.apid,emp_id,fname,lname,a.status,b.email from tbl_applicant as a,tbl_employee as b,tbl_account c where a.apid=b.apid and c.acc_id = b.acc_id and a.status='employ' and email!='' and c.acc_description='Admin' and b.isActive='yes'");

		foreach ($query->result() as $row){
			$arr[]=  array( 			
				"emp_id"=>$row->emp_id,
				"label"=>$row->fname." ".$row->lname,
				"email"=>$row->email
				);

		}
		echo json_encode($arr);
	}

	//----------------------------------------------NICCA-----------------------------------------------
	public function getTaxEmployeeShare($taxid){
		$salary = $this->input->post('salary');
		$query = $this->db->query("select b.deduction from tbl_tax a,tbl_tax_deduction b,tbl_tax_description c,tbl_tax_percent d where d.percent_id = b.percent_id and a.deduction_id= b.deduction_id and a.tax_desc_id = c.tax_desc_id and c.tax_desc_id='".$taxid."' and salarybase<='".$salary."' order by salarybase desc LIMIT 1");
		$q = $query->row();
		echo $q->deduction;
	}

	public function UpdateEmployeeValue(){

		$id = $this->input->post('id');
		$idname = $this->input->post('idname');
		$table = $this->input->post('table');
		$name = $this->input->post('name');
		$value = $this->input->post('value');
		$data = array($name => $value);
		// var_dump($data);
		$this->db->where($idname, $id);
		$this->db->update($table, $data);
		if( $this->db->affected_rows()>0){
			echo 'Success';
		}else{
			echo 'Fail';
		}
	}
	public function UpdateEmployeeSupervisor(){

		$emp_id = $this->input->post('emp_id');
		$email = $this->input->post('email');
		$supervisor = $this->input->post('supervisor');
		$data = array('Supervisor' => $supervisor, 'supEmail' => $email);
		// var_dump($data);
		$this->db->where('emp_id', $emp_id);
		$this->db->update('tbl_employee', $data);
		if( $this->db->affected_rows()>0){
			echo 'Success';
		}else{
			echo 'Fail';
		}
	}
	public function insertNewTaxValue(){

		$taxid = $this->input->post('taxid');
		$emp_id = $this->input->post('emp_id');

		$query = $this->db->query("SELECT taxemp_id FROM tbl_tax_emp where emp_id='".$emp_id."' AND isActive=1");
		$result = $query->result();
		if($result!=NULL){
			foreach($result as $r){
				$this->db->where('taxemp_id', $r->taxemp_id);
				$this->db->update('tbl_tax_emp', array('isActive'=>'0'));
			}
		}
		$data = array('tax_desc_id' => $taxid, 'emp_id' => $emp_id,'isActive'=>'1');
		// var_dump($data);
		$this->db->insert('tbl_tax_emp', $data);
		if( $this->db->affected_rows()>0){
			echo 'Success';
		}else{
			echo 'Fail';
		}
	}

	//----------------------------------------------NICCA-----------------------------------------------
}