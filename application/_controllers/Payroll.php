<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll extends CI_Controller {

	// public $logo= 'C:\inetpub\wwwroot\nicca\sz3\assets\images\logo2.png';
	// public $file = 'C:\inetpub\wwwroot\nicca\sz3\reports\philhealth.xlsx';

	//public $logo= "C:\\xampp\\htdocs\\supportzebra\\assets\\images\\logo2.png";
	 public $logo=  "/usr/local/www/apache24/data/supportzebra/assets/images/logo2.png";
	//public $file= "C:\\xampp\\htdocs\\supportzebra\\reports\\philhealth.xlsx";
	 public $file= "/usr/local/www/apache24/data/supportzebra/reports/philhealth.xlsx";
	// public $taxfile= "C:\\xampp\\htdocs\\supportzebra\\reports\\tax.xlsx";
	public $taxfile= "/usr/local/www/apache24/data/supportzebra/reports/tax.xlsx";
	//public $sssfile= "C:\\xampp\\htdocs\\supportzebra\\reports\\sss.xlsx";
	 public $sssfile= "/usr/local/www/apache24/data/supportzebra/reports/sss.xlsx";
	//public $holidayfile= "C:\\xampp\\htdocs\\supportzebra\\reports\\holiday.xlsx";
	 public $holidayfile=  "/usr/local/www/apache24/data/supportzebra/reports/holiday.xlsx";
	//public $employeeadditions= "C:\\xampp\\htdocs\\supportzebra\\reports\\employeeadditions.xlsx";
	 public $employeeadditions= "/usr/local/www/apache24/data/supportzebra/reports/employeeadditions.xlsx";
	//public $employeedeductions= "C:\\xampp\\htdocs\\supportzebra\\reports\\employeedeductions.xlsx";
	 public $employeedeductions= "/usr/local/www/apache24/data/supportzebra/reports/employeedeductions.xlsx";
	public function __construct(){
		parent::__construct();
		$this->load->model('PayrollModel');
	}
	public function index(){
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/' and is_assign=1");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){

				$this->load->view('payrollIndex',$arr);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}
	}
	
	public function philhealth($id=NULL){
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/Philhealth' and is_assign=1");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
				if($id==NULL){
					$data['values'] = $this->PayrollModel->getActivePhilhealth();
				}else{
					$data['values'] = $this->PayrollModel->getSelectedPhilhealth($id);
				}
				$data['dates'] = $this->PayrollModel->getPhilhealthUsedDates();

				$this->load->view('philhealth',$data);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}



	}
	public function addphilhealth(){

		if($this->session->userdata('uid')){
			$data['date']=$this->PayrollModel->getPhilhealthDates();
			$this->load->view('addphilhealth',$data);
		}else{
			redirect('index.php/login');
		}
	}
	
	public function viewphilhealth(){

		if($this->session->userdata('uid')){
			$this->load->library('PHPExcel', NULL, 'excel');
			$objPHPExcel = PHPExcel_IOFactory::load($this->file);
			$maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
			$data = $objPHPExcel->getActiveSheet()->rangeToArray('A4:' . $maxCell['column'] . $maxCell['row']);
			$data = array_map('array_filter', $data);
			$data = array_filter($data);

			if(empty($data)){
				echo 'No Data Found';
			}else{
				echo  json_encode($data);
			}
		}else{
			redirect('index.php/login');
		}
	}
	public function savephilhealth($dateid){
		if($this->session->userdata('uid')){
			$this->load->library('PHPExcel', NULL, 'excel');
			$objPHPExcel = PHPExcel_IOFactory::load($this->file);
			$maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
			$data = $objPHPExcel->getActiveSheet()->rangeToArray('A4:' . $maxCell['column'] . $maxCell['row']);
			$data = array_map('array_filter', $data);
			$data = array_filter($data);
			foreach($data as $d){
				$values = array(
					'minrange' => str_replace( ',', '', $d[0]),
					'maxrange' => str_replace( ',', '', $d[1]),				
					'salarybase' => str_replace( ',', '', $d[2]),
					'employeeshare' => str_replace( ',', '', $d[3]),
					'employershare' => str_replace( ',', '', $d[4]),
					'totalmonthlypremium'=> str_replace( ',', '', $d[5]),
					'p_effec_id' => $dateid
				);
				$this->PayrollModel->savePhilhealth($values);
			// echo $d[0]."<br>";
			}
		}else{
			redirect('index.php/login');
		}
	}

	public function downloadphilhealth(){
		if($this->session->userdata('uid')){
			$this->load->library('PHPExcel', NULL, 'excel');
	//activate worksheet number 1
			$this->excel->setActiveSheetIndex(0);
	//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Philhealth Rates');
			$this->excel->getActiveSheet()->setShowGridlines(false);
	//------------------------INSERT LOGO-------------------------------------------
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath($this->logo);
	$objDrawing->setOffsetX(0);    // setOffsetX works properly
	$objDrawing->setOffsetY(0);  //setOffsetY has no effect
	$objDrawing->setCoordinates('C1');
	$objDrawing->setHeight(100); // logo height
	// $objDrawing->setWidth(320); // logo width
	// $objDrawing->setWidthAndHeight(200,400);
	$objDrawing->setResizeProportional(true);
	$objDrawing->setWorksheet($this->excel->getActiveSheet());
	//----------------------------------------------------------------------
	$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38);
	$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(38);

	//set cell A1 content with some text
	$this->excel->getActiveSheet()->setCellValue('A3', 'Salary Minimum');
	$this->excel->getActiveSheet()->setCellValue('B3', 'Salary Maximum');
	$this->excel->getActiveSheet()->setCellValue('C3', 'Salary Base');
	$this->excel->getActiveSheet()->setCellValue('D3', 'Employee Share');
	$this->excel->getActiveSheet()->setCellValue('E3', 'Employer Share');
	$this->excel->getActiveSheet()->setCellValue('F3', 'Total Monthly Premium');
	//merge cell A1 until D1
	$this->excel->getActiveSheet()->mergeCells('A1:F2');
	//set aligment to center for that merged cell (A1 to D1)
	$this->excel->getActiveSheet()->getStyle('A1:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$this->excel->getActiveSheet()->getStyle('F2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

	$this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
	$this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'ffffff'))));
	$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => '000000'))));


	$this->excel->getActiveSheet()->setAutoFilter('A3:F3');
	$this->excel->getActiveSheet()->getProtection()->setSort(true);

	$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
	$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
	$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
	$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

	$this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
	$this->excel->getActiveSheet()->getProtection()->setSheet(true);
	$this->excel->getActiveSheet()->getStyle('A4:F31')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
	$this->excel->getActiveSheet()->getStyle('A4:F31')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

	$objValidation = $this->excel->getActiveSheet()->getCell('A4')->getDataValidation();
	$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
	$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
	$objValidation->setAllowBlank(true);
	$objValidation->setShowInputMessage(true);
	$objValidation->setShowErrorMessage(true);
	$objValidation->setErrorTitle('Input error');
	$objValidation->setError('Letters are not allowed!');
	$objValidation->setPromptTitle('Allowed input');
	$objValidation->setFormula1(0);
	$objValidation->setFormula2(1000000);
	$j = 4;
	while($j <= 31){
		$this->excel->getActiveSheet()->getCell("A$j")->setDataValidation(clone $objValidation);  
		$this->excel->getActiveSheet()->getCell("B$j")->setDataValidation(clone $objValidation);  
		$this->excel->getActiveSheet()->getCell("C$j")->setDataValidation(clone $objValidation);  
		$this->excel->getActiveSheet()->getCell("D$j")->setDataValidation(clone $objValidation);   
		$this->excel->getActiveSheet()->getCell("E$j")->setDataValidation(clone $objValidation);  
		$this->excel->getActiveSheet()->getCell("F$j")->setDataValidation(clone $objValidation);   
		$j++;
	}

	$this->excel->getActiveSheet()->getStyle('A1:F31')->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
$filename='PhilhealthTemplate.xlsx'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache

//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
}else{
	redirect('index.php/login');
}
}
public function uploadphilhealth(){

	if($this->session->userdata('uid')){
		$path = $_FILES["uploadFile"]["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Manila');	

		$target_dir = "./reports/";
		$new_name = "philhealth";
		$target_file = $target_dir.$new_name.'.'.$extension;

		$allowed = array('xls', 'xlsx', 'csv');

		if($_FILES["uploadFile"]["name"]){
			if(!in_array($extension,$allowed)){
				echo 'File type is not allowed.';
			} else {
				if(move_uploaded_file($_FILES["uploadFile"]["tmp_name"],$target_file)){
					echo 'Successfully Uploaded.';
				}else{
					echo 'Error Uploading';
				}

			}

		}
	}else{
		redirect('index.php/login');
	}
}

public function downloadtax(){
	$this->load->library('PHPExcel', NULL, 'excel');
	//activate worksheet number 1
	$this->excel->setActiveSheetIndex(0);
	//name the worksheet
	$this->excel->getActiveSheet()->setTitle('Philhealth Rates');
	// $this->excel->getActiveSheet()->setShowGridlines(false);
	//------------------------INSERT LOGO-------------------------------------------
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setName('Logo');
	$objDrawing->setDescription('Logo');
	$objDrawing->setPath($this->logo);
	$objDrawing->setOffsetX(0);    // setOffsetX works properly
	$objDrawing->setOffsetY(0);  //setOffsetY has no effect
	$objDrawing->setCoordinates('D1');
	$objDrawing->setHeight(100); // logo height
	// $objDrawing->setWidth(320); // logo width
	// $objDrawing->setWidthAndHeight(200,400);
	$objDrawing->setResizeProportional(true);
	$objDrawing->setWorksheet($this->excel->getActiveSheet());
	//----------------------------------------------------------------------
	$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38);
	$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(38);


	$this->excel->getActiveSheet()->setCellValue('A3', 'Status');
	$this->excel->getActiveSheet()->setCellValue('B3', 'Rate');
	$this->excel->getActiveSheet()->setCellValue('C3', '+0% over');
	$this->excel->getActiveSheet()->setCellValue('D3', '+5% over');
	$this->excel->getActiveSheet()->setCellValue('E3', '+10% over');
	$this->excel->getActiveSheet()->setCellValue('F3', '+15% over');
	$this->excel->getActiveSheet()->setCellValue('G3', '+20% over');
	$this->excel->getActiveSheet()->setCellValue('H3', '+25% over');
	$this->excel->getActiveSheet()->setCellValue('I3', '+30% over');
	$this->excel->getActiveSheet()->setCellValue('J3', '+32% over');

	$this->excel->getActiveSheet()->setCellValue('A4', 'A. Table for employees without qualified dependent');
	$this->excel->getActiveSheet()->setCellValue('A5', 'Z');
	$this->excel->getActiveSheet()->setCellValue('A6', 'S/ME');
	$this->excel->getActiveSheet()->setCellValue('A7', 'B. Table for single/married employee with qualified dependent child(ren)');
	$this->excel->getActiveSheet()->setCellValue('A8', 'ME1 / S1');
	$this->excel->getActiveSheet()->setCellValue('A9', 'ME2 / S2');
	$this->excel->getActiveSheet()->setCellValue('A10', 'ME3 / S3');
	$this->excel->getActiveSheet()->setCellValue('A11', 'ME4 / S4');
	//merge cell A1 until D1
	$this->excel->getActiveSheet()->mergeCells('A1:J2');

	// $this->excel->getActiveSheet()->mergeCells('A3:B14');
	//set aligment to center for that merged cell (A1 to D1)
	$this->excel->getActiveSheet()->getStyle('A1:J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$this->excel->getActiveSheet()->getStyle('A4:J4')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '666666'))));
	$this->excel->getActiveSheet()->getStyle('A7:J7')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '666666'))));
	$this->excel->getActiveSheet()->getStyle('A3:J3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '42b9f4'))));
	$this->excel->getActiveSheet()->getStyle('A3:J3')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => '000000'))));

	$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
	$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
	$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
	$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
	$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
	$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
	$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
	$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
	$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
	$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(13);


	$this->excel->getActiveSheet()->getStyle('A3:J11')->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
	$this->excel->getActiveSheet()->getStyle('A1:J11')->applyFromArray(array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THICK))));



	$this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
	$this->excel->getActiveSheet()->getProtection()->setSheet(true);
	$this->excel->getActiveSheet()->getStyle('B5:J11')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
	$this->excel->getActiveSheet()->getStyle('B5:J11')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

	$objValidation = $this->excel->getActiveSheet()->getCell('B5')->getDataValidation();
	$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
	$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
	$objValidation->setAllowBlank(true);
	$objValidation->setShowInputMessage(true);
	$objValidation->setShowErrorMessage(true);
	$objValidation->setErrorTitle('Input error');
	$objValidation->setError('Letters are not allowed!');
	$objValidation->setPromptTitle('Allowed input');
	$objValidation->setFormula1(0);
	$objValidation->setFormula2(1000000);
	$j = 5;
	while($j <= 11){ 
		if($j!=7){
			$this->excel->getActiveSheet()->getCell("B$j")->setDataValidation(clone $objValidation);  
			$this->excel->getActiveSheet()->getCell("C$j")->setDataValidation(clone $objValidation);  
			$this->excel->getActiveSheet()->getCell("D$j")->setDataValidation(clone $objValidation);   
			$this->excel->getActiveSheet()->getCell("E$j")->setDataValidation(clone $objValidation);  
			$this->excel->getActiveSheet()->getCell("F$j")->setDataValidation(clone $objValidation);   
			$this->excel->getActiveSheet()->getCell("G$j")->setDataValidation(clone $objValidation);   
			$this->excel->getActiveSheet()->getCell("H$j")->setDataValidation(clone $objValidation);  
			$this->excel->getActiveSheet()->getCell("I$j")->setDataValidation(clone $objValidation);   
			$this->excel->getActiveSheet()->getCell("J$j")->setDataValidation(clone $objValidation);   
			
		}
		$j++;
	} 

$filename='BIRTaxTemplate.xlsx'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache

//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');

}
public function addtax(){
	$data['dates'] = $this->PayrollModel->getTaxDates();
	$data['types'] = $this->PayrollModel->getTaxType();
	$this->load->view('addtax',$data);
}
public function uploadtax(){
	$path = $_FILES["uploadFile"]["name"];
	$extension = pathinfo($path, PATHINFO_EXTENSION);
	date_default_timezone_set('Asia/Manila');	

	$target_dir = "./reports/";
	$new_name = "tax";
	$target_file = $target_dir.$new_name.'.'.$extension;

	$allowed = array('xls', 'xlsx', 'csv');

	if($_FILES["uploadFile"]["name"]){
		if(!in_array($extension,$allowed)){
			echo 'File type is not allowed.';
		} else {
			if(move_uploaded_file($_FILES["uploadFile"]["tmp_name"],$target_file)){
				echo 'Successfully Uploaded.';
			}else{
				echo 'Error Uploading';
			}

		}

	}
}

public function viewtax(){

	$this->load->library('PHPExcel', NULL, 'excel');
	$objPHPExcel = PHPExcel_IOFactory::load($this->taxfile);
	$data = $objPHPExcel->getActiveSheet()->rangeToArray('A5:J6');
	$data2 = $objPHPExcel->getActiveSheet()->rangeToArray('A8:J11');
	foreach($data2 as $d){
		array_push($data,$d);
	}
	// print_r($data);
	if(empty($data)){
		echo 'No Data Found';
	}else{
		echo  json_encode($data);
	}
}
public function savetax($typeid,$dateid){
	$this->load->library('PHPExcel', NULL, 'excel');
	$objPHPExcel = PHPExcel_IOFactory::load($this->taxfile);
	$data = $objPHPExcel->getActiveSheet()->rangeToArray('A5:J6');
	$data2 = $objPHPExcel->getActiveSheet()->rangeToArray('A8:J11');
	foreach($data2 as $d){
		array_push($data,$d);
	}
	$id = array();
	// $deduction = $this->PayrollModel->getDeductions();
	$deduction = array('0.00','0.00','20.83','104.17','354.17','937.50','2083.33','5208.33');

	$col = 1;
	foreach($deduction as $d){
		array_push($id,$this->PayrollModel->saveDeduction($col,$d,$col,$typeid,$dateid));
		$col++;
	}
	// var_dump($deduction);
	
	$descid = 1;
	foreach($data as $d){
		$increment = 0;
		for($x=2;$x<10;$x++){
			$this->PayrollModel->saveTax($descid,str_replace( ',', '', $d[$x]),$id[$increment]);
			$increment++;
		}
		$descid++;
	}
}
public function tax($id=NULL){
	if($this->session->userdata('uid')){
		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/TAX' and is_assign=1");
		foreach ($qry->result() as $row)
		{
			$arr['setting'] = array("settings" => $row->settings);
		}
		if ($qry->num_rows() > 0){
			if($id==NULL){
				$data['values'] = $this->PayrollModel->getTaxActive();
			}else{
				$data['values'] = $this->PayrollModel->getSelectedTax($id);
			}
			$data['id'] = $id;
			$data['dates'] = $this->PayrollModel->getTaxUsedDates();
			$values = $data['values'];
			$this->load->view('tax',$data);		
		}else{
			redirect('index.php/home/error');
		}
	}else{
		redirect('index.php/login');
	}
	
}

public function sss($id=NULL){
	if($this->session->userdata('uid')){
		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/SSS' and is_assign=1");
		foreach ($qry->result() as $row)
		{
			$arr['setting'] = array("settings" => $row->settings);
		}
		if ($qry->num_rows() > 0){

			if($id==NULL){
				$data['values'] = $this->PayrollModel->getActiveSSS();
			}else{
				$data['values'] = $this->PayrollModel->getSelectedSSS($id);
			}
			$data['id']=$id;
			$data['date'] = $this->PayrollModel->getSSSUsedDates();
			$this->load->view('sss',$data);
		}else{
			redirect('index.php/home/error');
		}
	}else{
		redirect('index.php/login');
	}
	
	
	
}
public function addsss(){
	$data['date'] = $this->PayrollModel->getSSSDates();
	$this->load->view('addsss',$data);
}
public function downloadsss(){
	if($this->session->userdata('uid')){
		$this->load->library('PHPExcel', NULL, 'excel');
	//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
	//name the worksheet
		$this->excel->getActiveSheet()->setTitle('SSS Rates');
		$this->excel->getActiveSheet()->setShowGridlines(false);
	//------------------------INSERT LOGO-------------------------------------------
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($this->logo);
	$objDrawing->setOffsetX(0);    // setOffsetX works properly
	$objDrawing->setOffsetY(0);  //setOffsetY has no effect
	$objDrawing->setCoordinates('B1');
	$objDrawing->setHeight(100); // logo height
	// $objDrawing->setWidth(320); // logo width
	// $objDrawing->setWidthAndHeight(200,400);
	$objDrawing->setResizeProportional(true);
	$objDrawing->setWorksheet($this->excel->getActiveSheet());
	//----------------------------------------------------------------------
	$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38);
	$this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(38);

	//set cell A1 content with some text
	$this->excel->getActiveSheet()->setCellValue('A3', 'Salary Minimum');
	$this->excel->getActiveSheet()->setCellValue('B3', 'Salary Maximum');
	$this->excel->getActiveSheet()->setCellValue('C3', 'Monthly Salary Credit');
	$this->excel->getActiveSheet()->setCellValue('D3', 'SSS ER');
	$this->excel->getActiveSheet()->setCellValue('E3', 'SSS EE');
	$this->excel->getActiveSheet()->setCellValue('F3', 'EC ER');
	//merge cell A1 until D1
	$this->excel->getActiveSheet()->mergeCells('A1:F2');
	//set aligment to center for that merged cell (A1 to D1)
	$this->excel->getActiveSheet()->getStyle('A1:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$this->excel->getActiveSheet()->getStyle('F2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

	$this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
	$this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('font' => array('size' => 11,'bold' => true,'color' => array('rgb' => 'ffffff'))));
	$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => '000000'))));


	$this->excel->getActiveSheet()->setAutoFilter('A3:F3');
	$this->excel->getActiveSheet()->getProtection()->setSort(true);

	$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
	$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
	$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(17);
	$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
	$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(12);

	$this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
	$this->excel->getActiveSheet()->getProtection()->setSheet(true);
	$this->excel->getActiveSheet()->getStyle('A4:F34')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
	$this->excel->getActiveSheet()->getStyle('A4:F34')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

	$objValidation = $this->excel->getActiveSheet()->getCell('A4')->getDataValidation();
	$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
	$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
	$objValidation->setAllowBlank(true);
	$objValidation->setShowInputMessage(true);
	$objValidation->setShowErrorMessage(true);
	$objValidation->setErrorTitle('Input error');
	$objValidation->setError('Letters are not allowed!');
	$objValidation->setPromptTitle('Allowed input');
	$objValidation->setFormula1(0);
	$objValidation->setFormula2(1000000);
	$j = 4;
	while($j <= 34)
	{
		$this->excel->getActiveSheet()->getCell("A$j")->setDataValidation(clone $objValidation);  
		$this->excel->getActiveSheet()->getCell("B$j")->setDataValidation(clone $objValidation);  
		$this->excel->getActiveSheet()->getCell("C$j")->setDataValidation(clone $objValidation);  
		$this->excel->getActiveSheet()->getCell("D$j")->setDataValidation(clone $objValidation);   
		$this->excel->getActiveSheet()->getCell("E$j")->setDataValidation(clone $objValidation);  
		$this->excel->getActiveSheet()->getCell("F$j")->setDataValidation(clone $objValidation);   
		$j++;
	}

	$this->excel->getActiveSheet()->getStyle('A1:F34')->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
$filename='SSSTemplate.xlsx'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache

//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
}else{
	redirect('index.php/login');
}
}

public function uploadsss(){
	
	if($this->session->userdata('uid')){
		$path = $_FILES["uploadFile"]["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Manila');	

		$target_dir = "./reports/";
		$new_name = "sss";
		$target_file = $target_dir.$new_name.'.'.$extension;

		$allowed = array('xls', 'xlsx', 'csv');

		if($_FILES["uploadFile"]["name"]){
			if(!in_array($extension,$allowed)){
				echo 'File type is not allowed.';
			} else {
				if(move_uploaded_file($_FILES["uploadFile"]["tmp_name"],$target_file)){
					echo 'Successfully Uploaded.';
				}else{
					echo 'Error Uploading';
				}

			}

		}
	}else{
		redirect('index.php/login');
	}
}

public function viewsss(){

	if($this->session->userdata('uid')){
		$this->load->library('PHPExcel', NULL, 'excel');
		$objPHPExcel = PHPExcel_IOFactory::load($this->sssfile);
		$maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
		$data = $objPHPExcel->getActiveSheet()->rangeToArray('A4:' . $maxCell['column'] . $maxCell['row']);
		$data = array_map('array_filter', $data);
		$data = array_filter($data);

		if(empty($data)){
			echo 'No Data Found';
		}else{
			echo  json_encode($data);
		}
	}else{
		redirect('index.php/login');
	}
}
public function savesss($dateid){
	if($this->session->userdata('uid')){
		$this->load->library('PHPExcel', NULL, 'excel');
		$objPHPExcel = PHPExcel_IOFactory::load($this->sssfile);
		$maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
		$data = $objPHPExcel->getActiveSheet()->rangeToArray('A4:' . $maxCell['column'] . $maxCell['row']);
		$data = array_map('array_filter', $data);
		$data = array_filter($data);
		foreach($data as $d){
			$values = array(
				'min_range' => str_replace( ',', '', $d[0]),
				'max_range' => str_replace( ',', '', $d[1]),				
				'salary_credit' => str_replace( ',', '', $d[2]),
				'sss_employer' => str_replace( ',', '', $d[3]),
				'sss_employee' => str_replace( ',', '', $d[4]),
				'ec_employer'=> str_replace( ',', '', $d[5]),
				'sss_effec_id' => $dateid
			);
			$this->PayrollModel->saveSSS($values);
			// echo $d[0]."<br>";
		}
	}else{
		redirect('index.php/login');
	}
}

public function pagibig(){
	if($this->session->userdata('uid')){
		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/Pagibig' and is_assign=1 ");
		foreach ($qry->result() as $row)
		{
			$arr['setting'] = array("settings" => $row->settings);
		}
		if ($qry->num_rows() > 0){
			$data['values'] = $this->PayrollModel->getPagibig();
	// var_dump($data['values']);
			$this->load->view('pagibig',$data);			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}

	}
	public function savepagibig(){
		$employee = str_replace("%","",$this->input->post('employee'))/100;
		$employer = str_replace("%","",$this->input->post('employer'))/100;
		$date = explode('-', $this->input->post('date'));
		$newdate = $date[2].'-'.$date[0].'-'.$date[1];
		$values = array(
			'employeeshare'=>$employee,
			'employershare'=>$employer,
			'effectivity_date'=>$newdate,
			'isActive'=>'0'
		);
		$id = $this->PayrollModel->insertPagibig($values);
		if($id==NULL){
			echo 'Error';
		}else{
			echo 'Success';
		}

	}
	public function holiday()
    {
        $holidays = $this->PayrollModel->getHoliday();
        $sites = $this->db->query("SELECT * FROM tbl_site")->result();

        foreach ($holidays as $hol)
        {
            $holidaysites = explode(",", $hol->site);
            $holidaysites_str = array();
            foreach ($holidaysites as $holsite)
            {
                foreach ($sites as $site)
                {
                    if ($holsite === $site->site_ID)
                    {
                        $holidaysites_str[] = $site->code;
                    }
                }
            }
            $hol->sites_str = implode(",", $holidaysites_str);
        }
        $data['sites'] = $sites;
        $data['values'] = $holidays;
        $this->load->view('holiday', $data);
    }

    public function deleteHoliday()
    {
        $holiday_id = $this->input->post('holiday_id');
        $this->db->query("DELETE FROM tbl_holiday WHERE holiday_id=$holiday_id");
        if ($this->db->affected_rows() > 0)
        {
            $status = "Success";
        }
        else
        {
            $status = "Failed";
        }
        echo json_encode(array('status' => $status));
    }

    public function saveholiday()
    {
        $sites = $this->input->post('sites');
        $holiday = $this->input->post('holiday');
        $description = $this->input->post('description');
        $type = $this->input->post('type');
        $date = explode('-', $this->input->post('date'));
        $newdate = '2000-' . $date[0] . '-' . $date[1];
        if ($type == 'Regular')
        {
            $calc = '1.0';
        }
        else
        {
            $calc = '0.3';
        }
        $values = array(
            'holiday' => $holiday,
            'description' => $description,
            'type' => $type,
            'date' => $newdate,
            'calc' => $calc,
            'site' => $sites
        );
        $id = $this->PayrollModel->insertHoliday($values);
        if ($id == NULL)
        {
            echo 'Error';
        }
        else
        {
            echo 'Success';
        }
    }

	public function deductions(){
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/Deductions' and is_assign=1");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
				$data['values'] = $this->PayrollModel->getpayrolldeductions();
				$this->load->view('payrolldeductions',$data);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}
	}
	public function additions(){
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/Additions' and is_assign=1");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
				$data['values'] = $this->PayrollModel->getpayrolladditions();
				$this->load->view('payrolladditions',$data);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}
	}
	public function savepayrolldeduction(){
		$deductionname = $this->input->post('deductionname');
		$description = $this->input->post('description');
		$id = $this->session->userdata('uid');
		$date = date('Y-m-d H:i:s');
		$values = array(
			'deductionname'=> $deductionname,
			'description'=>$description,
			'isActive' => '1',
			'addedBy' => $id,
			'addedOn' => $date
		);
		$id = $this->PayrollModel->savepayrolldeduction($values);
		if($id!=NULL){
			echo 'Success';
		}else{
			echo 'Failed';
		}

	}
	public function savepayrolladdition(){
		$additionname = $this->input->post('additionname');
		$description = $this->input->post('description');
	// $percentage = $this->input->post('percentage');
	// $p = str_replace("%",'',$percentage);
	// if($p=='00'){
	// 	$percentage = '1';
	// }else{
	// 	$percentage = $p/100;
	// }
		$id = $this->session->userdata('uid');
		$date = date('Y-m-d H:i:s');
		$values = array(
			'additionname'=> $additionname,
			'description'=>$description,
		// 'defaultval'=>$percentage,
			'isActive' => '1',
			'addedBy' => $id,
			'addedOn' => $date
		);
	// var_dump($values);
		$row = $this->PayrollModel->savepayrolladdition($values);
		if($row!=0){
			echo 'Success';
		}else{
			echo 'Failed';
		}
	// echo $id;

	}
	public function updatepayrolladdition($id){
		$additionname = $this->input->post('additionname2');
		$description = $this->input->post('description2');
	// $percentage = $this->input->post('percentage2');
	// $p = str_replace("%",'',$percentage);
	// if($p=='00'){
	// 	$percentage = '1';
	// }else{
	// 	$percentage = $p/100;
	// }

		$uid = $this->session->userdata('uid');
		$date = date('Y-m-d H:i:s');
		$values = array(
			'additionname'=> $additionname,
		// 'defaultval'=> $percentage,
			'description'=>$description,
			'updatedBy' => $uid,
			'updatedOn' => $date
		);
		$row = $this->PayrollModel->updateAddition($id,$values);
		if($row!=0){
			echo 'Success';
		}else{
			echo 'Failed';
		}
	// echo $row;

	}
	public function updatepayrolldeduction($id){
		$deductionname = $this->input->post('deductionname2');
		$description = $this->input->post('description2');
		$uid = $this->session->userdata('uid');
		$date = date('Y-m-d H:i:s');
		$values = array(
			'deductionname'=> $deductionname,
			'description'=>$description,
			'updatedBy' => $uid,
			'updatedOn' => $date
		);
		$row = $this->PayrollModel->updateDeduction($id,$values);
		if($row!=0){
			echo 'Success';
		}else{
			echo 'Failed';
		}
	// echo $row;

	}

	function cmp($a, $b)
	{
		return strcmp($a->lname, $b->lname);
	}
//--------------------------------------------------------------ADDITIONS----------------------------------------------------------------

	// public function employeeAdditions($coverage_id=NULL){
	// 	$orig_coverage_id = $coverage_id;
	// 	$coverage = $this->PayrollModel->getAllWithValueCoverage();
	// 	// var_dump($data['coverage']);
	// 	if($coverage_id==NULL){
	// 		$cnt = count($coverage);
	// 		if($cnt==0){
	// 			$datavalues = NULL;
	// 		}else{
	// 			$coverage_id = $coverage[$cnt-1]->coverage_id;
	// 			$datavalues = $this->PayrollModel->getEmployeeAdditions($coverage_id);
	// 		}	
	// 	}else{
	// 		$datavalues = $this->PayrollModel->getEmployeeAdditions($coverage_id);
	// 	}

	// 	if($datavalues != NULL || $datavalues != ''){
	// 		for($z = 0;$z<count($datavalues);$z++){
	// 			$posempstat = $this->PayrollModel->getPosEmpStatId($datavalues[$z]->emp_id);
	// 			$onPayroll = $this->PayrollModel->onPayroll($coverage_id,$posempstat->emp_promoteID);
	// 			if($onPayroll!=NULL||$onPayroll!=''){
	// 				$datavalues[$z]->isFinal = 1;
	// 			}else{
	// 				$datavalues[$z]->isFinal = 0;
	// 			}
	// 		}
	// 	}
	// 	$data['values'] = $datavalues;
	// 	// var_dump($datavalues);
	// 	$notonlistfinal = array();
	// 	$notonlist =  $this->PayrollModel->getUsersNotListedAdd($coverage_id);
	// 	foreach($notonlist as $n){
	// 		array_push($notonlistfinal,$n->emp_id);
	// 	}
	// 	$data['count'] = $this->PayrollModel->countAdditions();
	// 	$data['alladditions'] = $this->PayrollModel->getAllAdditions();
	// 	$data['usercount'] = $this->PayrollModel->countAllUsers($coverage_id);
	// 	$datanotonlist =  $this->PayrollModel->getAllActiveEmployeeAdd($notonlistfinal);

	// 	$data['notonlist'] = $datanotonlist;
	// 	// var_dump($datanotonlist);
	// 	$data['coverage'] = $coverage;
	// 	$data['id'] = $coverage_id;
	// 	$data['cstatus'] = $this->PayrollModel->getCoverageStatus($coverage_id);
	// 	if($this->session->userdata('uid')){
	// 		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/employeeAdditions' and is_assign=1 order by lname");
	// 		foreach ($qry->result() as $row)
	// 		{
	// 			$arr['setting'] = array("settings" => $row->settings);
	// 		}
	// 		if ($qry->num_rows() > 0){
	// 			$this->load->view('employeeAdditionsAll',$data);
	// 		}else{
	// 			redirect('index.php/home/error');
	// 		}
	// 	}else{
	// 		redirect('index.php/login');
	// 	}



	// // var_dump($data['alladditions']);
	// }
	public function employeeAdditions2($stafftype,$status,$coverage_id,$site=null){
		$statz = explode("-",$status);
 		if($statz[0]=='ProbReg'){
			$datavalues = $this->PayrollModel->getEmployeeAdditionsProbReg($stafftype,$statz[1],$coverage_id);
		}else{
			$datavalues = $this->PayrollModel->getEmployeeAdditions2($stafftype,$status,$coverage_id,$site);
		}
		if($datavalues != NULL || $datavalues != ''){
			for($z = 0;$z<count($datavalues);$z++){
				$posempstat = $this->PayrollModel->getPosEmpStatId($datavalues[$z]->emp_id);
				$onPayroll = $this->PayrollModel->onPayroll($coverage_id,$posempstat->emp_promoteID);
				if($onPayroll!=NULL||$onPayroll!=''){
					$datavalues[$z]->isFinal = 1;
				}else{
					$datavalues[$z]->isFinal = 0;
				}
			}
		}
		$data['values'] = $datavalues;
		$notonlistfinal = array();
		$notonlist2 =  $this->PayrollModel->getUsersNotListedAdd($coverage_id);
		if($statz[0]=='ProbReg'){
			$staffstatuslist = $this->PayrollModel->getSpecificActiveEmployeeProbReg($stafftype,$statz[1]);
		}else{
			$staffstatuslist = $this->PayrollModel->getSpecificActiveEmployee($stafftype,$status,$site);
		}
		$notonlist = array();
		foreach($staffstatuslist as $sslist => &$ss){
			foreach($notonlist2 as $noton){
				if($ss->emp_id==$noton->emp_id){
					unset($staffstatuslist[$sslist]);
				}
			}	
		}
		// echo "<pre>";
		// var_dump($staffstatuslist);
		// echo "</pre>";
		$data['count'] = $this->PayrollModel->countAdditions();
		$data['alladditions'] = $this->PayrollModel->getAllAdditions();
		$data['usercount'] = $this->PayrollModel->countAllUsers($coverage_id);

		// var_dump($datanotonlist);
		$data['coverage'] = $this->PayrollModel->getCoverageDate($coverage_id);
		$data['id'] = $coverage_id;
		$data['stafftype'] = $stafftype;
		$data['status'] = $status;
		$data['site'] = $site;
		$data['cstatus'] = $this->PayrollModel->getCoverageStatus($coverage_id);
		$data['notonlist'] = $staffstatuslist;
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/employeeAdditions2' and is_assign=1");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){

				$this->load->view('employeeAdditions',$data,$arr);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}

	}

	// public function addEmployeeAdditions(){
	// 	$data['coverages'] = $this->PayrollModel->getAllWithValueCoverage();
	// 	$this->load->view('addEmployeeAdditions',$data);
	// // var_dump($data['coverages']);
	// }


	public function addEmployeeAdditions2($stafftype,$status,$coverage_id,$site=null){	
	if($this->session->userdata('uid')){

	$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/employeeAdditions2' and is_assign=1");   

	if ($qry->num_rows() > 0){

		$data['coverage'] = $this->PayrollModel->getCoverageDate($coverage_id);	
		$data['stafftype'] = $stafftype;
		$data['status'] = $status;
		$data['id'] = $coverage_id;
		$data['site'] = $site;
		$this->load->view('addEmployeeAdditions',$data);

	}else{
	redirect('index.php/home/error');
	}

	}

	// var_dump($data['coverages']);
	}

	public function viewEmployeeAdditions(){
		if($this->session->userdata('uid')){
			$this->load->library('PHPExcel', NULL, 'excel');
			$objPHPExcel = PHPExcel_IOFactory::load($this->employeeadditions);
			$maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
			$data = $objPHPExcel->getActiveSheet()->rangeToArray('B3:' . $maxCell['column'] . $maxCell['row']);
			$data = array_map('array_filter', $data);
			$data = array_filter($data);

			if(empty($data)){
				echo 'No Data Found';
			}else{
				echo  json_encode($data);
			}
		}else{
			redirect('index.php/login');
		}
	}
	public function saveEmployeeAdditions($cover_id){
		if($this->session->userdata('uid')){
			$this->load->library('PHPExcel', NULL, 'excel');
			$objPHPExcel = PHPExcel_IOFactory::load($this->employeeadditions);
			$coverage_id = $objPHPExcel->getActiveSheet()->getCell('D2')->getValue();
			if($cover_id==$coverage_id){
				$maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
				$headerid = $objPHPExcel->getActiveSheet()->rangeToArray('E2:' . $maxCell['column'] .'2');
				$headeridnew = array();
				foreach($headerid as $h){
					for($y=0;$y<count($h);$y++){
						array_push($headeridnew,$h[$y]);
					}
				}
				$data = $objPHPExcel->getActiveSheet()->rangeToArray('A4:' . $maxCell['column'] . $maxCell['row']);
				foreach ($data as $d) {
                    for ($x = 4; $x < count($d); $x++) {
                        $this->PayrollModel->deletePrevCoverageEmpAdditions($coverage_id, $d[0]);
                    }
                }
				foreach($data as $d){
					$y=0;
					for($x=4;$x<count($d);$x++){
						if($d[$x]!='0.00'){
							$data2 = $this->PayrollModel->empAddChecker($d[0],$coverage_id,$headeridnew[$y]);
							if(empty($data2)){
								$val = array(
									'emp_id'=>$d[0],
									'coverage_id'=>$coverage_id,
									'paddition_id'=>$headeridnew[$y],
									'value'=>str_replace(",","",$d[$x])
								);

								$rows = $this->PayrollModel->saveEmployeeAdditions($val);

							}else{
								$rows = $this->PayrollModel->updateEmployeeAdditions($d[0],$coverage_id,$headeridnew[$y],str_replace(",","",$d[$x]));

							}
							if($rows>0){
								echo "Success <br>";
							}else{
								echo "Failed <br>";
							}
						}
						$y++;
					}
				}
			}else{
				echo "DifferentPeriod";
			}


		}else{
			redirect('index.php/login');
		}
	}

	public function downloadEmployeeAdditions($stafftype=NULL,$status=NULL,$coverage_id=NULL,$site=null){
		if($stafftype==NULL && $status==NULL && $coverage_id==NULL){

			$employees = $this->PayrollModel->getAllActiveEmployee();
		}else{
					$statz = explode("-",$status);
 		if($statz[0]=='ProbReg'){
				$employees = $this->PayrollModel->getSpecificActiveEmployeeProbReg($stafftype,$statz[1]);
			}else{
				$employees = $this->PayrollModel->getSpecificActiveEmployee($stafftype,$status,$site);
			}
		}

		usort($employees, array($this, "cmp"));
		$additions = $this->PayrollModel->getAllAdditions();
		$coverage = $this->PayrollModel->getCoverage($coverage_id);

		$new = explode(" - ",$coverage->daterange);
		$date1=date_create($new[0]);
		$date2=date_create($new[1]);
		$date1 = date_format($date1,"F d,Y");
		$date2 = date_format($date2,"F d,Y");

		if($this->session->userdata('uid')){
			$this->load->library('PHPExcel', NULL, 'excel');
	//activate worksheet number 1
			$this->excel->setActiveSheetIndex(0);
	//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Employee Additions');
			$this->excel->getActiveSheet()->setShowGridlines(false);
	//------------------------INSERT LOGO-------------------------------------------
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath($this->logo);
		$objDrawing->setOffsetX(0);    // setOffsetX works properly
		$objDrawing->setOffsetY(0);  //setOffsetY has no effect
		$objDrawing->setCoordinates('A1');
		$objDrawing->setHeight(92); // logo height
	// $objDrawing->setWidth(320); // logo width
	// $objDrawing->setWidthAndHeight(200,400);
		$objDrawing->setResizeProportional(true);
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
	//----------------------------------------------------------------------
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(75);

	//set cell A1 content with some text
		$headerrow = 3;
		$headercol = 3;
		$this->excel->getActiveSheet()->setCellValue('A3', 'ID Number');
		$this->excel->getActiveSheet()->setCellValue('B3', 'Full Name');
		$this->excel->getActiveSheet()->setCellValue('C3', 'Code');
		$this->excel->getActiveSheet()->setCellValue('D3', 'Status');
		$letters = array();
		$this->excel->getActiveSheet()->setCellValue('D1', "From:\n$date1\nTo:\n$date2");
		$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, $coverage_id);
		foreach($additions as $add){
			$headercol++;
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow($headercol, $headerrow, $add->additionname);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow($headercol, 2, $add->paddition_id);
			$this->excel->getActiveSheet()->getRowDimension(2)->setVisible(false);
			array_push($letters,PHPExcel_Cell::stringFromColumnIndex($headercol));
		}

	//VALUES HERE
		$emprow = 4;
		foreach($employees as $emp){
			// if($emp->id_num==NULL||$emp->id_num==''){
			// 	$this->excel->getActiveSheet()->getStyle('A'.$emprow)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'ff4242'))));
			// }

			$this->excel->getActiveSheet()->setCellValue('A'.$emprow, $emp->emp_id);
			$this->excel->getActiveSheet()->setCellValue('B'.$emprow, $emp->lname.', '.$emp->fname);
			$this->excel->getActiveSheet()->setCellValue('C'.$emprow, $emp->pos_name);
			$this->excel->getActiveSheet()->setCellValue('D'.$emprow, $emp->status);

			$info = $this->PayrollModel->getAllEmpAdditionPerCoverage($emp->emp_id,$coverage_id);
			$cntr = 0;
			foreach($additions as $add){
				$val = 0;
				foreach($info as $nf){
					if($add->paddition_id==$nf->paddition_id){
						$val = $nf->value;
					}
				}
				$this->excel->getActiveSheet()->setCellValue($letters[$cntr].''.$emprow, $val);
				$cntr++;
			}
			$emprow++;
		}

		$column = $this->excel->getActiveSheet()->getHighestColumn();
		$row = $this->excel->getActiveSheet()->getHighestRow();
	//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:C1');
	//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A1:'.$column.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$this->excel->getActiveSheet()->getStyle('A1:'.$column.'1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$this->excel->getActiveSheet()->getStyle('A3:'.$column.'3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
		$this->excel->getActiveSheet()->getStyle('A3:'.$column.'3')->applyFromArray(array('font' => array('size' => 11,'bold' => true,'color' => array('rgb' => 'ffffff'))));


		$this->excel->getActiveSheet()->setAutoFilter('A3:'.$column.'3');
		$this->excel->getActiveSheet()->getProtection()->setSort(true);

		$this->excel->getActiveSheet()->getStyle('A3:'.$column.''.$row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		foreach($letters as $l){
			$this->excel->getActiveSheet()->getColumnDimension($l)->setAutoSize(true);

		// $this->excel->getActiveSheet()->getColumnDimension($l)->setWidth(15);
		}
// echo $this->excel->getActiveSheet()->getCell('G2')->getValue();
		$this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
		$this->excel->getActiveSheet()->getProtection()->setSheet(true);
		$this->excel->getActiveSheet()->getStyle('E4:'.$column.''.$row)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
		$this->excel->getActiveSheet()->getStyle('E4:'.$column.''.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

		$objValidation = $this->excel->getActiveSheet()->getCell('E4')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(true);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Letters are not allowed!');
		$objValidation->setPromptTitle('Allowed input');
		$objValidation->setFormula1(0);
		$objValidation->setFormula2(1000000);
		$j = 4;
		while($j <= $row)
		{ 
			foreach($letters as $l){
				$this->excel->getActiveSheet()->getCell("$l$j")->setDataValidation(clone $objValidation); 
			}
			$j++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(FALSE);
		$filename='EmployeeAdditionsTemplate.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache

//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
}else{
	redirect('index.php/login');
}
}
public function uploadEmployeeAdditions(){
	if($this->session->userdata('uid')){
		$path = $_FILES["uploadFile"]["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Manila');	

		$target_dir = "./reports/";
		$new_name = "employeeadditions";
		$target_file = $target_dir.$new_name.'.'.$extension;

		$allowed = array('xls', 'xlsx', 'csv');
		if($_FILES["uploadFile"]["name"]){
			if(!in_array($extension,$allowed)){
				echo 'File type is not allowed.';
			} else {
				if(move_uploaded_file($_FILES["uploadFile"]["tmp_name"],$target_file)){
					echo 'Successfully Uploaded.';
				}else{
					echo 'Error Uploading';
				}
			}
		}
	}else{
		redirect('index.php/login');
	}
}

//--------------------------------------------------------------DEDUCTIONS----------------------------------------------------------------

// public function employeeDeductions($coverage_id=NULL){

// 	$coverage = $this->PayrollModel->getAllWithValueCoverage();
// 	// var_dump($data['coverage']);
// 	if($coverage_id==NULL){
// 		$cnt = count($coverage);
// 		if($cnt==0){
// 			$datavalues = NULL;
// 		}else{
// 			$coverage_id = $coverage[$cnt-1]->coverage_id;
// 			$datavalues = $this->PayrollModel->getEmployeeDeductions($coverage_id);
// 		}
// 	}else{
// 		$datavalues = $this->PayrollModel->getEmployeeDeductions($coverage_id);
// 	}
// 	if($datavalues != NULL || $datavalues != ''){
// 		for($z = 0;$z<count($datavalues);$z++){
// 			$posempstat = $this->PayrollModel->getPosEmpStatId($datavalues[$z]->emp_id);
// 			$onPayroll = $this->PayrollModel->onPayroll($coverage_id,$posempstat->emp_promoteID);
// 			if($onPayroll!=NULL||$onPayroll!=''){
// 				$datavalues[$z]->isFinal = 1;
// 			}else{
// 				$datavalues[$z]->isFinal = 0;
// 			}

// 		}
// 	}
// 	$data['values'] = $datavalues;
// 	$notonlistfinal = array();
// 	$notonlist =  $this->PayrollModel->getUsersNotListedDeduct($coverage_id);
// 	foreach($notonlist as $n){
// 		array_push($notonlistfinal,$n->emp_id);
// 	}
// 	$data['count'] = $this->PayrollModel->countDeductions();
// 	$data['alldeductions'] = $this->PayrollModel->getAllDeductions();
// 	$data['usercount'] = $this->PayrollModel->countAllUsers($coverage_id);
// 	$data['notonlist'] =  $this->PayrollModel->getAllActiveEmployeeDeduct($notonlistfinal);
// 	$data['coverage'] = $coverage;
// 	$data['id'] = $coverage_id;

// 	$data['cstatus'] = $this->PayrollModel->getCoverageStatus($coverage_id);
// 	if($this->session->userdata('uid')){
// 		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/employeeDeductions' and is_assign=1 order by lname");
// 		foreach ($qry->result() as $row)
// 		{
// 			$arr['setting'] = array("settings" => $row->settings);
// 		}
// 		if ($qry->num_rows() > 0){
// 			$this->load->view('employeeDeductions',$data);
// 		}else{
// 			redirect('index.php/home/error');
// 		}
// 	}else{
// 		redirect('index.php/login');
// 	}
// }

public function employeeDeductions2($stafftype,$status,$coverage_id,$site=null){
	if($this->session->userdata('uid')){

	$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/employeeDeductions2' and is_assign=1");   

	if ($qry->num_rows() > 0){
		$statz = explode("-",$status);
 		if($statz[0]=='ProbReg'){

			$datavalues = $this->PayrollModel->getEmployeeDeductionsProbReg($stafftype,$statz[1],$coverage_id);
		}else{
			$datavalues = $this->PayrollModel->getEmployeeDeductions2($stafftype,$status,$coverage_id,$site);
		}
		if($datavalues != NULL || $datavalues != ''){
			for($z = 0;$z<count($datavalues);$z++){
				$posempstat = $this->PayrollModel->getPosEmpStatId($datavalues[$z]->emp_id);
				$onPayroll = $this->PayrollModel->onPayroll($coverage_id,$posempstat->emp_promoteID);
				if($onPayroll!=NULL||$onPayroll!=''){
					$datavalues[$z]->isFinal = 1;
				}else{
					$datavalues[$z]->isFinal = 0;
				}
			}
		}
		$data['values'] = $datavalues;
		$notonlistfinal = array();
		$notonlist2 =  $this->PayrollModel->getUsersNotListedDeduct($coverage_id);
 		if($statz[0]=='ProbReg'){
			$staffstatuslist = $this->PayrollModel->getSpecificActiveEmployeeProbReg($stafftype,$statz[1]);
		}else{
			$staffstatuslist = $this->PayrollModel->getSpecificActiveEmployee($stafftype,$status,$site);
		}
		$notonlist = array();
		foreach($staffstatuslist as $sslist => &$ss){
			foreach($notonlist2 as $noton){
				if($ss->emp_id==$noton->emp_id){
					unset($staffstatuslist[$sslist]);
				}
			}	
		}
		$data['count'] = $this->PayrollModel->countDeductions();
		$data['alldeductions'] = $this->PayrollModel->getAllDeductions();
		$data['usercount'] = $this->PayrollModel->countAllUsers($coverage_id);
		$data['notonlist'] =  $staffstatuslist;
		$data['coverage'] = $this->PayrollModel->getCoverageDate($coverage_id);
		$data['id'] = $coverage_id;
		$data['stafftype'] = $stafftype;
		$data['status'] = $status;
		$data['site'] = $site;
		$data['cstatus'] = $this->PayrollModel->getCoverageStatus($coverage_id);
		$this->load->view('employeeDeductions',$data);

	}else{
	redirect('index.php/home/error');
	}

	}
	
}

// public function addEmployeeDeductions(){
// 	$data['coverages'] = $this->PayrollModel->getAllWithValueCoverage();
// 	$this->load->view('addEmployeeDeductions',$data);
// 	// var_dump($data['coverages']);
// }

public function addEmployeeDeductions2($stafftype,$status,$coverage_id,$site=null){	
	$data['coverage'] = $this->PayrollModel->getCoverageDate($coverage_id);	
	$data['stafftype'] = $stafftype;
	$data['status'] = $status;
	$data['id'] = $coverage_id;
	$data['site'] = $site;
	$this->load->view('addEmployeeDeductions',$data);
	// var_dump($data['coverages']);
}
public function viewEmployeeDeductions(){
	if($this->session->userdata('uid')){
		$this->load->library('PHPExcel', NULL, 'excel');
		$objPHPExcel = PHPExcel_IOFactory::load($this->employeedeductions);
		$maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
		$data = $objPHPExcel->getActiveSheet()->rangeToArray('B3:' . $maxCell['column'] . $maxCell['row']);
		$data = array_map('array_filter', $data);
		$data = array_filter($data);

		if(empty($data)){
			echo 'No Data Found';
		}else{
			echo  json_encode($data);
		}
	}else{
		redirect('index.php/login');
	}
}

public function saveEmployeeDeductions($cover_id){

	if($this->session->userdata('uid')){
		$this->load->library('PHPExcel', NULL, 'excel');
		$objPHPExcel = PHPExcel_IOFactory::load($this->employeedeductions);
		$coverage_id = $objPHPExcel->getActiveSheet()->getCell('D2')->getValue();
		if($cover_id==$coverage_id){
			$maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
			$headerid = $objPHPExcel->getActiveSheet()->rangeToArray('E2:' . $maxCell['column'] .'2');
			$headeridnew = array();
			foreach($headerid as $h){
				for($y=0;$y<count($h);$y++){
					array_push($headeridnew,$h[$y]);
				}
			}
			$data = $objPHPExcel->getActiveSheet()->rangeToArray('A4:' . $maxCell['column'] . $maxCell['row']);
			 foreach ($data as $d) {
                    for ($x = 4; $x < count($d); $x++) {
                        $this->PayrollModel->deletePrevCoverageEmpDeductions($coverage_id, $d[0]);
                    }
                }
			foreach($data as $d){
				$y=0;
				for($x=4;$x<count($d);$x++){
					if($d[$x]!='0.00'){
						$data2 = $this->PayrollModel->empDedChecker($d[0],$coverage_id,$headeridnew[$y]);
						if(empty($data2)){

							$val = array(
								'emp_id'=>$d[0],
								'coverage_id'=>$coverage_id,
								'pdeduct_id'=>$headeridnew[$y],
								'value'=>str_replace(",","",$d[$x])
							);

							$rows = $this->PayrollModel->saveEmployeeDeductions($val);

						}else{
							$rows = $this->PayrollModel->updateEmployeeDeductions($d[0],$coverage_id,$headeridnew[$y],str_replace(",","",$d[$x]));

						}
						if($rows>0){
							echo "Success <br>";
						}else{
							echo "Failed <br>";
						}
					}
					$y++;
				}
			}
		}else{
			echo "DifferentPeriod";
		}
	}else{
		redirect('index.php/login');
	}
}

public function downloadEmployeeDeductions($stafftype,$status,$coverage_id,$site=null){
	if($stafftype==NULL && $status==NULL && $coverage_id==NULL){

		$employees = $this->PayrollModel->getAllActiveEmployee();
	}else{
		$statz = explode("-",$status);
 		if($statz[0]=='ProbReg'){
			$employees = $this->PayrollModel->getSpecificActiveEmployeeProbReg($stafftype,$statz[1]);
		}else{
			$employees = $this->PayrollModel->getSpecificActiveEmployee($stafftype,$status,$site);
		}
	}

	usort($employees, array($this, "cmp"));
	$deductions = $this->PayrollModel->getAllDeductions();
	$coverage = $this->PayrollModel->getCoverage($coverage_id);

	$new = explode(" - ",$coverage->daterange);
	$date1=date_create($new[0]);
	$date2=date_create($new[1]);
	$date1 = date_format($date1,"F d,Y");
	$date2 = date_format($date2,"F d,Y");

	if($this->session->userdata('uid')){
		$this->load->library('PHPExcel', NULL, 'excel');
	//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
	//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Employee Deductions');
		$this->excel->getActiveSheet()->setShowGridlines(false);
	//------------------------INSERT LOGO-------------------------------------------
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($this->logo);
	$objDrawing->setOffsetX(0);    // setOffsetX works properly
	$objDrawing->setOffsetY(0);  //setOffsetY has no effect
	$objDrawing->setCoordinates('A1');
	$objDrawing->setHeight(92); // logo height
	// $objDrawing->setWidth(320); // logo width
	// $objDrawing->setWidthAndHeight(200,400);
	$objDrawing->setResizeProportional(true);
	$objDrawing->setWorksheet($this->excel->getActiveSheet());
	//----------------------------------------------------------------------
	$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(75);

	//set cell A1 content with some text
	$headerrow = 3;
	$headercol = 3;
	$this->excel->getActiveSheet()->setCellValue('A3', 'ID Number');
	$this->excel->getActiveSheet()->setCellValue('B3', 'Full Name');
	$this->excel->getActiveSheet()->setCellValue('C3', 'Code');
	$this->excel->getActiveSheet()->setCellValue('D3', 'Status');
	$letters = array();
	$this->excel->getActiveSheet()->setCellValue('D1', "From:\n$date1\nTo:\n$date2");
	$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, $coverage_id);
	foreach($deductions as $deduct){
		$headercol++;
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($headercol, $headerrow, $deduct->deductionname);
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($headercol, 2, $deduct->pdeduct_id);
		$this->excel->getActiveSheet()->getRowDimension(2)->setVisible(false);
		array_push($letters,PHPExcel_Cell::stringFromColumnIndex($headercol));
	}

	//VALUES HERE
	$emprow = 4;
	foreach($employees as $emp){
		// if($emp->id_num==NULL||$emp->id_num==''){
		// 	$this->excel->getActiveSheet()->getStyle('A'.$emprow)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'ff4242'))));
		// }
		$this->excel->getActiveSheet()->setCellValue('A'.$emprow, $emp->emp_id);
		$this->excel->getActiveSheet()->setCellValue('B'.$emprow, $emp->lname.', '.$emp->fname);
		$this->excel->getActiveSheet()->setCellValue('C'.$emprow, $emp->pos_name);
		$this->excel->getActiveSheet()->setCellValue('D'.$emprow, $emp->status);
		
		$info = $this->PayrollModel->getAllEmpDeductionPerCoverage($emp->emp_id,$coverage_id);
		$cntr = 0;
		foreach($deductions as $ded){
			$val = 0;
			foreach($info as $nf){
				if($ded->pdeduct_id==$nf->pdeduct_id){
					$val = $nf->value;
				}
			}
			$this->excel->getActiveSheet()->setCellValue($letters[$cntr].''.$emprow, $val);
			$cntr++;
		}
		$emprow++;
	}

	$column = $this->excel->getActiveSheet()->getHighestColumn();
	$row = $this->excel->getActiveSheet()->getHighestRow();
	//merge cell A1 until D1
	$this->excel->getActiveSheet()->mergeCells('A1:C1');
	//set aligment to center for that merged cell (A1 to D1)
	$this->excel->getActiveSheet()->getStyle('A1:'.$column.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$this->excel->getActiveSheet()->getStyle('A1:'.$column.'1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$this->excel->getActiveSheet()->getStyle('A3:'.$column.'3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
	$this->excel->getActiveSheet()->getStyle('A3:'.$column.'3')->applyFromArray(array('font' => array('size' => 11,'bold' => true,'color' => array('rgb' => 'ffffff'))));


	$this->excel->getActiveSheet()->setAutoFilter('A3:'.$column.'3');
	$this->excel->getActiveSheet()->getProtection()->setSort(true);

	$this->excel->getActiveSheet()->getStyle('A3:'.$column.''.$row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));

	$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	foreach($letters as $l){
		$this->excel->getActiveSheet()->getColumnDimension($l)->setAutoSize(true);

		// $this->excel->getActiveSheet()->getColumnDimension($l)->setWidth(15);
	}
// echo $this->excel->getActiveSheet()->getCell('G2')->getValue();
	$this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
	$this->excel->getActiveSheet()->getProtection()->setSheet(true);
	$this->excel->getActiveSheet()->getStyle('E4:'.$column.''.$row)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
	$this->excel->getActiveSheet()->getStyle('E4:'.$column.''.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

	$objValidation = $this->excel->getActiveSheet()->getCell('E4')->getDataValidation();
	$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
	$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
	$objValidation->setAllowBlank(true);
	$objValidation->setShowInputMessage(true);
	$objValidation->setShowErrorMessage(true);
	$objValidation->setErrorTitle('Input error');
	$objValidation->setError('Letters are not allowed!');
	$objValidation->setPromptTitle('Allowed input');
	$objValidation->setFormula1(0);
	$objValidation->setFormula2(1000000);
	$j = 4;
	while($j <= $row)
	{ 
		foreach($letters as $l){
			$this->excel->getActiveSheet()->getCell("$l$j")->setDataValidation(clone $objValidation); 
		}
		$j++;
	}

	$this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(FALSE);
$filename='EmployeeDeductionsTemplate.xlsx'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache

//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
}else{
	redirect('index.php/login');
}
}
public function uploadEmployeeDeductions(){
	if($this->session->userdata('uid')){
		$path = $_FILES["uploadFile"]["name"];
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Manila');	

		$target_dir = "./reports/";
		$new_name = "employeedeductions";
		$target_file = $target_dir.$new_name.'.'.$extension;

		$allowed = array('xls', 'xlsx', 'csv');

		if($_FILES["uploadFile"]["name"]){
			if(!in_array($extension,$allowed)){
				echo 'File type is not allowed.';
			} else {
				if(move_uploaded_file($_FILES["uploadFile"]["tmp_name"],$target_file)){
					echo 'Successfully Uploaded.';
				}else{
					echo 'Error Uploading';
				}

			}

		}
	}else{
		redirect('index.php/login');
	}
}

//------------------------------------------------------UPDATEEMP-----------------------------------------------------------------------

public function updateEmpDeduct(){
	$coverageid = $this->input->post('coverageid');
	$empid = $this->input->post('empid');
	$deductionid = $this->input->post('deductionid');
	$value = (double)$this->input->post('value');
	$row = $this->PayrollModel->updateEmpDeduct($empid,$coverageid,$deductionid,$value);
	if($row>0){
		echo $value;
		// var_dump($row);
	}else{
		// var_dump($row);
		echo 'Failed';
	}
}
public function updateEmpAdd(){
	$coverageid = $this->input->post('coverageid');
	$empid = $this->input->post('empid');
	$additionid = $this->input->post('additionid');
	$value = (double)$this->input->post('value');
	$row = $this->PayrollModel->updateEmpAdd($empid,$coverageid,$additionid,$value);
	if($row>0){
		echo $value;
		// var_dump($row);
	}else{
		// var_dump($row);
		echo 'Failed';
	}
}


public function getEmpAddExistAdditions(){
	$coverage_id = $this->input->post('coverage_id');
	$empid = $this->input->post('empid');
	$ids = $this->input->post('ids');
	$addids = explode('|',$ids);
	$data = $this->PayrollModel->getEmpAddExistAdditions($coverage_id,$empid,$addids);
	if($data==NULL){
		echo 'Empty';
	}else{
		echo json_encode($data);
	}

}

public function addExistEmpAddition(){
	$coverage_id = $this->input->post('coverage_id');
	$empid = $this->input->post('empid');
	$addition_id = $this->input->post('addition_id');
	$value = $this->input->post('value');
	$data = array(
		'emp_id' => $empid,
		'coverage_id' => $coverage_id,
		'paddition_id' => $addition_id,
		'value' => $value
	);
	$rows = $this->PayrollModel->addExistEmpAddition($data);
	if($rows>0){
		$this->session->set_flashdata('success', 'Successfully Added Employee Addition');
		echo 'Success';
	}else{
		echo 'Failed';
	}
}

public function getEmpAddExistDeductions(){
	$coverage_id = $this->input->post('coverage_id');
	$empid = $this->input->post('empid');
	$ids = $this->input->post('ids');
	$deductids = explode('|',$ids);
	$data = $this->PayrollModel->getEmpAddExistDeductions($coverage_id,$empid,$deductids);
	if($data==NULL){
		echo 'Empty';
	}else{
		echo json_encode($data);
	}

}

public function addExistEmpDeduction(){
	$coverage_id = $this->input->post('coverage_id');
	$empid = $this->input->post('empid');
	$deduction_id = $this->input->post('deduction_id');
	$value = $this->input->post('value');
	$data = array(
		'emp_id' => $empid,
		'coverage_id' => $coverage_id,
		'pdeduct_id' => $deduction_id,
		'value' => $value
	);
	$rows = $this->PayrollModel->addExistEmpDeduction($data);
	if($rows>0){

		$this->session->set_flashdata('success', 'Successfully Added Employee Addition');
		echo 'Success';
	}else{
		echo 'Failed';
	}
}
public function employeetaxtype(){
	if($this->session->userdata('uid')){
		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/Employeetaxtype' and is_assign=1");
		foreach ($qry->result() as $row)
		{
			$arr['setting'] = array("settings" => $row->settings);
		}
		if ($qry->num_rows() > 0){
			$data['taxdesc'] = $this->PayrollModel->getTaxDesc();
			$data['values'] = $this->PayrollModel->getEmpTaxtype();
			$this->load->view('employeetaxtype',$data);
		}else{
			redirect('index.php/home/error');
		}
	}else{
		redirect('index.php/login');
	}
	
}

public function insertNewTaxValue(){

	$taxid = $this->input->post('taxid');
	$emp_id = $this->input->post('emp_id');
	$query = $this->db->query("SELECT taxemp_id FROM tbl_tax_emp where emp_id='".$emp_id."' AND isActive=1");
	$result = $query->result();
	if($this->db->affected_rows()>0){
		foreach($result as $r){
			$this->db->where('taxemp_id', $r->taxemp_id);
			$this->db->update('tbl_tax_emp', array('isActive'=>'0'));
		}
	}
	$data = array('tax_desc_id' => $taxid, 'emp_id' => $emp_id,'isActive'=>'1');
	$this->db->insert('tbl_tax_emp', $data);
	// echo $this->db->last_query();
	if( $this->db->affected_rows()>0){
		echo 'Success';
	}else{
		echo 'Fail';
	}
}
//new
public function onPayroll(){
	$emp_id = $this->input->post('emp_id');
	$coverage_id = $this->input->post('coverage_id');

	$posempstat = $this->PayrollModel->getPosEmpStatId($emp_id);
	$onPayroll = $this->PayrollModel->onPayroll($coverage_id,$posempstat->emp_promoteID);
	if($onPayroll!=NULL||$onPayroll!=''){
		echo 'Yes';
	}else{
		echo 'No';
	}
}
public function generatePayroll($stat=null,$pid=null,$site=null){
	
	if($this->session->userdata('uid')){
		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='GeneratePayroll/$stat' and is_assign=1");
		foreach ($qry->result() as $row)
		{
			$arr['setting'] = array("settings" => $row->settings);
		}
		if ($qry->num_rows() > 0){


			if($pid>0){
				$per_id =  $this->db->query("select coverage_id,daterange from tbl_payroll_coverage where coverage_id=".$pid."");
				foreach ($per_id->result() as $row)
				{
					$arr['period'] = array(
						"daterange" => $row->daterange,
						"coverage_id" => $row->coverage_id,

					);
				}
				
				$arr["stat"] = array("status" => $stat);
				$arr["site"] = array("loc" => $site);
				$this->load->view('generatePayroll',$arr);
			}else{

			}
		}else{
			  redirect('index.php/home/error');
			 //echo "GeneratePayroll/$stat";
 		}
	}else{
		redirect('index.php/login');
	}
}
public function payroll_index($pid=null){
	
		if($this->session->userdata('uid')){
		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/generatePayrolls' and is_assign=1 ");
		foreach ($qry->result() as $row)
		{
			$arr['setting'] = array("settings" => $row->settings);
		}
		if ($qry->num_rows() > 0){


			if($pid>0){
				$per_id =  $this->db->query("select coverage_id,daterange from tbl_payroll_coverage where coverage_id=".$pid."");
				foreach ($per_id->result() as $row)
				{
					$arr['period'] = array(
						"daterange" => $row->daterange,
						"coverage_id" => $row->coverage_id,

					);
				}
				
 				$this->load->view('payroll_index',$arr);
			}else{
				echo "ERROR";
			}
		}else{
			redirect('index.php/home/error');
		}
	}else{
		redirect('index.php/login');
	}
	
}
public function pcInsert(){
	$pcyear = $this->input->post('pcyear');
	$pcmonth = $this->input->post('pcmonth');
	$pcperiod = $this->input->post('pcperiod');
	$pcDescription = $this->input->post('pcDescription');
	$transact_date= explode("-",$pcDescription);
	$transact_date2= date('y-m-d',strtotime($transact_date[1]."+1 days"));


	$getLoan = $this->db->query("select b.emp_id,a.fname,a.lname from tbl_applicant a,tbl_employee b,tbl_user c,tbl_emp_promote d where a.apid=b.apid and b.emp_id=d.emp_id and b.emp_id = c.emp_id and b.isActive='Yes' and c.isActive=1 and d.isActive=1");
	$qry =  $this->db->query("insert into tbl_payroll_coverage(daterange,transact_date,month,year,period,status) values('$pcDescription','".$transact_date2."','$pcmonth','$pcyear','$pcperiod','Pending')");
	$id = $this->db->insert_id();
		if($this->db->affected_rows() >0){
			
 
 
			$savefile0 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/$pcyear/"; 
			$savefile1 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/$pcyear/$pcmonth/"; 
			$savefile2 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/$pcyear/$pcmonth/$pcperiod/"; 
			$savefile3 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/$pcyear/$pcmonth/$pcperiod/agent"; 
			$savefile4 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/$pcyear/$pcmonth/$pcperiod/admin"; 
			$savefile5 = "/usr/local/www/apache24/data/supportzebra/reports/payroll/$pcyear/$pcmonth/$pcperiod/report"; 
			//echo $id;
			if (!file_exists($savefile2)){
				 mkdir($savefile0, 0777, true);
				 chmod($savefile0, 0777); 
				 mkdir($savefile1, 0777, true);
				 chmod($savefile1, 0777); 
				 mkdir($savefile2, 0777, true);
				 chmod($savefile2, 0777); 
				 mkdir($savefile3, 0777, true);
				 chmod($savefile3, 0777); 
				 mkdir($savefile4, 0777, true);
				 chmod($savefile4, 0777);
				 mkdir($savefile5, 0777, true);
				 chmod($savefile5, 0777);
				 echo $savefile2;
			}else{
			echo "wala";
			}
			
		
 		}else{
			echo 0;

		}
	

}
 public function loanList($empid){
			$query = $this->db->query("SELECT * FROM `tbl_payroll_emp_adjustment` where isActive=1 and emp_id =".$empid);
			 return $query->result();

		 }
 public function loanList2($pemp_adjust_id){
			$query = $this->db->query("SELECT count(*) as cnt FROM `tbl_payroll_deduct_child` where pemp_adjust_id =".$pemp_adjust_id);
			 return $query->result();

		 }
public function pcCheck(){
	$pcyear = $this->input->post('pcyear');
	$pcmonth = $this->input->post('pcmonth');
	$pcperiod = $this->input->post('pcperiod');
	if(!empty($pcyear) and !empty($pcmonth) and !empty($pcperiod) ){
		$qry =  $this->db->query("select count(*) as cnt from tbl_payroll_coverage where year=".$pcyear." and month='".$pcmonth."'  and period=".$pcperiod."");
		foreach ($qry->result() as $row)
		{
			echo $row->cnt;
		}
	}else{
		echo "3";
	}
}
public function generatePayrolls(){
 
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/GeneratePayrolls' and is_assign=1");
			$pcover =  $this->db->query("select * from tbl_payroll_coverage order by coverage_id DESC");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			foreach ($pcover->result() as $row)
			{
				$arr['pcover'][$row->coverage_id] = array(
					"coverage_id" => $row->coverage_id,
					"daterange" => $row->daterange,
					"transact_date" => $row->transact_date,
					"status" => $row->status,
				);
			}
			if ($qry->num_rows() > 0){

				$this->load->view('generatePayroll2',$arr);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}
	 
}

//---------------------------NEW CODES----------------------------------------------------------
public function employeepaytype(){

	$data['values'] = $this->PayrollModel->getEmpPaytype();
	$this->load->view('employeepaytype',$data);        
}
public function updateSalaryMode(){
	$value = $this->input->post('value');
	$emp_id = $this->input->post('emp_id');
	$row = $this->PayrollModel->updateSalaryMode($value,$emp_id);
	if($row>0){
		echo 'Success';
	}else{
		echo 'Failed';
	}
}

public function holidaytime(){
	 //  $qry =  $this->db->query("SELECT * FROM `tbl_time` where ho_start_time='' and ho_end_time='' and  bta_id=1");
	$qry =  $this->db->query("SELECT * FROM `tbl_time` where   bta_id=1");
		foreach($qry->result() as $row){
				$arr['nd'][$row->time_id] = array(
					"time_id" => $row->time_id,
					"time_start" => $row->time_start,
					"time_end" => $row->time_end,
					"ho_time_start" => $row->ho_start_time,
					"ho_time_end" => $row->ho_end_time,
 				);
       }
	 $this->load->view('holidaytime',$arr);
}
public function nightdiff(){
	 // $qry =  $this->db->query("SELECT a.time_id,time_start,time_end,pnd_id,hour,minutes,isActive,nd_time_start,nd_time_end FROM tbl_time a, `tbl_payroll_nd` b where a.time_id = b.time_id and time_start!='TIME' and nd_time_start='0.00' and nd_time_end='0.00'");
	 $qry =  $this->db->query("SELECT a.time_id,time_start,time_end,pnd_id,hour,minutes,isActive,nd_time_start,nd_time_end FROM tbl_time a, `tbl_payroll_nd` b where a.time_id = b.time_id and time_start!='TIME' ");
 		foreach($qry->result() as $row){
				$arr['nd'][$row->time_id] = array(
					"time_id" => $row->time_id,
					"pnd_id" => $row->pnd_id,
					"time_start" => $row->time_start,
					"time_end" => $row->time_end,
					"hour" => $row->hour,
					"minutes" => $row->minutes,
					"isActive" => $row->isActive,
					"nd_time_start" => $row->nd_time_start,
					"nd_time_end" => $row->nd_time_end,
 				);
       }
	    $shiftND =  $this->db->query("SELECT a.time_id,time_start,time_end FROM tbl_time a left join `tbl_payroll_nd` b on a.time_id = b.time_id WHERE pnd_id is null and bta_id=1");
	 		foreach($shiftND->result() as $row){
			$arr["shiftND"][$row->time_id] = array(
					"time_start" => $row->time_start,
					"time_end" => $row->time_end,
				);   
				}
	 $this->load->view('nightdiff',$arr);
	}
	
	public function updateND(){
	$nd = trim($this->input->post('nd'));
	$ndSHol = trim($this->input->post('ndSHol'));
	$ndEHol = trim($this->input->post('ndEHol'));
	$txtIDTym = trim($this->input->post('txtIDTym'));
	if($nd!=" "){
		$equal = explode(".",$nd);
		$eq2 = (($equal[1]/100)*60);
	 $qry =  $this->db->query("update tbl_payroll_nd set hour=".$equal[0].",minutes=".$eq2." where time_id=".$txtIDTym);
	}
		if($ndSHol!=" " && $ndEHol!=" "){
		 $qry =  $this->db->query("update tbl_payroll_nd set nd_time_start=".$ndSHol.",nd_time_end=".$ndEHol." where time_id=".$txtIDTym);
		}
	}
	public function addND(){
	$nd = trim($this->input->post('nd'));
	$ndSHol = trim($this->input->post('ndSHol'));
	$ndEHol = trim($this->input->post('ndEHol'));
	$ShiftSelect = trim($this->input->post('ShiftSelect'));
	if($nd!=" " && $ndSHol!=" " && $ndEHol!=" "){
		$equal = explode(".",$nd);
		$eq2 = (($equal[1]/100)*60);
	 $qry =  $this->db->query("insert into tbl_payroll_nd(hour,minutes,nd_time_start,nd_time_end,date_effect,isActive,time_id) values('$equal[0]','$eq2','$ndSHol','$ndEHol',curdate(),1,$ShiftSelect)");
	}
	}
 
	public function checkND(){
	$time_id = $this->input->post('time_id');
	 $qry =  $this->db->query("SELECT a.time_id,time_start,time_end,pnd_id,hour,minutes,isActive,nd_time_start,nd_time_end FROM tbl_time a, `tbl_payroll_nd` b where a.time_id = b.time_id and time_start!='TIME' and a.time_id=".$time_id);
	 		foreach($qry->result() as $row){
			$hour = abs($row->hour);
			$minutes = ($row->minutes/60);
				echo $hour+$minutes."|".$row->nd_time_start."|".$row->nd_time_end;
			}
 	}
	
	
	public function updateHO(){
 	$ndSHol = trim($this->input->post('ndSHol'));
	$ndEHol = trim($this->input->post('ndEHol'));
	$txtIDTym = trim($this->input->post('txtIDTym'));
	 
		if($ndSHol!=" " && $ndEHol!=" "){
		 $qry =  $this->db->query("update tbl_time set ho_start_time=".$ndSHol.",ho_end_time=".$ndEHol." where time_id=".$txtIDTym);
		}
	}
	
	public function checkHo(){
	$time_id = $this->input->post('time_id');
	 $qry =  $this->db->query("SELECT * from tbl_time where time_id=".$time_id);
	 		foreach($qry->result() as $row){
				echo $row->ho_start_time."|".$row->ho_end_time;
			}
 	}
	public function statCoverage(){
			$code = $this->input->post('code');
			if($code==1){
				$status = "Review";
			}else if($code==2){
				$status = "Final";
			}else{
				$status = "Pending";
			}
			$cid = $this->input->post('cid');
			$row = $this->PayrollModel->updatePayrollPeriodStatus($status,$cid);
			if($row>0){
				echo $status;
			}else{
				echo 'Failed';
			}

	}
	public function report1(){
		 // $qry =  $this->db->query("SELECT sum(final) as sum FROM `tbl_payroll` a,tbl_payroll_coverage b  where a.coverage_id=b.coverage_id and b.year= DATE_FORMAT(curdate(), '%Y') and emp_type='agent-probationary'");
		 $qry =  $this->db->query("SELECT * FROM `tbl_payroll` a,tbl_payroll_coverage b  where a.coverage_id=b.coverage_id and b.year= DATE_FORMAT(curdate(), '%Y') and emp_type like '%agent%' group by a.coverage_id");
		 if($qry->num_rows() >0 ){
		foreach($qry->result() as $row){
			$trainee = $this->traineeTotalP($row->month,$row->year);
			$prob = $this->probTotalP($row->month,$row->year);
			$reg = $this->regTotalP($row->month,$row->year);
				$arr['total'][$row->month." ".$row->year] = array(
					  "trainee" => (count($trainee) >0) ?  (int)$trainee[0]->sum: 0,
					  "prob" => (count($prob) >0) ?   (int)$prob[0]->sum: 0,
					"reg" => (count($reg) >0) ? (int)$reg[0]->sum: 0,
  				);
		}}else{
			$arr['total'] = Array(0);
		
		}
 			//echo json_encode($arr);
			
			$this->load->view('payrollReport1',$arr);
 
		}
	public function reportEmpType(){
	$selectedVal = $this->post->input('selectedVal');
		 // $qry =  $this->db->query("SELECT sum(final) as sum FROM `tbl_payroll` a,tbl_payroll_coverage b  where a.coverage_id=b.coverage_id and b.year= DATE_FORMAT(curdate(), '%Y') and emp_type='agent-probationary'");
 		 $qry =  $this->db->query("SELECT * FROM `tbl_payroll` a,tbl_payroll_coverage b  where a.coverage_id=b.coverage_id and b.year= '2017' and emp_type like '%agent%' group by a.coverage_id");
		foreach($qry->result() as $row){
			$trainee = $this->traineeTotalP($row->month,$row->year);
			$prob = $this->probTotalP($row->month,$row->year);
			$reg = $this->regTotalP($row->month,$row->year);
				$arr['total'][$row->month] = array(
					  "trainee" => (count($trainee) >0) ?  (int)$trainee[0]->sum: 0,
					  "prob" => (count($prob) >0) ?   (int)$prob[0]->sum: 0,
					"reg" => (count($reg) >0) ? (int)$reg[0]->sum: 0,
  				);
		}
 		 echo json_encode($arr);
			
  
		}
			public function traineeTotalP($month,$year){
			$qry =  $this->db->query("SELECT sum(final) as sum,emp_type,month,a.coverage_id,year FROM `tbl_payroll` a,tbl_payroll_coverage b  where a.coverage_id=b.coverage_id and b.year= '$year' and emp_type='agent-trainee'  and month='$month'   group by month");
		 
			return $qry->result();
			}
			
			
			public function probTotalP($month,$year){
			$qry =  $this->db->query("SELECT sum(final) as sum,emp_type,month,a.coverage_id,year FROM `tbl_payroll` a,tbl_payroll_coverage b  where a.coverage_id=b.coverage_id and b.year= '$year'    and emp_type='agent-probationary' and month='$month' group by month");
			 
			return $qry->result();
			}
			
		
			public function regTotalP($month,$year){
			$qry =  $this->db->query("SELECT sum(final) as sum,emp_type,month,a.coverage_id,year FROM `tbl_payroll` a,tbl_payroll_coverage b  where a.coverage_id=b.coverage_id and b.year= '$year'   and emp_type='agent-regular'  and month='$month'  group by month ");
		 
			return $qry->result();
			}
			/*********************************************REPORT*********************************************************/
			 public function pagibigcontribution_report(){
			 if($this->session->userdata('uid')){
					$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/pagibigcontribution_report' and is_assign=1");
					foreach ($qry->result() as $row)
					{
						$arr['setting'] = array("settings" => $row->settings);
					}
					if ($qry->num_rows() > 0){

						$data['index'] = $this->PayrollModel->pagibig_report_index();
 
							// $data['list'] = $this->PayrollModel->pagibig_contri(90);

						$this->load->view('pagibigcontri_report',$data);
					}else{
						redirect('index.php/home/error');
					}
				}else{
					redirect('index.php/login');
				}
			 }
			 public function pagibigReport(){
				 $cid = $this->input->post("cid");
				$data["list"] = $this->PayrollModel->pagibig_contri($cid);
				$data["add_pagibig"] = $this->PayrollModel->pagibig_contri_add($cid);
					
					echo json_encode($data);
			 }
			 
			 public function exportbirReport_excel(){
				 $cid = $this->input->post("cid");
				 $cidYear = $this->input->post("cidYear");
				$objPHPExcel = $this->phpexcel;
				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$type2= "report";
				$sheet = $objPHPExcel->getActiveSheet(0);

				$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
				$style_text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
				$text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					) 
				);
				$text_left_BOLD = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					),
					'font' => array(
						'bold' => true
					)
				);



					$sheet->setShowGridlines(false);
					$objPHPExcel->createSheet();
					$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payroll");
					
					
					
					$sheet->setCellValue('A1',"FBC BUSINESS SOLUTIONS");
					$sheet->setCellValue('A2',"Tax computation");
					$sheet->setCellValue('J2',"Taxable income");
					$sheet->setCellValue('L2',"Tax");
					$sheet->setCellValue('A3'," ");
					$sheet->setCellValue('B3',"Status");
					$sheet->setCellValue('C3',"Relationship");
					$sheet->setCellValue('D3',"Basic Salary");
					$sheet->setCellValue('E3',"SSS");
					$sheet->setCellValue('F3',"HDMF");
					$sheet->setCellValue('G3',"PHIC");
					$sheet->setCellValue('H3',"Taxable income");
					$sheet->setCellValue('I3',"Non-taxable income");
					$sheet->setCellValue('J3',"15th");
					$sheet->setCellValue('K3',"30th");
					$sheet->setCellValue('L3',"15TH");
					$sheet->setCellValue('M3',"30th");
					$sheet->setCellValue('N3'," ");
					$data["emp"] = $this->PayrollModel->tax_contri($cid,0,$cidYear);
					$roww=4;
					foreach($data["emp"] as $row){
						$data["list"]= $this->PayrollModel->tax_contri($cid,$row->emp_promoteId,$cidYear);
							foreach($data["list"] as $row2){
								  $arr[$row->emp_id][$row2->period] = array(
									"lname" => $row->lname,
									"fname" => $row->fname,
									"mname" => $row->mname,
									"quinsina" => $row2->quinsina,
									"sss" => $row2->sss_details,
									"phic" => $row2->phic_details,
									"hdmf" => $row2->hdmf_details,
									"bir" => $row2->bir_details,
									"period" => $row2->period,
									"emp_promoteId" => $row2->emp_promoteId,
									"salarymode" => $row2->salarymode,
									"isDaily" => $row2->isDaily
								);   
							}
						}
						//echo json_encode($arr);
						$salary=0;
						$total_salary=0;
						$taxable=0;
						$ntaxable=0;
						$total_sss=0;
						$total_hdmf=0;
						$total_phic=0;
						$total_taxable=0;
						$total_ntaxable=0;
						$total_J=0;
						$total_K=0;
						$total_L=0;
						$total_M=0;
						$total_ntaxable_daily=0;
						foreach($arr as $row => $val){
									foreach($val as $row2 => $val2){
										$lname= $val2["lname"];
										$fname= $val2["fname"];
										$quinsina= $val2["quinsina"];
										$sss_details= $val2["sss"];
										$hdmf_details= $val2["hdmf"];
										$phic_details= $val2["phic"];
										$bir=explode("(",$val2["bir"]);
										$isDaily=$val2["isDaily"];
										if($val2["isDaily"]==1){
											$salary+=$val2["quinsina"];
											
										}else{
											$salary=$val2["quinsina"];
										}
										$ar_count = count($val);
										$period = $val2["period"];
										$isDaily = ($val2["salarymode"]=="Daily") ? "1 month period" : "-";

									}
								

								if($bir[0]!="0.00"){
									$ntaxable=$sss_details + $hdmf_details + $phic_details;
								}else{
									$ntaxable=$salary + $sss_details + $hdmf_details + $phic_details;
								}
								$taxable= ($bir[0]!="0.00") ? ($salary-$ntaxable) : 0.00;
								$p1 = ($period==1) ? $taxable : "0.00";
								$p2 = ($period==2) ? $taxable : "0.00";
								$t1 = ($period==1) ? $bir[0] : "0.00";
								$t2 = ($period==2) ? $bir[0] : "0.00";
							  $sheet->setCellValue('A'.$roww,$lname.", ".$fname);
								$sheet->setCellValue('B'.$roww,"-");
								$sheet->setCellValue('C'.$roww,"-");
								$sheet->setCellValue('D'.$roww,$salary);
								$sheet->setCellValue('E'.$roww,$sss_details);
								$sheet->setCellValue('F'.$roww,$hdmf_details);
								$sheet->setCellValue('G'.$roww,$phic_details);
								$sheet->setCellValue('H'.$roww,$taxable);
								$sheet->setCellValue('I'.$roww,$ntaxable);
								if($ar_count==2){
									$sheet->setCellValue('J'.$roww,$taxable);
									$sheet->setCellValue('K'.$roww,$taxable);
									$sheet->setCellValue('L'.$roww,$bir[0]);
									$sheet->setCellValue('M'.$roww,$bir[0]);
										$total_J+=$taxable;
										$total_K+=$taxable;
										$total_L+=$bir[0];
										$total_M+=$bir[0];
								}else{
									$sheet->setCellValue('J'.$roww,$p1);
									$sheet->setCellValue('K'.$roww,$p2);
									$sheet->setCellValue('L'.$roww,$t1);
									$sheet->setCellValue('M'.$roww,$t2);
										$total_J+=$p1;
										$total_K+=$p2;
										$total_L+=$t1;
										$total_M+=$t2;
								}
								$sheet->setCellValue('N'.$roww,$isDaily );
								$sheet->getStyle("A".$roww.":N".$roww)->applyFromArray($style_text_left);
								$sheet->getStyle("A3:N3")->applyFromArray($style);
								$sheet->getStyle("A3:N3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('7b7b7b');
								$sheet->getStyle("D".$roww.":M".$roww)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

								foreach(range('A','Z') as $columnID){
									$sheet->getColumnDimension($columnID)->setAutoSize(true);
								}
								$roww++; 
								$total_salary+=$salary;
								$total_sss+=$sss_details;
								$total_hdmf+=$hdmf_details;
								$total_phic+=$phic_details;
								$total_taxable+=$taxable;
								$total_ntaxable+=$ntaxable;
								$total_ntaxable_daily += ($isDaily==1) ? $ntaxable :0;
								$salary=0;
								$taxable=0;
								$ntaxable=0;
						}
							$sheet->getStyle("A".$roww.":N".$roww)->applyFromArray($text_left_BOLD);

							$sheet->setCellValue('D'.$roww,$total_salary);
							$sheet->setCellValue('E'.$roww,$total_sss);
							$sheet->setCellValue('F'.$roww,$total_hdmf);
							$sheet->setCellValue('G'.$roww,$total_phic);
							$sheet->setCellValue('H'.$roww,$total_taxable);
							$sheet->setCellValue('I'.$roww,$total_ntaxable);
							$sheet->setCellValue('J'.$roww,$total_J);
							$sheet->setCellValue('K'.$roww,$total_K);
							$sheet->setCellValue('L'.$roww,$total_L);
							$sheet->setCellValue('M'.$roww,$total_M);
							$sheet->getStyle('D'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('E'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('F'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('G'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('H'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('I'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('J'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('K'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('L'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('M'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');

							$sheet->setCellValue('I'.($roww+4),"Non Taxable Compensation");
							$sheet->setCellValue('J'.($roww+4),"Taxable Compensation");
							$sheet->setCellValue('K'.($roww+4),"Total Compensation");
							$sheet->setCellValue('M'.($roww+4),"Tax Payable");
							
							$sheet->setCellValue('I'.($roww+5),$total_ntaxable);
							$sheet->setCellValue('J'.($roww+5),($total_J+$total_K));
							$sheet->setCellValue('K'.($roww+5),$total_ntaxable +($total_J+$total_K));
							$sheet->setCellValue('M'.($roww+5),($total_L+$total_M));
							$sheet->getStyle('I'.($roww+5))->applyFromArray($text_left);
							$sheet->getStyle('J'.($roww+5))->applyFromArray($text_left);
							$sheet->getStyle('K'.($roww+5))->applyFromArray($text_left);
							$sheet->getStyle('M'.($roww+5))->applyFromArray($text_left);
							$sheet->getStyle('I'.($roww+5))->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('J'.($roww+5))->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('K'.($roww+5))->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('M'.($roww+5))->getNumberFormat()->setFormatCode('#,##0.00');
								
							
							$sheet->setCellValue('H'.($roww+7),"MWE");
							$sheet->setCellValue('H'.($roww+8),"Other NTC");
							$sheet->setCellValue('I'.($roww+7),$total_ntaxable_daily);
							$sheet->setCellValue('I'.($roww+8),($total_ntaxable-$total_ntaxable_daily));
							$sheet->setCellValue('I'.($roww+9),$total_ntaxable);
								$sheet->getStyle('I'.($roww+7))->applyFromArray($text_left);						
								$sheet->getStyle('I'.($roww+8))->applyFromArray($text_left);						
								$sheet->getStyle('I'.($roww+9))->applyFromArray($text_left);						
								$sheet->getStyle('I'.($roww+7))->getNumberFormat()->setFormatCode('#,##0.00');
								$sheet->getStyle('I'.($roww+8))->getNumberFormat()->setFormatCode('#,##0.00');
								$sheet->getStyle('I'.($roww+9))->getNumberFormat()->setFormatCode('#,##0.00');

					$file = 'PAYROLL_BIR_REPORT_'.$cidYear.'_'.$cid.'_bir'.$this->session->userdata('uid').'.xlsx';
					$path= "reports/payroll/".$file;
			 
			  header("Pragma: public");
			  header("Expires: 0");
			  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			  header("Content-Type: application/force-download");
			  header("Content-Type: application/octet-stream");
			  header("Content-Type: application/download");;
			  header("Content-Disposition: attachment; filename=$file");
			  header("Content-Transfer-Encoding: binary ");
			  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007'); 
			  $objWriter->save($path);
			  ob_clean();
			  return $path;  
			 }
			 public function taxReport(){
				 $cid = $this->input->post("cid");
				 $cidYear = $this->input->post("cidYear");
				$data["emp"] = $this->PayrollModel->tax_contri($cid,0,$cidYear);
					$arr= array();
					foreach($data["emp"] as $row){
						$data["list"]= $this->PayrollModel->tax_contri($cid,$row->emp_promoteId,$cidYear);
							foreach($data["list"] as $row2){
								$arr["test"][$row->emp_id][$row2->payroll_id] = array(
									"lname" => $row->lname,
									"fname" => $row->fname,
									"mname" => $row->mname,
									"quinsina" => $row2->quinsina,
									"sss" => $row2->sss_details,
									"phic" => $row2->phic_details,
									"hdmf" => $row2->hdmf_details,
									"bir" => $row2->bir_details,
									"period" => $row2->period,
									"emp_promoteId" => $row2->emp_promoteId,
									"salarymode" => $row2->salarymode,
									"isDaily" => $row2->isDaily
								); 
							}
						}
					echo (count($arr)>0) ? json_encode($arr) : 0;
			 }
			public function taxcontribution_report(){
			 if($this->session->userdata('uid')){
					$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/taxcontribution_report' and is_assign=1");
					foreach ($qry->result() as $row)
					{
						$arr['setting'] = array("settings" => $row->settings);
					}
					if ($qry->num_rows() > 0){

						$data['index'] = $this->PayrollModel->pagibig_report_index();
							// $data['list'] = $this->PayrollModel->pagibig_contri(90);

						$this->load->view('taxcontribution_report',$data);
					}else{
						redirect('index.php/home/error');
					}
				}else{
					redirect('index.php/login');
				}
			 }
			public function getcoverage_date(){
					 $cid = $this->input->post("cid");
					 $empzType = $this->input->post("empzType");
					 $empzCat = $this->input->post("empzCat");
						if($empzType=="Admin"){
							if(trim($empzCat)=="Probationary_and_Regular"){
								$type= "admin-both-nonconfi";

							}else if(trim($empzCat)=="Confidential"){
								$type= "admin-both-confi";

							}else{
								$type= "admin-trainee";
							}
						}else{
							$type= strtolower($empzType)."-".strtolower($empzCat);
						}
						$data = $this->PayrollModel->pagibig_get_coverage($cid);
						foreach($data as $row){
							echo $row->year."|".ucwords($row->month)."|".$row->period."|".$this->session->userdata('uid')."|".strtoupper($row->month)."|".strtoupper(trim($empzType))."|".strtoupper(trim($empzCat));
						}
			 }
			 public function exportpagibig_excel(){
		$objPHPExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$payrollC = $this->input->post('cid');
		$sheet = $objPHPExcel->getActiveSheet(0);
			$coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=(select coverage_id from tbl_payroll_coverage where period=1 and month='".$payrollC."')");
			$year= $coverage->result()[0]->year;
			$month= $coverage->result()[0]->month; 
			$period= $coverage->result()[0]->period;
			$data["list"] = $this->PayrollModel->pagibig_contri($payrollC);
				$data["add_pagibig"] = $this->PayrollModel->pagibig_contri_add($payrollC);
	$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
			 $style_text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					)
				);


				$month_arr = array(
				"January" => 1,
				"February" => 2,
				"March" => 3,
				"April" => 4,
				"May" => 5,
				"June" => 6,
				"July" => 7,
				"August" => 8,
				"September" => 9,
				"October" => 10,
				"November" => 11,
				"December" => 12			
				);
			$sheet->setShowGridlines(false);
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payslip");
				$sheet->setCellValue('A1',"Pag-IBIG Branch Code :");
				$sheet->setCellValue('A2',"Period Covered :");
				$sheet->setCellValue('A3',"Employer Name :");
				$sheet->setCellValue('A4',"Employer Address :");
				$sheet->setCellValue('A5',"Employer ID :");
				$sheet->setCellValue('A6',"Postal Code No. :");
				$sheet->setCellValue('A7',"Telephone Number :");
				$sheet->setCellValue('A8',"Payment Type  : (MC=Monthly Contribution; ST=Short Term; CL=Calamity Loan)");
				$sheet->setCellValue('A9',"Employer Type :(G=Government; P=Private; O=Self Paying)");

				$sheet->setCellValue('B1',"82");
				$sheet->setCellValue('B2',strtoupper($year).$month_arr[ucwords($month)]);
				$sheet->setCellValue('B3',"Support Zebra");
				$sheet->setCellValue('B4',"3rd floor Former Graduate School Building Capitol University");
				$sheet->setCellValue('B5',"202124330004");
				$sheet->setCellValue('B6',"9000");
				$sheet->setCellValue('B7',"8551519");
				$sheet->setCellValue('B8',"MC");
				$sheet->setCellValue('B9',"P");
				$sheet->setCellValue('A11',"Employee Details");
				$sheet->mergeCells('A11:I11');
				$sheet->getStyle("A11:I11")->applyFromArray($style);
				for($i=1;$i<=9;$i++){
					$sheet->getStyle("B".$i)->applyFromArray($style_text_left);
					$sheet->getStyle("A".$i)->getAlignment()->setWrapText(true);;
				}
				$sheet->setCellValue('A12',"ID Number");
				$sheet->setCellValue('B12',"Employee's Company ID");
				$sheet->setCellValue('C12',"Last name");
				$sheet->setCellValue('D12',"First Name");
				$sheet->setCellValue('E12',"Middle Name");
				$sheet->setCellValue('F12',"Employee Contribution");
				$sheet->setCellValue('G12',"Employer Contribution");
				$sheet->setCellValue('H12',"TIN");
				$sheet->setCellValue('I12',"Birth Date");
				$sheet->getStyle("A12:I12")->applyFromArray($style);

				for($i=0;$i<count($data["add_pagibig"]);$i++){
						$arr[$i] =array(
						"emp_id" =>  $data["add_pagibig"][$i]->emp_id,
						"amount" =>  $data["add_pagibig"][$i]->amount,
						
						);
						
				 
				}
				foreach($data as $row1 => $val){
						$row =13;
						$col=3;
						$addd=0;
						$newVall=0;
					
						if($row1=="list"){
						foreach($val as $row2){
							$employer=($row2->rate*0.02);
							for($i=0;$i<count($arr);$i++){
								if($row2->emp_id===$arr[$i]["emp_id"]){
									$addd = $arr[$i]["amount"];
									$newVall= $employer + ($addd);
								}
							}
						
						
						
 						$sheet->setCellValue('A'.$row, $row2->pagibig);
						$sheet->setCellValue('B'.$row, "");
						$sheet->setCellValue('C'.$row, $row2->lname);
						$sheet->setCellValue('D'.$row, $row2->fname);
						$sheet->setCellValue('E'.$row, $row2->mname);
						$sheet->setCellValue('F'.$row, (($row2->salarymode=="Monthly") ? (($newVall!=0) ? $newVall : $employer ) : "150.00"));
						$sheet->setCellValue('G'.$row,(($row2->salarymode=="Monthly") ? $employer : "150.00"));
						$sheet->setCellValue('H'.$row, $row2->bir);
						$sheet->setCellValue('I'.$row, $row2->birthday);
						$sheet->getStyle("A".$row.":I".$row)->applyFromArray($style);
					$row++;
					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					$sheet->getColumnDimension('A')->setWidth('17');
					$sheet->getColumnDimension('B')->setWidth('25');
					$col++;
					$addd=0;
					$newVall=0;
						}
					}
				}
	  $file = 'PAG-IBIG_Contribution_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_u'.$this->session->userdata('uid').'.xlsx';
      $path= "reports/payroll/$year/$month/$period/report/".$file;
 
	  header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");;
      header("Content-Disposition: attachment; filename=$file");
      header("Content-Transfer-Encoding: binary ");
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007'); 
	  $objWriter->save($path);
      ob_clean();
      return $path; 

	}



			public function bank_atm_report(){
			 if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/bank_atm_report' and is_assign=1 ");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){

						$data['index'] = $this->PayrollModel->pagibig_report_index();
	 
					$this->load->view('bank_atm_report',$data);
				}else{
					redirect('index.php/home/error');
				}
			}else{
				redirect('index.php/login');
			}
			 }
			 public function bank_atm_show_details(){
				 $cid = $this->input->post("cid");
				 $atmType = $this->input->post("atmType");
				$data = $this->PayrollModel->atm_report($cid,$atmType);
					
					echo json_encode($data);

			 }
	public function exportATM_excel(){
		$objPHPExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$payrollC = $this->input->post('cid');
		$atmType = $this->input->post('atmType');
		$sheet = $objPHPExcel->getActiveSheet(0);
			$coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$payrollC);
			$year= $coverage->result()[0]->year;
			$month= $coverage->result()[0]->month; 
			$period= $coverage->result()[0]->period;
			$data["list"] = $this->PayrollModel->atm_report($payrollC,$atmType);
				$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
				$style_text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					)
				);


				$month_arr = array(
				"January" => 1,
				"February" => 2,
				"March" => 3,
				"April" => 4,
				"May" => 5,
				"June" => 6,
				"July" => 7,
				"August" => 8,
				"September" => 9,
				"October" => 10,
				"November" => 11,
				"December" => 12			
				);
			$sheet->setShowGridlines(false);
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payslip");
			
				foreach($data as $row1 => $val){
						$row =2;
						$roww =1;
						$col=1;
						$sheet->setCellValue('A1', "ATM Account Number");
						$sheet->setCellValue('B1', "LAST NAME");
						$sheet->setCellValue('C1', "FIRST NAME");
						$sheet->setCellValue('D1', "MIDDLE NAME");
						$sheet->setCellValue('E1', "TYPE");
						$sheet->setCellValue('F1', "AMOUNT");

						if($row1=="list"){
						foreach($val as $row2){
							$atm = (!empty($row2->atm_account_number) ? $row2->atm_account_number : "< null >") ;
							if($row2->isAtm == 1){
								$isATM =  "ATM";
							}elseif($row2->isAtm == 2){
								$isATM =  "HOLD";
							}else{
								$isATM =  "NON-ATM";
							}
							// $isATM = ($row2->isAtm == 1) ? "ATM" : "NON-ATM" ;
						
						
						
 						$sheet->setCellValue('A'.$row, $atm);
						$sheet->setCellValue('B'.$row, $row2->lname);
						$sheet->setCellValue('C'.$row, $row2->fname);
						$sheet->setCellValue('D'.$row, $row2->mname);
						$sheet->setCellValue('E'.$row, $isATM);
						$sheet->setCellValue('F'.$row, trim($row2->final));
						$sheet->getStyle("A".$roww.":F".$roww)->applyFromArray($style);
						$sheet->getStyle('F'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
						$sheet->getStyle('A'.$row)->getNumberFormat()->setFormatCode('0');
						
 					$row++;
					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					// $sheet->getColumnDimension('A')->setWidth('17');
					// $sheet->getColumnDimension('B')->setWidth('25');
					$col++;
						}
					}
				}
	  $file = 'PAYROLL_ATM_REPORT_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_u'.$this->session->userdata('uid').'.xlsx';
      $path= "reports/payroll/$year/$month/$period/report/".$file;
 
	  header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");;
      header("Content-Disposition: attachment; filename=$file");
      header("Content-Transfer-Encoding: binary ");
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007'); 
	  $objWriter->save($path);
      ob_clean();
      return $path; 

	}
function columnLetter($c){

$c = intval($c);
if ($c <= 0) return '';

$letter = '';

while($c != 0){
$p = ($c - 1) % 26;
$c = intval(($c - $p) / 26);
$letter = chr(65 + $p) . $letter;
}

return $letter;

}
public function reports(){
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/reports' and is_assign=1");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){

				$this->load->view('payroll_report_index',$arr);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}
	}
public function payroll_report_detail(){
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/payroll_report_detail' and is_assign=1");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
			$per_id =  $this->db->query("select GROUP_CONCAT(coverage_id,'|',daterange) as daterange,month from tbl_payroll_coverage where coverage_id>=89 group by month");
							$uid =  $this->db->query("select password from tbl_user where uid=".$this->session->userdata('uid'));
							foreach ($per_id->result() as $row){
									$arr['period'][$row->month] = array(
									"daterange" => $row->daterange,
									"month" => $row->month,
									);
							}

				$this->load->view('payroll_report_detail',$arr);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}
	}
	
	public function extractData(){		
			$coverage = $this->input->post("payrollC");
			$empzCat = $this->input->post("empzCat");
			$empzType = $this->input->post("empzType");
			$empzSite = $this->input->post("empzSite");
			// echo $empzCat;
			if($empzType=="Admin"){
				if(trim($empzCat)=="Probationary_and_Regular"){
					$type= "admin-both-nonconfi";

				}else if(trim($empzCat)=="Confidential"){
					$type= "admin-both-confi";

				}else{
					$type= "admin-trainee";
				}
			}else{
				$type= strtolower($empzType)."-".strtolower($empzCat).strtolower($empzSite);
			}
	
		$query = $this->db->query("SELECT * FROM `tbl_payroll` a,tbl_emp_promote b,tbl_pos_emp_stat c,tbl_employee d,tbl_applicant e ,tbl_position f where f.pos_id=c.pos_id and a.emp_promoteID=b.emp_promoteID and b.posempstat_id =c.posempstat_id and d.emp_id = b.emp_id and e.apid =d.apid and coverage_id= $coverage and emp_type='$type' order by lname"); 
		// echo "SELECT * FROM `tbl_payroll` a,tbl_emp_promote b,tbl_pos_emp_stat c,tbl_employee d,tbl_applicant e ,tbl_position f where f.pos_id=c.pos_id and a.emp_promoteID=b.emp_promoteID and b.posempstat_id =c.posempstat_id and d.emp_id = b.emp_id and e.apid =d.apid and coverage_id= $coverage and emp_type='$type' order by lname";
		$coveraged = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$coverage);
		$year= $coveraged->result()[0]->year;
		$month= $coveraged->result()[0]->month; 
		$period= $coveraged->result()[0]->period;
		$type2= "report";
			foreach($query->result() as $row){
				$arr["payroll"][$row->payroll_id] = array(
					"fname" => $row->fname,
					"lname" => $row->lname,
					"quinsina" => $row->quinsina,
					"pos_name" => $row->pos_name,
					"isAtm" => $row->isAtm,
					"clothing" => $row->clothing,
					"laundry" => $row->laundry,
					"rice" => $row->rice,
					"nd" => $row->nd,
					"ot_bct" => $row->ot_bct,
					"hp" => $row->hp,
					"ho" => $row->ho,
					"adjust_details" => $row->adjust_details, // Get Bonus
					"bonus_details" => $row->bonus_details, // Get Bonus
					"tax" => $row->bir_details, // Get Bonus
					"sss" => $row->sss_details, // Get Bonus
					"phic" => $row->phic_details, // Get Bonus
					"hdmf" => $row->hdmf_details, // Get Bonus
					"absentLate" => $row->absentLate, // Absent/Late/UT
					"deduct_details" => $row->deduct_details, // deduction

				);
			}
			echo json_encode($arr);

		}
	 public function exportpayrolldetail_excel(){
	 		$objPHPExcel = $this->phpexcel;
			$objDrawing = new PHPExcel_Worksheet_Drawing();

			$coverage = $this->input->post("payrollC");
			$empzCat = htmlentities($this->input->post("empzCat"));
			$empzType = $this->input->post("empzType");
			$empzSite = $this->input->post("empzSite");
		 
			if($empzType=="Admin"){
				if(trim($empzCat)=="Probationary_and_Regular"){
					$type= "admin-both-nonconfi";

				}else if(trim($empzCat)=="Confidential"){
					$type= "admin-both-confi";

				}else{
					$type= "admin-trainee";
				}
			}else{
				$type= strtolower($empzType)."-".strtolower($empzCat).strtolower($empzSite);
			}
	
			$query = $this->db->query("SELECT * FROM `tbl_payroll` a,tbl_emp_promote b,tbl_pos_emp_stat c,tbl_employee d,tbl_applicant e ,tbl_position f where f.pos_id=c.pos_id and a.emp_promoteID=b.emp_promoteID and b.posempstat_id =c.posempstat_id and d.emp_id = b.emp_id and e.apid =d.apid and coverage_id= $coverage and emp_type='$type' order by lname"); 
			  // echo "SELECT * FROM `tbl_payroll` a,tbl_emp_promote b,tbl_pos_emp_stat c,tbl_employee d,tbl_applicant e ,tbl_position f where f.pos_id=c.pos_id and a.emp_promoteID=b.emp_promoteID and b.posempstat_id =c.posempstat_id and d.emp_id = b.emp_id and e.apid =d.apid and coverage_id= $coverage and emp_type='$type' order by lname";
			$coveraged = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$coverage);
			$year= $coveraged->result()[0]->year;
			$month= $coveraged->result()[0]->month; 
			$period= $coveraged->result()[0]->period;
			$daterange= $coveraged->result()[0]->daterange;
			$sheet = $objPHPExcel->getActiveSheet(0);
			//------------------------INSERT LOGO-------------------------------------------
				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Logo');
				$objDrawing->setPath($this->logo);
			$objDrawing->setOffsetX(0);    // setOffsetX works properly
			$objDrawing->setOffsetY(0);  //setOffsetY has no effect
			$objDrawing->setCoordinates('A1');
			$objDrawing->setHeight(92); // logo height
			// $objDrawing->setWidth(320); // logo width
			// $objDrawing->setWidthAndHeight(200,400);
			$objDrawing->setResizeProportional(true);
			$objDrawing->setWorksheet($sheet);
			
			//----------------------------------------------------------------------

			$type2= "report";
				$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
				$style_text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
 
					$sheet->setShowGridlines(false);
					$objPHPExcel->createSheet();
					$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payroll");
				$sheet->setCellValue('G1', "Payroll Period:");
				$sheet->setCellValue('H1', $daterange);
				$sheet->setCellValue('G2', "Year:");
				$sheet->setCellValue('H2', $year);
				$sheet->setCellValue('G3', "Month:");
				$sheet->setCellValue('H3', $month);
				$sheet->setCellValue('G4', "Period:");
				$sheet->setCellValue('H4', $period);
				$sheet->setCellValue('A6', "LAST NAME");
				$sheet->setCellValue('B6', "FIRST NAME");
				$sheet->setCellValue('C6', "POSITION");
				$sheet->setCellValue('D6', "ATM");
				$sheet->setCellValue('E6', "BASIC SALARY");
				$sheet->setCellValue('F6', "CLOTHING");
				$sheet->setCellValue('G6', "LAUNDRY");
				$sheet->setCellValue('H6', "RICE");
				$sheet->setCellValue('I6', "ND");
				$sheet->setCellValue('J6', "OT-BCT");
				$sheet->setCellValue('K6', "HP");
				$sheet->setCellValue('L6', "HO");
				$sheet->setCellValue('M6', "ALLOWANCE DETAILS");
				$sheet->setCellValue('N6', "ALLOWANCE");
				$sheet->setCellValue('O6', "ADJUSTMENT DETAILS");
				$sheet->setCellValue('P6', "ADJUSTMENT");
				$sheet->setCellValue('Q6', "GROSS");
				$sheet->setCellValue('R6', "TAX");
				$sheet->setCellValue('S6', "SSS");
				$sheet->setCellValue('T6', "HDMF");
				$sheet->setCellValue('U6', "PHIC");
				$sheet->setCellValue('V6', "ABSENCE/UT/LATE");
				$sheet->setCellValue('W6', "DEDUCTION DETAIL");
				$sheet->setCellValue('X6', "TOTAL DEDUCTION");
				$sheet->setCellValue('Y6', "NET");
				$sheet->getStyle("A6:Y6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF808080');
				$sheet->getStyle("A6:Y6")->applyFromArray($style);

					for($h=1;$h<=4;$h++){
						$sheet->getStyle("A".$h.":Y".$h)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					}
							$row =7;
							$roww =7;
							$col=6;
							$bonus_detail="";
							$adjj_detail="";
							$ded_detail="";
 							$bonus_total=0;
 							$adjj_total=0;
 							$ded_total=0;
							$TOTAL_Basic = 0;
							$TOTAL_Clothing = 0;
							$TOTAL_Laundry = 0;
							$TOTAL_Rice = 0;
							$TOTAL_ND = 0;
							$TOTAL_OTBCT = 0;
							$TOTAL_HP = 0;
							$TOTAL_HO = 0;
							$TOTAL_Allowance = 0;
							$TOTAL_Adjustment = 0;
							$TOTAL_Gross = 0;
							$TOTAL_Tax = 0;
							$TOTAL_SSS = 0;
							$TOTAL_HDMF = 0;
							$TOTAL_PHIC = 0;
							$TOTAL_UT = 0;
							$TOTAL_Deduction = 0;
							$TOTAL_NET = 0;
							$TOTAL_ATM = 0;
							$TOTAL_NONATM = 0;
							$TOTAL_HOLDATM = 0;
							$newRow = 0;
					foreach($query->result() as $row1){
							
							$clothing = explode("(",$row1->clothing);
							$laundry = explode("(",$row1->laundry);
							$rice = explode("(",$row1->rice);
							$tax = explode("(",$row1->bir_details);
							$adjbonus = explode(",",$row1->adjust_details);
							$bonus = explode(",",$row1->bonus_details);
							$deductionz = explode(",", $row1->deduct_details);
							
								$isATM = ($row1->isAtm == 1) ? "ATM" : "NON-ATM" ;
	 
							$sheet->setCellValue('A'.$row, $row1->lname);
							$sheet->setCellValue('B'.$row, $row1->fname);
							$sheet->setCellValue('C'.$row, $row1->pos_name);
							$sheet->setCellValue('D'.$row, $isATM);
							$sheet->setCellValue('E'.$row, $row1->quinsina);
							$sheet->setCellValue('F'.$row, trim($clothing[0]));
							$sheet->setCellValue('G'.$row, trim($laundry[0]));
							$sheet->setCellValue('H'.$row, trim($rice[0]));
							$sheet->setCellValue('I'.$row, $row1->nd);
							$sheet->setCellValue('J'.$row, $row1->ot_bct);
							$sheet->setCellValue('K'.$row, $row1->hp);
							$sheet->setCellValue('L'.$row, $row1->ho);
								if(count($adjbonus)>=1){
										for($i=0;$i<count($adjbonus);$i++){
											$adjbon = explode("|",$adjbonus[$i]);
												if(trim($adjbon[0])=="Bonus"){
													$bon = explode("=",$adjbon[1]);
													$bonus_detail .= "[".$bon[0]." = ".$bon[1]."], ";
													$bonus_total+=$bon[1]; // Total Bonus under adjust_detail
												}
										}
									}else{
										$bonus_detail .="";	
									}
 									if($bonus!="0"){
										for($i=0;$i<count($bonus);$i++){
													$bon = explode("=",$bonus[$i]);
													$bonus_detail .= "[".$bon[0]." = ".$bon[1]."], ";
													$bonus_total+=$bon[1]; // Total Bonus under bonus_detail
										}
									}else{
										$bonus_detail ="";	
									}
							$sheet->setCellValue('M'.$row, $bonus_detail);
							$sheet->setCellValue('N'.$row, $bonus_total);
								// if($adjbonus!="0"){
								if(count($adjbonus)>=1){
									for($i=0;$i<count($adjbonus);$i++){
										$adjbon = explode("|",$adjbonus[$i]);
											if(trim($adjbon[0])!="Bonus"){
												$bon = explode("=",$adjbon[1]);
												$adjj_detail .= "[".$bon[0]." = ".$bon[1]."], ";
												$adjj_total+=$bon[1]; // Total Bonus under adjust_detail
											}else{
											$adjj_detail .= "";
											}
									}
								}
							
							$sheet->setCellValue('O'.$row, $adjj_detail);
							$sheet->setCellValue('P'.$row, $adjj_total);
							$gross = $row1->quinsina + $clothing[0] + $laundry[0] + $rice[0] + $row1->nd + $row1->ot_bct + $row1->hp+ $row1->ho+$bonus_total+$adjj_total;
							$sheet->setCellValue('Q'.$row, $gross);
							$sheet->setCellValue('R'.$row, $tax[0]);
							$sheet->setCellValue('S'.$row,  $row1->sss_details);
							$sheet->setCellValue('T'.$row,  $row1->hdmf_details);
							$sheet->setCellValue('U'.$row,  $row1->phic_details);
							$sheet->setCellValue('V'.$row,  trim($row1->absentLate));
							// if($deductionz!=0){
							if(count($deductionz)>=1){
								 for($i=0;$i<count($deductionz);$i++){
													$ded = explode("=",$deductionz[$i]);
													$ded_detail .= "[".$ded[0]." = ".$ded[1]."], ";
													$ded_total+=$ded[1]; // Total deduction under deduct_detail
										}
								}else{
										$ded_detail ="";	
							}
							
							$sheet->setCellValue('W'.$row, $ded_detail);
							$sheet->setCellValue('X'.$row, $ded_total);
							$totalDeduction = $tax[0] +  $row1->sss_details + $row1->hdmf_details + $row1->phic_details + $row1->absentLate + $ded_total;
							$takehome = $gross - $totalDeduction;
							$sheet->setCellValue('Y'.$row, $takehome);
							
								$sheet->getStyle("A".$roww.":L".$roww)->applyFromArray($style);
							  $sheet->getStyle("M".$roww)->applyFromArray($style_text_left);
							  $sheet->getStyle("N".$roww)->applyFromArray($style);
							  $sheet->getStyle("O".$roww)->applyFromArray($style_text_left);
								$sheet->getStyle("P".$roww.":V".$roww)->applyFromArray($style);
								$sheet->getStyle("W".$roww)->applyFromArray($style_text_left);
								$sheet->getStyle("X".$roww.":Y".$roww)->applyFromArray($style);

								$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setVisible(false);
								$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setVisible(false);
								$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setVisible(false);

						
						 if($this->columnLetter($col)!='M' && $this->columnLetter($col) !='O'){
							$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
						 }else{
						 $sheet->getColumnDimension('M')->setWidth(30);
						 $sheet->getColumnDimension('O')->setWidth(30);

						 }
						 $TOTAL_Basic += $row1->quinsina;
						 $TOTAL_Clothing += $clothing[0];
						 $TOTAL_Laundry += $laundry[0];
						 $TOTAL_Rice += $rice[0];
						 $TOTAL_ND += $row1->nd;
						 $TOTAL_OTBCT += $row1->ot_bct;
						 $TOTAL_HP += $row1->hp;
						 $TOTAL_HO += $row1->ho;
						 $TOTAL_Allowance += $bonus_total;
						 $TOTAL_Adjustment += $adjj_total;
						 $TOTAL_Gross += $gross;
						 $TOTAL_Tax += $tax[0];
						 $TOTAL_SSS += $row1->sss_details;
						 $TOTAL_HDMF += $row1->hdmf_details;
						 $TOTAL_PHIC += $row1->phic_details;
						 $TOTAL_UT += $row1->absentLate;
						 $TOTAL_Deduction += $ded_total;
						 $TOTAL_NET += $takehome;
						
						if($row1->isAtm == 1){
							$TOTAL_ATM+=$takehome;
						}elseif($row1->isAtm == 2){
							$TOTAL_HOLDATM+=$takehome;
						}else{
							$TOTAL_NONATM+=$takehome;
						}
						
						$row++;
						$roww++;
						$col++;
						$bonus_detail="";
						$adjj_detail="";
						$ded_detail="";
						$bonus_total=0;
						$ded_total=0;
						$adjj_total=0;
						$takehome=0;
						$gross=0;
						$totalDeduction=0;
					}
					 $sheet->setCellValue('E'.$row, $TOTAL_Basic);
					 $sheet->setCellValue('F'.$row, $TOTAL_Clothing);
					 $sheet->setCellValue('G'.$row, $TOTAL_Laundry);
					 $sheet->setCellValue('H'.$row, $TOTAL_Rice);
					 $sheet->setCellValue('I'.$row, $TOTAL_ND);
					 $sheet->setCellValue('J'.$row, $TOTAL_OTBCT);
					 $sheet->setCellValue('K'.$row, $TOTAL_HP);
					 $sheet->setCellValue('L'.$row, $TOTAL_HO);
					 $sheet->setCellValue('N'.$row, $TOTAL_Allowance);
					 $sheet->setCellValue('P'.$row, $TOTAL_Adjustment);
					 $sheet->setCellValue('Q'.$row, $TOTAL_Gross);
					 $sheet->setCellValue('R'.$row, $TOTAL_Tax);
					 $sheet->setCellValue('S'.$row, $TOTAL_SSS);
					 $sheet->setCellValue('T'.$row, $TOTAL_HDMF);
					 $sheet->setCellValue('U'.$row, $TOTAL_PHIC);
					 $sheet->setCellValue('V'.$row, $TOTAL_UT);
					 $sheet->setCellValue('X'.$row, $TOTAL_Deduction);
					 $sheet->setCellValue('Y'.$row, $TOTAL_NET);
	 				$sheet->getStyle("A".$row.":Y".$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFEB3B');
					$sheet->getStyle("A".$row.":Y".$row)->applyFromArray($style);

					$sheet->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('F'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('G'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('H'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('I'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('J'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('K'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('L'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('N'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('P'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('Q'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('R'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('S'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('T'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('U'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('V'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('X'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('Y'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;

 				  	$row+=6;
					
					$sheet->setCellValue('B'.$row, "Prepared By:");
					$sheet->setCellValue('I'.$row, "Checked By:");
					$sheet->setCellValue('R'.$row, "Approved By:");
					$sheet->getStyle('B'.$row.':R'.$row)->getFont()->setBold( true );
					$sheet->getStyle('B'.$row.':R'.$row)->getFont()->setUnderline( true );
					
					$sheet->setCellValue('B'.($row+5), "AGNES CLARA B. LAMCO");
					$sheet->getStyle('B'.($row+5))->getFont()->setUnderline( true );

					$sheet->setCellValue('B'.($row+8), "ANA BELLA CHRISTINA CHEE");
					$sheet->setCellValue('B'.($row+10), "HRT - Compensation and Benefits");
					$sheet->getStyle('B'.($row+8))->getFont()->setUnderline( true );

					$sheet->setCellValue('I'.($row+5), "DOROTHY JOY D. DAGCUTA");
					$sheet->setCellValue('I'.($row+7),  "General Accounting III");
					$sheet->getStyle('I'.($row+5))->getFont()->setUnderline( true );

					$sheet->setCellValue('I'.($row+10), "SHENDIE CHATTO-VALLENTE");
					$sheet->setCellValue('I'.($row+12), "Senior Finance Officer");
					$sheet->getStyle('I'.($row+10))->getFont()->setUnderline( true );

					$sheet->setCellValue('R'.($row+5), "DR. NIÑO MAE V. DURAN");
					$sheet->setCellValue('R'.($row+7), "Managing Director");  
					$sheet->getStyle('R'.($row+5))->getFont()->setUnderline( true );

					/****NEW SHEET***/
					$objPHPExcel->createSheet();
					$objPHPExcel->setActiveSheetIndex(1)->setTitle("SUMMARY");
					$sheet1 = $objPHPExcel->getActiveSheet();
					$sheet1->setShowGridlines(false);

					$objDrawing1 = new PHPExcel_Worksheet_Drawing();
					$objDrawing1->setName('Logo');
					$objDrawing1->setDescription('Logo');
					$objDrawing1->setPath($this->logo);
					$objDrawing1->setOffsetX(0);    // setOffsetX works properly
					$objDrawing1->setOffsetY(0);  //setOffsetY has no effect
					$objDrawing1->setCoordinates('A1');
					$objDrawing1->setHeight(92); // logo height
					// $objDrawing->setWidth(320); // logo width
					// $objDrawing->setWidthAndHeight(200,400);
					$objDrawing1->setResizeProportional(true);
					$objDrawing1->setWorksheet($sheet1);
					 
						$sheet1->setCellValue('G1', "Payroll Period:");
						$sheet1->setCellValue('H1', $daterange);
						$sheet1->setCellValue('G2', "Year:");
						$sheet1->setCellValue('H2', $year);
						$sheet1->setCellValue('G3', "Month:");
						$sheet1->setCellValue('H3', $month);
						$sheet1->setCellValue('G4', "Period:");
						$sheet1->setCellValue('H4', $period);
						
						
						$sheet1->setCellValue('A7', "SUMMARY:");
						$sheet1->setCellValue('A8', "ATM:");
						$sheet1->setCellValue('A9', "NON-ATM:");
						$sheet1->setCellValue('A10',  "HOLD:");
						
						$sheet1->setCellValue('B8',  $TOTAL_ATM);
						$sheet1->setCellValue('B9',  $TOTAL_NONATM);
						$sheet1->setCellValue('B10',  $TOTAL_HOLDATM);
						
						$sheet1->setCellValue('A11',  "TOTAL NET PAY:");
						$sheet1->setCellValue('B11',  $TOTAL_NET);
						foreach(range('A','Z') as $columnID)
						{
							$sheet1->getColumnDimension($columnID)->setAutoSize(true);
						}					
											
							for($h=1;$h<=4;$h++){
								$sheet1->getStyle("G".$h.":I".$h)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
							}
	 				$sheet1->getStyle("A7")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFEB3B');
					$sheet1->mergeCells('A7:B7');
					for($c=7;$c<=12;$c++){
						$sheet1->getStyle('A'.$c)->getFont()->setBold( true );
						$sheet1->getStyle('B'.$c)->getNumberFormat()->setFormatCode('#,##0.00');
					}
					/****END OF NEW SHEET***/
					
					
					
					
		$sheet->getProtection()->setSheet(true);
		$sheet->getProtection()->setPassword('pazzword');
		$sheet1->getProtection()->setSheet(true);
		$sheet1->getProtection()->setPassword('pazzword');
	   $file = 'PAYROLL_REPORT'.strtoupper(trim($empzSite)).'_'.strtoupper(trim($empzType)).'_'.strtoupper(trim($empzCat)).'_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_pr'.$this->session->userdata('uid').'.xlsx';
      $path= "reports/payroll/$year/$month/$period/report/".$file;
 
	  header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");;
      header("Content-Disposition: attachment; filename=$file");
      header("Content-Transfer-Encoding: binary ");
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007'); 
	  $objWriter->save($path);
      ob_clean();
      return $path; 
	 
	 }
	 public function deduct_details($dname,$account,$emp_type,$cid){
	 
		$query = $this->db->query("SELECT a.deduct_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   deduct_details like '%$dname%' and account like '%$account%' and emp_type like '%$emp_type%'");
		$total=0;
		  foreach ($query->result() as $row){
				$deduct = explode(",",$row->deduct_details);	
				  for($i=0;$i<count($deduct);$i++){
					$deduct1 = explode("=",$deduct[$i]);	
						  if(trim($deduct1[0])==$dname){
							$total += $deduct1[1];
						  }
				  }
		  }
			  return $total;
		}
		public function add_details($dname,$account,$emp_type,$cid){
		$query = $this->db->query("SELECT a.adjust_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   adjust_details like '%$dname%' and account like '%$account%' and emp_type like '%$emp_type%'");
		$total=0;
		  foreach ($query->result() as $row){
				$added = explode(",",$row->adjust_details);	
				  for($i=0;$i<count($added);$i++){
				  $add = explode("|",$added[$i]);
					$add1 = explode("=",$add[1]);	
						  if(trim($add1[0])==$dname){
							$total += $add1[1];
						  }
				  }
		  }
			  return $total."-SELECT a.adjust_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   adjust_details like '%$dname%' and account like '%$account%' and emp_type like '%$emp_type%'";
		}
		public function bonus_details($bname,$account,$emp_type,$cid){
	 
		$query = $this->db->query("SELECT a.bonus_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   bonus_details like '%$bname%' and account like '%$account%' and emp_type like '%$emp_type%'");
		$total=0;
		  foreach ($query->result() as $row){
				$deduct = explode(",",$row->bonus_details);	
				  for($i=0;$i<count($deduct);$i++){
					$deduct1 = explode("=",$deduct[$i]);	
						  if(trim($deduct1[0])==$bname){
							$total += $deduct1[1];
						  }
				  }
		  }
			  return $total;
		}
		public function payrolled($bname,$account,$emp_type,$cid){
	 
		$query = $this->db->query("SELECT a.".$bname."  FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid  and account like '%$account%' and emp_type like '%$emp_type%'");
		$total=0;
		  foreach ($query->result() as $row){
 						   if(trim($bname)=="bir_details"){
							$bir=explode("(",$row->$bname);
								$total += number_format($bir[0]);
							}else{
								$total += $row->$bname;

							}
						  
 		  }
			  return $total;
		}
	 function export_payroll_summary(){  
		$objPHPExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$payrollC = $this->input->post('payrollC');
		$empzType = $this->input->post('empzType');
 		$sheet = $objPHPExcel->getActiveSheet(0);
			$coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$payrollC);
			$year= $coverage->result()[0]->year;
			$month= $coverage->result()[0]->month; 
			$period= $coverage->result()[0]->period;
			$daterange= $coverage->result()[0]->daterange;

			$data["list_account"] = $this->PayrollModel->payroll_account($payrollC,$empzType);
			$data["list_account2"] = $this->PayrollModel->payroll_account2($payrollC,$empzType);
			$data["deduction_name"] = $this->PayrollModel->getAllDeductions();
			$data["adittion_name"] = $this->PayrollModel->additionName();
			$data["bonuz_name"] = $this->PayrollModel->bonuzName();
			$data["other_head"] = array("QUINSINA","CLOTHING","LAUNDRY","RICE","OT-BCT","NIGHT PREMIUM","HAZARD PAY","HOLIDAY","SSS","PHIC","HDMF","BIR");
			$other = array("quinsina","clothing","laundry","rice","ot_bct","nd","hp","ho","sss_details","phic_details","hdmf_details","bir_details");
				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Logo');
				$objDrawing->setPath($this->logo);
			$objDrawing->setOffsetX(0);    // setOffsetX works properly
			$objDrawing->setOffsetY(0);  //setOffsetY has no effect
			$objDrawing->setCoordinates('A1');
			$objDrawing->setHeight(92); // logo height
			// $objDrawing->setWidth(320); // logo width
			// $objDrawing->setWidthAndHeight(200,400);
			$objDrawing->setResizeProportional(true);
			$objDrawing->setWorksheet($sheet);

				$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
				$style_text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					)
				);

			$sheet->setShowGridlines(false);
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(0)->setTitle("Summary_".$year."_".$month."_".$period);
			$headercol=3;
			$row=7;
			$inc=6;
			$headerrow=6;
 				$sheet->setCellValue('C1', "Payroll Period:");
				$sheet->setCellValue('D1', $daterange);
				$sheet->setCellValue('C2', "Year:");
				$sheet->setCellValue('D2', $year);
				$sheet->setCellValue('C3', "Month:");
				$sheet->setCellValue('D3', $month);
				$sheet->setCellValue('C4', "Period:");
				$sheet->setCellValue('D4', $period);

				$sheet->setCellValue('A6',"DEPARTMENT");
				$sheet->setCellValue('B6', "ACCOUNTS");
				$sheet->setCellValue('C6', "HEADCOUNT");
				$sheet->getStyle("A6:C6")->applyFromArray($style);

				for($i=0;$i<count($data["other_head"]);$i++){
					$sheet->setCellValueByColumnAndRow($headercol, $headerrow,$data["other_head"][$i]);
					$headercol++;
					$sheet->getStyle($this->columnLetter($headercol).''.($inc).':'.$this->columnLetter($headercol).''.($inc))->applyFromArray($style);
                                                                                                      
				}
 				foreach($data["adittion_name"] as $row_adittion){
					$sheet->setCellValueByColumnAndRow($headercol, $headerrow,$row_adittion->additionname);
					$headercol++;
					$sheet->getStyle($this->columnLetter($headercol).''.($inc).':'.$this->columnLetter($headercol).''.($inc))->applyFromArray($style);

				}
				foreach($data["bonuz_name"] as $row_bonuz){
					$sheet->setCellValueByColumnAndRow($headercol, $headerrow,$row_bonuz->bonus_name);
					$headercol++;
					$sheet->getStyle($this->columnLetter($headercol).''.($inc).':'.$this->columnLetter($headercol).''.($inc))->applyFromArray($style);

				}
				foreach($data["deduction_name"] as $row_deduct){
					$sheet->setCellValueByColumnAndRow($headercol, $headerrow,$row_deduct->deductionname);
					$headercol++;
					$sheet->getStyle($this->columnLetter($headercol).''.($inc).':'.$this->columnLetter($headercol).''.($inc))->applyFromArray($style);
 				}
						
						$head1=3;
						$head2=15;
						$head3=34;
						$head3=34;
						$head4=42;
						$headrow1=7;
						$headrow2=7;
						$headrow3=7;
						$headrow4=7;
						$inc=7;
					foreach($data as $row1 => $val){
								if($row1=="list_account"){
									foreach($val as $row2){
										
										$account = explode("|",$row2->account);
										$depName = $this->db->query("SELECT dep_details FROM tbl_account a, tbl_department b where a.dep_id=b.dep_id and acc_id=".$account[0]);
										$sheet->setCellValue('A'.$row,$depName->result()[0]->dep_details);
										$sheet->setCellValue('B'.$row,$account[1]);
										$sheet->setCellValue('C'.$row,$row2->headcount);
										$sheet->getStyle("A".$row.":C".$row)->applyFromArray($style);

										$row++;

									}
									foreach($val as $row2){
											for($i=0;$i<count($other);$i++){
												$typ = explode("-",$row2->emp_type);
												$accz = explode("|",$row2->account);
											
												$dd = $this->payrolled($other[$i],trim($accz[1]),$typ[0],$row2->coverage_id);
												// $dd = $this->payroll($other[$i],$row2->account,$typ[0],$row2->coverage_id);
											
													$sheet->setCellValueByColumnAndRow($head1, $headrow1,number_format($dd,2,".",","));
												$head1++;
												$sheet->getStyle($this->columnLetter($head1).''.($inc).':'.$this->columnLetter($head1).''.($inc))->applyFromArray($style);
											}
										$head1=3;
										$headrow1++;
										$inc++;
									 }								 
								 }
								 $inc=7;
								 if($row1=="list_account"){
									 		foreach ($val as $row2){
												foreach($data["adittion_name"] as $row_adittion){
												$typ = explode("-",$row2->emp_type);
												$accz = explode("|",$row2->account);
												$add = $this->add_details($row_adittion->additionname,$accz[0]."|".trim($accz[1]),$typ[0],$row2->coverage_id);
												 $rs = explode("-",$add);
													$sheet->setCellValueByColumnAndRow($head2, $headrow2,number_format($rs[0],2,".",","));
												$head2++;
												$sheet->getStyle($this->columnLetter($head2).''.($inc).':'.$this->columnLetter($head2).''.($inc))->applyFromArray($style);

												}
											$head2=count($other)+3;
											$headrow2++;
											$inc++;
											}
											$inc=7;
											foreach ($val as $row2){
												foreach($data["bonuz_name"] as $row_bonuz){
												$typ = explode("-",$row2->emp_type);
												$accz = explode("|",$row2->account);
												$bon = $this->bonus_details($row_bonuz->bonus_name,$accz[0]."|".trim($accz[1]),$typ[0],$row2->coverage_id);
												 
													$sheet->setCellValueByColumnAndRow($head3, $headrow3,number_format($bon,2,".",","));
												$head3++;
												$sheet->getStyle($this->columnLetter($head3).''.($inc).':'.$this->columnLetter($head3).''.($inc))->applyFromArray($style);

												}
											$head3=count($data["adittion_name"])+$head2;
											$headrow3++;
											$inc++;

											}  
											$inc=7;
											foreach ($val as $row2){
												foreach($data["deduction_name"] as $row_deduct){
												$typ = explode("-",$row2->emp_type);
												$accz = explode("|",$row2->account);
												$deduc = $this->deduct_details($row_deduct->deductionname,$accz[0]."|".trim($accz[1]),$typ[0],$row2->coverage_id);
												 
													$sheet->setCellValueByColumnAndRow($head4, $headrow4,number_format($deduc,2,".",","));
												$head4++;
												$sheet->getStyle($this->columnLetter($head4).''.($inc).':'.$this->columnLetter($head4).''.($inc))->applyFromArray($style);

												}
											$head4=count($data["bonuz_name"])+$head3;
											$headrow4++;
											$inc++;

											}  
											$inc=7;
											
								 }
							
						 
					}
						
 						$ending = count($other)+count($data["adittion_name"])+count($data["bonuz_name"])+count($data["deduction_name"])+3;
						for($i=1;$i<=$ending;$i++){
						  $sheet->getStyle($this->columnLetter($i).'6')->getFont()->setBold( true );
						  $sheet->getStyle($this->columnLetter($i).'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFEB3B');
						  $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						  $sheet->getStyle("B".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						  $sheet->getColumnDimension($this->columnLetter($i))->setAutoSize(true);
						}
							for($h=1;$h<=4;$h++){
								$sheet->getStyle("D".$h.":F".$h)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
							    $sheet->getStyle('D'.$h)->getFont()->setBold( true );
	
							}
 				/*  foreach($data["list_account2"] as $row1 => $val){
							foreach ($val as $row2){
								foreach($data["adittion_name"] as $row_adittion){
								$typ = explode("-",$row2->emp_type);
								$add = $this->add_details($row_adittion->additionname,$row2->account,$typ[0],$row2->coverage_id);
								 
									$sheet->setCellValueByColumnAndRow($head2, $headrow2,number_format($add,2,".",","));
								$head2++;
								  
							}
							$head2=2;
							$headrow2++;
						
							}  
					
					} */
		$sheet->getProtection()->setSheet(true);
		$sheet->getProtection()->setPassword('pazzword');

 	 $file = 'PAYROLL_SUMMARY_REPORT_'.strtoupper($empzType).'_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_u'.$this->session->userdata('uid').'.xlsx';
      $path= "reports/payroll/$year/$month/$period/report/".$file;
 
	  header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");;
      header("Content-Disposition: attachment; filename=$file");
      header("Content-Transfer-Encoding: binary ");
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007'); 
	  $objWriter->save($path);
      ob_clean();
      return $path;     
		 
	}

}