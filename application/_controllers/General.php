<?php

defined('BASEPATH') or exit('No direct script access allowed');

class General extends CI_Controller {

    private $perPage = 10; //how many notifications queried per scroll.
    // public  $dir = "C:\Users\SDT-Programmer\Sites\sz"; //if windows Cariliman's local
    public $dir = "/usr/local/www/apache24/data/sz"; //linux

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('uid'))
        {
            redirect('login');
            date_default_timezone_set('Asia/Manila');
        }
    }

    private function error_403()
    {
        // $this->load->view('_partials/header', $data);
        $this->load->view('errors/custom/error_403');
    }

    public function error_404()
    {
        $this->output->set_status_header('404');
        $this->load->view('errors/custom/error_404');
    }

    protected function check_access()
    {
        $user_id = $this->session->userdata('uid');
        $current_uri = $this->uri->uri_string();
        $fields = "userAccess.useraccess_id";
        $where = "userAccess.menu_item_id = menuItems.menu_item_id AND menuItems.item_link LIKE '%" . $current_uri . "%' AND userAccess.user_id = $user_id AND is_assign =1";
        $tables = "tbl_user_access userAccess, tbl_menu_items menuItems";
        $record = $this->general_model->fetch_specific_val($fields, $where, $tables);
        if (count($record) > 0)
        {
            return true;
        }
        else
        {
            $this->error_403();
            return false;
        }
    }

    protected function load_template_view($path, $data)
    {
        $query = $this->db->query("select  distinct(a.tab_id),tab_name,icon from tbl_menu_tab_item a,tbl_menu_items b,tbl_user_access c,tbl_user d where a.tab_id =b.tab_id and b.menu_item_id=c.menu_item_id and d.uid=c.user_id and d.uid='" . $this->session->userdata('uid') . "'  and c.is_assign=1 and b.isNewTemplate=1 and b.isShow=1");
        $rez = $query->result();
        $data["res"] = array();
        foreach ($rez as $key => $v)
        {
            $data["res"][$v->tab_name . "|" . $v->icon] = $this->sideBarMenu($v->tab_id);
        }
        $data['uri_segment'] = $this->uri->segment_array();

        $data['session'] = $this->session->userdata();
        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar', $data);
        $this->load->view('_partials/header_topbar', $data);
        $this->load->view($path);
        $this->load->view('_partials/footer', $data);
    }

    private function sideBarMenu($rez)
    {

        $query = $this->db->query("select item_link,item_title from tbl_menu_tab_item a,tbl_menu_items b,tbl_user_access c,tbl_user d where a.tab_id =b.tab_id and b.menu_item_id=c.menu_item_id and d.uid=c.user_id and b.tab_id =" . $rez . "  and uid='" . $this->session->userdata('uid') . "'  and c.is_assign=1 and b.is_active=1 and b.isShow=1 and b.isNewTemplate=1");

        return $query->result();
    }

    private function time_elapsed_string($datetime, $full = false)
    {
        date_default_timezone_set('Asia/Manila');

        $now = new DateTime();
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v)
        {
            if ($diff->$k)
            {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            }
            else
            {
                unset($string[$k]);
            }
        }

        if (!$full)
        {
            $string = array_slice($string, 0, 1);
        }
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function get_latest_notifications()
    {
        $limiter = $this->input->post('limiter'); //start of query limit
        $uid = $this->session->userdata('uid');
        // $uid = 5;
        $notifications = $this->general_model->custom_query("SELECT a.notification_ID,a.sender_ID,a.message,a.link,a.createdOn,b.notificationRecipient_ID,b.recipient_ID,c.status_ID,c.description as status FROM tbl_notification a,tbl_notification_recipient b,tbl_status c WHERE a.notification_ID=b.notification_ID AND b.recipient_ID=$uid AND b.status_ID=c.status_ID ORDER BY a.createdOn DESC LIMIT $limiter,$this->perPage");

        if (empty($notifications))
        {
            $status = "Empty";
        }
        else
        {
            foreach ($notifications as $notif)
            {
                $notif->timeElapsed = $this->time_elapsed_string($notif->createdOn);
                $user = $this->general_model->fetch_specific_val("c.fname,c.lname,c.pic,a.uid", "a.emp_id=b.emp_id AND b.apid=c.apid AND a.uid=$notif->sender_ID", "tbl_user a,tbl_employee b, tbl_applicant c");
                $notif->sender = $user->lname . ", " . $user->fname;
                $notif->pic = $user->pic;
                $notif->uid = $user->uid;
            }
            $status = "Success";
        }
        echo json_encode(array('status' => $status, 'notifications' => $notifications));
    }

    public function get_latest_system_notifications()
    {
        $limiter = $this->input->post('limiter'); //start of query limit
        $uid = $this->session->userdata('uid');
        // $uid = 5;
        $notifications = $this->general_model->custom_query("SELECT a.systemNotification_ID,a.message,a.link,a.createdOn,b.systemNotificationRecipient_ID,b.recipient_ID,c.status_ID,c.description as status FROM tbl_system_notification a,tbl_system_notification_recipient b,tbl_status c WHERE a.systemNotification_ID=b.systemNotification_ID AND b.recipient_ID=$uid AND b.status_ID=c.status_ID ORDER BY a.createdOn DESC LIMIT $limiter,$this->perPage");

        if (empty($notifications))
        {
            $status = "Empty";
        }
        else
        {
            foreach ($notifications as $notif)
            {
                $notif->timeElapsed = $this->time_elapsed_string($notif->createdOn);
            }
            $status = "Success";
        }
        echo json_encode(array('status' => $status, 'notifications' => $notifications));
    }

    public function get_notification_details()
    {
        $notification_ID = $this->input->post('notification_ID');
        $notifications = $this->general_model->fetch_specific_val("sender_ID,message,link,createdOn", "notification_ID=$notification_ID", "tbl_notification");
        if (empty($notifications) or $notifications === '')
        {
            $status = "Empty";
        }
        else
        {
            $status = "Success";
            $recipients = $this->general_model->fetch_specific_vals("a.notificationRecipient_ID,a.recipient_ID,a.status_ID,a.createdOn,a.updatedOn,b.description as status", "a.notification_ID=$notification_ID AND a.status_ID=b.status_ID", "tbl_notification_recipient a,tbl_status b");
            $user = $this->general_model->fetch_specific_val("c.fname,c.lname,c.pic", "a.emp_id=b.emp_id AND b.apid=c.apid AND a.uid=$notifications->sender_ID", "tbl_user a,tbl_employee b, tbl_applicant c");
            $notifications->sender = $user->lname . ", " . $user->fname;
            $notifications->pic = $user->pic;
            foreach ($recipients as $reci)
            {
                $reci->timeElapsed = $this->time_elapsed_string($reci->createdOn);
                $reci->unreadCount = $this->general_model->fetch_specific_val("COUNT(*) as unreadCount", "a.notification_ID=b.notification_ID AND b.recipient_ID=$reci->recipient_ID AND b.status_ID=c.status_ID AND c.description='unread'", "tbl_notification a,tbl_notification_recipient b,tbl_status c ")->unreadCount;
            }
            $notifications->recipients = $recipients;
        }

        echo json_encode(array('status' => $status, 'notifications' => $notifications));
    }

    public function get_system_notification_details()
    {

        $systemNotification_ID = $this->input->post('systemNotification_ID');
        $notifications = $this->general_model->fetch_specific_val("message,link,createdOn", "systemNotification_ID=$systemNotification_ID", "tbl_system_notification");

        if (empty($notifications) or $notifications === '')
        {
            $status = "Empty";
        }
        else
        {
            $status = "Success";
            $recipients = $this->general_model->fetch_specific_vals("a.systemNotificationRecipient_ID,a.recipient_ID,a.status_ID,a.createdOn,a.updatedOn,b.description as status", "a.systemNotification_ID=$systemNotification_ID AND a.status_ID=b.status_ID", "tbl_system_notification_recipient a,tbl_status b");
            foreach ($recipients as $reci)
            {
                $reci->timeElapsed = $this->time_elapsed_string($reci->createdOn);
                $reci->unreadCount = $this->general_model->fetch_specific_val("COUNT(*) as unreadCount", "a.systemNotification_ID=b.systemNotification_ID AND b.recipient_ID=$reci->recipient_ID AND b.status_ID=c.status_ID AND c.description='unread'", "tbl_system_notification a,tbl_system_notification_recipient b,tbl_status c ")->unreadCount;
            }
            $notifications->recipients = $recipients;
        }

        echo json_encode(array('status' => $status, 'notifications' => $notifications));
    }

    public function setSystemNotif($message, $link, $sup)
    {
        // $message= $_SESSION["fname"]." ".$_SESSION["lname"]." was late on ".date("Y-m-d h");
        // $link="test/test";
        // $sup=114;
        $systemData["message"] = $message;
        $systemData["link"] = $link;
        $systemNotif = $this->general_model->insert_vals_last_inserted_id($systemData, 'tbl_system_notification');
        $systemDataRecipient["systemNotification_ID"] = $systemNotif;
        $systemDataRecipient["recipient_ID"] = $sup;
        $systemDataRecipient["status_ID"] = 8;
        $systemNotifRec = $this->general_model->insert_vals_last_inserted_id($systemDataRecipient, 'tbl_system_notification_recipient');
        return $systemNotif;
    }

    public function set_datatable_query($datatable, $query)
    {
        $sql = $query['query'];

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';

        if (isset($query['search']['append']))
        {
            $sql .= $query['search']['append'];

            $search = $query['query'] . $query['search']['total'];

            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);

            $page = ($page > $pages) ? 1 : $page;
        }
        else
        {
            $total = count($this->general_model->custom_query($sql));
        }

        if (isset($datatable['pagination']))
        {

            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';

            if ($perpage < 0)
            {
                $limit = ' LIMIT 0';
            }

            $sql .= $order . ' ' . $sort . $limit;
        }

        $data = $this->general_model->custom_query($sql);
//echo $this->db->last_query();
        //            var_dump($data);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];

        $result = [
            'meta' => $meta,
            'data' => $data,
        ];

        return $result;
    }

    // MIC
    // NOTIFICATION
    protected function ordinal($number)
    {
        $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
        if ((($number % 100) >= 11) && (($number % 100) <= 13))
        {
            return $number . 'th';
        }
        else
        {
            return $number . $ends[$number % 10];
        }
    }

    // REQUEST NOTIFICATION
    private function create_notif($sender_id, $notif_mssg, $link)
    {
        $data['sender_ID'] = $sender_id;
        $data['message'] = $notif_mssg;
        $data['link'] = $link;
        return $notification_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_notification');
    }

    private function send_notif($notif_Id, $recipients)
    {
        $notif_recip = [];
        for ($loop = 0; $loop < count($recipients); $loop++)
        {
            $notif_recip[$loop] = [
                "notification_ID" => $notif_Id,
                "recipient_ID" => $recipients[$loop]['userId'],
                "status_ID" => 8,
            ];
        }
        return $insert_status = $this->general_model->batch_insert($notif_recip, 'tbl_notification_recipient');
    }

    protected function set_notif($sender_id, $notif_mssg, $link, $recipientArray)
    {
        $notif_id = [];
        $approval_type = 1; // 1- Sequential, 2 - Parallel
        if ($approval_type === 1)
        {
            $recipients[] = $recipientArray[0];
        }
        else
        {
            $recipients = $recipientArray;
        }
        $notif_id[0] = $this->create_notif($sender_id, $notif_mssg, $link);
        $data['notif_id'] = $notif_id;
        $data['send_stat'] = $this->send_notif($notif_id[0], $recipients);
        return $data;
    }

    protected function set_notif_preceeding($sender_id, $notif_mssg, $link, $recipientArray)
    {
        $notif_id[0] = $this->create_notif($sender_id, $notif_mssg, $link);
        $data['notif_id'] = $notif_id;
        $data['send_stat'] = $this->send_notif($notif_id[0], $recipientArray);
        return $data;
    }

    //SYSTEM NOTIFICATION
    private function send_system_notif($notif_Id, $recipients)
    {
        $notif_recip = [];
        for ($loop = 0; $loop < count($recipients); $loop++)
        {
            $notif_recip[$loop] = [
                "systemNotification_ID" => $notif_Id,
                "recipient_ID" => $recipients[$loop]['userId'],
                "status_ID" => 8,
            ];
        }
        return $insert_status = $this->general_model->batch_insert($notif_recip, 'tbl_system_notification_recipient');
    }

    private function create_system_notif($notif_mssg, $link)
    {
        $data['message'] = $notif_mssg;
        $data['link'] = $link;
        return $notification_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_system_notification');
    }

    protected function set_system_notif($notif_mssg, $link, $recipientArray)
    {
        $notif_id = [];
        $approval_type = 1; // 1- Sequential, 2 - Parallel
        if ($approval_type === 1)
        {
            $recipients[] = $recipientArray[0];
        }
        else
        {
            $recipients = $recipientArray;
        }
        $notif_id[0] = $this->create_system_notif($notif_mssg, $link);
        $data['notif_id'] = $notif_id;
        $data['send_stat'] = $this->send_system_notif($notif_id[0], $recipients);
        return $data;
    }

    protected function set_system_notif_preceeding($notif_mssg, $link, $recipientArray)
    {
        $notif_id[0] = $this->create_system_notif($notif_mssg, $link);
        $data['notif_id'] = $notif_id;
        $data['send_stat'] = $this->send_system_notif($notif_id[0], $recipientArray);
        return $data;
    }

    private function get_approval_number($module_id)
    {
        $fields = "module, numberOfApproval";
        $where = "module_ID = $module_id";
        $approval_num = $this->general_model->fetch_specific_val($fields, $where, "tbl_module");
        return $approval_num->numberOfApproval;
    }

    private function get_module_links($module_id)
    {
        $fields = "approvalPageLink, pendingRequestPage, monitoringPageLink, abbreviation";
        $where = "module_ID = $module_id";
        return $links = $this->general_model->fetch_specific_val($fields, $where, "tbl_module");
    }

    protected function get_direct_supervisor($user_id)
    {
        $fields = "emp.Supervisor";
        $where = "emp.emp_id = users.emp_id AND users.uid = $user_id";
        $tables = "tbl_employee emp, tbl_user users";
        $sup_emp_id = $this->general_model->fetch_specific_val($fields, $where, $tables);
        if ($sup_emp_id->Supervisor === 0)
        {
            return 0;
        }
        else
        {
            $fields = "uid";
            $where = "emp_id = $sup_emp_id->Supervisor";
            $tables = "tbl_user";
            return $sup_user_details = $this->general_model->fetch_specific_val($fields, $where, $tables);
        }
    }

    protected function get_direct_supervisor_emp($user_id)
    {
        $fields = "emp.Supervisor";
        $where = "emp.emp_id = users.emp_id AND users.uid = $user_id";
        $tables = "tbl_employee emp, tbl_user users";
        $sup_emp_id = $this->general_model->fetch_specific_val($fields, $where, $tables);
        if ($sup_emp_id->Supervisor === 0)
        {
            return 0;
        }
        else
        {
           return $sup_emp_id;
        }
    }

    private function set_deadline($approvers, $module_id, $request_id, $column, $requestor)
    {
        for ($loop = 0; $loop < count($approvers); $loop++)
        {
            unset($approvers[$loop]['approvalStatus_ID']);
            unset($approvers[$loop][$column]);
            $approvers[$loop]["module_ID"] = $module_id;
            $approvers[$loop]["request_ID"] = $request_id;
            $approvers[$loop]["status_ID"] = 2;
            $approvers[$loop]["requestor"] = $requestor;
        }
        // var_dump($approvers);
        return $insert_stat = $this->general_model->batch_insert($approvers, 'tbl_deadline');
    }

    // SET APPROVERS

    public function set_approvers_test()
    {
        $date_filed = "2018-10-04";
        $module_id =1;
        $request_id = 1;
        $column = "additionalHourRequest_ID";
        $approvers = [];
        $approvers_deadline_follow = [];
        $orig_user_id = 196;
        $user_id = 196;
        $approval_num = $this->get_approval_number(1);
        $approval_type = 1; // 1- Sequential, 2 - Parallel
        $approval_level = 1;
        $approval_stat_init = 4;
        // echo "approval_num ->".$approval_num."<br>";
        for ($loop = 0; $loop < $approval_num; $loop++)
        {
            echo "loop ->".$loop."<br>";
            $sup_user_details = $this->get_direct_supervisor($user_id);
            if (count($sup_user_details) > 0)
            {
                echo $sup_user_details->uid ." - level = ".$approval_level."<br>";
                $deadline_date = Date("Y-m-d", strtotime($date_filed . ' 2 days'));
                $deadline_date_time = $deadline_date . " 23:59:59";
                if($sup_user_details->uid == '766' && $approval_level > 1)
                {
                //    echo "cut on sir mae";
                //     $approver_id = 316;
                }else{
                    if($sup_user_details->uid == '766' && $approval_level == 1){
                        $approver_id = 316;
                    }
                    else{
                        $approver_id = (int) $sup_user_details->uid;
                        "save to approver ----------- <br>";
                        
                    }
                    if ($loop == 0){
                        $follow = 0;
                    }else{
                        $follow = $deadline_date . " 00:00:00";
                    }
                    $approval_stat = $approval_stat_init;
                    $approvers_deadline_follow[$loop] = [
                        'userId' => $approver_id,
                        // $column => $request_id,
                        'approvalStatus_ID' => $approval_stat,
                        'approvalLevel' => $approval_level,
                        'deadline' => $deadline_date_time,
                        'follow' => $follow,
                    ];
                    $approvers[$loop] = [
                        'userId' => $approver_id,
                        // $column => $request_id,
                        'approvalStatus_ID' => $approval_stat,
                        'approvalLevel' => $approval_level,
                            // 'employeeName' => $sup_user_details->fname." ".$sup_user_details->fname,
                    ];
                    $user_id = $sup_user_details->uid;
                    if ($approval_type === 1)
                    {
                        $approval_stat_init = 2;
                    }
                    else
                    {
                        $approval_stat_init = 4;
                    }
                    $date_filed = $deadline_date;
                    $approval_level++;
                }
               
            }
        }
        $this->set_deadline($approvers_deadline_follow, $module_id, $request_id, $column, $orig_user_id);
        var_dump($approvers);
    }


    protected function set_approvers($date_filed, $user_id, $module_id, $column, $request_id)
    {
        $approvers = [];
        $approvers_deadline_follow = [];
        $orig_user_id = $user_id;
        $approval_num = $this->get_approval_number($module_id);
        $approval_type = 1; // 1- Sequential, 2 - Parallel
        $approval_level = 1;
        $approval_stat_init = 4;
        for ($loop = 0; $loop < $approval_num; $loop++)
        {
            $sup_user_details = $this->get_direct_supervisor($user_id);
            if (count($sup_user_details) > 0)
            {
                $deadline_date = Date("Y-m-d", strtotime($date_filed . ' 2 days'));
                $deadline_date_time = $deadline_date . " 23:59:59";
                if($sup_user_details->uid == '766' && $approval_level > 1)
                {
                //    echo "cut on sir mae";
                }else{
                    if($sup_user_details->uid == '766' && $approval_level == 1){
                        $approver_id = 316;
                    }else{
                        $approver_id = (int) $sup_user_details->uid;
                    }
                    if ($loop == 0)
                    {
                        $follow = 0;
                    }
                    else
                    {
                        $follow = $deadline_date . " 00:00:00";
                    }
                    $approval_stat = $approval_stat_init;
                    $approvers_deadline_follow[$loop] = [
                        'userId' => $approver_id,
                        $column => $request_id,
                        'approvalStatus_ID' => $approval_stat,
                        'approvalLevel' => $approval_level,
                        'deadline' => $deadline_date_time,
                        'follow' => $follow,
                    ];
                    $approvers[$loop] = [
                        'userId' => $approver_id,
                        $column => $request_id,
                        'approvalStatus_ID' => $approval_stat,
                        'approvalLevel' => $approval_level,
                            // 'employeeName' => $sup_user_details->fname." ".$sup_user_details->fname,
                    ];
                    $user_id = $sup_user_details->uid;
                    if ($approval_type === 1)
                    {
                        $approval_stat_init = 2;
                    }
                    else
                    {
                        $approval_stat_init = 4;
                    }
                    $date_filed = $deadline_date;
                    $approval_level++;
                }                
            }
        }
        $this->set_deadline($approvers_deadline_follow, $module_id, $request_id, $column, $orig_user_id);
        return $approvers;
    }

    protected function get_current_date_time()
    {
        $date = new DateTime("now", new DateTimeZone('Asia/Manila'));
        $dates = [
            'dateTime' => $date->format('Y-m-d H:i:s'),
            'date' => $date->format('Y-m-d'),
            'time' => $date->format('H:i:s'),
        ];
        return $dates;
    }

    // GET DEADLINE DATA FOR APPROVERS

    public function get_pending_deadline()
    {
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        $fields = "module_ID, request_ID, requestor, approvalLevel, deadline";
        $where = "userId = $user_id AND DATE(deadline) ='" . $dateTime['date'] . "' AND status_ID = 2";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_deadline");
        echo json_encode($record);
    }

    protected function get_preceding_approvers_due($approvalLevel, $request_id, $module_id)
    {
        $fields = "userId, approvalLevel";
        $where = "request_ID = $request_id AND module_ID = $module_id AND approvalLevel < $approvalLevel";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_deadline");
    }

    protected function get_user_details_due($user_id)
    {
        $fields = "a.uid, c.fname, c.lname";
        $where = "a.uid = $user_id AND a.emp_id = b.emp_id AND b.apid=c.apid";
        return $requestor = $this->general_model->fetch_specific_val($fields, $where, "tbl_user a,tbl_employee b,tbl_applicant c");
    }

    private function notify_preceding_approvers($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link)
    {
        $links = $this->get_module_links($module_id);
        $preceeding_approvers = $this->get_preceding_approvers_due($approval_level, $request_id, $module_id);
        for ($loop = 0; $loop < count($preceeding_approvers); $loop++)
        {
            $recipients[$loop] = [
                "userId" => $preceeding_approvers[$loop]->userId,
            ];
        }
        $notif_mssg = "<i class='fa fa-info-circle'></i> " . $approver->fname . " $approver->lname was not able to give approval decision to the $module of " . $requestor->fname . " " . $requestor->lname . " .<br><small><b>" . $links->abbreviation . "-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
        $link = $links->approvalPageLink;
        return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $recipients);
    }

    private function notify_requestor($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link)
    {
        $links = $this->get_module_links($module_id);
        $requestors[0] = ['userId' => $requestor->uid];
        $approval_level_ordinal = $this->ordinal($approval_level);
        $notif_mssg = "<i class='fa fa-info-circle'></i> " . $approver->fname . " $approver->lname, your $approval_level_ordinal level approver, was not able to give approval decision to your $module.<br><small><b>" . $links->abbreviation . "-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
        $link = $links->pendingRequestPage;
        return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $requestors);
    }

    private function notify_dued_approver($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link, $deadline)
    {
        $links = $this->get_module_links($module_id);
        $approvers[0] = ['userId' => $approver->uid];
        $approval_level_ordinal = $this->ordinal($approval_level);
        $notif_mssg = "<i class='fa fa-warning'></i> You have missed to give an approval decision as the $approval_level_ordinal level approver to the $module of $requestor->fname " . $requestor->lname . " prior " . date("g:i A", strtotime($deadline)) . " last " . date("M j, Y", strtotime($deadline)) . ".<br><small><b>" . $links->abbreviation . "-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
        $link = $links->approvalPageLink;
        return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $approvers);
    }

    private function get_max_approval_level_deadline($request_id, $module_id)
    {
        $fields = "MAX(approvalLevel) as approvalLevel";
        $where = "request_ID = $request_id AND module_ID = $module_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_deadline');
    }

    // public function set_columns(){
    //     $column = [
    //         'request_id_col' => 'additionalHourRequest_ID',
    //         'approvalLevel' => 'approvalLevel',
    //         'status_col' => 'approvalStatus_ID',
    //         'table' => 'tbl_additional_hour_request_approval',
    //     ];
    //     $col = json_encode($column);
    //     $data['approvalAttrib'] = $col;
    //     $where = "module_ID = 1";
    //     $update_stat = $this->general_model->update_vals($data, $where, 'tbl_module');
    //     echo json_encode($update_stat);
    // }

    private function set_specific_approver_stat($request_id, $request_col, $approval_level, $approvalLevel_col, $status_id, $status_col, $table)
    {
        $data["$status_col"] = $status_id;
        $where = "$request_col = $request_id AND $approvalLevel_col = $approval_level";
        $update_stat = $this->general_model->update_vals($data, $where, $table);
        return ($update_stat);
    }

    private function get_approval_attrib($module_id)
    {
        $fields = "approvalAttrib";
        $where = "module_ID = $module_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_module');
    }

    private function get_follow_user_id($module_id, $approval_level, $request_id)
    {
        $fields = "userId";
        $where = "request_ID = $request_id AND module_ID = $module_id AND approvalLevel = $approval_level";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_deadline');
    }

    private function notify_approver_follow($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link, $deadline)
    {
        $follow_approver = $this->get_follow_user_id($module_id, $approval_level + 1, $request_id);
        $approvers[0] = ['userId' => $follow_approver->userId];
        $next_approval_level_ordinal = $this->ordinal($approval_level + 1);
        $missed_approval_level_ordinal = $this->ordinal($approval_level);
        $notif_mssg = "<i class='fa fa-info-circle'></i> The $module of $requestor->fname " . $requestor->lname . " was automatically forwarded to you for $next_approval_level_ordinal level approval because $approver->fname " . $approver->lname . " ($missed_approval_level_ordinal level) was not able to give a decision before the deadline: " . date("g:i A", strtotime($deadline)) . ", " . date("M j, Y", strtotime($deadline)) . ".<br><small>" . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
        return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $approvers);
    }

    private function get_default_approver($module_id)
    {
        $fields = "supervisingUserId";
        $where = "module_ID = $module_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_module');
    }

    private function notify_default_approver($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link, $deadline)
    {
        $links = $this->get_module_links($module_id);
        $default_approver = $this->get_default_approver($module_id);
        $approvers[0] = ['userId' => $default_approver->supervisingUserId];
        $missed_approval_level_ordinal = $this->ordinal($approval_level);
        $notif_mssg = "<i class='fa fa-info-circle'></i> The $module of $requestor->fname " . $requestor->lname . " was automatically forwarded to you for default approval because $approver->fname " . $approver->lname . " ($missed_approval_level_ordinal level) was not able to give a decision before the deadline: " . date("g:i A", strtotime($deadline)) . ", " . date("M j, Y", strtotime($deadline)) . ".<br><small><b>" . $links->abbreviation . "-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
        $link = $links->monitoringPageLink;
        return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $approvers);
    }

    public function set_current_approver_as_missed($module_id, $request_id, $approval_level, $approver_id)
    {
        $approval_attrib = json_decode($this->get_approval_attrib($module_id)->approvalAttrib);
        $request_col = $approval_attrib->request_id_col;
        $request_details_col = $approval_attrib->requestCol;
        $approvalLevel_col = $approval_attrib->approvalLevel;
        $status_col = $approval_attrib->status_col;
        $approval_table = $approval_attrib->approvalTable;
        $request_tbl = $approval_attrib->requestTable;
        //set missed
        $status_id = 12;
        //approval table
        $curr_approver_stat['set_stat'] = $this->set_specific_approver_stat($request_id, $request_col, $approval_level, $approvalLevel_col, $status_id, $status_col, $approval_table);
        //details
        if (((int) $module_id !== 1) AND ( (int) $module_id !== 3))
        {
            $leave_approval_id = $this->get_request_leave_approval($request_details_col, $request_col, $request_id, $approver_id, $approval_table);
            $data['status'] = 12;
            $where = "$request_details_col = ".$leave_approval_id->$request_details_col;
            $curr_approver_stat['set_details_stat'] = $this->general_model->update_vals($data, $where, $request_tbl);
        }
        return $curr_approver_stat;
    }

    private function set_next_approver_as_current($module_id, $request_id, $approval_level)
    {
        $approval_attrib = json_decode($this->get_approval_attrib($module_id)->approvalAttrib);
        $request_col = $approval_attrib->request_id_col;
        $approvalLevel_col = $approval_attrib->approvalLevel;
        $status_col = $approval_attrib->status_col;
        $approval_table = $approval_attrib->approvalTable;
        $approval_level++;
        $status_id = 4;
        return $set_stat = $this->set_specific_approver_stat($request_id, $request_col, $approval_level, $approvalLevel_col, $status_id, $status_col, $approval_table);
    }

    private function set_deadline_stat($deadline_id)
    {
        $data['status_ID'] = 12;
        $where = "deadline_ID = $deadline_id";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_deadline');
    }

    private function set_main_request($request_id, $request_id_col, $request_tbl, $status_col)
    {
        $data[$status_col] = 12;
        $where = "$request_id_col = $request_id";
        return $update_stat = $this->general_model->update_vals($data, $where, $request_tbl);
    }

    private function get_request_leave_approval($request_details_col, $request_id_col, $request_id, $approver_id, $approval_tbl)
    {
        $fields = "$request_details_col";
        $where = "$request_id_col = $request_id AND uid = $approver_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, $approval_tbl);
    }

    private function set_leave_status_missed($request_id_col, $request_id, $final_status_col, $main_table)
    {
        $data[$final_status_col] = 12;
        $where = "$request_id_col = $request_id";
        return $update_stat = $this->general_model->update_vals($data, $where, $main_table);
    }

    public function set_request_as_missed($module_id, $request_id)
    {
        $approval_attrib = json_decode($this->get_approval_attrib($module_id)->approvalAttrib);
        if (((int) $module_id !== 1) AND ( (int) $module_id !== 3))
        {
            $update_stat = $this->set_leave_status_missed($approval_attrib->request_id_col, $request_id, $approval_attrib->final_status_col, $approval_attrib->final_table);
        }
        else
        {
            if ((int) $module_id == 3)
            {
                $status_col = $approval_attrib->request_stat_col;
            }
            else
            {
                $status_col = $approval_attrib->status_col;
            }
            $update_stat = $this->set_main_request($request_id, $approval_attrib->requestCol, $approval_attrib->requestTable, $status_col);
        }
        return $update_stat;
    }

    private function get_due_deadlines()
    {
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        $fields = "dead.deadline_ID, dead.module_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage, dead.request_ID, dead.requestor, dead.userId, dead.approvalLevel, dead.deadline";
        $where = "mod.module_ID = dead.module_ID AND dead.userId = $user_id AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND dead.status_ID = 2";
        $table = "tbl_deadline dead, tbl_module mod";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    public function get_follow_requests()
    {
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        $fields = "module_ID, request_ID, requestor, approvalLevel, follow";
        $where = "userId = $user_id AND DATE(follow) <= '" . $dateTime['date'] . "' AND status_ID = 2";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_deadline");
        // var_dump($record);
        return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_deadline");
    }

    private function get_due_deadlines_approvers()
    {
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        $fields = "dead.deadline_ID, dead.module_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage, dead.request_ID, dead.requestor, dead.userId, dead.approvalLevel, dead.deadline";
        $where = "mod.module_ID = dead.module_ID AND dead.userId = $user_id AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND dead.status_ID = 2";
        $table = "tbl_deadline dead, tbl_module mod";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    private function get_request_deadlines($module_id, $request_id, $approvalLevel)
    {
        $dateTime = $this->get_current_date_time();
        $fields = "dead.deadline_ID, dead.module_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage, dead.request_ID, dead.requestor, dead.userId, dead.approvalLevel, dead.deadline";
        $where = "mod.module_ID = dead.module_ID AND dead.module_ID = $module_id AND dead.request_ID = $request_id AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND dead.approvalLevel = $approvalLevel AND dead.status_ID = 2";
        $table = "tbl_deadline dead, tbl_module mod";
        return $record = $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    private function check_if_request_exist($request_deadline)
    {
        $approval_attrib = json_decode($this->get_approval_attrib($request_deadline->module_ID)->approvalAttrib);
        if (((int) $request_deadline->module_ID !== 1) AND ( (int) $request_deadline->module_ID !== 3))
        {
            $table = $approval_attrib->final_table;
            $attribute = $approval_attrib->request_id_col;
        }
        else
        {
            $table = $approval_attrib->requestTable;
            $attribute = $approval_attrib->requestCol;
        }
        $fields = "$attribute";
        $where = "$attribute = $request_deadline->request_ID";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        if (count($record) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function proccess_missed_deadlines($request_deadline)
    {
        $requestor = $this->get_user_details_due($request_deadline->requestor);
        $approver = $this->get_user_details_due($request_deadline->userId);
        $approval_level = $request_deadline->approvalLevel;
        $module = $request_deadline->module;
        $module_id = $request_deadline->module_ID;
        $request_id = $request_deadline->request_ID;
        $approvalLink = $request_deadline->approvalPageLink;
        $requestLink = $request_deadline->pendingRequestPage;
        $deadline = $request_deadline->deadline;
        if ($this->check_if_request_exist($request_deadline))
        {
            $max_approval_level = $this->get_max_approval_level_deadline($request_id, $module_id);
            if ($approval_level > 1)
            {
                $due['notify_preceding_approvers'] = $this->notify_preceding_approvers($requestor, $approver, $approval_level, $request_id, $module, $module_id, $approvalLink);
            }
            $due['missed_approver_stat'] = $this->set_current_approver_as_missed($module_id, $request_id, $approval_level, $approver->uid);
            if ($approval_level < $max_approval_level->approvalLevel)
            {
                //set status of the missed approver and next approver
                $due['next_approver_stat'] = $this->set_next_approver_as_current($module_id, $request_id, $approval_level);
                //notify next approver
                $due['notify_follow_approver'] = $this->notify_approver_follow($requestor, $approver, $approval_level, $request_id, $module, $module_id, $approvalLink, $deadline);
            }
            if ($approval_level == $max_approval_level->approvalLevel)
            {
                $due['notify_default_approver'] = $this->notify_default_approver($requestor, $approver, $approval_level, $request_id, $module, $module_id, $approvalLink, $deadline);
                $due['set_request_stat'] = $this->set_request_as_missed($module_id, $request_id);
            }
            //system notify requestor
            $due['notify_requestor'] = $this->notify_requestor($requestor, $approver, $approval_level, $request_id, $module, $module_id, $requestLink);
            //notify the deadlined approver
            $due['notify_missed_approver'] = $this->notify_dued_approver($requestor, $approver, $approval_level, $request_id, $module, $module_id, $approvalLink, $deadline);
            $due['set_deadline_status'] = $this->set_deadline_stat($request_deadline->deadline_ID);
            $due['exist'] = true;
        }
        else
        {
            $due['exist'] = false;
        }
        return $due;
    }

    // public function get_missed_preceding_approvers(){
    // }
    // approvers;

    public function get_missed_leave_appr()
    {
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        $fields = "dead.request_ID, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
        $where = "mod.module_ID = dead.module_ID AND dead.request_ID = leaveAppr.requestLeave_ID AND dead.module_ID = 2 AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND leaveMain.requestLeave_ID = leaveAppr.requestLeave_ID and leaveAppr.uid = $user_id AND leaveMain.finalStatus =2 AND dead.status_ID = 2";
        $table = "tbl_module mod, tbl_request_leave_approval leaveAppr, tbl_request_leave leaveMain, tbl_deadline dead";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        // $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        // var_dump($record);
    }

    public function get_missed_retract_leave_appr()
    {
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        $fields = "dead.request_ID, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
        $where = "mod.module_ID = dead.module_ID AND dead.request_ID = retractLeaveAppr.retractLeave_ID AND dead.module_ID = 4 AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND retractLeave.retractLeave_ID = retractLeaveAppr.retractLeave_ID AND retractLeaveAppr.uid = $user_id AND retractLeave.finalStatus = 2 AND dead.status_ID = 2";
        $table = "tbl_module mod, tbl_retract_leave_approval retractLeaveAppr, tbl_retract_leave retractLeave, tbl_deadline dead";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        // $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        // var_dump($record);
    }

    public function get_missed_dtr_appr()
    {
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        $fields = "dead.request_ID, dead.requestor, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
        $where = "mod.module_ID = dead.module_ID AND dead.request_ID = dtrReqAppr.dtrRequest_ID AND dead.module_ID = 3 AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND dtrReq.dtrRequest_ID = dtrReqAppr.dtrRequest_ID AND dtrReqAppr.userId = $user_id AND dtrReq.status_ID = 2 AND dead.status_ID = 2";
        $table = "tbl_module mod, tbl_dtr_request dtrReq, tbl_dtr_request_approval dtrReqAppr, tbl_deadline dead";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        // $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        // var_dump($record);
    }

    public function get_missed_ahr_appr()
    {
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        $fields = "dead.request_ID, dead.requestor, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
        $where = "mod.module_ID = dead.module_ID AND dead.request_ID =  ahrAppr.additionalHourRequest_ID AND dead.module_ID = 1 AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND ahr.additionalHourRequestId = ahrAppr.additionalHourRequest_ID AND ahrAppr.userId = $user_id AND ahr.approvalStatus_ID = 2 AND dead.status_ID = 2";
        $table = "tbl_module mod, tbl_additional_hour_request ahr, tbl_additional_hour_request_approval ahrAppr, tbl_deadline dead";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        // $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        // var_dump($record);
    }

    // public function get_missed_deadline_test()
    // {

    //     //identify if first approver
    //     $approvers = [];
    //     $due_deadlines = $this->get_missed_leave_appr();

    //     // var_dump($due_deadlines);

    //     // $leave = $this->get_missed_leave_appr();
    //     $retract = $this->get_missed_retract_leave_appr();
    //     $dtr = $this->get_missed_dtr_appr();
    //     $ahr = $this->get_missed_ahr_appr();
    //     if (count($retract) > 0)
    //     {
    //         $due_deadlines = (object) array_merge((array) $due_deadlines, (array) $retract);
    //     }
    //     if (count($dtr) > 0)
    //     {
    //         $due_deadlines = (object) array_merge((array) $due_deadlines, (array) $dtr);
    //     }
    //     if (count($ahr) > 0)
    //     {
    //         $due_deadlines = (object) array_merge((array) $due_deadlines, (array) $ahr);
    //     }

    //     // $due_deadlines = $this->get_due_deadlines_approvers();
    //     $count = 0;
    //     // $due['deadline'] = $due_deadlines;

    //     // -----------------------  
    //     if (count($due_deadlines))
    //     {
    //         $due['has_deadline'] = 1;
    //         foreach ($due_deadlines as $missed_requests)
    //         {
    //             $request_count = 0;
    //             $max_approval_level = $this->get_max_approval_level_deadline($missed_requests->request_ID, $missed_requests->module_ID);
    //             for ($approvalLevel = 0; $approvalLevel < $max_approval_level->approvalLevel; $approvalLevel++)
    //             {
    //                 $request_deadline = $this->get_request_deadlines($missed_requests->module_ID, $missed_requests->request_ID, $approvalLevel + 1);
    //                 if (count($request_deadline))
    //                 {
    //                     $proc_missed_deadline = $this->proccess_missed_deadlines($request_deadline);
    //                     if ($proc_missed_deadline['exist'])
    //                     {
    //                         $approvers['missed_approvers'][$request_count] = $proc_missed_deadline;
    //                         $request_count++;
    //                     }
    //                 }
    //             }
    //             if (count($approvers) > 0)
    //             {
    //                 $due['missed_requests'][$count] = $approvers;
    //             }
    //             $count++;
    //         }
    //     }
    //     else
    //     {
    //         $due['has_deadline'] = 0;
    //     }
    //     if (!array_key_exists('missed_requests', $due))
    //     {
    //         $due['has_deadline'] = 0;
    //     };
    //     echo json_encode($due);
    // }

    public function get_approval_deadlines_count_leave_retract($today_bool)
    {
        $deadline_today ="";
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        if($today_bool == 1){
            $deadline_today = "AND DATE(dead.deadline) = '".$dateTime['date']."'";
        }
        $fields = "dead.request_ID, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mods.module, mods.approvalPageLink, mods.pendingRequestPage";
        $where = "mods.module_ID = dead.module_ID AND dead.request_ID = retractLeaveAppr.retractLeave_ID $deadline_today AND dead.module_ID = 4 AND retractLeave.retractLeave_ID = retractLeaveAppr.retractLeave_ID AND dead.userId = retractLeaveAppr.uid AND retractLeaveAppr.uid = $user_id AND retractLeaveAppr.semiStatus = 4 AND dead.status_ID = 2";
        $table = "tbl_module mods, tbl_retract_leave_approval retractLeaveAppr, tbl_retract_leave retractLeave, tbl_deadline dead";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return count($record);
    }

    public function get_approval_deadlines_count_leave($today_bool)
    {
        $deadline_today ="";
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        if($today_bool == 1){
            $deadline_today = "AND DATE(dead.deadline) = '".$dateTime['date']."'";
        }
        $fields = "dead.request_ID, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mods.module, mods.approvalPageLink, mods.pendingRequestPage";
        $where = " mods.module_ID = dead.module_ID AND leaveAppr.requestLeave_ID = leaveAppr.requestLeave_ID $deadline_today  AND dead.module_ID = 2 AND leaveMain.requestLeave_ID = leaveAppr.requestLeave_ID AND dead.userId = leaveAppr.uid AND leaveAppr.uid = $user_id AND leaveAppr.semiStatus = 4 AND dead.status_ID = 2";
        $table = "tbl_module mods, tbl_request_leave_approval leaveAppr, tbl_request_leave leaveMain, tbl_deadline dead";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return count($record);
    }

    public function get_approval_deadlines_count_dtr($today_bool)
    {
        // $user_id = 2;
        $deadline_today ="";
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        if($today_bool == 1){
            $deadline_today = "AND DATE(dead.deadline) = '".$dateTime['date']."'";
        }
        $fields = "dead.request_ID, dead.requestor, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mods.module, mods.approvalPageLink, mods.pendingRequestPage";
        $where = "mods.module_ID = dead.module_ID AND dead.request_ID = dtrReqAppr.dtrRequest_ID $deadline_today AND dead.module_ID = 3 AND dtrReq.dtrRequest_ID = dtrReqAppr.dtrRequest_ID AND dead.userId = dtrReqAppr.userId AND dtrReqAppr.userId = $user_id AND dtrReqAppr.approvalStatus_ID = 4 AND dead.status_ID = 2";
        $table = "tbl_module mods, tbl_dtr_request dtrReq, tbl_dtr_request_approval dtrReqAppr, tbl_deadline dead";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return count($record);
    }
    
    // AHR
    
    public function get_approval_deadlines_count_ahr($today_bool){
        // $user_id = 81;
        $deadline_today ="";
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        if($today_bool){
            $deadline_today = "AND DATE(dead.deadline) = '".$dateTime['date']."'";
        }
        $fields = "dead.request_ID, dead.requestor, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
        $where = "mod.module_ID = dead.module_ID AND dead.request_ID =  ahrAppr.additionalHourRequest_ID $deadline_today AND dead.module_ID = 1 AND ahr.additionalHourRequestId = ahrAppr.additionalHourRequest_ID AND dead.userId = ahrAppr.userId AND ahrAppr.userId = $user_id AND ahrAppr.approvalStatus_ID = 4";
        $table = "tbl_module mod, tbl_additional_hour_request ahr, tbl_additional_hour_request_approval ahrAppr, tbl_deadline dead";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return count($record);

    }

    public function get_approval_links(){
        $appr_link['ahr_appr_link'] = $this->get_module_links(1)->approvalPageLink;
        $appr_link['dtr_appr_link'] = $this->get_module_links(3)->approvalPageLink;
        $appr_link['leave_appr_link'] = $this->get_module_links(2)->approvalPageLink;
        $appr_link['leave_retract_appr_link'] = $this->get_module_links(4)->approvalPageLink;
        echo json_encode($appr_link);
    }

    public function get_pending_appr_counts(){
        $appr_count['ahr_appr_pending'] = $this->get_approval_deadlines_count_ahr(0);
        $appr_count['dtr_appr_pending'] = $this->get_approval_deadlines_count_dtr(0);
        $appr_count['leave_appr_pending'] = $this->get_approval_deadlines_count_leave(0);
        $appr_count['leave_retract_appr_pending'] = $this->get_approval_deadlines_count_leave_retract(0);
        echo json_encode($appr_count);
        // var_dump($appr_count);

    }

    public function get_deadline_today_appr_counts(){
        $appr_count['ahr_appr_today'] = $this->get_approval_deadlines_count_ahr(1);
        $appr_count['dtr_appr_today'] = $this->get_approval_deadlines_count_dtr(1);
        $appr_count['leave_appr_today'] = $this->get_approval_deadlines_count_leave(1);
        $appr_count['leave_retract_appr_today'] = $this->get_approval_deadlines_count_leave_retract(1);
        echo json_encode($appr_count);
    }



    public function get_missed_deadline()
    {
        //identify if first approver
        $approvers = [];
        $due_deadlines = $this->get_missed_leave_appr();

        $leave = $this->get_missed_leave_appr();
        $retract = $this->get_missed_retract_leave_appr();
        $dtr = $this->get_missed_dtr_appr();
        $ahr = $this->get_missed_ahr_appr();
        if (count($retract) > 0)
        {
            $due_deadlines = (object) array_merge((array) $due_deadlines, (array) $retract);
        }
        if (count($dtr) > 0)
        {
            $due_deadlines = (object) array_merge((array) $due_deadlines, (array) $dtr);
        }
        if (count($ahr) > 0)
        {
            $due_deadlines = (object) array_merge((array) $due_deadlines, (array) $ahr);
        }

        // $due_deadlines = $this->get_due_deadlines_approvers();
        $count = 0;
        // $due['deadline'] = $due_deadlines;
        if (count($due_deadlines))
        {
            $due['has_deadline'] = 1;
            foreach ($due_deadlines as $missed_requests)
            {
                $request_count = 0;
                $max_approval_level = $this->get_max_approval_level_deadline($missed_requests->request_ID, $missed_requests->module_ID);
                for ($approvalLevel = 0; $approvalLevel < $max_approval_level->approvalLevel; $approvalLevel++)
                {
                    $request_deadline = $this->get_request_deadlines($missed_requests->module_ID, $missed_requests->request_ID, $approvalLevel + 1);
                    if (count($request_deadline))
                    {
                        $proc_missed_deadline = $this->proccess_missed_deadlines($request_deadline);
                        if ($proc_missed_deadline['exist'])
                        {
                            $approvers['missed_approvers'][$request_count] = $proc_missed_deadline;
                            $request_count++;
                        }
                    }
                }
                if (count($approvers) > 0)
                {
                    $due['missed_requests'][$count] = $approvers;
                }
                $count++;
            }
        }
        else
        {
            $due['has_deadline'] = 0;
        }
        if (!array_key_exists('missed_requests', $due))
        {
            $due['has_deadline'] = 0;
        };
        echo json_encode($due);
    }

    private function get_modules($requestor)
    {
        $dateTime = $this->get_current_date_time();
        $fields = "DISTINCT(module_ID)";
        if ($requestor != 0)
        {
            $where = "requestor = $requestor AND DATE(deadline) <'" . $dateTime['date'] . "' AND status_ID = 2";
        }
        else
        {
            $where = "DATE(deadline) <'" . $dateTime['date'] . "' AND status_ID = 2";
        }
        return $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_deadline');
    }

    //for Requestor
    private function get_pending_request($requestor, $module_id)
    {
        $dateTime = $this->get_current_date_time();
        $fields = "DISTINCT(request_ID)";
        if ($requestor != 0)
        {
            $where = "requestor = $requestor AND DATE(deadline) <'" . $dateTime['date'] . "' AND status_ID = 2 AND module_ID = $module_id";
        }
        else
        {
            $where = "DATE(deadline) <'" . $dateTime['date'] . "' AND status_ID = 2 AND module_ID = $module_id";
        }
        return $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_deadline');
    }

    public function check_if_monitoring_assigned()
    {
        $user_id = $this->session->userdata('uid');
        $fields = "useraccess_id";
        $where = "menu_item_id IN (97, 90) AND user_id = $user_id";
        $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_user_access');
        if (count($record) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function get_missed_deadline_requestor()
    {
        $user_id = $this->session->userdata('uid');
        $modules = $this->get_modules($user_id);
        $count = 0;
        $approvers = [];
        if (count($modules))
        {
            $due['has_deadline'] = 1;
            foreach ($modules as $modules_requests)
            {
                $due_deadlines = $this->get_pending_request($user_id, $modules_requests->module_ID);
                foreach ($due_deadlines as $missed_requests)
                {
                    $max_approval_level = $this->get_max_approval_level_deadline($missed_requests->request_ID, $modules_requests->module_ID);
                    $request_count = 0;
                    for ($approvalLevel = 0; $approvalLevel < $max_approval_level->approvalLevel; $approvalLevel++)
                    {
                        $request_deadline = $this->get_request_deadlines($modules_requests->module_ID, $missed_requests->request_ID, $approvalLevel + 1);
                        if (count($request_deadline))
                        {
                            $proc_missed_deadline = $this->proccess_missed_deadlines($request_deadline);
                            if ($proc_missed_deadline['exist'])
                            {
                                $approvers['missed_approvers'][$request_count] = $proc_missed_deadline;
                                $request_count++;
                            }
                        }
                    }
                    if (count($approvers) > 0)
                    {
                        $due['missed_requests'][$count] = $approvers;
                    }
                    $count++;
                }
            }
        }
        else
        {
            $due['has_deadline'] = 0;
        }
        if (!array_key_exists('missed_requests', $due))
        {
            $due['has_deadline'] = 0;
        };
        echo json_encode($due);
    }

    // public function check_if_assigned_with_monitoring(){
    // }
    //for Monitoring
    public function get_missed_deadline_monitoring()
    {
        $user_id = 0;
        if ($this->check_if_monitoring_assigned())
        {
            $modules = $this->get_modules($user_id);
            $count = 0;
            $approvers = [];
            if (count($modules))
            {
                $due['has_deadline'] = 1;
                foreach ($modules as $modules_requests)
                {
                    $due_deadlines = $this->get_pending_request($user_id, $modules_requests->module_ID);
                    // var_dump($due_deadlines);
                    foreach ($due_deadlines as $missed_requests)
                    {
                        $max_approval_level = $this->get_max_approval_level_deadline($missed_requests->request_ID, $modules_requests->module_ID);
                        // var_dump($max_approval_level);
                        $request_count = 0;
                        for ($approvalLevel = 0; $approvalLevel < $max_approval_level->approvalLevel; $approvalLevel++)
                        {
                            $request_deadline = $this->get_request_deadlines($modules_requests->module_ID, $missed_requests->request_ID, $approvalLevel + 1);
                            if (count($request_deadline))
                            {
                                $proc_missed_deadline = $this->proccess_missed_deadlines($request_deadline);
                                if ($proc_missed_deadline['exist'])
                                {
                                    $approvers['missed_approvers'][$request_count] = $proc_missed_deadline;
                                    $request_count++;
                                }
                            }
                        }
                        if (count($approvers) > 0)
                        {
                            $due['missed_requests'][$count] = $approvers;
                        }
                        $count++;
                    }
                }
            }
            else
            {
                $due['has_deadline'] = 0;
            }
            if (!array_key_exists('missed_requests', $due))
            {
                $due['has_deadline'] = 0;
            };
        }
        else
        {
            $due['has_deadline'] = 0;
        }
        echo json_encode($due);
    }

    // private function get_requestor_due_approvals()
    // {
    //     $dateTime = $this->get_current_date_time();
    //     $fields = "dead.deadline_ID, dead.module_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage, dead.request_ID, dead.requestor, dead.userId, dead.approvalLevel, dead.deadline";
    //     $where = "mod.module_ID = dead.module_ID AND dead.userId = $user_id AND DATE(dead.deadline) <'".$dateTime['date']."' AND dead.status_ID = 2";
    //     $table = "tbl_deadline dead, tbl_module mod";
    //     return $record = $this->general_model->fetch_specific_vals($fields,$where, $table);
    // }
    // public function get_requestor_due_approvers(){
    //     $fields = "userId, ";
    //     $where = "mod.module_ID = dead.module_ID AND dead.userId = $user_id AND DATE(dead.deadline) <'".$dateTime['date']."' AND dead.status_ID = 2";
    //     $table = "tbl_deadline dead, tbl_module mod";
    //     return $record = $this->general_model->fetch_specific_vals($fields,$where, $table);
    // }  
    // REMOVE DEADLINE

    protected function remove_deadlines($module_id, $request_id)
    {
        $where['module_ID'] = $module_id;
        $where['request_ID'] = $request_id;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_deadline');
    }

    // private function get_follow_data(){
    //     $user_id = $this->session->userdata('uid');
    //     $fields = "approvalLevel";
    //     $where = "userId = $user_id";
    //     return $record = $this->general_model->fetch_specific_vals($fields,$where,"tbl_deadline");
    // }
    // private function get_follow_requests(){
    // }
    // public function get_approvers_deadline_follow()
    // {
    //     $deadline['pending_deadline'] = $this->get_pending_deadline();
    //     $deadline['due_deadline'] = $this->get_due_deadlines();
    //     $deadline['follow'] = $this->get_follow_requests();
    //     echo json_encode($deadline);
    // }
    function getCurrentPosition($emp_id)
    {

        $fields = "pos_details,d.status";
        $where = "a.posempstat_id =b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and a.isActive=1 and emp_id=" . $emp_id;
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d");
    }

    //Mark Test
    public function totalHours($id, $custom = null)
    {
        $time = $this->empshifttime_fetch($id);
        $schedDate = $time[0]->sched_date;
        $inlog = $this->empshiftin_fetch($id);
        $outlog = $this->empshiftout_fetch($id);
        $shiftIN = date_format(date_create($time[0]->time_start), 'H:i');
        $shiftOUT = date_format(date_create($time[0]->time_end), 'H:i');
        $ampm = explode("|", $this->getTimeGreeting($shiftIN, $shiftOUT));

        $dateOut = ($ampm[1] == "Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($schedDate))) : $schedDate;

        $logoutNew = $dateOut . " " . $shiftOUT;

        $break = $this->empshiftgetrbreak_fetch($time[0]->acc_time_id);
        $totalBreak = $break[0]->hr + ($break[0]->min / 60);


        if (count($inlog) > 0 && count($outlog) > 0)
        {
            $in = $schedDate . " " . date_format(date_create($inlog[0]->login), 'H:i');
            $out = $outlog[0]->logout;
            $logout = (strtotime($out) >= strtotime($logoutNew)) ? $logoutNew : $out;

            $loginNew = (strtotime($in) <= strtotime($schedDate . " " . $shiftIN)) ? $schedDate . " " . $shiftIN : $in;

            $diff = strtotime($logout) - strtotime($loginNew);
            $totalWorked = (($diff / 60) / 60) - $totalBreak;
        }
        else
        {
            $totalWorked = 0;
            $totalShift = 0;
        }
        $diff_shift = strtotime($logoutNew) - strtotime($schedDate . " " . $shiftIN);
        $totalShift = (($diff_shift / 60) / 60) - $totalBreak;
        /* 			$arr["in"] = $in;
          $arr["loginNew"] = $loginNew;
          $arr["logoutNew"] = $logoutNew;
         */ $arr["total_actual_worked"] = $totalWorked;
        $arr["total_shift_hours"] = $totalShift;
        $arr["remaining_hour"] = abs($totalShift - $totalWorked);
        if ($custom == null)
        {
            echo json_encode($arr);
        }
        else
        {
            return $arr;
        }
    }

    public function empshifttime_fetch($sched_id)
    {
        $query = $this->db->query("SELECT time_start,time_end,sched_date,a.acc_time_id FROM tbl_schedule a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and sched_id=$sched_id");
        return $query->result();
    }

    public function empshiftgetrbreak_fetch($acc_time_id)
    {
        $query = $this->db->query("SELECT sum(hour) hr,sum(min) min FROM tbl_shift_break a,tbl_break_time_account b,tbl_break_time c where a.bta_id=b.bta_id and b.btime_id=c.btime_id and acc_time_id=$acc_time_id");
        return $query->result();
    }

    public function empshiftout_fetch($shift)
    {
        $query = $this->db->query("select dtr_id,emp_id,log as logout,acc_time_id from tbl_dtr_logs where entry='O' and type='DTR' and sched_id=" . $shift . " limit 1");
        return $query->result();
    }

    public function empshiftin_fetch($shift)
    {
        $query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and sched_id=" . $shift . " limit 1");
        return $query->result();
    }

    function getTimeGreeting($shiftIN, $shiftOUT)
    {
        date_default_timezone_set('Asia/Manila');
        $getTimeIn = explode(":", $shiftIN);
        $getTimeOut = explode(":", $shiftOUT);
        if (($getTimeIn[0] >= 0 && $getTimeIn[0] < 12) && ($getTimeOut[0] >= 0 && $getTimeOut[0] < 12))
        {

            $ampm = "AM|AM";
        }
        else
        {
            $in = ($getTimeIn[0] >= 0 && $getTimeIn[0] < 12) ? "AMs" : "PM";
            $out = (($getTimeOut[0] >= 12 && $getTimeOut[0] <= 23)) ? "PM" : "AM";
            $ampm = $in . "|" . $out;
        }

        if ($ampm == "PM|AM")
        {
            $newVal = "Today|Tomorrow";
        }
        else
        {
            $newVal = "Today|Today";
        }
        return $newVal;
    }

    protected function get_yearly_leave_credits($leaveType_ID)
    {
        $minusplus = 1;
        $uid = $this->session->userdata('uid');
        $emp_id = $this->session->userdata('emp_id');
        $currentPosition = $this->general_model->fetch_specific_val("a.emp_promoteID,a.dateFrom,b.posempstat_id,c.pos_details,d.status,e.credit", "a.emp_id=$emp_id AND a.isHistory=1 AND a.isActive=1 AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND e.isActive=1 AND e.posempstat_id=b.posempstat_id AND e.leaveType_ID=$leaveType_ID", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_leave_credit e", "DATE(dateFrom) DESC");
        if ($currentPosition == null)
        {
            return array('remaining' => null, 'credits' => null, 'taken' => null);
        }
        else
        {
            $request_leave_details = $this->general_model->fetch_specific_vals("minutes", "b.date >= '" . $currentPosition->dateFrom . "' AND b.isActive=1 AND YEAR(b.date) = '" . date('Y') . "' AND b.overAllStatus IN (2,5) AND a.requestLeave_ID=b.requestLeave_ID AND b.leaveType_ID = $leaveType_ID AND a.uid = $uid", "tbl_request_leave a,tbl_request_leave_details b");

            $total_minutes = 0;
            foreach ($request_leave_details as $rld)
            {
                $total_minutes += $rld->minutes;
            }
            $total_request_leave = $total_minutes / 480;
            //INSERT HERE QUERY FOR NEWLY REGULARIZED CHURVABELS
            $credit = $currentPosition->credit;
            $regularizedDate = $this->newly_regularized_checker($currentPosition->posempstat_id, $leaveType_ID);
            if ($regularizedDate !== NULL)
            {
                $regDate = strtotime($regularizedDate);
                if (date('Y') === date('Y', $regDate))
                {
                    $monthnum = date('n', $regDate);
                    $monthsleft = 12 - $monthnum + $minusplus; // plus 1 originally maybe to include current month || minus 1 if not include current month
                    $credit = $credit / 12 * $monthsleft;
                }
            }
            //INSERT HERE  QUERY FOR NEWLY REGULARIZED CHURVABELS
            //FOR DECIMAL CHURVA OF REGULARIZED
            if (is_numeric($credit) === true && floor($credit) !== $credit)
            {
                $number = (float) $credit;
                $whole = floor($number);      // 1
                $fraction = $number - $whole; // .25
                if ($fraction != 0)
                {
                    if ($fraction <= 0.25)
                    {
                        $credit = $whole;
                    }
                    else if ($fraction >= 0.75)
                    {
                        $credit = $whole + 1;
                    }
                    else
                    {
                        $credit = (float) $whole . '.5';
                    }
                }
            }

            //FOR DECIMAL CHURVA OF REGULARIZED
            $remaining = $credit - $total_request_leave;
            $final_remaining = ($remaining < 0) ? 0 : round($remaining, 2);
            return array('remaining' => $final_remaining, 'credits' => $credit, 'taken' => $total_request_leave);
        }
    }

    public function newly_regularized_checker($posempstat_id, $leaveType_ID)
    {
        //if employee latest emp promote is his 1st regularization
        //then if current history is the 1st regularization
        if ($leaveType_ID == 2 || $leaveType_ID == 6)
        {
            //2 = vacation
            //6 = sick
            $emp_id = $this->session->userdata('emp_id');
            $history = $this->general_model->fetch_specific_val("posempstat_id ", "posempstat_id =$posempstat_id AND isActive = 1 AND empstat_id = 3", "tbl_pos_emp_stat ");

            if ($history === NULL)
            {//NOT REGULAR
                return NULL;
            }
            else
            {
                $currentPosition = $this->general_model->fetch_specific_val("b.emp_promoteID, a.posempstat_id, b.dateFrom ", "emp_id =$emp_id AND a.isActive = 1 AND b.ishistory = 1 AND a.posempstat_id=b.posempstat_id AND b.isActive = 1", "tbl_pos_emp_stat a,tbl_emp_promote b", "b.dateFrom ASC");

                $regularization = $this->general_model->fetch_specific_val("b.emp_promoteID, a.posempstat_id, b.dateFrom ", "emp_id =$emp_id AND a.isActive = 1 AND b.ishistory = 1 AND a.posempstat_id=b.posempstat_id AND empstat_id = 3", "tbl_pos_emp_stat a,tbl_emp_promote b", "b.dateFrom ASC");
                if ($regularization !== NULL)
                {
                    if ($currentPosition->emp_promoteID == $regularization->emp_promoteID)
                    {
                        //latest promote == 1st regularization
                        return $regularization->dateFrom;
                    }
                }
                else
                {
                    return NULL;
                }
            }
        }
        else
        {
            return NULL;
        }
    }

//------------------------------NOTIFICATIONS---------------------------------------------------
    public function readNotificationOnPage()
    {
        $mainNotifTable = $this->input->post('mainNotifTable');
        $subNotifTable = $this->input->post('subNotifTable');
        $mainNotifID = $this->input->post('mainNotifID');
        $subNotifID = $this->input->post('subNotifID');
        $link = $this->input->post('link');
        $uid = $this->session->userdata('uid');
        $final_path = str_replace(base_url(), "", $link);
        $pos = strpos($final_path, "#");
        if ($pos !== false)
        {
            $final_path = substr($final_path, 0, $pos);
        }
        $stat_read = 9;
        $stat_unread = 8;
        $notifications = $this->general_model->fetch_specific_vals("$subNotifID", "a.$mainNotifID=b.$mainNotifID AND b.recipient_ID=$uid AND status_ID=$stat_unread AND link='$final_path'", "$mainNotifTable a, $subNotifTable b");
        $data = array();
        foreach ($notifications as $notif)
        {
            $data[] = array('data' => array('status_ID' => $stat_read), 'where' => $subNotifID . '=' . $notif->$subNotifID);
        }
        $res = $this->general_model->update_array_vals($data, $subNotifTable);
//        echo $this->db->last_query();
        if ($res)
        {
            $unreadcount = $this->general_model->fetch_specific_val("COUNT(*) as unreadcount", "a.$mainNotifID=b.$mainNotifID AND b.recipient_ID=$uid AND status_ID=$stat_unread", " $mainNotifTable a, $subNotifTable b")->unreadcount;
        }
        else
        {
            $unreadcount = 0;
            //echo failed;
        }
        echo json_encode(array('unreadCount' => $unreadcount));
    }

    public function readNotification()
    {
        $recipient_ID = $this->input->post('recipient_ID');
        $subNotifTable = $this->input->post('subNotifTable');
        $status_ID = 9;
        $res = $this->general_model->update_vals(array('status_ID' => $status_ID), "recipient_ID=$recipient_ID", $subNotifTable);

        if ($res)
        {
            echo json_encode(array('status' => 'Success'));
        }
        else
        {
            echo json_encode(array('status' => 'Failed'));
        }
    }

    public function getMyAuditTrail()
    {
        $uid = $this->session->userdata('uid');
        $limiter = $this->input->post('limiter');
        $data = $this->general_model->custom_query("SELECT io_id,log,ipaddress,remark FROM tbl_login_logout_session WHERE uid=$uid ORDER BY log DESC LIMIT $limiter,$this->perPage");
        foreach ($data as $d)
        {
            if ($d->remark === 'Login' || $d->remark === 'Logout')
            {
                $d->colorclass = 'metal';
            }
            else if ($d->remark === 'I - DTR' || $d->remark === 'O - DTR' || $d->remark === 'I - DTR (w/o sched)' || $d->remark === 'O - DTR (w/o sched)')
            {
                $d->colorclass = 'info';
            }
            else
            {
                $d->colorclass = 'warning';
            }
        }
        echo json_encode(array('audittrail' => $data));
    }

    public function getEmployeeSubordinate($uid)
    {
        $query = "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,c.uid,acc_name,a.pic FROM tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.Supervisor=c.emp_id AND b.isActive='yes' AND c.uid=" . $uid;
        $data = $this->general_model->custom_query($query);
        $results = $data;
        $stop = false;
        if (!empty($data))
        {
            while ($stop === false)
            {
                foreach ($data as $d)
                {
                    $emps[] = $d->emp_id;
                }
                $query2 = "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,d.acc_name,e.uid FROM tbl_applicant a,tbl_employee b,tbl_account d ,tbl_user e WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.isActive='yes' AND e.emp_id=b.emp_id AND b.Supervisor IN (" . implode(',', $emps) . ")";

                $data = $this->general_model->custom_query($query2);
                if (!empty($data))
                {
                    $results = array_merge($results, $data);
                    unset($emps);
                }
                else
                {
                    $stop = true;
                }
                usort($results, array($this, "cmp"));
            }
        }
        return $results;
    }

    function cmp($a, $b)
    {
        return strcmp($a->lname, $b->lname);
    }

    protected function update_sched_lock($sched_id, $remarks, $module_id, $locked_stat){
        $data['remarks'] = $remarks;
        $data['module_ID'] = $module_id;
        $data['isLocked'] = $locked_stat;
        $where = "sched_id = $sched_id";
        return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_schedule');
    }

    protected function get_emp_account_details($emp_id)
    {
        $fields = "accounts.acc_id, accounts.acc_name, beforeCallingTime";
        $where = "accounts.acc_id = emp.acc_id AND emp.emp_id = $emp_id";
        $tables = "tbl_account accounts, tbl_employee emp";
        return $record = $this->general_model->fetch_specific_val($fields, $where, $tables);
    }


    protected function get_allowable_break($account_id, $sched_id)
    {
        $fields = "accTime.acc_time_id, break.break_type, breakTime.hour, breakTime.min";
        $where = "breakTime.btime_id = accountBreak.btime_id AND break.break_id = accountBreak.break_id AND accountBreak.bta_id = shiftBreak.bta_id AND times.time_id = accTime.time_id AND shiftBreak.isActive = 1 AND shiftBreak.acc_time_id = accTime.acc_time_id AND  accTime.acc_time_id = sched.acc_time_id  AND acc_id =$account_id AND sched.sched_id = $sched_id";
        $tables = "tbl_acc_time accTime, tbl_shift_break shiftBreak, tbl_break_time_account accountBreak, tbl_break_time breakTime, tbl_break break, tbl_time times, tbl_schedule sched";
        $breaks = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        $hours = 0;
        $minutes = 0;
        if(count($breaks) > 0){
            foreach ($breaks as $break_val) {
                $hours += $break_val->hour;
                $minutes += $break_val->min;
            }
            $hours_to_min = $hours * 60;
            $minutes += $hours_to_min;
            $break['breakData'] = $breaks;
            $break['minutes'] = $minutes;
        }else{
            $break['breakData'] = 0;
            $break['minutes'] = 0;
        }
        return $break;
    }

    public function get_breaks($login_id, $logout_id, $emp_id, $entry){
        $fields = "dtr.dtr_id, dtr.emp_id, dtr.acc_time_id, dtr.entry, dtr.log, dtr.note, dtr.type, dtr.sched_id, break.break_type, bt.hour, bt.min";
        $where = "break.break_id = bta.break_id AND bt.btime_id = bta.btime_id AND bta.bta_id = shiftBreak.bta_id AND shiftBreak.sbrk_id = dtr.acc_time_id AND dtr.dtr_id > $login_id AND dtr.dtr_id < $logout_id AND dtr.emp_id = $emp_id AND dtr.type = 'Break' AND dtr.entry = '$entry'";
        $table = "tbl_dtr_logs dtr, tbl_shift_break shiftBreak, tbl_break_time_account bta, tbl_break_time bt, tbl_break break";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, 'dtr_id', 'ASC');
    }

    protected function check_start_end_shift($date_time_start, $date_time_end)
    {
        if (strtotime($date_time_end) < strtotime($date_time_start)) {
            $date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' 1 days'));
            $date_time_start = $date_time_start;
        }
        $shift_start_date_time = new DateTime($date_time_start);
        $formatted_shift_start = $shift_start_date_time->format("H:i");
        if ($formatted_shift_start == "00:00") {
            $date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' 1 days'));
            $date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
        }
        $new_date_shift = [
            'start' => $date_time_start,
            'end' => $date_time_end,
        ];
        return $new_date_shift;
    }

    protected function new_format_midnight($formatted_date, $date){
        $new_format_date = $formatted_date;
        if($formatted_date == $date." 00:00 AM"){
            $new_format_date =  $date." 24:00";
        }
        return $new_format_date;
    }

    protected function get_human_time_format($date_time_from, $date_time_to, $bct)
    {
        $diff = $date_time_to - $date_time_from;
        $hour = $diff / 3600;
        $hour_bct_diff = $hour - $bct;
        $time['hoursTotal'] = $hour;
        $time['hours'] = (int) $hour_bct_diff;
        $decimal = $hour_bct_diff - (int) $hour_bct_diff;
        $time['minutes'] = round($decimal * 60);
        return $time;
    }

    public function create_log_history($module_ID, $emp_id, $text)
    {
        return $this->general_model->insert_vals(array('module_ID' => $module_ID, 'emp_id' => $emp_id, 'text' => $text), "tbl_log_history");
    }

}
