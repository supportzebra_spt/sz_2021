<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Templatedesign extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function sideBar()
	{
				// $this->load->view('TemplateSideBar',$query);
		$data = array();
		if($this->session->userdata('uid')){
			$query = $this->db->query("select  distinct(a.tab_id),tab_name,icon from tbl_menu_tab_item a,tbl_menu_items b,tbl_user_access c,tbl_user d where a.tab_id =b.tab_id and b.menu_item_id=c.menu_item_id and d.uid=c.user_id and d.uid='".$this->session->userdata('uid')."'  and c.is_assign=1 and isNewTemplate=0");
			$rez = $query->result();
			  
			foreach($rez as $key =>$v){
 				 		  $data [$v->tab_name."|".$v->icon] = $this->sideBarMenu($v->tab_id);
			}
			 						 // $data['items'] = $this->sideBarMenu($data);
				
			//echo json_encode($data);
			$this->load->view('TemplateSideBar',array('res'=>$data));
		
		}else{
					redirect('login');
		}
	}
		public function sideBarMenu($rez)
	{
 
  $query = $this->db->query("select item_link,item_title from tbl_menu_tab_item a,tbl_menu_items b,tbl_user_access c,tbl_user d where a.tab_id =b.tab_id and b.menu_item_id=c.menu_item_id and d.uid=c.user_id and b.tab_id =".$rez."  and uid='".$this->session->userdata('uid')."'  and c.is_assign=1 and b.is_active=1 and b.isShow=1 and isNewTemplate=0");
  
     return $query->result();

 }
	public function headerLeft()
	{
		$this->load->view('HeaderLeft');
		
	}
	public function Templategear()
	{
		$this->load->view('Templategear');
	}
}
