<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AttndMonitoring extends CI_Controller {

	public function __construct(){
		parent::__construct(); 
		$this->load->model('AttndMonitoringModel');
	}
	public function index(){if($this->session->userdata('uid')){
		$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='AttndMonitoring' and is_assign=1");
		foreach ($qry->result() as $row)
		{
			$arr['setting'] = array("settings" => $row->settings);
		}
		if ($qry->num_rows() > 0){
					// $this->load->view('attendancelist');

			$this->load->view('attendancelist2',$arr);
		}else{
			redirect('index.php/home/error');
		}
	}else{
		redirect('index.php/login');
	}
}
public function getAttendancelist(){
	$fromDate = $this->input->post('fromDate');
	$toDate = $this->input->post('toDate');
	$employeelist = $this->input->post('employeelist');	
	// $fromDate = "2017-11-21";
	// $toDate = "2017-11-24";
	// $employeelist = "639-636-600-378-572-613-199-612-500-599-432-506-197-627-195-630-526-635-396-241-501-611-498-617-415-497-638-286-213-425-614-502-417-409-37-637";
	// $employeelist = "425-502";
	$employeelist = explode('-',$employeelist);

	$employees = $this->AttndMonitoringModel->getEmployeeDetails($employeelist);
	$period = new DatePeriod(
		new DateTime("$fromDate"),
		new DateInterval('P1D'),
		new DateTime("$toDate +1 day ")
	);

	foreach ($period as $date) {
		$dates[] = $date->format("Y-m-d");
	}

	foreach($employees as $emp){
		$datex = array();
		//GETTING THE WHOLE DTR INS AND OUTS-------------------------------------------------------
		$dtrin = $this->AttndMonitoringModel->getDTRIns($emp->emp_id,reset($dates),end($dates));
		$dtrout = $this->AttndMonitoringModel->getDTROuts($emp->emp_id,reset($dates),end($dates));
		foreach($dtrin as $din){
			$naa=false;
			foreach($dtrout as $dout){
				if($din->dtr_id_in==$dout->note){
					$din->dtr_id_out = $dout->dtr_id_out;
					$din->log_out = $dout->log_out;
					$din->log_out_view = $dout->log_out_view;
					$naa=true;

				}
			}

			if($naa==false){
				$din->dtr_id_out = null;
				$din->log_out = null;
			}
		}
		
		foreach($dates as $date){
			//COMPUTATION OF SCHEDULES------------------------------------------------------
			$exists = false;
			$schedx = $this->AttndMonitoringModel->getEmpSchedule($emp->emp_id,$date);
			if($schedx==''||$schedx==NULL){
				$schedule = NULL;
			}else{
				$schedule = $schedx;

				foreach($dtrin as $key => $din){
					if($din->sched_id==$schedule->sched_id){
						$logs = $din;
						$exists = true;
						$schedStat = 'Tagged';
					//BREAKS RETRIEVAL ---------------------------------------
						if($din->dtr_id_out==NULL){
							$fb = $this->getFirstBreak($emp->emp_id,$din->dtr_id_in);
							$lunchb = $this->getLunchBreak($emp->emp_id,$din->dtr_id_in);
							$lb = $this->getLastBreak($emp->emp_id,$din->dtr_id_in);
						}else{
							$fb = $this->getFirstBreak($emp->emp_id,$din->dtr_id_in,$din->dtr_id_out);
							$lunchb = $this->getLunchBreak($emp->emp_id,$din->dtr_id_in,$din->dtr_id_out);
							$lb = $this->getLastBreak($emp->emp_id,$din->dtr_id_in,$din->dtr_id_out);
						}
					}
				}
			}
			
			if($exists==false){
				$logs = NULL;
				$fb = NULL;
				$lunchb = NULL;
				$lb = NULL;
				$schedStat = '-';
			}
			//BREAKS COMPUTATIONS--------------------------------------------
			$totalbreaks = 0;
			if(empty($fb)){
				$totalhours = '-';
			}else if(count($fb)==1){
				$totalhours = '-';
			}else{
				$datetime1 = strtotime($fb[0]->log);
				$datetime2 = strtotime($fb[1]->log);
				$interval  = abs($datetime2 - $datetime1);
				$minutes   = round($interval / 60);
				$totalbreaks += $minutes;
			}
			if(empty($lunchb)){
				$totalhours = '-';
			}else if(count($lunchb)==1){
				$totalhours = '-';
			}else{
				$datetime1 = strtotime($lunchb[0]->log);
				$datetime2 = strtotime($lunchb[1]->log);
				$interval  = abs($datetime2 - $datetime1);
				$minutes   = round($interval / 60);
				$totalbreaks += $minutes;
			}
			if(empty($lb)){
				$totalhours = '-';
			}else if(count($lb)==1){
				$totalhours = '-';
			}else{
				$datetime1 = strtotime($lb[0]->log);
				$datetime2 = strtotime($lb[1]->log);
				$interval  = abs($datetime2 - $datetime1);
				$minutes   = round($interval / 60);
				$totalbreaks += $minutes;
			}

			//BREAKS COMPUTATION BASE ON THE ALLOTED BREAK TIMES------------------------------
			if(empty($fb)){
				$fbAdd = 0;
			}else{
				$res = $this->AttndMonitoringModel->getBreakTime('FIRST BREAK',$emp->acc_id);
				if(empty($res)){
					$fbAdd = 0;
				}else{
					$hour = $res->hour;
					$min = $res->min;
					$hourtomin = $hour * 60;
					$fbAdd = $hourtomin+$min;
				}
			}
			if(empty($lunchb)){
				$lunchAdd = 0;
			}else{
				$res = $this->AttndMonitoringModel->getBreakTime('LUNCH',$emp->acc_id);
				if(empty($res)){
					$lunchAdd = 0;
				}else{
					$hour = $res->hour;
					$min = $res->min;
					$hourtomin = $hour * 60;
					$lunchAdd = $hourtomin+$min;
				}
			}
			if(empty($lb)){
				$lbAdd = 0;
			}else{
				$res = $this->AttndMonitoringModel->getBreakTime('LAST BREAK',$emp->acc_id);
				if(empty($res)){
					$lbAdd = 0;
				}else{
					$hour = $res->hour;
					$min = $res->min;
					$hourtomin = $hour * 60;
					$lbAdd = $hourtomin+$min;
				}
			}
			$totalbreaktimes = $fbAdd+$lunchAdd+$lbAdd;

			//FINAL COMPUTATIONS AREA ----------------------------------------------------
			$status = "<td width='80px'>None</td>";
			if($schedule==NULL){
				$totalhours = "<span >0 hrs & 0 mins</span>";
			}else if($schedule->type=="Rest Day"){
				$totalhours = "<span>0 hrs & 0 mins</span>";
				$status   = '<td width="80px"><span class="font-gray-dark">Rest Day</span></td>';
			}else if($schedule->type=="Leave"){
				$totalhours = "<span>0 hrs & 0 mins</span>";
				$status   = '<td width="80px"><span class="font-gray-dark">Leave</span></td>';
			}else if($logs==NULL){
				$totalhours = "<span class='font-orange'>0 hrs & 0 mins</span>";
			}else if($logs->log_out==NULL){
				$totalhours = "<span>0 hrs & 0 mins</span>";
				$status   = '<td width="80px"><span class="text-warning">No Log Out</span></td>';
			}else if($schedule->type=='Normal' || $schedule->type=='WORD'){
				//COMPUTATION FOR THE SCHEDULE IN AND OUT------------------------------------
				$ts = strtotime($schedule->time_start);
				$te = strtotime($schedule->time_end);
				if($logs->login_date==$schedule->sched_date){
					if($te<$ts){
						$schedoutdt = strtotime("+1 day", strtotime($logs->login_date.' '.$schedule->time_end));
					}else{
						$schedoutdt = strtotime($logs->login_date.' '.$schedule->time_end);
					}
					if($schedule->time_start=='12:00:00 AM'){
						$schedindt = strtotime("+1 day", strtotime($logs->login_date.' '.$schedule->time_start));
						$schedoutdt += 86400;
					}else{
						$schedindt = strtotime($logs->login_date.' '.$schedule->time_start);
					}
				}else{
					if($te<$ts){
						$schedoutdt = strtotime("+1 day", strtotime($schedule->sched_date.' '.$schedule->time_end));
					}else{
						$schedoutdt = strtotime($schedule->sched_date.' '.$schedule->time_end);
					}
					if($schedule->time_start=='12:00:00 AM'){
						$schedindt = strtotime("+1 day", strtotime($schedule->sched_date.' '.$schedule->time_start));
						$schedoutdt += 86400;
					}else{
						$schedindt = strtotime($schedule->sched_date.' '.$schedule->time_start);
					}
				}
				
				//COMPUTATION FOR LOGS-------------------------------------------------
				$logindt = strtotime($logs->log_in);
				$logoutdt = strtotime($logs->log_out);
				$BCTdt = strtotime('-10 minutes',$schedindt);
				if($logindt<$schedindt){
					$starttouse = $schedindt;
					// echo 'early';
				}else{
					$starttouse = $logindt;
					// echo 'late';
				}
				if($logoutdt<$schedoutdt){
					$endtouse = $logoutdt;
					// echo 'undertime';
				}else if($logoutdt<$schedindt){
					// echo "No TIME";
					$endtouse = 0;
					$starttouse = 0;
				}else{
					// echo 'normal';
					$endtouse = $schedoutdt;
				}

				//TOTAL COMPUTATION -------------------------------------------------------------
				$totalhours = $endtouse-$starttouse-($totalbreaktimes*60);
				$h = $totalhours / 3600 % 24;
				$m = $totalhours / 60 % 60; 
				if($h<0 || $m<0){
					$totalhours = "<span class='font-orange'>0 hrs & 0 mins</span>";
				}else{
					$totalhours = "<span>$h hrs & $m mins</span>";
				}

				//DISPLAY CODES-----------------------------------------------------------------
				if($schedoutdt<$logindt){
					$status   = '<td width="80px"><span class="text-danger">Logs are not between shift</span>';
					$totalhours = "<span class='font-warning'>0 hrs & 0 mins</span>";
				}else if($logindt<$BCTdt){
					$interval  = strtotime("+10 minutes",abs($logindt - $BCTdt));
					$schedindt -= 86400;
					if($logoutdt<$schedindt){
						$status   = '<td width="80px"><span class="text-danger">Logs are not between shift</span>';
						$totalhours = "<span class='font-warning'>0 hrs & 0 mins</span>";
					} else if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-success">'.floor($interval / 60) .' mins early</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-success">'.floor($interval / 60) .' mins early</span></td>';
					}
					// echo 'early';
				}else if($logindt==$BCTdt){
					$status   = '<td width="80px" class="text-success">On Time</td>';
					if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-success">On Time</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-success">On Time</span></td>';
					}
				}else if($logindt<$schedindt){
					$interval  = abs($logindt - $schedindt);
					if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-warning">'.abs(floor(($interval / 60)-10)) .' mins late BCT</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-warning">'.abs(floor(($interval / 60)-10)) .' mins late BCT</span></td>';
					}
					//echo 'bct late'
				}else if($logindt==$schedindt){
					if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-warning">10 mins late BCT</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-warning">10 mins late BCT</span></td>';
					}
				}else{
					$interval  = abs($logindt - $schedindt);
					if($logoutdt<$schedoutdt){	
						$status   = '<td width="80px"><span class="text-danger">'.floor($interval / 60) .' mins late ACT</span><br> AND <br><span class="text-danger">'.floor(($schedoutdt-$logoutdt) / 60) .' mins undertime</span> </td>';
					}else{
						$status   = '<td width="80px"><span class="text-danger">'.floor($interval / 60) .' mins late ACT</span></td>';
					}
					// echo 'act late';
				}
			}else{
				$totalhours = "<span class='font-orange'>0 hrs & 0 mins</span>";
			}

			if($emp->acc_id==1){
				if($totalbreaks<=90){
					$totalbreaks = $totalbreaks.' mins / '.floor($totalbreaktimes).' mins';
				}else{
					$totalbreaks = "<span class='font-red'>$totalbreaks mins</span> / ".floor($totalbreaktimes)." mins";
				}
			}else{
				if($totalbreaks<=60){
					$totalbreaks = $totalbreaks.' mins / '.floor($totalbreaktimes).' mins';
				}else{
					$totalbreaks = "<span class='font-red'>$totalbreaks mins</span> / ".floor($totalbreaktimes)." mins";
				}
			}
			//FINAL ARRAY PER EMPLOYEE----------------------------------------------------------------
			$datex[] = array('date'=>$date,'schedule'=>$schedule,'logs'=>$logs,'firstbreak'=>$fb,'lunchbreak'=>$lunchb,'lastbreak'=>$lb,'totalhours'=>$totalhours,'totalbreaks'=>$totalbreaks,'status'=>$status,'schedStat'=>$schedStat);
			
		}

		$emp->details = $datex;
	}

	// echo "<pre>";
	// var_dump($employees);
	// echo "</pre>";
	echo json_encode($employees);
}


public function getFirstBreak($emp_id,$dtr_id_in,$dtr_id_out=NULL){
	if($dtr_id_out==NULL){
		$dtr_id_out = $dtr_id_in + 10000;
	}
	$fb = $this->AttndMonitoringModel->getBreaks(2,$emp_id,$dtr_id_in,$dtr_id_out);
	return $fb ;
}
public function getLunchBreak($emp_id,$dtr_id_in,$dtr_id_out=NULL){
	if($dtr_id_out==NULL){
		$dtr_id_out = $dtr_id_in + 10000;
	}
	$lb = $this->AttndMonitoringModel->getBreaks(3,$emp_id,$dtr_id_in,$dtr_id_out);
	return $lb;
}
public function getLastBreak($emp_id,$dtr_id_in,$dtr_id_out=NULL){
	if($dtr_id_out==NULL){
		$dtr_id_out = $dtr_id_in + 10000;
	}
	$lb = $this->AttndMonitoringModel->getBreaks(4,$emp_id,$dtr_id_in,$dtr_id_out);
	return $lb;
}
}
