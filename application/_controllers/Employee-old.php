<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	
	public function index(){
		$query = $this->db->query("select 
		fname,
		lname,
		pic,
		b.apid,
		b.emp_id,
		id_num,
		bday,
		placebday,
		intellicare,
		pioneer,
		BIR,
		philhealth,
		pagibig,
		sss,
		TIMESTAMPDIFF(YEAR,bday,now()) as age,
		a.pos_id,
		c.pos_name,
		b.emp_stat,
		cell
		from tbl_applicant a,tbl_employee b,tbl_position c where c.pos_id=a.pos_id and a.apid=b.apid and b.isActive='yes' order by lname ASC");
			foreach ($query->result() as $row)
			{
				$arr['data'][$row->apid] = array(
				"apid" => $row->apid,
				"emp_id" => $row->emp_id,
				"fname" => $row->fname,
				"lname" => $row->lname,
				"pic" => $row->pic,
				"id_num" => $row->id_num,
				"bday" => $row->bday,
				"age" => $row->age,
				"placebday" => $row->placebday,
				"intellicare" => $row->intellicare,
				"pioneer" => $row->pioneer,
				"BIR" => $row->BIR,
				"philhealth" => $row->philhealth,
				"pagibig" => $row->pagibig,
				"sss" => $row->sss,
				"pos_id" => $row->pos_id,
				"pos_name" => $row->pos_name,
				"emp_stat" => $row->emp_stat,
				"cell" => $row->cell,
 				);
			}
 		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='employee' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
					$this->load->view('employees',$arr);
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}
 

 		}
		
		
	public function updateEmployee(){
		$empid = $this->input->post('emp_id');
		if($empid!=null){
			$query = $this->db->query("select 
 		fname,
		mname,
		lname,
		nikname,
		ccaExp,
		gender,
		civil,
		pic,
		b.apid,
		b.emp_id,
		id_num,
		bday,
		placebday,
		intellicare,
		pioneer,
		BIR,
		philhealth,
		pagibig,
		sss,
		TIMESTAMPDIFF(YEAR,bday,now()) as age,
		a.pos_id,
		c.pos_name,
		b.emp_stat,
		cell,
		tel,
		email,
		blood,
		relijon,
		school_name,
		course,
		school_note,
		emergency_fname,
		emergency_mname,
		emergency_lname,
		emergency_extname,
		emergency_contact,
		other_latestEmp,
		other_lastPosition,
		other_inclusiveDate,
		intellicare,
		pioneer,
		BIR,
		philhealth,
		pagibig,
		sss,
		dtr_uname,
		dtr_password,
		Supervisor,
		supEmail
		from tbl_applicant a,tbl_employee b,tbl_position c where c.pos_id=a.pos_id and a.apid=b.apid and b.isActive='yes' and emp_id= ".$empid." order by lname ASC");
			foreach ($query->result() as $row)
			{
				
				$arr['emp'] = array(
				"apid" => $row->apid,
				"emp_id" => $row->emp_id,
 				"fname" => $row->fname,
				"lname" => $row->lname,
				"mname" => $row->mname,
				"nikname" => $row->nikname,
				"gender" => $row->gender,
				"civil" => $row->civil,
				"pic" => $row->pic,
				"id_num" => $row->id_num,
				"bday" => $row->bday,
				"age" => $row->age,
				"placebday" => $row->placebday,
				"intellicare" => $row->intellicare,
				"pioneer" => $row->pioneer,
				"BIR" => $row->BIR,
				"philhealth" => $row->philhealth,
				"pagibig" => $row->pagibig,
				"sss" => $row->sss,
				"pos_id" => $row->pos_id,
				"pos_name" => $row->pos_name,
				"emp_stat" => $row->emp_stat,
				"cell" => $row->cell,
				"tel" => $row->tel,
				"email" => $row->email,
				"blood" => $row->blood,
				"relijon" => $row->relijon,
				"school_name" => $row->school_name,
				"course" => $row->course,
				"school_note" => $row->school_note,
				"emergency_fname" => $row->emergency_fname,
				"emergency_mname" => $row->emergency_mname,
				"emergency_lname" => $row->emergency_lname,
				"emergency_extname" => $row->emergency_extname,
				"emergency_contact" => $row->emergency_contact,
				"ccaExp" => $row->ccaExp,
				"other_latestEmp" => $row->other_latestEmp,
				"other_lastPosition" => $row->other_lastPosition,
				"other_inclusiveDate" => $row->other_inclusiveDate,
				"intellicare" => $row->intellicare,
				"pioneer" => $row->pioneer,
				"BIR" => $row->BIR,
				"philhealth" => $row->philhealth,
				"pagibig" => $row->pagibig,
				"sss" => $row->sss,
				"dtr_uname" => $row->dtr_uname,
				"dtr_password" => $row->dtr_password,
				"Supervisor" => $row->Supervisor,
				"supEmails" => $row->supEmail
				);
			} 
				 
			if($this->session->userdata('uid')){
				if($arr['emp']['Supervisor'] != null){
					$query2 = $this->db->query("select fname, mname, lname from tbl_applicant a,tbl_employee b,tbl_position c where c.pos_id=a.pos_id and a.apid=b.apid and b.isActive='yes' and emp_id= ".$arr['emp']['Supervisor']." order by lname ASC");
				foreach ($query2->result() as $row){
					$arr['Supname'] = $row->fname." ".$row->lname;
				}
				}else{
					$arr['Supname'] = " ";
				}
				$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Applicant' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
					   $this->load->view('employeeUpdate',$arr);
			 
		}else{
			redirect('index.php/login');
		}

		 }else{
			redirect('index.php/Employee');
		}
	}
 public function listofSupervisor(){
	 $query = $this->db->query("select b.apid,emp_id,fname,lname,a.status,b.email from tbl_applicant as a,tbl_employee as b,tbl_account c where a.apid=b.apid and c.acc_id = b.acc_id and a.status='employ' and email!='' and c.acc_description='Admin' and b.isActive='yes'");
	 
	 foreach ($query->result() as $row){
			$arr[]=  array( 			
			"emp_id"=>$row->emp_id,
			"label"=>$row->fname." ".$row->lname,
			"email"=>$row->email
			);

	 }
	 echo json_encode($arr);
 }
}