<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Org extends CI_Controller {

  public function __construct(){
        parent::__construct();

			}
	public function index(){
		$this->load->view('org_view');
	}
	public function sh($acc_id){
	$data["accid"] = $acc_id;
		 $this->load->view('sh',$data);
	}
	public function loadPosition(){
	$acc_id = $this->input->post("acc_id");
	// $acc_id = 6;
		$qry = "SELECT * FROM `tbl_account` where acc_id=$acc_id";
			$rs = $this->db->query($qry);
		$type = $rs->result()[0]->acc_description;
		$qry1 = "select pos_id,pos_name,pos_details from tbl_position where class='$type' and pos_id not in (SELECT pos_id FROM `tbl_account` a,tbl_account_position b,tbl_position c where a.acc_id=b.accounts_ID and b.positions_ID=c.pos_id and a.acc_id=$acc_id  and accountReference is not null) order by pos_details ASC";
		$query = $this->db->query($qry1);
			foreach($query->result() as $row){
				$data[$row->pos_id."-".$row->pos_details] = array(
					"pos_id" => $row->pos_id,
					"pos_name" => $row->pos_name,
					"pos_details" => $row->pos_details,
				);	
			}
	echo json_encode($data);
	}
	public function addAccountPosition($flag=null,$accpos){
		if($flag==1){
		$pos_id = $this->input->post("pos_id");
		$acc_id =$accpos;
		$level = 1;	
		$this->addifPositionLevelisOne($acc_id,$pos_id);
		}else{		
		
		$accpos_id = explode("-",$accpos);
		$acc_id=$accpos_id[1];
		$selected_pos_id=$accpos_id[0]; // OrigNode
		
		$pos_id = $this->input->post("pos_id2"); //Node to be added
		$lvl = $this->input->post("level");
		$rs = $this->checkIfAccountPosExist($acc_id,$selected_pos_id);
		$getCurrentPos = $rs[0]->accountPositionLevel;
		
		if($lvl=="Above"){
			if($getCurrentPos==1){
				$this->updateifPositionLevelDecrement($acc_id,$getCurrentPos);
				$this->addifPositionLevelisOne($acc_id,$pos_id);
			}else{
				$rs = 0;
			}
			
		}elseif($lvl=="Under"){
			$this->updateifPositionLevelDecrement($acc_id,$getCurrentPos+1,"and accountPositionLevel>".$getCurrentPos);
			$this->addifPositionLevelisOne($acc_id,$pos_id,$getCurrentPos);
		}else{
		
			$this->addifPositionLevelisOne($acc_id,$pos_id,$getCurrentPos-1);
		}
		}
		//echo $getCurrentPos;
		/* $qry = "insert into tbl_account_position(accounts_ID,positions_ID,accountPositionLevel) values($acc_id,$pos_id,$level)";
		$rs = $this->db->query($qry);
		echo $this->db->affected_rows(); */
		
	}
	
	public function addifPositionLevelisOne($acc_id,$pos_id,$level=null){
	$lvl = ($level!=null) ? $level+1 : 1;
		$insert = $this->db->query("insert into tbl_account_position(accounts_ID,positions_ID,accountPositionLevel) values($acc_id,$pos_id,$lvl)");
	}
	public function updateifPositionLevelDecrement($acc_id,$getCurrentPos,$condition=null){
	$cdt  = ($condition!=null) ? $condition : "";
		$qry = $this->db->query("SELECT * FROM `tbl_account_position` where accounts_ID=$acc_id ".$cdt);
		$i=$getCurrentPos+1;
		foreach($qry->result() as $row){
			$this->db->query("update tbl_account_position set accountPositionLevel=".$i." where account_position_ID=".$row->account_position_ID);	
			$i++;
		}
	}
	public function deleteNode(){
		$acc_pos_id = explode(",",$this->input->post("acc_pos_id"));
		// echo $acc_pos_id[0]." // ".$acc_pos_id[1];
			$rs = $this->db->query("delete from tbl_account_position where accounts_ID=".$acc_pos_id[1]." and positions_ID=".$acc_pos_id[0]);
			echo $this->db->affected_rows();
	}
	public function checkIfAccountPosExist($acc_id,$pos_id){
			$qry = "SELECT * FROM `tbl_account_position` where accounts_ID=$acc_id and positions_ID=$pos_id";
			$rs = $this->db->query($qry);
			return $rs->result();
		
	}
	public function loadPositionAccount(){
	$acc_id = $this->input->post("acc_id");
		$qry = "SELECT * FROM `tbl_account` where acc_id=$acc_id";
		$rs = $this->db->query($qry);
		$type = $rs->result()[0]->acc_description;
	$qry1 = "select pos_id,pos_name,pos_details from tbl_position where class='$type' and pos_id  in (SELECT pos_id FROM `tbl_account` a,tbl_account_position b,tbl_position c where a.acc_id=b.accounts_ID and b.positions_ID=c.pos_id and a.acc_id=$acc_id  and accountReference is not null) order by pos_details asc";
	$query = $this->db->query($qry1);
	foreach($query->result() as $row){
		$data[$row->pos_id."-".$row->pos_details] = array(
 			"pos_name" => $row->pos_name,
			"pos_details" => $row->pos_details,
		
		);	
	}
	echo json_encode($data);
	}
	public function sh3($acc_id){
	$qry ="SELECT * FROM `tbl_account` where acc_id=$acc_id";
	$query = $this->db->query($qry);
	$data[] = array(
		"key" => $query->result()[0]->acc_id,
		"name" => $query->result()[0]->acc_name,
		"title" => "",
	);
	echo json_encode($data);
	}
	public function sh2($acc_id){
	$qry ="SELECT acc_id,acc_name,accountReference,accountPositionLevel,accountLevel,pos_id,pos_name,pos_details FROM `tbl_account` a,tbl_account_position b,tbl_position c where a.acc_id=b.accounts_ID and b.positions_ID=c.pos_id and a.acc_id=$acc_id and accountReference is not null order by accountPositionLevel";
	$query = $this->db->query($qry);
	if($query->num_rows() >0){
		
			$data[] = array(
				"key" => $query->result()[0]->acc_id,
				"name" => $query->result()[0]->acc_name,
				"title" => "",
			);
			$i=1000;
		foreach($query->result() as $row){
		if($row->accountPositionLevel==1){
				$lvl = "1st";
		}elseif($row->accountPositionLevel==2){
			$lvl = "2nd";
		}elseif($row->accountPositionLevel==3){
			$lvl = "3rd";
		}elseif($row->accountPositionLevel==4){
			$lvl = "4th";
		}elseif($row->accountPositionLevel==5){
			$lvl = "5th";
		}elseif($row->accountPositionLevel==6){
			$lvl = "6th";
		}else{
			$lvl = "nth";
		}
			$data[] = array(
				"key" => $row->pos_id.",".$row->acc_id,
				"name" => $row->pos_details,
				"title" => $lvl." Level",
				"parent" => $row->acc_id,
			);
		}
		 echo json_encode($data);
	}else{
	echo 0;
	}
 	}
	public function show(){
			$qry ="SELECT acc_id,acc_name,accountReference,accountLevel FROM `tbl_account` where status_ID !=11 and accountReference is not null order by accountLevel";
		
		$query = $this->db->query($qry);
		foreach($query->result() as $row){
		if($row->accountLevel==0){
				$lvl = "1st";
		}elseif($row->accountLevel==1){
			$lvl = "2nd";
		}elseif($row->accountLevel==2){
			$lvl = "3rd";
		}elseif($row->accountLevel==3){
			$lvl = "4th";
		}elseif($row->accountLevel==4){
			$lvl = "5th";
		}elseif($row->accountLevel==5){
			$lvl = "6th";
		}else{
			$lvl = "nth";
		}
			$data[] = array(
				"key" => $row->acc_id,
				"name" => $row->acc_name,
				"title" => $lvl." Level",
				"parent" => $row->accountReference,
			
			);
		}
		echo json_encode($data);
	}
}