<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	
	public function index(){ 

	}
	public function ssscontribution(){ 
		$this->load->view('sss_contribution_table');
	}
	public function ssscontribution_upload(){ 
		  $this->load->view('sss_contribution_upload');
	
	} 
	public function ssscontribution_download(){ 
		$objPhpExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('./assets/images/logo2.png');
		$objDrawing->setHeight(100);
		$objDrawing->setWorksheet($objPhpExcel->getActiveSheet());
		$objPhpExcel->getActiveSheet()->setShowGridlines(false);
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('argb' => 'ffffff'),
 				),
			),
			'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb'=>'6699cc'),
                ),
				 
			'font' => array(
                        'bold' => true,
                )
		);
 		$sheet = $objPhpExcel->getActiveSheet();
		$headers = array('Min Range','Max Range','Monthly Salary Credit','SS-ER','SS-EE','SS-Total','EC-ER','Total Contribution','Total Contribution (SE/VM/OFW)');
		for($i=0;$i<=8;$i++){
			$in = $this->alphaNum($i);
			$sheet->getColumnDimension($in)->setAutoSize(true);
			$sheet->setCellValue($in.'6',$headers[$i]);
 		}
		$sheet->getStyle("A6:I6")->applyFromArray($styleArray);
		$sheet->setAutoFilter("A6:I6");
 		$writer = PHPExcel_IOFactory::createWriter($objPhpExcel, 'Excel2007');

		header('Content-Disposition: attachment;filename="myfile.xlsx"');
		header('Content-type: application/vnd.ms-excel');
		$writer->save('php://output');
	
	}
	public function alphaNum($num){
	   $alphabet = array( 'a', 'b', 'c', 'd', 'e',
                       'f', 'g', 'h', 'i', 'j',
                       'k', 'l', 'm', 'n', 'o',
                       'p', 'q', 'r', 's', 't',
                       'u', 'v', 'w', 'x', 'y',
                       'z'
                       );
 		return $alphabet[$num];
	}
		
		 
}