<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PositionAssignment extends CI_Controller {
    public function index(){
        // if($this->session->userdata('uid')){
//
//            $qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='<LINK>' and is_assign=1 order by lname");   
//            if ($qry->num_rows() > 0){
                    $sessionData = $this->session->userdata();
                    $this->load->view('positionAssignment', $sessionData);
//            }else{
//                redirect('index.php/home/error');
//            }
//        }   
    }
    // READ ---------------------------------------------------------------------------
    public function getEmployeePositions($accountPositionId){
        $fields = "pos.pos_details";
        $where = "pos.pos_id = acctPos.positions_ID AND acctPos.account_position_ID = $accountPositionId";
        $tables = "tbl_position pos, tbl_account_position acctPos";
        $record = $this->GeneralModel->fetchSpecificValue($fields,$where, $tables);
        return $record;
    }
    
    public function getUsers(){
        $fields = "users.uid, users.fname, users.lname, users.accountPositionId";
        $where = "emp.emp_id = users.emp_id AND emp.isActive = 'yes'";
        $table = "tbl_user users, tbl_employee emp";
        $record = $this->GeneralModel->fetchSpecificValues($fields,$where,$table);
        $count = 0;
        foreach($record as $rows){
            // echo $rows->accountPositionId;
            if($rows->accountPositionId == NULL){
                $employeePosition = "No Assigned Position";
            }else{
                $position = $this->getEmployeePositions($rows->accountPositionId);
                $employeePosition = $position->pos_details;
            }
            $employeeData[$count] = [
                'uid' => $rows->uid,
                'fname' => $rows->fname,
                'lname' => $rows->lname,
                'accountPositionId' => $rows->accountPositionId,
                'position' => $employeePosition
            ];
            $count++;
        }
        echo json_encode($employeeData);
        // var_dump($employeeData);
    }

    public function getAccounts(){
        $fields = "acc_id, acc_name";
        $where = "acc_id IS NOT NULL";
        $record = $this->GeneralModel->fetchSpecificValues($fields,$where,'tbl_account');
        echo json_encode($record);
    }

    public function getPosition(){
        $accountId = $this->input->post('accountId');
        $fields = "pos.pos_id, pos.pos_details";
        $where = "pos.pos_id = acctpos.positions_ID AND acctpos.accounts_ID = $accountId";
        $tables = "tbl_position pos, tbl_account_position acctpos";
        $record = $this->GeneralModel->fetchSpecificValues($fields,$where, $tables);
        echo json_encode($record);
    }

    public function getAccountPositionId($accountId, $positionId){
        $fields = "account_position_ID";
        $where = "accounts_ID = $accountId AND positions_ID = $positionId";
        $record = $this->GeneralModel->fetchSpecificValue($fields,$where, 'tbl_account_position');
        return $record;
    }

    public function getAccountPositionDetails(){
        $accountPositionId = $this->input->post('accountPositionId');
        $fields = "accounts_ID, positions_ID";
        $where = "account_position_ID = $accountPositionId";
        $record = $this->GeneralModel->fetchSpecificValue($fields,$where, 'tbl_account_position');
        echo json_encode($record);
    }

    private function getEmployeeId($userId){
        $fields = "emp_id";
        $where = "uid = $userId";
        return $record = $this->GeneralModel->fetchSpecificValue($fields, $where, 'tbl_user');
    }

    // UPDATE -------------------------------------------------------------------------

    private function setAccountofEmployee($userId, $accountId){
        $employeeId = $this->getEmployeeId($userId);
        $data['acc_id'] = $accountId;
        $where = "emp_id = $employeeId->emp_id";
        return $updateStatus = $this->GeneralModel->updateValue($data, $where, 'tbl_employee');
    }

    public function setAccountPosition(){
        $accountPositionId = $this->getAccountPositionId($this->input->post('accountId'), $this->input->post('positionId'));
        $userId = $this->input->post('userId');
        $data['accountPositionId'] = $accountPositionId->account_position_ID;
        $where = "uid = $userId";
        $updateStatus['setUserAccountPosition'] = $this->GeneralModel->updateValue($data, $where, 'tbl_user');
        $updateStatus['setAccountOfEmployee'] = $this->setAccountofEmployee($userId, $this->input->post('accountId'));
        echo json_encode($updateStatus);
    }
}