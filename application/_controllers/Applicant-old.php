<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Applicant extends CI_Controller {

  public function __construct(){
        parent::__construct();
		$this->load->model("ApplicantModel");
 }
	public function index(){
		$query = $this->db->query("select apid,fname,lname,a.pos_id,pos_name,pos_details,pic,cell,nextension,gender from tbl_applicant a,tbl_position b where a.pos_id=b.pos_id and isActive='Yes' and PF='yez'");
			foreach ($query->result() as $row)
			{
				$arr['data'][$row->apid] = array(
				"apid" => $row->apid,
				"cell" => $row->cell,
				"fname" => $row->fname,
				"nextension" => $row->nextension,
				"lname" => $row->lname,
				"gender" => $row->gender,
				"pos_id" => $row->pos_id,
				"pos_name" => $row->pos_name,
				"pos_details" => $row->pos_details,
 				"pic" => $row->pic,
 				);
			}
 		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Applicant' and is_assign=1");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
				if ($qry->num_rows() > 0){
					$this->load->view('applicantview',$arr);
				}else{
					redirect('index.php/home/error');
				}
			}else{
				redirect('index.php/login');
			}
		}
	
	
	public function applyUpload(){
		$file = $_FILES["image"];
		$temp = explode(".", $_FILES["image"]["name"]);
		$newfilename = round(microtime(true)).'.'.$temp[1];

		$target_dir = "./assets/images/img/";
	//$target_file = $target_dir.basename($_FILES["image"]["name"]);
	$target_file = $target_dir.$newfilename;
// $query = $this->db->query("insert into tbl_applicant(fname,mname,lname,nextension,gender,Source,ccaExp,tel,cell,isActive,PF,pic,pos_id,dateCreated,presentAddrezs,relijon,cr1,cr2,cr3,permanentAddrezs,civilStatus,birthday,previouscompany) values('".$_POST['fname']."','".$_POST['mname']."','".$_POST['lname']."','".$_POST['suffix']."','".$_POST['gender']."','".$_POST['source']."','".$_POST['ccexpo']."','".$_POST['altcontactnum']."','".$_POST['contactnum']."','yes','yez','img/".$_FILES["image"]["name"]."',(select pos_id from tbl_position where pos_details='".$_POST['position']."'),now(),'".$_POST['presentAddr']."','".$_POST['religion']."','".$_POST['creference1']."','".$_POST['creference2']."','".$_POST['creference3']."','".$_POST['permanentAddr']."','".$_POST['cs']."','".$_POST['bday']."','".$_POST['previous']."')");
$query = $this->db->query("insert into tbl_applicant(fname,mname,lname,nextension,gender,source,referral,ccaExp,tel,cell,isActive,PF,pic,pos_id,dateCreated,presentAddrezs,relijon,cr1,cr2,cr3,permanentAddrezs,civilStatus,birthday,previouscompany) values('".$_POST['fname']."','".$_POST['mname']."','".$_POST['lname']."','".$_POST['suffix']."','".$_POST['gender']."','".$_POST['sourceApply']."','".$_POST['source']."','".$_POST['ccexpo']."','".$_POST['altcontactnum']."','".$_POST['contactnum']."','yes','yez','img/".$newfilename."',(select pos_id from tbl_position where pos_details='".$_POST['position']."'),now(),'".$_POST['presentAddr']."','".$_POST['religion']."','".$_POST['creference1']."','".$_POST['creference2']."','".$_POST['creference3']."','".$_POST['permanentAddr']."','".$_POST['cs']."','".$_POST['bday']."','".$_POST['previous']."')");

	if((move_uploaded_file($_FILES["image"]["tmp_name"],$target_file)) && $query){
				$this->email->initialize(array(
				  'protocol' => 'smtp',
				  'smtp_host' => 'smtp.gmail.com',
				  'smtp_user' => 'recruitment@supportzebra.com',
				  'smtp_pass' => 'hrdepartment2014',
				  'smtp_port' => 587,
				  'smtp_crypto' => 'tls',
				  'mailtype	' => 'html',
				  'validate' => TRUE,
				  'crlf' => "\r\n",
				  'charset' => "iso-8859-1",
				  'newline' => "\r\n"
				));
					$data = array(
					 'Name'=> $_POST['fname']." ".$_POST['mname']." ".$_POST['lname'],
					 'Address'=> $_POST['presentAddr'],
					 'source'=> $_POST['sourceApply'],
					 'gender'=> $_POST['gender'],
					 'contact'=> $_POST['contactnum'],
					 'img'=> 'img/'.$newfilename.'',
					 'img2'=> 'http://10.200.101.250/sz3/assets/images/img/'.$newfilename
						 );
				$this->email->from('recruitment@supportzebra.com', 'HR - RECRUITMENT');
				$this->email->to('recruitment@supportzebra.com');
				$this->email->cc('jeffrey@supportzebra.com');
				$this->email->subject('REPORT | Applicants on '.date("m.d.y").'');
				$body = $this->load->view('testEmail.php',$data,TRUE);
			    $this->email->attach($data['img2']);
 				$this->email->message($body);
				$this->email->send();

				echo $this->email->print_debugger();
		echo "ok";
		}else{
			echo "fail";
		}


 }
	public function apply2(){
			$query = $this->db->query("select pos_id,pos_details from tbl_position where isHiring='Yes'");
		foreach ($query->result() as $row){
			$arr['data'][$row->pos_id] = array(
			"pos_id" => $row->pos_id,
			"pos_details" => $row->pos_details,
			);
		}
		$this->load->view('applicant_form2',$arr);
		}
		public function apply3(){
			$query = $this->db->query("select pos_id,pos_details from tbl_position where isHiring='Yes'");
		foreach ($query->result() as $row){
			$arr['data'][$row->pos_id] = array(
			"pos_id" => $row->pos_id,
			"pos_details" => $row->pos_details,
			);
		}
		$this->load->view('applicant_form3',$arr);
		}
		public function apply4(){
			$query = $this->db->query("select pos_id,pos_details from tbl_position where isHiring='Yes'");
		foreach ($query->result() as $row){
			$arr['data'][$row->pos_id] = array(
			"pos_id" => $row->pos_id,
			"pos_details" => $row->pos_details,
			);
		}
		$this->load->view('applicant_form4',$arr);
		}
	public function apply(){
	$query = $this->db->query("select pos_id,pos_details from tbl_position where isHiring='Yes'");
		foreach ($query->result() as $row){
			$arr['data'][$row->pos_id] = array(
			"pos_id" => $row->pos_id,
			"pos_details" => $row->pos_details,
			);
		}
		$this->load->view('applicant_form',$arr);
	}
	
	 
			 
			 
 
	public function insertRecord(){
		$data = json_decode(file_get_contents("php://input"));
		$fname = $data->fname;
		$mname = $data->mname;
		$lname = $data->lname;
		$mobile = $data->mobile;
		$tel = $data->tel;
		$Gender = $data->Gender;
		$Religion = $data->Religion;
		$position = $data->position;
		$source = $data->source;
		$ccexpo = $data->ccexpo;
		$address = $data->address;
		$cr1 = $data->cr1;
		$cr2 = $data->cr2;
		$cr3 = $data->cr3;
		$this->ApplicantModel->addApplicantRecord($fname,$mname,$lname,$mobile,$Gender,$position,$source,$ccexpo,$address,$Religion,$cr1,$cr2,$cr3,$tel);	
    }
 	public function evaluateApplicant(){
		if($this->input->post('apid')!=null){
$query = $this->db->query("select test_id,lname,c.pos_id,test_info,count,time_count,times,result,remarks,date_taken,interviewer,uid,c.apid,pos_name from tbl_applicant c left join tbl_test a on c.apid=a.apid left join tbl_position b on a.pos_id=b.pos_id   where c.apid=".$this->input->post('apid')."");
			$rez = $query->result();
			 if ($query->num_rows() > 0)
			{
				foreach($rez as $key => $val){
						$arr['user'][$val->test_id] = array(
							"test_id" => $val->test_id,
							"lname" => $val->lname,
							"pos_id" => $val->pos_id,
							"test_info" => $val->test_info,
							"count" => $val->count,
							"time_count" => $val->time_count,
							"times" => $val->times,
							"result" => $val->result,
							"remarks" => $val->remarks,
							"date_taken" => $val->date_taken,
							"interviewer" => $val->interviewer,
							"uid" => $val->uid,
							"apid" => $val->apid,
							"pos_name" => $val->pos_name,
						);
						
						}
						$arr['lname'] = $val->lname;
						$arr['apid'] = $val->apid;
						$arr['pos_id'] = $val->pos_id;
						$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Applicant' and is_assign=1");
						foreach ($qry->result() as $row)
						{
								$arr['setting'] = array("settings" => $row->settings);
						}
  					$this->load->view('applicantEvaluate',$arr);
			}else{
 					$this->load->view('applicantEvaluate');
			}
 
 		}else{
			redirect('index.php/applicant');
		}
	}
 	public function updaterecord(){
		if($this->input->post('apid')!=null){
			 $query = $this->db->query("select apid,fname,lname,mname,nextension,gender,source,ccaExp,cell,tel,isActive,a.pos_id,presentAddrezs,permanentAddrezs,relijon,cr1,cr2,cr3,pic,civilStatus,birthday,presentAddrezs,pos_details,previouscompany from tbl_applicant a,tbl_position b where a.pos_id =b.pos_id and  isActive='Yes' and apid=".$this->input->post('apid')."");
			$query2 = $this->db->query("select pos_id,pos_details from tbl_position where isHiring='Yes'");
			// $query3 = $this->db->query("SELECT b.ic_id,category_name,group_concat(question SEPARATOR '<br>') as question from tbl_interviewCategory a,tbl_interviewQuestions b where a.ic_id= b.ic_id and pos_dep_id = 49 and pos_dep_id in (select pos_dep_id from tbl_interviewQuestions) ");
			$query3 = $this->db->query("SELECT  * from tbl_interviewCategory");
			
					$rez = $query->result();
			 if ($query->num_rows() > 0 || $query2->num_rows() > 0)
			{
				foreach($rez as $key => $val){
					foreach($val as $k => $v){
							$arr['user'][$k] = $v;
						}
					}
				foreach ($query2->result() as $row){
					// $arr['data'][$row->pos_id] = "{value:".$row->pos_id.",text:'".$row->pos_details."'}";
					  $arr['data'][$row->pos_id] = array(
						"pos_id" => $row->pos_id,
						"pos_details" => $row->pos_details,
						);
				}
				$qryInt =  $this->db->query("SELECT time_count FROM tbl_test where apid=".$this->input->post('apid')." and test_info='Interview' order by test_id DESC limit 1");
				$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Applicant' and is_assign=1");
				 
				if($qryInt->num_rows() ==0){
					$lvl = "Initial";
 				}else{
					 $row = $qryInt->row_array();
					 if($row["time_count"]=="Initial"){
						$lvl = "Second";
					 }elseif($row["time_count"]=="Second"){
						$lvl = "Final";
					 }else{
						$lvl = " ";

					 }

				}
				$arr['lvl'] = array("lvl" => $lvl);
				 
						foreach ($qry->result() as $row)
						{
								 $arr['setting'] = array("settings" => $row->settings);
						}
						$arrs = array();
						if($query3->num_rows() >0){
							foreach ($query3->result() as $row){
 
								 $intQuestions = $this->InterviewQuestions($row->ic_id,$this->input->post('pos_id'),$lvl);
								 foreach ($intQuestions as $row2 => $val){
								  $arr['catInt'][$row->category_name."*".$row->ic_id][$val->iq_id] = array(
								  "question" => $val->question,
								  "iq_id" => $val->iq_id,
								  "ic_id" => $row->ic_id,
								  );
								} 
							}
						}else{
							$arr['catInt'] = null;
						}
						 //print_r($arrs);
 				  $this->load->view('applicantUpdate',$arr);
				}else{
					  $this->load->view('applicantUpdate');
				}
		}else{
			 redirect('index.php/applicant');
		}
	}
	public function InterviewQuestions($iq,$posid,$lvl){
		$iqq = $this->db->query("select * from tbl_interviewQuestions where ic_id=".$iq." and isActive=1 and level= '".$lvl."' and pos_id like '%".$posid."%'");
		return $iqq->result();
	}
	public function addExamApplicant(){
 
				$exam= $this->input->post('exam');
				$score= $this->input->post('score');
				$dateInt= $this->input->post('dateInt');
				$commentInt= $this->input->post('commentInt');
				$scoreDetails= $this->input->post('scoreDetails');
				$level= $this->input->post('level');
				$pos_id= $this->input->post('pos_id');
				$uid= $this->input->post('uid');
				$apid= $this->input->post('apid');
				$interviewer= $this->input->post('interviewer');
				if($level=='Initial'){
					$cnt = 1;
				}elseif($level=='Second'){
					$cnt = 2;
				}else{
					$cnt = 3;
				}
				if($score>60){
					$result = "Passed";
				}else{
					$result = "Failed";
				}
 
 		 $query =  $this->db->query("insert into tbl_test(pos_id,test_info,count,result,remarks,date_taken,interviewer,uid,apid,score,time_count) values($pos_id,'$exam',$cnt,'$result','$commentInt',CURDATE(),'$interviewer',$uid,$apid,$score,'$level')");
 	}
	public function updateApplicantss(){
		$fName= $this->input->post('fName');
		$mName= $this->input->post('mName');
		$lName= $this->input->post('lName');
		$nextension= $this->input->post('nextension');
		$bday= $this->input->post('bday');
		$civilStatus= $this->input->post('civilStatus');
		$relijon= $this->input->post('relijon');
		$gender= $this->input->post('gender');
		$cell= $this->input->post('cell');
		$tel= $this->input->post('tel');
		$presentAddrezs= $this->input->post('presentAddrezs');
		$permanentAddrezs= $this->input->post('permanentAddrezs');
		$pos_id= $this->input->post('pos_id');
		$Source= $this->input->post('Source');
		$ccaExp= $this->input->post('ccaExp');
		$cr1= $this->input->post('cr1');
		$cr2= $this->input->post('cr2');
		$cr3= $this->input->post('cr3');
		$apid= $this->input->post('apid');
		// $this->ApplicantModel->updateApplicantss($fName,$mName,$lName,$nextension,$bday,$civilStatus,$relijon,$gender,$cell,$tel,$presentAddrezs,$permanentAddrezs,$pos_id,$Source,$ccaExp,$cr1,$cr2,$cr3,$apid);	
 		$query =  $this->db->query("update tbl_applicant set fname='".$fName."',mname='".$mName."',lname='".$lName."',nextension='".$nextension."',birthday='".$bday."',tel='".$tel."',cell='".$cell."',gender='".$gender."',source='".$Source."',ccaExp='".$ccaExp."',pos_id=(select pos_id from tbl_position where pos_details='".$pos_id."'),presentAddrezs='".$presentAddrezs."',permanentAddrezs='".$permanentAddrezs."',relijon='".$relijon."',civilStatus='".$civilStatus."',cr1='".$cr1."',cr2='".$cr2."',cr3='".$cr3."' where apid=".$apid."");

		if($query){
		echo "1 update tbl_applicant set fname='".$fName."',mname='".$mName."',lname='".$lName."',nextension='".$nextension."',birthday='".$bday."',tel='".$tel."',cell='".$cell."',gender='".$gender."',source='".$Source."',ccaExp='".$ccaExp."',pos_id=(select pos_id from tbl_position where pos_details='".$pos_id."'),presentAddrezs='".$presentAddrezs."',permanentAddrezs='".$permanentAddrezs."',relijon='".$relijon."',civilStatus='".$civilStatus."',cr1='".$cr1."',cr2='".$cr2."',cr3='".$cr3."' where apid=".$apid."";	
		}else{
			echo "update tbl_applicant set fname='".$fName."',mname='".$mName."',lname='".$lName."',nextension='".$nextension."',birthday='".$bday."',tel='".$tel."',cell='".$cell."',gender='".$gender."',source='".$Source."',ccaExp='".$ccaExp."',pos_id=(select pos_id from tbl_position where pos_details='".$pos_id."'),presentAddrezs='".$presentAddrezs."',permanentAddrezs='".$permanentAddrezs."',relijon='".$relijon."',civilStatus='".$civilStatus."',cr1='".$cr1."',cr2='".$cr2."',cr3='".$cr3."' where apid=".$apid."";
		}
	}
	public function writeApplicantEval(){
		$result= $this->input->post('result');
		$remark= $this->input->post('remark');
		$date= $this->input->post('date');
		$apid= $this->input->post('apid');
		$posid= $this->input->post('posid');
		$this->ApplicantModel->addWritten($result,$remark,$date,$apid,$posid);
		
	}
	public function interviewApplicantEval(){
       		   
		$result= $this->input->post('result');
		$remark= $this->input->post('remark');
		$date= $this->input->post('date');
		$apid= $this->input->post('apid');
		$posid= $this->input->post('posid');
		$interviewer= $this->input->post('interviewer');
		$level= $this->input->post('level');
		$this->ApplicantModel->addInterview($result,$remark,$date,$apid,$posid,$interviewer,$level);
		
	}
		//---------------------------------------------------NICCA CODES----------------------------------

	public function login(){
		if($this->session->userdata('applicantId')!=NULL){
			redirect('index.php/Applicant/landing');
		}else{
			$this->load->view('applicantlogin');	
		}
	}
	public function doLogin(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data = $this->ApplicantModel->findApplicant($username,$password);

		if($data == NULL){
			echo 'Username and Password mismatch';
		}else{
			if(strcmp($username,$data->username)==0 && strcmp($password,$data->password)==0){

				$sets = $this->ApplicantModel->getSets();
				$set = array_rand($sets);
				$set = $sets[$set]['examset'];
				$this->session->set_userdata('applicantId', $data->apid);
				$this->session->set_userdata('name', $data->lname.', '.$data->fname.' '.$data->mname);
				$this->session->set_userdata('positionApplied', $data->pos_details);
				$this->session->set_userdata('positionId', $data->pos_id);
				$this->session->set_userdata('examset', $set);
				$newtimestamp = strtotime(DATE('M j, Y H:i:s').' + 30 minutes');
				$enddate = date('M j, Y H:i:s', $newtimestamp);
				$this->session->set_userdata('endtime', $enddate);
				$result = $this->ApplicantModel->isFinished($this->session->userdata('applicantId'),$this->session->userdata('positionId'));
				if($result != NULL){
					echo 'You have already taken this exam.';
				}else{

					echo 'Success';
				}
			}else{
				echo 'Username and Password mismatch';
			}
		}
	}
	public function landing(){
		if($this->session->userdata('applicantId')==NULL){
			redirect('index.php/Applicant/login');
		}else{
			$size = 0;

			$questions = $this->ApplicantModel->getQuestions($this->session->userdata('examset'));
			shuffle($questions);
			$data['questions'] = $questions;
			foreach($questions as $q){
				$size = $size + $q->points;
			}
			$data['totalscore'] = $size;
				// var_dump($data['questions']);
			$this->load->view('writtenExamination',$data);
		}

	}
	public function submitAnswer(){

		$result = $this->ApplicantModel->isFinished($this->session->userdata('applicantId'),$this->session->userdata('positionId'));
		if($result != NULL){
			echo 'Stop';
		}else{
			$questions = $this->ApplicantModel->getQuestions($this->session->userdata('examset'));
			$correct = $size = 0;
			$remarks = '';
			foreach($questions as $q){

				$answer = $this->input->post($q->writtenid);

				if(trim($answer)==trim($q->answer)){
						// echo $answer.'<br>';
					$correct=$correct+$q->points;
					$remarks =  $q->writtenid.','.$q->points.'|'.$remarks;
				}else{
					$remarks =  $q->writtenid.',0|'.$remarks;
				}
				$size = $size + $q->points;
			}

			echo $size.'|';
			echo $correct.'|';
			$result = $correct/$size;
			if($result>0.7){
				$r = 'Passed';
			}else{
				$r = 'Failed';
			}
			echo $r;
			$insertdata = array(
				'pos_id' => $this->session->userdata('positionId'),
				'test_info' => 'Written',
				'count' => '',
				'time_count' => '',
				'times' => '',
				'result' => $r,
				'remarks' => $remarks,
				'date_taken' => DATE('Y-m-d'),
				'interviewer' => '',
				'uid' => 2,
				'apid' =>$this->session->userdata('applicantId')
				);
			$this->ApplicantModel->insertTest($insertdata);
		}
		
	}

	public function endExam(){
		$this->session->unset_userdata('applicantId');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('positionApplied');
		$this->session->unset_userdata('endtime');
		$this->session->unset_userdata('examset');
		$this->session->sess_destroy();
		redirect('index.php/Applicant/login');
	}
	public function isAlreadyFinished(){
		$result = $this->ApplicantModel->isFinished($this->session->userdata('applicantId'),$this->session->userdata('positionId'));
		if($result == NULL){
			echo 'Continue';
		}else{
			echo 'Stop';
		}
	}
}