<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends CI_Controller {

  public function __construct(){
        parent::__construct();
			}
	
	public function index($pid=null){
				// $query = $this->db->query("select b.apid,fname,lname,emp_id from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes'");
				// $query = $this->db->query("select distinct(b.emp_id),b.apid,fname,lname,pos_name from tbl_applicant a,tbl_employee b,tbl_position d  where a.apid=b.apid  and a.pos_id = d.pos_id and b.isActive='yes' and d.class='Agent' and b.isActive='yes' order by lname");
				// $query = $this->db->query("select b.emp_id,b.apid,fname,lname,pos_name from tbl_applicant a,tbl_employee b,tbl_position d  where a.apid=b.apid  and a.pos_id = d.pos_id and b.isActive='yes' and d.class='Agent' and b.isActive='yes' order by lname");
				  // $query = $this->db->query("select b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and d.isActive=1 and status='Probationary' or status='regular' order by lname ");
				 $query = $this->db->query("select b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id  and d.isActive=1 and (g.status='Probationary' or g.status='Regular') and e.class='agent' order by lname");
		foreach ($query->result() as $row)
			{
				$arr['employee'][$row->apid] = array(
				"apid" => $row->apid,
 				"fname" => $row->fname,
 				"lname" => $row->lname,
 				"emp_id" => $row->emp_id,
  				);
			}
	if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='attendance' and is_assign=1 order by lname ");
			foreach ($qry->result() as $row)
			{
					$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
				if($pid>0){
						$per_id =  $this->db->query("select coverage_id,daterange,month from tbl_payroll_coverage where coverage_id=".$pid."");
					foreach ($per_id->result() as $row)
					{
							$arr['period'] = array(
							"daterange" => $row->daterange,
							"coverage_id" => $row->coverage_id,
							"month" => $row->month,
							
							);
					}
					$this->load->view('attendance',$arr);
				}else{
					
				}
			}else{
				redirect('index.php/home/error');
			}
		}else{
					redirect('index.php/login');
		}
	
	
		}
		public function emptype(){
		 $payrollC = $this->input->post('payrollC');
		 $employz = $this->input->post('employz');
		  $date = explode("-",$payrollC);
			$HolidateSEnd = array(
			 '2:00:00 PM' => '0',
			 '3:00:00 PM' => '0',
			 '4:00:00 PM' => '1',
			 '5:00:00 PM' => '2',
			 '6:00:00 PM' => '2.75',
			 '7:00:00 PM' => '3.25',
			 '8:00:00 PM' => '4.25',
			 '9:00:00 PM' => '5.25',
			 '9:30:00 PM' => '5.5',
			 '10:00:00 PM' => '6',
			 '10:15:00 PM' => '6.25',
			 '10:30:00 PM' => '6.5',
			 '11:00:00 PM' => '7',
			 '12:00:00 AM' => '0',
			 '1:00:00 AM' => '0',
			 '2:00:00 AM' => '0',
			 '3:00:00 AM' => '0',
			 '4:00:00 AM' => '0',
			 '5:00:00 AM' => '0',
			 '6:00:00 AM' => '0',
			 '6:30:00 AM' => '0',
			 '7:00:00 AM' => '0',
			 '8:00:00 AM' => '0',
			 '9:00:00 AM' => '0',
			 '9:30:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
		  );
		  $HolidateStart = array(
			 '2:00:00 PM' => '8',
			 '3:00:00 PM' => '8',
			 '4:00:00 PM' => '7',
			 '5:00:00 PM' => '6',
			 '6:00:00 PM' => '5.25',
			 '7:00:00 PM' => '4.75',
			 '8:00:00 PM' => '3.75',
			 '9:00:00 PM' => '2.75',
			 '9:30:00 PM' => '2.5',
			 '10:00:00 PM' => '2',
			 '10:15:00 PM' => '1.75',
			 '10:30:00 PM' => '1.5',
			 '11:00:00 PM' => '1',
			 '12:00:00 AM' => '8',
			 '01:00:00 AM' => '8',
			 '02:00:00 AM' => '8',
			 '03:00:00 AM' => '8',
			 '04:00:00 AM' => '8',
			 '05:00:00 AM' => '8',
			 '06:00:00 AM' => '8',
			 '07:00:00 AM' => '8',
			 '08:00:00 AM' => '8',
			 '09:00:00 AM' => '8',
			 '09:30:00 AM' => '8',
			 '10:00:00 AM' => '8',
			 '11:00:00 AM' => '8',
			 '12:00:00 PM' => '8',
			 '01:00:00 PM' => '8',
		  );
		   $NDHolidateStart= array(
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
			 '2:00:00 PM' => '1',
			 '3:00:00 PM' => '2',
			 '4:00:00 PM' => '1.75',
			 '5:00:00 PM' => '1.75',
			 '6:00:00 PM' => '1.5',
			 '7:00:00 PM' => '1.5',
			 '8:00:00 PM' => '1.75',
			 '9:00:00 PM' => '1.75',
			 '9:30:00 PM' => '1.75',
			 '10:00:00 PM' => '2',
			 '10:15:00 PM' => '1.75',
			 '10:30:00 PM' => '1.5',
			 '11:00:00 PM' => '1',
			 '12:00:00 AM' => '0',
			 '1:00:00 AM' => '0',
			 '2:00:00 AM' => '0',
			 '3:00:00 AM' => '0',
			 '4:00:00 AM' => '0',
			 '5:00:00 AM' => '0',
			 '6:00:00 AM' => '0',
			 '6:30:00 AM' => '0',
			 '7:00:00 AM' => '0',
			 '8:00:00 AM' => '0',
			 '9:00:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
			
		  );
		  	$NDHolidateEnd= array(
			 '12:00:00 PM' => '0',
			 '1:00:00 PM' => '0',
			 '2:00:00 PM' => '0',
			 '3:00:00 PM' => '0',
			 '4:00:00 PM' => '1',
			 '5:00:00 PM' => '2',
			 '6:00:00 PM' => '2.75',
			 '7:00:00 PM' => '3.75',
			 '8:00:00 PM' => '4.25',
			 '9:00:00 PM' => '5.25',
			 '9:30:00 PM' => '5.25',
			 '10:00:00 PM' => '5',
			 '10:15:00 PM' => '5',
			 '10:30:00 PM' => '5',
			 '11:00:00 PM' => '5',
			 '12:00:00 AM' => '5.25',
			 '1:00:00 AM' => '4.25',
			 '2:00:00 AM' => '3.75',
			 '3:00:00 AM' => '2.75',
			 '4:00:00 AM' => '2',
			 '5:00:00 AM' => '0',
			 '6:00:00 AM' => '0',
			 '6:30:00 AM' => '0',
			 '7:00:00 AM' => '0',
			 '8:00:00 AM' => '0',
			 '9:00:00 AM' => '0',
			 '10:00:00 AM' => '0',
			 '11:00:00 AM' => '0',
		  );
		$query = $this->db->query("select b.apid,fname,lname,emp_id from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes' and b.emp_id in (".$employz.")");
			foreach ($query->result() as $row)
			{
				$DateRangeShift = $this->DateRangeShift($date);
				
					foreach($DateRangeShift as $row1 => $data){
 							$empLogs2 = $this->emplogs($data,$row->emp_id);
							 foreach($empLogs2 as $row2 => $data2){	
									$emplogsOut2 = $this->emplogsOut($data2->dtr_id,$row->emp_id);
									$emplogsShift2 = $this->emplogsShift($data2->acc_time_id,$row->emp_id);
									$empRate2 = $this->empRate($row->emp_id);
									$SSSCompute2 = $this->SSSCompute($row->emp_id);
									$PhilHealthCompute2 = $this->PhilHealthCompute($row->emp_id);
									// $isLoginHoliday2 = $this->isLoginHoliday($data2->login);
									
									// $NightDiff2 = $this->NightDiff($data4->time_start);
									foreach($emplogsOut2 as $row3 => $data3){	
										foreach($emplogsShift2 as $row4 => $data4){
												   $shiftFrom = date("H:i:s", strtotime($data4->time_start));
													 
													$NDbegin = "10:10:00 PM";
													$NDend   = "6:00:00 AM";
													
													$HPbegin = "12:00:00 AM";
													$HPend   = "3:00:00 AM";
													
													$HPbegin2 = "2:00:00 PM";
													$HPend2   = "11:59:59 PM";
													
													$HPbegin3 = "4:00:00 AM";
													$HPend3   = "5:00:00 AM";
													 
													$NDdate1 = DateTime::createFromFormat('H:i a', $data4->time_start);
													$NDdate2 = DateTime::createFromFormat('H:i a', $NDbegin);
													$NDdate3 = DateTime::createFromFormat('H:i a', $NDend);

													$HPdate1 = DateTime::createFromFormat('H:i a', $data4->time_start);
													$HPdate2 = DateTime::createFromFormat('H:i a', $HPbegin);
													$HPdate3 = DateTime::createFromFormat('H:i a', $HPend);
													
													$HPdate4 = DateTime::createFromFormat('H:i a', $HPbegin2);
													$HPdate5 = DateTime::createFromFormat('H:i a', $HPend2);
													
													$HPdate6 = DateTime::createFromFormat('H:i a', $HPbegin3);
													$HPdate7 = DateTime::createFromFormat('H:i a', $HPend3);
													if ($NDdate1 >= $NDdate2 or $NDdate1 <= $NDdate3){
													   $ndRemark = "Y";
													}else{
													   $ndRemark= "N";
													}
													/* if (($HPdate1 >= $HPdate2) and ($HPdate1 <= $HPdate3)){
													   $hpRemark = "Y";
													   $hpRate = '0.10';
													}else{
													   $hpRemark= "N";
													   $hpRate = '';
													} */
													if (($HPdate1 >= $HPdate4 and $HPdate1 <= $HPdate5) or ($HPdate1 >= $HPdate6 and $HPdate1 <= $HPdate7) ){
																$hpRemark = "Y";
																$hpRate = '0.08';
													}else if(($HPdate1 >= $HPdate2) and ($HPdate1 <= $HPdate3)){
															   $hpRemark = "Y";
																$hpRate = '0.10';
													}else{
																$hpRemark= "N";
																 $hpRate = '';	
													}
													 
 												$to_time = strtotime($data2->login);
												$BCTSched = date('H:i:s', strtotime('-10 mins', strtotime($data4->time_start)));
												
 											 if($data4->time_start == "12:00:00 AM"){
												 if(date('h:i:s a', strtotime($data2->login))>date('h:i:s a', strtotime("12:00:00 am"))){
														 $data= date('Y-m-d', strtotime('-1 day', strtotime($data)));
														 $from_time = strtotime($data." 24:00:00");
														$res = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
												 }else{
														 
														$from_time = strtotime($data." 24:00:00");
														$res = round(abs($from_time - $to_time) / 60,2)." mins. early";
												 }												 
											 }else{
												  if(strtotime(date('h:i:s a', strtotime($data2->login)))>strtotime(date('h:i:s a', strtotime($data4->time_start)))){
													$from_time = strtotime($data." ".$shiftFrom);
														$res = "<span style='color:red;'>".(round(abs($from_time - $to_time) / 60,2))." mins. late</span>";
												  }else{
														$from_time = strtotime($data." ".$shiftFrom);
														$res = round(abs($from_time - $to_time) / 60,2)." mins. early";
												  }
											 }
												$bct = ($data." ".$BCTSched.":00" > $data2->login ) ? "Q" : "<span style='color:red'>DQ</span>";
												$from_timeLog = strtotime($data2->login);
 												$to_timeLog = strtotime($data3->logout);
												$resLog = round(abs(($to_timeLog-$from_timeLog)/3600),2);
												

												foreach($empRate2 as $row5 => $data5){
													foreach($SSSCompute2 as $row6 => $data6){
														foreach($PhilHealthCompute2 as $row7 => $data7){
														 $isLogoutHoliday2 = $this->isLogoutHoliday($data3->logout,$data4->time_end);
														 $isLoginHoliday2 = $this->isLoginHoliday($data2->login,$data4->time_start);
		  $TotalAmountHoliday1=0;
		  $TotalAmountHoliday2=0;	
		  $NDComputeHolidayLogout=0;	
		  $HPComputeHolidayLogout=0;	
		  $NDComputeHolidayLogin=0;	
		  $HPComputeHolidayLogin=0;	
		  $HolidayType1 =0;
		  $HolidayType2=0;
				  if(count($isLogoutHoliday2 )>0){
					  
		$HolidayType = ($isLogoutHoliday2[0]->type=='Regular') ? 1 : .30;
		if(array_key_exists($data4->time_start,$HolidateSEnd)){
			$isLogoutHoliday1  =  $HolidateSEnd[$data4->time_start];
			$isLogoutHolidayND1  =  $NDHolidateEnd[$data4->time_start];
			$TotalAmountHoliday1 = (((($data5->rate*12)/261)/8)*$HolidayType)*$isLogoutHoliday1;  
			$NDComputeHolidayLogout = ($isLogoutHoliday2[0]->type=='Regular') ? $isLogoutHolidayND1 : ($isLogoutHolidayND1 *.30);
			$HPComputeHolidayLogout = ($isLogoutHoliday2[0]->type=='Regular') ? $isLogoutHolidayND1 : ($isLogoutHolidayND1 *.30);
			$HolidayType2 =  $isLogoutHoliday2[0]->type;
 		}else{
			$isLogoutHoliday1  =  0 ;
			
		}

	  }else{
		 $isLogoutHoliday1  =  0;
		  
	  }
	  if(count($isLoginHoliday2 )>0){
		  $HolidayType = ($isLoginHoliday2[0]->type=='Regular') ? 1 : .30;
		if(array_key_exists($data4->time_start,$HolidateStart)){
			$isLoginHoliday1  =  $HolidateStart[$data4->time_start];
			$isLoginHolidayND1  =  $NDHolidateStart[$data4->time_start];

	$TotalAmountHoliday2 = (((($data5->rate*12)/261)/8)*$HolidayType)*$isLoginHoliday1;  
	$NDComputeHolidayLogin = ($isLoginHoliday2[0]->type=='Regular') ? $isLoginHolidayND1  : ($isLoginHolidayND1 *.30);
	$HPComputeHolidayLogin = ($isLoginHoliday2[0]->type=='Regular') ? $isLoginHolidayND1  : ($isLoginHolidayND1 * .30);
	$HolidayType1 =  $isLoginHoliday2[0]->type;
		}else{
			$isLoginHoliday1  =  0 ;
		}
	  }else{
		 $isLoginHoliday1  =  0;
	  }
				
 													$arr['employee'][$row->emp_id][$data] = array(
														"fname" => $row->lname.", ".$row->fname,
														"date" => $data,
														"shiftStart" => $data4->time_start,
														"shiftEnd" => $data4->time_end,
														"login" => date('Y-m-d h:i:s a', strtotime($data2->login)),
														"nd" => $data4->nd,
														"logout" =>  date('Y-m-d h:i:s a', strtotime($data3->logout)),
														"late" =>  $res ,
														"bct" =>   $bct,
														"total" =>   $resLog,
														"rate" =>   $data5->rate,
														"pos_name" =>   $data5->pos_name,
														"ndRemark" =>   $ndRemark,
														"hpRemark" =>   $hpRemark,
														"hpRate" =>   $hpRate,
														"SSS" =>   $data6->sss_employee,
														"PhilHealthCompute" =>   $data7->employeeshare,
														"isHoliday" =>    $isLogoutHoliday1+ $isLoginHoliday1,
														 "isLoginHoliday" =>    $isLoginHoliday1,
														 "isLogoutHoliday" =>    $isLogoutHoliday1,
														"TotalAmountHoliday" =>   number_format($TotalAmountHoliday1+$TotalAmountHoliday2,2),
														// "TotalAmountHoliday2Login" =>   number_format(,2),
														  "HPComputeHolidayLogin" =>   $HPComputeHolidayLogin,
														  "NDComputeHolidayLogin" =>   $NDComputeHolidayLogin,
														  "NDComputeHolidayLogout" =>   $NDComputeHolidayLogout,
														  "HPComputeHolidayLogout" =>   $HPComputeHolidayLogout,
														  "HolidayType1" =>   $HolidayType1,
														  "HolidayType2" =>   $HolidayType2,
  													);	  
												 
													}	
												}
											}
										}
									}
								}
						}
				}
			    echo json_encode($arr);
 		}
		public function emptyperesult(){
			$Emptype = $this->input->post('Emptype');
			
		$query = $this->db->query("select b.apid,fname,lname,b.emp_id,pos_name,e.class from tbl_applicant a,tbl_employee b,tbl_emp_promote c,tbl_pos_emp_stat d,tbl_position e where a.apid=b.apid and b.emp_id=c.emp_id and d.posempstat_id=c.posempstat_id and d.pos_id=e.pos_id and b.isActive='yes' and c.isActive=1 and e.class='".$Emptype."'");
		foreach ($query->result() as $row)
			{
				$arr['emp'][$row->emp_id] = array(
					"fname" => $row->lname.", ".$row->fname,
					"emp_id" => $row->emp_id 
					
				);
			}
		
			    echo json_encode($arr);

		}
		 public function DateRangeShift($date){
		$strDateFrom = date("Y-m-d", strtotime($date[0]));
		$strDateTo = date("Y-m-d", strtotime($date[1]));
		$aryRange=array();
		$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
			if($iDateTo>=$iDateFrom){
				array_push($aryRange,date('Y-m-d',$iDateFrom));
				 while($iDateFrom<$iDateTo){
					$iDateFrom+=86400; // add 24 hours
						array_push($aryRange,date('Y-m-d',$iDateFrom));
					}
			}
			 return $aryRange;
			 		
		}
		 
		  public function emplogs($date,$empid){
		// public function emplogs(){
				// $date = '2017-04-05';
				// $empid = 350;
			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and log like '%".$date."%' and emp_id=".$empid."");
				
			  return $query->result();
			// echo json_encode($query->result());
 			 
 		}
		public function emplogsOut($dtrid,$empid){
 
			$query = $this->db->query("select dtr_id,emp_id,log as logout from tbl_dtr_logs where entry='O' and type='DTR' and note=".$dtrid." and emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function emplogsShift($acc_time_id,$empid){
 
			// $query = $this->db->query("select time_start,time_end from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.acc_time_id=".$acc_time_id." and emp_id=".$empid."");
			$query = $this->db->query("select distinct(c.time_id),time_start,time_end,(select group_concat(concat(hour,'-',minutes))  from tbl_payroll_nd as s where s.time_id=c.time_id) as nd from tbl_dtr_logs a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and a.acc_time_id=".$acc_time_id."  and emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function empRate($empid){
 
			$query = $this->db->query("select b.apid,b.emp_id,fname,lname,pos_name,g.status,rate from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g  where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 and b.emp_id=".$empid."");
				
			 return $query->result();
 			 
 		}
		public function NightDiff($nd){
 			$query = $this->db->query("select time_start,time_end,hour,minutes from tbl_payroll_nd a,tbl_time b where a.time_id=b.time_id and a.isActive=1 and b.time_start ='".$nd."'");
				
			 return $query->result();
  		}
		public function SSSCompute($empid){
 			// $query = $this->db->query("select sss_employee from (select rate from tbl_rate a,tbl_position b,tbl_employee c,tbl_applicant d where a.pos_id=b.pos_id and c.apid=d.apid and b.pos_id =d.pos_id and a.isActive=1 and c.isActive='yes' and c.emp_id=".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
 			  $query = $this->db->query("select sss_employee from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_sss as b,tbl_sss_effectivity c where b.sss_effec_id = c.sss_effec_id and rate<=max_range and rate>=min_range and c.isActive=1");
				
			 return $query->result();
  		}
		public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare from (select pos_name,rate,e.emp_id from tbl_rate a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_emp_promote e where a.posempstat_id=b.posempstat_id and c.pos_id=b.pos_id and d.emp_id=e.emp_id and b.posempstat_id=e.posempstat_id and b.pos_id = c.pos_id and a.isActive=1  and e.isActive=1 and e.emp_id  =".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		}
		public function isLoginHoliday($login,$timeStart){
			$year = date_format(date_create($login),"Y");
			  $date = date_format(date_create($login),"Y-m-d");
			  $timeStart = date_format(date_create($timeStart),"h:i:s");
 			
	$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeStart'),concat(date,' ','00:00:00')),'%h:%i') as sec,type from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type from tbl_holiday) as a  where '".$login."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");

			return $query->result();
  		}
		public function isLogoutHoliday($logout,$timeEnd){
			  $year = date_format(date_create($logout),"Y");
			  $date = date_format(date_create($logout),"Y-m-d");
			  $timeEnd = date_format(date_create($timeEnd),"h:i:s");
 			
	$query = $this->db->query("select date,time_format(timediff(concat('$date',' ','$timeEnd'),concat(date,' ','00:00:00')),'%h:%i') as sec,type from (select  DATE_FORMAT(date,'".$year."-%m-%d') as date,type from tbl_holiday) as a  where '".$logout."' between concat(date,' ','00:00:00') and concat(date,' ','23:59:59')");

			return $query->result();
  		}
		 
		/* public function PhilHealthCompute($empid){
 			$query = $this->db->query("select employeeshare from (select rate from tbl_rate a,tbl_position b,tbl_employee c,tbl_applicant d where a.pos_id=b.pos_id and c.apid=d.apid and b.pos_id =d.pos_id and a.isActive=1 and c.isActive='yes' and c.emp_id=".$empid.") as a,tbl_philhealth as b,tbl_philhealth_effectivity c where b.p_effec_id = c.p_effec_id and rate<=maxrange and rate>=minrange and c.isActive=1");
			 return $query->result();
  		} */
	
 
}