<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Raffle extends General
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *     - or -
     *         http://example.com/index.php/welcome/index
     *     - or -
     */
    public function wheel_fortune(){
        if($this->check_access()){
            $this->load->view('templates/games/wheel_fortune/index');
        }        
    }
   

    // CREATE ---------------------------------------------------------------------------------

    // public function insert_properties(){
    //     $prizes = ["PHP 50","PHP 100","SWEET LEAF MILK TEA","JUNK FOOD","PHP 50","STARBUCKS","EAT ALL YOU CAN","JUNK FOOD","PHP 100","STARBUCKS","LOAD WORTH 50","SWEET LEAF MILK TEA","JUNK FOOD","SZ SHIRT","PHP 50","GIFT CERTIFICATE","SWEET LEAF MILK TEA","PHP 100","LOAD WORTH 50","STARBUCKS","PHP 100","EAT ALL YOU CAN","PHP 50","JUNK FOOD","PHP 50","LOAD WORTH 50","SZ SHIRT","LOAD WORTH 50","PHP 100","JUNK FOOD","SZ SHIRT","STARBUCKS","LOAD WORTH 50","JACKPOT"];

    //     $percent = [ 6, 3, 4, 4, 6, 4, 2, 4, 4, 1, 5, 3, 3, 1, 4, 1, 4, 3, 2, 1, 4, 1, 4, 4, 2, 2, 2, 2, 3, 4, 1, 3, 2, 1];

    //     $colors = ["#00B0F0","#000000","#8497B0","#FF7A0D","#00B0F0","#000000","#0D7D58","#000000","#FF7A0D","#FF0000","#000000","#00B0F0","#0D7D58","#FF0000","#000000","#00B0F0","#FF7A0D","#000000","#0D7D58","#FF0000","#00B0F0","#0D7D58","#000000","#FF7A0D","#0D7D58","#000000","#FF0000","#00B0F0","#FF7A0D","#000000","#8497B0","#0D7D58","#FF7A0D","#FF0000"];

    //     for($loop = 0; $loop < count($prizes); $loop++){
    //         $properties[$loop] = [
    //             'prize' => $prizes[$loop],
    //             'degrees' => $percent[$loop],
    //             'color' => $colors[$loop]
    //         ];
    //     }

    //     $this->general_model->batch_insert($properties, 'tbl_wheel_prize');

    // }

    
    // READ -----------------------------------------------------------------------------------
    public function get_sz_wheel_properties(){
        $fields = "prize as text, percent, color as fillStyle";
        $where = "wheelPrize_ID IS NOT NULL";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_wheel_prize");
        echo json_encode($record);
    }
}
