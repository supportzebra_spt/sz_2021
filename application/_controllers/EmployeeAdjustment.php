<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeAdjustment extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	public function index(){ 
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='EmployeeAdjustment' and is_assign=1");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
				
				
				$arr['employee'] = $this->db->query("select b.emp_id,a.lname,a.fname,e.pos_id,g.status,pos_name,pos_details from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_emp_stat g where g.empstat_id = c.empstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1  and c.isActive=1")->result();
				$this->load->view('employeeadjustment',$arr);
				
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}
		

	}
	public function emp($empid=null){
		if($this->session->userdata('uid')){
			$qry =  $this->db->query("select b.user_id,settings from tbl_menu_items a,tbl_user_access b,tbl_user c where a.menu_item_id=b.menu_item_id and c.uid = b.user_id and b.user_id=".$this->session->userdata('uid')." and item_link='Payroll/' and is_assign=1 ");
			foreach ($qry->result() as $row)
			{
				$arr['setting'] = array("settings" => $row->settings);
			}
			if ($qry->num_rows() > 0){
				
				
				if($empid>0){
					$per_id =  $this->db->query("select coverage_id,daterange from tbl_payroll_coverage where coverage_id=".$empid."");
					foreach ($per_id->result() as $row)
					{
						$arr['period'] = array(
							"daterange" => $row->daterange,
							"coverage_id" => $row->coverage_id,
							
							);
					}
					$arr['types'] = $this->db->query("SELECT pdeduct_id,deductionname,description FROM tbl_payroll_deductions WHERE pdeduct_id NOT IN(SELECT pdeduct_id FROM tbl_payroll_emp_adjustment WHERE emp_id=".$empid." AND isActive=1) AND isActive=1")->result();
					$values = $this->db->query("SELECT a.isActive,b.deductionname,DATE_FORMAT(adj_date,'%M %d, %Y') as adj_date,emp_id,a.pdeduct_id,max_amount,adj_amount,count,remarks,pemp_adjust_id FROM tbl_payroll_emp_adjustment as a,tbl_payroll_deductions as b WHERE a.pdeduct_id=b.pdeduct_id AND emp_id='".$empid."'")->result();
					for($x = 0;$x<count($values);$x++){
						$result = $this->db->query("SELECT pemp_adjust_id FROM tbl_payroll WHERE pemp_adjust_id='".$values[$x]->pemp_adjust_id."'")->result();
						$rows = count($result);
						// echo $rows;
						if($rows>0){
							$values[$x]->updateable = 'no';
						}else{

							$values[$x]->updateable = 'yes';
						}
					}
					// var_dump($values);
					$arr['values'] = $values;
					$arr['emp_id'] = $empid;
					$arr['details'] = $this->db->query("select a.pic,a.lname,a.fname,e.pos_id,g.status,pos_name,pos_details from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_emp_stat g where g.empstat_id = c.empstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1  and c.isActive=1 and b.emp_id=".$empid)->row();
					// var_dump($arr['details']);
					$this->load->view('employeeadjustment2',$arr);
				}else{

				}
			}else{
				redirect('index.php/home/error');
			}
		}else{
			redirect('index.php/login');
		}

	}
	public function newAdjustment(){
		$emp_id = $this->input->post('emp_id');
		$pdeduct_id = $this->input->post('pdeduct_id');
		$max_amount = $this->input->post('max_amount');
		$adj_amount = $this->input->post('adj_amount');
		$adj_date = $this->input->post('adj_date');
		$count = $this->input->post('count');
		$remarks = $this->input->post('remarks');

		$dater = explode('-',$adj_date);
		$adj_date = $dater[2].'-'.$dater[0].'-'.$dater[1];

		$data = array(
			'emp_id' => $emp_id,
			'pdeduct_id' => $pdeduct_id,
			'max_amount' => $max_amount,
			'adj_amount' => $adj_amount,
			'adj_date' => $adj_date,
			'isActive' => 1,
			'remarks' => $remarks,
			'count' => $count

			);
		$this->db->insert('tbl_payroll_emp_adjustment',$data);
		if($this->db->affected_rows()>0){
			echo 'Success';
		}else{
			echo 'Failed';
		}
	}
	public function getAdjustmentFromId(){
		$pemp_adjust_id = $this->input->post('pemp_adjust_id');
		// $pemp_adjust_id = 13;
		$data = $this->db->query("SELECT b.deductionname,DATE_FORMAT(adj_date,'%M %d, %Y') as adj_date,emp_id,a.pdeduct_id,max_amount,adj_amount,count,remarks,pemp_adjust_id FROM tbl_payroll_emp_adjustment as a,tbl_payroll_deductions as b WHERE a.pdeduct_id=b.pdeduct_id AND a.pemp_adjust_id='".$pemp_adjust_id."'")->row();
		if(count($data)>0){
			echo json_encode($data);
		}else{
			echo 'Failed';
		}
	}
	public function updateAdjustment(){
		$max_amount  = $this->input->post("max_amount");
		$adj_amount  = $this->input->post("adj_amount");
		$count  = $this->input->post("count");
		$remarks  = $this->input->post("remarks");
		$adjustid  = $this->input->post("adjustid");
		$data = array(
			'max_amount' => $max_amount,
			'adj_amount' => $adj_amount,
			'count' => $count,
			'remarks' => $remarks
			);
		$this->db->where('pemp_adjust_id',$adjustid);
		$this->db->update('tbl_payroll_emp_adjustment',$data);
		if($this->db->affected_rows()>0){
			echo 'Success';
		}else{
			echo 'Failed';
		}
	}

	public function getActiveAdjustments(){
		$emp_id = $this->input->post('emp_id');
		$values = $this->db->query("SELECT a.isActive,b.deductionname,DATE_FORMAT(adj_date,'%M %d, %Y') as adj_date,emp_id,a.pdeduct_id,max_amount,adj_amount,count,remarks,pemp_adjust_id FROM tbl_payroll_emp_adjustment as a,tbl_payroll_deductions as b WHERE a.pdeduct_id=b.pdeduct_id AND emp_id='".$emp_id."' AND a.isActive=1")->result();
		echo json_encode($values);
	}

	public function updateActiveness(){
		$pemp_adjust_id = $this->input->post('pemp_adjust_id');
		$pdeduct_id = $this->input->post('pdeduct_id');
		$activevalue = $this->input->post('activevalue');
		$emp_id = $this->input->post('emp_id');
		// echo $pemp_adjust_id.' - '.$pdeduct_id.' - '.$activevalue.' - '.$emp_id;
		$query = $this->db->query("SELECT pemp_adjust_id FROM tbl_payroll_emp_adjustment WHERE isActive=1 AND pdeduct_id=".$pdeduct_id." AND emp_id=".$emp_id)->row();
		if($query!=''||$query!=NULL){
			if($activevalue==0){
				$this->db->where('pemp_adjust_id',$pemp_adjust_id);
				$this->db->update('tbl_payroll_emp_adjustment',array('isActive'=>$activevalue));
				if($this->db->affected_rows()>0){
					echo 'Success';
				}else{
					echo 'Failed';
				}
			}else{
				echo 'Exists';
			}
		}else{
			$this->db->where('pemp_adjust_id',$pemp_adjust_id);
			$this->db->update('tbl_payroll_emp_adjustment',array('isActive'=>$activevalue));
			if($this->db->affected_rows()>0){
				echo 'Success';
			}else{
				echo 'Failed';
			}
		}
		// echo $this->db->last_query();
	}
}