<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once dirname(__FILE__) . "/General.php";

class Szfive extends General {

    protected $title = 'SnapSZ';
    protected $count_users = [];
    protected $userdata = [];
    protected $account_id = [];

    #================================
    # PUBLIC METHODS
    #================================

    public function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        $this->_get_userdata();
        $this->load->model('szfive_model');

    }

    /**
     * @return SZFive main index page
     */

    private function getEmpStat($emp_id){
       return $this->general_model->fetch_specific_val("c.status as emp_stat","a.emp_id = $emp_id AND a.posempstat_id = b.posempstat_id AND b.empstat_id = c.empstat_id","tbl_emp_promote a,tbl_pos_emp_stat b,tbl_emp_stat c")->emp_stat; 
   }
   private function getSupervisor($emp_id){
       return $this->general_model->fetch_specific_val("a.fname,a.lname,a.email","a.apid=b.apid AND b.emp_id = $emp_id","tbl_applicant a,tbl_employee b"); 
   }
   public function index()
   {
        // Get query for collecting the high five data
    $sql = $this->szfive_model->get_highfive();

        // Get the five most recently received High Fives
    $received_highfives = $this->general_model->custom_query($sql['query'] . " LEFT JOIN tbl_szfive_high_five_details as k ON k.id = a.id WHERE k.recipientId = " . $this->userdata['uid'] . " ORDER BY a.id DESC LIMIT 0, 5");
    $data['received_highfives'] = [];

    if (!empty($received_highfives))
    {
        foreach ($received_highfives as $highfive)
        {
            $highfive = $this->_get_mentioned_users($highfive);
            $highfive->totalComment = $this->general_model->fetch_specific_val('count(*) as count', ['parentId' => $highfive->hiFiveId, 'type' => 'sz_highfive', 'isPublic' => 1], 'tbl_szfive_comments');

            array_push($data['received_highfives'], $highfive);
        }
    }

        // Get user details
    $user_details = $this->general_model->custom_query($this->szfive_model->get_user_details(['uid' => $this->userdata['uid']]));

    $data['user_details'] = $user_details[0];
    $data['uri_segment'] = $this->uri->segment_array();
    $data['title'] = $this->title . ' - Home';
    $data['nav_list'] = $this->_navigation_bar_list('Home');

    $this->load_template_view('templates/szfive/index', $data);
}

    /**
     * @return SZFive daily survey index page
     */
    public function daily_survey()
    {
        $has_access = $this->_check_menu_access('szfive/daily_survey');

        if ($has_access == false)
        {
            $this->load->view('errors/custom/error_403');
        }
        else
        {
            $survey_query = [
                'fields' => 'a.*, b.*',
                'table' => 'tbl_szfive_survey_details as a',
                'join' => [
                    'tbl_szfive_survey_questions as b' => 'b.questionId = a.questionId',
                ],
            ];

            $data = [
                'uri_segment' => $this->uri->segment_array(),
                'title' => $this->title . ' - Daily Survey',
                'surveys' => $this->general_model->join_select($survey_query),
                'emp_statuses' => $this->general_model->fetch_all('*', 'tbl_emp_stat', 'status'),
                'accounts' => $this->general_model->fetch_all('*', 'tbl_account', 'acc_name'),
                'choices' => $this->general_model->fetch_all('*', 'tbl_szfive_survey_choices', null),
                'permissions' => $this->general_model->fetch_specific_val('settings', ['role_id' => $this->userdata['role_id']], 'tbl_szfive_survey_permissions'),
                'this_week' => $this->_get_this_week_date_range(),
                'nav_list' => $this->_navigation_bar_list('Daily Survey'),
            ];

            $this->load_template_view('templates/szfive/survey/index', $data);
        }
    }

    /**
     * @return SZFive high fives index page
     */
    public function high_fives()
    {
        $role_id = $this->session->userdata('role_id');
        $uid = $this->session->userdata('uid');
        $img = base_url() . "assets/images/";
        $highfives = [];

        // Get current user's high five stats
        $user_stat = $this->general_model->fetch_specific_val('count(*) as count', ['senderId' => $this->userdata['uid']], 'tbl_szfive_high_fives');
        $user_receive_count = $this->general_model->fetch_specific_val('count(*) as count', ['recipientId' => $this->userdata['uid']], 'tbl_szfive_high_five_details');

        $user_stat->sent_title = 'Sent';
        $user_stat->sent_count = $user_stat->count;
        $user_stat->receive_title = 'Received';
        $user_stat->receive_count = $user_receive_count->count;
        $user_stat->username = $this->session->userdata('fname') . ' ' . $this->session->userdata('lname');
        $user_stat->pic = $this->session->userdata('pic');
        $user_stat->uid = $this->session->userdata('uid');
        $user_stat->title = 'Your stats';

        // Get the `most appreciated` employee
        $rdata = $this->szfive_model->get_top_highfive(['img' => $img, 'id' => 'recipientId', 'table' => 'tbl_szfive_high_five_details']);

        if (!empty($rdata))
        {
            $rdata[0]->hi5_user = 'receiver';
            $rdata[0]->title = 'Most Appreciated';
            $highfives[] = $rdata[0];
        }

        // Get the `top high fiver` employee
        $sdata = $this->szfive_model->get_top_highfive(['img' => $img, 'id' => 'senderId', 'table' => 'tbl_szfive_high_fives']);

        if (!empty($sdata))
        {
            $sdata[0]->hi5_user = 'sender';
            $sdata[0]->title = 'Top Appreciator';
            $highfives[] = $sdata[0];
        }

        $all_users = $this->general_model->custom_query("SELECT * FROM tbl_user a,tbl_employee b,tbl_applicant c WHERE b.isActive = 'yes' AND a.emp_id = b.emp_id AND b.apid=c.apid ORDER BY c.fname ASC");

        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => $this->title . ' - High Five',
            'highfives' => $highfives,
            'user_stat' => $user_stat,
            'all_users' => $all_users,
            'nav_list' => $this->_navigation_bar_list('SZhoutOut'),
        ];

        $this->load_template_view('templates/szfive/highfive/index', $data);
    }

    /**
     * @return SZFive reviews index page
     */
    public function reviews()
    {
        $has_access = $this->_check_menu_access('szfive/reviews');

        if ($has_access == false)
        {
            $this->load->view('errors/custom/error_403');
        }
        else
        {

            $query = "SELECT a.*, b.* FROM tbl_szfive_reviews as a JOIN tbl_szfive_survey_answers as b ON b.answerId = a.answerId";

            // Get reviews
            $review_data = $this->general_model->custom_query($query);

            // Get reviews stats
            $review_stats = $this->general_model->custom_query($query . ' WHERE b.isReviewed = 1');

            // Get user details
            $query = $this->szfive_model->get_user_details(['uid' => $this->userdata['uid']]);
            $user_details = $this->general_model->custom_query($query);

            $all_reviewed = $this->general_model->custom_query("SELECT count(*) as count FROM tbl_szfive_reviews WHERE reviewerId = " . $this->userdata['uid'] . " AND isReviewed = 1");
            $all_unreviewed = $this->general_model->custom_query("SELECT count(*) as count FROM tbl_szfive_reviews WHERE reviewerId = " . $this->userdata['uid'] . " AND isReviewed = 0");
            $total_reviews = $all_reviewed[0]->count + $all_unreviewed[0]->count;

            $review_percentage = round(@($all_reviewed[0]->count / $total_reviews) * 100, 2);

            $data = [
                'uri_segment' => $this->uri->segment_array(),
                'title' => $this->title . ' - Reviews',
                'review_data' => $review_data,
                'review_stats' => $review_stats,
                'departments' => $this->general_model->custom_query("SELECT * FROM tbl_department WHERE (dep_name != 'test')"),
                'this_week' => $this->_get_this_week_date_range(),
                'user_details' => $user_details[0],
                'all_reviewed' => $all_reviewed,
                'all_unreviewed' => $all_unreviewed,
                'review_percentage' => $review_percentage,
                'nav_list' => $this->_navigation_bar_list('Reviews'),
            ];

            if ($this->userdata['role_id'] != 1)
            {
                $this->load_template_view('templates/szfive/reviews/_index_sub', $data);
            }
            else
            {
                $this->load_template_view('templates/szfive/reviews/index', $data);
            }
        }
    }

    /**
     * @return SZFive activity page with public data
     */
    public function activity_public()
    {
        $data = $this->_get_activity_data();

        $this->load_template_view('templates/szfive/highfive/_activity_public', $data);
    }

    /**
     * @return SZFive activity page with private data
     */
    public function activity_private()
    {
        $data = $this->_get_activity_data();
        $this->load_template_view('templates/szfive/highfive/_activity_private', $data);
    }

    private function _get_activity_data()
    {
        $query = $this->szfive_model->get_user_details(['uid' => $this->userdata['uid']]);
        $user_details = $this->general_model->custom_query($query);

        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => $this->title . ' - My SZhoutOuts',
            'user_details' => $user_details[0],
            'nav_list' => $this->_navigation_bar_list('Home'),
        ];

        return $data;
    }

    /**
     * @return SZFive feeling page
     */
    public function feeling()
    {
        $query = "SELECT a.*, b.*, DATE_FORMAT(a.dateCreated, '%M %e, %Y') as date, DATE_FORMAT(a.dateCreated, '%M %e, %Y %r') as dateAnswered, a.dateCreated as dateSubmitted, CONCAT(e.fname, ' ', e.lname) as user_name, b.role as position, c.Supervisor, c.acc_id, c.apid, c.isActive, b.emp_id, d.*, e.pic, g.*, f.emp_id as supervisorId
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_user as b ON b.uid = a.employeeId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
        LEFT JOIN tbl_applicant as e ON e.apid = c.apid
        LEFT JOIN tbl_user as f ON f.emp_id = c.Supervisor
        LEFT JOIN tbl_szfive_survey_details as g ON g.surveyId = a.surveyId
        WHERE b.uid = " . $this->userdata['uid'] . "
        LIMIT 5";

        $answers = $this->general_model->custom_query($query);

        foreach ($answers as $key => $value)
        {
            $value->emp_stat = $this->getEmpStat($value->emp_id);
            $supervisor = $this->getSupervisor($value->emp_id);
            $value->supervisor = $supervisor->lname.', '.$supervisor->fname;
            $value->supEmail = $supervisor->email;
            $value->dateSubmitted = $this->_time_elapsed_string($value->dateSubmitted);
        }

        $query = $this->szfive_model->get_user_details(['uid' => $this->userdata['uid']]);
        $user_details = $this->general_model->custom_query($query);
        $subordinates = $this->_has_subordinates();

        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => $this->title,
            'subtitle' => 'MY ANSWERS',
            'answer_type' => 'user',
            'permissions' => $this->general_model->fetch_specific_val('settings', ['role_id' => $this->userdata['role_id']], 'tbl_szfive_survey_permissions'),
            'answers' => $answers,
            'user_details' => $user_details[0],
            'nav_list' => $this->_navigation_bar_list('Home'),
            'has_subordinates' => $subordinates['has_subordinates'],
        ];

        $this->load_template_view('templates/szfive/survey/_feeling', $data);
    }

    /**
     * @return SZFive team answers page
     */
    public function team_answers()
    {
        $user_details = $this->general_model->custom_query($this->szfive_model->get_user_details(['uid' => $this->userdata['uid']]));
        $subordinates = $this->_has_subordinates();

        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => $this->title,
            'subtitle' => 'TEAM ANSWERS',
            'answer_type' => 'team',
            'permissions' => $this->general_model->fetch_specific_val('settings', ['role_id' => $this->userdata['role_id']], 'tbl_szfive_survey_permissions'),
            'nav_list' => $this->_navigation_bar_list('Home'),
            'user_details' => $user_details[0],
            'has_subordinates' => $subordinates['has_subordinates'],
        ];

        if (strpos($data['permissions']->settings, 'viewTeamAnswers') !== false)
        {
            $this->load_template_view('templates/szfive/survey/_feeling', $data);
        }
    }

    /**
     * @return SZFive reporting page
     */
    public function reporting()
    {
        if (!empty($this->_get_menu_item(['link' => 'szfive/reporting'])))
        {
            $data = [
                'uri_segment' => $this->uri->segment_array(),
                'title' => $this->title . ' - Reporting',
                'nav_list' => $this->_navigation_bar_list('Reporting'),
                'this_week' => $this->_get_this_week_date_range(),
                'accounts' => $this->general_model->custom_query("SELECT acc_id, acc_name FROM tbl_account ORDER BY acc_name ASC"),
            ];
            $this->load_template_view('templates/szfive/reporting/index', $data);
        }
        else
        {
            redirect('szfive');
        }
    }

    /**
     * Get survey data
     * @return array annswers|question|choice labels
     */
    public function get_survey_data()
    {
        $surveyId = $this->input->post('surveyId');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $query['fields'] = 'a.*, b.*';
        $query['table'] = 'tbl_szfive_survey_details as a';
        $query['join'] = ['tbl_szfive_survey_questions as b' => 'b.questionId = a.questionId'];
        $query['where'] = ['a.surveyId' => $surveyId];

        $data['survey_details'] = $this->general_model->join_select($query);

        $sql = "SELECT a.*, b.*, c.*, d.*, e.sched_id, e.sched_date
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_user as b ON b.uid = a.employeeId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_user as d ON d.emp_id = c.Supervisor
        LEFT JOIN tbl_schedule as e ON e.sched_id = a.sched_id
        WHERE e.sched_date >=  '$start_date' AND e.sched_date <= '$end_date' AND a.surveyId = '$surveyId' AND a.sched_id != 0
        ";

        if ($this->userdata['role_id'] != 1)
        {

            $sub_data = $this->_has_subordinates();

            // Include answers of the current user
            if ($sub_data['has_subordinates'] == false)
            {
                $sql .= " AND a.employeeId = " . $this->userdata['uid'];
            }
            else
            {
                //If current user has subordinates, then fetch his' and his subordinates' answers
                $subordinates = $this->_get_subordinates($sub_data['data']);
                $string = "(" . implode(",", $subordinates) . ")";
                $sql .= " AND (b.emp_id IN $string OR a.employeeId = " . $this->userdata['uid'] . ")";
            }
        }

        $data['survey_answers'] = $this->general_model->custom_query($sql);

        if (!empty($data['survey_answers']))
        {
            // Get total survey answers
            $total_anwers = count($data['survey_answers']);
            // Get all questions
            $question = $data['survey_details'][0]->question;
            // Get choice labels
            $choice_labels = json_decode($data['survey_details'][0]->choiceLabels);

            $answers = [];

            foreach ($data['survey_answers'] as $survey)
            {
                array_push($answers, $survey->answer);
            }

            $data = [];
            $sum = 0;

            foreach ($choice_labels as $choice)
            {
                $cdata = $this->general_model->fetch_specific_vals("*", ['choiceId' => $choice], 'tbl_szfive_survey_choices', 'label');

                // Get answer label/title
                $value = array_search($choice, $choice_labels) + 1;

                // Count the occurence of the answer in the array of answers
                $count_answers = count(array_keys($answers, $value));

                $percent = round(@($count_answers / $total_anwers) * 100, 2);
                $sum = $sum + ($count_answers * $value);

                $sdata = [
                    'name' => $cdata[0]->label,
                    'id' => $value,
                    'y' => floatval($percent),
                    'answered' => count(array_keys($answers, $value)),
                ];

                $count[] = $value;

                array_push($data, $sdata);
            }
            $average = round(@$sum / $total_anwers, 2);

            $survey = [
                'data' => $data,
                'total' => $total_anwers,
                'count' => $count,
                'question' => $question,
                'date' => $question,
                'average' => $average,
            ];
        }
        else
        {
            $survey = null;
        }

        echo json_encode(['data' => $survey]);
    }

    public function get_drilldown_survey_answers()
    {
        $post = $this->input->post('params');
        $param = [
            'survey_id' => $post['surveyId'],
            'start_date' => $post['startDate'],
            'end_date' => $post['endDate'],
        ];

        $query = $this->_get_answer_detail_query($param) . ' AND a.answer = ' . $post['surveyAnswer'];

        $answers = $this->general_model->custom_query($query);

        foreach ($answers as $answer)
        {
            $answer_data[] = [
                'name' => $answer->user_name,
                'y' => 1,
            ];
        }

        $index = intval($post['surveyAnswer']);

        $data[$index] = [
            'name' => $index,
            'data' => $answer_data,
        ];
        echo json_encode($data);
    }

    /*
     * Get all survey details (integration of question and choices)
     */

    public function get_surveys()
    {
        $datatable = $this->input->post('datatable');
        $query = $this->szfive_model->datatable_survey_query($datatable);
        $data = $this->set_datatable_query($datatable, $query);

        $newdata = [];
        foreach ($data['data'] as $mdata)
        {
            $mdata->question = ucfirst($mdata->question);
            $mdata->choiceLabels = json_decode($mdata->choiceLabels);
            $c_labels = array();

            foreach ($mdata->choiceLabels as $label)
            {

                $choice_label = $this->general_model->fetch_specific_val('*', ['choiceId' => $label], 'tbl_szfive_survey_choices');
                $c_labels[] = $choice_label->label;
            }

            $mdata->clabels = implode(", ", $c_labels);

            $mdata->permissions = $this->general_model->fetch_specific_val('settings', ['role_id' => $this->userdata['role_id']], 'tbl_szfive_survey_permissions');
            array_push($newdata, $mdata);
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $newdata]);
    }

    /*
     * Get all questions
     */

    public function get_questions()
    {
        $datatable = $this->input->post('datatable');

        $query['query'] = "SELECT * FROM tbl_szfive_survey_questions";

        if (isset($datatable['query']['questionSearch']))
        {
            $keyword = $datatable['query']['questionSearch'];

            $query['search']['append'] = " WHERE question LIKE '%" . $keyword . "%' OR questionId LIKE '%" . $keyword . "%'";

            $query['search']['total'] = " WHERE question LIKE '%" . $keyword . "%' OR questionId LIKE '%" . $keyword . "%'";
        }

        $data = $this->set_datatable_query($datatable, $query);

        $newdata = [];

        foreach ($data['data'] as $mdata)
        {
            $mdata->question = ucfirst($mdata->question);
            $mdata->permissions = $this->general_model->fetch_specific_val('settings', ['role_id' => $this->userdata['role_id']], 'tbl_szfive_survey_permissions');

            array_push($newdata, $mdata);
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $newdata]);
    }

    /*
     * Get all choices
     */

    public function get_choices()
    {
        $datatable = $this->input->post('datatable');

        $query['query'] = "SELECT * FROM tbl_szfive_survey_choices";

        if (isset($datatable['query']['choiceSearch']))
        {
            $keyword = $datatable['query']['choiceSearch'];

            $query['search']['append'] = " WHERE label LIKE '%" . $keyword . "%' OR  choiceId LIKE '%" . $keyword . "%'";

            $query['search']['total'] = " WHERE label LIKE '%" . $keyword . "%' OR  choiceId LIKE '%" . $keyword . "%'";
        }

        $data = $this->set_datatable_query($datatable, $query);

        $newdata = [];

        foreach ($data['data'] as $mdata)
        {
            $mdata->label = ucfirst($mdata->label);
            $mdata->permissions = $this->general_model->fetch_specific_val('settings', ['role_id' => $this->userdata['role_id']], 'tbl_szfive_survey_permissions');
            array_push($newdata, $mdata);
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $newdata]);
    }

    /**
     * Get choices for specific survey
     */
    public function get_specific_choices()
    {
        $survey_id = $this->input->post('surveyId');

        $data = $this->general_model->fetch_specific_vals('*', ['surveyId' => $survey_id], 'tbl_szfive_survey_details', null);

        $choices = json_decode($data[0]->choiceLabels);

        echo json_encode($choices);
    }

    /**
     * Get survey answers for for specified survey question and date
     */
    public function get_answers()
    {
        $datatable = $this->input->post('datatable');
        $surveyId = $datatable['query']['surveyId'];
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];

        $query['query'] = $this->_get_answer_detail_query(['start_date' => $start_date, 'end_date' => $end_date, 'survey_id' => $surveyId]);

        $search_append = '';
        $search_total = '';

        if (isset($datatable['query']['answerSearch']))
        {
            $keyword = ($datatable['query']['answerSearch'] != 'all') ? $datatable['query']['answerSearch'] : '';

            $search_append = $search_append . " AND (a.detailedAnswer LIKE '%" . $keyword . "%' OR  a.answer LIKE '%" . $keyword . "%' OR  b.fname LIKE '%" . $keyword . "%' OR  b.lname LIKE '%" . $keyword . "%' OR  g.choiceLabels LIKE '%" . $keyword . "%' OR c.emp_stat LIKE '%" . $keyword . "%' OR c.emp_stat LIKE '%" . $keyword . "%' OR d.acc_name LIKE '%" . $keyword . "%' OR a.answer LIKE '%" . $keyword . "%') ";

            $search_total = $search_total . " AND (a.detailedAnswer LIKE '%" . $keyword . "%' OR  a.answer LIKE '%" . $keyword . "%' OR  b.fname LIKE '%" . $keyword . "%' OR  b.lname LIKE '%" . $keyword . "%' OR  g.choiceLabels LIKE '%" . $keyword . "%' OR c.emp_stat LIKE '%" . $keyword . "%' OR d.acc_id LIKE '%" . $keyword . "%' OR d.acc_name LIKE '%" . $keyword . "%' OR a.answer LIKE '%" . $keyword . "%') ";

            $query['search']['append'] = $search_append;
            $query['search']['total'] = $search_total;
        }

        if (isset($datatable['query']['employeeStatus']) and isset($datatable['query']['accountName']))
        {
            $status = $datatable['query']['employeeStatus'];
            $account = $datatable['query']['accountName'];

            $search_append = $search_append . " AND (c.emp_stat LIKE '%" . $status . "%' AND d.acc_id LIKE '%" . $account . "%')";
            $search_total = $search_total . " AND (c.emp_stat LIKE '%" . $status . "%' AND d.acc_id LIKE '%" . $account . "%')";

            $query['search']['append'] = $search_append;
            $query['search']['total'] = $search_total;
        }

        $data = $this->set_datatable_query($datatable, $query);

        foreach ($data['data'] as $mdata)
        {

            $labels = json_decode($mdata->choiceLabels);
            $choiceId = $labels[$mdata->answer - 1];
            $choice_label = $this->general_model->fetch_specific_val('label', ['choiceId' => $choiceId], 'tbl_szfive_survey_choices');
            $mdata->c_label = $choice_label->label;
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_answer_details()
    {
        $id = $this->input->post('id');

        $query = "SELECT j.pos_name, j.pos_details, a.answer, a.detailedAnswer, a.dateCreated, e.fname, e.lname, b.role as position, c.isActive, b.emp_id, d.acc_name, e.pic, g.numberOfChoices, g.choiceLabels, k.sched_date, l.dateUpdated, a.isReviewed, m.link
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_user as b ON b.uid = a.employeeId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
        LEFT JOIN tbl_applicant as e ON e.apid = c.apid
        LEFT JOIN tbl_user as f ON f.emp_id = c.Supervisor
        LEFT JOIN tbl_szfive_survey_details as g ON g.surveyId = a.surveyId
        LEFT JOIN tbl_emp_promote as h ON (h.emp_id = c.emp_id AND h.isActive = 1)
        LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = h.posempstat_id)
        LEFT JOIN tbl_position as j ON (j.pos_id = i.pos_id)
        LEFT JOIN tbl_schedule as k ON k.sched_id = a.sched_id
        LEFT JOIN tbl_szfive_reviews as l ON l.answerId = a.answerId
        LEFT JOIN tbl_system_notification as m ON m.systemNotification_ID = l.notificationId
        WHERE a.answerId = $id
        ";

        $data = $this->general_model->custom_query($query);

        foreach ($data as $key => $value)
        {
            $value->emp_stat = $this->getEmpStat($value->emp_id);
            $supervisor = $this->getSupervisor($value->emp_id);
            $value->supervisor = $supervisor->lname.', '.$supervisor->fname;
            $value->supEmail = $supervisor->email;
        }

        $choices = json_decode($data[0]->choiceLabels);
        $choice_data = $this->general_model->fetch_specific_vals("label", ['choiceId' => $choices[$data[0]->answer - 1]], 'tbl_szfive_survey_choices', 'label');
        $data[0]->answer_label = strtolower($choice_data[0]->label);

        echo json_encode($data[0]);
    }

    public function add_survey()
    {
        $data['questionId'] = $this->input->post('question');
        $data['numberOfChoices'] = $this->input->post('numberOfChoices');
        $data['choiceLabels'] = json_encode($this->input->post('choiceLabels'));
        $data['dateCreated'] = date('Y-m-d h:i:s');

        $surveyData = $this->general_model->insert_vals($data, 'tbl_szfive_survey_details');

        echo json_encode(array('status' => true, 'data' => $surveyData));
    }

    public function add_question()
    {
        $data['question'] = $this->input->post('question');

        $question = $this->general_model->fetch_specific_val('*', ['question' => $data['question']], 'tbl_szfive_survey_questions');

        if (empty($question))
        {
            $this->general_model->insert_vals($data, 'tbl_szfive_survey_questions');
            $success = true;
        }
        else
        {
            $success = false;
        }

        echo json_encode($success);
    }

    public function add_choice()
    {
        $data['label'] = $this->input->post('choice');

        $choice = $this->general_model->fetch_specific_val('*', ['label' => $data['label']], 'tbl_szfive_survey_choices');

        if (empty($choice))
        {
            $this->general_model->insert_vals($data, 'tbl_szfive_survey_choices');
            $success = true;
        }
        else
        {
            $success = false;
        }

        echo json_encode($success);
    }

    public function view_survey()
    {
        $data['surveyId'] = $this->input->post('surveyId');

        $query = "(SELECT a.*, b.*
        FROM tbl_szfive_survey_details as a
        JOIN tbl_szfive_survey_questions as b ON b.questionId = a.questionId
        WHERE a.surveyId = '" . $data['surveyId'] . "'
    )";

    $data = $this->general_model->custom_query($query);
    $data[0]->choiceLabels = json_decode($data[0]->choiceLabels);
    foreach ($data[0]->choiceLabels as $label)
    {

        $choice_label = $this->general_model->fetch_specific_val('*', ['choiceId' => $label], 'tbl_szfive_survey_choices');
        $c_labels[] = $choice_label->label;
        $c_label_ids[] = ['choiceLabels' => $choice_label->choiceId];
    }

    $data[0]->clabels = $c_labels;
    $data[0]->c_label_ids = $c_label_ids;

    echo json_encode($data[0]);
}

public function view_question()
{
    $data['questionId'] = $this->input->post('questionId');

    $questionData = $this->general_model->fetch_specific_val('*', $data, 'tbl_szfive_survey_questions', null);

    $questionData->question = ucfirst($questionData->question);

    echo json_encode($questionData);
}

public function view_choice()
{
    $data['choiceId'] = $this->input->post('choiceId');

    $choiceData = $this->general_model->fetch_specific_val('*', $data, 'tbl_szfive_survey_choices', null);
    $choiceData->label = ucfirst($choiceData->label);

    echo json_encode($choiceData);
}

public function edit_survey()
{
    $data['questionId'] = $this->input->post('question');
    $data['numberOfChoices'] = $this->input->post('numberOfChoices');
    $data['choiceLabels'] = json_encode($this->input->post('choiceLabels'));
    $data['dateCreated'] = date('Y-m-d h:i:s');

    $where = "surveyId = " . $this->input->post('surveyId');

    $edit = $this->general_model->update_vals($data, $where, 'tbl_szfive_survey_details');

    if ($edit)
    {
        $success = true;
    }
    else
    {
        $success = false;
    }

    echo json_encode(array('status' => $success));
}

public function edit_question()
{
    $data['questionId'] = $this->input->post('questionId');
    $data['question'] = $this->input->post('question');

    $where = "questionId = " . $data['questionId'];

    $questionData = $this->general_model->update_vals($data, $where, 'tbl_szfive_survey_questions');

    echo json_encode(array('status' => true, 'data' => $questionData));
}

public function edit_choice()
{
    $data['label'] = $this->input->post('label');
    $choice = $this->input->post('choice');

    $where = "choiceId = " . $choice;
    $choiceData = $this->general_model->update_vals($data, $where, 'tbl_szfive_survey_choices');

    echo json_encode(array('status' => true, 'data' => $choiceData));
}

public function submit_survey()
{
    $notif_id = 0;

    if (!empty($this->input->post('survey')))
    {
        $data['surveyId'] = $this->input->post('survey');
    }

        // if (!empty($this->input->post('employee'))) {
    $data['employeeId'] = $this->userdata['uid'];
        // }

    if (!empty($this->input->post('answerNum')))
    {
        $data['answer'] = $this->input->post('answerNum');
    }

        // $data['detailedAnswer'] = ucfirst(trim($this->input->post('notrequiredAnswer'), " \t"));

    if (!empty($this->input->post('requiredAnswer')))
    {
        $data['detailedAnswer'] = ucfirst(trim($this->input->post('requiredAnswer'), " \t"));
    }

    if (!empty($this->input->post('sched_id')))
    {
        $data['sched_id'] = ucfirst(trim($this->input->post('sched_id'), " \t."));
    }

    if (!empty($this->input->post('choiceId')))
    {
        $choiceId = $this->input->post('choiceId');
    }

    $answer_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_szfive_survey_answers');

        // if ($median = $this->input->post('median')) {
        // if ($data['answer'] <= $median) {

    $employee = $this->session->userdata('fname') . ' ' . $this->session->userdata('lname');
    $emp_id = $this->session->userdata('emp_id');

    $survey_details = $this->general_model->fetch_specific_val('label', ['choiceId' => $choiceId], 'tbl_szfive_survey_choices');

    $message = $employee . ' feels ' . strtolower($survey_details->label) . '.';
    $link = 'szfive/survey/view/' . $emp_id . '/' . $answer_id;

    $query ="select uid,fname,lname,a.emp_id  as supervisor from  tbl_employee a, tbl_user b,tbl_applicant c where  b.emp_id = a.emp_id and a.apid=c.apid and b.emp_id=(SELECT supervisor FROM tbl_employee a where emp_id=".$_SESSION["emp_id"].")";

    $supervisor = $this->general_model->custom_query($query);

    $notif_id = $this->setSystemNotif($message, $link, $supervisor[0]->uid);

        // Also add the following data to survey review table
        // so direct supervisors will review user answers
    $review_data = [
        'answerId' => $answer_id,
        'reviewerId' => $supervisor[0]->uid,
        'notificationId' => $notif_id,
    ];

    $this->general_model->insert_vals($review_data, 'tbl_szfive_reviews');
        // }
        // }

    echo json_encode(array('status' => true, 'systemNotifID' => $notif_id));
}

public function popup_survey()
{
    $current_date = date('Y-m-d');
    $sched_id = $this->input->post('sched_id');
        $data['has_taken'] = true; //default

        if ($sched_id != null or $sched_id != 0)
        {

            $survey_answer = $this->general_model->fetch_specific_vals("*", ['employeeId' => $this->userdata['uid'], 'sched_id' => $sched_id], 'tbl_szfive_survey_answers', 'answerId');

            if (!empty($survey_answer))
            {
                $data['has_taken'] = true;
            }
            else
            {
                $data['has_taken'] = false;
            }
        }

        $query = "SELECT a.*, b.*
        FROM tbl_szfive_survey_details as a
        JOIN tbl_szfive_survey_questions as b ON b.questionId = a.questionId
        WHERE a.isDefault = 1
        ";

        $survey_data = $this->general_model->custom_query($query);

        $survey_data[0]->choiceLabels = json_decode($survey_data[0]->choiceLabels);

        $count = 0;
        foreach ($survey_data[0]->choiceLabels as $choice)
        {
            $count = $count + 1;
            $cdata = $this->general_model->fetch_specific_vals("*", ['choiceId' => $choice], 'tbl_szfive_survey_choices', 'label');
            $survey_data[0]->clabels[] = ['choiceId' => $cdata[0]->choiceId, 'num' => $count, 'label' => $cdata[0]->label, 'img' => $cdata[0]->img];
        }

        $data['data'] = $survey_data;

        echo json_encode($data);
    }

    public function survey_settings()
    {
        if ($this->session->userdata('role_id') != '1')
        {
            redirect('survey');
        }
        else
        {
            $survey = [
                'fields' => 'a.*, b.*',
                'table' => 'tbl_szfive_survey_details as a',
                'join' => [
                    'tbl_szfive_survey_questions as b' => 'b.questionId = a.questionId',
                ],
            ];


            $admins = $this->general_model->fetch_specific_vals("CONCAT(fname, ' ', lname) as name", "a.role_id = 1 AND a.emp_id = b.emp_id AND b.apid=c.apid", 'tbl_user a,tbl_employee b,tbl_applicant c', 'fname');
            $managers = $this->general_model->fetch_specific_vals("CONCAT(fname, ' ', lname) as name", "a.role_id = 2 AND a.emp_id = b.emp_id AND b.apid=c.apid", 'tbl_user a,tbl_employee b,tbl_applicant c ', 'fname');

            $data = [
                'uri_segment' => $this->uri->segment_array(),
                'title' => $this->title,
                'surveys' => $this->general_model->join_select($survey),
                'admins' => $admins,
                'managers' => $managers,
            ];
            $this->load_template_view('templates/szfive/survey/settings', $data);
        }
    }

    public function save_settings()
    {
        $survey_id = $this->input->post('surveyId');
        $permissions = $this->input->post('permissions');

        foreach ($permissions as $permission)
        {

            if (isset($permission['actions']))
            {
                $actions = $permission['actions'];
            }
            else
            {
                $actions = [];
            }

            $data = [
                'role_id' => intval($permission['role_id']),
                'settings' => json_encode($actions),
            ];

            $this->db->where('role_id', $data['role_id']);
            $q = $this->db->get('tbl_szfive_survey_permissions');

            if ($q->num_rows() > 0)
            {
                $this->db->where('role_id', $data['role_id']);
                $this->db->update('tbl_szfive_survey_permissions', $data);
            }
            else
            {
                $this->db->set('role_id', $data['role_id']);
                $this->db->insert('tbl_szfive_survey_permissions', $data);
            }
        }

        //First, set isDefault field equals to `0`
        $this->general_model->update_vals(['isDefault' => 0], ['surveyId <>' => null], 'tbl_szfive_survey_details');

        //Then, assign default value `1` for specific survey
        $update = $this->general_model->update_vals(['isDefault' => 1], ['surveyId' => $survey_id], 'tbl_szfive_survey_details');

        echo json_encode($permissions);
    }

    public function get_permissions()
    {
        $permissions = [
            'fields' => 'a.*, b.description as role, b.role_id',
            'table' => 'tbl_szfive_survey_permissions as a',
            'join' => [
                'tbl_user_role as b' => 'b.role_id = a.role_id',
            ],
        ];

        $permissions = $this->general_model->join_select($permissions);

        $default_actions = $this->_survey_actions();
        $newdata = [];

        foreach ($permissions as $key => $permission)
        {
            $permission->role = ucfirst($permission->role);
            $settings = json_decode($permission->settings);
            $actions = $this->_survey_actions();

            if ($settings != null)
            {
                foreach ($settings as $setting)
                {

                    $find_key[] = array_keys(array_keys($actions), $setting);

                    if ($find_key)
                    {
                        $actions[$setting][1] = 1;
                    }
                }
                $permission->actions = $actions;
            }
            else
            {
                $permission->actions = $this->_survey_actions();
            }

            array_push($newdata, $permission);
        }

        $data = $newdata;

        echo json_encode(['permissions' => $data]);
    }

    public function get_detailed_answers()
    {
        $post_data = $this->input->post('adata');

        $query['all'] = "SELECT j.pos_name, j.pos_details, a.detailedAnswer, a.dateCreated as dateAnswered, f.choiceLabels, a.answer, b.emp_id, CONCAT(e.fname, ' ', e.lname) as user_name, b.role as position, c.isActive,  e.pic, d.*, m.dep_name, o.link
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_user as b ON b.uid = a.employeeId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
        LEFT JOIN tbl_applicant as e ON e.apid = c.apid
        LEFT JOIN tbl_szfive_survey_details as f ON f.surveyId = a.surveyId
        LEFT JOIN tbl_emp_promote as k ON (k.emp_id = c.emp_id AND k.isActive = 1)
        LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = k.posempstat_id)
        LEFT JOIN tbl_position as j ON (j.pos_id = i.pos_id)
        LEFT JOIN tbl_schedule as l ON l.sched_id = a.sched_id
        LEFT JOIN tbl_department as m ON m.dep_id = d.dep_id
        LEFT JOIN tbl_szfive_reviews as n ON n.answerId = a.answerId
        LEFT JOIN tbl_system_notification as o ON o.systemNotification_ID = n.notificationId
        WHERE l.sched_date >=  " . "'" . $post_data['start_date'] . "'" . " AND l.sched_date <= " . "'" . $post_data['end_date'] . "'" . " AND a.surveyId =  " . $post_data['survey_id'] . " AND a.sched_id != 0 AND a.answer = " . $post_data['answer'] . "
        ";

        if ($this->userdata['role_id'] != 1)
        {

            $sub_data = $this->_has_subordinates();

            //If current user has subordinates, then fetch only his subordinates' answers
            if ($sub_data['has_subordinates'] == false)
            {
                // Get user's answer
                $query['all'] .= " AND a.employeeId = " . $this->userdata['uid'];
            }
            else
            {

                $subordinates = $this->_get_subordinates($sub_data['data']);
                $string = "(" . implode(",", $subordinates) . ")";
                $query['all'] .= " AND (b.emp_id IN $string OR a.employeeId = " . $this->userdata['uid'] . ")";
            }
        }

        $query['all'] .= " ORDER BY a.answerId DESC";
        $query['with_limit'] = $query['all'] . " LIMIT " . $post_data['offset'] . "," . $post_data['items'];

        $count = count($this->general_model->custom_query($query['all']));
        $data = $this->general_model->custom_query($query['with_limit']);

        foreach ($data as $key => $value)
        {
            $value->emp_stat = $this->getEmpStat($value->emp_id);
            $supervisor = $this->getSupervisor($value->emp_id);
            $value->supervisor = $supervisor->lname.', '.$supervisor->fname;
            $value->supEmail = $supervisor->email;
        }

        $choice_labels = json_decode($data[0]->choiceLabels);

        $choice_data = $this->general_model->fetch_specific_vals("label", ['choiceId' => $choice_labels[$data[0]->answer - 1]], 'tbl_szfive_survey_choices', 'label');

        $data[0]->answer_label = $choice_data[0]->label;

        echo json_encode(['data' => $data, 'count' => $count]);
    }

    /**
     * Get survey answers for the logged in user/team answers
     */
    public function get_user_answers()
    {
        $datatable = $this->input->post('datatable');

        $user_id = $datatable['query']['user_id'];
        $answer_type = $datatable['query']['answer_type'];

        $query['query'] = "SELECT a.*, b.*, DATE_FORMAT(a.dateCreated, '%M %e, %Y') as date, DATE_FORMAT(a.dateCreated, '%M %e, %Y %r') as dateAnswered, CONCAT(b.fname, ' ', b.lname) as user_name, b.role as position, c.Supervisor, c.acc_id, c.apid, c.email, c.emp_stat, c.supEmail, c.isActive, b.emp_id, d.*, e.pic, CONCAT(f.fname,' ',f.lname) as supervisor, g.*, f.emp_id as supervisorId, i.link
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_user as b ON b.uid = a.employeeId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
        LEFT JOIN tbl_applicant as e ON e.apid = c.apid
        LEFT JOIN tbl_user as f ON f.emp_id = c.Supervisor
        LEFT JOIN tbl_szfive_survey_details as g ON g.surveyId = a.surveyId
        LEFT JOIN tbl_szfive_reviews as h ON h.answerId = a.answerId
        LEFT JOIN tbl_system_notification as i ON i.systemNotification_ID = h.notificationId
        ";

        if ($answer_type == 'user')
        {
            $query['query'] .= " WHERE b.uid = " . $user_id;
        }

        if ($answer_type == 'team')
        {
            $query['query'] .= " WHERE f.uid  = " . $user_id;
        }

        if (isset($datatable['query']['userAnswerSearch']))
        {
            $keyword = $datatable['query']['userAnswerSearch'];

            $query['search']['append'] = " AND (a.detailedAnswer LIKE '%" . $keyword . "%' OR  a.answer LIKE '%" . $keyword . "%' OR  b.fname LIKE '%" . $keyword . "%' OR  b.lname LIKE '%" . $keyword . "%' OR  g.choiceLabels LIKE '%" . $keyword . "%' OR c.emp_stat LIKE '%" . $keyword . "%' OR c.emp_stat LIKE '%" . $keyword . "%' OR d.acc_name LIKE '%" . $keyword . "%' OR a.answer LIKE '%" . $keyword . "%')";

            $query['search']['total'] = " AND (a.detailedAnswer LIKE '%" . $keyword . "%' OR  a.answer LIKE '%" . $keyword . "%' OR  b.fname LIKE '%" . $keyword . "%' OR  b.lname LIKE '%" . $keyword . "%' OR  g.choiceLabels LIKE '%" . $keyword . "%' OR c.emp_stat LIKE '%" . $keyword . "%' OR d.acc_id LIKE '%" . $keyword . "%' OR d.acc_name LIKE '%" . $keyword . "%' OR a.answer LIKE '%" . $keyword . "%')";
        }

        $data = $this->set_datatable_query($datatable, $query);

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function generate_report_general($survey_id, $start_date, $end_date, $avg)
    {
        $query = "SELECT j.pos_name, j.pos_details, a.*, a.dateCreated as date1, DATE_FORMAT(a.dateCreated, '%b %e') as date, DATE_FORMAT(a.dateCreated, '%M %e, %Y %r') as dateAnswered, e.fname as firstname, e.lname as lastname, b.role as position, c.Supervisor as supervisor, c.acc_id, c.apid, e.email, c.isActive, b.emp_id, d.*, e.pic, CONCAT(e.fname,' ',e.lname) as supervisor, f.*, g.*, h.*
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_user as b ON b.uid = a.employeeId
        JOIN tbl_szfive_survey_details as g ON g.surveyId = a.surveyId
        JOIN tbl_szfive_survey_questions as h ON h.questionId = g.questionId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
        LEFT JOIN tbl_applicant as e ON e.apid = c.apid
        LEFT JOIN tbl_user as f ON f.emp_id = c.Supervisor
        LEFT JOIN tbl_emp_promote as k ON (k.emp_id = c.emp_id AND k.isActive = 1)
        LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = k.posempstat_id)
        LEFT JOIN tbl_position as j ON (j.pos_id = i.pos_id)
        LEFT JOIN tbl_schedule as l ON l.sched_id = a.sched_id
        WHERE l.sched_date >=  '$start_date' AND l.sched_date <= '$end_date' AND a.surveyId = '$survey_id' AND a.sched_id != 0
        ORDER BY e.lname
        ";

        $var = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'avg' => $avg,
        ];

        $this->_download_report($query, $var);
    }

    public function generate_report_specific($survey_id, $start_date, $end_date, $avg, $status, $account)
    {
        $query = "SELECT j.pos_name, j.pos_details, a.*, a.dateCreated as date1, DATE_FORMAT(a.dateCreated, '%b %e') as date, DATE_FORMAT(a.dateCreated, '%M %e, %Y %r') as dateAnswered, e.fname as firstname, e.lname as lastname, b.role as position, c.Supervisor as supervisor, c.acc_id, c.apid, e.email, c.emp_stat, c.supEmail, c.isActive, b.emp_id, d.*, e.pic, CONCAT(e.fname,' ',e.lname) as supervisor, f.*, g.*, h.*
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_user as b ON b.uid = a.employeeId
        JOIN tbl_szfive_survey_details as g ON g.surveyId = a.surveyId
        JOIN tbl_szfive_survey_questions as h ON h.questionId = g.questionId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
        LEFT JOIN tbl_applicant as e ON e.apid = c.apid
        LEFT JOIN tbl_user as f ON f.emp_id = c.Supervisor
        LEFT JOIN tbl_emp_promote as k ON (k.emp_id = c.emp_id AND k.isActive = 1)
        LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = k.posempstat_id)
        LEFT JOIN tbl_position as j ON (j.pos_id = i.pos_id)
        WHERE a.dateCreated >=  '" . $start_date . " 00:00:00' AND a.dateCreated <= '" . $end_date . " 23:59:59' AND a.surveyId = " . $survey_id . "
        ";

        $status = $status != "0" ? " AND c.emp_stat = '" . $status . "'" : '';
        $account = $account != "0" ? " AND d.acc_id = " . $account : '';

        $query .= $status . $account;
        $query .= " ORDER BY f.lname";
        $var = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'avg' => $avg,
        ];
        $this->_download_report($query, $var);
    }

    public function user_mention()
    {
        $img = "" . base_url() . "assets/images/";
        $uid = $this->session->userdata('uid');
        $date = date('Y-m-d');

        $data = $this->general_model->custom_query("SELECT a.recipientId FROM tbl_szfive_high_five_details as a LEFT JOIN tbl_szfive_high_fives as b ON b.id = a.id WHERE b.dateCreated >= '$date  00:00:00' AND b.dateCreated <= '$date  23:59:59' AND b.senderId = $uid");

        foreach ($data as $value)
        {
            $recipient_ids[] = $value->recipientId;
        }

        if (!empty($recipient_ids))
        {
            $str = "(" . implode(",", $recipient_ids) . ")";
        }

        $append = (isset($str)) ? " AND a.uid NOT IN " . $str : '';
        $criteria = ['img' => $img, 'uid' => $uid, 'append' => $append];
        $query = $this->szfive_model->user_mention($criteria);

        $employees = $this->general_model->custom_query($query);

        // $departments = $this->general_model->custom_query("SELECT CONCAT('t_', dep_id) as id, dep_details as name FROM tbl_department");
        $departments = $this->general_model->custom_query("SELECT CONCAT('t_', acc_id) as id, acc_name as  name FROM tbl_account");

        $result = array_merge_recursive($employees, $departments);

        echo json_encode($result);
    }

    public function user_comment_mentions()
    {
        $criteria = ['img' => "" . base_url() . "assets/images/", 'uid' => $this->userdata['uid'], 'append' => ' AND a.uid <> ' . $this->userdata['uid']];

        $query = $this->szfive_model->user_mention($criteria);

        $employees = $this->general_model->custom_query($query);

        echo json_encode($employees);
    }

    public function add_high_five()
    {
        $uid = $this->session->userdata('uid');
        $message = $this->input->post('mention');
        $user_ids = $this->input->post('userdata');
        $ispublic = $this->input->post('isPublic');

        $all_ids = [];
        $t_message = '';

        foreach ($user_ids as $udata)
        {

            if (preg_match('/t_/', $udata['id']))
            {
                $t_message = ' gave your team a High Five: <br>' . substr($message, 0, 50);
                $acc_id = str_replace('t_', '', $udata['id']);
                $m_ids = $this->_get_members_by_dept_id($acc_id);

                foreach ($m_ids as $id)
                {
                    $all_ids[] = $id->uid;
                }
            }
            else
            {
                $all_ids[] = $udata['id'];
            }
        }

        $recipient_ids = array_unique($all_ids);

        $no_of_recepients = count($recipient_ids);

        $data = array(
            'senderId' => intval($uid),
            'message' => $message,
            'noOfRecepients' => $no_of_recepients,
            'isPublic' => $ispublic,
        );

        $last_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_szfive_high_fives');

        foreach ($recipient_ids as $id)
        {
            $insert[] = $this->general_model->insert_vals(['id' => $last_id, 'recipientId' => $id], 'tbl_szfive_high_five_details');

            $recipients[] = ['userId' => $id];
        }

        // Save and send notification for High Five
        $emp_id = $this->session->userdata('emp_id');
        $emp_name = $this->session->userdata('fname') . ' ' . substr($this->session->userdata('lname'), 0, 1);

        if ($ispublic == '1')
        {
            if ($t_message != '')
            {
                $message = "<b>" . $emp_name . "</b> $t_message";
            }
            else
            {
                $message = '<b>' . $emp_name . '</b> gave you a High Five: <br>' . substr($message, 0, 50);
            }
        }
        else
        {
            $message = '<b>' . $emp_name . '</b> gave you a private High Five: <br>' . substr($message, 0, 50);
        }

        $link = 'szfive/highfive/view/' . $emp_id . '/' . $last_id;

        $notification = $this->set_system_notif_preceeding($message, $link, $recipients);

        // Get Supervisor ID as Reviewer ID
        $query = "SELECT uid FROM (SELECT supervisor FROM `tbl_employee` a WHERE emp_id=" . $emp_id . ") a, tbl_user b where a.supervisor = b.emp_id";

        $supervisor = $this->general_model->custom_query($query);

        if (!empty($supervisor))
        {
            // Update High Five details
            $details['reviewerId'] = $supervisor[0]->uid;
        }

        $details['notificationId'] = $notification['notif_id'][0];

        $this->general_model->update_vals($details, " id = $last_id", 'tbl_szfive_high_fives');

        echo json_encode(array('status' => $notification['send_stat'], 'sys_notif_id' => $notification['notif_id']));
    }

    public function get_all_highfive_posts()
    {
        $param = $this->input->post('params');

        $data = $this->_get_all_highfive_posts($param);

        echo json_encode($data);
    }

    public function get_public_highfives()
    {
        $datatable = $this->input->post('datatable');
        $query = $this->szfive_model->get_highfive();
        $role_id = $this->userdata['role_id'];

        $query['query'] .= " WHERE a.isPublic = 1 ORDER BY a.id DESC";
        $query['query'] .= " ORDER BY a.id DESC";

        $data = $this->set_datatable_query($datatable, $query);
        $newdata = [];

        foreach ($data['data'] as $value)
        {
            $value = $this->_get_mentioned_users($value);
            $value->totalComment = $this->general_model->fetch_specific_val('count(*) as count', ['parentId' => $value->hiFiveId, 'type' => 'sz_highfive', 'isPublic' => 1], 'tbl_szfive_comments');


            array_push($newdata, $value);
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $newdata]);
    }

    public function get_sent_highfives()
    {
        $datatable = $this->input->post('datatable');
        $dt_query = $this->input->post('query');
        $query = $this->szfive_model->get_highfive();
        $role_id = $this->userdata['role_id'];

        $query['query'] .= " WHERE a.senderId = " . $this->userdata['uid'] . " AND a.isPublic = " . $dt_query['isPublic'] . " ORDER BY a.id DESC";

        $data = $this->set_datatable_query($datatable, $query);
        $newdata = [];

        foreach ($data['data'] as $value)
        {
            $value = $this->_get_mentioned_users($value);
            $value->totalComment = $this->general_model->fetch_specific_val('count(*) as count', ['parentId' => $value->hiFiveId, 'type' => 'sz_highfive', 'isPublic' => 1], 'tbl_szfive_comments');


            array_push($newdata, $value);
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $newdata]);
    }

    public function get_received_highfives()
    {
        $datatable = $this->input->post('datatable');
        $dt_query = $this->input->post('query');
        $query = $this->szfive_model->get_highfive();
        $role_id = $this->userdata['role_id'];

        $query['query'] .= " LEFT JOIN tbl_szfive_high_five_details as k ON k.id = a.id WHERE k.recipientId = " . $this->userdata['uid'] . " AND a.isPublic = " . $dt_query['isPublic'] . " ORDER BY a.id DESC";

        $data = $this->set_datatable_query($datatable, $query);
        $newdata = [];

        foreach ($data['data'] as $value)
        {
            $value = $this->_get_mentioned_users($value);
            $value->totalComment = $this->general_model->fetch_specific_val('count(*) as count', ['parentId' => $value->hiFiveId, 'type' => 'sz_highfive', 'isPublic' => 1], 'tbl_szfive_comments');

            array_push($newdata, $value);
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $newdata]);
    }

    public function get_highfive_comments()
    {
        $id = $this->input->post('id');
        $offset = $this->input->post('offset');
        $no_of_items = $this->input->post('noOfItems');

        $query = "SELECT a.id , a.id as hiFiveId, a.message, a.noOfRecepients, a.senderId as uid, a.dateCreated, a.dateCreated as dateSubmitted, a.isPublic,  a.isReviewed, a.dateReviewed, f.fname as supervisorFname, f.lname as supervisorLname, e.pic as supervisorPic, c.emp_id as supId
        FROM tbl_szfive_high_fives as a
        LEFT JOIN tbl_user as b ON b.uid = a.reviewerId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_applicant as f ON c.apid = f.apid
        LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
        LEFT JOIN tbl_applicant as e ON e.apid = c.apid
        WHERE a.id = $id";

        $qdata = $this->general_model->custom_query($query);

        $data = $this->_get_entry_details($qdata);
        $data['entry'] = $this->_get_mentioned_users($data['entry']);
        $data['entry']->totalComment = $this->general_model->fetch_specific_val('count(*) as count', ['parentId' => $id, 'type' => 'sz_highfive', 'isPublic' => 1], 'tbl_szfive_comments');

        $total_reicipient = $data['entry']->noOfRecepients;
        $data['entry']->session_id = $this->userdata['uid'];

        if ($total_reicipient > 1)
        {
            $data['entry']->header_msg = $total_reicipient . ' people';
        }
        else
        {
            $uid = $this->general_model->fetch_specific_val('recipientId', ['id' => $id], 'tbl_szfive_high_five_details');
            $query = $this->szfive_model->get_user_details(['uid' => $uid->recipientId]);
            $recipient_data = $this->general_model->custom_query($query);
            $data['entry']->header_msg = $recipient_data[0]->name;
        }

        $data['child_comments'] = $this->_get_comments(['parent_id' => $id, 'isPublic' => 1, 'sort' => "a.dateCreated ASC", 'type' => 'sz_highfive', 'offset' => $offset, 'no_of_items' => $no_of_items]);
        $data['sent_to_users'] = $this->_sent_to_users($data['entry'], $data['child_comments']);

        echo json_encode($data);
    }

    public function load_more_comments()
    {
        $param = [
            'parent_id' => $this->input->post('id'),
            'offset' => $this->input->post('offset'),
            'no_of_items' => $this->input->post('noOfItems'),
            'type' => $this->input->post('type'),
        ];

        $total = $this->general_model->fetch_specific_val('count(*) as count', ['parentId' => $param['parent_id'], 'type' => "'" . $param['type'] . "'"], 'tbl_szfive_comments');
        $info = ['total' => $total->count, 'session_id' => $this->userdata['uid'], 'hiFiveId' => $param['parent_id']];

        $param['sort'] = 'a.dateCreated ASC';
        $param['isPublic'] = 1;
        $comments = $this->_get_comments($param);

        echo json_encode(['comments' => $comments, 'info' => $info]);
    }

    public function add_highfive_comment()
    {
        $data = [
            'parentId' => $this->input->post('hiFiveId'),
            'type' => 'sz_highfive',
            'senderId' => $this->userdata['uid'],
            'content' => $this->input->post('content'),
            'isPublic' => $this->input->post('isPublic'), // 0 = private comment, 1 = public comment
        ];

        $comment_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_szfive_comments');

        $param = [
            'parent_id' => $data['parentId'],
            'recipient_id' => $this->input->post('privateToUser'),
            'comment_id' => $comment_id,
            'content' => $data['content'],
            'ispublic' => $data['isPublic'],
            'usermention' => $this->input->post('usermention'),
        ];

        $notif_ids = $this->_get_comment_notification($param);

        $query = "SELECT a.id as commentId, a.senderId,  a.content, a.dateCreated, CONCAT(d.fname, ' ', d.lname) as fullname
        FROM tbl_szfive_comments as a
        JOIN tbl_user as b ON b.uid = a.senderId
        JOIN tbl_employee as c ON b.emp_id = c.emp_id
        JOIN tbl_applicant as d ON d.apid = c.apid
        WHERE a.id = $comment_id
        ";

        $cdata = $this->general_model->custom_query($query);
        $cdata[0]->session_id = $this->userdata['uid'];
        $cdata[0]->emp_id = $this->userdata['emp_id'];
        $cdata[0]->post_id = $data['parentId'];

        $pattern = '/@(\w+)/';
        $replacement = '<span class="user-mention">@${1}</span>';
        $cdata[0]->content = preg_replace($pattern, $replacement, $cdata[0]->content);
        // $cdata[0]->dateCreated = $this->_time_elapsed_string($cdata[0]->dateCreated);
        $cdata[0]->dateCreated = 'Just now';

        echo json_encode(['data' => $cdata[0], 'system_notif_ids' => $notif_ids]);
    }

    public function get_hi5_comment_details()
    {
        $id = $this->input->post('id');
        $comment = $this->general_model->fetch_specific_vals('id, content, isPublic', ['id' => $id], 'tbl_szfive_comments', null);

        echo json_encode($comment[0]);
    }

    public function update_hi5_comment()
    {
        $usermention = $this->input->post('usermention');
        $param = [
            'parent_id' => $this->input->post('hiFiveId'),
            'comment_id' => $this->input->post('commentId'),
            'content' => $this->input->post('content'),
            'ispublic' => $this->input->post('isPublic'),
            'recipient_id' => $this->input->post('privateToUser'),
        ];

        $update = $this->general_model->update_vals(['content' => $param['content']], ['id' => $param['comment_id']], 'tbl_szfive_comments');

        $notif_ids = [];

        if ($usermention != 0)
        {
            $exclude_ids = [];

            $existing_notifs = $this->general_model->custom_query("SELECT DISTINCT(c.recipient_ID) AS recipient_id FROM tbl_szfive_comments as a LEFT JOIN tbl_szfive_usermentions as b ON b.parentId = a.id LEFT JOIN tbl_system_notification_recipient AS c ON c.systemNotification_ID = b.notificationId WHERE b.parentId = " . $param['comment_id'] . " AND b.type = 'szfive_comment'");

            foreach ($existing_notifs as $notif)
            {
                $exclude_ids[] = $notif->recipient_id;
            }

            foreach ($usermention as $user)
            {
                $recipient_ids[] = $user['id'];
            }

            $usermention_ids = array_diff($recipient_ids, $exclude_ids);

            if (!empty($usermention_ids))
            {
                foreach ($usermention_ids as $id)
                {
                    $param['usermention'][] = ['id' => $id];
                }
                $notif_ids = $this->_get_comment_notification($param);
            }
        }

        $pattern = '/@(\w+)/';
        $replacement = '<span class="user-mention">@${1}</span>';

        $comment = preg_replace($pattern, $replacement, $param['content']);

        echo json_encode(['status' => $update, 'comment' => $comment, 'system_notif_ids' => $notif_ids]);
    }

    public function delete_hi5_comment()
    {
        $id = $this->input->post('id');
        $this->general_model->delete_vals(['id' => $id], 'tbl_szfive_comments_private');
        $delete = $this->general_model->delete_vals(['id' => $id], 'tbl_szfive_comments');

        echo json_encode(['status' => $delete]);
    }

    public function delete_hi5_post()
    {
        $id = $this->input->post('id');
        $this->general_model->delete_vals(['id' => $id], 'tbl_szfive_high_fives');
        // $this->general_model->delete_vals(['id' => $id], 'tbl_szfive_comments_private');
        $delete_comment = $this->general_model->delete_vals(['parentId' => $id, 'type' => 'sz_highfive'], 'tbl_szfive_comments');

        echo json_encode(['status' => $delete_comment]);
    }

    public function send_email_notification()
    {
        $week = $this->_check_week_end();
        $survey_id = "3";
        $this_week = 0;
        $last_week = 0;

        $query = "SELECT AVG(answer) as avg FROM tbl_szfive_survey_answers WHERE dateCreated >=  '" . $week['this_week_startdate'] . " 00:00:00' AND dateCreated <= '" . $week['this_week_enddate'] . " 23:59:59' AND surveyId = " . $survey_id . "";

        $this_week = $this->general_model->custom_query($query);

        if (!empty($this_week))
        {
            $this_week_avg = number_format((float) $this_week[0]->avg, 2, '.', '');
            $this_week_dates = date_format(date_create($week['this_week_startdate']), 'F j, Y') . ' - ' . date_format(date_create($week['this_week_enddate']), 'F j, Y');
        }

        $query = "SELECT AVG(answer) as avg FROM tbl_szfive_survey_answers WHERE dateCreated >=  '" . $week['last_week_startdate'] . " 00:00:00' AND dateCreated <= '" . $week['last_week_enddate'] . " 23:59:59' AND surveyId = " . $survey_id . "";

        $last_week = $this->general_model->custom_query($query);

        if (!empty($last_week))
        {
            $last_week_avg = number_format((float) $last_week[0]->avg, 2, '.', '');
            $last_week_dates = date_format(date_create($week['last_week_startdate']), 'F j, Y') . ' - ' . date_format(date_create($week['last_week_enddate']), 'F j, Y');
        }

        if ($this_week_avg > $last_week_avg)
        {
            $pulse_stat = 'increase';
        }
        else
        {
            $pulse_stat = 'decline';
        }

        $img = "http://10.200.101.107/sz/assets/images/";

        $rdata = $this->szfive_model->get_top_highfive(['img' => $img, 'id' => 'recipientId', 'table' => 'tbl_szfive_high_five_details']);

        if (!empty($rdata))
        {
            $rdata[0]->msg = 'Received the most SZhoutOuts (' . $rdata[0]->count . ')';
            $highfives[] = $rdata[0];
        }

        $sdata = $this->szfive_model->get_top_highfive(['img' => $img, 'id' => 'senderId', 'table' => 'tbl_szfive_high_fives']);
        if (!empty($sdata))
        {
            $sdata[0]->msg = 'Gave the most SZhoutOuts (' . $sdata[0]->count . ')';
            $highfives[] = $sdata[0];
        }

        $criteria = array();
        $criteria['current_week'] = $this_week_dates;
        $criteria['pulse_stat'] = $pulse_stat;
        $criteria['pulse_avg'] = $this_week_avg;
        $criteria['pulse_last_avg'] = $last_week_avg;
        $criteria['high_fivers'] = $highfives;

        $query = "SELECT a.*, b.*, c.email FROM tbl_szfive_email_notifications as a JOIN tbl_user as b ON b.uid = a.uid JOIN tbl_employee as c ON c.emp_id = b.emp_id JOIN tbl_applicant as d ON d.apid = c.apid";
        $userdata = $this->general_model->custom_query($query);

        $not_sent = array();

        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => 'szdummysz@gmail.com',
            'smtp_pass' => 'szdummy123',
            'smtp_port' => 587,
            'smtp_crypto' => 'tls',
            'mailtype    ' => "html",
            'validate' => true,
            'crlf' => "\r\n",
            'charset' => "utf-8",
            'newline' => "\r\n",
        ));

        foreach ($userdata as $user)
        {

            if ($user->email != null)
            {
                $this->email->from('notification@supportzebra.com', 'SupportZebra');
                $this->email->subject('SZ Survey - Weekly Admin Digest');
                $this->email->set_mailtype("html");
                $this->email->to($user->email);

                $criteria['username'] = $user->fname;
                $data['criteria'] = $criteria;

                $body = $this->load->view('templates/szfive/survey/_email.php', $data, true);
                $this->email->message($body);
                $this->email->send();
                echo $this->email->print_debugger();
            }
            else
            {
                $not_sent[] = $user->fname . ' ' . $user->lname;
            }
        }
        echo json_encode(['not sent' => $not_sent]);
    }

    public function get_highfive_post()
    {
        $id = $this->input->post('id');
        $subordinates = $this->general_model->fetch_specific_vals('emp_id', ['Supervisor' => $user->emp_id, 'isActive' => 'yes'], 'tbl_employee', null);

        echo json_encode($data);
    }

    public function get_answer_entry($user_id = null, $answer_id = null)
    {
        $query = "SELECT a.answerId, a.answer, a.detailedAnswer, a.employeeId as uid, a.dateCreated, a.isReviewed, b.numberOfChoices, b.choiceLabels, c.sched_date, d.message, d.dateUpdated as dateReviewed, d.reviewerId, h.fname as supervisorFname, h.lname as supervisorLname, h.pic as supervisorPic, f.emp_id as supId
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_szfive_survey_details as b ON b.surveyId = a.surveyId
        LEFT JOIN tbl_schedule as c ON c.sched_id = a.sched_id
        LEFT JOIN tbl_szfive_reviews as d ON d.answerId = a.answerId
        LEFT JOIN tbl_user as e ON e.uid = d.reviewerId
        LEFT JOIN tbl_employee as f ON f.emp_id = e.emp_id
        LEFT JOIN tbl_account as g ON g.acc_id = f.acc_id
        LEFT JOIN tbl_applicant as h ON h.apid = f.apid
        WHERE a.answerId = $answer_id";

        $qdata = $this->general_model->custom_query($query);

        if (!empty($qdata))
        {

            $data = $this->_get_entry_details($qdata);
            $choice = json_decode($data['entry']->choiceLabels);
            $answernum = $data['entry']->answer - 1;
            $choicedata = $this->general_model->fetch_specific_val('*', ['choiceId' => $choice[$answernum]], 'tbl_szfive_survey_choices');

            $data['entry']->survey_date = DateTime::createFromFormat('Y-m-d', $data['entry']->sched_date)->format('F j, Y');

            $dayOfWeek = date('l', strtotime($data['entry']->sched_date));
            $formatDate = DateTime::createFromFormat('Y-m-d H:i:s', $data['entry']->dateCreated)->format('F j');

            $data['entry']->date_answered = $dayOfWeek . ', ' . $formatDate;
            $data['entry']->choice_label = $choicedata->label;
            $data['entry']->choice_img = $choicedata->img;
            $data['uri_segment'] = $this->uri->segment_array();
            $data['title'] = $this->title . ' - Survey Answer';

            $this->load_template_view('templates/szfive/survey/_entry', $data);
        }
        else
        {
            $this->error_404();
        }
    }

    public function get_highfive_entry($emp_id = null, $highfive_id = null)
    {
        $param = [
            'emp_id' => $emp_id,
            'highfive_id' => $highfive_id,
            'isPublic' => 1
        ];

        $this->_get_highfive_enty_comments($param);
    }

    public function load_highfive_entry_comments()
    {
        $param = $this->input->post('param');

        $data['total'] = count($this->general_model->custom_query("SELECT id FROM tbl_szfive_comments WHERE parentId = " . $param['entryId'] . " AND isPublic = " . $param['isPublic']));

        $data['comments'] = $this->_get_comments(['parent_id' => $param['entryId'], 'isPublic' => $param['isPublic'], 'sort' => "a.dateCreated DESC", 'type' => 'sz_highfive', 'offset' => $param['offset'], 'no_of_items' => 5]);
        arsort($data['comments']);
        $data['session_id'] = $this->userdata['uid'];

        echo json_encode($data);
    }

    public function submit_review_answer()
    {
        $answerId = $this->input->post('answerId');
        $reviewerId = $this->userdata['uid'];

        if ($this->input->post('message') != '')
        {
            $message = trim($this->input->post('message'), " \t");
            $data['message'] = $message;
        }

        $data['isReviewed'] = 1;

        $reviewed = $this->general_model->update_vals($data, ['answerId' => $answerId], 'tbl_szfive_reviews');

        if ($reviewed)
        {
            $upd = $this->general_model->update_vals(['isReviewed' => 1], ['answerId' => $answerId], 'tbl_szfive_survey_answers');
            $reviewdata = $this->general_model->fetch_specific_val('*', ['answerId' => $answerId], 'tbl_szfive_reviews');

            $reviewdata->uid = $this->userdata['uid'];
            $reviewdata->pic = base_url() . '/assets/images/' . $this->session->userdata('pic');
            $reviewdata->reviewer_name = $this->session->userdata('fname') . ' ' . $this->session->userdata('lname');

            $date = new DateTime($reviewdata->dateCreated);

            $reviewdata->dateCreated = date_format($date, 'F j, Y  h:i:s A');

            $success = true;
        }
        else
        {
            $success = false;
        }

        echo json_encode(['status' => $success, 'data' => $reviewdata]);
    }

    public function get_to_be_reviewed_answers()
    {
        $datatable = $this->input->post('datatable');
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];

        $query['query'] = "SELECT d.systemNotification_ID as notifId, a.*, b.employeeId as uid, CONCAT(e.fname, ' ', e.lname) as supervisor, c.message as notif_message, c.link, b.answer, IFNULL(b.isReviewed, 0) as stat, DATE_FORMAT(b.dateCreated, '%b %e, %Y') as dateAnswered, g.fname as empFname, g.lname as empLname, g.uid, h.numberOfChoices, b.detailedAnswer, h.choiceLabels, b.answerId
        FROM tbl_szfive_reviews as a
        LEFT JOIN tbl_szfive_survey_answers as b ON b.answerId = a.answerId
        LEFT JOIN tbl_system_notification as c ON c.systemNotification_ID = a.notificationId
        LEFT JOIN tbl_system_notification_recipient as d ON d.systemNotification_ID = c.systemNotification_ID
        LEFT JOIN tbl_user as e ON e.uid = a.reviewerId
        LEFT JOIN tbl_employee as f ON f.emp_id = e.emp_id
        LEFT JOIN tbl_user as g ON g.uid = b.employeeId
        LEFT JOIN tbl_szfive_survey_details as h ON h.surveyId = b.surveyId
        WHERE b.dateCreated >=  '$start_date' AND b.dateCreated <= '$end_date'
        ";

        if ($this->userdata['role_id'] != 1)
        {

            $sub_data = $this->_has_subordinates();

            if ($sub_data['has_subordinates'] == true)
            {
                $subordinates = $this->_get_subordinates($sub_data['data']);
                $string = "(" . implode(",", $subordinates) . ")";
                $query['query'] .= " AND g.emp_id IN $string";
            }
        }

        $search_append = '';
        $search_total = '';

        if (isset($datatable['query']['status']))
        {
            $keyword = $datatable['query']['status'];

            if ($keyword != null AND $keyword != 'all')
            {
                $string = " AND b.isReviewed = '$keyword'";
            }
            else
            {
                $string = '';
            }
            $search_append = $search_append . $string;
            $search_total = $search_total . $search_append;

            $query['search']['append'] = $search_append;
            $query['search']['total'] = $search_total;
        }

        if (isset($datatable['query']['reviewsSearch']))
        {
            $keyword = $datatable['query']['reviewsSearch'];

            $search_append = $search_append . " AND (g.fname LIKE '%$keyword%' OR g.lname LIKE '%$keyword%' OR b.answer LIKE '%$keyword%' OR b.detailedAnswer LIKE '%$keyword%')";
            $search_total = $search_total . $search_append;

            $query['search']['append'] = $search_append;
            $query['search']['total'] = $search_total;
        }

        $data = $this->set_datatable_query($datatable, $query);

        foreach ($data['data'] as $adata)
        {
            $choice = json_decode($adata->choiceLabels);
            $answernum = $adata->answer - 1;

            $choicedata = $this->general_model->fetch_specific_val('*', ['choiceId' => $choice[$answernum]], 'tbl_szfive_survey_choices');
            $adata->choice_img = $choicedata->img;
            $adata->choice_label = $choicedata->label;
            $query = $this->szfive_model->get_user_details(['uid' => $adata->uid]);
            $adata->user_img = $this->general_model->custom_query($query)[0]->avatar;
        }
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function add_high_five_reaction()
    {
        $data['szfiveId'] = $this->input->post('highfiveId');
        $data['type'] = 'sz_highfive';
        $data['uid'] = $this->userdata['uid'];
        $data['reactionId'] = $this->input->post('reactionType');

        $highfive_likes = $this->general_model->custom_query("SELECT * FROM tbl_szfive_likes WHERE szfiveId = " . $data['szfiveId'] . " AND uid = " . $data['uid']);

        if (!empty($highfive_likes))
        {
            $where = "likeId = " . $highfive_likes[0]->likeId;
            $like_id = $this->general_model->update_vals(['reactionId' => $data['reactionId'], 'dateUpdated' => date('Y-m-d H:i:s')], $where, 'tbl_szfive_likes');
        }
        else
        {
            $like_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_szfive_likes');

            $highfive_likes = $this->general_model->custom_query("SELECT * FROM tbl_szfive_likes WHERE likeId = $like_id");
        }

        $likes = $this->_get_highfive_likes($highfive_likes[0], ['hiFiveId' => $data['szfiveId'], 'type' => 'sz_highfive']);

        $highfive_data = $this->general_model->fetch_specific_val('senderId', ['id' => $data['szfiveId']], 'tbl_szfive_high_fives');

        $notif_id = [];

        if (!empty($highfive_data))
        {

            if ($highfive_data->senderId != $this->session->userdata('uid'))
            {

                $emp_name = $this->session->userdata('fname') . ' ' . substr($this->session->userdata('lname'), 0, 1) . '.';
                $link = 'szfive/highfive/view/' . $highfive_data->senderId . '/' . $data['szfiveId'];

                if ($likes->reaction_name == 'like')
                {
                    $message = '<b>' . $emp_name . '</b> liked your High Five.';
                }
                else
                {
                    $message = '<b>' . $emp_name . '</b> reacted to your High Five.';
                }

                $recipient_id[] = ['userId' => $highfive_data->senderId];

                $send_notification = $this->set_system_notif_preceeding($message, $link, $recipient_id);
                $notif_id[] = $send_notification['notif_id'][0];
            }
        }

        if (!empty($like_id))
        {
            $success = true;
        }

        echo json_encode(['details' => $likes, 'success' => $success, 'system_notif_ids' => $notif_id]);
    }

    public function delete_high_five_reaction()
    {
        $data['szfiveId'] = $this->input->post('highfiveId');
        $data['uid'] = $this->userdata['uid'];

        $delete = $this->general_model->delete_vals($data, 'tbl_szfive_likes');

        $highfive_likes = $this->general_model->custom_query("SELECT * FROM tbl_szfive_likes WHERE szfiveId = " . $data['szfiveId']);

        if (!empty($highfive_likes))
        {
            $likes = $this->_get_highfive_likes($highfive_likes[0], ['hiFiveId' => $data['szfiveId'], 'type' => 'sz_highfive']);
        }
        else
        {
            $likes = [];
        }

        echo json_encode(['success' => $delete, 'data' => $likes]);
    }

    public function get_reviews()
    {
        $query['query'] = "SELECT a.*, b.* FROM tbl_szfive_reviews as a JOIN tbl_szfive_survey_answers as b ON b.answerId = a.answerId";

        $datatable = $this->input->post('datatable');
        $data = $this->set_datatable_query($datatable, $query);
        $i = 0;
        $newdata = [];

        echo json_encode(['meta' => $data['meta'], 'data' => $newdata]);
    }

    public function get_by_departments_reviews()
    {
        $post_data = $this->input->post('params');
        $start_date = $post_data['startDate'];
        $end_date = $post_data['endDate'];

        if (isset($post_data['depId']))
        {
            $dep_ids = $post_data['depId'];
        }
        else
        {
            $dep_ids = [];
        }

        if (empty($dep_ids))
        {
            $append = "WHERE (dep_name != 'test')";
        }
        else if (count($dep_ids) == 1)
        {
            $append = "WHERE dep_id = $dep_ids[0]";
        }
        else if (count($dep_ids) > 1)
        {
            $ids = implode(",", $dep_ids);
            $append = "WHERE dep_id IN ($ids)";
        }

        $departments = $this->general_model->custom_query("SELECT * FROM tbl_department $append");

        $query = "FROM tbl_szfive_reviews as a LEFT JOIN tbl_user as b ON b.uid = a.reviewerId LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id WHERE a.dateCreated >=  '$start_date 00:00:00' AND a.dateCreated <= '$end_date 23:59:59' AND d.dep_id = ";

        foreach ($departments as $department)
        {
            $dept_name = $department->dep_name;
            $dept_names[] = $dept_name;

            $all_reviews = $this->general_model->custom_query("SELECT a.reviewId, a.reviewerId, a.isReviewed, c.emp_id, c.acc_id, d.dep_id " . $query . $department->dep_id);

            $unreviewed_data = $this->general_model->custom_query("SELECT a.reviewId, a.reviewerId, a.isReviewed, c.emp_id, c.acc_id, d.dep_id " . $query . $department->dep_id . " AND a.isReviewed = 0");
            $all = count($all_reviews);
            $reviewed = count($all_reviews) - count($unreviewed_data);

            $percent = round(@($reviewed / $all) * 100, 2);
            $percents[] = $percent;

            $dept_reviews[] = [
                'name' => $dept_name,
                'title' => $dept_name,
                'y' => floatval($percent),
                'drilldown' => true,
                'total' => $all,
                'reviewed' => $reviewed,
            ];

            $drilldown[] = [
                'name' => $dept_name,
                'id' => $department->dep_id,
            ];
        }

        if (count(array_unique($percents)) > 1)
        {
            $data['hasData'] = true;
        }
        else
        {
            $data['hasData'] = false;
        }

        $data = ['dept_reviews' => $dept_reviews, 'dept_names' => $dept_names, 'dept_drilldown' => $drilldown];

        echo json_encode($data);
    }

    public function get_by_department_reviewers()
    {
        $post_data = $this->input->post('params');
        $start_date = $post_data['startDate'];
        $end_date = $post_data['endDate'];
        $dep_name = $post_data['depName'];

        if (isset($post_data['depId']))
        {
            $dep_ids = $post_data['depId'];
        }
        else
        {
            $dep_ids = [];
        }

        if (empty($dep_ids))
        {
            $where = "WHERE dep_name = '$dep_name'";
        }
        else if (count($dep_ids) == 1)
        {
            $where = "WHERE dep_name = '$dep_name' AND dep_id = $dep_ids[0]";
        }
        else
        {
            $ids = implode(",", $dep_ids);
            $where = "WHERE dep_name = '$dep_name' AND dep_id IN ($ids)";
        }

        $dept_data = $this->general_model->custom_query("SELECT dep_id, dep_name FROM tbl_department $where");
        $dept = $dept_data[0];

        $query = "FROM tbl_szfive_reviews as a LEFT JOIN tbl_user as b ON b.uid = a.reviewerId LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id WHERE a.dateCreated >=  '$start_date 00:00:00' AND a.dateCreated <= '$end_date 23:59:59' AND d.dep_id = " . $dept->dep_id;

        $total_reviews = count($this->general_model->custom_query("SELECT a.reviewId, a.reviewerId, a.isReviewed, c.emp_id, c.acc_id, d.dep_id " . $query));
        $reviewers = $this->general_model->custom_query("SELECT a.reviewerId, count(*) as count " . $query . " GROUP BY a.reviewerId");

        foreach ($reviewers as $val)
        {
            $user = $this->general_model->fetch_specific_val('fname, lname', ['uid' => $val->reviewerId], 'tbl_user');
            $reviewed = $this->general_model->fetch_specific_val('count(*) as count', ['reviewerId' => $val->reviewerId, 'isReviewed' => 1], 'tbl_szfive_reviews');

            $percent = round(@($reviewed->count / $val->count) * 100, 2);

            $reviewer = [
                'name' => $user->fname . ' ' . $user->lname,
                'y' => $percent,
                'reviewed' => $reviewed->count,
                'total' => $val->count,
            ];

            $reviewer_data[] = $reviewer;
        }

        $data[$dept->dep_name] = [
            'name' => $dept->dep_name,
            'data' => $reviewer_data,
        ];

        echo json_encode($data);
    }

    public function get_survey_reviewers()
    {
        $datatable = $this->input->post('datatable');
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];

        if (isset($datatable['query']['depId']))
        {
            $dep_ids = $datatable['query']['depId'];
        }
        else
        {
            $dep_ids = [];
        }

        if (empty($dep_ids))
        {
            $append = "";
        }
        else if (count($dep_ids) == 1)
        {
            $append = "AND d.dep_id = $dep_ids[0]";
        }
        else
        {
            $ids = implode(",", $dep_ids);
            $append = "AND d.dep_id IN ($ids)";
        }

        if (isset($datatable['query']['reviewerDetailsSearch']))
        {
            $keyword = $datatable['query']['reviewerDetailsSearch'];
            $append .= " AND (b.fname LIKE '%$keyword%' OR b.lname LIKE '%$keyword%')";
        }

        $query['query'] = "SELECT a.* FROM tbl_szfive_reviews as a JOIN tbl_user as b ON b.uid = a.reviewerId  JOIN tbl_employee as c ON c.emp_id = b.emp_id JOIN tbl_account as d ON d.acc_id = c.acc_id WHERE a.dateCreated >=  '$start_date 00:00:00' AND a.dateCreated <= '$end_date 23:59:59' $append GROUP BY reviewerId ORDER BY reviewId DESC";

        $data = $this->set_datatable_query($datatable, $query);

        if (!empty($data['data']))
        {
            foreach ($data['data'] as $ndata)
            {
                $u_query = $this->szfive_model->get_user_details(['uid' => $ndata->reviewerId]);
                $user_details = $this->general_model->custom_query($u_query);
                if (!empty($user_details))
                {
                    $ndata->reviewer = $user_details[0]->name;
                    $ndata->pic = $user_details[0]->avatar;
                    $ndata->position = $user_details[0]->pos_details;
                    $ndata->account = $user_details[0]->acc_name;
                    $ndata->account_type = $user_details[0]->acc_description;
                    $ndata->dept_abbv = $user_details[0]->dep_name;
                    $ndata->dept_name = $user_details[0]->dep_details;

                    $r_query = "SELECT count(*) as count FROM tbl_szfive_reviews";
                    $total_reviews = $this->general_model->custom_query($r_query . " WHERE reviewerId = " . $ndata->reviewerId);
                    $total_reviewed = $this->general_model->custom_query($r_query . " WHERE isReviewed = 1 AND reviewerId = " . $ndata->reviewerId);

                    $ndata->total_reviews = $total_reviews[0]->count;
                    $ndata->total_reviewed = $total_reviewed[0]->count;
                    $ndata->percent = round(@($ndata->total_reviewed / $ndata->total_reviews) * 100, 2) . '%';
                }
            }
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_survey_notifications()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "SELECT a.systemNotification_ID,a.message,a.link,a.createdOn,b.systemNotificationRecipient_ID,b.recipient_ID,c.status_ID,c.description as status
        FROM tbl_system_notification as a
        JOIN tbl_system_notification_recipient as b ON b.systemNotification_ID = a.systemNotification_ID
        JOIN tbl_status as c ON c.status_ID = b.status_ID
        WHERE a.link LIKE 'szfive/survey/view%'
        ORDER BY a.createdOn DESC
        ";

        $data = $this->set_datatable_query($datatable, $query);
        foreach ($data['data'] as $value)
        {
            $value->dateSubmitted = $this->_time_elapsed_string($value->createdOn);
        }
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_reviewed_survey_answers()
    {
        $datatable = $this->input->post('datatable');
        $reviewer_id = $datatable['query']['reviewerId'];
        $is_reviewed = $datatable['query']['isReviewed'];
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];

        $query['query'] = "SELECT a.answerId, a.message, DATE_FORMAT(a.dateUpdated, '%b %e, %Y %r') as dateUpdated, DATE_FORMAT(b.dateCreated, '%b %e, %Y %r') as dateCreated, b.isReviewed, c.fname, c.lname, e.pic, b.employeeId as empId, d.Supervisor, j.pos_details as position
        FROM tbl_szfive_reviews as a
        JOIN tbl_szfive_survey_answers as b ON b.answerId = a.answerId
        LEFT JOIN tbl_user as c ON c.uid = b.employeeId
        JOIN tbl_employee as d ON d.emp_id = c.emp_id
        JOIN tbl_applicant as e ON e.apid = d.apid
        LEFT JOIN tbl_emp_promote as h ON (h.emp_id = c.emp_id  AND h.isActive = 1)
        LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = h.posempstat_id)
        LEFT JOIN tbl_position as j ON (j.pos_id = i.pos_id)
        WHERE a.reviewerId = $reviewer_id AND a.isReviewed = $is_reviewed";

        if ($start_date != 0 and $end_date != 0)
        {
            $query['query'] .= " AND a.dateUpdated >=  '$start_date 00:00:00' AND a.dateUpdated <= '$end_date 23:59:59'";
        }

        if (isset($datatable['query']['reviewersListSearch']))
        {
            $keyword = $datatable['query']['reviewersListSearch'];
            $query['query'] .= " AND (c.fname LIKE '%$keyword%' OR c.lname LIKE '%$keyword%' OR j.pos_details LIKE '%$keyword%')";
        }

        $data = $this->set_datatable_query($datatable, $query);

        foreach ($data['data'] as $val)
        {
            $supervisor_data = $this->general_model->fetch_specific_val('*', ['emp_id' => $val->Supervisor], 'tbl_user');
            $val->supervisor_name = $supervisor_data->fname . ' ' . $supervisor_data->lname;
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_reviews_specific_answer()
    {
        $answerId = $this->input->post('answerId');

        $query = "SELECT a.answerId, a.answer, a.detailedAnswer, a.employeeId as uid, a.dateCreated, a.isReviewed, b.numberOfChoices, b.choiceLabels, c.sched_date, d.message, d.dateUpdated as dateReviewed, d.reviewerId, g.fname as supervisorFname, g.lname as supervisorLname
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_szfive_survey_details as b ON b.surveyId = a.surveyId
        LEFT JOIN tbl_schedule as c ON c.sched_id = a.sched_id
        LEFT JOIN tbl_szfive_reviews as d ON d.answerId = a.answerId
        LEFT JOIN tbl_user as e ON e.uid = d.reviewerId
        LEFT JOIN tbl_employee as f ON e.emp_id = f.emp_id
        LEFT JOIN tbl_applicant as g ON f.apid = g.apid
        WHERE a.answerId = $answerId";

        $temp_data = $this->general_model->custom_query($query);
        $data = $temp_data[0];

        $choice = json_decode($data->choiceLabels);
        $answernum = $data->answer - 1;

        $choicedata = $this->general_model->fetch_specific_val('*', ['choiceId' => $choice[$answernum]], 'tbl_szfive_survey_choices');

        $query = $this->szfive_model->get_user_details(['uid' => $data->uid]);
        $userdata = $this->general_model->custom_query($query);

        $data->fullname = $userdata[0]->firstname . ' ' . $userdata[0]->lastname;
        $data->pic = $userdata[0]->avatar;
        $data->choice_label = $choicedata->label;
        $data->choice_img = $choicedata->img;
        $data->survey_date = DateTime::createFromFormat('Y-m-d', $data->sched_date)->format('F j, Y');
        $data->answer_date = DateTime::createFromFormat('Y-m-d H:i:s', $data->dateCreated)->format('F j, Y h:i:s:A');
        $data->session_id = $this->userdata['uid'];
        $data->time_diff = $this->_datetime_difference(['start' => $data->dateCreated, 'end' => $data->dateReviewed]);

        echo json_encode($data);
    }

    public function review_highfive()
    {
        $id = $this->input->post('id');

        $data['isReviewed'] = 1;
        $data['dateReviewed'] = date('Y-m-d H:i:s');

        $update = $this->general_model->update_vals($data, " id = $id", 'tbl_szfive_high_fives');

        echo json_encode(['status' => $update]);
    }

    public function review_details()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Review Details - Survey',
            'this_week' => $this->_get_this_week_date_range(),
            'nav_list' => $this->_navigation_bar_list('Reviews'),
        ];

        if ($this->userdata['role_id'] != 1)
        {
            redirect('szfive/reviews');
        }
        else
        {
            $this->load_template_view('templates/szfive/reviews/_details', $data);
        }
    }

    public function get_review_details()
    {
        $datatable = $this->input->post('datatable');

        $param = $this->input->post('query');
        $reviewType = $param['reviewType'];
        $query = "SELECT a.answerId, a.dateUpdated as dateReviewed, c.dateCreated as dateAnswered, d.sched_date, b.fname as reviewerFname, b.lname as reviewerLname, e.fname as answerFname, e.lname as answerLname FROM tbl_szfive_reviews as a  JOIN tbl_user as b ON b.uid = a.reviewerId JOIN tbl_szfive_survey_answers as c ON c.answerId = a.answerId  LEFT JOIN tbl_schedule as d ON d.sched_id = c.sched_id LEFT JOIN tbl_user as e ON e.uid = c.employeeId WHERE a.isReviewed = 1";

        if ($reviewType == 'all')
        {

            $sql['query'] = "$query AND d.sched_date >= '" . $param['startDate'] . "' AND d.sched_date <= '" . $param['endDate'] . "' ORDER BY d.sched_date DESC";

            $data = $this->set_datatable_query($datatable, $sql);
            $sdata = $data['data'];
        }
        else
        {
            $sql['query'] = "SELECT b.sched_date FROM tbl_szfive_survey_answers as a LEFT JOIN tbl_schedule as b ON b.sched_id = a.sched_id WHERE b.sched_date >=  '" . $param['startDate'] . "' AND b.sched_date <= '" . $param['endDate'] . "' AND a.isReviewed = 1 GROUP BY b.sched_date ORDER BY b.sched_date DESC";

            $data = $this->set_datatable_query($datatable, $sql);

            $tdata = $this->_get_top_reviewers_by_dates($data['data']);

            if ($reviewType == 'top_reviews')
            {

                foreach ($tdata['top_early'] as $value)
                {
                    $early_reviewer = $this->general_model->custom_query($query . " AND a.reviewId = $value->reviewId");
                    $udata[] = $early_reviewer[0];
                }

                $sdata = $udata;
            }

            if ($reviewType == 'late_reviews')
            {

                foreach ($tdata['top_late'] as $value)
                {
                    $late_reviewer = $this->general_model->custom_query($query . " AND a.reviewId = $value->reviewId");
                    $udata[] = $late_reviewer[0];
                }

                $sdata = $udata;
            }
        }

        echo json_encode(['meta' => $data['meta'], 'data' => $sdata]);
    }

    public function get_top_reviewers()
    {
        $post = $this->input->post('params');
        $param = [
            'startDate' => $post['startDate'],
            'endDate' => $post['endDate'],
        ];

        $reviewers = [];

        $sql = "SELECT b.sched_date FROM tbl_szfive_survey_answers as a LEFT JOIN tbl_schedule as b ON b.sched_id = a.sched_id WHERE b.sched_date >=  '" . $param['startDate'] . "' AND b.sched_date <= '" . $param['endDate'] . "' AND a.isReviewed = 1 GROUP BY b.sched_date ORDER BY b.sched_date DESC";

        $sched_dates = $this->general_model->custom_query($sql);

        $data = $this->_get_top_reviewers_by_dates($sched_dates);

        if (!empty($data))
        {
            if (!empty($data['top_early']))
            {
                foreach ($data['top_early'] as $min_diff)
                {
                    $early_reviewer_ids[] = $min_diff->reviewerId;
                    $early_reviewer_diffs[] = $min_diff->diff;
                }

                // Get the number of occurences of each reviewer id
                $occurence_min = array_count_values($early_reviewer_ids);

                // If occurences of the reviewer ids are equal, get the reviewer id with the lowest time diff
                if (count(array_unique($occurence_min)) === 1)
                {

                    $lowest_diff = min($early_reviewer_diffs);
                    $early_reviewer_id = $early_reviewer_ids[array_search($lowest_diff, $early_reviewer_diffs)];
                }
                else
                {
                    arsort($occurence_min);
                    $early_reviewer_id = array_slice(array_keys($occurence_min), 0, 1, true)[0];
                }

                $query = $this->szfive_model->get_user_details(['uid' => $early_reviewer_id]);
                $early_reviewer = $this->general_model->custom_query($query);
                if (!empty($early_reviewer))
                {
                    $early_reviewer[0]->title = 'Top Early Reviewer';
                    $reviewers[] = $early_reviewer[0];
                }
            }

            if (!empty($data['top_late']))
            {

                foreach ($data['top_late'] as $max_diff)
                {
                    $late_reviewer_ids[] = $max_diff->reviewerId;
                    $late_reviewer_diffs[] = $max_diff->diff;
                }

                // Get the number of occurences of each reviewer id
                $occurence_max = array_count_values($late_reviewer_ids);

                // If occurences of the reviewer ids are equal, get the reviewer id with the lowest time diff
                if (count(array_unique($occurence_max)) === 1)
                {

                    $lowest_diff = max($late_reviewer_diffs);
                    $late_reviewer_id = $late_reviewer_ids[array_search($lowest_diff, $late_reviewer_diffs)];
                }
                else
                {
                    arsort($occurence_max);
                    $late_reviewer_id = array_slice(array_keys($occurence_max), 0, 1, true)[0];
                }

                $query = $this->szfive_model->get_user_details(['uid' => "'" . $late_reviewer_id . "'"]);
                $late_reviewer = $this->general_model->custom_query($query);
                if (!empty($late_reviewer))
                {
                    $late_reviewer[0]->title = 'Top Late Reviewer';

                    $reviewers[] = $late_reviewer[0];
                }
            }
        }

        echo json_encode(['data' => $reviewers]);
    }

    public function get_survey_list()
    {
        $survey_query = [
            'fields' => 'a.*, b.*',
            'table' => 'tbl_szfive_survey_details as a',
            'join' => [
                'tbl_szfive_survey_questions as b' => 'b.questionId = a.questionId',
            ],
        ];
        $surveys = $this->general_model->join_select($survey_query);

        foreach ($surveys as $survey)
        {
            $id[] = $survey->questionId;
        }

        $string = "(" . implode(",", $id) . ")";

        $questions = $this->general_model->custom_query("SELECT * FROM tbl_szfive_survey_questions WHERE questionId NOT IN $string");
        echo json_encode($questions);
    }

    public function get_question_list()
    {
        $questions = $this->general_model->custom_query("SELECT * FROM tbl_szfive_survey_questions");
        echo json_encode($questions);
    }

    public function get_all_choices()
    {
        $choices = $this->input->post('choices');
        $string = '';

        if (!empty($choices))
        {
            $string = "(" . implode(",", $choices) . ")";

            if ($string != '()')
            {
                $string = " WHERE choiceId NOT IN $string";
            }
            else
            {
                $string = '';
            }
        }

        $data = $this->general_model->custom_query("SELECT * FROM tbl_szfive_survey_choices $string");
        echo json_encode($data);
    }

    public function get_summary_report()
    {
        $param = $this->input->post('param');

        $data = $this->_get_summary_report_data($param);

        echo json_encode($data);
    }

    public function download_summary_report($start_date, $end_date, $user_type)
    {
        $param['startDate'] = $start_date;
        $param['endDate'] = $end_date;
        $param['user_type'] = 'Admin';
        $param['account_id'] = $this->session->userdata('szfive_account_id');

        $data = $this->_get_summary_report_data($param);
        $column_index = "A";
        $colum_limit = "ZZ";
        $last_col = '';
        // Initialize PHP Excel object
        $this->_set_excel_object();

        // Set header titles
        foreach ($data['dates'] as $cell)
        {
            $cell_key = $column_index . '7';
            $this->excel->getActiveSheet()->setCellValue($cell_key, $cell);
            $this->excel->getActiveSheet()->getStyle($cell_key)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle($cell_key)->getFont()->getColor()->setRGB('FFFFFF');
            $last_col = $column_index;
            $max_column[] = $last_col;
            $column_index++;
        }

        // Set each row data, starting from row number 6
        $row_num = 8;

        foreach ($data['data'] as $row)
        {
            $this->excel->getActiveSheet()->setCellValue('A' . $row_num, $row['title']);
            $column_index = "B";
            foreach ($row['data'] as $row_data)
            {
                $row_key = $column_index . $row_num;
                $this->excel->getActiveSheet()->setCellValue($row_key, $row_data);
                $column_index++;
            }
            $row_num++;
        }

        // Auto size for each column and align text on the center until the last
        // Except column 'A'
        $last_row = $row_num - 1;
        unset($max_column[0]);
        foreach ($max_column as $col)
        {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($col . '7:' . $col . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        // Set the width for column A which contains the logo and row descriptions
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(50);

        // Format start and end dates
        $date = new DateTime($param['startDate']);
        $from = $date->format('F d, Y');
        $date = new DateTime($param['endDate']);
        $to = $date->format('F d, Y');
        $str_account = '';
        if (!empty($param['account_id']))
        {

            $accounts = $this->general_model->custom_query("SELECT acc_id, acc_name FROM tbl_account WHERE acc_id IN (" . implode(",", $param['account_id']) . ")");

            foreach ($accounts as $account)
            {
                $str_account .= $account->acc_name . ', ';
            }
        }
        else
        {
            $str_account = 'All';
        }

        if ($user_type == 'all')
        {
            $user_type = 'Ambassadors & Admins';
        }
        if ($user_type == 'Agent')
        {
            $user_type = "Ambassadors";
        }

        // Set the file's title above the row data
        $this->excel->getActiveSheet()->setCellValue('B2', 'SZFive / SZhoutOuts Summary Report');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Employee: ' . $user_type);
        $this->excel->getActiveSheet()->setCellValue('B4', 'Department/Account: ' . $str_account);
        $this->excel->getActiveSheet()->setCellValue('B5', 'Date: ' . $from . ' - ' . $to);

        // Merge cells
        $this->excel->getActiveSheet()->mergeCells('B2:' . $last_col . '2');
        $this->excel->getActiveSheet()->mergeCells('B3:' . $last_col . '3');
        $this->excel->getActiveSheet()->mergeCells('B4:' . $last_col . '4');
        $this->excel->getActiveSheet()->mergeCells('B5:' . $last_col . '5');
        $this->excel->getActiveSheet()->getStyle('B4:B' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getRowDimension(4)->setRowHeight(30);

        $styleArray = array('font' => array('bold' => true, 'size' => 16, 'name' => 'Verdana'), 'alignment' => array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ));

        $this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('B3')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('B4')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('B5')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('B3')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('B4')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('B5')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('A7:' . $cell_key)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');

        $borderStyle = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => 'DDDDDD'))));

        $this->excel->getActiveSheet()->getStyle('A7:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray($borderStyle);

        $this->excel->getActiveSheet()->freezePane('B1');
        // $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        // $this->excel->getActiveSheet()->getProtection()->setSheet(true);

        $filename = 'SZFive_Reporting_' . $param['startDate'] . '_to_' . $param['endDate'] . '.xlsx';
        // header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
    }

    /**
     * @return SZFive comments page
     */
    public function comments()
    {
        $user_details = $this->general_model->custom_query($this->szfive_model->get_user_details(['uid' => $this->userdata['uid']]));

        $data['user_details'] = $user_details[0];
        $data['uri_segment'] = $this->uri->segment_array();
        $data['title'] = $this->title . ' - Home';
        $data['nav_list'] = $this->_navigation_bar_list('Home');


        $this->load_template_view('templates/szfive/home/comments', $data);
    }

    /**
     * Get all comments sent to current user
     * 
     * @return void
     */
    public function get_high_five_comments()
    {
        $datatable = $this->input->post('datatable');
        $dt_query = $this->input->post('query');

        if ($dt_query['isPublic'] == 0)
        {

            $query['query'] = "SELECT a.id as commentId, a.parentId, a.senderId, a.content, a.dateCreated, a.isPublic, b.id, b.recipientId, CONCAT(c.fname, ' ', c.lname) as sender, f.pic as senderPic, a.notificationId, h.link, h.message FROM tbl_szfive_comments AS a LEFT JOIN tbl_szfive_comments_private AS b ON b.id = a.id LEFT JOIN tbl_szfive_high_fives as g ON g.id = a.parentId LEFT JOIN tbl_system_notification as h ON h.systemNotification_ID = a.notificationId LEFT JOIN tbl_user as c ON c.uid = a.senderId 
            JOIN tbl_employee as d ON d.emp_id = c.emp_id JOIN tbl_applicant as f ON f.apid = d.apid  WHERE a.type = 'sz_highfive' AND b.recipientId = " . $this->userdata['uid'] . " AND a.isPublic = " . $dt_query['isPublic'] . " ORDER BY a.dateCreated DESC";
        }
        else
        {
            $query['query'] = "SELECT a.id as commentId, a.parentId, a.senderId, a.content, a.dateCreated, a.isPublic, a.notificationId, c.recipient_ID, CONCAT(e.fname, ' ', e.lname) as sender, f.pic as senderPic, h.link, h.message FROM tbl_szfive_comments AS a LEFT JOIN tbl_szfive_high_fives as b ON b.id = a.parentId JOIN tbl_system_notification_recipient as c ON c.systemNotification_ID = a.notificationId LEFT JOIN tbl_system_notification as h ON h.systemNotification_ID = c.systemNotification_ID LEFT JOIN tbl_user as e ON e.uid = a.senderId JOIN tbl_employee as d ON d.emp_id = e.emp_id JOIN tbl_applicant as f ON f.apid = d.apid WHERE a.type = 'sz_highfive' AND c.recipient_ID = " . $this->userdata['uid'] . " AND a.isPublic = " . $dt_query['isPublic'] . " ORDER BY a.dateCreated DESC";
        }

        $data = $this->set_datatable_query($datatable, $query);

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function view_highfive_comment($emp_id = null, $highfive_id = null)
    {
        $param = [
            'emp_id' => $emp_id,
            'highfive_id' => $highfive_id,
            'isPublic' => 0
        ];

        $this->_get_highfive_enty_comments($param);
    }

    public function get_accounts()
    {
        $param = $this->input->post('param');

        $query = "SELECT acc_id, acc_name FROM tbl_account";

        if ($param['user_type'] != 'all')
        {
            $query .= " WHERE acc_description = " . "'" . $param['user_type'] . "'  ORDER BY acc_name ASC";
        }

        $accounts = $this->general_model->custom_query($query);

        echo json_encode($accounts);
    }

    #================================
    # PRIVATE & PROTECTED METHODS
    #================================

    /**
     * @return Session user data
     */
    protected function _get_userdata()
    {
        $this->userdata = [
            'uid' => $this->session->userdata('uid'),
            'role_id' => $this->session->userdata('role_id'),
            'emp_id' => $this->session->userdata('emp_id'),
        ];
    }

    private function _survey_actions()
    {
        $actions = [
            'viewSurveys' => ['View Surveys', 0],
            'viewQuestions' => ['View Questions', 0],
            'viewChoices' => ['View Choices', 0],
            'editSurveys' => ['Edit Surveys', 0],
            'editQuestions' => ['Edit Questions', 0],
            'editChoices' => ['Edit Choices', 0],
            'addSurveys' => ['Add Surveys', 0],
            'addQuestions' => ['Add Questions', 0],
            'addChoices' => ['Add Choices', 0],
            'viewOwnAnswers' => ['View Own Answers', 0],
            'viewTeamAnswers' => ['View Team Answers', 0],
        ];

        return $actions;
    }

    private function _time_elapsed_string($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );

        foreach ($string as $k => &$v)
        {
            if ($diff->$k)
            {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            }
            else
            {
                unset($string[$k]);
            }
        }

        if (!$full)
        {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string) . ' ago' : 'Just now';
    }

    private function _download_report($query, $var)
    {
        // $logo = 'C:\Users\SDT-Programmer\Sites\sz\assets\images\img\logo2.png';
        $logo = $this->dir . '/assets/images/img/logo2.png';

        $col_data = $this->general_model->custom_query($query);
		foreach ($col_data as $key => $value)
        {
            $value->emp_stat = $this->getEmpStat($value->emp_id);
            $supervisor = $this->getSupervisor($value->emp_id);
            $value->supervisor = $supervisor->lname.', '.$supervisor->fname;
            $value->supEmail = $supervisor->email;
            $value->dateSubmitted = $this->_time_elapsed_string($value->date1);
        }
        $this->load->library('PHPExcel', null, 'excel');

        $this->excel->setActiveSheetIndex(0);
        $this->excel->createSheet(1);
        $this->excel->getActiveSheet()->setTitle('Survey Report');

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($logo);
        $objDrawing->setOffsetX(10); // setOffsetX works properly
        $objDrawing->setOffsetY(10); //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(70); // logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $this->excel->getActiveSheet()->setShowGridlines(false);

        $cells = [
            ['col' => '', 'id' => 'D2', 'title' => 'Survey:'],
            ['col' => '', 'id' => 'D3', 'title' => 'Duration:'],
            ['col' => '', 'id' => 'D4', 'title' => 'Average / Percent:'],
            ['col' => 'A', 'id' => 'A6', 'title' => 'ID No.'],
            ['col' => 'B', 'id' => 'B6', 'title' => 'Lastname'],
            ['col' => 'C', 'id' => 'C6', 'title' => 'Firstname'],
            ['col' => 'D', 'id' => 'D6', 'title' => 'Answer'],
            ['col' => 'E', 'id' => 'E6', 'title' => 'Label'],
            ['col' => 'F', 'id' => 'F6', 'title' => 'Position'],
            ['col' => 'G', 'id' => 'G6', 'title' => 'Account'],
            ['col' => 'H', 'id' => 'H6', 'title' => 'Description'],
            ['col' => 'I', 'id' => 'I6', 'title' => 'Status'],
            ['col' => 'J', 'id' => 'J6', 'title' => 'Supervisor'],
            ['col' => 'K', 'id' => 'K6', 'title' => 'Date Answered'],
            ['col' => 'L', 'id' => 'L6', 'title' => 'Comments'],
        ];

        $duplicate = $cells;
        unset($duplicate[0], $duplicate[1], $duplicate[2]);

        foreach ($cells as $cell)
        {
            $this->excel->getActiveSheet()->setCellValue($cell['id'], $cell['title']);
            $this->excel->getActiveSheet()->getStyle($cell['id'])->getFont()->setBold(true);
        }

        foreach ($duplicate as $dup)
        {
            $this->excel->getActiveSheet()->getStyle($dup['id'])->getFont()->getColor()->setRGB('FFFFFF');
        }

        $first_data = $col_data[0];
        $date = new DateTime($var['start_date']);
        $from = $date->format('F d, Y');
        $date = new DateTime($var['end_date']);
        $to = $date->format('F d, Y');
        $duration = $from . ' - ' . $to;
        $percent = ($var['avg'] / $first_data->numberOfChoices) * 100 . '%';
        $col = $col_data;
        $row = 7;
        $last_row = count($col_data) + $row;

        foreach ($col as $data)
        {
            $labels = json_decode($data->choiceLabels);
            $i = $data->answer - 1;

            $this->excel->getActiveSheet()->setCellValue('A' . $row, $data->emp_id);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $data->lastname);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, $data->firstname);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $data->answer . '/' . $data->numberOfChoices);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, $labels[$i]);
            $this->excel->getActiveSheet()->setCellValue('F' . $row, $data->pos_details);
            $this->excel->getActiveSheet()->setCellValue('G' . $row, $data->acc_name);
            $this->excel->getActiveSheet()->setCellValue('H' . $row, $data->acc_description);
            $this->excel->getActiveSheet()->setCellValue('I' . $row, $data->emp_stat);
            $this->excel->getActiveSheet()->setCellValue('J' . $row, $data->supervisor);
            $this->excel->getActiveSheet()->setCellValue('K' . $row, $data->date1);
            $this->excel->getActiveSheet()->setCellValue('L' . $row, $data->detailedAnswer);
            $row++;
        }

        $this->excel->getActiveSheet()->getStyle('B6:B' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('C6:C' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('F6:F' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('G6:G' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('H6:H' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('I6:I' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('J6:J' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('K6:K' . $this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

        // Set column data alignments
        $this->excel->getActiveSheet()->getStyle('A6:A' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('D6:D' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E6:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $this->excel->getActiveSheet()->getStyle('L6:L' . $last_row)->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('D4:D4')->getAlignment()->setWrapText(true);

        // Set column values
        $this->excel->getActiveSheet()->setCellValue('E2', $first_data->question);
        $this->excel->getActiveSheet()->setCellValue('E3', $duration);
        $this->excel->getActiveSheet()->setCellValue('E4', $var['avg'] . '/' . $first_data->numberOfChoices . ' (' . $percent . ')');

        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(60);
        $this->excel->getActiveSheet()->getStyle('A6:L6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');

        $this->excel->getActiveSheet()->getStyle('A6:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'DDDDDD'),
                    ),
                ),
            )
        );
        $this->excel->getActiveSheet()->freezePane('D1');
        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);

        $filename = 'Survey__' . $var['start_date'] . '_' . $var['end_date'] . '.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
    }

    private function _loop_users($users)
    {
        $udata = array();
        $ndata = array();
        $subdata = array();

        foreach ($users as $user)
        {

            $subordinates = $this->general_model->fetch_specific_vals('emp_id', ['Supervisor' => $user->emp_id, 'isActive' => 'yes'], 'tbl_employee', null);

            if (!empty($subordinates))
            {

                foreach ($subordinates as $value)
                {

                    $subdata[] = $value;
                }
                $this->_loop_users($subordinates);
            }
        }

        foreach ($subdata as $data)
        {
            if (!empty($data))
            {
                foreach ($data as $mdata)
                {
                    $this->count_users[] = $mdata;
                }
            }
        }

        return $this->count_users;
    }

    private function _has_subordinates()
    {
        $current_user = $this->general_model->fetch_specific_val('*', ['uid' => $this->userdata['uid']], 'tbl_user');
        // Get users supervised by the current user
        $subordinates = $this->general_model->fetch_specific_vals('emp_id', ['Supervisor' => $current_user->emp_id, 'isActive' => 'yes'], 'tbl_employee', null);

        if (!empty($subordinates))
        {
            $data['has_subordinates'] = true;
            $data['data'] = $subordinates;
        }
        else
        {
            $data['has_subordinates'] = false;
            $data['data'] = [];
        }

        return $data;
    }

    private function _get_subordinates($subordinates)
    {
        $ids = [];

        foreach ($subordinates as $key => $value)
        {
            $users[] = $value->emp_id;
        }

        $subordinates = $this->_loop_users($subordinates);
        $ids = array_merge($users, $subordinates);

        return $ids;
    }

    private function _get_mentioned_users($value)
    {
        $uid = $this->userdata['uid'];
        $value->dateSubmitted = $this->_time_elapsed_string($value->dateSubmitted);

        $patterns = '/@(\w+)/';

        $replacements = '<span class="user-mention">@${1}</span>';

        if ($value->noOfRecepients > 1)
        {
            $value->header_msg = $value->noOfRecepients . ' people';
            $value->moreThanOne = true;
            $value->hi5recepient = $value->noOfRecepients;
        }
        else
        {

            $highfive = $this->general_model->custom_query("SELECT recipientId FROM tbl_szfive_high_five_details WHERE id = " . $value->id);
            $query = $this->szfive_model->get_user_details(['uid' => $highfive[0]->recipientId]);
            $recipient_data = $this->general_model->custom_query($query);
            $value->header_msg = $recipient_data[0]->name;
            $value->moreThanOne = false;
            $value->hi5recepient = $recipient_data[0]->avatar;
            $value->hi5recepientId = $recipient_data[0]->emp_id;
        }

        $mention = preg_replace($patterns, $replacements, $value->message);
        $value->message = $mention;
        $value->sessionId = $uid;

        // Get High Five reactions/likes
        $likes = $this->_get_highfive_likes($value, ['hiFiveId' => $value->hiFiveId, 'type' => 'sz_highfive']);
        $value = $likes;

        return $value;
    }

    private function _check_week_end($curtime = null)
    {
        $date_array = getdate(time());
        $numdays = $date_array["wday"];

        $this_week_startdate = date("Y-m-d", time() - (($numdays - 1) * 24 * 60 * 60));
        $this_week_enddate = date("Y-m-d", time() + ((7 - $numdays) * 24 * 60 * 60));

        $last_week_startdate = date("Y-m-d", time() - (($numdays + 6) * 24 * 60 * 60));
        $last_week_enddate = date("Y-m-d", time() - (($numdays) * 24 * 60 * 60));

        $week = [
            'this_week_startdate' => $this_week_startdate,
            'this_week_enddate' => $this_week_enddate,
            'last_week_startdate' => $last_week_startdate,
            'last_week_enddate' => $last_week_enddate,
        ];

        return $week;
    }

    private function _datetime_difference($date)
    {
        $start = new DateTime($date['start']);
        $end = new DateTime($date['end']);
        $diff = $start->diff($end);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );

        foreach ($string as $k => &$v)
        {
            if ($diff->$k)
            {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            }
            else
            {
                unset($string[$k]);
            }
        }

        return $string ? implode(', ', $string) . '' : 'Just now';
    }

    private function _get_query_by_review_type($datatable, $query)
    {
        $queries = ['dates' => '', 'search' => '', 'where' => '', 'order' => '', 'append' => '', 'query' => ''];

        if ($query['reviewType'] != 'all')
        {

            $reviewType = $query['reviewType'];

            if ($reviewType == 'top_reviews')
            {
                $sort = ' ASC';
            }if ($reviewType == 'late_reviews')
            {
                $sort = ' DESC';
            }

            $queries['query'] = $this->szfive_model->get_review_details(['num' => 0, 'sort' => $sort]);

            if ($query['startDate'] != "" or $query['endDate'] != "")
            {
                $queries['where'] = " WHERE subTable.sched_date >=  '" . $query['startDate'] . "' AND subTable.sched_date <= '" . $query['endDate'] . "' AND subTable.isReviewed = 1";
            }
            else
            {
                $queries['where'] = " WHERE subTable.isReviewed = 1";
            }

            $queries['order'] = " GROUP BY sched_date";

            if (!isset($datatable['sort']['field']))
            {
                $queries['order'] .= "  ORDER BY sched_date DESC";
            }
        }
        else
        {
            $queries['query'] = $this->szfive_model->get_review_details(['num' => 0, 'sort' => 'ASC']);

            if ($query['startDate'] != "" or $query['endDate'] != "")
            {
                $queries['where'] = " WHERE subTable.sched_date >=  '" . $query['startDate'] . "' AND subTable.sched_date <= '" . $query['endDate'] . "' AND subTable.isReviewed = 1";
            }
            else
            {
                $queries['where'] = " WHERE subTable.isReviewed = 1";
            }

            if (!isset($datatable['sort']['field']))
            {
                $queries['order'] = " ORDER BY sched_date DESC";
            }
        }

        if (isset($query['detailsReviewSearch']))
        {
            $keyword = $query['detailsReviewSearch'];
            $queries['search'] = " (b.fname LIKE '%$keyword%' OR b.lname LIKE '%$keyword%' OR e.fname LIKE '%$keyword%' OR e.lname LIKE '%$keyword%')";

            if ($queries['where'] != '')
            {
                $queries['search'] = " AND " . $queries['search'];
            }
            else
            {
                $queries['search'] = " WHERE " . $queries['search'];
            }
        }

        return $queries;
    }

    private function _get_this_week_date_range()
    {
        $sunday = strtotime("last sunday");
        $sunday = date('w', $sunday) == date('w') ? $sunday + 7 * 86400 : $sunday;
        $monday = strtotime(date("Y-m-d", $sunday) . " +6 days");
        $this_week_startday = date("M j, Y", $sunday);
        $this_week_endday = date("M j, Y", $monday);

        return $this_week_startday . ' - ' . $this_week_endday;
    }

    private function _get_top_reviewers_by_dates($sched_dates)
    {
        if (!empty($sched_dates))
        {
            foreach ($sched_dates as $sched_date)
            {
                // get lowest response time => EARLY REVIEWER
                $min_diff_query = "SELECT a.reviewId, a.reviewerId, c.sched_date, TIME_TO_SEC(TIMEDIFF(a.dateUpdated, b.dateCreated)) as diff FROM tbl_szfive_reviews as a JOIN tbl_szfive_survey_answers as b ON b.answerId = a.answerId LEFT JOIN tbl_schedule as c ON c.sched_id = b.sched_id WHERE c.sched_date = '$sched_date->sched_date' ORDER BY diff ASC LIMIT 1";

                $max_diff_query = "SELECT a.reviewId, a.reviewerId, c.sched_date, TIME_TO_SEC(TIMEDIFF(a.dateUpdated, b.dateCreated)) as diff FROM tbl_szfive_reviews as a JOIN tbl_szfive_survey_answers as b ON b.answerId = a.answerId LEFT JOIN tbl_schedule as c ON c.sched_id = b.sched_id WHERE c.sched_date = '$sched_date->sched_date' ORDER BY diff DESC LIMIT 1";

                $query = $this->db->query($min_diff_query);
                $top_early[] = $query->row();

                $query = $this->db->query($max_diff_query);
                $top_late[] = $query->row();
            }

            $data = [
                'top_early' => $top_early,
                'top_late' => $top_late,
            ];
        }
        else
        {
            $data = [];
        }
        return $data;
    }

    private function _get_all_highfive_posts($param)
    {
        // $high_five_details = $this->general_model->custom_query($query . $order_by . $limit);

        $h_query = $this->szfive_model->get_highfive();
        $query = $h_query['query'];
        $limit = " LIMIT " . $param['offset'] . " ," . $param['noOfItems'];

        if ($param['selectedPerson'] == 'company_wide')
        {

            $order_by = " WHERE a.isPublic = 1 ORDER BY a.dateCreated DESC";
        }
        else
        {
            if ($param['selectedType'] == 'all')
            {
                $order_by = " LEFT JOIN tbl_szfive_high_five_details as k ON k.id = a.id WHERE a.isPublic = 1 AND b.uid = " . $param['selectedPerson'] . " OR k.recipientId = " . $param['selectedPerson'] . "  GROUP BY a.id ORDER BY a.dateCreated DESC";
            }
            else if ($param['selectedType'] == 'recieved_by')
            {
                $order_by = " LEFT JOIN tbl_szfive_high_five_details as k ON k.id = a.id WHERE a.isPublic = 1 AND k.recipientId = " . $param['selectedPerson'] . " GROUP BY a.id ORDER BY a.dateCreated DESC";
            }
            else
            { // selected type is `given_by`
                $order_by = " WHERE a.isPublic = 1 AND a.senderId = " . $param['selectedPerson'] . " GROUP BY a.id ORDER BY a.dateCreated DESC";
            }
        }

        $data = $this->general_model->custom_query($query . $order_by . $limit);

        $count = count($this->general_model->custom_query($query . $order_by));

        $newdata = [];

        foreach ($data as $value)
        {
            $value = $this->_get_mentioned_users($value);
            $value->totalComment = $this->general_model->fetch_specific_val('count(*) as count', ['parentId' => $value->hiFiveId, 'type' => 'sz_highfive', 'isPublic' => 1], 'tbl_szfive_comments');

            array_push($newdata, $value);
        }

        $h_data = ['data' => $newdata, 'count' => $count];

        return $h_data;
    }

    private function _get_highfive_likes($value, $param)
    {
        $highfive_likes = $this->general_model->custom_query("
            SELECT a.uid, a.reactionId, e.fname, e.lname, c.name as reaction 
            FROM tbl_szfive_likes as a 
            LEFT JOIN tbl_user AS b ON b.uid = a.uid 
            LEFT JOIN tbl_szfive_reactions AS c ON c.reactionId = a.reactionId 
            LEFT JOIN tbl_employee d ON b.emp_id = d.emp_id
            LEFT JOIN tbl_applicant e ON d.apid=e.apid
            WHERE a.szfiveId = " . $param['hiFiveId'] . " AND a.type = '" . $param['type'] . "' 
            ORDER BY likeId DESC");

        $total_likes = count($highfive_likes);
        $value->tooltip = '';

        if ($total_likes != 0)
        {
            foreach ($highfive_likes as $val)
            {
                $uids[] = $val->uid;
                $reaction_ids[] = $val->reactionId;
            }

            if ($total_likes == 1)
            {
                $value->likes_info = $highfive_likes[0]->fname . ' ' . $highfive_likes[0]->lname;
                $value->tooltip = $highfive_likes[0]->fname . ' ' . $highfive_likes[0]->lname;
            }
            else if ($total_likes == 2)
            {
                $value->likes_info = $highfive_likes[0]->fname . ' ' . $highfive_likes[0]->lname . ' and ' . $highfive_likes[1]->fname . ' ' . $highfive_likes[1]->lname;
                $value->tooltip = $highfive_likes[0]->fname . ' ' . $highfive_likes[0]->lname . ' <br> ' . $highfive_likes[1]->fname . ' ' . $highfive_likes[1]->lname;
            }
            else
            {
                $total_likes = $total_likes - 1;
                $value->likes_info = $highfive_likes[0]->fname . ' ' . $highfive_likes[0]->lname . " and $total_likes others";

                foreach ($highfive_likes as $highfive_like)
                {
                    $value->tooltip .= $highfive_like->fname . ' ' . $highfive_like->lname . '<br>';
                }
            }

            $reaction_str = '';
            $reactions = [];
            foreach ($highfive_likes as $highfive)
            {
                if (!isset($reactions[$highfive->reaction]))
                {
                    $reactions[$highfive->reaction] = $highfive->reaction;
                    $reaction_str .= '<i class="' . strtolower($highfive->reaction) . 'IconSmall likeTypeSmall"></i>';
                }
            }
            $value->reactions = $reaction_str;
        }
        else
        {
            $value->likes_info = '';
            $value->reactions = '';
        }

        $my_highfive_like = $this->general_model->fetch_specific_vals("*", ['szfiveId' => $param['hiFiveId'], 'type' => $param['type'], 'uid' => $this->userdata['uid']], 'tbl_szfive_likes', 'likeId DESC');

        if (!empty($my_highfive_like))
        {
            $my_reaction = $this->general_model->custom_query("SELECT DISTINCT(name) FROM tbl_szfive_reactions WHERE reactionId = " . $my_highfive_like[0]->reactionId);
            $value->my_reaction = '<i class="' . strtolower($my_reaction[0]->name) . 'IconSmall likeTypeSmall defaultIcon"></i> ' . $my_reaction[0]->name;
            $value->reaction_name = strtolower($my_reaction[0]->name);
        }
        else
        {
            $value->my_reaction = '';
            $value->reaction_name = '';
        }
        return $value;
    }

    private function _get_entry_details($qdata)
    {
        $data = $qdata[0];

        $query = $this->szfive_model->get_user_details(['uid' => $data->uid]);
        $userdata = $this->general_model->custom_query($query);

        $data->fullname = $userdata[0]->firstname . ' ' . $userdata[0]->lastname;
        $data->pic = $userdata[0]->avatar;
        $data->position = $userdata[0]->pos_details;
        $data->department = $userdata[0]->dep_name . ' | ' . $userdata[0]->dep_details;

        if ($data->isReviewed == 1)
        {
            $dayOfWeek = date('l', strtotime($data->dateReviewed));
            $formatDate = DateTime::createFromFormat('Y-m-d H:i:s', $data->dateReviewed)->format('F j');

            $data->date_reviewed = $dayOfWeek . ', ' . $formatDate;
        }
        else
        {
            $data->date_reviewed = '';
        }

        $data->session_id = $this->userdata['uid'];

        $adata['entry'] = $data;

        return $adata;
    }

    private function _get_comments($param)
    {
        $query = "SELECT a.dateCreated as postDate, a.id as commentId, a.content, b.*, e.pic,e.fname,e.lname, a.isPublic FROM tbl_szfive_comments as a JOIN tbl_user as b ON b.uid = a.senderId JOIN tbl_employee as c ON c.emp_id = b.emp_id JOIN tbl_account as d ON d.acc_id = c.acc_id JOIN tbl_applicant as e ON e.apid = c.apid WHERE a.parentId = " . $param['parent_id'] . " AND a.type = " . "'" . $param['type'] . "'  AND a.isPublic = " . $param['isPublic'] . " ORDER BY " . $param['sort'] . " LIMIT " . $param['offset'] . ", " . $param['no_of_items'];

        $data = $this->general_model->custom_query($query);
        $pattern = '/@(\w+)/';
        $replacement = '<span class="user-mention">@${1}</span>';

        foreach ($data as $value)
        {
            $value->dateCreatedRaw = $value->postDate;
            $value->dateCreated = $this->_time_elapsed_string($value->postDate);
            $value->content = $comment = preg_replace($pattern, $replacement, $value->content);
        }

        return $data;
    }

    private function _sent_to_users($entry, $child_comments)
    {
        $sent_to_users = []; // Array that holds the users for a private comment

        if ($entry->uid != $this->userdata['uid'])
        {
            $sent_to_users[$entry->uid] = $entry->fullname;
        }

        if ($entry->supId != $this->userdata['emp_id'])
        {
            $reviewerdata = $this->general_model->fetch_specific_val('uid', ['emp_id' => $entry->supId], 'tbl_user');
            if (!empty($reviewerdata))
            {
                $sent_to_users[$reviewerdata->uid] = $entry->supervisorFname . ' ' . $entry->supervisorLname;
            }
        }

        foreach ($child_comments as $user)
        {
            if (!isset($sent_to_users[$user->uid]))
            {
                $sent_to_users[$user->uid] = $user->fname . ' ' . $user->lname;
            }
        }

        unset($sent_to_users[$this->userdata['uid']]);

        return $sent_to_users;
    }

    private function _get_comment_notification($param)
    {
        $emp_id = $this->session->userdata('emp_id');
        $emp_name = $this->session->userdata('fname') . ' ' . substr($this->session->userdata('lname'), 0, 1) . '.';
        $recipientId = $param['recipient_id'];
        $comment_id = $param['comment_id'];

        $highfive_data = $this->general_model->custom_query("SELECT a.senderId, b.emp_id FROM tbl_szfive_high_fives as a LEFT JOIN tbl_user as b ON b.uid = a.senderId WHERE a.id = " . $param['parent_id']);

        $comment_recipient[] = ['userId' => $highfive_data[0]->senderId];
        $notif_ids = [];

        if ($recipientId != 0)
        {
            if ($highfive_data[0]->emp_id != $emp_id)
            {
                if ($param['ispublic'] == 0)
                { // private comment
                    $this->general_model->insert_vals(['id' => $comment_id, 'recipientId' => $recipientId], 'tbl_szfive_comments_private');
                    $message = '<b>' . $emp_name . '</b> gave a private comment on your High Five: <br>' . substr($param['content'], 0, 50);
                    $link = 'szfive/highfive/private_comment/' . $highfive_data[0]->emp_id . '/' . $param['parent_id'] . '#user-comment-' . $comment_id;
                    $comment_notif = $this->set_system_notif_preceeding($message, $link, $comment_recipient);
                }
                else
                {
                    $message = '<b>' . $emp_name . '</b> commented on your High Five: <br>' . substr($param['content'], 0, 50);
                    $link = 'szfive/highfive/view/' . $highfive_data[0]->emp_id . '/' . $param['parent_id'] . '#user-comment-' . $comment_id;
                    $comment_notif = $this->set_system_notif_preceeding($message, $link, $comment_recipient);
                }

                $this->general_model->update_vals(['notificationId' => $comment_notif['notif_id'][0]], ['id' => $comment_id], 'tbl_szfive_comments');
                $notif_ids[] = $comment_notif['notif_id'][0];
            }
            else
            {
                if ($param['ispublic'] == 0)
                { // private comment
                    $this->general_model->insert_vals(['id' => $comment_id, 'recipientId' => $recipientId], 'tbl_szfive_comments_private');
                    $message = '<b>' . $emp_name . '</b> gave you a private comment: <br>' . substr($param['content'], 0, 50);
                    $link = 'szfive/highfive/private_comment/' . $highfive_data[0]->emp_id . '/' . $param['parent_id'] . '#user-comment-' . $comment_id;
                    $comment_notif = $this->set_system_notif_preceeding($message, $link, [0 => ['userId' => $recipientId]]);

                    $this->general_model->update_vals(['notificationId' => $comment_notif['notif_id'][0]], ['id' => $comment_id], 'tbl_szfive_comments');
                    $notif_ids[] = $comment_notif['notif_id'][0];
                }
            }
        }

        // Send notification to mentioned users
        if ($param['usermention'] != 0)
        {
            foreach ($param['usermention'] as $user)
            {
                $mention_recipients[] = ['userId' => $user['id']];
            }

            $message = '<b>' . $emp_name . '</b> mentioned you in a comment: <br>' . substr($param['content'], 0, 50);
            $mention_notif = $this->set_system_notif_preceeding($message, $link, $mention_recipients);

            // Add the data of the usermentions from the comment
            $this->general_model->insert_vals(['parentId' => $comment_id, 'type' => 'szfive_comment', 'notificationId' => $mention_notif['notif_id'][0], 'senderId' => $this->userdata['uid']], 'tbl_szfive_usermentions');

            $this->general_model->update_vals(['notificationId' => $mention_notif['notif_id'][0]], ['id' => $comment_id], 'tbl_szfive_comments');
            $notif_ids[] = $mention_notif['notif_id'][0];
        }

        return $notif_ids;
    }

    private function _get_answer_detail_query($param)
    {
        $query = "SELECT j.pos_name, j.pos_details, a.*, DATE_FORMAT(a.dateCreated, '%b %e') as date, DATE_FORMAT(a.dateCreated, '%M %e, %Y %r') as dateAnswered, a.dateCreated as answeredDate, CONCAT(e.fname, ' ', e.lname) as user_name, b.role as position, c.Supervisor, c.acc_id, c.apid, email, (SELECT email FROM tbl_applicant y,tbl_employee z WHERE y.apid=z.apid AND z.emp_id = c.Supervisor) as supEmail, c.isActive, b.emp_id, d.*, e.pic, CONCAT(e.fname,' ',e.lname) as supervisor, g.*, k.sched_date, k.sched_id, m.link
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_user as b ON b.uid = a.employeeId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
        LEFT JOIN tbl_applicant as e ON e.apid = c.apid
        LEFT JOIN tbl_user as f ON f.emp_id = c.Supervisor
        LEFT JOIN tbl_szfive_survey_details as g ON g.surveyId = a.surveyId
        LEFT JOIN tbl_emp_promote as h ON (h.emp_id = c.emp_id AND h.isActive = 1)
        LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = h.posempstat_id)
        LEFT JOIN tbl_position as j ON (j.pos_id = i.pos_id)
        LEFT JOIN tbl_schedule as k ON k.sched_id = a.sched_id
        LEFT JOIN tbl_szfive_reviews as l ON l.answerId = a.answerId
        LEFT JOIN tbl_system_notification as m ON m.systemNotification_ID = l.notificationId
        WHERE k.sched_date >=  " . "'" . $param['start_date'] . "'" . " AND k.sched_date <= " . "'" . $param['end_date'] . "'" . " AND a.surveyId = " . $param['survey_id'] . " AND a.sched_id != 0
        ";

        if ($this->userdata['role_id'] != 1)
        {

            $data = $this->_has_subordinates();

            if ($data['has_subordinates'] == false)
            {
                $query .= " AND a.employeeId = " . $this->userdata['uid'];
            }
            else
            {
                $subordinates = $this->_get_subordinates($data['data']);
                $string = "(" . implode(",", $subordinates) . ")";
                $query .= " AND (b.emp_id IN $string OR a.employeeId = " . $this->userdata['uid'] . ")";
            }
        }

        return $query;
    }

    private function _get_menu_item($param)
    {
        $menu_item = $this->general_model->custom_query("SELECT menu_item_id FROM tbl_menu_items WHERE item_link = " . "'" . $param['link'] . "'");
        $menu_item_id = $menu_item[0]->menu_item_id;
        $user_id = $this->userdata['uid'];
        $menu_access = $this->general_model->custom_query("SELECT useraccess_id FROM tbl_user_access WHERE menu_item_id = $menu_item_id AND user_id = $user_id");

        return $menu_access;
    }

    private function _navigation_bar_list($page)
    {
        $navs[0] = ['title' => 'Home', 'link' => 'szfive'];
        $navs[1] = ['title' => 'SZhoutOuts', 'link' => 'szfive/high_fives'];

        $sub_data = $this->_has_subordinates();

        if ($this->userdata['role_id'] == 1)
        {
            $sub_data['has_subordinates'] = true;
        }

        if ($sub_data['has_subordinates'] == false)
        {
            if (!empty($this->_get_menu_item(['link' => 'szfive/daily_survey'])))
            {
                $navs[2] = ['title' => 'Daily Survey', 'link' => 'szfive/daily_survey'];
            }

            if (!empty($this->_get_menu_item(['link' => 'szfive/reviews'])))
            {
                $navs[3] = ['title' => 'Reviews', 'link' => 'szfive/reviews'];
            }
        }
        else
        {
            $navs[2] = ['title' => 'Daily Survey', 'link' => 'szfive/daily_survey'];
            $navs[3] = ['title' => 'Reviews', 'link' => 'szfive/reviews'];
        }

        if (!empty($this->_get_menu_item(['link' => 'szfive/reporting'])))
        {
            $navs[4] = ['title' => 'Reporting', 'link' => 'szfive/reporting'];
        }

        foreach ($navs as $nav)
        {
            if ($nav['title'] == $page)
            {
                $active = 'szfive-nav-item--active';
            }
            else
            {
                $active = '';
            }

            $list[] = array_merge($nav, ['active' => $active]);
        }

        return $list;
    }

    /**
     * Generate an array of string dates between 2 dates
     *
     * @param string $start Start date
     * @param string $end End date
     * @param string $format Output format (Default: Y-m-d)
     *
     * @return array
     */
    private function _get_dates_from_range($start, $end, $format = 'Y-m-d')
    {
        $array = array();
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        foreach ($period as $date)
        {
            $array[] = $date->format($format);
        }

        return $array;
    }

    private function _set_excel_object()
    {
        $logo = 'C:\Users\SDT-Programmer\Sites\sz\assets\images\img\logo2.png';
        // $logo = $this->dir . '/assets/images/img/logo2.png';

        $this->load->library('PHPExcel', null, 'excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->createSheet(1);
        $this->excel->getActiveSheet()->setTitle('Survey Report');

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($logo);
        $objDrawing->setOffsetX(10); // setOffsetX works properly
        $objDrawing->setOffsetY(5); //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(70); // logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $this->excel->getActiveSheet()->setShowGridlines(false);
    }

    /**
     * Check if user has a special access on the menu
     * @param SZFive URL/link
     * @return boolean
     */
    private function _check_menu_access($link)
    {
        $sub_data = $this->_has_subordinates();

        if ($this->userdata['role_id'] == 1)
        {
            return true;
        }

        if ($sub_data['has_subordinates'] == false)
        {
            if (!empty($this->_get_menu_item(['link' => $link])))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    private function _get_highfive_enty_comments($param)
    {
        $highfive_id = $param['highfive_id'];
        $emp_id = $param['emp_id'];
        $isPublic = $param['isPublic'];

        $query = "SELECT a.id , a.id as hiFiveId, a.message, a.noOfRecepients, a.senderId as uid, a.dateCreated, a.dateCreated as dateSubmitted, a.isPublic,  a.isReviewed, a.dateReviewed, f.fname as supervisorFname, f.lname as supervisorLname, e.pic as supervisorPic, c.emp_id as supId
        FROM tbl_szfive_high_fives as a
        LEFT JOIN tbl_user as b ON b.uid = a.reviewerId
        LEFT JOIN tbl_employee as c ON c.emp_id = b.emp_id
        LEFT JOIN tbl_applicant as f ON c.apid = f.apid

        LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
        LEFT JOIN tbl_applicant as e ON e.apid = c.apid
        WHERE a.id = $highfive_id";

        $qdata = $this->general_model->custom_query($query);

        if (!empty($qdata))
        {

            $data = $this->_get_entry_details($qdata);
            $data['entry'] = $this->_get_mentioned_users($data['entry']);
            $data['entry']->totalComment = $this->general_model->fetch_specific_val('count(*) as count', ['parentId' => $highfive_id, 'type' => 'sz_highfive', 'isPublic' => $isPublic], 'tbl_szfive_comments');

            $dayOfWeek = date('l', strtotime($data['entry']->dateCreated));
            $formatDate = DateTime::createFromFormat('Y-m-d H:i:s', $data['entry']->dateCreated)->format('F j');
            $data['uri_segment'] = $this->uri->segment_array();
            $data['title'] = $this->title . ' - SZhoutOut';
            $data['nav_list'] = $this->_navigation_bar_list('SZhoutOuts');

            $data['entry']->date_answered = $dayOfWeek . ', ' . $formatDate;

            $data['child_comments'] = $this->_get_comments(['parent_id' => $highfive_id, 'isPublic' => $isPublic, 'sort' => 'a.dateCreated DESC', 'type' => 'sz_highfive', 'offset' => 0, 'no_of_items' => 5]);
            asort($data['child_comments']);
            $data['sent_to_users'] = $this->_sent_to_users($data['entry'], $data['child_comments']);

            if ($qdata[0]->isPublic != '0')
            {
                $this->load_template_view('templates/szfive/highfive/_entry', $data);
            }
            else
            {
                $uid = $this->userdata['uid'];
                $highfive = $this->general_model->custom_query("SELECT a.senderId, b.recipientId FROM tbl_szfive_high_fives as a LEFT JOIN tbl_szfive_high_five_details as b ON b.id = a.id WHERE a.id = " . $qdata[0]->id);

                if ($highfive[0]->senderId == $uid or $highfive[0]->recipientId == $uid)
                {
                    $this->load_template_view('templates/szfive/highfive/_entry', $data);
                }
                else
                {
                    $this->load_template_view('templates/szfive/error_access', $data);
                }
            }
        }
        else
        {
            $this->error_404();
        }
    }

    private function _get_summary_report_data($param)
    {
        $dates = $this->_get_dates_from_range($param['startDate'], $param['endDate']);
        $survey = $this->general_model->fetch_specific_val('choiceLabels', ['surveyId' => '3'], 'tbl_szfive_survey_details');
        $choice_labels = json_decode($survey->choiceLabels);
        $append = '';

        if ($param['user_type'] != '' && $param['user_type'] != 'all')
        {
            $append .= " AND f.acc_description = " . "'" . $param['user_type'] . "'";
        }

        if (!empty($param['account_id']))
        {
            $append .= " AND f.acc_id IN (" . implode(",", $param['account_id']) . ")";
            $this->account_id = $param['account_id'];
            $this->session->set_userdata('szfive_account_id', $param['account_id']);
        }
        else
        {
            $this->session->unset_userdata('szfive_account_id');
        }

        $sql = "SELECT a.*, b.*, c.*, d.*, e.sched_id, e.sched_date,x.*
        FROM tbl_szfive_survey_answers as a
        JOIN tbl_user as b ON b.uid = a.employeeId
        JOIN tbl_employee as c ON c.emp_id = b.emp_id
        JOIN tbl_applicant x ON c.apid=x.apid
        JOIN tbl_account as f ON f.acc_id = c.acc_id
        LEFT JOIN tbl_user as d ON d.emp_id = c.Supervisor
        LEFT JOIN tbl_schedule as e ON e.sched_id = a.sched_id
        ";

        foreach ($dates as $key => $date)
        {
            $survey_answer = $this->general_model->custom_query($sql . " WHERE e.sched_date >=  '$date' AND e.sched_date <= '$date' $append");

            if (!empty($survey_answer))
            {

                $answers = [];

                foreach ($survey_answer as $val)
                {
                    $answers[] = $val->answer;
                }

                $sum = 0;
                foreach ($choice_labels as $choice)
                {
                    $value = array_search($choice, $choice_labels) + 1;
                    $count_answers = count(array_keys($answers, $value));
                    $sum = $sum + ($count_answers * $value);
                }

                $total_anwers = count($survey_answer);
                $avg = round(@$sum / $total_anwers, 2);
                $all_averages[] = $avg;
            }
            else
            {
                $total_anwers = 0;
                $all_averages[] = $total_anwers;
            }

            $users = $this->general_model->custom_query("SELECT DISTINCT(a.emp_id) as emp_id FROM `tbl_schedule` as a JOIN tbl_employee as c ON c.emp_id = a.emp_id
                JOIN tbl_account as f ON f.acc_id = c.acc_id where sched_date = '$date' AND a.schedtype_id IN (1,4,5) $append");

            if (!empty($users))
            {
                $total_users = count($users);
                $percent = round(@($total_anwers / $total_users) * 100, 2);
                $all_answers[] = $percent . '% or ' . $total_anwers;
                $all_users[] = $total_users;
            }
            else
            {
                $all_answers[] = '0% or 0';
                $all_users[] = 0;
            }

            $reviewed_answers = $this->general_model->custom_query($sql . " WHERE e.sched_date >=  '$date' AND e.sched_date <= '$date' AND a.isReviewed = 1 $append");

            if (!empty($reviewed_answers))
            {
                $count_rev_answers = count($reviewed_answers);
                $percent = round(@($count_rev_answers / $total_anwers) * 100, 2);
                $all_reviewed_answers[] = $percent . '% or ' . $count_rev_answers;
            }
            else
            {
                $all_reviewed_answers[] = '0% or 0';
            }

            $likes = $this->general_model->custom_query("SELECT count(a.likeId) as count FROM tbl_szfive_likes as a JOIN tbl_user as b ON b.uid = a.uid
               JOIN tbl_employee as c ON c.emp_id = b.emp_id
               JOIN tbl_account as f ON f.acc_id = c.acc_id WHERE a.dateCreated >=  '$date 00:00:00' AND a.dateCreated <= '$date 23:59:59' $append");

            if (!empty($likes))
            {
                $all_likes[] = $likes[0]->count;
            }
            else
            {
                $all_likes[] = 0;
            }

            $high_fives = $this->general_model->custom_query("SELECT count(a.id) as count FROM tbl_szfive_high_fives as a JOIN tbl_user as b ON b.uid = a.senderId
               JOIN tbl_employee as c ON c.emp_id = b.emp_id
               JOIN tbl_account as f ON f.acc_id = c.acc_id WHERE a.dateCreated >=  '$date 00:00:00' AND a.dateCreated <= '$date 23:59:59' $append");

            if (!empty($likes))
            {
                $all_high_fives[] = $high_fives[0]->count;
            }
            else
            {
                $all_high_fives[] = 0;
            }

            $comments = $this->general_model->custom_query("SELECT count(a.id) as count FROM tbl_szfive_comments as a JOIN tbl_user as b ON b.uid = a.senderId
               JOIN tbl_employee as c ON c.emp_id = b.emp_id
               JOIN tbl_account as f ON f.acc_id = c.acc_id WHERE a.dateCreated >=  '$date 00:00:00' AND a.dateCreated <= '$date 23:59:59' $append");

            if (!empty($comments))
            {
                $all_comments[] = $comments[0]->count;
            }
            else
            {
                $all_comments[] = 0;
            }

            $query = "SELECT x.fname, x.lname, count(*) as count
            FROM tbl_szfive_high_five_details as a
            LEFT JOIN tbl_szfive_high_fives as c ON c.id = a.id
            JOIN tbl_user as b ON b.uid = a.recipientId
            JOIN tbl_employee as d ON d.emp_id = b.emp_id
            JOIN tbl_applicant x ON x.apid=d.apid
            JOIN tbl_account as f ON f.acc_id = d.acc_id
            WHERE c.dateCreated >= '$date 00:00:00' AND c.dateCreated <= '$date 23:59:59' $append
            GROUP BY a.recipientId ORDER BY count DESC LIMIT 1";

            $receiver = $this->general_model->custom_query($query);
            if (!empty($receiver))
            {
                $all_top_receivers[] = $receiver[0]->fname . ' ' . $receiver[0]->lname;
            }
            else
            {
                $all_top_receivers[] = '--';
            }

            $query = "SELECT x.fname, x.lname, count(*) as count
            FROM tbl_szfive_high_fives as a
            JOIN tbl_user as b ON b.uid = a.senderId
            JOIN tbl_employee as c ON c.emp_id = b.emp_id
            JOIN tbl_applicant x ON x.apid=c.apid
            JOIN tbl_account as f ON f.acc_id = c.acc_id
            WHERE a.dateCreated >= '$date 00:00:00' AND a.dateCreated <= '$date 23:59:59' $append
            GROUP BY a.senderId ORDER BY count DESC LIMIT 1";

            $sender = $this->general_model->custom_query($query);

            if (!empty($sender))
            {
                $all_top_senders[] = $sender[0]->fname . ' ' . $sender[0]->lname;
            }
            else
            {
                $all_top_senders[] = '--';
            }

            $date = new DateTime($date);
            $formatted_date = $date->format('n/d/Y');
            $all_dates[] = $formatted_date;
        }

        $titles = ['SZFive', 'Company wide Pulse', 'Active People using SZFive (Ambassadors & Admins)', 'Submitted Surveys', 'Reviewed Surveys', 'SZhoutOuts', 'Likes', 'Comments', 'Received the most SZhoutOuts', 'Gave the most SZhoutOuts'];

        if (!empty($all_averages))
        {
            array_unshift($all_dates, $titles[0]);
        }

        if ($param['user_type'] == 'Admin')
        {
            $titles[2] = "Active People using SZFive (Admins)";
        }

        if ($param['user_type'] == 'Agent')
        {
            $titles[2] = "Active People using SZFive (Ambassadors)";
        }

        $data = [
            'dates' => $all_dates,
            'data' => [
                'average' => ['title' => $titles[1], 'data' => $all_averages],
                'users' => ['title' => $titles[2], 'data' => $all_users],
                'answers' => ['title' => $titles[3], 'data' => $all_answers],
                'reviewed_answers' => ['title' => $titles[4], 'data' => $all_reviewed_answers],
                'high_fives' => ['title' => $titles[5], 'data' => $all_high_fives],
                'likes' => ['title' => $titles[6], 'data' => $all_likes],
                'comments' => ['title' => $titles[7], 'data' => $all_comments],
                'receiver' => ['title' => $titles[8], 'data' => $all_top_receivers],
                'sender' => ['title' => $titles[9], 'data' => $all_top_senders],
            ],
        ];

        return $data;
    }

    private function _get_members_by_dept_id($acc_id)
    {
        /*  $query = "SELECT a.uid FROM tbl_user as a
          JOIN tbl_employee as b ON b.emp_id = a.emp_id
          JOIN tbl_account as c ON c.acc_id = b.acc_id
          LEFT JOIN tbl_department as k ON k.dep_id = c.dep_id
          WHERE k.dep_id = $dep_id AND a.uids <> " . $this->userdata['uid']; */

          $query = "SELECT a.uid,fname FROM tbl_user as a JOIN tbl_employee as b ON b.emp_id = a.emp_id JOIN tbl_applicant as d ON b.apid = d.apid JOIN tbl_account as c ON c.acc_id = b.acc_id WHERE c.acc_id = $acc_id AND a.uid <>" . $this->userdata['uid'];

          $employees = $this->general_model->custom_query($query);
          return $employees;
      }

  }
