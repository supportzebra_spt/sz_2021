<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Leave extends General {

    public $logo = "C:\\wamp64\\www\\Cloned\\assets\\images\\img\\logo2.png";
    protected $title = array('title' => 'Leave');

    public function control_panel()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        $data['departments'] = $this->general_model->fetch_all("dep_id,dep_name", "tbl_department", "dep_name ASC");
        if ($this->check_access())
        {
            $this->load_template_view('templates/leave/control_panel', $data);
        }
    }

    public function types()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access())
        {
            $this->load_template_view('templates/leave/types', $data);
        }
    }

    public function rules()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access())
        {
            $this->load_template_view('templates/leave/rules', $data);
        }
    }

    public function credits()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access())
        {
            $this->load_template_view('templates/leave/credits', $data);
        }
    }

    public function users()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access())
        {
            $this->load_template_view('templates/leave/users', $data);
        }
    }

    public function request()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        $this->load_template_view('templates/leave/request', $data);
    }

    public function approval()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access())
        {
            $this->load_template_view('templates/leave/approval', $data);
        }
    }

    public function retraction()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        $this->load_template_view('templates/leave/retraction', $data);
    }

    public function monitoring()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        $query1 = "SELECT a.requestLeave_ID,a.reason,a.createdOn,e.fname,e.lname FROM tbl_request_leave a,tbl_user c,tbl_employee d,tbl_applicant e WHERE c.emp_id = d.emp_id AND d.apid=e.apid AND a.finalStatus = 12 AND a.uid=c.uid ORDER BY a.createdOn DESC";
        $count1 = COUNT($this->general_model->custom_query($query1));

        $query2 = "SELECT a.retractLeave_ID,a.reason,a.createdOn,c.fname,c.lname FROM tbl_retract_leave a ,tbl_user c,tbl_employee d,tbl_applicant e WHERE c.emp_id = d.emp_id AND d.apid=e.apid AND a.finalStatus = 12 AND a.uid=c.uid ORDER BY a.createdOn DESC";
        $count2 = COUNT($this->general_model->custom_query($query2));
        $total = $count1 + $count2;
        $data['missed_total'] = $total;
        if ($this->check_access())
        {
            $this->load_template_view('templates/leave/monitoring', $data);
        }
    }

    public function my_dates()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();

        $this->load_template_view('templates/leave/leave_list', $data);
    }

// STARTOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------
// STARTOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------
// STARTOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------
// STARTOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------
// STARTOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------        

    public function get_leave_types()
    {
        $datatable = $this->input->post('datatable');

        $query['query'] = "SELECT leaveType_ID,leaveType,description

		FROM tbl_leave_type";

        if (isset($datatable['query']['input-search']) && $datatable['query']['input-search'] !== '')
        {
            $keyword = $datatable['query']['input-search'];

            $query['search']['append'] = " WHERE leaveType LIKE '%" . $keyword . "%' OR  leaveType_ID LIKE '%" . $keyword . "%'";

            $query['search']['total'] = "  WHERE leaveType LIKE '%" . $keyword . "%' OR  leaveType_ID LIKE '%" . $keyword . "%'";
        }

        $data = $this->set_datatable_query($datatable, $query);

        echo json_encode($data);
    }

    public function get_leave_lists()
    {
        $datatable = $this->input->post('datatable');

        $query['query'] = "SELECT leave_id,leave_name,description,validationType

		FROM tbl_leave";

        if (isset($datatable['query']['input-search']) && $datatable['query']['input-search'] !== '')
        {
            $keyword = $datatable['query']['input-search'];

            $query['search']['append'] = " WHERE leave_name LIKE '%" . $keyword . "%' OR  leave_id LIKE '%" . $keyword . "%'";

            $query['search']['total'] = "  WHERE leave_name LIKE '%" . $keyword . "%' OR  leave_id LIKE '%" . $keyword . "%'";
        }

        $data = $this->set_datatable_query($datatable, $query);

        echo json_encode($data);
    }

    public function new_leave_type()
    {

        $uid = $this->session->userdata('uid');
        $data = json_decode($this->input->post('data'));
        $leaveType = $data[0]->leaveType;
        $description = $data[1]->description;
        $changedBy = $uid;
        $array = array(
            'leaveType' => $leaveType,
            'description' => $description,
            'isActive' => 1,
            'changedBy' => $changedBy
        );
        $result = $this->general_model->insert_vals($array, "tbl_leave_type");
        if ($result)
        {
            $status = 'Success';
        }
        else
        {
            $status = 'Failed';
        }
        echo json_encode(array('status' => $status));
    }

    public function new_leave_record()
    {

        $uid = $this->session->userdata('uid');
        $data = json_decode($this->input->post('data'));
        $leave_name = $data[0]->leave_name;
        $description = $data[1]->description;
        $leaveType_ID = $data[2]->leaveType;
        $refreshOccur = ($data[3]->refreshOccur === 'NULL') ? NULL : $data[3]->refreshOccur;
        $preFilingLimit = $data[4]->preFilingLimit;
        $allowedDateStart = $data[5]->allowedDateStart;
        $allowedDateEnd = $data[6]->allowedDateEnd;
        $pickerMode = $data[7]->pickerMode;
        $minimumDays = ($data[8]->minimumDays === '') ? NULL : $data[8]->minimumDays;
        $maximumDays = $data[9]->maximumDays;
        $allowBackup = $data[10]->allowBackup;
        if ($allowBackup === 'no')
        {
            $backupLeave = NULL;
            $maximumCredits = NULL;
        }
        else
        {
            $backupLeave = $data[11]->backupLeave;
            $maximumCredits = $data[12]->maximumCredits;
        }

        $allowRetraction = $data[13]->allowRetraction;

        $allowHalfDay = $data[14]->allowHalfDay;

        $defaultPayment = $data[15]->defaultPayment;
        $changedBy = $uid;
        $array = array(
            'leave_name' => $leave_name,
            'description' => $description,
            'leaveType_ID' => $leaveType_ID,
            'isActive' => 1,
            'refreshOccur' => $refreshOccur,
            'preDays' => $preFilingLimit,
            'isStatic' => 0,
            'allowedDateStart' => $allowedDateStart,
            'allowedDateEnd' => $allowedDateEnd,
            'validationType' => 'all',
            'pickerMode' => $pickerMode,
            'minimumDays' => $minimumDays,
            'maximumDays' => $maximumDays,
            'allowBackup' => ($allowBackup == 'yes') ? 1 : 0,
            'backupLeave_ID' => $backupLeave,
            'backupMax' => $maximumCredits,
            'allowRetraction' => $allowRetraction,
            'defaultPayment' => $defaultPayment,
            'allowHalfDay' => ($allowHalfDay == 'yes') ? 1 : 0,
            'changedBy' => $changedBy
        );
        $result = $this->general_model->insert_vals($array, "tbl_leave");
        if ($result)
        {
            $status = 'Success';
        }
        else
        {
            $status = 'Failed';
        }
        echo json_encode(array('status' => $status));
    }

    public function update_leave_type()
    {
        $leaveType_ID = $this->input->post('leaveType_ID');

        $uid = $this->session->userdata('uid');
        $data = json_decode($this->input->post('data'));
        $leaveType = $data[0]->leaveType;
        $description = $data[1]->description;
        $changedBy = $uid;
        $array = array(
            'leaveType' => $leaveType,
            'description' => $description,
            'isActive' => 1,
            'changedBy' => $changedBy
        );
        $result = $this->general_model->update_vals($array, "leaveType_ID=$leaveType_ID", "tbl_leave_type");
        if ($result)
        {
            $status = 'Success';
        }
        else
        {
            $status = 'Failed';
        }
        echo json_encode(array('status' => $status));
    }

    public function update_leave_record()
    {

        $uid = $this->session->userdata('uid');
        $leave_id = $this->input->post('leave_id');
        $data = json_decode($this->input->post('data'));
        $leave_name = $data[0]->leave_name;
        $description = $data[1]->description;
        $leaveType_ID = $data[2]->leaveType;
        $refreshOccur = ($data[3]->refreshOccur === 'NULL') ? NULL : $data[3]->refreshOccur;
        $preFilingLimit = $data[4]->preFilingLimit;
        $allowedDateStart = $data[5]->allowedDateStart;
        $allowedDateEnd = $data[6]->allowedDateEnd;
        $pickerMode = $data[7]->pickerMode;
        $minimumDays = ($data[8]->minimumDays === '') ? NULL : $data[8]->minimumDays;
        $maximumDays = $data[9]->maximumDays;
        $allowBackup = $data[10]->allowBackup;
        if ($allowBackup === 'no')
        {
            $backupLeave = NULL;
            $maximumCredits = NULL;
        }
        else
        {
            $backupLeave = $data[11]->backupLeave;
            $maximumCredits = $data[12]->maximumCredits;
        }

        $allowRetraction = $data[13]->allowRetraction;

        $allowHalfDay = $data[14]->allowHalfDay;
        $defaultPayment = $data[15]->defaultPayment;

        $changedBy = $uid;
        $array = array(
            'leave_name' => $leave_name,
            'description' => $description,
            'leaveType_ID' => $leaveType_ID,
            'refreshOccur' => $refreshOccur,
            'preDays' => $preFilingLimit,
            'allowedDateStart' => $allowedDateStart,
            'allowedDateEnd' => $allowedDateEnd,
            'pickerMode' => $pickerMode,
            'minimumDays' => $minimumDays,
            'maximumDays' => $maximumDays,
            'allowBackup' => ($allowBackup == 'yes') ? 1 : 0,
            'backupLeave_ID' => $backupLeave,
            'backupMax' => $maximumCredits,
            'allowRetraction' => $allowRetraction,
            'defaultPayment' => $defaultPayment,
            'allowHalfDay' => ($allowHalfDay == 'yes') ? 1 : 0,
            'changedBy' => $changedBy
        );
        $result = $this->general_model->update_vals($array, "leave_id=$leave_id", "tbl_leave");
        if ($result)
        {
            $status = 'Success';
        }
        else
        {
            $status = 'Failed';
        }
        echo json_encode(array('status' => $status));
    }

    public function names_init()
    {
        $leaveTypes = $this->general_model->fetch_all("leaveType_ID,leaveType", "tbl_leave_type");
        echo json_encode(array('leave_types' => $leaveTypes));
    }

    public function get_single_leave_details()
    {
        $leave_id = $this->input->post('leave_id');
        $leaveDetails = $this->general_model->fetch_specific_val("*", "leave_id=$leave_id", "tbl_leave");
        echo json_encode(array('leave_details' => $leaveDetails));
    }

    public function get_single_leave_types()
    {
        $leaveType_ID = $this->input->post('leaveType_ID');
        $leaveDetails = $this->general_model->fetch_specific_val("*", "leaveType_ID=$leaveType_ID", "tbl_leave_type");
        echo json_encode(array('leave_details' => $leaveDetails));
    }

    public function credits_init()
    {
        $leave_list = $this->general_model->fetch_all("leaveType_ID,leaveType", "tbl_leave_type", "leaveType ASC");
        $employee_status = $this->general_model->fetch_all("empstat_id,status", "tbl_emp_stat");

        echo json_encode(array('employee_status' => $employee_status, 'leave_list' => $leave_list));
    }

    public function get_all_position_status_credits()
    {
        $leave_names = $this->input->post('leave_names');
        $classes = $this->input->post('classes');
        $status = $this->input->post('status');
        $data = $this->general_model->fetch_all("c.posempstat_id,pos_name,pos_details,b.status,class", "a.pos_id=c.pos_id AND b.empstat_id=c.empstat_id AND b.empstat_id IN ('" . implode("','", $status) . "') AND class IN ('" . implode("','", $classes) . "')", "tbl_position a,tbl_emp_stat b,tbl_pos_emp_stat c", "pos_details ASC");
        foreach ($data as $d)
        {
            $d->credits = $this->general_model->custom_query("SELECT * FROM tbl_leave_credit WHERE posempstat_id=$d->posempstat_id");
        }
        echo json_encode(array('data' => $data));
    }

    public function get_position_status_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $searchstatus = $this->input->post('searchstatus');
        $searchclass = $this->input->post('searchclass');
        $searchcustom = $this->input->post('searchcustom');
        $search = array();
        $searchString = "";
        if ($searchstatus !== '')
        {
            $search[] = "b.status='$searchstatus'";
        }
        if ($searchclass !== '')
        {
            $search[] = "a.class='$searchclass'";
        }
        if ($searchcustom !== '')
        {
            $search[] = "a.pos_details LIKE '%$searchcustom%'";
        }
        if (!empty($search))
        {
            $searchString = "WHERE " . implode(" AND ", $search);
        }
        $query = "SELECT c.posempstat_id,pos_name,pos_details,b.status,class "
                . "FROM tbl_position a "
                . "INNER JOIN tbl_pos_emp_stat c ON a.pos_id=c.pos_id "
                . "INNER JOIN tbl_emp_stat b ON b.empstat_id=c.empstat_id $searchString ORDER BY pos_name ASC";

        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $count = COUNT($this->general_model->custom_query($query));
        $leave_list = $this->general_model->fetch_all("leaveType_ID,leaveType", "tbl_leave_type", "leaveType ASC");
        foreach ($data as $pos_stat)
        {
            $credits = array();
            foreach ($leave_list as $leave)
            {
                $cred = $this->general_model->fetch_specific_val("leaveCredit_ID,credit", "leaveType_ID=$leave->leaveType_ID AND posempstat_id=$pos_stat->posempstat_id", "tbl_leave_credit");

                if (empty($cred))
                {
                    $credits[] = array('leaveType_ID' => $leave->leaveType_ID, 'credit' => "");
                }
                else
                {
                    $credits[] = array('leaveType_ID' => $leave->leaveType_ID, 'credit' => $cred->credit);
                }
            }
            $pos_stat->credits = $credits;
        }
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    public function update_leave_credit()
    {
        $uid = $this->session->userdata('uid');
        $value = $this->input->post('value');
        $posempstat_id = $this->input->post('posempstat_id');
        $leaveType_ID = $this->input->post('leaveType_ID');
        $old_credit = $this->general_model->fetch_specific_val("leaveCredit_ID", "posempstat_id=$posempstat_id AND leaveType_ID=$leaveType_ID", "tbl_leave_credit");

        $changedBy = $uid;
        if ($old_credit === NULL)
        {
            $res = $this->general_model->insert_vals(array('posempstat_id' => $posempstat_id, 'leaveType_ID' => $leaveType_ID, 'credit' => $value, 'isActive' => 1, 'changedBy' => $changedBy), "tbl_leave_credit");
        }
        else
        {
            $res = $this->general_model->update_vals(array('credit' => $value, 'isActive' => 1, 'changedBy' => $changedBy), "leaveCredit_ID=$old_credit->leaveCredit_ID", "tbl_leave_credit");
        }
        if ($res)
        {
            $status = 'Success';
        }
        else
        {
            $status = 'Failed';
        }

        echo json_encode(array('status' => $status));
    }

    public function users_init()
    {
        $statuses = $this->general_model->fetch_all("empstat_id,status", "tbl_emp_stat");
        $users = $this->general_model->fetch_specific_vals("c.fname,c.lname,a.uid", "a.isActive=1 AND b.isActive='yes' AND a.emp_id = b.emp_id AND b.apid=c.apid", "tbl_user a,tbl_employee b,tbl_applicant c", "lname ASC");
        echo json_encode(array('statuses' => $statuses, 'users' => $users));
    }

    public function get_leave_user_validation()
    {
        $leave_id = $this->input->post('leave_id');
        $data = $this->general_model->fetch_specific_val("validationType,leave_name", "leave_id=$leave_id", "tbl_leave");
        if ($data->validationType === 'general')
        {
            $data->general = $this->general_model->fetch_specific_val("leaveGeneral_ID,positionType,positionStatus,gender,civilStatus", "leave_id=$leave_id", "tbl_leave_general_validation");
        }
        else if ($data->validationType === 'specific')
        {
            $data->specific = $this->general_model->fetch_specific_vals("leaveSpecific_ID,uid", "leave_id=$leave_id", "tbl_leave_specific_validation");
        }
        echo json_encode(array('data' => $data));
    }

    public function update_leave_validation()
    {
        $leave_id = $this->input->post('leave_id');
        $validationType = $this->input->post('validationType');
        $status[] = $this->general_model->update_vals(array('validationType' => $validationType), "leave_id=$leave_id", "tbl_leave");
        $status[] = $this->general_model->delete_vals("leave_id=$leave_id", 'tbl_leave_specific_validation');
        $status[] = $this->general_model->delete_vals("leave_id=$leave_id", 'tbl_leave_general_validation');
        if ($validationType === 'specific')
        {
            $data = $this->input->post('data');
            $users = $data['users'];
            $arr = array();

            if ($users !== null)
            {
                foreach ($users as $u)
                {
                    $arr[] = array(
                        'leave_id' => $leave_id,
                        'uid' => $u,
                        'isActive' => 1
                    );
                }
                $status[] = $this->general_model->insert_array_vals($arr, "tbl_leave_specific_validation");
            }
        }
        else if ($validationType === 'general')
        {
            $data = $this->input->post('data');
            $gender = $data['gender'];
            $civilStatus = $data['civilStatus'];
            $classes = $data['classes'];
            $positionStatus = $data['positionStatus'];
            $status[] = $this->general_model->insert_vals(array('leave_id' => $leave_id, 'positionType' => $classes, 'positionStatus' => $positionStatus, 'gender' => $gender, 'civilStatus' => $civilStatus), "tbl_leave_general_validation");
        }
        $statuses = (count(array_unique($status)) === 1);
        echo json_encode(array('status' => ($statuses) ? "Success" : "Failed"));
    }

// ENDOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------
// ENDOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------
// ENDOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------
// ENDOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------
// ENDOFCONTROLPANEL------------------------------------------------------------------------------------------------------------------------------------------
// START OF FILING--------------------------------------------------------------------------------------------------------------------------------------------
// START OF FILING--------------------------------------------------------------------------------------------------------------------------------------------
// START OF FILING--------------------------------------------------------------------------------------------------------------------------------------------
// START OF FILING--------------------------------------------------------------------------------------------------------------------------------------------
// START OF FILING--------------------------------------------------------------------------------------------------------------------------------------------


    public function request_init()
    {
        $user = $this->general_model->fetch_specific_val("birthday,civilStatus,gender", "a.apid=b.apid AND b.emp_id=" . $this->session->emp_id, "tbl_applicant a,tbl_employee b");
        $promote = $this->general_model->fetch_specific_vals("a.emp_promoteID,a.posempstat_id,a.dateFrom,c.pos_details,d.status", "a.emp_id = " . $this->session->emp_id . " AND a.posempstat_id=b.posempstat_id AND b.pos_id = c.pos_id AND b.empstat_id=d.empstat_id AND a.ishistory =1", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d");
        $emppromoteMissing = array();
        foreach ($promote as $pro)
        {
            if ($pro->dateFrom === '' || $pro->dateFrom === NULL)
            {
                $emppromoteMissing[] = $pro->pos_details . ' - ' . $pro->status;
            }
        }
        $emppromoteMissing = implode(', ', $emppromoteMissing);
        $m1 = (trim($user->birthday) === '') ? "Birthday, " : "";
        $m2 = (trim($user->civilStatus) === '') ? "Civil Status, " : "";
        $m3 = (trim($user->gender) === '') ? "Gender, " : "";
        $missing = $m1 . $m2 . $m3;
        if ($missing === "" && $emppromoteMissing === "")
        {
            $leave_names = $this->get_allowed_leave_names();
            if (empty($leave_names))
            {
                $status = "Empty";
            }
            else
            {
                $status = "Success";
            }
        }
        else
        {
            if (substr($missing, -2, 2) == ', ')
            {
                $missing = substr($missing, 0, -2);
            }
            $leave_names = array();
            $status = array('missing' => $missing, 'emppromoteMissing' => $emppromoteMissing);
        }
        echo json_encode(array('leave_names' => $leave_names, 'status' => $status));
    }

    private function get_allowed_leave_names()
    {
        $uid = $this->session->userdata('uid');
        $emp_id = $this->session->userdata('emp_id');
        $leave_list = $this->general_model->fetch_specific_vals("leave_id,leave_name,validationType", "isActive=1", "tbl_leave", "leave_name ASC");
        foreach ($leave_list as $key => $leave)
        {
            if ($leave->validationType === 'general')
            {
                $general = $this->general_model->fetch_specific_val("leaveGeneral_ID,positionType,positionStatus,gender,civilStatus", "leave_id=" . $leave->leave_id . " AND isActive=1", "tbl_leave_general_validation");
                if (empty($general) || $general === '')
                {
//                    echo "Failed";
                }
                else
                {
                    $positionType = explode(",", $general->positionType);
                    $positionStatus = explode(",", $general->positionStatus);
                    $gender = explode(",", $general->gender);
                    $civilStatus = explode(",", $general->civilStatus);
                    $where = "";
                    $where .= ($general->positionType === '') ? '' : " AND f.class IN ('" . implode("','", $positionType) . "')";
                    $where .= ($general->positionStatus === '') ? '' : " AND e.empstat_id IN ('" . implode("','", $positionStatus) . "')";
                    $where .= ($general->gender === '') ? '' : " AND a.gender IN ('" . implode("','", $gender) . "')";
                    $where .= ($general->civilStatus === '') ? '' : " AND a.civilStatus IN ('" . implode("','", $civilStatus) . "')";

                    $query = "SELECT a.lname,a.fname,a.gender,a.civilStatus,f.class,e.status FROM tbl_applicant a,tbl_employee b,tbl_leave_general_validation c,tbl_emp_promote d,tbl_emp_stat e,tbl_position f,tbl_pos_emp_stat g WHERE a.apid=b.apid AND b.emp_id=$emp_id AND b.emp_id=d.emp_id AND d.posempstat_id=g.posempstat_id AND g.empstat_id=e.empstat_id AND g.pos_id=f.pos_id AND d.isActive=1 AND leaveGeneral_ID=" . $general->leaveGeneral_ID . " $where";
                    $data = $this->general_model->custom_query($query);
//                    echo $this->db->last_query().'<br><br>';
                    if (empty($data) || $data === '')
                    {
                        unset($leave_list[$key]);
                    }
                }
            }
            else if ($leave->validationType === 'specific')
            {
                $data = $this->general_model->fetch_specific_val("leaveSpecific_ID", "leave_id=" . $leave->leave_id . " AND uid=$uid AND isActive=1", "tbl_leave_specific_validation");
                if (empty($data) || $data === '')
                {
                    unset($leave_list[$key]);
                }
            }
        }
        return $leave_list;
    }

    public function get_leave_details()
    {
        $emp_id = $this->session->userdata('emp_id');
        $leave_id = $this->input->post('leave_id');
        $leave_details = $this->general_model->fetch_specific_val("description,refreshOccur,preDays,isStatic,betweenStart,betweenEnd,allowedDateStart,allowedDateEnd,pickerMode,minimumDays,maximumDays,allowBackup,allowHalfDay,leaveType_ID", "leave_id=$leave_id", "tbl_leave");
        if ($leave_details->leaveType_ID === '5') //Birthday
        {
            $leave_details->staticDate = $this->general_model->fetch_specific_val("a.birthday", "a.apid=b.apid AND b.emp_id = $emp_id", "tbl_applicant a,tbl_employee b")->birthday;
        }
        echo json_encode(array('leave_details' => $leave_details));
    }

    public function get_preview_leave_details()//try using this passing today date for credits
    {
        $emp_id = $this->session->userdata('emp_id');
        $leave_id = $this->input->post('leave_id');
        $date_list = $this->input->post('date_list');
//        $date_list = ['2015-03-01', '2016-03-01', '2017-04-02', '2017-04-03', '2017-04-01', '2017-06-02', '2017-06-03', '2018-06-04', '2018-06-05', '2018-10-05', '2019-01-01'];
        foreach ($date_list as $date)
        {
            $date = date('Y-m-d', strtotime($date));
        }
        $d = $this->general_model->fetch_specific_val("refreshOccur,b.leaveType_ID,allowHalfDay,allowBackup,backupLeave_ID,backupMax,b.leaveType_ID,defaultPayment", "a.leave_id=$leave_id AND a.leaveType_ID=b.leaveType_ID", "tbl_leave a,tbl_leave_type b");
        if ($d->allowBackup === '1')
        {
            $backupLeaveName = $this->general_model->fetch_specific_val("leaveType", "leaveType_ID=" . $d->backupLeave_ID, "tbl_leave_type")->leaveType;
        }
        else
        {
            $backupLeaveName = '';
        }
        $refreshOccur = $d->refreshOccur;
        $leaveType_ID = $d->leaveType_ID;
        $history = $this->general_model->fetch_specific_vals("a.emp_promoteID,a.dateFrom,b.posempstat_id,c.pos_details,d.status,e.credit", "a.emp_id=$emp_id AND a.isHistory=1 AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND e.isActive=1 AND e.posempstat_id=b.posempstat_id AND e.leaveType_ID=$leaveType_ID", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_leave_credit e", "DATE(dateFrom) DESC");

        $temp_history = $this->general_model->fetch_specific_vals("a.emp_promoteID,a.dateFrom,b.posempstat_id", "a.emp_id=$emp_id AND a.isHistory=1 AND a.posempstat_id=b.posempstat_id", "tbl_emp_promote a,tbl_pos_emp_stat b", "DATE(dateFrom) ASC");

        if (empty($history))
        {
            $status = "Failed"; //kulang ang idisplay if walay credits pud iyang history position
            $end_data = array();
        }
        else
        {

            $groupedArray = array();
            if ($refreshOccur === 'semiannual')
            {
                foreach ($date_list as $date)
                {
                    $year = date('Y', strtotime($date));
                    if (date('n', strtotime($date)) <= 6)
                    {
                        $groupedArray[$year]['H1'][] = $date;
                    }
                    else if (date('n', strtotime($date)) >= 7 && date('n', strtotime($date)) <= 12)
                    {
                        $groupedArray[$year]['H2'][] = $date;
                    }
                }
                $final = $this->get_quarter_and_half_preview($groupedArray, $history, $temp_history, $d->backupLeave_ID, $d->leaveType_ID, $refreshOccur);
            }
            else if ($refreshOccur === 'quarter')
            {
                foreach ($date_list as $date)
                {
                    $year = date('Y', strtotime($date));
                    if (date('n', strtotime($date)) <= 3)
                    {
                        $groupedArray[$year]['Q1'][] = $date;
                    }
                    else if (date('n', strtotime($date)) >= 4 && date('n', strtotime($date)) <= 6)
                    {
                        $groupedArray[$year]['Q2'][] = $date;
                    }
                    else if (date('n', strtotime($date)) >= 7 && date('n', strtotime($date)) <= 9)
                    {
                        $groupedArray[$year]['Q3'][] = $date;
                    }
                    else if (date('n', strtotime($date)) >= 10 && date('n', strtotime($date)) <= 12)
                    {
                        $groupedArray[$year]['Q4'][] = $date;
                    }
                }
                $final = $this->get_quarter_and_half_preview($groupedArray, $history, $temp_history, $d->backupLeave_ID, $d->leaveType_ID, $refreshOccur);
            }
            else if ($refreshOccur === 'year')
            {
                foreach ($date_list as $date)
                {
                    $year = date('Y', strtotime($date));
                    $groupedArray[$year][] = $date;
                }
                $final = $this->get_year_and_norefresh_preview($groupedArray, $history, $temp_history, $d->backupLeave_ID, $d->leaveType_ID, $refreshOccur);
            }
            else
            {
                foreach ($date_list as $date)
                {
                    $year = date('Y', strtotime($date));
                    $groupedArray[$year][] = $date;
                }
                $final = $this->get_year_and_norefresh_preview($groupedArray, $history, $temp_history, $d->backupLeave_ID, $d->leaveType_ID, $refreshOccur);
            }
            $status = "Success";
            $end_data = array(
                'final' => $final,
                'refreshOccur' => $refreshOccur,
                'allowHalfDay' => $d->allowHalfDay,
                'allowBackup' => $d->allowBackup,
                'backupLeave_ID' => $d->backupLeave_ID,
                'backupLeaveName' => $backupLeaveName,
                'backupMax' => $d->backupMax
            );
        }
        echo json_encode(array('status' => $status, 'data' => $end_data, 'defaultPayment' => $d->defaultPayment));
    }

    private function getScheduleLogs($details)
    {

        $emp_id = $this->session->userdata('emp_id');

        $schedulelogs = array();

        foreach ($details as $thedates)
        {
            $schedule = $this->general_model->custom_query("SELECT a.sched_id,d.type,d.style,c.time_start,c.time_end,e.leaveType,d.schedtype_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id LEFT JOIN tbl_leave_type e ON a.leavenote=e.leaveType_ID  INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id WHERE a.emp_id = $emp_id AND a.sched_date='$thedates' ORDER BY d.schedtype_id ASC");

            foreach ($schedule as $sched)
            {
                $clock_in = $this->general_model->fetch_specific_val("dtr_id,log as login", "emp_id = $emp_id AND type='DTR' AND entry ='I' AND sched_id = " . $sched->sched_id, "tbl_dtr_logs", "dtr_id ASC");

                if ($clock_in === NULL)
                {
                    $sched->clock_in = "";
                    $sched->clock_out = "";
                }
                else
                {
                    $clock_out = $this->general_model->fetch_specific_val("dtr_id,log as logout", "emp_id = $emp_id AND type='DTR' AND entry ='O' AND sched_id = " . $sched->sched_id, "tbl_dtr_logs", "dtr_id ASC");
                    $sched->clock_in = $clock_in->login;
                    if ($clock_out === NULL)
                    {
                        $sched->clock_out = "";
                    }
                    else
                    {
                        $sched->clock_out = $clock_out->logout;
                    }
                }
            }
            $schedulelogs[] = json_encode($schedule);
        }
        return $schedulelogs;
    }

    private function get_quarter_and_half_preview($groupedArray, $history, $temp_history, $backupLeave_ID, $leaveType_ID, $refreshOccur)//samesame sa norefresh
    {
//        echo "quarter";
        $uid = $this->session->userdata('uid');
        $final = array();

        foreach ($groupedArray as $yearkey => $grpdates)//2018
        {
            foreach ($history as $hist)//compare if date belongs to specific history
            {
                $details = array();
                foreach ($grpdates as $quarterkey => $dates)//Q1 or H1
                {
                    foreach ($dates as $index => $date)//index of date 0,1,2,3
                    {
                        if (strtotime($hist->dateFrom) <= strtotime($date))
                        {
                            $details[$quarterkey][] = $date;
                            unset($grpdates[$quarterkey][$index]);
                        }
                    }
                }
                $hist->details = $details;
            }
            $new_history = [];
            foreach ($history as $hist)
            {
                if (!empty($hist->details))
                {
                    $new_history[] = $hist;
                }
            }

            foreach ($new_history as $new_hist)
            {


                $dateTo = date('Y-m-d');
                foreach ($temp_history as $index => $temp)
                {
                    if ($new_hist->emp_promoteID == $temp->emp_promoteID)
                    {
                        if (array_key_exists($index + 1, $temp_history))
                        {
                            $dateTo = $temp_history[$index + 1]->dateFrom;
                        }
                    }
                }


                foreach ($new_hist->details as $quarterkey => $dates)
                {
                    $schedulelogs = $this->getScheduleLogs($dates);
                    if ($quarterkey === 'Q1')
                    {
                        $monthStart = 1;
                        $monthEnd = 3;
                    }
                    else if ($quarterkey === 'Q2')
                    {
                        $monthStart = 4;
                        $monthEnd = 6;
                    }
                    else if ($quarterkey === 'Q3')
                    {
                        $monthStart = 7;
                        $monthEnd = 9;
                    }
                    else if ($quarterkey === 'Q4')
                    {
                        $monthStart = 10;
                        $monthEnd = 12;
                    }
                    else if ($quarterkey === 'H1')
                    {
                        $monthStart = 1;
                        $monthEnd = 6;
                    }
                    else if ($quarterkey === 'H2')
                    {
                        $monthStart = 7;
                        $monthEnd = 12;
                    }
                    else
                    {
//                        echo "ERROR";
                    }
                    $remaining = $this->get_quarterhalf_remaining_credits($yearkey, $refreshOccur, $dateTo, $new_hist, $monthStart, $monthEnd, $leaveType_ID, $uid, $quarterkey);
                    $dates_count = COUNT($new_hist->details);
                    if ($remaining >= $dates_count)
                    {
                        $paid = $dates_count;
                        $unpaid = 0;
                    }
                    else
                    {
                        $paid = $remaining;
                        $unpaid = $dates_count - $remaining;
                    }
                    $backup_remaining = $this->get_quarterhalf_remaining_credits($yearkey, $refreshOccur, $dateTo, $new_hist, $monthStart, $monthEnd, $backupLeave_ID, $uid, $quarterkey, 1);
                    $new_hist->details[$quarterkey] = array('dates' => $dates, 'schedulelogs' => $schedulelogs, 'credit' => $remaining, 'paid' => $paid, 'unpaid' => $unpaid, 'backupCredits' => $backup_remaining);
                }
            }
            if (COUNT($new_history) !== 0)
            {
                $final[] = json_encode(array('year' => $yearkey, 'history' => $new_history));
            }
        }
        return $final;
    }

    private function get_year_and_norefresh_preview($groupedArray, $history, $temp_history, $backupLeave_ID, $leaveType_ID, $refreshOccur)//samesame sa 
    {
        $uid = $this->session->userdata('uid');
        $emp_id = $this->session->userdata('emp_id');

        $finalhistory = array();
        $final = array();
        if ($refreshOccur != 'year')
        {
            $yeardisplay = '';
            foreach ($groupedArray as $yearkey => $val)
            {
                $yeardisplay .= $yearkey . ',';
            }
            $yeardisplay = explode(",", $yeardisplay);
            array_pop($yeardisplay);
            $yeardisplay = implode(",", $yeardisplay);
        }
        foreach ($groupedArray as $yearkey => $grpdates)
        {
            foreach ($history as $hist)
            {
                $arr = array();
                foreach ($grpdates as $index => $date)
                {
                    if (strtotime($hist->dateFrom) <= strtotime($date))
                    {
                        $arr[] = $date;
                        unset($grpdates[$index]);
                    }
                }
                $hist->details = $arr;
            }

            $new_history = [];
            foreach ($history as $hist)
            {
                if (!empty($hist->details))
                {
                    $new_history[] = $hist;
                }
            }
            foreach ($new_history as $new_hist)
            {

                $schedulelogs = $this->getScheduleLogs($new_hist->details);

                $dateTo = date('Y-m-d');
                foreach ($temp_history as $index => $temp)
                {
                    if ($new_hist->emp_promoteID == $temp->emp_promoteID)
                    {
                        if (array_key_exists($index + 1, $temp_history))
                        {
                            $dateTo = $temp_history[$index + 1]->dateFrom;
                        }
                    }
                }
                $remaining = $this->get_yearno_remaining_credits($dateTo, $new_hist, $yearkey, $leaveType_ID, $uid, $refreshOccur);
                $dates_count = COUNT($new_hist->details);
                if ($remaining >= $dates_count)
                {
                    $paid = $dates_count;
                    $unpaid = 0;
                }
                else
                {
                    $paid = $remaining;
                    $unpaid = $dates_count - $remaining;
                }
                $backup_remaining = $this->get_yearno_remaining_credits($dateTo, $new_hist, $yearkey, $backupLeave_ID, $uid, $refreshOccur, 1);
                $new_hist->details = array('dates' => $new_hist->details, 'schedulelogs' => $schedulelogs, 'credit' => $remaining, 'paid' => $paid, 'unpaid' => $unpaid, 'backupCredits' => $backup_remaining);
            }
            if (COUNT($new_history) !== 0)
            {
                if ($refreshOccur === 'year')
                {
                    $final[] = json_encode(array('year' => $yearkey, 'history' => $new_history));
                }
                else
                {
//                    $finalhistory = array_merge($finalhistory,$new_history);
                    $finalhistory[] = json_encode($new_history);
                }
            }
        }
        if ($refreshOccur != 'year')
        {
            $semiFinal = array();
            foreach ($finalhistory as $finhist)
            {
                $semiFinal[] = json_decode($finhist);
            }
            $finalhistory = $semiFinal[0];
            for ($x = 1; $x < COUNT($semiFinal); $x++)
            {
                foreach ($semiFinal[$x][0]->details->dates as $date)
                {
                    array_push($finalhistory[0]->details->dates, $date);
                }
            }
            $final[] = json_encode(array('year' => $yeardisplay, 'history' => $finalhistory));
        }

        return $final;
    }

    private function get_yearno_remaining_credits($dateTo, $new_hist, $yearkey, $leaveType_ID, $uid, $refreshOccur, $isbackup = 0)
    {
        $minusplus = 1;
        $emp_id = $this->session->userdata('emp_id');
        if ($leaveType_ID === null)
        {
            $remaining = '';
        }
        else
        {
            if ($dateTo === date('Y-m-d'))
            {
                $request_leave_details = $this->general_model->fetch_specific_vals("*", "b.date >= '$new_hist->dateFrom' AND b.isActive=1 AND b.isPaid=1 AND YEAR(b.date) = '$yearkey' AND b.overAllStatus IN (2,5) AND a.requestLeave_ID=b.requestLeave_ID AND b.leaveType_ID = $leaveType_ID AND a.uid = $uid", "tbl_request_leave a,tbl_request_leave_details b");
            }
            else
            {
                $request_leave_details = $this->general_model->fetch_specific_vals("*", "b.date BETWEEN '$new_hist->dateFrom' AND '$dateTo' AND b.isActive=1 AND b.isPaid=1 AND YEAR(b.date) = '$yearkey' AND b.overAllStatus IN (2,5) AND a.requestLeave_ID=b.requestLeave_ID AND b.leaveType_ID = $leaveType_ID AND a.uid = $uid", "tbl_request_leave a,tbl_request_leave_details b");
            }
            $total_minutes = 0;
            foreach ($request_leave_details as $rld)
            {
                $total_minutes += $rld->minutes;
            }
            $total_request_leave = $total_minutes / 480;
            if ($isbackup)
            {
                $backupRefreshOccur = $this->general_model->fetch_specific_val("refreshOccur ", "leaveType_ID = $leaveType_ID", "tbl_leave", "leave_id")->refreshOccur;

                $backupCredit = $this->general_model->fetch_specific_val("e.credit", "a.emp_id=$emp_id AND a.isHistory=1 AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND e.isActive=1 AND e.posempstat_id=b.posempstat_id AND e.leaveType_ID=$leaveType_ID AND a.emp_promoteID = " . $new_hist->emp_promoteID, "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_leave_credit e");

                if ($backupCredit === NULL)
                {
                    $remaining = 0;
                }
                else
                {
                    //INSERT HERE QUERY FOR NEWLY REGULARIZED CHURVABELS
                    $credit = $backupCredit->credit;
                    $regularizedDate = $this->newly_regularized_checker($new_hist->posempstat_id, $leaveType_ID);

                    if ($regularizedDate !== NULL)
                    {
                        $start = strtotime($yearkey . "-01-01");
                        $regDate = strtotime($regularizedDate);
                        $end = strtotime($yearkey . "-12-31");
                        if ($backupRefreshOccur === 'quarter')
                        {
                            if ($start <= $regDate && $regDate <= $end)
                            {
                                $deduction = 0;
                                $monthnum = date('n', $regDate);

                                if ($monthnum <= 3)
                                {
                                    $deduction = 3;
                                }
                                else if ($monthnum <= 6)
                                {
                                    $deduction = 6;
                                }
                                else if ($monthnum <= 9)
                                {
                                    $deduction = 9;
                                }

                                $monthsleft = 3 - ($monthnum - $deduction) + $minusplus;

                                $credit = $credit / 3 * $monthsleft;
                            }
                        }
                        else if ($backupRefreshOccur === 'half')
                        {
                            if ($start <= $regDate && $regDate <= $end)
                            {
                                $deduction = 0;
                                $monthnum = date('n', $regDate);
                                if ($monthnum <= 6)
                                {
                                    $deduction = 6;
                                }
                                $monthsleft = 6 - ($monthnum - $deduction) + $minusplus;
                                $credit = $credit / 6 * $monthsleft;
                            }
                        }
                        else if ($backupRefreshOccur === 'year')
                        {
                            if ($yearkey . "" === date('Y', $regDate))
                            {
                                $monthnum = date('n', $regDate);
                                $monthsleft = 12 - $monthnum + $minusplus;
                                $credit = $credit / 12 * $monthsleft;
                            }
                        }
                    }
                    //INSERT HERE  QUERY FOR NEWLY REGULARIZED CHURVABELS
                    //FOR DECIMAL CHURVA OF REGULARIZED
                    if (is_numeric($credit) === true && floor($credit) !== $credit)
                    {
                        $number = (float) $credit;
                        $whole = floor($number);      // 1
                        $fraction = $number - $whole; // .25
                        if ($fraction != 0)
                        {
                            if ($fraction <= 0.25)
                            {
                                $credit = $whole;
                            }
                            else if ($fraction >= 0.75)
                            {
                                $credit = $whole + 1;
                            }
                            else
                            {
                                $credit = (float) $whole . '.5';
                            }
                        }
                    }
                    $remaining = $credit - $total_request_leave;
                }
            }
            else
            {
                //INSERT HERE QUERY FOR NEWLY REGULARIZED CHURVABELS
                $credit = $new_hist->credit;
                $regularizedDate = $this->newly_regularized_checker($new_hist->posempstat_id, $leaveType_ID);
                if ($regularizedDate !== NULL)
                {
                    $start = strtotime($yearkey . "-01-01");
                    $regDate = strtotime($regularizedDate);
                    $end = strtotime($yearkey . "-12-31");
                    if ($refreshOccur === 'quarter')
                    {
                        if ($start <= $regDate && $regDate <= $end)
                        {
                            $deduction = 0;
                            $monthnum = date('n', $regDate);

                            if ($monthnum <= 3)
                            {
                                $deduction = 3;
                            }
                            else if ($monthnum <= 6)
                            {
                                $deduction = 6;
                            }
                            else if ($monthnum <= 9)
                            {
                                $deduction = 9;
                            }

                            $monthsleft = 3 - ($monthnum - $deduction) + $minusplus;

                            $credit = $credit / 3 * $monthsleft;
                        }
                    }
                    else if ($refreshOccur === 'half')
                    {
                        if ($start <= $regDate && $regDate <= $end)
                        {
                            $deduction = 0;
                            $monthnum = date('n', $regDate);
                            if ($monthnum <= 6)
                            {
                                $deduction = 6;
                            }
                            $monthsleft = 6 - ($monthnum - $deduction) + $minusplus;
                            $credit = $credit / 6 * $monthsleft;
                        }
                    }
                    else if ($refreshOccur === 'year')
                    {
                        if ($yearkey . "" === date('Y', $regDate))
                        {
                            $monthnum = date('n', $regDate);
                            $monthsleft = 12 - $monthnum + $minusplus; // plus 1 originally maybe to include current
                            $credit = $credit / 12 * $monthsleft;
                        }
                    }
                }
                //INSERT HERE  QUERY FOR NEWLY REGULARIZED CHURVABELS
                //FOR DECIMAL CHURVA OF REGULARIZED
                if (is_numeric($credit) === true && floor($credit) !== $credit)
                {
                    $number = (float) $credit;
                    $whole = floor($number);      // 1
                    $fraction = $number - $whole; // .25
                    if ($fraction != 0)
                    {
                        if ($fraction <= 0.25)
                        {
                            $credit = $whole;
                        }
                        else if ($fraction >= 0.75)
                        {
                            $credit = $whole + 1;
                        }
                        else
                        {
                            $credit = (float) $whole . '.5';
                        }
                    }
                }
                $remaining = $credit - $total_request_leave;
            }
        }
        return ($remaining < 0) ? 0 : round($remaining, 2);
    }

    private function get_quarterhalf_remaining_credits($yearkey, $refreshOccur, $dateTo, $new_hist, $monthStart, $monthEnd, $leaveType_ID, $uid, $quarterkey, $isbackup = 0)
    {
        $minusplus = 1;
        $emp_id = $this->session->userdata('emp_id');
        if ($leaveType_ID === null)
        {
            $remaining = '';
        }
        else
        {

            if ($dateTo === date('Y-m-d'))
            {
                $request_leave_details = $this->general_model->fetch_specific_vals("*", "b.date >= '$new_hist->dateFrom' AND b.isActive=1 AND b.isPaid=1 AND b.overAllStatus IN (2,5) AND MONTH(b.date) BETWEEN $monthStart AND $monthEnd AND a.requestLeave_ID=b.requestLeave_ID AND b.leaveType_ID = $leaveType_ID AND a.uid = $uid", "tbl_request_leave a,tbl_request_leave_details b");
            }
            else
            {
                $request_leave_details = $this->general_model->fetch_specific_vals("*", "b.date BETWEEN '$new_hist->dateFrom' AND '$dateTo' AND b.isActive=1 AND b.isPaid=1 AND b.overAllStatus IN (2,5) AND MONTH(b.date) BETWEEN $monthStart AND $monthEnd AND a.requestLeave_ID=b.requestLeave_ID AND b.leaveType_ID = $leaveType_ID AND a.uid = $uid", "tbl_request_leave a,tbl_request_leave_details b");
            }
            $total_minutes = 0;
            foreach ($request_leave_details as $rld)
            {
                $total_minutes += $rld->minutes;
            }

            $total_request_leave = $total_minutes / 480;

            //INSERT HERE  QUERY FOR NEWLY REGULARIZED CHURVABELS
            if ($isbackup)
            {
                $backupCredit = $this->general_model->fetch_specific_val("e.credit", "a.emp_id=$emp_id AND a.isHistory=1 AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND e.isActive=1 AND e.posempstat_id=b.posempstat_id AND e.leaveType_ID=$leaveType_ID AND a.emp_promoteID = " . $new_hist->emp_promoteID, "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_leave_credit e");
                $backupRefreshOccur = $this->general_model->fetch_specific_val("refreshOccur ", "leaveType_ID = $leaveType_ID", "tbl_leave", "leave_id")->refreshOccur;
                if ($backupCredit === NULL)
                {
                    $remaining = 0;
                }
                else
                {
                    $credit = $backupCredit->credit;
                    $regularizedDate = $this->newly_regularized_checker($new_hist->posempstat_id, $leaveType_ID);
                    if ($regularizedDate !== NULL)
                    {
                        $s = date('Y-m-d', strtotime($yearkey . '-' . $monthStart . '-01'));
                        $e = date('Y-m-t', strtotime($yearkey . '-' . $monthEnd . '-01'));
                        $start = strtotime($s);
                        $regDate = strtotime($regularizedDate);
                        $end = strtotime($e);
                        if ($backupRefreshOccur === 'quarter')
                        {
                            if ($start <= $regDate && $regDate <= $end)
                            {
                                $deduction = 0;
                                if ($quarterkey === 'Q2')
                                {
                                    $deduction = 3;
                                }
                                else if ($quarterkey === 'Q3')
                                {
                                    $deduction = 6;
                                }
                                else if ($quarterkey === 'Q4')
                                {
                                    $deduction = 9;
                                }


                                $monthnum = date('n', $regDate);
                                $monthsleft = 3 - ($monthnum - $deduction) + $minusplus;
                                $credit = $credit / 3 * $monthsleft;
                            }
                        }
                        else if ($backupRefreshOccur === 'half')
                        {
                            if ($start <= $regDate && $regDate <= $end)
                            {
                                $deduction = 0;
                                if ($quarterkey === 'H2')
                                {
                                    $deduction = 6;
                                }
                                $monthnum = date('n', $regDate);
                                $monthsleft = 6 - ($monthnum - $deduction) + $minusplus;
                                $credit = $credit / 6 * $monthsleft;
                            }
                        }
                        else if ($backupRefreshOccur === 'year')
                        {
                            if ($yearkey . "" === date('Y', $regDate))
                            {

                                $monthnum = date('n', $regDate);
                                $monthsleft = 12 - $monthnum + $minusplus;
                                $credit = $credit / 12 * $monthsleft;
                            }
                        }
                    }
                    //INSERT HERE  QUERY FOR NEWLY REGULARIZED CHURVABELS
                    //FOR DECIMAL CHURVA OF REGULARIZED
                    if (is_numeric($credit) === true && floor($credit) !== $credit)
                    {
                        $number = (float) $credit;
                        $whole = floor($number);      // 1
                        $fraction = $number - $whole; // .25
                        if ($fraction != 0)
                        {
                            if ($fraction <= 0.25)
                            {
                                $credit = $whole;
                            }
                            else if ($fraction >= 0.75)
                            {
                                $credit = $whole + 1;
                            }
                            else
                            {
                                $credit = (float) $whole . '.5';
                            }
                        }
                    }
                    $remaining = $credit - $total_request_leave;
                }
            }
            else
            {
                //INSERT HERE QUERY FOR NEWLY REGULARIZED CHURVABELS
                $credit = $new_hist->credit;
                $regularizedDate = $this->newly_regularized_checker($new_hist->posempstat_id, $leaveType_ID);
                if ($regularizedDate !== NULL)
                {
//                    echo $quarterkey.'------------------------------------------------<br>';
//                    echo $refreshOccur;
                    if ($refreshOccur === 'quarter')
                    {
                        $s = date('Y-m-d', strtotime($yearkey . '-' . $monthStart . '-01'));
                        $e = date('Y-m-t', strtotime($yearkey . '-' . $monthEnd . '-01'));
                        $start = strtotime($s);
                        $regDate = strtotime($regularizedDate);
                        $end = strtotime($e);
//                        echo $s.'<br>';
//                        echo date('Y-m-d', strtotime($regularizedDate)).'<br>';
//                        echo $e.'<br>';
                        if ($start <= $regDate && $regDate <= $end)
                        {
                            $deduction = 0;
                            if ($quarterkey === 'Q2')
                            {
                                $deduction = 3;
                            }
                            else if ($quarterkey === 'Q3')
                            {
                                $deduction = 6;
                            }
                            else if ($quarterkey === 'Q4')
                            {
                                $deduction = 9;
                            }
                            $monthnum = date('n', $regDate);
//                            var_dump($monthnum);
                            $monthsleft = 3 - ($monthnum - $deduction) + $minusplus;
//                            var_dump($monthsleft);
                            $credit = $credit / 3 * $monthsleft;
//                            var_dump($credit);
                        }
                    }
                    else if ($refreshOccur === 'half')
                    {
                        $start = strtotime($monthStart);
                        $regDate = strtotime($regularizedDate);
                        $end = strtotime($monthEnd);
                        if ($start <= $regDate && $regDate <= $end)
                        {
                            $deduction = 0;
                            if ($quarterkey === 'H2')
                            {
                                $deduction = 6;
                            }
                            $monthnum = date('n', $regDate);
                            $monthsleft = 6 - ($monthnum - $deduction) + $minusplus;
                            $credit = $credit / 6 * $monthsleft;
                        }
                    }
                    else if ($refreshOccur === 'year')
                    {
                        if ($yearkey . "" === date('Y', $regDate))
                        {
                            $monthnum = date('n', $regDate);

                            $monthsleft = 12 - $monthnum + $minusplus;
                            $credit = $credit / 12 * $monthsleft;
                        }
                    }
                }
                //INSERT HERE  QUERY FOR NEWLY REGULARIZED CHURVABELS
                //FOR DECIMAL CHURVA OF REGULARIZED
                if (is_numeric($credit) === true && floor($credit) !== $credit)
                {
                    $number = (float) $credit;
                    $whole = floor($number);      // 1
                    $fraction = $number - $whole; // .25
                    if ($fraction != 0)
                    {
                        if ($fraction <= 0.25)
                        {
                            $credit = $whole;
                        }
                        else if ($fraction >= 0.75)
                        {
                            $credit = $whole + 1;
                        }
                        else
                        {
                            $credit = (float) $whole . '.5';
                        }
                    }
                }
                $remaining = $credit - $total_request_leave;
            }
        }
        return ($remaining < 0) ? 0 : round($remaining, 2);
    }

    public function new_leave_request()
    {
        $uid = $this->session->userdata('uid');
        $leave_id = $this->input->post('leave_id');
        $reason = $this->input->post('reason');
        $data = $this->input->post('data');
        $notif_details = NULL;
        $backupLeaveType_ID = NULL;
        $leave = $this->general_model->fetch_specific_val("b.leaveType_ID,a.backupLeave_ID", "a.leave_id=$leave_id AND a.leaveType_ID=b.leaveType_ID", "tbl_leave a,tbl_leave_type b");
        if ($leave->backupLeave_ID !== NULL)
        {
            $backupLeaveType = $this->general_model->fetch_specific_val("b.leaveType_ID", "a.leave_id=" . $leave->backupLeave_ID . " AND a.leaveType_ID=b.leaveType_ID", "tbl_leave a,tbl_leave_type b");
            $backupLeaveType_ID = $backupLeaveType->leaveType_ID;
        }
        $leaveType_ID = $leave->leaveType_ID;
        $backupLeave_ID = $leave->backupLeave_ID;
        $arr = array(
            'uid' => $uid,
            'reason' => $reason,
            'finalStatus' => 2 //pending
        );
        $requestLeave_ID = $this->general_model->insert_vals_last_inserted_id($arr, "tbl_request_leave");
        if ($requestLeave_ID === 0)
        {
            $status = "Failed";
        }
        else
        {
            $arr2 = array();
            foreach ($data as $d)
            {
                $temp_array = array(
                    'requestLeave_ID' => $requestLeave_ID,
                    'uid' => $uid,
                    'date' => $d['date'],
                    'minutes' => ($d['fullhalf'] === '480') ? 480 : 240,
                    'isPaid' => ($d['useBackup'] === 'false') ? $d['paid'] : 1,
                    'isActive' => 1,
                    'isRetracted' => 0,
                    'isReleased' => 0,
                    'schedule_ID' => ($d['schedule_ID'] === '') ? NULL : $d['schedule_ID'],
                    'overAllStatus' => 2, //pending
                    'leaveType_ID' => ($d['useBackup'] === 'false') ? $leaveType_ID : $backupLeaveType_ID,
                    'leave_id' => ($d['useBackup'] === 'false') ? $leave_id : $backupLeave_ID,
                    'original_leave_ID' => $leave_id,
                    'original_leaveType_ID' => $leaveType_ID,
                    'isBackup' => ($d['useBackup'] === 'false') ? 0 : 1
                );
                array_push($arr2, $temp_array);
            }
            $result = $this->general_model->batch_insert($arr2, "tbl_request_leave_details");
            $status = ($result) ? "Success" : "Failed";
            //approval codes -------------------------------------------------------------------------

            $request_leave_details = $this->general_model->fetch_specific_vals("requestLeaveDetails_ID", "requestLeave_ID=$requestLeave_ID", "tbl_request_leave_details");
            $createdOn = $this->general_model->fetch_specific_val("createdOn", "requestLeave_ID=$requestLeave_ID", "tbl_request_leave")->createdOn;
            $approvers = $this->set_approvers($createdOn, $uid, 2, 'requestLeave_ID', $requestLeave_ID);
            $formatted_approvers = array();
            foreach ($approvers as $raw)
            {
                $formatted_approvers[] = array(
                    'requestLeave_ID' => $raw['requestLeave_ID'],
                    'uid' => $raw['userId'],
                    'approvalLevel' => $raw['approvalLevel'],
                    'semiStatus' => $raw['approvalStatus_ID']
                );
            }
            foreach ($formatted_approvers as $approver)
            {
                $data = array();
                $requestLeaveApproval_ID = $this->general_model->insert_vals_last_inserted_id($approver, "tbl_request_leave_approval");
                foreach ($request_leave_details as $rld)
                {
                    $data[] = array(
                        'requestLeaveApproval_ID' => $requestLeaveApproval_ID,
                        'requestLeaveDetails_ID' => $rld->requestLeaveDetails_ID,
                        'status' => 2
                    );
                }
                $res = $this->general_model->batch_insert($data, "tbl_request_leave_approval_details");
                $status = ($res) ? "Success" : "Failed";
            }
            // SET NOTIFICATION
            $notif_mssg = "has sent you a leave request.  <br><small><b>LR-ID</b>: " . str_pad($requestLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "Leave/approval";
            $notif_details = $this->set_notif($uid, $notif_mssg, $link, $approvers);
        }
        echo json_encode(array('status' => $status, 'notif_details' => $notif_details));
    }

//START----------------------------------------------

    public function get_other_request_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');
        $searchcustom = $this->input->post('searchcustom');
        $uid = $this->session->userdata('uid');
        $search = array();
        $searchString = "";
        if ($searchcustom !== '')
        {
            $search[] = "reason LIKE '%$searchcustom%'";
        }
        if (!empty($search))
        {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT requestLeave_ID,reason,createdOn FROM tbl_request_leave WHERE uid=$uid AND finalStatus NOT IN (2,12) AND createdOn between date('$datestart') AND date('$dateend') $searchString  ORDER BY createdOn DESC";

        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");

        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    public function get_ongoing_request_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $uid = $this->session->userdata('uid');
        $query = "SELECT requestLeave_ID,reason,createdOn FROM tbl_request_leave WHERE uid=$uid AND finalStatus IN (2,12) ORDER BY createdOn DESC";
        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    //END--------------------------------------------------------------
    public function get_request_details()
    {
        $requestLeave_ID = $this->input->post('requestLeave_ID');
        // I INCLUDED THE MISSED PARA MADELETE NALANG ANG MISSED DATES
        // $count = $this->general_model->fetch_specific_val("COUNT(*) as count", " requestLeave_ID = $requestLeave_ID AND approvalLevel = 1 AND semiStatus IN (4,2)", " tbl_request_leave_approval ")->count;
        $count = $this->general_model->fetch_specific_val("COUNT(*) as count", " requestLeave_ID = $requestLeave_ID AND approvalLevel = 1 AND semiStatus IN (4,2,12)", " tbl_request_leave_approval ")->count;
        $data = $this->general_model->fetch_specific_vals("d.pickerMode,c.leaveType,a.requestLeaveDetails_ID,a.date,a.minutes,a.isPaid,a.isActive,a.isRetracted,a.isReleased,a.overAllStatus,a.createdOn,b.description", "requestLeave_ID = $requestLeave_ID AND a.overAllStatus=b.status_ID AND a.leaveType_ID = c.leaveType_ID AND d.leaveType_ID=c.leaveType_ID AND d.leave_ID=a.leave_ID", " tbl_request_leave_details a, tbl_status b,tbl_leave_type c,tbl_leave d", " date ASC");

        echo json_encode(array('data' => $data, 'approvalCount' => $count));
    }

    public function delete_request()
    {
        $name = $this->general_model->fetch_specific_val("a.fname,a.lname", "a.apid=b.apid AND b.emp_id=" . $this->session->userdata('emp_id'), "tbl_applicant a,tbl_employee b");
        $requestLeave_ID = $this->input->post('requestLeave_ID');
        $approval = $this->general_model->fetch_specific_vals("requestLeaveApproval_ID,uid", "requestLeave_ID=$requestLeave_ID", "tbl_request_leave_approval", "approvalLevel ASC");
        $ids = array();
        $res = array();
        foreach ($approval as $app)
        {
            $ids[] = $app->requestLeaveApproval_ID;
        }
        $res[] = $this->general_model->delete_vals("requestLeaveApproval_ID IN (" . implode(',', $ids) . ")", "tbl_request_leave_approval_details");
        $res[] = $this->general_model->delete_vals("requestLeave_ID=$requestLeave_ID", "tbl_request_leave_approval");
        $res[] = $this->general_model->delete_vals("requestLeave_ID=$requestLeave_ID", "tbl_request_leave_details");
        $res[] = $this->general_model->delete_vals("requestLeave_ID=$requestLeave_ID", "tbl_request_leave");
        $res[] = $this->general_model->delete_vals("request_ID=$requestLeave_ID AND module_ID=2", "tbl_deadline");
        $notif_details = array();
        if (array_sum($res) == count($res))
        {
            $status = ($res[0] === 1) ? "Success" : "Error";
            // SET NOTIFICATION
            $notif_mssg = $name->fname . " " . $name->lname . " has deleted the filed leave request.  <br><small><b>LR-ID</b>: " . str_pad($requestLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "Leave/approval";
            $notif_details[] = $this->set_system_notif($notif_mssg, $link, array(array('userId' => $approval[0]->uid)));
        }
        else
        {
            $status = "Error";
        }
        echo json_encode(array('status' => $status, 'notif_details' => $notif_details));
    }

    public function deleteTransferStat($leave)
    {
        $backup_ld = $this->general_model->fetch_specific_vals("requestLeaveDetails_ID,minutes,isPaid,leaveType_ID,leave_ID,isBackup", "requestLeave_ID=" . $leave->requestLeave_ID . ' AND isBackup=1 AND requestLeaveDetails_ID <>' . $leave->requestLeaveDetails_ID, "tbl_request_leave_details", "date ASC");
        $unpaid_ld = $this->general_model->fetch_specific_vals("requestLeaveDetails_ID,minutes,isPaid,leaveType_ID,leave_ID,isBackup", "requestLeave_ID=" . $leave->requestLeave_ID . ' AND isPaid=0 AND requestLeaveDetails_ID <>' . $leave->requestLeaveDetails_ID, "tbl_request_leave_details", "date ASC");

        if (COUNT($unpaid_ld) !== 0 || COUNT($backup_ld) !== 0)
        {
            if ($leave->isBackup === 1)
            {
                return true;
            }
            else
            {
                $res = array();
                if ($leave->isPaid === '1')
                {
                    $replacement = NULL;
                    if ($leave->minutes === '480')
                    {
                        foreach ($unpaid_ld as $ld)
                        {
                            if ($ld->minutes === '480')
                            {
                                $replacement = $ld;
                                break;
                            }
                        }
                        if ($replacement === NULL)
                        {
                            $temp = array();
                            foreach ($unpaid_ld as $ld)
                            {
                                if ($ld->minutes === '240')
                                {
                                    $temp[] = $ld;
                                }
                            }
                            if (COUNT($temp) !== 0)
                            {
                                $replacement = array_slice($temp, 0, 2);
                            }
                        }
                        if ($replacement === NULL)
                        {
                            foreach ($backup_ld as $ld)
                            {
                                if ($ld->minutes === '480')
                                {
                                    $replacement = $ld;
                                    break;
                                }
                            }
                            if ($replacement === NULL)
                            {
                                $temp = array();
                                foreach ($backup_ld as $ld)
                                {
                                    if ($ld->minutes === '240')
                                    {
                                        $temp[] = $ld;
                                    }
                                }
                                if (COUNT($temp) !== 0)
                                {
                                    $replacement = array_slice($temp, 0, 2);
                                }
                            }
                            if ($replacement !== NULL)
                            {
                                if (isset($leave->leaveType_ID) && isset($leave->leave_ID))
                                {
                                    $res[] = $this->general_model->update_vals(array('isPaid' => 1, 'isBackup' => 0, 'leaveType_ID' => $leave->leaveType_ID, 'leave_ID' => $leave->leave_ID), "requestLeaveDetails_ID=" . $replacement->requestLeaveDetails_ID, "tbl_request_leave_details");
                                }
                                else
                                {
                                    foreach ($replacement as $replace)
                                    {
                                        $res[] = $this->general_model->update_vals(array('isPaid' => 1, 'isBackup' => 0, 'leaveType_ID' => $leave->leaveType_ID, 'leave_ID' => $leave->leave_ID), "requestLeaveDetails_ID=" . $replace->requestLeaveDetails_ID, "tbl_request_leave_details");
                                    }
                                }
                            }//error 1861 nicca
                            else
                            {
                                $res[] = 1;
                            }
                        }
                        else
                        {
                            if (isset($replacement->requestLeaveDetails_ID))
                            {
                                $res[] = $this->general_model->update_vals(array('isPaid' => 1), "requestLeaveDetails_ID=" . $replacement->requestLeaveDetails_ID, "tbl_request_leave_details");
                            }
                            else
                            {
                                foreach ($replacement as $replace)
                                {
                                    $res[] = $this->general_model->update_vals(array('isPaid' => 1), "requestLeaveDetails_ID=" . $replace->requestLeaveDetails_ID, "tbl_request_leave_details");
                                }
                            }
                        }
                    }
                    else
                    {
                        $temp = array();
                        foreach ($unpaid_ld as $ld)
                        {
                            if ($ld->minutes === '240')
                            {
                                $temp[] = $ld;
                            }
                        }

                        if (COUNT($temp) !== 0)
                        {
                            $replacement = array_slice($temp, 0, 1);
                        }
                        if ($replacement === NULL)
                        {
                            $temp = array();
                            foreach ($backup_ld as $ld)
                            {
                                if ($ld->minutes === '240')
                                {
                                    $temp[] = $ld;
                                }
                            }
                            if (COUNT($temp) !== 0)
                            {
                                $replacement = array_slice($temp, 0, 1);
                            }
                            if ($replacement !== NULL)
                            {
                                if (isset($leave->leaveType_ID) && isset($leave->leave_ID))
                                {
                                    $res[] = $this->general_model->update_vals(array('isPaid' => 1, 'isBackup' => 0, 'leaveType_ID' => $leave->leaveType_ID, 'leave_ID' => $leave->leave_ID), "requestLeaveDetails_ID=" . $replacement->requestLeaveDetails_ID, "tbl_request_leave_details");
                                }
                                else
                                {
                                    foreach ($replacement as $replace)
                                    {
                                        $res[] = $this->general_model->update_vals(array('isPaid' => 1, 'isBackup' => 0, 'leaveType_ID' => $leave->leaveType_ID, 'leave_ID' => $leave->leave_ID), "requestLeaveDetails_ID=" . $replace->requestLeaveDetails_ID, "tbl_request_leave_details");
                                    }
                                }
                            }
                            else
                            {
                                $res[] = 1;
                            }
                        }
                        else
                        {
                            if (isset($replacement->requestLeaveDetails_ID))
                            {
                                $res[] = $this->general_model->update_vals(array('isPaid' => 1), "requestLeaveDetails_ID=" . $replacement->requestLeaveDetails_ID, "tbl_request_leave_details");
                            }
                            else
                            {
                                foreach ($replacement as $replace)
                                {
                                    $res[] = $this->general_model->update_vals(array('isPaid' => 1), "requestLeaveDetails_ID=" . $replace->requestLeaveDetails_ID, "tbl_request_leave_details");
                                }
                            }
                        }
                    }

                    if (array_sum($res) == count($res))
                    {
                        $status = ($res === 1) ? true : false;
                    }
                    else
                    {
                        $status = false;
                    }
                    return $status;
                }
                else
                {
                    return true;
                }
            }
        }
        else
        {
            return true;
        }
    }

    public function delete_request_detail()
    {//kulang pa checker if paid ang gidelete then change unpaid to paid remaining
        $requestLeaveDetails_ID = $this->input->post('requestLeaveDetails_ID');
        $name = $this->general_model->fetch_specific_val("a.fname,a.lname", "a.apid=b.apid AND b.emp_id=" . $this->session->userdata('emp_id'), "tbl_applicant a,tbl_employee b");
        $leave = $this->general_model->fetch_specific_val("requestLeave_ID,requestLeaveDetails_ID,minutes,isPaid,leaveType_ID,leave_ID,isBackup", "requestLeaveDetails_ID=$requestLeaveDetails_ID", "tbl_request_leave_details");
        $notif_details = array();
        if ($leave === NULL)
        {
            $status = "Failed";
        }
        else
        {
            $res0 = $this->general_model->delete_vals("requestLeaveDetails_ID=$requestLeaveDetails_ID", "tbl_request_leave_approval_details");
            $res = $this->general_model->delete_vals("requestLeaveDetails_ID=$requestLeaveDetails_ID", "tbl_request_leave_details");
            //CHECK FIRST FOR UNPAID CHURVA
            $this->deleteTransferStat($leave);

            //CHECK FIRST FOR UNPAID CHURVA
            if ($res && $res0)
            {
                $count = $this->general_model->fetch_specific_val("COUNT(requestLeaveDetails_ID) as count", "requestLeave_ID=" . $leave->requestLeave_ID, "tbl_request_leave_details")->count;

                if ($count === '0')
                {
                    $approver_1 = $this->general_model->fetch_specific_val("uid", "approvalLevel=1 AND requestLeave_ID=" . $leave->requestLeave_ID, "tbl_request_leave_approval");
                    $notif_mssg = $name->fname . " " . $name->lname . " has deleted the filed leave request.  <br><small><b>LR-ID</b>: " . str_pad($leave->requestLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                    $link = "Leave/approval";
                    $notif_details[] = $this->set_system_notif($notif_mssg, $link, array(array('userId' => $approver_1->uid)));
                    //delete request
                    $res2a = $this->general_model->delete_vals("requestLeave_ID=" . $leave->requestLeave_ID, "tbl_request_leave_approval");
                    $res2b = $this->general_model->delete_vals("requestLeave_ID=" . $leave->requestLeave_ID, "tbl_request_leave");
                    $res2c = $this->general_model->delete_vals("request_ID=" . $leave->requestLeave_ID . " AND module_ID=2", "tbl_deadline");
                    $status = ($res2a && $res2b && $res2c) ? "All" : "Failed";
                }
                else
                {
                    $approver_1 = $this->general_model->fetch_specific_val("uid", "approvalLevel=1 AND requestLeave_ID=" . $leave->requestLeave_ID, "tbl_request_leave_approval");
                    $notif_mssg = $name->fname . " " . $name->lname . " has deleted a leave date from the filed leave request.  <br><small><b>LR-ID</b>: " . str_pad($leave->requestLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                    $link = "Leave/approval";
                    $notif_details[] = $this->set_system_notif($notif_mssg, $link, array(array('userId' => $approver_1->uid)));

                    $status = "Success";
                }
            }
            else
            {
                $status = "Failed";
            }
        }
        echo json_encode(array('status' => $status, 'notif_details' => $notif_details));
    }

    //START ____________________________________________________
    public function get_ongoing_approval_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $uid = $this->session->userdata('uid');
        $query = "SELECT a.requestLeave_ID,a.reason,a.createdOn,e.fname,e.lname FROM tbl_request_leave a, tbl_request_leave_approval b ,tbl_user c,tbl_employee d,tbl_applicant e WHERE c.emp_id=d.emp_id AND d.apid=e.apid AND b.uid=$uid AND b.semiStatus = 4 AND a.requestLeave_ID=b.requestLeave_ID AND a.uid=c.uid ORDER BY a.createdOn DESC";
        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    public function get_other_approval_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');
        $searchcustom = $this->input->post('searchcustom');
        $uid = $this->session->userdata('uid');
        $search = array();
        $searchString = "";
        if ($searchcustom !== '')
        {
            $search[] = "reason LIKE '%$searchcustom%'";
        }
        if (!empty($search))
        {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT a.requestLeave_ID,a.reason,a.createdOn,e.fname,e.lname FROM tbl_request_leave a, tbl_request_leave_approval b ,tbl_user c,tbl_employee d,tbl_applicant e WHERE c.emp_id = d.emp_id AND d.apid=e.apid AND b.uid=$uid AND b.semiStatus NOT IN (4,2,7) AND a.requestLeave_ID=b.requestLeave_ID AND a.uid=c.uid AND a.createdOn between date('$datestart') AND date('$dateend') $searchString  ORDER BY a.createdOn DESC";

        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");

        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    //END _________________________________________________________
    public function submit_approval_decision()
    {
        $uid = $this->session->userdata('uid');
        $requestLeave_ID = $this->input->post('requestLeave_ID');
        $comment = $this->input->post('comment');
        $semiStatus = $this->input->post('semiStatus');
        $data = $this->input->post('data');
        $from = $this->input->post('from');
        if ($from === 'missed')
        {
            $module = $this->general_model->fetch_specific_val("numberOfApproval", "module_ID=2", "tbl_module");
            $num = (int) $module->numberOfApproval + 1;
            $arrayx = array('requestLeave_ID' => $requestLeave_ID, 'uid' => $uid, 'approvalLevel' => $num, 'semiStatus' => 4, 'decisionOn' => date("Y-m-d H:i:s"), 'comment' => $comment);
            $request_leave_details = $this->general_model->fetch_specific_vals("requestLeaveDetails_ID", "requestLeave_ID=$requestLeave_ID", "tbl_request_leave_details");
            $requestLeaveApproval_ID = $this->general_model->insert_vals_last_inserted_id($arrayx, "tbl_request_leave_approval");
            foreach ($request_leave_details as $rld)
            {
                $datax[] = array(
                    'requestLeaveApproval_ID' => $requestLeaveApproval_ID,
                    'requestLeaveDetails_ID' => $rld->requestLeaveDetails_ID,
                    'status' => 2
                );
            }
            $this->general_model->batch_insert($datax, "tbl_request_leave_approval_details");
        }
        $requestLeaveApproval = $this->general_model->fetch_specific_val("requestLeaveApproval_ID,approvalLevel", "requestLeave_ID=$requestLeave_ID AND uid=$uid", "tbl_request_leave_approval");
        $requestLeave = $this->general_model->fetch_specific_val("a.uid,a.reason,a.finalStatus,a.createdOn,a.updatedOn,d.fname,d.lname", "a.requestLeave_ID=$requestLeave_ID AND a.uid=b.uid AND b.emp_id = c.emp_id AND c.apid=d.apid", "tbl_request_leave a,tbl_user b,tbl_employee c,tbl_applicant d");
        $notif_details = array();
        $arr = array();
        foreach ($data as $d)
        {
            $arr[] = array(
                'data' => array('status' => ($d['stat'] === 'true') ? 5 : 6),
                'where' => "requestLeaveApproval_ID=" . $requestLeaveApproval->requestLeaveApproval_ID . " AND requestLeaveDetails_ID=" . $d['requestLeaveDetails_ID']
            );
        }
        $rlad_stat = $this->general_model->update_array_vals($arr, "tbl_request_leave_approval_details");

        if ($rlad_stat)
        {
            $rla_stat = $this->general_model->update_vals(array('semiStatus' => intval($semiStatus), 'decisionOn' => date("Y-m-d H:i:s")), "requestLeaveApproval_ID=" . $requestLeaveApproval->requestLeaveApproval_ID, "tbl_request_leave_approval");
            if ($rla_stat)
            {
                if (intval($semiStatus) === 6)
                {
                    if ($from !== 'missed')
                    {
                        //COMPLETE DEADLINE-------------------------------------------------------------------------------------------------------

                        $this->general_model->update_vals(array('status_ID' => 3), "userId=$uid AND request_ID=$requestLeave_ID AND module_ID=2", "tbl_deadline");
                        $deadline = $this->general_model->fetch_specific_val("approvalLevel", "userId=$uid AND request_ID=$requestLeave_ID AND module_ID=2", "tbl_deadline");
                        $this->general_model->update_vals(array('status_ID' => 7), "approvalLevel>" . $deadline->approvalLevel . " AND request_ID=$requestLeave_ID AND module_ID=2", "tbl_deadline");

                        //COMPLETE DEADLINE-------------------------------------------------------------------------------------------------------
                    }
                    $approvalLevel = intval($requestLeaveApproval->approvalLevel); //7==cancelled cancel uppers
                    $upper_approvers = $this->general_model->fetch_specific_vals("requestLeaveApproval_ID", "requestLeave_ID=$requestLeave_ID AND approvalLevel>$approvalLevel", "tbl_request_leave_approval");
                    $array_reject = array();
                    foreach ($upper_approvers as $uppers)
                    {
                        $array_reject[] = array(
                            'data' => array('status' => 7),
                            'where' => "requestLeaveApproval_ID=" . $uppers->requestLeaveApproval_ID
                        );
                    }

                    $rej_rlad = $this->general_model->update_array_vals($array_reject, "tbl_request_leave_approval_details");
                    $rej_rla = $this->general_model->update_vals(array('semiStatus' => 7, 'decisionOn' => date("Y-m-d H:i:s")), "requestLeave_ID=$requestLeave_ID AND approvalLevel>$approvalLevel", "tbl_request_leave_approval");
                    $rej_rl = $this->general_model->update_vals(array('finalStatus' => intval($semiStatus)), "requestLeave_ID=$requestLeave_ID", "tbl_request_leave");
                    $rej_rld = $this->general_model->update_vals(array('overAllStatus' => intval($semiStatus)), "requestLeave_ID=$requestLeave_ID", "tbl_request_leave_details");

                    $status = ($rej_rl && $rej_rla && $rej_rlad && $rej_rld) ? "Success" : "Failed";
                    // SET NOTIFICATION FOR REQUESTOR
                    $notif_mssg = "has rejected your leave request.  <br><small><b>LR-ID</b>: " . str_pad($requestLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                    $link = "Leave/request";
                    $notif_details[] = $this->set_notif($uid, $notif_mssg, $link, array(array('userId' => $requestLeave->uid)));
                }
                else
                {
                    if ($from !== 'missed')
                    {
                        //COMPLETE DEADLINE-------------------------------------------------------------------------------------------------------
                        $this->general_model->update_vals(array('status_ID' => 3), "userId=$uid AND request_ID=$requestLeave_ID AND module_ID=2", "tbl_deadline");
                        //COMPLETE DEADLINE-------------------------------------------------------------------------------------------------------
                    }
                    $approvalLevel = intval($requestLeaveApproval->approvalLevel) + 1;
                    $nextRLA = $this->general_model->fetch_specific_val("requestLeaveApproval_ID,approvalLevel", "requestLeave_ID=$requestLeave_ID AND approvalLevel=$approvalLevel", "tbl_request_leave_approval");
                    if (empty($nextRLA) || $nextRLA === NULL)
                    {
                        //FINAL APPROVER ALREADY
                        $last_stat = $this->general_model->update_vals(array('finalStatus' => $semiStatus), "requestLeave_ID=$requestLeave_ID", "tbl_request_leave");
                        //update the requestleavedetails final decision

                        $arr_rejected = array();
                        $arr_approved = array();
                        $ids = array();
                        foreach ($data as $d)
                        {
                            if ($d['stat'] !== 'true')
                            {
                                $arr_rejected[] = array(
                                    'data' => array('overAllStatus' => 6),
                                    'where' => "requestLeaveDetails_ID=" . $d['requestLeaveDetails_ID']
                                );
                            }
                            else
                            {
                                $arr_approved[] = array(
                                    'data' => array('overAllStatus' => 5),
                                    'where' => "requestLeaveDetails_ID=" . $d['requestLeaveDetails_ID']
                                );
                                $ids[] = $d['requestLeaveDetails_ID'];
                            }
                        }

                        $rld_stat_app = $this->general_model->update_array_vals($arr_rejected, "tbl_request_leave_details");
                        $rld_stat_rej = $this->general_model->update_array_vals($arr_approved, "tbl_request_leave_details");
                        $status = ($last_stat && $rld_stat_app && $rld_stat_rej) ? "Success" : "Failed";

                        //CHANGE SCHEDULE//------------------------------------------------
//                        $approvedDetails = $this->general_model->fetch_specific_vals("a.date,a.leaveType_ID,c.emp_id,a.isPaid,a.schedule_ID,a.minutes", "a.requestLeaveDetails_ID IN (" . implode(',', $ids) . ") AND a.requestLeave_ID=b.requestLeave_ID AND b.uid=c.uid", 'tbl_request_leave_details a,tbl_request_leave b,tbl_user c');
                        $approvedDetails = $this->general_model->custom_query("SELECT a.date,a.leaveType_ID,c.emp_id,a.isPaid,a.schedule_ID,a.minutes,d.schedtype_id,a.requestLeaveDetails_ID FROM tbl_request_leave_details a INNER JOIN tbl_request_leave b ON a.requestLeave_ID=b.requestLeave_ID INNER JOIN tbl_user c ON b.uid=c.uid LEFT JOIN tbl_schedule d ON a.schedule_ID=d.sched_id WHERE a.requestLeaveDetails_ID IN (" . implode(',', $ids) . ")");
                        $insert_schedtype = array('4', '5', '7', '9'); //include 1 if apil ang normal shift
                        foreach ($approvedDetails as $appr)
                        {
                            //3 - with pay | 8 - without pay
                            $data = array(
                                'emp_id' => $appr->emp_id,
                                'sched_date' => $appr->date,
                                'schedtype_id' => ($appr->isPaid === '1') ? 3 : 8,
                                'acc_time_id' => NULL,
                                'remarks' => ($appr->minutes === '480') ? 'Request-Full' : 'Request-Half',
                                'leavenote' => $appr->leaveType_ID,
                                'updated_by' => $uid,
                                'updated_on' => date('Y-m-d H:i:s'),
                                'isLocked' => 14
                            );
                            // if (strtotime(date('YYYY-MM-DD')) >= strtotime('2019-01-01'))
                            // {
                           if ($appr->minutes === '240')
                            {
                                $retract_sched_id = $this->general_model->insert_vals_last_inserted_id($data, "tbl_schedule");
                            }
                            else if ($appr->schedule_ID === NULL || $appr->schedule_ID === 0 || in_array($appr->schedtype_id, $insert_schedtype))
                            {
                                $retract_sched_id = $this->general_model->insert_vals_last_inserted_id($data, "tbl_schedule"); //ploase add update if nag exist 
                            }
                            else
                            {
                                $this->general_model->update_vals($data, "sched_id=" . $appr->schedule_ID, "tbl_schedule");
                                $retract_sched_id = $appr->schedule_ID;
                            }
                            if($retract_sched_id!==0 && $retract_sched_id!==NULL){
                                $this->general_model->update_vals(array('retract_sched_id'=>$retract_sched_id),"requestLeaveDetails_ID = ".$appr->requestLeaveDetails_ID, "tbl_request_leave_details");
                            }
                            // }
                        }
                        //END OF CHANGE SCHEDULE//-----------------------------------------
                        //system_notify user
                        // SET NOTIFICATION FOR REQUESTOR
                        $notif_mssg = "has approved your leave request.   <br><small><b>LR-ID</b>: " . str_pad($requestLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                        $link = "Leave/request";
                        $notif_details[] = $this->set_notif($uid, $notif_mssg, $link, array(array('userId' => $requestLeave->uid)));
                    }
                    else
                    {
                        //HAS NEXT APPROVER
                        $last_stat = $this->general_model->update_vals(array('semiStatus' => 4, 'decisionOn' => date("Y-m-d H:i:s")), "requestLeaveApproval_ID=" . $nextRLA->requestLeaveApproval_ID, "tbl_request_leave_approval");
                        $arr_rejected_approval = array();
                        $arr_rejected = array();
                        $approvalLevel = intval($requestLeaveApproval->approvalLevel); //7==cancelled
                        $upper_approvers = $this->general_model->fetch_specific_vals("requestLeaveApproval_ID,uid", "requestLeave_ID=$requestLeave_ID AND approvalLevel>$approvalLevel", "tbl_request_leave_approval");

                        foreach ($data as $d)
                        {
                            if ($d['stat'] !== 'true')
                            {
                                $arr_rejected[] = array(
                                    'data' => array('overAllStatus' => 6),
                                    'where' => "requestLeaveDetails_ID=" . $d['requestLeaveDetails_ID']
                                );
                                foreach ($upper_approvers as $uppers)
                                {
                                    $arr_rejected_approval[] = array(
                                        'data' => array('status' => 7),
                                        'where' => "requestLeaveDetails_ID=" . $d['requestLeaveDetails_ID'] . " AND requestLeaveApproval_ID=" . $uppers->requestLeaveApproval_ID
                                    );
                                }
                            }
                        }

                        $rld_stat_app = $this->general_model->update_array_vals($arr_rejected, "tbl_request_leave_details");
                        $rld_stat_approval = $this->general_model->update_array_vals($arr_rejected_approval, "tbl_request_leave_approval_details");
                        $status = ($last_stat && $rld_stat_app && $rld_stat_approval) ? "Success" : "Failed";
                        //notify next approver and user
                        // SET NOTIFICATION FOR REQUESTOR
                        $notif_mssg = "has approved your leave request and forwarded it to the next approver.   <br><small><b>LR-ID</b>: " . str_pad($requestLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                        $link = "Leave/request";
                        $notif_details[] = $this->set_notif($uid, $notif_mssg, $link, array(array('userId' => $requestLeave->uid)));
                        // SET NOTIFICATION FOR NEXT APPROVER
                        $notif_mssg = "forwarded you the leave request of " . $requestLeave->lname . ", " . $requestLeave->fname . ".   <br><small><b>LR-ID</b>: " . str_pad($requestLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                        $link = "Leave/approval";
                        $notif_details[] = $this->set_notif($uid, $notif_mssg, $link, array(array('userId' => $upper_approvers[0]->uid)));
                    }
                }
            }
            else
            {
                $status = "Failed";
            }
        }
        else
        {
            $status = "Failed";
        }
        echo json_encode(array('status' => $status, 'notif_details' => $notif_details));
    }

    public function get_request_approval_flow()
    {
        $requestLeave_ID = $this->input->post('requestLeave_ID');
        $request_leave = $this->general_model->fetch_specific_val("a.uid, a.reason, a.finalStatus, a.createdOn, b.emp_id, d.fname, d.lname,d.pic", "a.requestLeave_ID=$requestLeave_ID AND a.uid=b.uid AND b.emp_id=c.emp_id AND c.apid=d.apid", "tbl_request_leave a,tbl_user b, tbl_employee c,tbl_applicant d");
        $job = $this->general_model->fetch_specific_val("c.pos_details,d.status", "a.emp_id = " . $request_leave->emp_id . " AND a.isActive=1 AND a.ishistory=1 AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d");
        $request_leave->job = $job->pos_details;
        $request_leave->jobstat = $job->status;
        $request_leave_approval = $this->general_model->fetch_specific_vals("a.requestLeaveApproval_ID,a.approvalLevel,a.comment,a.decisionOn,d.fname,d.lname,a.semiStatus", "a.requestLeave_ID=$requestLeave_ID AND a.uid=b.uid AND b.emp_id = c.emp_id AND c.apid=d.apid", "tbl_request_leave_approval a,tbl_user b,tbl_employee c,tbl_applicant d", "approvalLevel ASC");
        foreach ($request_leave_approval as $rla)
        {
            $rla->details = $this->general_model->fetch_specific_vals("c.leaveType,a.date,a.minutes,a.isPaid,a.isActive,a.isRetracted,a.isReleased,a.createdOn,b.description,d.status", "requestLeave_ID = $requestLeave_ID AND d.status=b.status_ID AND a.leaveType_ID = c.leaveType_ID AND a.requestLeaveDetails_ID=d.requestLeaveDetails_ID AND d.requestLeaveApproval_ID=$rla->requestLeaveApproval_ID", " tbl_request_leave_details a, tbl_status b,tbl_leave_type c, tbl_request_leave_approval_details d", " date ASC");
        }
        $status = (empty($request_leave_approval) || $request_leave === NULL) ? "Failed" : "Success";
        echo json_encode(array('status' => $status, 'request_leave' => $request_leave, 'request_leave_approval' => $request_leave_approval));
    }

    public function check_exist()
    {
        $name = $this->input->post('name');
        $value = $this->input->post($name);
        $table = $this->input->post('table');
        $isUpdate = $this->input->post('isUpdate');
        $count = intval($this->general_model->fetch_specific_val("COUNT(*) as count", "$name='$value'", $table)->count);


        if ($isUpdate === 'false')
        {
            if ($count > 0)
            {
                echo 'false';
            }
            else
            {
                echo 'true';
            }
        }
        else
        {
            $data_update = json_decode($isUpdate);
            if ($count === 1)
            {
                $count2 = $this->general_model->fetch_specific_val("COUNT(*) as count", $data_update->name . "='" . $data_update->id . "' AND $name='$value'", $table)->count;
//                echo $this->db->last_query();
                if ($count2 > 0)
                {
                    echo 'true';
                }
                else
                {
                    echo 'false';
                }
            }
            else
            {
                echo "true";
            }
        }
    }

    public function get_available_retraction_dates()
    {
        $preDays = 3; //days before the actual leave dates 
        $date = strtotime("+" . $preDays . " day");
        $basis_date = date('Y-m-d', $date);
        $uid = $this->session->userdata('uid');
        $available_leavedates = $this->general_model->fetch_specific_vals("a.requestLeaveDetails_ID, b.requestLeave_ID, a.date,c.allowRetraction,c.leave_name,c.leave_id", "a.date > date('$basis_date') AND a.requestLeave_ID=b.requestLeave_ID AND b.uid=$uid AND a.leave_id=c.leave_id AND c.allowRetraction IN ('single','group') AND a.isReleased=0", "tbl_request_leave_details a,tbl_request_leave b,tbl_leave c", "a.date ASC");

        //REMOVE ALREADY HAVE AT LEAST 1 APPROVAL DECISION
        $new_rl_ids = array();
        foreach ($available_leavedates as $avail_dates)
        {
            $new_rl_ids[] = $avail_dates->requestLeave_ID;
        }
        $new_rl_ids = array_unique($new_rl_ids);
        $remove_requestLeave_IDs = array();
        foreach ($new_rl_ids as $requestLeave_ID)
        {
            $count = $this->general_model->fetch_specific_val("COUNT(*) as count", " a.requestLeave_ID = $requestLeave_ID AND a.approvalLevel = 1 AND a.semiStatus IN (5) AND a.requestLeave_ID=b.requestLeave_ID AND b.finalStatus IN (2,5)", " tbl_request_leave_approval a,tbl_request_leave b")->count;
            if ($count === '0')//atleast 1 approval
            {
                $remove_requestLeave_IDs[] = $requestLeave_ID;
            }
        }
        foreach ($available_leavedates as $key => $avail_dates)
        {
            foreach ($remove_requestLeave_IDs as $requestLeave_ID)
            {
                if ($avail_dates->requestLeave_ID === $requestLeave_ID)
                {
                    unset($available_leavedates[$key]);
                }
            }
        }
        //END OF REMOVE ALREADY HAVE AT LEAST 1 APPROVAL DECISION
        //changes - start

        $leave_names = $this->general_model->fetch_specific_vals("leave_id", "allowRetraction='group'", "tbl_leave");
        $leave_idS = array();
        for ($i = 0; $i < COUNT($leave_names); $i++)
        {
            $leave_idS[] = $leave_names[$i]->leave_id;
        }
        $requestLeave_IDS = array();
        foreach ($available_leavedates as $key => $avail_dates)
        {
            if (in_array($avail_dates->leave_id, $leave_idS))
            {
                if (!in_array($avail_dates->requestLeave_ID, $requestLeave_IDS))
                {
                    $requestLeave_IDS[] = $avail_dates->requestLeave_ID;
                }
            }
        }
        foreach ($requestLeave_IDS as $rl_id)
        {
            $result = $this->general_model->fetch_specific_vals("requestLeaveDetails_ID", "requestLeave_ID=$rl_id AND date <= date('$basis_date')", "tbl_request_leave_details");
            if (!empty($result))
            {
                foreach ($available_leavedates as $key => $avail_dates)
                {
                    if ($avail_dates->requestLeave_ID === $rl_id)
                    {
                        unset($available_leavedates[$key]);
                    }
                }
            }
        }
        //changes - end
        //
        //START OF REMOVE ALREADY RETRACTED DATES------------------
        $requestLeaveDetails_IDS = array();
        foreach ($available_leavedates as $avail_ld)
        {
            $requestLeaveDetails_IDS[] = $avail_ld->requestLeaveDetails_ID;
        }
        $imploded = "'" . implode("','", $requestLeaveDetails_IDS) . "'";
        $retracted_leaves = $this->general_model->fetch_specific_vals("requestLeaveDetails_ID", "requestLeaveDetails_ID IN ($imploded)", "tbl_retract_leave_details");

        if (!empty($retracted_leaves))
        {

            foreach ($available_leavedates as $key => $avail_dates)
            {
                foreach ($retracted_leaves as $ret_leave)
                {
                    if ($avail_dates->requestLeaveDetails_ID === $ret_leave->requestLeaveDetails_ID)
                    {
                        unset($available_leavedates[$key]);
                    }
                }
            }
        }
        //END OF REMOVE ALREADY RETRACTED DATES------------------
        $status = (empty($available_leavedates)) ? 'Empty' : 'Success';
        echo json_encode(array('status' => $status, 'available_leavedates' => $available_leavedates));
    }

    public function submit_file_retraction()
    {
        $uid = $this->session->userdata('uid');
        $data = $this->input->post('data');
        $reason = $this->input->post('reason');

        //---
        $notif_details = NULL;
        $arr = array(
            'uid' => $uid,
            'reason' => $reason,
            'finalStatus' => 2 //pending
        );
        $retractLeave_ID = $this->general_model->insert_vals_last_inserted_id($arr, "tbl_retract_leave");
        if ($retractLeave_ID === 0)
        {
            $status = "Failed";
        }
        else
        {
            $arr2 = array();
            $arrToRequestDetails = array();
            foreach ($data as $d)
            {
                $temp_array = array(
                    'retractLeave_ID' => $retractLeave_ID,
                    'overAllStatus' => 2, //pending,
                    'requestLeaveDetails_ID' => $d['requestLeaveDetails_ID']
                );
                array_push($arr2, $temp_array);
                $arrToRequestDetails[] = array(
                    'data' => array('isRetracted' => 1),
                    'where' => "requestLeaveDetails_ID=" . $d['requestLeaveDetails_ID']
                );
            }
            $result = $this->general_model->batch_insert($arr2, "tbl_retract_leave_details");
            $result2 = $this->general_model->update_array_vals($arrToRequestDetails, "tbl_request_leave_details"); //$arr2
            $status = ($result && $result2) ? "Success" : "Failed";
            //approval codes -------------------------------------------------------------------------

            $retract_leave_details = $this->general_model->fetch_specific_vals("retractLeaveDetails_ID", "retractLeave_ID=$retractLeave_ID", "tbl_retract_leave_details");
            $createdOn = $this->general_model->fetch_specific_val("createdOn", "retractLeave_ID=$retractLeave_ID", "tbl_retract_leave")->createdOn;
            $approvers = $this->set_approvers($createdOn, $uid, 4, 'retractLeave_ID', $retractLeave_ID);
            $formatted_approvers = array();
            foreach ($approvers as $raw)
            {
                $formatted_approvers[] = array(
                    'retractLeave_ID' => $raw['retractLeave_ID'],
                    'uid' => $raw['userId'],
                    'approvalLevel' => $raw['approvalLevel'],
                    'semiStatus' => $raw['approvalStatus_ID']
                );
            }
            foreach ($formatted_approvers as $approver)
            {
                $data = array();
                $retractLeaveApproval_ID = $this->general_model->insert_vals_last_inserted_id($approver, "tbl_retract_leave_approval");
                foreach ($retract_leave_details as $rld)
                {
                    $data[] = array(
                        'retractLeaveApproval_ID' => $retractLeaveApproval_ID,
                        'retractLeaveDetails_ID' => $rld->retractLeaveDetails_ID,
                        'status' => 2
                    );
                }
                $res = $this->general_model->batch_insert($data, "tbl_retract_leave_approval_details");
                $status = ($res) ? "Success" : "Failed";
            }
            // SET NOTIFICATION
            $notif_mssg = "has sent you a leave retraction request.  <br><small><b>LRR-ID</b>: " . str_pad($retractLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "Leave/approval";
            $notif_details = $this->set_notif($uid, $notif_mssg, $link, $approvers);
        }
        echo json_encode(array('status' => $status, 'notif_details' => $notif_details));
    }

    public function get_other_retraction_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');
        $searchcustom = $this->input->post('searchcustom');
        $uid = $this->session->userdata('uid');
        $search = array();
        $searchString = "";
        if ($searchcustom !== '')
        {
            $search[] = "reason LIKE '%$searchcustom%'";
        }
        if (!empty($search))
        {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT retractLeave_ID,reason,createdOn FROM tbl_retract_leave WHERE uid=$uid AND finalStatus NOT IN (2,12) AND createdOn between date('$datestart') AND date('$dateend') $searchString  ORDER BY createdOn DESC";

        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");

        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    public function get_ongoing_retraction_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $uid = $this->session->userdata('uid');
        $query = "SELECT retractLeave_ID,reason,createdOn FROM tbl_retract_leave WHERE uid=$uid AND finalStatus IN (2,12) ORDER BY createdOn DESC";
        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    public function get_retraction_details()
    {
        $retractLeave_ID = $this->input->post('retractLeave_ID');
        $data = $this->general_model->fetch_specific_vals("a.requestLeaveDetails_ID,a.retractLeaveDetails_ID,a.overAllStatus,b.date,b.minutes,b.isPaid,b.isRetracted,c.leaveType,d.description as status", "a.retractLeave_ID = $retractLeave_ID AND a.requestLeaveDetails_ID=b.requestLeaveDetails_ID AND b.leaveType_ID=c.leaveType_ID AND a.overAllStatus=d.status_ID", " tbl_retract_leave_details a,tbl_request_leave_details b,tbl_leave_type c,tbl_status d", " b.date ASC");
        echo json_encode($data);
    }

    public function get_retraction_approval_flow()
    {
        $retractLeave_ID = $this->input->post('retractLeave_ID');
        $retract_leave = $this->general_model->fetch_specific_val("a.uid, a.reason, a.finalStatus, a.createdOn, b.emp_id, d.fname, d.lname,d.pic", "a.retractLeave_ID=$retractLeave_ID AND a.uid=b.uid AND b.emp_id=c.emp_id AND c.apid=d.apid", "tbl_retract_leave a,tbl_user b, tbl_employee c,tbl_applicant d");
        $job = $this->general_model->fetch_specific_val("c.pos_details,d.status", "a.emp_id = " . $retract_leave->emp_id . " AND a.isActive=1 AND a.ishistory=1 AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d");
        $retract_leave->job = $job->pos_details;
        $retract_leave->jobstat = $job->status;
        $retract_leave_approval = $this->general_model->fetch_specific_vals("a.retractLeaveApproval_ID,a.approvalLevel,a.comment,a.decisionOn,d.fname,d.lname,a.semiStatus", "a.retractLeave_ID=$retractLeave_ID AND a.uid=b.uid AND b.emp_id=c.emp_id AND c.apid=d.apid", "tbl_retract_leave_approval a,tbl_user b,tbl_employee c,tbl_applicant d", "approvalLevel ASC");
        foreach ($retract_leave_approval as $rla)
        {
            $rla->details = $this->general_model->fetch_specific_vals("c.leaveType,e.date,e.minutes,e.isPaid,e.isActive,e.isRetracted,e.isReleased,a.createdOn,b.description,d.status", "retractLeave_ID = $retractLeave_ID AND d.status=b.status_ID AND e.leaveType_ID = c.leaveType_ID AND a.requestLeaveDetails_ID=e.requestLeaveDetails_ID AND a.retractLeaveDetails_ID=d.retractLeaveDetails_ID AND d.retractLeaveApproval_ID=$rla->retractLeaveApproval_ID", " tbl_retract_leave_details a, tbl_status b,tbl_leave_type c, tbl_retract_leave_approval_details d, tbl_request_leave_details e", " date ASC");
        }
        $status = (empty($retract_leave_approval) || $retractLeave_ID === NULL) ? "Failed" : "Success";
        echo json_encode(array('status' => $status, 'retract_leave' => $retract_leave, 'retract_leave_approval' => $retract_leave_approval));
    }

    //START ________________________________________________
    public function get_ongoing_approval_retraction_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $uid = $this->session->userdata('uid');
        $query = "SELECT a.retractLeave_ID,a.reason,a.createdOn,e.fname,e.lname FROM tbl_retract_leave a, tbl_retract_leave_approval b ,tbl_user c,tbl_employee d,tbl_applicant e WHERE c.emp_id=d.emp_id AND d.apid=e.apid AND b.uid=$uid AND b.semiStatus = 4 AND a.retractLeave_ID=b.retractLeave_ID AND a.uid=c.uid ORDER BY a.createdOn DESC";
        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    public function get_other_approval_retraction_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');
        $searchcustom = $this->input->post('searchcustom');
        $uid = $this->session->userdata('uid');
        $search = array();
        $searchString = "";
        if ($searchcustom !== '')
        {
            $search[] = "reason LIKE '%$searchcustom%'";
        }
        if (!empty($search))
        {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT a.retractLeave_ID,a.reason,a.createdOn,e.fname,e.lname FROM tbl_retract_leave a, tbl_retract_leave_approval b ,tbl_user c,tbl_employee d,tbl_applicant e WHERE c.emp_id =d.emp_id AND d.apid=e.apid AND b.uid=$uid AND b.semiStatus NOT IN (4,2,7) AND a.retractLeave_ID=b.retractLeave_ID AND a.uid=c.uid AND a.createdOn between date('$datestart') AND date('$dateend') $searchString  ORDER BY a.createdOn DESC";

        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");

        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    //END ________________________________________________

    public function submit_approval_decision_for_retraction()
    {
        $uid = $this->session->userdata('uid');
        $retractLeave_ID = $this->input->post('retractLeave_ID');
        $comment = $this->input->post('comment');
        $semiStatus = $this->input->post('semiStatus');
        $data = $this->input->post('data');
        $from = $this->input->post('from');
        if ($from === 'missed')
        {
            $module = $this->general_model->fetch_specific_val("numberOfApproval", "module_ID=4", "tbl_module");
            $num = (int) $module->numberOfApproval + 1;
            $arrayx = array('retractLeave_ID' => $retractLeave_ID, 'uid' => $uid, 'approvalLevel' => $num, 'semiStatus' => 4, 'decisionOn' => date("Y-m-d H:i:s"), 'comment' => $comment);
            $retract_leave_details = $this->general_model->fetch_specific_vals("retractLeaveDetails_ID", "retractLeave_ID=$retractLeave_ID", "tbl_retract_leave_details");
            $retractLeaveApproval_ID = $this->general_model->insert_vals_last_inserted_id($arrayx, "tbl_retract_leave_approval");
            foreach ($retract_leave_details as $rld)
            {
                $datax[] = array(
                    'retractLeaveApproval_ID' => $retractLeaveApproval_ID,
                    'retractLeaveDetails_ID' => $rld->retractLeaveDetails_ID,
                    'status' => 2
                );
            }
            $this->general_model->batch_insert($datax, "tbl_retract_leave_approval_details");
        }
        $retractLeaveApproval = $this->general_model->fetch_specific_val("retractLeaveApproval_ID,approvalLevel", "retractLeave_ID=$retractLeave_ID AND uid=$uid", "tbl_retract_leave_approval");
        $retractLeave = $this->general_model->fetch_specific_val("a.uid,a.reason,a.finalStatus,a.createdOn,a.updatedOn,d.fname,d.lname", "a.retractLeave_ID=$retractLeave_ID AND a.uid=b.uid AND b.emp_id = c.emp_id AND c.apid=d.apid", "tbl_retract_leave a,tbl_user b,tbl_employee c,tbl_applicant d");
        $notif_details = array();
        $arr = array();
        foreach ($data as $d)
        {
            $arr[] = array(
                'data' => array('status' => ($d['stat'] === 'true') ? 5 : 6),
                'where' => "retractLeaveApproval_ID=" . $retractLeaveApproval->retractLeaveApproval_ID . " AND retractLeaveDetails_ID=" . $d['retractLeaveDetails_ID']
            );
        }
        $rlad_stat = $this->general_model->update_array_vals($arr, "tbl_retract_leave_approval_details");
        if ($rlad_stat)
        {
            $rla_stat = $this->general_model->update_vals(array('semiStatus' => intval($semiStatus), 'decisionOn' => date("Y-m-d H:i:s")), "retractLeaveApproval_ID=" . $retractLeaveApproval->retractLeaveApproval_ID, "tbl_retract_leave_approval");
            if ($rla_stat)
            {
                if (intval($semiStatus) === 6)
                {
                    if ($from !== 'missed')
                    {
                        //COMPLETE DEADLINE-------------------------------------------------------------------------------------------------------

                        $this->general_model->update_vals(array('status_ID' => 3), "userId=$uid AND request_ID=$retractLeave_ID AND module_ID=4", "tbl_deadline");
                        $deadline = $this->general_model->fetch_specific_val("approvalLevel", "userId=$uid AND request_ID=$retractLeave_ID AND module_ID=4", "tbl_deadline");
                        $this->general_model->update_vals(array('status_ID' => 7), "approvalLevel>" . $deadline->approvalLevel . " AND request_ID=$retractLeave_ID AND module_ID=4", "tbl_deadline");

                        //COMPLETE DEADLINE-------------------------------------------------------------------------------------------------------
                    }
                    $approvalLevel = intval($retractLeaveApproval->approvalLevel); //7==cancelled
                    $upper_approvers = $this->general_model->fetch_specific_vals("retractLeaveApproval_ID", "retractLeave_ID=$retractLeave_ID AND approvalLevel>$approvalLevel", "tbl_retract_leave_approval");

                    $array_reject = array();
                    foreach ($upper_approvers as $uppers)
                    {
                        $array_reject[] = array(
                            'data' => array('status' => 7),
                            'where' => "retractLeaveApproval_ID=" . $uppers->retractLeaveApproval_ID
                        );
                    }
                    $rej_rlad = $this->general_model->update_array_vals($array_reject, "tbl_retract_leave_approval_details");
                    $rej_rla = $this->general_model->update_vals(array('semiStatus' => 7, 'decisionOn' => date("Y-m-d H:i:s")), "retractLeave_ID=$retractLeave_ID AND approvalLevel>$approvalLevel", "tbl_retract_leave_approval");
                    $rej_rl = $this->general_model->update_vals(array('finalStatus' => intval($semiStatus)), "retractLeave_ID=$retractLeave_ID", "tbl_retract_leave");
                    $rej_rld = $this->general_model->update_vals(array('overAllStatus' => intval($semiStatus)), "retractLeave_ID=$retractLeave_ID", "tbl_retract_leave_details");

                    $status = ($rej_rl && $rej_rla && $rej_rlad && $rej_rld) ? "Success" : "Failed";
                    // SET NOTIFICATION FOR REQUESTOR
                    $notif_mssg = "has rejected your leave retraction request.  <br><small><b>LRR-ID</b>: " . str_pad($retractLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                    $link = "Leave/retraction";
                    $notif_details[] = $this->set_notif($uid, $notif_mssg, $link, array(array('userId' => $retractLeave->uid)));
                }
                else
                {
                    if ($from !== 'missed')
                    {
                        //COMPLETE DEADLINE-------------------------------------------------------------------------------------------------------
                        $this->general_model->update_vals(array('status_ID' => 3), "userId=$uid AND request_ID=$retractLeave_ID AND module_ID=4", "tbl_deadline");
                        //COMPLETE DEADLINE-------------------------------------------------------------------------------------------------------
                    }
                    $approvalLevel = intval($retractLeaveApproval->approvalLevel) + 1;
                  
                    $nextRLA = $this->general_model->fetch_specific_val("retractLeaveApproval_ID,approvalLevel", "retractLeave_ID=$retractLeave_ID AND approvalLevel=$approvalLevel", "tbl_retract_leave_approval");
                    if (empty($nextRLA) || $nextRLA === NULL)
                    {
                        //FINAL APPROVER ALREADY
                        $last_stat = $this->general_model->update_vals(array('finalStatus' => $semiStatus), "retractLeave_ID=$retractLeave_ID", "tbl_retract_leave");
                        //update the requestleavedetails final decision

                        $arr_rejected = array();
                        $arr_approved = array();
                        $retractleavedetailsid = array();
                        foreach ($data as $d)
                        {
                            if ($d['stat'] !== 'true')
                            {
                                $arr_rejected[] = array(
                                    'data' => array('overAllStatus' => 6),
                                    'where' => "retractLeaveDetails_ID=" . $d['retractLeaveDetails_ID']
                                );
                            }
                            else
                            {
                                $arr_approved[] = array(
                                    'data' => array('overAllStatus' => 5),
                                    'where' => "retractLeaveDetails_ID=" . $d['retractLeaveDetails_ID']
                                );
                                //xxx
                                $leavexx = $this->general_model->fetch_specific_val("requestLeave_ID,requestLeaveDetails_ID,minutes,isPaid,leaveType_ID,leave_ID,isBackup", "requestLeaveDetails_ID=" . $d['requestLeaveDetails_ID'], "tbl_request_leave_details");
                                //CHECK FIRST FOR UNPAID CHURVA
                                $this->deleteTransferStat($leavexx);
                                //CHECK FIRST FOR UNPAID CHURVA
                                $retractleavedetailsid[] = $d['retractLeaveDetails_ID'];
                                $arr_approved_request_details[] = "UPDATE tbl_request_leave_details a,tbl_retract_leave_details b SET a.overAllStatus=13,a.isRetracted=1,a.isActive=0 WHERE a.requestLeaveDetails_ID=b.requestLeaveDetails_ID AND b.retractLeaveDetails_ID=" . $d['retractLeaveDetails_ID'];
                            }
                        }

                        $rld_stat_app = $this->general_model->update_array_vals($arr_rejected, "tbl_retract_leave_details");
                        $rld_stat_rej = $this->general_model->update_array_vals($arr_approved, "tbl_retract_leave_details");

                        //UPDATE THE TBL_REQUEST_LEAVE_DETAILS FOR FINAL DECISION HERE

                        $rld_stat_request = $this->general_model->custom_query_no_return_array($arr_approved_request_details);


                        //check if all dates of request leave has been retracted then change status to retracted
                        $requestleaveids = $this->general_model->custom_query("SELECT a.requestLeave_ID FROM tbl_request_leave a,tbl_request_leave_details b,tbl_retract_leave_details c WHERE b.requestLeaveDetails_ID=c.requestLeaveDetails_ID AND a.requestLeave_ID=b.requestLeave_ID AND c.retractLeaveDetails_ID IN (" . implode(',', $retractleavedetailsid) . ") GROUP BY a.requestLeave_ID");

                        foreach ($requestleaveids as $id)
                        {
                            $count_withstatus = $this->general_model->fetch_specific_val("COUNT(*) as count", "overAllStatus=13 AND requestLeave_ID=" . $id->requestLeave_ID, "tbl_request_leave_details")->count;
                            $count_nostatus = $this->general_model->fetch_specific_val("COUNT(*) as count", "requestLeave_ID=" . $id->requestLeave_ID, "tbl_request_leave_details")->count;
                            if ($count_withstatus === $count_nostatus)
                            {
                                $rex = $this->general_model->update_vals(array('finalStatus' => 13), "requestLeave_ID=" . $id->requestLeave_ID, "tbl_request_leave");
                            }
                        }

                        //--------------------------------------------------------------
                        $status = ($last_stat && $rld_stat_app && $rld_stat_rej && $rld_stat_request) ? "Success" : "Failed";
                        // if (strtotime(date('YYYY-MM-DD')) >= strtotime('2019-01-01'))
                        // {
                        foreach ($retractleavedetailsid as $id)
                        {
                            $scheduleid = $this->general_model->fetch_specific_val("b.retract_sched_id", "a.requestLeaveDetails_ID=b.requestLeaveDetails_ID AND a.retractLeaveDetails_ID = $id", 'tbl_retract_leave_details a,tbl_request_leave_details b');
                            if (isset($scheduleid->retract_sched_id) && $scheduleid->retract_sched_id != NULL && $scheduleid->retract_sched_id != 0)
                            {
                                $this->general_model->delete_vals('schedtype_id IN (3,8) AND sched_id=' . $scheduleid->retract_sched_id, 'tbl_schedule');
                            }
                        }
                        // }
                        //system_notify user
                        // SET NOTIFICATION FOR REQUESTOR
                        $notif_mssg = "has approved your leave retraction request.   <br><small><b>LR-ID</b>: " . str_pad($retractLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                        $link = "Leave/retraction";
                        $notif_details[] = $this->set_notif($uid, $notif_mssg, $link, array(array('userId' => $retractLeave->uid)));
                    }
                    else
                    {
                        //HAS NEXT APPROVER
                    	
                        $last_stat = $this->general_model->update_vals(array('semiStatus' => 4, 'decisionOn' => date("Y-m-d H:i:s")), "retractLeaveApproval_ID=" . $nextRLA->retractLeaveApproval_ID, "tbl_retract_leave_approval");
                       
                        $arr_rejected_approval = array();
                        $arr_rejected = array();
                        $approvalLevel = intval($retractLeaveApproval->approvalLevel); //7==cancelled
                        $upper_approvers = $this->general_model->fetch_specific_vals("retractLeaveApproval_ID,uid", "retractLeave_ID=$retractLeave_ID AND approvalLevel>$approvalLevel", "tbl_retract_leave_approval");

                        foreach ($data as $d)
                        {
                            if ($d['stat'] !== 'true')
                            {
                                $arr_rejected[] = array(
                                    'data' => array('overAllStatus' => 6),
                                    'where' => "retractLeaveDetails_ID=" . $d['retractLeaveDetails_ID']
                                );
                                foreach ($upper_approvers as $uppers)
                                {
                                    $arr_rejected_approval[] = array(
                                        'data' => array('status' => 7),
                                        'where' => "retractLeaveDetails_ID=" . $d['retractLeaveDetails_ID'] . " AND retractLeaveApproval_ID=" . $uppers->retractLeaveApproval_ID
                                    );
                                }
                            }
                        }

                        $rld_stat_app = $this->general_model->update_array_vals($arr_rejected, "tbl_retract_leave_details");
                        $rld_stat_approval = $this->general_model->update_array_vals($arr_rejected_approval, "tbl_retract_leave_approval_details");
                        $status = ($last_stat && $rld_stat_app && $rld_stat_approval) ? "Success" : "Failed";
                        //notify next approver and user
                        // SET NOTIFICATION FOR REQUESTOR
                        $notif_mssg = "has approved your leave retraction request and forwarded it to the next approver.   <br><small><b>LRR-ID</b>: " . str_pad($retractLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                        $link = "Leave/retraction";
                        $notif_details[] = $this->set_notif($uid, $notif_mssg, $link, array(array('userId' => $retractLeave->uid)));
                        // SET NOTIFICATION FOR NEXT APPROVER
                        $notif_mssg = "forwarded you the leave retraction request of " . $retractLeave->lname . ", " . $retractLeave->fname . ".   <br><small><b>LRR-ID</b>: " . str_pad($retractLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                        $link = "Leave/approval";
                        
                        $notif_details[] = $this->set_notif($uid, $notif_mssg, $link, array(array('userId' => $upper_approvers[0]->uid)));
                    }
                }
            }
            else
            {
                $status = "Failed";
            }
        }
        else
        {
            $status = "Failed";
        }
        echo json_encode(array('status' => $status, 'notif_details' => $notif_details));
    }

    public function delete_retraction_detail()
    {
        $retractLeaveDetails_ID = $this->input->post('retractLeaveDetails_ID');
        $name = $this->general_model->fetch_specific_val("a.fname,a.lname", "a.apid=b.apid AND b.emp_id=" . $this->session->userdata('emp_id'), "tbl_applicant a,tbl_employee b");
        $leave = $this->general_model->fetch_specific_val("retractLeave_ID", "retractLeaveDetails_ID=$retractLeaveDetails_ID", "tbl_retract_leave_details");
        $notif_details = array();
        if ($leave === NULL)
        {
            $status = "Failed";
        }
        else
        {
            $res0 = $this->general_model->delete_vals("retractLeaveDetails_ID=$retractLeaveDetails_ID", "tbl_retract_leave_approval_details");
            $res = $this->general_model->delete_vals("retractLeaveDetails_ID=$retractLeaveDetails_ID", "tbl_retract_leave_details");
            if ($res && $res0)
            {
                $count = $this->general_model->fetch_specific_val("COUNT(retractLeaveDetails_ID) as count", "retractLeave_ID=" . $leave->retractLeave_ID, "tbl_retract_leave_details")->count;

                if ($count === '0')
                {
                    $approver_1 = $this->general_model->fetch_specific_val("uid", "approvalLevel=1 AND retractLeave_ID=" . $leave->retractLeave_ID, "tbl_retract_leave_approval");
                    $notif_mssg = $name->fname . " " . $name->lname . " has deleted the filed leave retraction.  <br><small><b>LRR-ID</b>: " . str_pad($leave->retractLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                    $link = "Leave/approval";
                    $notif_details[] = $this->set_system_notif($notif_mssg, $link, array(array('userId' => $approver_1->uid)));
                    //delete request
                    $res2a = $this->general_model->delete_vals("retractLeave_ID=" . $leave->retractLeave_ID, "tbl_retract_leave_approval");
                    $res2b = $this->general_model->delete_vals("retractLeave_ID=" . $leave->retractLeave_ID, "tbl_retract_leave");
                    $res2c = $this->general_model->delete_vals("request_ID=" . $leave->retractLeave_ID . " AND module_ID=4", "tbl_deadline");
                    $status = ($res2a && $res2b) ? "All" : "Failed";
                }
                else
                {
                    $approver_1 = $this->general_model->fetch_specific_val("uid", "approvalLevel=1 AND retractLeave_ID=" . $leave->retractLeave_ID, "tbl_retract_leave_approval");
                    $notif_mssg = $name->fname . " " . $name->lname . " has deleted a leave date from the filed leave retraction.  <br><small><b>LRR-ID</b>: " . str_pad($leave->retractLeave_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                    $link = "Leave/approval";
                    $notif_details[] = $this->set_system_notif($notif_mssg, $link, array(array('userId' => $approver_1->uid)));
                    $status = "Success";
                }
            }
            else
            {
                $status = "Failed";
            }
        }
        echo json_encode(array('status' => $status, 'notif_details' => $notif_details));
    }

    //HR MONITORING PAGE _____________________________________________________________________________________________________________________________________
    //HR MONITORING PAGE _____________________________________________________________________________________________________________________________________
    //HR MONITORING PAGE _____________________________________________________________________________________________________________________________________
    //HR MONITORING PAGE _____________________________________________________________________________________________________________________________________
    //HR MONITORING PAGE _____________________________________________________________________________________________________________________________________

    public function get_all_request_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');
        $searchcustom = $this->input->post('searchcustom');
        $searchstatus = $this->input->post('searchstatus');
        $search = array();
        $searchString = "";
        if ($searchcustom !== '')
        {
            $search[] = "(reason LIKE '%$searchcustom%' OR d.fname LIKE '%$searchcustom%' OR d.lname LIKE '%$searchcustom%')";
        }
        if ($searchstatus !== '')
        {
            if ($searchstatus === 'ongoing')
            {
                $search[] = "finalStatus IN (2,12)";
            }
            else if ($searchstatus === 'history')
            {
                $search[] = "finalStatus NOT IN (2,12)";
            }
//            else if ($searchstatus === 'retracted')
//            {
//                $search[] = "finalStatus = 13";
//            }
        }
        if (!empty($search))
        {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT a.requestLeave_ID,a.reason,a.createdOn,d.fname,d.lname FROM tbl_request_leave a,tbl_user b,tbl_employee c,tbl_applicant d WHERE b.emp_id=c.emp_id AND c.apid=d.apid AND a.uid=b.uid AND finalStatus NOT IN (12,13) AND createdOn between date('$datestart') AND date('$dateend') $searchString  ORDER BY createdOn DESC";

        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");

        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    public function get_all_leavedates_list($uid = NULL)
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');
        $searchpayment = $this->input->post('searchpayment');
        $searchreleased = $this->input->post('searchreleased');
        $searchminutes = $this->input->post('searchminutes');
        $searchstatus = $this->input->post('searchstatus');
        $search = array();
        $searchString = "";
        if ($searchpayment !== '')
        {
            $search[] = "c.isPaid = $searchpayment";
        }
        if ($searchreleased !== '')
        {
            $search[] = "c.isReleased = $searchreleased";
        }
        if ($searchminutes !== '')
        {
            $search[] = "c.minutes = $searchminutes";
        }
        if ($searchstatus !== '')
        {
            $search[] = "e.description = '$searchstatus'";
        }
        if ($uid !== NULL)
        {
            $search[] = "c.uid = $uid";
        }
        if (!empty($search))
        {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT a.requestLeave_ID,c.requestLeaveDetails_ID,c.date,c.minutes,c.isPaid,c.isReleased,c.overAllStatus,e.description,d.leaveType,g.fname,g.lname FROM tbl_request_leave a,tbl_user b,tbl_request_leave_details c,tbl_leave_type d,tbl_status e,tbl_employee f,tbl_applicant g WHERE b.emp_id = f.emp_id AND f.apid=g.apid AND e.status_ID=c.overAllStatus  AND c.leaveType_ID=d.leaveType_ID AND a.requestLeave_ID=c.requestLeave_ID AND a.uid=b.uid AND c.overAllStatus <> 12 AND c.date between date('$datestart') AND date('$dateend') $searchString  ORDER BY date DESC";

        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");

        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    public function get_date_approval_flow()
    {
        $requestLeave_ID = $this->input->post('requestLeave_ID');
        $requestLeaveDetails_ID = $this->input->post('requestLeaveDetails_ID');
        $request_leave = $this->general_model->fetch_specific_val("a.uid, a.reason, a.finalStatus, a.createdOn, b.emp_id, d.fname, d.lname,d.pic,e.date,f.leaveType,e.isReleased,e.minutes,e.isPaid", "a.requestLeave_ID=$requestLeave_ID AND a.uid=b.uid AND b.emp_id=c.emp_id AND c.apid=d.apid AND a.requestLeave_ID=e.requestLeave_ID AND e.leaveType_ID=f.leaveType_ID AND e.requestLeaveDetails_ID = $requestLeaveDetails_ID", "tbl_request_leave a,tbl_user b, tbl_employee c,tbl_applicant d,tbl_request_leave_details e,tbl_leave_type f");
        $retract_leave = $this->general_model->fetch_specific_val("c.retractLeave_ID, c.reason, c.finalStatus, c.createdOn", "a.requestLeaveDetails_ID=$requestLeaveDetails_ID AND a.requestLeaveDetails_ID=b.requestLeaveDetails_ID AND b.retractLeave_ID=c.retractLeave_ID", "tbl_request_leave_details a, tbl_retract_leave_details b,tbl_retract_leave c");
        $job = $this->general_model->fetch_specific_val("c.pos_details,d.status", "a.emp_id = " . $request_leave->emp_id . " AND a.isActive=1 AND a.ishistory=1 AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d");
        $request_leave->job = $job->pos_details;
        $request_leave->jobstat = $job->status;
        $request_leave_approval = $this->general_model->fetch_specific_vals("a.requestLeaveApproval_ID,a.approvalLevel,a.comment,a.decisionOn,d.fname,d.lname,a.semiStatus", "a.requestLeave_ID=$requestLeave_ID AND a.uid=b.uid AND b.emp_id =c.emp_id AND c.apid=d.apid", "tbl_request_leave_approval a,tbl_user b,tbl_employee c,tbl_applicant d", "approvalLevel ASC");
        foreach ($request_leave_approval as $rla)
        {
            $rla->details = $this->general_model->fetch_specific_val("c.leaveType,a.date,a.minutes,a.isPaid,a.isActive,a.isRetracted,a.isReleased,a.createdOn,b.description,d.status", "requestLeave_ID = $requestLeave_ID AND a.requestLeaveDetails_ID=$requestLeaveDetails_ID AND d.status=b.status_ID AND a.leaveType_ID = c.leaveType_ID AND a.requestLeaveDetails_ID=d.requestLeaveDetails_ID AND d.requestLeaveApproval_ID=$rla->requestLeaveApproval_ID", " tbl_request_leave_details a, tbl_status b,tbl_leave_type c, tbl_request_leave_approval_details d", " date ASC");
        }
        if ($retract_leave === NULL)
        {
            $retract_leave_approval = NULL;
        }
        else
        {
            $retract_leave_approval = $this->general_model->fetch_specific_vals("a.retractLeaveApproval_ID,a.approvalLevel,a.comment,a.decisionOn,d.fname,d.lname,a.semiStatus", "a.retractLeave_ID=" . $retract_leave->retractLeave_ID . " AND a.uid=b.uid AND b.emp_id = c.emp_id AND c.apid=d.apid", "tbl_retract_leave_approval a,tbl_user b,tbl_employee c,tbl_applicant d", "approvalLevel ASC");
            foreach ($retract_leave_approval as $rla2)
            {
                $rla2->details = $this->general_model->fetch_specific_val("c.leaveType,e.date,e.minutes,e.isPaid,e.isActive,e.isRetracted,e.isReleased,a.createdOn,b.description,d.status", "retractLeave_ID = " . $retract_leave->retractLeave_ID . " AND d.status=b.status_ID AND e.leaveType_ID = c.leaveType_ID AND a.requestLeaveDetails_ID=e.requestLeaveDetails_ID AND a.retractLeaveDetails_ID=d.retractLeaveDetails_ID AND d.retractLeaveApproval_ID=$rla2->retractLeaveApproval_ID", " tbl_retract_leave_details a, tbl_status b,tbl_leave_type c, tbl_retract_leave_approval_details d, tbl_request_leave_details e", " date ASC");
            }
        }

        $status = (empty($request_leave_approval) || $request_leave === NULL) ? "Failed" : "Success";
        echo json_encode(array('status' => $status, 'request_leave' => $request_leave, 'request_leave_approval' => $request_leave_approval, 'retract_leave' => $retract_leave, 'retract_leave_approval' => $retract_leave_approval));
    }

    public function get_missed_approval_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $query = "SELECT a.requestLeave_ID,a.reason,a.createdOn,e.fname,e.lname FROM tbl_request_leave a,tbl_user c,tbl_employee d,tbl_applicant e WHERE c.emp_id = d.emp_id AND d.apid=e.apid AND a.finalStatus = 12 AND a.uid=c.uid ORDER BY a.createdOn DESC";
//        $query = "SELECT a.requestLeave_ID,a.reason,a.createdOn,c.fname,c.lname FROM tbl_request_leave a, tbl_request_leave_approval b ,tbl_user c WHERE b.semiStatus = 13 AND a.requestLeave_ID=b.requestLeave_ID AND a.uid=c.uid ORDER BY a.createdOn DESC";
        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    public function get_missed_approval_retraction_list()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $query = "SELECT a.retractLeave_ID,a.reason,a.createdOn,e.fname,e.lname FROM tbl_retract_leave a ,tbl_user c,tbl_employee d,tbl_applicant e WHERE c.emp_id = d.emp_id AND d.apid=e.apid AND a.finalStatus = 12 AND a.uid=c.uid ORDER BY a.createdOn DESC";
//         $query = "SELECT a.retractLeave_ID,a.reason,a.createdOn,c.fname,c.lname FROM tbl_retract_leave a, tbl_retract_leave_approval b ,tbl_user c WHERE  b.semiStatus = 13 AND a.retractLeave_ID=b.retractLeave_ID AND a.uid=c.uid ORDER BY a.createdOn DESC";
        $data = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $data, 'total' => $count));
    }

    //---------------------------------REPORTS---------------------------------------------------------------------------------------------------
    //---------------------------------REPORTS---------------------------------------------------------------------------------------------------
    //---------------------------------REPORTS---------------------------------------------------------------------------------------------------
    //---------------------------------REPORTS---------------------------------------------------------------------------------------------------

    public function report_status()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access())
        {
        $this->load_template_view('templates/leave/reports/status', $data);
        }
    }

    public function get_report_status_year()
    {
        $startyear = $this->input->post('startyear');
        $endyear = $this->input->post('endyear');
        $years = array();
        for ($x = (int) $endyear; $x >= (int) $startyear; $x--)
        {
            $years[] = $x;
        }
        $statuses = array(5, 6, 13, 2);
//        $statuses = array(5);
        $yeardata = array();
        foreach ($years as $key => $year)
        {
            $yeardata[$key]['year'] = $year;
            $yeardata[$key]['count'] = $this->general_model->fetch_specific_val("COUNT(*) as count", "overAllStatus IN (" . implode(',', $statuses) . ") AND YEAR(date)=$year", "tbl_request_leave_details")->count;
        }
        echo json_encode(array('yeardata' => $yeardata));
    }

    public function get_report_status_month()
    {
        $year = $this->input->post('year');
        $statuses = array(5, 6, 13, 2);
//        $statuses = array(5);
        $monthdata = array();
        for ($monthnum = 1; $monthnum <= 12; $monthnum++)
        {
            $monthdata[$monthnum] = $this->general_model->fetch_specific_val("COUNT(*) as count", "overAllStatus IN (" . implode(',', $statuses) . ") AND YEAR(date)=$year AND MONTH(date)=$monthnum", "tbl_request_leave_details")->count;
        }

        echo json_encode(array('monthdata' => $monthdata));
    }

    public function get_report_status_details()
    {
        $dateArray = $this->input->post('dateArray');
        $statuses = array(2, 5, 6, 13);
        $details = array();
        foreach ($statuses as $status_ID)
        {
            $counts = array();
            foreach ($dateArray as $date)
            {
                $counts[] = $this->general_model->fetch_specific_val("COUNT(*) as count", "overAllStatus = $status_ID AND date='$date'", "tbl_request_leave_details")->count;
            }
            $details[$status_ID] = $counts;
        }
        echo json_encode(array('details' => $details));
    }

    public function get_accounts_per_department()
    {
        $dep_id = $this->input->post('dep_id');
        $accounts = $this->general_model->fetch_specific_vals("acc_id,acc_name", "dep_id=$dep_id", "tbl_account", "acc_name ASC");
        echo json_encode($accounts);
    }

    public function get_report_leave_type_per_date()
    {
        $class = $this->input->post('class');
        $ispaid = $this->input->post('ispaid');
        $released = $this->input->post('released');
        $minutes = $this->input->post('minutes');
        $department = $this->input->post('department');
        $account = $this->input->post('account');
        $dateArray = $this->input->post('dateArray');
        $leaveType = $this->input->post('leaveType');
        $details = array();

        $searchString = $this->search_report_leave_type($class, $ispaid, $released, $minutes, $department, $account);
        foreach ($dateArray as $date)
        {
            $base_query = "SELECT a.minutes FROM tbl_request_leave_details a,tbl_request_leave b,tbl_user c,tbl_emp_promote d,tbl_pos_emp_stat e,tbl_position f,tbl_pos_dep g,tbl_department h,tbl_account i,tbl_employee j,tbl_leave_type k WHERE k.leaveType_ID=a.leaveType_ID AND a.overAllStatus=5  AND b.requestLeave_ID=a.requestLeave_ID AND b.uid=c.uid AND c.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=f.pos_id AND f.pos_id=g.pos_id AND g.dep_id=h.dep_id AND h.dep_id=i.dep_id AND d.isActive=1 AND e.isActive=1 AND j.acc_id=i.acc_id AND c.emp_id=j.emp_id 
    		AND k.leaveType='$leaveType' AND a.date='$date'";
            $resultcount = $this->general_model->custom_query($base_query . $searchString);
            $total = 0;
            foreach ($resultcount as $rc)
            {
                $total += $rc->minutes / 480;
            }
            $details[] = $total;
        }

        echo json_encode(array('details' => $details));
    }

    public function get_report_leave_type_per_employee()
    {
        $class = $this->input->post('class');
        $ispaid = $this->input->post('ispaid');
        $released = $this->input->post('released');
        $minutes = $this->input->post('minutes');
        $department = $this->input->post('department');
        $account = $this->input->post('account');
        $leaveType = $this->input->post('leaveType');
        $date = $this->input->post('date');

        $searchString = $this->search_report_leave_type($class, $ispaid, $released, $minutes, $department, $account);
        $base_query = "SELECT CONCAT(z.lname,', ',z.fname) as fullname,f.pos_details,z.pic, a.minutes FROM tbl_request_leave_details a,tbl_request_leave b,tbl_user c,tbl_emp_promote d,tbl_pos_emp_stat e,tbl_position f,tbl_pos_dep g,tbl_department h,tbl_account i,tbl_employee j ,tbl_leave_type k,tbl_employee x,tbl_applicant z WHERE k.leaveType_ID=a.leaveType_ID AND a.overAllStatus=5  AND b.requestLeave_ID=a.requestLeave_ID AND b.uid=c.uid AND c.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=f.pos_id AND f.pos_id=g.pos_id AND g.dep_id=h.dep_id AND h.dep_id=i.dep_id AND d.isActive=1 AND e.isActive=1 AND j.acc_id=i.acc_id AND c.emp_id=j.emp_id 
    	AND k.leaveType='$leaveType' AND a.date='$date' AND c.emp_id = x.emp_id AND x.apid=z.apid";
        $names = $this->general_model->custom_query($base_query . $searchString);
        echo json_encode(array('names' => $names));
    }

    function search_report_leave_type($class, $ispaid, $released, $minutes, $department, $account)
    {
        $search = array();
        $searchString = "";
        if ($class !== '')
        {
            $search[] = "f.class = '$class'";
        }
        if ($ispaid !== '')
        {
            $search[] = "a.isPaid = $ispaid";
        }
        if ($released !== '')
        {
            $search[] = "a.isReleased = $released";
        }

        if ($minutes !== '')
        {
            $search[] = "a.minutes = $minutes";
        }
        if ($department !== '')
        {
            $search[] = "h.dep_id = $department";
        }
        if ($account !== '')
        {
            $search[] = "i.acc_id = $account";
        }
        if (!empty($search))
        {
            $searchString = "AND " . implode(" AND ", $search);
        }
        return $searchString;
    }

    public function get_report_per_leave_type()
    {
        $dateArray = $this->input->post('dateArray');
        $class = $this->input->post('class');
        $ispaid = $this->input->post('ispaid');
        $released = $this->input->post('released');
        $minutes = $this->input->post('minutes');
        $department = $this->input->post('department');
        $account = $this->input->post('account');
        $leave_types = $this->general_model->fetch_specific_vals("leaveType_ID,leaveType", "isActive=1", "tbl_leave_type", "leaveType ASC");
        $searchString = $this->search_report_leave_type($class, $ispaid, $released, $minutes, $department, $account);
        foreach ($leave_types as $leave_type)
        {
            $counts = array();
            foreach ($dateArray as $dates)
            {
                $base_query = "SELECT a.minutes FROM tbl_request_leave_details a,tbl_request_leave b,tbl_user c,tbl_emp_promote d,tbl_pos_emp_stat e,tbl_position f,tbl_pos_dep g,tbl_department h,tbl_account i,tbl_employee j WHERE a.overAllStatus=5  AND b.requestLeave_ID=a.requestLeave_ID AND b.uid=c.uid AND c.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=f.pos_id AND f.pos_id=g.pos_id AND g.dep_id=h.dep_id AND h.dep_id=i.dep_id AND d.isActive=1 AND e.isActive=1 AND j.acc_id=i.acc_id AND c.emp_id=j.emp_id 
    			AND a.leaveType_ID=" . $leave_type->leaveType_ID . " AND a.date BETWEEN DATE('" . $dates['startDate'] . "') AND DATE('" . $dates['endDate'] . "')";
                $resultcount = $this->general_model->custom_query($base_query . $searchString);
                $total = 0;
                foreach ($resultcount as $rc)
                {
                    $total += $rc->minutes / 480;
                }
                $counts[] = $total;
            }
            $leave_type->counts = $counts;
        }

        echo json_encode(array('leave_types' => $leave_types));
    }

    public function export_report_leave_type($year, $class = NULL, $department = NULL, $account = NULL)
    {


        $this->load->library('PHPExcel', NULL, 'excel');

        //load our new PHPExcel library
        // $this->load->library('excel');
//activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
//name the worksheet
        $this->excel->getActiveSheet()->setTitle('Leave Report');

//-------------------------------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(0);  //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(100); // logo height
        // $objDrawing->setWidth(320); // logo width
        // $objDrawing->setWidthAndHeight(200,400);
        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
//----------------------------------------------------------------------
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(38);
        //PERSONAL CODES--------------------------------------------------------

        $dateArray = array();
        for ($x = 1; $x <= 12; $x++)
        {
            $startDate = date('Y-m-01', strtotime($year . "-" . sprintf("%02d", $x) . "-01"));
            $endDate = date('Y-m-t', strtotime($year . "-" . sprintf("%02d", $x) . "-01"));
            $dateArray[] = array('startDate' => $startDate, 'endDate' => $endDate);
        }
        $categories = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $leave_types = $this->general_model->fetch_specific_vals("leaveType_ID,leaveType", "isActive=1", "tbl_leave_type", "leaveType ASC");
        $search = array();
        $searchString = "";
        if ($class !== '' && $class !== NULL)
        {
            $search[] = "f.class = '$class'";
        }
        if ($department !== '' && $department !== NULL)
        {
            $search[] = "h.dep_id = $department";
        }
        if ($account !== '' && $account !== NULL)
        {
            $search[] = "i.acc_id = $account";
        }
        if (!empty($search))
        {
            $searchString = "AND " . implode(" AND ", $search);
        }
        foreach ($leave_types as $leave_type)
        {
            $counts = array();
            foreach ($dateArray as $dates)
            {
                $base_query = "SELECT COUNT(a.requestLeaveDetails_ID) as count FROM tbl_request_leave_details a,tbl_request_leave b,tbl_user c,tbl_emp_promote d,tbl_pos_emp_stat e,tbl_position f,tbl_pos_dep g,tbl_department h,tbl_account i,tbl_employee j WHERE a.overAllStatus=5  AND b.requestLeave_ID=a.requestLeave_ID AND b.uid=c.uid AND c.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=f.pos_id AND f.pos_id=g.pos_id AND g.dep_id=h.dep_id AND h.dep_id=i.dep_id AND d.isActive=1 AND e.isActive=1 AND j.acc_id=i.acc_id AND c.emp_id=j.emp_id 
        		AND a.leaveType_ID=" . $leave_type->leaveType_ID . " AND a.date BETWEEN DATE('" . $dates['startDate'] . "') AND DATE('" . $dates['endDate'] . "')";
                $resultcount = $this->general_model->custom_query($base_query . $searchString);
                $counts[] = $resultcount[0]->count;
            }
            $leave_type->counts = $counts;
        }
        //END OF CODES----------------------------------------------------------
        $this->excel->getActiveSheet()->setCellValue('B1', "Date Downloaded:");
        $this->excel->getActiveSheet()->setCellValue('B2', DATE('F d, Y h:i a'));
        $this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setWrapText(true);
//set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A3', 'Leave Name');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Count');
//merge cell A1 until D1
        $this->excel->getActiveSheet()->mergeCells('A1:A2');
//set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '000000'))));
        $this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray(array('font' => array('size' => 12, 'bold' => true, 'color' => array('rgb' => 'ffffff'))));

        $this->excel->getActiveSheet()->setAutoFilter('A3:B3');
        $this->excel->getActiveSheet()->getProtection()->setSort(true);

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $x = 4;
        foreach ($categories as $key => $month)
        {
            $this->excel->getActiveSheet()->setCellValue('A' . $x, $month . " " . $year);
            $this->excel->getActiveSheet()->mergeCells('A' . $x . ':B' . $x);

            $this->excel->getActiveSheet()->getStyle('A' . $x)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '555555'))));
            $this->excel->getActiveSheet()->getStyle('A' . $x)->applyFromArray(array('font' => array('size' => 12, 'bold' => true, 'color' => array('rgb' => 'ffffff'))));
            $x++;
            foreach ($leave_types as $leave_type)
            {
                $this->excel->getActiveSheet()->setCellValue('A' . $x, $leave_type->leaveType . " Leave");
                $this->excel->getActiveSheet()->setCellValue('B' . $x, $leave_type->counts[$key]);
                if ($leave_type->counts[$key] !== '0')
                {
                    $this->excel->getActiveSheet()->getStyle('B' . $x)->applyFromArray(array('font' => array('size' => 12, 'bold' => true, 'color' => array('rgb' => '189700'))));
                }
                else
                {
                    $this->excel->getActiveSheet()->getStyle('B' . $x)->applyFromArray(array('font' => array('size' => 12, 'bold' => true, 'color' => array('rgb' => 'ff0000'))));
                }
                $x++;
            }
        }
        $x--;
        $this->excel->getActiveSheet()->getStyle('A1:B' . $x)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
        $filename = 'LeaveReport.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function totalHoursForLeave()
    {
        $sched_id = $this->input->post('sched_id');
        $clock_in = $this->input->post('clock_in');
        $clock_out = $this->input->post('clock_out');
        $time = $this->empshifttime_fetch($sched_id);
        $schedDate = $time[0]->sched_date;
        $shiftIN = date_format(date_create($time[0]->time_start), 'H:i');
        $shiftOUT = date_format(date_create($time[0]->time_end), 'H:i');
        $ampm = explode("|", $this->getTimeGreeting($shiftIN, $shiftOUT));

        $dateOut = ($ampm[1] == "Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($schedDate))) : $schedDate;

        $logoutNew = $dateOut . " " . $shiftOUT;

        $break = $this->empshiftgetrbreak_fetch($time[0]->acc_time_id);
        $totalBreak = $break[0]->hr + ($break[0]->min / 60);

        if ($clock_in !== "" && $clock_out !== "")
        {
            $in = $schedDate . " " . date_format(date_create($clock_in), 'H:i');
            $out = $clock_out;
            $logout = (strtotime($out) >= strtotime($logoutNew)) ? $logoutNew : $out;

            $loginNew = (strtotime($in) <= strtotime($schedDate . " " . $shiftIN)) ? $schedDate . " " . $shiftIN : $in;

            $diff = strtotime($logout) - strtotime($loginNew);
            $totalWorked = (($diff / 60) / 60) - $totalBreak;
        }
        else
        {
            $totalWorked = 0;
            $totalShift = 0;
        }
        $diff_shift = strtotime($logoutNew) - strtotime($schedDate . " " . $shiftIN);
        $totalShift = (($diff_shift / 60) / 60) - $totalBreak;
        $arr["total_actual_worked"] = $totalWorked;
        $arr["total_shift_hours"] = $totalShift;
        $arr["remaining_hour"] = abs($totalShift - $totalWorked);
        echo json_encode($arr);
    }

    //----------------------------------------------MANAGEMENTS REPORTS----------------------------------------------------------------------

    public function report_raw()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
       if ($this->check_access())
       {
        $this->load_template_view('templates/leave/reports/report_raw', $data);
       }
    }

    public function report_summary()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access())
        {
            $this->load_template_view('templates/leave/reports/report_summary', $data);
        }
    }

    public function get_management_report_raw()
    {
//        SELECT b.requestLeave_ID,b.createdOn,e.fname,e.lname,e.mname,f.acc_description,g.leaveType,a.isPaid,a.minutes,b.reason FROM tbl_request_leave_details a,tbl_request_leave b,tbl_user c,tbl_employee d,tbl_applicant e,tbl_account f,tbl_leave_type g WHERE a.requestLeave_ID=b.requestLeave_ID AND a.uid=c.uid AND c.emp_id=d.emp_id AND d.apid=e.apid AND d.acc_id=f.acc_id AND a.leaveType_ID=g.leaveType_ID AND a.overAllStatus = 5
        $datatable = $this->input->post('datatable');

        $query['query'] = "SELECT b.requestLeave_ID,b.createdOn,e.fname,e.lname,e.mname,f.acc_description,g.leaveType,a.isPaid,a.minutes,b.reason FROM tbl_request_leave_details a,tbl_request_leave b,tbl_user c,tbl_employee d,tbl_applicant e,tbl_account f,tbl_leave_type g WHERE a.requestLeave_ID=b.requestLeave_ID AND a.uid=c.uid AND c.emp_id=d.emp_id AND d.apid=e.apid AND d.acc_id=f.acc_id AND a.leaveType_ID=g.leaveType_ID AND a.overAllStatus = 5";

        if (isset($datatable['query']['input-search']) && $datatable['query']['input-search'] !== '')
        {
            $keyword = $datatable['query']['input-search'];

            $query['search']['append'] = " WHERE leaveType LIKE '%" . $keyword . "%' OR  leaveType_ID LIKE '%" . $keyword . "%'";

            $query['search']['total'] = "  WHERE leaveType LIKE '%" . $keyword . "%' OR  leaveType_ID LIKE '%" . $keyword . "%'";
        }

        $data = $this->set_datatable_query($datatable, $query);

        echo json_encode($data);
    }

    public function report_getLeaveTypes()
    {
        $data = $this->general_model->fetch_all("leaveType_ID,leaveType", "tbl_leave_type", "leaveType ASC");
        echo json_encode(array('leaveTypes' => $data));
    }

    public function report_getLeaveRaw()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $datefiled_start = $this->input->post('datefiled_start');
        $datefiled_end = $this->input->post('datefiled_end');
        $leavedates_start = $this->input->post('leavedates_start');
        $leavedates_end = $this->input->post('leavedates_end');
        $searchString = "";
        $query = "SELECT c.requestLeave_ID,a.requestLeaveDetails_ID,a.createdOn,a.uid,b.leaveType,a.date,a.minutes,a.isPaid,a.isActive,a.isReleased,c.reason,a.original_leaveType_ID FROM tbl_request_leave_details a INNER JOIN tbl_leave_type b ON a.leaveType_ID=b.leaveType_ID INNER JOIN tbl_request_leave c ON a.requestLeave_ID=c.requestLeave_ID WHERE a.isActive=1 AND a.overAllStatus=5 AND date(a.createdOn) BETWEEN date('$datefiled_start') AND date('$datefiled_end') AND date(a.date) BETWEEN date('$leavedates_start') AND date('$leavedates_end') $searchString ORDER BY a.createdOn asc";
        $leave_request_details = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
//        echo $this->db->last_query();
        foreach ($leave_request_details as $lrd)//if backup is 1
        {
            if ($lrd->original_leaveType_ID != NULL)
            {
                $lrd->original_leaveType = $this->general_model->fetch_specific_val("b.leaveType", "a.requestLeaveDetails_ID = 1 AND a.original_leaveType_ID=b.leaveType_ID", "tbl_request_leave_details a,tbl_leave_type b")->leaveType;
            }
            else
            {
                $lrd->original_leaveType = NULL;
            }
            $lrd->requestLeave_ID_str = str_pad($lrd->requestLeave_ID, 8, '0', STR_PAD_LEFT);
            $lrd->employee = $this->general_model->fetch_specific_val("a.fname,a.lname,a.mname,b.emp_id,d.acc_description,b.isActive", "b.acc_id=d.acc_id AND a.apid=b.apid AND b.emp_id=c.emp_id AND b.isActive='yes' AND c.uid=" . $lrd->uid, "tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d,tbl_");
        }
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $leave_request_details, 'total' => $count));
    }

}
