<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once dirname(__FILE__) . "/General_model.php";

class Process_model extends General_model
{
	public function get_allfolders($folder_ids, $word = null)
	{
		$newAppend = ($folder_ids != NULL && !empty($folder_ids)) ? "AND folder.folder_ID NOT IN (" . implode(',', $folder_ids) . ")" : "";
		$append = ($word != null) ? " AND (folder.folderName like '%$word%' OR CONCAT(app.fname,' ',app.lname) like '%$word%')" : "";
		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where = "folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1' AND folder.status_ID!=15 AND folder.isRemoved!=1 $append $newAppend";
		$this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_ownfolders($folder_ids, $word = null, $userid, $perpage = 6)
	{
		$newAppend = ($folder_ids != NULL && !empty($folder_ids)) ? "AND folder.folder_ID NOT IN (" . implode(',', $folder_ids) . ")" : "";
		$append = ($word != null) ? " AND folder.folderName like '%$word%'" : "";
		$this->db->select('folder.*, app.fname,app.lname,app.mname,app.apid');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where = "folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1' AND folder.isRemoved!=1 AND folder.status_ID!=15 AND folder.createdBy=$userid $append $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_assignedfolders($folder_ids, $word = null, $userid)
	{
		$newAppend = ($folder_ids != NULL && !empty($folder_ids)) ? "AND folder.folder_ID NOT IN (" . implode(',', $folder_ids) . ")" : "";
		$append = ($word != null) ? " AND folder.folderName like '%$word%'" : "";
		$this->db->select('folder.*, app.fname,app.lname,app.apid, ass.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');

		$where = "folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1' AND folder.folder_ID=ass.folProCheTask_ID AND ass.type='folder' AND ass.assignedTo=$userid AND folder.status_ID!=15 AND folder.isRemoved!=1 $append  $newAppend";
		$this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextallfolders($lastid,$str=null){
		$append = ($str!=null) ? " AND folder.folderName like '%$str%'" : "";
		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.createdBy=user.uid"
		. " AND user.emp_id=employee.emp_id"
		. " AND employee.apid=app.apid"
		. " AND folder.referenceFolder='1'"
		. " AND folder.folder_ID!='1'"
		. " AND folder.status_ID!=15"
		. " AND folder.folder_ID >$lastid $append";
		// $this->db->limit(6);
		$this->db->order_by("folder.folder_ID", "asc");
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextownedfolders($lastid,$str=null,$userid){
		$append = ($str!=null) ? " AND folder.folderName like '%$str%'" : "";

		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.createdBy=user.uid"
		. " AND user.emp_id=employee.emp_id"
		. " AND employee.apid=app.apid"
		. " AND folder.referenceFolder='1'"
		. " AND folder.folder_ID!='1'"
		. " AND AND folder.createdBy=$userid"
		. " AND folder.folder_ID >$lastid $append";
		// $this->db->limit(6);
		$this->db->order_by("folder.folder_ID", "asc");
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextassignedfolders($lastid,$str=null,$userid){
		$append = ($str!=null) ? " AND folder.folderName like '%$str%'" : "";
		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');
		$where="folder.createdBy=user.uid"
		. " AND user.emp_id=employee.emp_id"
		. " AND employee.apid=app.apid"
		. " AND folder.referenceFolder='1'"
		. " AND folder.folder_ID!='1'"
		. " AND folder.folder_ID=ass.folProCheTask_ID"
		. " AND ass.type='folder'"
		. " AND ass.assignedTo=$userid"
		. " AND folder.folder_ID >$lastid $append";
		// $this->db->limit(6);
		$this->db->order_by("folder.folder_ID", "asc");
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	//ARCHIVED FOLDERS
	public function get_allarchivedfolders($word=null,$folder_display,$userid){
		$append = ($word!=null) ? " AND folder.folderName like '%$word%'" : "";
		$this->db->select('folder.*, app.*');
		if($folder_display=="1"){
			$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
			$where="folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.folder_ID!='1' AND folder.status_ID=15 $append";
		}else{
			$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
			$where="folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.folder_ID!='1' AND folder.createdBy=$userid AND folder.status_ID=15 $append";
		}
		// $this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_allprocess($process_ids = NULL, $word = null, $select_val = null)
	{
		$append = ($word != null) ? " AND process.processTitle like '%$word%' OR folder.folderName like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND process.processstatus_ID like '%$select_val%'" : "";
		$newAppend = ($process_ids != null && !empty($process_ids)) ? "AND process.process_ID NOT IN (" . implode(',', $process_ids) . ")" : "";
		$this->db->select('process.*, app.fname,app.lname,app.mname,folder.folder_ID,folder.folderName');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		$where = "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND folder.status_ID!=15 AND folder.isRemoved!=1 AND process.processstatus_ID!=15 AND process.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit(6);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_allprocess_subfolder($process_ids, $folder_ID, $word = null, $select_val = null, $perpage = 6)
	{
		$newAppend = ($process_ids != null && !empty($process_ids)) ? "AND process.process_ID NOT IN (" . implode(',', $process_ids) . ")" : "";
		$append = ($word != null) ? " AND process.processTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND process.processstatus_ID like '%$select_val%'" : "";
		$this->db->select('process.*, app.fname,app.lname,app.mname,folder.folder_ID,folder.folderName');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		$where = "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=$folder_ID AND process.folder_ID=folder.folder_ID AND folder.status_ID!=15 AND process.processstatus_ID!=15 AND folder.isRemoved!=1 AND process.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_allarchivedprocesstemp($word=null,$process_display,$userid){
		$append = ($word!=null) ? " AND process.processTitle like '%$word%'" : "";
		$this->db->select('process.*, app.fname,app.lname,app.mname,folder.folder_ID,folder.folderName');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		if($process_display=="1"){
			$where="process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND process.processstatus_ID=15 $append";
		}else{
			$where="process.createdBy=user.uid AND process.createdBy=$userid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND process.processstatus_ID=15 $append";
		}
		// $this->db->limit(6);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_allarchivedprocess_underfolder($word=null,$process_display,$userid){
		$append = ($word!=null) ? " AND process.processTitle like '%$word%'" : "";
		$this->db->select('process.*, app.fname,app.lname,app.mname,folder.folder_ID,folder.folderName');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		
		if($process_display=="1"){
			$where="process.createdBy=user.uid AND process.createdBy=$userid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND folder.status_ID=15 $append";
		}else{
			$where="process.createdBy=user.uid AND process.createdBy=$userid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND folder.status_ID=15 $append";
		}
		// $this->db->limit(6);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_allarchivedchecklist($word=null,$checklist_display,$userid){
		$append = ($word!=null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$this->db->select('checklist.*,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist,tbl_processST_process process');

		if($checklist_display=="1"){
			$where="checklist.isArchived='1' AND checklist.process_ID=process.process_ID AND checklist.checklistTitle IS NOT NULL $append";			
		}else{
			$where="checklist.createdBy=$userid AND checklist.isArchived='1' AND checklist.process_ID=process.process_ID AND checklist.checklistTitle IS NOT NULL $append";
		}
		// $this->db->limit(6);
		$this->db->where($where);
		$this->db->order_by("checklist.checklistTitle", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_allarchivedchecklist_process($word=null,$checklist_display,$userid){
		$append = ($word!=null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$this->db->select('checklist.*,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process');

		if($checklist_display=="1"){
			$where="checklist.process_ID=process.process_ID AND checklist.checklistTitle IS NOT NULL AND process.processstatus_ID='15' $append";
		}else{
			$where="checklist.createdBy=$userid AND checklist.process_ID=process.process_ID AND checklist.checklistTitle IS NOT NULL AND process.processstatus_ID='15' $append";
		}
		// $this->db->limit(6);
		$this->db->where($where);
		$this->db->order_by("checklist.checklistTitle", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_allarchivedchecklist_folder($word=null,$checklist_display,$userid){
		$append = ($word!=null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$this->db->select('checklist.*,folder.status_ID as folstat,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process, tbl_processST_folder folder');
		if($checklist_display=="1"){
			$where="checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND checklist.checklistTitle IS NOT NULL AND folder.status_ID='15' $append";
		}else{
			$where="checklist.createdBy=$userid AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND checklist.checklistTitle IS NOT NULL AND folder.status_ID='15' $append";
		}
		// $this->db->limit(6);
		$this->db->where($where);
		$this->db->order_by("checklist.checklistTitle", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	//ALL PROCESSES INSIDE FOLDER
	public function get_assignedprocess_insidefolder($process_ids, $userid, $folderid, $word = null, $select_val = null,$perpage = 6)
	{
		$newAppend = ($process_ids != null && !empty($process_ids)) ? "AND process.process_ID NOT IN (" . implode(',', $process_ids) . ")" : "";
		$append = ($word != null) ? " AND process.processTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND process.processstatus_ID like '%$select_val%'" : "";
		$this->db->select('process.*, app.fname,app.lname,app.apid,folder.folderName,folder.folder_ID,folder.referenceFolder,folder.status_ID,folder.createdBy,folder.updatedBy,ass.type, ass.assignmentRule_ID as processruleid, ass.assignedTo, ass.assign_ID');

		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee,tbl_processST_assignment ass');
		$where = "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND process.processstatus_ID!=15 AND folder.status_ID!=15 AND process.processstatus_ID!=15 AND process.process_ID=ass.folProCheTask_ID AND ass.type='process' AND ass.assignedTo=$userid AND process.folder_ID=$folderid AND folder.folder_ID=$folderid AND process.isRemoved!=1 AND folder.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_processunder_folder_insidefolder($process_ids, $userid, $folderid, $word = null, $select_val = null,$perpage = 6)
	{
		$newAppend = ($process_ids != null && !empty($process_ids)) ? "AND process.process_ID NOT IN (" . implode(',', $process_ids) . ")" : "";
		$append = ($word != null) ? " AND process.processTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND process.processstatus_ID like '%$select_val%'" : "";
		$this->db->select('process.process_ID, process.processTitle,process.processType_ID,process.processstatus_ID,process.createdBy,process.dateTimeCreated,process.folder_ID as processfolderid,app.fname,app.lname,app.apid,folder.folderName,folder.folder_ID,folder.referenceFolder,folder.status_ID,folder.createdBy,folder.updatedBy,ass.type, ass.assignmentRule_ID as folderruleid, ass.assignedTo, ass.assign_ID');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');

		$where = "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND folder.status_ID!=15 AND ass.type='folder' AND ass.assignedTo=$userid AND ass.folProCheTask_ID=process.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND ass.folProCheTask_ID=$folderid AND process.folder_ID=$folderid AND folder.folder_ID=$folderid AND process.processstatus_ID!=15 AND process.isRemoved!=1 AND folder.isRemoved!=1 AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_ownedprocessunder_folder_insidefolder($process_ids, $userid, $folderid, $word = null, $select_val = null,$perpage = 6)
	{
		$newAppend = ($process_ids != null && !empty($process_ids)) ? "AND process.process_ID NOT IN (" . implode(',', $process_ids) . ")" : "";
		$append = ($word != null) ? " AND process.processTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND process.processstatus_ID like '%$select_val%'" : "";
		$this->db->select('process.process_ID, process.processTitle,process.processType_ID,process.processstatus_ID,process.createdBy,process.dateTimeCreated,process.folder_ID as processfolderid,app.fname,app.lname,app.apid,folder.folderName,folder.folder_ID,folder.referenceFolder,folder.status_ID,folder.createdBy,folder.updatedBy,ass.type, ass.assignmentRule_ID as folderruleid, ass.assignedTo, ass.assign_ID');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');
		$where = "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND folder.status_ID!=15 AND ass.type='folder' AND ass.assignedTo=$userid AND ass.folProCheTask_ID=process.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND ass.folProCheTask_ID=$folderid AND process.folder_ID=$folderid AND folder.folder_ID=$folderid AND process.processstatus_ID!=15 AND process.isRemoved!=1 AND folder.isRemoved!=1 AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7) AND process.createdBy=$userid $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	//
	public function get_ownprocess($process_ids, $word = null, $select_val = null, $userid, $perpage = 6)
	{
		$newAppend = ($process_ids != null && !empty($process_ids)) ? "AND process.process_ID NOT IN (" . implode(',', $process_ids) . ")" : "";
		$append = ($word != null) ? " AND process.processTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND process.processstatus_ID like '%$select_val%'" : "";
		$this->db->select('process.process_ID, process.processTitle,process.processType_ID,process.processstatus_ID,process.createdBy as ownid,process.dateTimeCreated,process.folder_ID,app.fname,app.lname,app.apid,folder.folderName,folder.folder_ID,folder.referenceFolder,folder.status_ID,folder.createdBy,folder.updatedBy');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		$where = "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND process.createdBy=$userid AND process.isRemoved!=1 AND process.processstatus_ID!=15 AND folder.status_ID!=15 AND folder.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_assignedprocess($process_ids, $word = null, $select_val = null, $userid)
	{
		$append = ($word != null) ? " AND process.processTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND process.processstatus_ID like '%$select_val%'" : "";
		$newAppend = ($process_ids != null && !empty($process_ids)) ? "AND process.process_ID NOT IN (" . implode(',', $process_ids) . ")" : "";
		$this->db->select('process.*, app.fname,app.lname,app.apid,folder.folderName,folder.folder_ID,folder.referenceFolder,folder.status_ID,folder.createdBy,folder.updatedBy,ass.*');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee,tbl_processST_assignment ass');
		$where = "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND process.processstatus_ID!=15 AND folder.status_ID!=15 AND process.process_ID=ass.folProCheTask_ID AND ass.type='process' AND ass.assignedTo=$userid AND folder.isRemoved!=1 AND process.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit(6);
		$this->db->where($where);  
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_processunder_folder($process_ids, $word = null, $select_val = null, $userid, $perpage = 6)
	{
		$newAppend = ($process_ids != null && !empty($process_ids)) ? "AND process.process_ID NOT IN (" . implode(',', $process_ids) . ")" : "";
		$append = ($word != null) ? " AND process.processTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND process.processstatus_ID like '%$select_val%'" : "";
		$this->db->select('process.process_ID, process.processTitle,process.processType_ID,process.processstatus_ID,process.createdBy,process.dateTimeCreated,process.folder_ID as processfolderid,app.fname,app.lname,app.apid,folder.folderName,folder.folder_ID,folder.referenceFolder,folder.status_ID,folder.createdBy,folder.updatedBy,ass.*');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');
		$where = "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.processstatus_ID!=15 AND process.folder_ID=folder.folder_ID AND folder.status_ID!=15 AND ass.type='folder' AND ass.assignedTo=$userid AND ass.folProCheTask_ID=process.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND folder.isRemoved!=1 AND process.isRemoved!=1 AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_ownedprocessunder_folder($process_ids, $word = null, $select_val = null, $userid, $perpage = 6)
	{
		$append = ($word != null) ? " AND process.processTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND process.processstatus_ID like '%$select_val%'" : "";
		$newAppend = ($process_ids != null && !empty($process_ids)) ? "AND process.process_ID NOT IN (" . implode(',', $process_ids) . ")" : "";
		$this->db->select('process.process_ID, process.processTitle,process.processType_ID,process.processstatus_ID,process.createdBy,process.dateTimeCreated,process.folder_ID as processfolderid,app.fname,app.lname,app.apid,folder.folderName,folder.folder_ID,folder.referenceFolder,folder.status_ID,folder.createdBy,folder.updatedBy,ass.*');

		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');
		$where = "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND folder.status_ID!=15 AND ass.type='folder' AND ass.assignedTo=$userid AND ass.folProCheTask_ID=process.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND folder.isRemoved!=1 AND process.isRemoved!=1 AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7) AND process.createdBy=$userid $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$this->db->order_by("process.process_ID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextallprocesses($lastid,$str=null){
		$append = ($str!=null) ? " AND process.processTitl  -e like '%$str%'" : "";
		$this->db->select('process.*, app.*,folder.*');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		$where="process.createdBy=user.uid"
		. " AND user.emp_id=employee.emp_id"
		. " AND employee.apid=app.apid"
		. " AND process.process_ID >$lastid $append"
		. " AND process.folder_ID=folder.folder_ID"  
		. " AND process.processstatus_ID!=15"
		. " AND folder.status_ID!=15";
		// $this->db->limit(6);
		$this->db->order_by("process.process_ID", "asc");
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextownedprocesses($lastid,$str=null,$userid){
		$append = ($word!=null) ? " AND process.processTitle like '%$str%'" : "";
		$this->db->select('process.*, app.*,folder.*');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		$where="process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND process.createdBy=$userid AND process.process_ID >$lastid $append";
		// $this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextassignedprocesses($lastid,$str=null,$userid){
		$append = ($word!=null) ? " AND process.processTitle like '%$word%'" : "";
		$this->db->select('process.*, app.*,folder.*');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee,tbl_processST_assignment ass');
		$where="process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND process.process_ID=ass.folProCheTask_ID AND ass.type='process' AND ass.assignedTo=$userid AND process.process_ID >$lastid $append";
		// $this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_allchecklist(){
		$this->db->select('*');
		$this->db->from('tbl_processST_checklist');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_userinfo($userid){
		$this->db->select('applicant.fname,applicant.mname,applicant.lname');
		$this->db->from('tbl_applicant applicant, tbl_employee employee, tbl_user user');
		$where="user.uid=$userid"
		. " AND user.emp_id=employee.emp_id"
		. " AND employee.apid=applicant.apid";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_process($folder_ID){
		$this->db->select('process.*,folder.*');
		$this->db->from('tbl_processST_process process, tbl_processST_folder folder');
		$where="process.folder_ID=$folder_ID"
		. " AND folder.folder_ID=$folder_ID";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_subfolders($folder_ID){
		$this->db->select('folder.*,app.*');
		$this->db->from('tbl_processST_folder folder,tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.referenceFolder=$folder_ID"
		. " AND user.emp_id=employee.emp_id" 
		. " AND employee.apid=app.apid"
		. " AND folder.createdBy=user.uid";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}	
	public function get_answers($task_ID,$checklist_ID){
		$this->db->select('*');
		$this->db->from('tbl_processST_subtask subtask, tbl_processST_answer ans');
		$where="subtask.task_ID=$task_ID"
		. " AND ans.subtask_ID=subtask.subTask_ID"
		. " AND ans.checklist_ID=$checklist_ID";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();		
	}
	public function get_task($process_ID){
		$this->db->select('task.*,process.process_ID,process.processTitle,process.processType_ID,process.processstatus_ID,process.isRemoved as process_isRemoved');
		$this->db->from('tbl_processST_task task, tbl_processST_process process');
		$where="task.process_ID=$process_ID"
		. " AND task.process_ID=process.process_ID";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_task_cl($process_ID){
		$this->db->select('task_ID');
		$this->db->from('tbl_processST_task');
		$where="process_ID=$process_ID";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_answerabletask($checklist_ID){
		$this->db->select('task.*,checkStatus.*');
		$this->db->from('tbl_processST_task task, tbl_processST_checklistStatus checkStatus');
		$where="task.task_ID=checkStatus.task_ID"
		. " AND checkStatus.checklist_ID=$checklist_ID";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_checklist($process_ID, $word = null, $select_val = null,$check_ids = NULL)
	{
		$newAppend = ($check_ids != NULL && !empty($check_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $check_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist,tbl_processST_process process,tbl_processST_folder folder');
		$where = "checklist.process_ID=$process_ID AND checklist.isArchived!=1 AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND process.processstatus_ID!=15 
		AND folder.status_ID!=15 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_ownedchecklists($checklist_ids, $word = null, $select_val = null, $userid, $perpage = 6)
	{
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$newAppend = ($checklist_ids != NULL && !empty($checklist_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $checklist_ids) . ")" : "";
		$this->db->select('checklist.checklist_ID,checklist.process_ID,checklist.checklistTitle,checklist.status_ID,checklist.isArchived,checklist.dueDate,checklist.createdBy as ownid,checklist.updatedBy,checklist.dateTimeCreated, app.fname,app.lname,app.apid,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_process process, tbl_processST_folder folder');
		$where = "checklist.createdBy=user.uid AND checklist.checklistTitle IS NOT NULL AND user.emp_id=employee.emp_id AND checklist.createdBy=$userid AND employee.apid=app.apid AND checklist.isArchived!=1 AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND process.processstatus_ID!=15 
		AND folder.status_ID!=15 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 $append $append_selectval $newAppend";

		$this->db->limit($perpage);
		$this->db->where($where);
		$this->db->order_by("checklist.checklistTitle", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	//GET ASSIGNED CHECKLIST UNDER PROCESS
	public function get_ownedchecklists_underprocess($userid,$process_ID,$word=null){
		$append = ($word!=null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$this->db->select('checklist.checklist_ID,checklist.process_ID,checklist.checklistTitle,checklist.status_ID,checklist.isArchived,checklist.dueDate,checklist.createdBy as ownid,checklist.updatedBy,checklist.dateTimeCreated, app.fname,app.lname,app.apid,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_user user, tbl_applicant app, tbl_employee employee,tbl_processST_process process');

		$where="checklist.createdBy=user.uid AND user.emp_id=employee.emp_id AND checklist.createdBy=$userid AND checklist.process_ID=$process_ID AND employee.apid=app.apid AND checklist.isArchived!=1 AND checklist.process_ID=process.process_ID $append";
		// $this->db->limit(6);
		$this->db->where($where);
		$this->db->order_by("checklist.checklistTitle", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_assignedchecklists_underprocess($check_ids, $userid, $process_ID, $word = null, $select_val = null)
	{
		$newAppend = ($check_ids != NULL && !empty($check_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $check_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.*, process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass, tbl_processST_process process, tbl_processST_folder folder');

		$where = "checklist.createdBy=user.uid AND user.emp_id=employee.emp_id AND checklist.checklist_ID=ass.folProCheTask_ID AND ass.type='checklist' AND ass.assignedTo=$userid AND employee.apid=app.apid AND checklist.process_ID=$process_ID AND checklist.isArchived!=1 AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND process.processstatus_ID!=15 AND folder.status_ID!=15 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_checklistunder_insideprocess($check_ids, $userid, $process_ID, $word = null, $select_val = null,$perpage = 6)
	{
		$newAppend = ($check_ids != NULL && !empty($check_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $check_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.type, ass.assignmentRule_ID as processruleid, ass.assignedTo, ass.assign_ID,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process, tbl_user user, tbl_applicant app, tbl_employee employee,tbl_processST_folder folder, tbl_processST_assignment ass');
		$where = "checklist.createdBy=user.uid AND user.emp_id=employee.emp_id AND checklist.process_ID=process.process_ID AND process.processstatus_ID!=15 AND ass.folProCheTask_ID=process.process_ID AND ass.type='process' AND folder.folder_ID=process.folder_ID AND folder.status_ID!=15 AND ass.assignedTo=$userid AND employee.apid=app.apid AND checklist.process_ID=$process_ID AND checklist.isArchived!=1 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_ownedchecklistunder_insideprocess($check_ids, $userid, $process_ID, $word = null, $select_val = null,$perpage = 6)
	{
		$newAppend = ($check_ids != NULL && !empty($check_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $check_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.type, ass.assignmentRule_ID as processruleid, ass.assignedTo, ass.assign_ID,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process, tbl_user user, tbl_applicant app, tbl_employee employee,tbl_processST_folder folder, tbl_processST_assignment ass');
		$where = "checklist.createdBy=user.uid AND user.emp_id=employee.emp_id AND checklist.process_ID=process.process_ID AND process.processstatus_ID!=15 AND ass.folProCheTask_ID=process.process_ID AND ass.type='process' AND folder.folder_ID=process.folder_ID AND folder.status_ID!=15 AND ass.assignedTo=$userid AND employee.apid=app.apid AND checklist.process_ID=$process_ID AND checklist.isArchived!=1 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7) AND checklist.createdBy=$userid $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_checklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word = null, $select_val = null,$perpage = 6)
	{
		$newAppend = ($check_ids != NULL && !empty($check_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $check_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.type, ass.assignmentRule_ID as folderruleid, ass.assignedTo, ass.assign_ID, folder.folder_ID, folder.folderName, process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process,tbl_processST_folder folder,tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');
		$where = "checklist.createdBy=user.uid AND user.emp_id=employee.emp_id AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND ass.type='folder' AND ass.assignedTo=$userid AND employee.apid=app.apid AND checklist.process_ID=$process_ID AND folder.status_ID!=15 AND checklist.isArchived!=1 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_ownedchecklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word = null, $select_val = null,$perpage = 6)
	{	
		$newAppend = ($check_ids != NULL && !empty($check_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $check_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.type, ass.assignmentRule_ID as folderruleid, ass.assignedTo, ass.assign_ID, folder.folder_ID, folder.folderName, process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process,tbl_processST_folder folder,tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');

		$where = "checklist.createdBy=user.uid AND user.emp_id=employee.emp_id AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND ass.type='folder' AND ass.assignedTo=$userid AND employee.apid=app.apid AND checklist.process_ID=$process_ID AND folder.status_ID!=15 AND checklist.isArchived!=1 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7) AND checklist.createdBy=$userid $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	//
	public function get_checklistunderprocess($checklist_ids, $word = null, $select_val = null, $userid, $perpage = 6)
	{
		$newAppend = ($checklist_ids != NULL && !empty($checklist_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $checklist_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.*,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process, tbl_user user, tbl_applicant app, tbl_employee employee,tbl_processST_folder folder, tbl_processST_assignment ass');
		$where = "checklist.createdBy=user.uid AND checklist.checklistTitle IS NOT NULL AND user.emp_id=employee.emp_id AND checklist.process_ID=process.process_ID AND process.processstatus_ID!=15 AND ass.folProCheTask_ID=process.process_ID AND ass.type='process' AND folder.folder_ID=process.folder_ID AND folder.status_ID!=15 AND ass.assignedTo=$userid AND employee.apid=app.apid AND checklist.isArchived!=1 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_ownedchecklistunderprocess($checklist_ids, $word = null, $select_val = null, $userid, $perpage = 6)
	{
		$newAppend = ($checklist_ids != NULL && !empty($checklist_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $checklist_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.*,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process, tbl_user user, tbl_applicant app, tbl_employee employee,tbl_processST_folder folder, tbl_processST_assignment ass');
		$where = "checklist.createdBy=user.uid AND checklist.checklistTitle IS NOT NULL AND user.emp_id=employee.emp_id AND checklist.process_ID=process.process_ID AND process.processstatus_ID!=15 AND ass.folProCheTask_ID=process.process_ID AND ass.type='process' AND folder.folder_ID=process.folder_ID AND folder.status_ID!=15 AND ass.assignedTo=$userid AND employee.apid=app.apid AND checklist.isArchived!=1 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7) AND checklist.createdBy=$userid $append $append_selectval  $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_checklistunder_processfolder($checklist_ids, $word = null, $select_val = null, $userid, $perpage = 6)
	{
		$newAppend = ($checklist_ids != NULL && !empty($checklist_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $checklist_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.*, folder.folder_ID, folder.folderName,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process,tbl_processST_folder folder,tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');
		$where = "checklist.createdBy=user.uid AND checklist.checklistTitle IS NOT NULL AND user.emp_id=employee.emp_id AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND ass.type='folder' AND ass.assignedTo=$userid AND employee.apid=app.apid AND folder.status_ID!=15 AND process.processstatus_ID!=15 AND checklist.isArchived!=1 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_ownedchecklistunder_processfolder($checklist_ids, $word = null, $select_val = null, $userid, $perpage = 6)
	{
		$newAppend = ($checklist_ids != NULL && !empty($checklist_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $checklist_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.*, folder.folder_ID, folder.folderName,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_processST_process process,tbl_processST_folder folder,tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass');
		$where = "checklist.createdBy=user.uid AND checklist.checklistTitle IS NOT NULL AND user.emp_id=employee.emp_id AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND ass.type='folder' AND ass.assignedTo=$userid AND employee.apid=app.apid AND folder.status_ID!=15 AND process.processstatus_ID!=15 AND checklist.isArchived!=1 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7) AND checklist.createdBy=$userid $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_assignedchecklists($checklist_ids, $word = null, $select_val = null, $userid)
	{
		$newAppend = ($checklist_ids != NULL && !empty($checklist_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $checklist_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.assign_ID,ass.type,ass.assignmentRule_ID as checklist_assrule,ass.folProCheTask_ID,ass.assignedTo,process.processTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass, tbl_processST_process process, tbl_processST_folder folder');

		$where = "checklist.createdBy=user.uid AND checklist.checklistTitle IS NOT NULL AND user.emp_id=employee.emp_id AND checklist.checklist_ID=ass.folProCheTask_ID AND ass.type='checklist' AND ass.assignedTo=$userid AND employee.apid=app.apid AND checklist.isArchived!=1 AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND process.processstatus_ID!=15 AND folder.status_ID!=15 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_assignedtasks_underchecklists($checklist_ids, $word = null, $select_val = null, $userid, $perpage = 6)
	{
		$newAppend = ($checklist_ids != NULL && !empty($checklist_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $checklist_ids) . ")" : "";
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$this->db->select('checklist.*, app.fname,app.lname,app.apid, ass.assign_ID, ass.type,ass.assignmentRule_ID as taskruleid, ass.assignedTo, ass.folProCheTask_ID, ass.dateTimeAssigned, process.processTitle');
		$this->db->group_by('checklist.checklistTitle');
		$this->db->from('tbl_processST_checklist checklist, tbl_user user, tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass, tbl_processST_checklistStatus as checkstat, tbl_processST_process process');
		$where = "checklist.createdBy=user.uid AND checklist.checklistTitle IS NOT NULL AND user.emp_id=employee.emp_id AND checkstat.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedTo=$userid AND employee.apid=app.apid AND checkstat.checklist_ID=checklist.checklist_ID AND checklist.isArchived!=1 AND checklist.process_ID=process.process_ID AND process.isRemoved!=1 AND checklist.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit($perpage);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextallchecklist($process_ID,$lastid){
		$this->db->select('checklist.*');
		$this->db->from('tbl_processST_checklist checklist');
		$where="checklist.process_ID=$process_ID"
		. " AND checklist.checklist_ID > $lastid";
		// $this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_checklistStat(){
		$this->db->select('*');
		$this->db->from('tbl_processST_checklistStatus');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_singlefolderinfo($folder_ID){
		$result = $this->db->get_where('tbl_processST_folder', array( 
			'folder_ID'=>$folder_ID));
		return $result->row();
	}
	public function get_singleprocessinfo($process_ID){
		$result = $this->db->get_where('tbl_processST_process', array( 
			'process_ID'=>$process_ID));
		return $result->row();
	}
	public function get_singlechecklistinfo($checklist_ID){
		$result = $this->db->get_where('tbl_processST_checklist', array( 
			'checklist_ID'=>$checklist_ID));
		return $result->row();
	}
	public function get_singleCheckstatinfo($checkliststat_ID){
		$this->db->from('tbl_processST_checklistStatus');
		$this->db->where(array('checklistStatus_ID'=>$checkliststat_ID));
		return $this->db->get()->row_array();
	}
	public function get_checklistStatusID($checklist_ID,$task_ID){
		$this->db->select('check.checklistStatus_ID');
		$this->db->from('tbl_processST_checklistStatus check');
		$where="check.task_ID=$task_ID"
		. " AND check.checklist_ID=$checklist_ID";
		$this->db->where($where);
		return $this->db->get('tbl_processST_checklistStatus');
	}
	public function get_appid($emp_id){
		$this->db->select('emp.apid');
		$this->db->from('tbl_employee emp');
		$where="emp.emp_id=$emp_id";
		$this->db->where($where);
		return $this->db->get('tbl_employee');
	}
	public function get_empid($uid){
		$this->db->select('user.emp_id');
		$this->db->from('tbl_user user');
		$where="user.uid=$uid";
		$this->db->where($where);
		return $this->db->get('tbl_user');
	}
	public function get_empid_assignment($apid){
		$this->db->select('emp.emp_id');
		$this->db->from('tbl_employee emp');
		$where="emp.apid=$apid";
		$this->db->where($where);
		return $this->db->get('tbl_employee');
	}
	public function get_userid_assignment($empid){
		$this->db->select('user.uid');
		$this->db->from('tbl_user user');
		$where="user.emp_id=$empid";
		$this->db->where($where);
		return $this->db->get('tbl_user');
	}
	public function get_answerid($subtask,$checklist){
		$this->db->select('ans.answer_ID');
		$this->db->from('tbl_processST_answer ans');
		$where="ans.subtask_ID=$subtask"
		. " AND ans.checklist_ID=$checklist";
		$this->db->where($where);
		return $this->db->get('tbl_processST_answer');
	}
	public function get_ansid($subtask_ID,$checklist_ID,$answer){
		$this->db->select('ans.answer_ID');
		$this->db->from('tbl_processST_answer ans');
		$where="ans.checklist_ID=$checklist_ID"
		." AND ans.subtask_ID=$subtask_ID"
		." AND ans.answer='$answer'";
		$this->db->where($where);
		return $this->db->get('tbl_processST_answer');
	}
	public function get_dropdowninfo($stat){
		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1' AND folder.status_ID=$stat";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_owneddropdowninfo($stat,$userid ){
		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1' AND folder.status_ID=$stat AND folder.createdBy=$userid";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_assigneddropdowninfo($stat){
		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1' AND folder.status_ID=$stat";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_dropdownproinfo($stat){
		$this->db->select('process.*, app.*,folder.*');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		$where="process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND process.processstatus_ID=$stat";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	//GET CHECKLISTS
	public function get_alldisplaychecklist($checklist_ids, $word = null, $select_val = null)
	{
		$append = ($word != null) ? " AND checklist.checklistTitle like '%$word%'" : "";
		$append_selectval = ($select_val != null) ? " AND checklist.status_ID like '%$select_val%'" : "";
		$newAppend = ($checklist_ids != NULL && !empty($checklist_ids)) ? " AND checklist.checklist_ID NOT IN (" . implode(',', $checklist_ids) . ")" : "";
		$this->db->select('checklist.*,process.processTitle');

		$this->db->from('tbl_processST_checklist checklist, tbl_processST_folder folder, tbl_processST_process process');
		$where = "checklist.isArchived!='1' AND checklist.checklistTitle IS NOT NULL AND checklist.process_ID=process.process_ID AND process.folder_ID=folder.folder_ID AND process.processstatus_ID!=15 AND folder.status_ID!=15 AND folder.isRemoved!=1 AND process.isRemoved!=1 AND checklist.isRemoved!=1 $append $append_selectval $newAppend";
		$this->db->limit(6);
		$this->db->where($where);
		$this->db->order_by("checklist.checklistTitle", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function add_admin($data){
		$this->db->insert('tbl_processST_admin_access', $data);
	}
	public function add_folder($data){
		$this->db->insert('tbl_processST_folder', $data);
	}
	public function add_checklist($data){
		$this->db->insert('tbl_processST_checklist', $data);
	}
	public function add_processfolderM($data){
		$this->db->insert('tbl_processST_process', $data);
		$insertId = $this->db->insert_id();
		return $insertId;
	}
	public function add_anotherchecklist($data){
		$this->db->insert('tbl_processST_checklist', $data);
		$insertId = $this->db->insert_id();
		return $insertId;
	}
	public function add_task($data){
		$this->db->insert('tbl_processST_task', $data);
	}
	public function add_anothertask($data){
		$this->db->insert('tbl_processST_task', $data);
		$insertId = $this->db->insert_id();
		return $insertId;
	}
	public function add_checklistStatus($data){
		$this->db->insert('tbl_processST_checklistStatus', $data);
	}
	public function addbatch_checkstatus($data,$table){
		$this->db->trans_start();
		$this->db->insert_batch($table, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	function save_upload($image){
		$data = array(
			'task_ID'     => 001,
			'complabel' => $image
		);
		$result= $this->db->insert('tbl_processST_subtask',$data);
		return $result;
	}
	public function delete_assignment($id){
		$where="assign_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_assignment');
	}
	public function delete_folprochetask_assignment($id){
		$where="folProCheTask_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_assignment');
	}
	public function delete_answer($id){
		$where="answer_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_answer');
	}
	public function delete_all_answer($id){
		$where="subtask_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_answer');
	}
	public function delete_checklist_answer($id){
		$where="checklist_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_answer');
	}
	public function delete_checklistStat($id){
		$where="task_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_checklistStatus');
	}
	public function delete_check_checkliststat($id){
		$where="checklist_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_checklistStatus');
	}
	public function delete_process($id){
		$where="folder_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_process');
	}
	public function delete_option($id){
		$where="componentSubtask_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_component_subtask');
	}
	public function delete_compsubtask($id){
		$where="subTask_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_component_subtask');
	}
	public function delete_subtask($id){
		$where="subTask_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_subtask');
	}
	public function delete_checklist($id){
		$where="process_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_checklist');      
	}
	public function delete_onechecklist($id){
		$where="checklist_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_checklist');      
	}
	public function delete_folder($id){
		$where="folder_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_folder');
	}
	public function delete_task($id){
		$where="task_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_task');
	}
	public function delete_subfolder($id){
		$where="referenceFolder=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_folder');
	}
	public function fetch_update($id){
		$this->db->from('tbl_processST_folder');
		$this->db->where(array('folder_ID'=> $id));
		return $this->db->get()->row_array();
	}
	public function fetchprocess_update($id){
		$this->db->from('tbl_processST_process');
		$this->db->where(array('process_ID'=> $id));
		return $this->db->get()->row_array();
	}
	public function fetchchecklist_update($id){
		$this->db->from('tbl_processST_checklist');
		$this->db->where(array('checklist_ID'=> $id));
		return $this->db->get()->row_array();
	}
	public function fetchtask_update($id){
		$this->db->from('tbl_processST_task');
		$this->db->where(array('task_ID'=> $id));
		return $this->db->get()->row_array();
	}
	public function update_removefolprocheck($id,$type,$data){
		if($type=="folder"){
			$this->db->where('folder_ID',$id);
			$this->db->update('tbl_processST_folder',$data);	
		}else if($type=="process"){
			$this->db->where('process_ID',$id);
			$this->db->update('tbl_processST_process',$data);
		}else{
			$this->db->where('checklist_ID',$id);
			$this->db->update('tbl_processST_checklist',$data);
		}
	}
	public function updatefolder_process($id,$data){
		$this->db->where('folder_ID',$id);
		$this->db->update('tbl_processST_folder',$data);
	}
	public function update_process($id,$data){
		$this->db->where('process_ID',$id);
		$this->db->update('tbl_processST_process',$data);
	}
	public function update_checklist($id,$data){
		$this->db->where('checklist_ID',$id);
		$this->db->update('tbl_processST_checklist',$data);
	}
	public function update_checklistStat($id,$data){
		$this->db->where('process_ID',$id);
		$this->db->update('tbl_processST_checklist',$data);
	}
	public function update_taskdetails($id,$data){
		$this->db->where('task_ID',$id);
		$this->db->update('tbl_processST_task',$data);
	}
	public function update_taskStatus($id,$data){
		$this->db->where('checklistStatus_ID',$id);
		$this->db->update('tbl_processST_checklistStatus',$data);
	}
	public function update_taskAllStatus($id,$data){
		$this->db->where('checklist_ID',$id);
		$this->db->update('tbl_processST_checklistStatus',$data);
	}
	public function update_checklistdetails($id,$data){
		$this->db->where('checklist_ID',$id);
		$this->db->update('tbl_processST_checklist',$data);
	}
	public function update_subtaskdetails($id,$data){
		$this->db->where('subTask_ID',$id);
		$this->db->update('tbl_processST_subtask',$data);
	}
	public function update_taskduedate($id,$data){
		$this->db->where('checklistStatus_ID',$id);
		$this->db->update('tbl_processST_checklistStatus',$data);
	}
	public function update_compsubtaskdetails($id,$data){
		$this->db->where('componentSubtask_ID',$id);
		$this->db->update('tbl_processST_component_subtask',$data);
	}
	public function update_answerdetails($ansid,$data){
		$this->db->where('answer_ID',$ansid);
		$this->db->update('tbl_processST_answer',$data);
	}
	public function update_inputformdetails($id,$data){
		$this->db->where('subTask_ID',$id);
		$this->db->update('tbl_processST_subtask',$data);
	}
	public function update_assignmentpermission($id,$data){
		$this->db->where('assign_ID',$id);
		$this->db->update('tbl_processST_assignment',$data);
	}
	public function count_checklistStatus($id){
		$this->db->where(array('checklist_ID'=>$id));
		$this->db->from('tbl_processST_checklistStatus');
		return $this->db->count_all_results();
	}
	public function count_folders($lastid){
		$this->db->where(array('folder_ID',$lastid));
		$this->db->from('tbl_processST_folder');
		return $this->db->count_all_results();
	}
	public function count_all_folders(){
		$this->db->where(array('referenceFolder'=>'1'));
		$this->db->from('tbl_processST_folder');
		return $this->db->count_all_results();
	}
	public function count_processtemp($lastid){
		$this->db->where(array('process_ID',$lastid));
		$this->db->from('tbl_processST_process');
		return $this->db->count_all_results();
	}
	public function count_checklist($lastid){
		$this->db->where(array('checklist_ID',$lastid));
		$this->db->from('tbl_processST_checklist ');
		return $this->db->count_all_results();
	}
	public function countallforms($id){
		$this->db->where(array('task_ID',$id));
		$this->db->from('tbl_processST_subtask');
		return $this->db->count_all_results();
	}
	public function check_answerexist($subtaskid,$checklistid){
		$result = $this->db->get_where('tbl_processST_answer', array( 
			'subtask_ID'=>$subtaskid, 'checklist_ID'=>$checklistid));
		$count = $result->num_rows();
		return $count;
	}
}