<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Meszenger extends General
{

    protected $title = 'Meszenger';
    // public $logo = "/var/www/html/sz/assets/images/img/logo2.png";
    public $logo = "C:\\wamp64\\www\\sz\\assets\\images\\img\\logo2.png";
    public function __construct()
    {
        parent::__construct();
    }


    public function group_chat()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Control Panel',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/meszenger/group_chat', $data);
        }
    }

    public function group_chat_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT mg.meszenger_group_ID, mg.meszenger_group_name, mg.flag, mg.createdBy, mg.createdOn, app.lname, app.fname, app.mname FROM tbl_meszenger_group mg, tbl_employee emp, tbl_applicant app WHERE mg.createdBy = emp.emp_id AND emp.apid = app.apid";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((mg.meszenger_group_name LIKE '%$keyword[$x]%') OR (DATE_FORMAT(mg.createdOn,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((mg.meszenger_group_name LIKE '%$keyword[$x]%') OR (DATE_FORMAT(mg.createdOn,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
            }
        }
        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);
    }

    public function select_option()
    {
        $emp_id = ($this->input->post('emp_id') != '') ? $this->input->post('emp_id') : 0;
        $acc_id = ($this->input->post('acc_id') != '') ? $this->input->post('acc_id') : 0;
        $account = $this->general_model->custom_query(
            "SELECT acc_id, acc_name, if(find_in_set(acc_id, '".$acc_id."'), 1, 0) checked
            FROM tbl_account ORDER BY checked DESC, acc_name ASC"
        );
        $employee = $this->general_model->custom_query(
            "SELECT emp.emp_id, app.lname, app.fname, app.mname, if(find_in_set(emp.emp_id, '".$emp_id."'), 1, 0) checked FROM tbl_employee emp, tbl_applicant app WHERE emp.apid = app.apid AND emp.isActive = 'yes' ORDER BY checked DESC, app.lname ASC"
        );
        $account_display = '';
        $employee_display = '';
        foreach($account as $value){
            $checked = ($value->checked == 1) ? 'selected' : '';
            $account_display .= "<option ".$checked." value='".$value->acc_id."'>".$value->acc_name."</option>";
        }
        foreach($employee as $value){
            $checked = ($value->checked == 1) ? 'selected' : '';
            $employee_display .= "<option ".$checked." value='".$value->emp_id."'>".ucwords($value->lname).", ".ucwords($value->fname)." ".ucwords($value->mname)."</option>";
        }
        echo json_encode(['account' => $account_display, 'employee' => $employee_display]);
    }

    public function create_group_chat()
    {
        $data =  array(
            "meszenger_group_name"  => $this->input->post('name'),
            "group_acc_id"          => $this->input->post('accounts'),
            "group_emp_id"          => $this->input->post('employee'),
            "flag"                  => $this->input->post('flag'),
            "createdBy"             => $this->session->emp_id
        );
        $query = $this->general_model->insert_vals($data, "tbl_meszenger_group");
        echo json_encode($query);
    }

    public function get_meszenger_data()
    {
        $id = $this->input->post('id');
        $query = $this->general_model->fetch_specific_val(
            "meszenger_group_name, group_acc_id, group_emp_id, flag",
            "meszenger_group_ID = $id",
            "tbl_meszenger_group"
        );
        echo json_encode($query);
    }

    public function update_group_chat()
    {
        $id = $this->input->post('id');
        $data =  array(
            "meszenger_group_name"  => $this->input->post('name'),
            "group_acc_id"          => $this->input->post('accounts'),
            "group_emp_id"          => $this->input->post('employee'),
            "flag"                  => $this->input->post('flag'),
            "updateBy"              => $this->session->emp_id
        );
        $query = $this->general_model->update_vals($data, "meszenger_group_ID = $id", "tbl_meszenger_group");
        echo json_encode($query);
    }

    public function flag_status()
    {
        $id = $this->input->post('id');
        $data = array (
            "flag"                  => $this->input->post('flag')
        );
        $query = $this->general_model->update_vals($data, "meszenger_group_ID = $id", "tbl_meszenger_group");
        echo json_encode($query);
    }

    public function control_panel()
    {
        $stats_emp_id = [];
        $active_emp_id = [];
        $flag_emp_id = [];
        $flag_acc_id = [];
        $isStatus = 0;
        $emp_id = $this->general_model->fetch_specific_vals("emp_id, acc_id", "isActive = 'yes'", "tbl_employee");
        $status = $this->general_model->fetch_all("emp_id", "tbl_meszenger_status");
        $flag = $this->general_model->fetch_specific_vals("group_emp_id, group_acc_id", "flag=1", "tbl_meszenger_group");
        foreach($flag as $value){
            $list_emp = explode(',', $value->group_emp_id);
            $list_acc = explode(',', $value->group_acc_id);
            for($x=0; $x<count($list_acc); $x++){
                array_push($flag_acc_id, $list_acc[$x]);
            }
            for($x=0; $x<count($list_emp); $x++){
                array_push($flag_emp_id, $list_emp[$x]);
            }
        }
        foreach($status as $stat){
            array_push($stats_emp_id, $stat->emp_id);
        }

        foreach($emp_id as $value){
            array_push($active_emp_id, $value->emp_id);
            if(!in_array($value->emp_id, $stats_emp_id)){
                if(in_array($value->acc_id, $flag_acc_id) || in_array($value->emp_id, $flag_emp_id)){
                    $isStatus = 3;
                }
                $datas = array(
                    "emp_id"        => $value->emp_id,
                    "status"        => $isStatus,
                );
                $this->general_model->insert_vals($datas, "tbl_meszenger_status");
                $isStatus = 0;
            }
        }
        foreach($status as $value){
            if(!in_array($value->emp_id, $active_emp_id)){
                $this->general_model->delete_vals("emp_id=$value->emp_id", "tbl_meszenger_status");
            }
        }
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Default Contact',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/meszenger/control_panel', $data);
        }
    }

    public function control_panel_data()
    {
       $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT emp.emp_id, app.lname, app.fname, app.mname, acc.acc_name, app.pic, mz.meszenger_status_ID, mz.status FROM tbl_employee emp, tbl_applicant app, tbl_account acc, tbl_meszenger_status mz WHERE mz.emp_id = emp.emp_id AND emp.apid = app.apid AND emp.acc_id = acc.acc_id AND emp.isActive = 'yes'";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((app.lname LIKE '%$keyword[$x]%') OR (app.fname LIKE '%$keyword[$x]%') OR (app.mname LIKE '%$keyword[$x]%') OR (acc.acc_name LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((app.lname LIKE '%$keyword[$x]%') OR (app.fname LIKE '%$keyword[$x]%') OR (app.mname LIKE '%$keyword[$x]%') OR (acc.acc_name LIKE '%$keyword[$x]%'))";
            }
        }
        if ($datatable['query']['searchStatus'] != '') {
            $stat = $datatable['query']['searchStatus'];
            $query['search']['append'] .= " AND mz.status = $stat";
            $query['search']['total'] .= " AND mz.status = $stat";
        }
        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);
    }

    public function remove_default_user()
    {
        $id = $this->input->post('id');
        $query = $this->general_model->delete_vals("emp_id = $id", "tbl_meszenger_default");
        echo json_encode($query);
    }

    public function change_status()
    {
        $id = $this->input->post('id');
        $data = array (
            "status"    => $this->input->post('status')
        );
        $query = $this->general_model->update_vals($data, "meszenger_status_ID=$id", "tbl_meszenger_status");
        echo json_encode($query);
    }

    public function chat_display()
    {
        $emp_id = $this->session->emp_id;
        $data = $this->general_model->fetch_specific_val("status", "emp_id=$emp_id", "tbl_meszenger_status");
        echo json_encode($data);
    }

    public function resetData()
    {
        $passtext = $this->input->post('passtext');
        if($passtext == "I want to reset the data!"){
            $this->db->truncate('tbl_meszenger_status');
            echo json_encode(1);  
        }
    }
    
}
