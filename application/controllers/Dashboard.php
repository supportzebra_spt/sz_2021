<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Dashboard extends General {

    protected $title = 'Dashboards';

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => $this->title,
        ];
        $fields = "a.apid,fname,lname,pic,emp_id,gender,acc_name,birthday";
        $where = "a.apid=b.apid and c.acc_id=b.acc_id and c.acc_id NOT IN (59,71,92) and month(birthday)=" . date("m") . " and day(birthday)=" . date("d") . " and b.isActive='yes'";
        $result = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_applicant a,tbl_employee b,tbl_account c');
        foreach ($result as $row)
        {
            $job = $this->getCurrentPosition($row->emp_id);
            $data["celebrants"][$row->emp_id] = array(
                "emp_id" => $row->emp_id,
                "apid" => $row->apid,
                "fname" => $row->fname,
                "lname" => $row->lname,
                "pic" => $row->pic,
                "gender" => $row->gender,
                "acc_name" => $row->acc_name,
                "birthday" => $row->birthday,
                "job" => $job,
            );
        }
        $this->load_template_view('templates/dashboard/index', $data);
    }

    public function get_leave_credits()
    {
        $leaveType_IDS = array(5, 6, 2); //BL,SL,VL
        $leave_credits = array();
        foreach ($leaveType_IDS as $leaveType_ID)
        {
            $credits = $this->get_yearly_leave_credits($leaveType_ID);
            $credits['leaveType'] = $this->general_model->fetch_specific_val("leaveType", "leaveType_ID=$leaveType_ID", "tbl_leave_type")->leaveType;
            $leave_credits[] = $credits;
        }
        $uid = $this->session->uid;
        $nextLeave = $this->general_model->custom_query("SELECT a.date,b.leaveType FROM tbl_request_leave_details a,tbl_leave_type b WHERE a.uid = $uid AND a.date > CURDATE() AND a.overAllStatus = 5 AND a.leaveType_ID = b.leaveType_ID ORDER BY date ASC LIMIT 1");
        
        echo json_encode(array('leave_credits' => $leave_credits, 'nextLeave' => $nextLeave));
    }


    public function get_current_schedule(){
       $schedules_today = $this->general_model->custom_query("SELECT a.*,b.*,c.leaveType FROM tbl_schedule a INNER JOIN tbl_schedule_type b ON a.schedtype_id = b.schedtype_id LEFT JOIN tbl_leave_type c ON a.leavenote=c.leaveType_ID WHERE a.sched_date=CURDATE() AND a.emp_id=".$this->session->emp_id);
        $schedules_tomorrow = $this->general_model->custom_query("SELECT a.*,b.*,c.leaveType FROM tbl_schedule a INNER JOIN tbl_schedule_type b ON a.schedtype_id = b.schedtype_id LEFT JOIN tbl_leave_type c ON a.leavenote=c.leaveType_ID WHERE a.sched_date=DATE_ADD(CURDATE(), INTERVAL 1 DAY) AND a.emp_id=".$this->session->emp_id);
       foreach($schedules_today as $sched){
            if($sched->schedtype_id==='1' || $sched->schedtype_id==='4' || $sched->schedtype_id==='5'){
                $shift = $this->empshifttime_fetch($sched->sched_id)[0];
                $sched->time_start = $shift->time_start;
                $sched->time_end = $shift->time_end;
            }else{
                $sched->time_start = NULL;
                $sched->time_end = NULL;
            }
           
        }
        foreach($schedules_tomorrow as $sched){
            if($sched->schedtype_id==='1' || $sched->schedtype_id==='4' || $sched->schedtype_id==='5'){
                $shift = $this->empshifttime_fetch($sched->sched_id)[0];
                $sched->time_start = $shift->time_start;
                $sched->time_end = $shift->time_end;
            }else{
                $sched->time_start = NULL;
                $sched->time_end = NULL;
            }
        }
        echo json_encode(['today'=>$schedules_today,'tomorrow'=>$schedules_tomorrow]);
    }
    
       
}
