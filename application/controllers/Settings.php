<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once dirname(__FILE__) . "/General.php";

class Settings extends General
{

    protected $title = array('title' => 'Settings');

    public function __construct()
    {
        parent::__construct();
    }

    public function user_access_rights()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/settings/user_access_rights', $data);
        }
    }

    public function getMenuTabs()
    {
        $data = $this->general_model->fetch_all("tab_id,tab_name,icon", "tbl_menu_tab_item", "tab_name ASC");
        echo json_encode(array('tabs' => $data));
    }

    public function getMenuItems()
    {

        $searchLinkType = $this->input->post('searchLinkType');
        $searchSystemType = $this->input->post('searchSystemType');
        $search = array();
        $searchString = "";
        if ($searchLinkType !== '' && $searchLinkType !== null) {
            $search[] = "a.tab_id = $searchLinkType";
        }
        if ($searchSystemType !== '') {
            $search[] = "b.isNewTemplate = $searchSystemType";
        }
        if (!empty($search)) {
            $searchString = " WHERE " . implode(" AND ", $search);
        }
        $menu_items = $this->general_model->custom_query("SELECT * FROM tbl_menu_tab_item a INNER JOIN tbl_menu_items b ON a.tab_id=b.tab_id $searchString ORDER BY tab_name ASC,item_title ASC");
        echo json_encode($menu_items);
    }

    public function setUserAccess_supervisor() 
    {
        $type = $this->input->post('type');
        $uid = $this->input->post('uid');
        if ($type == 'tl') {
            $menu_item_ids = [
                18, //kudos_all
                48, //kudos_add
                83, //leave/approval
                179, //leave/supervisor_list
                94, //approval_ahr
                98, //dtrmonitor
                99, //dtr/request
                101, //schedule_logs
                104, //schedule/manual
                105, //schedule/excel
                174, //schedule/settings
                189, //schedule/intraday
                190, //employee/contacts
                178 //discipline/dms_supervision

            ];
        } else if ($type == 'admin') {
            $menu_item_ids = [
                83, //leave/approval
                179, //leave/supervisor_list
                94, //approval_ahr
                98, //dtrmonitor
                99, //dtr/request
                101, //schedule_logs
                104, //schedule/manual
                105, //schedule/excel
                174, //schedule/settings
                190, //employee/contacts
                178 //discipline/dms_supervision
            ];
        }
        $exist_useraccess_id = [];
        $data = $this->general_model->custom_query("SELECT useraccess_id,menu_item_id FROM tbl_user_access WHERE user_id=$uid AND menu_item_id IN (" . implode($menu_item_ids, ',') . ")");

        $final_data = array();
        foreach ($data as $d) {
            foreach ($menu_item_ids as $key => $menu_item_id) {
                if ((int) $d->menu_item_id == (int) $menu_item_id) {
                    unset($menu_item_ids[$key]);
                }
            }
            $exist_useraccess_id[] = $d->useraccess_id;
        }
        $queries_insert = [];
        foreach ($menu_item_ids as $menu_item_id) {
            $queries_insert[] = array('user_id' => $uid, 'menu_item_id' => $menu_item_id, 'is_assign' => 1, 'created_by' => $this->session->userdata('uid'), 'updated_by' => $this->session->userdata('uid'));
        }

        foreach ($exist_useraccess_id as $useraccess_id) {
            $datax = array('is_assign' => 1, 'updated_by' => $this->session->userdata('uid'));
            $queries_update[] = array('data' => $datax, 'where' => "useraccess_id=$useraccess_id");
        }
        $res1 = 1;
        $res2 = 1;
        if(!empty($queries_insert)){
            $res1 = $this->general_model->insert_array_vals($queries_insert, 'tbl_user_access');
        }
        if(!empty($queries_update)){
            $res2 = $this->general_model->update_array_vals($queries_update, 'tbl_user_access');
        }
        

        $status = ($res1 && $res2) ? 1 : 0;
        echo $status;
    }

    public function setUserAccess()
    {
        $menu_item_id = $this->input->post('menu_item_id');
        $uid = $this->input->post('uid');
        $status = $this->input->post('status');
        if ($status == 'true') { //ADD TO DB
            $res = $this->general_model->insert_vals(array('user_id' => $uid, 'menu_item_id' => $menu_item_id, 'is_assign' => 1, 'created_by' => $this->session->userdata('uid'), 'updated_by' => $this->session->userdata('uid')), 'tbl_user_access');
        } else { //DELETE TO DB
            $res = $this->general_model->delete_vals("user_id=$uid AND menu_item_id=$menu_item_id", 'tbl_user_access');
        }
        echo json_encode(array('status' => $res));
    }

    public function getUserAccess()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $searchClass = $this->input->post('searchClass');
        $searchName = $this->input->post('searchName');
        $search = array();
        $searchString = "";
        if ($searchClass !== '') {
            $search[] = "d.acc_description = '$searchClass'";
        }
        if ($searchName !== '') {
            $search[] = "(a.fname LIKE \"%$searchName%\" OR a.lname LIKE \"%$searchName%\")";
        }
        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT a.fname,a.lname,a.mname,a.apid,b.emp_id,c.uid FROM tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d WHERE b.acc_id=d.acc_id AND a.apid=b.apid AND b.emp_id=c.emp_id AND b.isActive='yes' $searchString ORDER BY a.lname ASC";
        $employees = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");

        foreach ($employees as $emp) {
            $user_access = $this->general_model->fetch_specific_vals("useraccess_id,menu_item_id,is_assign", "is_assign=1 AND user_id=" . $emp->uid, "tbl_user_access");
            $emp->data = $user_access;
        }
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $employees, 'total' => $count));
    }

    public function tab_menu()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/settings/tab_menu', $data);
        }
    }

    public function updateTabMenu()
    {
        $tab_id = $this->input->post('tab_id');
        $tab_name = $this->input->post('tab_name');
        $icon = $this->input->post('icon');
        $res = $this->general_model->update_vals(['tab_name' => $tab_name, 'icon' => $icon], "tab_id = $tab_id", "tbl_menu_tab_item");
        echo json_encode(['status' => ($res) ? 'Success' : 'Failed']);
    }

    public function createTabMenu()
    {
        $tab_name = $this->input->post('tab_name');
        $icon = $this->input->post('icon');
        $res = $this->general_model->insert_vals(['tab_name' => $tab_name, 'icon' => $icon], "tbl_menu_tab_item");
        echo json_encode(['status' => ($res) ? 'Success' : 'Failed']);
    }

    public function getMenuItem_single()
    {
        $menu_item_id = $this->input->post('menu_item_id');
        $data = $this->general_model->fetch_specific_val("*", "menu_item_id=$menu_item_id", "tbl_menu_items");
        echo json_encode(['menu_item' => $data]);
    }

    public function createMenuLink()
    {
        $item_title = $this->input->post('item_title');
        $item_link = $this->input->post('item_link');
        $tab_id = $this->input->post('tab_id');
        $isNewTemplate = $this->input->post('isNewTemplate');
        $isSidebar = $this->input->post('isSidebar');
        $res = $this->general_model->insert_vals(['item_title' => $item_title, 'item_link' => $item_link, 'tab_id' => $tab_id, 'isNewTemplate' => $isNewTemplate, 'isSidebar' => $isSidebar], "tbl_menu_items");
        echo json_encode(['status' => ($res) ? 'Success' : 'Failed']);
    }

    public function updateMenuLink()
    {
        $menu_item_id = $this->input->post('menu_item_id');
        $item_title = $this->input->post('item_title');
        $item_link = $this->input->post('item_link');
        $tab_id = $this->input->post('tab_id');
        $isNewTemplate = $this->input->post('isNewTemplate');
        $isSidebar = $this->input->post('isSidebar');
        $res = $this->general_model->update_vals(['item_title' => $item_title, 'item_link' => $item_link, 'tab_id' => $tab_id, 'isNewTemplate' => $isNewTemplate, 'isSidebar' => $isSidebar], "menu_item_id=$menu_item_id", "tbl_menu_items");
        echo json_encode(['status' => ($res) ? 'Success' : 'Failed']);
    }

    public function deleteMenuLink()
    {
        $menu_item_id = $this->input->post('menu_item_id');
        $res = $this->general_model->delete_vals("menu_item_id=$menu_item_id", "tbl_menu_items");
        echo json_encode(['status' => ($res) ? 'Success' : 'Failed']);
    }
}
