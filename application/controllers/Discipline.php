 <?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Discipline extends General
{
    public $logo = "/var/www/html/sz/assets/images/img/logo2.png";
    // public $logo = "C:\Users\SDT-Programmer\Sites\sz\assets\images\img\logo2.png";
    public function dms_settings()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'COD Settings',
        ];
        if($this->check_access()){
        $this->load_template_view('templates/dms/dms_settings', $data);
        }        
    }

    public function dms_supervision()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'DMS Supervision',
        ];
        if($this->check_access()){
        $this->load_template_view('templates/dms/dms_supervision', $data);
        }        
    }

    public function dms_monitoring()
    {
        $emp_ids = [1783, 44, 41, 43, 97, 54, 1357, 2135, 1755, 286, 1977, 46, 168, 27];
        $class = ['All', 'Admin', 'Agent'];
        $emp_id = $this->session->userdata('emp_id');
        $main_nav = '<ul class="dms m-nav m-nav--inline">
                        <li class="m-nav__item active">
                            <a href="#" class="dmsMonitoringMainNav m-nav__link active" data-navcontent="dms_dashboard">
                                <i class="m-nav__link-icon fa fa-dashboard"></i>
                                <span class="m-nav__link-text">Dashboard</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="dmsMonitoringMainNav m-nav__link" data-navcontent="dtrViolationsMonitoring">
                                <i class="m-nav__link-icon fa fa-clock-o"></i>
                                <span class="m-nav__link-text">DTR Violations Monitoring</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" id="monitoring_records_button" class="dmsMonitoringMainNav m-nav__link"
                                data-navcontent="incidentReportMonitoring">
                                <i class="m-nav__link-icon fa fa-gears"></i>
                                <span class="m-nav__link-text">Incident Report Monitoring</span>
                                <!-- <span class="m-nav__link-badge">
                                    <span class="m-badge m-badge--primary m-badge--wide">23</span>
                                </span> -->
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" id="eroPendingIR_tabbutton" class="dmsMonitoringMainNav m-nav__link"
                                data-navcontent="eroPendingIR">
                                <i class="m-nav__link-icon fa fa-tasks"></i>
                                <span class="m-nav__link-text">ERO Tasks</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="dmsMonitoringMainNav m-nav__link" data-navcontent="monitoringIrProfile"
                                id="monitoringIrProfileContent">
                                <i class="m-nav__link-icon fa fa-user-circle-o"></i>
                                <span class="m-nav__link-text">IR Profile</span>
                            </a>
                        </li>
                    </ul>';
        if(in_array($emp_id, $emp_ids)){
            $main_nav = '<ul class="dms m-nav m-nav--inline">
                            <li class="m-nav__item active">
                                <a href="#" id="monitoring_records_button" class="dmsMonitoringMainNav m-nav__link active"
                                    data-navcontent="incidentReportMonitoring">
                                    <i class="m-nav__link-icon fa fa-gears"></i>
                                    <span class="m-nav__link-text">Incident Report Monitoring</span>
                                    <!-- <span class="m-nav__link-badge">
                                        <span class="m-badge m-badge--primary m-badge--wide">23</span>
                                    </span> -->
                                </a>
                            </li>
                        </ul>';
            $class = ['Agent'];
        }
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'DMS Monitoring',
            'main_nav' => $main_nav,
            'class' => implode(',',$class)
        ];
        if($this->check_access()){
            $this->load_template_view('templates/dms/dms_monitoring', $data);
        }        
    }
    
    public function dms_ero()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'DMS Employee Relations Officer',
        ];
        if($this->check_access()){
        $this->load_template_view('templates/dms/dms_ero', $data);
        }        
    }

    public function dms_personal()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'DMS Personal',
        ];
        // if($this->check_access()){
        $this->load_template_view('templates/dms/dms_personal', $data);
        // }        
    }

    public function dms_cod_view()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'DMS COD & DTR Violation Rules',
        ];
        // if($this->check_access()){
        $this->load_template_view('templates/dms/cod_view', $data);
        // }        
    }

    public function dms_add_history()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Add Latest IR Record',
        ];
        if($this->check_access()){
        $this->load_template_view('templates/dms/dms_add_history', $data);
        }        
    }


    // CREATE -----------------------------------------------------------------------------------
    public function add_disciplinary_action()
    {
        $data['action'] = $this->input->post("action");
        $data['abbreviation'] = $this->input->post("abbr");
        $data['periodMonth'] = $this->input->post("period");
        $data['changedBy'] = $this->session->userdata('emp_id');
        if ($this->input->post('isSuspension') == 'true') {
            $data['label'] = 's';
        } else {
            $data['label'] = 'w';
        }
        // ////var_dump($data);
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_dms_disciplinary_action');
        echo json_encode($insert_stat);
    }

    public function add_category()
    {
        $data['category'] = strtolower($this->input->post("category"));
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_dms_discipline_category');
        echo json_encode($insert_stat);
    }

    public function add_offense_level()
    {
        $disciplinary_action_id = $this->input->post("disciplinaryActionId");
        $this->create_new_disc_action_category();
        $category_settings = $this->get_current_disciplinary_action_category_settings();
        $max_level = 0;
        $max_offense_level = $this->get_max_offense_level($this->input->post("categoryId"), $category_settings->disciplinaryActionCategorySettings_ID);
        if ($max_offense_level->level !== NULL) {
            $max_level = $max_offense_level->level;
        }
        $data['disciplineCategory_ID'] = $this->input->post("categoryId");
        $data['disciplinaryAction_ID'] = $this->input->post("disciplinaryActionId");
        $data['disciplinaryActionCategorySettings_ID'] =  $category_settings->disciplinaryActionCategorySettings_ID;
        $data['level'] = $max_level + 1;
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_dms_disciplinary_action_category');
        echo json_encode($insert_stat);
    }

    public function add_offense_category()
    {
        $data['letter'] = strtolower($this->input->post("categoryLetter"));
        $data['offenseType'] = $this->input->post("offenseCategory");
        $data['changedBy'] = $this->session->userdata('emp_id');
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_dms_offense_type');
        echo json_encode($insert_stat);
    }

    public function add_offense()
    {
        if ($this->check_if_order_num_exist($this->input->post("offenseNum"), $this->input->post("offenseCategoryId"))) {
            $this->insert_num_order($this->input->post("offenseNum") - 1, $this->input->post("offenseCategoryId"));
        }
        $data['orderNum'] = $this->input->post("offenseNum");
        $data['offenseType_ID'] = $this->input->post("offenseCategoryId");
        // $data['disciplineCategory_ID'] = $this->input->post("disciplineCategoryId");
        $data['offense'] = $this->input->post("offense");
        $data['multiCategory'] = $this->input->post("multiCateg");
        $data['multiCategoryLetters'] = $this->input->post("disciplineCategoryLetter");
        $data['changedBy'] = $this->session->userdata('emp_id');
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_dms_offense');
        echo json_encode($insert_stat);
    }

    public function add_default_violation_settings($dtr_violation_type_id)
    {
        $offense = $this->get_offense_for_dtr_vio_add_settings();
        // if(count($offense) > 0){
        //     $data['offense_ID'] = $offense->offense_ID;
        // }
        $data['dtrViolationType_ID'] = $dtr_violation_type_id;
        $data['changedBy'] = $this->session->userdata('emp_id');
        return $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dms_dtr_violation_type_settings');
    }

    public function add_default_ir_deadline_settings()
    {
        $data['deadlineToExplain'] = 1;
        $data['deadlineToExplainOption'] = 1;
        $data['deadlineToRecommend'] = 1;
        $data['deadlineToRecommendOption'] = 1;
        $data['deadlineLastRecommendation'] = 1;
        $data['deadlineFinalDecision'] = 1;
        $data['deadlineUploadDocs'] = 1;
        $data['changedBy'] = $this->session->userdata('emp_id');
        return $this->general_model->insert_vals($data, 'tbl_dms_incident_report_deadline_settings');
    }

    public function assign_qip()
    {
        $qip_emp_list = $this->input->post('qipEmpList');
        for ($loop = 0; $loop < count($qip_emp_list); $loop++) {
            $data[$loop] = [
                'emp_id' => $qip_emp_list[$loop],
                'createdBy' => $this->session->userdata('emp_id')
            ];
        }
        $insert_stat = $this->general_model->batch_insert($data, 'tbl_dms_qip_assignment');
        echo json_encode($insert_stat);
    }

    public function assign_super_ero($emp_id)
    {
        $assigned_with_sup = $this->check_if_assigned_with_sup($emp_id);
        if (count($assigned_with_sup) > 0) {
            $assign_sup = 2;
        } else {
            $user_details = $this->get_user_by_emp_id($emp_id);
            if (count($user_details) > 0) {
                $sup_user_details = $this->get_direct_supervisor_emp($user_details->uid);
                $data['employeeRelationsSupervisor'] = $sup_user_details->Supervisor;
                $data['emp_id'] = $emp_id;
                $assign_sup = $this->general_model->insert_vals($data, 'tbl_dms_employee_relations_supervisor');
            } else {
                $assign_sup = 0;
            }
        }
    }

    public function assign_ero()
    {
        $account_ids = $this->input->post('accountIds');
        $site_id = $this->input->post('siteId');
        $emp_id = $this->input->post('empId');
        $insert_stat['assign_ero_sup'] = $this->assign_super_ero($emp_id);
        for ($loop = 0; $loop < count($account_ids); $loop++) {
            $data[$loop] = [
                'acc_id' => $account_ids[$loop],
                'site_ID' => $site_id,
                'emp_ID' => $emp_id,
                'createdBy' => $this->session->userdata('emp_id'),
            ];
        }
        $insert_stat['assign_ero'] = $this->general_model->batch_insert($data, 'tbl_dms_employee_relations_officer');
        echo json_encode($insert_stat);
    }

    public function assign_offense_type()
    {
        $acc_id = $this->input->post('accId');
        $offense_type_id = $this->input->post('offenseTypeId');
        for ($loop = 0; $loop < count($offense_type_id); $loop++) {
            $data[$loop] = [
                'offenseType_ID' => $offense_type_id[$loop],
                'acc_id' => $acc_id,
                'createdBy' => $this->session->userdata('emp_id'),
            ];
        }
        $insert_stat['assign_offense_type'] = $this->general_model->batch_insert($data, 'tbl_dms_offense_type_assignment');
        echo json_encode($insert_stat);
    }

    public function assign_recommendation_number()
    {
        $data['account_ID'] = $this->input->post("accId");
        $data['number'] = $this->input->post("number");
        $data['changedBy'] = $this->session->userdata('emp_id');
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_dms_account_recommendation_number');
        echo json_encode($insert_stat);
    }

    public function assign_recommendation_change_to($dms_cus_rec_id, $assign_to)
    {
        $data['dmsCustomRecommendation_ID'] = $dms_cus_rec_id;
        $data['emp_id'] = $assign_to;
        return $this->general_model->insert_vals($data, 'tbl_dms_custom_recommendation_change');
    }
    public function assign_recommendation_change()
    {
        $data['account_ID'] = $this->input->post('accId');
        $data['level'] = $this->input->post('level');
        $data['emp_id'] = $this->input->post('recommender');
        $data['createdBy'] = $this->session->userdata('emp_id');
        $data['changeType'] = $this->input->post('changeType');
        $assign_to = $this->input->post('assignTo');
        $changeType = $this->input->post('changeType');
        if ($changeType == "change") {
            $dms_cus_rec_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dms_custom_recommendation');
            $assign['assign_stat'] = $this->assign_recommendation_change_to($dms_cus_rec_id, $assign_to);
        } else {
            $assign['assign_stat'] = $this->general_model->insert_vals($data, 'tbl_dms_custom_recommendation');
        }
        echo json_encode($assign);
    }

    private function create_disc_action_category_settings()
    {
        $data['status_ID'] = 10;
        $data['changedBy'] = $this->session->userdata('emp_id');
        return $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dms_disciplinary_action_category_settings');
    }

    private function add_new_disc_action_category_settings()
    {
        $deactivate_categ_settings_stat = $this->deactivate_old_disc_action_category_settings();
        if ($deactivate_categ_settings_stat) {
            return $this->create_disc_action_category_settings();
        } else {
            return 0;
        }
    }
    public function create_new_disc_action_category()
    {
        $check_categ = $this->check_if_category_settings_is_used_by_ir();
        // var_dump($check_categ);
        if ($check_categ['exist']) { // CHECK IF USED BY IR
            //    fetch all current settings and 
            $disc_action_categ = $this->get_all_disc_action_category();
            $act_categ_settings_id = $this->add_new_disc_action_category_settings();
            $action_categ_arr = [];
            foreach ($disc_action_categ as $disc_action_categ_index => $disc_action_categ_row) {
                $disc_action_categ_row->{'disciplinaryActionCategorySettings_ID'} = $act_categ_settings_id;
                $action_categ_arr[$disc_action_categ_index] = $disc_action_categ_row;
            }
            if (count($action_categ_arr) > 0) {
                $disc_action_categ_json  = json_encode($action_categ_arr);
                $disc_action_categ_arr = json_decode($disc_action_categ_json, true);
                $insert_stat = $this->general_model->batch_insert($disc_action_categ_arr, 'tbl_dms_disciplinary_action_category');
            }
        }
    }

    private function create_individual_deadline($record_id, $emp_id, $deadline, $deadline_type_id, $level)
    {
        $data['record_ID'] = $record_id;
        $data['emp_id'] = $emp_id;
        $data['deadline'] = $deadline;
        $data['dmsDeadlineType_ID'] = $deadline_type_id;
        $data['level'] = $level;
        return $this->general_model->insert_vals($data, 'tbl_dms_deadline');
    }

    private function notify_subject($ir_id, $subj_empid, $subj_explanation)
    {
        $deadline_notif = "";
        if ($subj_explanation['with_deadline']) {
            $deadline_notif = "Your deadline to explain is until " . date("g:i A", strtotime($subj_explanation['deadline'])) . ", " . date("M j, Y", strtotime($subj_explanation['deadline']));
        }
        $incident_report = $this->get_ir($ir_id);
        $supervisor_empid = $this->get_direct_supervisor_emp_id($subj_empid);

        $sup_det = $this->get_emp_details_via_emp_id($supervisor_empid->Supervisor);
        $subj_det = $this->get_emp_details_via_emp_id($subj_empid);
        $notif_mssg = "You currently have an ongoing Incident Report. Please approach <b>" . ucwords($sup_det->fname . " " . $sup_det->lname) . "</b> and give your IR-ID " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " for your explanation. " . $deadline_notif . ".<br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
        $link = "discipline/dms_personal";
        $data['notifsubj_id'] = [$this->simple_system_notification($notif_mssg, $link, $subj_det->uid, 'dms')];
        return $data;
    }
    private function notify_witness($ir_id, $witnesses)
    {
        $incident_report = $this->get_ir($ir_id);
        //var_dump($incident_report);
        $recipient = [];
        for ($loop = 0; $loop < count($witnesses); $loop++) {
            $emp_details = $this->get_emp_details_via_emp_id($witnesses[$loop]);
            $recipient[$loop] = [
                "userId" => $emp_details->uid
            ];
        }
        $notif_mssg = "You are added as witness to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ".</b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
        $link = "discipline/witness_confirmation/$ir_id";
        $notif_id[0] = $this->create_system_notif($notif_mssg, $link, 'dms');
        $data['notif_id'] = $notif_id;
        $data['send_stat'] = $this->send_system_notif($notif_id[0], $recipient);
        return $data;
    }

    protected function check_witness_confirmation($ir_id)
    {
        $witnesses = $this->qry_witnesses_by_stat($ir_id, 2);
        if (count($witnesses) < 1) {
            $dateTime = $this->get_current_date_time();
            $ir = $this->get_ir($ir_id);
            if ((int) $ir->liabilityStat !== 27) {
                $subj_explanation = $this->subject_explanation_deadline($ir_id, $ir->subjectEmp_ID, $dateTime['dateTime']);
                $this->notify_subject($ir_id, $ir->subjectEmp_ID, $subj_explanation);
            }
        }
    }

    public function subject_explanation_deadline($ir_id, $subj_emp_id, $dateTime)
    {
        $data['with_deadline'] = 0;
        $rec_deadline = $this->get_ir_deadline_settings_details_recorded();

        $deadline_explain_option = (int) $rec_deadline['record']->deadlineToExplainOption;
        $days_to_explain = (int) $rec_deadline['record']->deadlineToExplain;
        $deadline_date = Date("Y-m-d", strtotime($dateTime . $days_to_explain . ' days'));
        $deadline_date_time = $deadline_date . " 23:59:59";
        // if ($deadline_explain_option == 1) {
        $data['with_deadline'] = 1;
        $data['deadline'] = $deadline_date_time;
        $this->set_dms_deadline($subj_emp_id, $ir_id, $days_to_explain, 2, 1);
        // }
        return $data;
    }

    private function insert_witnesses($ir_id, $witnesses)
    {
        for ($loop = 0; $loop < count($witnesses); $loop++) {
            $data[$loop] = [
                'incidentReport_ID' => $ir_id,
                'witnessEmp_ID' => $witnesses[$loop]
            ];
        }
        return $this->general_model->batch_insert($data, 'tbl_dms_incident_report_witness');
    }

    private function insert_incident_report($data, $witnesses, $evidences)
    {
        $dateTime = $this->get_current_date_time();
        // session source
        $data['sourceEmp_ID'] = $this->session->userdata('emp_id');
        $data['dateTimeFiled'] = $dateTime['dateTime'];
        $ir_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dms_incident_report');

        if ($witnesses[0] !== "") {
            $add['insert_witnesses'] = $this->insert_witnesses($ir_id, $witnesses);
            $add['notify_witnesses'] = $this->notify_witness($ir_id, $witnesses);
        } else {
            if($data['liabilityStat'] !== '27'){
                $subj_explanation = $this->subject_explanation_deadline($ir_id, $data['subjectEmp_ID'], $dateTime['dateTime']);
                $add['notify_subject'] = $this->notify_subject($ir_id, $data['subjectEmp_ID'], $subj_explanation);
            }
        }

        if ($evidences['name']['0'] !== "") {
            $this->upload_evidences($ir_id, $evidences, $dateTime['dateTime'], 1);
        }
        // set deadline for subject to explain

        $add['incident_report_id'] = $ir_id;
        return $add;
    }
    public function upload_evidences($ir_id, $evidences, $dateTime, $purposeid)
    {
        $path = 'uploads/dms/evidences/';
        for ($loop = 0; $loop < count($evidences['name']); $loop++) {
            // $emp_details = $this->get_emp_details_via_emp_id($evidences[$loop]);
            $size = $evidences['size'][$loop];
            $temp = $evidences['tmp_name'][$loop];
            $name = $evidences['name'][$loop];
            $final_name = pathinfo($name, PATHINFO_FILENAME) . time() . "." . pathinfo($name, PATHINFO_EXTENSION);

            $new_path = $path . strtolower($final_name);
            if (move_uploaded_file($temp, $new_path)) {
                $data[$loop] = [
                    "link" => $new_path,
                    "mediaType" => $evidences['type'][$loop],
                    "incidentReportAttachmentPurpose_ID" => $purposeid,
                    "incidentReport_ID" => $ir_id,
                    "dateUploaded" => $dateTime
                ];
            }
        }
        $this->general_model->batch_insert($data, 'tbl_dms_incident_report_attachment');
    }


    private function add_auto_ir_related_details($ir_id, $qualified_dtr_vio, $supervisors_note)
    {
        $data['incidentReport_ID'] = $ir_id;
        $data['qualifiedUserDtrViolation_ID'] = $qualified_dtr_vio;
        $data['supervisorsNote'] = $supervisors_note;
        $data['directSupEmpId'] = $this->session->userdata('emp_id');
        return $this->general_model->insert_vals($data, 'tbl_dms_auto_ir_related_details');
    }

    private function default_termination()
    {
        $data['action'] = "Termination";
        $data['periodMonth'] = 1;
        $data['abbreviation'] = "TERM";
        $data['label'] = 't';
        $data['changedBy'] = 316;
        return $this->general_model->insert_vals($data, 'tbl_dms_disciplinary_action');
    }

    public function file_ir_qualified_dtr_vio()
    {
        $add_ir['recheck_pending'] = 1;
        $recheck_filing = $this->recheck_ir_filing($this->input->post('empId'),  $this->input->post('offenseId'));
        if ($recheck_filing) {
            $add_ir['recheck_pending'] = 1;
        } else {
            $add_ir['recheck_pending'] = 0;
            $qualified_id = $this->input->post('qualifiedId');
            $data['incidentDate'] = $this->input->post('incidentDate');
            $data['incidentTime'] = $this->input->post('incidentTime');
            $data['place'] = 'Support Zebra';
            $data['expectedAction'] = $this->input->post('expectedAction');
            $data['subjectEmp_ID'] = $this->input->post('empId');
            $data['subjectPosition'] = $this->input->post('position');
            $data['subjectEmpStat'] = $this->input->post('empStat');
            $data['subjectAccount'] = $this->input->post('account');
            $data['acc_id'] = $this->input->post('accountId');
            $data['empType'] = $this->input->post('empType');
            $data['details'] = $this->input->post('details');
            $data['prescriptionEnd'] = $this->input->post('cureDate');
            $data['periodNum'] = $this->input->post('periodNum');
            $data['periodUnit'] = $this->input->post('periodunit');
            $data['prescriptiveStat'] = 2;
            $data['liabilityStat'] = 2;
            $data['prescriptiveId'] = $this->input->post('prescriptiveId');
            $data['occurence'] = $this->input->post('occurence');
            $data['offense_ID'] = $this->input->post('offenseId');
            $data['disciplinaryActionCategory_ID'] = $this->input->post('discActionCategId');
            $data['finaldisciplinaryActionCategory_ID'] = $this->input->post('finalDiscActionCategId');
            $data['disciplinaryActionCategorySettings_ID'] = $this->input->post('discActioncategSettingsId');
            $data['offense'] = $this->input->post('offense');
            $data['incidentReportFilingType_ID'] = 2;
            $data['incidentReportType_ID'] = 1;
            $data['incidentReportStages_ID'] = 1;
            // ////var_dump($data);
            $witnesses =  explode(",", $this->input->post('witnesses'));
            $evidences = $_FILES['upload_evidence_files_qualified'];
            // var_dump($witnesses);
            // var_dump($evidences);

            /* insert IR */
            $ir = $this->insert_incident_report($data,  $witnesses, $evidences);
            if ($witnesses[0] !== "") {
                $add_ir['with_witness'] = 1;
                $add_ir['notify_wit'] = $ir['notify_witnesses'];
            } else {
                $add_ir['with_witness'] = 0;
                $add_ir['notify_sub'] = $ir['notify_subject'];
            }
            $add_ir['add_ir'] = $ir;
            // add auto ir details
            $add_ir['add_auto_ir_related_stat'] = $this->add_auto_ir_related_details($ir['incident_report_id'], $this->input->post('qualifiedId'), $this->input->post('supervisorsNote'));
            //  set qualified to complete
            $update_status['status_ID'] = 3;
            $add_ir['set_qualified_to_complete'] = $this->update_qualified_dtr_vio($this->input->post('qualifiedId'), $update_status);
            // set supervisor notification to complete
            $add_ir['set_supervisor_notif_to_complete'] = $this->update_supervisor_qualified_notification($this->input->post('qualifiedId'), $this->session->userdata('emp_id'), $update_status);
            //  set deadline to complete
            $add_ir['set_deadline_to_complete'] = $this->update_dms_deadline_with_type($this->input->post('qualifiedId'), $this->session->userdata('emp_id'), 1, $update_status);
        }
        echo json_encode($add_ir);
    }

    protected function add_dismiss_record($data)
    {
        return $this->general_model->insert_vals($data, 'tbl_dms_dismiss_qualified_dtr_violation');
    }

    protected function reset_user_dtr_related_statuses($user_dtr_vio_id)
    {
        // reset_status
        $related_arr = [];
        $related_details = $this->get_user_dtr_violation_related_details($user_dtr_vio_id);
        if (count($related_details) > 0) {
            foreach ($related_details as $related_index => $related_row) {
                $related_arr[$related_index] = [
                    'userDtrViolation_ID' => $related_row->relatedUserDtrViolation_ID,
                    'status_ID' => 2
                ];
            }
            $this->general_model->batch_update($related_arr, 'userDtrViolation_ID', 'tbl_dms_user_dtr_violation_related_details');
            // remove user dtr_violation 
            // $this->remove_user_dtr_violation_related_details($user_dtr_vio_id);
        }
    }

    public function dismiss_qualified_dtr_violation()
    {
        $dismiss['dismissable'] = 0;
        $dismiss['dismiss_status'] = 0;

        $qualified_violation_id = $this->input->post('qualifiedViolationId');
        $dateTime = $this->get_current_date_time();
        $qualified_record = $this->get_qualified_dtr_violation($qualified_violation_id); // qualified user dtr violation detials
        $user_dtr_vio = $this->get_user_dtr_violation($qualified_record->userDtrViolation_ID); // user dtr violation details
        $violation_rules = $this->get_dtr_violation_settings($user_dtr_vio->dtrViolationType_ID); // dtr violation type settings details
        // if ((int) $violation_rules->deadlineOption == 2) { // check if dismissable
            // add dismiss record
            $dismiss['dismissable'] = 1;
            $data['dateTime'] = $dateTime['dateTime'];
            $data['qualifiedUserDtrViolation_ID'] = $qualified_violation_id;
            $data['reason'] = $this->input->post('reason');
            $data['emp_id'] = $this->session->userdata('emp_id');
            $add_dismiss_stat = $this->add_dismiss_record($data);
            // set qualified record to dismiss
            $update_stat['status_ID'] = 20;
            $set_qualified_to_dismiss = $this->update_qualified_dtr_vio($qualified_violation_id, $update_stat);
            // var_dump($set_qualified_to_dismiss);
            // set superviosr notif to dismiss
            $set_supervisor_notification_to_dismiss = $this->update_supervisor_qualified_notification($qualified_violation_id, $this->session->userdata('emp_id'), $update_stat);
            // set deadline to dismiss
            $this->update_dms_deadline_with_type($qualified_violation_id, $this->session->userdata('emp_id'), 1, $update_stat);
            // set user dtr violation to dismiss
            $set_user_dtr_violation_stat = $this->update_user_dtr_violation($qualified_record->userDtrViolation_ID, $update_stat);
            // check on hold 
            $dismiss['check_on_hold_qualified_user_dtr_violation'] = $this->access_on_hold_during_dismmissal($qualified_violation_id);
            //  reset status of related details
            $this->reset_user_dtr_related_statuses($qualified_record->userDtrViolation_ID);
            if (($add_dismiss_stat == 1) && ($set_qualified_to_dismiss == 1) && ($set_supervisor_notification_to_dismiss == 1) && ($set_user_dtr_violation_stat == 1)) {
                $dismiss['dismiss_status'] = 1;
            }
        // }
        echo json_encode($dismiss);
    }

    public function test_on_hold_on_dismiss(){
        $this->access_on_hold_during_dismmissal(19);
    }

    protected function insert_ir_history($data){
        $ir_history_exist = $this->check_if_ir_history_exist($data['subjectEmp_ID'], $data['offense_ID']);
        if(count($ir_history_exist) > 0){
            return 2;
        }else{
            $ir['incidentReport_ID'] =  $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dms_incident_report');
            return $this->general_model->insert_vals($ir, 'tbl_dms_added_ir_history');
        }
    }

    protected function check_if_ir_history_exist($subject_emp_id, $offense_id){
        $fields = "incidentReport_ID";
        $where = "subjectEmp_ID = $subject_emp_id AND offense_ID = $offense_id";
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_incident_report");
    }

    public function get_ir_approvers($ir_ids){
        $fields = "incidentReportRecommendation_ID, emp_ID, liabilityStat_ID, level, incidentReport_ID, datedLiabilityStat";
        $where = "incidentReport_ID IN ($ir_ids)";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_dms_incident_report_recommendation");
    }

    public function add_ir_history(){
        $dateTime = $this->get_current_date_time();
        $incident_date_time = new DateTime($dateTime['dateTime']);
        $emp_details = $this->get_emp_details_via_emp_id($this->input->post("empId"));
        $cure_date_str = strtotime($this->input->post("cureDate"));
        $curr_date = strtotime($dateTime['date']);
        $diff = date_diff(date_create($this->input->post("incidentDate")), date_create($this->input->post("cureDate")));
        if ($cure_date_str > $curr_date){
            $data['prescriptiveStat'] = 3;
        }else{
            $data['prescriptiveStat'] = 4;
        }
        $data['subjectEmp_ID'] = $this->input->post("empId");
        $data['incidentDate'] = date_format(date_create($this->input->post("incidentDate")), 'Y-m-d');
        $data['incidentTime'] = $incident_date_time->format("H:i");
        $data['place'] = "Support Zebra";
        $data['expectedAction'] = "This is an added IR";
        $data['sourceEmp_ID'] = $this->session->userdata('emp_id');
        $data['subjectPosition'] = $emp_details->positionDescription;
        $data['subjectEmpStat'] = $emp_details->empStatus;
        $data['subjectAccount'] = $emp_details->accountDescription;
        $data['acc_id'] = $emp_details->acc_id;
        $data['empType'] = $emp_details->empType;
        $data['details'] = $this->input->post("incidentDescription");
        $data['dateTimeFiled'] = $dateTime['dateTime'];
        $data['offense_ID'] = $this->input->post("offenseId");
        $data['offense'] = $this->input->post("offense");
        $data['disciplinaryActionCategory_ID'] = $this->input->post("discActionCateg");
        $data['finaldisciplinaryActionCategory_ID'] = $this->input->post("discActionCateg");
        $data['prescriptionEnd'] = date_format(date_create($this->input->post("cureDate")), 'Y-m-d');
        $data['periodNum'] = $diff->m;
        $data['periodUnit'] = "month";
        $data['liabilityStat'] = 17;
        $data['datedLiabilityStat'] = $dateTime['dateTime'];
        $data['explanationDate'] = $dateTime['dateTime'];
        $data['subjectExplanationStat'] = 3;
        $data['prescriptiveId'] = $this->qry_max_prescriptive_id() + 1;
        $data['occurence'] = 1;
        $data['disciplinaryActionCategorySettings_ID'] = $this->input->post('discActionCategSettingsId');
        $data['incidentReportFilingType_ID'] = 1;
        $data['incidentReportType_ID'] = 1;
        $data['incidentReportStages_ID'] = 3;
        $data['deadlineToRecommendOption'] = 1;
        $ir_history['insert_stat'] = $this->insert_ir_history($data);
        echo json_encode($ir_history);
    }

    // public function test_time(){
    //     $shift_in= strtotime("2019-02-2 12:00 AM");
    //     $log_in = strtotime("2019-02-1 11:30 PM");
    //     echo  ($shift_in - $log_in)/3600;
    // }
    // END OF CREATE -----------------------------------------------------------------------------------
    // READ -----------------------------------------------------------------------------------
    public function qry_disciplinary_action()
    {
        $fields = "disciplinaryAction_ID, action, periodMonth, abbreviation, label";
        $where = "disciplinaryAction_ID IS NOT NULL";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_disciplinary_action');
    }

    public function get_disciplinary_action()
    {
        $qry_disc_action = $this->qry_disciplinary_action();
        if (count($qry_disc_action) > 0) {
            echo json_encode($qry_disc_action);
        } else {
            $this->default_termination();
            $default_disc_action = $this->qry_disciplinary_action();
            echo json_encode($default_disc_action);
        }
        // echo json_encode([]);
    }

    public function get_specific_diciplinary_action()
    {
        $disciplinary_action_id = $this->input->post('disciplinaryActionId');
        $fields = "disciplinaryAction_ID, action, periodMonth, abbreviation, label";
        $where = "disciplinaryAction_ID = $disciplinary_action_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_disciplinary_action');
        if (count($record) < 0) {
            echo json_encode(0);
        } else {
            echo json_encode($record);
        }
    }

    private function get_discipline_category()
    {
        $fields = "disciplineCategory_ID, category";
        $where = "disciplineCategory_ID IS NOT NULL";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_dms_discipline_category");
    }

    private function get_specific_discipline_category($discipline_category_id)
    {
        $fields = "disciplineCategory_ID, category";
        $where = "disciplineCategory_ID = $discipline_category_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_discipline_category");
    }

    private function get_offense_per_category($discipline_category_id, $active_disc_action_categ_settings_ID)
    {
        $fields = "discActionCateg.disciplinaryActionCategory_ID, discCateg.category, discAction.disciplinaryAction_ID, discAction.action, discActionCateg.level";
        $where = "discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discActionCateg.disciplineCategory_ID = $discipline_category_id AND discActionCateg.disciplinaryActionCategorySettings_ID = $active_disc_action_categ_settings_ID";
        $table = "tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_discipline_category discCateg, tbl_dms_disciplinary_action discAction";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "level", "asc");
    }
    public function get_offense_category()
    {
        $discipline_category = $this->get_discipline_category();
        $this->get_active_dis_action_categ_settings();
        $category_settings = $this->get_current_disciplinary_action_category_settings();
        $count = 0;
        foreach ($discipline_category as $rows) {
            $offense_per_category = $this->get_offense_per_category($rows->disciplineCategory_ID, $category_settings->disciplinaryActionCategorySettings_ID);
            if (count($offense_per_category) > 0) {
                $offense_category[$count] = [
                    "disciplineCategory_ID" => $rows->disciplineCategory_ID,
                    "category" => $rows->category,
                    "offense_per_category" => $offense_per_category,
                    "offense_per_category_count" => count($offense_per_category)
                ];
            } else {
                $offense_category[$count] = [
                    "disciplineCategory_ID" => $rows->disciplineCategory_ID,
                    "category" => $rows->category,
                    "offense_per_category" => '',
                    "offense_per_category_count" => count($offense_per_category)
                ];
            }
            $count++;
        }
        if (count($offense_category) > 0) {
            echo json_encode($offense_category);
        } else {
            echo json_encode([]);
        }
    }

    public function get_max_offense_level($category_id, $active_disc_action_categ_settings_ID)
    {
        $fields = "MAX(level) level";
        $where = "disciplineCategory_ID = $category_id AND disciplinaryActionCategorySettings_ID = $active_disc_action_categ_settings_ID";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_disciplinary_action_category");
    }

    private function get_ir_by_category_settings($disc_action_categ_settings_id)
    {
        $fields = "incidentReport_ID";
        $where = "disciplinaryActionCategorySettings_ID = $disc_action_categ_settings_id";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_dms_incident_report");
    }

    private function get_all_disc_action_category()
    {
        $fields = "actionCateg.disciplineCategory_ID, actionCateg.disciplinaryAction_ID, actionCateg.level";
        $where = "actionCateg.disciplinaryActionCategorySettings_ID = actionCategSettings.disciplinaryActionCategorySettings_ID AND actionCategSettings.status_ID = 10";
        $table = "tbl_dms_disciplinary_action_category actionCateg, tbl_dms_disciplinary_action_category_settings actionCategSettings";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    private function get_active_dis_action_categ_settings()
    {
        $category_settings = $this->get_current_disciplinary_action_category_settings();
        if (count($category_settings) < 1) {
            $act_categ_settings_id = $this->add_new_disc_action_category_settings();
        }
    }

    public function check_if_category_settings_is_used_by_ir()
    {
        $data['exist'] = 0;
        $this->get_active_dis_action_categ_settings();
        $category_settings = $this->get_current_disciplinary_action_category_settings();
        $ir_records = $this->get_ir_by_category_settings($category_settings->disciplinaryActionCategorySettings_ID);
        if (count($ir_records) > 0) {
            $data['exist'] = 1;
        } else {
            $data['exist'] = 0;
        }
        return $data;
    }


    public function get_unused_disciplinary_action($category_id,  $active_disc_action_categ_settings_ID)
    {
        $query = "SELECT disciplinaryAction_ID, action, abbreviation FROM tbl_dms_disciplinary_action WHERE disciplinaryAction_ID NOT IN (SELECT disciplinaryAction_ID FROM tbl_dms_disciplinary_action_category WHERE disciplineCategory_ID = $category_id AND disciplinaryActionCategorySettings_ID = $active_disc_action_categ_settings_ID)";
        return $record = $this->general_model->custom_query($query);
    }

    public function get_add_offense_level_form_details()
    {
        $category_id = $this->input->post('categoryId');
        $category_settings = $this->get_current_disciplinary_action_category_settings();
        $unused_disc_action = $this->get_unused_disciplinary_action($category_id, $category_settings->disciplinaryActionCategorySettings_ID);
        $max_offense_level = $this->get_max_offense_level($category_id, $category_settings->disciplinaryActionCategorySettings_ID);
        if (count($unused_disc_action) > 0) {
            $disciplinary_action['record'] = $unused_disc_action;
        } else {
            $disciplinary_action['record'] = 0;
        }
        if ($max_offense_level->level === NULL) {
            $disciplinary_action['max_level'] = 0;
        } else {
            $disciplinary_action['max_level'] = $max_offense_level->level;
        }
        echo json_encode($disciplinary_action);
    }

    public function get_max_category()
    {
        $fields = "disciplineCategory_ID, category";
        $where = "disciplineCategory_ID IS NOT NULL";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_discipline_category", "category desc");
        if (count($record) > 0) {
            $max['exist'] = 1;
            $max['record'] = $record;
        } else {
            $max['exist'] = 0;
            $max['record'] = 0;
        }
        echo json_encode($max);
    }

    public function get_max_offense_category()
    {
        $fields = "offenseType_ID, letter";
        $where = "status_ID = 10 AND    offenseType_ID IS NOT NULL";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense_type', "letter desc");
        if (count($record) > 0) {
            $max['exist'] = 1;
            $max['record'] = $record;
        } else {
            $max['exist'] = 0;
            $max['record'] = 0;
        }
        echo json_encode($max);
    }

    public function check_if_category_exist()
    {
        $category = strtolower($this->input->post('category'));
        $fields = "disciplineCategory_ID";
        $where = "category = '$category'";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_discipline_category");
        if (count($record) > 0) {
            $validation['valid'] = false;
        } else {
            $validation['valid'] = true;
        }
        echo json_encode($validation);
    }

    public function check_if_offense_category_exist()
    {
        $category = strtolower($this->input->post('categoryLetter'));
        $fields = "offenseType_ID";
        $where = "letter = '$category' AND status_ID = 10";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense_type');
        if (count($record) > 0) {
            $validation['valid'] = false;
        } else {
            $validation['valid'] = true;
        }
        echo json_encode($validation);
    }

    private function check_if_category_exist_offense($category_id)
    {
        $fields = "offense_ID";
        $where = "multiCategory LIKE '%$category_id%'";
        return $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense');
    }

    private function get_offense_with_spec_category($category_id)
    {
        $fields = "offense_ID, multiCategory, multiCategoryLetters";
        $where = "multiCategory LIKE '%$category_id%'";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense');
    }

    public function check_if_category_has_offenses()
    {
        $category_id = $this->input->post('categoryId');
        $record = $this->get_offense_with_spec_category($category_id);
        if (count($record) > 0) {
            $exist['exist'] = 1;
            $exist['offense_count'] = count($record);
        } else {
            $exist['exist'] = 0;
            $exist['offense_count'] = count($record);
        }
        echo json_encode($exist);
    }

    private function get_offenses_under_offense_type($offense_category_id)
    {
        $fields = "offense_ID";
        $where = "offenseType_ID = $offense_category_id";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense');
    }

    public function check_if_category_type_has_offenses()
    {
        $offense_category_id = $this->input->post('offenseCategoryId');
        $record = $this->get_offenses_under_offense_type($offense_category_id);
        if (count($record) > 0) {
            $exist['exist'] = 1;
            $exist['offense_count'] = count($record);
        } else {
            $exist['exist'] = 0;
            $exist['offense_count'] = count($record);
        }
        echo json_encode($exist);
    }

    public function check_if_disc_action_has_category()
    {
        $disciplinary_action_id = $this->input->post('disciplinaryActionId');
        $fields = "disciplinaryActionCategory_ID";
        $where = "disciplinaryAction_ID = $disciplinary_action_id";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_dms_disciplinary_action_category");
        if (count($record) > 0) {
            $exist['exist'] = 1;
            $exist['disciplinary_action_count'] = count($record);
        } else {
            $exist['exist'] = 0;
            $exist['disciplinary_action_count'] = count($record);
        }
        echo json_encode($exist);
    }

    private function check_if_assigned_with_sup($emp_id)
    {
        $fields = "employeeRelationSupervisor_ID";
        $where = "emp_id = $emp_id";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_employee_relations_supervisor');
    }

    public function get_specific_offense_type()
    {
        $fields = "DISTINCT(offenseCateg.offenseType_ID) offenseType_ID, offenseCateg.letter, offenseCateg.offenseType";
        $where = "offenseCateg.offenseType_ID = offense.offenseType_ID";
        $table = "tbl_dms_offense offense, tbl_dms_offense_type offenseCateg";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "offenseCateg.letter ASC");
        if (count($record) > 0) {
            $exist['exist'] = 1;
            $exist['record'] = $record;
        } else {
            $exist['exist'] = 0;
            $exist['record'] = 0;
        }
        echo json_encode($exist);
    }

    public function get_default_offense()
    {
        $offense_category_id = $this->input->post("offenseCategoryId");
        if ((int) $offense_category_id > 0) {
            $where = "status_ID = 10 AND offenseType_ID = $offense_category_id ";
        } else {
            $where = "status_ID = 10 AND offenseType_ID IS NOT NULL";
        }
        $fields = "offense_ID, offense, offenseType_ID, multiCategory, multiCategoryLetters, orderNum";;
        $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense', 'orderNum ASC');
        if (count($record) > 0) {
            $exist['exist'] = 1;
            $exist['record'] = $record;
        } else {
            $exist['exist'] = 0;
            $exist['record'] = 0;
        }
        echo json_encode($exist);
    }

    public function get_all_specific_offense_category()
    {
        $search_offense_category = $this->input->post('searchOffenseCategory');
        $offense_categ_description = "";
        if (!empty($search_offense_category)) {
            $offense_categ_description = "AND (offenseType LIKE '%" . $search_offense_category . "%' OR letter LIKE '%" . $search_offense_category . "%')";
        }
        $fields = "offenseType_ID, letter, offenseType";
        $where = "offenseType_ID IS NOT NULL $offense_categ_description AND status_ID = 10";
        $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense_type', 'letter', 'ASC');
        if (count($record) > 0) {
            $exist['exist'] = 1;
            $exist['spec_offense_category'] = $record;
        } else {
            $exist['exist'] = 0;
            $exist['spec_offense_category'] = 0;
        }
        echo json_encode($exist);
    }

    public function qry_emp_qip_offenses()
    {
        $emp_details = $this->get_emp_details_via_emp_id($this->session->userdata('emp_id'));
        $fields = "offType.offenseType_ID, offType.letter, offType.offenseType";
        $where = "offType.offenseType_ID = offTypeAss.offenseType_ID AND offTypeAss.acc_id = $emp_details->acc_id AND offType.status_ID = 10";
        $table = "tbl_dms_offense_type_assignment offTypeAss, tbl_dms_offense_type offType";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "offType.letter ASC");
    }

    public function get_emp_qip_offenses()
    {
        $emp_qip_offenses = $this->qry_emp_qip_offenses();
        $exist = 0;
        if (count($emp_qip_offenses) > 0) {
            $exist = 1;
        }
        echo json_encode($exist);
    }

    public function get_all_nature_offense_view_general()
    {
        $search_offense_category = $this->input->post('searchOffenseCategory');
        $natureOffenseType = $this->input->post('natureOfOffenseType');
        $offense_categ_description = "";
        if (!empty($search_offense_category)) {
            $offense_categ_description = "AND (offenseType LIKE '%" . $search_offense_category . "%' OR letter LIKE '%" . $search_offense_category . "%')";
        }
        if ($natureOffenseType == 1) {
            $fields = "offenseType_ID, letter, offenseType";
            $where = "offenseType_ID IS NOT NULL $offense_categ_description AND status_ID = 10 AND offenseType_ID NOT IN (SELECT offenseType_ID FROM tbl_dms_offense_type_assignment)";
            $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense_type', 'letter', 'ASC');
        } else {
            $emp_details = $this->get_emp_details_via_emp_id($this->session->userdata('emp_id'));
            $fields = "offType.offenseType_ID, offType.letter, offType.offenseType";
            $where = "offType.offenseType_ID = offTypeAss.offenseType_ID AND offTypeAss.acc_id = $emp_details->acc_id AND offType.status_ID = 10 $offense_categ_description";
            $table = "tbl_dms_offense_type_assignment offTypeAss, tbl_dms_offense_type offType";
            $record =  $this->general_model->fetch_specific_vals($fields, $where, $table, "offType.letter ASC");
        }
        if (count($record) > 0) {
            $exist['exist'] = 1;
            $exist['spec_offense_category'] = $record;
        } else {
            $exist['exist'] = 0;
            $exist['spec_offense_category'] = 0;
        }
        echo json_encode($exist);
    }

    public function get_one_specific_offense_category()
    {
        $offense_category_id = $this->input->post('offenseCategoryId');
        $fields = "offenseType_ID, letter, offenseType";
        $where = "offenseType_ID = $offense_category_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense_type');
        if (count($record) > 0) {
            $exist['exist'] = 1;
            $exist['spec_offense_category'] = $record;
        } else {
            $exist['exist'] = 0;
            $exist['spec_offense_category'] = 0;
        }
        echo json_encode($exist);
    }

    public function get_all_offense_category()
    {
        $category_settings = $this->get_current_disciplinary_action_category_settings();
        $fields = "DISTINCT(discActionCateg.disciplineCategory_ID), discCateg.category";
        $where = "discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discActionCateg.disciplinaryActionCategorySettings_ID = " . $category_settings->disciplinaryActionCategorySettings_ID;
        $table = "tbl_dms_discipline_category discCateg, tbl_dms_disciplinary_action_category discActionCateg";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'discCateg.category', 'ASC');
        if (count($record) > 0) {
            $exist['exist'] = 1;
            $exist['spec_offenses'] = $record;
        } else {
            $exist['exist'] = 0;
            $exist['spec_offenses'] = 0;
        }
        echo json_encode($exist);
    }

    public function get_max_offense_order()
    {
        $offense_category_id = $this->input->post('offenseCategoryId');
        $fields = "MAX(orderNum) orderNum";
        $where = "offenseType_ID = $offense_category_id AND status_ID = 10";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense');
        if ($record->orderNum != NULL) {
            $max_order['max_order'] = $record->orderNum + 1;
        } else {
            $max_order['max_order'] = 1;
        }
        echo json_encode($max_order);
    }
    private function get_specific_offense_category($offense_category_id)
    {
        $fields = "offenseType_ID, letter, offenseType, dateCreated, dateChanged, changedBy";
        $where = "offenseType_ID = $offense_category_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense_type');
    }
    private function get_specific_offenses($offense_category_id)
    {
        $fields = "offense_ID, offense, offenseType_ID, orderNum, multiCategory, multiCategoryLetters";
        $where = "offenseType_ID = $offense_category_id AND status_ID=10";
        $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense');
        if (count($record) > 0) {
            $offense['exist'] = 1;
            $offense['record'] = $record;
        } else {
            $offense['exist'] = 0;
            $offense['record'] = 0;
        }
        return $offense;
    }
    protected function get_all_offenses()
    {
        $fields = "offense_ID, offense, offenseType_ID, orderNum, multiCategory, multiCategoryLetters";
        $where = "status_ID=10";
        $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense');
        if (count($record) > 0) {
            $offense['exist'] = 1;
            $offense['record'] = $record;
        } else {
            $offense['exist'] = 0;
            $offense['record'] = 0;
        }
        return $offense;
        // var_dump($offense);
    }

    protected function get_all_assignable_class(){
        $action_categ_settings = $this->get_current_disciplinary_action_category_settings();
        $fields = "DISTINCT(categ.disciplineCategory_ID) categId, categ.category";
        $where = "categ.disciplineCategory_ID = actionCateg.disciplineCategory_ID AND actionCateg.disciplinaryActionCategorySettings_ID = ". $action_categ_settings->disciplinaryActionCategorySettings_ID;
        $table = "tbl_dms_discipline_category categ, tbl_dms_disciplinary_action_category actionCateg";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        if (count($record) > 0) {
            $offense['exist'] = 1;
            $offense['record'] = $record;
        } else {
            $offense['exist'] = 0;
            $offense['record'] = 0;
        }
        return $offense;
    }

    protected function get_all_assignable_disc_action($categ_id){
        $action_categ_settings = $this->get_current_disciplinary_action_category_settings();
        $fields = "actionCateg.disciplinaryActionCategory_ID, actions.action, actions.periodMonth, actions.abbreviation, actions.label, actionCateg.disciplinaryActionCategorySettings_ID";
        $where = "actions.disciplinaryAction_ID = actionCateg.disciplinaryAction_ID AND actionCateg.disciplineCategory_ID = $categ_id AND actionCateg.disciplinaryActionCategorySettings_ID = ". $action_categ_settings->disciplinaryActionCategorySettings_ID;
        $table = "tbl_dms_disciplinary_action actions ,tbl_dms_disciplinary_action_category actionCateg";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        if (count($record) > 0) {
            $disc_action['exist'] = 1;
            $disc_action['record'] = $record;
        } else {
            $disc_action['exist'] = 0;
            $disc_action['record'] = 0;
        }
        return $disc_action;
    }

    protected function get_all_categories()
    {
        $fields = "offense_ID, offense, offenseType_ID, orderNum, multiCategory, multiCategoryLetters";
        $where = "status_ID=10";
        $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense');
        if (count($record) > 0) {
            $offense['exist'] = 1;
            $offense['record'] = $record;
        } else {
            $offense['exist'] = 0;
            $offense['record'] = 0;
        }
        return $offense;
        // var_dump($offense);
    }

    public function get_all_offenses_records(){
        echo json_encode($this->get_all_offenses());
    }

    public function get_all_class_records(){
        echo json_encode($this->get_all_assignable_class());
    }

    public function get_all_assignable_disc_action_record(){
        $categ_id = $this->input->post('categId');
        if($categ_id == ""){
            $categ_id = 0; 
        }
        echo json_encode($this->get_all_assignable_disc_action($categ_id));
    }

    private function get_offense($offense_id)
    {
        $fields = "offense_ID, offense, multiCategory, multiCategoryLetters, offenseType_ID, orderNum, dateCreated, dateChanged, changedBy";
        $where = "offense_ID = $offense_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense');
    }

    private function get_specific_account_recommendation_number($acc_recommendation_id)
    {
        $fields = "accRecNum.accountRecommendationNumber_ID, accRecNum.account_ID, acc.acc_name, accRecNum.number, accRecNum.dateCreated, accRecNum.dateChanged, accRecNum.changedBy";
        $where = "acc.acc_id = accRecNum.account_ID AND accRecNum.accountRecommendationNumber_ID = $acc_recommendation_id";
        $table =  "tbl_dms_account_recommendation_number accRecNum, tbl_account acc";
        return $record = $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    private function get_account_recommendation_num_via_account($account_id)
    {
        $fields = "accountRecommendationNumber_ID, account_ID, number, dateCreated, dateChanged, changedBy";
        $where = "account_ID = $account_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_account_recommendation_number");
    }

    public function get_specific_offense()
    {
        $offense_id = $this->input->post('offenseId');
        $record = $this->get_offense($offense_id);
        if (count($record) > 0) {
            $offense['exist'] = 1;
            $offense['record'] = $record;
        } else {
            $offense['exist'] = 0;
            $offense['record'] = 0;
        }
        echo json_encode($offense);
    }

    private function get_used_offense_ir($offense_id)
    {
        $fields = "incidentReport_ID";
        $where = "offense_ID = $offense_id AND prescriptiveStat = 2";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_dms_incident_report");
    }

    private function get_used_offense_ir_off_type($offense_type)
    {
        $fields = "ir.incidentReport_ID";
        $where = "ir.offense_ID = off.offense_ID AND off.offenseType_ID = $offense_type AND ir.prescriptiveStat = 2";
        $table = "tbl_dms_incident_report ir, tbl_dms_offense off";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    public function check_offense_is_used()
    {
        $offense_id = $this->input->post('offenseId');
        $record = $this->get_used_offense_ir($offense_id);
        if (count($record) > 0) {
            $offense['exist'] = 1;
        } else {
            $offense['exist'] = 0;
        }
        echo json_encode($offense);
    }

    public function check_offense_type_is_used()
    {
        $offense_type_id = $this->input->post('offenseTypeId');
        $record = $this->get_used_offense_ir_off_type($offense_type_id);
        if (count($record) > 0) {
            $offense['exist'] = 1;
        } else {
            $offense['exist'] = 0;
        }
        echo json_encode($offense);
    }

    public function get_specific_recommendation_num()
    {
        $acc_recommendation_id = $this->input->post('recommendationNumId');
        $record = $this->get_specific_account_recommendation_number($acc_recommendation_id);
        if (count($record) > 0) {
            $data['exist'] = 1;
            $data['record'] = $record;
        } else {
            $data['exist'] = 0;
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_selected_offense_category_details()
    {
        $offense_category_id = $this->input->post('offenseCategoryId');
        $details['category'] = $this->get_specific_offense_category($offense_category_id);
        $details['offenses'] = $this->get_specific_offenses($offense_category_id);
        echo json_encode($details);
    }

    public function distinct_offense_category()
    {
        $offense_category_id = $this->input->post('offenseCategoryId');
        $fields = "DISTINCT(off.disciplineCategory_ID), discCateg.category";
        $where = "discCateg.disciplineCategory_ID = off.disciplineCategory_ID AND off.offenseType_ID = $offense_category_id";
        $table = "tbl_dms_offense off, tbl_dms_discipline_category discCateg";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'discCateg.category', 'ASC');
        if (count($record) > 0) {
            $offense['exist'] = 1;
            $offense['record'] = $record;
        } else {
            $offense['exist'] = 0;
            $offense['record'] = 0;
        }
        echo json_encode($offense);
    }

    public function get_offenses_datatable()
    {
        $datatable = $this->input->post('datatable');
        $offense_category_id = $datatable['query']['offenseCategoryId'];
        // $discipline_category_id = $datatable['query']['disciplineCategoryId'];
        $disc_where = "";
        // if((int) $discipline_category_id !== 0){
        //     $disc_where = "AND off.disciplineCategory_ID = $discipline_category_id";
        // }
        $query['query'] = "SELECT off.offense_ID, off.orderNum, off.offense, off.offenseType_ID, off.multiCategory, off.multiCategoryLetters FROM tbl_dms_offense off WHERE off.offenseType_ID = $offense_category_id AND off.status_ID = 10";
        if ($datatable['query']['offenseSearchField'] != '') {
            $keyword = $datatable['query']['offenseSearchField'];
            $query['search']['append'] = " AND off.offense LIKE '%" . $keyword . "%'";
            $query['search']['total'] = " AND off.offense LIKE '%" . $keyword . "%'";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    private function check_if_order_num_exist($order_num, $offense_category_id)
    {
        $fields = "offense_ID";
        $where = "orderNum = $order_num AND offenseType_ID = $offense_category_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense');
        if (count($record)) {
            return 1;
        } else {
            return 0;
        }
    }

    private function get_dtr_violation_settings_with_category($dtr_violation_type_id)
    {
        $query = "SELECT dtrVioSettings.dtrViolationTypeSettings_ID, dtrVioSettings.dtrViolationType_ID, dtrVioSettings.oneTimeStat, dtrVioSettings.oneTimeVal, dtrVioSettings.consecutiveStat, dtrVioSettings.consecutiveVal, dtrVioSettings.nonConsecutiveStat, dtrVioSettings.nonConsecutiveVal, dtrVioSettings.durationType, dtrVioSettings.durationVal, dtrVioSettings.durationUnit, dtrVioSettings.offense_ID, dtrVioSettings.category_ID, dtrVioSettings.daysDeadline, dtrVioSettings.deadlineOption, categ.category FROM tbl_dms_dtr_violation_type_settings dtrVioSettings LEFT JOIN tbl_dms_discipline_category categ ON categ.disciplineCategory_ID = dtrVioSettings.category_ID WHERE dtrVioSettings.dtrViolationType_ID = $dtr_violation_type_id";
        return $this->general_model->custom_query_row($query);
    }

    public function get_previous_user_dtr_violation($user_dtr_violation_id, $user_id, $violation_type)
    {
        $fields = "userDtrViolation_ID, user_ID, supervisor_ID, dtrViolationType_ID, sched_ID, month, year, sched_date, occurence, dateRecord";
        $where = "userDtrViolation_ID < $user_dtr_violation_id AND user_ID = $user_id AND dtrViolationType_ID = $violation_type";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_user_dtr_violation", "userDtrViolation_ID DESC");
    }


    private function get_ir_deadline_settings($field)
    {
        $fields = "deadlineToExplain, deadlineToExplainOption, deadlineToRecommend, deadlineToRecommendOption, deadlineLastRecommendation, deadlineFinalDecision, deadlineUploadDocs";
        $where = "deadlineToExplain IS NOT NULL";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_incident_report_deadline_settings');
    }

    protected function get_ir_deadline_settings_details_recorded()
    {
        // $field = $this->input->post('field');
        $field = "deadlineToExplain";
        $ir_deadline_settings = $this->get_ir_deadline_settings($field);
        if (count($ir_deadline_settings) > 0) {
            $deadline_settings['record'] = $ir_deadline_settings;
        } else {
            $add_default_stat = $this->add_default_ir_deadline_settings();
            if ($add_default_stat) {
                $deadline_settings['record'] = $this->get_ir_deadline_settings($field);
            } else {
                $deadline_settings['record'] = 0;
            }
        }
        return $deadline_settings;
    }

    public function get_ir_deadline_settings_details()
    {
        // $field = $this->input->post('field');
        // $field = "deadlineToExplain";
        // $ir_deadline_settings = $this->get_ir_deadline_settings($field);
        // if(count($ir_deadline_settings)> 0){
        //     $deadline_settings['record'] = $ir_deadline_settings;
        // }else{
        //     $add_default_stat = $this->add_default_ir_deadline_settings();
        //     if($add_default_stat){
        //         $deadline_settings['record'] = $this->get_ir_deadline_settings($field);
        //     }else{
        //         $deadline_settings['record'] = 0;
        //     }
        // }
        echo json_encode($this->get_ir_deadline_settings_details_recorded());
    }




    private function get_offense_with_type($offense_id)
    {
        $fields = "offense.offense_ID, offense.offense, offense.multiCategory, offense.multiCategoryLetters, offense.offenseType_ID, offenseCateg.offenseType, offenseCateg.letter";
        $where = "offenseCateg.offenseType_ID = offense.offenseType_ID AND offense.offense_ID = $offense_id";
        $table = "tbl_dms_offense offense, tbl_dms_offense_type offenseCateg";
        return $record = $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    private function get_dtr_vio_settings_main($dtr_violation_type_id)
    {
        $dtr_violation_settings = $this->get_dtr_violation_settings_with_category($dtr_violation_type_id);
        if (count($dtr_violation_settings) > 0) {
            $dtr_vio_settings['record'] = $dtr_violation_settings;
            $dtr_vio_settings['offense'] = $this->get_offense_with_type($dtr_violation_settings->offense_ID);
        } else {
            $default_violation_settings_id = $this->add_default_violation_settings($dtr_violation_type_id);
            $dtr_violation_settings2 = $this->get_dtr_violation_settings($dtr_violation_type_id);
            $dtr_vio_settings['record'] = $dtr_violation_settings2;
            $dtr_vio_settings['offense'] = $this->get_offense_with_type($dtr_violation_settings2->offense_ID);
        }
        return $dtr_vio_settings;
    }

    public function get_dtr_violation_settings_details()
    {
        $dtr_violation_type_id = $this->input->post('dtrViolationTypeId');
        $dtr_vio_settings = $this->get_dtr_vio_settings_main($dtr_violation_type_id);
        echo json_encode($dtr_vio_settings);
    }

    public function get_offense_for_dtr_vio_add_settings()
    {
        $fields = "offense_ID, offense, multiCategory, offenseType_ID, orderNum, dateCreated, dateChanged, changedBy";
        $where = "offense_ID IS NOT NULL";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense');
    }

    private function get_account_by_class($class)
    {
        $where = "acc_id IS NOT NULL";
        if ($class !== '0') {
            $where = "acc_description = '$class'";
        }
        $fields = "acc_id, acc_name";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_account", "acc_name", 'ASC');
    }

    private function get_emp_by_acc_class($class, $acc_id, $ass_type)
    {
        $where_class = "";
        $where_acc = "";
        $where_qip = "";
        if ($class !== '0') {
            $where_class = "AND acc.acc_description = '$class'";
        }
        if ((int) $acc_id !== 0) {
            $where_acc = "AND acc.acc_id = $acc_id";
        }
        if ($ass_type == "qip") {
            $where_qip = " AND emp.emp_id NOT IN (SELECT emp_id FROM tbl_dms_qip_assignment) ";
        }

        $where = "app.apid = emp.apid AND emp.acc_id = acc.acc_id AND emp.isActive = 'yes' " . $where_class . " " . $where_acc . " " . $where_qip;
        $fields = "emp.emp_id, app.fname, app.mname, app.lname";
        $table = "tbl_applicant app, tbl_employee emp, tbl_account acc";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
    }


    public function get_account()
    {
        $class = $this->input->post('class');
        $record = $this->get_account_by_class($class);
        if (count($record) > 0) {
            $data['record'] = $record;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_assignable_acc_recommendation_num()
    {
        $class = $this->input->post('class');
        $where = "acc_id IS NOT NULL";
        if ($class !== '0') {
            $where = "acc_description = '$class'";
        }
        $fields = "acc_id, acc_name";
        $where .= " AND acc_id NOT IN (SELECT account_ID FROM tbl_dms_account_recommendation_number)";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_account", "acc_name", 'ASC');
        if (count($record) > 0) {
            $data['record'] = $record;
            $data['exist'] = 1;
        } else {
            $data['record'] = 0;
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_site()
    {
        $fields = "site_ID, code, siteName";
        $where = "site_ID IS NOT NULL";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_site", "code ASC");
        if (count($record) > 0) {
            $data['record'] = $record;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_offense_type_select()
    {
        $acc_id = $this->input->post('accId');
        if ($acc_id !== "") {
            $fields = "offenseType_ID, letter, offenseType";
            $where = "offenseType_ID NOT IN (SELECT offenseType_ID FROM tbl_dms_offense_type_assignment WHERE acc_id = $acc_id) AND status_ID != 11";
            $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense_type', "letter ASC");
        } else {
            $record = 0;
        }
        if (count($record) > 0) {
            $data['record'] = $record;
            $data['exist'] = 1;
        } else {
            $data['record'] = 0;
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_assignable_account()
    {
        $emp_id = $this->input->post('empId');
        $site_id = $this->input->post('siteId');
        if ($emp_id !== "" && $site_id !== "") {
            $fields = "acc.acc_id, acc.acc_name";
            $where = "acc.acc_id NOT IN (SELECT ero.acc_id FROM tbl_dms_employee_relations_officer ero WHERE site_ID = $site_id)";
            $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_account acc", 'acc.acc_name ASC');
        } else {
            $record = 0;
        }
        if (count($record) > 0) {
            $data['record'] = $record;
            $data['exist'] = 1;
        } else {
            $data['record'] = 0;
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_qip()
    {
        $qip_id = $this->input->post('qipId');
        $fields = "qipAssignment_ID, emp_id";
        $where = "qipAssignment_ID = $qip_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_qip_assignment');
        if (count($record) > 0) {
            $data['exist'] = 1;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    private function get_specific_ero_accounts_record($ero_id)
    {
        $fields = "employeeRelationsOfficer_ID, emp_id";
        $where = "employeeRelationsOfficer_ID = $ero_id";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_employee_relations_officer');
    }

    private function get_all_ero_accounts_records($emp_id)
    {
        $fields = "employeeRelationsOfficer_ID, emp_id";
        $where = "emp_id = $emp_id";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_employee_relations_officer');
    }

    public function get_specific_ero_account()
    {
        $ero_id = $this->input->post('eroId');
        $record = $this->get_specific_ero_accounts_record($ero_id);
        if (count($record) > 0) {
            $data['exist'] = 1;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_ero_account_by_emp()
    {
        $emp_id = $this->input->post('empId');
        // $emp_id = 26;
        $fields = "COUNT(employeeRelationsOfficer_ID) eroCount";
        $where = "emp_id = $emp_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_employee_relations_officer');
        if (count($record) > 0) {
            $data['exist'] = $record->eroCount;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_assigned_offense_type()
    {
        $off_type_ass_id = $this->input->post('offTypeAssId');
        // $off_type_ass_id = 1;
        $fields = "offenseTypeAssignment_ID, offenseType_ID, acc_id";
        $where = "offenseTypeAssignment_ID = $off_type_ass_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense_type_assignment');
        // ////var_dump($record);
        if (count($record) > 0) {
            $data['exist'] = $record->offenseTypeAssignment_ID;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_assigned_offense_type_by_acc_id()
    {
        $acc_id = $this->input->post('accId');
        $fields = "offenseTypeAssignment_ID, offenseType_ID";
        $where = "acc_ID = $acc_id";
        $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_offense_type_assignment');
        if (count($record) > 0) {
            $data['exist'] = count($record);
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_emp()
    {
        $class = $this->input->post('class');
        $acc_id = $this->input->post('acc_id');
        $ass_type = $this->input->post('ass_type');
        // $class = 'Admin';
        // $acc_id = 0;
        $record = $this->get_emp_by_acc_class($class, $acc_id, $ass_type);
        if (count($record) > 0) {
            $data['record'] = $record;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_qip_assignment_datatable()
    {
        $datatable = $this->input->post('datatable');
        // $class = $datatable['query']['class'];
        // $acc_id = $datatable['query']['accId'];
        $where_class = "";
        $where_acc = "";
        // if($class !== '0'){
        //     $where_class = "AND acc.acc_description = '$class'";
        // }
        // if((int)$acc_id !== 0){
        //     $where_acc = "AND acc.acc_id = $acc_id";
        // }
        $query['query'] = "SELECT qip.qipAssignment_ID, app.fname, app.mname, app.lname FROM tbl_dms_qip_assignment qip, tbl_employee emp, tbl_applicant app, tbl_account acc WHERE acc.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = qip.emp_id " . $where_acc . " " . $where_class;
        if ($datatable['query']['qipSearchField'] != '') {
            $keyword = $datatable['query']['qipSearchField'];
            $query['search']['append'] = " AND (app.fname LIKE '%" . $keyword . "%' OR app.mname LIKE '%" . $keyword . "%' OR app.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname ,app.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname, app.mname ,app.lname) LIKE '%" . $keyword . "%')";
            $query['search']['total'] = " AND (app.fname LIKE '%" . $keyword . "%' OR app.mname LIKE '%" . $keyword . "%' OR app.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname ,app.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname, app.mname ,app.lname) LIKE '%" . $keyword . "%')";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_ero_supervisor_datatable()
    {
        $datatable = $this->input->post('datatable');
        $ero_sup_id = $datatable['query']['eroSupId'];
        $ero_id = $datatable['query']['eroId'];
        $where_ero_sup = "";
        $where_ero = "";
        if ((int) $where_ero_sup !== 0) {
            $where_ero_sup = " AND eroSup.employeeRelationsSupervisor = $ero_sup_id";
        }
        if ((int) $where_ero !== 0) {
            $where_ero = " AND eroSup.emp_id = $ero_id";
        }
        $query['query'] = "SELECT eroSup.employeeRelationSupervisor_ID, empEro.emp_id empEroEmpId, appEro.fname appEroFname, appEro.mname appEroMname, appEro.lname appEroLname, empEroSup.emp_id empEroSupEmp_id, appEroSup.fname appEroSupFname, appEroSup.mname appEroSupMname, appEroSup.lname appEroSupLname FROM tbl_dms_employee_relations_supervisor eroSup, tbl_employee empEro, tbl_employee empEroSup, tbl_applicant appEro, tbl_applicant appEroSup WHERE appEro.apid = empEro.apid AND appEroSup.apid = empEroSup.apid AND empEro.emp_id = eroSup.emp_id AND empEroSup.emp_id = eroSup.employeeRelationsSupervisor";

        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_ero_assignment_datatable()
    {
        $datatable = $this->input->post('datatable');
        if ($datatable['query']['eroSearchField'] != '') {
            $query['query'] = "SELECT DISTINCT(ero.emp_id) emp_id, ero.employeeRelationsOfficer_ID, appEro.fname appEroFname, appEro.lname appEroLname, appEro.mname appEroMname, appEroSup.fname appEroSupFname, appEroSup.mname appEroSupMname, appEroSup.lname appEroSupLname, COUNT(ero.acc_id) acc_id FROM tbl_dms_employee_relations_officer ero, tbl_dms_employee_relations_supervisor eroSup, tbl_employee empEro, tbl_employee empEroSup, tbl_applicant appEro, tbl_applicant appEroSup WHERE appEro.apid = empEro.apid AND appEroSup.apid = empEroSup.apid AND empEroSup.emp_id = eroSup.employeeRelationsSupervisor AND eroSup.emp_id = ero.emp_id AND empEro.emp_id = ero.emp_id";
            $keyword = $datatable['query']['eroSearchField'];
            $query['search']['append'] = " AND (appEro.fname LIKE '%" . $keyword . "%' OR appEro.mname LIKE '%" . $keyword . "%' OR appEro.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',appEro.fname ,appEro.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',appEro.fname, appEro.mname ,appEro.lname) LIKE '%" . $keyword . "%' OR appEroSup.fname LIKE '%" . $keyword . "%' OR appEroSup.mname LIKE '%" . $keyword . "%' OR appEroSup.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',appEroSup.fname ,appEroSup.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',appEroSup.fname, appEroSup.mname ,appEroSup.lname) LIKE '%" . $keyword . "%') GROUP BY ero.emp_id";
            $query['search']['total'] = " AND (appEro.fname LIKE '%" . $keyword . "%' OR appEro.mname LIKE '%" . $keyword . "%' OR appEro.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',appEro.fname ,appEro.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',appEro.fname, appEro.mname ,appEro.lname) LIKE '%" . $keyword . "%' OR appEroSup.fname LIKE '%" . $keyword . "%' OR appEroSup.mname LIKE '%" . $keyword . "%' OR appEroSup.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',appEroSup.fname ,appEroSup.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',appEroSup.fname, appEroSup.mname ,appEroSup.lname) LIKE '%" . $keyword . "%') GROUP BY ero.emp_id";
        } else {
            $query['query'] = "SELECT DISTINCT(ero.emp_id) emp_id, ero.employeeRelationsOfficer_ID, appEro.fname appEroFname, appEro.lname appEroLname, appEro.mname appEroMname, appEroSup.fname appEroSupFname, appEroSup.mname appEroSupMname, appEroSup.lname appEroSupLname, COUNT(ero.acc_id) acc_id FROM tbl_dms_employee_relations_officer ero, tbl_dms_employee_relations_supervisor eroSup, tbl_employee empEro, tbl_employee empEroSup, tbl_applicant appEro, tbl_applicant appEroSup WHERE appEro.apid = empEro.apid AND appEroSup.apid = empEroSup.apid AND empEroSup.emp_id = eroSup.employeeRelationsSupervisor AND eroSup.emp_id = ero.emp_id AND empEro.emp_id = ero.emp_id GROUP BY ero.emp_id";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_offense_type_assignment_datatable()
    {
        $datatable = $this->input->post('datatable');
        $acc_id = $datatable['query']['accId'];
        $when_acc_id = "";
        if ((int) $acc_id !== 0) {
            $when_acc_id = " AND offTypeAss.acc_id = $acc_id";
        }
        $query['query'] = "SELECT DISTINCT(acc.acc_id) accId, acc.acc_name, acc.acc_description, COUNT(offTypeAss.offenseType_ID) offenseTypeCount FROM tbl_account acc, tbl_dms_offense_type_assignment offTypeAss WHERE acc.acc_id = offTypeAss.acc_id " . $when_acc_id . " GROUP BY acc.acc_id";
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    private function default_recommendation_number()
    {
        return 3;
    }

    public function default_ero()
    {
        return 1533;
    }

    private function default_ero_sup()
    {
        return 522;
    }

    public function get_default_recommendation_number()
    {
        $default_recommendation_num = $this->default_recommendation_number();
        echo json_encode($default_recommendation_num);
    }

    public function get_account_recommendation_number_datatable()
    {
        $datatable = $this->input->post('datatable');
        $acc_id = $datatable['query']['accId'];
        $when_acc_id = "";
        if ((int) $acc_id !== 0) {
            $when_acc_id = " AND recNum.account_ID = $acc_id";
        }
        $query['query'] = "SELECT recNum.accountRecommendationNumber_ID, acc.acc_id, acc.acc_name, acc.acc_description, recNum.number FROM tbl_dms_account_recommendation_number recNum, tbl_account acc WHERE acc.acc_id = recNum.account_ID" . $when_acc_id;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_custom_changes_datatable()
    {
        $datatable = $this->input->post('datatable');
        $emp_id = $datatable['query']['empId'];
        $when_emp_id = "";
        if ((int) $emp_id !== 0) {
            $when_emp_id = " AND cusRec.emp_id = $emp_id";
        }
        $query['query'] = "SELECT DISTINCT(emp.emp_id) emp_id, app.fname, app.mname, app.lname, SUM(cusRec.changeType = 'change') as changeCount, SUM(cusRec.changeType = 'exempt') as changeExempt FROM tbl_dms_custom_recommendation cusRec, tbl_employee emp, tbl_applicant app WHERE app.apid = emp.apid AND emp.emp_id = cusRec.emp_id" . $when_emp_id . " GROUP BY emp_id";
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_assigned_offenses_datatable()
    {
        $datatable = $this->input->post('datatable');
        $acc_id = $datatable['query']['accId'];
        if ($datatable['query']['assignedOffenseTypeSearch'] != '') {
            $query['query'] = "SELECT offTypeAss.offenseTypeAssignment_ID, offType.offenseType_ID, offType.letter, offType.offenseType FROM tbl_dms_offense_type_assignment offTypeAss, tbl_dms_offense_type offType WHERE offType.offenseType_ID = offTypeAss.offenseType_ID AND offTypeAss.acc_id = $acc_id";
            $keyword = $datatable['query']['assignedOffenseTypeSearch'];
            $query['search']['append'] = " AND (offType.letter LIKE '%" . $keyword . "%' OR offType.offenseType LIKE '%" . $keyword . "%') GROUP BY offType.letter";
            $query['search']['total'] = " AND (offType.letter LIKE '%" . $keyword . "%' OR offType.offenseType LIKE '%" . $keyword . "%') GROUP BY offType.letter";
        } else {
            $query['query'] = "SELECT offTypeAss.offenseTypeAssignment_ID, offType.offenseType_ID, offType.letter, offType.offenseType FROM tbl_dms_offense_type_assignment offTypeAss, tbl_dms_offense_type offType WHERE offType.offenseType_ID = offTypeAss.offenseType_ID AND offTypeAss.acc_id = $acc_id GROUP BY offType.letter";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    private function get_user_by_emp_id($emp_id)
    {
        $fields = "uid";
        $where = "emp_id = $emp_id";
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_user");
    }

    private function get_emp_details($emp_id)
    {
        $fields = "app.pic, app.fname, app.lname, app.mname, app.nameExt, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, empStat.status as empStatus";
        $where = "empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = $emp_id";
        $table = "tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app, tbl_account accounts";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function get_acc_details()
    {
        $acc_id = $this->input->post('accId');
        $fields = "acc.acc_id, acc.acc_name, acc.acc_description, dep.dep_details";
        $where = "dep.dep_id = acc.dep_id AND acc.acc_id = $acc_id";
        $table = "tbl_account acc, tbl_department dep";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        echo json_encode($record);
    }

    public function get_ero_account_datatable()
    {
        $datatable = $this->input->post('datatable');
        $class = $datatable['query']['class'];
        $site_id = $datatable['query']['siteId'];
        $emp_id = $datatable['query']['empId'];
        $where_class = "";
        $where_site = "";
        if ($class !== "0") {
            $where_class = "AND account.acc_description = '$class'";
        }
        if ($site_id !== "") {
            $where_site = "AND site.site_ID = $site_id";
        }
        // $fields = "ero.employeeRelationsOfficer_ID, account.acc_name, site.code";
        // $where = "account.acc_id = ero.acc_id AND site.site_ID = ero.site_ID AND ero.emp_id =  557 ".$where_class."  ".$where_site;
        // $table = "tbl_dms_employee_relations_officer ero, tbl_account account, tbl_site site";
        // return $this->general_model->fetch_specific_val($fields, $where, $table);
        $query['query'] = "SELECT ero.employeeRelationsOfficer_ID, account.acc_name, site.code FROM tbl_dms_employee_relations_officer ero, tbl_account account, tbl_site site WHERE account.acc_id = ero.acc_id AND site.site_ID = ero.site_ID AND ero.emp_id = $emp_id " . $where_class . "  " . $where_site;
        if ($datatable['query']['eroAccountSearchField'] != '') {
            $keyword = $datatable['query']['eroAccountSearchField'];
            $query['search']['append'] = " AND (account.acc_name LIKE '%" . $keyword . "%' OR site.code LIKE '%" . $keyword . "%')";
            $query['search']['total'] = " AND (account.acc_name LIKE '%" . $keyword . "%' OR site.code LIKE '%" . $keyword . "%')";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_specific_ero_supervisor($emp_id)
    {
        $fields = "eroSup.employeeRelationSupervisor_ID, eroSup.employeeRelationsSupervisor, app.fname, app.mname, app.lname";
        $where = "app.apid = emp.apid AND emp.emp_id = eroSup.employeeRelationsSupervisor AND eroSup.emp_id = $emp_id";
        $table = "tbl_applicant app, tbl_employee emp, tbl_dms_employee_relations_supervisor eroSup";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function get_ero_details()
    {
        $emp_id = $this->input->post('empId');
        $record = $this->get_emp_details($emp_id);
        if (count($record) > 0) {
            $data['record_exist'] = 1;
            $data['record'] = $record;
            $ero_sup = $this->get_specific_ero_supervisor($emp_id);
            if (count($ero_sup) > 0) {
                $data['ero_sup'] = $ero_sup;
                $data['ero_exist'] = 1;
            } else {
                $insert_ero_sup_stat = $this->assign_super_ero($emp_id);
                if ($insert_ero_sup_stat) {
                    $ero_sup2 = $this->get_specific_ero_supervisor($emp_id);
                    $data['ero_sup'] = $ero_sup2;
                    $data['ero_exist'] = 1;
                } else {
                    $data['ero_sup'] = 0;
                    $data['ero_exist'] = 0;
                }
            }
        } else {
            $data['record_exist'] = 0;
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    //   private function get_existing_ero(){
    //     $fields = "offense_ID, offense, multiCategory, offenseType_ID, orderNum, dateCreated, dateChanged, changedBy";
    //     $where = "offense_ID = $offense_id";
    //     return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense');
    // }

    public function get_assigned_accounts()
    {
        $query = "SELECT DISTINCT(acc.acc_id) accId, acc.acc_name, acc.acc_description, COUNT(offTypeAss.offenseType_ID) offenseTypeCount FROM tbl_account acc, tbl_dms_offense_type_assignment offTypeAss WHERE acc.acc_id = offTypeAss.acc_id GROUP BY acc.acc_id";
        $record = $this->general_model->custom_query($query);
        if (count($record) > 0) {
            $data['exist'] = 1;
            $data['record'] = $record;
        } else {
            $data['exist'] = 0;
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function check_if_qip_record_exist()
    {
        $fields = "qipAssignment_ID";
        $record =  $this->general_model->fetch_all($fields, 'tbl_dms_qip_assignment');
        if (count($record) > 0) {
            $data['exist'] = 1;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function check_if_ero_record_exist()
    {
        $fields = "employeeRelationsOfficer_ID";
        $record =  $this->general_model->fetch_all($fields, 'tbl_dms_employee_relations_officer');
        if (count($record) > 0) {
            $data['exist'] = 1;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    protected function check_if_ir_history_record_exist($ir_id)
    {
        $fields = "incidentReport_ID";
        $where = "incidentReport_ID = $ir_id";
        $table = "tbl_dms_incident_report";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function check_if_off_type_ass_record_exist()
    {
        $fields = "offenseTypeAssignment_ID";
        $record =  $this->general_model->fetch_all($fields, 'tbl_dms_offense_type_assignment');
        if (count($record) > 0) {
            $data['exist'] = 1;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    // Recommendation Settings
    public function get_custom_recommendation_accounts()
    {
        $fields = "acc.acc_id, acc.acc_name ";
        $where = "acc.acc_id = recNum.account_ID";
        $table = "tbl_dms_account_recommendation_number recNum, tbl_account acc";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'acc_name ASC');
        if (count($record) > 0) {
            $data['exist'] = 1;
            $data['record'] = $record;
        } else {
            $data['exist'] = 0;
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function check_if_account_recommendation_num_exist()
    {
        $acc_rec_num_id = $this->input->post('recommendationNumId');
        $fields = "accountRecommendationNumber_ID";
        $where = "accountRecommendationNumber_ID = $acc_rec_num_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_account_recommendation_number');
        if (count($record) > 0) {
            $data['exist'] = 1;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function check_if_custom_recommendation_exist()
    {
        $cus_rec_id = $this->input->post('cusRecId');
        $fields = "cusRec.dmsCustomRecommendation_ID, cusRec.level, acc.acc_name";
        $where = "acc.acc_id = cusRec.account_ID AND dmsCustomRecommendation_ID = $cus_rec_id";
        $table = "tbl_dms_custom_recommendation cusRec, tbl_account acc";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        if (count($record) > 0) {
            $data['exist'] = 1;
            $data['record'] = $record;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_direct_supervisors()
    {
        $fields = "emp.emp_id, app.fname, app.mname, app.lname";
        $where = "app.apid = emp.apid AND emp.emp_id IN (SELECT DISTINCT(Supervisor) FROM tbl_employee WHERE isActive = 'yes')";
        $table = "tbl_employee emp, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
        if (count($record) > 0) {
            $data['exist'] = 1;
            $data['record'] = $record;
        } else {
            $data['exist'] = 0;
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_recommendation_level_by_account()
    {
        $acc_id = $this->input->post('accId');
        $fields = "accountRecommendationNumber_ID, number";
        $where = "account_ID = $acc_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_account_recommendation_number');
        if (count($record) > 0) {
            $data['exist'] = 1;
            $data['record'] = $record;
        } else {
            $data['exist'] = 0;
            $data['record'] = 0;
        }
        echo json_encode($data);
    }


    private function get_offense_disc_action_by_level($offense_id, $level, $disciplinaryActionCategorySettings_ID, $disciplineCategory_ID)
    {
        $offense_with_type = $this->get_offense_with_type($offense_id);
        if (count($offense_with_type) > 0) {
            $disciplinary_category = $disciplineCategory_ID;
            $fields = "discAction.disciplinaryAction_ID, discAction.action, discAction.periodMonth, discAction.abbreviation, discAction.label, discActionCateg.disciplineCategory_ID, discCateg.category, discActionCateg.disciplinaryActionCategory_ID,discActionCateg.level, discActionCateg.disciplinaryActionCategorySettings_ID";
            $where = "discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discActionCateg.disciplineCategory_ID = $disciplinary_category AND discActionCateg.level = $level AND discActionCateg.disciplinaryActionCategorySettings_ID = " . $disciplinaryActionCategorySettings_ID;
            $table = "tbl_dms_disciplinary_action discAction, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_discipline_category discCateg";
            return $this->general_model->fetch_specific_val($fields, $where, $table);
        } else {
            return NULL;
        }
    }
    private function get_offense_disc_action_by_level_category($offense_id, $level, $category)
    {
        $offense_with_type = $this->get_offense_with_type($offense_id);
        $category_settings = $this->get_current_disciplinary_action_category_settings();
        if (count($offense_with_type) > 0) {
            $disciplinary_category = $category;
            $fields = "discAction.disciplinaryAction_ID, discAction.action, discAction.periodMonth, discAction.abbreviation, discAction.label, discActionCateg.disciplineCategory_ID, discCateg.category, discActionCateg.disciplinaryActionCategory_ID, discActionCateg.level, discActionCateg.disciplinaryActionCategorySettings_ID";
            $where = "discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discActionCateg.disciplineCategory_ID = $disciplinary_category AND discActionCateg.level = $level AND discActionCateg.disciplinaryActionCategorySettings_ID = " . $category_settings->disciplinaryActionCategorySettings_ID;
            $table = "tbl_dms_disciplinary_action discAction, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_discipline_category discCateg";
            return $this->general_model->fetch_specific_val($fields, $where, $table);
        }
    }

    private function get_existing_ir_category($subject_emp_id, $prescriptive_stat, $liability_stat, $offense_ID, $category_id)
    {
        $dateTime = $this->get_current_date_time();
        $fields = "ir.incidentReport_ID, ir.subjectEmp_ID, ir.sourceEmp_ID, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.liabilityStat, ir.datedLiabilityStat, ir.subjectExplanation, ir.occurence, discCateg.disciplineCategory_ID, discCateg.category, discAction.disciplinaryAction_ID, discAction.action, discActionCateg.level, ir.offense_ID, ir.offense, ir.prescriptiveId";
        $where = "discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discActionCateg.disciplineCategory_ID=$category_id AND discActionCateg.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND ir.prescriptiveStat = $prescriptive_stat AND ir.subjectEmp_ID = $subject_emp_id AND ir.liabilityStat = $liability_stat AND ir.offense_ID = $offense_ID AND date(ir.prescriptionEnd) >='" . $dateTime['date'] . "'";
        $table = "tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_discipline_category discCateg";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }
    public function test_existing_ir()
    {
        $existing_ir = $this->get_existing_ir(557, 4, 29);
        ////var_dump($existing_ir);
    }


    public function get_max_offense_disc_action_by_level($offense_id)
    {
        $offense_with_type = $this->get_offense_with_type($offense_id);
        $category_settings = $this->get_current_disciplinary_action_category_settings();
        if (count($offense_with_type) > 0) {
            $multiCateg = explode(",", $offense_with_type->multiCategory);
            $disciplinary_category = $multiCateg[0];
            $query = "SELECT MAX(level) as level, disciplinaryAction_ID FROM tbl_dms_disciplinary_action_category WHERE disciplineCategory_ID = $disciplinary_category AND disciplinaryActionCategorySettings_ID = " . $category_settings->disciplinaryActionCategorySettings_ID . " GROUP BY disciplinaryAction_ID ORDER BY level DESC LIMIT 1";
            return $this->general_model->custom_query($query);
        } else {
            return NULL;
        }
    }

    // public function get_max_offense_disc_action_by_level_new($offense_id = 22){
    //     $offense_with_type = $this->get_offense_with_type($offense_id);
    //     $category_settings = $this->get_current_disciplinary_action_category_settings();
    //     if(count($offense_with_type) > 0){
    //         $multiCateg = explode(",", $offense_with_type->multiCategory);
    //         $disciplinary_category = $multiCateg[0];
    //         $fields = "MAX(level) as level, disciplinaryAction_ID";
    //         $where = "disciplineCategory_ID = $disciplinary_category AND disciplinaryActionCategorySettings_ID = ".$category_settings->disciplinaryActionCategorySettings_ID;
    //         return $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_disciplinary_action_category");
    //     }else{
    //         return NULL;
    //     }
    // }
    public function get_max_offense_disc_action_by_level_category($offense_id, $category_id)
    {
        $offense_with_type = $this->get_offense_with_type($offense_id);
        $category_settings = $this->get_current_disciplinary_action_category_settings();
        if (count($offense_with_type) > 0) {
            $disciplinary_category = $category_id;
            $fields = "MAX(level) as level, disciplinaryAction_ID";
            $where = "disciplineCategory_ID = $disciplinary_category AND disciplinaryActionCategorySettings_ID = " . $category_settings->disciplinaryActionCategorySettings_ID;
            return $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_disciplinary_action_category");
        } else {
            return NULL;
        }
    }

    private function get_existing_ir_pending($subject_emp_id, $prescriptive_stat, $liability_stat, $offense_ID)
    {
        $dateTime = $this->get_current_date_time();
        $fields = "ir.incidentReport_ID, ir.subjectEmp_ID, ir.sourceEmp_ID, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.liabilityStat, ir.datedLiabilityStat, ir.subjectExplanation, ir.occurence, discCateg.disciplineCategory_ID, discCateg.category, discAction.disciplinaryAction_ID, discAction.action, discActionCateg.disciplinaryActionCategory_ID, discActionCateg.level, ir.offense_ID, ir.offense, ir.prescriptiveId, ir.disciplinaryActionCategorySettings_ID";
        $where = "discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discActionCateg.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND ir.prescriptiveStat = $prescriptive_stat AND ir.subjectEmp_ID = $subject_emp_id AND ir.liabilityStat = $liability_stat AND ir.offense_ID = $offense_ID";
        $table = "tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_discipline_category discCateg";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    // DMS SUPERVISION
    public function get_specific_qualified_emp_ir()
    {
        $sup_emp_id = $this->session->userdata('emp_id');
        $violation_type_id = $this->input->post('violationType');
        $sched_date = $this->input->post('sched');
        $emp_where = "";
        $sched_date_where = "";
        $violation_type_where = "";
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        if ($sched_date !== '') {
            $sched_date_where = "AND userDtrVio.sched_date = '$sched_date'";
        }
        $fields = "DISTINCT(emp.emp_id), app.fname, app.mname, app.lname, app.nameExt";
        $where = "userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND dmsDead.record_ID = qualified.qualifiedUserDtrViolation_ID AND qualified.status_ID = 2 AND qualified.qualifiedUserDtrViolation_ID = supQualified.qualifiedUserDtrViolation_ID AND supQualified.emp_id = $sup_emp_id AND supQualified.status_ID = 4 AND dmsDead.status_ID = 2 " . $violation_type_where . " " . $sched_date_where;
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_employee emp, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType, tbl_dms_deadline dmsDead, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
        if (count($record) > 0) {
            $data['record'] = $record;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_qualified_emp_ir_monitoring()
    {
        $sup_emp_id = $this->session->userdata('emp_id');
        $violation_type_id = $this->input->post('violationType');
        $sched_date = $this->input->post('sched');
        $stat = $this->input->post('stat');
        $emp_where = "";
        $sched_date_where = "";
        $stat_where = "";
        $violation_type_where = "";
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        if ($sched_date !== '') {
            $sched_date_where = "AND userDtrVio.sched_date = '$sched_date'";
        }
        if ((int) $stat !== 0) {
            $stat_where = "AND supQualified.status_ID = $stat";
        } else {
            $stat_where = "AND supQualified.status_ID NOT IN (3, 20)";
        }
        $fields = "DISTINCT(emp.emp_id), app.fname, app.mname, app.lname, app.nameExt";
        $where = "app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND qualified.qualifiedUserDtrViolation_ID = supQualified.qualifiedUserDtrViolation_ID AND qualified.status_ID = 2 " . $stat_where . " " . $violation_type_where . " " . $sched_date_where;
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified, tbl_employee emp, tbl_applicant app, tbl_user_dtr_violation userDtrVio";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
        if (count($record) > 0) {
            $data['record'] = $record;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_qualified_emp_ir_record()
    {
        $sup_emp_id = $this->session->userdata('emp_id');
        $status_val = $this->input->post('status');
        $violation_type_id = $this->input->post('violationType');
        $date_start = $this->input->post('dateStart');
        $date_end = $this->input->post('dateEnd');
        $status_val_where = "";
        $violation_type_where = "";
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        if ((int) $status_val !== 0) {
            $status_val_where = "AND supQualified.status_ID = $status_val";
        } else {
            $status_val_where = "AND supQualified.status_ID IN (3, 12)";
        }
        $fields = "DISTINCT(emp.emp_id), app.fname, app.mname, app.lname, app.nameExt";
        $where = "userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND qualified.qualifiedUserDtrViolation_ID = supQualified.qualifiedUserDtrViolation_ID AND supQualified.emp_id = $sup_emp_id AND DATE(userDtrVio.sched_date) >= '" . $date_start . "' AND DATE(userDtrVio.sched_date) <= '" . $date_end . "' " . $violation_type_where . " " . $status_val_where;
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation userDtrVio, tbl_employee emp, tbl_applicant app, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
        if (count($record) > 0) {
            $data['record'] = $record;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_qualified_emp_ir_monitoring_record()
    {
        $violation_type_id = $this->input->post('violationType');
        $date_start = $this->input->post('dateStart');
        $date_end = $this->input->post('dateEnd');
        $status_val_where = "";
        $violation_type_where = "";
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        if ((int) $status_val !== 0) {
            $status_val_where = "AND qualified.status_ID = $status_val";
        } else {
            $status_val_where = "AND qualified.status_ID IN (3, 12)";
        }
        $fields = "DISTINCT(emp.emp_id), app.fname, app.mname, app.lname, app.nameExt";
        $where = "userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND DATE(userDtrVio.sched_date) >= '" . $date_start . "' AND DATE(userDtrVio.sched_date) <= '" . $date_end . "' AND qualified.status_ID = 3 " . $violation_type_where;
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation userDtrVio, tbl_employee emp, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
        if (count($record) > 0) {
            $data['record'] = $record;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_qualified_emp_ir_record_monitoring()
    {
        $violation_type_id = $this->input->post('violationType');
        $date_start = $this->input->post('dateStart');
        $date_end = $this->input->post('dateEnd');
        $violation_type_where = "";
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        $fields = "DISTINCT(emp.emp_id), app.fname, app.mname, app.lname, app.nameExt";
        $where = "userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND DATE(userDtrVio.sched_date) >= '" . $date_start . "' AND DATE(userDtrVio.sched_date) <= '" . $date_end . "' AND qualified.status_ID = 3 " . $violation_type_where;
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation userDtrVio, tbl_employee emp, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
        if (count($record) > 0) {
            $data['record'] = $record;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_specific_dismissed_qualified_emp()
    {
        $violation_type_id = $this->input->post('violationType');
        $date_start = $this->input->post('dateStart');
        $date_end = $this->input->post('dateEnd');
        $violation_type_where = "";
        // if ((int) $violation_type_id !== 0) {
        //     $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        // }
        $fields = "DISTINCT(emp.emp_id), app.fname, app.mname, app.lname, app.nameExt";
        $where = "userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND DATE(userDtrVio.sched_date) >= '" . $date_start . "' AND DATE(userDtrVio.sched_date) <= '" . $date_end . "' AND qualified.status_ID = 20 " . $violation_type_where;
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation userDtrVio, tbl_employee emp, tbl_applicant app";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
        if (count($record) > 0) {
            $data['record'] = $record;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }




    // get irable record
    // SELECT app.fname, app.mname, app.lname, app.nameExt, userDtrVio.dtrViolationType_ID, dtrVioType.description, userDtrVio.sched_date, dmsDead.deadline FROM tbl_dms_qualified_user_dtr_violation qualified, tbl_employee emp, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType, tbl_dms_deadline dmsDead WHERE app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND dtrVioType.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND dmsDead.record_ID = qualified.qualifiedUserDtrViolation_ID
    public function check_if_date($value)
    {
        $date_bool = 0;
        // echo ucfirst($value);
        $formats = ['M d, Y', 'Y-m-d'];
        foreach ($formats as $f) {
            $d = DateTime::createFromFormat($f, ucfirst($value));
            $is_date = $d && $d->format($f) === ucfirst($value);
            if (true == $is_date) {
                $date_bool = 1;
            }
        }
        return $date_bool;
    }

    public function convert_date($dateVal)
    {
        $date = date_create($dateVal);
        return date_format($date, "Y-m-d");
    }

    public function get_irable_dtr_violation_datatable()
    {
        $datatable = $this->input->post('datatable');
        $violation_type_id = $datatable['query']['violationType'];
        $emp_id = $datatable['query']['empId'];
        $sched_date = $datatable['query']['sched'];
        $sup_emp_id = $this->session->userdata('emp_id');
        $emp_where = "";
        $violation_type_where = "";
        $sched_date_where = "";
        if ((int) $emp_id !== 0) {
            $emp_where = "AND qualified.emp_id = $emp_id";
        }
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        if ($sched_date !== '') {
            $sched_date_where = "AND userDtrVio.sched_date = '$sched_date'";
        }
        $query['query'] = "SELECT DISTINCT(qualified.qualifiedUserDtrViolation_ID), app.fname, app.mname, app.lname, app.nameExt, app.pic, userDtrVio.dtrViolationType_ID, dtrVioType.description, userDtrVio.sched_date, dmsDead.deadline FROM tbl_dms_qualified_user_dtr_violation qualified, tbl_employee emp, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType, tbl_dms_deadline dmsDead, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified WHERE app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND dtrVioType.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND dmsDead.record_ID = qualified.qualifiedUserDtrViolation_ID AND dmsDead.dmsDeadlineType_ID = 1 AND qualified.status_ID = 2 AND qualified.qualifiedUserDtrViolation_ID = supQualified.qualifiedUserDtrViolation_ID AND supQualified.emp_id = $sup_emp_id  AND supQualified.status_ID = 4 AND dmsDead.status_ID = 2 " . $emp_where . " " . $violation_type_where . " " . $sched_date_where;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_irable_dtr_violation_monitoring_datatable()
    {
        $datatable = $this->input->post('datatable');
        $violation_type_id = $datatable['query']['violationType'];
        $stat = $datatable['query']['stat'];
        $emp_id = $datatable['query']['empId'];
        $sched_date = $datatable['query']['sched'];
        $emp_where = "";
        $stat_where = "";
        $violation_type_where = "";
        $sched_date_where = "";
        if ((int) $emp_id !== 0) {
            $emp_where = "AND qualified.emp_id = $emp_id";
        }
        if ((int) $stat !== 0) {
            $stat_where = "AND supQualified.status_ID = $stat";
        }
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        if ($sched_date !== '') {
            $sched_date_where = "AND userDtrVio.sched_date = '$sched_date'";
        }
        $query['query'] = "SELECT qualified.qualifiedUserDtrViolation_ID, app.fname, app.mname, app.lname, app.nameExt, app.pic, userDtrVio.dtrViolationType_ID, dtrVioType.description, userDtrVio.sched_date, emp2.emp_id supEmp_id, app2.fname supFname, app2.mname supMname, app2.lname supLname, app2.pic supPic, qualified.status_ID, supQualified.status_ID supStatus_ID FROM tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified, tbl_employee emp, tbl_employee emp2, tbl_applicant app2, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType WHERE app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND app2.apid = emp2.apid AND emp2.emp_id = supQualified.emp_id AND supQualified.qualifiedUserDtrViolation_ID = qualified.qualifiedUserDtrViolation_ID AND dtrVioType.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.status_ID = 2 " . $emp_where . " " . $violation_type_where . " " . $sched_date_where . " " . $stat_where;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_irable_dtr_violation_record_datatable()
    {
        $datatable = $this->input->post('datatable');
        $violation_type_id = $datatable['query']['violationType'];
        $emp_id = $datatable['query']['empId'];
        $status_val = $datatable['query']['status'];
        $date_start = $datatable['query']['dateStart'];
        $date_end = $datatable['query']['dateEnd'];
        $sup_emp_id = $this->session->userdata('emp_id');
        $emp_where = "";
        $status_val_where = "";
        $violation_type_where = "";
        if ((int) $emp_id !== 0) {
            $emp_where = "AND qualified.emp_id = $emp_id";
        }
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        if ((int) $status_val !== 0) {
            $status_val_where = "AND supQualified.status_ID = $status_val";
        } else {
            $status_val_where = "AND supQualified.status_ID IN (3, 12)";
        }
        $query['query'] = "SELECT DISTINCT(qualified.qualifiedUserDtrViolation_ID) qualifiedUserDtrViolation_ID, app.fname, app.mname, app.lname, app.nameExt, app.pic, userDtrVio.dtrViolationType_ID, dtrVioType.description, userDtrVio.sched_date, dmsDead.deadline, stat.description as statDescription, supQualified.status_ID FROM tbl_dms_qualified_user_dtr_violation qualified, tbl_employee emp, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType, tbl_dms_deadline dmsDead, tbl_status stat, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified WHERE app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND dtrVioType.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND dmsDead.record_ID = qualified.qualifiedUserDtrViolation_ID AND qualified.qualifiedUserDtrViolation_ID = supQualified.qualifiedUserDtrViolation_ID AND supQualified.emp_id = $sup_emp_id AND stat.status_ID = supQualified.status_ID AND DATE(userDtrVio.sched_date) >= '" . $date_start . "' AND DATE(userDtrVio.sched_date) <= '" . $date_end . "' " . $emp_where . " " . $violation_type_where . " " . $status_val_where . " GROUP BY qualified.qualifiedUserDtrViolation_ID";
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_irable_dtr_violation_record_monitoring_datatable()
    {
        $datatable = $this->input->post('datatable');
        $violation_type_id = $datatable['query']['violationType'];
        $emp_id = $datatable['query']['empId'];
        $date_start = $datatable['query']['dateStart'];
        $date_end = $datatable['query']['dateEnd'];
        $emp_where = "";
        $status_val_where = "";
        $violation_type_where = "";
        if ((int) $emp_id !== 0) {
            $emp_where = "AND qualified.emp_id = $emp_id";
        }
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }

        $query['query'] = "SELECT qualified.qualifiedUserDtrViolation_ID, app.fname, app.mname, app.lname, app.nameExt, app.pic, userDtrVio.dtrViolationType_ID, dtrVioType.description, userDtrVio.sched_date, emp2.emp_id supEmp_id, app2.fname supFname, app2.mname supMname, app2.lname supLname, app2.pic supPic, qualified.status_ID FROM tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified, tbl_employee emp, tbl_employee emp2, tbl_applicant app2, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType WHERE app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND app2.apid = emp2.apid AND emp2.emp_id = supQualified.emp_id AND supQualified.status_ID = 3 AND supQualified.qualifiedUserDtrViolation_ID = qualified.qualifiedUserDtrViolation_ID AND dtrVioType.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.status_ID = 3 AND DATE(userDtrVio.sched_date) >= '" . $date_start . "' AND DATE(userDtrVio.sched_date) <= '" . $date_end . "' " . $emp_where . " " . $violation_type_where;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_dismissed_dtr_violation_datatable()
    {
        $datatable = $this->input->post('datatable');
        $violation_type_id = $datatable['query']['violationType'];
        $emp_id = $datatable['query']['empId'];
        $date_start = $datatable['query']['dateStart'];
        $date_end = $datatable['query']['dateEnd'];
        $emp_where = "";
        $status_val_where = "";
        $violation_type_where = "";
        if ((int) $emp_id !== 0) {
            $emp_where = "AND qualified.emp_id = $emp_id";
        }
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }

        $query['query'] = "SELECT qualified.qualifiedUserDtrViolation_ID, qualified.emp_id, app.fname, app.mname, app.lname, app.nameExt, app.pic, userDtrVio.dtrViolationType_ID, dtrVioType.description, userDtrVio.sched_date, emp2.emp_id supEmp_id, app2.fname supFname, app2.mname supMname, app2.lname supLname, app2.pic supPic, qualified.status_ID FROM tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified, tbl_employee emp, tbl_employee emp2, tbl_applicant app2, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType WHERE app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND app2.apid = emp2.apid AND emp2.emp_id = supQualified.emp_id AND supQualified.status_ID = 20 AND supQualified.qualifiedUserDtrViolation_ID = qualified.qualifiedUserDtrViolation_ID AND dtrVioType.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.status_ID = 20 AND DATE(userDtrVio.sched_date) >= '" . $date_start . "' AND DATE(userDtrVio.sched_date) <= '" . $date_end . "' " . $emp_where . " " . $violation_type_where;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    // DMS DISMISSED RECORDS FOR SUPERVISION
    public function get_dismissed_dtr_violation_datatable_supervision()
    {
        $datatable = $this->input->post('datatable');
        $ses_emp_id = $this->session->userdata('emp_id');
        $violation_type_id = $datatable['query']['violationType'];
        $emp_id = $datatable['query']['empId'];
        $date_start = $datatable['query']['dateStart'];
        $date_end = $datatable['query']['dateEnd'];
        $emp_where = "";
        $status_val_where = "";
        $violation_type_where = "";
        if ((int) $emp_id !== 0) {
            $emp_where = "AND qualified.emp_id = $emp_id";
        }
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        $query['query'] = "SELECT qualified.qualifiedUserDtrViolation_ID, qualified.emp_id, app.fname, app.mname, app.lname, app.nameExt, app.pic, userDtrVio.dtrViolationType_ID, dtrVioType.description, userDtrVio.sched_date,qualified.status_ID FROM tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_dismiss_qualified_dtr_violation dismissq, tbl_employee emp, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType WHERE dismissq.emp_id=$ses_emp_id AND dismissq.qualifiedUserDtrViolation_ID=qualified.qualifiedUserDtrViolation_ID AND userDtrVio.dtrViolationType_ID=dtrVioType.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID=qualified.userDtrViolation_ID AND app.apid=emp.apid AND emp.emp_id=qualified.emp_id AND DATE(userDtrVio.sched_date) >= '" . $date_start . "' AND DATE(userDtrVio.sched_date) <= '" . $date_end . "' " . $emp_where . " " . $violation_type_where;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_dismissed_dtr_violation_data($emp_id, $violation_type_id, $date_start, $date_end){
        $emp_where = "";
        $violation_type_where = "";
        if ((int) $emp_id !== 0) {
            $emp_where = "AND qualified.emp_id = $emp_id";
        }
        if ((int) $violation_type_id !== 0) {
            $violation_type_where = "AND userDtrVio.dtrViolationType_ID = $violation_type_id";
        }
        $qry = "SELECT qualified.qualifiedUserDtrViolation_ID, qualified.emp_id, qualified.userDtrViolation_ID, app.fname, app.mname, app.lname, app.nameExt, app.pic, userDtrVio.dtrViolationType_ID, dtrVioType.description, userDtrVio.sched_date, emp2.emp_id supEmp_id, app2.fname supFname, app2.mname supMname, app2.lname supLname, app2.pic supPic, qualified.status_ID FROM tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified, tbl_employee emp, tbl_employee emp2, tbl_applicant app2, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType WHERE app.apid = emp.apid AND emp.emp_id = qualified.emp_id AND app2.apid = emp2.apid AND emp2.emp_id = supQualified.emp_id AND supQualified.status_ID = 20 AND supQualified.qualifiedUserDtrViolation_ID = qualified.qualifiedUserDtrViolation_ID AND dtrVioType.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.status_ID = 20 AND DATE(userDtrVio.sched_date) >= '" . $date_start . "' AND DATE(userDtrVio.sched_date) <= '" . $date_end . "' " . $emp_where . " " . $violation_type_where;
        return $this->general_model->custom_query($qry);
    }

    private function columnLetter($c)
	{
		$c = intval($c);
		if ($c <= 0) return '';
		$letter = '';

		while ($c != 0) {
			$p = ($c - 1) % 26;
			$c = intval(($c - $p) / 26);
			$letter = chr(65 + $p) . $letter;
		}
		return $letter;
	}

    public function export_dismissed_dtr_vio(){
        $emp_id = $this->input->post('emp_id'); 
        $violation_type_id = $this->input->post('vio_type_id'); 
        $date_start = $this->input->post('date_start'); 
        $date_end = $this->input->post('date_end');
        $dismissed_dtr_data = $this->get_dismissed_dtr_violation_data($emp_id, $violation_type_id, $date_start, $date_end);

        $logo = $this->dir . '/assets/images/img/logo2.png';
		$this->load->library('PHPExcel', null, 'excel');
		$this->excel->createSheet(0);
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Dismissed DTR Violation');
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($logo);
		$objDrawing->setOffsetX(5); // setOffsetX works properly
		$objDrawing->setOffsetY(5); //setOffsetY has no effect
		$objDrawing->setCoordinates('A1');
		$objDrawing->setHeight(60); // logo height
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
		$this->excel->getActiveSheet()->setShowGridlines(false);
        // var_dump($dismissed_dtr_data);
        $main_col = [
			['title' => 'Q-ID'],
			['title' => 'Last Name'],
			['title' => 'First Name'],
			['title' => 'Middle Name'],
			['title' => 'Schedule'],
			['title' => 'DTR Violation'],
			['title' => 'Duration'],
			['title' => 'Dismissed by'],
			['title' => 'Dismissed on'],
			['title' => 'Reason'],
        ];
        $headers = [];
        $letter_num = 1;
        for($main_col_loop = 0; $main_col_loop < count($main_col); $main_col_loop++) {
			$headers[$main_col_loop] = [
				'col' => $this->columnLetter($letter_num),
				'id' => $this->columnLetter($letter_num) . '6',
				'title' => $main_col[$main_col_loop]['title'],
			];
			// $orig_cell_num_start++;
			$letter_num++;
        }
        for ($excel_data_header_loop = 0; $excel_data_header_loop < count($headers); $excel_data_header_loop++) {
			// echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
			$this->excel->getActiveSheet()->setCellValue($headers[$excel_data_header_loop]['id'], $headers[$excel_data_header_loop]['title']);
			$this->excel->getActiveSheet()->getStyle($headers[$excel_data_header_loop]['id'])->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle($headers[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
			$this->excel->getActiveSheet()->getStyle($headers[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getColumnDimension($headers[$excel_data_header_loop]['col'])->setWidth(20);
		}
        $this->excel->getActiveSheet()->getStyle($headers[0]['id'] . ':' . $headers[count($headers) - 1]['id'])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
        $rowNum = 7;
        foreach($dismissed_dtr_data as $dismissed_dtr_data_row){
            $qualified_record = $this->get_qualified_dtr_violation($dismissed_dtr_data_row->qualifiedUserDtrViolation_ID);
            $user_dtr_vio = $this->get_user_dtr_violation($dismissed_dtr_data_row->userDtrViolation_ID);
            $shift = explode('-', $user_dtr_vio->shift);
            $dated_shift = $this->get_dated_shift($user_dtr_vio->sched_date, trim($shift[0]), trim($shift[1]));
            $dismiss_reason = $this->get_dismiss_reason($dismissed_dtr_data_row->qualifiedUserDtrViolation_ID);
            $dismissed_by = $this->get_emp_details_via_emp_id($dismiss_reason->emp_id);;
            $incident_details = $this->get_incident_details($user_dtr_vio, $qualified_record->occurrenceValue); // Incident Details
            $duration = '-';
            if($user_dtr_vio->dtrViolationType_ID == 1 || $user_dtr_vio->dtrViolationType_ID == 2 || $user_dtr_vio->dtrViolationType_ID == 3){
                $duration = $incident_details['duration'];
            }else if($user_dtr_vio->dtrViolationType_ID == 4){
                $duration = $incident_details['excess'];
            }
            $this->excel->getActiveSheet()->setCellValue('A' . $rowNum, str_pad($dismissed_dtr_data_row->qualifiedUserDtrViolation_ID, 8, '0', STR_PAD_LEFT));
            $this->excel->getActiveSheet()->setCellValue('B' . $rowNum, $dismissed_dtr_data_row->lname);
            $this->excel->getActiveSheet()->setCellValue('C' . $rowNum, $dismissed_dtr_data_row->fname);
            $this->excel->getActiveSheet()->setCellValue('D' . $rowNum, $dismissed_dtr_data_row->mname);
            $this->excel->getActiveSheet()->setCellValue('E' . $rowNum, $dismissed_dtr_data_row->sched_date);
            $this->excel->getActiveSheet()->setCellValue('F' . $rowNum, $dismissed_dtr_data_row->description);
            $this->excel->getActiveSheet()->setCellValue('G' . $rowNum, $duration);
            $this->excel->getActiveSheet()->setCellValue('H' . $rowNum, $dismissed_by->lname.", ".$dismissed_by->fname);
            $this->excel->getActiveSheet()->setCellValue('I' . $rowNum, date_format(date_create($dismiss_reason->dateTime), "Y-m-d g:i:s A"));
            $this->excel->getActiveSheet()->setCellValue('J' . $rowNum, $dismiss_reason->reason);
            $rowNum++;
        }
        for($cell_loop = 0; $cell_loop < count($headers); $cell_loop++) {
            $this->excel->getActiveSheet()->getColumnDimension($headers[$cell_loop]['col'])->setAutoSize(true);
        }

        $this->excel->getActiveSheet()->getStyle('A6:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => 'DDDDDD'),
					),
				),
			)
		);

		$this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
		$this->excel->getActiveSheet()->getProtection()->setSheet(true);

		$filename = 'Dismissed DTR Violations.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		// header('Set-Cookie: fileDownload=true; path=/');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
    }

    // DMS COUNTS---------------------
    //  DTR VIOLATIONS TAB  
    public function get_file_dtr_violation_ir_count()
    {
        $sup_emp_id = $this->session->userdata('emp_id');
        $fields = "COUNT(qualified.qualifiedUserDtrViolation_ID) as total";
        $where = "userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.status_ID = 2 AND qualified.qualifiedUserDtrViolation_ID = supQualified.qualifiedUserDtrViolation_ID AND supQualified.emp_id = $sup_emp_id  AND supQualified.status_ID = 4";
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified, tbl_user_dtr_violation userDtrVio";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        $data['count'] = $record->total;
        echo json_encode($data);
    }

    public function get_file_dtr_violation_ir_count_monitoring()
    {
        $fields = "COUNT(qualified.qualifiedUserDtrViolation_ID) as total";
        $where = "qualified.status_ID IN (2, 12)";
        $table = "tbl_dms_qualified_user_dtr_violation qualified";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        $data['count'] = $record->total;
        echo json_encode($data);
    }

    public function get_distinct_custom_recommendation_emp()
    {
        $fields = "DISTINCT(cusRec.emp_id) emp_id, app.fname, app.mname, app.lname";
        $where = "app.apid = emp.apid AND emp.emp_id = cusRec.emp_id";
        $table = "tbl_dms_custom_recommendation cusRec, tbl_applicant app, tbl_employee emp";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
        if (count($record) > 0) {
            $data['exist'] = 1;
            $data['record'] = $record;
        } else {
            $data['exist'] = 0;
            $data['record'] = 0;
        }
        echo json_encode($data);
    }

    public function get_assign_to_recommendation($custom_rec_id)
    {
        $fields = "recChange.emp_id toEmpId, app.fname toFname, app.mname toMname, app.lname toLname";
        $where = "app.apid = emp.apid AND emp.emp_id = recChange.emp_id AND recChange.dmsCustomRecommendation_ID = $custom_rec_id";
        $table = "tbl_dms_custom_recommendation_change recChange, tbl_applicant app, tbl_employee emp";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function check_if_already_assigned_recommendation_change()
    {
        $acc_id = $this->input->post('accId');
        $level = $this->input->post('level');
        $emp_id = $this->input->post('recommender');
        $assignTo = $this->input->post('assignTo');
        $changeType = $this->input->post('changeType');
        $fields = "cusRec.dmsCustomRecommendation_ID, cusRec.emp_id, app.fname, app.mname, app.lname, acc.acc_name, cusRec.level";
        $where = "app.apid = emp.apid AND emp.emp_id = cusRec.emp_id AND acc.acc_id = cusRec.account_ID AND cusRec.account_ID = $acc_id AND cusRec.level = $level AND cusRec.changeType = '$changeType' AND cusRec.emp_id = $emp_id";
        $table = "tbl_dms_custom_recommendation cusRec, tbl_applicant app, tbl_employee emp, tbl_account acc";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        if (count($record) > 0) {
            $data['exist'] = 1;

            if ($changeType == "change") {
                $assign_to_obj = $this->get_assign_to_recommendation($record->dmsCustomRecommendation_ID);
                if (count($assign_to_obj) > 0) {
                    $record->{"toEmpId"} = $assign_to_obj->toEmpId;
                    $record->{"toLname"} = $assign_to_obj->toLname;
                    $record->{"toFname"} = $assign_to_obj->toFname;
                    $record->{"toMname"} = $assign_to_obj->toMname;
                } else {
                    $this->delete_assigned_to_recommmendation($record->dmsCustomRecommendation_ID);
                }
            }
            $data['record'] = $record;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }



    private function check_if_pending_ir_exist($dtr_vio_type, $emp_id)
    {
        $fields = "ir.incidentReport_ID, userDtrVio.dtrViolationType_ID";
        $where = "userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.qualifiedUserDtrViolation_ID = autoIr.qualifiedUserDtrViolation_ID AND autoIr.incidentReport_ID = ir.incidentReport_ID AND ir.subjectEmp_ID = $emp_id AND userDtrVio.dtrViolationType_ID = $dtr_vio_type";
        $table = "tbl_dms_incident_report ir, tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_auto_ir_related_details autoIr, tbl_user_dtr_violation userDtrVio";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        if (count($record) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    private function get_specific_recommendation_account($emp_id, $change_type)
    {
        $fields = "DISTINCT(cusRec.account_ID) acc_id, acc.acc_name";
        $where = "acc.acc_id = cusRec.account_ID AND cusRec.changeType = '$change_type' AND cusRec.emp_id = $emp_id";
        $table = "tbl_dms_custom_recommendation cusRec, tbl_account acc";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    public function get_custom_recommendation_info()
    {
        $emp_id = $this->input->post('empId');
        $change_type = $this->input->post('changeType');
        $emp_details = $this->get_emp_details($emp_id);
        $spec_rec_account = $this->get_specific_recommendation_account($emp_id, $change_type);
        if (count($spec_rec_account) > 0) {
            $data['exist'] = 1;
            $data['emp_details'] = $emp_details;
            $data['accounts'] = $spec_rec_account;
        } else {
            $data['exist'] = 0;
        }
        echo json_encode($data);
    }

    public function get_reassignment_datatable()
    {
        $datatable = $this->input->post('datatable');
        $rec_emp_id = $datatable['query']['empId'];
        $acc_id = $datatable['query']['accId'];
        $acc_where = "";
        if ((int) $acc_id !== 0) {
            $acc_where = "AND dmsCusRec.account_ID = $acc_id";
        }
        $query['query'] = "SELECT acc.acc_id, acc.acc_name, dmsCusRec.level, dmsCusRec.changeType, dmsCusRec.dmsCustomRecommendation_ID, dmsCusRecChange.dmsCustomRecommendationChange_ID, dmsCusRecChange.emp_id, app.fname, app.mname, app.lname FROM tbl_dms_custom_recommendation dmsCusRec, tbl_dms_custom_recommendation_change dmsCusRecChange, tbl_applicant app, tbl_employee emp, tbl_account acc WHERE acc.acc_id = dmsCusRec.account_ID AND app.apid = emp.apid AND emp.emp_id = dmsCusRecChange.emp_id AND dmsCusRecChange.dmsCustomRecommendation_ID = dmsCusRec.dmsCustomRecommendation_ID AND dmsCusRec.emp_id = $rec_emp_id AND dmsCusRec.changeType = 'change' " . $acc_where;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function get_exemption_datatable()
    {
        $datatable = $this->input->post('datatable');
        $rec_emp_id = $datatable['query']['empId'];
        $acc_id = $datatable['query']['accId'];
        $acc_where = "";
        if ((int) $acc_id !== 0) {
            $acc_where = "AND dmsCusRec.account_ID = $acc_id";
        }
        $query['query'] = "SELECT acc.acc_id, acc.acc_name, dmsCusRec.level, dmsCusRec.changeType, dmsCusRec.dmsCustomRecommendation_ID FROM tbl_dms_custom_recommendation dmsCusRec, tbl_account acc WHERE acc.acc_id = dmsCusRec.account_ID AND dmsCusRec.emp_id = $rec_emp_id AND dmsCusRec.changeType = 'exempt' " . $acc_where;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function check_specific_custom_change_record()
    {
        $emp_id = $this->input->post('empId');
        $type = $this->input->post('type');
        $fields = "COUNT(dmsCustomRecommendation_ID) customRecCount";
        $where = "changeType = '$type' AND emp_id = $emp_id";
        $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_custom_recommendation");
        echo json_encode($record);
    }
   
    private function get_user_dtr_volation_logs($user_dtr_violation_id, $type, $entry)
    {
        $type_where = "";
        $entry_where = "";
        if ($type != "") {
            $type_where = " AND type = '$type'";
        }
        if ($entry != "") {
            $entry_where = " AND entry = '$entry'";
        }
        $fields = "userDtrViolationDetails_ID, dtr_id, log, type, entry, minDuration";
        $where = "userDtrViolation_ID = $user_dtr_violation_id" . $type_where . $entry_where;
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_user_dtr_violation_details');
    }

    private function get_violation_allowable_breaks($user_dtr_violation_id)
    {
        $fields = "	userDtrViolationAllowableBreak_ID, breakType, duration";
        $where = "userDtrViolation_ID = $user_dtr_violation_id";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_user_dtr_violation_allowable_break');
    }

    public function break_test()
    {
        $user_dtr_vio_id = 1;
        $allowable_break = $this->get_violation_allowable_breaks($user_dtr_vio_id);
        $missing_logs = [];
        $missing_logs_count = 0;
        if (count($allowable_break) > 0) {
            foreach ($allowable_break as $allowable_row) {
                $out_break = "";
                $in_break = "";
                $break_logs = $this->get_user_dtr_volation_logs($user_dtr_vio_id, $allowable_row->breakType, "");
                if (count($break_logs) > 0) {
                    foreach ($break_logs as $break_logs_row) {
                        if ($break_logs_row->entry == "O") {
                            $out_break = $break_logs_row->log;
                        } else {
                            $in_break = $break_logs_row->log;
                        }
                    }
                    if ($out_break == "" && $in_break != "") {
                        $missing_logs[$missing_logs_count] = "BREAK-OUT Log for " . ucfirst($allowable_row->breakType);
                        $missing_logs_count++;
                    } else if ($out_break != "" && $in_break == "") {

                        $missing_logs[$missing_logs_count] = "BREAK-IN Log for " . ucfirst($allowable_row->breakType);
                        $missing_logs_count++;
                    } else if ($out_break == "" && $in_break == "") {
                        $missing_logs[$missing_logs_count] = "BOTH Logs for " . ucfirst($allowable_row->breakType);
                        $missing_logs_count++;
                    }
                }
                if ($out_break == "" && $in_break != "") {
                    $missing_logs[$missing_logs_count] = "BREAK-OUT Log for " . ucfirst($allowable_row->breakType);
                    $missing_logs_count++;
                } else if ($out_break != "" && $in_break == "") {

                    $missing_logs[$missing_logs_count] = "BREAK-IN Log for " . ucfirst($allowable_row->breakType);
                    $missing_logs_count++;
                } else if ($out_break == "" && $in_break == "") {
                    $missing_logs[$missing_logs_count] = "BOTH Logs for " . ucfirst($allowable_row->breakType);
                    $missing_logs_count++;
                }
            }
        }
        $logs = $this->get_user_dtr_volation_logs($user_dtr_vio_id, "DTR", "");
        if (count($logs) > 0) {
            $clock_in = "";
            $clock_out = "";
            foreach ($logs as $logs_row) {
                if ($logs_row->entry == "I") {
                    $clock_in = $logs_row->log;
                } else {
                    $clock_out = $logs_row->log;
                }
            }
            if ($clock_in == "") {
                array_unshift($missing_logs, "CLOCK IN Log");
            }
            if ($clock_out == "") {
                array_push($missing_logs, "CLOCK OUT Log");
            }
        } else {
            array_unshift($missing_logs, "CLOCK IN Log");
            array_push($missing_logs, "CLOCK OUT Log");
        }
        ////var_dump($missing_logs);
    }

    private function get_incident_details($user_dtr_violation_obj, $occurrenceValue)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //LATE
        {
            $logs = $this->get_user_dtr_volation_logs($user_dtr_violation_obj->userDtrViolation_ID, "DTR", "I");
            if (count($logs) > 0) {
                $data['log'] = $logs[0]->log;
                $data['log_label'] = "Clock In";
                $data['duration_min'] = $user_dtr_violation_obj->duration;
                $data['duration'] = $this->get_formatted_time($user_dtr_violation_obj->duration / 60);
            } else {
                $data['error'] = 1;
                $data['error_details'] = [
                    'error' => "login log not found",
                    'origin' => 'get_incident_details (late)',
                    'process' => 'getting of login log',
                ];
            }
        } else if (((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) || ((int) $user_dtr_violation_obj->dtrViolationType_ID == 3)) // UT & Forgot to Logout
        {
            if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) {
                $data['allowable_logout'] = $occurrenceValue;
                $data['allowable_logout_formatted'] = $this->get_formatted_time($occurrenceValue / 60);
            }
            $logs = $this->get_user_dtr_volation_logs($user_dtr_violation_obj->userDtrViolation_ID, "DTR", "O");
            if (count($logs) > 0) {
                $data['log'] = $logs[0]->log;
                $data['log_label'] = "Clock Out";
                $data['duration_min'] = $user_dtr_violation_obj->duration;
                $data['duration'] = $this->get_formatted_time($user_dtr_violation_obj->duration / 60);
            } else {
                $data['error'] = 1;
                $data['error_details'] = [
                    'error' => "logout log not found",
                    'origin' => 'get_incident_details (UT)',
                    'process' => 'getting of logout log',
                ];
            }
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
        {
            // get_allowable_breaks
            $allowable_break = $this->get_violation_allowable_breaks($user_dtr_violation_obj->userDtrViolation_ID);
            $break_details = [];
            $break_count = 0;
            $total_allowable = 0;
            $total_covered = 0;
            if (count($allowable_break) > 0) {
                foreach ($allowable_break as $allowable_row) {
                    $out_break = NULL;
                    $in_break = NULL;
                    $duration = 0;
                    $logs = $this->get_user_dtr_volation_logs($user_dtr_violation_obj->userDtrViolation_ID, $allowable_row->breakType, "");
                    if (count($logs) > 0) {
                        foreach ($logs as $logs_row) {
                            if ($logs_row->entry == "O") {
                                $out_break = $logs_row->log;
                            } else {
                                $in_break = $logs_row->log;
                                $duration = $logs_row->minDuration;
                            }
                        }
                    }
                    $total_allowable += (int) $allowable_row->duration;
                    $total_covered += (int) $duration;
                    $break_details[$break_count] = [
                        "type" => $allowable_row->breakType,
                        "allowable_mins" => $allowable_row->duration,
                        "allowable" => $this->get_formatted_time($allowable_row->duration / 60),
                        "out" => $out_break,
                        "in" => $in_break,
                        "covered_mins" => $duration,
                        "covered" => $this->get_formatted_time($duration / 60),
                    ];
                    $break_count++;
                }
                $data['break'] = $break_details;
                $data['total_covered_min'] = $total_covered;
                $data['total_covered'] = $this->get_formatted_time(($total_covered) / 60);
                $data['total_allowable_min'] = $total_allowable;
                $data['total_allowable'] = $this->get_formatted_time($total_allowable / 60);
                $data['excess_min'] = $total_covered - $total_allowable;
                $data['excess'] = $this->get_formatted_time(($total_covered - $total_allowable) / 60);
            } else {
                $data['error'] = 1;
                $data['error_details'] = [
                    'error' => "allowable breaks not found",
                    'origin' => 'get_incident_details (OB)',
                    'process' => 'getting of allowable break',
                ];
            }
        } else if (((int) $user_dtr_violation_obj->dtrViolationType_ID == 6) || ((int) $user_dtr_violation_obj->dtrViolationType_ID == 7)) // INC BREAK
        {
            $allowable_break = $this->get_violation_allowable_breaks($user_dtr_violation_obj->userDtrViolation_ID);
            $missing_logs = [];
            $missing_logs_count = 0;
            $data['missing_num'] = 0;
            // ////var_dump($allowable_break);
            if (count($allowable_break) > 0) {
                foreach ($allowable_break as $allowable_row) {
                    $out_break = "";
                    $in_break = "";
                    $break_logs = $this->get_user_dtr_volation_logs($user_dtr_violation_obj->userDtrViolation_ID, $allowable_row->breakType, "");
                    if (count($break_logs) > 0) {
                        foreach ($break_logs as $break_logs_row) {
                            if ($break_logs_row->entry == "O") {
                                $out_break = $break_logs_row->log;
                            } else {
                                $in_break = $break_logs_row->log;
                            }
                        }
                    }
                    if ($out_break == "" && $in_break != "") {
                        $missing_logs[$missing_logs_count] = "BREAK-OUT Log for " . ucfirst($allowable_row->breakType);
                        $missing_logs_count++;
                        $data['missing_num'] = 1;
                    } else if ($out_break != "" && $in_break == "") {
                        $data['missing_num'] = 1;
                        $missing_logs[$missing_logs_count] = "BREAK-IN Log for " . ucfirst($allowable_row->breakType);
                        $missing_logs_count++;
                    } else if ($out_break == "" && $in_break == "") {
                        $data['missing_num'] = 2;
                        $missing_logs[$missing_logs_count] = "BOTH Logs for " . ucfirst($allowable_row->breakType);
                        $missing_logs_count++;
                    }
                }
            }
            $logs = $this->get_user_dtr_volation_logs($user_dtr_violation_obj->userDtrViolation_ID, "DTR", "");
            // echo $user_dtr_violation_obj->userDtrViolation_ID;
            // ////var_dump($logs);
            if (count($logs) > 0) {
                $clock_in = "";
                $clock_out = "";
                foreach ($logs as $logs_row) {
                    if ($logs_row->entry == "I") {
                        $clock_in = $logs_row->log;
                    } else {
                        $clock_out = $logs_row->log;
                    }
                }
                if ($clock_in == "") {
                    array_unshift($missing_logs, "CLOCK IN Log");
                }
                if ($clock_out == "") {
                    array_push($missing_logs, "CLOCK OUT Log");
                }
            } else {
                array_unshift($missing_logs, "CLOCK IN Log");
                array_push($missing_logs, "CLOCK OUT Log");
            }
            if (count($missing_logs) > 0) {
                $data['missing'] = $missing_logs;
            } else {
                $data['error'] = 1;
                $data['error_details'] = [
                    'error' => "allowable breaks not found",
                    'origin' => 'get_incident_details (OB)',
                    'process' => 'getting of allowable break',
                ];
            }
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 5) // ABSENT
        {
            $data['details'] = "none";
        } else {
            $data['error'] = 1;
            $data['error_details'] = "violation type does not exist";
        }
        return $data;
    }

    private function get_related_user_dtr_violation($user_dtr_vio_id, $user_dtr_vio_type)
    {
        $fields = "userDtrVio.userDtrViolation_ID, userDtrVio.user_ID, userDtrVio.supervisor_ID, userDtrVio.dtrViolationType_ID, userDtrVio.sched_ID, userDtrVio.status_ID, userDtrVio.sched_date, userDtrVio.shift, userDtrVio.occurence, userDtrVio.duration, userDtrVio.dateRecord";
        $where = "userDtrVio.userDtrViolation_ID = related.relatedUserDtrViolation_ID AND related.userDtrViolation_ID = $user_dtr_vio_id AND userDtrVio.dtrViolationType_ID = $user_dtr_vio_type";
        $table = "tbl_dms_user_dtr_violation_related_details related, tbl_user_dtr_violation userDtrVio";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }
    private function get_ir_history($subject_emp_id, $offense_id, $prescriptive_id)
    {
        $fields = "ir.incidentReport_ID, ir.incidentDate, ir.dateTimeFiled, ir.datedLiabilityStat, sugDiscAction.action sugAction, sugDiscAction.abbreviation sugAbbreviation, discAction.abbreviation,  discAction.disciplinaryAction_ID, discAction.action, discAction.label, ir.prescriptiveId, ir.prescriptionEnd, discActionCateg.level";
        $where = "sugDiscAction.disciplinaryAction_ID = sugDiscActionCateg.disciplinaryAction_ID AND sugDiscActionCateg.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discActionCateg.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND ir.offense_ID = $offense_id AND ir.subjectEmp_ID = $subject_emp_id AND ir.prescriptiveId = $prescriptive_id";
        $table = "tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_discipline_category discCateg, tbl_dms_disciplinary_action_category sugDiscActionCateg, tbl_dms_disciplinary_action sugDiscAction";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }
    private function prev_break_computation($user_dtr_vio_id)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $allowable_break = $this->get_violation_allowable_breaks($user_dtr_vio_id);
        $total_allowable = 0;
        $total_covered = 0;
        if (count($allowable_break) > 0) {
            foreach ($allowable_break as $allowable_row) {
                $duration = 0;
                $logs = $this->get_user_dtr_volation_logs($user_dtr_vio_id, $allowable_row->breakType, "I");
                if (count($logs) > 0) {
                    foreach ($logs as $logs_row) {
                        $duration = $logs_row->minDuration;
                    }
                }
                $total_allowable += $allowable_row->duration;
                $total_covered += $duration;
            }
        } else {
            $data['error'] = 1;
            $data['error_details'] = [
                'error' => "allowable breaks not found",
                'origin' => 'get_incident_details (OB)',
                'process' => 'getting of allowable break',
            ];
        }
        $data['allowable'] = $total_allowable;
        $data['covered'] = $total_covered;
        $data['ob'] = $total_covered - $total_allowable;
        return $data;
    }

    public function get_previous_occurence($user_dtr_vio_id, $user_dtr_vio_type)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        // $user_dtr_vio_id = 1694;
        // $user_dtr_vio_type = 4;
        $prev_dtr_vio = $this->get_related_user_dtr_violation($user_dtr_vio_id, $user_dtr_vio_type);
        $prev_details = "";
        $prev_details_count = 0;
        if (count($prev_dtr_vio) > 0) {
            if ($user_dtr_vio_type == 1) //LATE
            {
                foreach ($prev_dtr_vio as $pre_vio_rows) {
                    $logs = $this->get_user_dtr_volation_logs($pre_vio_rows->userDtrViolation_ID, "DTR", "I");
                    if (count($logs) > 0) {
                        $prev_details[$prev_details_count] = [
                            'date' => $pre_vio_rows->sched_date,
                            'shift' => $pre_vio_rows->shift,
                            'log' => $logs[0]->log,
                            'duration' => $pre_vio_rows->duration,
                            'formatted_duration' => $this->get_formatted_time($pre_vio_rows->duration / 60),
                        ];
                        $prev_details_count++;
                    } else {
                        $data['error'] = 1;
                        $data['error_details'] = [
                            'error' => "login log not found",
                            'origin' => 'get_incident_details (late)',
                            'process' => 'getting of login log',
                        ];
                    }
                }
            } else if ($user_dtr_vio_type == 2 || $user_dtr_vio_type == 3) // UT
            {
                foreach ($prev_dtr_vio as $pre_vio_rows) {
                    $logs = $this->get_user_dtr_volation_logs($pre_vio_rows->userDtrViolation_ID, "DTR", "O");
                    if (count($logs) > 0) {
                        $prev_details[$prev_details_count] = [
                            'date' => $pre_vio_rows->sched_date,
                            'shift' => $pre_vio_rows->shift,
                            'log' => $logs[0]->log,
                            'duration' => $pre_vio_rows->duration,
                            'formatted_duration' => $this->get_formatted_time($pre_vio_rows->duration / 60),
                        ];
                        $prev_details_count++;
                    } else {
                        $data['error'] = 1;
                        $data['error_details'] = [
                            'error' => "logout log not found",
                            'origin' => 'get_incident_details (UT)',
                            'process' => 'getting of logout log',
                        ];
                    }
                }
            } else if ($user_dtr_vio_type == 4) {
                foreach ($prev_dtr_vio as $pre_vio_rows) {
                    $allowable_break = 0;
                    $covered_break = 0;
                    $excess_break = 0;
                    $break_computation = $this->prev_break_computation($pre_vio_rows->userDtrViolation_ID);
                    if ($break_computation['error'] == 0) {
                        $allowable_break = (int) $break_computation['allowable'];
                        $covered_break = (int) $break_computation['covered'];
                        $excess_break = (int) $break_computation['ob'];
                    }

                    $prev_details[$prev_details_count] = [
                        'date' => $pre_vio_rows->sched_date,
                        'shift' => $pre_vio_rows->shift,
                        'allowable' => $this->get_formatted_time($allowable_break / 60),
                        'covered' => $this->get_formatted_time($covered_break / 60),
                        'ob' => $this->get_formatted_time($excess_break / 60),
                    ];
                }
            } else if (($user_dtr_vio_type == 6) || ($user_dtr_vio_type == 7)) {
                // ////var_dump($prev_dtr_vio);
                foreach ($prev_dtr_vio as $pre_vio_rows) {

                    $prev_incident_details = $this->get_incident_details($pre_vio_rows, 0);
                    // ////var_dump($prev_incident_details);
                    $prev_details[$prev_details_count] = [
                        'date' => $pre_vio_rows->sched_date,
                        'shift' => $pre_vio_rows->shift,
                        'note' => $prev_incident_details['missing_num'] . " Missing Logs Detected"
                    ];
                }
            } else if ($user_dtr_vio_type == 5) {
                foreach ($prev_dtr_vio as $pre_vio_rows) {
                    $prev_details[$prev_details_count] = [
                        'date' => $pre_vio_rows->sched_date,
                        'shift' => $pre_vio_rows->shift,
                        'note' => "Absent",
                    ];
                }
            }
            $data['prev_details'] = $prev_details;
            $data['with_prev'] = 1;
        } else {
            $data['with_prev'] = 0;
        }
        return $data;
    }

    private function get_ir_rule($occurrenceRuleNum, $occurrenceValue, $user_dtr_vio)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $related_occurence_details = 0;
        $ruleMssg = "An Incident Report should be filed";
        $appendMssg = "";
        if ($occurrenceRuleNum == 1) {
            if ($occurrenceValue == 0) {
                $appendMssg = "if one had committed <b>" . ucfirst($user_dtr_vio->description) . "</b> violation regardless of duration.";
            } else {
                $unit = "minute";
                if ($occurrenceValue > 1) {
                    $unit .= "s";
                }
                if ((int)$user_dtr_vio->dtrViolationType_ID == 3) {
                    $appendMssg = "if the <b>Clock out duration</b> exceeded the <b>$occurrenceValue " . $unit . "</b> Clock out allowance.";
                } else {
                    $appendMssg = "if the <b>" . ucfirst($user_dtr_vio->description) . "</b> duration committed is equal or more than </b>$occurrenceValue " . $unit . "</b>.";
                }
            }
        } else if ($occurrenceRuleNum == 2) {
            $appendMssg = "if one had committed <b>$occurrenceValue consecutive " . ucfirst($user_dtr_vio->description) . "s</b>, regardless of violation duration.";
            $related_occurence_details = 1;
        } else if ($occurrenceRuleNum == 3) {
            if ($user_dtr_vio->dtrViolationType_ID == 3) {
                $appendMssg = "if one had committed <b>$occurrenceValue non-consecutive " . ucfirst($user_dtr_vio->description) . "<b> type violation<b>, regardles of violation duration.";
            } else if (($user_dtr_vio->dtrViolationType_ID == 6) || ($user_dtr_vio->dtrViolationType_ID == 7)) {
                $appendMssg = "if one had committed <b>$occurrenceValue or more Incomplete Logs</b> violation occurrences.";
            } else {
                $unit = "minute";
                if ($occurrenceValue > 1) {
                    $unit .= "s";
                }
                $appendMssg = "if one had accumulated a total of <b>$occurrenceValue " . $unit . " " . ucfirst($user_dtr_vio->description) . "</b>, regardless on the number and nature of occurence.";
            }
            $related_occurence_details = 1;
        }
        $data['rule'] = $ruleMssg . " " . $appendMssg;
        $data['previous_details'] = $related_occurence_details;
        return $data;
    }

    private function get_offense_off_type_details($offense_id)
    {
        $fields = "off.orderNum, off.offense, offType.letter, offType.offenseType";
        $where = " offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = $offense_id";
        $table = "tbl_dms_offense off, tbl_dms_offense_type offType";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    private function get_disc_action_details($category_id, $level)
    {
        $category_settings = $this->get_current_disciplinary_action_category_settings();

        $fields = "categ.category, actionCateg.level, discAction.action, discAction.periodMonth";
        $where = "categ.disciplineCategory_ID = actionCateg.disciplineCategory_ID AND discAction.disciplinaryAction_ID = actionCateg.disciplinaryAction_ID AND actionCateg.disciplineCategory_ID = $category_id AND actionCateg.level = $level";
        $table = "tbl_dms_discipline_category categ, tbl_dms_disciplinary_action_category actionCateg, tbl_dms_disciplinary_action discAction";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    private function identify_prescriptive_period($user_dtr_violation, $disc_action_details)
    {
        // ////var_dump($violation_rules);
        $incident_date = $user_dtr_violation->sched_date;
        $cod_presc_period = $disc_action_details->periodMonth;
        $start_end_coverage = $this->unfixed_prescriptive_period($incident_date, $cod_presc_period, 'month');
        $data['period'] = $disc_action_details->periodMonth;
        $data['unit'] = 'month';
        $data['cure_date'] = $start_end_coverage['end'];
        if ($start_end_coverage['end'] == $incident_date) {
            $start_end_coverage = $this->unfixed_prescriptive_period($incident_date, $cod_presc_period + 1, 'month');
            $data['cure_date'] = $start_end_coverage['end'];
        }
        return $data;
    }

    protected function get_disc_action_via_dis_categ_id($disc_action_categ_id){
        $fields = "actions.*";
        $where = "actions.disciplinaryAction_ID = actionCateg.disciplinaryAction_ID AND actionCateg.disciplinaryActionCategory_ID = $disc_action_categ_id";
        $table = "tbl_dms_disciplinary_action actions, tbl_dms_disciplinary_action_category actionCateg";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function get_prescriptive_period($ir_id, $disc_action_categ_id){
        $ir_details = $this->get_ir($ir_id);
        $disc_action_details = $this->get_disc_action_via_dis_categ_id($disc_action_categ_id);
        $incident_date = $ir_details->incidentDate;
        $cod_presc_period = $disc_action_details->periodMonth;
        $start_end_coverage = $this->unfixed_prescriptive_period($incident_date, $cod_presc_period, 'month');
        $data['period'] = $disc_action_details->periodMonth;
        $data['unit'] = 'month';
        $data['cure_date'] = $start_end_coverage['end'];
        if ($start_end_coverage['end'] == $incident_date) {
            $start_end_coverage = $this->unfixed_prescriptive_period($incident_date, $cod_presc_period + 1, 'month');
            $data['cure_date'] = $start_end_coverage['end'];
        }
        return $data;
    }

    private function identify_disc_action($violation_rules, $user_dtr_violation, $with_ir_history, $level, $disciplinaryActionCategorySettings_ID, $disciplineCategory_ID)
    {
        $offense_id = $violation_rules->offense_ID;
        $data['error'] = 0;
        $data['error_details'] = '';
        if ((int) $with_ir_history) {
            $next_level_disc_action = $this->get_offense_disc_action_by_level($offense_id, $level + 1, $disciplinaryActionCategorySettings_ID, $disciplineCategory_ID);
            // var_dump($next_level_disc_action);
            if (count($next_level_disc_action) > 0) {
                $data['next_level'] =  $next_level_disc_action;
            } else {
                $max = $this->get_max_offense_disc_action_by_level_category($offense_id, $disciplineCategory_ID);
                $get_max_level = $max->level;
                $data['next_level'] = $this->get_offense_disc_action_by_level_category($offense_id, $get_max_level, $disciplineCategory_ID);
            }
            $data['prescriptive_period'] = $this->identify_prescriptive_period($user_dtr_violation, $data['next_level']);
        } else {
            $next_level_disc_action = $this->get_offense_disc_action_by_level($offense_id, 1, $disciplinaryActionCategorySettings_ID, $disciplineCategory_ID);
            if (count($next_level_disc_action) > 0) {
                $data['next_level'] = $next_level_disc_action;
            } else {
                $data['error'] = 1;
                $data['error_details'] = "1st level not found";
            }
            $data['prescriptive_period'] = $this->identify_prescriptive_period($user_dtr_violation, $data['next_level']);
        }
        return $data;
    }

    public function get_dated_shift($date, $start, $end)
    {
        $shift_start_date_time = new DateTime($date . " " . $start);
        $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
        $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $date);
        $shift_end_date_time = new DateTime($date . " " . $end);
        $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
        $new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
        $new_formatted_shift_end = $this->new_format_midnight_shift_out($new_shifts['end']);
        $data['shift_start'] = $new_formatted_shift_start;
        $data['shift_end'] = $new_formatted_shift_end;
        return $data;
    }

    public function get_dated_shift_test($date = '2019-06-19', $start = '12:00:00 AM', $end = '09:00:00 AM')
    {
        $shift_start_date_time = new DateTime($date . " " . $start);
        $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
        $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $date);
        $shift_end_date_time = new DateTime($date . " " . $end);
        $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
        $new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
        $new_formatted_shift_end = $this->new_format_midnight_shift_out($new_shifts['end']);
        $data['shift_start'] = $new_formatted_shift_start;
        $data['shift_end'] = $new_formatted_shift_end;
        // return $data;
        var_dump($data);
    }


    // public function construct_incident_details($qualified_violation_id = 111){
    //     $data['error'] = 0;
    //     $data['error_details'] = "";
    //     $details = "";
    //     $qualified_record = $this->get_qualified_dtr_violation($qualified_violation_id); // qualified user dtr violation
    //     if (count($qualified_record) > 0) {
    //         $emp_details = $this->get_emp_details_via_emp_id($qualified_record->emp_id); // employee details
    //         ////var_dump($emp_details);
    //         $user_dtr_vio = $this->get_user_dtr_violation($qualified_record->userDtrViolation_ID);
    //         $incident_details = $this->get_incident_details($user_dtr_vio, $qualified_record->occurrenceValue);
    //         $shift = explode('-', $user_dtr_vio->shift);
    //         $dated_shift = $this->get_dated_shift($user_dtr_vio->sched_date, trim($shift[0]), trim($shift[1]));
    //         ////var_dump($dated_shift);
    //         ////var_dump($incident_details);
    //         ////var_dump($user_dtr_vio);
    //         if($user_dtr_vio->dtrViolationType_ID == 1){ //LATE
    //             $details = ucwords($emp_details->fname." ". $emp_details->fname)." clocked in by ".$dated_shift."". $incident_details['duration'];
    //         } else if ($user_dtr_vio->dtrViolationType_ID == 2) {

    //         } else if ($user_dtr_vio->dtrViolationType_ID == 3) {

    //         } else if ($user_dtr_vio->dtrViolationType_ID == 4) { 

    //         } else if ($user_dtr_vio->dtrViolationType_ID == 5) {

    //         } else { 

    //         }
    //     }else{
    //         $data['error'] = 1;
    //         $data['error_details'] = [
    //             'error' => "qualified user dtr violation not found",
    //             'origin' => 'get_qualified_dtr_vio_details',
    //             'process' => 'getting of qualified user dtr violation'
    //         ];
    //     }
    // }


    public function get_qualified_dtr_vio_details()
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $error_count = 0;
        $related_occurence_details = "";
        $details['with_prev'] = 0;
        $details['with_history'] = 0;
        $details['dismissable'] = 0;
        $details['with_rule'] = 0;
        // $emp_id = $this->input->post('empId');
        $qualified_violation_id = $this->input->post('qualifiedViolationId');
        // $qualified_violation_id = 127;
        $qualified_record = $this->get_qualified_dtr_violation($qualified_violation_id); // qualified user dtr violation
        if (count($qualified_record) > 0) {
            $details['emp_details'] = $this->get_emp_details_via_emp_id($qualified_record->emp_id); // employee details
            $user_dtr_vio = $this->get_user_dtr_violation($qualified_record->userDtrViolation_ID); // user DTR Violation

            $category_settings = $this->get_current_disciplinary_action_category_settings();
            $violation_rules = $this->get_dtr_violation_settings($user_dtr_vio->dtrViolationType_ID);
            $shift = explode('-', $user_dtr_vio->shift);
            $details['dated_shift'] = $this->get_dated_shift($user_dtr_vio->sched_date, trim($shift[0]), trim($shift[1]));
            if (count($user_dtr_vio) > 0) {
                $existing_ir_level = 0;
                $existing_ir_disciplinaryActionCategorySettings_ID = $category_settings->disciplinaryActionCategorySettings_ID;
                $existing_ir_disciplineCategory_ID = $violation_rules->category_ID;
                $details['user_dtr_violation'] = $user_dtr_vio;
                $details['sched_date'] = $user_dtr_vio->sched_date;
                $details['shift'] = $user_dtr_vio->shift;
                $details['qualified_record'] = $qualified_record;
                if ((int)$user_dtr_vio->dtrViolationType_ID == 6) {
                    $details['violation_type'] = "INC. Break Logs";
                } else if((int)$user_dtr_vio->dtrViolationType_ID == 7) {
                    $details['violation_type'] = "INC. DTR Logs";
                }else{
                    if ((int) $user_dtr_vio->dtrViolationType_ID == 3) {
                        $details['reason'] = $this->get_forgot_logout_reason($qualified_record->userDtrViolation_ID);
                    }
                    $details['violation_type'] = $user_dtr_vio->description;
                }
                
                $details['incident_details'] = $this->get_incident_details($user_dtr_vio, $qualified_record->occurrenceValue); // Incident Details
                $existing_ir = $this->get_existing_ir($qualified_record->emp_id, 4, 17, $violation_rules->offense_ID);
                if (count($existing_ir) > 0) {
                    // IR History
                    // ////var_dump($violation_rules->offense_ID);
                    // var_dump($existing_ir);
                    $details['prescriptive_id'] = $existing_ir->prescriptiveId;
                    $details['occurence'] = $existing_ir->occurence + 1;
                    if (count($existing_ir) > 0) {
                        $ir_history = $this->get_ir_history($qualified_record->emp_id, $violation_rules->offense_ID, $existing_ir->prescriptiveId);
                        $existing_ir_level = $existing_ir->level;
                        $existing_ir_disciplinaryActionCategorySettings_ID = $existing_ir->disciplinaryActionCategorySettings_ID;
                        $existing_ir_disciplineCategory_ID = $existing_ir->disciplineCategory_ID;

                        // ////var_dump($ir_history);
                        if (count($ir_history) > 0) {
                            $details['with_history'] = 1;
                            $details['ir_history'] = $ir_history;
                        } else {
                            $data['error'] = 1;
                            $data['error_details'][$error_count] = [
                                'error' => "ir history not found",
                                'origin' => 'get_qualified_dtr_vio_details',
                                'process' => 'getting of IR History',
                            ];
                            $error_count++;
                        }
                    } else {
                        $data['error'] = 1;
                        $data['error_details'][$error_count] = [
                            'error' => "existing IR not found",
                            'origin' => 'get_qualified_dtr_vio_details',
                            'process' => 'getting of existing of IR',
                        ];
                        $error_count++;
                    }
                    // ////var_dump($existing_ir); 
                }else{
                    $details['prescriptive_id'] = $this->qry_max_prescriptive_id() + 1;
                    $details['occurence'] = 1;
                }
                //  else {
                    // IR RULE
                    if ((int) $qualified_record->occurrenceRuleNum !== 0) {
                        $details['with_rule'] = 1;
                        $details['ir_rule'] = $this->get_ir_rule($qualified_record->occurrenceRuleNum, $qualified_record->occurrenceValue, $user_dtr_vio); // IR Rule 
                        if ($details['ir_rule']['previous_details']) { // previous details
                            $prev_occurrence = $this->get_previous_occurence($user_dtr_vio->userDtrViolation_ID, $user_dtr_vio->dtrViolationType_ID);
                            $details['with_prev'] = $prev_occurrence['with_prev'];
                            if ($prev_occurrence['with_prev']) {
                                $details['previous_details'] = $prev_occurrence['prev_details'];
                            }
                        }
                    }
                    
                // }
                // get offense type, offense
                $offense_off_type_details = $this->get_offense_off_type_details($violation_rules->offense_ID);
                if (count($offense_off_type_details) > 0) {
                    $details['offense_details'] = [
                        "offense_type" => ucwords($offense_off_type_details->letter) . ' ' . $offense_off_type_details->offenseType,
                        "offense_num" => $offense_off_type_details->orderNum,
                        "offense_id" => $violation_rules->offense_ID,
                        "offense" => $offense_off_type_details->offense,
                    ];
                } else {
                    $data['error'] = 1;
                    $data['error_details'][$error_count] = [
                        'error' => "offense Details not found",
                        'origin' => 'get_qualified_dtr_vio_details',
                        'process' => 'getting of offense and offense type details',
                    ];
                    $error_count++;
                }
                // $disc_action_details = $this->get_disc_action_details($qualified_record->category_ID, $qualified_record->discActionLevel, $details['with_history']);
                $disc_action_details = $this->identify_disc_action($violation_rules, $user_dtr_vio, $details['with_history'], $existing_ir_level, $existing_ir_disciplinaryActionCategorySettings_ID, $existing_ir_disciplineCategory_ID);
                if ($disc_action_details['error'] == 0) {
                    $details['action_details'] = [
                        "discActionCateg_ID" => $disc_action_details['next_level']->disciplinaryActionCategory_ID,
                        "discActionCategSettings_ID" => $disc_action_details['next_level']->disciplinaryActionCategorySettings_ID,
                        "category" => "Category " . ucwords($disc_action_details['next_level']->category),
                        "level" => $this->ordinal($disc_action_details['next_level']->level),
                        "action" => $disc_action_details['next_level']->action,
                        "cod_period" => $disc_action_details['next_level']->periodMonth,
                        "period_to_follow" => $disc_action_details['prescriptive_period']['period'],
                        "unit_to_follow" => $disc_action_details['prescriptive_period']['unit'],
                        "cure_date" => $disc_action_details['prescriptive_period']['cure_date'],
                    ];
                } else {
                    $data['error'] = 1;
                    $data['error_details'][$error_count] = [
                        'error' => "disciplinary action details not found",
                        'origin' => 'get_qualified_dtr_vio_details',
                        'process' => 'getting of disciplinary action, category, level, and prescriptive period details',
                    ];
                    $error_count++;
                }
                // Check if Dismissable
                if ((int) $violation_rules->deadlineOption == 2) {
                    $details['dismissable'] = 1;
                } else {
                    $file_deadline = $this->get_specific_dms_deadline($qualified_violation_id, $this->session->userdata('emp_id'), 1);
                    if (count($file_deadline) < 1) {
                        $details['dismissable'] = 1;
                    }
                }
            } else {
                $data['error'] = 1;
                $data['error_details'] = [
                    'error' => "user dtr violation not found",
                    'origin' => 'get_qualified_dtr_vio_details',
                    'process' => 'getting of user dtr violation',
                ];
            }
        } else {
            $data['error'] = 1;
            $data['error_details'] = [
                'error' => "qualified user dtr violation not found",
                'origin' => 'get_qualified_dtr_vio_details',
                'process' => 'getting of qualified user dtr violation',
            ];
        }
        $data['details'] = $details;
        // ////var_dump($data);
        echo json_encode($data);

        // incident details
        // IR Rule
        // Previous Occurence (depending on the occurence rule)
        // Incident History if not the first occurence
        // offense Details and Disciplinary Action
    }

    public function get_qualified_dtr_vio_details_test()
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $error_count = 0;
        $related_occurence_details = "";
        $details['with_prev'] = 0;
        $details['with_history'] = 0;
        $details['dismissable'] = 0;
        $details['with_rule'] = 0;
        // $emp_id = $this->input->post('empId');
        $qualified_violation_id = 8545;
        // $qualified_violation_id = 127;
        $qualified_record = $this->get_qualified_dtr_violation($qualified_violation_id); // qualified user dtr violation
        if (count($qualified_record) > 0) {
            $details['emp_details'] = $this->get_emp_details_via_emp_id($qualified_record->emp_id); // employee details
            $user_dtr_vio = $this->get_user_dtr_violation($qualified_record->userDtrViolation_ID); // user DTR Violation

            $category_settings = $this->get_current_disciplinary_action_category_settings();
            $violation_rules = $this->get_dtr_violation_settings($user_dtr_vio->dtrViolationType_ID);
            $shift = explode('-', $user_dtr_vio->shift);
            $details['dated_shift'] = $this->get_dated_shift($user_dtr_vio->sched_date, trim($shift[0]), trim($shift[1]));
            if (count($user_dtr_vio) > 0) {
                $existing_ir_level = 0;
                $existing_ir_disciplinaryActionCategorySettings_ID = $category_settings->disciplinaryActionCategorySettings_ID;
                $existing_ir_disciplineCategory_ID = $violation_rules->category_ID;
                $details['user_dtr_violation'] = $user_dtr_vio;
                $details['sched_date'] = $user_dtr_vio->sched_date;
                $details['shift'] = $user_dtr_vio->shift;
                $details['qualified_record'] = $qualified_record;
                if ((int)$user_dtr_vio->dtrViolationType_ID == 6) {
                    $details['violation_type'] = "INC. Break Logs";
                } else if((int)$user_dtr_vio->dtrViolationType_ID == 7) {
                    $details['violation_type'] = "INC. DTR Logs";
                }else{
                    if ((int) $user_dtr_vio->dtrViolationType_ID == 3) {
                        $details['reason'] = $this->get_forgot_logout_reason($qualified_record->userDtrViolation_ID);
                    }
                    $details['violation_type'] = $user_dtr_vio->description;
                }
                
                $details['incident_details'] = $this->get_incident_details($user_dtr_vio, $qualified_record->occurrenceValue); // Incident Details
                $existing_ir = $this->get_existing_ir($qualified_record->emp_id, 4, 17, $violation_rules->offense_ID);
                // var_dump($existing_ir);
                if (count($existing_ir) > 0) {
                    // IR History
                    // ////var_dump($violation_rules->offense_ID);
                    // var_dump($existing_ir);
                    $details['prescriptive_id'] = $existing_ir->prescriptiveId;
                    $details['occurence'] = $existing_ir->occurence + 1;
                    if (count($existing_ir) > 0) {
                        $ir_history = $this->get_ir_history($qualified_record->emp_id, $violation_rules->offense_ID, $existing_ir->prescriptiveId);
                        $existing_ir_level = $existing_ir->level;
                        $existing_ir_disciplinaryActionCategorySettings_ID = $existing_ir->disciplinaryActionCategorySettings_ID;
                        $existing_ir_disciplineCategory_ID = $existing_ir->disciplineCategory_ID;

                        // ////var_dump($ir_history);
                        if (count($ir_history) > 0) {
                            $details['with_history'] = 1;
                            $details['ir_history'] = $ir_history;
                        } else {
                            $data['error'] = 1;
                            $data['error_details'][$error_count] = [
                                'error' => "ir history not found",
                                'origin' => 'get_qualified_dtr_vio_details',
                                'process' => 'getting of IR History',
                            ];
                            $error_count++;
                        }
                    } else {
                        $data['error'] = 1;
                        $data['error_details'][$error_count] = [
                            'error' => "existing IR not found",
                            'origin' => 'get_qualified_dtr_vio_details',
                            'process' => 'getting of existing of IR',
                        ];
                        $error_count++;
                    }
                    // ////var_dump($existing_ir); 
                }else{
                    $details['prescriptive_id'] = $this->qry_max_prescriptive_id() + 1;
                    $details['occurence'] = 1;
                }
                //  else {
                    // IR RULE
                    if ((int) $qualified_record->occurrenceRuleNum !== 0) {
                        $details['with_rule'] = 1;
                        $details['ir_rule'] = $this->get_ir_rule($qualified_record->occurrenceRuleNum, $qualified_record->occurrenceValue, $user_dtr_vio); // IR Rule 
                        if ($details['ir_rule']['previous_details']) { // previous details
                            $prev_occurrence = $this->get_previous_occurence($user_dtr_vio->userDtrViolation_ID, $user_dtr_vio->dtrViolationType_ID);
                            $details['with_prev'] = $prev_occurrence['with_prev'];
                            if ($prev_occurrence['with_prev']) {
                                $details['previous_details'] = $prev_occurrence['prev_details'];
                            }
                        }
                    }
                    
                // }
                // get offense type, offense
                $offense_off_type_details = $this->get_offense_off_type_details($violation_rules->offense_ID);
                if (count($offense_off_type_details) > 0) {
                    $details['offense_details'] = [
                        "offense_type" => ucwords($offense_off_type_details->letter) . ' ' . $offense_off_type_details->offenseType,
                        "offense_num" => $offense_off_type_details->orderNum,
                        "offense_id" => $violation_rules->offense_ID,
                        "offense" => $offense_off_type_details->offense,
                    ];
                } else {
                    $data['error'] = 1;
                    $data['error_details'][$error_count] = [
                        'error' => "offense Details not found",
                        'origin' => 'get_qualified_dtr_vio_details',
                        'process' => 'getting of offense and offense type details',
                    ];
                    $error_count++;
                }
                // $disc_action_details = $this->get_disc_action_details($qualified_record->category_ID, $qualified_record->discActionLevel, $details['with_history']);
                var_dump($existing_ir_level);
                $disc_action_details = $this->identify_disc_action($violation_rules, $user_dtr_vio, $details['with_history'], $existing_ir_level, $existing_ir_disciplinaryActionCategorySettings_ID, $existing_ir_disciplineCategory_ID);
                if ($disc_action_details['error'] == 0) {
                    $details['action_details'] = [
                        "discActionCateg_ID" => $disc_action_details['next_level']->disciplinaryActionCategory_ID,
                        "discActionCategSettings_ID" => $disc_action_details['next_level']->disciplinaryActionCategorySettings_ID,
                        "category" => "Category " . ucwords($disc_action_details['next_level']->category),
                        "level" => $this->ordinal($disc_action_details['next_level']->level),
                        "action" => $disc_action_details['next_level']->action,
                        "cod_period" => $disc_action_details['next_level']->periodMonth,
                        "period_to_follow" => $disc_action_details['prescriptive_period']['period'],
                        "unit_to_follow" => $disc_action_details['prescriptive_period']['unit'],
                        "cure_date" => $disc_action_details['prescriptive_period']['cure_date'],
                    ];
                } else {
                    $data['error'] = 1;
                    $data['error_details'][$error_count] = [
                        'error' => "disciplinary action details not found",
                        'origin' => 'get_qualified_dtr_vio_details',
                        'process' => 'getting of disciplinary action, category, level, and prescriptive period details',
                    ];
                    $error_count++;
                }
                // Check if Dismissable
                if ((int) $violation_rules->deadlineOption == 2) {
                    $details['dismissable'] = 1;
                } else {
                    $file_deadline = $this->get_specific_dms_deadline($qualified_violation_id, $this->session->userdata('emp_id'), 1);
                    if (count($file_deadline) < 1) {
                        $details['dismissable'] = 1;
                    }
                }
            } else {
                $data['error'] = 1;
                $data['error_details'] = [
                    'error' => "user dtr violation not found",
                    'origin' => 'get_qualified_dtr_vio_details',
                    'process' => 'getting of user dtr violation',
                ];
            }
        } else {
            $data['error'] = 1;
            $data['error_details'] = [
                'error' => "qualified user dtr violation not found",
                'origin' => 'get_qualified_dtr_vio_details',
                'process' => 'getting of qualified user dtr violation',
            ];
        }
        $data['details'] = $details;
        // ////var_dump($data);
        echo json_encode($data);

        // incident details
        // IR Rule
        // Previous Occurence (depending on the occurence rule)
        // Incident History if not the first occurence
        // offense Details and Disciplinary Action
    }


    protected function get_dismiss_reason($qualified_id){
        $fields = "dismissQualifiedDtrViolation_ID, qualifiedUserDtrViolation_ID, reason, emp_id, dateTime";
        $where = "qualifiedUserDtrViolation_ID = $qualified_id";
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_dismiss_qualified_dtr_violation");
    }

    public function dismissed_qualified_dtr_details_view()
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $error_count = 0;
        $related_occurence_details = "";
        $details['with_prev'] = 0;
        $details['with_history'] = 0;
        $details['with_rule'] = 0;
        // $emp_id = $this->input->post('empId');
        $qualified_violation_id = $this->input->post('qualifiedViolationId');
        $qualified_record = $this->get_qualified_dtr_violation($qualified_violation_id); // qualified user dtr violation
        if (count($qualified_record) > 0) {
            $details['emp_details'] = $this->get_emp_details_via_emp_id($qualified_record->emp_id); // employee details
            $user_dtr_vio = $this->get_user_dtr_violation($qualified_record->userDtrViolation_ID); // user DTR Violation
            $category_settings = $this->get_current_disciplinary_action_category_settings();
            $violation_rules = $this->get_dtr_violation_settings($user_dtr_vio->dtrViolationType_ID);
            $shift = explode('-', $user_dtr_vio->shift);
            $details['dated_shift'] = $this->get_dated_shift($user_dtr_vio->sched_date, trim($shift[0]), trim($shift[1]));
            $details['dismiss_reason'] = $this->get_dismiss_reason($qualified_violation_id);
            // $details['ir_qualified_details'] = $this->get_auto_ir_qulified_via_qualified($qualified_violation_id);
            $details['sup_details'] = $this->get_emp_details_via_emp_id($details['dismiss_reason']->emp_id);
            if (count($user_dtr_vio) > 0) {
                $existing_ir_level = 0;
                $existing_ir_disciplinaryActionCategorySettings_ID = $category_settings->disciplinaryActionCategorySettings_ID;
                $existing_ir_disciplineCategory_ID = $violation_rules->category_ID;
                $details['user_dtr_violation'] = $user_dtr_vio;
                $details['sched_date'] = $user_dtr_vio->sched_date;
                $details['shift'] = $user_dtr_vio->shift;
                $details['qualified_record'] = $qualified_record;
                if (((int) $user_dtr_vio->dtrViolationType_ID == 6) || ((int) $user_dtr_vio->dtrViolationType_ID == 7)) {
                    $details['violation_type'] = "inc. dtr logs";
                    if ((int) $user_dtr_vio->dtrViolationType_ID == 3) {
                        $details['reason'] = $this->get_forgot_logout_reason($qualified_record->userDtrViolation_ID);
                    }
                } else {
                    $details['violation_type'] = $user_dtr_vio->description;
                }
                $details['incident_details'] = $this->get_incident_details($user_dtr_vio, $qualified_record->occurrenceValue); // Incident Details
                $existing_ir = $this->get_existing_ir($qualified_record->emp_id, 4, 17, $violation_rules->offense_ID);
                if (count($existing_ir) > 0) {
                    // IR History
                    // ////var_dump($violation_rules->offense_ID);
                    // ////var_dump($existing_ir);
                    $details['prescriptive_id'] = $existing_ir->prescriptiveId;
                    $details['occurence'] = $existing_ir->occurence + 1;
                    if (count($existing_ir) > 0) {
                        $ir_history = $this->get_ir_history($qualified_record->emp_id, $violation_rules->offense_ID, $existing_ir->prescriptiveId);
                        $existing_ir_level = $existing_ir->level;
                        $existing_ir_disciplinaryActionCategorySettings_ID = $existing_ir->disciplinaryActionCategorySettings_ID;
                        $existing_ir_disciplineCategory_ID = $existing_ir->disciplineCategory_ID;

                        // ////var_dump($ir_history);
                        if (count($ir_history) > 0) {
                            $details['with_history'] = 1;
                            $details['ir_history'] = $ir_history;
                        } else {
                            $data['error'] = 1;
                            $data['error_details'][$error_count] = [
                                'error' => "ir history not found",
                                'origin' => 'get_qualified_dtr_vio_details',
                                'process' => 'getting of IR History',
                            ];
                            $error_count++;
                        }
                    } else {
                        $data['error'] = 1;
                        $data['error_details'][$error_count] = [
                            'error' => "existing IR not found",
                            'origin' => 'get_qualified_dtr_vio_details',
                            'process' => 'getting of existing of IR',
                        ];
                        $error_count++;
                    }
                    // ////var_dump($existing_ir); 
                } 
                if ((int) $qualified_record->occurrenceRuleNum !== 0) {
                    // IR RULE
                    $details['with_rule'] = 1;
                    $details['ir_rule'] = $this->get_ir_rule($qualified_record->occurrenceRuleNum, $qualified_record->occurrenceValue, $user_dtr_vio); // IR Rule 
                    $details['prescriptive_id'] = $this->qry_max_prescriptive_id() + 1;
                    $details['occurence'] = 1;
                    if ($details['ir_rule']['previous_details']) { // previous details
                        $prev_occurrence = $this->get_previous_occurence($user_dtr_vio->userDtrViolation_ID, $user_dtr_vio->dtrViolationType_ID);
                        $details['with_prev'] = $prev_occurrence['with_prev'];
                        if ($prev_occurrence['with_prev']) {
                            $details['previous_details'] = $prev_occurrence['prev_details'];
                        }
                    }
                }
            }
        }
        $data['details'] = $details;
        // ////var_dump($data);
        echo json_encode($data);
    }

    public function dtr_details_view()
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $error_count = 0;
        $related_occurence_details = "";
        $details['with_prev'] = 0;
        $details['with_history'] = 0;
        $details['with_rule'] = 0;
        // $emp_id = $this->input->post('empId');
        $qualified_violation_id = $this->input->post('qualifiedViolationId');
        $qualified_record = $this->get_qualified_dtr_violation($qualified_violation_id); // qualified user dtr violation
        if (count($qualified_record) > 0) {
            $details['emp_details'] = $this->get_emp_details_via_emp_id($qualified_record->emp_id); // employee details
            $user_dtr_vio = $this->get_user_dtr_violation($qualified_record->userDtrViolation_ID); // user DTR Violation
            $category_settings = $this->get_current_disciplinary_action_category_settings();
            $violation_rules = $this->get_dtr_violation_settings($user_dtr_vio->dtrViolationType_ID);
            $shift = explode('-', $user_dtr_vio->shift);
            $details['dated_shift'] = $this->get_dated_shift($user_dtr_vio->sched_date, trim($shift[0]), trim($shift[1]));
            $details['ir_qualified_details'] = $this->get_auto_ir_qulified_via_qualified($qualified_violation_id);
            $details['sup_details'] = $this->get_emp_details_via_emp_id($details['ir_qualified_details']->directSupEmpId);
            if (count($user_dtr_vio) > 0) {
                $existing_ir_level = 0;
                $existing_ir_disciplinaryActionCategorySettings_ID = $category_settings->disciplinaryActionCategorySettings_ID;
                $existing_ir_disciplineCategory_ID = $violation_rules->category_ID;
                $details['user_dtr_violation'] = $user_dtr_vio;
                $details['sched_date'] = $user_dtr_vio->sched_date;
                $details['shift'] = $user_dtr_vio->shift;
                $details['qualified_record'] = $qualified_record;
                // if (((int) $user_dtr_vio->dtrViolationType_ID == 6) || ((int) $user_dtr_vio->dtrViolationType_ID == 7)) {
                //     $details['violation_type'] = "inc. dtr logs";
                    
                // } else {
                // }
                $details['violation_type'] = $user_dtr_vio->description;
                if ((int) $user_dtr_vio->dtrViolationType_ID == 3) {
                    $details['reason'] = $this->get_forgot_logout_reason($qualified_record->userDtrViolation_ID);
                }
                $details['incident_details'] = $this->get_incident_details($user_dtr_vio, $qualified_record->occurrenceValue); // Incident Details
                $existing_ir = $this->get_existing_ir($qualified_record->emp_id, 4, 17, $violation_rules->offense_ID);
                if (count($existing_ir) > 0) {
                    // IR History
                    // ////var_dump($violation_rules->offense_ID);
                    // ////var_dump($existing_ir);
                    $details['prescriptive_id'] = $existing_ir->prescriptiveId;
                    $details['occurence'] = $existing_ir->occurence + 1;
                    if (count($existing_ir) > 0) {
                        $ir_history = $this->get_ir_history($qualified_record->emp_id, $violation_rules->offense_ID, $existing_ir->prescriptiveId);
                        $existing_ir_level = $existing_ir->level;
                        $existing_ir_disciplinaryActionCategorySettings_ID = $existing_ir->disciplinaryActionCategorySettings_ID;
                        $existing_ir_disciplineCategory_ID = $existing_ir->disciplineCategory_ID;

                        // ////var_dump($ir_history);
                        if (count($ir_history) > 0) {
                            $details['with_history'] = 1;
                            $details['ir_history'] = $ir_history;
                        } else {
                            $data['error'] = 1;
                            $data['error_details'][$error_count] = [
                                'error' => "ir history not found",
                                'origin' => 'get_qualified_dtr_vio_details',
                                'process' => 'getting of IR History',
                            ];
                            $error_count++;
                        }
                    } else {
                        $data['error'] = 1;
                        $data['error_details'][$error_count] = [
                            'error' => "existing IR not found",
                            'origin' => 'get_qualified_dtr_vio_details',
                            'process' => 'getting of existing of IR',
                        ];
                        $error_count++;
                    }
                    // ////var_dump($existing_ir); 
                }
                if((int)$qualified_record->occurrenceRuleNum !== 0){
                    // IR RULE
                    $details['with_rule'] = 1;
                    $details['ir_rule'] = $this->get_ir_rule($qualified_record->occurrenceRuleNum, $qualified_record->occurrenceValue, $user_dtr_vio); // IR Rule 
                    $details['prescriptive_id'] = $this->qry_max_prescriptive_id() + 1;
                    $details['occurence'] = 1;
                    if ($details['ir_rule']['previous_details']) { // previous details
                        $prev_occurrence = $this->get_previous_occurence($user_dtr_vio->userDtrViolation_ID, $user_dtr_vio->dtrViolationType_ID);
                        $details['with_prev'] = $prev_occurrence['with_prev'];
                        if ($prev_occurrence['with_prev']) {
                            $details['previous_details'] = $prev_occurrence['prev_details'];
                        }
                    }
                }
            }
        }
        $data['details'] = $details;
        // ////var_dump($data);
        echo json_encode($data);
    }

    private function qry_ir_details($ir_id)
    {
        $fields = "*";
        $where = "incidentReport_ID = $ir_id";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_incident_report');
    }

    private function identify_assigned_ero($acc_id, $site_id)
    {
        $ero = $this->default_ero();
        $assigned_ero = $this->get_assigned_ero($acc_id, $site_id);
        if (count($assigned_ero) > 0) {
            $ero = $assigned_ero->emp_id;
        }
        return $ero;
    }

    private function check_if_ero_is_subject($acc_id, $site_id, $subjectEmp_ID)
    {
        $ero = $this->identify_assigned_ero($acc_id, $site_id);
        if ($ero == $subjectEmp_ID) {
            $ero_sup = $this->get_ero_supervisor($ero);
            if (count($ero_sup) > 0) {
                $ero = $ero_sup->employeeRelationsSupervisor;
            } else {
                $ero = $this->default_ero_sup();
            }
        }
        return $ero;
    }

    private function filter_exemption_or_reassignment($supervisors,  $acc_id,  $subjectEmp_ID,  $site_id)
    {
        $recommenders =  $supervisors['supervisors'];
        $exemption =  $this->get_specific_exempt_via_account($acc_id);
        $reassign =  $this->get_specific_reassignment_via_account($acc_id, 'change');
        $ero =  $this->identify_assigned_ero($acc_id,  $site_id);
        for ($sup_loop = 0; $sup_loop < count($recommenders); $sup_loop++) {
            if ($recommenders[$sup_loop]['emp_id'] ==  $ero) { //check if ero is one of the normal recommendation
                unset($recommenders[$sup_loop]);
            } else if (count($exemption) > 0) { // check exemption
                $level =  $sup_loop + 1;
                $emp_id =  $recommenders[$sup_loop]['emp_id'];
                $remove_sup = array_filter(
                    $exemption,
                    function ($e) use ($level,  $emp_id) {
                        return (($e->level ==  $level) && ($e->emp_id ==  $emp_id));
                    }
                );
                if (count($remove_sup) > 0) {
                    // echo "remove sup  $sup_loop <br>";
                    unset($recommenders[$sup_loop]);
                } else {
                    // echo "reassign sup  $sup_loop <br>";
                    if (count($reassign) > 0) {
                        $level =  $sup_loop + 1;
                        $emp_id =  $recommenders[$sup_loop]['emp_id'];
                        $reassign_sup = array_filter(
                            $reassign,
                            function ($e) use ($level,  $emp_id) {
                                return (($e->level ==  $level) && ($e->emp_id ==  $emp_id));
                            }
                        );
                        $reassign_sup = array_merge($reassign_sup);
                        if (count($reassign_sup) > 0) { // check if reassign sup
                            if ($reassign_sup[0]->assignTo !=  $subjectEmp_ID) {
                                $recommenders[$sup_loop]['emp_id'] =  $reassign_sup[0]->assignTo;
                            }
                        }
                    }
                }
            }
        }
        return  array_merge($recommenders);
    }

    private function set_recommmendation_deadline($recommenders,  $ir,  $first_id)
    {
        $rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
        $recommendationFlow = (int) $rec_deadline['record']->deadlineToRecommendOption;
        $days_recommend = (int) $rec_deadline['record']->deadlineToRecommend;
        $days_last_recommend = (int) $rec_deadline['record']->deadlineLastRecommendation;
        $dateTime = $this->get_current_date_time();
        $start_date =  $dateTime["date"];
        $deadline_type = 3;
        for ($loopRec = 0; $loopRec < count($recommenders); $loopRec++) {
            if ($recommenders[$loopRec]->incidentReportRecommendationType_ID == 1) {
                $deadline_type = 3;
                $recommend_date = Date("Y-m-d", strtotime($start_date . "  $days_recommend days"));
                $deadline_to_recommend =  $recommend_date . " 23:59:59";
                if ($recommendationFlow == 1) {
                    $start_date =  $recommend_date;
                }
            } else {
                $deadline_type = 4;
                if ($recommendationFlow == 1) {
                    $recommend_date = Date("Y-m-d", strtotime($start_date . " $days_last_recommend days"));
                    $deadline_to_recommend =  $recommend_date . " 23:59:59";
                } else {
                    $rec_days =  $days_recommend +  $days_last_recommend;
                    $recommend_date = Date("Y-m-d", strtotime($start_date . " $rec_days days"));
                    $deadline_to_recommend =  $recommend_date . " 23:59:59";
                }
            }
            $ir_recommendation_deadline[$loopRec] = [
                'record_ID' =>  $first_id +  $loopRec,
                'emp_id' =>  $recommenders[$loopRec]->emp_id,
                'deadline' =>  $deadline_to_recommend,
                'dmsDeadlineType_ID' =>  $deadline_type,
                'status_ID' => 2,
                'level' =>  $loopRec + 1,
            ];
        }
        return  $this->general_model->batch_insert($ir_recommendation_deadline, 'tbl_dms_deadline');
    }

    private function set_ir_recommendation($recommenders,  $ir,  $acc_id,  $site_id)
    {
        $ir_recommendation = [];
        $filtered_recommendation = [];
        $rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
        $recommendationFlow = (int) $rec_deadline['record']->deadlineToRecommendOption;
        $assigned_ero =  $this->check_if_ero_is_subject($acc_id,  $site_id,  $ir->subjectEmp_ID);
        $status = 4;
        for ($loopRec = 0; $loopRec < count($recommenders); $loopRec++) {
            $ir_recommendation[$loopRec] = (object) [
                'incidentReport_ID' =>  $ir->incidentReport_ID,
                'emp_id' =>  $recommenders[$loopRec]['emp_id'],
                // 'level' =>  $loopRec + 1,
                'disciplinaryActionCategory_ID' =>  $ir->disciplinaryActionCategory_ID,
                'incidentReportRecommendationType_ID' => 1,
            ];
        }
        $filtered_recommender_by_ero = array_filter(
            $ir_recommendation,
            function ($e) use ($assigned_ero) {
                return ($e->emp_id !==  $assigned_ero);
            }
        );
        $final_normal_recommenders = array_merge($filtered_recommender_by_ero);
        $final_normal_recommenders[count($final_normal_recommenders)] = (object) [
            'incidentReport_ID' =>  $ir->incidentReport_ID,
            'emp_id' =>  $assigned_ero,
            'disciplinaryActionCategory_ID' =>  $ir->disciplinaryActionCategory_ID,
            'incidentReportRecommendationType_ID' => 2,
        ];
        foreach ($final_normal_recommenders as $index => $row) {
            if ($index + 1 == count($final_normal_recommenders)) {
                $status = 2;
            } else if ($index > 0 &&  $recommendationFlow == 1) {
                $status = 2;
            }
            $row->{'liabilityStat_ID'} = $status;
            $row->{'level'} = $index + 1;
            $filtered_recommendation[$index] = $row;
        }
        $data['recommenders'] =  $filtered_recommendation;
        $data['first_id'] =  $this->general_model->batch_insert_first_inserted_id($filtered_recommendation, 'tbl_dms_incident_report_recommendation');
        return  $data;
    }
    private function set_ir_recommendation_test($recommenders,  $ir,  $acc_id,  $site_id)
    {
        $ir_recommendation = [];
        $filtered_recommendation = [];
        $rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
        $recommendationFlow = (int) $rec_deadline['record']->deadlineToRecommendOption;
        $assigned_ero =  $this->check_if_ero_is_subject($acc_id,  $site_id,  $ir->subjectEmp_ID);
        $status = 4;
        for ($loopRec = 0; $loopRec < count($recommenders); $loopRec++) {
            $ir_recommendation[$loopRec] = (object) [
                'incidentReport_ID' =>  $ir->incidentReport_ID,
                'emp_id' =>  $recommenders[$loopRec]['emp_id'],
                // 'level' =>  $loopRec + 1,
                'disciplinaryActionCategory_ID' =>  $ir->disciplinaryActionCategory_ID,
                'incidentReportRecommendationType_ID' => 1,
            ];
        }
        $filtered_recommender_by_ero = array_filter(
            $ir_recommendation,
            function ($e) use ($assigned_ero) {
                return ($e->emp_id !==  $assigned_ero);
            }
        );
        $final_normal_recommenders = array_merge($filtered_recommender_by_ero);
        $final_normal_recommenders[count($final_normal_recommenders)] = (object) [
            'incidentReport_ID' =>  $ir->incidentReport_ID,
            'emp_id' =>  $assigned_ero,
            'disciplinaryActionCategory_ID' =>  $ir->disciplinaryActionCategory_ID,
            'incidentReportRecommendationType_ID' => 2,
        ];
        foreach ($final_normal_recommenders as $index => $row) {
            if ($index + 1 == count($final_normal_recommenders)) {
                $status = 2;
            } else if ($index > 0 &&  $recommendationFlow == 1) {
                $status = 2;
            }
            $row->{'liabilityStat_ID'} = $status;
            $row->{'level'} = $index + 1;
            $filtered_recommendation[$index] = $row;
        }
        $data['recommenders'] =  $filtered_recommendation;
        // $data['first_id'] =  $this->general_model->batch_insert_first_inserted_id($filtered_recommendation, 'tbl_dms_incident_report_recommendation');
        return  $data;
    }

    private function notify_by_flow($recommenders, $incident_report)
    {
        $normal_recommenders = [];
        $normal_recommenders_count = 0;
        $rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
        $recommendationFlow = (int) $rec_deadline['record']->deadlineToRecommendOption;
        if ((int) $recommendationFlow == 1) {
            $emp_details = $this->get_emp_details_via_emp_id($recommenders[0]->emp_id);
            if ($recommenders[0]->incidentReportRecommendationType_ID == 2) {
                $notif_mssg = "You are notified to give <b>Last Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                $link = "discipline/dms_ero/eroRecommendationTab";
            } else {
                $notif_mssg = "You are notified to give <b>1st Level Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                $link = "discipline/dms_supervision/irSupervision/irSup_recommendation";
            }
            $data['notif_id'] = $this->simple_system_notification($notif_mssg, $link, $emp_details->uid, 'dms');
        } else {
            foreach ($recommenders as $recommenders_rows) {
                if ($recommenders_rows->incidentReportRecommendationType_ID == 1) {
                    $emp_details = $this->get_emp_details_via_emp_id($recommenders_rows->emp_id);
                    $normal_recommenders[$normal_recommenders_count] = [
                        "userId" => $emp_details->uid
                    ];
                    $normal_recommenders_count++;
                }
            }
            if (count($normal_recommenders) > 0) {
                $notif_mssg = "You are notified to give <b>Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                $link = "discipline/dms_supervision/irSupervision/irSup_recommendation";
                $notif_id[0] = $this->create_system_notif($notif_mssg, $link, 'dms');
                $data['notif_id'] = $notif_id;
                $data['send_stat'] = $this->send_system_notif($notif_id[0], $normal_recommenders);
            } else {
                $ero_details = $this->get_emp_details_via_emp_id($recommenders[0]->emp_id);
                $notif_mssg = "You are notified to give <b>Last Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                $link = "discipline/dms_ero";
                $data['notif_id'] = $this->simple_system_notification($notif_mssg, $link, $ero_details->uid, 'dms');
            }
        }
        return $data;
    }

    public function set_current_recommendation_option($ir_id){
        $rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
        $recommendationFlow = (int) $rec_deadline['record']->deadlineToRecommendOption;
        $data['deadlineToRecommendOption'] = $recommendationFlow;
        $where = "incidentReport_ID = $ir_id";
        $this->general_model->update_vals($data, $where, 'tbl_dms_incident_report');
    }

    public function set_recommenders($ir_id)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $ir = $this->get_ir($ir_id);
        // UPDATE ir reccommendation option
        $this->set_current_recommendation_option($ir_id);
        $exempt = [];
        $rec_num = 0;
        if (count($ir) > 0) {
            $emp_details =  $this->get_emp_details_via_emp_id($ir->subjectEmp_ID);
            // get recommendation number
            $acc_rec_num =  $this->get_account_recommendation_num_via_account($emp_details->acc_id);
            // get exemptions             
            if (count($acc_rec_num) > 0) {
                $rec_num =  $acc_rec_num->number;
            } else {
                $rec_num =  $this->default_recommendation_number();
            }
            $supervisors = $this->get_supervisors($ir->subjectEmp_ID, $rec_num);
            if ($supervisors['error'] != 1) {
                $data['recommenders'] = $this->filter_exemption_or_reassignment($supervisors, $emp_details->acc_id, $ir->subjectEmp_ID, $emp_details->site_ID);
                $data['set_recommendation'] = $this->set_ir_recommendation($data['recommenders'], $ir, $emp_details->acc_id, $emp_details->site_ID);
                $data['set_deadline'] = $this->set_recommmendation_deadline($data['set_recommendation']['recommenders'], $ir, $data['set_recommendation']['first_id']);
                $data['notification'] = $this->notify_by_flow($data['set_recommendation']['recommenders'], $ir);
            } else {
                $data['error'] = 1;
                $data['error_details'] =  $supervisors['error_details'];
            }
        } else {
            $data['error'] = 1;
            $data['error_details'] = "IR Does not exist";
        }
        return $data;
    }

    public function set_recommenders_test($ir_id = 2520)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $ir = $this->get_ir($ir_id);
        // UPDATE ir reccommendation option
        $this->set_current_recommendation_option($ir_id);
        $exempt = [];
        $rec_num = 0;
        if (count($ir) > 0) {
            $emp_details =  $this->get_emp_details_via_emp_id($ir->subjectEmp_ID);
            // get recommendation number
            $acc_rec_num =  $this->get_account_recommendation_num_via_account($emp_details->acc_id);
            // get exemptions             
            if (count($acc_rec_num) > 0) {
                $rec_num =  $acc_rec_num->number;
            } else {
                $rec_num =  $this->default_recommendation_number();
            }
            $supervisors = $this->get_supervisors($ir->subjectEmp_ID, $rec_num);
            if ($supervisors['error'] != 1) {
                $data['recommenders'] = $this->filter_exemption_or_reassignment($supervisors, $emp_details->acc_id, $ir->subjectEmp_ID, $emp_details->site_ID);
                var_dump($data['recommenders']);
                $data['set_recommendation'] = $this->set_ir_recommendation($data['recommenders'], $ir, $emp_details->acc_id, $emp_details->site_ID);
                var_dump($data['set_recommendation']);$data['set_recommendation'];
                // $data['set_deadline'] = $this->set_recommmendation_deadline($data['set_recommendation']['recommenders'], $ir, $data['set_recommendation']['first_id']);
                // $data['notification'] = $this->notify_by_flow($data['set_recommendation']['recommenders'], $ir);
            } else {
                $data['error'] = 1;
                $data['error_details'] =  $supervisors['error_details'];
            }
        } else {
            $data['error'] = 1;
            $data['error_details'] = "IR Does not exist";
        }
        return $data;
    }

    public function deadline_final_decision($ir_recommendation_id)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $current_recommendation = $this->get_current_recommendation($ir_recommendation_id);
        $rec_deadline = $this->get_ir_deadline_settings_details_recorded();
        $dateTime = $this->get_current_date_time();
        if (count($current_recommendation) > 0) {
            if ($current_recommendation->incidentReportRecommendationType_ID == 3) { // final
                $days_final_recommend = $rec_deadline['record']->deadlineFinalDecision;
                $recommend_date = Date("Y-m-d", strtotime($dateTime['date'] . "$days_final_recommend days"));
                $deadline_to_recommend = $recommend_date . " 23:59:59";
            }
            $data['set_deadline_final_recommend_stat'] = $this->create_individual_deadline($current_recommendation->incidentReportRecommendation_ID, $current_recommendation->emp_ID, $deadline_to_recommend, 5, $current_recommendation->level);
        } else {
            $data['error'] = 0;
            $data['error_details'] = "IR recommendation record not found";
        }
        return $data;
    }

    public function deadline_necessary_doc($ir_id)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $necessary_docs = $this->get_necessary_docs_details($ir_id);
        $incident_report = $this->get_ir($ir_id);
        if (count($necessary_docs) > 0) {
            $rec_deadline = $this->get_ir_deadline_settings_details_recorded();
            $dateTime = $this->get_current_date_time();
            $emp_details =  $this->get_emp_details_via_emp_id($necessary_docs->eroEmp_ID);
            $upload_date = Date("Y-m-d", strtotime($dateTime['date'] . $rec_deadline['record']->deadlineUploadDocs . " days"));
            $deadline_to_upload = $upload_date . " 23:59:59";
            $data['upload_docs_deadline_stat'] = $this->create_individual_deadline($necessary_docs->irNecessaryDocument_ID, $necessary_docs->eroEmp_ID, $deadline_to_upload, 7, 1);
            $notif_mssg = "You are notified to <b>Upload Necessary Document/s</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "discipline/dms_ero/eroUploadTab";
            $data['notif_id'] = $this->simple_system_notification($notif_mssg, $link, $emp_details->uid, 'dms');
        } else {
            $data['error'] = 1;
            $data['error_details'] = "Necessary Docs Record not found";
        }
        return $data;
    }

    public function get_current_recommendation($ir_recommendation_id)
    {
        $fields = "incidentReportRecommendation_ID, incidentReport_ID, emp_ID, liabilityStat_ID, level, incidentReportRecommendationType_ID";
        $where = "incidentReportRecommendation_ID = $ir_recommendation_id";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_incident_report_recommendation');
    }

    public function get_next_recommendation($ir_id, $next_level)
    {
        $fields = "recommend.incidentReportRecommendation_ID, recommend.incidentReport_ID, recommend.emp_ID, recommend.liabilityStat_ID, recommend.level, recommend.datedLiabilityStat, recommend.disciplinaryActionCategory_ID, recommend.incidentReportRecommendationType_ID, recommendType.type, recommend.notes, recommend.susTermDetails, dead.dmsDeadline_ID, dead.deadline";
        $where = "recommendType.incidentReportRecommendationType_ID = recommend.incidentReportRecommendationType_ID AND dead.record_ID = recommend.incidentReportRecommendation_ID AND dead.emp_id = recommend.emp_ID AND recommend.incidentReport_ID = $ir_id AND recommend.level = $next_level";
        $table = "tbl_dms_deadline dead, tbl_dms_incident_report_recommendation recommend, tbl_dms_incident_report_recommendation_type recommendType";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function get_ero_recommendation($ir_id)
    {
        $fields = "recommend.incidentReportRecommendation_ID, recommend.incidentReport_ID, recommend.emp_ID, recommend.liabilityStat_ID, recommend.level, recommend.datedLiabilityStat, recommend.disciplinaryActionCategory_ID, recommend.incidentReportRecommendationType_ID, recommendType.type, recommend.notes, recommend.susTermDetails, dead.dmsDeadline_ID, dead.deadline";
        $where = "recommendType.incidentReportRecommendationType_ID = recommend.incidentReportRecommendationType_ID AND dead.record_ID = recommend.incidentReportRecommendation_ID AND dead.emp_id = recommend.emp_ID AND recommend.incidentReport_ID = $ir_id AND recommend.incidentReportRecommendationType_ID = 2";
        $table = "tbl_dms_deadline dead, tbl_dms_incident_report_recommendation recommend, tbl_dms_incident_report_recommendation_type recommendType";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function get_all_pending_recommendation($ir_id)
    {
        $fields = "recommend.incidentReportRecommendation_ID, recommend.incidentReport_ID, recommend.emp_ID, recommend.liabilityStat_ID, recommend.level, recommend.datedLiabilityStat, recommend.disciplinaryActionCategory_ID, recommend.incidentReportRecommendationType_ID, recommendType.type, recommend.notes, recommend.susTermDetails, dead.dmsDeadline_ID, dead.deadline";
        $where = "recommendType.incidentReportRecommendationType_ID = recommend.incidentReportRecommendationType_ID AND dead.record_ID = recommend.incidentReportRecommendation_ID AND dead.emp_id = recommend.emp_ID AND recommend.incidentReport_ID = $ir_id AND recommend.liabilityStat_ID = 4";
        $table = "tbl_dms_deadline dead, tbl_dms_incident_report_recommendation recommend, tbl_dms_incident_report_recommendation_type recommendType";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    public function get_necessary_docs_details($ir_id)
    {
        $fields = "	irNecessaryDocument_ID, incidentReport_ID, incidentReportAttachmentPurpose_ID, eroEmp_ID, status_ID";
        $where = "incidentReport_ID = $ir_id";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_necessary_document');
    }


    public function sequential_recommendation($current_recommendation, $incident_report)
    {
        $data['error'] = 0;
        $data['error_details'] = "";

        if ((int) $current_recommendation->incidentReportRecommendationType_ID == 1) {
            // get next level recommendation
            $next_recommendation = $this->get_next_recommendation($current_recommendation->incidentReport_ID, (int) $current_recommendation->level + 1);
            if (count($next_recommendation) > 0) {
                $next_level_emp_details = $this->get_emp_details_via_emp_id($next_recommendation->emp_ID);
                $data['update_next_recommendation_stat'] = $this->update_recommendation($next_recommendation->incidentReportRecommendation_ID, 4);
                if ($next_recommendation->incidentReportRecommendationType_ID == 1) {
                    $notif_mssg = "You are notified to give <b>" . $this->ordinal($next_recommendation->level) . " Level Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                    $link = "discipline/dms_supervision/irSupervision/irSup_recommendation";
                } else {
                    $notif_mssg = "You are notified to give <b>" . ucfirst($next_recommendation->type) . " Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                    $link = "discipline/dms_ero/eroRecommendationTab";
                }
                $data['notif_id'] = $this->simple_system_notification($notif_mssg, $link, $next_level_emp_details->uid, 'dms');
            } else {
                $data['error'] = 1;
                $data['error_details'] = "Next Recommender Not Found";
            }
        }
        //var_dump($data);
    }

    public function parallel_recommendation($incident_report)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $all_pending = $this->get_all_pending_recommendation($incident_report->incidentReport_ID);
        if (count($all_pending) > 0) {
            // not proceed to ero
        } else {
            // proceed to ero
            $ero_recommendation = $this->get_ero_recommendation($incident_report->incidentReport_ID);
            if (count($ero_recommendation) > 0) {
                ////var_dump($ero_recommendation);
                $ero_emp_details = $this->get_emp_details_via_emp_id($ero_recommendation->emp_ID);
                $data['update_ero_stat'] = $this->update_recommendation($ero_recommendation->incidentReportRecommendation_ID, 4);
                $notif_mssg = "You are notified to give <b>" . ucfirst($ero_recommendation->type) . " Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
                $link = "discipline/dms_ero/eroRecommendationTab";
                $data['notif_id'] = $this->simple_system_notification($notif_mssg, $link, $ero_emp_details->uid, 'dms');
            } else {
                $data['error'] = 1;
                $data['error_details'] = "ERO Recommender Not Found";
            }
        }
        return $data;
    }

    public function after_recommmending($ir_recommendation_id)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        //SET recommendation to completed for the current recommender
        $current_recommendation = $this->get_current_recommendation($ir_recommendation_id);
        if (count($current_recommendation) > 0) {
            if ((int) $current_recommendation->incidentReportRecommendationType_ID == 1) {
                $deadline_type = 3;
            } else {
                $deadline_type = 4;
            }
            $dead['status_ID'] = 3;
            // $data['set_deadline_to_complete'] = $this->update_dms_deadline($dead, $current_recommendation->incidentReportRecommendation_ID, $current_recommendation->emp_ID, $deadline_type);
            $data['set_deadline_to_complete'] = $this->update_dms_deadline_with_type($current_recommendation->incidentReportRecommendation_ID, $current_recommendation->emp_ID, $deadline_type, $dead);
            $incident_report = $this->get_ir($current_recommendation->incidentReport_ID);
            if (count($incident_report) > 0) {
                if ($incident_report->deadlineToRecommendOption == 1) {
                    ////var_dump($incident_report);
                    //     // sequential
                    $this->sequential_recommendation($current_recommendation, $incident_report);
                } else {
                    // parallel
                    $this->parallel_recommendation($incident_report);
                }
            } else {
                $data['error'] = 1;
                $data['error_details'] = "IR Record not found";
            }
        } else {
            $data['error'] = 1;
            $data['error_details'] = "Recommendation Record not found";
        }
        // ////var_dump($incident_report);
    }

    private function get_specific_exempt_via_account($acc_id)
    {
        $fields = "level, emp_id";
        $where = "account_ID =  $acc_id AND changeType = 'exempt'";
        return  $this->general_model->fetch_specific_vals($fields,  $where, 'tbl_dms_custom_recommendation');
    }

    private function get_specific_reassignment_via_account($acc_id)
    {
        $fields = "cusRec.level, cusRec.emp_id, change.emp_id as assignTo";
        $where = "change.dmsCustomRecommendation_ID = cusRec.dmsCustomRecommendation_ID AND cusRec.account_ID =  $acc_id AND cusRec.changeType = 'change'";
        $tables = "tbl_dms_custom_recommendation cusRec, tbl_dms_custom_recommendation_change change";
        return  $this->general_model->fetch_specific_vals($fields,  $where,  $tables);
    }

    private function get_assigned_ero($acc_id,  $site_id)
    {
        $fields = "emp_id";
        $where = "acc_id =  $acc_id AND site_ID =  $site_id";
        return  $this->general_model->fetch_specific_val($fields,  $where, "tbl_dms_employee_relations_officer");
    }

    private function get_ero_supervisor($emp_id)
    {
        $fields = "employeeRelationsSupervisor";
        $where = "emp_id =  $emp_id";
        return  $this->general_model->fetch_specific_val($fields,  $where, "tbl_dms_employee_relations_supervisor");
    }

    private function check_evidences($ir_id)
    {
        $fields = "COUNT(ir_attachment_ID) ir_attachment";
        $where = "incidentReport_ID =  $ir_id AND incidentReportAttachmentPurpose_ID = 1";
        return  $this->general_model->fetch_specific_val($fields,  $where, 'tbl_dms_incident_report_attachment');
    }

    private function check_ir_attachment($ir_id, $purpose_id)
    {
        $fields = "COUNT(ir_attachment_ID) ir_attachment";
        $where = "incidentReport_ID =  $ir_id AND incidentReportAttachmentPurpose_ID = $purpose_id";
        return  $this->general_model->fetch_specific_val($fields,  $where, 'tbl_dms_incident_report_attachment');
    }

    private function qry_ir_history($ir_id,  $prescriptive_id, $occurence)
    {
        $fields = "ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.dateTimeFiled, ir.datedLiabilityStat, ir.liabilityStat, sugDiscAction.action sugAction, sugDiscAction.abbreviation sugAbbreviation, discAction.abbreviation,  discAction.disciplinaryAction_ID, discAction.action, discAction.label, ir.prescriptiveId, ir.prescriptionEnd, discActionCateg.level";
        $where = "sugDiscAction.disciplinaryAction_ID = sugDiscActionCateg.disciplinaryAction_ID AND sugDiscActionCateg.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discActionCateg.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND ir.prescriptiveId =  $prescriptive_id AND ir.incidentReport_ID !=  $ir_id AND ir.occurence < $occurence";
        $table = "tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_discipline_category discCateg, tbl_dms_disciplinary_action_category sugDiscActionCateg, tbl_dms_disciplinary_action sugDiscAction";
        return  $this->general_model->fetch_specific_vals($fields,  $where,  $table, 'ir.incidentReport_ID ASC');
    }

    private function qry_suggested_disc_action($ir_id)
    {
        $fields = "ir.incidentReport_ID, discActionCateg.level, discAction.action, discAction.abbreviation, categ.category, discAction.periodMonth, discAction.label";
        $where = "categ.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND  discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discActionCateg.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND ir.incidentReport_ID =  $ir_id";
        $table = "tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_discipline_category categ";
        return  $this->general_model->fetch_specific_val($fields,  $where,  $table);
    }

    private function qry_witnesses($ir_id)
    {
        $fields = "witness.incidentReportWitness_ID, app.fname, app.mname, app.lname, app.nameExt, statuses.status_ID, statuses.description, witness.datedStatus";
        $where = "statuses.status_ID = witness.status_ID AND app.apid = emp.apid AND emp.emp_id = witness.witnessEmp_ID AND witness.status_ID = 24 and witness.incidentReport_ID =  $ir_id";
        $table = "tbl_dms_incident_report_witness witness, tbl_applicant app, tbl_employee emp, tbl_status statuses";
        return  $this->general_model->fetch_specific_vals($fields,  $where,  $table);
    }

    protected function qry_pending_witnesses($ir_id)
    {
        $fields = "witness.incidentReportWitness_ID, app.fname, app.mname, app.lname, app.nameExt, statuses.status_ID, statuses.description, witness.datedStatus";
        $where = "statuses.status_ID = witness.status_ID AND app.apid = emp.apid AND emp.emp_id = witness.witnessEmp_ID AND witness.status_ID = 2 and witness.incidentReport_ID =  $ir_id";
        $table = "tbl_dms_incident_report_witness witness, tbl_applicant app, tbl_employee emp, tbl_status statuses";
        return  $this->general_model->fetch_specific_vals($fields,  $where,  $table);
    }

    private function qry_witnesses_by_stat($ir_id, $status_id)
    {
        $fields = "app.fname, app.mname, app.lname, app.nameExt, statuses.status_ID, statuses.description, witness.datedStatus";
        $where = "statuses.status_ID = witness.status_ID AND app.apid = emp.apid AND emp.emp_id = witness.witnessEmp_ID and witness.incidentReport_ID =  $ir_id AND witness.status_ID = $status_id";
        $table = "tbl_dms_incident_report_witness witness, tbl_applicant app, tbl_employee emp, tbl_status statuses";
        return  $this->general_model->fetch_specific_vals($fields,  $where,  $table);
        // return null;
    }

    private function qry_ir_recommendation($ir_id)
    {
        $fields = "irRecommendation.incidentReportRecommendation_ID, app.fname, app.mname, app.lname, app.nameExt, app.pic, irRecommendation.datedLiabilityStat, statuses.description, irRecommendation.liabilityStat_ID, irRecommendation.level recommendationLevel, discActionCateg.level actionLevel, discAction.action, discAction.label, discAction.abbreviation, discAction.label, irRecommendation.datedLiabilityStat, irRecommendation.incidentReportRecommendationType_ID, irRecommendationType.type, irRecommendation.notes, irRecommendation.susTermDetails";
        $where = "statuses.status_ID = irRecommendation.liabilityStat_ID AND app.apid = emp.apid AND emp.emp_id = irRecommendation.emp_ID AND discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND  discActionCateg.disciplinaryActionCategory_ID = irRecommendation.disciplinaryActionCategory_ID AND irRecommendationType.incidentReportRecommendationType_ID = irRecommendation.incidentReportRecommendationType_ID AND irRecommendation.incidentReport_ID =  $ir_id";
        $table = "tbl_dms_incident_report_recommendation irRecommendation, tbl_applicant app, tbl_employee emp, tbl_dms_incident_report_recommendation_type irRecommendationType, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_status statuses";
        return  $this->general_model->fetch_specific_vals($fields,  $where,  $table, 'irRecommendation.level ASC');
    }

    private function qry_specific_dtr_vio_type($dtr_vio_type_id)
    {
        $fields = "description";
        $where = "dtrViolationType_ID = $dtr_vio_type_id";
        return  $this->general_model->fetch_specific_val($fields,  $where,  'tbl_dtr_violation_type');
    }

    private function qry_max_prescriptive_id()
    {
        $fields = "MAX(prescriptiveId) as maxPrescriptiveId";
        $where = "incidentReport_ID IS NOT NULL";
        $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_incident_report');
        if (count($record) > 0) {
            return (int) $record->maxPrescriptiveId;
        } else {
            return 1;
        }
    }

    public function get_incident_report_history()
    {
        if (count($existing_ir) > 0) {
            $ir_history = $this->get_ir_history($qualified_record->emp_id, $qualified_record->offense_ID, $existing_ir->prescriptiveId);
        }
    }

    public function test_ir_view()
    {
        $ir_details = $this->get_ir(2);
        ////var_dump($ir_details);
    }

    public function get_recommendation_details()
    {
        $data['content'] = "";
        $data['exist'] = 0;
        $ir_id = $this->input->post('irId');
        $recommendation = $this->qry_ir_recommendation($ir_id);

        $ir_details = $this->get_ir($ir_id);
        $recommendation_details = "";
        // ////var_dump($recommendation);
        // ////var_dump($ir_details);
        if (count($recommendation) > 0) {
            foreach ($recommendation as $recommendation_row) {
                $recommendation_label = "";
                $rec_badge = "";
                $color = "btn-info";
                $spin = "";
                $liability_stat = "Pending";
                $recommendation_stat = "Pending since";
                $notes = "No Notes found.";
                $icon_stat = "fa-spinner";
                $recommendation_action = "";
                // Recommendation label
                if ($ir_details->deadlineToRecommendOption == 1 && $recommendation_row->incidentReportRecommendationType_ID == 1) {
                    $recommendation_label = "Recommender " . $recommendation_row->recommendationLevel;
                } else {
                    $recommendation_label = ucwords($recommendation_row->type) . " Recommendation";
                    if ($recommendation_row->incidentReportRecommendationType_ID != 4) {
                        $recommendation_label .= " (ERO)";
                        if ($recommendation_row->label == 's' && $recommendation_row->incidentReportRecommendationType_ID == 2) { }
                    }
                }
                if ($recommendation_row->liabilityStat_ID == 4) {
                    $spin = "fa-spin";
                    $recommendation_action = '<div class="col-md-12 mt-2" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: #b70000;"><span style="color: #585858;">Waiting for Recommendation</div>';
                    $rec_badge = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink ml-2" style="min-height: 7px;min-width: 7px;"></span>';
                } else if ($recommendation_row->liabilityStat_ID == 2) {
                    $spin = "fa-spin";
                    $recommendation_action = '<div class="col-md-12 mt-2" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: #b70000;"><span style="color: #585858;">Waiting for Recommendation</div>';
                } else if ($recommendation_row->liabilityStat_ID == 17) {
                    $recommendation_action = '<div class="col-md-12 mt-2" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: #b70000;"><span style="color: #585858;">Recommendation: </span>' . ucwords($recommendation_row->action) . '</div>';
                    $icon_stat = "fa-thumbs-o-up";
                    $recommendation_stat = "Recommended last";
                    $liability_stat = $recommendation_row->description;
                    $color = "btn-danger";
                    // var_dump($recommendation_row->label);
                    if ($recommendation_row->label == 's') {
                        $dayLabel = "Day";
                        if ((int) $recommendation_row->incidentReportRecommendationType_ID == 2) {
                            if ((int) $recommendation_row->susTermDetails > 1) {
                                $dayLabel .= 's';
                            }
                            $recommendation_action = '<div class="col-md-12 mt-2" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: #b70000;"><span style="color: #585858;">Recommendation: </span>' . $recommendation_row->susTermDetails . ' ' . $dayLabel . ' Suspension</div>';
                        } else if ((int) $recommendation_row->incidentReportRecommendationType_ID == 3) {
                            $suspension_days = $this->get_suspension_days_value($ir_id);
                            if (count($suspension_days) > 0) {
                                $sus_day = 'Day';
                                if ((int) $suspension_days->days > 1) {
                                    $dayLabel .= 's';
                                }
                                $recommendation_action = '<div class="col-md-12 mt-2" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: #b70000;"><span style="color: #585858;">Recommendation: </span>' . $suspension_days->days . ' ' . $dayLabel . ' Suspension</div>';
                            }
                        } else if ((int) $recommendation_row->incidentReportRecommendationType_ID == 4) {
                            $client_confirmation = $this->get_client_confirmation_value($ir_id);
                            if (count($client_confirmation) > 0) {
                                $sus_day = 'Day';
                                $liability_stat = "Pending";
                                if ((int) $client_confirmation->suggestion > 1) {
                                    $dayLabel .= 's';
                                }
                                $recommendation_action = '<div class="col-md-12 mt-2" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: #b70000;"><span style="color: #585858;">Recommendation: </span>' . $client_confirmation->suggestion . ' ' . $dayLabel . ' Suspension</div>';
                                $liability_stat = $client_confirmation->description;
                            }
                        }
                    } else if ($recommendation_row->label == 't') {
                        if ((int) $recommendation_row->incidentReportRecommendationType_ID == 2) {
                            $recommendation_action = '<div class="col-md-12 mt-2" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: #b70000;"><span style="color: #585858;">Recommendation: </span> Terminate on ' . date_format(date_create($recommendation_row->susTermDetails), "M j, Y")  . '</div>';
                        } else if ((int) $recommendation_row->incidentReportRecommendationType_ID == 3) {
                            $termination_date = $this->get_termination_date($ir_id);
                            if (count($termination_date) > 0) {
                                $recommendation_action = '<div class="col-md-12 mt-2" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: #b70000;"><span style="color: #585858;">Recommendation: </span>Terminate on ' . date_format(date_create($termination_date->date), "M j, Y")  . '</div>';
                            }
                            $suspension_days = $this->get_suspension_days_value($ir_id);
                        } else if ((int) $recommendation_row->incidentReportRecommendationType_ID == 4) {
                            $client_confirmation = $this->get_client_confirmation_value($ir_id);
                            if (count($client_confirmation) > 0) {
                                $liability_stat = $client_confirmation->description;
                                $date = DateTime::createFromFormat("Y-m-d", $client_confirmation->suggestion);
                                if ($date == false) {
                                    $term_recommendation = 'N/A';
                                }else{
                                    $term_recommendation = 'Terminate on ' . date_format(date_create($client_confirmation->suggestion), "M j, Y");
                                }
                                $recommendation_action = '<div class="col-md-12 mt-2" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: #b70000;"><span style="color: #585858;">Recommendation: </span>'.$term_recommendation.'</div>';
                            }
                        }
                    }
                } else if ($recommendation_row->liabilityStat_ID == 18) {
                    $icon_stat = "fa-thumbs-o-down";
                    $recommendation_action = '';
                    $recommendation_stat = "Recommended last";
                    $liability_stat = $recommendation_row->description;
                    $color = "btn-success";
                } else {
                    $icon_stat = "fa-exclamation-circle m-animate-blink";
                    $recommendation_action = '';
                    $recommendation_stat = "Missed last";
                    $liability_stat = "Missed to give recommendaton";
                    $color = "btn-warning";
                }

                // notes
                if (trim($recommendation_row->notes) != '') {
                    $notes = $recommendation_row->notes;
                    if ($recommendation_row->incidentReportRecommendationType_ID == 4) {
                        $client_confirmation = $this->get_client_confirmation_value($ir_id);
                        if (count($client_confirmation) > 0) {
                            $notes = $client_confirmation->notes;
                        }
                    }
                }
                $dated_stat = "Pending";
                if ($recommendation_row->datedLiabilityStat !== '0000-00-00 00:00:00') {
                    $dated_stat = $recommendation_stat . ' ' . date_format(date_create($recommendation_row->datedLiabilityStat), "M j, Y");
                }
                $recommendation_details .= '<div class="m-portlet bg-secondary">';
                $recommendation_details .= '<div class="col-md-12 pt-2 pb-2" style="background: #505a6b30;">';
                $recommendation_details .= '<div class="row">';
                $recommendation_details .= '<div class="col-7 col-sm-7 col-md-7 pt-2">' . $recommendation_label . "" . $rec_badge . '</div>';
                $recommendation_details .= '<div class="col-5 col-sm-5 col-md-5">';
                $recommendation_details .= '<div class="btn pull-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;background: #505a6b;">';
                $recommendation_details .= '<div class="btn ' . $color . ' m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air ' . $spin . '" style="width: 25px !important;height: 25px !important;"><i class="fa ' . $icon_stat . '" style="font-size: 17 px;"></i></div><span class="button-content text-light text-capitalize pl-2">' . ucwords($liability_stat) . '</span>';
                $recommendation_details .= '</div>';
                $recommendation_details .= '</div>';
                $recommendation_details .= '</div>';
                $recommendation_details .= '</div>';
                $recommendation_details .= '<div class="m-portlet__body pt-3 pl-4 pb-3 pr-4">';
                $recommendation_details .= '<div class="row">';
                $recommendation_details .= '<div class="col-2 col-sm-2 col-md-2 d-none d-sm-block"><img src="' . base_url('/' . $recommendation_row->pic) . '" onerror="noImageFound(this)" width="58" alt="" class="mx-auto rounded-circle"></div>';
                $recommendation_details .= '<div class="col-12 col-sm-10 col-md-10 pl-1">';
                $recommendation_details .= '<div class="col-md-12 font-weight-bold" style="font-size: 15px;">' . ucwords($recommendation_row->fname . ' ' . mb_substr($recommendation_row->mname, 0, 1, "UTF-8") . ' ' . $recommendation_row->lname) . '</div>';
                $recommendation_details .= '<div class="col-md-12" style="font-size: 11px;margin-top: -3px;font-weight: bolder; color: #179e9e;">' . $dated_stat . ' </div>';
                $recommendation_details .= $recommendation_action;
                $recommendation_details .= '<div class="col-md-12 mt-3" style="font-size: 12px;"><i> - ' . $notes . '</i></div>';
                $recommendation_details .= '</div>';
                $recommendation_details .= '<div class="col-md-12 pt-3"></div>';
                $recommendation_details .= '</div>';
                $recommendation_details .= '</div>';
                $recommendation_details .= '</div>';
            }
            $data['content'] = $recommendation_details;
            $data['exist'] = 1;
        }
        echo json_encode($data);
    }

    public function get_ir_annexes_details()
    {
        $ir_id = $this->input->post('irId');
        $data['with_annexes'] = 0;
        $incident_report = $this->get_ir($ir_id);
        $data['emp_details'] = [
            'name' => ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname),
            'position' => ucwords($incident_report->subjectPosition),
            'pos_status' => ucwords($incident_report->subjectEmpStat),
            'account' => ucwords($incident_report->subjectAccount),
            'pic' => $incident_report->pic
        ];
        $annexes = $this->get_ir_annexes($ir_id);
        if (count($annexes)  > 0) {
            $data['with_annexes'] = 1;
            $data['num_annex'] = count($annexes);
            foreach ($annexes as $annexes_index => $annexes_row) {
                $annex_irs[$annexes_index] = $annexes_row->attachedIncidentReport_ID;
            }
            $annex_str = implode(",", array_values($annex_irs));
            $annex_details = $this->get_annexes_details($annex_str);
            // var_dump($annex_details);
            foreach ($annex_details as $annex_details_index => $annex_details_index_row) {
                $annex_witness = $this->qry_witnesses($annex_details_index_row->incidentReport_ID);
                $with_annex_witness = 0;
                if (count($annex_witness) > 0) {
                    $with_annex_witness = 1;
                }
                $data['annex_details'][$annex_details_index] = [
                    'details' => $annex_details_index_row,
                    'evidences' => $this->check_evidences($annex_details_index_row->incidentReport_ID),
                    'with_witnesses' => $with_annex_witness,
                    'witnesses' => $annex_witness
                ];
            }
        }
        echo json_encode($data);
    }

    public function get_raw_ir_details($ir_id)
    {
        $data['error'] = 0;
        $data['error_details'] = '';
        $data['finalized_stat'] = 0;
        $data['recommendation_stat'] = 0;
        $data['with_annex'] = 0;

        $annex_irs = [];
        $ir_details = $this->get_ir($ir_id);
        // ////var_dump($ir_details);
        if (count($ir_details) > 0) {
            $recommendation = $this->qry_ir_recommendation($ir_id);
            // ////var_dump($recommendation);
            $data['ir_details'] = $ir_details;
            $data['evidences'] = $this->check_ir_attachment($ir_id, 1); //evidence
            $data['explain_attachment'] = $this->check_ir_attachment($ir_id, 5); //evidence
            $data['dtr_violation_details'] = 'no function yet';
            $data['ir_history'] =  $this->qry_ir_history($ir_details->incidentReport_ID,  $ir_details->prescriptiveId, $ir_details->occurence);
            $data['suggested_disc_action'] =  $this->qry_suggested_disc_action($ir_id);
            $data['witnesses'] =  $this->qry_witnesses($ir_id);
            $annexes = $this->get_ir_annexes($ir_id);
            if (count($annexes) > 0) {
                $data['with_annex'] = 1;
                foreach ($annexes as $annexes_index => $annexes_row) {
                    $annex_irs[$annexes_index] = $annexes_row->attachedIncidentReport_ID;
                }
                $annex_str = implode(",", array_values($annex_irs));
                // $data['annex'] = $this->get_annexes_details($annex_str);
                $data['annex_num'] = count($this->get_annexes_details($annex_str));
            }

            if ($ir_details->incidentReportFilingType_ID == 2) {
                $data['dtr_violation_details'] = $this->get_auto_ir_qulified($ir_id);
                // ////var_dump($data['dtr_violation_details']);
            }
            if ($ir_details->incidentReportStages_ID == 3) {
                $data['finalized_stat'] = 1;
            }
            // ////var_dump($recommendation);
            if (count($recommendation) > 0) {
                $data['recommendation_stat'] = 1;
                // $normal_type = 1;
                // $normal_recommenders = array_filter(
                //     $recommendation,
                //     function ($e) use ($normal_type) {
                //         return  $e->incidentReportRecommendationType_ID ==  $normal_type;
                //     }
                // );
                // $ero_type = 2;
                // $ero = array_filter(
                //     $recommendation,
                //     function ($e) use ($ero_type) {
                //         return  $e->incidentReportRecommendationType_ID ==  $ero_type;
                //     }
                // );
                // $data['recommendation'] =  $recommendation;
                // $data['normal_recommenders'] =  $normal_recommenders;
                // $data['ero'] =  $ero;
            }
        } else {
            $data['error'] = 1;
            $data['error_details'] = 'IR Not Found';
        }
        return  $data;
    }

    public function get_ir_witnesses(){
        $data['exist'] = 0;
        $witness =  $this->qry_pending_witnesses($this->input->post('irId'));
        if(count($witness) > 0){
            $data['exist'] = 1;
            $data['witnesses'] = $witness;
        }
        echo json_encode($data);
    }

    public function get_witness_view(){
        $subject_details = "";
        $ir_details = $this->get_ir($this->input->post('irId'));
        $incident_time = new DateTime($ir_details->incidentDate . " " .  $ir_details->incidentTime);
        // $data['witnesses'] =  $this->qry_witnesses($this->input->post('irId'));
        $data['ir_details'] = $ir_details;
        $data['subject_details'] = '<div class="row">'.
            '<div class="col-sm-12 col-md-4 col-lg-3"><img src="' . base_url('/' .  $ir_details->pic) . '" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block"></div>'.
            '<div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">'.
                '<div class="row">'.
                    '<div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;">'. ucwords($ir_details->subjectFname . ' ' . $ir_details->subjectLname).'</div>'.
                    '<div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;">' . $ir_details->subjectPosition . '<span style="font-size: 11px;color: #0012ff;"> - ' . ucfirst($ir_details->subjectEmpStat) . '</span></div>'.
                    '<div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px">' . $ir_details->subjectAccount . '</div>'.
                '</div>'.
            '</div>'.
        '</div>';
        $data['incident'] = '<div class="col-12"><span class="font-weight-bold mr-2">IR-ID:</span>' . str_pad($ir_details->incidentReport_ID, 8, '0', STR_PAD_LEFT) . '</span></div>'.
            '<div class="col-12"><span class="font-weight-bold mr-2">Incident Date & Time:</span>' . date_format(date_create($ir_details->incidentDate), "M j, Y") .' '.  $incident_time->format('g:i A') . '</div>'.
        '<div class="col-12"><span class="font-weight-bold mr-2">Nature of Offense:</span><span class="">' . strtoupper($ir_details->letter) . '. ' . ucwords($ir_details->offenseType) . '</span></div>'.
        '<div class="col-12"><span class="font-weight-bold mr-2">Offense:</span><span class="font-italic" style="font-size: 13px; font-weight: 400; color: #c31717;">' . ucwords($ir_details->offense) . '</span></div>'.
        '<div class="col-12"><span class="font-weight-bold mr-2">Reported By:</span><span  style="font-size: 13px; font-weight: 400;">' . ucwords($ir_details->sourceFname . ' ' . $ir_details->sourceLname) . '</span></div>';
        echo json_encode($data);
    }

    public function get_layouted_ir_details($ir_id, $with_evidences = 0, $with_witnesses = 0, $with_source = 0, $with_subject_explanation = 0, $with_annex = 0, $with_recommendation = 0)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $raw_ir_details = $this->get_raw_ir_details($ir_id);
        $subject_details = "";
        $incident_details = "";
        $expected_action = "";
        $ir_history_details = "";
        $suggested_disc_action = "";
        $witness_details = "";
        $subject_explanation = "";
        $dtr_violation_link = "";
        $evidences_link = "";
        $evidence_word = "Evidence";
        $month_word = "Month";
        $source_name = "";
        $annex_details = "";
        $ir_details_layout = "";
        $ir_finalization = "";
        $subject_explanation_details = "";
        if ($raw_ir_details['error'] != 1) {
            $incident_time = new DateTime($raw_ir_details['ir_details']->incidentDate . " " .  $raw_ir_details['ir_details']->incidentTime);
            if ($raw_ir_details['ir_details']->incidentReportFilingType_ID == 2) {
                $dtr_violation_link = '<div class="col-12 mt-3"><span class="font-weight-bold mr-2">DTR Violation Record:</span><span class="viewIrDetails" id="viewQualifiedDetails" data-qualifiedid="' . $raw_ir_details['dtr_violation_details']->qualifiedUserDtrViolation_ID . '" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '">View Details</span></div> ';
            }
            if (($with_evidences == 1) && ((int) $raw_ir_details['evidences']->ir_attachment > 0)) {
                if ((int) $raw_ir_details['evidences']->ir_attachment > 1) {
                    $evidence_word . "s";
                }
                $evidences_link = '<div class="col-12 mt-3"><span class="font-weight-bold mr-2">Evidence/s:</span><span class="viewIrDetails" id="irEvidence" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '">' . (int) $raw_ir_details['evidences']->ir_attachment . ' Attached ' .  $evidence_word . '</span></div>';
            }
            if ($with_source) {
                $source_name = '<div class="col-12 mt-3"><span class="font-weight-bold mr-2">Reported By:</span><span class="">' . ucfirst($raw_ir_details['ir_details']->sourceFname) . ' ' . ucfirst($raw_ir_details['ir_details']->sourceLname) . '</span></div>';
            }
            if ($with_annex) {
                if ($raw_ir_details['with_annex']) {
                    $annex_num_label = $raw_ir_details['annex_num'] . " Similar Incident";
                    if (count($raw_ir_details['annex_num']) > 1) {
                        $annex_num_label .= "s";
                    }
                    $annex_details = '<div class="col-12 mt-3"><span class="font-weight-bold mr-2">Similar Reported Incidents:</span><span class="viewIrDetails" id="irAnnexes" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '">' . $annex_num_label . '</span></div>';
                }
            }
            if (count($raw_ir_details['ir_history']) > 0) {
                $ir_history_table_body = "";
                foreach ($raw_ir_details['ir_history'] as  $ir_history_row) {
                    $liability_date = new DateTime($ir_history_row->datedLiabilityStat);
                    $ir_incident_date = new DateTime($ir_history_row->incidentDate);
                    $cure_date_value = "N/A";
                    $final_label = strtoupper($ir_history_row->abbreviation);
                    if ($ir_history_row->label !== "t") {
                        $cure_date_value = date_format(date_create($ir_history_row->prescriptionEnd), "M j, Y");
                    }
                    if ($ir_history_row->liabilityStat == 18) {
                        $final_label = "Not-Liable";
                        $cure_date_value = "N/A";
                    }
                    $ir_history_table_body .= '<tr>';
                    $ir_history_table_body .=    '<td class="text-center">' . str_pad($ir_history_row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . '</td>';
                    $ir_history_table_body .=    '<td class="text-center">' .  $ir_incident_date->format('M j, Y') . '</td>';
                    $ir_history_table_body .=    '<td class="text-center">' .  strtoupper($ir_history_row->sugAbbreviation) . '</td>';
                    $ir_history_table_body .=    '<td class="text-center">' .  $final_label . '</td>';
                    $ir_history_table_body .=    '<td class="text-center">' . $cure_date_value . '</td>';
                    $ir_history_table_body .= '</tr>';
                }
                $ir_history_details .= '<div class="row p-3">';
                $ir_history_details .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR History</span>';
                $ir_history_details .= '<div class="col">';
                $ir_history_details .= '<hr style="margin-top: 0.8rem !important;">';
                $ir_history_details .= '</div>';
                $ir_history_details .= '</div>';
                $ir_history_details .= '</div>';
                $ir_history_details .= '<div class="col-12">';
                $ir_history_details .= '<div class="row pl-5 pr-5">';
                $ir_history_details .= '<div class="col-12 mt-2">';
                $ir_history_details .= '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">';
                $ir_history_details .= '<thead style="background: #555;color: white;">';
                $ir_history_details .= '<tr>';
                $ir_history_details .= '<th class="text-center">IR-ID</th>';
                $ir_history_details .= '<th class="text-center">Incident Date</th>';
                $ir_history_details .= '<th class="text-center">Suggested</th>';
                $ir_history_details .= '<th class="text-center">Final</th>';
                $ir_history_details .= '<th class="text-center">Cure Date</th>';
                $ir_history_details .= '</tr>';
                $ir_history_details .= '</thead>';
                $ir_history_details .= '<tbody>';
                $ir_history_details .=  $ir_history_table_body;
                $ir_history_details .= '</tbody>';
                $ir_history_details .= '</table>';
                $ir_history_details .= '</div>';
                $ir_history_details .= '</div>';
            }


            // Subject Details
            $subject_details .= '<div class="col-12 col-md-9">';
            $subject_details .=    '<div class="row">';
            $subject_details .=        '<div class="col-sm-12 col-md-4 col-lg-3">';
            $subject_details .=            '<img src="' . base_url('/'.$raw_ir_details['ir_details']->pic) . '" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">';
            $subject_details .=        '</div>';
            $subject_details .=        '<div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">';
            $subject_details .=            '<div class="row">';
            $subject_details .=                '<div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;">' . ucwords($raw_ir_details['ir_details']->subjectFname . ' ' . $raw_ir_details['ir_details']->subjectLname) . '</div>';
            $subject_details .=                '<div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;">' . $raw_ir_details['ir_details']->subjectPosition . '<span style="font-size: 11px;color: #0012ff;"> - ' . ucfirst($raw_ir_details['ir_details']->subjectEmpStat) . '</span></div>';
            $subject_details .=                '<div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px">' . $raw_ir_details['ir_details']->subjectAccount . '</div>';
            $subject_details .=            '</div>';
            $subject_details .=        '</div>';
            $subject_details .=    '</div>';
            $subject_details .= '</div>';
            $subject_details .= '<div class="col-12 col-md-3">';
            $subject_details .=    '<div class="row p-2">';
            $subject_details .=        '<div class="col-12">';
            $subject_details .=            '<div class="row pl-4 pr-4">';
            $subject_details .=                '<span  class="font-weight-bold mr-2">IR-ID:</span>';
            $subject_details .=                '<span>' . str_pad($raw_ir_details['ir_details']->incidentReport_ID, 8, '0', STR_PAD_LEFT) . '</span>';
            $subject_details .=            '</div>';
            $subject_details .=        '</div>';
            $subject_details .=        '<div class="col-12">';
            $subject_details .=            '<div class="row pl-4 pr-4">';
            $subject_details .=                '<span  class="font-weight-bold mr-2">Status:</span>';
            $subject_details .=                '<span>' . $raw_ir_details["ir_details"]->description . '</span>';
            $subject_details .=            '</div>';
            $subject_details .=        '</div>';
            $subject_details .=    '</div>';
            $subject_details .= '</div>';

            // Incident Details
            $incident_details .= '<div class="col-12">';
            $incident_details .= '<div class="row p-3">';
            $incident_details .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Incident</span>';
            $incident_details .= '<div class="col">';
            $incident_details .= '<hr style="margin-top: 0.8rem !important;">';
            $incident_details .= '</div>';
            $incident_details .= '</div>';
            $incident_details .= '<div class="col-12">';
            $incident_details .=     '<div class="row pl-5 pr-5">';
            $incident_details .=         '<div class="col-12 col-md-6">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Date:</span>';
            $incident_details .=             '<span class="">' . date_format(date_create($raw_ir_details['ir_details']->incidentDate), "M j, Y") . '</span>';
            $incident_details .=         '</div>  ';
            $incident_details .=         '<div class="col-12 col-md-6 text-md-right mt-2 mt-md-0">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Time:</span>';
            $incident_details .=             '<span class="">' .  $incident_time->format('g:i A') . '</span>';
            $incident_details .=         '</div>  ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Date Filed:</span>';
            $incident_details .=             '<span class="">' .  date_format(date_create($raw_ir_details['ir_details']->dateTimeFiled), "M j, Y") . '</span>';
            $incident_details .=         '</div> ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Place:</span>';
            $incident_details .=             '<span class="">' .  $raw_ir_details['ir_details']->place . '</span>';
            $incident_details .=         '</div> ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Nature of Offense:</span>';
            $incident_details .=             '<span class="">' . strtoupper($raw_ir_details['ir_details']->letter) . '. ' . ucwords($raw_ir_details['ir_details']->offenseType) . '</span>';
            $incident_details .=         '</div>  ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Offense:</span>';
            $incident_details .=             '<span class="font-italic" style="font-size: 13px; font-weight: 400; color: #c31717;">' . ucwords($raw_ir_details['ir_details']->offense) . '</span>';
            $incident_details .=         '</div>  ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Details:</span>';
            $incident_details .=             '<span class="">' . ucfirst($raw_ir_details['ir_details']->details) . '</span>';
            $incident_details .=         '</div> ';
            $incident_details .=          $dtr_violation_link;
            $incident_details .=          $evidences_link;
            $incident_details .=          $source_name;
            $incident_details .=          $annex_details;
            $incident_details .=     '</div>';
            $incident_details .= '</div>';

            // Expected course of Action
            $expected_action .=    '<div class="row p-3">';
            $expected_action .=        '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Expected Course of Action</span>';
            $expected_action .=        '<div class="col">';
            $expected_action .=            '<hr style="margin-top: 0.8rem !important;">';
            $expected_action .=        '</div>';
            $expected_action .=    '</div>';
            $expected_action .= '<div class="col-12">';
            $expected_action .=     '<div class="col-12 pl-5 pr-5">';
            $expected_action .=         '<div class="col-md-12 ahr_quote pt-3 pb-1 text-center" style="background: #c6fff6!important;">';
            $expected_action .=             '<blockquote>';
            $expected_action .=                 '<i class="fa fa-quote-left mr-1"></i>';
            $expected_action .=                 '<span class="mb-0">' . ucfirst($raw_ir_details['ir_details']->expectedAction) . '</span>';
            $expected_action .=                 '<i class="fa fa-quote-right ml-1"></i>';
            $expected_action .=             '</blockquote>';
            $expected_action .=         '</div>';
            $expected_action .=     '</div>';
            $expected_action .= '</div>';


            if (count($raw_ir_details['suggested_disc_action']) > 0) {
                if ((int)$raw_ir_details['suggested_disc_action']->periodMonth > 1) {
                    $month_word .= "s";
                }
                $cure_details = "";
                $recommendation_details = "";
                if ($raw_ir_details['suggested_disc_action']->label != 't') {
                    $cure_details  .=         '<div class="col-12 col-md-4 font-weight-bold">Period:</div>';
                    $cure_details  .=         '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">' .  $raw_ir_details['suggested_disc_action']->periodMonth . ' ' .  $month_word . '</div>';
                }
                if ($with_recommendation) {
                    if ($raw_ir_details['recommendation_stat']) {
                        $recommendation_details  .=         '<div class="col-12 col-md-4 font-weight-bold">Recommendation:</div>';
                        $recommendation_details  .=         '<div class="col-12 col-md-8 pl-5 pl-md-0 viewIrDetails" style="font-size: 13px; font-weight: 400" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '" id="recommendationDetailsView">Click to View Details</div>';
                    }
                }
                $suggested_disc_action .= '<div class="row p-3">';
                $suggested_disc_action .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Suggested Disciplinary Action</span>';
                $suggested_disc_action .= '<div class="col">';
                $suggested_disc_action .= '<hr style="margin-top: 0.8rem !important;">';
                $suggested_disc_action .= '</div>';
                $suggested_disc_action .= '</div>';
                $suggested_disc_action .= '<div class="col-12">';
                $suggested_disc_action .=     '<div class="row pl-5 pr-5">';
                $suggested_disc_action .=         '<div class="col-12 col-md-4 font-weight-bold">Category:</div>';
                $suggested_disc_action .=         '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"> Category ' .  strtoupper($raw_ir_details['suggested_disc_action']->category) . '</div>';
                $suggested_disc_action .=         '<div class="col-12 col-md-4 font-weight-bold">Level:</div>';
                $suggested_disc_action .=         '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">' .  $this->ordinal($raw_ir_details['suggested_disc_action']->level) . ' Level</div>';
                $suggested_disc_action .=         '<div class="col-12 col-md-4 font-weight-bold">Disciplinary Action:</div>';
                $suggested_disc_action .=         '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 17px; font-weight: 500">' .  $raw_ir_details['suggested_disc_action']->action . '</div>';
                $suggested_disc_action .=  $cure_details;
                $suggested_disc_action .=  $recommendation_details;
                $suggested_disc_action .=     '</div>';
                $suggested_disc_action .= '</div>';
            }
            // Witnesses
            if (($with_witnesses == 1) && (count($raw_ir_details['witnesses']) > 0)) {
                $witness_table_body = "";
                foreach ($raw_ir_details['witnesses'] as  $ir_witness_row) {
                    //  $liability_date = new DateTime( $ir_history_row->datedLiabilityStat);
                    $witness_table_body .= '<tr>';
                    $witness_table_body .=    '<td class="text-center">' . ucfirst($ir_witness_row->fname) . ' ' . ucfirst($ir_witness_row->lname) . '</td>';
                    $witness_table_body .=    '<td class="text-center">' . ucfirst($ir_witness_row->description) . '</td>';
                    $witness_table_body .= '</tr>';
                }
                $witness_details .= '<div class="row p-3">';
                $witness_details .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Witnesses</span>';
                $witness_details .= '<div class="col">';
                $witness_details .= '<hr style="margin-top: 0.8rem !important;">';
                $witness_details .= '</div>';
                $witness_details .= '</div>';
                $witness_details .= '<div class="col-12">';
                $witness_details .= '<div class="row pl-5 pr-5">';
                $witness_details .= '<div class="col-12 mt-2">';
                $witness_details .= '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">';
                $witness_details .= '<thead style="background: #555;color: white;">';
                $witness_details .= '<tr>';
                $witness_details .= '<th class="text-center">Witness Name</th>';
                $witness_details .= '<th class="text-center">Confirmation</th>';
                $witness_details .= '</tr>';
                $witness_details .= '</thead>';
                $witness_details .= '<tbody>';
                $witness_details .=  $witness_table_body;
                $witness_details .= '</tbody>';
                $witness_details .= '</table>';
                $witness_details .= '</div>';
                $witness_details .= '</div>';
                $witness_details .= '</div>';
            }

            if ($with_subject_explanation == 1) {
                $with_explanation_sub = 0;
                if (trim($raw_ir_details['ir_details']->subjectExplanation) != '') {
                    $with_explanation_sub = 1;
                    $subject_explanation .= '<div class="col-12">';
                    $subject_explanation .=     '<div class="col-12 pl-5 pr-5">';
                    $subject_explanation .=         '<div class="col-md-12 ahr_quote pt-3 pb-1 text-center" style="background: #c6fff6!important;">';
                    $subject_explanation .=             '<blockquote>';
                    $subject_explanation .=                 '<i class="fa fa-quote-left mr-1"></i>';
                    $subject_explanation .=             '<span class="mb-0">' .  $raw_ir_details["ir_details"]->subjectExplanation . '</span>';
                    $subject_explanation .=             '<i class="fa fa-quote-right ml-1"></i>';
                    $subject_explanation .=             '</blockquote>';
                    $subject_explanation .=         '</div>';
                    $subject_explanation .=     '</div>';
                    $subject_explanation .= '</div>';
                }
                if ((int) $raw_ir_details['explain_attachment']->ir_attachment > 0) {
                    $with_explanation_sub = 1;
                    $subject_explanation .= '<div class="col-12">';
                    $subject_explanation .=     '<div class="row pl-5 pr-5">';
                    $subject_explanation .=         '<div class="col-12 mt-3">';
                    $subject_explanation .=             '<span class="font-weight-bold mr-2">Uploaded Explanation:</span>';
                    $subject_explanation .=             '<span class="viewIrDetails" id="uploadedExplanation" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '">View Attached Explanation</span>'; // UPLOADED EXPLANATION HERE
                    $subject_explanation .=         '</div> ';
                    $subject_explanation .=     '</div>';
                    $subject_explanation .= '</div>';
                }
                if ($with_explanation_sub) {
                    $subject_explanation_details .=     '<div class="row p-3">';
                    $subject_explanation_details .=         "<span class='ml-3' style='font-size: 14px;font-weight: 500;color: #5d4747;'><i class='la la-tag' style='font-size: 1.1rem;'></i>" . ucwords($raw_ir_details["ir_details"]->subjectFname) . "'s Explanation</span>";
                    $subject_explanation_details .=         '<div class="col">';
                    $subject_explanation_details .=             '<hr style="margin-top: 0.8rem !important;">';
                    $subject_explanation_details .=         '</div>';
                    $subject_explanation_details .=     '</div>';
                    $subject_explanation_details .=     $subject_explanation;
                }
            }

            if ($raw_ir_details['ir_details']->incidentReportStages_ID == 3) {
                if ((int) $raw_ir_details['ir_details']->liabilityStat == 17) {
                    $sanction = $raw_ir_details['ir_details']->finalAction;
                    $final_date_label = "Cure Date:";
                    $final_date = date_format(date_create($raw_ir_details['ir_details']->prescriptionEnd), "M j, Y");
                    if ($raw_ir_details['ir_details']->label == 's') {
                        $suspension_days = $this->get_suspension_days_value($raw_ir_details['ir_details']->incidentReport_ID);
                        if (count($suspension_days) > 0) {
                            $sus_day = 'Day';
                            if ((int) $suspension_days->days > 1) {
                                $sus_day .= "s";
                            }
                            $sanction = $suspension_days->days . " " . $sus_day . " Suspension";
                        }
                    } else if ($raw_ir_details['ir_details']->label == 't') {
                        $termination_date = $this->get_termination_date($ir_id);
                        if (count($termination_date) > 0) {
                            $final_date_label = "Effective on:";
                            $final_date = date_format(date_create($termination_date->date), "M j, Y");
                            $sanction = " Termination";
                        }
                    }
                    $ir_finalization .= '<div class="row p-3">';
                    $ir_finalization .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR Finalization</span>';
                    $ir_finalization .= '<div class="col">';
                    $ir_finalization .= '<hr style="margin-top: 0.8rem !important;">';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '<div class="col-12">';
                    $ir_finalization .= '<div class="row ml-5 mr-5 py-3" style=" background: #ffe0e0; border-left: #ff9090 0.6rem solid;">';
                    $ir_finalization .= '<div class="col-12 col-md-4 font-weight-bold" style="font-size: 1.3rem;">Liability Status:</div>';
                    $ir_finalization .= '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 21px;font-weight: 400;">' . ucwords($raw_ir_details['ir_details']->liabilityStatDesc) . '</div>';
                    $ir_finalization .= '<div class="col-12 col-md-4 font-weight-bold" style="font-size: 1.3rem;">Sanction:</div>';
                    $ir_finalization .= '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 21px;font-weight: 400;">' . ucwords($sanction) . '</div>';
                    $ir_finalization .= '<div class="col-12 col-md-4 font-weight-bold" style="font-size: 1rem;">' . $final_date_label . '</div>';
                    $ir_finalization .= '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 15px;font-weight: 400;">' . $final_date . '</div>';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '</div>';
                } else if ((int) $raw_ir_details['ir_details']->liabilityStat == 18) {
                    $ir_finalization .= '<div class="row p-3">';
                    $ir_finalization .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR Finalization</span>';
                    $ir_finalization .= '<div class="col">';
                    $ir_finalization .= '<hr style="margin-top: 0.8rem !important;">';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '<div class="col-12">';
                    $ir_finalization .= '<div class="row ml-5 mr-5 py-3" style="background: #e0ffed;border-left: #68ecc6 0.6rem solid;">';
                    $ir_finalization .= '<div class="col-12 col-md-5 font-weight-bold" style="font-size: 1.7rem;">Liability Status:</div>';
                    $ir_finalization .= '<div class="col-12 col-md-7 pl-5 pl-md-0" style="font-size: 26px;font-weight: 400;">Non-Liable</div>';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '</div>';
                }
            }

            $ir_details_layout =  $subject_details .  $incident_details .  $expected_action .  $ir_history_details .  $suggested_disc_action .  $witness_details .  $subject_explanation_details . $ir_finalization;
            $data['ir_details_layout'] =  $ir_details_layout;
        } else {
            $data['error'] = 1;
            $data['error_details'] = "";
        }
        return $data;
        // echo json_encode( $data);
    }

    public function get_layouted_ir_details_test($ir_id = 385, $with_evidences = 0, $with_witnesses = 0, $with_source = 0, $with_subject_explanation = 0, $with_annex = 0, $with_recommendation = 0)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $raw_ir_details = $this->get_raw_ir_details($ir_id);
        $subject_details = "";
        $incident_details = "";
        $expected_action = "";
        $ir_history_details = "";
        $suggested_disc_action = "";
        $witness_details = "";
        $subject_explanation = "";
        $dtr_violation_link = "";
        $evidences_link = "";
        $evidence_word = "Evidence";
        $month_word = "Month";
        $source_name = "";
        $annex_details = "";
        $ir_details_layout = "";
        $ir_finalization = "";
        $subject_explanation_details = "";
        if ($raw_ir_details['error'] != 1) {
            $incident_time = new DateTime($raw_ir_details['ir_details']->incidentDate . " " .  $raw_ir_details['ir_details']->incidentTime);
            if ($raw_ir_details['ir_details']->incidentReportFilingType_ID == 2) {
                $dtr_violation_link = '<div class="col-12 mt-3"><span class="font-weight-bold mr-2">DTR Violation Record:</span><span class="viewIrDetails" id="viewQualifiedDetails" data-qualifiedid="' . $raw_ir_details['dtr_violation_details']->qualifiedUserDtrViolation_ID . '" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '">View Details</span></div> ';
            }
            if (($with_evidences == 1) && ((int) $raw_ir_details['evidences']->ir_attachment > 0)) {
                if ((int) $raw_ir_details['evidences']->ir_attachment > 1) {
                    $evidence_word . "s";
                }
                $evidences_link = '<div class="col-12 mt-3"><span class="font-weight-bold mr-2">Evidence/s:</span><span class="viewIrDetails" id="irEvidence" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '">' . (int) $raw_ir_details['evidences']->ir_attachment . ' Attached ' .  $evidence_word . '</span></div>';
            }
            if ($with_source) {
                $source_name = '<div class="col-12 mt-3"><span class="font-weight-bold mr-2">Reported By:</span><span class="">' . ucfirst($raw_ir_details['ir_details']->sourceFname) . ' ' . ucfirst($raw_ir_details['ir_details']->sourceLname) . '</span></div>';
            }
            if ($with_annex) {
                if ($raw_ir_details['with_annex']) {
                    $annex_num_label = $raw_ir_details['annex_num'] . " Similar Incident";
                    if (count($raw_ir_details['annex_num']) > 1) {
                        $annex_num_label .= "s";
                    }
                    $annex_details = '<div class="col-12 mt-3"><span class="font-weight-bold mr-2">Similar Reported Incidents:</span><span class="viewIrDetails" id="irAnnexes" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '">' . $annex_num_label . '</span></div>';
                }
            }
            if (count($raw_ir_details['ir_history']) > 0) {
                $ir_history_table_body = "";
                foreach ($raw_ir_details['ir_history'] as  $ir_history_row) {
                    $liability_date = new DateTime($ir_history_row->datedLiabilityStat);
                    $cure_date_value = "N/A";
                    if ($ir_history_row->label !== "t") {
                        $cure_date_value = date_format(date_create($ir_history_row->prescriptionEnd), "M j, Y");
                    }
                    $ir_history_table_body .= '<tr>';
                    $ir_history_table_body .=    '<td class="text-center">' . str_pad($ir_history_row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . '</td>';
                    $ir_history_table_body .=    '<td class="text-center">' .  $liability_date->format('M j, Y') . '</td>';
                    $ir_history_table_body .=    '<td class="text-center">' .  strtoupper($ir_history_row->sugAbbreviation) . '</td>';
                    $ir_history_table_body .=    '<td class="text-center">' .  strtoupper($ir_history_row->abbreviation) . '</td>';
                    $ir_history_table_body .=    '<td class="text-center">' . $cure_date_value . '</td>';
                    $ir_history_table_body .= '</tr>';
                }
                $ir_history_details .= '<div class="row p-3">';
                $ir_history_details .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR History</span>';
                $ir_history_details .= '<div class="col">';
                $ir_history_details .= '<hr style="margin-top: 0.8rem !important;">';
                $ir_history_details .= '</div>';
                $ir_history_details .= '</div>';
                $ir_history_details .= '</div>';
                $ir_history_details .= '<div class="col-12">';
                $ir_history_details .= '<div class="row pl-5 pr-5">';
                $ir_history_details .= '<div class="col-12 mt-2">';
                $ir_history_details .= '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">';
                $ir_history_details .= '<thead style="background: #555;color: white;">';
                $ir_history_details .= '<tr>';
                $ir_history_details .= '<th class="text-center">IR-ID</th>';
                $ir_history_details .= '<th class="text-center">Incident Date</th>';
                $ir_history_details .= '<th class="text-center">Suggested</th>';
                $ir_history_details .= '<th class="text-center">Final</th>';
                $ir_history_details .= '<th class="text-center">Cure Date</th>';
                $ir_history_details .= '</tr>';
                $ir_history_details .= '</thead>';
                $ir_history_details .= '<tbody>';
                $ir_history_details .=  $ir_history_table_body;
                $ir_history_details .= '</tbody>';
                $ir_history_details .= '</table>';
                $ir_history_details .= '</div>';
                $ir_history_details .= '</div>';
            }


            // Subject Details
            $subject_details .= '<div class="col-12 col-md-9">';
            $subject_details .=    '<div class="row">';
            $subject_details .=        '<div class="col-sm-12 col-md-4 col-lg-3">';
            $subject_details .=            '<img src="' . base_url('/'.$raw_ir_details['ir_details']->pic) . '" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">';
            $subject_details .=        '</div>';
            $subject_details .=        '<div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">';
            $subject_details .=            '<div class="row">';
            $subject_details .=                '<div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;">' . ucwords($raw_ir_details['ir_details']->subjectFname . ' ' . $raw_ir_details['ir_details']->subjectLname) . '</div>';
            $subject_details .=                '<div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;">' . $raw_ir_details['ir_details']->subjectPosition . '<span style="font-size: 11px;color: #0012ff;"> - ' . ucfirst($raw_ir_details['ir_details']->subjectEmpStat) . '</span></div>';
            $subject_details .=                '<div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px">' . $raw_ir_details['ir_details']->subjectAccount . '</div>';
            $subject_details .=            '</div>';
            $subject_details .=        '</div>';
            $subject_details .=    '</div>';
            $subject_details .= '</div>';
            $subject_details .= '<div class="col-12 col-md-3">';
            $subject_details .=    '<div class="row p-2">';
            $subject_details .=        '<div class="col-12">';
            $subject_details .=            '<div class="row pl-4 pr-4">';
            $subject_details .=                '<span  class="font-weight-bold mr-2">IR-ID:</span>';
            $subject_details .=                '<span>' . str_pad($raw_ir_details['ir_details']->incidentReport_ID, 8, '0', STR_PAD_LEFT) . '</span>';
            $subject_details .=            '</div>';
            $subject_details .=        '</div>';
            $subject_details .=        '<div class="col-12">';
            $subject_details .=            '<div class="row pl-4 pr-4">';
            $subject_details .=                '<span  class="font-weight-bold mr-2">Status:</span>';
            $subject_details .=                '<span>' . $raw_ir_details["ir_details"]->description . '</span>';
            $subject_details .=            '</div>';
            $subject_details .=        '</div>';
            $subject_details .=    '</div>';
            $subject_details .= '</div>';

            // Incident Details
            $incident_details .= '<div class="col-12">';
            $incident_details .= '<div class="row p-3">';
            $incident_details .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Incident</span>';
            $incident_details .= '<div class="col">';
            $incident_details .= '<hr style="margin-top: 0.8rem !important;">';
            $incident_details .= '</div>';
            $incident_details .= '</div>';
            $incident_details .= '<div class="col-12">';
            $incident_details .=     '<div class="row pl-5 pr-5">';
            $incident_details .=         '<div class="col-12 col-md-6">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Date:</span>';
            $incident_details .=             '<span class="">' . date_format(date_create($raw_ir_details['ir_details']->incidentDate), "M j, Y") . '</span>';
            $incident_details .=         '</div>  ';
            $incident_details .=         '<div class="col-12 col-md-6 text-md-right mt-2 mt-md-0">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Time:</span>';
            $incident_details .=             '<span class="">' .  $incident_time->format('g:i A') . '</span>';
            $incident_details .=         '</div>  ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Date Filed:</span>';
            $incident_details .=             '<span class="">' .  date_format(date_create($raw_ir_details['ir_details']->dateTimeFiled), "M j, Y") . '</span>';
            $incident_details .=         '</div> ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Place:</span>';
            $incident_details .=             '<span class="">' .  $raw_ir_details['ir_details']->place . '</span>';
            $incident_details .=         '</div> ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Nature of Offense:</span>';
            $incident_details .=             '<span class="">' . strtoupper($raw_ir_details['ir_details']->letter) . '. ' . ucwords($raw_ir_details['ir_details']->offenseType) . '</span>';
            $incident_details .=         '</div>  ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Offense:</span>';
            $incident_details .=             '<span class="font-italic" style="font-size: 13px; font-weight: 400; color: #c31717;">' . ucwords($raw_ir_details['ir_details']->offense) . '</span>';
            $incident_details .=         '</div>  ';
            $incident_details .=         '<div class="col-12 mt-3">';
            $incident_details .=             '<span class="font-weight-bold mr-2">Details:</span>';
            $incident_details .=             '<span class="">' . ucfirst($raw_ir_details['ir_details']->details) . '</span>';
            $incident_details .=         '</div> ';
            $incident_details .=          $dtr_violation_link;
            $incident_details .=          $evidences_link;
            $incident_details .=          $source_name;
            $incident_details .=          $annex_details;
            $incident_details .=     '</div>';
            $incident_details .= '</div>';

            // Expected course of Action
            $expected_action .=    '<div class="row p-3">';
            $expected_action .=        '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Expected Course of Action</span>';
            $expected_action .=        '<div class="col">';
            $expected_action .=            '<hr style="margin-top: 0.8rem !important;">';
            $expected_action .=        '</div>';
            $expected_action .=    '</div>';
            $expected_action .= '<div class="col-12">';
            $expected_action .=     '<div class="col-12 pl-5 pr-5">';
            $expected_action .=         '<div class="col-md-12 ahr_quote pt-3 pb-1 text-center" style="background: #c6fff6!important;">';
            $expected_action .=             '<blockquote>';
            $expected_action .=                 '<i class="fa fa-quote-left mr-1"></i>';
            $expected_action .=                 '<span class="mb-0">' . ucfirst($raw_ir_details['ir_details']->expectedAction) . '</span>';
            $expected_action .=                 '<i class="fa fa-quote-right ml-1"></i>';
            $expected_action .=             '</blockquote>';
            $expected_action .=         '</div>';
            $expected_action .=     '</div>';
            $expected_action .= '</div>';


            if (count($raw_ir_details['suggested_disc_action']) > 0) {
                if ((int)$raw_ir_details['suggested_disc_action']->periodMonth > 1) {
                    $month_word .= "s";
                }
                $cure_details = "";
                $recommendation_details = "";
                var_dump($raw_ir_details);
                if ($raw_ir_details['suggested_disc_action']->label != 't') {
                    $cure_details  .=         '<div class="col-12 col-md-4 font-weight-bold">Period:</div>';
                    $cure_details  .=         '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">' .  $raw_ir_details['suggested_disc_action']->periodMonth . ' ' .  $month_word . '</div>';
                }
                if ($with_recommendation) {
                    if ($raw_ir_details['recommendation_stat']) {
                        $recommendation_details  .=         '<div class="col-12 col-md-4 font-weight-bold">Recommendation:</div>';
                        $recommendation_details  .=         '<div class="col-12 col-md-8 pl-5 pl-md-0 viewIrDetails" style="font-size: 13px; font-weight: 400" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '" id="recommendationDetailsView">Click to View Details</div>';
                    }
                }
                $suggested_disc_action .= '<div class="row p-3">';
                $suggested_disc_action .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Suggested Disciplinary Action</span>';
                $suggested_disc_action .= '<div class="col">';
                $suggested_disc_action .= '<hr style="margin-top: 0.8rem !important;">';
                $suggested_disc_action .= '</div>';
                $suggested_disc_action .= '</div>';
                $suggested_disc_action .= '<div class="col-12">';
                $suggested_disc_action .=     '<div class="row pl-5 pr-5">';
                $suggested_disc_action .=         '<div class="col-12 col-md-4 font-weight-bold">Level:</div>';
                $suggested_disc_action .=         '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">' .  $this->ordinal($raw_ir_details['suggested_disc_action']->level) . ' Level</div>';
                $suggested_disc_action .=         '<div class="col-12 col-md-4 font-weight-bold">Disciplinary Action:</div>';
                $suggested_disc_action .=         '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 17px; font-weight: 500">' .  $raw_ir_details['suggested_disc_action']->action . '</div>';
                $suggested_disc_action .=  $cure_details;
                $suggested_disc_action .=  $recommendation_details;
                $suggested_disc_action .=     '</div>';
                $suggested_disc_action .= '</div>';
            }
            // Witnesses
            if (($with_witnesses == 1) && (count($raw_ir_details['witnesses']) > 0)) {
                $witness_table_body = "";
                foreach ($raw_ir_details['witnesses'] as  $ir_witness_row) {
                    //  $liability_date = new DateTime( $ir_history_row->datedLiabilityStat);
                    $witness_table_body .= '<tr>';
                    $witness_table_body .=    '<td class="text-center">' . ucfirst($ir_witness_row->fname) . ' ' . ucfirst($ir_witness_row->lname) . '</td>';
                    $witness_table_body .=    '<td class="text-center">' . ucfirst($ir_witness_row->description) . '</td>';
                    $witness_table_body .= '</tr>';
                }
                $witness_details .= '<div class="row p-3">';
                $witness_details .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Witnesses</span>';
                $witness_details .= '<div class="col">';
                $witness_details .= '<hr style="margin-top: 0.8rem !important;">';
                $witness_details .= '</div>';
                $witness_details .= '</div>';
                $witness_details .= '<div class="col-12">';
                $witness_details .= '<div class="row pl-5 pr-5">';
                $witness_details .= '<div class="col-12 mt-2">';
                $witness_details .= '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">';
                $witness_details .= '<thead style="background: #555;color: white;">';
                $witness_details .= '<tr>';
                $witness_details .= '<th class="text-center">Witness Name</th>';
                $witness_details .= '<th class="text-center">Confirmation</th>';
                $witness_details .= '</tr>';
                $witness_details .= '</thead>';
                $witness_details .= '<tbody>';
                $witness_details .=  $witness_table_body;
                $witness_details .= '</tbody>';
                $witness_details .= '</table>';
                $witness_details .= '</div>';
                $witness_details .= '</div>';
                $witness_details .= '</div>';
            }

            if ($with_subject_explanation == 1) {
                $with_explanation_sub = 0;
                if (trim($raw_ir_details['ir_details']->subjectExplanation) != '') {
                    $with_explanation_sub = 1;
                    $subject_explanation .= '<div class="col-12">';
                    $subject_explanation .=     '<div class="col-12 pl-5 pr-5">';
                    $subject_explanation .=         '<div class="col-md-12 ahr_quote pt-3 pb-1 text-center" style="background: #c6fff6!important;">';
                    $subject_explanation .=             '<blockquote>';
                    $subject_explanation .=                 '<i class="fa fa-quote-left mr-1"></i>';
                    $subject_explanation .=             '<span class="mb-0">' .  $raw_ir_details["ir_details"]->subjectExplanation . '</span>';
                    $subject_explanation .=             '<i class="fa fa-quote-right ml-1"></i>';
                    $subject_explanation .=             '</blockquote>';
                    $subject_explanation .=         '</div>';
                    $subject_explanation .=     '</div>';
                    $subject_explanation .= '</div>';
                }
                if ((int) $raw_ir_details['explain_attachment']->ir_attachment > 0) {
                    $with_explanation_sub = 1;
                    $subject_explanation .= '<div class="col-12">';
                    $subject_explanation .=     '<div class="row pl-5 pr-5">';
                    $subject_explanation .=         '<div class="col-12 mt-3">';
                    $subject_explanation .=             '<span class="font-weight-bold mr-2">Uploaded Explanation:</span>';
                    $subject_explanation .=             '<span class="viewIrDetails" id="uploadedExplanation" data-irid="' . $raw_ir_details['ir_details']->incidentReport_ID . '">View Attached Explanation</span>'; // UPLOADED EXPLANATION HERE
                    $subject_explanation .=         '</div> ';
                    $subject_explanation .=     '</div>';
                    $subject_explanation .= '</div>';
                }
                if ($with_explanation_sub) {
                    $subject_explanation_details .=     '<div class="row p-3">';
                    $subject_explanation_details .=         "<span class='ml-3' style='font-size: 14px;font-weight: 500;color: #5d4747;'><i class='la la-tag' style='font-size: 1.1rem;'></i>" . ucwords($raw_ir_details["ir_details"]->subjectFname) . "'s Explanation</span>";
                    $subject_explanation_details .=         '<div class="col">';
                    $subject_explanation_details .=             '<hr style="margin-top: 0.8rem !important;">';
                    $subject_explanation_details .=         '</div>';
                    $subject_explanation_details .=     '</div>';
                    $subject_explanation_details .=     $subject_explanation;
                }
            }

            if ($raw_ir_details['ir_details']->incidentReportStages_ID == 3) {
                if ((int) $raw_ir_details['ir_details']->liabilityStat == 17) {
                    $sanction = $raw_ir_details['ir_details']->finalAction;
                    $final_date_label = "Cure Date:";
                    $final_date = date_format(date_create($raw_ir_details['ir_details']->prescriptionEnd), "M j, Y");
                    if ($raw_ir_details['ir_details']->label == 's') {
                        $suspension_days = $this->get_suspension_days_value($raw_ir_details['ir_details']->incidentReport_ID);
                        if (count($suspension_days) > 0) {
                            $sus_day = 'Day';
                            if ((int) $suspension_days->days > 1) {
                                $sus_day .= "s";
                            }
                            $sanction = $suspension_days->days . " " . $sus_day . " Suspension";
                        }
                    } else if ($raw_ir_details['ir_details']->label == 't') {
                        $termination_date = $this->get_termination_date($ir_id);
                        if (count($termination_date) > 0) {
                            $final_date_label = "Effective on:";
                            $final_date = date_format(date_create($termination_date->date), "M j, Y");
                            $sanction = " Termination";
                        }
                    }
                    $ir_finalization .= '<div class="row p-3">';
                    $ir_finalization .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR Finalization</span>';
                    $ir_finalization .= '<div class="col">';
                    $ir_finalization .= '<hr style="margin-top: 0.8rem !important;">';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '<div class="col-12">';
                    $ir_finalization .= '<div class="row ml-5 mr-5 py-3" style=" background: #ffe0e0; border-left: #ff9090 0.6rem solid;">';
                    $ir_finalization .= '<div class="col-12 col-md-4 font-weight-bold" style="font-size: 1.3rem;">Liability Status:</div>';
                    $ir_finalization .= '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 21px;font-weight: 400;">' . ucwords($raw_ir_details['ir_details']->liabilityStatDesc) . '</div>';
                    $ir_finalization .= '<div class="col-12 col-md-4 font-weight-bold" style="font-size: 1.3rem;">Sanction:</div>';
                    $ir_finalization .= '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 21px;font-weight: 400;">' . ucwords($sanction) . '</div>';
                    $ir_finalization .= '<div class="col-12 col-md-4 font-weight-bold" style="font-size: 1rem;">' . $final_date_label . '</div>';
                    $ir_finalization .= '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 15px;font-weight: 400;">' . $final_date . '</div>';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '</div>';
                } else if ((int) $raw_ir_details['ir_details']->liabilityStat == 18) {
                    $ir_finalization .= '<div class="row p-3">';
                    $ir_finalization .= '<span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR Finalization</span>';
                    $ir_finalization .= '<div class="col">';
                    $ir_finalization .= '<hr style="margin-top: 0.8rem !important;">';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '<div class="col-12">';
                    $ir_finalization .= '<div class="row ml-5 mr-5 py-3" style="background: #e0ffed;border-left: #68ecc6 0.6rem solid;">';
                    $ir_finalization .= '<div class="col-12 col-md-5 font-weight-bold" style="font-size: 1.7rem;">Liability Status:</div>';
                    $ir_finalization .= '<div class="col-12 col-md-7 pl-5 pl-md-0" style="font-size: 26px;font-weight: 400;">Non-Liable</div>';
                    $ir_finalization .= '</div>';
                    $ir_finalization .= '</div>';
                }
            }

            $ir_details_layout =  $subject_details .  $incident_details .  $expected_action .  $ir_history_details .  $suggested_disc_action .  $witness_details .  $subject_explanation_details . $ir_finalization;
            $data['ir_details_layout'] =  $ir_details_layout;
        } else {
            $data['error'] = 1;
            $data['error_details'] = "";
        }
        return $data;
        // echo json_encode( $data);
    }

    public function get_ir_details()
    {
        $ir_details = $this->get_layouted_ir_details($this->input->post('ir_id'), 1, 1, 1, 1, 1, 1);
        echo json_encode($ir_details);
    }

    public function access_ir_details()
    {
        $ir_details = $this->get_layouted_ir_details(83, 1, 1, 1, 1);
        echo json_encode($ir_details);
    }

    private function recheck_ir_filing($subject_emp_id, $offense_ID)
    {
        $ongoing_prescriptive_stat_ir = $this->get_existing_ir($subject_emp_id, 2, 2, $offense_ID);
        // var_dump($ongoing_prescriptive_stat_ir);
        if (count($ongoing_prescriptive_stat_ir) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function set_deadline_to_explain()
    {
        $rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
        $days_recommend = (int) $rec_deadline['record']->deadlineToRecommend;
    }

    public function get_dtr_vio_view()
    {
        $dtr_vio_type_id = $this->input->post('dtrViolationTypeId');
        // $dtr_vio_type_id = 5;
        $unit = '';
        $intro = "";
        $refresh = "";
        $dtr_vio_settings = $this->get_dtr_vio_settings_main($dtr_vio_type_id);
        $dtr_vio_type = $this->qry_specific_dtr_vio_type($dtr_vio_type_id);
        $offense_details = "";
        $dtrVioRule = "";
        // var_dump($dtr_vio_settings);
        if(count($dtr_vio_settings) > 0){
            if ($dtr_vio_type_id != 5) {
                if (($dtr_vio_settings['record']->offense_ID != '') && ($dtr_vio_settings['record']->offense_ID != NULL) && ($dtr_vio_settings['record']->category_ID != '') && ($dtr_vio_settings['record']->category_ID != NULL)) {
                    $offense_details = " with a Nature of Offense <span style='font-weight: 500;'>\"" . $dtr_vio_settings['offense']->offenseType . "\"</span> and a <span style='font-weight: 500;'>Category " . ucwords($dtr_vio_settings['record']->category) . "</span> offense of <span style='font-weight: 500;'>\"" . $dtr_vio_settings['offense']->offense . "\"</span>";
                }
                $intro = '<p class="" style="font-size: 1.1rem;text-indent: 3rem;">An <span style="font-weight: 500;">Incident Report</span> (IR)' . $offense_details . ' will be automatically prepared by the system if one has committed <span style="font-weight: 500;">' . ucwords($dtr_vio_type->description) . '</span> violation of the following circumstances:</p>';
            } else {
                if (($dtr_vio_settings['record']->offense_ID != '') && ($dtr_vio_settings['record']->offense_ID != NULL) && ($dtr_vio_settings['record']->category_ID != '') && ($dtr_vio_settings['record']->category_ID != NULL)) {
                    $offense_details = " with a Nature of Offense <span style='font-weight: 500;'>\"" . $dtr_vio_settings['offense']->offenseType . "\"</span> and a <span style='font-weight: 500;'>Category " . ucwords($dtr_vio_settings['record']->category) . "</span> offense of <span style='font-weight: 500;'>\"" . $dtr_vio_settings['offense']->offense . "\"</span>";
                }
                $intro = '<p class="" style="font-size: 1.1rem;text-indent: 3rem;">An <span>Incident Report</span> (IR)' . $offense_details . ' will be automatically prepared by the system if one has committed <span style="font-weight: 500;">' . ucwords($dtr_vio_type->description) . '</span> violation.</p>';
            }

            $list = '<ul style="font-size: 1.1rem;">';
            if ((int) $dtr_vio_settings['record']->oneTimeStat) {
                $unit = 'minute';
                if ((int) $dtr_vio_settings['record']->oneTimeVal > 1) {
                    $unit .= "s";
                }
                $list .= '<li>One-time occurence of ' . $dtr_vio_settings['record']->oneTimeVal . ' ' . $unit . ' or up </li>';
            }
            if ((int) $dtr_vio_settings['record']->consecutiveStat) {
                $unit = 'time';
                if ((int) $dtr_vio_settings['record']->consecutiveVal > 1) {
                    $unit .= "s";
                }
                $list .=  '<li>' . $dtr_vio_settings['record']->consecutiveVal . ' ' . $unit . ' consecutive occurence regardless on the number of minutes</li>';
            }
            if ((int) $dtr_vio_settings['record']->nonConsecutiveStat) {
                $unit = 'minute';
                $rule = "Accumulated";
                if ($dtr_vio_settings['record']->dtrViolationType_ID == 6 || $dtr_vio_settings['record']->dtrViolationType_ID == 7) {
                    $unit = 'time';
                    if ((int) $dtr_vio_settings['record']->consecutiveVal > 1) {
                        $unit .= "s";
                    }
                    $list .= '<li>' . $dtr_vio_settings['record']->nonConsecutiveVal . ' ' . $unit . ' occurrences regardless if consecutive or not</li>';
                    // $list .= '<li>'. $dtr_vio_settings['record']->nonConsecutiveVal.'Accumulated 15 minutes</li>';
                } else if ($dtr_vio_settings['record']->dtrViolationType_ID == 3) {
                    $list .= $dtr_vio_settings['record']->nonConsecutiveVal . ' ' . $unit . ' and up</li>';
                } else {
                    if ((int) $dtr_vio_settings['record']->nonConsecutiveVal > 1) {
                        $unit .= "s";
                    }
                    $list .= '<li>Accumulated ' . $dtr_vio_settings['record']->nonConsecutiveVal . ' ' . $unit . ' occurrences regardless if consecutive or not</li>';
                }
            }
            $list .= '</ul>';

            if ((int) $dtr_vio_settings['record']->durationType == 1) {
                $periodUnit = $dtr_vio_settings['record']->durationUnit;
                if ((int) $dtr_vio_settings['record']->durationVal > 1) {
                    $periodUnit = $dtr_vio_settings['record']->durationVal . ' ' . $periodUnit . 's';
                } else {
                    $periodUnit .= 'ly';
                }
                $refresh = '<p class="" style="font-size: 1.1rem;">Counting of instance/occurences are based on a ' . $periodUnit . ' basis, and shall be refreshed on the following ' . $dtr_vio_settings['record']->durationUnit . '</p>';
            } else if ((int) $dtr_vio_settings['record']->durationType == 2) {
                $periodUnit = 'day';
                if ((int) $dtr_vio_settings['record']->durationVal > 1) {
                    $periodUnit = $dtr_vio_settings['record']->durationVal . ' ' . $periodUnit . 's';
                } else {
                    $periodUnit .= $dtr_vio_settings['record']->durationVal . ' ' . $periodUnit;
                }
                $refresh = '<p class="" style="font-size: 1.1rem;">Counting of instance/occurences are based on a ' . $periodUnit . ' basis starting from the date of occurence, and shall be refreshed after the ' . $this->ordinal($dtr_vio_settings['record']->durationVal) . ' day.</p>';
            } else {
                $refresh =  '<p class="" style="font-size: 1.1rem;">Counting of instance/occurences are based on the prescriptive period stated the Code of Discipine.</p>';
            }
            /*         echo $intro;
        echo $list;
        echo $refresh; */
            if (($dtr_vio_settings['record']->offense_ID != '') && ($dtr_vio_settings['record']->offense_ID != NULL) && ($dtr_vio_settings['record']->category_ID != '') && ($dtr_vio_settings['record']->category_ID != NULL)) {
                $dtrVioRule = $intro . "" . $list . "" . $refresh;
            }else{
                $dtrVioRule = '<div class="col-md-12 p-0" style="display: flex;align-items:center;">';
                $dtrVioRule .=     '<div class="col-md-12 p-0 text-center">';
                $dtrVioRule .=         '<div class="m-demo-icon__preview">';
                $dtrVioRule .=             '<i style="font-size: 8rem;" class="flaticon-notes"></i>';
                $dtrVioRule .=         '</div>';
                $dtrVioRule .=         '<span class="m-demo-icon__class" style="font-size: 2rem;">No Rules Set</span>';
                $dtrVioRule .=     '</div>';
                $dtrVioRule .= '</div>';
            }
        }else{
            $dtrVioRule .= '<div class="col-md-12 p-0" style="display: flex;align-items:center;">';
            $dtrVioRule .=     '<div class="col-md-12 p-0 text-center">';
            $dtrVioRule .=         '<div class="m-demo-icon__preview">';
            $dtrVioRule .=             '<i style="font-size: 8rem;" class="flaticon-notes"></i>';
            $dtrVioRule .=         '</div>';
            $dtrVioRule .=         '<span class="m-demo-icon__class" style="font-size: 2rem;">No Rules Set</span>';
            $dtrVioRule .=     '</div>';
            $dtrVioRule .= '</div>';
        }
        echo json_encode($dtrVioRule);
    }

    private function get_auto_ir_qulified($ir_id)
    {
        $fields = "autoIrRelatedDetails_ID, qualifiedUserDtrViolation_ID, directSupEmpId, supervisorsNote";
        $where = "incidentReport_ID =  $ir_id";
        return  $this->general_model->fetch_specific_val($fields,  $where,  "tbl_dms_auto_ir_related_details");
    }

    protected function get_auto_ir_qulified_via_qualified($qualified_id)
    {
        $fields = "autoIrRelatedDetails_ID, qualifiedUserDtrViolation_ID, directSupEmpId, supervisorsNote";
        $where = "qualifiedUserDtrViolation_ID =  $qualified_id";
        return  $this->general_model->fetch_specific_val($fields,  $where,  "tbl_dms_auto_ir_related_details");
    }

    protected function get_forgot_logout_reason($user_dtr_vio_id)
    {
        $fields = "forgotLogoutExplanation_ID, explanation, userDtrViolation_ID, dateCreated";
        $where = "userDtrViolation_ID =  $user_dtr_vio_id";
        return  $this->general_model->fetch_specific_val($fields,  $where,  "tbl_dms_forgot_logout_explanation");
    }

    protected function get_termination_date($ir_id)
    {
        $fields = "days.suspensionTerminationDays_ID, days.incidentReport_ID, days.directSupEmp_ID, days.status_ID, dates.date";
        $where = "dates.suspensionTerminationDays_ID = days.suspensionTerminationDays_ID AND days.incidentReport_ID = $ir_id";
        $table = "tbl_dms_suspension_termination_days days, tbl_dms_suspension_dates dates";
        return  $this->general_model->fetch_specific_val($fields,  $where, $table);
    }

    protected function get_suspension_days_value($ir_id)
    {
        $fields = "suspensionTerminationDays_ID, incidentReport_ID, directSupEmp_ID, days, status_ID";
        $where = "incidentReport_ID =  $ir_id";
        return  $this->general_model->fetch_specific_val($fields,  $where,  "tbl_dms_suspension_termination_days");
    }

    protected function get_client_confirmation_value($ir_id)
    {
        $fields = "clientConfirmation_id, directSupEmp_ID, confirmationStat, disciplinaryActionCategory_ID, attachment, suggestion, notes, description";
        $where = "status_ID = confirmationStat AND incidentReport_ID =  $ir_id";
        $table = "tbl_dms_client_confirmation, tbl_status";
        return  $this->general_model->fetch_specific_val($fields,  $where, $table);
    }

    protected function get_ir_annexes($ir_id)
    {
        $fields = "incidentReportAnnex_ID, attachedIncidentReport_ID";
        $where = " incidentReport_ID =  $ir_id";
        return  $this->general_model->fetch_specific_vals($fields,  $where, "tbl_dms_incident_report_annex");
    }

    protected function get_annexes_details($ir_ids)
    {
        $fields = "ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, ir.liabilityStat, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, ir.incidentReportStages_ID, ir.subjectEmp_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, sourceApp.pic sourcePic";
        $where = "subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.incidentReport_ID IN ($ir_ids)";
        $table = "tbl_dms_incident_report ir, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp";
        return  $this->general_model->fetch_specific_vals($fields,  $where,  $table);
    }

    protected function get_first_on_hold_qualified_dtr_violation($emp_id, $offense_id)
    {
        $fields = "qualified.qualifiedUserDtrViolation_ID, qualified.userDtrViolation_ID, settings.offense_ID, qualified.emp_id, qualified.status_ID, userDtrVio.sched_id";
        $where = "settings.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.emp_id = $emp_id AND settings.offense_ID = $offense_id AND qualified.status_ID = 21";
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation userDtrVio, tbl_dms_dtr_violation_type_settings settings";
        return  $this->general_model->fetch_specific_val($fields,  $where,  $table, 'qualified.qualifiedUserDtrViolation_ID ASC');
    }

    protected function get_qualified_with_offense_details($qualified_dtr_vio)
    {
        $fields = "qualified.qualifiedUserDtrViolation_ID, qualified.userDtrViolation_ID, settings.offense_ID, qualified.emp_id, qualified.status_ID";
        $where = "settings.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.qualifiedUserDtrViolation_ID = $qualified_dtr_vio";
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation userDtrVio, tbl_dms_dtr_violation_type_settings settings";
        return  $this->general_model->fetch_specific_val($fields,  $where,  $table);
    }

    protected function get_user_dtr_violation_related_details($user_dtr_vio_id)
    {
        $fields = "relatedUserDtrViolation_ID";
        $where = "userDtrViolation_ID = $user_dtr_vio_id";
        return  $this->general_model->fetch_specific_vals($fields,  $where,  "tbl_dms_user_dtr_violation_related_details");
    }

    public function access_on_hold_during_dismmissal($qualified_dtr_vio_id)
    {
        // during dismissal
        $qualified_details = $this->get_qualified_with_offense_details($qualified_dtr_vio_id);
        return $this->check_on_hold_qualified_dtr_violation($qualified_details->emp_id, $qualified_details->offense_ID);
    }

    public function access_on_hold__test()
    {
        // during dismissal
        $qualified_details = $this->get_qualified_with_offense_details($qualified_dtr_vio_id);
        return $this->check_on_hold_qualified_dtr_violation($qualified_details->emp_id, $qualified_details->offense_ID);
    }

    public function reset_related_details_if_non_liable($ir_id)
    {
        $ir = $this->get_ir($ir_id);
        if ($ir->incidentReportFilingType_ID == 2) {
            $auto_ir_qualified = $this->get_auto_ir_qulified($ir_id);
            if (count($auto_ir_qualified) > 0) {
                $qualified_details = $this->get_qualified_with_offense_details($auto_ir_qualified->qualifiedUserDtrViolation_ID);
                $this->reset_user_dtr_related_statuses($qualified_details->userDtrViolation_ID);
            }
        }
    }

    public function access_on_hold_during_ir_complete($ir_id = 1303)
    {
        // during dismissal
        $ir = $this->get_ir($ir_id);
        return $this->check_on_hold_qualified_dtr_violation($ir->subjectEmp_ID, $ir->offense_ID);
    }

    public function access_on_hold_during_ir_complete_test($ir_id = 405)
    {
        // during dismissal
        $ir = $this->get_ir($ir_id);
        return $this->check_on_hold_qualified_dtr_violation_test($ir->subjectEmp_ID, $ir->offense_ID);
    }

    public function main_access_on_hold_during_ir_complete_test(){
        $on_hold_record = $this->access_on_hold_during_ir_complete_test(320);
        var_dump($on_hold_record);
    }

    public function check_on_hold_qualified_dtr_violation($emp_details, $offense_id)
    {
        $qualify['valid'] = 0;
        $qualify['error'] = 0;
        $qualify['details'] = "";
        $qualify['remove_qualified_dtr_violation'] = 0;
        $qualifiedStat = 0;
        $remove_qualified = 0;
        // $qualified_details = $this->get_qualified_with_offense_details($qualified_dtr_vio_id);
        // var_dump($qualified_details);
        $first_on_hold = $this->get_first_on_hold_qualified_dtr_violation($emp_details, $offense_id);
        // var_dump($first_on_hold);
        if (count($first_on_hold) > 0) {
            // var_dump($first_on_hold);
            $ongoing_prescriptive_stat_ir = $this->get_existing_ir($first_on_hold->emp_id, 2, 2, $first_on_hold->offense_ID);
            // var_dump($ongoing_prescriptive_stat_ir);
            if (count($ongoing_prescriptive_stat_ir) < 1) { // if no pending IR of the same offense exist
                // remove all related details of the qualified dtr violation
                $this->remove_qualified_related_details($first_on_hold->userDtrViolation_ID);
                $user_dtr_violation = $this->get_user_dtr_violation($first_on_hold->userDtrViolation_ID);
                $subject_user_emp_details = $this->get_emp_details_via_user_id($user_dtr_violation->user_ID);
                $violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID); //Get violation rules and prescription (violation settings)
                $same_sched_offense = $this->get_same_offense_of_same_sched($violation_rules->offense_ID, $user_dtr_violation->sched_ID); // check if ir of the same offense of same sched id exist
                if(count($same_sched_offense) < 1){
                    $violation_details = $this->get_violation_related_details($user_dtr_violation);
                    if ($violation_details['valid']) //check if incident is valid
                    {
                        $qualify['valid'] = 1;
                        $refreshment = $this->check_refreshment_date($user_dtr_violation->dtrViolationType_ID, $subject_user_emp_details->emp_id, $user_dtr_violation->sched_date, $first_on_hold->userDtrViolation_ID);
                        // var_dump($refreshment);
                        if (count($refreshment) > 0) {
                            $qualified_stat['status_ID'] = 2;
                            $qualified_stat['refreshment_date'] = $refreshment->refreshment_date;
                            $this->update_qualified_dtr_vio($first_on_hold->qualifiedUserDtrViolation_ID, $qualified_stat);
                            $qualify['on_hold_qualified_user_dtr_violation'] = $first_on_hold->qualifiedUserDtrViolation_ID;
                            //auto_notify_ir_direct_sup 
                            $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $first_on_hold->qualifiedUserDtrViolation_ID, $violation_rules);
                            $qualify['qualify_status'] = "An IR of the same offense that has a currently ongoing prescriptive period exist";
                        } else {
                            $refreshment_value = $this->identify_refreshment($violation_rules, $user_dtr_violation);
                            $auto_ir_notify_rules = $this->check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules);
                                    // var_dump($auto_ir_notify_rules);
                            if ($auto_ir_notify_rules['valid']) {
                                $qualifiedStat = 1;
                                $qualify['qualify_status'] = "The User DTR Violation fits on a specific violation rule";
                            } else {
                                if ($user_dtr_violation->dtrViolationType_ID == 5) {
                                    $qualifiedStat = 1;
                                    $qualify['qualify_status'] = "The User DTR violation detected is Absent";
                                } else {
                                    $qualify['error_details'] = "Does not qualify Rules";
                                    $remove_qualified = 1;
                                }
                            }
                            if ($qualifiedStat) {
                                // set on hold qualified status to pending
                                $qualify['valid'] = 1;
                                $qualified_stat['status_ID'] = 2;
                                $qualified_stat['occurrenceRuleNum'] = $auto_ir_notify_rules['occurence_rule_num'];
                                $qualified_stat['occurrenceValue'] = $auto_ir_notify_rules['occurence_value'];
                                $qualified_stat['refreshment_date'] = $refreshment_value["cure_date"];
                                $this->update_qualified_dtr_vio($first_on_hold->qualifiedUserDtrViolation_ID, $qualified_stat);
                                $qualify['on_hold_qualified_user_dtr_violation'] = $first_on_hold->qualifiedUserDtrViolation_ID;
                                //auto_notify_ir_direct_sup
                                $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $first_on_hold->qualifiedUserDtrViolation_ID, $violation_rules);
                                $user_data['status_ID'] = 3;
                                $qualify['user_dtr_violation_stat'] = $this->update_user_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data);
                            }
                        }
                        // $ir_exist = $this->check_if_previous_ir_exist($user_dtr_violation, $subject_user_emp_details, $violation_rules); // Check if current IR of the same violation exist
                        // if ($ir_exist['exist']) // if a liable IR of current status and the same offense exist
                        // {
                        //     // set on hold qualified status to pending
                        //     $qualified_stat['status_ID'] = 2;
                        //     $this->update_qualified_dtr_vio($first_on_hold->qualifiedUserDtrViolation_ID, $qualified_stat);
                        //     $qualify['on_hold_qualified_user_dtr_violation'] = $first_on_hold->qualifiedUserDtrViolation_ID;
                        //     //auto_notify_ir_direct_sup 
                        //     $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $first_on_hold->qualifiedUserDtrViolation_ID, $violation_rules);
                        //     $qualify['qualify_status'] = "An IR of the same offense that has a currently ongoing prescriptive period exist";
                        // } else {

                        // }
                    } else {
                        // remove qualified user dtr violation
                        $remove_qualified = 1;
                        $qualify['error_details'] = "Violation is not true";
                        $qualify['related_details'] = $violation_details['error_details'];
                        // set USER DTR VIOLATION to INVALID
                        $user_data_vio['status_ID'] = 23;
                        $qualify['user_dtr_update_to_invalid'] = $this->update_user_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data_vio);
                    }
                    // REMOVE on hold qualified dtr violation
                    if ($remove_qualified) {
                        $qualify['remove_qualified_dtr_violation'] = 1;
                        $this->remove_qualified_user_dtr_violation($first_on_hold->qualifiedUserDtrViolation_ID);
                    }
                }else{
                    // REMOVE on hold qualified dtr violation to avoid ir jeopardy
                    $qualify['remove_qualified_dtr_violation'] = 1;
                    $this->remove_qualified_user_dtr_violation($first_on_hold->qualifiedUserDtrViolation_ID);
                }
            }
        } else {
            $qualify['details'] = "no on hold qualified dtr violations of the same offense";
        }
        return $qualify;
    }

    public function check_on_hold_qualified_dtr_violation_test($emp_details = 1751, $offense_id = 5)
    {
        $qualify['valid'] = 0;
        $qualify['error'] = 0;
        $qualify['details'] = "";
        $qualify['remove_qualified_dtr_violation'] = 0;
        $qualifiedStat = 0;
        $remove_qualified = 0;
        // $qualified_details = $this->get_qualified_with_offense_details($qualified_dtr_vio_id);
        // var_dump($qualified_details);
        $first_on_hold = $this->get_first_on_hold_qualified_dtr_violation($emp_details, $offense_id);
        // var_dump($first_on_hold);
        if (count($first_on_hold) > 0) {
            var_dump($first_on_hold);
            $ongoing_prescriptive_stat_ir = $this->get_existing_ir($first_on_hold->emp_id, 2, 2, $first_on_hold->offense_ID);
            var_dump($ongoing_prescriptive_stat_ir);
            if (count($ongoing_prescriptive_stat_ir) < 1) { // if no pending IR of the same offense exist
        //         // remove all related details of the qualified dtr violation
        //         $this->remove_qualified_related_details($first_on_hold->userDtrViolation_ID);
                $user_dtr_violation = $this->get_user_dtr_violation($first_on_hold->userDtrViolation_ID);
                var_dump($user_dtr_violation);
                $subject_user_emp_details = $this->get_emp_details_via_user_id($user_dtr_violation->user_ID);
                var_dump($subject_user_emp_details);
                $violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID); //Get violation rules and prescription (violation settings)
                $same_sched_offense = $this->get_same_offense_of_same_sched($violation_rules->offense_ID, $user_dtr_violation->sched_ID); // check if ir of the same offense of same sched id exist
                var_dump($same_sched_offense);
        //         if(count($same_sched_offense) < 1){
        //             $violation_details = $this->get_violation_related_details($user_dtr_violation);
        //             if ($violation_details['valid']) //check if incident is valid
        //             {
        //                 $qualify['valid'] = 1;
        //                 $refreshment = $this->check_refreshment_date($user_dtr_violation->dtrViolationType_ID, $subject_user_emp_details->emp_id, $user_dtr_violation->sched_date);
        //                 if (count($refreshment) > 0) {
        //                     $qualified_stat['status_ID'] = 2;
        //                     $qualified_stat['refreshment_date'] = $refreshment->refreshment_date;
        //                     $this->update_qualified_dtr_vio($first_on_hold->qualifiedUserDtrViolation_ID, $qualified_stat);
        //                     $qualify['on_hold_qualified_user_dtr_violation'] = $first_on_hold->qualifiedUserDtrViolation_ID;
        //                     //auto_notify_ir_direct_sup 
        //                     $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $first_on_hold->qualifiedUserDtrViolation_ID, $violation_rules);
        //                     $qualify['qualify_status'] = "An IR of the same offense that has a currently ongoing prescriptive period exist";
        //                 } else {
        //                     $refreshment_value = $this->identify_refreshment($violation_rules, $user_dtr_violation);
        //                     $auto_ir_notify_rules = $this->check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules);
        //                     //         var_dump($auto_ir_notify_rules);
        //                     if ($auto_ir_notify_rules['valid']) {
        //                         $qualifiedStat = 1;
        //                         $qualify['qualify_status'] = "The User DTR Violation fits on a specific violation rule";
        //                     } else {
        //                         if ($user_dtr_violation->dtrViolationType_ID == 5) {
        //                             $qualifiedStat = 1;
        //                             $qualify['qualify_status'] = "The User DTR violation detected is Absent";
        //                         } else {
        //                             $qualify['error_details'] = "Does not qualify Rules";
        //                             $remove_qualified = 1;
        //                         }
        //                     }
        //                     if ($qualifiedStat) {
        //                         // set on hold qualified status to pending
        //                         $qualify['valid'] = 1;
        //                         $qualified_stat['status_ID'] = 2;
        //                         $qualified_stat['occurrenceRuleNum'] = $auto_ir_notify_rules['occurence_rule_num'];
        //                         $qualified_stat['occurrenceValue'] = $auto_ir_notify_rules['occurence_value'];
        //                         $qualified_stat['refreshment_date'] = $refreshment_value["cure_date"];
        //                         $this->update_qualified_dtr_vio($first_on_hold->qualifiedUserDtrViolation_ID, $qualified_stat);
        //                         $qualify['on_hold_qualified_user_dtr_violation'] = $first_on_hold->qualifiedUserDtrViolation_ID;
        //                         //auto_notify_ir_direct_sup
        //                         $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $first_on_hold->qualifiedUserDtrViolation_ID, $violation_rules);
        //                         $user_data['status_ID'] = 3;
        //                         $qualify['user_dtr_violation_stat'] = $this->update_user_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data);
        //                     }
        //                 }
        //                 // $ir_exist = $this->check_if_previous_ir_exist($user_dtr_violation, $subject_user_emp_details, $violation_rules); // Check if current IR of the same violation exist
        //                 // if ($ir_exist['exist']) // if a liable IR of current status and the same offense exist
        //                 // {
        //                 //     // set on hold qualified status to pending
        //                 //     $qualified_stat['status_ID'] = 2;
        //                 //     $this->update_qualified_dtr_vio($first_on_hold->qualifiedUserDtrViolation_ID, $qualified_stat);
        //                 //     $qualify['on_hold_qualified_user_dtr_violation'] = $first_on_hold->qualifiedUserDtrViolation_ID;
        //                 //     //auto_notify_ir_direct_sup 
        //                 //     $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $first_on_hold->qualifiedUserDtrViolation_ID, $violation_rules);
        //                 //     $qualify['qualify_status'] = "An IR of the same offense that has a currently ongoing prescriptive period exist";
        //                 // } else {

        //                 // }
        //             } else {
        //                 // remove qualified user dtr violation
        //                 $remove_qualified = 1;
        //                 $qualify['error_details'] = "Violation is not true";
        //                 $qualify['related_details'] = $violation_details['error_details'];
        //                 // set USER DTR VIOLATION to INVALID
        //                 $user_data_vio['status_ID'] = 23;
        //                 $qualify['user_dtr_update_to_invalid'] = $this->update_user_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data_vio);
        //             }
        //             // REMOVE on hold qualified dtr violation
        //             if ($remove_qualified) {
        //                 $qualify['remove_qualified_dtr_violation'] = 1;
        //                 $this->remove_qualified_user_dtr_violation($first_on_hold->qualifiedUserDtrViolation_ID);
        //             }
        //         }else{
        //             // REMOVE on hold qualified dtr violation to avoid ir jeopardy
        //             $qualify['remove_qualified_dtr_violation'] = 1;
        //             $this->remove_qualified_user_dtr_violation($first_on_hold->qualifiedUserDtrViolation_ID);
        //         }
            }
        } else {
            $qualify['details'] = "no on hold qualified dtr violations of the same offense";
        }
        // return $qualify;
    }

    protected function get_current_ir_via_priscriptive_id($prescriptive_id, $emp_id, $offense_id)
    {
        $fields = "incidentReport_ID, subjectEmp_ID, sourceEmp_ID, dateTimeFiled, details, prescriptionEnd, prescriptiveStat, liabilityStat, datedLiabilityStat, subjectExplanation, occurence, offense_ID, offense, prescriptiveId, disciplinaryActionCategorySettings_ID";
        $where = "prescriptiveId = $prescriptive_id AND prescriptiveStat = 4 AND subjectEmp_ID = $emp_id AND offense_ID = $offense_id";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_dms_incident_report", "occurence ASC");
    }

    // protected function get_ir_profile($emp_id){
    //     // GET EMPLOYEE PROFILE
    //     $profile['emp_details'] = $this->get_emp_details_via_emp_id($emp_id);
    //     var_dump($profile);
    //     // 

    // }

    public function access_personal_ir_profile()
    {
        $emp_details = $this->get_emp_details_via_emp_id($this->session->userdata('emp_id'));
        echo json_encode($emp_details);
    }

    protected function check_witness_confirmation_status($witness_id)
    {
        $fields = "status_ID";
        $where = "incidentReportWitness_ID = $witness_id";
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_incident_report_witness");
    }

    protected function get_add_ir_emp(){
        $fields = "DISTINCT(ir.subjectEmp_ID) emp_id, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic";
        $where = "subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND ir.incidentReport_ID = history.incidentReport_ID";
        $table = "tbl_dms_added_ir_history history, tbl_dms_incident_report ir, tbl_applicant subApp, tbl_employee subEmp";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    public function get_add_ir_emp_list(){
        $record['exist'] = 0;
        $ir_emp = $this->get_add_ir_emp();
        if(count($ir_emp) > 0){
            $record['exist'] = 1;
            $record['record'] = $ir_emp;
        }
        echo json_encode($record);
    }


    public function get_added_ir_history_datatable()
    {
        $datatable = $this->input->post('datatable');
        $emp_id = $datatable['query']['empId'];
        // $discipline_category_id = $datatable['query']['disciplineCategoryId'];
        $emp_where = "";
        if((int) $emp_id !== 0){
            $emp_where = "AND ir.subjectEmp_ID = $emp_id";
        }
        $query['query'] = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.occurence, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID sourceEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_added_ir_history history, tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.incidentReport_ID = history.incidentReport_ID ". $emp_where;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    // END OF READ -----------------------------------------------------------------------------------

    // UPDATE -----------------------------------------------------------------------------------
    public function update_disciplinary_action()
    {
        $disciplinary_action_id =  $this->input->post('disciplinaryActionId');
        $data['action'] =  $this->input->post('action');
        $data['abbreviation'] =  $this->input->post('abbr');
        $data['periodMonth'] =  $this->input->post('period');
        $data['changedBy'] =  $this->session->userdata('emp_id');
        if ($this->input->post('isSuspension') == 'true') {
            $data['label'] = 's';
        } else {
            $data['label'] = 'w';
        }
        $where = "disciplinaryAction_ID =  $disciplinary_action_id";
        $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_disciplinary_action');
        echo json_encode($update_stat);
    }

    public function update_specific_offense_category()
    {
        $offense_category_id =  $this->input->post('specificOffenseCategoryId');
        // add offense type history
        $offense_type =  $this->get_specific_offense_category($offense_category_id);
        $this->add_offense_type_history($offense_type);
        $data['offenseType'] =  $this->input->post('offenseCategoryVal');
        $data['changedBy'] =  $this->session->userdata('emp_id');
        $where = "offenseType_ID =  $offense_category_id";
        $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_offense_type');
        echo json_encode($update_stat);
    }

    public function update_offense()
    {
        if ($this->input->post('origOffenseNum') <  $this->input->post('offenseNum')) {
            $this->decrement_order_num($this->input->post('offenseNum'),  $this->input->post('origOffenseNum'),  $this->input->post('offenseCategoryId'));
            $data['orderNum'] =  $this->input->post('offenseNum');
        } else if ($this->input->post('origOffenseNum') >  $this->input->post('offenseNum')) {
            $this->increment_order_num($this->input->post('offenseNum'),  $this->input->post('origOffenseNum'),  $this->input->post('offenseCategoryId'));
            $data['orderNum'] =  $this->input->post('offenseNum');
        }
        // backup offense history
        $offense_id =  $this->input->post('offenseId');
        $offense_obj =  $this->get_offense($offense_id);
        $this->add_offense_history($offense_obj);
        //  $data ['disciplineCategory_ID']=  $this->input->post('disciplineCategoryId');
        $data['offense'] =  $this->input->post('offense');
        $data['multiCategory'] = '' .  $this->input->post('multiCateg');
        $data['multiCategoryLetters'] =  $this->input->post("disciplineCategoryLetter");
        $data['changedBy'] =  $this->session->userdata('emp_id');
        $where = "offense_ID =  $offense_id";
        $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_offense');
        echo json_encode($update_stat);
    }

    private function decrement_order_num($num,  $orig_num,  $offense_category_id)
    {
        $query = "UPDATE tbl_dms_offense SET orderNum = orderNum-1 WHERE offenseType_ID =  $offense_category_id AND orderNum >  $orig_num AND  orderNum <  $num+1";
        return  $record =  $this->general_model->custom_query_no_return($query);
    }

    private function increment_order_num($num,  $orig_num,  $offense_category_id)
    {
        $query = "UPDATE tbl_dms_offense SET orderNum = orderNum+1 WHERE offenseType_ID =  $offense_category_id AND orderNum <  $orig_num AND  orderNum >  $num-1";
        return  $record =  $this->general_model->custom_query_no_return($query);
    }
    private function set_new_level2($new_level,  $disciplinary_action_id,  $category_id,  $active_disc_action_categ_settings_ID)
    {
        $data['level'] =  $new_level;
        $where = "disciplineCategory_ID =   $category_id AND disciplinaryAction_ID =  $disciplinary_action_id AND disciplinaryActionCategorySettings_ID =  $active_disc_action_categ_settings_ID";
        return  $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_disciplinary_action_category');
    }

    private function transfer_level($exchange_level,  $new_level,  $category_id,  $active_disc_action_categ_settings_ID)
    {
        $data['level'] =  $exchange_level;
        $where = "disciplineCategory_ID =   $category_id AND level =  $new_level AND disciplinaryActionCategorySettings_ID =  $active_disc_action_categ_settings_ID";
        return  $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_disciplinary_action_category');
    }

    public function rearrange_level()
    {
        $category_settings =  $this->get_current_disciplinary_action_category_settings();
        $disc_action_category_id =  $this->input->post('disciplinaryActionCategoryId');
        $new_level =  $this->input->post('newLevel');
        $exchange_level =  $this->input->post('exchangeLevel');
        $category_id =  $this->input->post('categoryId');
        $disciplinary_action_id =  $this->input->post('disciplinaryActionId');
        $update['transfer_level'] =  $this->transfer_level($exchange_level,  $new_level,  $category_id,  $category_settings->disciplinaryActionCategorySettings_ID);
        $update['set_new_level'] =  $this->set_new_level2($new_level,  $disciplinary_action_id,  $category_id,  $category_settings->disciplinaryActionCategorySettings_ID);
        echo json_encode($update);
    }

    private function move_level($category_id,  $level,  $active_disc_action_categ_settings_ID)
    {
        $query = "UPDATE tbl_dms_disciplinary_action_category SET level = level-1 WHERE disciplineCategory_ID =  $category_id AND level >  $level AND disciplinaryActionCategorySettings_ID =  $active_disc_action_categ_settings_ID";
        return  $record =  $this->general_model->custom_query_no_return($query);
    }

    private function insert_num_order($num,  $offense_category_id)
    {
        $query = "UPDATE tbl_dms_offense SET orderNum = orderNum+1 WHERE offenseType_ID =  $offense_category_id AND orderNum >  $num";
        return  $record =  $this->general_model->custom_query_no_return($query);
    }

    private function remove_num_order($num,  $offense_category_id)
    {
        $query = "UPDATE tbl_dms_offense SET orderNum = orderNum-1 WHERE offenseType_ID =  $offense_category_id AND orderNum >  $num";
        return  $record =  $this->general_model->custom_query_no_return($query);
    }

    private function deactivate_offense($offense_id)
    {
        $data['status_ID'] = 11;
        $where = "offense_ID =  $offense_id";
        return  $this->general_model->update_vals($data,  $where, 'tbl_dms_offense');
    }

    private function deactivate_offense_via_type($offense_category_id)
    {
        $data['status_ID'] = 11;
        $where = "offenseType_ID =  $offense_category_id";
        return  $this->general_model->update_vals($data,  $where, 'tbl_dms_offense');
    }

    private function deactivate_offense_type($offense_category_id)
    {
        $data['status_ID'] = 11;
        $where = "offenseType_ID =  $offense_category_id";
        return  $this->general_model->update_vals($data,  $where, 'tbl_dms_offense_type');
    }

    public function update_offense_multi_category($category_id)
    {
        $spec_category =  $this->get_offense_with_spec_category($category_id);
        $category_details =  $this->get_specific_discipline_category($category_id);
        $loop_count = 0;
        if (count($spec_category) > 0) {
            foreach ($spec_category as  $rows) {
                $categ_id_arr = explode(',',  $rows->multiCategory);
                $categ_letter_arr = explode(',',  $rows->multiCategoryLetters);
                $categ_id_key = array_search($category_id . "",  $categ_id_arr);
                $categ_letter_key = array_search($category_details->category . "",  $categ_letter_arr);
                if (count($categ_id_arr) > 1) {
                    if (false !==  $categ_id_key) {
                        unset($categ_id_arr[$categ_id_key]);
                    }
                    if (false !==  $categ_letter_key) {
                        unset($categ_letter_arr[$categ_letter_key]);
                    }
                } else {
                    if (false !==  $categ_id_key) {
                        $categ_id_arr[$categ_id_key] = '0';
                    }
                    if (false !==  $categ_letter_key) {
                        $categ_letter_arr[$categ_letter_key] = '0';
                    }
                }
                $updated_multicateg[$loop_count] = [
                    'offense_ID' =>  $rows->offense_ID,
                    'multiCategory' => implode(",", array_values($categ_id_arr)),
                    'multiCategoryLetters' => implode(",", array_values($categ_letter_arr)),
                ];
                $loop_count++;
            }
            return  $this->general_model->batch_update($updated_multicateg, 'offense_ID', 'tbl_dms_offense');
        } else {
            return 2;
        }
    }

    public function update_dtr_violation_settings()
    {
        $violation_id =  $this->input->post('violationId');
        $field_1 =  $this->input->post('field1');
        $value_1 =  $this->input->post('value1');
        $field_2 =  $this->input->post('field2');
        $value_2 =  $this->input->post('value2');
        if ($field_2 == 'durationUnit') {
            $data[$field_2] =  $value_2;
        }
        $data[$field_1] =  $value_1;
        $data['changedBy'] =  $this->session->userdata('emp_id');
        $where = "dtrViolationType_ID =  $violation_id";
        $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_dtr_violation_type_settings');
        echo json_encode($update_stat);
    }

    public function update_ir_deadline_settings()
    {
        $field =  $this->input->post('field');
        $value =  $this->input->post('value');
        $exist = 0;
        $ir_deadline_settings =  $this->get_ir_deadline_settings($field);
        if (count($ir_deadline_settings) > 0) {
            $exist = 1;
        } else {
            $add_default_stat =  $this->add_default_ir_deadline_settings();
            if ($add_default_stat) {
                $exist = 1;
            } else {
                $exist = 0;
            }
        }
        if ($exist) {
            $data[$field] =  $value;
            $data['changedBy'] =  $this->session->userdata('emp_id');
            $where = " $field IS NOT NULL";
            $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_incident_report_deadline_settings');
        } else {
            $update_stat = 0;
        }
        echo json_encode($update_stat);
    }

    public function update_ero_supervisor()
    {
        $ero_sup_id =  $this->input->post('supId');
        $emp_id =  $this->input->post('empId');
        $data['employeeRelationsSupervisor'] =  $ero_sup_id;
        $where = "emp_id =  $emp_id";
        $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_employee_relations_supervisor');
        echo json_encode($update_stat);
    }

    private function update_dtr_vio_settings_offense($offense_list)
    {
        $data['offense_ID'] = NULL;
        $where = "offense_ID IN ( $offense_list)";
        return  $this->general_model->update_vals($data,  $where, 'tbl_dms_dtr_violation_type_settings');
    }

    public function update_account_recommendation_number()
    {
        $data['number'] =  $this->input->post('recommendationNumberVal');
        $acc_rec_num_id =  $this->input->post('accountRecommendationNumId');
        $where = "accountRecommendationNumber_ID =  $acc_rec_num_id";
        $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_account_recommendation_number');
        echo json_encode($update_stat);
    }

    private function update_recommendation($ir_recommendation_id, $stat_id)
    {
        date_default_timezone_set('Asia/Manila');

        $data['liabilityStat_ID']  = $stat_id;
        $data['datedLiabilityStat']  = date("Y-m-d H:i");
        $where = "incidentReportRecommendation_ID = $ir_recommendation_id";
        $update_stat = $this->general_model->update_vals($data, $where, 'tbl_dms_incident_report_recommendation');
        return $update_stat;
    }

    private function update_qualified_dtr_vio_status($data,  $qualified_dtr_vio_id)
    {
        $where = "qualifiedUserDtrViolation_ID =  $qualified_dtr_vio_id";
        return  $this->general_model->update_vals($data,  $where, 'tbl_dms_qualified_user_dtr_violation');
    }

    public function update_default_category()
    {
        $data['category_ID']  =  $this->input->post('categoryId');
        $violation_type =  $this->input->post('violationType');
        $where = "dtrViolationType_ID =  $violation_type";
        $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_dtr_violation_type_settings');
        echo json_encode($update_stat);
    }

    private function deactivate_old_disc_action_category_settings()
    {
        $data['status_ID']  = 11;
        $where = "status_ID = 10";
        $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_disciplinary_action_category_settings');
        return  $update_stat;
    }
    private function update_dms_deadline($data, $record_id, $emp_id)
    {
        $where = "record_ID = $record_id AND emp_id = $emp_id";
        $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_deadline');
        return $update_stat;
    }

    public function set_current_prescriptive_stat_to_complete_if_liable($ir_id)
    {
        $ir = $this->get_ir($ir_id);
        $current_irs = [];
        $ir_current_ir = $this->get_current_ir_via_priscriptive_id($ir->prescriptiveId, $ir->subjectEmp_ID, $ir->offense_ID);
        // set to complete previous current ir
        if (count($ir_current_ir) > 0) {
            foreach ($ir_current_ir as $current_index => $current_row) {
                $current_irs[$current_index] = [
                    "incidentReport_ID" => $current_row->incidentReport_ID,
                    "prescriptiveStat" => 3
                ];
            }
            $this->general_model->batch_update($current_irs, 'incidentReport_ID', 'tbl_dms_incident_report');
        }
        // set new ir to current
        $where = "incidentReport_ID = $ir_id";
        $data['prescriptiveStat'] = 4;
        $this->general_model->update_vals($data,  $where, 'tbl_dms_incident_report');
    }

    public function set_to_complete_ongoing_ir_if_non_liable($ir_id)
    {
        $where = "incidentReport_ID = $ir_id";
        $data['prescriptiveStat'] = 3;
        $this->general_model->update_vals($data,  $where, 'tbl_dms_incident_report');
    }

    public function set_witness_to_cancelled($witness_id){
        $where = "incidentReportWitness_ID = $witness_id";
        $data['status_ID'] = 7;
        return $this->general_model->update_vals($data,  $where, 'tbl_dms_incident_report_witness');
    }


    // END OF UPDATE -----------------------------------------------------------------------------------

    // DELETE -----------------------------------------------------------------------------------
    public function delete_disciplinary_action()
    {
        $where['disciplinaryAction_ID'] =  $this->input->post('disciplinaryActionId');
        $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_disciplinary_action');
        echo json_encode($delete_stat);
    }

    public function delete_offense_level($disc_action_category_id)
    {
        $where['disciplinaryActionCategory_ID'] =  $disc_action_category_id;
        $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_disciplinary_action_category');
        return json_encode($delete_stat);
    }
    public function remove_offense_disciplinary_action()
    {
        $category_settings =  $this->get_current_disciplinary_action_category_settings();
        $disc_action_category_id =  $this->input->post('disciplinaryActionCategoryId');
        $category_id =  $this->input->post('categoryId');
        $level =  $this->input->post('level');
        $remove['delete_offense_level'] =  $this->delete_offense_level($disc_action_category_id);
        $remove['move_level'] =  $this->move_level($category_id,  $level,  $category_settings->disciplinaryActionCategorySettings_ID);
        echo json_encode($remove);
    }

    public function remove_category_levels($category_id)
    {
        $where['disciplineCategory_ID'] =  $category_id;
        $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_disciplinary_action_category');
        return json_encode($delete_stat);
    }

    public function remove_category_in_offense($category_id)
    {
        $where['disciplineCategory_ID'] =  $category_id;
        $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_disciplinary_action_category');
        return json_encode($delete_stat);
    }

    public function delete_offense_category($category_id)
    {
        $where['disciplineCategory_ID'] =  $category_id;
        $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_discipline_category');
        return json_encode($delete_stat);
    }

    public function delete_specific_category()
    {
        $category_id =  $this->input->post("categoryId");
        $delete_category['remove_category_levels'] =  $this->remove_category_levels($category_id);
        $delete_category['remove_category_on_offenses'] =  $this->update_offense_multi_category($category_id);
        $delete_category['delete_offense_category'] =  $this->delete_offense_category($category_id);
        echo json_encode($delete_category);
    }

    private function remove_offense_via_type($offense_category_id)
    {
        $where['offenseType_ID'] =  $offense_category_id;
        return  $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_offense');
    }

    private function delete_offense_category_type($offense_category_id)
    {
        $where['offenseType_ID'] =  $offense_category_id;
        return  $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_offense_type');
    }

    public function remove_assigned_offenses_via_type($offense_category_id)
    {
        $offenses =  $this->get_offenses_under_offense_type($offense_category_id);
        $offense_list = [];
        $count = 0;
        if (count($offenses) > 0) {
            foreach ($offenses as  $rows) {
                $offense_list[$count] =  $rows->offense_ID;
                $count++;
            }
            return  $this->update_dtr_vio_settings_offense(implode(',',  $offense_list));
        } else {
            return 0;
        }
    }

    public function remove_assigned_offense_type($offense_category_id)
    {
        $where['offenseType_ID'] =  $offense_category_id;
        return  $this->general_model->delete_vals($where, 'tbl_dms_offense_type_assignment');
    }

    public function remove_specific_offense_type()
    {
        $offense_category_id =  $this->input->post('offenseCategoryId');
        $del_offense_type['del_assigned_offenses_types'] =  $this->remove_assigned_offense_type($offense_category_id);
        $del_offense_type['del_assigned_offenses'] =  $this->remove_assigned_offenses_via_type($offense_category_id);
        $del_offense_type['deactivate_offense_stat'] =  $this->deactivate_offense_via_type($offense_category_id);
        $del_offense_type['deactivate_offense_type_stat'] =  $this->deactivate_offense_type($offense_category_id);
        echo json_encode($del_offense_type);
    }

    private function delete_offense($offense_id)
    {
        $where['offense_ID'] =  $offense_id;
        return  $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_offense');
    }
    public function deactivate_offense_process()
    {
        $offense_id =  $this->input->post("offenseId");
        $record =  $this->get_offense($offense_id);
        $remove['reorder_stat'] =  $this->remove_num_order($record->orderNum,  $record->offenseType_ID);
        $remove['remove_dtr_vio_set_offense'] =  $this->update_dtr_vio_settings_offense($offense_id);
        //  $remove ['remove_stat'] =  $this->delete_offense( $offense_id);
        $remove['deactivate_stat'] =  $this->deactivate_offense($offense_id);
        echo json_encode($remove);
    }

    public function remove_qip_assignment()
    {
        $qip_id =  $this->input->post("qipId");
        $where['qipAssignment_ID'] =  $qip_id;
        $delete_stat['remove_stat'] =  $this->general_model->delete_vals($where, 'tbl_dms_qip_assignment');
        echo json_encode($delete_stat);
    }

    public function remove_ero_account()
    {
        $ero_id =  $this->input->post("eroId");
        $emp_id =  $this->input->post("empId");
        $where['employeeRelationsOfficer_ID'] =  $ero_id;
        $delete_stat['remove_stat'] =  $this->general_model->delete_vals($where, 'tbl_dms_employee_relations_officer');
        $ero_records =  $this->get_all_ero_accounts_records($emp_id);
        if (count($ero_records) == 0) {
            $delete_stat['remove_sup_stat'] =  $this->remove_ero_supervisor($emp_id);
        }
        echo json_encode($delete_stat);
    }

    public function remove_offense_type_by_offense_type_ass_id()
    {
        $off_type_ass_id =  $this->input->post("offTypeAssId");
        $where['offenseTypeAssignment_ID'] =  $off_type_ass_id;
        $delete_stat['remove_stat'] =  $this->general_model->delete_vals($where, 'tbl_dms_offense_type_assignment');
        echo json_encode($delete_stat);
    }

    public function remove_offense_type_by_acc_id()
    {
        $acc_id =  $this->input->post("accId");
        $where['acc_id'] =  $acc_id;
        $delete_stat['remove_stat'] =  $this->general_model->delete_vals($where, 'tbl_dms_offense_type_assignment');
        echo json_encode($delete_stat);
    }

    public function remove_ero_supervisor($ero_id)
    {
        $where['emp_id'] =  $ero_id;
        return  $this->general_model->delete_vals($where, 'tbl_dms_employee_relations_supervisor');
    }

    public function remove_employee_relations_officer()
    {
        $where['emp_id'] =  $this->input->post('empId');
        $delete_stat['remove_sup_stat'] =  $this->remove_ero_supervisor($this->input->post('empId'));
        $delete_stat['remove_ero_stat'] =  $this->general_model->delete_vals($where, 'tbl_dms_employee_relations_officer');
        echo json_encode($delete_stat);
    }

    public function remove_recommendation_num()
    {
        $where['accountRecommendationNumber_ID'] =  $this->input->post("recommendationNumId");
        $delete_stat['remove_stat'] =  $this->general_model->delete_vals($where, 'tbl_dms_account_recommendation_number');
        echo json_encode($delete_stat);
    }

    public function remove_custom_recommendation_changes($cus_rec_id)
    {
        $where['dmsCustomRecommendation_ID'] =  $cus_rec_id;
        return  $this->general_model->delete_vals($where, 'tbl_dms_custom_recommendation_change');
    }

    public function remove_custom_recommendation()
    {
        $cus_rec_id =  $this->input->post("cusRecdId");
        $type =  $this->input->post("type");
        if ($type == "change") {
            $delete_stat['remove_change_to'] =  $this->remove_custom_recommendation_changes($cus_rec_id);
        }
        $where['dmsCustomRecommendation_ID'] =  $cus_rec_id;
        $delete_stat['remove_stat'] =  $this->general_model->delete_vals($where, 'tbl_dms_custom_recommendation');
        echo json_encode($delete_stat);
    }

    public function delete_assigned_to_recommmendation($dms_cus_rec_id)
    {
        $where['dmsCustomRecommendation_ID'] =  $dms_cus_rec_id;
        return  $this->general_model->delete_vals($where, 'tbl_dms_custom_recommendation');
    }
    public function delete_all_related_details($user_dtr_vio_id, $table)
    {
        $where['userDtrViolation_ID'] =  $user_dtr_vio_id;
        return  $this->general_model->delete_vals($where, $table);
    }
    
    protected function remove_qualified_related_details($user_dtr_vio_id)
    {
        // remove records in tbl_dms_user_dtr_violation_details
        $this->delete_all_related_details($user_dtr_vio_id, 'tbl_dms_user_dtr_violation_details');
        // remove records in tbl_dms_user_dtr_violation_related_details
        $this->delete_all_related_details($user_dtr_vio_id, 'tbl_dms_user_dtr_violation_related_details');
        // remove records in tbl_dms_user_dtr_violation_allowable_break
        $this->delete_all_related_details($user_dtr_vio_id, 'tbl_dms_user_dtr_violation_allowable_break');
    }
    protected function remove_qualified_user_dtr_violation($qualified_user_dtr_vio_id)
    {
        $where['qualifiedUserDtrViolation_ID'] =  $qualified_user_dtr_vio_id;
        return  $this->general_model->delete_vals($where, 'tbl_dms_qualified_user_dtr_violation');
    }

    protected function remove_user_dtr_violation_related_details($user_dtr_vio_id)
    {
        $where['userDtrViolation_ID'] =  $user_dtr_vio_id;
        return  $this->general_model->delete_vals($where, 'tbl_dms_user_dtr_violation_related_details');
    }

    protected function remove_added_ir_history($ir_id)
    {
        $where['incidentReport_ID'] =  $ir_id;
        return  $this->general_model->delete_vals($where, 'tbl_dms_added_ir_history');
    }

    protected function remove_main_ir_of_added_ir_history($ir_id)
    {
        $where['incidentReport_ID'] =  $ir_id;
        return  $this->general_model->delete_vals($where, 'tbl_dms_incident_report');
    }

    public function removeWitness(){
        $witness_id = $this->input->post('witnessId');
        $data['confirmed'] = 1;
        $witness_stat = $this->check_witness_confirmation_status($witness_id);
        if((int)$witness_stat->status_ID != 2){
            $data['confirmed'] = 0;
        }else{
            $data['cancel_stat'] = $this->set_witness_to_cancelled($witness_id);
        }
        echo json_encode($data);
    }

    public function remove_ir_history(){
        $ir_id =  $this->input->post('irId');
        $ir_history_record = $this->check_if_ir_history_record_exist($ir_id);
        if(count($ir_history_record) > 0){
            $history = $this->remove_added_ir_history($ir_id);
            $ir = $this->remove_main_ir_of_added_ir_history($ir_id);
            if($history == 1 && $ir == 1){
                echo json_encode(1);
            }else{
                echo json_encode(0);
            }
        }else{
            echo json_encode(2);
        }
    }

    // END OF DELETE

    public function get_allowable_break_test()
    {
        $allowable =  $this->get_allowable_break(37, 150947);
        ////var_dump($allowable['breakData']);
        $hours = 0;
        $minutes = 0;
        $allow_count = 0;
        foreach ($allowable['breakData'] as  $rows) {
            $hours +=  $rows->hour;
            $minutes +=  $rows->min;
            $hours_to_min =  $hours * 60;
            $minutes +=  $hours_to_min;
            $dtr_vio_allowable_break[$allow_count] = [
                "userDtrViolation_ID" => 1,
                "breakType" =>  $rows->break_type,
                "duration" =>  $minutes
            ];
            $allow_count++;
        }
        ////var_dump($dtr_vio_allowable_break);
    }

    // $shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
    // $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
    // $new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
    // $new_formatted_shift_end = $this->new_format_midnight_shift_out($new_shifts['end']);
    // $shift_out_str = strtotime($new_formatted_shift_end . ' +120 minutes');

    // public function monthly_check(){
    //      $dateTime =  $this->get_current_date_time();
    //      $date = new DateTime( $dateTime ['date']);
    //      $month =  $date->format("m");
    //     echo "Monthnummer:  $month";
    // }

    public function weekly_check()
    {
        $ddate = "2019-02-17";
        $date = new DateTime($ddate);
        $week =  $date->format("W");
        echo "Weeknummer:  $week";
    }

    private function add_offense_history($offense_obj)
    {
        $data['offense_ID'] = $offense_obj->offense_ID;
        $data['updatedBy'] = $this->session->userdata('emp_id');
        $data['offense'] = $offense_obj->offense;
        $data['multiCategory'] = $offense_obj->multiCategory;
        return $this->general_model->insert_vals($data, "tbl_dms_offense_history");
    }

    private function add_offense_type_history($offense_type_obj)
    {
        $dateTime =  $this->get_current_date_time();
        $data['offenseType_ID'] = $offense_type_obj->offenseType_ID;
        $data['letter'] = $offense_type_obj->letter;
        $data['offenseType'] = $offense_type_obj->offenseType;
        $data['updatedBy'] = $this->session->userdata('emp_id');
        $data['updatedOn'] =  $dateTime['dateTime'];
        return $this->general_model->insert_vals($data, "tbl_dms_offense_type_history");
    }

    // public function set_deadline_to_file_ir( $direct_sup,  $qualified_user_dtr_violation_id,  $violation_rules){
    //      $dateTime =  $this->get_current_date_time();
    //      $deadline_date = Date("Y-m-d", strtotime( $dateTime ['date'] .  $violation_rules->daysDeadline.' days'));
    //      $deadline_date_time =  $deadline_date . " 23:59:59";
    //      $data ['record_ID'] =  $qualified_user_dtr_violation_id;
    //      $data ['emp_id'] =  $direct_sup->Supervisor;
    //      $data ['deadline'] =  $deadline_date_time;
    //      $data ['dmsDeadlineType_ID'] = 1;
    //      $data ['status_ID'] = 2;
    //      $data ['level'] = 1;
    //     return  $this->general_model->insert_vals( $data, "tbl_dms_deadline");
    // }

    public function testVioSettingsBool()
    {
        $violation_rules =  $this->get_dtr_violation_settings(7); //Get violation rules  and prescription (violation settings)

        if (($violation_rules->offense_ID == NULL) || ($violation_rules->category_ID == NULL)) {
            echo "incomplete settings";
        } else {
            echo "proceed checking";
        }
    }

    public function recheck_qualified_dtr_violation()
    { }

    public function test_bool_assign()
    {
        $offense_ID = NULL;
        $category_ID = NULL;
        if (($offense_ID == NULL) || ($category_ID == NULL)) {
            echo "ERROR";
        } else {
            echo "OK";
        }
    }



    // public function auto_ir_notification_process()
    // {
    //     // 1687 - late 1693 -2, 1696
    //     // 1688 - ob 1694 -2 1698, 1699
    //     // 1689 - ut
    //     // 1690 - forgot to logout
    //     // 1691 - inc break 1695 -2
    //     // 1692 - inc dtr logs 1700
    //     // 1697 -absent
    //     $qualified = 0;
    //     $qualify['valid'] = 0;
    //     $qualify['error'] = 0;
    //     $qualify['details'] = "";
    //     $valid = 0;
    //     // 1687
    //     $user_dtr_violation_id = 1713;
    //     // GET USER DTR VIOLATION
    //     $user_dtr_violation = $this->get_user_dtr_violation($user_dtr_violation_id);
    //     $subject_user_emp_details = $this->get_emp_details_via_user_id($user_dtr_violation->user_ID);
    //     $notif_supvervisor_bool = 0;
    //     if(count($user_dtr_violation) > 0)
    //     {
    //         $violation_details = $this->get_violation_related_details($user_dtr_violation);// check if violation is valid
    //         if ($violation_details['valid']) //check if incident is valid
    //         {
    //             $qualify['valid'] = 1;
    //             $violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID); //Get violation rules and prescription (violation settings)
    //             if (($violation_rules->offense_ID == NULL) || ($violation_rules->category_ID == NULL))
    //             {
    //                 $qualify['error'] = 1;
    //                 $qualify['details'] = "Either Default Offense or Default Category was not set for this violation type";
    //             } else {
    //                 $ongoing_prescriptive_stat_ir = $this->get_existing_ir($subject_user_emp_details->emp_id, 2, 2, $violation_rules->offense_ID);
    //                 $pending_qualified_exist = $this->check_if_pending_qualified_exist($violation_rules->offense_ID, $user_dtr_violation->user_ID); // Check if pending qualified user dtr violation exist
    //                 if(count($ongoing_prescriptive_stat_ir) > 0){
    //                     $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0);
    //                     $qualify['qualify_status'] = "A pending IR of the same offense exist";
    //                 }else if(count($pending_qualified_exist) > 0){
    //                     $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0);
    //                     $qualify['qualify_status'] = "A pending qualified user dtr violation of same offense exist";
    //                 }else{
    //                     $ir_exist = $this->check_if_previous_ir_exist($user_dtr_violation, $subject_user_emp_details, $violation_rules); // Check if IR of the same violation exist
    //                     if ($ir_exist['exist']) // if a liable IR of current status and the same offense exist
    //                     {
    //                         $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2, 0, $user_dtr_violation_id, 0);
    //                         $qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
    //                         //auto_notify_ir_direct_sup 
    //                         $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules, $violation_details);
    //                         $qualify['qualify_status'] = "An IR of the same offense that has a currently ongoing prescriptive period exist";
    //                     }else{
    //                         $auto_ir_notify_rules = $this->check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules);
    //                         if ($auto_ir_notify_rules['valid']) {
    //                             $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2, $auto_ir_notify_rules['occurence_rule_num'], $user_dtr_violation_id, $auto_ir_notify_rules['occurence_value']);
    //                             // $user_data['status_ID'] = 3;
    //                             // $qualify['user_dtr_violation_stat'] = $this->update_user_dtr_violation($user_dtr_violation_id, $user_data);
    //                             $qualify['qualify_status'] = "The User DTR Violation fits on a specific violation rule";
    //                         }else{
    //                             if ($user_dtr_violation->dtrViolationType_ID == 5) {
    //                                 $qualify['valid'] = 1;
    //                                 $qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
    //                                 //auto_notify_ir_direct_sup
    //                                 $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules, $violation_details);
    //                                 $qualify['qualify_status'] = "The User DTR violation detected is Absent";
    //                             } else {
    //                                 $qualify['error_details'] = "Does not qualify Rules";
    //                             }
    //                         }
    //                     }
    //                 }
    //                 // OLD LINE CODES
    //                 $ir_exist = $this->check_if_previous_ir_exist($user_dtr_violation, $subject_user_emp_details, $violation_rules); // Check if IR of the same violation exist
    //                 if ($ir_exist['exist'])// if a liable IR of current status and the same offense exist
    //                 {
    //                     // Escalate
    //                     $qualify['valid'] = 1;

    //                     $category_settings = $this->get_current_disciplinary_action_category_settings();
    //                     $next_level_disc_action = $this->get_offense_disc_action_by_level($violation_rules->offense_ID, $ir_exist['ir_details']->level+1, $category_settings->disciplinaryActionCategorySettings_ID);


    //                     if (count($next_level_disc_action) > 0)
    //                     {
    //                         $next_level_stat = 1;
    //                         $disc_level = $ir_exist['ir_details']->level + 1;
    //                         $disc_action_id = $next_level_disc_action->disciplinaryAction_ID;
    //                     }
    //                     else
    //                     {
    //                         $max_level_disc_action = $this->get_max_offense_disc_action_by_level($violation_rules->offense_ID);
    //                         if (count($max_level_disc_action) > 0)
    //                         {
    //                             $next_level_stat = 2;
    //                             $disc_level = $max_level_disc_action->level;
    //                             $disc_action_id = $max_level_disc_action->disciplinaryAction_ID;
    //                         }
    //                     }
    //                     if ($next_level_stat != 0)
    //                     {
    //                         //save IR Details
    //                         $pending_qualified_exist = $this->check_if_pending_qualified_exist($violation_rules->offense_ID, $user_dtr_violation->user_ID); // Check if pending qualified user dtr violation exist
    //                         $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, $disc_level, 2, 0, $user_dtr_violation_id, $disc_action_id, $violation_rules->offense_ID, $violation_rules->category_ID, 0);
    //                         $user_data['status_ID'] = 3;
    //                         $qualify['user_data_violation_stat'] = $this->update_user_dtr_violation($user_dtr_violation_id, $user_data);
    //                         if ($pending_qualified_exist)
    //                         {
    //                             // set qualified dtr vio to on hold status
    //                             $data['status_ID'] = 21;
    //                             $qualify['set_to_on_hold'] = $this->update_qualified_dtr_vio_status($data, $qualified_user_dtr_violation_id);
    //                             $qualify['details'] = "pending qualified exist";
    //                         }
    //                         else
    //                         {
    //                             $pending_ir_exist = $this->check_if_pending_ir_exist($user_dtr_violation->dtrViolationType_ID, $subject_user_emp_details->emp_id); // Check if pending ir exist
    //                             if ($pending_ir_exist)
    //                             {
    //                                 // set qualified dtr vio to on hold status
    //                                 $data['status_ID'] = 21;
    //                                 $qualify['set_to_on_hold'] = $this->update_qualified_dtr_vio_status($data, $qualified_user_dtr_violation_id);
    //                                 $qualify['details'] = "pending qualified IR";
    //                             }
    //                             else
    //                             {
    //                                 $qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
    //                                 //auto_notify_ir_direct_sup
    //                                 $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules, $violation_details);
    //                                 $qualify['qualify_status'] = "The User DTR violation detected is Absent";
    //                             } else {
    //                                 $qualify['error_details'] = "Does not qualify Rules";
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //         }else{
    //             $qualify['error_details'] = "Violation is not true";
    //             $qualify['related_details'] = $violation_details['error_details'];
    //             // set USER DTR VIOLATION to INVALID
    //             $user_data_vio['status_ID'] = 23;
    //             $qualify['user_dtr_update_to_invalid'] = $this->update_user_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data_vio);
    //         }
    //     } else {
    //         $qualify['error'] = 1;
    //         $qualify['error_details'] = "user dtr violation not found";
    //     }
    //     ////var_dump($qualify);
    // }

    public function user_dtr_vio_generator($sched_id,  $sched_date)
    {
        $data['user_ID'] = 275;
        $data['supervisor_ID'] = 26;
        $data['dtrViolationType_ID'] = 7;
        $data['sched_ID'] =  $sched_id;
        $data['sched_date'] =  $sched_date;
        $data['month'] = date("F", strtotime($sched_date));
        $data['year'] = date("Y", strtotime($sched_date));
        return  $this->general_model->insert_vals_last_inserted_id($data, 'tbl_user_dtr_violation');
    }

    // public function test_calendar_change(){
    //      $feb =
    // }

    public function log_generator($log,  $acc_time_id,  $entry,  $type,  $sched_id = null,  $note = null)
    {
        // LOGIN LOG
        $login['emp_id'] = 557;
        $login['acc_time_id'] =  $acc_time_id;
        $login['entry'] =  $entry;
        $login['type'] =  $type;
        $login['log'] =  $log;
        $login['sched_id'] =  $sched_id;
        $login['note'] =  $note;
        return  $this->general_model->insert_vals_last_inserted_id($login, 'tbl_dtr_logs');
    }

    public function sched_generator()
    {
        $sched_date = "2019-02-28";
        $sched['emp_id'] = 557;
        $sched['sched_date'] =  $sched_date;
        $sched['schedtype_id'] = 1;
        $sched['acc_time_id'] = 1833;
        $sched['remarks'] = 'Excel';
        $sched['updated_by'] = 0;
        $sched['isActive'] = 1;
        $sched_id =  $this->general_model->insert_vals_last_inserted_id($sched, 'tbl_schedule');
        ////var_dump($sched_id);
    }

    public function dtr_transaction()
    {
        // 161343, 161345, 161346,
        // 161448 - inc break sched
        //161449 second late occ, 161452
        //161450 second ob 161454, 161455
        $sched_date = '2019-05-31';

        // LOGIN
        // acc_id = 1847
        $log = "2019-05-31 20:58:10";
        $acc_time_id = 1833; //1833, 1847
        $entry = 'I';
        $type = "DTR";
        $sched_id = 198529;
        $note = null;

        // BREAK IN
        //  $log = "2019-05-28 01:58:00";
        //  $acc_time_id = 1847;
        //  $entry = 'I';
        //  $type = "Break";
        //  $sched_id = NULL;
        //  $note = 948255;
        // // BREAK OUT
        //  $log = "2019-02-29 01:00:00";
        //  $acc_time_id = 4614;
        //  $entry = 'O';
        //  $type = "Break";
        //  $sched_id = NULL;
        //  $note = NULL;
        // // LOGOUT
        //  $log = "2019-05-30 07:58:00";
        //  $acc_time_id = 1833;
        //  $entry = 'O';
        //  $type = "DTR";
        $sched_id = 198529;
        //  $note = 948259;
        //  $log_gen =  $this->log_generator( $log,  $acc_time_id,  $entry,  $type,  $sched_id,  $note);
        // ////var_dump( $log_gen);
        //  $sched_id = 161448;
        $user_data_vio =  $this->user_dtr_vio_generator($sched_id,  $sched_date);
        ////var_dump($user_data_vio);
    }
    // Charise start code
    // public function get_selected_employee_forIR()
    // {
    //     $empid =  $this->input->post("emp_id");
    //     $result =  $this->get_emp_details_via_emp_id($empid);
    //     echo json_encode($result);
    // }
    public function get_selected_employee_forIR()
    {
        $empid =  $this->input->post("emp_id");
        $empses_id = $this->session->userdata('emp_id');
        $result['empdet'] =  $this->get_emp_details_via_emp_id($empid);
        $witnessquery = "SELECT emp.emp_id, users.uid, app.pic, app.fname, app.lname, app.mname, app.nameExt, app.gender, emp.Supervisor, accounts.acc_id, accounts.acc_name accountDescription, accounts.acc_description empType, positions.pos_details positionDescription, positions.site_ID, empStat.status as empStatus FROM tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app, tbl_account accounts, tbl_user users WHERE empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.emp_id != $empid AND users.emp_id != $empses_id";
        $result['witselect'] = $this->general_model->custom_query($witnessquery);
        echo json_encode($result);
    }
    private function fetching_emp($accountid = null)
    {
        $empid =  $this->session->userdata('emp_id');
        $append = ($accountid != null) ? " AND emp.acc_id=acc.acc_id AND emp.acc_id= $accountid AND acc.acc_id= $accountid " : "";
        $data =  $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description, estat.status as estatus", "emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes' AND emp.emp_id!=$empid AND emp.emp_id!=184 AND emp.emp_id=prom.emp_id AND prom.isActive='1' AND prom.ishistory='1' AND prom.posempstat_id=epstat.posempstat_id AND epstat.empstat_id=estat.empstat_id AND user.emp_id=emp.emp_id  $append", "tbl_applicant ap, tbl_employee emp,tbl_account acc, tbl_user user, tbl_emp_promote prom, tbl_pos_emp_stat epstat, tbl_emp_stat estat", "ap.lname ASC");
        return  $data;
    }
    public function get_all_employees()
    {
        $data =  $this->fetching_emp();
        echo json_encode($data);
    }
    public function qip_privilege()
    {
        $empid =  $this->session->userdata('emp_id');
        $assigned_memberqip =  $this->general_model->fetch_specific_val("qipAssignment_ID", "emp_id= $empid", "tbl_dms_qip_assignment");
        $q_normal_offense = "SELECT offenseType,letter,offenseType_ID FROM tbl_dms_offense_type WHERE isDefault=0 AND offenseType_ID NOT IN (SELECT offenseType_ID FROM tbl_dms_offense_type_assignment) GROUP BY letter ASC";

        if ($assigned_memberqip != null) {
            $q_employee = "SELECT acc_id FROM tbl_employee WHERE emp_id= $empid";

            $q2 =  $this->general_model->custom_query($q_employee);
            $emp_acc = array_shift($q2);
            $accountid =  $emp_acc->{'acc_id'};

            $q_assigned_accountqip = $this->general_model->custom_query("SELECT offenseTypeAssignment_ID,offenseType_ID FROM tbl_dms_offense_type_assignment WHERE acc_id= $accountid");
                
            if ($q_assigned_accountqip != NULL) {
                // $q =  $this->general_model->custom_query($q_assigned_accountqip);
                // $offense = array_shift($q);
                // var_dump($q);
                // $offense_ID =  $offense->{'offenseType_ID'};
                $offense_ID = $q_assigned_accountqip[0]->offenseType_ID;

                $offense_type =  $this->general_model->fetch_specific_val("offense_ID", "offenseType_ID= $offense_ID", "tbl_dms_offense");

                // GET QIP OFFENSE TYPE
                if ($offense_type != null) {

                    $q_qip_offensetypes = "SELECT ass.offenseTypeAssignment_ID,ass.offenseType_ID,ass.acc_id,off.letter,off.offenseType,off.offenseType_ID FROM tbl_dms_offense_type_assignment ass,tbl_dms_offense_type off WHERE ass.offenseType_ID= $offense_ID AND off.offenseType_ID= $offense_ID AND ass.offenseType_ID=off.offenseType_ID GROUP BY off.letter ASC";
                    $qip_values['offense_types_qip'] =  $this->general_model->custom_query($q_qip_offensetypes);
                    $qip_values['enable'] = "1";
                } else {
                    $qip_values['enable'] = "0";
                }
            } else {
                $qip_values['enable'] = "0";
            }
        } else {
            $qip_values['enable'] = "0";
        }
        $qip_values['offense_type'] =  $this->general_model->custom_query($q_normal_offense);
        echo json_encode($qip_values);
    }
    public function get_alloffense_types()
    {
        $ot = "SELECT offenseType_ID, offenseType FROM tbl_dms_offense_type";
        $offense_types =  $this->general_model->custom_query($ot);
        echo json_encode($offense_types);
    }
    public function incident_type()
    {
        $o_val =  $this->input->post("offense_val");
        $o_id =  $this->input->post("offense_id");
        $acc_id =  $this->input->post("accountID");
        $empid =  $this->input->post("emp_id");
        if ($o_val == 2) {
            //  $q_offense = "SELECT ass.offenseTypeAssignment_ID,ass.offenseType_ID,off.letter,off.offenseType,off.offenseType_ID FROM tbl_dms_offense_type_assignment ass,tbl_dms_offense_type off WHERE ass.acc_id= $acc_id AND ass.offenseType_ID=off.offenseType_ID";
            $q_offense = "SELECT ass.offenseTypeAssignment_ID,ass.offenseType_ID,offense.letter,offense.offenseType,offense.offenseType_ID FROM tbl_dms_offense_type_assignment ass,tbl_dms_offense_type offense,tbl_employee emp WHERE ass.acc_id=emp.acc_id AND offense.status_ID=10 AND ass.offenseType_ID=offense.offenseType_ID AND emp.emp_id=' $empid'";
            //  $offense ['employee_qip'] =  $this->fetching_emp( $acc_id);
        } else {
            $q_offense = "SELECT offenseType,letter,offenseType_ID FROM tbl_dms_offense_type WHERE isDefault=0 AND status_ID=10 AND offenseType_ID NOT IN (SELECT offenseType_ID FROM tbl_dms_offense_type_assignment) GROUP BY letter ASC";
        }
        $offense['employee_qip'] =  $this->fetching_emp();
        //  $offense ['employees_qip']= $this->get_all_employees( $acc_id);
        $offense['offense_val'] =  $this->general_model->custom_query($q_offense);
        echo json_encode($offense);
    }
    public function get_offenses_selected_OffenseType()
    {
        $offenseID =  $this->input->post("offense_id");
        $data_tofetch =  $this->input->post("tofetch");
        $subj_emp_id =  $this->input->post("subj_emp_id");
        $incidentDate =  $this->input->post("incident_date");
        // if fetch offense base from offense type
        if ($data_tofetch == "1") {
            $fetchoffenses =  $this->get_specific_offenses($offenseID);
        } else {
            // if fetch category from offense            
            $existing_IR =  $this->get_existing_ir($subj_emp_id, 4, 17,  $offenseID);
            // var_dump("EXISTING IR ------------------>");
            // var_dump($existing_IR);
            $pending_IR =  $this->get_existing_ir_pending($subj_emp_id, 2, 2,  $offenseID);
            if (count($pending_IR) > 0) {
                $category_ID =  $pending_IR->disciplineCategory_ID;
                $fetchoffenses['exist_ornot'] = 2;
                $fetchoffenses['categories'] =  $pending_IR;
            } else if (count($existing_IR) > 0) {
                $category_ID =  $existing_IR->disciplineCategory_ID;
                $current_presc_str = strtotime(date("Y-m-d", strtotime($existing_IR->prescriptionEnd)));
                $date_detected_str = strtotime(date("Y-m-d", strtotime($incidentDate)));
                if ($date_detected_str <  $current_presc_str) {
                    // var_dump('less than');
                    $fetchoffenses['exist_ornot'] = 1;
                    // GEt categories with IR history 
                    $fetchoffenses['spec_category'] = $this->general_model->custom_query("SELECT cat.disciplineCategory_ID,cat.category,of.multiCategoryLetters FROM tbl_dms_discipline_category cat, tbl_dms_offense of WHERE of.multiCategory=cat.disciplineCategory_ID AND of.offense_ID=$offenseID");
                    $fetchoffenses['categories'] =  $existing_IR;
                    $prescriptive_id =  $existing_IR->prescriptiveId;
                    $disciplinary_categset_id =  $existing_IR->disciplinaryActionCategorySettings_ID;
                    // $fetchoffenses['history'] = $this->qry_ir_history($existing_IR->incidentReport_ID,  $prescriptive_id, $existing_IR->occurence+1);
                    $fetchoffenses['history'] =  $this->get_ir_history($subj_emp_id,  $offenseID,  $prescriptive_id);
                    // var_dump("IR HISTORY ------------------>");
                    // var_dump($fetchoffenses['history']);
                    $level =  $existing_IR->level;
                    // var_dump("LEVEL ------------------>");
                    // var_dump($level);
                    $level++;
                    $check_next =  $this->get_offense_disc_action_by_level($offenseID,  $level,  $disciplinary_categset_id,  $category_ID);
                    // var_dump("NEXT DISC ACTION ------------------>");
                    // var_dump($check_next);
                    if ($check_next != null) {
                        $fetchoffenses['nextlevel'] =  $check_next;
                        $month_period =  $check_next->periodMonth;
                    } else {
                        $max =  $this->get_max_offense_disc_action_by_level($offenseID,  $category_ID);
                        // var_dump($max);
                        $get_max_level =  $max[0]->level;
                        $max_nextlevel =  $this->get_offense_disc_action_by_level_category($offenseID,  $get_max_level,  $category_ID);
                        $fetchoffenses['nextlevel'] =  $max_nextlevel;
                        $month_period =  $max_nextlevel->periodMonth;
                    }
                    $start_end_date =  $this->unfixed_prescriptive_period($incidentDate,  $month_period, 'month');
                    $fetchoffenses['curedate'] =   $start_end_date['end'];
                    if ($start_end_date['end'] ==  $incidentDate) {
                        $start_end_coverage =  $this->unfixed_prescriptive_period($incidentDate,  $month_period + 1, 'month');
                        $fetchoffenses['curedate'] =  $start_end_coverage['end'];
                    }
                } else {
                    // var_dump('greater than');
                    $fetchoffenses['exist_ornot'] = 3;
                    $oc =  $this->get_offense($offenseID);
                    $multiCategory =  $oc->multiCategory;
                    if ($multiCategory == "all" ||  $multiCategory == "0") {
                        $q_category = "SELECT DISTINCT a.disciplineCategory_ID, b.category FROM tbl_dms_disciplinary_action_category a,tbl_dms_discipline_category b WHERE a.disciplineCategory_ID=b.disciplineCategory_ID AND disciplinaryActionCategorySettings_ID = (SELECT disciplinaryActionCategorySettings_ID FROM tbl_dms_disciplinary_action_category_settings WHERE status_ID = 10)";
                    } else {
                        $q_category = "SELECT disciplineCategory_ID,category FROM tbl_dms_discipline_category WHERE disciplineCategory_ID IN ( $multiCategory)";
                    }
                    $fetchoffenses['categories'] =  $this->general_model->custom_query($q_category);
                }
            } else {
                $fetchoffenses['exist_ornot'] = 0;
                $oc =  $this->get_offense($offenseID);
                $multiCategory =  $oc->multiCategory;
                if ($multiCategory == "all" ||  $multiCategory == "0") {
                    $q_category = "SELECT DISTINCT a.disciplineCategory_ID, b.category FROM tbl_dms_disciplinary_action_category a,tbl_dms_discipline_category b WHERE a.disciplineCategory_ID=b.disciplineCategory_ID AND disciplinaryActionCategorySettings_ID = (SELECT disciplinaryActionCategorySettings_ID FROM tbl_dms_disciplinary_action_category_settings WHERE status_ID = 10)";
                } else {
                    $q_category = "SELECT disciplineCategory_ID,category FROM tbl_dms_discipline_category WHERE disciplineCategory_ID IN ( $multiCategory)";
                }
                // no history query for categories below
                $fetchoffenses['categories'] =  $this->general_model->custom_query($q_category);
            }
        }

        //  if (count($existing_IR) > 0) {
        //      $category_ID =  $existing_IR->disciplineCategory_ID;
        //      $current_presc_str = strtotime(date("Y-m-d", strtotime($existing_IR->prescriptionEnd)));
        //      $date_detected_str = strtotime(date("Y-m-d", strtotime($incidentDate)));
        //      if ($date_detected_str <  $current_presc_str) {
        //          $fetchoffenses['exist_ornot'] = 1;
        //          $fetchoffenses['categories'] =  $existing_IR;
        //          $prescriptive_id =  $existing_IR->prescriptiveId;
        //          $disciplinary_categset_id =  $existing_IR->disciplinaryActionCategorySettings_ID;
        //          $fetchoffenses['history'] =  $this->get_ir_history($subj_emp_id,  $offenseID,  $prescriptive_id);

        //          $level =  $existing_IR->level;
        //          $level++;
        //          $check_next =  $this->get_offense_disc_action_by_level($offenseID,  $level,  $disciplinary_categset_id,  $category_ID);
        //          if ($check_next != null) {
        //              $fetchoffenses['nextlevel'] =  $check_next;
        //              $month_period =  $check_next->periodMonth - 1;
        //          } else {
        //              $max =  $this->get_max_offense_disc_action_by_level($offenseID,  $category_ID);
        //              $get_max_level =  $max->level;
        //              $max_nextlevel =  $this->get_offense_disc_action_by_level_category($offenseID,  $get_max_level,  $category_ID);
        //              $fetchoffenses['nextlevel'] =  $max_nextlevel;
        //              $month_period =  $max_nextlevel->periodMonth - 1;
        //          }
        //          $start_end_date =  $this->get_start_and_end_date_coverage_increment($incidentDate,  $month_period, 'month');
        //          $fetchoffenses['curedate'] =   $start_end_date['end'];
        //          if ($start_end_date['end'] ==  $incidentDate) {
        //              $start_end_coverage =  $this->get_start_and_end_date_coverage_increment($incidentDate,  $month_period + 1, 'month');
        //              $fetchoffenses['curedate'] =  $start_end_coverage['end'];
        //          }
        //      } else {
        //          $fetchoffenses['exist_ornot'] = 3;
        //          $oc =  $this->get_offense($offenseID);
        //          $multiCategory =  $oc->multiCategory;
        //          if ($multiCategory == "all" ||  $multiCategory == "0") {
        //              $q_category = "SELECT DISTINCT a.disciplineCategory_ID, b.category FROM tbl_dms_disciplinary_action_category a,tbl_dms_discipline_category b WHERE a.disciplineCategory_ID=b.disciplineCategory_ID AND disciplinaryActionCategorySettings_ID = (SELECT disciplinaryActionCategorySettings_ID FROM tbl_dms_disciplinary_action_category_settings WHERE status_ID = 10)";
        //          } else {
        //              $q_category = "SELECT disciplineCategory_ID,category FROM tbl_dms_discipline_category WHERE disciplineCategory_ID IN ( $multiCategory)";
        //          }
        //          $fetchoffenses['categories'] =  $this->general_model->custom_query($q_category);
        //      }
        //  } else {
        //      $pending_IR =  $this->get_existing_ir_pending($subj_emp_id, 2, 2,  $offenseID);
        //      if (count($pending_IR) > 0) {
        //          $category_ID =  $pending_IR->disciplineCategory_ID;
        //          $fetchoffenses['exist_ornot'] = 2;
        //          $fetchoffenses['categories'] =  $pending_IR;
        //      } else {
        //          $fetchoffenses['exist_ornot'] = 0;
        //          $oc =  $this->get_offense($offenseID);
        //          $multiCategory =  $oc->multiCategory;
        //          if ($multiCategory == "all" ||  $multiCategory == "0") {
        //              $q_category = "SELECT DISTINCT a.disciplineCategory_ID, b.category FROM tbl_dms_disciplinary_action_category a,tbl_dms_discipline_category b WHERE a.disciplineCategory_ID=b.disciplineCategory_ID AND disciplinaryActionCategorySettings_ID = (SELECT disciplinaryActionCategorySettings_ID FROM tbl_dms_disciplinary_action_category_settings WHERE status_ID = 10)";
        //          } else {
        //              $q_category = "SELECT disciplineCategory_ID,category FROM tbl_dms_discipline_category WHERE disciplineCategory_ID IN ( $multiCategory)";
        //          }
        //          $fetchoffenses['categories'] =  $this->general_model->custom_query($q_category);
        //      }
        //  }

        echo json_encode($fetchoffenses);
    }

    public function unfixed_prescriptive_period($sched_date,  $rule,  $unit)
    {
        // $sched_date = "2019-08-01";
        // $rule = 1;
        // $unit = 'month';
        if ($unit == 'month') {
            $coverage_to_month = Date("Y-m-d", strtotime($sched_date . '+' .  $rule . ' months'));
            $start = new DateTime($sched_date);
            $start_day = Date("d", strtotime($sched_date));
            $end_day = Date("d", strtotime($sched_date . '+' .  $rule . ' months'));
            $date['start'] =  $sched_date;
            $date['end'] =  $coverage_to_month;
            if ($start_day !== $end_day) {
                $prev_month = Date("Y-m-d", strtotime($coverage_to_month . '-1 months'));
                $prev_month_dated = new DateTime($prev_month);
                $date['end'] =  $prev_month_dated->format('Y-m-t');
            }
        } else {
            $start_date = new DateTime($sched_date);
            $start_year = (int) Date("Y", strtotime($sched_date));
            $start_week = (int) Date("W", strtotime($sched_date));
            $date['start'] =  $start_date->setISODate($start_year,  $start_week)->format('Y-m-d');
            $end_date = new DateTime(Date("Y-m-d", strtotime($sched_date . '+' .  $rule . ' weeks')));
            $end_year = (int) Date("Y", strtotime($sched_date . '+' .  $rule . ' weeks'));
            $end_week = (int) Date("W", strtotime($sched_date . '+' .  $rule . ' weeks'));
            $end_date->setISODate($end_year,  $end_week)->format('Y-m-d');
            $date['end'] =  $end_date->modify('+6 days')->format('Y-m-d');
        }
        return $date;
        // var_dump($date);
    }

    public function check_existing_IR_same_offense()
    {
        $offenseID =  $this->input->post("offense_id");
        $category_ID =  $this->input->post("category");
        $emp_ID =  $this->input->post("subject_emp_id");
        $incidentDate =   Date("Y-m-d", strtotime($this->input->post("incident_date")));
        $firstlevel =  $this->get_offense_disc_action_by_level_category($offenseID, 1,  $category_ID);
        $month_period =   $firstlevel->periodMonth;
        $start_end_date =  $this->unfixed_prescriptive_period($incidentDate,  $month_period, 'month');
        $ir_action['firstlevel'] =  $firstlevel;
        $ir_action['curedate'] =   $start_end_date['end'];
        // if ($start_end_date['end'] ==  $incidentDate) {
        //     $start_end_coverage =  $this->unfixed_prescriptive_period($incidentDate,  $month_period + 1, 'month');
        //     $ir_action['curedate'] =  $start_end_coverage['end'];
        // }
        echo json_encode($ir_action);
    }

    public function check_existing_IR_same_offense2()
    {
        $offenseID =  $this->input->post("offense_id");
        $category_ID =  $this->input->post("category");
        $emp_ID =  $this->input->post("subject_emp_id");
        $incidentDate =   Date("Y-m-d", strtotime($this->input->post("incident_date")));
        $incidentDate2 =  $this->input->post("incident_date");
        $firstlevel =  $this->get_offense_disc_action_by_level_category($offenseID, 1,  $category_ID);
        $month_period =   $firstlevel->periodMonth;
        $start_end_date =  $this->unfixed_prescriptive_period($incidentDate,  $month_period, 'month');
        $ir_action['firstlevel'] =  $firstlevel;


        $existing_IR =  $this->get_existing_ir($emp_ID, 4, 17,  $offenseID);

        // Check if IR has history 
        if (count($existing_IR) > 0) {
            $prescriptive_id = $existing_IR->prescriptiveId;
            $level =  $existing_IR->level;
            $level++;
            $category_ID =  $existing_IR->disciplineCategory_ID;
            $disciplinary_categset_id =  $existing_IR->disciplinaryActionCategorySettings_ID;
            $check_next =  $this->get_offense_disc_action_by_level($offenseID,  $level,  $disciplinary_categset_id,  $category_ID);

            if ($check_next != null) {
                $ir_action['nextlevel'] =  $check_next;
                $month_period =  $check_next->periodMonth;
            } else {
                $max =  $this->get_max_offense_disc_action_by_level($offenseID,  $category_ID);
                // var_dump($max);
                $get_max_level =  $max[0]->level;
                $max_nextlevel =  $this->get_offense_disc_action_by_level_category($offenseID,  $get_max_level,  $category_ID);
                $ir_action['nextlevel'] =  $max_nextlevel;
                $month_period =  $max_nextlevel->periodMonth;
            }

            $start_end_date =  $this->unfixed_prescriptive_period($incidentDate,  $month_period, 'month');
            $ir_action['curedate'] =   $start_end_date['end'];
            if ($start_end_date['end'] ==  $incidentDate) {
                $start_end_coverage =  $this->unfixed_prescriptive_period($incidentDate,  $month_period + 1, 'month');
                $ir_action['curedate'] =  $start_end_coverage['end'];
            }

            $ir_action['history_stat'] = 1;
            $ir_action['history'] =  $this->get_ir_history($emp_ID,  $offenseID,  $prescriptive_id);
        }else{
            $ir_action['curedate'] =   $start_end_date['end'];
            $ir_action['history_stat'] = 0;
            $ir_action['history'] = 0;
        }
        // if ($start_end_date['end'] ==  $incidentDate) {
        //     $start_end_coverage =  $this->unfixed_prescriptive_period($incidentDate,  $month_period + 1, 'month');
        //     $ir_action['curedate'] =  $start_end_coverage['end'];
        // }
        echo json_encode($ir_action);
    }

    private function fetch_prescriptive_date($sched_date,  $rule)
    {
        //  $sched_date = '2019-1-29';
        //  $rule = 2;
        $coverage_from_month = Date("Y-m-d", strtotime($sched_date . '-' .  $rule . ' months'));
        $start = new DateTime($coverage_from_month);
        $date['start'] =  $start->format('Y-m') . "-01";
        $end = new DateTime($sched_date);
        $date['end'] =  $end->format('Y-m-t');
        return date;
    }
    function mymap_arrays()
    {
        $args = func_get_args();
        $key = array_shift($args);
        return array_combine($key, $args);
    }
    public function save_specialIR()
    {
        $witnesses =  explode(",", $this->input->post('witnesses'));
        $evidences = $_FILES['upload_evidence_files'];

        $termination =  $this->input->post('termination');
        $filingtype =  $this->input->post('ir_filingtype');
        $data['incidentDate'] =  date_format(date_create($this->input->post('date_ofincident')), "Y-m-d");
        $data['incidentTime'] =  $this->input->post('time_ofincident');
        $data['place'] =  $this->input->post('place');
        $data['expectedAction'] =  $this->input->post('courseaction');
        $data['subjectEmp_ID'] =  $this->input->post('empid_subj');
        //  $sourceEmp_ID= $this->session->emp_id;
        $data['subjectPosition'] =  $this->input->post('position');
        $data['subjectEmpStat'] =  $this->input->post('emp_stat');
        $data['acc_id'] =  $this->input->post('acc_id');
        $data['empType'] =  $this->input->post('emp_type');
        $data['subjectAccount'] =  $this->input->post('account');
        //  $dateTimeFiled= $this->get_current_date_time();
        $data['details'] =  $this->input->post('description');
        $data['periodUnit'] = "month";
        // if filed IR has pending or ongoing IR
        if ($filingtype == "2") {
            // prescriptive id is null
            $data['prescriptiveStat'] = "27";
            $data['liabilityStat'] = "27";
            $data['incidentReportStages_ID'] = "4";
            $data['occurence'] = "0";
        } else if ($filingtype == "1") {
            // if filed IR is first level
            // get maximum prescriptive id
            $data['prescriptiveId'] =  $this->qry_max_prescriptive_id() + 1;
            $data['prescriptiveStat'] = "2";
            $data['liabilityStat'] = "2";
            $data['incidentReportStages_ID'] = "1";
            $data['occurence'] = "1";
            $data['prescriptionEnd'] =  $this->input->post('prescription_end');
            $data['periodNum'] =  $this->input->post('period_num');
        } else {
            // if filed IR has IR history
            $data['prescriptiveId'] =  $this->input->post('prescriptionid');
            $data['prescriptiveStat'] = "2";
            $data['liabilityStat'] = "2";
            $data['incidentReportStages_ID'] = "1";
            $occurence =  $this->input->post('occurence');
            $int_occu =  $occurence + 1;
            $data['occurence'] =  $int_occu;
            $data['prescriptionEnd'] =  $this->input->post('prescription_end');
            $data['periodNum'] =  $this->input->post('period_num');
        }
        //  $datedLiabilityStat= $this->get_current_date_time();
        $data['offense_ID'] =  $this->input->post('offense_id');
        $data['disciplinaryActionCategorySettings_ID'] =  $this->input->post('disActioncategorysettingsid');
        $data['disciplinaryActionCategory_ID'] =  $this->input->post('disActioncategoryid');
        $data['finaldisciplinaryActionCategory_ID'] =  $this->input->post('disActioncategoryid');
        $data['offense'] =  $this->input->post('offense_desc');
        $data['incidentReportFilingType_ID'] = "1";
        $data['incidentReportType_ID'] =  $this->input->post('incident_type');
        // var_dump($data);
        // INSERTING OF INCIDENT REPORT DATA
        $inserted_ir =  $this->insert_incident_report($data,  $witnesses, $evidences);

        if ($witnesses[0] !== "") {
            $notif_specialir['with_witness'] = 1;
            $notif_specialir['notify_wit'] = $inserted_ir['notify_witnesses'];
            $notif_specialir['with_subject'] = 0;
            // else{
            //         $notif_specialir['notify_sub'] = $inserted_ir['notify_subject'];
            //         $notif_specialir['with_subject'] = 1;
            // }
        } else {
            $notif_specialir['with_witness'] = 0;
            if($data['liabilityStat'] !== '27'){
                $notif_specialir['notify_sub'] = $inserted_ir['notify_subject'];
                $notif_specialir['with_subject'] = 1;
            }
        }
        if ($filingtype == "2") {
            $notif_specialir['with_subject'] = 0;
            $annex['attachedIncidentReport_ID'] =  $inserted_ir['incident_report_id'];
            $annex['incidentReport_ID'] =  $this->input->post('irid_pending');
            $this->insert_incident_report_annex($annex);
        }

        echo json_encode($notif_specialir);
    }
    public function add_additionalEvidences($irid)
    {
        $dateTime = $this->get_current_date_time();
        $evidences = $_FILES['upload_evidence_files'];
        if ($evidences['name']['0'] !== "") {
            $this->upload_evidences($irid, $evidences, $dateTime['dateTime'], 4);
            $ok = 1;
        } else {
            $ok = 0;
        }
        echo json_encode($ok);
    }
    private function report_deadline_settings()
    {
        $q_recommend = "SELECT deadlineToRecommendOption FROM tbl_dms_incident_report_deadline_settings";
        $a =  $this->general_model->custom_query($q_recommend);
        return  $a;
    }
    private function insert_incident_report_annex($data)
    {
        $dateTime =  $this->get_current_date_time();
        $data['dateCreated'] =  $dateTime['dateTime'];
        $this->general_model->insert_vals($data, 'tbl_dms_incident_report_annex');
    }
    public function fetch_report_evidences($irid)
    {
        $et = $this->input->post("evidencetype");
        $ap = $this->input->post("purpose");
        if ($et == 1) {
            $evidencetype = "image/";
        } else if ($et == 2) {
            $evidencetype = "video/";
        } else if ($et == 3) {
            $evidencetype = "audio/";
        } else if ($et == 4) {
            $evidencetype = "application/";
        }
         else if ($et == 5) {
        $evidencetype = "document";
        }
         else {
            $evidencetype = null;
        }
        $evidences['display'] = $this->report_evidences($irid, 4, $evidencetype, $ap);
        $evidences['select_purposes'] = $this->attachment_purposes($irid);
        echo json_encode($evidences);
    }
    // public function fetch_irall_evidences($irid){
    //     $et = $this->input->post("evidencetype");
    //     $ap = $this->input->post("purpose");
    //     if ($et == 1) {
    //         $evidencetype = "image/";
    //     } else if ($et == 2) {
    //         $evidencetype = "video/";
    //     } else if ($et == 3) {
    //         $evidencetype = "audio/";
    //     } else if ($et == 4) {
    //         $evidencetype = "application/";
    //     } else {
    //         $evidencetype = null;
    //     }
    //     $evidences['display'] = $this->report_evidences($irid, 2, $evidencetype, $ap);
    //     $evidences['select_purposes'] = $this->attachment_purposes($irid);
    //     echo json_encode($evidences);
    // }
    public function fetch_purposes($irid)
    {
        $purposes = $this->attachment_purposes($irid);
        echo json_encode($purposes);
    }
    private function report_evidences($irid, $purpose_type, $evidencetype = null, $purpose)
    {
        $append = ($evidencetype != null) ? " AND ir.mediaType like '%$evidencetype%'" : "";
        $append2 = ($purpose != null) ? " AND ir.incidentReportAttachmentPurpose_ID like '%$purpose%'" : "";
        // $append2 = ($evidencetype2 != null) ? " OR mediaType like '%$evidencetype2%'" : "";
        $fields = "ir.ir_attachment_ID,ir.link,ir.incidentReportAttachmentPurpose_ID, ir.mediaType, attach.purpose";
        if ($purpose_type == 4) {
            $where = "attach.incidentReportAttachmentPurpose_ID=ir.incidentReportAttachmentPurpose_ID AND ir.incidentReport_ID=$irid $append $append2";
        } else {
            $where = "ir.incidentReportAttachmentPurpose_ID=$purpose_type AND ir.incidentReport_ID=$irid";
        }
        return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_dms_incident_report_attachment ir,tbl_dms_incident_report_attachment_purpose attach");
    }
    private function attachment_purposes($irid)
    {
        $query = "SELECT ir.ir_attachment_ID,ir.link,ir.incidentReportAttachmentPurpose_ID, ir.mediaType, attach.purpose FROM tbl_dms_incident_report_attachment ir,tbl_dms_incident_report_attachment_purpose attach WHERE ir.incidentReport_ID=$irid AND ir.incidentReportAttachmentPurpose_ID=attach.incidentReportAttachmentPurpose_ID GROUP BY ir.incidentReportAttachmentPurpose_ID";
        return $result = $this->general_model->custom_query($query);
    }
    public function remove_attachment()
    {
        $where['ir_attachment_ID'] = $this->input->post('ir_attachmentid');
        $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_incident_report_attachment');
        echo json_encode($delete_stat);
    }
    public function remove_memo()
    {
        $where['dmsMemo_ID'] = $this->input->post('memo_id');
        $delete_stat =  $this->general_model->delete_vals($where, 'tbl_dms_memo');
        echo json_encode($delete_stat);
    }
    public function save_memo()
    {
        $dateTime = $this->get_current_date_time();
        $path = 'uploads/dms/memo/';
        $name = $_FILES['upload_memo']['name'];
        $temp = $_FILES['upload_memo']['tmp_name'];
        $final_name = pathinfo($name, PATHINFO_FILENAME) . time() . "." . pathinfo($name, PATHINFO_EXTENSION);
        $new_path = $path . strtolower($final_name);
        if (move_uploaded_file($temp, $new_path)) {
            $data['link'] = $new_path;
        }
        $data['uploadedBy'] = $this->session->userdata('emp_id');
        $data['dateUploaded'] = $dateTime['dateTime'];
        $data['title'] =  $this->input->post('memotitle');
        $data['number'] =  $this->input->post('memonumber');
        $data['memoDate'] =  $this->input->post('memodate');
        $this->general_model->insert_vals($data, "tbl_dms_memo");
    }
    public function list_memo_datatable()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "SELECT dmsMemo_ID,title, number,memoDate,dateUploaded,link FROM tbl_dms_memo";

        if ($datatable['query']['search_memoid'] != '' || $datatable['query']['search_date'] != '') {
            $keyword = $datatable['query']['search_memoid'];
            $keyword2 = $datatable['query']['search_date'];
            $query['search']['append'] = " WHERE (number LIKE '%" . $keyword . "%' OR title LIKE '%" . $keyword . "%') AND memoDate LIKE '%" . $keyword2 . "%'";
            $query['search']['total'] = " WHERE (number LIKE '%" . $keyword . "%' OR title LIKE '%" . $keyword . "%') AND memoDate LIKE '%" . $keyword2 . "%'";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function list_irero_datatable()
    {
        $emp_id = $this->session->emp_id;
        $datatable = $this->input->post('datatable');
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];
        $et = $datatable['query']['emp_type'];
        $lt = $datatable['query']['liability_type'];
        $rt = $datatable['query']['report_type'];
        $subj = $datatable['query']['subject'];
        $off = $datatable['query']['offense'];
        $offn = $datatable['query']['offense_nature'];

        $emptype_where = "";
        $lia_where = "";
        $reporttype_where = "";
        $subject_where = "";
        $offense_where = "";
        $offensenature_where = "";

        if ($et !== null && $et !== "") {
            //  $etype="";
            //  if($et==1){
            //      $etype="Admin";
            //  }else if($et==2){
            //      $etype="Agent";
            //  }
            $emptype_where = "AND ir.empType LIKE '%" . $et . "%'";
        }
        if ((int) $lt !== 0) {
            $lia_where = "AND ir.liabilityStat = $lt";
        }
        if ((int) $rt !== 0) {
            $reporttype_where = "AND ir.incidentReportType_ID = $rt";
        }
        if ((int) $subj !== 0) {
            $subject_where = "AND ir.subjectEmp_ID = $subj";
        }
        if ((int) $off !== 0) {
            $offense_where = "AND off.offense_ID = $off";
        }
        if ((int) $offn !== 0) {
            $offensenature_where = "AND offType.offenseType_ID = $offn";
        }
        $query['query'] = "SELECT DISTINCT(ir.incidentReport_ID), ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin, tbl_dms_incident_report_recommendation rec WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18) AND ir.incidentReport_ID = rec.incidentReport_ID AND rec.emp_ID = $emp_id AND (rec.incidentReportRecommendationType_ID = 2 OR rec.incidentReportRecommendationType_ID = 3) AND DATE(ir.incidentDate) >= '$start_date' AND DATE(ir.incidentDate) <= '$end_date' " . $lia_where . " " . $reporttype_where . " " . $subject_where . " " . $offense_where . " " . $offensenature_where . " " . $emptype_where;
        if ($datatable['query']['re_irid'] != '') {
            $keyword = $datatable['query']['re_irid'];
            $where = "ir.incidentReport_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        // var_dump($query);
        // echo $this->db->last_query();

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function list_irpending_datatable()
    {
        $emp_id = $this->session->emp_id;
        $datatable = $this->input->post('datatable');
        $subj = $datatable['query']['subject_id'];
        $acc = $datatable['query']['account_id'];
        $et = $datatable['query']['empall'];
        $rt = $datatable['query']['reptype'];
        $off = $datatable['query']['off_all'];
        $offn = $datatable['query']['off_t'];
        $access = $datatable['query']['access'];

        $subject_where = "";
        $acc_where = "";
        $offense_where = "";
        $offensenature_where = "";
        $emptype_where = "";
        $reporttype_where = "";
        $access_where = "";

        if ((int) $subj !== 0) {
            $subject_where = "AND ir.subjectEmp_ID = $subj";
        }
        if ((int) $acc !== 0) {
            $acc_where = "AND ir.acc_id = $acc";
        }
        if ((int) $off !== 0) {
            $offense_where = "AND off.offense_ID = $off";
        }
        if ((int) $offn !== 0) {
            $offensenature_where = "AND offType.offenseType_ID = $offn";
        }
        if ($et != '0') {
            $emptype_where = "AND ir.empType = '" . $et . "'";
        }
        if ((int) $rt !== 0) {
            $reporttype_where = "AND ir.incidentReportType_ID = $rt";
        }

        // $query['query'] = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.liabilityStat=2 AND ir.prescriptiveStat=2 " . $subject_where . " " . $acc_where;
        $query['query'] = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.liabilityStat=2 AND ir.prescriptiveStat=2 " . $subject_where . " " . $acc_where. " " . $offense_where . " " . $offensenature_where. " " . $emptype_where. " " . $reporttype_where;
        if ($datatable['query']['search_irid'] != '') {
            $keyword = $datatable['query']['search_irid'];
            $where = "ir.incidentReport_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        // $a = $this->db->last_query();
        // var_dump($a);
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function list_irall_datatable()
    {
        $datatable = $this->input->post('datatable');
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];
        $et = $datatable['query']['empall'];
        $lt = $datatable['query']['liaall'];
        $rt = $datatable['query']['reptype'];
        $acc = $datatable['query']['acc'];
        $empsub = $datatable['query']['empsub'];
        $off = $datatable['query']['off_all'];
        $offn = $datatable['query']['off_t'];
        $access = $datatable['query']['access'];

        $emptype_where = "";
        $lia_where = "";
        $reporttype_where = "";
        $acc_where = "";
        $subject_where = "";
        $offense_where = "";
        $offensenature_where = "";
        $daterange = "";
        $access_where = "";

        if ($et != '0') {
            $emptype_where = "AND ir.empType = '" . $et . "'";
        }
        if ((int) $lt !== 0) {
            $lia_where = "AND ir.liabilityStat = $lt";
        }
        if ((int) $rt !== 0) {
            $reporttype_where = "AND ir.incidentReportType_ID = $rt";
        }
        if ((int) $off !== 0) {
            $offense_where = "AND off.offense_ID = $off";
        }
        if ((int) $offn !== 0) {
            $offensenature_where = "AND offType.offenseType_ID = $offn";
        }
        if ((int) $acc !== 0) {
            $acc_where = "AND ir.acc_id =  $acc";
        }
        if ((int) $empsub !== 0) {
            $subject_where = "AND ir.subjectEmp_ID = $empsub";
        }    

        $query['query'] = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount,ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18 OR ir.liabilityStat=2) AND DATE(ir.incidentDate) >= '$start_date' AND DATE(ir.incidentDate) <= '$end_date' " . $emptype_where . " " . $lia_where . " " . $reporttype_where . " " . $acc_where . " " . $subject_where . " " . $offense_where . " " . $offensenature_where;


        if ($datatable['query']['re_irid'] != '') {
            $keyword = $datatable['query']['re_irid'];
            $where = "ir.incidentReport_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function list_irsusterm_datatable()
    {
        $datatable = $this->input->post('datatable');
        $sub = $datatable['query']['subjectname'];
        $acc = $datatable['query']['ir_account'];
        $da = $datatable['query']['discaction'];
        $access = $datatable['query']['access'];

        $acc_where = "";
        $subject_where = "";
        $disact_where = "";
        $access_where = "";

        if ((int) $acc !== 0) {
            $acc_where = "AND ir.acc_id =  $acc";
        }
        if ((int) $sub !== 0) {
            $subject_where = "AND ir.subjectEmp_ID = $sub";
        }
        if ($da !== null && $da !== "") {
            $disact_where = "AND dmsActionFin.label LIKE '%" . $da . "%'";
        }
        // if ($access!== null && $access !== "") {
        //     $access_where = "AND ir.empType LIKE '%" . $access . "%'";
        // }
        
        if ($access != '0') {
            $access_where = "AND ir.empType = '" . $access . "'";
        }

        $groupbysyn = "GROUP BY ir.incidentReport_ID ";
        $query['query'] = "SELECT ir.incidentReport_ID,ir.incidentDate,ir.liabilityStat,sup.suspensionTerminationDays_ID,sup.days,supdate.suspensionTerminationDays_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, dmsActionFin.action finalAction, dmsActionFin.label, supdate.date, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_dms_suspension_termination_days sup, tbl_applicant subApp, tbl_employee subEmp, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin, tbl_dms_suspension_dates supdate WHERE sup.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND sup.suspensionTerminationDays_ID=supdate.suspensionTerminationDays_ID AND dmsActionFin.label IN ('s','t') " . $subject_where . " " . $acc_where . " " . $disact_where . " " .$access_where. " " . $groupbysyn;

        if ($datatable['query']['ir_recid'] != '') {
            $keyword = $datatable['query']['ir_recid'];
            $where = "ir.incidentReport_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }

        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function list_irpendingUploads_datatable()
    {
        $datatable = $this->input->post('datatable');
        $sub = $datatable['query']['subjectname'];
        $ero = $datatable['query']['eroname'];
        $req = $datatable['query']['req_erofile'];
        // $irrid = $datatable['query']['ir_recid'];
        $ero_where = "";
        $subject_where = "";
        $req_where = "";

        // if ((int) $acc !== 0) {
        //     $acc_where = "AND ir.acc_id =  $acc";
        // }
        if ((int) $sub !== 0) {
            $subject_where = "AND ir.subjectEmp_ID = $sub";
        }
        if ((int) $ero !== 0) {
            $ero_where = "AND docu.eroEmp_ID = $ero";
        }
        if ((int) $req !== 0) {
            $req_where = "AND docu.incidentReportAttachmentPurpose_ID = $req";
        }
        $query['query'] = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.dateTimeFiled, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID sourceEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname, docu.eroEmp_ID, repAttach.purpose,docu.incidentReportAttachmentPurpose_ID FROM tbl_dms_incident_report ir, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_necessary_document docu, tbl_dms_incident_report_attachment_purpose repAttach, tbl_applicant eroApp, tbl_employee eroEmp WHERE subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18) AND docu.incidentReport_ID = ir.incidentReport_ID AND docu.status_ID=2 AND docu.incidentReportAttachmentPurpose_ID = repAttach.incidentReportAttachmentPurpose_ID AND docu.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid " . $subject_where . " " . $ero_where . " " . $req_where;

        if ($datatable['query']['ir_recuploadid'] != '') {
            $keyword = $datatable['query']['ir_recuploadid'];
            $where = "ir.incidentReport_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }

        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function list_irpendingRecommendation_datatable()
    {
        $datatable = $this->input->post('datatable');
        $sub = $datatable['query']['subjectname'];
        $ero = $datatable['query']['eroname'];
        $req = $datatable['query']['reco_erofile'];
        $ero_where = "";
        $subject_where = "";
        $req_where = "";

        if ((int) $sub !== 0) {
            $subject_where = "AND ir.subjectEmp_ID = $sub";
        }
        if ((int) $ero !== 0) {
            $ero_where = "AND penRecom.emp_id = $ero";
        }
        if ((int) $req !== 0) {
            $req_where = "AND penRecom.incidentReportRecommendationType_ID = $req";
        }
        
        $query['query'] = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.dateTimeFiled, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID sourceEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname, penRecom.emp_id, penRecomtype.type, penRecom.liabilityStat_ID,penRecomtype.incidentReportRecommendationType_ID FROM tbl_dms_incident_report ir, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_applicant eroApp, tbl_employee eroEmp, tbl_dms_incident_report_recommendation penRecom, tbl_dms_incident_report_recommendation_type penRecomtype WHERE subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND penRecom.liabilityStat_ID=4 AND penRecom.incidentReportRecommendationType_ID= penRecomtype.incidentReportRecommendationType_ID AND (penRecomtype.incidentReportRecommendationType_ID = 2 OR penRecomtype.incidentReportRecommendationType_ID = 3) AND eroEmp.apid = eroApp.apid AND penRecom.incidentReport_ID=ir.incidentReport_ID AND penRecom.emp_ID = eroEmp.emp_id AND ir.liabilityStat=2 " . $subject_where . " " . $ero_where . " " . $req_where;
        
        if ($datatable['query']['ir_recpendingid'] != '') {
            $keyword = $datatable['query']['ir_recpendingid'];
            $where = "ir.incidentReport_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function list_irmissedDeadline_datatable()
    {
        $datatable = $this->input->post('datatable');
        $sub = $datatable['query']['subjectname'];
        $ero = $datatable['query']['eroname'];
        $task = $datatable['query']['task'];
        
        $ero_where = "";
        $subject_where = "";
        $task_where = "";

        if ((int) $sub !== 0) {
            $subject_where = "AND ir.subjectEmp_ID = $sub";
        }
        if ((int) $ero !== 0) {
            $ero_where = "AND erom.eroEmp_ID = $ero";
        }
        if ((int) $task !== 0) {
            $task_where = "AND dlt.dmsDeadlineType_ID = $task";
        }
        
        // $query['query'] = "SELECT ir.incidentReport_ID,purpose.purpose as a_purpose ,ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_necessary_document dmsNece, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp,tbl_dms_incident_report_attachment_purpose purpose WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsNece.irNecessaryDocument_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND dl.dmsDeadlineType_ID=7 AND dmsNece.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid AND purpose.incidentReportAttachmentPurpose_ID = dmsNece.incidentReportAttachmentPurpose_ID GROUP BY ir.subjectEmp_ID ". $subject_where . " " . $ero_where . " " . $task_where . " UNION SELECT ir.incidentReport_ID,dmsRecom.notes as a_purpose,ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_incident_report_recommendation dmsRecom, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsRecom.incidentReportRecommendation_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND (dl.dmsDeadlineType_ID=4 OR dl.dmsDeadlineType_ID=5) AND dmsRecom.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid " . $subject_where . " " . $ero_where . " " . $task_where;
        $query['query'] = "SELECT ir.incidentReport_ID, purpose.purpose as a_purpose, ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_necessary_document dmsNece, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp, tbl_dms_incident_report_attachment_purpose purpose WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsNece.irNecessaryDocument_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND dl.dmsDeadlineType_ID=7 AND dmsNece.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid AND purpose.incidentReportAttachmentPurpose_ID = dmsNece.incidentReportAttachmentPurpose_ID " . $subject_where . " " . $ero_where . " " . $task_where . " UNION SELECT ir.incidentReport_ID, dmsRecom.notes as a_purpose, ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_incident_report_recommendation dmsRecom, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsRecom.incidentReportRecommendation_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND (dl.dmsDeadlineType_ID=4 OR dl.dmsDeadlineType_ID=5) AND dmsRecom.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid " .$subject_where . " " . $ero_where . " " . $task_where;
        
        if ($datatable['query']['ir_idmissed'] != '') {
            $keyword = $datatable['query']['ir_idmissed'];
            $where = "ir.incidentReport_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function access_ir_details_ero()
    {
        $irid = $this->input->post('irid');
        $ir_details = $this->get_layouted_ir_details($irid, 1, 1, 1, 1, 1, 1);
        echo json_encode($ir_details);
    }
    public function ero_recordsfilter()
    {
        $emp_id = $this->session->emp_id;
        $sub = "SELECT DISTINCT(ir.incidentReport_ID), ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin, tbl_dms_incident_report_recommendation rec WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18) AND ir.incidentReport_ID = rec.incidentReport_ID AND rec.emp_ID = $emp_id AND (rec.incidentReportRecommendationType_ID = 2 OR rec.incidentReportRecommendationType_ID = 3) GROUP BY ir.subjectEmp_ID ASC";
        $off = "SELECT DISTINCT(ir.incidentReport_ID), ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin, tbl_dms_incident_report_recommendation rec WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18) AND ir.incidentReport_ID = rec.incidentReport_ID AND rec.emp_ID = $emp_id AND (rec.incidentReportRecommendationType_ID = 2 OR rec.incidentReportRecommendationType_ID = 3) GROUP BY off.offense_ID ASC";
        $oft = "SELECT DISTINCT(ir.incidentReport_ID), ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin, tbl_dms_incident_report_recommendation rec WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18) AND ir.incidentReport_ID = rec.incidentReport_ID AND rec.emp_ID = $emp_id AND (rec.incidentReportRecommendationType_ID = 2 OR rec.incidentReportRecommendationType_ID = 3) GROUP BY offType.offenseType_ID";
        $filter_details['offense'] = $this->general_model->custom_query($off);
        $filter_details['offense_type'] = $this->general_model->custom_query($oft);
        $filter_details['subject'] = $this->general_model->custom_query($sub);
        echo json_encode($filter_details);
    }
    public function allir_recordsfilter()
    {
        $tab = $this->input->post('tab');
        $access = $this->input->post('access');
        $toAccess = "";

        if ($access != '0') {
            $toAccess = "AND ir.empType = '" . $access . "'";
        }

        $pen_sub = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.liabilityStat=2 AND ir.prescriptiveStat=2 ".  $toAccess. " " . "GROUP BY ir.subjectEmp_ID ORDER BY subApp.lname ASC";
        $pen_acc = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.liabilityStat=2 AND ir.prescriptiveStat=2 ".  $toAccess. " " . "GROUP BY ir.acc_id ORDER BY ir.subjectAccount ASC";
        $pen_offense = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount,ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.liabilityStat=2 ".  $toAccess. " " . " GROUP BY off.offense_ID ORDER BY off.offense ASC";
        $pen_offenseT = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount,ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.liabilityStat=2 ".  $toAccess. " " . " GROUP BY offType.offenseType_ID ORDER BY offType.offenseType ASC";

        $all_sub = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount,ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18 OR ir.liabilityStat=2) ".  $toAccess. " " . " GROUP BY ir.subjectEmp_ID ORDER BY subApp.lname ASC";
        $all_acc = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount,ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18 OR ir.liabilityStat=2) ".  $toAccess. " " . " GROUP BY ir.acc_id ORDER BY ir.subjectAccount ASC";
        $all_offense = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount,ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18 OR ir.liabilityStat=2) ".  $toAccess. " " . " GROUP BY off.offense_ID ORDER BY off.offense ASC";
        $all_offenseT = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount,ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.incidentReportType_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.offenseType_ID, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18 OR ir.liabilityStat=2) ".  $toAccess. " " . " GROUP BY offType.offenseType_ID ORDER BY offType.offenseType ASC";

        $sus_term = "SELECT ir.incidentReport_ID,ir.incidentDate, sup.suspensionTerminationDays_ID,sup.days,supdate.suspensionTerminationDays_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, dmsActionFin.action finalAction, supdate.date FROM tbl_dms_incident_report ir, tbl_dms_suspension_termination_days sup, tbl_applicant subApp, tbl_employee subEmp, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin, tbl_dms_suspension_dates supdate WHERE sup.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND sup.suspensionTerminationDays_ID=supdate.suspensionTerminationDays_ID AND dmsActionFin.label IN ('s','t') " .  $toAccess;
        $susterm_acc = "SELECT ir.acc_id, ir.subjectAccount, ir.incidentReport_ID,ir.incidentDate, sup.suspensionTerminationDays_ID,sup.days,supdate.suspensionTerminationDays_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, dmsActionFin.action finalAction, dmsActionFin.label, supdate.date, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_dms_suspension_termination_days sup, tbl_applicant subApp, tbl_employee subEmp, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin, tbl_dms_suspension_dates supdate WHERE sup.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND sup.suspensionTerminationDays_ID=supdate.suspensionTerminationDays_ID AND dmsActionFin.label IN ('s','t') ".  $toAccess. " " . " GROUP BY ir.acc_id ORDER BY ir.subjectAccount ASC";
        $susterm_sub = "SELECT ir.incidentReport_ID,ir.incidentDate,ir.subjectEmp_ID, sup.suspensionTerminationDays_ID,sup.days,supdate.suspensionTerminationDays_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, dmsActionFin.action finalAction, dmsActionFin.label, supdate.date, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_dms_suspension_termination_days sup, tbl_applicant subApp, tbl_employee subEmp, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin, tbl_dms_suspension_dates supdate WHERE sup.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND sup.suspensionTerminationDays_ID=supdate.suspensionTerminationDays_ID AND dmsActionFin.label IN ('s','t') ".  $toAccess. " " . " GROUP BY ir.subjectEmp_ID ORDER BY subApp.lname ASC";


        $filter_details['pending_subject'] = $this->general_model->custom_query($pen_sub);
        $filter_details['pending_account'] = $this->general_model->custom_query($pen_acc);
        $filter_details['pending_offense'] = $this->general_model->custom_query($pen_offense);
        $filter_details['pending_offenseType'] = $this->general_model->custom_query($pen_offenseT);
        $filter_details['all_subject'] = $this->general_model->custom_query($all_sub);
        $filter_details['all_acc'] = $this->general_model->custom_query($all_acc);
        $filter_details['all_offense'] = $this->general_model->custom_query($all_offense);
        $filter_details['all_offenseType'] = $this->general_model->custom_query($all_offenseT);
        $filter_details['susterm'] = $this->general_model->custom_query($sus_term);
        $filter_details['susterm_account'] = $this->general_model->custom_query($susterm_acc);
        $filter_details['susterm_subject'] = $this->general_model->custom_query($susterm_sub);
        echo json_encode($filter_details);
    }
    public function eropending_recordsfilter()
    {
        // $emp_id = $this->session->emp_id;
        $penUpload_sub = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.dateTimeFiled, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID sourceEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname, docu.eroEmp_ID, repAttach.purpose,docu.incidentReportAttachmentPurpose_ID FROM tbl_dms_incident_report ir, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_necessary_document docu, tbl_dms_incident_report_attachment_purpose repAttach, tbl_applicant eroApp, tbl_employee eroEmp WHERE subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18) AND docu.incidentReport_ID = ir.incidentReport_ID AND docu.status_ID=2 AND docu.incidentReportAttachmentPurpose_ID = repAttach.incidentReportAttachmentPurpose_ID AND docu.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid GROUP BY ir.subjectEmp_ID ORDER BY subApp.lname ASC";
        $penUpload_ero = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.dateTimeFiled, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID sourceEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname, docu.eroEmp_ID, repAttach.purpose,docu.incidentReportAttachmentPurpose_ID FROM tbl_dms_incident_report ir, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_necessary_document docu, tbl_dms_incident_report_attachment_purpose repAttach, tbl_applicant eroApp, tbl_employee eroEmp WHERE subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18) AND docu.incidentReport_ID = ir.incidentReport_ID AND docu.status_ID=2 AND docu.incidentReportAttachmentPurpose_ID = repAttach.incidentReportAttachmentPurpose_ID AND docu.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid GROUP BY docu.eroEmp_ID ORDER BY eroApp.lname ASC";
        
        $penRecommend_sub = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.dateTimeFiled, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID sourceEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,penRecomtype.type, penRecom.liabilityStat_ID,penRecomtype.incidentReportRecommendationType_ID FROM tbl_dms_incident_report ir, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_applicant eroApp, tbl_employee eroEmp, tbl_dms_incident_report_recommendation penRecom, tbl_dms_incident_report_recommendation_type penRecomtype WHERE subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND penRecom.liabilityStat_ID=4 AND penRecom.incidentReportRecommendationType_ID= penRecomtype.incidentReportRecommendationType_ID AND (penRecomtype.incidentReportRecommendationType_ID = 2 OR penRecomtype.incidentReportRecommendationType_ID = 3) AND eroEmp.apid = eroApp.apid AND penRecom.incidentReport_ID=ir.incidentReport_ID AND penRecom.emp_ID = eroEmp.emp_id AND ir.liabilityStat=2 GROUP BY ir.subjectEmp_ID ORDER BY subApp.lname ASC";
        $penRecommend_ero = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.dateTimeFiled, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID sourceEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,penRecomtype.type,penRecom.emp_ID empEro_id, penRecom.liabilityStat_ID,penRecomtype.incidentReportRecommendationType_ID FROM tbl_dms_incident_report ir, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_applicant eroApp, tbl_employee eroEmp, tbl_dms_incident_report_recommendation penRecom, tbl_dms_incident_report_recommendation_type penRecomtype WHERE subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND penRecom.liabilityStat_ID=4 AND penRecom.incidentReportRecommendationType_ID= penRecomtype.incidentReportRecommendationType_ID AND (penRecomtype.incidentReportRecommendationType_ID = 2 OR penRecomtype.incidentReportRecommendationType_ID = 3) AND eroEmp.apid = eroApp.apid AND penRecom.incidentReport_ID=ir.incidentReport_ID AND penRecom.emp_ID = eroEmp.emp_id AND ir.liabilityStat=2 GROUP BY penRecom.emp_ID ORDER BY subApp.lname ASC";
        
        
        $missedd_sub = "SELECT ir.incidentReport_ID,ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_necessary_document dmsNece, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsNece.irNecessaryDocument_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND dl.dmsDeadlineType_ID=7 AND dmsNece.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid GROUP BY ir.subjectEmp_ID UNION SELECT ir.incidentReport_ID,ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_incident_report_recommendation dmsRecom, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsRecom.incidentReportRecommendation_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND (dl.dmsDeadlineType_ID=4 OR dl.dmsDeadlineType_ID=5) AND dmsRecom.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid GROUP BY ir.subjectEmp_ID";
        $missedd_ero = "SELECT ir.incidentReport_ID,ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_necessary_document dmsNece, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsNece.irNecessaryDocument_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND dl.dmsDeadlineType_ID=7 AND dmsNece.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid GROUP BY erom.eroEmp_ID UNION SELECT ir.incidentReport_ID,ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_incident_report_recommendation dmsRecom, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsRecom.incidentReportRecommendation_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND (dl.dmsDeadlineType_ID=4 OR dl.dmsDeadlineType_ID=5) AND dmsRecom.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid GROUP BY erom.eroEmp_ID";
        $missedd_task = "SELECT ir.incidentReport_ID,ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_necessary_document dmsNece, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsNece.irNecessaryDocument_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND dl.dmsDeadlineType_ID=7 AND dmsNece.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid GROUP BY dlt.dmsDeadlineType_ID UNION SELECT ir.incidentReport_ID,ir.incidentDate,ir.subjectEmp_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic,eroApp.fname eroFname, eroApp.mname eroMname,eroApp.lname eroLname,erom.eroEmp_ID, dl.dmsDeadline_ID,dl.deadline, dl.record_ID,dl.dmsDeadlineType_ID, dlt.typeDescription FROM tbl_dms_incident_report ir,tbl_dms_incident_report_recommendation dmsRecom, tbl_dms_ero_missed_deadlines erom,tbl_dms_deadline dl,tbl_dms_deadline_type dlt, tbl_applicant subApp, tbl_employee subEmp,tbl_applicant eroApp, tbl_employee eroEmp WHERE dl.dmsDeadline_ID=erom.dmsDeadline_ID AND dl.record_id=dmsRecom.incidentReportRecommendation_ID AND dl.dmsDeadlineType_ID=dlt.dmsDeadlineType_ID AND (dl.dmsDeadlineType_ID=4 OR dl.dmsDeadlineType_ID=5) AND dmsRecom.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND erom.eroEmp_ID = eroEmp.emp_id AND eroEmp.apid = eroApp.apid GROUP BY dlt.dmsDeadlineType_ID";

        $filter_details['penUpload_subject'] = $this->general_model->custom_query($penUpload_sub);
        $filter_details['penUpload_eroname'] = $this->general_model->custom_query($penUpload_ero);
        $filter_details['penRecom_subject'] = $this->general_model->custom_query($penRecommend_sub);
        $filter_details['penRecom_eroname'] = $this->general_model->custom_query($penRecommend_ero);
        
        $filter_details['penMissedDeadline_subject'] = $this->general_model->custom_query($missedd_sub);
        $filter_details['penMissedDeadline_eroname'] = $this->general_model->custom_query($missedd_ero);
        $filter_details['penMissedDeadline_task'] = $this->general_model->custom_query($missedd_task);
        echo json_encode($filter_details);
    }
    public function get_irattachment_uploaded()
    {
        $irid = $this->input->post('irid');
        $pse = $this->input->post('purpose');
        $ot = "SELECT ir_attachment_ID,link FROM `tbl_dms_incident_report_attachment` WHERE incidentReport_ID=$irid AND incidentReportAttachmentPurpose_ID=$pse";
        $attachment =  $this->general_model->custom_query($ot);
        // var_dump($attachment);
        echo json_encode($attachment);
    }

    public function get_max_ir_approval($ir_ids){
        $query = "SELECT rec.incidentReport_ID, MAX(rec.level) maxLevel FROM tbl_dms_incident_report_recommendation rec WHERE rec.incidentReport_ID IN ($ir_ids) GROUP BY incidentReport_ID";
        return $record = $this->general_model->custom_query($query);
    }
    public function downloadExcel_allir()
    {
        $searchString = "";
        $search = array();
        $incidentDate_start = $this->input->post('incidentDate_start');
        $incidentDate_end = $this->input->post('incidentDate_end');
        $violation_ir_id = $this->input->post('violation_ir_id');
        $violation_class = $this->input->post('violation_class'); //admin agent
        $violation_type = $this->input->post('violation_type'); //liable
        $report_type = $this->input->post('report_type'); //normal qip
        $violation_accounts = $this->input->post('violation_accounts');
        $violation_subject = $this->input->post('violation_subject');
        $violation_offense = $this->input->post('offense');
        $violation_offense_nature = $this->input->post('offense_nature');
        if ($incidentDate_start !== '' &&  $incidentDate_end !== '' && $incidentDate_start != null && $incidentDate_end != null) {
            $search[] = "ir.incidentDate >= '$incidentDate_start' AND ir.incidentDate <= '$incidentDate_end'";
        }
        if ($violation_ir_id !== '' && $violation_ir_id != null) {
            $search[] = "ir.incidentReport_ID=$violation_ir_id";
        }
        if ($report_type !== '' && $report_type != null) {
            $search[] = "ir.incidentReportType_ID='$report_type'";
        }
        if ($violation_class != '0') {
            $search[] = "ir.empType='$violation_class'";
        }
        if ($violation_type !== '' && $violation_type != null) {
            $search[] = "ir.liabilityStat='$violation_type'";
        }
        if ($violation_accounts !== '' && $violation_accounts != null && $violation_accounts != 0) {
            $search[] = "ir.acc_id=$violation_accounts";
        }
        if ($violation_subject  !== '' && $violation_subject != null && $violation_subject != 0) {
            $search[] = "ir.subjectEmp_ID = $violation_subject";
        }
        if ($violation_offense !== '' && $violation_offense != null && $violation_offense != 0) {
            $search[] = "ir.offense_ID='$violation_offense'";
        }
        if ($violation_offense_nature !== '' && $violation_offense_nature != null && $violation_offense_nature != 0) {
            $search[] = "offType.offenseType_ID='$violation_offense_nature'";
        }
        if (!empty($search)) {
            $searchString = " AND " . implode(" AND ", $search);
        }

        $ero_emp_id = $this->session->emp_id;
        // $query = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description filingTypeDescription, ir.incidentReportStages_ID, irStages.description as irstageDescription, ir.subjectEmp_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18) AND ir.acc_id IN (SELECT acc_id FROM `tbl_dms_employee_relations_officer` WHERE emp_id= $ero_emp_id ) $searchString";
        $query = "SELECT termDates.date termDate, term.days,ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, 
        ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, 
        liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, 
        ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description filingTypeDescription, 
        ir.incidentReportStages_ID, irStages.description as irstageDescription, ir.subjectEmp_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, 
        subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, 
        sourceApp.nameExt sourceNameExt, dmsCategSug.category, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, 
        dmsActionFin.action finalAction, dmsActionFin.label
        FROM tbl_dms_incident_report ir 
        INNER JOIN tbl_status prescStat ON prescStat.status_ID = ir.prescriptiveStat 
        INNER JOIN tbl_status liableStat ON liableStat.status_ID = ir.liabilityStat 
        INNER JOIN tbl_dms_incident_report_filing_type irFilingType ON irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID 
        INNER JOIN tbl_dms_incident_report_stages irStages ON irStages.incidentReportStages_ID = ir.incidentReportStages_ID 
        INNER JOIN tbl_employee subEmp ON subEmp.emp_id = ir.subjectEmp_ID
        INNER JOIN tbl_employee sourceEmp ON sourceEmp.emp_id = ir.sourceEmp_ID
        INNER JOIN tbl_applicant sourceApp ON sourceApp.apid = sourceEmp.apid 
        INNER JOIN tbl_applicant subApp ON subApp.apid = subEmp.apid
        INNER JOIN tbl_dms_offense off ON off.offense_ID = ir.offense_ID
        INNER JOIN tbl_dms_offense_type offType ON offType.offenseType_ID = off.offenseType_ID
        INNER JOIN tbl_dms_disciplinary_action_category dmsActionCategSug ON dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID
        INNER JOIN tbl_dms_disciplinary_action_category dmsActionCategFin ON  dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID
        INNER JOIN tbl_dms_disciplinary_action dmsActionSug ON dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID
        INNER JOIN tbl_dms_disciplinary_action dmsActionFin ON dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID
        INNER JOIN tbl_dms_discipline_category dmsCategSug ON dmsCategSug.disciplineCategory_ID = dmsActionCategSug.disciplineCategory_ID
        LEFT JOIN tbl_dms_suspension_termination_days term ON ir.incidentReport_ID=term.incidentReport_ID
        LEFT JOIN tbl_dms_suspension_dates termDates ON termDates.suspensionTerminationDays_ID=term.suspensionTerminationDays_ID
        WHERE (ir.liabilityStat=17 OR ir.liabilityStat=18 OR ir.liabilityStat=2) $searchString GROUP BY ir.incidentReport_ID ORDER BY ir.incidentReport_ID ASC";
        $ir_data = $this->general_model->custom_query($query);

        // var_dump($ir_data);
        // echo $this->db->last_query();
        //EXCEL PROPER
        $this->load->library('PHPExcel', NULL, 'excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('DTR System Violations');
        $this->excel->getActiveSheet()->setShowGridlines(false);
        //------------------------INSERT LOGO-------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(5);  //setOffsetY has no effect
        $objDrawing->setCoordinates('B1');
        $objDrawing->setHeight(80); // logo height
        // $objDrawing->setWidth(320); // logo width
        // $objDrawing->setWidthAndHeight(200,400);
        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('3')->setRowHeight(30);
        //set cell A1 content with some text

        // $this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(FALSE);
        // $this->excel->getActiveSheet()->setCellValue('A1', 'CODE-SZ_IRExport');
        $this->excel->getActiveSheet()->setCellValue('A3', 'IR ID');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Incident Date');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Incident Time');
        $this->excel->getActiveSheet()->setCellValue('D3', 'DateTime Filed');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Maturity (days)');

        $this->excel->getActiveSheet()->setCellValue('F3', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('G3', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('H3', 'Middle Name');
        $this->excel->getActiveSheet()->setCellValue('I3', 'Name Ext');
        $this->excel->getActiveSheet()->setCellValue('J3', 'Employee Type');
        $this->excel->getActiveSheet()->setCellValue('K3', 'Position');
        $this->excel->getActiveSheet()->setCellValue('L3', 'Employee Status');
        $this->excel->getActiveSheet()->setCellValue('M3', 'Account');
        $this->excel->getActiveSheet()->setCellValue('N3', 'Nature of Offense');
        $this->excel->getActiveSheet()->setCellValue('O3', 'Offense');
        $this->excel->getActiveSheet()->setCellValue('P3', 'Class');
        $this->excel->getActiveSheet()->setCellValue('Q3', 'Incident');
        $this->excel->getActiveSheet()->setCellValue('R3', 'Incident Place');
        $this->excel->getActiveSheet()->setCellValue('S3', 'Expected Action');
        $this->excel->getActiveSheet()->setCellValue('T3', 'Cure Date'); //prescriptionEnd
        $this->excel->getActiveSheet()->setCellValue('U3', 'Presciptive Status');
        $this->excel->getActiveSheet()->setCellValue('V3', 'Liability Status');
        $this->excel->getActiveSheet()->setCellValue('W3', 'Date of Liability');
        $this->excel->getActiveSheet()->setCellValue('X3', 'Explanation Date');
        $this->excel->getActiveSheet()->setCellValue('Y3', 'Subject Explanation');
        $this->excel->getActiveSheet()->setCellValue('Z3', 'Type of Filing'); //filingTypeDescription
        $this->excel->getActiveSheet()->setCellValue('AA3', 'IR Stage'); //irstageDescription
        $this->excel->getActiveSheet()->setCellValue('AB3', 'Source First Name');
        $this->excel->getActiveSheet()->setCellValue('AC3', 'Source Middle Name');
        $this->excel->getActiveSheet()->setCellValue('AD3', 'Source Last Name');
        $this->excel->getActiveSheet()->setCellValue('AE3', 'Source Name Extension');
        $this->excel->getActiveSheet()->setCellValue('AF3', 'Suggested Action');
        $this->excel->getActiveSheet()->setCellValue('AG3', 'Final Action');
        $this->excel->getActiveSheet()->setCellValue('AH3', 'Effectivity');
        $emprow = 4;
        $ir_stat = 2;
        $completed_ir = [];
        $ir_approvals = [];
        $max_approvals = [];
        $completed_ir = array_merge(array_filter($ir_data, function ($e) use ($ir_stat) {
                return $e->liabilityStat != $ir_stat;
            }
        ));
        // var_dump($completed_ir);
        if(count($completed_ir) > 0){
            $completed_ir_ids = array_column($completed_ir, 'incidentReport_ID');
            // var_dump($completed_ir_ids);
            $ir_approvals = $this->get_ir_approvers(implode(',', $completed_ir_ids));
            $max_approvals = $this->get_max_ir_approval(implode(',', $completed_ir_ids));
        }
        foreach ($ir_data as $ir) {
            $from = $ir->dateTimeFiled;
            $current_ir_id = $ir->incidentReport_ID;
            $to = "N/A";
            $effectivity = "";
            if($ir->label === 's'){
                $effectivity  = $ir->days;
                $sus_dates = $this->get_suspension_dates($ir->incidentReport_ID);
                if(count($sus_dates) > 0){
                    $date_unit = "day";
                    if(count($sus_dates) > 1){
                        $date_unit .= "s";
                    }
                    $effectivity .= " ".$date_unit;
                    $dates = [];
                    foreach($sus_dates as $row_index=>$row_value){
                        $dates[$row_index] = $row_value->date;
                    }
                    $effectivity .= " (". implode(', ', $dates) .")";
                }
            }else if($ir->label === 't'){
                $effectivity = "Effective on ".date('Y/m/d', strtotime($ir->termDate));
            }
            $finalActionDesc = $ir->finalAction;
            if($ir->liabilityStat == 18){
                $finalActionDesc = ucwords($ir->liabilityStatDesc);
            }
            if($ir->liabilityStat == 2){
                $dateofliability_display = "-";
                $final_aCtion = "-";
                $f_effectivity = "-";
                $dateTime = $this->get_current_date_time();
                $to = $dateTime['dateTime'];
            }else{
                // $ir_approvals
                $specific_max_approvals = [];
                $dateofliability_display = date('Y/m/d H:i', strtotime($ir->datedLiabilityStat));
                $final_aCtion = $finalActionDesc;
                $f_effectivity = $effectivity;
                //Check maturity
                $specific_max_approvals = array_merge(array_filter($max_approvals, function ($e) use ($current_ir_id) {
                        return $e->incidentReport_ID == $current_ir_id;
                    }
                ));
                if(count($specific_max_approvals) > 0){
                    $max_level = $specific_max_approvals[0]->maxLevel;  
                    $completed_ir = array_merge(array_filter($ir_approvals, function ($e) use ($current_ir_id, $max_level) {
                            return (($e->incidentReport_ID == $current_ir_id) && ($e->level == $max_level));
                        }
                    ));
                    $to = $completed_ir[0]->datedLiabilityStat;
                    $dateofliability_display = date('Y/m/d H:i', strtotime($completed_ir[0]->datedLiabilityStat));
                }else{
                    $dateofliability_display = date('Y/m/d H:i', strtotime($ir->datedLiabilityStat));
                }
            }
            $form_str = strtotime($from);
            $to_str = strtotime($to);
            $duration = $this->get_human_time_format($form_str, $to_str, 0);
            $days = $duration['hoursTotal']/24;
            $formatted_days = number_format((float)$days, 2, '.', '');
            if($formatted_days <= 0){
                $formatted_days = 0;
            }
            $this->excel->getActiveSheet()->setCellValue('A' . $emprow, sprintf("%06d", $ir->incidentReport_ID));
            $this->excel->getActiveSheet()->setCellValue('B' . $emprow, date('Y/m/d', strtotime($ir->incidentDate)));
            $this->excel->getActiveSheet()->setCellValue('C' . $emprow, date('H:i', strtotime($ir->incidentTime)));
            $this->excel->getActiveSheet()->setCellValue('D' . $emprow, date('Y/m/d H:i', strtotime($ir->dateTimeFiled)));
                        
            $this->excel->getActiveSheet()->setCellValue('E' . $emprow, $formatted_days);

            $this->excel->getActiveSheet()->setCellValue('F' . $emprow, ucwords($ir->subjectLname));
            $this->excel->getActiveSheet()->setCellValue('G' . $emprow, ucwords($ir->subjectFname));
            $this->excel->getActiveSheet()->setCellValue('H' . $emprow, ucwords($ir->subjectMname));
            $this->excel->getActiveSheet()->setCellValue('I' . $emprow, ucwords($ir->subjectNameExt));
            $this->excel->getActiveSheet()->setCellValue('J' . $emprow, ucwords($ir->empType));
            $this->excel->getActiveSheet()->setCellValue('K' . $emprow, ucwords($ir->subjectPosition));
            $this->excel->getActiveSheet()->setCellValue('L' . $emprow, ucwords($ir->subjectEmpStat));
            $this->excel->getActiveSheet()->setCellValue('M' . $emprow, ucwords($ir->subjectAccount));
            $this->excel->getActiveSheet()->setCellValue('N' . $emprow, $ir->offenseType);
            $this->excel->getActiveSheet()->setCellValue('O' . $emprow, $ir->offense);
            $this->excel->getActiveSheet()->setCellValue('P' . $emprow, ucwords($ir->category));
            $this->excel->getActiveSheet()->setCellValue('Q' . $emprow, $ir->details);
            $this->excel->getActiveSheet()->setCellValue('R' . $emprow, $ir->place);
            $this->excel->getActiveSheet()->setCellValue('S' . $emprow, $ir->expectedAction);
            $this->excel->getActiveSheet()->setCellValue('T' . $emprow, date('Y/m/d', strtotime($ir->prescriptionEnd)));
            $this->excel->getActiveSheet()->setCellValue('U' . $emprow, ucwords($ir->prescriptiveStatDesc));
            $this->excel->getActiveSheet()->setCellValue('V' . $emprow, ucwords($ir->liabilityStatDesc));
            $this->excel->getActiveSheet()->setCellValue('W' . $emprow, $dateofliability_display);   
            $this->excel->getActiveSheet()->setCellValue('X' . $emprow, date('Y/m/d H:i', strtotime($ir->explanationDate)));
            $this->excel->getActiveSheet()->setCellValue('Y' . $emprow, $ir->subjectExplanation);
            $this->excel->getActiveSheet()->setCellValue('Z' . $emprow, ucwords($ir->filingTypeDescription));
            $this->excel->getActiveSheet()->setCellValue('AA' . $emprow, ucwords($ir->irstageDescription));
            $this->excel->getActiveSheet()->setCellValue('AB' . $emprow, ucwords($ir->sourceFname));
            $this->excel->getActiveSheet()->setCellValue('AC' . $emprow, ucwords($ir->sourceMname));
            $this->excel->getActiveSheet()->setCellValue('AD' . $emprow, ucwords($ir->sourceLname));
            $this->excel->getActiveSheet()->setCellValue('AE' . $emprow, ucwords($ir->sourceNameExt));
            $this->excel->getActiveSheet()->setCellValue('AF' . $emprow, $ir->suggestedAction);
            $this->excel->getActiveSheet()->setCellValue('AG' . $emprow, $final_aCtion);
            $this->excel->getActiveSheet()->setCellValue('AH' . $emprow, $f_effectivity);
            $emprow++;
        }

        $column = $this->excel->getActiveSheet()->getHighestColumn();
        $row = $this->excel->getActiveSheet()->getHighestRow();
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('font' => array('size' => 10), 'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '4b4c54')), 'font' => array('bold' => true, 'color' => array('rgb' => 'ffffff'))));
        //        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('A3:E' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('O3:O' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('S3:S' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('V3:W' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('AG3:AH' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);
        $this->excel->getActiveSheet()->freezePane('H4');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
        $excessletters = ['H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH'];
        foreach ($excessletters as $l) {
            $this->excel->getActiveSheet()->getColumnDimension($l)->setAutoSize(true);
        }
        ob_clean();
        $filename = 'SZ_IR_'.$incidentDate_start.'_'.$incidentDate_end.'.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function fetch_sustermdetails()
    {
        $irid = $this->input->post('irid');
        $qry = "SELECT ir.incidentReport_ID,ir.incidentDate, sup.suspensionTerminationDays_ID,sup.days,supdate.date sustermdate,supdate.suspensionTerminationDays_ID,subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, dmsActionFin.action finalAction, dmsActionFin.label, supdate.date FROM tbl_dms_incident_report ir, tbl_dms_suspension_termination_days sup, tbl_applicant subApp, tbl_employee subEmp, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin, tbl_dms_suspension_dates supdate WHERE ir.incidentReport_ID=$irid AND sup.incidentReport_ID=ir.incidentReport_ID AND subApp.apid = subEmp.apid AND subEmp.emp_id = ir.subjectEmp_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND sup.suspensionTerminationDays_ID=supdate.suspensionTerminationDays_ID";
        $result = $this->general_model->custom_query($qry);
        echo json_encode($result);
    }
    // Charise end code
    // MJM start  code
    // public function submitRecommendClienttest(){
    //     $irid = 3324;

    //     // $qry="SELECT * from tbl_dms_incident_report_recommendation where incidentReport_ID=$irid order by level DESC limit 1";
    //     $qry = "SELECT a.*,label FROM tbl_dms_incident_report_recommendation a,tbl_dms_disciplinary_action_category b,tbl_dms_disciplinary_action c where a.disciplinaryActionCategory_ID=b.disciplinaryActionCategory_ID and b.disciplinaryAction_ID=c.disciplinaryAction_ID and a.incidentReport_ID=$irid and incidentReportRecommendationType_ID=2 order by a.level DESC limit 1";
    //     $rs = $this->general_model->custom_query($qry);
    //     var_dump($rs);
    // }
    public function submitRecommendClient() // Client Recommendation using Approve action
    {
        date_default_timezone_set('Asia/Manila');
        $irid = $this->input->post('irid');

        // $qry="SELECT * from tbl_dms_incident_report_recommendation where incidentReport_ID=$irid order by level DESC limit 1";
        $qry = "SELECT a.*,label FROM tbl_dms_incident_report_recommendation a,tbl_dms_disciplinary_action_category b,tbl_dms_disciplinary_action c where a.disciplinaryActionCategory_ID=b.disciplinaryActionCategory_ID and b.disciplinaryAction_ID=c.disciplinaryAction_ID and a.incidentReport_ID=$irid and incidentReportRecommendationType_ID=2 order by a.level DESC limit 1";
        $rs = $this->general_model->custom_query($qry);

        // $data1['incidentReport_ID']= $irid;
        // $data1['liabilityStat_ID']= 4; 
        // $data1['level']= $rs[0]->level+2;
        // $data1['datedLiabilityStat']= date("Y-m-d H:i:s");
        // $data1['disciplinaryActionCategory_ID']= $rs[0]->disciplinaryActionCategory_ID;
        // $data1['susTermDetails']= $rs[0]->susTermDetails;
        // $data1['incidentReportRecommendationType_ID']= 3;
        // $data1['emp_ID']= $rs[0]->emp_ID;
        // $addClientRec1 = $this->general_model->insert_vals($data1, 'tbl_dms_incident_report_recommendation');
        $data4['days'] = $rs[0]->susTermDetails;
        $data4['status_ID'] = 26;
        $where = "incidentReport_ID = $irid";
        $update_stat1 = $this->general_model->update_vals($data4, $where, 'tbl_dms_suspension_termination_days');

        // $data1['disciplinaryActionCategory_ID'] = $rs[0]->disciplinaryActionCategory_ID;
        $dataFinalReco['liabilityStat_ID'] = $rs[0]->liabilityStat_ID;
        $dataFinalReco['disciplinaryActionCategory_ID'] = $rs[0]->disciplinaryActionCategory_ID;
        $dataFinalReco['notes'] = "";
        $dataFinalReco['datedLiabilityStat'] = date("Y-m-d H:i:s");
        $whereFinalReco = "incidentReport_ID = $irid and emp_ID=" . $_SESSION["emp_id"] . " and liabilityStat_ID=4";
        $this->general_model->update_vals($dataFinalReco, $whereFinalReco, 'tbl_dms_incident_report_recommendation');

        // update the cure date in case the disciplinary action is lowered down
        $cure_date_details = $this->get_prescriptive_period($irid, $rs[0]->disciplinaryActionCategory_ID);
        $data1['prescriptionEnd'] = $cure_date_details['cure_date'];
        // update the cure date in case the disciplinary action is lowered down (END)
        $data1['finaldisciplinaryActionCategory_ID'] = $rs[0]->disciplinaryActionCategory_ID;
        $data1['liabilityStat'] = $rs[0]->liabilityStat_ID;
        $data1['datedLiabilityStat'] = date("Y-m-d H:i:s");
        $data1['incidentReportStages_ID'] = 3;
        $data1['datedLiabilityStat'] = date("Y-m-d H:i:s");
        $where = "incidentReport_ID = $irid";
        $this->general_model->update_vals($data1, $where, 'tbl_dms_incident_report');

        $dataClient['confirmationStat'] = 5;
        $dataClient['disciplinaryActionCategory_ID'] = $rs[0]->disciplinaryActionCategory_ID;
        $dataClient['suggestion'] = $rs[0]->susTermDetails;
        $where = "incidentReport_ID = $irid and confirmationStat=4 and directSupEmp_ID=" . $_SESSION["emp_id"];
        $update_stat = $this->general_model->update_vals($dataClient, $where, 'tbl_dms_client_confirmation');

        if ($rs[0]->label == 's') {
            $dataAttach['incidentReportAttachmentPurpose_ID'] = 2;
            $susOrterm = "Suspension";
        } else if ($rs[0]->label == 't') {
            $dataAttach['incidentReportAttachmentPurpose_ID'] = 3;
            $susOrterm = "Termination";
            $_stid = $this->general_model->fetch_specific_val("suspensionTerminationDays_ID", "incidentReport_ID = $irid", "tbl_dms_suspension_termination_days");
            $this->addSusTermDetails($rs[0]->susTermDetails, 0, 0, $_stid->suspensionTerminationDays_ID, 0);
        }

        $dataAttach['incidentReport_ID'] = $irid;
        $dataAttach['eroEmp_ID'] = $rs[0]->emp_ID;
        $dataAttach['status_ID'] = 2;
        $dataAttach['dateStatus'] = date("Y-m-d H:i:s");
        $saveRecordAttach = $this->general_model->insert_vals($dataAttach, 'tbl_dms_necessary_document');

        #Notify
        $incident_report = $this->get_ir($irid);
        $notif_mssg = "The client approves your decision to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($irid, 8, '0', STR_PAD_LEFT) . " </small>";
        $link = "discipline/dms_ero";
        $next_level_emp_details = $this->get_emp_details_via_emp_id($rs[0]->emp_ID);
        $notify = $this->simple_system_notification($notif_mssg, $link, $next_level_emp_details->uid, 'dms');

        $notif  = $this->sendNotifFinalRecommend($irid, $_SESSION["emp_id"], $susOrterm, null, $rs[0]->liabilityStat_ID);
        /* if($ch == 't') {
			
		} */
        echo $update_stat;
    }

    public function submitRecommend($irid)
    {
        date_default_timezone_set('Asia/Manila');
        $actCat_ID =  $this->input->post('disciplinaryActionCategory_ID');
        $recommenlvl =  trim($this->input->post('recommenlvl'));
        $typ =  trim($this->input->post('typ'));
        $ch =  $this->input->post('recDisAct'); // This is if suspension,termination or just warning

        if ($actCat_ID != 0) {
            $data['disciplinaryActionCategory_ID'] = $actCat_ID;
            $dataIRclient['disciplinaryActionCategory_ID'] = $actCat_ID;
            $dataClient['disciplinaryActionCategory_ID'] = $actCat_ID;
            if ($ch != 'w') {

                $req1 = ($ch == 's') ? $this->input->post('reqNumDays') : $this->input->post('reqDate');
                if ($recommenlvl >= 2) {
                    $data['susTermDetails'] = $req1;
                }
                $dataIRclient['susTermDetails'] = $req1;
                $dataIRclient1['susTermDetails'] = $req1;
                $dataClient['suggestion'] = $req1;
            }
        }
        $qryRec = "SELECT * from tbl_dms_incident_report_recommendation where incidentReport_ID=$irid and incidentReportRecommendationType_ID=2 order by level DESC limit 1";
        $qryRecCurrent = "SELECT * from tbl_dms_incident_report_recommendation where incidentReport_ID=$irid and emp_ID=" . $_SESSION["emp_id"] . " order by level DESC limit 1";
        $rs = $this->general_model->custom_query($qryRec);
        $rsCurrent = $this->general_model->custom_query($qryRecCurrent);
        if ($typ == 3) { // this block of code is intended only for Client Recommenders. This is also only for disapproving the Client Recommendation

            $req = $this->input->post('reqDate');

            // $dataIRclient['incidentReport_ID']= $irid;
            // $dataIRclient['liabilityStat_ID']= $this->input->post('liabilityStat_ID');
            // $dataIRclient['level']= $rs[0]->level+1;
            // $dataIRclient['datedLiabilityStat']= date("Y-m-d H:i:s");
            // $dataIRclient['disciplinaryActionCategory_ID']= $rs[0]->disciplinaryActionCategory_ID;
            // $dataIRclient['incidentReportRecommendationType_ID']= 4;
            // $dataIRclient['emp_ID']= $_SESSION["emp_id"];
            // $addClientRec = $this->general_model->insert_vals($dataIRclient, 'tbl_dms_incident_report_recommendation');


            $data['notes'] =  $this->input->post('txt');
            // $data['susTermDetails'] = $rs[0]->susTermDetails;
            $data['liabilityStat_ID'] = $this->input->post('liabilityStat_ID');
            $data['datedLiabilityStat'] = $rs[0]->datedLiabilityStat;
            $where = "incidentReport_ID = $irid and incidentReportRecommendationType_ID=4 and emp_ID=" . $_SESSION["emp_id"];
            $update_stat = $this->general_model->update_vals($data, $where, 'tbl_dms_incident_report_recommendation');

            if ($ch == 't') {
                $_stid = $this->general_model->fetch_specific_val("suspensionTerminationDays_ID", "incidentReport_ID = $irid", "tbl_dms_suspension_termination_days");
                $this->addSusTermDetails($req, 0, 0, $_stid->suspensionTerminationDays_ID, 0);
            }
            if ($ch == 's') {
                $req = $this->input->post('reqNumDays');
                $data4['days'] = $req;
            } else {
                $data4['days'] = 1;
            }
            $data4['status_ID'] = 26;
            $where = "incidentReport_ID = $irid";
            $update_sta = $this->general_model->update_vals($data4, $where, 'tbl_dms_suspension_termination_days');

            $dataIRclient1['incidentReport_ID'] = $irid;
            $dataIRclient1['notes'] = $this->input->post('txt');
            $dataIRclient1['liabilityStat_ID'] = 4;
            $dataIRclient1['level'] = $rs[0]->level + 2;
            $dataIRclient1['datedLiabilityStat'] = date("Y-m-d H:i:s");
            $dataIRclient1['disciplinaryActionCategory_ID'] = $rs[0]->disciplinaryActionCategory_ID;
            $dataIRclient1['incidentReportRecommendationType_ID'] = 3;
            $dataIRclient1['emp_ID'] = $rs[0]->emp_ID;
            $addClientRec1 = $this->general_model->insert_vals_last_inserted_id($dataIRclient1, 'tbl_dms_incident_report_recommendation');
            $setDeadlineForFinalRec = $this->deadline_final_decision($addClientRec1);

            $dataClient['confirmationStat'] = 6;
            $dataClient['notes'] = $this->input->post('txt');
            $where = "incidentReport_ID = $irid and confirmationStat=4 and directSupEmp_ID=" . $_SESSION["emp_id"];
            $update_stat1 = $this->general_model->update_vals($dataClient, $where, 'tbl_dms_client_confirmation');

            #Notify
            $incident_report = $this->get_ir($irid);
            $notif_mssg = "You are notified to give final decision to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($irid, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "discipline/dms_ero/eroRecommendationTab";
            $next_level_emp_details = $this->get_emp_details_via_emp_id($rs[0]->emp_ID);
            $notify = $this->simple_system_notification($notif_mssg, $link, $next_level_emp_details->uid, 'dms');
        } else {


            $after_recommmending_ID = $this->general_model->fetch_specific_val("incidentReportRecommendation_ID as id", "incidentReport_ID = $irid and liabilityStat_ID=4 and emp_ID=" . $_SESSION["emp_id"], "tbl_dms_incident_report_recommendation");
            //  var_dump($after_recommmending_ID);
            $data['notes'] = $this->input->post('txt');
            $data['liabilityStat_ID'] = $this->input->post('liabilityStat_ID');
            $data['datedLiabilityStat'] = date("Y-m-d H:i:s");
            $where = "incidentReport_ID = $irid and liabilityStat_ID=4 and emp_ID=" . $_SESSION["emp_id"];
           
            
            $update_stat = $this->general_model->update_vals($data, $where, 'tbl_dms_incident_report_recommendation');
            
            $update_stat1 = 0;
            if ($update_stat > 0) { // Checkes if the insertion above is successful
                if ($actCat_ID != 0) { // Checks if Liable 
                    if (trim($ch) == "w") { // If only warning, it will make the recommendation COMPLETED
                        $update_stat1 = 1;
                        if ((int) $recommenlvl == 2 || (int) $recommenlvl == 3) {
                            // update the cure date in case the disciplinary action is lowered down
                            $cure_date_details = $this->get_prescriptive_period($irid, $actCat_ID);
                            $data1['prescriptionEnd'] = $cure_date_details['cure_date'];
                            // update the cure date in case the disciplinary action is lowered down (END)
                            $data1['finaldisciplinaryActionCategory_ID'] = $actCat_ID;
                            $data1['liabilityStat'] = $this->input->post('liabilityStat_ID');
                            $data1['datedLiabilityStat'] = date("Y-m-d H:i:s");
                            $data1['incidentReportStages_ID'] = 3;
                            $where = "incidentReport_ID = $irid";
                            $update_stat1 = $this->general_model->update_vals($data1, $where, 'tbl_dms_incident_report');

                            $this->set_current_prescriptive_stat_to_complete_if_liable($irid);
                            $this->check_if_cure_date_is_completed($irid);
                            $this->access_on_hold_during_ir_complete($irid);
                        }
                        // else{
                        // $dataLiable['liabilityStat_ID'] = 4;
                        // $dataLiable['datedLiabilityStat'] = date("Y-m-d H:i:s");
                        // $whereLiable = "incidentReport_ID = $irid and liabilityStat_ID=2 and level=".($rsCurrent[0]->level+1);
                        // $update_stat1 = $this->general_model->update_vals($dataLiable, $whereLiable, 'tbl_dms_incident_report_recommendation');
                        // }
                        $r = $this->after_recommmending($after_recommmending_ID->id);
                    } else { // This action is for Suspension or Termination
                        if ($ch == 's') {
                            $req = $this->input->post('reqNumDays');
                            $data2['days'] = $req;
                            $data4['days'] = $req;
                            $susOrterm = "Suspension";
                            $dataAttach['incidentReportAttachmentPurpose_ID'] = 2;
                        } else {
                            $req = $this->input->post('reqDate');
                            $data2['days'] = 1;
                            $data4['days'] = 1;
                            $dataAttach['incidentReportAttachmentPurpose_ID'] = 3;
                            $susOrterm = "Termination";
                        }
                        $supervisor = $this->general_model->custom_query("SELECT supervisor FROM tbl_dms_incident_report a,tbl_employee b where a.subjectEmp_ID = b.emp_id and a.incidentReport_ID=" . $irid);
                        if ($recommenlvl . "" == "2") { // Last Recommender
                           

                            #1 - Save first the basis as to how many days of suspension/Termination. 
                            $data2['incidentReport_ID'] = $irid;
                            $data2['directSupEmp_ID'] = $supervisor[0]->supervisor;
                            $this->general_model->insert_vals($data2, 'tbl_dms_suspension_termination_days');

                            #2 - Insert record to Client table for Last Recommendation


                            $data1['incidentReport_ID'] = $irid;
                            $data1['directSupEmp_ID'] = $supervisor[0]->supervisor;
                            $data1['confirmationStat'] = 4;
                            $data1['disciplinaryActionCategory_ID'] = $actCat_ID;
                            $data1['suggestion'] = $req;
                            $data1['notes'] = $this->input->post('txt');
                            $update_stat1 = $this->general_model->insert_vals($data1, 'tbl_dms_client_confirmation');

                            #Note: transfer this code to the line where client recommender is inserted
                            $data['incidentReport_ID'] = $irid;
                            $data['level'] = $rs[0]->level + 1;
                            $data['disciplinaryActionCategory_ID'] = $rs[0]->disciplinaryActionCategory_ID;
                            $data['incidentReportRecommendationType_ID'] = 4;
                            $data['liabilityStat_ID'] = 4;
                            $data['emp_ID'] = $supervisor[0]->supervisor;
                            $addClientRec = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dms_incident_report_recommendation');
                            
                            // $r = $this->after_recommmending($after_recommmending_ID->id);
                            
                            #Notify 
                            $incident_report = $this->get_ir($irid);
                            $notif_mssg = "You are notified to <b>confirm the client</b> about the decision of the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($irid, 8, '0', STR_PAD_LEFT) . " </small>";
                            $link = "discipline/dms_supervision/irSupervision/irSup_clientConfirmation";
                            $next_level_emp_details = $this->get_emp_details_via_emp_id($supervisor[0]->supervisor);
                            $notify = $this->simple_system_notification($notif_mssg, $link, $next_level_emp_details->uid, 'dms');
                        } else if ($recommenlvl . "" == "3") { // Final Recommender
                           
                            $data4['status_ID'] = 26;
                            $where = "incidentReport_ID = $irid";
                            $update_stat1 = $this->general_model->update_vals($data4, $where, 'tbl_dms_suspension_termination_days');
                            // MIC CODES set prescription end if the termination disc asction is lowered down
                            $ir_details = $this->get_ir($irid);
                            $sug_disc_action_details = $this->get_disc_action_via_dis_categ_id($ir_details->disciplinaryActionCategory_ID);
                            $final_disc_action_details = $this->get_disc_action_via_dis_categ_id($actCat_ID);
                            if($sug_disc_action_details->label == 't' && $final_disc_action_details->label != 't'){
                                $cure_date_details = $this->get_prescriptive_period($irid, $actCat_ID);
                                $data1['prescriptionEnd'] = $cure_date_details['cure_date'];
                            }
                            // MIC CODES end
                            $data1['finaldisciplinaryActionCategory_ID'] = $actCat_ID;
                            $data1['liabilityStat'] = $this->input->post('liabilityStat_ID');
                            $data1['datedLiabilityStat'] = date("Y-m-d H:i:s");
                            $data1['incidentReportStages_ID'] = 3;
                            $where = "incidentReport_ID = $irid";
                            $this->general_model->update_vals($data1, $where, 'tbl_dms_incident_report');

                            $dataAttach['incidentReport_ID'] = $irid;
                            $dataAttach['eroEmp_ID'] = $rs[0]->emp_ID;
                            $dataAttach['status_ID'] = 2;
                            $dataAttach['dateStatus'] = date("Y-m-d H:i:s");
                            $saveRecordAttach = $this->general_model->insert_vals($dataAttach, 'tbl_dms_necessary_document');
                            #Notify
                            $notif  = $this->sendNotifFinalRecommend($irid, $supervisor[0]->supervisor, $susOrterm, 1, $this->input->post('liabilityStat_ID'));


                            if ($ch == 't') {
                                $_stid = $this->general_model->fetch_specific_val("suspensionTerminationDays_ID", "incidentReport_ID = $irid", "tbl_dms_suspension_termination_days");
                                $this->addSusTermDetails($req, 0, 0, $_stid->suspensionTerminationDays_ID, 0);
                            }
                        } else if ($recommenlvl . "" == "1") {
                            //echo $recommenlvl;

                            $r = $this->after_recommmending($after_recommmending_ID->id);
                            $update_stat1 = ($r["error"] == 0) ? 1 : 0;
                        }
                    }
                } else { // If Non-Liable
                    $update_stat1 = 1;
                    if ((int) $recommenlvl == 2 || (int) $recommenlvl == 3) {
                        $data1['liabilityStat'] = $this->input->post('liabilityStat_ID');
                        $data1['datedLiabilityStat'] = date("Y-m-d H:i:s");
                        $data1['incidentReportStages_ID'] = 3;
                        $where = "incidentReport_ID = $irid";
                        $update_stat1 = $this->general_model->update_vals($data1, $where, 'tbl_dms_incident_report');

                        $this->set_to_complete_ongoing_ir_if_non_liable($irid);
                        $this->reset_related_details_if_non_liable($irid);
                        $this->access_on_hold_during_ir_complete($irid);
                    }/* else{
							$datanonLiable['liabilityStat_ID'] = 4;
							$datanonLiable['datedLiabilityStat'] = date("Y-m-d H:i:s");
							$wherenonLiable = "incidentReport_ID = $irid and liabilityStat_ID=2 and level=".($rsCurrent[0]->level+1);
							$update_stat1 = $this->general_model->update_vals($datanonLiable, $wherenonLiable, 'tbl_dms_incident_report_recommendation');

						} */
                    $r = $this->after_recommmending($after_recommmending_ID->id);
                }
            }
        }
        echo json_encode($update_stat1);
    }

    public function submitRecommend_test($irid = 1303){
        date_default_timezone_set('Asia/Manila');
        // $actCat_ID =  $this->input->post('disciplinaryActionCategory_ID');
        $actCat_ID =  5;
        // $recommenlvl =  trim($this->input->post('recommenlvl'));
        $recommenlvl =  2;
        // $typ =  trim($this->input->post('typ'));
        $typ =  2;
        // var_dump($typ);
        // $ch =  $this->input->post('recDisAct'); // This is if suspension,termination or just warning
        $ch =  'w';

        if ($actCat_ID != 0) {
            $data['disciplinaryActionCategory_ID'] = $actCat_ID;
            $dataIRclient['disciplinaryActionCategory_ID'] = $actCat_ID;
            $dataClient['disciplinaryActionCategory_ID'] = $actCat_ID;
            if ($ch != 'w') {

                $req1 = ($ch == 's') ? $this->input->post('reqNumDays') : $this->input->post('reqDate');
                if ($recommenlvl >= 2) {
                    $data['susTermDetails'] = $req1;
                }
                $dataIRclient['susTermDetails'] = $req1;
                $dataIRclient1['susTermDetails'] = $req1;
                $dataClient['suggestion'] = $req1;
            }
        }
        $qryRec = "SELECT * from tbl_dms_incident_report_recommendation where incidentReport_ID=$irid and incidentReportRecommendationType_ID=2 order by level DESC limit 1";
        $qryRecCurrent = "SELECT * from tbl_dms_incident_report_recommendation where incidentReport_ID=$irid and emp_ID=" . $_SESSION["emp_id"] . " order by level DESC limit 1";
        $rs = $this->general_model->custom_query($qryRec);
        $rsCurrent = $this->general_model->custom_query($qryRecCurrent);
        if ($typ == 3) { // this block of code is intended only for Client Recommenders. This is also only for disapproving the Client Recommendation

            $req = $this->input->post('reqDate');

            // $dataIRclient['incidentReport_ID']= $irid;
            // $dataIRclient['liabilityStat_ID']= $this->input->post('liabilityStat_ID');
            // $dataIRclient['level']= $rs[0]->level+1;
            // $dataIRclient['datedLiabilityStat']= date("Y-m-d H:i:s");
            // $dataIRclient['disciplinaryActionCategory_ID']= $rs[0]->disciplinaryActionCategory_ID;
            // $dataIRclient['incidentReportRecommendationType_ID']= 4;
            // $dataIRclient['emp_ID']= $_SESSION["emp_id"];
            // $addClientRec = $this->general_model->insert_vals($dataIRclient, 'tbl_dms_incident_report_recommendation');


            $data['notes'] =  $this->input->post('txt');
            // $data['susTermDetails'] = $rs[0]->susTermDetails;
            $data['liabilityStat_ID'] = $this->input->post('liabilityStat_ID');
            $data['datedLiabilityStat'] = $rs[0]->datedLiabilityStat;
            $where = "incidentReport_ID = $irid and incidentReportRecommendationType_ID=4 and emp_ID=" . $_SESSION["emp_id"];
            // $update_stat = $this->general_model->update_vals($data, $where, 'tbl_dms_incident_report_recommendation');

            if ($ch == 't') {
                $_stid = $this->general_model->fetch_specific_val("suspensionTerminationDays_ID", "incidentReport_ID = $irid", "tbl_dms_suspension_termination_days");
                // $this->addSusTermDetails($req, 0, 0, $_stid->suspensionTerminationDays_ID, 0);
            }
            if ($ch == 's') {
                $req = $this->input->post('reqNumDays');
                $data4['days'] = $req;
            } else {
                $data4['days'] = 1;
            }
            $data4['status_ID'] = 26;
            $where = "incidentReport_ID = $irid";
            // $update_sta = $this->general_model->update_vals($data4, $where, 'tbl_dms_suspension_termination_days');

            $dataIRclient1['incidentReport_ID'] = $irid;
            $dataIRclient1['notes'] = $this->input->post('txt');
            $dataIRclient1['liabilityStat_ID'] = 4;
            $dataIRclient1['level'] = $rs[0]->level + 2;
            $dataIRclient1['datedLiabilityStat'] = date("Y-m-d H:i:s");
            $dataIRclient1['disciplinaryActionCategory_ID'] = $rs[0]->disciplinaryActionCategory_ID;
            $dataIRclient1['incidentReportRecommendationType_ID'] = 3;
            $dataIRclient1['emp_ID'] = $rs[0]->emp_ID;
            // $addClientRec1 = $this->general_model->insert_vals_last_inserted_id($dataIRclient1, 'tbl_dms_incident_report_recommendation');
            // $setDeadlineForFinalRec = $this->deadline_final_decision($addClientRec1);

            $dataClient['confirmationStat'] = 6;
            $dataClient['notes'] = $this->input->post('txt');
            $where = "incidentReport_ID = $irid and confirmationStat=4 and directSupEmp_ID=" . $_SESSION["emp_id"];
            // $update_stat1 = $this->general_model->update_vals($dataClient, $where, 'tbl_dms_client_confirmation');

            #Notify
            $incident_report = $this->get_ir($irid);
            $notif_mssg = "You are notified to give final decision to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($irid, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "discipline/dms_ero/eroRecommendationTab";
            $next_level_emp_details = $this->get_emp_details_via_emp_id($rs[0]->emp_ID);
            // $notify = $this->simple_system_notification($notif_mssg, $link, $next_level_emp_details->uid, 'dms');
        } else {


            $after_recommmending_ID = $this->general_model->fetch_specific_val("incidentReportRecommendation_ID as id", "incidentReport_ID = $irid and liabilityStat_ID=4 and emp_ID=" . $_SESSION["emp_id"], "tbl_dms_incident_report_recommendation");
            //  var_dump($after_recommmending_ID);
            $data['notes'] = $this->input->post('txt');
            $data['liabilityStat_ID'] = $this->input->post('liabilityStat_ID');
            $data['datedLiabilityStat'] = date("Y-m-d H:i:s");
            $where = "incidentReport_ID = $irid and liabilityStat_ID=4 and emp_ID=" . $_SESSION["emp_id"];
           
            
            // $update_stat = $this->general_model->update_vals($data, $where, 'tbl_dms_incident_report_recommendation');
            
            $update_stat = 1;
            // $update_stat1 = 0;
            if ($update_stat > 0) { // Checkes if the insertion above is successful
                if ($actCat_ID != 0) { // Checks if Liable 
                    var_dump("Liable");
                    var_dump($ch);
                    if (trim($ch) == "w") { // If only warning, it will make the recommendation COMPLETED
                        var_dump("if Written");
                        $update_stat1 = 1;
                        if ((int) $recommenlvl == 2 || (int) $recommenlvl == 3) {
                            var_dump("final");
                            // update the cure date in case the disciplinary action is lowered down
                            $cure_date_details = $this->get_prescriptive_period($irid, $actCat_ID);
                            $data1['prescriptionEnd'] = $cure_date_details['cure_date'];
                            // update the cure date in case the disciplinary action is lowered down (END)
                            $data1['finaldisciplinaryActionCategory_ID'] = $actCat_ID;
                            $data1['liabilityStat'] = $this->input->post('liabilityStat_ID');
                            $data1['datedLiabilityStat'] = date("Y-m-d H:i:s");
                            $data1['incidentReportStages_ID'] = 3;
                            $where = "incidentReport_ID = $irid";
                            // $update_stat1 = $this->general_model->update_vals($data1, $where, 'tbl_dms_incident_report');

                            // $this->set_current_prescriptive_stat_to_complete_if_liable($irid);
                            // $this->check_if_cure_date_is_completed($irid);
                            $this->main_access_on_hold_during_ir_complete_test($irid);
                            // ()
                        }
                        // else{
                        // $dataLiable['liabilityStat_ID'] = 4;
                        // $dataLiable['datedLiabilityStat'] = date("Y-m-d H:i:s");
                        // $whereLiable = "incidentReport_ID = $irid and liabilityStat_ID=2 and level=".($rsCurrent[0]->level+1);
                        // $update_stat1 = $this->general_model->update_vals($dataLiable, $whereLiable, 'tbl_dms_incident_report_recommendation');
                        // }
                        // $r = $this->after_recommmending($after_recommmending_ID->id);
                    } else { // This action is for Suspension or Termination
                        var_dump("If Suspension & Termination");
                        if ($ch == 's') {
                            $req = $this->input->post('reqNumDays');
                            $data2['days'] = $req;
                            $data4['days'] = $req;
                            $susOrterm = "Suspension";
                            $dataAttach['incidentReportAttachmentPurpose_ID'] = 2;
                        } else {
                            $req = $this->input->post('reqDate');
                            $data2['days'] = 1;
                            $data4['days'] = 1;
                            $dataAttach['incidentReportAttachmentPurpose_ID'] = 3;
                            $susOrterm = "Termination";
                        }
                        $supervisor = $this->general_model->custom_query("SELECT supervisor FROM tbl_dms_incident_report a,tbl_employee b where a.subjectEmp_ID = b.emp_id and a.incidentReport_ID=" . $irid);
                        if ($recommenlvl . "" == "2") { // Last Recommender
                           

                            #1 - Save first the basis as to how many days of suspension/Termination. 
                            $data2['incidentReport_ID'] = $irid;
                            $data2['directSupEmp_ID'] = $supervisor[0]->supervisor;
                            // $this->general_model->insert_vals($data2, 'tbl_dms_suspension_termination_days');

                            #2 - Insert record to Client table for Last Recommendation


                            $data1['incidentReport_ID'] = $irid;
                            $data1['directSupEmp_ID'] = $supervisor[0]->supervisor;
                            $data1['confirmationStat'] = 4;
                            $data1['disciplinaryActionCategory_ID'] = $actCat_ID;
                            $data1['suggestion'] = $req;
                            $data1['notes'] = $this->input->post('txt');
                            // $update_stat1 = $this->general_model->insert_vals($data1, 'tbl_dms_client_confirmation');

                            #Note: transfer this code to the line where client recommender is inserted
                            $data['incidentReport_ID'] = $irid;
                            $data['level'] = $rs[0]->level + 1;
                            $data['disciplinaryActionCategory_ID'] = $rs[0]->disciplinaryActionCategory_ID;
                            $data['incidentReportRecommendationType_ID'] = 4;
                            $data['liabilityStat_ID'] = 4;
                            $data['emp_ID'] = $supervisor[0]->supervisor;
                            // $addClientRec = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dms_incident_report_recommendation');
                            
                            // $r = $this->after_recommmending($after_recommmending_ID->id);
                            
                            #Notify 
                            $incident_report = $this->get_ir($irid);
                            $notif_mssg = "You are notified to <b>confirm the client</b> about the decision of the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($irid, 8, '0', STR_PAD_LEFT) . " </small>";
                            $link = "discipline/dms_supervision/irSupervision/irSup_clientConfirmation";
                            $next_level_emp_details = $this->get_emp_details_via_emp_id($supervisor[0]->supervisor);
                            // $notify = $this->simple_system_notification($notif_mssg, $link, $next_level_emp_details->uid, 'dms');
                        } else if ($recommenlvl . "" == "3") { // Final Recommender
                           
                            $data4['status_ID'] = 26;
                            $where = "incidentReport_ID = $irid";
                            // $update_stat1 = $this->general_model->update_vals($data4, $where, 'tbl_dms_suspension_termination_days');
                            // MIC CODES set prescription end if the termination disc asction is lowered down
                            $ir_details = $this->get_ir($irid);
                            $sug_disc_action_details = $this->get_disc_action_via_dis_categ_id($ir_details->disciplinaryActionCategory_ID);
                            $final_disc_action_details = $this->get_disc_action_via_dis_categ_id($actCat_ID);
                            if($sug_disc_action_details->label == 't' && $final_disc_action_details->label != 't'){
                                $cure_date_details = $this->get_prescriptive_period($irid, $actCat_ID);
                                $data1['prescriptionEnd'] = $cure_date_details['cure_date'];
                            }
                            // MIC CODES end
                            $data1['finaldisciplinaryActionCategory_ID'] = $actCat_ID;
                            $data1['liabilityStat'] = $this->input->post('liabilityStat_ID');
                            $data1['datedLiabilityStat'] = date("Y-m-d H:i:s");
                            $data1['incidentReportStages_ID'] = 3;
                            $where = "incidentReport_ID = $irid";
                            // $this->general_model->update_vals($data1, $where, 'tbl_dms_incident_report');

                            $dataAttach['incidentReport_ID'] = $irid;
                            $dataAttach['eroEmp_ID'] = $rs[0]->emp_ID;
                            $dataAttach['status_ID'] = 2;
                            $dataAttach['dateStatus'] = date("Y-m-d H:i:s");
                            // $saveRecordAttach = $this->general_model->insert_vals($dataAttach, 'tbl_dms_necessary_document');
                            #Notify
                            // $notif  = $this->sendNotifFinalRecommend($irid, $supervisor[0]->supervisor, $susOrterm, 1, $this->input->post('liabilityStat_ID'));


                            if ($ch == 't') {
                                $_stid = $this->general_model->fetch_specific_val("suspensionTerminationDays_ID", "incidentReport_ID = $irid", "tbl_dms_suspension_termination_days");
                                // $this->addSusTermDetails($req, 0, 0, $_stid->suspensionTerminationDays_ID, 0);
                            }
                        } else if ($recommenlvl . "" == "1") {
                            //echo $recommenlvl;

                            // $r = $this->after_recommmending($after_recommmending_ID->id);
                            // $update_stat1 = ($r["error"] == 0) ? 1 : 0;
                        }
                    }
                } else { // If Non-Liable
                    var_dump("Non Liable");
                    $update_stat1 = 1;
                    if ((int) $recommenlvl == 2 || (int) $recommenlvl == 3) {
                        $data1['liabilityStat'] = $this->input->post('liabilityStat_ID');
                        $data1['datedLiabilityStat'] = date("Y-m-d H:i:s");
                        $data1['incidentReportStages_ID'] = 3;
                        $where = "incidentReport_ID = $irid";
                        // $update_stat1 = $this->general_model->update_vals($data1, $where, 'tbl_dms_incident_report');

                        // $this->set_to_complete_ongoing_ir_if_non_liable($irid);
                        // $this->reset_related_details_if_non_liable($irid);
                        // $this->access_on_hold_during_ir_complete($irid);
                    }/* else{
							$datanonLiable['liabilityStat_ID'] = 4;
							$datanonLiable['datedLiabilityStat'] = date("Y-m-d H:i:s");
							$wherenonLiable = "incidentReport_ID = $irid and liabilityStat_ID=2 and level=".($rsCurrent[0]->level+1);
							$update_stat1 = $this->general_model->update_vals($datanonLiable, $wherenonLiable, 'tbl_dms_incident_report_recommendation');

						} */
                    // $r = $this->after_recommmending($after_recommmending_ID->id);
                }
            }
        }
        // echo json_encode($update_stat1);
    }

    public function sendNotifFinalRecommend($irid, $supervisor, $susOrterm, $recIDFinal = null, $liabilityStat_ID)
    {
        $incident_report = $this->get_ir($irid);

        $sendDeadlineforDocUploadNorNte = $this->deadline_necessary_doc($irid);

        if ($recIDFinal != null) { //Update deadline for final Recommendation
            $finalReco = $this->general_model->custom_query("SELECT incidentReportRecommendation_ID,emp_ID FROM tbl_dms_incident_report_recommendation where incidentReportRecommendationType_ID=3 and incidentReport_ID=" . $irid);
            $dead['status_ID'] = 3;
            $this->update_dms_deadline($dead, $finalReco[0]->incidentReportRecommendation_ID, $finalReco[0]->emp_ID);
        }

        if ((int) $liabilityStat_ID == 17) {
            $this->set_current_prescriptive_stat_to_complete_if_liable($irid);
            $this->check_if_cure_date_is_completed($irid);
        } else {
            $this->set_to_complete_ongoing_ir_if_non_liable($irid);
            $this->reset_related_details_if_non_liable($irid);
        }
        $this->access_on_hold_during_ir_complete($irid);

        #Notify Suspension/Termination Date(s)
        $notif_mssg_sus_term = "You are notified to set the " . $susOrterm . " date(s) of  <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($irid, 8, '0', STR_PAD_LEFT) . " </small>";
        $link_sus_term = "discipline/dms_supervision/irSupervision/irSup_setDates";
        $next_level_emp_details_sus_term = $this->get_emp_details_via_emp_id($supervisor);
        $notify_sus_term = $this->simple_system_notification($notif_mssg_sus_term, $link_sus_term, $next_level_emp_details_sus_term->uid, 'dms');

        #Notify Subject
        if ($incident_report->liabilityStat == "17") {
            $isLiable = "as liable";
        } else if ($incident_report->liabilityStat == "18") {
            $isLiable = "as non-liable";
        } else {
            $isLiable = "";
        }
        $notif_mssg_subject = "Your Incident Report with an IR-ID of " . str_pad($irid, 8, '0', STR_PAD_LEFT) . " was finalized " . $isLiable;
        $link_subject = "discipline/dms_personal";
        $next_level_subject = $this->get_emp_details_via_emp_id($incident_report->subjectEmp_ID);
        $notify_subject = $this->simple_system_notification($notif_mssg_subject, $link_subject, $next_level_subject->uid, 'dms');

        #Notify Source
        if ($incident_report->liabilityStat == 17) {
            $isLiable = "as liable";
        } else if ($incident_report->liabilityStat == 18) {
            $isLiable = "as non-liable";
        } else {
            $isLiable = "";
        }
        $notif_mssg_source = "The incident Report you filed for <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b> with an IR-ID of " . str_pad($irid, 8, '0', STR_PAD_LEFT) . " was finalized " . $isLiable;
        $link_source = "discipline/dms_supervision/irSupervision/irSup_clientConfirmation";
        $next_level_source = $this->get_emp_details_via_emp_id($incident_report->sourceEmp_ID);
        $notify_source = $this->simple_system_notification($notif_mssg_source, $link_source, $next_level_source->uid, 'uid');
    }
    public function saveScreentShotClient($irid)
    {
        date_default_timezone_set('Asia/Manila');

        $maxsize = 5000000; // 5MB
        $size = $_FILES['upload_file_client']['size'];
        $rs1 = 0;
        if ($size <= $maxsize) {
            $img = $_FILES['upload_file_client']['name'];
            $tmp = $_FILES['upload_file_client']['tmp_name'];
            $path = 'uploads/dms/client/'; // upload directory
            $final_image = time() . $img;
            $path = $path . strtolower($final_image);
            $courtesy = $_SESSION["fname"] . " " . $_SESSION["lname"];

            if (move_uploaded_file($tmp, $path)) {
                $rs1 = $this->addUploadedFilez($irid, $path, $courtesy, "document", 6);
            }
        }

        echo $rs1;
    }
    public function submitExplain($irid, $ifDismiss)
    {
        date_default_timezone_set('Asia/Manila');

        $arr["irid"] = $irid;
        $arr["ifDismiss"] = $ifDismiss;
        $arr["txt"] = $this->input->post('txt');


        if ((int) $ifDismiss == 0) { // if not required  to explain

            $update_stat = $this->submitExplain_func($arr);
        } else {
            $maxsize = 5000000; // 5MB
            $size = $_FILES['upload_file_explain']['size'];

            if ($size <= $maxsize) {
                $img = $_FILES['upload_file_explain']['name'];
                $tmp = $_FILES['upload_file_explain']['tmp_name'];
                $path = 'uploads/dms/explain/'; // upload directory
                $final_image = time() . $img;
                $path = $path . strtolower($final_image);
                $courtesy = $_SESSION["fname"] . " " . $_SESSION["lname"];

                if (move_uploaded_file($tmp, $path)) {
                    $rs1 = $this->addUploadedFilez($irid, $path, $courtesy, "document", 5);
                }
            }
            $update_stat = $this->submitExplain_func($arr);
        }
        echo $update_stat;
    }
    private function check_if_recommenders_exist($ir_id){
        $recommenders = $this->qry_ir_recommendation($ir_id);
        if(count($recommenders) > 0){
            return true;
        }else{
            return false;
        }
    }
    public function submitExplain_func($arr)
    {
        date_default_timezone_set('Asia/Manila');
        $txt = ((int) $arr["ifDismiss"] === 0) ? "" : $arr["txt"];
        $data['subjectExplanation'] = $txt;
        $data['incidentReportStages_ID'] = 2;
        $data['subjectExplanationStat'] = 3;
        $data['explanationDate'] = date("Y-m-d H:i:s");
        $where = "incidentReport_ID =  " . $arr["irid"];
        $update_stat = $this->general_model->update_vals($data, $where, 'tbl_dms_incident_report');

        #Set Deadline to complete after explanation
        $recommenders_exist = $this->check_if_recommenders_exist($arr["irid"]);
        if(!$recommenders_exist){
            $this->set_recommenders($arr["irid"]);
        }
        $incident_report = $this->get_ir($arr["irid"]);
        $data1["status_ID"] = 3;
        $this->update_dms_deadline_with_type($arr["irid"], $incident_report->subjectEmp_ID, 2, $data1);

        return $update_stat;
    }
    public function uploadFile($irid, $nteornor)
    {
        date_default_timezone_set('Asia/Manila');
        $maxsize = 5000000; // 5MB
        $img = $_FILES['upload_files_nor']['name'];
        $tmp = $_FILES['upload_files_nor']['tmp_name'];
        $size = $_FILES['upload_files_nor']['size'];

        $img1 = $_FILES['upload_files_nte']['name'];
        $tmp1 = $_FILES['upload_files_nte']['tmp_name'];
        $size1 = $_FILES['upload_files_nte']['size'];

        $path = 'uploads/dms/nor_nte/'; // upload directory

        // can upload same image using rand function
        $final_image = time() . $img;
        $final_image1 = time() . $img1;
        // check's valid format
        $path = $path . strtolower($final_image);
        $path1 = $path . strtolower($final_image1);
        $ok = 0;
        $courtesy = $_SESSION["fname"] . " " . $_SESSION["lname"];
        if ($size <= $maxsize) {
            if (move_uploaded_file($tmp, $path)) { // NOR
                $rs1 = $this->addUploadedFilez($irid, $path, $courtesy, "document", 2);
                $rs = $this->updateUploadStatus($irid);
                $ok += 1;
                /*  if ($nteornor == 2) {
                    $rs = $this->updateUploadStatus($irid);

                    $ok += 1;
                } else {
                    $ok += 1;
                } */
            }
        }
        if ($size1 <= $maxsize) {
            if (move_uploaded_file($tmp1, $path1)) { // NTE
                $rs1 = $this->addUploadedFilez($irid, $path1, $courtesy, "document", 3);
                $rs = $this->updateUploadStatus($irid);
                $ok += 1;
                /*  if ($nteornor == 3) {
                    $rs = $this->updateUploadStatus($irid);

                    $ok += 1;
                } else {
                    $ok += 1;
                } */
            }
        }

        echo $ok;
    }
    public function updateUploadStatus($irid)
    {
        date_default_timezone_set('Asia/Manila');
        $data1['status_ID'] = 3;
        $data1['dateStatus'] = date("Y-m-d H:i:s");
        $where = "incidentReport_ID = $irid and status_ID=2 and eroEmp_ID=" . $_SESSION["emp_id"];
        $this->general_model->update_vals($data1, $where, 'tbl_dms_necessary_document');

        #updating Deadline for uploading Necessary Doc of ERO.
        $getNecDoc = $this->general_model->fetch_specific_val("irNecessaryDocument_ID,eroEmp_ID", "incidentReport_ID = $irid and eroEmp_ID=" . $_SESSION["emp_id"], "tbl_dms_necessary_document");
        $data['status_ID'] = 3;
        $this->update_dms_deadline_with_type($getNecDoc->irNecessaryDocument_ID, $getNecDoc->eroEmp_ID, 7, $data);
    }
    public function addUploadedFilez($irid, $path, $courtesy, $mediatype, $purpose_ID)
    {

        $dataAttach['incidentReport_ID'] = $irid;
        $dataAttach['incidentReportAttachmentPurpose_ID'] = $purpose_ID;
        $dataAttach['link'] = $path;
        $dataAttach['mediaType'] = $mediatype;
        $dataAttach['courtesyOf'] = $courtesy;
        $dataAttach['dateUploaded'] = date("Y-m-d H:i:s");
        $saveRecordAttach = $this->general_model->insert_vals($dataAttach, 'tbl_dms_incident_report_attachment');
    }
    public function check_if_pending_witness_confirmation_exist()
    {
        // Check if Pending Witness Confirmation exist by Mic2x
        $data["can_explain"] = 1;
        $witnesses = $this->qry_witnesses_by_stat($this->input->post("irId"), 2);
        if (count($witnesses) > 0) {
            $data["can_explain"] = 0;
        }
        echo json_encode($data);
    }
    public function empDetailsSE()
    {
        $disc_action_categ = [];
        $subjectEmp_ID = $this->input->post("id");
        $incidentReport_ID = $this->input->post("incidentReport_ID");
        $disciplinaryActionCategory_ID = $this->input->post("disciplinaryActionCategory_ID");
        $typ = $this->input->post("typ");
        $level = $this->input->post("lvl");
        $disciplineCategory_ID = $this->input->post("disciplineCategory_ID");
        $disciplinaryActionCategorySettings_ID = $this->input->post("disciplinaryActionCategorySettings_ID");
        // $qry="SELECT c.fname,c.lname,e.password as pw,a.*,(select concat(lname,', ',fname) from tbl_applicant x,tbl_employee y where x.apid=y.apid and y.emp_id = a.sourceEmp_ID) as source FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_incident_report_stages d,tbl_user e where a.subjectEmp_ID = b.emp_id and b.emp_id=e.emp_id and b.apid=c.apid and a.incidentReportStages_ID = d.incidentReportStages_ID and  d.incidentReportStages_ID= 1 and a.incidentReport_ID =".$incidentReport_ID;
        // $rs["irdetails"] = $this->general_model->custom_query($qry);
        $rs["category"] = $this->get_discipline_category();
        if ($typ == 2 or $typ == 5 or $typ == 3) {
            
            $rs["currentLevel"] = $this->general_model->fetch_specific_val("incidentReportRecommendationType_ID as level", "emp_ID=" . $_SESSION["emp_id"] . " and liabilityStat_ID=4 and incidentReport_ID=" . $incidentReport_ID, "tbl_dms_incident_report_recommendation");
            $limit =  "";
            $append =  "";
            if ($typ == 5) {
                $disciplineCategory_ID = $rs["category"][0]->disciplineCategory_ID;
            } else {
                if($typ != 3){
                    $getLevel = $this->general_model->fetch_specific_val("level", "disciplinaryActionCategory_ID=$disciplinaryActionCategory_ID ", "tbl_dms_disciplinary_action_category");
                    $limit = " limit 2";
                    $append = "and level<=" . $getLevel->level;
                }else{
                    $field = "categ.disciplineCategory_ID";
                    $where = "categ.disciplineCategory_ID = action_categ.disciplineCategory_ID AND action_categ.disciplinaryActionCategory_ID = rec.disciplinaryActionCategory_ID AND rec.incidentReport_ID = $incidentReport_ID AND rec.incidentReportRecommendationType_ID = 2";
                    $tables = "tbl_dms_discipline_category categ, tbl_dms_disciplinary_action_category action_categ, tbl_dms_incident_report_recommendation rec";
                    $last_ero_categ_action = $this->general_model->fetch_specific_val($field, $where, $tables);
                    // echo $this->db->last_query();
                    // var_dump($last_ero_categ_action);
                    $disciplineCategory_ID  = $last_ero_categ_action->disciplineCategory_ID;
                }
            }
            // var_dump($disciplineCategory_ID);
            if(count($rs["category"]) > 0){
                $qry = "SELECT disciplinaryActionCategory_ID,action,a.disciplinaryActionCategorySettings_ID,label FROM `tbl_dms_disciplinary_action_category` a,tbl_dms_disciplinary_action c where a.disciplinaryAction_ID=c.disciplinaryAction_ID " . $append . " and disciplineCategory_ID= $disciplineCategory_ID and a.disciplinaryActionCategorySettings_ID=$disciplinaryActionCategorySettings_ID order by level DESC " . $limit;
                $disc_action_categ = $this->general_model->custom_query($qry);
            }
            // echo $this->db->last_query();
            $rs["discipline_cat"] = $disc_action_categ;
            $ifClientPageAppend = ($typ == 3) ? " and incidentReportRecommendationType_ID!=4" : "";
            $qry = "SELECT incidentReportRecommendation_ID,incidentReport_ID,a.emp_ID,action, f.category,c.label,a.notes,fname,lname,susTermDetails,incidentReportRecommendationType_ID,a.liabilityStat_ID FROM tbl_dms_incident_report_recommendation a,tbl_dms_disciplinary_action_category b,tbl_dms_disciplinary_action c,tbl_employee d,tbl_applicant e, tbl_dms_discipline_category f where a.emp_ID=d.emp_id and e.apid=d.apid and a.disciplinaryActionCategory_ID=b.disciplinaryActionCategory_ID AND f.disciplineCategory_ID = b.disciplineCategory_ID and b.disciplinaryAction_ID=c.disciplinaryAction_ID and incidentReport_ID=$incidentReport_ID and liabilityStat_ID!=4 and (datedLiabilityStat!='0000-00-00 00:00:00' or a.liabilityStat_ID=12) $ifClientPageAppend order by a.level";
            $rs["discipline_recommenders"] = $this->general_model->custom_query($qry);
            $rs["layout"] = $this->get_layouted_ir_details($incidentReport_ID, 1, 1, 1, 1,1);
        }

        if ($typ == 1) {
            $rs["layout"] = $this->get_layouted_ir_details($incidentReport_ID, 1, 1, 0, 0);

            // $this->set_dms_deadline($subjectEmp_ID, $incidentReport_ID, 2, 2, 1); // testing only. Just to set deadline
        }
        // $qryDismissDeadline = "SELECT * FROM tbl_dms_deadline where record_ID=$incidentReport_ID and emp_id= $subjectEmp_ID and dmsDeadlineType_ID=2 and status_ID=2";

        // $rs["check_dismiss_deadline"] = $this->general_model->custom_query($qryDismissDeadline);
        $rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
        if ((int) $rec_deadline['record']->deadlineToExplainOption === 1) {
            $rs["check_dismiss_deadline"] = 0;
        } else {
            $rs["check_dismiss_deadline"] = 1;
        }

        if ($typ == 5) {
            $rs["layout"] = $this->get_layouted_ir_details($incidentReport_ID, 1, 1, 1, 1);
        }
        if ($typ == 7) {
            $rs["layout"] = $this->get_layouted_ir_details($incidentReport_ID, 1, 1, 1, 1);
            $qry = "SELECT incidentReportRecommendation_ID,incidentReport_ID,a.emp_ID,action, f.category,c.label,a.notes,fname,lname,susTermDetails,incidentReportRecommendationType_ID,a.liabilityStat_ID FROM tbl_dms_incident_report_recommendation a,tbl_dms_disciplinary_action_category b,tbl_dms_disciplinary_action c,tbl_employee d,tbl_applicant e, tbl_dms_discipline_category f where a.emp_ID=d.emp_id and e.apid=d.apid and a.disciplinaryActionCategory_ID=b.disciplinaryActionCategory_ID AND f.disciplineCategory_ID = b.disciplineCategory_ID and b.disciplinaryAction_ID=c.disciplinaryAction_ID and incidentReport_ID=$incidentReport_ID and liabilityStat_ID!=4 and datedLiabilityStat!='0000-00-00 00:00:00' order by a.level";
            $rs["discipline_recommenders"] = $this->general_model->custom_query($qry);
            $rs["discipline_cat"] = array();
        }
        if ($typ == 5  or $typ == 3  or $typ == 7) {
            $qryclient = "SELECT suggestion,label FROM tbl_dms_client_confirmation a,tbl_dms_disciplinary_action_category b,tbl_dms_disciplinary_action c where a.disciplinaryActionCategory_ID=b.disciplinaryActionCategory_ID and b.disciplinaryAction_ID=c.disciplinaryAction_ID and incidentReport_ID=" . $incidentReport_ID;
            $rs["client_confirm"] = $this->general_model->custom_query($qryclient);
        }
        $qryAttachClient = "SELECT * FROM tbl_dms_incident_report_attachment where  incidentReportAttachmentPurpose_ID=6 and incidentReport_ID=" . $incidentReport_ID;
        $rs["client_attach"] = $this->general_model->custom_query($qryAttachClient);

        $rs["emp_details"] = $this->get_emp_details_via_emp_id($_SESSION["emp_id"]);
        $rs["ir_details"] = $this->general_model->fetch_specific_val('subjectEmp_ID', "incidentReport_ID=$incidentReport_ID", "tbl_dms_incident_report");

        $rs["emp_details_subject"] = $this->get_emp_details_via_emp_id($rs["ir_details"]->subjectEmp_ID);

        echo json_encode($rs);
    }

    public function getSchedforDMS()
    {
        $irid = $this->input->post('irid');
        $dates = $this->input->post('dates');
        $arr = array();

        $subjectEmp_ID = $this->general_model->fetch_specific_vals('subjectEmp_ID', "incidentReport_ID=$irid", "tbl_dms_incident_report");


        $qry = "SELECT sched_id,emp_id,sched_date,a.schedtype_id,a.acc_time_id,time_start,time_end,d.type FROM tbl_schedule a left join tbl_acc_time b on a.acc_time_id=b.acc_time_id left join tbl_schedule_type d on a.schedtype_id=d.schedtype_id left join tbl_time c on b.time_id=c.time_id where a.emp_id=" . $subjectEmp_ID[0]->subjectEmp_ID . " and a.sched_date='" . $dates . "'";
        $arr["sched"] = $this->general_model->custom_query($qry);

        echo json_encode($arr);
    }

    public function saveTermDates()
    {
        date_default_timezone_set('Asia/Manila');
        $termid = $this->input->post('termid');
        $dated = $this->input->post('dated');
        $data_sched['date'] =  $dated;
        $data_sched['dateCreated'] = date("Y-m-d H:i:s");
        $where = "suspensionDates_ID = " . $termid;
        $update_stat =  $this->general_model->update_vals($data_sched,  $where, 'tbl_dms_suspension_dates');
        echo $update_stat;
    }
    public function saveSusTermDates()
    {
        date_default_timezone_set('Asia/Manila');

        $irid = $this->input->post('irid');
        $stid = $this->input->post('stid');
        $date = $this->input->post('dates');
        $sched = $this->input->post('sched');
        $rs["ir_details"] = $this->general_model->fetch_specific_val('fname,lname,days,label,a.*', "a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReport_ID=d.incidentReport_ID and a.incidentReport_ID=$irid and a.disciplinaryActionCategory_ID=e.disciplinaryActionCategory_ID and e.disciplinaryAction_ID=f.disciplinaryAction_ID", "tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_suspension_termination_days d,tbl_dms_disciplinary_action_category e,tbl_dms_disciplinary_action f ");
        $arr_sched_insert = array(3, 8, 4, 5, 7, 9, 12);


        // $date=explode("_",$rs["dates"][$i]);
        if ($sched != 0) {
            $sch = $this->general_model->fetch_specific_val('acc_time_id,sched_id,schedtype_id', "emp_id=" . $rs["ir_details"]->subjectEmp_ID . " and sched_id=$sched", "tbl_schedule");
            $rs["schedule"][$date] = $sch;

            if (in_array($sch->schedtype_id, $arr_sched_insert)) {
                $data_sched['emp_id'] = $rs["ir_details"]->subjectEmp_ID;
                $data_sched['sched_date'] = $date;
                $data_sched['schedtype_id'] = 12;
                $data_sched['remarks'] = "DMS";
                $data_sched['updated_by'] = $_SESSION["uid"];
                $data_sched['updated_on'] = date("Y-m-d H:i:s");
                $data_sched['isLocked'] = 14;
                $insert_sched = $this->general_model->insert_vals_last_inserted_id($data_sched, 'tbl_schedule');
                $sch_id = $insert_sched;
                $sch_type_id = 12;
                $isInsert = 1;
            } else {
                $sch_id = $sch->sched_id;
                $sch_type_id = $sch->schedtype_id;
                $isInsert = 0;

                $data_sched['schedtype_id'] =  12;
                $data_sched['remarks'] = "DMS";
                $data_sched['isLocked'] =  14;
                $where = "sched_id = " . $sch->sched_id;
                $update_stat =  $this->general_model->update_vals($data_sched,  $where, 'tbl_schedule');
            }
            $suspension_insert = $this->addSusTermDetails($date, $sch_type_id, $sch_id, $stid, $isInsert);
        } else {
            $rs["schedule"][$date] = "No Sched; So Insert;";
            $data_sched['emp_id'] = $rs["ir_details"]->subjectEmp_ID;
            $data_sched['sched_date'] = $date;
            $data_sched['schedtype_id'] = 12;
            $data_sched['remarks'] = "DMS";
            $data_sched['updated_by'] = $_SESSION["uid"];
            $data_sched['updated_on'] = date("Y-m-d H:i:s");
            $data_sched['isLocked'] = 14;
            $insert_sched = $this->general_model->insert_vals_last_inserted_id($data_sched, 'tbl_schedule');

            $suspension_insert = $this->addSusTermDetails($date, 12, $insert_sched, $stid, 1);
        }

        echo json_encode($suspension_insert);
    }
    public function manual_saveSusTermDates()
    {
        date_default_timezone_set('Asia/Manila');

        // $irid = $this->input->post('irid');
        // $stid = $this->input->post('stid');
        // $date = $this->input->post('dates');
        // $sched = $this->input->post('sched');
        $irid = 4184;
        $stid = 388;
        $date = '2021-08-25';
        $sched = 527561;
        $rs["ir_details"] = $this->general_model->fetch_specific_val('fname,lname,days,label,a.*', "a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReport_ID=d.incidentReport_ID and a.incidentReport_ID=$irid and a.disciplinaryActionCategory_ID=e.disciplinaryActionCategory_ID and e.disciplinaryAction_ID=f.disciplinaryAction_ID", "tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_suspension_termination_days d,tbl_dms_disciplinary_action_category e,tbl_dms_disciplinary_action f ");
        $arr_sched_insert = array(3, 8, 4, 5, 7, 9, 12);

        var_dump($rs);
        // $date=explode("_",$rs["dates"][$i]);
        if ($sched != 0) {
            echo "sched is not 0";
            $sch = $this->general_model->fetch_specific_val('acc_time_id,sched_id,schedtype_id', "emp_id=" . $rs["ir_details"]->subjectEmp_ID . " and sched_id=$sched", "tbl_schedule");
            $rs["schedule"][$date] = $sch;

            if (in_array($sch->schedtype_id, $arr_sched_insert)) {
                
                $data_sched['emp_id'] = $rs["ir_details"]->subjectEmp_ID;
                $data_sched['sched_date'] = $date;
                $data_sched['schedtype_id'] = 12;
                $data_sched['remarks'] = "DMS";
                $data_sched['updated_by'] = $_SESSION["uid"];
                $data_sched['updated_on'] = date("Y-m-d H:i:s");
                $data_sched['isLocked'] = 14;
                echo "insert sched";
                var_dump($data_sched);
                $insert_sched = $this->general_model->insert_vals_last_inserted_id($data_sched, 'tbl_schedule');
                $sch_id = $insert_sched;
                $sch_type_id = 12;
                $isInsert = 1;
            } else {
                $sch_id = $sch->sched_id;
                $sch_type_id = $sch->schedtype_id;
                $isInsert = 0;

                $data_sched['schedtype_id'] =  12;
                $data_sched['remarks'] = "DMS";
                $data_sched['isLocked'] =  14;
                $where = "sched_id = " . $sch->sched_id;
                echo "update sched";
                var_dump($data_sched);
                $update_stat =  $this->general_model->update_vals($data_sched,  $where, 'tbl_schedule');
            }
            $suspension_insert = $this->addSusTermDetails($date, $sch_type_id, $sch_id, $stid, $isInsert);
        } else {
            echo "sched is 0";
            $rs["schedule"][$date] = "No Sched; So Insert;";
            $data_sched['emp_id'] = $rs["ir_details"]->subjectEmp_ID;
            $data_sched['sched_date'] = $date;
            $data_sched['schedtype_id'] = 12;
            $data_sched['remarks'] = "DMS";
            $data_sched['updated_by'] = $_SESSION["uid"];
            $data_sched['updated_on'] = date("Y-m-d H:i:s");
            $data_sched['isLocked'] = 14;
                echo "update sched";
                var_dump($data_sched);
            $insert_sched = $this->general_model->insert_vals_last_inserted_id($data_sched, 'tbl_schedule');

            $suspension_insert = $this->addSusTermDetails($date, 12, $insert_sched, $stid, 1);
        }

        // echo json_encode($suspension_insert);
    }
    public function addSusTermDetails($date, $origSchedtype_id, $sched_id, $stid, $isInsert)
    {

        $data['date'] = $date;
        $data['origSchedtype_id'] = $origSchedtype_id;
        $data['sched_id'] = $sched_id;
        $data['dateCreated'] = date("Y-m-d H:i:s");
        $data['suspensionTerminationDays_ID'] = $stid;
        $data['isInsert'] = $isInsert;
        $insert_stat = $this->general_model->insert_vals($data, 'tbl_dms_suspension_dates');
        return $insert_stat;
    }
    public function getIrSusTermDetails()
    {
        $suspensionDates_ID = $this->input->post('id');

        $rs = $this->general_model->fetch_specific_val('suspensionDates_ID,a.date,a.suspensionTerminationDays_ID,origSchedtype_id,a.sched_id,isInsert,subjectEmp_ID', "a.suspensionTerminationDays_ID=b.suspensionTerminationDays_ID and b.incidentReport_ID = c.incidentReport_ID and a.suspensionDates_ID=$suspensionDates_ID", "tbl_dms_suspension_dates a, tbl_dms_suspension_termination_days b, tbl_dms_incident_report c");
        $act = 0;
        $delete_stat = 0;
        if ($rs->isInsert == 1) {
            $where['sched_id'] =  $rs->sched_id;
            $act =  $this->general_model->delete_vals($where, 'tbl_schedule');
        } else {
            $data['schedtype_id'] =  $rs->origSchedtype_id;
            $data['isLocked'] =  0;
            $where = "sched_id = " . $rs->sched_id;
            $act =  $this->general_model->update_vals($data,  $where, 'tbl_schedule');
        }
        if ($act > 0) {
            $where1['suspensionDates_ID'] =  $suspensionDates_ID;
            $delete_stat =  $this->general_model->delete_vals($where1, 'tbl_dms_suspension_dates');
        }
        echo $delete_stat;
    }
    public function getTermSusDetails()
    {
        date_default_timezone_set('Asia/Manila');

        $irid = $this->input->post('irid');
        $rs["ir_details"] = $this->general_model->fetch_specific_val('fname,lname,days,label,a.*,d.suspensionTerminationDays_ID', "a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReport_ID=d.incidentReport_ID and a.incidentReport_ID=$irid and a.finaldisciplinaryActionCategory_ID=e.disciplinaryActionCategory_ID and e.disciplinaryAction_ID=f.disciplinaryAction_ID", "tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_suspension_termination_days d,tbl_dms_disciplinary_action_category e,tbl_dms_disciplinary_action f ");

        $rs["ir_days"] = $this->general_model->fetch_specific_vals('*', "a.suspensionTerminationDays_ID=b.suspensionTerminationDays_ID and a.incidentReport_ID=$irid", "tbl_dms_suspension_termination_days a,tbl_dms_suspension_dates b");

        $rs["now"] = date("Y-m-d");
        $rs["past"] = array();
        foreach ($rs["ir_days"] as $row) {
            if (strtotime($row->date) <= strtotime($rs["now"])) {
                $rs["past"][$row->suspensionDates_ID] = array(
                    "date" => $row->date,
                );
                $dataUpdate['status_id'] =  14;
                $whereUpdate = "suspensionDates_ID = " . $row->suspensionDates_ID;
                $act =  $this->general_model->update_vals($dataUpdate,  $whereUpdate, 'tbl_dms_suspension_dates');
            }
        }

        // $rs["set_susterm_complete"] = count($rs["past"])." ".$rs["ir_details"]->days;
        if (count($rs["past"]) === (int) $rs["ir_details"]->days) {
            $rs["set_susterm_complete"] = "YES";
            $data['status_ID'] =  3;
            $where = "suspensionTerminationDays_ID =" . $rs["ir_details"]->suspensionTerminationDays_ID;
            $update_stat =  $this->general_model->update_vals($data,  $where, 'tbl_dms_suspension_termination_days');
        }
        echo json_encode($rs);
    }
    public function getNumTabsERO()
    {


        $recommendation  = "SELECT count(*) cnt FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_incident_report_stages d,tbl_dms_incident_report_recommendation e,tbl_dms_disciplinary_action_category f,tbl_dms_disciplinary_action g where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReportStages_ID = d.incidentReportStages_ID and a.incidentReport_ID=e.incidentReport_ID and a.finaldisciplinaryActionCategory_ID = f.disciplinaryActionCategory_ID  and  e.emp_ID =" . $_SESSION['emp_id'] . " and e.liabilityStat_ID=4 and e.incidentReportRecommendationType_ID in (2,3) and g.disciplinaryAction_ID=f.disciplinaryAction_ID";
        $query['ero'] =   $this->general_model->custom_query($recommendation);

        $upload  = "SELECT count(*) cnt FROM tbl_dms_necessary_document a,tbl_dms_incident_report b,tbl_employee c,tbl_applicant d where a.incidentReport_ID = b.incidentReport_ID and a.incidentReport_ID and b.subjectEmp_ID=c.emp_id and c.apid=d.apid and eroEmp_ID=" . $_SESSION['emp_id'] . " and a.status_ID=2";
        $query['upload'] =   $this->general_model->custom_query($upload);

        echo json_encode($query);
    }
    public function getNumTabs()
    {
        $explain = "SELECT count(*) cnt  FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_incident_report_stages d where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReportStages_ID = d.incidentReportStages_ID and  d.incidentReportStages_ID= 1 and b.Supervisor =" . $_SESSION['emp_id'];
        $query['explain'] =   $this->general_model->custom_query($explain);

        $recommendation  = "SELECT count(*) cnt FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_incident_report_stages d,tbl_dms_incident_report_recommendation e,tbl_dms_disciplinary_action_category f where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReportStages_ID = d.incidentReportStages_ID and a.incidentReport_ID=e.incidentReport_ID and a.disciplinaryActionCategory_ID = f.disciplinaryActionCategory_ID  and  e.emp_ID =" . $_SESSION['emp_id'] . " and e.liabilityStat_ID=4 and e.incidentReportRecommendationType_ID=1";
        $query['recommendation'] =   $this->general_model->custom_query($recommendation);

        $client = "SELECT count(*) cnt FROM tbl_dms_incident_report a, tbl_dms_client_confirmation b,tbl_employee c,tbl_applicant d,tbl_dms_disciplinary_action_category e where e.disciplinaryActionCategory_ID=a.disciplinaryActionCategory_ID AND a.incidentReport_ID=b.incidentReport_ID AND a.subjectEmp_ID = c.emp_id and d.apid=c.apid AND confirmationStat=4 AND b.directSupEmp_ID=" . $_SESSION['emp_id'];
        $query['client'] =   $this->general_model->custom_query($client);

        $effectivity =  "SELECT count(*) cnt FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_suspension_termination_days d where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReport_ID=d.incidentReport_ID and directSupEmp_ID=" . $_SESSION['emp_id'] . " and status_ID=26 and a.incidentReportStages_ID=3 and a.liabilityStat=17";

        $query['effectivity'] =   $this->general_model->custom_query($effectivity);

        echo json_encode($query);
    }
    public function getAllSubordinates($typ, $involveType = null)
    {

        $datatable = $this->input->post('datatable');
        $group_by_where = "";
        if ($typ == 1) {
            $emplist = $this->getEmployeeSubordinate($_SESSION['uid']);
            // var_dump($emplist);
			$arr_emp_subordinate = array_column($emplist, 'emp_id');
			// var_dump($arr_emp_subordinate);
            $group_by_where = "GROUP BY a.incidentReport_ID";
            $query['query'] = "SELECT c.fname,c.lname,f.action,a.*, COUNT(g.incidentReportWitness_ID) witnessCount FROM tbl_dms_incident_report a INNER JOIN tbl_employee b ON a.subjectEmp_ID = b.emp_id INNER JOIN tbl_applicant c ON b.apid = c.apid INNER JOIN tbl_dms_incident_report_stages d ON a.incidentReportStages_ID = d.incidentReportStages_ID INNER JOIN tbl_dms_disciplinary_action_category e ON a.finaldisciplinaryActionCategory_ID = e.disciplinaryActionCategory_ID INNER JOIN tbl_dms_disciplinary_action f ON e.disciplinaryAction_ID = f.disciplinaryAction_ID LEFT JOIN tbl_dms_incident_report_witness g ON a.incidentReport_ID = g.incidentReport_ID AND g.status_ID = 2 WHERE d.incidentReportStages_ID=1 AND b.emp_id in (".implode(",",$arr_emp_subordinate).")";
        } else if ($typ == 2) {
            // $query['query']="SELECT c.fname,c.lname,a.* FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_incident_report_stages d,tbl_dms_incident_report_recommendation e where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReportStages_ID = d.incidentReportStages_ID and a.incidentReport_ID=e.incidentReport_ID and d.incidentReportStages_ID= 2 and b.Supervisor =".$_SESSION['emp_id']." and e.emp_ID =".$_SESSION['emp_id']." and e.liabilityStat_ID=4";

            $query['query'] = "SELECT c.fname,c.lname,f.level,f.disciplineCategory_ID,a.*,action FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_incident_report_stages d,tbl_dms_incident_report_recommendation e,tbl_dms_disciplinary_action_category f,tbl_dms_disciplinary_action g  where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReportStages_ID = d.incidentReportStages_ID and a.incidentReport_ID=e.incidentReport_ID and a.finaldisciplinaryActionCategory_ID = f.disciplinaryActionCategory_ID  and  e.emp_ID =" . $_SESSION['emp_id'] . " and e.liabilityStat_ID=4 and e.incidentReportRecommendationType_ID=1 and g.disciplinaryAction_ID=f.disciplinaryAction_ID";
        } else if ($typ == 4) {
            $query['query'] = "SELECT fname,lname,days,a.*,action FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_suspension_termination_days d,tbl_dms_disciplinary_action_category f,tbl_dms_disciplinary_action g where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReport_ID=d.incidentReport_ID and directSupEmp_ID=" . $_SESSION['emp_id'] . " and status_ID=26 and a.incidentReportStages_ID=3 and a.liabilityStat=17 and a.finaldisciplinaryActionCategory_ID = f.disciplinaryActionCategory_ID and g.disciplinaryAction_ID=f.disciplinaryAction_ID AND g.label IN ('s', 't')";
        } else if ($typ == 5) {
            $query['query'] = "SELECT c.fname,c.lname,f.level,f.disciplineCategory_ID,e.incidentReportRecommendationType_ID,a.*,action FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_incident_report_stages d,tbl_dms_incident_report_recommendation e,tbl_dms_disciplinary_action_category f,tbl_dms_disciplinary_action g where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReportStages_ID = d.incidentReportStages_ID and a.incidentReport_ID=e.incidentReport_ID and a.finaldisciplinaryActionCategory_ID = f.disciplinaryActionCategory_ID  and  e.emp_ID =" . $_SESSION['emp_id'] . " and e.liabilityStat_ID=4 and e.incidentReportRecommendationType_ID in (2,3) and g.disciplinaryAction_ID=f.disciplinaryAction_ID";
        } else if ($typ == 3) {
            // $query['query']="SELECT c.fname,c.lname,f.level,f.disciplineCategory_ID,a.* FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_incident_report_stages d,tbl_dms_incident_report_recommendation e,tbl_dms_disciplinary_action_category f where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReportStages_ID = d.incidentReportStages_ID and a.incidentReport_ID=e.incidentReport_ID and a.disciplinaryActionCategory_ID = f.disciplinaryActionCategory_ID  and  e.emp_ID =".$_SESSION['emp_id']." and e.liabilityStat_ID=4 and e.incidentReportRecommendationType_ID in (2,3)";
            $query['query'] = "SELECT a.*,d.fname,d.lname,e.level,e.disciplineCategory_ID,action FROM tbl_dms_incident_report a, tbl_dms_client_confirmation b,tbl_employee c,tbl_applicant d,tbl_dms_disciplinary_action_category e,tbl_dms_disciplinary_action f where e.disciplinaryActionCategory_ID=a.finaldisciplinaryActionCategory_ID AND a.incidentReport_ID=b.incidentReport_ID AND a.subjectEmp_ID = c.emp_id and d.apid=c.apid AND confirmationStat=4 and e.disciplinaryAction_ID=f.disciplinaryAction_ID  AND b.directSupEmp_ID=" . $_SESSION['emp_id'];
        } else if ($typ == 6) {
            $query['query'] = "SELECT b.*,a.incidentReportAttachmentPurpose_ID,fname,lname FROM tbl_dms_necessary_document a,tbl_dms_incident_report b,tbl_employee c,tbl_applicant d where a.incidentReport_ID = b.incidentReport_ID and a.incidentReport_ID and b.subjectEmp_ID=c.emp_id and c.apid=d.apid and eroEmp_ID=" . $_SESSION['emp_id'] . " and a.status_ID=2";
        } else if ($typ == 7) {
            // if($involveType=='r'){
            // echo $datatable['query']["start"];
            // }else{
            // echo $datatable['query']["end"];
            // }
            if ($involveType == 'r') {
                $query['query'] = "SELECT DISTINCT(a.incidentReport_ID),c.fname,c.lname,f.level,f.disciplineCategory_ID,a.*,action FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_incident_report_stages d,tbl_dms_incident_report_recommendation e,tbl_dms_disciplinary_action_category f,tbl_dms_disciplinary_action g  where a.subjectEmp_ID = b.emp_id and b.apid=c.apid and a.incidentReportStages_ID = d.incidentReportStages_ID and a.incidentReport_ID=e.incidentReport_ID and a.finaldisciplinaryActionCategory_ID = f.disciplinaryActionCategory_ID and e.emp_ID =" . $_SESSION["emp_id"] . " and a.incidentReportStages_ID=3 and a.incidentDate between '" . $datatable['query']["start"] . "' and '" . $datatable['query']["end"] . "'  and g.disciplinaryAction_ID=f.disciplinaryAction_ID";
            } else {
                $query['query'] = "SELECT c.fname,c.lname,a.*,action FROM tbl_dms_incident_report a,tbl_employee b,tbl_applicant c,tbl_dms_disciplinary_action_category f,tbl_dms_disciplinary_action g where a.subjectEmp_ID=b.emp_id and b.apid=c.apid and a.subjectEmp_ID in (select emp_id from tbl_employee a,tbl_applicant b where a.apid=b.apid and supervisor=" . $_SESSION["emp_id"] . ") and incidentReportStages_ID=3 and a.finaldisciplinaryActionCategory_ID = f.disciplinaryActionCategory_ID and a.incidentDate between '" . $datatable['query']["start"] . "' and '" . $datatable['query']["end"] . "' and g.disciplinaryAction_ID=f.disciplinaryAction_ID";
            }
        }

        if (isset($datatable['query']['ongoingSearch' . $typ]) && $datatable['query']['ongoingSearch' . $typ] != "") {
            $keyword = $datatable['query']['ongoingSearch' . $typ];

            $query['search']['append'] = " and  (a.incidentReport_ID like '%" . (int) $keyword . "%') $group_by_where";

            $query['search']['total'] = " and ( a.incidentReport_ID like '%" . (int) $keyword . "%') $group_by_where";
        }else{
            if ($typ == 1) {
                    $query['search']['append'] = " ".$group_by_where;
                    $query['search']['total'] = " ".$group_by_where;
            }
        }
        $data = $this->set_datatable_query($datatable, $query);
        // $data["acc"] = $appAccess;

        echo json_encode($data);
        // echo json_encode($record);
    }
    // 
    // MJM end code
    //--------------------------------Start of Nicca Arique---------------------------------------

    public function witness_confirmation($ir_id = NULL)
    {
        $emp_id = $this->session->emp_id;
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'DMS Witness Confirmation',
        ];
        $data['incident_report'] = NULL;
        if ($ir_id === NULL || $ir_id === '' || empty($ir_id)) {
            $data['status'] = "NoID";
            $data['status_details'] = "No Incident Report ID provided.";
        } else {
            //kulang ug date,time,place of incident
            $incident_report = $this->general_model->fetch_specific_val('a.liabilityStat,a.subjectEmp_ID,a.sourceEmp_ID,a.subjectPosition,a.subjectEmpStat,a.details,a.offense,e.category,a.incidentDate,a.incidentTime,a.place,c.offenseType', "incidentReport_ID=$ir_id AND b.offenseType_ID=c.offenseType_ID AND b.offense_ID=a.offense_ID AND a.disciplinaryActionCategory_ID=d.disciplinaryActionCategory_ID AND d.disciplineCategory_ID=e.disciplineCategory_ID", "tbl_dms_incident_report a,tbl_dms_offense b,tbl_dms_offense_type c,tbl_dms_disciplinary_action_category d,tbl_dms_discipline_category e");
            if (isset($incident_report->liabilityStat) && ((int) $incident_report->liabilityStat === 2 || (int) $incident_report->liabilityStat === 27)) {
                $ir_witness = $this->general_model->fetch_specific_val('incidentReportWitness_ID,status_ID', "incidentReport_ID=$ir_id AND witnessEmp_ID=$emp_id", "tbl_dms_incident_report_witness");
                if (!isset($ir_witness->incidentReportWitness_ID)) {
                    $data['status'] = 'NotWitness';
                    $data['status_details'] = "You are not a witness to this incident.";
                } else {
                    $statuses = [24, 25]; //status for witness: acknowledge or renounce
                    if (in_array((int) $ir_witness->status_ID, $statuses)) {
                        $data['status'] = 'Confirmed'; // Done acknowledging or renouncing witness
                        $data['status_details'] = "You have already given your acknowledgment to this incident.";
                    }else if((int) $ir_witness->status_ID == 7){
                    	$data['status'] = 'Removed as Witness'; // Done acknowledging or renouncing witness
                        $data['status_details'] = "You are officially removed as witness for this incident.";
                    } else {
                        $employee_details = $this->general_model->fetch_specific_val("a.fname,a.lname,a.pic,c.acc_name", "a.apid=b.apid AND b.acc_id=c.acc_id AND b.emp_id = " . $incident_report->subjectEmp_ID, "tbl_applicant a,tbl_employee b,tbl_account c");
                        $incident_report->subjectFname = $employee_details->fname;
                        $incident_report->subjectLname = $employee_details->lname;
                        $incident_report->pic = $employee_details->pic;
                        $incident_report->subjectAccount = $employee_details->acc_name;

                        $source_details = $this->general_model->fetch_specific_val("a.fname,a.lname,a.pic,c.acc_name", "a.apid=b.apid AND b.acc_id=c.acc_id AND b.emp_id = " . $incident_report->sourceEmp_ID, "tbl_applicant a,tbl_employee b,tbl_account c");
                        $incident_report->src_fname = $source_details->fname;
                        $incident_report->src_lname = $source_details->lname;
                        $incident_report->src_pic = $source_details->pic;
                        $incident_report->src_account = $source_details->acc_name;
                        $evidences = $this->general_model->custom_query("SELECT * FROM tbl_dms_incident_report_attachment a, tbl_dms_incident_report_attachment_purpose b WHERE a.incidentReport_ID=$ir_id AND a.incidentReportAttachmentPurpose_ID=b.incidentReportAttachmentPurpose_ID AND b.purpose = 'evidence'");
                        $myEvidences = [];
                        foreach ($evidences as $evi) {
                            if (strpos($evi->mediaType, 'image') !== false) {
                                $myEvidences['images'][] = $evi;
                            } else if (strpos($evi->mediaType, 'video') !== false) {
                                $myEvidences['videos'][] = $evi;
                            } else if (strpos($evi->mediaType, 'audio') !== false) {
                                $myEvidences['audios'][] = $evi;
                            } else if (strpos($evi->mediaType, 'text') !== false) {
                                $myEvidences['text'][] = $evi;
                            } else {
                                $myEvidences['others'][] = $evi;
                            }
                        }
                        $incident_report->evidences = $myEvidences;
                        $data['incident_report'] = $incident_report;
                        $data['status'] = 'Success';
                        $data['status_details'] = "";
                    }
                }
            } else {
                $data['status'] = "Completed"; //IR is already complete
                $data['status_details'] = "The incident report is already completed.";
            }
        }
        $data['ir_id'] = $ir_id;
        $this->load_template_view('templates/dms/dms_witness_confirmation', $data);
    }

    public function save_witness_confirmation()
    {

        $status_ID = $this->input->post('status_ID');
        $incidentReport_ID = $this->input->post('incidentReport_ID');
        $emp_id = $this->session->emp_id;
        $fname = $this->session->fname;
        $lname = $this->session->lname;
        $res = $this->general_model->update_vals(['status_ID' => $status_ID], "witnessEmp_ID=$emp_id AND incidentReport_ID=$incidentReport_ID", "tbl_dms_incident_report_witness");
        $this->check_witness_confirmation($incidentReport_ID); // check if pending witnesses exist. Added by Mic mic
        if ($res) {
            $source_uid = $this->general_model->fetch_specific_val("b.uid", "a.incidentReport_ID=$incidentReport_ID AND a.sourceEmp_ID=b.emp_id", "tbl_dms_incident_report a,tbl_user b")->uid;
            if ((int) $status_ID === 24) {
                $notif_mssg = "Incident Report has been acknowledged by the witness, $fname $lname .<br><small><b>IR-ID</b>: " . str_pad($incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
            } else {
                $notif_mssg = "Incident Report has been renounced by the witness, $fname $lname .<br><small><b>IR-ID</b>: " . str_pad($incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
            }

            $link = "Discipline/dms_personal"; //LInk should be the IR Involvement page xxx
            $systemNotification_ID = $this->simple_system_notification($notif_mssg, $link, $source_uid, 'dms');
            $status = "Success";
        } else {
            $systemNotification_ID = null;
            $status = "Failed";
        }
        echo json_encode(['status' => $status, 'systemNotification_ID' => $systemNotification_ID]);
    }

    public function get_my_personal_irs()
    {
        $datatable = $this->input->post('datatable');
        $emp_id = $this->session->emp_id;
        $search = array();

        $query['query'] = "SELECT a.*,c.action,e.fname,e.lname FROM tbl_dms_incident_report a,tbl_dms_disciplinary_action_category b,tbl_dms_disciplinary_action c, tbl_employee d, tbl_applicant e WHERE a.subjectEmp_ID=$emp_id AND a.disciplinaryActionCategory_ID=b.disciplinaryActionCategory_ID AND b.disciplinaryAction_Id=c.disciplinaryAction_ID AND a.subjectEmp_ID = d.emp_id AND d.apid=e.apid";

        if (isset($datatable['query']['liabilityStatus']) && $datatable['query']['liabilityStatus'] !== '') {
            $search[] = "liabilityStat IN (" . implode(',', $datatable['query']['liabilityStatus']) . ")";
        }
        if (isset($datatable['query']['incidentDate_start']) && $datatable['query']['incidentDate_start'] !== '' && isset($datatable['query']['incidentDate_end']) && $datatable['query']['incidentDate_end'] !== '') {
            $start = $datatable['query']['incidentDate_start'];
            $end = $datatable['query']['incidentDate_end'];
            $search[] = "incidentDate >= '$start' AND incidentDate <= '$end'";
        }
        if (isset($datatable['query']['myIrs_search']) && $datatable['query']['myIrs_search'] !== '') {
            $keyword = trim($datatable['query']['myIrs_search']);
            $search[] = "(details LIKE '%$keyword%' OR c.action LIKE '%$keyword%' OR offense LIKE '%$keyword%')";
        }
        if (!empty($search)) {
            $searchString = " AND " . implode(" AND ", $search);
            $query['search']['append'] = $searchString;
            $query['search']['total'] = $searchString;
        }

        $data = $this->set_datatable_query($datatable, $query);

        echo json_encode($data);
    }

    public function get_my_involvement_list()
    {
        $datatable = $this->input->post('datatable');
        $emp_id = $this->session->emp_id;
        $search = array();
        if (isset($datatable['query']['involvement_type']) && $datatable['query']['involvement_type'] !== '') {
            if ($datatable['query']['involvement_type'] === 'source') {
                $query['query'] = "SELECT a.*,'source' as customtype,c.action,'' as witness_status,e.fname,e.lname FROM tbl_dms_incident_report a,tbl_dms_disciplinary_action_category b,tbl_dms_disciplinary_action c, tbl_employee d, tbl_applicant e WHERE sourceEmp_ID = $emp_id AND a.disciplinaryActionCategory_ID=b.disciplinaryActionCategory_ID AND b.disciplinaryAction_Id=c.disciplinaryAction_ID AND a.liabilityStat!=27 AND a.subjectEmp_ID=d.emp_id AND d.apid=e.apid";
            } else {
                $query['query'] = "SELECT x.*,'witness' as customtype,z2.action,y.status_ID as witness_status,e2.fname,e2.lname FROM tbl_dms_incident_report x,tbl_dms_incident_report_witness y,tbl_dms_disciplinary_action_category z1,tbl_dms_disciplinary_action z2, tbl_employee d2, tbl_applicant e2 WHERE y.witnessEmp_ID = $emp_id AND x.incidentReport_ID = y.incidentReport_ID AND x.disciplinaryActionCategory_ID=z1.disciplinaryActionCategory_ID AND z1.disciplinaryAction_Id=z2.disciplinaryAction_ID AND x.subjectEmp_ID = d2.emp_id AND d2.apid=e2.apid";
            }

            $search_connector = "AND";
        } else {
            $query['query'] = "SELECT * FROM ((SELECT a.*,'source' as customtype,c.action,'' as witness_status,e.fname,e.lname FROM tbl_dms_incident_report a,tbl_dms_disciplinary_action_category b,tbl_dms_disciplinary_action c, tbl_employee d, tbl_applicant e WHERE sourceEmp_ID = $emp_id AND a.disciplinaryActionCategory_ID=b.disciplinaryActionCategory_ID AND b.disciplinaryAction_Id=c.disciplinaryAction_ID AND a.liabilityStat!=27 AND a.subjectEmp_ID=d.emp_id AND d.apid=e.apid) UNION (SELECT x.*,'witness' as customtype,z2.action,y.status_ID as witness_status,e2.fname,e2.lname FROM tbl_dms_incident_report x,tbl_dms_incident_report_witness y,tbl_dms_disciplinary_action_category z1,tbl_dms_disciplinary_action z2, tbl_employee d2, tbl_applicant e2 WHERE  y.witnessEmp_ID = $emp_id AND x.incidentReport_ID = y.incidentReport_ID AND x.disciplinaryActionCategory_ID=z1.disciplinaryActionCategory_ID AND z1.disciplinaryAction_Id=z2.disciplinaryAction_ID AND x.subjectEmp_ID = d2.emp_id AND d2.apid=e2.apid)) as result";
            $search_connector = "WHERE";
        }
        if (isset($datatable['query']['incidentDate_start']) && $datatable['query']['incidentDate_start'] !== '' && isset($datatable['query']['incidentDate_end']) && $datatable['query']['incidentDate_end'] !== '') {
            $start = $datatable['query']['incidentDate_start'];
            $end = $datatable['query']['incidentDate_end'];
            $search[] = "incidentDate >= '$start' AND incidentDate <= '$end'";
        }
        if (isset($datatable['query']['myInvolvement_search']) && $datatable['query']['myInvolvement_search'] !== '') {
            $keyword = trim($datatable['query']['myInvolvement_search']);
            $search[] = "(details LIKE '%$keyword%' OR action LIKE '%$keyword%' OR offense LIKE '%$keyword%')";
        }


        if (!empty($search)) {
            $searchString = " $search_connector " . implode(" AND ", $search);
            $query['search']['append'] = $searchString;
            $query['search']['total'] = $searchString;
        }

        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode($data);
    }

    public function downloadPdf_nte($ir_id)
    {
        $this->load->library('m_pdf');
        $details = $this->general_model->fetch_specific_val('a.occurence, a.prescriptiveId,a.incidentReport_ID,f.description,a.liabilityStat,a.subjectEmp_ID,a.details,a.offense,e.category,a.incidentDate,a.incidentTime,a.place,c.offenseType,a.expectedAction,a.subjectExplanation,a.sourceEmp_ID,a.explanationDate,a.datedLiabilityStat,a.subjectPosition,a.subjectAccount, g.action suggestedAction, g.abbreviation suggestedAbbreviation , h.action finalAction, h.abbreviation finalAbbreviation, a.prescriptionEnd', "incidentReport_ID=$ir_id AND b.offenseType_ID=c.offenseType_ID AND b.offense_ID=a.offense_ID AND h.disciplinaryAction_ID = i.disciplinaryAction_ID AND i.disciplinaryActionCategory_ID = a.finaldisciplinaryActionCategory_ID AND a.disciplinaryActionCategory_ID=d.disciplinaryActionCategory_ID AND d.disciplineCategory_ID=e.disciplineCategory_ID AND f.status_ID=a.liabilityStat", "tbl_dms_incident_report a,tbl_dms_offense b,tbl_dms_offense_type c,tbl_dms_disciplinary_action_category d,tbl_dms_discipline_category e,tbl_status f, tbl_dms_disciplinary_action g, tbl_dms_disciplinary_action h, tbl_dms_disciplinary_action_category i");
        $subject = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.Supervisor,b.signature", "a.apid=b.apid AND b.emp_id = " . $details->subjectEmp_ID, "tbl_applicant a,tbl_employee b");
        $details->subjectName = $subject->name;
        $details->subjectSignature = $subject->signature;
        $supervisor = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.signature", "a.apid=b.apid AND b.emp_id = " . $subject->Supervisor, "tbl_applicant a,tbl_employee b");
        $details->supervisorName = $supervisor->name;
        $details->supervisorSignature = $supervisor->signature;
        $source = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.signature", "a.apid=b.apid AND b.emp_id = " . $details->sourceEmp_ID, "tbl_applicant a,tbl_employee b");
        $details->sourceName = $source->name;
        $details->sourceSignature = $source->signature;
        $finalRecommender_id = $this->general_model->fetch_specific_val("emp_ID", "incidentReport_ID=$ir_id AND incidentReportRecommendationType_ID=2", "tbl_dms_incident_report_recommendation");
        $finalRecommender = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.signature", "a.apid=b.apid AND b.emp_id = " . $finalRecommender_id->emp_ID, "tbl_applicant a,tbl_employee b");
        $details->finalRecommender = $finalRecommender->name;
        $details->finalRecommenderSignature = $finalRecommender->signature;
        $data['history'] = $this->qry_ir_history($ir_id, $details->prescriptiveId, $details->occurence);
        $data['details'] = $details;
        $html = $this->load->view('templates/dms/nte_pdf', $data, true);
        $pdfFilePath = "SZ-DMS-NTE.pdf";

        try {
            $pdf = $this->m_pdf->load();
            $pdf->SetHTMLHeader('<img src="assets/images/img/logo_header.png"/>');
            $pdf->SetHTMLFooter('<img src="assets/images/img/logo_footer.png"/>');
            // $pdf->debug = true;
            $pdf->SetTitle('Notice to Explain');
            $pdf->WriteHTML($html);
            header("Content-type:application/pdf");
            header("Content-Disposition:inline;filename='" . $pdfFilePath . "'");
            $pdf->Output($pdfFilePath, "I");
        } catch (\Mpdf\MpdfException $e) {
            echo $e->getMessage();
        }
        exit;
    }

    public function downloadPdf_nor($ir_id)
    {
        $this->load->library('m_pdf');
        $details = $this->general_model->fetch_specific_val('a.prescriptiveId,a.incidentReport_ID,f.description,a.liabilityStat,a.subjectEmp_ID,a.details,a.offense,e.category,a.incidentDate,a.incidentTime,a.place,c.offenseType,a.expectedAction,a.subjectExplanation,a.sourceEmp_ID,a.explanationDate,a.datedLiabilityStat,a.subjectPosition,a.subjectAccount', "incidentReport_ID=$ir_id AND b.offenseType_ID=c.offenseType_ID AND b.offense_ID=a.offense_ID AND a.disciplinaryActionCategory_ID=d.disciplinaryActionCategory_ID AND d.disciplineCategory_ID=e.disciplineCategory_ID AND f.status_ID=a.liabilityStat", "tbl_dms_incident_report a,tbl_dms_offense b,tbl_dms_offense_type c,tbl_dms_disciplinary_action_category d,tbl_dms_discipline_category e,tbl_status f");
        $subject = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.Supervisor,b.signature", "a.apid=b.apid AND b.emp_id = " . $details->subjectEmp_ID, "tbl_applicant a,tbl_employee b");
        $details->subjectName = $subject->name;
        $details->subjectSignature = $subject->signature;
        $supervisor = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.signature", "a.apid=b.apid AND b.emp_id = " . $subject->Supervisor, "tbl_applicant a,tbl_employee b");
        $details->supervisorName = $supervisor->name;
        $details->supervisorSignature = $supervisor->signature;
        $source = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.signature", "a.apid=b.apid AND b.emp_id = " . $details->sourceEmp_ID, "tbl_applicant a,tbl_employee b");
        $details->sourceName = $source->name;
        $details->sourceSignature = $source->signature;
        $finalRecommender_id = $this->general_model->fetch_specific_val("emp_ID", "incidentReport_ID=$ir_id AND incidentReportRecommendationType_ID=2", "tbl_dms_incident_report_recommendation");
        $finalRecommender = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.signature", "a.apid=b.apid AND b.emp_id = " . $finalRecommender_id->emp_ID, "tbl_applicant a,tbl_employee b");
        $details->finalRecommender = $finalRecommender->name;
        $details->finalRecommenderSignature = $finalRecommender->signature;
        $data['attachmentPurpose'] = $this->general_model->fetch_specific_val("incidentReportAttachmentPurpose_ID as purpose", "incidentReport_ID=$ir_id AND incidentReportAttachmentPurpose_ID IN (3,5)", "tbl_dms_incident_report_attachment");
        $data['suspension'] = $this->general_model->fetch_specific_val("days", "incidentReport_ID=$ir_id AND status_ID=26", "tbl_dms_suspension_termination_days");
        $data['details'] = $details;
        $html = $this->load->view('templates/dms/nor_pdf', $data, true);
        $pdfFilePath = "SZ-DMS-NOR.pdf";
        try {
            $pdf = $this->m_pdf->load();
            $pdf->SetHTMLHeader('<img src="assets/images/img/logo_header.png"/>');
            $pdf->SetHTMLFooter('<img src="assets/images/img/logo_footer.png"/>');
            // $pdf->debug = true;
            $pdf->SetTitle('Notice of Resolution');
            $pdf->WriteHTML($html, 2);
            header("Content-type:application/pdf");
            header("Content-Disposition:inline;filename='" . $pdfFilePath . "'");
            $pdf->Output($pdfFilePath, "I");
        } catch (\Mpdf\MpdfException $e) {
            echo $e->getMessage();
        }
        exit;
    }

    public function downloadPdf_ir($ir_id)
    {
        $this->load->library('m_pdf');
        $details = $this->general_model->fetch_specific_val('a.occurence, a.prescriptiveId,a.incidentReport_ID,f.description,a.liabilityStat,a.subjectEmp_ID,a.details,a.offense,e.category,a.incidentDate,a.incidentTime,a.place,c.offenseType,a.expectedAction,a.subjectExplanation,a.sourceEmp_ID,a.explanationDate,a.datedLiabilityStat,a.subjectExplanationStat, g.action suggestedAction, g.abbreviation suggestedAbbreviation , h.action finalAction, h.abbreviation finalAbbreviation, a.prescriptionEnd', "incidentReport_ID=$ir_id AND b.offenseType_ID=c.offenseType_ID AND b.offense_ID=a.offense_ID AND h.disciplinaryAction_ID = i.disciplinaryAction_ID AND i.disciplinaryActionCategory_ID = a.finaldisciplinaryActionCategory_ID AND g.disciplinaryAction_ID = d.disciplinaryAction_ID AND a.disciplinaryActionCategory_ID=d.disciplinaryActionCategory_ID AND d.disciplineCategory_ID=e.disciplineCategory_ID AND f.status_ID=a.liabilityStat", "tbl_dms_incident_report a,tbl_dms_offense b,tbl_dms_offense_type c,tbl_dms_disciplinary_action_category d,tbl_dms_discipline_category e,tbl_status f, tbl_dms_disciplinary_action g, tbl_dms_disciplinary_action h, tbl_dms_disciplinary_action_category i");
        $subject = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.Supervisor,b.signature", "a.apid=b.apid AND b.emp_id = " . $details->subjectEmp_ID, "tbl_applicant a,tbl_employee b");
        $details->subjectName = $subject->name;
        $details->subjectSignature = $subject->signature;
        $supervisor = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.signature", "a.apid=b.apid AND b.emp_id = " . $subject->Supervisor, "tbl_applicant a,tbl_employee b");
        $details->supervisorName = $supervisor->name;
        $details->supervisorSignature = $supervisor->signature;
        $source = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.signature", "a.apid=b.apid AND b.emp_id = " . $details->sourceEmp_ID, "tbl_applicant a,tbl_employee b");
        $details->sourceName = $source->name;
        $details->sourceSignature = $source->signature;
        $recommenders = $this->general_model->fetch_specific_vals("CONCAT(a.fname,' ',a.lname) as name, level,b.signature,c.liabilityStat_ID,c.datedLiabilityStat", "incidentReport_ID=$ir_id AND incidentReportRecommendationType_ID=1 AND a.apid=b.apid AND b.emp_id=c.emp_ID", "tbl_applicant a,tbl_employee b, tbl_dms_incident_report_recommendation c", "c.level ASC");
        $finalRecommender_id = $this->general_model->fetch_specific_val("emp_ID,liabilityStat_ID,datedLiabilityStat", "incidentReport_ID=$ir_id AND incidentReportRecommendationType_ID=2", "tbl_dms_incident_report_recommendation");
        $finalRecommender = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.lname) as name,b.signature", "a.apid=b.apid AND b.emp_id = " . $finalRecommender_id->emp_ID, "tbl_applicant a,tbl_employee b");

        $details->finalRecommender = $finalRecommender->name;
        $details->finalRecommenderSignature = $finalRecommender->signature;
        $details->finalRecommenderLiabilityStat_ID = $finalRecommender_id->liabilityStat_ID;
        $details->finalRecommenderDatedLiabilityStat = $finalRecommender_id->datedLiabilityStat;
        $data['history'] = $this->qry_ir_history($ir_id, $details->prescriptiveId, $details->occurence);
        $data['recommenders'] = $recommenders;
        $data['details'] = $details;
        $data['witnesses'] = $this->general_model->custom_query("SELECT CONCAT(a.fname,' ',a.lname) as name,b.signature,c.status_ID,c.datedStatus FROM tbl_applicant a,tbl_employee b, tbl_dms_incident_report_witness c WHERE a.apid=b.apid AND b.emp_id = c.witnessEmp_ID AND c.incidentReport_ID = $ir_id");
        $html = $this->load->view('templates/dms/ir_pdf', $data, true);

        // echo "<pre>";
        // var_dump($details);

        // echo "</pre>";
        $pdfFilePath = "SZ-DMS-IR.pdf";
        try {
            $pdf = $this->m_pdf->load();
            $pdf->SetHTMLHeader('<img src="assets/images/img/logo_header.png"/>');
            $pdf->SetHTMLFooter('<img src="assets/images/img/logo_footer.png"/>');
            // $pdf->debug = true;
            $pdf->SetTitle('Incident Report');
            $pdf->WriteHTML($html, 2);
            header("Content-type:application/pdf");
            header("Content-Disposition:inline;filename='" . $pdfFilePath . "'");
            $pdf->Output($pdfFilePath, "I");
        } catch (\Mpdf\MpdfException $e) {
            echo $e->getMessage();
        }
        exit;
    }
    public function get_dashboard_dtr_system_violations()
    {
        $accessType = $this->input->post('accessType'); //isERO
        $incidentDate_start = $this->input->post('incidentDate_start');
        $incidentDate_end = $this->input->post('incidentDate_end');
        $violation_search = $this->input->post('violation_search');
        $violation_class = $this->input->post('violation_class');
        $violation_accounts = $this->input->post('violation_accounts');
        $violation_type = $this->input->post('violation_type');
        if ($incidentDate_start !== '' && $incidentDate_end !== '') {
            $start = $incidentDate_start;
            $end = $incidentDate_end;
            $search[] = "a.dateRecord >= '$start' AND a.dateRecord <= '$end'";
        }
        if ($violation_search !== '') {
            $keyword = trim($violation_search);
            $search[] = "(e.fname LIKE '%$keyword%' OR e.lname LIKE '%$keyword%')";
        }
        if ($violation_class !== '') {
            $search[] = "f.acc_description='" . $violation_class . "'";
        }

        if ($violation_type !== '') {
            $search[] = "a.dtrViolationType_ID=" . $violation_type;
        }
        if ($violation_accounts !== '') {
            $search[] = "d.acc_id=" . $violation_accounts;
        }
        if ($accessType === 'ERO') {
            $eroUnder = $this->general_model->custom_query("SELECT acc_id,site_ID FROM tbl_dms_employee_relations_officer WHERE emp_id = " . $this->session->emp_id);
            $ero_acc_ids = array();
            $ero_site_ids = array();
            foreach ($eroUnder as $under) {
                $ero_acc_ids[] = $under->acc_id;
                $ero_site_ids[] = $under->site_ID;
            }
            if (empty($ero_acc_ids)) {
                $search[] = "d.acc_id IN (0)";
            } else {
                $search[] = "d.acc_id IN (" . implode(",", $ero_acc_ids) . ")";
            }

            if (empty($ero_site_ids)) {
                $search[] = "u.site_ID IN (0)";
            } else {
                $search[] = "u.site_ID IN (" . implode(",", $ero_site_ids) . ")";
            }
            $search[] = "s.isActive=1";

            $query['query'] = "SELECT a.dateRecord,a.sched_ID,a.userDtrViolation_ID,a.dtrViolationType_ID,month,year,user_ID,b.description,e.fname,e.lname,g.sched_date FROM tbl_user_dtr_violation a INNER JOIN tbl_dtr_violation_type b ON a.dtrViolationType_ID=b.dtrViolationType_ID INNER JOIN tbl_user c ON a.user_ID=c.uid INNER JOIN tbl_employee d ON c.emp_id=d.emp_id INNER JOIN tbl_applicant e ON d.apid=e.apid INNER JOIN tbl_account f ON d.acc_id=f.acc_id INNER JOIN tbl_emp_promote s ON d.emp_id=s.emp_id INNER JOIN tbl_pos_emp_stat t ON s.posempstat_id=t.posempstat_id INNER JOIN tbl_position u ON t.pos_id=u.pos_id LEFT JOIN tbl_schedule g ON a.sched_ID=g.sched_id";
        } else {
            $query['query'] = "SELECT a.dateRecord,a.sched_ID,a.userDtrViolation_ID,a.dtrViolationType_ID,month,year,user_ID,b.description,e.fname,e.lname,g.sched_date FROM tbl_user_dtr_violation a INNER JOIN  tbl_dtr_violation_type b ON a.dtrViolationType_ID=b.dtrViolationType_ID INNER JOIN tbl_user c ON  a.user_ID=c.uid  INNER JOIN tbl_employee d ON  c.emp_id=d.emp_id INNER JOIN tbl_applicant e ON  d.apid=e.apid INNER JOIN tbl_account f ON d.acc_id=f.acc_id LEFT JOIN tbl_schedule g ON a.sched_ID=g.sched_id";
        }

        $search[] = "b.isActive=1";
        $searchString = "";
        if ($accessType === 'Supervisor') {
            $subordinates = $this->getEmployeeSubordinate($this->session->uid);
            $uids = array();
            foreach ($subordinates as $sub) {
                $uids[] = $sub->uid;
            }
            if (empty($uids)) {
                $uids[] = 0;
            }
            $search[] = "a.user_ID IN (" . implode(',', $uids) . ")";
        } else if ($accessType === 'Personal') {
            $search[] = "a.user_ID = " . $this->session->uid;
        } else if ($accessType === "Monitoring") { }


        $search[] = "b.isActive = 1";
        if (!empty($search)) {
            $searchString = " WHERE " . implode(" AND ", $search);
        }

        // $dtrViolationTypes = $this->general_model->fetch_all("dtrViolationType_ID,description", "tbl_dtr_violation_type", "description ASC");
        $dtrViolationTypes = $this->general_model->fetch_specific_vals("dtrViolationType_ID,description", "isActive=1", "tbl_dtr_violation_type",  "description ASC");
        if ($accessType === 'ERO') {
            $dtrViolations = $this->general_model->custom_query("SELECT a.userDtrViolation_ID,a.dtrViolationType_ID FROM tbl_user_dtr_violation a INNER JOIN  tbl_dtr_violation_type b ON a.dtrViolationType_ID=b.dtrViolationType_ID INNER JOIN tbl_user c ON  a.user_ID=c.uid  INNER JOIN tbl_employee d ON  c.emp_id=d.emp_id INNER JOIN tbl_applicant e ON  d.apid=e.apid INNER JOIN tbl_account f ON d.acc_id=f.acc_id INNER JOIN tbl_emp_promote s ON d.emp_id=s.emp_id INNER JOIN tbl_pos_emp_stat t ON s.posempstat_id=t.posempstat_id INNER JOIN tbl_position u ON t.pos_id=u.pos_id  $searchString");
        } else {
            $dtrViolations = $this->general_model->custom_query("SELECT a.userDtrViolation_ID,a.dtrViolationType_ID FROM tbl_user_dtr_violation a INNER JOIN  tbl_dtr_violation_type b ON a.dtrViolationType_ID=b.dtrViolationType_ID INNER JOIN tbl_user c ON  a.user_ID=c.uid  INNER JOIN tbl_employee d ON  c.emp_id=d.emp_id INNER JOIN tbl_applicant e ON  d.apid=e.apid INNER JOIN tbl_account f ON d.acc_id=f.acc_id  $searchString");
        }
        $total = 0;
        foreach ($dtrViolationTypes as $type) {
            $type->count = 0;
            foreach ($dtrViolations as $violation) {
                if ($type->dtrViolationType_ID === $violation->dtrViolationType_ID) {
                    $type->count = $type->count + 1;
                    $total = $total + 1;
                }
            }
        }
        echo json_encode(['violationsTypes' => $dtrViolationTypes, 'dtrViolations' => $dtrViolations, 'total' => $total]);
    }
    private function get_dtr_system_violations_general($datatable, $isERO, $search = [])
    {
        if (isset($datatable['query']['incidentDate_start']) && $datatable['query']['incidentDate_start'] !== '' && isset($datatable['query']['incidentDate_end']) && $datatable['query']['incidentDate_end'] !== '') {
            $start = $datatable['query']['incidentDate_start'];
            $end = $datatable['query']['incidentDate_end'];
            $search[] = "a.dateRecord >= '$start' AND a.dateRecord <= '$end'";
        }
        if (isset($datatable['query']['violation_search']) && $datatable['query']['violation_search'] !== '') {
            $keyword = trim($datatable['query']['violation_search']);
            $search[] = "(e.fname LIKE '%$keyword%' OR e.lname LIKE '%$keyword%')";
        }
        if (isset($datatable['query']['violation_class']) && $datatable['query']['violation_class'] !== '') {
            $search[] = "f.acc_description='" . $datatable['query']['violation_class'] . "'";
        }

        if (isset($datatable['query']['violation_type']) && $datatable['query']['violation_type'] !== '') {
            $search[] = "a.dtrViolationType_ID=" . $datatable['query']['violation_type'];
        }
        if (isset($datatable['query']['violation_accounts']) && $datatable['query']['violation_accounts'] !== '') {
            $search[] = "d.acc_id=" . $datatable['query']['violation_accounts'];
        }
        if ($isERO) {
            $eroUnder = $this->general_model->custom_query("SELECT acc_id,site_ID FROM tbl_dms_employee_relations_officer WHERE emp_id = " . $this->session->emp_id);
            $ero_acc_ids = array();
            $ero_site_ids = array();
            foreach ($eroUnder as $under) {
                $ero_acc_ids[] = $under->acc_id;
                $ero_site_ids[] = $under->site_ID;
            }
            if (empty($ero_acc_ids)) {
                $search[] = "d.acc_id IN (0)";
            } else {
                $search[] = "d.acc_id IN (" . implode(",", $ero_acc_ids) . ")";
            }

            if (empty($ero_site_ids)) {
                $search[] = "u.site_ID IN (0)";
            } else {
                $search[] = "u.site_ID IN (" . implode(",", $ero_site_ids) . ")";
            }
            $search[] = "s.isActive=1";

            $query['query'] = "SELECT a.dateRecord,a.sched_ID,a.userDtrViolation_ID,a.dtrViolationType_ID,month,year,user_ID,b.description,e.fname,e.lname,g.sched_date FROM tbl_user_dtr_violation a INNER JOIN tbl_dtr_violation_type b ON a.dtrViolationType_ID=b.dtrViolationType_ID INNER JOIN tbl_user c ON a.user_ID=c.uid INNER JOIN tbl_employee d ON c.emp_id=d.emp_id INNER JOIN tbl_applicant e ON d.apid=e.apid INNER JOIN tbl_account f ON d.acc_id=f.acc_id INNER JOIN tbl_emp_promote s ON d.emp_id=s.emp_id INNER JOIN tbl_pos_emp_stat t ON s.posempstat_id=t.posempstat_id INNER JOIN tbl_position u ON t.pos_id=u.pos_id LEFT JOIN tbl_schedule g ON a.sched_ID=g.sched_id";
        } else {
            $query['query'] = "SELECT a.dateRecord,a.sched_ID,a.userDtrViolation_ID,a.dtrViolationType_ID,month,year,user_ID,b.description,e.fname,e.lname,g.sched_date FROM tbl_user_dtr_violation a INNER JOIN  tbl_dtr_violation_type b ON a.dtrViolationType_ID=b.dtrViolationType_ID INNER JOIN tbl_user c ON  a.user_ID=c.uid  INNER JOIN tbl_employee d ON  c.emp_id=d.emp_id INNER JOIN tbl_applicant e ON  d.apid=e.apid INNER JOIN tbl_account f ON d.acc_id=f.acc_id LEFT JOIN tbl_schedule g ON a.sched_ID=g.sched_id";
        }

        $search[] = "b.isActive=1";
        if (!empty($search)) {
            $searchString = " WHERE " . implode(" AND ", $search);
            $query['search']['append'] = $searchString;
            $query['search']['total'] = $searchString;
        }
        $data = $this->set_datatable_query($datatable, $query);
        return $data;
    }
    public function get_tabular_dtr_system_violations()
    {
        $datatable = $this->input->post('datatable');
        $data = $this->get_dtr_system_violations_general($datatable, true);
        echo json_encode($data);
    }
    public function get_monitoring_dtr_system_violations()
    {
        $datatable = $this->input->post('datatable');
        $search = array();
        $data = $this->get_dtr_system_violations_general($datatable, false, $search);
        echo json_encode($data);
    }
    public function get_supervisors_dtr_system_violations()
    {
        $datatable = $this->input->post('datatable');
        $subordinates = $this->getEmployeeSubordinate($this->session->uid);
        $uids = array();
        foreach ($subordinates as $sub) {
            $uids[] = $sub->uid;
        }
        if (empty($uids)) {
            $uids[] = 0;
        }
        $search[] = "a.user_ID IN (" . implode(',', $uids) . ")";
        $data = $this->get_dtr_system_violations_general($datatable, false, $search);
        echo json_encode($data);
    }

    public function get_personal_dtr_system_violations()
    {
        $datatable = $this->input->post('datatable');
        $search[] = "a.user_ID = " . $this->session->uid;
        $data = $this->get_dtr_system_violations_general($datatable, false, $search);
        echo json_encode($data);
    }

    public function get_ero_accounts()
    {
        $data = $this->general_model->custom_query("SELECT DISTINCT(a.acc_id),a.acc_name,a.acc_description FROM tbl_account a,tbl_dms_employee_relations_officer b WHERE a.acc_id=b.acc_id AND emp_id =" . $this->session->emp_id);
        echo json_encode(array('accounts' => $data));
    }
    public function get_violation_types()
    {
        // $dtrViolationTypes = $this->general_model->fetch_all("dtrViolationType_ID,description", "tbl_dtr_violation_type", "description ASC");
        $dtrViolationTypes =   $this->general_model->fetch_specific_vals("dtrViolationType_ID,description", "isActive=1", "tbl_dtr_violation_type",  "description ASC");
        echo json_encode(array('violationTypes' => $dtrViolationTypes));
    }

    public function get_unique_accounts_subordinates()
    {
        $uid = $this->session->userdata('uid');
        $acc_ids = array();

        $subordinates = $this->getEmployeeSubordinate($uid);
        $empids = array();
        foreach ($subordinates as $s) {
            $empids[] = $s->emp_id;
        }
        if (empty($empids)) {
            $query = "SELECT acc_id FROM tbl_employee  WHERE isActive='yes' AND emp_id IN (0)";
        } else {
            $query = "SELECT acc_id FROM tbl_employee WHERE isActive='yes' AND emp_id IN (" . implode(",", $empids) . ")";
        }
        $account_ids = $this->general_model->custom_query($query);
        foreach ($account_ids as $acc_id) {
            if ($acc_id->acc_id !== NULL) {
                $acc_ids[] = $acc_id->acc_id;
            }
        }
        if (empty($acc_ids)) {
            $accounts = $this->general_model->fetch_specific_vals("acc_id,acc_name,acc_description", "acc_id IN (0)", "tbl_account", "acc_name ASC");
        } else {
            $unique_acc_ids = array_unique($acc_ids);
            $accounts = $this->general_model->fetch_specific_vals("acc_id,acc_name,acc_description", "acc_id IN (" . implode(",", $unique_acc_ids) . ")", "tbl_account", "acc_name ASC");
        }

        echo json_encode(array('accounts' => $accounts));
    }
    public function ir_details_myirs()
    {
        $ir_id = $this->input->post('ir_id');
        $details = $this->get_layouted_ir_details($ir_id, 1, 1, 0, 1);
        echo json_encode($details);
    }
    public function ir_details_myinvolvement_witness()
    {
        $ir_id = $this->input->post('ir_id');
        $details = $this->get_layouted_ir_details($ir_id, 1, 0, 0, 1);
        echo json_encode($details);
    }
    public function ir_details_myinvolvement_source()
    {
        $ir_id = $this->input->post('ir_id');
        $details = $this->get_layouted_ir_details($ir_id, 1, 1, 1, 1, 1, 1);
        echo json_encode($details);
    }

    public function downloadExcel_dtrSystemViolations()
    {
        $searchString = "";
        $search = array();
        $incidentDate_start = $this->input->post('incidentDate_start');
        $incidentDate_end = $this->input->post('incidentDate_end');
        $violation_search = $this->input->post('violation_search');
        $violation_class = $this->input->post('violation_class');
        $violation_type = $this->input->post('violation_type');
        $violation_accounts = $this->input->post('violation_accounts');
        if ($incidentDate_start !== '' &&  $incidentDate_end !== '') {
            $search[] = "a.dateRecord >= '$incidentDate_start' AND a.dateRecord <= '$incidentDate_end'";
        }
        if ($violation_search !== '') {
            $keyword = trim($violation_search);
            $search[] = "(e.fname LIKE '%$keyword%' OR e.lname LIKE '%$keyword%')";
        }
        if ($violation_class !== '') {
            $search[] = "f.acc_description='" . $violation_class . "'";
        }

        if ($violation_type !== '') {
            $search[] = "a.dtrViolationType_ID=" . $violation_type;
        }
        if ($violation_accounts !== '') {
            $search[] = "d.acc_id=" . $violation_accounts;
        }
        $eroUnder = $this->general_model->custom_query("SELECT acc_id,site_ID FROM tbl_dms_employee_relations_officer WHERE emp_id = " . $this->session->emp_id);
        $ero_acc_ids = array();
        $ero_site_ids = array();
        foreach ($eroUnder as $under) {
            $ero_acc_ids[] = $under->acc_id;
            $ero_site_ids[] = $under->site_ID;
        }
        if (empty($ero_acc_ids)) {
            $search[] = "d.acc_id IN (0)";
        } else {
            $search[] = "d.acc_id IN (" . implode(",", $ero_acc_ids) . ")";
        }

        if (empty($ero_site_ids)) {
            $search[] = "u.site_ID IN (0)";
        } else {
            $search[] = "u.site_ID IN (" . implode(",", $ero_site_ids) . ")";
        }
        $search[] = "s.isActive=1";
        $search[] = "b.isActive=1";

        if (!empty($search)) {
            $searchString = " WHERE " . implode(" AND ", $search);
        }


        $query = "SELECT a.shift,a.dateRecord,a.sched_ID,a.userDtrViolation_ID,a.dtrViolationType_ID,month,year,user_ID,b.description,e.fname,e.lname,g.sched_date FROM tbl_user_dtr_violation a INNER JOIN tbl_dtr_violation_type b ON a.dtrViolationType_ID=b.dtrViolationType_ID INNER JOIN tbl_user c ON a.user_ID=c.uid INNER JOIN tbl_employee d ON c.emp_id=d.emp_id INNER JOIN tbl_applicant e ON d.apid=e.apid INNER JOIN tbl_account f ON d.acc_id=f.acc_id INNER JOIN tbl_emp_promote s ON d.emp_id=s.emp_id INNER JOIN tbl_pos_emp_stat t ON s.posempstat_id=t.posempstat_id INNER JOIN tbl_position u ON t.pos_id=u.pos_id LEFT JOIN tbl_schedule g ON a.sched_ID=g.sched_id $searchString";
        $systemViolations = $this->general_model->custom_query($query);


        //EXCEL PROPER
        $this->load->library('PHPExcel', NULL, 'excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('DTR System Violations');
        $this->excel->getActiveSheet()->setShowGridlines(false);
        //------------------------INSERT LOGO-------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(5);  //setOffsetY has no effect
        $objDrawing->setCoordinates('B1');
        $objDrawing->setHeight(80); // logo height
        // $objDrawing->setWidth(320); // logo width
        // $objDrawing->setWidthAndHeight(200,400);
        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
        //set cell A1 content with some text

        $this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(FALSE);
        $this->excel->getActiveSheet()->setCellValue('A1', 'CODE-SZ_DTRSystemViolationExport');
        $this->excel->getActiveSheet()->setCellValue('A3', 'Violation ID');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Violation Type');
        $this->excel->getActiveSheet()->setCellValue('C3', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Schedule Date');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Shift');
        $this->excel->getActiveSheet()->setCellValue('G3', 'Date Recorded');

        $emprow = 4;
        foreach ($systemViolations as $violation) {
            $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $violation->userDtrViolation_ID);
            $this->excel->getActiveSheet()->setCellValue('B' . $emprow, ucwords($violation->description));
            $this->excel->getActiveSheet()->setCellValue('C' . $emprow, $violation->fname);
            $this->excel->getActiveSheet()->setCellValue('D' . $emprow, $violation->lname);
            $this->excel->getActiveSheet()->setCellValue('E' . $emprow, $violation->sched_date);
            $this->excel->getActiveSheet()->setCellValue('F' . $emprow, $violation->shift);
            $this->excel->getActiveSheet()->setCellValue('G' . $emprow, date('M d, Y h:i:s a', strtotime($violation->dateRecord)));
            $emprow++;
        }


        $column = $this->excel->getActiveSheet()->getHighestColumn();
        $row = $this->excel->getActiveSheet()->getHighestRow();
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '4b4c54')), 'font' => array('bold' => true, 'color' => array('rgb' => 'ffffff'))));
        //        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);
        // $this->excel->getActiveSheet()->freezePane('D4');

        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(23);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
        $excessletters = ['D', 'E', 'F', 'G'];
        foreach ($excessletters as $l) {
            $this->excel->getActiveSheet()->getColumnDimension($l)->setAutoSize(true);
        }

        $filename = 'SZ_DTR Violations ' . date_format(date_create($incidentDate_start), "Y-m-d") . ' to ' . date_format(date_create($incidentDate_end), "Y-m-d") . '.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function dms_memo()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'DMS Memo',
        ];
        // if($this->check_access()){
        $this->load_template_view('templates/dms/dms_memo', $data);
        // }        
    }

    // public function get_all_memos()
    // {
    //     $limiter = $this->input->post('limiter');
    //     $perPage = $this->input->post('perPage');
    //     $query = "FROM tbl_dms_memo ORDER BY dmsMemo_ID DESC";
    //     $memos = $this->general_model->custom_query("SELECT * $query LIMIT $limiter,$perPage");
    //     $allmemos = $this->general_model->custom_query("SELECT COUNT(*) as count " . $query);
    //     $count = $allmemos[0]->count;
    //     echo json_encode(array('memos' => $memos, 'total' => $count));
    // }
    public function get_all_memos()
    {
        $orderMemo = $this->input->post('orderMemo');
        $orderTypeMemo = $this->input->post('orderTypeMemo');
        $searchMemo = $this->input->post('searchMemo');
        $searchString = "";
        if ($searchMemo !== '' && $searchMemo != null) {
            $searchString = "WHERE (title LIKE '%$searchMemo%' OR number LIKE '%$searchMemo%')";
        }

        $memos = $this->general_model->custom_query("SELECT * FROM tbl_dms_memo $searchString ORDER BY $orderMemo $orderTypeMemo");
        echo json_encode(array('memos' => $memos));
    }

    public function downloadExcel_ir()
    {
        $searchString = "";
        $search = array();
        $incidentDate_start = $this->input->post('incidentDate_start');
        $incidentDate_end = $this->input->post('incidentDate_end');
        $violation_ir_id = $this->input->post('violation_ir_id');
        $violation_class = $this->input->post('violation_class'); //admin agent
        $violation_type = $this->input->post('violation_type'); //liable
        $report_type = $this->input->post('report_type'); //normal qip
        $violation_accounts = $this->input->post('violation_accounts');
        $violation_subject = $this->input->post('violation_subject');
        $violation_offense = $this->input->post('offense');
        $violation_offense_nature = $this->input->post('offense_nature');
        if ($incidentDate_start !== '' &&  $incidentDate_end !== '' && $incidentDate_start != null && $incidentDate_end != null) {
            $search[] = "ir.incidentDate >= '$incidentDate_start' AND ir.incidentDate <= '$incidentDate_end'";
        }
        if ($violation_ir_id !== '' && $violation_ir_id != null) {
            $search[] = "ir.incidentReport_ID=$violation_ir_id";
        }
        if ($report_type !== '' && $report_type != null) {
            $search[] = "ir.incidentReportType_ID='$report_type'";
        }
        if ($violation_class !== '' && $violation_class != null) {
            $search[] = "ir.empType='$violation_class'";
        }
        if ($violation_type !== '' && $violation_type != null) {
            $search[] = "ir.liabilityStat='$violation_type'";
        }
        if ($violation_accounts !== '' && $violation_accounts != null) {
            $search[] = "ir.acc_id=$violation_accounts";
        }
        if ($violation_subject  !== '' && $violation_subject != null && $violation_subject != 0) {
            $search[] = "ir.subjectEmp_ID = $violation_subject";
        }
        if ($violation_offense !== '' && $violation_offense != null && $violation_offense != 0) {
            $search[] = "ir.offense_ID='$violation_offense'";
        }
        if ($violation_offense_nature !== '' && $violation_offense_nature != null && $violation_offense_nature != 0) {
            $search[] = "offType.offenseType_ID='$violation_offense_nature'";
        }
        if (!empty($search)) {
            $searchString = " AND " . implode(" AND ", $search);
        }

        $ero_emp_id = $this->session->emp_id;
        // $query = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description filingTypeDescription, ir.incidentReportStages_ID, irStages.description as irstageDescription, ir.subjectEmp_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND (ir.liabilityStat=17 OR ir.liabilityStat=18) AND ir.acc_id IN (SELECT acc_id FROM `tbl_dms_employee_relations_officer` WHERE emp_id= $ero_emp_id ) $searchString";
        $query = "SELECT DISTINCT(ir.incidentReport_ID),termDates.date termDate, term.days, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, 
   ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, 
   liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, 
   ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description filingTypeDescription, 
   ir.incidentReportStages_ID, irStages.description as irstageDescription, ir.subjectEmp_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, 
   subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, 
   sourceApp.nameExt sourceNameExt, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, 
   dmsActionFin.action finalAction, dmsActionFin.label
   FROM tbl_dms_incident_report ir 
    INNER JOIN tbl_status prescStat ON prescStat.status_ID = ir.prescriptiveStat 
    INNER JOIN tbl_status liableStat ON liableStat.status_ID = ir.liabilityStat 
    INNER JOIN tbl_dms_incident_report_filing_type irFilingType ON irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID 
    INNER JOIN tbl_dms_incident_report_stages irStages ON irStages.incidentReportStages_ID = ir.incidentReportStages_ID 
    INNER JOIN tbl_employee subEmp ON subEmp.emp_id = ir.subjectEmp_ID
    INNER JOIN tbl_employee sourceEmp ON sourceEmp.emp_id = ir.sourceEmp_ID
    INNER JOIN tbl_applicant sourceApp ON sourceApp.apid = sourceEmp.apid 
    INNER JOIN tbl_applicant subApp ON subApp.apid = subEmp.apid
    INNER JOIN tbl_dms_offense off ON off.offense_ID = ir.offense_ID
    INNER JOIN tbl_dms_offense_type offType ON offType.offenseType_ID = off.offenseType_ID
    INNER JOIN tbl_dms_disciplinary_action_category dmsActionCategSug ON dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID
    INNER JOIN tbl_dms_disciplinary_action_category dmsActionCategFin ON  dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID
    INNER JOIN tbl_dms_disciplinary_action dmsActionSug ON dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID
    INNER JOIN tbl_dms_disciplinary_action dmsActionFin ON dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID
    INNER JOIN tbl_dms_incident_report_recommendation recom ON ir.incidentReport_ID = recom.incidentReport_ID AND recom.emp_ID = $ero_emp_id AND (recom.incidentReportRecommendationType_ID = 2 OR recom.incidentReportRecommendationType_ID = 3) 
    LEFT JOIN tbl_dms_suspension_termination_days term ON ir.incidentReport_ID=term.incidentReport_ID
    LEFT JOIN tbl_dms_suspension_dates termDates ON termDates.suspensionTerminationDays_ID=term.suspensionTerminationDays_ID
    WHERE (ir.liabilityStat=17 OR ir.liabilityStat=18) $searchString ORDER BY ir.incidentReport_ID DESC";
     $ir_data = $this->general_model->custom_query($query);

        // var_dump($ir_data);
        // echo $this->db->last_query();
        // die();
        //EXCEL PROPER
        $this->load->library('PHPExcel', NULL, 'excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('DTR System Violations');
        $this->excel->getActiveSheet()->setShowGridlines(false);
        //------------------------INSERT LOGO-------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(5);  //setOffsetY has no effect
        $objDrawing->setCoordinates('B1');
        $objDrawing->setHeight(80); // logo height
        // $objDrawing->setWidth(320); // logo width
        // $objDrawing->setWidthAndHeight(200,400);
        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('3')->setRowHeight(30);
        //set cell A1 content with some text

        // $this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(FALSE);
        // $this->excel->getActiveSheet()->setCellValue('A1', 'CODE-SZ_IRExport');
        $this->excel->getActiveSheet()->setCellValue('A3', 'IR ID');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Incident Date');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Incident Time');
        $this->excel->getActiveSheet()->setCellValue('D3', 'DateTime Filed');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('F3', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('G3', 'Middle Name');
        $this->excel->getActiveSheet()->setCellValue('H3', 'Name Ext');
        $this->excel->getActiveSheet()->setCellValue('I3', 'Employee Type');
        $this->excel->getActiveSheet()->setCellValue('J3', 'Position');
        $this->excel->getActiveSheet()->setCellValue('K3', 'Employee Status');
        $this->excel->getActiveSheet()->setCellValue('L3', 'Account');
        $this->excel->getActiveSheet()->setCellValue('M3', 'Nature of Offense');
        $this->excel->getActiveSheet()->setCellValue('N3', 'Offense');
        $this->excel->getActiveSheet()->setCellValue('O3', 'Class');
        $this->excel->getActiveSheet()->setCellValue('P3', 'Incident');
        $this->excel->getActiveSheet()->setCellValue('Q3', 'Incident Place');
        $this->excel->getActiveSheet()->setCellValue('R3', 'Expected Action');
        $this->excel->getActiveSheet()->setCellValue('S3', 'Cure Date'); //prescriptionEnd
        $this->excel->getActiveSheet()->setCellValue('T3', 'Presciptive Status');
        $this->excel->getActiveSheet()->setCellValue('U3', 'Liability Status');
        $this->excel->getActiveSheet()->setCellValue('V3', 'Date of Liability');
        $this->excel->getActiveSheet()->setCellValue('W3', 'Explanation Date');
        $this->excel->getActiveSheet()->setCellValue('X3', 'Subject Explanation');
        $this->excel->getActiveSheet()->setCellValue('Y3', 'Type of Filing'); //filingTypeDescription
        $this->excel->getActiveSheet()->setCellValue('Z3', 'IR Stage'); //irstageDescription
        $this->excel->getActiveSheet()->setCellValue('AA3', 'Source First Name');
        $this->excel->getActiveSheet()->setCellValue('AB3', 'Source Middle Name');
        $this->excel->getActiveSheet()->setCellValue('AC3', 'Source Last Name');
        $this->excel->getActiveSheet()->setCellValue('AD3', 'Source Name Extension');
        $this->excel->getActiveSheet()->setCellValue('AE3', 'Suggested Action');
        $this->excel->getActiveSheet()->setCellValue('AF3', 'Final Action');
        $this->excel->getActiveSheet()->setCellValue('AG3', 'Effectivity');

        $emprow = 4;
        foreach ($ir_data as $ir) {
            $effectivity = "";
            if($ir->label === 's'){
                $effectivity  = $ir->days;
                $sus_dates = $this->get_suspension_dates($ir->incidentReport_ID);

                if(count($sus_dates) > 0){
                    $date_unit = "day";
                    if(count($sus_dates) > 1){
                        $date_unit .= "s";
                    }
                    $effectivity .= " ".$date_unit;
                    $dates = [];
                    foreach($sus_dates as $row_index=>$row_value){
                        $dates[$row_index] = $row_value->date;
                    }
                    $effectivity .= " (". implode(', ', $dates) .")";
                }
            }else if($ir->label === 't'){
                $effectivity = "Effective on ".date('Y/m/d', strtotime($ir->termDate));
            }
            $finalActionDesc = $ir->finalAction;
            if($ir->liabilityStat == 18){
                $finalActionDesc = ucwords($ir->liabilityStatDesc);
            }
            $this->excel->getActiveSheet()->setCellValue('A' . $emprow, sprintf("%06d", $ir->incidentReport_ID));
            $this->excel->getActiveSheet()->setCellValue('B' . $emprow, date('Y/m/d', strtotime($ir->incidentDate)));
            $this->excel->getActiveSheet()->setCellValue('C' . $emprow, date('H:i', strtotime($ir->incidentTime)));
            $this->excel->getActiveSheet()->setCellValue('D' . $emprow, date('Y/m/d H:i', strtotime($ir->dateTimeFiled)));
            $this->excel->getActiveSheet()->setCellValue('E' . $emprow, ucwords($ir->subjectLname));
            $this->excel->getActiveSheet()->setCellValue('F' . $emprow, ucwords($ir->subjectFname));
            $this->excel->getActiveSheet()->setCellValue('G' . $emprow, ucwords($ir->subjectMname));
            $this->excel->getActiveSheet()->setCellValue('H' . $emprow, ucwords($ir->subjectNameExt));
            $this->excel->getActiveSheet()->setCellValue('I' . $emprow, ucwords($ir->empType));
            $this->excel->getActiveSheet()->setCellValue('J' . $emprow, ucwords($ir->subjectPosition));
            $this->excel->getActiveSheet()->setCellValue('K' . $emprow, ucwords($ir->subjectEmpStat));
            $this->excel->getActiveSheet()->setCellValue('L' . $emprow, ucwords($ir->subjectAccount));
            $this->excel->getActiveSheet()->setCellValue('M' . $emprow, $ir->offenseType);
            $this->excel->getActiveSheet()->setCellValue('N' . $emprow, $ir->offense);
            $this->excel->getActiveSheet()->setCellValue('O' . $emprow, ucwords($ir->letter));
            $this->excel->getActiveSheet()->setCellValue('P' . $emprow, $ir->details);
            $this->excel->getActiveSheet()->setCellValue('Q' . $emprow, $ir->place);
            $this->excel->getActiveSheet()->setCellValue('R' . $emprow, $ir->expectedAction);
            $this->excel->getActiveSheet()->setCellValue('S' . $emprow, date('Y/m/d', strtotime($ir->prescriptionEnd)));
            $this->excel->getActiveSheet()->setCellValue('T' . $emprow, ucwords($ir->prescriptiveStatDesc));
            $this->excel->getActiveSheet()->setCellValue('U' . $emprow, ucwords($ir->liabilityStatDesc));
            $this->excel->getActiveSheet()->setCellValue('V' . $emprow, date('Y/m/d H:i', strtotime($ir->datedLiabilityStat)));
            $this->excel->getActiveSheet()->setCellValue('W' . $emprow, date('Y/m/d H:i', strtotime($ir->explanationDate)));
            $this->excel->getActiveSheet()->setCellValue('X' . $emprow, $ir->subjectExplanation);
            $this->excel->getActiveSheet()->setCellValue('Y' . $emprow, ucwords($ir->filingTypeDescription));
            $this->excel->getActiveSheet()->setCellValue('Z' . $emprow, ucwords($ir->irstageDescription));
            $this->excel->getActiveSheet()->setCellValue('AA' . $emprow, ucwords($ir->sourceFname));
            $this->excel->getActiveSheet()->setCellValue('AB' . $emprow, ucwords($ir->sourceMname));
            $this->excel->getActiveSheet()->setCellValue('AC' . $emprow, ucwords($ir->sourceLname));
            $this->excel->getActiveSheet()->setCellValue('AD' . $emprow, ucwords($ir->sourceNameExt));
            $this->excel->getActiveSheet()->setCellValue('AE' . $emprow, $ir->suggestedAction);
            $this->excel->getActiveSheet()->setCellValue('AF' . $emprow, $finalActionDesc);
            $this->excel->getActiveSheet()->setCellValue('AG' . $emprow, $effectivity);
            $emprow++;
        }

        $column = $this->excel->getActiveSheet()->getHighestColumn();
        $row = $this->excel->getActiveSheet()->getHighestRow();
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('font' => array('size' => 10), 'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '4b4c54')), 'font' => array('bold' => true, 'color' => array('rgb' => 'ffffff'))));
        //        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('A3:D' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('O3:O' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('S3:S' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('V3:W' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('AG3:AH' . $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);
        $this->excel->getActiveSheet()->freezePane('G4');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
        $excessletters = ['G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH'];
        foreach ($excessletters as $l) {
            $this->excel->getActiveSheet()->getColumnDimension($l)->setAutoSize(true);
        }
        ob_clean();
        $filename = 'SZ_IR ' . date_format(date_create($incidentDate_start), "Y-m-d") . ' to ' . date_format(date_create($incidentDate_end), "Y-m-d") . '.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function get_all_nature_of_offense()
    {
        $data = $this->general_model->fetch_specific_vals("offenseType_ID,offenseType", "status_ID=10", "tbl_dms_offense_type", "offenseType ASC");
        echo json_encode(array('natureOffense' => $data));
    }
    public function get_ir_monitoring_chart_offenseType()
    {
        $emp_stat =  $this->input->post('emp_stat'); //str (Regular, Probationary , etc..)
        $emp_type =  $this->input->post('emp_type'); //str (Admin)
        $natureOffense  = $this->input->post('natureOffense');
        $accounts =  $this->input->post('accounts'); //acc_id 
        $date_end  = $this->input->post('date_end');
        $date_start  = $this->input->post('date_start');

        $searchString1 = "";
        $searchString2 = "";
        $search = array();

        if ($emp_stat !== '' && $emp_stat !== null) {
            $search[] = "subjectEmpStat='$emp_stat'";
        }
        if ($emp_type !== '' && $emp_type !== null) {
            $search[] = "empType='$emp_type'";
        }
        if ($accounts !== '' && $accounts !== null) {
            $search[] = "acc_id=$accounts";
        }
        if ($date_end !== '' && $date_start !== '' && $date_end !== null && $date_start !== null) {
            $search[] = "incidentDate>='$date_start' AND incidentDate<='$date_end'";
        }
        if (!empty($search)) {
            $searchString1 = " AND " . implode(" AND ", $search);
        }

        if ($natureOffense !== '') {
            $searchString2 = " AND offenseType_ID=$natureOffense";
        }

        $overAllTotal = 0;
        $offenseTypes = $this->general_model->fetch_specific_vals("offenseType_ID,letter,offenseType", "status_ID=10 $searchString2", "tbl_dms_offense_type", "offenseType DESC");
        $offenses = $this->general_model->fetch_specific_vals("offense_ID,offense,offenseType_ID", "status_ID=10 $searchString2", "tbl_dms_offense", "offense ASC");
        $offense_ids = array();
        foreach ($offenses as $off) {
            $offense_ids[] = $off->offense_ID;
        }
        $series = array();
        $drilldownSeries = array();
        if (!empty($offense_ids)) {
            $incidentReports = $this->general_model->custom_query("SELECT offense_ID,COUNT(*) as cnt FROM tbl_dms_incident_report WHERE liabilityStat IN (17,18) AND offense_ID IN (" . implode(',', $offense_ids) . ") $searchString1 GROUP BY offense_ID");

            foreach ($offenses as $off) {
                $off->count = 0;
                foreach ($incidentReports as $key => $ir) {
                    if ($off->offense_ID === $ir->offense_ID) {
                        $off->count = (int) $ir->cnt;
                        unset($incidentReports[$key]);
                    }
                }
            }
            foreach ($offenseTypes as $offType) {
                $offType->count = 0;
                foreach ($offenses as $off) {
                    if ($offType->offenseType_ID === $off->offenseType_ID) {
                        $offType->count += $off->count;
                    }
                }
            }
            $colors = [
                "#64C1E8", "#D85B63", "#D680AD", "#5C5C5C", "#C0BA80", "#FDC47D", "#EA3B46", // nice
                "#418171", "#AEADAB", "#F68951", "#E1B1A1", "#7E5D54", "#F69991", "#FDB954", //earthtones
                "#5867DD", "#34BFA3", "#36A3F7", "#FFB822", "#F4516C", "#9816F4", "#00C5DC",
            ];
            $colorcount = COUNT($colors);
            $colorcnt = 0;
            foreach ($offenseTypes as $offType) {
                $overAllTotal += $offType->count;
                if ($colorcnt === $colorcount) {
                    $colorcnt = 0;
                }
                $curcolor = $colorcnt;
                // if ($offType->count !== 0) { //remove this filter if you want to display all offenseType by default even if zero
                $series[] = [
                    'name' => ucfirst($offType->offenseType),
                    'y' => $offType->count,
                    'color' => $colors[$curcolor],
                    'drilldown' => $offType->offenseType_ID
                ];
                $drilldownSub = array();
                foreach ($offenses as $off) {
                    if ($offType->offenseType_ID === $off->offenseType_ID) {
                        // if ($off->count !== 0) { //remove this filter if you want all instance of offense to display by default even if zero
                        $drilldownSub[] = ['name' => ucfirst($off->offense), 'y' => $off->count];
                        // }
                    }
                }
                $drilldownSeries[] = [
                    'name' => ucfirst($offType->offenseType),
                    'id' => $offType->offenseType_ID,
                    'data' => $drilldownSub
                ];
                // }
                $colorcnt++;
            };
        }
        usort($series, array($this, "countSort"));
        $series = array_reverse($series);
        echo json_encode(['series' => $series, 'drilldownSeries' => $drilldownSeries, 'total' => $overAllTotal]);
    }

    public function get_ir_monitoring_chart_offense()
    {
        $emp_stat =  $this->input->post('emp_stat'); //str (Regular, Probationary , etc..)
        $emp_type =  $this->input->post('emp_type'); //str (Admin)
        $natureOffense  = $this->input->post('natureOffense');
        $accounts =  $this->input->post('accounts'); //acc_id 
        $date_end  = $this->input->post('date_end');
        $date_start  = $this->input->post('date_start');

        $searchString1 = "";
        $searchString2 = "";
        $search = array();

        if ($emp_stat !== '' && $emp_stat !== null) {
            $search[] = "subjectEmpStat='$emp_stat'";
        }
        if ($emp_type !== '' && $emp_type !== null) {
            $search[] = "empType='$emp_type'";
        }
        if ($accounts !== '' && $accounts !== null) {
            $search[] = "acc_id=$accounts";
        }
        if ($date_end !== '' && $date_start !== '' && $date_end !== null && $date_start !== null) {
            $search[] = "incidentDate>='$date_start' AND incidentDate<='$date_end'";
        }
        if (!empty($search)) {
            $searchString1 = " AND " . implode(" AND ", $search);
        }

        if ($natureOffense !== '') {
            $searchString2 = " AND offenseType_ID=$natureOffense";
        }

        $overAllTotal = 0;
        $offenses = $this->general_model->fetch_specific_vals("offense_ID,offense,offenseType_ID", "status_ID=10 $searchString2", "tbl_dms_offense", "offense DESC");
        $offense_ids = array();
        foreach ($offenses as $off) {
            $offense_ids[] = $off->offense_ID;
        }
        $series = array();
        if (!empty($offense_ids)) {
            $incidentReports = $this->general_model->custom_query("SELECT offense_ID,COUNT(*) as cnt FROM tbl_dms_incident_report WHERE liabilityStat IN (17,18) AND offense_ID IN (" . implode(',', $offense_ids) . ") $searchString1 GROUP BY offense_ID");

            foreach ($offenses as $off) {
                $off->count = 0;
                foreach ($incidentReports as $key => $ir) {
                    if ($off->offense_ID === $ir->offense_ID) {
                        $off->count = (int) $ir->cnt;
                        unset($incidentReports[$key]);
                    }
                }
            }
            $colors = [
                "#64C1E8", "#D85B63", "#D680AD", "#5C5C5C", "#C0BA80", "#FDC47D", "#EA3B46", // nice
                "#418171", "#AEADAB", "#F68951", "#E1B1A1", "#7E5D54", "#F69991", "#FDB954", //earthtones
                "#5867DD", "#34BFA3", "#36A3F7", "#FFB822", "#F4516C", "#9816F4", "#00C5DC",
            ];
            $colorcount = COUNT($colors);
            $colorcnt = 0;
            foreach ($offenses as $off) {
                $overAllTotal += $off->count;
                if ($colorcnt === $colorcount) {
                    $colorcnt = 0;
                }
                $curcolor = $colorcnt;
                // if ($offType->count !== 0) { //remove this filter if you want to display all offenseType by default even if zero
                $series[] = [
                    'name' => ucfirst($off->offense),
                    'y' => $off->count,
                    'color' => $colors[$curcolor]
                ];
                // }
                $colorcnt++;
            };
        }
        usort($series, array($this, "countSort"));
        $series = array_reverse($series);
        echo json_encode(['series' => $series, 'total' => $overAllTotal]);
    }
    public function get_ir_monitoring_chart_account()
    {
        $emp_stat =  $this->input->post('emp_stat'); //str (Regular, Probationary , etc..)
        $emp_type =  $this->input->post('emp_type'); //str (Admin)
        $natureOffense  = $this->input->post('natureOffense');
        $accounts =  $this->input->post('accounts'); //acc_id 
        $date_end  = $this->input->post('date_end');
        $date_start  = $this->input->post('date_start');

        $searchString1 = "";
        $searchString2 = "";
        $searchString3 = "";
        $search1 = array();
        $search2 = array();


        if ($natureOffense !== '' && $natureOffense !== null) {
            $search1[] = "offenseType_ID=$natureOffense";
            $searchString3 = " AND offenseType_ID=$natureOffense";
        }
        if ($emp_stat !== '' && $emp_stat !== null) {
            $search1[] = "subjectEmpStat='$emp_stat'";
        }
        if ($emp_type !== '' && $emp_type !== null) {
            $search1[] = "empType='$emp_type'";
            $search2[] = "acc_description='$emp_type'";
        }
        if ($accounts !== '' && $accounts !== null) {
            $search1[] = "acc_id=$accounts";
            $search2[] = "acc_id=$accounts";
        }
        if ($date_end !== '' && $date_start !== '' && $date_end !== null && $date_start !== null) {
            $search1[] = "incidentDate>='$date_start' AND incidentDate<='$date_end'";
        }
        if (!empty($search1)) {
            $searchString1 = " AND " . implode(" AND ", $search1);
        }

        if (!empty($search2)) {
            $searchString2 = " AND " . implode(" AND ", $search2);
        }
        $overAllTotal = 0;
        $accounts_list = $this->general_model->fetch_specific_vals("acc_id,acc_name,acc_description", "status_ID = 1 $searchString2", "tbl_account", "acc_name ASC");
        $offenses = $this->general_model->fetch_specific_vals("offense_ID,offense,offenseType_ID", "status_ID=10  $searchString3", "tbl_dms_offense", "offense ASC");
        $acc_ids = array();
        foreach ($accounts_list as $acc) {
            $acc_ids[] = $acc->acc_id;
        }
        $series = array();
        $drilldownSeries = array();

        if (!empty($accounts_list)) {
            $incidentReports = $this->general_model->custom_query("SELECT acc_id,offense_ID  FROM tbl_dms_incident_report WHERE liabilityStat IN (17,18) AND acc_id IN (" . implode(',', $acc_ids) . ") $searchString1");
      
            foreach ($accounts_list as $acc) {
                $acc->count = 0;
                $acc->offenses = array();
                foreach ($offenses as $off) {
                    $counter = 0;
                    foreach ($incidentReports as $key => $ir) {
                        if ($off->offense_ID === $ir->offense_ID && $acc->acc_id === $ir->acc_id) {
                            $counter += 1;
                            $acc->count += 1;
                            unset($incidentReports[$key]);
                        }
                    }
                    if ($counter!==0) {
                        $acc->offenses[] = array('count'=>(int) $counter,'offense'=>$off->offense,'acc_id'=>$acc->acc_id);
                    }
                }
            }
            
            $colors = [
                "#64C1E8", "#D85B63", "#D680AD", "#5C5C5C", "#C0BA80", "#FDC47D", "#EA3B46", // nice
                "#418171", "#AEADAB", "#F68951", "#E1B1A1", "#7E5D54", "#F69991", "#FDB954", //earthtones
                "#5867DD", "#34BFA3", "#36A3F7", "#FFB822", "#F4516C", "#9816F4", "#00C5DC",
            ];
            $colorcount = COUNT($colors);
            $colorcnt = 0;
            foreach ($accounts_list as $acc) {
                $overAllTotal += $acc->count;
                if ($colorcnt === $colorcount) {
                    $colorcnt = 0;
                }
                $curcolor = $colorcnt;
                $series[] = [
                    'name' => ucfirst($acc->acc_name),
                    'y' => $acc->count,
                    'color' => $colors[$curcolor],
                    'drilldown' => $acc->acc_id
                ];
                $drilldownSub = array();
                foreach ($acc->offenses as $off) {
                  
                    $drilldownSub[] = ['name' => ucfirst($off['offense']), 'y' => $off['count']];
                   
                }
                $drilldownSeries[] = [
                    'name' => ucfirst($acc->acc_name),
                    'id' => $acc->acc_id,
                    'data' => $drilldownSub
                ];
                $colorcnt++;
            };
        }
        echo json_encode(['series' => $series, 'drilldownSeries' => $drilldownSeries, 'total' => $overAllTotal]);
    }

    function countSort($a, $b)
    {
        return strcmp($a['y'], $b['y']);
    }
    //--------------------------------End of Nicca Arique---------------------------------------
     // NEW CHANGES FOR DMS MIC (DMS COUNTS)
    private function qry_file_ir_dtr_violations($emp_id) //file ir for dtr violations
    {
        $fields = "COUNT(DISTINCT(qualified.qualifiedUserDtrViolation_ID)) as recordCount";
        $where = "qualified.qualifiedUserDtrViolation_ID = supQualified.qualifiedUserDtrViolation_ID AND qualified.status_ID = 2 AND supQualified.status_ID NOT IN (3, 20, 12) AND supQualified.emp_id = $emp_id";
        $table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        return $record->recordCount;
    }
    private function qry_my_witness($emp_id) //me as witness in an IR count
    {
        $fields = "DISTINCT(ir.incidentReport_ID)";
        $where = "ir.incidentReport_ID = witness.incidentReport_ID AND ir.incidentReportStages_ID = 1 AND witness.status_ID = 2 AND witness.witnessEmp_ID = $emp_id";
        $table = "tbl_dms_incident_report_witness witness, tbl_dms_incident_report ir";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
        // return $record->recordCount;
    }
    private function subordinate_as_sub_explanation($emp_id) //subordinates that has ir for explanation
    {
        $emplist = $this->getEmployeeSubordinate($_SESSION['uid']);
        $arr_emp_subordinate = array_column($emplist, 'emp_id');
        $sub_list = 0;
        if(count($arr_emp_subordinate) > 0){
            $sub_list = implode(",",$arr_emp_subordinate);
        }
        $fields = "COUNT(DISTINCT(ir.incidentReport_ID)) as recordCount";
        $where = " ir.incidentReportStages_ID = 1 AND subjectEmp_ID IN ($sub_list)";
        $table = "tbl_dms_incident_report ir";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        return $record->recordCount;
    }
    // private function my_sub_explanation($emp_id) ///my ir for explanation
    // {
    //     $fields = "COUNT(DISTINCT(ir.incidentReport_ID)) as recordCount";
    //     // $where = "ir.subjectEmp_ID = emp.emp_id AND ir.incidentReportStages_ID = 1 AND emp.emp_id = $emp_id";
    //     $where = "ir.subjectEmp_ID = emp.emp_id AND ir.incidentReportStages_ID = 1 AND emp.emp_id = $emp_id AND ir.incidentReport_ID NOT IN (SELECT ir.incidentReport_ID FROM tbl_dms_incident_report_witness witness, tbl_dms_incident_report ir WHERE witness.incidentReport_ID = ir.incidentReport_ID AND ir.subjectEmp_ID = $emp_id AND witness.status_ID = 2)";

    //     $table = "tbl_dms_incident_report ir, tbl_employee emp";
    //     $record = $this->general_model->fetch_specific_val($fields, $where, $table);
    //     return $record->recordCount;
    // }
    private function my_sub_explanation($emp_id) ///my ir for explanation
    {
        $fields = "DISTINCT(ir.incidentReport_ID)";
        $where = "ir.subjectEmp_ID = emp.emp_id AND ir.incidentReportStages_ID = 1 AND emp.emp_id = $emp_id AND ir.incidentReport_ID NOT IN (SELECT ir.incidentReport_ID FROM tbl_dms_incident_report_witness witness, tbl_dms_incident_report ir WHERE witness.incidentReport_ID = ir.incidentReport_ID AND ir.subjectEmp_ID = $emp_id AND witness.status_ID = 2)";
        $table = "tbl_dms_incident_report ir, tbl_employee emp";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
        
    }
    private function set_effectivity_dates($emp_id){
        $fields = "COUNT(DISTINCT(days.incidentReport_ID)) as recordCount";
        $where = "days.status_ID = 2 AND days.directSupEmp_ID = $emp_id";
        $table = "tbl_dms_suspension_termination_days days";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        return $record->recordCount;
    }
    private function ir_for_my_recommendation($emp_id) //me as a recommending supervisor
    {
        $fields = "COUNT(DISTINCT(ir.incidentReport_ID)) as recordCount";
        $where = "dmsDead.record_ID = recommend.incidentReportRecommendation_ID AND dmsDead.dmsDeadlineType_ID = 3 AND dmsDead.status_ID = 2 AND dmsDead.emp_id = recommend.emp_ID AND ir.incidentReport_ID = recommend.incidentReport_ID AND recommend.liabilityStat_ID = 4 AND recommend.incidentReportRecommendationType_ID = 1 AND recommend.emp_ID = $emp_id";
        $table = "tbl_dms_incident_report ir, tbl_dms_incident_report_recommendation recommend, tbl_dms_deadline dmsDead";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        return $record->recordCount;
    }
    private function ir_my_client_confirmation($emp_id) //me as a recommending supervisor
    {
        $fields = "COUNT(DISTINCT(ir.incidentReport_ID)) as recordCount";
        $where = "ir.incidentReport_ID = recommend.incidentReport_ID AND recommend.liabilityStat_ID = 4 AND recommend.incidentReportRecommendationType_ID = 4 AND recommend.emp_ID = $emp_id";
        $table = "tbl_dms_incident_report ir, tbl_dms_incident_report_recommendation recommend";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        return $record->recordCount;
    }
    private function ir_my_ero_recommendation($emp_id) //me as a recommending supervisor
    {
        $fields = "COUNT(DISTINCT(ir.incidentReport_ID)) as recordCount";
        $where = "dmsDead.record_ID = recommend.incidentReportRecommendation_ID AND dmsDead.dmsDeadlineType_ID IN (4, 5) AND dmsDead.emp_id = recommend.emp_ID AND ir.incidentReport_ID = recommend.incidentReport_ID AND recommend.liabilityStat_ID = 4 AND recommend.incidentReportRecommendationType_ID IN (2, 3) AND recommend.emp_ID = $emp_id";
        $table = "tbl_dms_incident_report ir, tbl_dms_incident_report_recommendation recommend, tbl_dms_deadline dmsDead";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        return $record->recordCount;
    }
    private function ir_upload_documents($emp_id) //me as a recommending supervisor
    {
        $fields = "COUNT(DISTINCT(ir.incidentReport_ID)) as recordCount";
        $where = " ir.incidentReport_ID = doc.incidentReport_ID AND dmsDead.record_ID = doc.irNecessaryDocument_ID AND dmsDead.dmsDeadlineType_ID = 7 AND doc.status_ID = 2 AND doc.eroEmp_ID = $emp_id";
        $table = "tbl_dms_incident_report ir, tbl_dms_deadline dmsDead, tbl_dms_necessary_document doc";
        $record = $this->general_model->fetch_specific_val($fields, $where, $table);
        return $record->recordCount;
    }
    public function get_dms_counts(){
        $emp_id = $this->session->userdata('emp_id');
        // $emp_id = 207;
        $user_id =  $this->session->userdata('uid');
        $emp_sup = $this->get_direct_supervisor($user_id);
        $data['file_dtr_violation'] = $this->qry_file_ir_dtr_violations($emp_id);
        $my_witness = $this->qry_my_witness($emp_id);
        $data['my_witness'] = count($my_witness);
        $data['my_witness_list'] = $my_witness;
        $data['subordinate_sub_explanation'] = $this->subordinate_as_sub_explanation($emp_id);
        $sub_explanation = $this->my_sub_explanation($emp_id);
        $data['my_sub_explanation'] = count($sub_explanation);
        if(count($emp_sup) > 0){
            $data['my_supervisor'] = $this->get_emp_details_via_user_id($emp_sup->uid);
        }        
        $data['my_sub_explanation_list'] = $sub_explanation;
        $data['ir_my_recommendation'] = $this->ir_for_my_recommendation($emp_id);
        $data['ir_my_client_confirmation'] = $this->ir_my_client_confirmation($emp_id);
        $data['effectivity_dates'] = $this->set_effectivity_dates($emp_id);
        $data['ir_my_ero_recommendation'] = $this->ir_my_ero_recommendation($emp_id);
        $data['upload_documents'] = $this->ir_upload_documents($emp_id);
        echo json_encode($data);
    }
    //NICCA CODES FOR SUBJECT EXPLANATION

    public function get_my_subject_explanation()
    {
        $datatable = $this->input->post('datatable');
        $emp_id = $this->session->emp_id;
        $search = array();
        $group_by_where = " GROUP BY a.incidentReport_ID";
        $query['query'] = "SELECT c.fname,c.lname,f.action,a.*, COUNT(g.incidentReportWitness_ID) witnessCount FROM tbl_dms_incident_report a INNER JOIN tbl_employee b ON a.subjectEmp_ID = b.emp_id INNER JOIN tbl_applicant c ON b.apid = c.apid INNER JOIN tbl_dms_incident_report_stages d ON a.incidentReportStages_ID = d.incidentReportStages_ID INNER JOIN tbl_dms_disciplinary_action_category e ON a.finaldisciplinaryActionCategory_ID = e.disciplinaryActionCategory_ID INNER JOIN tbl_dms_disciplinary_action f ON e.disciplinaryAction_ID = f.disciplinaryAction_ID LEFT JOIN tbl_dms_incident_report_witness g ON a.incidentReport_ID = g.incidentReport_ID AND g.status_ID = 2 WHERE d.incidentReportStages_ID=1 AND a.subjectEmp_ID = $emp_id";
        if (isset($datatable['query']['mySubjectExplanation_search']) && $datatable['query']['mySubjectExplanation_search'] !== '') {
            $keyword = trim($datatable['query']['mySubjectExplanation_search']);
            $search[] = "(details LIKE '%$keyword%' OR f.action LIKE '%$keyword%' OR offense LIKE '%$keyword%')";
        }
        if (!empty($search)) {
            $searchString = " AND " . implode(" AND ", $search);
            $query['search']['append'] = $searchString." ".$group_by_where;
            $query['search']['total'] = $searchString." ".$group_by_where;
        }else{
            $query['search']['append'] = $group_by_where;
            $query['search']['total'] = $group_by_where;
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode($data);
    }

	public function submitExplanation(){
        $irid = $this->input->post('irid');
        $explanation = $this->input->post('explanation');
        $fileupload = $this->input->post('fileupload');
        try {
            if($fileupload=='true'){
                if (!isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
                    throw new RuntimeException('Invalid parameters.');
                }
    
                switch ($_FILES['file']['error']) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new RuntimeException('No file sent.');
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new RuntimeException('Exceeded filesize limit.');
                    default:
                        throw new RuntimeException('Unknown errors.');
                }
                $dir = 'uploads/dms/explain/';
                if (file_exists($dir) && is_dir($dir)) { // print $dir . ' is a directory!'; 
                } else {
                    mkdir($dir, 0777, true);
                    chmod($dir, 0777);
                }
                $filename = $_FILES['file']['name'];
                $subpath = time() . $filename;
                $path = $dir . strtolower($subpath);
                $courtesy = $_SESSION["fname"] . " " . $_SESSION["lname"];
                if (!move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    throw new RuntimeException('Failed to move uploaded file.');
                }else{
                    $this->addUploadedFilez($irid, $path, $courtesy, "document", 5);
                }
            }
            //USE SIR MARK CODES HERE
            $arr["irid"] = $irid;
            $arr["ifDismiss"] = 1;
            $arr["txt"] = $explanation;

            $update_stat = $this->submitExplain_func($arr);
            if(!$update_stat){
                throw new RuntimeException('Explanation not inserted');
            }
            //END OF USE SIR MARK CODES HERE
            // All good, send the response
            echo json_encode([
                'status' => 'Success'
            ]);
        } catch (RuntimeException $e) {
            // Something went wrong, send the err message as JSON
            http_response_code(400);

            echo json_encode([
                'status' => 'Failed',
                'message' => $e->getMessage()
            ]);
        }
    }
    //END NICCA CODES FOR SUBJECT EXPLANATION
    // Charise Code - March 2020
    public function get_filtersfordismissed(){
        $emp_id = $this->session->userdata('emp_id');
        $filterdis_details = $this->general_model->custom_query("SELECT DISTINCT(emp.emp_id),qualified.qualifiedUserDtrViolation_ID, qualified.emp_id as qualempid, app.fname, app.mname, app.lname, app.nameExt, app.pic FROM tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_dismiss_qualified_dtr_violation dismissq, tbl_employee emp, tbl_applicant app, tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type dtrVioType WHERE dismissq.emp_id=$emp_id AND dismissq.qualifiedUserDtrViolation_ID=qualified.qualifiedUserDtrViolation_ID AND userDtrVio.dtrViolationType_ID=dtrVioType.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID=qualified.userDtrViolation_ID AND app.apid=emp.apid AND emp.emp_id=qualified.emp_id GROUP BY emp.emp_id ORDER BY lname asc");
        echo json_encode($filterdis_details);
    }

    // Manual update

    public function get_manual_recommendations($level)
    {
        $fields = "incidentReportRecommendation_ID, incidentReport_ID, emp_ID, liabilityStat_ID, level, incidentReportRecommendationType_ID";
        $where = "incidentReport_ID IN (649, 629, 623, 622, 619, 617, 614, 613, 527, 488, 455, 412, 284) AND level = $level";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_incident_report_recommendation');
    }

    public function get_manual_deadline_recommendation($record_ids, $emp_ids){

        // var_dump($record_ids);
        $fields = "*";
        $where = "record_ID IN ($record_ids) AND emp_id IN($emp_ids) AND dmsDeadlineType_ID = 3";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dms_deadline');
    }
    public function manually_update_recommendation(){
        $level = 3;
        $emp_id = 44;
        $recommendations = $this->get_manual_recommendations($level);
        $deadline = $this->get_manual_deadline_recommendation(implode(', ', array_column($recommendations, 'incidentReportRecommendation_ID')), implode(', ',array_column($recommendations, 'emp_ID')));
        // var_dump($deadline);
        $recommendation_arr = [];
        $recommendation_deadline = [];
        foreach($recommendations as $recommendations_index=>$recommendations_row){
            $recommendation_arr[$recommendations_index] = [
                "incidentReportRecommendation_ID" => $recommendations_row->incidentReportRecommendation_ID,
                "emp_ID" => $emp_id
            ];
        }
        $deadline_data_time = "2020-06-22 23:59:59";
        foreach($deadline as $deadline_index => $deadline_row){
            $recommendation_deadline[$deadline_index] = [
                "dmsDeadline_ID" => $deadline_row->dmsDeadline_ID,
                "emp_ID" => $emp_id,
                "deadline" => $deadline_data_time
            ];
        }
        // var_dump($recommendation_arr);
        // $this->general_model->batch_update($recommendation_arr, 'incidentReportRecommendation_ID', 'tbl_dms_incident_report_recommendation');
        // $this->general_model->batch_update($recommendation_deadline, 'dmsDeadline_ID', 'tbl_dms_deadline');
        var_dump($recommendation_deadline);
    }
    // 7 - 16 - 2020 charise new code 
    protected function get_existing_ir_TEST($subject_emp_id, $prescriptive_stat, $liability_stat, $offense_ID)
	{
		$dateTime = $this->get_current_date_time();
		$where_cure_date = "";
		if ($prescriptive_stat == 4) {
			$where_cure_date = "AND date(ir.prescriptionEnd) >= '" . $dateTime['date'] . "'";
        }      
        return $this->general_model->custom_query("SELECT ir.incidentReport_ID, ir.subjectEmp_ID, ir.sourceEmp_ID, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.liabilityStat, ir.datedLiabilityStat, ir.subjectExplanation, ir.occurence, discCateg.disciplineCategory_ID, discCateg.category, discAction.disciplinaryAction_ID, discAction.action, discActionCateg.disciplinaryActionCategory_ID, discActionCateg.level, ir.offense_ID, ir.offense, ir.prescriptiveId, ir.disciplinaryActionCategorySettings_ID FROM tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_discipline_category discCateg WHERE discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discActionCateg.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND ir.prescriptiveStat = $prescriptive_stat AND ir.subjectEmp_ID = $subject_emp_id AND ir.liabilityStat = $liability_stat AND ir.offense_ID = $offense_ID");
		// $fields = "ir.incidentReport_ID, ir.subjectEmp_ID, ir.sourceEmp_ID, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.liabilityStat, ir.datedLiabilityStat, ir.subjectExplanation, ir.occurence, discCateg.disciplineCategory_ID, discCateg.category, discAction.disciplinaryAction_ID, discAction.action, discActionCateg.disciplinaryActionCategory_ID, discActionCateg.level, ir.offense_ID, ir.offense, ir.prescriptiveId, ir.disciplinaryActionCategorySettings_ID";
		// $where = "discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discActionCateg.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND ir.prescriptiveStat = $prescriptive_stat AND ir.subjectEmp_ID = $subject_emp_id AND ir.liabilityStat = $liability_stat AND ir.offense_ID = $offense_ID " . $where_cure_date;
		// $table = "tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_discipline_category discCateg";
		// return $this->general_model->fetch_specific_val($fields, $where, $table);
    }
     //Charise code for iRequest Missed
     public function cronjob_missed(){
        $empID = $this->session->emp_id;
        $dateTime = $this->get_current_date_time();
        $dnow = $dateTime['dateTime'];
        // $query = $this->general_model->custom_query("SELECT DISTINCT(ra.requestApproval_ID),ra.request_ID,ra.level FROM tbl_deadline d, tbl_irequest_request_approval ra, tbl_irequest_manpower_request req WHERE d.module_ID=6 AND d.deadline < '$dnow' AND ra.request_ID = d.request_ID AND req.irequest_ID = d.request_ID AND req.irequest_ID = ra.request_ID");
        // $d_query = $this->general_model->custom_query("SELECT DISTINCT(d.deadline_ID) FROM tbl_deadline d, tbl_irequest_request_approval ra, tbl_irequest_manpower_request req WHERE d.module_ID=6 AND d.deadline < '$dnow' AND ra.request_ID = d.request_ID AND req.irequest_ID = d.request_ID AND req.irequest_ID = ra.request_ID");
        $query = $this->general_model->custom_query("SELECT DISTINCT(ra.requestApproval_ID),ra.request_ID,ra.level,d.deadline_ID FROM tbl_deadline d, tbl_irequest_request_approval ra, tbl_irequest_manpower_request req WHERE d.module_ID=6 AND d.deadline < '$dnow' AND ra.requestApproval_ID = d.request_ID");
        $ar_approversdeadline = array();
        $ar_deadline = array();
        $loop=0;
        $loop2=0;
        foreach($query as $q1){
            $rid = $q1->requestApproval_ID;
            $req_Id = $q1->request_ID;
            if($q1->level == 1 || $q1->level == 2){
                // get next level
                $ql = $q1->level;
                $ql++;
                $query2 = $this->general_model->custom_query("SELECT requestApproval_ID,status FROM tbl_irequest_request_approval WHERE request_ID = $req_Id AND level = $ql AND status=2");
                $r['status'] = 4;
                $rnl = $query2[0]->requestApproval_ID;
                $wherereq = "requestApproval_ID = $rnl";
                if($query2!=null){
                    $this->general_model->update_vals($r, $wherereq, 'tbl_irequest_request_approval');
                }
            }else if($q1->level== 3){
                //set Manpower to Missed
                $re['status'] = 12;
                $wherereq2 = "irequest_ID = $req_Id";
                $this->general_model->update_vals($re, $wherereq2, 'tbl_irequest_manpower_request');
            }

            $query3 = $this->general_model->custom_query("SELECT request_ID,requestApproval_ID FROM tbl_irequest_request_approval WHERE request_ID=$req_Id AND (status=5 OR status=6 OR status=2 OR status=3 OR status=4)");

            // if detected all are missed 
            if($query3==null){
                $re['status'] = 12;
                $wherereq2 = "irequest_ID = $req_Id";
                $this->general_model->update_vals($re, $wherereq2, 'tbl_irequest_manpower_request');
            }

            $ar_approversdeadline[$loop] = [
                'requestApproval_ID' => $q1->requestApproval_ID,
                'status' => 12
            ];
            $loop++;

            $ar_deadline[$loop2] = [
                'deadline_ID' => $q1->deadline_ID,
                'status_ID' => 12
            ];
            $loop2++;
        }
        $this->general_model->batch_update($ar_deadline, 'deadline_ID', 'tbl_deadline');
        $this->general_model->batch_update($ar_approversdeadline, 'requestApproval_ID', 'tbl_irequest_request_approval');
    }
    public function get_dms_category(){
        $data['status'] = 0;
        $discipline_category = $this->get_discipline_category();
        if(count($discipline_category) > 0){
            $data['status'] = 1;
            $data['record'] = $discipline_category;
        }
        echo json_encode($data);
    }
    public function get_dms_recommend_action_categ(){
        $data['status'] = 0;
        $disciplinaryActionCategorySettings_ID = $this->input->post("action_categ_settings");
        $disciplineCategory_ID = $this->input->post("category_id");
        $qry = "SELECT disciplinaryActionCategory_ID,action,a.disciplinaryActionCategorySettings_ID,label FROM `tbl_dms_disciplinary_action_category` a,tbl_dms_disciplinary_action c where a.disciplinaryAction_ID=c.disciplinaryAction_ID and disciplineCategory_ID= $disciplineCategory_ID and a.disciplinaryActionCategorySettings_ID=$disciplinaryActionCategorySettings_ID order by level DESC";
        $disc_action_categ = $this->general_model->custom_query($qry);
        if(count($disc_action_categ) > 0){
            $data['status'] = 1;
            $data['record'] = $disc_action_categ;
        }
        echo json_encode($data);
    }
}