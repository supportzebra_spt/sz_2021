<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Company extends General
{

    protected $title = 'Company';

    public function __construct()
    {
        parent::__construct();
    }

    public function home()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'My Announcements',
        ];
        $this->load_template_view('templates/company/homev2', $data);
    }
    public function tutorials()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Tutorials',
        ];
        $this->load_template_view('templates/company/tutorials', $data);
    }
    public function get_demo_videos()
    {
        $search_string = $this->input->post('search_string');
        $search_category = $this->input->post('search_category');
        $search = array();
        $searchString = "";
        if ($search_string !== '') {
            $search[] = "(title LIKE '%$search_string%' || description LIKE '%$search_string%')";
        }
        if ($search_category !== '') {
            $search[] = "category = '$search_category'";
        }

        if (!empty($search)) {
            $searchString = "WHERE " . implode(" AND ", $search);
        }
        $data = $this->general_model->custom_query("SELECT * FROM tbl_demo_videos $searchString ORDER BY title ASC");
        echo json_encode(['videos' => $data]);
    }
	
   public function cyber()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Cyber Security',
        ];
        $this->load_template_view('templates/company/cyber', $data);
    }
   public function covid()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Covid 19 Info Drive',
        ];
        $this->load_template_view('templates/company/covid', $data);
    }

}
