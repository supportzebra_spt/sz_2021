<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Support_tracker extends General
{

    protected $title = 'Support_tracker';
    
    public $logo = "/var/www/html/sz/assets/images/img/logo2.png";
    // public $logo = "C:\\wamp64\\www\\sz\\assets\\images\\img\\logo2.png";

    public function __construct()
    {
        parent::__construct();
    }

    public function support_tracker() 
    {
        $data = [
            'uri_segment'  => $this->uri->segment_array(),
            'title'        => 'Support Tracker Page',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/support_tracker/support_tracker', $data);
        }
    }

   public function support_tracker_view()
    {
        $emp_id = $this->session->emp_id;
        $datatable = $this->input->post('datatable');
        $query['query'] = " SELECT 
        fp.support_tracker_id,
        fp.title,
        fp.description,
        fp.image,
        fp.link,
        fp.time_report,
        fp.time_confirm,
        fp.time_acknowledge,
        TIMEDIFF(fp.time_confirm, fp.time_report) as duration,
        fp.fixed,
        fp.acknowledged,
        fp.confirmed,
        fp.status,
        a.lname alname,
        a.fname afname,
        a.mname amname,
        f.lname flname,
        f.fname ffname,
        f.mname fmname,
        c.lname clname,
        c.fname cfname,
        c.mname cmname
        FROM 
        tbl_support_tracker fp
        INNER JOIN tbl_employee empA ON empA.emp_id = fp.acknowledged
        INNER JOIN tbl_applicant a ON a.apid = empA.apid
        INNER JOIN tbl_employee empF ON empF.emp_id = fp.fixed
        INNER JOIN tbl_applicant f ON f.apid = empF.apid
        INNER JOIN tbl_employee empC ON empC.emp_id = fp.confirmed
        INNER JOIN tbl_applicant c ON c.apid = empC.apid
        WHERE 
        fp.support_tracker_id IS NOT NULL AND
        ((($emp_id = 26 OR $emp_id = 114)) OR ($emp_id = fp.fixed))";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ', $datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND (fp.title LIKE '%$keyword[$x]%' OR fp.description LIKE '%$keyword[$x]%')";
                $query['search']['total'] .= " AND (fp.title LIKE '%$keyword[$x]%' OR fp.description LIKE '%$keyword[$x]%')";
            }
        }
        if ($datatable['query']['searchStatus'] != '') {
            $stat = $datatable['query']['searchStatus'];
            $query['search']['append'] .= " AND fp.status = $stat";
            $query['search']['total'] .= " AND fp.status = $stat";
        }
        if ($datatable['query']['start'] != '' && $datatable['query']['end'] != '') {
            $start = $datatable['query']['start'];
            $end = $datatable['query']['end'];
            $query['search']['append'] .= " AND DATE(fp.time_report) >= DATE('$start') AND fp.time_report < DATE_ADD('$end', INTERVAL 1 DAY)";
            $query['search']['total'] .= " AND DATE(fp.time_report) >= DATE('$start') AND fp.time_report < DATE_ADD('$end', INTERVAL 1 DAY)";
            
        }
        if ($datatable['query']['acknowledged'] != '') {
            $ackn = $datatable['query']['acknowledged'];
            $query['search']['append'] .= " AND empA.emp_id = $ackn";
            $query['search']['total'] .= " AND empA.emp_id = $ackn";
        }
        if ($datatable['query']['fixed'] != '') {
            $fix = $datatable['query']['fixed'];
            $query['search']['append'] .= " AND empF.emp_id = $fix";
            $query['search']['total'] .= " AND empF.emp_id = $fix";
        }
        // var_dump($query);

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';

        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? ','.$field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= 'ORDER BY time_report ASC'.$order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);
    }
    public function support_tracker_add()
    {
        $link = $this->input->post('link');
        if($link == null){
            try{
                $config['upload_path']="./uploads/support_tracker/";
                $config['allowed_types']='gif|jpg|png';
                $config['encrypt_name'] = TRUE;
                $config['max_size'] = 2048;

                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if($this->upload->do_upload("image")){
                    $image = $this->upload->data('file_name');


                    $array = array(
                        'title'          => $this->input->post('title'),
                        'description'    => $this->input->post('description'),
                        'acknowledged'   => $this->input->post('acknowledged'),
                        'fixed'          => $this->input->post('fixed'),
                        'confirmed'      => $this->input->post('confirmed'),
                        'image'          => $image,
                        'time_report'    => date('Y-m-d H:i:s', strtotime($this->input->post('time_report'))),
                        'time_acknowledge'=> date('Y-m-d H:i:s', strtotime($this->input->post('time_acknowledge'))),
                        'time_confirm'=> date('Y-m-d H:i:s', strtotime($this->input->post('time_confirm'))),
                        'status'=> $this->input->post('status'),
                    );

                    $qry = $this->general_model->insert_vals($array, 'tbl_support_tracker');
                    echo json_encode($qry);
                } else {
                    
                    var_dump($this->upload->display_errors());
                    throw new Exception("Error Processing");
                }
            }catch(Exception $err){
                echo json_encode($err);
            }
        } if($link != null){
            $array = array(
                'title'          => $this->input->post('title'),
                'description'    => $this->input->post('description'),
                'acknowledged'   => $this->input->post('acknowledged'),
                'fixed'          => $this->input->post('fixed'),
                'confirmed'      => $this->input->post('confirmed'),
                'link'           => $this->input->post('link'),
                'time_report'    => date('Y-m-d H:i:s', strtotime($this->input->post('time_report'))),
                'time_acknowledge'=> date('Y-m-d H:i:s', strtotime($this->input->post('time_acknowledge'))),
                'time_confirm'=> date('Y-m-d H:i:s', strtotime($this->input->post('time_confirm'))),
                'status'=> $this->input->post('status'),
            );
            $qry = $this->general_model->insert_vals($array, 'tbl_support_tracker');
            echo json_encode($qry);
        }
        
    }

public function support_tracker_update()
    {
        $image = $this->input->post('upimage');
        $link = $this->input->post('uplink');
        $support_tracker_id = $this->input->post('upsupport_tracker_id');
        // var_dump($support_tracker_id);
        if($image == null){
            try{
                $config['upload_path']="./uploads/support_tracker/";
                $config['allowed_types']='gif|jpg|png';
                $config['encrypt_name'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '60%';
                $config['width'] = 800;
                $config['height'] = 800;

                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if($this->upload->do_upload("upimage")){
                    $image = $this->upload->data('file_name');

                    $array = array(
                        'title'          => $this->input->post('uptitle'),
                        'description'    => $this->input->post('updescription'),
                        'acknowledged'   => $this->input->post('upacknowledged'),
                        'fixed'          => $this->input->post('upfixed'),
                        'confirmed'      => $this->input->post('upconfirmed'),
                        'image'          => $image,
                        'link'           => null,
                        'time_report'    => date('Y-m-d H:i:s', strtotime($this->input->post('uptime_report'))),
                        'time_acknowledge'=> date('Y-m-d H:i:s', strtotime($this->input->post('uptime_acknowledge'))),
                        'time_confirm'=> date('Y-m-d H:i:s', strtotime($this->input->post('uptime_confirm'))),
                        'status'         => $this->input->post('upstatus'),
                    );

                    $query = $this->general_model->update_vals($array, "support_tracker_id =$support_tracker_id", "tbl_support_tracker");
                    echo json_encode($query);
                } else {
                    echo $this->upload->display_errors();
                }
            }catch(Exception $err){
                var_dump($err);
            }
        } if($link != null){
        
        $data = array(
            'title'          => $this->input->post('uptitle'),
            'description'    => $this->input->post('updescription'),
            'acknowledged'   => $this->input->post('upacknowledged'),
            'fixed'          => $this->input->post('upfixed'),
            'confirmed'      => $this->input->post('upconfirmed'),
            'link'           => $this->input->post('uplink'),
            'image'          => null,
            'status'         => $this->input->post('upstatus'),
            'time_report'    => date('Y-m-d H:i:s', strtotime($this->input->post('uptime_report'))),
            'time_acknowledge'=> date('Y-m-d H:i:s', strtotime($this->input->post('uptime_acknowledge'))),
            'time_confirm'=> date('Y-m-d H:i:s', strtotime($this->input->post('uptime_confirm'))),
        );
        $query = $this->general_model->update_vals($data, "support_tracker_id =$support_tracker_id", "tbl_support_tracker");
        echo json_encode($query);
        } else {
             $data = array(
            'title'          => $this->input->post('uptitle'),
            'description'    => $this->input->post('updescription'),
            'acknowledged'   => $this->input->post('upacknowledged'),
            'fixed'          => $this->input->post('upfixed'),
            'confirmed'      => $this->input->post('upconfirmed'),
            // 'link'           => $this->input->post('uplink'),
            // 'image'          => null,
            'status'         => $this->input->post('upstatus'),
            'time_report'    => $this->input->post('uptime_report'),
            'time_acknowledge'=> $this->input->post('uptime_acknowledge'),
            'time_confirm'   => $this->input->post('uptime_confirm'),
        );
        $query = $this->general_model->update_vals($data, "support_tracker_id =$support_tracker_id", "tbl_support_tracker");
        echo json_encode($query);
        }
    }

    public function support_tracker_delete()
    {
        $support_tracker_id = $this->input->post('support_tracker_id');
        $data = $this->general_model->delete_vals("support_tracker_id =$support_tracker_id", "tbl_support_tracker");
        echo json_encode($data);
    }

    public function spt_list()
    {
        $data = $this->general_model->fetch_specific_vals(
            "app.lname, app.fname, app.mname, emp.emp_id",
            "emp.apid=app.apid AND emp.isActive='yes' AND emp.acc_id=37",
            "tbl_applicant app, tbl_employee emp");
        echo json_encode($data);
    }

    private function support_tracker_data($searchField,$searchStatus,$start,$end,$acknowledged,$fixed)
    {
        $search_filter = '';
        if ($searchField != '') {
            $keyword = explode(' ', $searchField);
            for($x=0; $x<count($keyword); $x++){
                $search_filter .= " AND (fp.title LIKE '%$keyword[$x]%' OR fp.description LIKE '%$keyword[$x]%')";
            }
        }

        $date_filter = '';
        if ($start != '' && $end != '') {
            $date_filter = " AND DATE(fp.time_report) >= DATE('$start') AND fp.time_report < DATE_ADD('$end', INTERVAL 1 DAY)";
        }
        
        $status_filter = '';
        if ($searchStatus != '') {
            $status_filter = " AND fp.status = $searchStatus";
        }

        $acknowledged_filter = '';
        if ($acknowledged != '') {
            $acknowledged_filter = " AND empA.emp_id = $acknowledged";
        }

        $fixed_filter = '';
        if ($fixed != '') {
            $fixed_filter = " AND empF.emp_id = $fixed";
        }
        $emp_id = $this->session->emp_id;
        $query = " SELECT 
        fp.support_tracker_id,
        fp.title,
        fp.description,
        fp.image,
        fp.link,
        fp.time_report,
        fp.time_confirm,
        fp.time_acknowledge,
        TIMEDIFF(fp.time_confirm, fp.time_report) as duration,
        fp.fixed,
        fp.acknowledged,
        fp.confirmed,
        fp.status,
        a.lname alname,
        a.fname afname,
        a.mname amname,
        f.lname flname,
        f.fname ffname,
        f.mname fmname,
        c.lname clname,
        c.fname cfname,
        c.mname cmname
        FROM 
        tbl_support_tracker fp
        INNER JOIN tbl_employee empA ON empA.emp_id = fp.acknowledged
        INNER JOIN tbl_applicant a ON a.apid = empA.apid
        INNER JOIN tbl_employee empF ON empF.emp_id = fp.fixed
        INNER JOIN tbl_applicant f ON f.apid = empF.apid
        INNER JOIN tbl_employee empC ON empC.emp_id = fp.confirmed
        INNER JOIN tbl_applicant c ON c.apid = empC.apid
        WHERE 
        fp.support_tracker_id IS NOT NULL AND
        ((($emp_id = 26 OR $emp_id = 114)) OR ($emp_id = fp.fixed)) $search_filter $date_filter $status_filter $acknowledged_filter $fixed_filter";
        
        $data = $this->general_model->custom_query($query);
        return array(
            'data' => $data,
            'currentDate'=>date('M d, Y'),
            'currentTime'=>date('h:i a'),
        );
    }

    public function downloadExcel_support_tracker()
    {
        $searchField    = $this->input->post('searchField');
        $searchStatus   = $this->input->post('searchStatus');
        $start          = $this->input->post('start');
        $end            = $this->input->post('end');
        $acknowledged   = $this->input->post('acknowledged');
        $fixed          = $this->input->post('fixed');
        $final_data     = $this->support_tracker_data($searchField,$searchStatus,$start,$end,$acknowledged,$fixed);
        //EXCEL PROPER
        $this->load->library('PHPExcel', NULL, 'excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->createSheet(1);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Support Tracker Report');
        $this->excel->getActiveSheet()->setShowGridlines(false);
        //------------------------INSERT LOGO-------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(5);  //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(80); // logo height

        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
        //set cell A1 content with some text


        $this->excel->getActiveSheet()->setCellValue('A3', 'GENERATED ON: '. $final_data['currentDate'].' '.$final_data['currentTime']);

        $this->excel->getActiveSheet()->setCellValue('A5', 'Title');
        $this->excel->getActiveSheet()->setCellValue('B5', 'Description');
        $this->excel->getActiveSheet()->setCellValue('C5', 'Acknowledged By');
        $this->excel->getActiveSheet()->setCellValue('D5', 'Fixed By');
        $this->excel->getActiveSheet()->setCellValue('E5', 'Date Reported');
        $this->excel->getActiveSheet()->setCellValue('F5', 'Date Acknowledged');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Date Fixed');
   
        $emprow = 6;
        foreach ($final_data['data'] as $data) {
            
            $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $data->title);
            $this->excel->getActiveSheet()->getStyle('A' . $emprow)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));

            $this->excel->getActiveSheet()->getStyle('B'. $emprow .':G'. $emprow)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
            $this->excel->getActiveSheet()->setCellValue('B' . $emprow, $data->description);
            $this->excel->getActiveSheet()->setCellValue('C' . $emprow, $data->afname.' '.$data->alname);
            $this->excel->getActiveSheet()->setCellValue('D' . $emprow, $data->ffname.' '.$data->flname);
            $this->excel->getActiveSheet()->setCellValue('E' . $emprow, $data->time_report);
            $this->excel->getActiveSheet()->setCellValue('F' . $emprow, $data->time_acknowledge);
            $this->excel->getActiveSheet()->setCellValue('G' . $emprow, $data->time_confirm);
            $emprow++;
           
        }


        $column = $this->excel->getActiveSheet()->getHighestColumn();
        $row = $this->excel->getActiveSheet()->getHighestRow();

        for($x = 6;$x<=$row;$x++){
            $this->excel->getActiveSheet()->getRowDimension($x)->setRowHeight(-1);
            $this->excel->getActiveSheet()->getStyle('A'.$x.':B'.$x)->getAlignment()->setWrapText(true);
        }

        $this->excel->getActiveSheet()->getStyle('C3:' . $column . '3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'ffffff')), 'font' => array('bold' => true, 'color' => array('rgb' => '002060'))));
        $this->excel->getActiveSheet()->getStyle('C3:' . $column . '3')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        
        // $this->excel->getActiveSheet()->getStyle('A5:' . $column . '' . $row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
        $this->excel->getActiveSheet()->getStyle('A5:' . $column . '5')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '0074CD')), 'font' => array('bold' => true, 'color' => array('rgb' => 'ffffff'))));
        $this->excel->getActiveSheet()->getStyle('A5:' . $column . '5')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('A5:' . $column . '' . $row)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

        $filename = 'Support_Tracker.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
}