<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Discipline_mic extends General
{
    protected function profile_nature_offense($emp_id, $range_start, $range_end, $liability_stat)
    {
        $where_liability = "AND ir.liabilityStat IN (17, 18)";
        if ((int) $liability_stat !== 0) {
            $where_liability = "AND ir.liabilityStat = $liability_stat";
        }
        $fields = "DISTINCT(offType.offenseType_ID) offType_id, offType.letter, offType.offenseType, COUNT(offType.offenseType_ID) offType_count";
        $where = "offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID  $where_liability  AND ir.subjectEmp_ID = $emp_id AND DATE(ir.incidentDate) >= '$range_start' AND DATE(ir.incidentDate) <= '$range_end' GROUP BY offType.offenseType_ID";
        $table = "tbl_dms_incident_report ir, tbl_dms_offense off, tbl_dms_offense_type offType";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    protected function profile_offense($emp_id, $range_start, $range_end, $offense_type_id_val, $liability_stat)
    {
        $offense_type_where = "";
        $offense_type_id = (int) $offense_type_id_val;
        $where_liability = "AND ir.liabilityStat IN (17, 18)";
        if ((int) $liability_stat !== 0) {
            $where_liability = "AND ir.liabilityStat = $liability_stat";
        }
        if ((int) $offense_type_id !== 0) {
            $offense_type_where = "AND off.offenseType_ID = $offense_type_id";
        }
        $fields = "DISTINCT(ir.offense_ID) offenseId, off.offense, COUNT(ir.offense_ID) offense_count, offType.offenseType_ID, offType.offenseType";
        $where = "offType.offenseType_ID = off.offenseType_ID " . $offense_type_where . " AND off.offense_ID = ir.offense_ID $where_liability AND ir.subjectEmp_ID = $emp_id AND DATE(ir.incidentDate) >= '$range_start' AND DATE(ir.incidentDate) <= '$range_end' GROUP BY off.offense_ID";
        $table = "tbl_dms_offense off, tbl_dms_incident_report ir, tbl_dms_offense_type offType";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    public function export_ir_history_data($liability_stat, $offense_id, $range_start, $range_end, $emp_id)
    {

        $liability_stat_where = "AND ir.liabilityStat IN (17, 18)";
        if ((int) $liability_stat !== 0) {
            $liability_stat_where = "AND ir.liabilityStat = $liability_stat";
        }
        // $fields = "ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.occurence, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label";
        // $where = "offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND ir.subjectEmp_ID = $emp_id AND $liability_stat_where AND ir.offense_ID = $offense_id AND DATE(ir.incidentDate) >= '$range_start' AND DATE(ir.incidentDate) <= '$range_end'";
        // $table = "tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin";
        // // return $this->general_model->fetch_specific_vals($fields, $where, $table);
        // return $this->general_model->fetch_specific_vals($fields, $where, $table);
        $query = "SELECT termDates.date termDate, term.days,ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, 
        ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.acc_id, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, 
        liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, 
        ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description filingTypeDescription, 
        ir.incidentReportStages_ID, irStages.description as irstageDescription, ir.subjectEmp_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, 
        subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, 
        sourceApp.nameExt sourceNameExt, dmsCategSug.category, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, 
        dmsActionFin.action finalAction, dmsActionFin.label
        FROM tbl_dms_incident_report ir 
        INNER JOIN tbl_status prescStat ON prescStat.status_ID = ir.prescriptiveStat 
        INNER JOIN tbl_status liableStat ON liableStat.status_ID = ir.liabilityStat 
        INNER JOIN tbl_dms_incident_report_filing_type irFilingType ON irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID 
        INNER JOIN tbl_dms_incident_report_stages irStages ON irStages.incidentReportStages_ID = ir.incidentReportStages_ID 
        INNER JOIN tbl_employee subEmp ON subEmp.emp_id = ir.subjectEmp_ID
        INNER JOIN tbl_employee sourceEmp ON sourceEmp.emp_id = ir.sourceEmp_ID
        INNER JOIN tbl_applicant sourceApp ON sourceApp.apid = sourceEmp.apid 
        INNER JOIN tbl_applicant subApp ON subApp.apid = subEmp.apid
        INNER JOIN tbl_dms_offense off ON off.offense_ID = ir.offense_ID
        INNER JOIN tbl_dms_offense_type offType ON offType.offenseType_ID = off.offenseType_ID
        INNER JOIN tbl_dms_disciplinary_action_category dmsActionCategSug ON dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID
        INNER JOIN tbl_dms_disciplinary_action_category dmsActionCategFin ON  dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID
        INNER JOIN tbl_dms_disciplinary_action dmsActionSug ON dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID
        INNER JOIN tbl_dms_disciplinary_action dmsActionFin ON dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID
        INNER JOIN tbl_dms_discipline_category dmsCategSug ON dmsCategSug.disciplineCategory_ID = dmsActionCategSug.disciplineCategory_ID
        LEFT JOIN tbl_dms_suspension_termination_days term ON ir.incidentReport_ID=term.incidentReport_ID
        LEFT JOIN tbl_dms_suspension_dates termDates ON termDates.suspensionTerminationDays_ID=term.suspensionTerminationDays_ID
        WHERE ir.incidentDate >= '$range_start' AND ir.incidentDate <= '$range_end' $liability_stat_where AND ir.offense_ID = $offense_id AND ir.subjectEmp_ID = $emp_id GROUP BY ir.incidentReport_ID ORDER BY ir.incidentReport_ID ASC";
        return $this->general_model->custom_query($query);
    }

    public function export_users()
    {
        $fields = "emp.id_num employeeId, CONCAT(app.lname, ' ', app.fname,  ' ', app.mname) employeeName, dep.dep_details Team, acc.acc_name Account, CONCAT(appSup.lname, ' ' , appSup.fname) Supervisor, users.username";
        $where = " dep.dep_id = acc.dep_id AND acc.acc_id = emp.acc_id AND app.apid = emp.apid AND users.emp_id = emp.emp_id AND emp.isActive = 'yes' AND appSup.apid = empSup.apid AND empSup.emp_id = emp.Supervisor";
        $table = " tbl_employee as emp, tbl_applicant as app, tbl_user as users, tbl_account as acc, tbl_department as dep, tbl_applicant as appSup, tbl_employee as empSup";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table, 'app.lname ASC');
        echo json_encode($record);
    }

    protected function get_off_nature_off($offense_id)
    {
        $fields = "off.offense, off.orderNum, offType.offenseType, offType.letter";
        $where = " offType.offenseType_ID = off.offenseType_ID AND off.offense_ID= $offense_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_offense off, tbl_dms_offense_type offType');
    }

    public function ir_history_excel_personal()
    {
        $liability_stat = $this->input->post('liabilityStat');
        $offense_id = $this->input->post('offenseId');
        $range_start = $this->input->post('range_start');
        $range_end = $this->input->post('range_end');
        $emp_id = $this->input->post('empId');
        $history_data = $this->export_ir_history_data($liability_stat, $offense_id, $range_start, $range_end, $emp_id);
        // var_dump($history_data);

        $logo = $this->dir . '/assets/images/img/logo2.png';
        $this->load->library('PHPExcel', null, 'excel');
        $this->excel->createSheet(0);
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Daily Pulse');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($logo);
        $objDrawing->setOffsetX(5); // setOffsetX works properly
        $objDrawing->setOffsetY(20); //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(45); // logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->setShowGridlines(false);
        $subj_det = $this->get_emp_details_via_emp_id($emp_id);
        $off_details = $this->get_off_nature_off($offense_id);
        $header = [
            ['col' => '', 'id' => 'C2', 'title' => 'IR History'],
            ['col' => '', 'id' => 'C3', 'title' => 'Subject: ' . ucwords($subj_det->lname . "," . $subj_det->fname . " " . $subj_det->mname)],
            ['col' => '', 'id' => 'C4', 'title' => 'Nature: ' . $off_details->offenseType],
            ['col' => '', 'id' => 'C5', 'title' => 'Offense: ' . $off_details->offense],
            ['col' => '', 'id' => 'C6', 'title' => 'Date: ' . date_format(date_create($range_start), "M j, Y") . ' - ' . date_format(date_create($range_end), "M j, Y")],
            ['col' => 'A', 'id' => 'A8', 'title' => 'IR-ID'],
            ['col' => 'B', 'id' => 'B8', 'title' => 'Incident Date'],
            ['col' => 'C', 'id' => 'C8', 'title' => 'Incident Time'],
            ['col' => 'D', 'id' => 'D8', 'title' => 'DateTime Filed'],
            ['col' => 'E', 'id' => 'E8', 'title' => 'Employee Type'],
            ['col' => 'F', 'id' => 'F8', 'title' => 'Positions'],
            ['col' => 'G', 'id' => 'G8', 'title' => 'Employee Status'],
            ['col' => 'H', 'id' => 'H8', 'title' => 'Account'],
            ['col' => 'I', 'id' => 'I8', 'title' => 'Class'],
            ['col' => 'J', 'id' => 'J8', 'title' => 'Incident'],
            ['col' => 'K', 'id' => 'K8', 'title' => 'Incident Place'],
            ['col' => 'L', 'id' => 'L8', 'title' => 'Expected Action'],
            ['col' => 'M', 'id' => 'M8', 'title' => 'Cure Date'],
            ['col' => 'N', 'id' => 'N8', 'title' => 'Prescriptive Status'],
            ['col' => 'O', 'id' => 'O8', 'title' => 'Liability Status'],
            ['col' => 'P', 'id' => 'P8', 'title' => 'Date of Liability'],
            ['col' => 'Q', 'id' => 'Q8', 'title' => 'Explanation Date'],
            ['col' => 'R', 'id' => 'R8', 'title' => 'Subject Explanation'],
            ['col' => 'S', 'id' => 'S8', 'title' => 'Type of Filing'],
            ['col' => 'T', 'id' => 'T8', 'title' => 'Source First Name'],
            ['col' => 'U', 'id' => 'U8', 'title' => 'Source Middle Name'],
            ['col' => 'V', 'id' => 'V8', 'title' => 'Source Last Name'],
            ['col' => 'W', 'id' => 'W8', 'title' => 'Suggested Action'],
            ['col' => 'X', 'id' => 'X8', 'title' => 'Final Action'],
            ['col' => 'Y', 'id' => 'Y8', 'title' => 'Effectivity'],

        ];
        for ($excel_data_header_loop = 0; $excel_data_header_loop < count($header); $excel_data_header_loop++) {
            $this->excel->getActiveSheet()->setCellValue($header[$excel_data_header_loop]['id'], $header[$excel_data_header_loop]['title']);
            $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
            $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
            $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $emprow = 9;
        foreach ($history_data as $ir) {
            $effectivity = "";
            if ($ir->label === 's') {
                $effectivity  = $ir->days;
                $sus_dates = $this->get_suspension_dates($ir->incidentReport_ID);

                if (count($sus_dates) > 0) {
                    $date_unit = "day";
                    if (count($sus_dates) > 1) {
                        $date_unit .= "s";
                    }
                    $effectivity .= " " . $date_unit;
                    $dates = [];
                    foreach ($sus_dates as $row_index => $row_value) {
                        $dates[$row_index] = $row_value->date;
                    }
                    $effectivity .= " (" . implode(', ', $dates) . ")";
                }
            } else if ($ir->label === 't') {
                $effectivity = "Effective on " . date('Y/m/d', strtotime($ir->termDate));
            }
            $this->excel->getActiveSheet()->setCellValue('A' . $emprow, sprintf("%08d", $ir->incidentReport_ID));
            $this->excel->getActiveSheet()->setCellValue('B' . $emprow, date('Y/m/d', strtotime($ir->incidentDate)));
            $this->excel->getActiveSheet()->setCellValue('C' . $emprow, date('H:i', strtotime($ir->incidentTime)));
            $this->excel->getActiveSheet()->setCellValue('D' . $emprow, date('Y/m/d H:i', strtotime($ir->dateTimeFiled)));
            $this->excel->getActiveSheet()->setCellValue('E' . $emprow, ucwords($ir->empType));
            $this->excel->getActiveSheet()->setCellValue('F' . $emprow, ucwords($ir->subjectPosition));
            $this->excel->getActiveSheet()->setCellValue('G' . $emprow, ucwords($ir->subjectEmpStat));
            $this->excel->getActiveSheet()->setCellValue('H' . $emprow, ucwords($ir->subjectAccount));
            $this->excel->getActiveSheet()->setCellValue('I' . $emprow, ucwords($ir->category));
            $this->excel->getActiveSheet()->setCellValue('J' . $emprow, $ir->details);
            $this->excel->getActiveSheet()->setCellValue('K' . $emprow, $ir->place);
            $this->excel->getActiveSheet()->setCellValue('L' . $emprow, $ir->expectedAction);
            $this->excel->getActiveSheet()->setCellValue('M' . $emprow, date('Y/m/d', strtotime($ir->prescriptionEnd)));
            $this->excel->getActiveSheet()->setCellValue('N' . $emprow, ucwords($ir->prescriptiveStatDesc));
            $this->excel->getActiveSheet()->setCellValue('O' . $emprow, ucwords($ir->liabilityStatDesc));
            $this->excel->getActiveSheet()->setCellValue('P' . $emprow, date('Y/m/d H:i', strtotime($ir->datedLiabilityStat)));
            $this->excel->getActiveSheet()->setCellValue('Q' . $emprow, date('Y/m/d H:i', strtotime($ir->explanationDate)));
            $this->excel->getActiveSheet()->setCellValue('R' . $emprow, $ir->subjectExplanation);
            $this->excel->getActiveSheet()->setCellValue('S' . $emprow, ucwords($ir->filingTypeDescription));
            $this->excel->getActiveSheet()->setCellValue('T' . $emprow, ucwords($ir->sourceFname));
            $this->excel->getActiveSheet()->setCellValue('U' . $emprow, ucwords($ir->sourceMname));
            $this->excel->getActiveSheet()->setCellValue('V' . $emprow, ucwords($ir->sourceLname));
            $this->excel->getActiveSheet()->setCellValue('W' . $emprow, $ir->suggestedAction);
            $this->excel->getActiveSheet()->setCellValue('X' . $emprow, $ir->finalAction);
            $this->excel->getActiveSheet()->setCellValue('Y' . $emprow, $effectivity);
            $emprow++;
        }


        $excessletters = ['D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y'];
        foreach ($excessletters as $l) {
            $this->excel->getActiveSheet()->getColumnDimension($l)->setAutoSize(true);
        }


        $this->excel->getActiveSheet()->getStyle('C2:C2')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('C2:C6')->getFont()->getColor()->setRGB('000000');
        $this->excel->getActiveSheet()->getStyle('C2:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->getStyle('A8:Y8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(17);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('c')->setWidth(15);
        $this->excel->getActiveSheet()->freezePane('D9');
        $this->excel->getActiveSheet()->getStyle('A8:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'DDDDDD'),
                    ),
                ),
            )
        );
        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);
        $filename = ucwords($subj_det->lname . " " . $subj_det->fname . " " . $subj_det->mname) . ' - IR History Nature of Offense ' . strtoupper($off_details->letter) . '_Offense No.' . $off_details->orderNum . ' ' . date_format(date_create($range_start), "Y-m-d") . ' to ' . date_format(date_create($range_end), "Y-m-d") . '.xlsx'; //save our workbook as this file name
        // header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        header('Set-Cookie: fileDownload=true; path=/');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
    }

    public function offense_history_datatable()
    {
        $datatable = $this->input->post('datatable');
        $liability_stat = $datatable['query']['liabilityStat'];
        $offense_id = $datatable['query']['offenseId'];
        $range_start = $datatable['query']['range_start'];
        $range_end = $datatable['query']['range_end'];
        $emp_id = $datatable['query']['empId'];
        $liability_stat_where = "ir.liabilityStat IN (17, 18)";
        if ((int) $liability_stat !== 0) {
            $liability_stat_where = "ir.liabilityStat = $liability_stat";
        }
        $query['query'] = "SELECT ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.occurence, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label FROM tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin WHERE offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND ir.subjectEmp_ID = $emp_id AND $liability_stat_where AND ir.offense_ID = $offense_id AND DATE(ir.incidentDate) >= '$range_start' AND DATE(ir.incidentDate) <= '$range_end'";
        if ($datatable['query']['searchProfileHistory'] != '') {
            $keyword = $datatable['query']['searchProfileHistory'];
            $where = "ir.incidentReport_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    // public function get_nature_offense($emp_id, $range_start, $range_end){
    //     $nature['with_nature_offense'] = 0;
    //     $emp_id = 557;
    //     $range_start = '2019-01-01';
    //     $range_end = '2019-08-31';
    //     $nature_of_offense = $this->profile_nature_offense($emp_id, $range_start, $range_end);
    //     if (count($nature_of_offense) > 0) {
    //         $nature['with_nature_offense'] = 1;
    //         $nature['record'] = $nature_of_offense;
    //     }
    //     echo json_encode($nature);
    // }
    public function get_ir_counts($emp_id, $range_start, $range_end, $offense_type_id, $liability_stat)
    {
        $where_liability = "AND ir.liabilityStat IN (17, 18)";
        $offense_type_where = "";
        if ((int) $liability_stat !== 0) {
            $where_liability = "AND ir.liabilityStat = $liability_stat";
        }
        if ((int) $offense_type_id !== 0) {
            $offense_type_where = "AND off.offenseType_ID = $offense_type_id";
        }
        $fields = "ir.incidentReport_ID, discAction.action, COUNT(discAction.action) actionCount";
        $where = "discAction.disciplinaryAction_ID = actionCateg.disciplinaryAction_ID AND actionCateg.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND off.offense_ID = ir.offense_ID $offense_type_where AND ir.subjectEmp_ID = $emp_id $where_liability AND DATE(ir.incidentDate) >= '$range_start' AND DATE(ir.incidentDate) <= '$range_end' GROUP BY discAction.action";
        $table = "tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category actionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_offense off";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "actionCount DESC");
    }

    public function get_personal_nature_offense()
    {
        $nature['with_nature_offense'] = 0;
        $nature_of_offense = $this->profile_nature_offense($this->session->userdata('emp_id'), $this->input->post('range_start'), $this->input->post('range_end'), $this->input->post('liabilityStat'));
        if (count($nature_of_offense) > 0) {
            $nature['with_nature_offense'] = 1;
            $nature['record'] = $nature_of_offense;
        }
        echo json_encode($nature);
    }

    public function get_personal_offenses()
    {
        $nature['with_offense'] = 0;
        $nature_of_offense = $this->profile_offense($this->input->post('emp_id'), $this->input->post('range_start'), $this->input->post('range_end'), $this->input->post('natureOffense'), $this->input->post('liabilityStat'));
        if (count($nature_of_offense) > 0) {
            $nature['with_offense'] = 1;
            $nature['record'] = $nature_of_offense;
        }
        echo json_encode($nature);
    }

    public function get_personal_profile_ir_counts()
    {
        $counts['with_disc_action_count'] = 0;
        $ir_counts = $this->get_ir_counts($this->input->post('emp_id'), $this->input->post('range_start'), $this->input->post('range_end'), $this->input->post('natureOffense'), $this->input->post('liabilityStat'));
        if (count($ir_counts) > 0) {
            $counts['with_disc_action_count'] = 1;
            $counts['record'] = $ir_counts;
        }
        echo json_encode($counts);
    }

    public function get_nature_offense_chart_data()
    {
        $nature['with_offense'] = 0;
        $offenses_by_nature_off = [];
        $nature_off_series = [];
        $offense_series = [];
        $dataValArr = [];
        $nature_of_offense = $this->profile_nature_offense($this->input->post('emp_id'), $this->input->post('range_start'), $this->input->post('range_end'), $this->input->post('liabilityStat'));
        $offense = $this->profile_offense($this->input->post('emp_id'), $this->input->post('range_start'), $this->input->post('range_end'), 0, $this->input->post('liabilityStat'));
        if (count($nature_of_offense) > 0) {
            $nature['with_offense'] = 1;
            foreach ($nature_of_offense as $nature_of_offense_index => $nature_of_offense_row) {
                // var_dump($nature_of_offense_row);
                $offenseType_ID = $nature_of_offense_row->offType_id;
                $natureOffName[$nature_of_offense_index] = [
                    'name' => $nature_of_offense_row->offenseType,
                    'y' => (int) $nature_of_offense_row->offType_count,
                    'drilldown' => $nature_of_offense_row->offType_id
                ];
                $offenses_by_nature_off = array_filter(
                    $offense,
                    function ($e) use ($offenseType_ID) {
                        return ($e->offenseType_ID ==  $offenseType_ID);
                    }
                );
                // var_dump($offenses_by_nature_off);
                $offenses_by_nature_off_index = 0;
                foreach ($offenses_by_nature_off as $offenses_by_nature_off_row) {
                    // var_dump($offenses_by_nature_off_row);
                    $dataValArr[$offenses_by_nature_off_index] = [
                        $offenses_by_nature_off_row->offense,
                        (int) $offenses_by_nature_off_row->offense_count
                    ];
                    $offenses_by_nature_off_index++;
                }
                $offense_series[$nature_of_offense_index] = [
                    'name' => $nature_of_offense_row->offenseType,
                    'id' => $nature_of_offense_row->offType_id,
                    'data' =>  $dataValArr
                ];
            }
            $nature['nature_offense_series'] = $natureOffName;
            $nature['nature_offense_series'] = $natureOffName;
            $nature['offense_series'] = $offense_series;
        }
        echo json_encode($nature);
    }

    public function get_ero_assigned_emp()
    {
        $emp_id = $this->session->userdata('emp_id');
        $query = "SELECT DISTINCT(ir.subjectEmp_ID) emp_id, app.fname, app.mname, app.lname, app.nameExt FROM tbl_dms_incident_report ir, tbl_dms_employee_relations_officer ero, tbl_applicant app, tbl_employee emp where app.apid = emp.apid AND emp.emp_id = ir.subjectEmp_ID AND ir.acc_id = ero.acc_id AND ero.emp_id = $emp_id AND ir.liabilityStat IN (17, 18) UNION SELECT DISTINCT(emp.emp_id) emp_id, app.fname, app.mname, app.lname, app.nameExt FROM tbl_dms_employee_relations_supervisor eroSup, tbl_applicant app, tbl_employee emp WHERE app.apid = emp.apid AND emp.emp_id= eroSup.emp_id AND eroSup.employeeRelationsSupervisor = $emp_id";
        return $this->general_model->custom_query($query);
    }

    public function get_ir_emp()
    {
        $fields = "DISTINCT(ir.subjectEmp_ID) emp_id, app.fname, app.mname, app.lname, app.nameExt";
        $where = "app.apid = emp.apid AND emp.emp_id = ir.subjectEmp_ID AND ir.liabilityStat IN (17, 18)";
        $table = "tbl_dms_incident_report ir, tbl_applicant app, tbl_employee emp";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, 'app.lname ASC');
    }

    public function access_employee_ir_profile()
    {
        $emp_details = $this->get_emp_details_via_emp_id($this->input->post('emp_id'));
        echo json_encode($emp_details);
    }

    public function get_employee_ir_profile_list()
    {
        // $this->session->userdata('emp_id');
        $data['exist'] = 0;
        if ($this->input->post('pageNum') == 1) {
            $ir_emp = $this->get_ir_emp();
            if (count($ir_emp) > 0) {
                if (count($ir_emp) > 0) {
                    $data['exist'] = 1;
                    $data['record'] = $ir_emp;
                }
            }
        } else if ($this->input->post('pageNum') == 2) {
            $assigned_emp = $this->get_ero_assigned_emp();
            if (count($assigned_emp) > 0) {
                if (count($assigned_emp) > 0) {
                    $data['exist'] = 1;
                    $data['record'] = $assigned_emp;
                }
            }
        } else if ($this->input->post('pageNum') == 3) {
            $subordinates = $this->getEmployeeSubordinate($this->session->uid);
            if (count($subordinates) > 0) {
                $data['exist'] = 1;
                $data['record'] = $subordinates;
            }
        }
        echo json_encode($data);
        // var_dump($subordinates);
        // $empids = array();
        // foreach ($subordinates as $sub) {
        //     $empids[] = $sub->emp_id;
        // }
        // if (empty($empids)) {
        //     $empids[] = 0;
        // }
        // var_dump($empids);
        // $search[] = "a.user_ID IN (" . implode(',', $uids) . ")";
    }

    // PROCESS STR

    private function duplicate_data($data, $table)
    {
        return $this->general_model->insert_vals_last_inserted_id($data, $table);
    }

    private function duplicate_mult_data($data, $table)
    {
        return $this->general_model->batch_insert($data, $table);
    }

    private function get_subtask_comp_qry($subtask_id)
    {
        $fields = "componentSubtask_ID, compcontent, sequence, correctAnswer";
        $where = "subTask_ID = $subtask_id";
        $table = "tbl_processST_component_subtask";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    private function get_all_subtask_comp_qry($subtask_arr)
    {
        $fields = "componentSubtask_ID, subTask_ID, compcontent, sequence, correctAnswer";
        $where = "subTask_ID IN ($subtask_arr)";
        $table = "tbl_processST_component_subtask";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    private function get_subtask_qry($task_id)
    {
        $fields = "*";
        $where = "task_ID = $task_id";
        $table = "tbl_processST_subtask";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    private function get_task_qry($task_id)
    {
        $fields = "process_ID, taskTitle, sequence, status_ID, createdBy, updatedBy";
        $where = "task_ID = $task_id";
        $table = "tbl_processST_task";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    private function get_task_checklist_qry($task_id)
    {
        $fields = "checklist_ID";
        $where = "task_ID = $task_id";
        $table = "tbl_processST_checklistStatus";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }

    public function check_task_details()
    {
        $task_id = $this->input->post('taskId');
        $task = $this->get_task_qry($task_id);
        $data['exist'] = 0;
        if ($task != NULL) {
            $data['exist'] = 1;
            $data['record'] = $task;
        }
        echo json_encode($data);
    }

    public function fetch_allsubtask_count2($task_ID)
    {
        // $task_ID = $_POST['task_ID'];
        // $data = $this->general_model->fetch_specific_vals("max(sequence) as max", "task_ID=$task_ID", "tbl_processST_subtask", "sequence ASC");
        $q = "SELECT max(sequence) as max FROM tbl_processST_subtask WHERE task_ID=$task_ID";
        $data = $this->general_model->custom_query($q);
        return $data[0]->max;
        // echo json_encode($data);
    }

    public function task_duplication()
    {
        $task_id = $this->input->post('taskId');
        $task = $this->get_task_qry($task_id);
        $task_checklist = $this->get_task_checklist_qry($task_id);
        $dup_task_checklist = [];
        // var_dump($task_checklist);
        $duplication['exist'] = 0;
        if ($task != NULL) {
            // duplicate task
            $duplication['exist'] = 1;
            $duplication['record'] = $task;
            $dupTask['process_ID'] = $task->process_ID;
            $dupTask['taskTitle'] = $task->taskTitle;
            $dupTask['sequence'] = $task->sequence + 1;
            $dupTask['status_ID'] = 2;
            $dupTask['createdBy'] =  $this->session->userdata('uid');
            $dupTask['updatedBy'] =  $this->session->userdata('uid');
            $new_task_id = $this->duplicate_data($dupTask, 'tbl_processST_task');
            if (count($task_checklist) > 0) {
                foreach ($task_checklist as $task_checklist_index => $task_checklist_row) {
                    $dup_task_checklist[$task_checklist_index] = [
                        "checklist_ID" => $task_checklist_row->checklist_ID,
                        "task_ID" => $new_task_id,
                        "isCompleted" => 2
                    ];
                }
                $this->duplicate_mult_data($dup_task_checklist, 'tbl_processST_checklistStatus');
            }
            $duplication['max_num'] = $this->fetch_allsubtask_count2($new_task_id);
            $duplication['new_task_id'] = strval($new_task_id);
            $duplication['process_id'] = $task->process_ID;
            $duplication['task_title'] = $task->taskTitle;
            $subtask = $this->get_subtask_qry($task_id); // qry subtask
            $subtask_arr = [];
            if (count($subtask) > 0) {
                foreach ($subtask as $subtask_index => $subtask_row) {
                    $subtask_arr[$subtask_index] = $subtask_row->subTask_ID;
                }
                $subtask_comp = $this->get_all_subtask_comp_qry(implode(',', $subtask_arr));
                foreach ($subtask as $subtask_index => $subtask_row) {
                    $dup_sub_comp = [];
                    $dupSubTask['task_ID'] = $new_task_id;
                    $dupSubTask['component_ID'] = $subtask_row->component_ID;
                    $dupSubTask['complabel'] = $subtask_row->complabel;
                    $dupSubTask['sublabel'] = $subtask_row->sublabel;
                    $dupSubTask['required'] = $subtask_row->required;
                    $dupSubTask['placeholder'] = $subtask_row->placeholder;
                    $dupSubTask['text_alignment'] = $subtask_row->text_alignment;
                    $dupSubTask['text_size'] = $subtask_row->text_size;
                    $dupSubTask['min'] = $subtask_row->min;
                    $dupSubTask['max'] = $subtask_row->max;
                    $dupSubTask['sequence'] = $subtask_row->sequence;
                    $dupSubTask['isRemoved'] = $subtask_row->isRemoved;
                    $new_sub_task_id = $this->duplicate_data($dupSubTask, 'tbl_processST_subtask'); // duplicate subtask
                    $sub_task_id = $subtask_row->subTask_ID;
                    // var_dump($sub_task_id);
                    if (count($subtask_comp) > 0) {
                        $spec_subtask_comp = array_filter(
                            $subtask_comp,
                            function ($e) use ($sub_task_id) {
                                return  $e->subTask_ID ==  $sub_task_id;
                            }
                        );
                        $new_sub_comp_count = 0;
                        foreach ($spec_subtask_comp as $spec_subtask_comp_row) {
                            $dup_sub_comp[$new_sub_comp_count] = [
                                'subTask_ID' => $new_sub_task_id,
                                'compcontent' => $spec_subtask_comp_row->compcontent,
                                'sequence' => $spec_subtask_comp_row->sequence,
                                'correctAnswer' => $spec_subtask_comp_row->correctAnswer,
                            ];
                            $new_sub_comp_count++;
                        }
                        $this->duplicate_mult_data($dup_sub_comp, 'tbl_processST_component_subtask'); // duplicate subtask components
                    }
                }
            }
        }
        echo json_encode($duplication);
        // insert subtask
        // insert subtask component
        // var_dump($subtask_arr);
        // var_dump($subtask);
    }

    protected function get_former_task($process_id, $sequence_num)
    {
        $fields = "task_ID, process_ID, taskTitle, sequence, status_ID, createdBy, updatedBy";
        $where = "process_ID = $process_id AND sequence = $sequence_num";
        $table = "tbl_processST_task";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function reorder_task_sequence()
    {
        $task_id = $this->input->post('taskId');
        $order_to = $this->input->post('orderTo');

        $task_details = $this->get_task_qry($task_id);
        $former_task_order = $this->get_former_task($task_details->process_ID, $order_to);// get the details of the task to be replaced
        //move task to the new order
        $task_order[0] = [
            'task_ID' => $task_id,
            'sequence' => $order_to
        ];
        //the task to be replaced will be swapped to the orig sequence of the "moving to" task
        $task_order[1] = [
            'task_ID' => $former_task_order->task_ID,
            'sequence' => $task_details->sequence
        ];
        $task_update['update'] = $this->general_model->batch_update($task_order, 'task_ID', 'tbl_processST_task');
        echo json_encode($task_update);
    }

    protected function check_coaching_log_acces($checklist_id, $subordinate_uid, $sup_uid)
    {
        $fields = "*";
        $where = "assignmentRule_ID = 9 AND type='checklist' AND folProCheTask_ID = $checklist_id AND assignedTo = $subordinate_uid AND assignedBy = $sup_uid";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_processST_assignment');
    }

    protected function check_coaching_log_assignment($checklist_id, $sup_uid)
    {
        $fields = "*";
        $where = "assignmentRule_ID = 9 AND type='checklist' AND folProCheTask_ID = $checklist_id AND assignedBy = $sup_uid";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_processST_assignment');
    }

    public function user_infos()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'PROCESS',
        ];
        $fields = "a.apid,fname,lname,pic,emp_id,gender,acc_name,birthday";
        $where = "a.apid=b.apid and c.acc_id=b.acc_id and month(birthday)=" . date("m") . " and day(birthday)=" . date("d") . " and b.isActive='yes'";
        $result = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_applicant a,tbl_employee b,tbl_account c');
        foreach ($result as $row) {
            $job = $this->getCurrentPosition($row->emp_id);
            $data["celebrants"][$row->emp_id] = array(
                "emp_id" => $row->emp_id,
                "apid" => $row->apid,
                "fname" => $row->fname,
                "lname" => $row->lname,
                "pic" => $row->pic,
                "gender" => $row->gender,
                "acc_name" => $row->acc_name,
                "birthday" => $row->birthday,
                "job" => $job,
            );
        }
        return $data;
    }

    public function check_if_userisadmin()
    {
        $userid = $_SESSION["uid"];
        $admin = "SELECT * FROM tbl_processST_admin_access WHERE user_ID=$userid";
        return $this->general_model->custom_query($admin);
    }


    public function check_access_privilege_answer($p_ID, $c_ID, $page)
    {
        $access = "0";
        $userid = $_SESSION["uid"];
        $a_admin =  $this->check_if_userisadmin();
        // if user ID is admin
        if ($a_admin != null) {
            $access = "1";
        } else {
            // if userid is not admin	
            // CHECK if owned
            // check if checklist owned
            $c_o = "SELECT checklist_ID FROM `tbl_processST_checklist` WHERE createdBy=$userid AND isRemoved=0 AND status_ID!=15 AND checklist_ID=$c_ID";
            $c_owned =  $this->general_model->custom_query($c_o);

            // check if process template owned
            $p_o = "SELECT process_ID FROM `tbl_processST_process` WHERE createdBy=$userid AND isRemoved=0 AND processstatus_ID!=15 AND process_ID=$p_ID";
            $p_owned =  $this->general_model->custom_query($p_o);

            $t_o = "SELECT task_ID FROM tbl_processST_task WHERE createdBy=$userid AND process_ID=$p_ID";
            $t_owned = $this->general_model->custom_query($t_o);

            if ($p_owned != null) {
                $access = "1";
            } else if ($c_owned != null) {
                $access = "1";
            } else if ($t_owned != null) {
                $access = "1";
            } else {
                if ($page == 'checklist_answerable') {
                    // condition if checklist owned				
                    // CHECK With Permission

                    // Task Permission
                    $t_p = "SELECT checkStat.checklistStatus_ID FROM tbl_processST_task task, tbl_processST_assignment ass,tbl_processST_checklistStatus checkStat WHERE ass.type='task' AND  ass.assignedTo=$userid AND checkStat.checklist_ID=$c_ID AND checkStat.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND task.task_ID=checkStat.task_ID";
                    $t_permission =  $this->general_model->custom_query($t_p);

                    // Checklist permission
                    $c_p = "SELECT checklist.checklist_ID FROM tbl_processST_checklist as checklist, tbl_processST_assignment ass WHERE ass.type='checklist' AND ass.assignedTo=$userid AND (ass.assignmentRule_ID=6 OR ass.assignmentRule_ID=5) AND ass.folProCheTask_ID=$c_ID AND ass.folProCheTask_ID=checklist.checklist_ID AND checklist.checklist_ID=$c_ID AND checklist.isRemoved=0 AND checklist.status_ID!=15";
                    $c_permission =  $this->general_model->custom_query($c_p);

                    // Process template permission
                    $p_p = "SELECT process.process_ID FROM tbl_processST_process as process, tbl_processST_assignment ass WHERE ass.type='process' AND ass.assignedTo=$userid AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) AND ass.folProCheTask_ID=$p_ID AND ass.folProCheTask_ID=process.process_ID AND process.process_ID=$p_ID AND process.isRemoved=0 AND process.processstatus_ID!=15";
                    $p_permission =  $this->general_model->custom_query($p_p);

                    //folder permission
                    $f_p = "SELECT folder.folder_ID FROM tbl_processST_process process, tbl_processST_folder folder ,tbl_processST_assignment ass WHERE ass.type='folder' AND ass.assignedTo=$userid AND ass.assignmentRule_ID=1 AND ass.folProCheTask_ID=folder.folder_ID AND process.process_ID=$p_ID AND folder.folder_ID=process.folder_ID AND folder.isRemoved=0 AND folder.status_ID!=15";
                    $f_permission =  $this->general_model->custom_query($f_p);
                    // CONDITION if checklist has permission to edit
                    if ($c_permission != null) {
                        $access = "1";
                    } else if ($p_permission != null) {
                        //CONDITION if process temp has permission to edit
                        $access = "1";
                    } else if ($f_permission != null) {
                        $access = "1";
                    } else if ($t_permission != null) {
                        $access = "1";
                    } else {
                        $access = "0";
                    }
                } else if ($page == 'checklist_update') {
                    $t_p = "SELECT checkStat.checklistStatus_ID FROM tbl_processST_task task, tbl_processST_assignment ass,tbl_processST_checklistStatus checkStat WHERE ass.assignmentRule_ID=6 and ass.type='task' AND  ass.assignedTo=$userid AND checkStat.checklist_ID=$c_ID AND checkStat.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND task.task_ID=checkStat.task_ID";
                    $t_permission =  $this->general_model->custom_query($t_p);

                    $p_p = "SELECT process.process_ID FROM tbl_processST_process as process, tbl_processST_assignment ass WHERE ass.type='process' AND ass.assignedTo=$userid AND ass.assignmentRule_ID=1 AND ass.folProCheTask_ID=$p_ID AND ass.folProCheTask_ID=process.process_ID AND process.process_ID=$p_ID AND process.isRemoved=0 AND process.processstatus_ID!=15";
                    $p_permission =  $this->general_model->custom_query($p_p);

                    $c_p = "SELECT checklist.checklist_ID FROM tbl_processST_checklist as checklist, tbl_processST_assignment ass WHERE ass.type='checklist' AND ass.assignedTo=$userid AND ass.assignmentRule_ID=6 AND ass.folProCheTask_ID=$c_ID AND ass.folProCheTask_ID=checklist.checklist_ID AND checklist.checklist_ID=$c_ID AND checklist.isRemoved=0 AND checklist.status_ID!=15";
                    $c_permission =  $this->general_model->custom_query($c_p);

                    if ($t_permission != null) {
                        $access = "1";
                    } else if ($c_permission != null) {
                        //CONDITION if process temp has permission to edit
                        $access = "1";
                    } else if ($p_permission != null) {
                        $access = "1";
                    } else {
                        $access = "0";
                    }
                }
            }
            // ==
        }
        if ($access == "1") {
            return true;
        } else {
            $this->error_403();
            return false;
        }
    }

    public function checklist_answerable($process_ID, $checklist_ID)
    {
        $data = $this->user_infos();
        $userid = $this->session->userdata('uid');

        if ($this->check_access_privilege_answer($process_ID, $checklist_ID, 'checklist_answerable')) {
            $data['user_info'] = $this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname", "user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid", "tbl_applicant applicant, tbl_employee employee, tbl_user user");
            $data['processID'] = $process_ID;
            $data['checklist'] = $this->general_model->fetch_specific_val("*", "checklist_ID=$checklist_ID", "tbl_processST_checklist", "checklistTitle ASC");
            $data['task'] = $this->general_model->fetch_specific_vals("task.*,checkStatus.*", "task.task_ID=checkStatus.task_ID AND checkStatus.checklist_ID=$checklist_ID", "tbl_processST_task task, tbl_processST_checklistStatus checkStatus");
            // $this->process_model->get_answerabletask($checklist_ID);
            $data['processinfo'] = $this->general_model->fetch_specific_val("*", "process_ID=$process_ID", "tbl_processST_process", "processTitle ASC");
            // $this->process_model->get_singleprocessinfo($process_ID);
            $data['user'] = $this->session->userdata('uid');
            $this->load_template_view('templates/process/checklist_answerable', $data);
        }
    }

    // public function coaching_log_access(){
    //     $data = [
    //         'uri_segment' => $this->uri->segment_array(),
    //         'title' => 'Coaching Log',
    //     ];
    //     $this->load_template_view('templates/process/coaching_log', $data);
    // }

    // public function update_coaching_log($process_id, $checklist_id){
    //     $this->checklist_answerable($process_id, $checklist_id);
    // }

    private function set_checklist_to_pending($checklist_id)
    {
        $data['status_ID'] = 2;
        $where = "checklist_ID = $checklist_id";
        $this->general_model->update_vals($data, $where, 'tbl_processST_checklist');
    }

    private function qry_checklist_status($checklist_id){
        $fields = "taskStat.checklistStatus_ID, taskStat.checklist_ID, taskStat.task_ID, taskStat.taskdueDate, taskStat.isCompleted, taskStat.task_status, taskStat.isRemoved, tasks.sequence";
        $where = "tasks.task_ID = taskStat.task_ID AND taskStat.checklist_ID = $checklist_id";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_processST_checklistStatus taskStat, tbl_processST_task tasks', 'tasks.sequence ASC');
    }

    private function set_checklist_task_to_pending($checklist_id){
        $checklist_status = $this->qry_checklist_status($checklist_id);
        $data['isCompleted'] = 2;
        $where = "checklistStatus_ID = $checklist_status->checklistStatus_ID";
        return $this->general_model->update_vals($data, $where, 'tbl_processST_checklistStatus');
    }

    public function update_coaching_log()
    {
        $checklist_id = $this->input->post('checklistId');
        $data['update_stat'] = $this->set_checklist_to_pending($checklist_id);
        $data['update_task_stat'] = $this->set_checklist_task_to_pending($checklist_id);
        echo json_encode($data);
    }

    public function coaching_log($checklist_id)
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Coaching Log',
        ];
        $session_uid = $this->session->userdata('uid');
        $checklist = $this->get_checklist_details($checklist_id);
        if (count($checklist) > 0) {
            if ($checklist->createdBy == $session_uid) {
                if ($checklist->status_ID == 2) {
                    $this->checklist_answerable($checklist->process_ID, $checklist_id);
                } else {
                    // $this->coaching_log_access();
                    $this->load_template_view('templates/process/coaching_log', $data);
                }
            } else {
                $subordinate_access = $this->check_coaching_log_acces($checklist_id,  $session_uid, $checklist->createdBy);
                if (count($subordinate_access) > 0) {
                    // $this->coaching_log_access();
                    $this->load_template_view('templates/process/coaching_log', $data);
                } else {
                    $this->error_403();
                }
            }
        } else {
            $this->error_404();
        }
    }


    private function set_deadline_general($deadline)
    {
        return $this->general_model->batch_insert($deadline, 'tbl_deadline');
    }

    private function set_deadline_coaching_log($deadline)
    {
        return $this->general_model->insert_vals($deadline, "tbl_deadline");
    }
    private function log_confirmation($confirm)
    {
        return $this->general_model->insert_vals($confirm, "tbl_processST_checklist_confirmation");
    }

    public function confirm_coaching_log()
    {
        $dateTime = $this->get_current_date_time();
        $log_confirm['confirmed'] = 0;
        $checklist = $this->get_checklist_details($this->input->post('checklistId'));
        $emp_details = $this->get_emp_details_via_user_id($this->session->userdata('uid'));
        if (count($checklist) > 0) {
            if ($checklist->status_ID == 3) {
                $confirm['confirmedBy'] = $this->session->userdata('uid');
                $confirm['checklist_ID'] = $this->input->post('checklistId');
                $confirm['dateConfirmed'] = $dateTime['dateTime'];
                $confirm['status_ID'] = 26;
                $log_confirm['confirmed'] = $this->log_confirmation($confirm);

                $notif_mssg = ucwords($emp_details->fname . ' ' . $emp_details->lname) . " already confirmed your coaching log details. <br><small><b>CL-ID</b>:" . $this->input->post('checklistId') . "</b></small>";
                $link = "discipline_mic/coaching_log/" . $this->input->post('checklistId');
                $log_confirm['notif_id'] = [$this->simple_system_notification($notif_mssg, $link, $checklist->createdBy, 'general')];
                $log_confirm['confirmed'] = 1;
            } else {
                $log_confirm['confirmed'] = 2;
            }
        }
        echo json_encode($log_confirm);
    }

    private function qry_confirmation_details($checklist_id, $confirmed_by)
    {
        $fields = "confirmation_ID, checklist_ID, status_ID, confirmedBy, dateConfirmed";
        $where = "checklist_ID = $checklist_id AND confirmedBy = $confirmed_by";
        $table = "tbl_processST_checklist_confirmation";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    public function coaching_log_notif_and_deadline_old($uids_arr)
    {
        $confirmation_deadline = [];
        $dateTime = $this->get_current_date_time();
        $deadline_date = Date("Y-m-d", strtotime($dateTime['date'] . ' 2 days'));
        $deadline_date_time = $deadline_date . " 23:59:59";
        $follow = $deadline_date . " 00:00:00";
        // var_dump($uids_arr);
        // echo count($uids_arr);
        for ($loop = 0; $loop < count($uids_arr); $loop++) {
            $pro_noun = "";
            $checklist = $this->get_checklist_details($uids_arr[$loop]['checklist_ID']);
            $emp_details = $this->get_emp_details_via_user_id($checklist->createdBy);
            // var_dump($emp_details->gender);
            // var_dump($checklist);
            if (trim($emp_details->gender) === "Male") {
                $pro_noun = "his";
            } else if (trim($emp_details->gender) === "Female") {
                $pro_noun = "her";
            }

            $notif_mssg = "needs your confirmation on " . $pro_noun . " coaching logs. <small><b>CL-ID</b>:" . $uids_arr[$loop]['checklist_ID'] . "</b></small>";
            $link = "discipline_mic/coaching_log/" . $uids_arr[$loop]['checklist_ID'];
            // var_dump($notif_mssg);
            $data['notif_id'] = $this->simple_notification($notif_mssg, $link, $uids_arr[$loop]['uid'], $checklist->createdBy);
            $confirmation_deadline[$loop] = [
                'module_ID' => 5,
                'userId' => $uids_arr[$loop]['uid'],
                'request_ID' => $checklist->checklist_ID,
                'requestor' => $checklist->createdBy,
                'approvalLevel' => 1,
                'deadline' => $deadline_date_time,
                'status_ID' => 2,
                'follow' => $follow,
            ];
        }
        $this->set_deadline_general($confirmation_deadline);
    }
    public function coaching_log_notif_and_deadline($checklist_id)
    {
        $dateTime = $this->get_current_date_time();
        $deadline_date = Date("Y-m-d", strtotime($dateTime['date'] . ' 2 days'));
        $deadline_date_time = $deadline_date . " 23:59:59";
        $follow = $deadline_date . " 00:00:00";
        $pro_noun = "";
        $checklist = $this->get_checklist_details($checklist_id);
        $emp_details = $this->get_emp_details_via_user_id($checklist->createdBy);
        $assignment = $this->check_coaching_log_assignment($checklist_id, $checklist->createdBy);
        if (trim($emp_details->gender) === "Male") {
            $pro_noun = "his";
        } else if (trim($emp_details->gender) === "Female") {
            $pro_noun = "her";
        }
        $notif_mssg = "needs your confirmation on " . $pro_noun . " coaching logs. <br><small><b>CL-ID</b>:" . $checklist_id . "</b></small>";
        $link = "discipline_mic/coaching_log/" . $checklist_id;
        $data['notif_id'] = $this->simple_notification($notif_mssg, $link, $assignment->assignedTo, $checklist->createdBy);

        $confirmation_deadline['module_ID'] = 5;
        $confirmation_deadline['userId'] = $assignment->assignedTo;
        $confirmation_deadline['request_ID'] = $checklist->checklist_ID;
        $confirmation_deadline['requestor'] = $checklist->createdBy;
        $confirmation_deadline['approvalLevel'] = 1;
        $confirmation_deadline['deadline'] = $deadline_date_time;
        $confirmation_deadline['status_ID'] = 2;
        $confirmation_deadline['follow'] = $follow;
        $this->set_deadline_coaching_log($confirmation_deadline);
    }

    public function get_coaching_log_details()
    {
        $data['content'] = "";
        $data['checklist_details'] = "";
        $data['checklist_exist'] = 0;
        $data['dates'] = "";
        // $checklist_id = 38;
        $checklist_id = $this->input->post('checklistId');
        $session_uid = $this->session->userdata('uid');
        $checklist = $this->get_checklist_details($checklist_id);
        $admin_access = $this->check_if_userisadmin();
        if (count($checklist) > 0) {
            $data['checklist_exist'] = 1;
            $data['checklist_details'] = $checklist;
            $assignment = $this->check_coaching_log_assignment($checklist_id, $checklist->createdBy);
            if (count($assignment) > 0) {
                $confirmation_details = $this->qry_confirmation_details($checklist_id, $assignment->assignedTo);
                $subordinate_details  = $this->get_emp_details_via_user_id($assignment->assignedTo);
                // var_dump($subordinate_details);
                $task_sub_task = [];
                $task_count = 0;
                $checklist_content = "";
                $checklist_dates = "";
                $task_title = '';
                $btn_coaching_log = '';
                $confirmation_mssg = 'Not Yet Confirmed';
                $border = '2px #673AB7';
                $background = '#673ab717';
                if(($session_uid != $checklist->createdBy) && ($admin_access != null)){
                    $btn_coaching_log =  '<div class="col-12 p-4 text-right" style="border-top: 2px dashed #9e9e9e61;"><a href="' . base_url('/discipline_mic/downloadPdf_coachingForm/' . $checklist_id) . '" target="_blank" class="btn btn-primary btn-md m-btn m-btn--icon"><span><i class="fa fa-print"></i><span>Print Log Details</span></span></a></div>';
                } else if (($session_uid == $checklist->createdBy) ||($admin_access != null)) {
                    if (count($confirmation_details) < 1) {
                        $btn_coaching_log =  '<div class="col-12 p-4 text-right" style="border-top: 2px dashed #9e9e9e61;"><button id="edit_coaching_log" class="btn btn-success btn-md m-btn m-btn--icon mr-2" data-procid="' . $checklist->process_ID . '" data-checklistid="' . $checklist->checklist_ID . '"><span><i class="fa fa-edit"></i><span>Edit Log Details</span></span></button><a href="' . base_url('/discipline_mic/downloadPdf_coachingForm/' . $checklist_id) . '" target="_blank" class="btn btn-primary btn-md m-btn m-btn--icon"><span><i class="fa fa-print"></i><span>Print Log Details</span></span></a></div>';
                    } else {
                        $btn_coaching_log =  '<div class="col-12 p-4 text-right" style="border-top: 2px dashed #9e9e9e61;"><a href="' . base_url('/discipline_mic/downloadPdf_coachingForm/' . $checklist_id) . '" target="_blank" class="btn btn-primary btn-md m-btn m-btn--icon"><span><i class="fa fa-print"></i><span>Print Log Details</span></span></a></div>';
                    }
                } else {
                    $subordinate_access = $this->check_coaching_log_acces($checklist_id,  $session_uid, $checklist->createdBy);
                    if (count($subordinate_access) > 0) {
                        if ($checklist->status_ID == 2) {
                            $btn_coaching_log = '<div class="col-12 p-4 text-center" style="border-top: 2px dashed #9e9e9e61;">Your Supervisor is currently updating the Coaching Log Details. Please constantly reload the page to check if coaching log is now ready for confirmation</div>';
                        } else if ((count($confirmation_details) < 1) && ($checklist->status_ID == 3)) {
                            $btn_coaching_log = '<div class="col-12 p-4 text-right" style="border-top: 2px dashed #9e9e9e61;"><button id="confirm_coaching_log" class="btn btn-success btn-md m-btn m-btn--icon" data-procid="' . $checklist->process_ID . '" data-checklistid="' . $checklist->checklist_ID . '"><span><i class="fa fa-check"></i><span>Confirm Details</span></span></button></div>';
                        }
                    }
                }
                // var_dump($checklist);
                // $checklist_details = [];
                if ($checklist !== NULL) {
                    // $task = $this->qry_process_task($checklist->process_ID);
                    $checklist_task = $this->qry_spec_checklist_task($checklist->process_ID, $checklist->checklist_ID);
                    $answers = $this->qry_spec_checklist_answers($checklist->checklist_ID);
                    // CHECKLIST DATES
                    $checklist_dates .=  '<span>Run last ' . date_format(date_create($checklist->dateTimeCreated), "M j, Y g:i:s A") . '</span><br>';
                    if (count($confirmation_details) > 0) {
                        $confirmation = "Confirmed last";
                        $border = '2px #4CAF50';
                        $background = '#4caf5017';
                        $confirmation_mssg = "Successfully Confirmed";
                        if ($confirmation_details->status_ID == 12) {
                            $border = '2px #ff9393';
                            $background = '#ffdada';
                            $confirmation = "Missed to confirm before";
                            $confirmation_mssg = "Missed to Confirm";
                        }
                        $checklist_dates .= '<span class="">' . $confirmation . ' ' . date_format(date_create($confirmation_details->dateConfirmed), "M j, Y g:i:s A") . '</span>';
                    }
                    $data['dates'] = $checklist_dates;
                    // EMP details
                    if (($session_uid == $checklist->createdBy) || ($admin_access != null)) {
                        $checklist_content .= '<div class="col-12 p-3" style="background: #f1f1f1;border-bottom: 1px solid #bbbbbb;">';
                        $checklist_content .= '<div class="row">';
                        // $checklist_content .= '<div class="col-12 col-sm-3 col-md-2 col-lg-2">';
                        //     $checklist_content .= '<img id="eroPic" src="'.base_url($subordinate_details->pic). '" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block" style="border: 1px solid #a5a2a2;">';
                        // $checklist_content .= '</div>';
                        // $checklist_content .= '<div class="col-12 col-sm-9 col-md-10 col-lg-10">';
                        //     $checklist_content .= '<div class="row">';
                        //         $checklist_content .= '<div class="col-12 col-md-12 mb-1 text-center text-sm-left text-capitalize" style="font-size: 19px;color: #5d5d5d;">' . ucwords($subordinate_details->fname . ' ' . $subordinate_details->lname) . '</div>';
                        //         $checklist_content .= '<div class="col-md-12 text-center text-sm-left text-capitalize" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: #07bdbd;">'. $subordinate_details->accountDescription.'</div>';
                        //         $checklist_content .= '<div class="col-md-12 mb-1 text-center text-sm-left text-capitalize" style="font-size: 12px;color: #191919;" id="eroAccountInfo">'. $subordinate_details->positionDescription.'</div>';
                        //     $checklist_content .= '</div>';
                        // $checklist_content .= '</div>';
                        $checklist_content .= '<div class="col-12 col-sm-12 col-md-7 col-lg-8">';
                        $checklist_content .= '<div class="row">';
                        $checklist_content .= '<div class="col-sm-12 col-md-4 col-lg-3"><img src="' . base_url($subordinate_details->pic) . '" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block"></div>';
                        $checklist_content .= '<div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">';
                        $checklist_content .= '<div class="row">';
                        $checklist_content .= '<div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;">' . ucwords($subordinate_details->fname . ' ' . $subordinate_details->lname) . '</div>';
                        $checklist_content .= '<div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;">' . $subordinate_details->positionDescription . '</div>';
                        $checklist_content .= '<div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px">' . $subordinate_details->accountDescription . '</div>';
                        $checklist_content .= '</div>';
                        $checklist_content .= '</div>';
                        $checklist_content .= '</div>';
                        $checklist_content .= '</div>';
                        $checklist_content .= '<div class="col-12 col-sm-12 col-md-5 col-lg-4">';
                        $checklist_content .= '<div class="row mx-2 mt-md-0 mt-2" style="border-style: dashed !important;border: ' . $border . ';background: ' . $background . ';height: 100%;align-items: center;word-break: break-word;">';
                        $checklist_content .= '<div class="col-12 text-center" style="font-size: 1.5rem;">';
                        $checklist_content .= $confirmation_mssg;
                        $checklist_content .= '</div>';
                        $checklist_content .= '</div>';
                        $checklist_content .= '</div>';
                        $checklist_content .= '</div>';
                        $checklist_content .= '</div>';
                    } else {
                        if (count($subordinate_access) > 0) {
                            if ((count($confirmation_details) < 1) && ($checklist->status_ID == 3)) {
                                $checklist_content .= '<div class="col-12 p-2 text-center text-primary" style="background: #673ab726;border-bottom: 1px solid #673ab738;font-weight: 500;">';
                                $checklist_content .= 'Waiting for your Confirmation...';
                                // $checklist_content .= '<span class="m-portlet__head-icon p-0 mr-2 fa-spin"><i class="fa fa-spinner coloricons" id=""></i></span>';
                            } else if ((count($confirmation_details) > 0) && ($confirmation_details->status_ID == 26)) {
                                $checklist_content .= '<div class="col-12 p-2 text-center" style="background: #8bc34a6b;border-bottom: 1px solid #4caf5063;color: green;font-weight: 500;">';
                                $checklist_content .= '<i class="fa fa-check mr-2"></i>Successfully Confirmed';
                            } else if ((count($confirmation_details) > 0) && ($confirmation_details->status_ID == 12)) {
                                $checklist_content .= '<div class="col-12 p-2 text-center" style="background: #ff980040;border-bottom: 1px solid #ffc10773;color: #a26100;font-weight: 500;">';
                                $checklist_content .= '<i class="fa fa-exclamation mr-2"></i>You have Missed to Confirm';
                            }
                            $checklist_content .= '</div>';
                        }

                        // <div ><i class="fa fa-check mr-2"></i>Successfully Confirmed</div>

                        // $checklist_content .= '<div class="row">';
                        // $checklist_content .= '<div class="col-12 col-sm-3 col-md-2 col-lg-2">';
                        //     $checklist_content .= '<img id="eroPic" src="'.base_url($subordinate_details->pic). '" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block" style="border: 1px solid #a5a2a2;">';
                        // $checklist_content .= '</div>';
                        // $checklist_content .= '<div class="col-12 col-sm-9 col-md-10 col-lg-10">';
                        //     $checklist_content .= '<div class="row">';
                        //         $checklist_content .= '<div class="col-12 col-md-12 mb-1 text-center text-sm-left text-capitalize" style="font-size: 19px;color: #5d5d5d;">' . ucwords($subordinate_details->fname . ' ' . $subordinate_details->lname) . '</div>';
                        //         $checklist_content .= '<div class="col-md-12 text-center text-sm-left text-capitalize" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: #07bdbd;">'. $subordinate_details->accountDescription.'</div>';
                        //         $checklist_content .= '<div class="col-md-12 mb-1 text-center text-sm-left text-capitalize" style="font-size: 12px;color: #191919;" id="eroAccountInfo">'. $subordinate_details->positionDescription.'</div>';
                        //     $checklist_content .= '</div>';
                        // $checklist_content .= '</div>';
                        //     $checklist_content .= '<div class="col-12">';
                        //         $checklist_content .= '<div class="row">';
                        //             $checklist_content .= '<div class="col-sm-12 col-md-4 col-lg-3"><img src="' . base_url($subordinate_details->pic) . '" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block"></div>';
                        //             $checklist_content .= '<div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">';
                        //                 $checklist_content .= '<div class="row">';
                        //                     $checklist_content .= '<div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;" id="annexSubName">' . ucwords($subordinate_details->fname . ' ' . $subordinate_details->lname) . '</div>';
                        //                     $checklist_content .= '<div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="annexSubPosition">' . $subordinate_details->positionDescription . '</div>';
                        //                     $checklist_content .= '<div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px" id="annexSubAccount">' . $subordinate_details->accountDescription . '</div>';
                        //                 $checklist_content .= '</div>';
                        //             $checklist_content .= '</div>';
                        //         $checklist_content .= '</div>';
                        //     $checklist_content .= '</div>';
                        //     $checklist_content .= '<div class="col-12 col-md-3">';
                        //         $checklist_content .= '<div class="row mx-2 mt-md-0 mt-2" style="border-style: dashed !important;border: '. $border. ';background: '.$background.';height: 100%;align-items: center;">';
                        //             $checklist_content .= '<div class="col-12 text-center" style="font-size: 1.5rem;">';
                        //                 $checklist_content .= $confirmation_mssg;
                        //             $checklist_content .= '</div>';
                        //         $checklist_content .= '</div>';
                        //     $checklist_content .= '</div>';
                        // $checklist_content .= '</div>';
                    }
                    if (count($checklist_task) > 0) {
                        foreach ($checklist_task as $task_row) {
                            $task_arr[$task_count] = $task_row->task_ID;
                            $task_count++;
                        }
                        $task_ids = implode(",", $task_arr);
                        $sub_task = $this->qry_spec_process_subtask($task_ids);
                        foreach ($checklist_task as $checklist_task_row_index => $checklist_task_row) {
                            // TASK HERE
                            $task_title = $checklist_task_row->taskTitle;
                            if (trim($checklist_task_row->taskTitle) == '') {
                                $task_title = 'Untitled Task';
                            }
                            $checklist_content .= '<div class="col-12 p-4">';
                            $checklist_content .= '<h3 style="color: #183442;"><i class="fa fa-tag mr-2" style="font-size: 1.7rem;"></i>' . $task_title . '</h3>';
                            $checklist_content .= '<div class="col-12 py-3">';
                            $searchedTaskValue = $checklist_task_row->task_ID;
                            $task_sub_task = array_filter(
                                $sub_task,
                                function ($e) use ($searchedTaskValue) {
                                    return $e->task_ID == $searchedTaskValue;
                                }
                            );
                            if (count($task_sub_task) > 0) {
                                foreach ($task_sub_task as $task_sub_task_row) {
                                    $subtask_answer_val = "No Answer";
                                    // var_dump($task_sub_task_row);
                                    // $checklist_content .= '<h5>'.$task_sub_task_row.'</h5>';
                                    if ((int) $task_sub_task_row->component_ID == 16) {
                                        $checklist_content .= '<div class="col-12 mb-4 pl-5 pr-5">';
                                        $checklist_content .= '<figure style="margin:0 !important;text-align:center"><img src="' . base_url($task_sub_task_row->complabel) . '" class="gambar img-responsiv" style="background:white;width:50%!important;object-fit:cover;"></figure>';
                                        $checklist_content .= '</div>';
                                    } else if ((int) $task_sub_task_row->component_ID == 17) {
                                        $checklist_content .= '<div class="col-12" style="padding: 1rem 12rem 1rem 12rem;">';
                                        $checklist_content .= '<div class="row iframepadding">';
                                        $checklist_content .= '<div class="embed-responsive embed-responsive-16by9">';
                                        $checklist_content .= '<video width="200" height="240" controls="" __idm_id__="379351041">';
                                        $checklist_content .= '<source src="' . base_url($task_sub_task_row->complabel) . '" type="video/mp4">';
                                        $checklist_content .= '<source src="' . base_url($task_sub_task_row->complabel) . '" type="video/m4v">';
                                        $checklist_content .= '<source src="' . base_url($task_sub_task_row->complabel) . '" type="video/avi">';
                                        $checklist_content .= '<source src="' . base_url($task_sub_task_row->complabel) . '" type="video/mpg">';
                                        $checklist_content .= '<source src="' . base_url($task_sub_task_row->complabel) . '" type="video/mov">';
                                        $checklist_content .= '<source src="' . base_url($task_sub_task_row->complabel) . '" type="video/mpg">';
                                        $checklist_content .= '<source src="' . base_url($task_sub_task_row->complabel) . '" type="video/mpeg">';
                                        $checklist_content .= '</video>';
                                        $checklist_content .= '</div>';
                                        $checklist_content .= '</div>';
                                        $checklist_content .= '</div>';
                                    } else if ((int) $task_sub_task_row->component_ID == 18) {
                                        // $file_type = $task_sub_task_row->complabel;
                                        $def_answer_file_name = explode("/", $task_sub_task_row->complabel);
                                        $format_start = strpos($task_sub_task_row->complabel, '.');
                                        $file_format = substr($task_sub_task_row->complabel, $format_start + 1);
                                        $file_format = $this->file_format_layout($file_format);
                                        $checklist_content .= '<div class="row px-3 pt-2 pb-4">';
                                        $checklist_content .= '<div class="col-md-3"></div>';
                                        $checklist_content .= '<a href="' . base_url('/' . $task_sub_task_row->complabel) . '" class="col-12 col-md-6 text-center p-2 uploadedFiles" style="border: 2px dashed ' . $file_format['border'] . ';background: ' . $file_format['background'] . ';" download>';
                                        $checklist_content .= '<div>';
                                        $checklist_content .= '<span><i class="fa fa-file-text-o mr-2"></i><span>' . $file_format['file_type'] . '</span></span>';
                                        $checklist_content .= '</div>';
                                        $checklist_content .= '<div style="font-size:1.1em;color:#7e61b1;word-break: break-word;">' . $def_answer_file_name[3] . '</div>';
                                        $checklist_content .= '</a>';
                                        $checklist_content .= '<div class="col-md-3"></div>';
                                        $checklist_content .= '</div>';
                                    } else if ((int) $task_sub_task_row->component_ID == 3) {
                                        $checklist_content .= '<div class="col-12 text-center" style="font-size: 2rem;font-weight: 500;">' . $task_sub_task_row->complabel . '</div>';
                                    } else if ((int) $task_sub_task_row->component_ID == 4) {
                                        $checklist_content .= '<p>' . $task_sub_task_row->complabel . '</p>';
                                    } else if ((int) $task_sub_task_row->component_ID == 9) {
                                        $checklist_content .= '<div class="col-12 px-0 py-0"><hr style="margin-top: 0.8rem !important;"></div>';
                                    } else if ((int) $task_sub_task_row->component_ID == 15) {
                                        // NO TITLE
                                    } else if (($task_sub_task_row->complabel == null)) {
                                        // $col_label = $task_sub_task_row->fieldType;
                                        $checklist_content .= '<h5>' . $task_sub_task_row->fieldType . '</h5>';
                                    } else {
                                        $checklist_content .= '<h5>' . $task_sub_task_row->complabel . '</h5>';
                                    }
                                    $searchedTaskValue = $task_sub_task_row->subTask_ID;
                                    $sub_task_answer = array_filter(
                                        $answers,
                                        function ($e) use ($searchedTaskValue) {
                                            return $e->subtask_ID == $searchedTaskValue;
                                        }
                                    );
                                    $mult_answer = [];
                                    $mult_answer_count = 0;
                                    if (count($sub_task_answer) > 0) {
                                        if (count($sub_task_answer) > 1) {
                                            foreach ($sub_task_answer as $sub_task_answer_row) {
                                                $mult_answer[$mult_answer_count] = $sub_task_answer_row->answer;
                                                $mult_answer_count++;
                                            }
                                            $subtask_answer_val = implode(", ", $mult_answer);
                                        } else {
                                            foreach ($sub_task_answer as $sub_task_answer_row) {
                                                if ($sub_task_answer_row->answer != null) {
                                                    $subtask_answer_val = $sub_task_answer_row->answer;
                                                }
                                            }
                                        }
                                        if ((int) $task_sub_task_row->component_ID == 15) {
                                            $answer_file_name = explode("/", $subtask_answer_val);
                                            $format_start = strpos($subtask_answer_val, '.');
                                            $file_format = substr($subtask_answer_val, $format_start + 1);
                                            $file_format = $this->file_format_layout($file_format);
                                            $checklist_content .= '<div class="row px-3 pt-2 pb-4">';
                                            $checklist_content .= '<div class="col-md-3"></div>';
                                            $checklist_content .= '<a href="' . base_url('/' . $subtask_answer_val) . '" class="col-12 col-md-6 text-center p-2 uploadedFiles" style="border: 2px dashed ' . $file_format['border'] . ';background: ' . $file_format['background'] . ';">';
                                            $checklist_content .= '<div>';
                                            $checklist_content .= '<span><i class="fa fa-file-text-o mr-2"></i><span>' . $file_format['file_type'] . '</span></span>';
                                            $checklist_content .= '</div>';
                                            $checklist_content .= '<div style="font-size:1.1em;color:#7e61b1;word-break: break-word;">' . $answer_file_name[3] . '</div>';
                                            $checklist_content .= '</a>';
                                            $checklist_content .= '<div class="col-md-3"></div>';
                                            $checklist_content .= '</div>';
                                        }
                                    }
                                    if (((int) $task_sub_task_row->component_ID == 15) || ((int) $task_sub_task_row->component_ID == 16) || ((int) $task_sub_task_row->component_ID == 17) || ((int) $task_sub_task_row->component_ID == 18) || ((int) $task_sub_task_row->component_ID == 3) || ((int) $task_sub_task_row->component_ID == 4)) {
                                        if (trim($task_sub_task_row->sublabel) !== '') {
                                            $checklist_content .= '<p>' . $task_sub_task_row->sublabel . '</p>';
                                        }
                                    } else if ((int) $task_sub_task_row->component_ID == 9) {
                                        // no answer
                                    } else {
                                        $checklist_content .= '<p>' . $subtask_answer_val . '</p>';
                                    }
                                }
                            } else {
                                $checklist_content .= '<h5>Empty Task</h5>';
                            }
                            // var_dump($subtask_answer_val);
                            $checklist_content .= '</div>';
                            $checklist_content .= '</div>';
                            if ($checklist_task_row_index < count($checklist_task) - 1) {
                                $checklist_content .= '<div class="col-12 px-0 py-0">';
                                $checklist_content .= '<hr style="margin-top: 0.8rem !important;border-top: 2px dashed rgba(0,0,0,.1);">';
                                $checklist_content .= '</div>';
                            }
                            // var_dump($task_sub_task);
                        }
                        $checklist_content .= $btn_coaching_log;
                        // var_dump($checklist_content);
                        $data['content'] =  $checklist_content;
                    }
                }
            }
        }

        echo json_encode($data);
    }

    //
    // UPDATE TASK SEQUENCE
    public function qry_all_process()
    {
        $fields = "process_ID, processTitle";
        $where = "process_ID IS NOT NULL";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_processST_process');
    }

    public function qry_all_task()
    {
        $fields = "task_ID, taskTitle, process_ID";
        $where = "task_ID IS NOT NULL AND isRemoved != 1";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_processST_task', 'sequence ASC');
    }

    //UNCOMMENT AND RUN ONCE TO CORRECT TASK SEQUENCE 
    //  public function update_tasks_sequence(){
    //     $proc_all = $this->qry_all_process();
    //     $task_all = $this->qry_all_task();
    //     $update_task = [];
    //     $update_task_count = 0;
    //     foreach($proc_all as $proc_all_row){
    //         $proc_id = $proc_all_row->process_ID;
    //         $proc_task = array_filter(
    //             $task_all,
    //             function ($e) use ($proc_id) {
    //                 return  $e->process_ID == $proc_id;
    //             }
    //         );
    //         // var_dump($proc_task);
    //         $task_count = 1;
    //         foreach($proc_task as $proc_task_row){
    //             // var_dump($task_count);
    //             // var_dump($proc_task_row);
    //             $update_task[$update_task_count] = [
    //                 'task_ID' => $proc_task_row->task_ID,
    //                 'sequence' => $task_count
    //             ];
    //             $update_task_count++;
    //             $task_count++;
    //         }
    //     }
    //     // var_dump($update_task);
    //     $this->general_model->batch_update($update_task, 'task_ID', 'tbl_processST_task');
    // }

    private function qry_no_limit_checklist_details($process_id, $checkListName, $status_id, $assignee)
    {
        // echo $checkListName;
        $where_name = "";
        $where_status = "";
        $where_assignee = "";
        if ($checkListName != "") {
            $where_name = " AND (chkList.checklistTitle LIKE \"%$checkListName%\" OR chkList.checklistTitle LIKE \"% $checkListName%\") ";
        }
        if ($status_id != "") {
            $where_status = " AND chkList.status_ID IN (" . $status_id . ")";
        }
        if ($assignee != "") {
            $where_assignee = " AND chkList.checklist_ID IN (SELECT DISTINCT(chkList.checklist_ID) as checklist_ID FROM tbl_processST_assignment ass, tbl_processST_checklist chkList WHERE chkList.checklist_ID = ass.folProCheTask_ID AND ass.type = 'checklist' AND ass.assignedTo IN ($assignee) AND chkList.process_ID  = $process_id UNION SELECT DISTINCT(chkList2.checklist_ID) as checklist_ID FROM tbl_processST_assignment ass2, tbl_processST_checklist chkList2, tbl_processST_checklistStatus chkListStat, tbl_processST_task task  WHERE task.task_ID = chkListStat.task_ID AND ass2.folProCheTask_ID = chkListStat.checklistStatus_ID AND chkListStat.checklist_ID = chkList2.checklist_ID AND ass2.assignedTo IN ($assignee) AND chkList2.process_ID  = $process_id)";
        }
        $query = "SELECT chkList.checklist_ID ,chkList.checklistTitle, chkList.status_ID , stat.description, chkList.dateTimeCreated, chkList.dueDate, chkList.dateTimeCompleted, chkList.completedBy FROM tbl_status stat, tbl_processST_checklist chkList WHERE stat.status_ID = chkList.status_ID AND chkList.isRemoved = 0 AND chkList.process_ID = $process_id" . $where_name . "" . $where_status . "" . $where_assignee;
        return $this->general_model->custom_query($query);
    }

     //UNCOMMENT AND RUN ONCE TO CORRECT SUB TASK SEQUENCE 
    public function correct_subtask_sequence()
    {
        $proc_all = $this->qry_all_process();
        $task_all = $this->qry_all_task();
        $task_arr = [];
        $task_count = 0;
        $subtask_update = [];
        $subtask_count = 0;
        foreach ($proc_all as $proc_all_row) {
            $proc_id = $proc_all_row->process_ID;
            $proc_task = array_filter(
                $task_all,
                function ($e) use ($proc_id) {
                    return  $e->process_ID == $proc_id;
                }
            );
            if (count($proc_task) > 0) {
                foreach ($proc_task as $proc_task_row) {
                    $task_arr[$task_count] = $proc_task_row->task_ID;
                    $task_count++;
                }
                $task_ids = implode(",", $task_arr);
                $sub_task = $this->qry_spec_process_subtask($task_ids);
                foreach ($proc_task as $proc_task_row) {
                    $searchedTaskValue = $proc_task_row->task_ID;
                    $task_sub_task = array_filter(
                        $sub_task,
                        function ($e) use ($searchedTaskValue) {
                            return $e->task_ID == $searchedTaskValue;
                        }
                    );

                    if (count($task_sub_task) > 0) {
                        $sub_task_count = 1;
                        foreach ($task_sub_task as $task_sub_task_count) {
                            $subtask_update[$subtask_count] = [
                                "subTask_ID" => $task_sub_task_count->subTask_ID,
                                "sequence" => $sub_task_count
                            ];
                            $subtask_count++;
                            $sub_task_count++;
                        }
                    }
                }
            }
        }
        $this->general_model->batch_update($subtask_update, 'subTask_ID', 'tbl_processST_subtask');
    }


    public function get_chklist_sequence()
    {
        $proc_all = $this->qry_all_process();
        // var_dump($proc_all);
        $all_sequence = [];
        if (count($proc_all) > 0) {
            $update_task_count = 0;
            foreach ($proc_all as $proc_all_row) {
                var_dump($proc_all_row);
                $all_checklist = $this->qry_no_limit_checklist_details($proc_all_row->process_ID, '', '', '');
                // var_dump($all_checklist);
                if (count($all_checklist) > 0) {
                    foreach ($all_checklist as $all_checklist_row) {
                        $checklist = $this->get_checklist_details($all_checklist_row->checklist_ID); // CHECKLIST
                        var_dump($checklist);
                        $task_count = 0;
                        if ($checklist !== NULL) {
                            $checklist_task = $this->qry_spec_checklist_task($checklist->process_ID, $checklist->checklist_ID);
                            var_dump($checklist_task);
                            //     $answers = $this->qry_spec_checklist_answers($checklist->checklist_ID);
                            if (count($checklist_task) > 0) {
                                foreach ($checklist_task as $task_row) {
                                    $task_arr[$task_count] = $task_row->task_ID;
                                    $task_count++;
                                }
                                $task_ids = implode(",", $task_arr);
                                $sub_task = $this->qry_spec_process_subtask($task_ids);
                                // var_dump($sub_task);
                                foreach ($checklist_task as $checklist_task_row_index => $checklist_task_row) {
                                    $sequence_count = 1;
                                    echo $checklist_task_row->taskTitle;
                                    $searchedTaskValue = $checklist_task_row->task_ID;
                                    $task_sub_task = array_filter(
                                        $sub_task,
                                        function ($e) use ($searchedTaskValue) {
                                            return $e->task_ID == $searchedTaskValue;
                                        }
                                    );
                                    // var_dump($task_sub_task);
                                    if (count($task_sub_task) > 0) {
                                        foreach ($task_sub_task as $task_sub_task_row) {
                                            echo "<br>" . $sequence_count;
                                            var_dump($task_sub_task_row);
                                            $all_sequence[$update_task_count] = [
                                                "subtask_ID" => $task_sub_task_row->subTask_ID,
                                                "sequence" => $sequence_count
                                            ];
                                            $sequence_count++;
                                            $update_task_count++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        var_dump($all_sequence);
        // $checklist_id = 7;
        // $checklist = $this->get_checklist_details($checklist_id); // CHECKLIST
        // var_dump($checklist);
        // $task_count = 0;
        // if ($checklist !== NULL) {
        //     $checklist_task = $this->qry_spec_checklist_task($checklist->process_ID, $checklist->checklist_ID);
        //     var_dump($checklist_task);
        // //     $answers = $this->qry_spec_checklist_answers($checklist->checklist_ID);
        //     if (count($checklist_task) > 0) {
        //         foreach ($checklist_task as $task_row) {
        //             $task_arr[$task_count] = $task_row->task_ID;
        //             $task_count++;
        //         }
        //         $task_ids = implode(",", $task_arr);
        //         $sub_task = $this->qry_spec_process_subtask($task_ids);
        //         // var_dump($sub_task);
        //         foreach ($checklist_task as $checklist_task_row_index => $checklist_task_row) {
        //             echo $checklist_task_row->taskTitle;
        //             $searchedTaskValue = $checklist_task_row->task_ID;
        //             $task_sub_task = array_filter(
        //                 $sub_task,
        //                 function ($e) use ($searchedTaskValue) {
        //                     return $e->task_ID == $searchedTaskValue;
        //                 }
        //             );
        //             var_dump($task_sub_task);
        //         }
        //     }
        // }
        // return $final_array;
    }

    // PROCESS PDF NICCA
    //transfer to process controller----------------------------------------
    public function downloadPdf_coachingForm($checklist_id)
    {
        $this->load->library('m_pdf');
        $data['details'] = $this->get_coaching_log_details2($checklist_id);
        // print_r($data);
        $html = $this->load->view('templates/process/pdf_coachingform', $data, true);
        $pdfFilePath = "SZ-CoachingForm.pdf";

        try {
            $pdf = $this->m_pdf->load();
            $pdf->SetHTMLHeader('<img src="assets/images/img/logo_header.png"/>');
            $pdf->SetHTMLFooter('<img src="assets/images/img/logo_footer.png"/>');
            // $pdf->debug = true;
            $pdf->SetTitle('COACHING and MENTORING FORM');
            $pdf->WriteHTML($html);
            header("Content-type:application/pdf");
            header("Content-Disposition:inline;filename='" . $pdfFilePath . "'");
            $pdf->Output($pdfFilePath, "I");
        } catch (\Mpdf\MpdfException $e) {
            echo $e->getMessage();
        }
        exit;
    }
    //remove this one-----------------------------------------------------------
    public function get_coaching_log_details2($checklist_id)
    {
        $checklist = $this->get_checklist_details($checklist_id); // CHECKLIST
        $task_arr = [];
        $task_sub_task = [];
        $task_count = 0;
        $checklist_content = "";
        $task_title = '';
        $final_array = array();
        if ($checklist !== NULL) {
            $checklist_task = $this->qry_spec_checklist_task($checklist->process_ID, $checklist->checklist_ID);
            $answers = $this->qry_spec_checklist_answers($checklist->checklist_ID);
            if (count($checklist_task) > 0) {
                foreach ($checklist_task as $task_row) {
                    $task_arr[$task_count] = $task_row->task_ID;
                    $task_count++;
                }
                $task_ids = implode(",", $task_arr);
                $sub_task = $this->qry_spec_process_subtask($task_ids);
                foreach ($checklist_task as $checklist_task_row_index => $checklist_task_row) {
                    $task_array = array();
                    // TASK HERE
                    $task_title = $checklist_task_row->taskTitle;
                    if (trim($checklist_task_row->taskTitle) == '') {
                        $task_title = '<span style="color:#666;font-style:italic">Untitled Task</span>';
                    }
                    $task_array['task_title'] = $task_title;

                    $searchedTaskValue = $checklist_task_row->task_ID;
                    $task_sub_task = array_filter(
                        $sub_task,
                        function ($e) use ($searchedTaskValue) {
                            return $e->task_ID == $searchedTaskValue;
                        }
                    );
                    $task_array['sub_tasks'] = NULL; //Empty Task
                    if (count($task_sub_task) > 0) {
                        foreach ($task_sub_task as $task_sub_task_row) {
                            $sub_task_array = array();
                            $sub_task_array['type'] = 'normal';
                            $sub_task_array['label'] = null;
                            $subtask_answer_val = '<span style="color:#666;font-weight:400;font-style:italic">No Answer</span>';
                            // $sub_task_array['test_id'] = $task_sub_task_row->component_ID;
                            $sub_task_array['sublabel'] = $task_sub_task_row->sublabel;

                            // SUB TASK HERE
                            $nolabel = '<span style="color:#666;font-weight:400;font-style:italic">' . $task_sub_task_row->fieldType . '</span>';
                            if ((int) $task_sub_task_row->component_ID == 9) {
                                $sub_task_array['type'] = 'divider';
                                $sub_task_array['label'] =  $nolabel;
                            } else if (($task_sub_task_row->complabel == null || $task_sub_task_row->complabel == '')) {
                                // $col_label = $task_sub_task_row->fieldType;
                                $sub_task_array['label'] = $nolabel;
                            } else if ((int) $task_sub_task_row->component_ID == 16) { // IMAGE
                                $sub_task_array['type'] = 'image';
                                $sub_task_array['label'] = '<img style="height:150px;" src="' . base_url($task_sub_task_row->complabel) . '">';
                                $subtask_answer_val = null;
                            } else if ((int) $task_sub_task_row->component_ID == 17) { // VIDEO
                                // $sub_task_array['label'] = null;//video
                                $sub_task_array['type'] = 'file';
                                $sub_task_array['label'] =  $nolabel;
                                $subtask_answer_val = '<span style="color:gray;font-style:italic;font-size:12px;"><b>Cannot display file</b></span>';
                            } else if ((int) $task_sub_task_row->component_ID == 18) { // FILE CONTENT
                                // $file_type = $task_sub_task_row->complabel;
                                $format_start = strpos($task_sub_task_row->complabel, '.');
                                $file_format = substr($task_sub_task_row->complabel, $format_start + 1);
                                $file_format = $this->file_format_layout($file_format);
                                // $sub_task_array['label'] = null;
                                $sub_task_array['type'] = 'file';
                                $sub_task_array['label'] =  $nolabel;
                                $subtask_answer_val = '<span style="color:gray;font-style:italic;font-size:12px;"><b>Cannot display file</b></span>';
                                // var_dump($task_sub_task_row->complabel);//might be needed
                            } else if ((int) $task_sub_task_row->component_ID == 3) { // HEADING
                                $sub_task_array['type'] = 'heading';
                                $sub_task_array['label'] = $task_sub_task_row->complabel;
                            } else if ((int) $task_sub_task_row->component_ID == 4) { // TEXT
                                $sub_task_array['label'] = $task_sub_task_row->complabel;
                            } else if ((int) $task_sub_task_row->component_ID == 15) {
                                // $sub_task_array['label'] = null;//file
                                $sub_task_array['type'] = 'file';
                                $sub_task_array['label'] =  $nolabel;
                                $subtask_answer_val = '<span style="color:gray;font-style:italic;font-size:12px;"><b>Cannot display file</b></span>';
                            } else {
                                $sub_task_array['label'] = $task_sub_task_row->complabel;
                            }
                            $searchedTaskValue = $task_sub_task_row->subTask_ID;
                            $sub_task_answer = array_filter(
                                $answers,
                                function ($e) use ($searchedTaskValue) {
                                    return $e->subtask_ID == $searchedTaskValue;
                                }
                            );
                            $mult_answer = [];
                            $mult_answer_count = 0;
                            // ANSWER HERE
                            if (count($sub_task_answer) > 0) {
                                if (count($sub_task_answer) > 1) {
                                    foreach ($sub_task_answer as $sub_task_answer_row) {
                                        $mult_answer[$mult_answer_count] = $sub_task_answer_row->answer;
                                        $mult_answer_count++;
                                    }
                                    $subtask_answer_val = implode(", ", $mult_answer);
                                } else {
                                    foreach ($sub_task_answer as $sub_task_answer_row) {
                                        if ($sub_task_answer_row->answer != null) {
                                            $subtask_answer_val = $sub_task_answer_row->answer;
                                        }
                                    }
                                }
                                if ((int) $task_sub_task_row->component_ID == 15) { // FILE
                                    $format_start = strpos($subtask_answer_val, '.');
                                    $file_format = substr($subtask_answer_val, $format_start + 1);
                                    $file_format = $this->file_format_layout($file_format);
                                    // var_dump($file_format);
                                    // var_dump($subtask_answer_val);
                                    if ($file_format['file_type'] === 'PNG' || $file_format['file_type'] === 'JPG') {
                                        $sub_task_array['type'] = 'image';
                                        $subtask_answer_val = '<img  style="height:150px;" src="' . base_url($subtask_answer_val) . '">';
                                    }
                                }
                            }

                            $sub_task_array['answer'] = $subtask_answer_val;
                            $task_array['sub_tasks'][] = $sub_task_array;
                        }
                    }
                    $final_array[] = $task_array;
                }
            }
        }
        return $final_array;
    }
}
