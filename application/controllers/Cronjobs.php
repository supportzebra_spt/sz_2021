<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cronjobs extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('general_model');
		$this->load->model('process_model');
	}
	/* public function addCron($processID,$cron_ID){
			$output = shell_exec('crontab -l');
			$rs = $this->db->query("Select * from tbl_processST_cronjobs where pstCron_ID=".$cron_ID);
			 
			$command = '* * 1 * 0,6 curl "'.base_url("cronjobs/addchecklist/").'"';
		
			file_put_contents('/tmp/crontab.txt', $output.$command.PHP_EOL);
			return exec('crontab /tmp/crontab.txt');
			} 
			public function deleteCron(){
				$output = shell_exec('crontab -l');
				$cronjob = ('curl http://10.200.101.250/_sz/test/test2');
				if (strstr($output, $cronjob)) {
				   echo 'found';
				} else {
				   echo 'not found';
				}
				$newcron = str_replace($cronjob,"",$output);
				echo "<pre>$newcron</pre>";
				file_put_contents('/tmp/crontab.txt', $newcron.PHP_EOL);
				echo exec('crontab /tmp/crontab.txt');

			} */
	public function addchecklist($processID, $pstCron_ID)
	{ // Duplicate of add_anotherchecklist function in Process.php
		date_default_timezone_set("Asia/Manila");

		$rs = $this->db->query("Select * from tbl_processST_process where process_ID=" . $processID);
		$processST_cronjobs_rs = $this->db->query("Select * from tbl_processST_cronjobs where pstCron_ID=" . $pstCron_ID);
		$rz = explode(",", $processST_cronjobs_rs->result()[0]->assignees);
		$imp = explode(",", $processST_cronjobs_rs->result()[0]->deadline);

		$deadlineAssignee = $this->setDeadlineProcess($imp);

		$userid = $this->session->userdata('uid');
		$processid = $processID;
		$checkTitle = $rs->result()[0]->processTitle . " - " . date("l M, d Y") . " at " . date("h:i a");
		$data['process_ID'] = $processid;
		$data['checklistTitle'] = $checkTitle;
		$data['status_ID'] = '2';
		$data['isArchived'] = '0';
		$data['createdBy'] = $processST_cronjobs_rs->result()[0]->added_by;
		$data['updatedBy'] = $processST_cronjobs_rs->result()[0]->added_by;
		$cid = $this->general_model->insert_vals_last_inserted_id($data, "tbl_processST_checklist");

		$email_arr = array();
		$subject1 = "";
		$data1 = array();
		foreach ($rz as $row) {
			$dueDate = ($processST_cronjobs_rs->result()[0]->deadline != "0,0,0,0,0") ? $deadlineAssignee : "0000-00-00 00:00:00";
			$data_assign['type'] = "checklist";
			$data_assign['assignmentRule_ID'] = 6;
			$data_assign['assignedBy'] = $processST_cronjobs_rs->result()[0]->added_by;
			$data_assign['assignedTo'] = $row;
			$data_assign['folProCheTask_ID'] = $cid;
			$data_assign['dateTimeDueDate'] = $dueDate;
			$data_assign['dateTimeAssigned'] = date("Y-m-d H:i");
			$assignID = $this->general_model->insert_vals_last_inserted_id($data_assign, "tbl_processST_assignment");
			$recipient[0] = [
				'userId' => $row,
			];
			$name = $this->assigner($processST_cronjobs_rs->result()[0]->added_by);
			$notif_message = $name[0]->fname . " " . $name[0]->lname . " " . "has assigned you " . $checkTitle . " checklist [ID :" . str_pad($cid, 8, '0', STR_PAD_LEFT) . "]";
			$link = "process";
			$insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);
			// echo json_encode($insert_stat);


			$email = $this->getEmail($row);

			if (filter_var($email[0]->email, FILTER_VALIDATE_EMAIL)) {
				$data1 = array(
					'senderName' => $name[0]->fname . " " . $name[0]->lname,
					'title' => $checkTitle,
					'cid' => str_pad($cid, 8, '0', STR_PAD_LEFT),
					'dueDate' => $dueDate,
					'link' =>  base_url("process"),
				);

				array_push($email_arr, $email[0]->email);
				$subject1 = $notif_message . " on Process Management System";
				// $this->sendSampleEmail($data1,$subject1,$email[0]->email);

			}
		}
		// echo json_encode($email_arr);
		if (count($email_arr) > 0) {
			$this->sendSampleEmail($data1, $subject1, $email_arr);
		}
		$alltask = $this->process_model->get_task($processid);
		$arTask = [];
		$taskNum = 0;
		foreach ($alltask as $alltaskrow) {
			$arTask[$taskNum] = [
				"checklist_ID" => $cid,
				"task_ID" => $alltaskrow->task_ID,
				"isCompleted" => 2,
			];
			$taskNum++;
		}
		$this->process_model->addbatch_checkstatus($arTask, 'tbl_processST_checklistStatus');
	}

	public function assigner($uid)
	{
		return $this->general_model->fetch_specific_vals("a.fname,a.lname,uid", "a.apid=b.apid and b.isActive='yes' and b.emp_id=c.emp_id and uid=" . $uid, "tbl_applicant a,tbl_employee b,tbl_user c", "lname ASC");;
	}
	public function getEmail($ids)
	{
		$data = $this->general_model->fetch_specific_vals("applicant.email", "user.uid in ($ids) AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid", "tbl_applicant applicant, tbl_employee employee, tbl_user user");

		return $data;
	}
	public function testt()
	{
		$email = "jeffrey@supportzebra.com";

		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			echo ("$email is a valid email address");
		} else {
			echo ("$email is not a valid email address");
		}
	}
	public function setDeadlineProcess($arr)
	{
		date_default_timezone_set("Asia/Manila");
		$dateString = date("Y-m-d H:i");
		$dt = new DateTime($dateString);
		$dt->modify('+' . $arr[0] . ' years +' . $arr[1] . ' months +' . $arr[2] . ' days +' . $arr[3] . ' hours +' . $arr[4] . ' minutes');
		return $dt->format('Y-m-d H:i');
	}
	//SYSTEM NOTIFICATION
	private function send_system_notif($notif_Id, $recipients)
	{
		$notif_recip = [];
		for ($loop = 0; $loop < count($recipients); $loop++) {
			$notif_recip[$loop] = [
				"systemNotification_ID" => $notif_Id,
				"recipient_ID" => $recipients[$loop]['userId'],
				"status_ID" => 8,
			];
		}
		return $insert_status = $this->general_model->batch_insert($notif_recip, 'tbl_system_notification_recipient');
	}

	private function create_system_notif($notif_mssg, $link)
	{
		$data['message'] = $notif_mssg;
		$data['link'] = $link;
		return $notification_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_system_notification');
	}
	protected function set_system_notif($notif_mssg, $link, $recipientArray)
	{
		$notif_id = [];
		$approval_type = 1; // 1- Sequential, 2 - Parallel
		if ($approval_type === 1) {
			$recipients[] = $recipientArray[0];
		} else {
			$recipients = $recipientArray;
		}
		$notif_id[0] = $this->create_system_notif($notif_mssg, $link);
		$data['notif_id'] = $notif_id;
		$data['send_stat'] = $this->send_system_notif($notif_id[0], $recipients);
		return $data;
	}

	protected function sendSampleEmail($data, $sub, $recipient)
	{
		if (count($recipient) > 0) {
			foreach ($recipient as $row) {

				$this->email->initialize(array(
					'protocol' => 'smtp',
					'smtp_host' => 'smtp.gmail.com',
					'smtp_user' => 'notification@supportzebra.com',
					'smtp_pass' => 'spt2019!',
					'smtp_port' => 587,
					'smtp_crypto' => 'tls',
					'mailtype	' => 'html',
					'validate' => TRUE,
					'crlf' => "\r\n",
					'charset' => "iso-8859-1",
					'newline' => "\r\n"
				));
				$this->email->set_mailtype("html");
				$this->email->from('supportzebra@no-reply.com', 'Process Management System');
				$this->email->to($row);
				// $this->email->cc('jeffrey@supportzebra.com');
				$this->email->subject($sub);
				$body = $this->load->view('testEmail.php', $data, TRUE);
				// $this->email->attach($data['img2']);
				$this->email->message($body);
				$this->email->send();

				echo $this->email->print_debugger();
			}
		}
	}

	// DAILY PUSLE DEADLINE CHECKER
	//Query 
	public function fetch_specific_val($fields, $where, $tables, $order = NULL)
	{ //get 1  record
		$this->db->select($fields);
		$this->db->where($where);
		if ($order !== NULL) {
			$this->db->order_by($order);
		}
		$query = $this->db->get($tables);
		return $query->row();
	}
	public function fetch_specific_vals($fields, $where, $tables, $order = NULL)
	{ //get more than 1 records
		$this->db->select($fields);
		$this->db->where($where);
		if ($order !== NULL) {
			$this->db->order_by($order);
		}
		$query = $this->db->get($tables);
		return $query->result();
	}

	public function batch_update($data, $field, $table)
	{
		$this->db->trans_start();
		$this->db->update_batch($table, $data, $field);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			return 0;
		} else {
			return 1;
		}
	}

	public function batch_insert($data, $table)
	{
		$this->db->trans_start();
		$this->db->insert_batch($table, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			return 0;
		} else {
			return 1;
		}
	}

	public function batch_insert_first_inserted_id($data, $table)
	{ // Batch Insert and return first inserted ID
		$this->db->trans_start();
		$this->db->insert_batch($table, $data);
		$firstInsertedId = $this->db->insert_id();
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			return 0;
		} else {
			return $firstInsertedId;
		}
	}

	private function get_current_date_time()
	{
		$date = new DateTime("now", new DateTimeZone('Asia/Manila'));
		$dates = [
			'dateTime' => $date->format('Y-m-d H:i:s'),
			'date' => $date->format('Y-m-d'),
			'time' => $date->format('H:i:s'),
		];
		return $dates;
	}

	private function get_due_intervention()
	{
		$dateTime = $this->get_current_date_time();
		$fields = "intervention.intervention_ID, intervention.intervener_empid, appIntervene.lname lnameIntervene, appIntervene.fname fnameIntervene, appIntervene.mname mnameIntervene, intervention.emp_id, app.lname, app.fname, app.mname, intervention.interveneLevel, intervention.status_ID, intervention.interventionDate, intervention.deadline, answers.answerId, answers.occurence, choices.choiceId";
		$where = "app.apid = emp.apid AND emp.emp_id = intervention.emp_id AND appIntervene.apid = empIntervene.apid AND empIntervene.emp_id = intervention.intervener_empid AND choices.levels = answers.answer AND answers.answerId = intervention.answer_id AND intervention.status_ID = 2 AND intervention.deadline <='" . $dateTime['dateTime'] . "'";
		$table = "tbl_daily_survey_intervention intervention, tbl_szfive_survey_answers answers, tbl_szfive_survey_choices choices, tbl_applicant app, tbl_employee emp, tbl_applicant appIntervene, tbl_employee empIntervene";
		return $this->fetch_specific_vals($fields, $where, $table);
	}

	private function get_max_choice_escalation($choice_id)
	{
		$fields = "MAX(occurence) as maximum";
		$where = "choice_ID = $choice_id";
		return $this->fetch_specific_val($fields, $where, "tbl_daily_pulse_escalation");
	}

	private function qry_all_next_intervention($choice_id, $occurrence, $existing_levels)
	{
		$fields = "intervention, occurence";
		$where = "choice_ID = $choice_id AND occurence > $occurrence AND intervention != 0 AND intervention NOT IN ($existing_levels)";
		return $this->fetch_specific_vals($fields, $where, "tbl_daily_pulse_escalation", "occurence ASC");
	}

	private function get_direct_supervisor_via_emp_id($emp_id)
	{
		$fields = "Supervisor";
		$where = "emp_id = $emp_id AND Supervisor !=''";
		return $this->fetch_specific_val($fields, $where, "tbl_employee");
	}

	private function get_interveners($answer_id)
	{
		$fields = "intervener_empid, interveneLevel";
		$where = "answer_id = $answer_id";
		return $this->fetch_specific_vals($fields, $where, "tbl_daily_survey_intervention", "interveneLevel ASC");
	}

	public function get_current_intervention_deadline($intervention_level, $emp_id, $existing_interveners_emp)
	{

		$hr_manager = 522;
		$managing_director = 980;
		$intervener = [];
		$intervener_count = 0;
		$direct_sup = $this->get_direct_supervisor_via_emp_id($emp_id);
		if (count($direct_sup) > 0) {
			$current_intervener = (int) $direct_sup->Supervisor;
			if ($current_intervener == $managing_director) {
				if (array_search($hr_manager, $intervener) === FALSE) {
					$intervener[0] = $hr_manager;
					$current_intervener = $hr_manager;
				} else {
					$intervener[0] = $current_intervener;
				}
			} else {
				$intervener[0] = $current_intervener;
			}
			for ($loop = 1; $loop < $intervention_level; $loop++) {
				$direct_sup_supervisor = $this->get_direct_supervisor_via_emp_id($current_intervener);
				if ((count($direct_sup_supervisor) > 0) && (strlen($direct_sup_supervisor->Supervisor) !== 0)) {
					$intervener_count++;
					$current_intervener = (int) $direct_sup_supervisor->Supervisor;
					if ($current_intervener == $managing_director) {
						if (array_search($hr_manager, $intervener) === FALSE) {
							$intervener[$intervener_count] = $hr_manager;
							$current_intervener = $hr_manager;
						} else {
							$intervener[$intervener_count] = $current_intervener;
						}
					} else {
						$intervener[$intervener_count] = $current_intervener;
					}
					if ($intervention_level == 4 && $loop == 3) {
						if (array_search($hr_manager, $intervener) === FALSE) {
							$intervener[$intervener_count] = $hr_manager;
						}
					} else if ($intervention_level >= 5 && $loop >= 4) {
						$intervener[$intervener_count] = 980;
					}
				}
			}
			if (in_array($intervener[count($intervener) - 1], $existing_interveners_emp)) {
				return 0;
			} else {
				return (int) $intervener[count($intervener) - 1];
			}
		} else {
			return 0;
		}
	}

	public function get_next_intervention($choice_id, $occurrence, $emp_id, $answer_id)
	{
		$next_intervention['level'] = 0;
		$next_intervention['occurrence'] = 0;
		$next_intervention['emp_id'] = 0;
		$max = $this->get_max_choice_escalation($choice_id);
		$existing_interveners_arr = [];
		$existing_interveners_emp = [];
		$existing_interveners_arr_count = 0;
		$existing_interveners = $this->get_interveners($answer_id);
		if (count($existing_interveners) > 0) {
			foreach ($existing_interveners as $existing_interveners_rows) {
				$existing_interveners_arr[$existing_interveners_arr_count] = $existing_interveners_rows->interveneLevel;
				$existing_interveners_emp[$existing_interveners_arr_count] = $existing_interveners_rows->intervener_empid;
				$existing_interveners_arr_count++;
			}
		}
		if ($occurrence <= $max->maximum) {
			$all_next_intervention = $this->qry_all_next_intervention($choice_id, $occurrence, implode(",", $existing_interveners_arr));
			if (count($all_next_intervention) > 0) {
				$current_intervention_emp = $this->get_current_intervention_deadline($all_next_intervention[0]->intervention, $emp_id, $existing_interveners_emp);
				if ($current_intervention_emp !== 0) {
					$next_intervention['level'] = (int) $all_next_intervention[0]->intervention;
					$next_intervention['occurrence'] = (int) $all_next_intervention[0]->occurence;
					$next_intervention['emp_id'] = $current_intervention_emp;
				}
			}
		}
		return $next_intervention;
	}

	public function check_intervention_deadline()
	{
		$dateTime = $this->get_current_date_time();
		$due_intervention = $this->get_due_intervention();
		$missed_intervention = [];
		$final_missed_intervention = [];
		$missed_intervention_count = 0;
		$next_intervention_count = 0;
		$final_missed_intervention_count = 0;
		// var_dump($due_intervention);
		if (count($due_intervention) > 0) {
			// transfer to next level supervisor
			foreach ($due_intervention as $due_intervention_row) {
				// var_dump($due_intervention_row);
				$emp_name = $due_intervention_row->fname . " " . $due_intervention_row->lname;
				$missed_emp_details = $this->get_emp_details_via_emp_id($due_intervention_row->intervener_empid);
				// notify missed intervention
				$missed_link = 'szfive/survey/view/' . $due_intervention_row->emp_id . '/' . $due_intervention_row->answerId;
				$missed_mssg = "You have missed to intervene to the Daily Pulse of " . ucwords($emp_name) . " prior " . date("g:i A", strtotime($due_intervention_row->deadline)) . " last " . date("M j, Y", strtotime($due_intervention_row->deadline));
				$missed_notif_id = $this->simple_system_notification($missed_mssg, $missed_link, $missed_emp_details->uid);

				// notify next intervention
				$missed_intervention[$missed_intervention_count] = [
					"intervention_ID" => $due_intervention_row->intervention_ID,
					"status_ID" => 12
				];

				// collect missed notification
				$intervener_name = $due_intervention_row->fnameIntervene . " " . $due_intervention_row->lnameIntervene;

				$missed_intervention_count++;
				$next_intervention_level = $this->get_next_intervention($due_intervention_row->choiceId, $due_intervention_row->occurence, $due_intervention_row->emp_id, $due_intervention_row->answerId);
				// var_dump($next_intervention_level);
				if ($next_intervention_level['level'] !== 0) {
					// collect next intervention of next level interveners
					$next_intervention_data["intervener_empid"] = $next_intervention_level['emp_id'];
					$next_intervention_data["emp_id"] = $due_intervention_row->emp_id;
					$next_intervention_data["answer_id"] =  $due_intervention_row->answerId;
					$next_intervention_data["interveneLevel"] = $next_intervention_level['level'];
					$next_intervention_data["interventionDate"] = $dateTime['dateTime'];
					$next_intervention_data["deadline"] = date("Y-m-d H:i:00", strtotime("+1 day", strtotime($dateTime['dateTime'])));

					$this->general_model->insert_vals($next_intervention_data, "tbl_daily_survey_intervention");

					// notify next intervener
					$next_emp_details = $this->get_emp_details_via_emp_id($next_intervention_level['emp_id']);
					$next_link = 'szfive/survey/view/' . $due_intervention_row->emp_id . '/' . $due_intervention_row->answerId;
					$next_mssg = "The Daily Pulse Intervention of " . ucwords($emp_name) . " was forwarded to you because " . ucwords($intervener_name) . " missed to intervene before the deadline: " . date("g:i A", strtotime($due_intervention_row->deadline)) . ", " . date("M j, Y", strtotime($due_intervention_row->deadline));
					$missed_notif_id = $this->simple_system_notification($next_mssg, $next_link, $next_emp_details->uid);
					$next_intervention_count++;
				} else {
					$final_missed_intervention[$final_missed_intervention_count] = [
						"answerId" => $due_intervention_row->answerId,
						"status_ID" => 12
					];
					$final_missed_intervention_count++;
				}
			}
			if (count($missed_intervention) > 0) {
				$this->batch_update($missed_intervention, 'intervention_ID', 'tbl_daily_survey_intervention');
			}
			if (count($final_missed_intervention) > 0) {
				$this->batch_update($final_missed_intervention, 'answerId', 'tbl_szfive_survey_answers');
			}
		}
	}
	public function sendEmail()
	{
		date_default_timezone_set("Asia/Manila");
		$senderName = $this->input->post("name");
		$pic = $this->input->post("pic");
		$msg = $this->input->post("message");
		$sender_email = $this->input->post("sender_email");
		$recipient_email = $this->input->post("recipient_email");

		$this->email->initialize(array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_user' => 'notification@supportzebra.com',
			'smtp_pass' => 'spt2019!',
			'smtp_port' => 587,
			'smtp_crypto' => 'tls',
			'mailtype	' => 'html',
			'validate' => TRUE,
			'crlf' => "\r\n",
			'charset' => "iso-8859-1",
			'newline' => "\r\n"
		));

		$this->email->set_mailtype("html");
		$this->email->cc('notification@supportzebra.com');

		$this->email->from($sender_email, 'CHAT');
		$this->email->to($recipient_email);
		$this->email->subject("$senderName sent you a message on " . date("F d, Y"));


		$img = $pic;
		if (file_exists($img)) {
			$this->email->attach($img);
			$cid = $this->email->attachment_cid($img);
			$data["img"] = "<img src='cid:" . $cid . "' alt='photo1' style='width: 100px;border-radius: 100%;border: 7px solid #eeee;'/>";
		}

		$data["message"] = $msg;

		$body = $this->load->view('chatTemplate.php', $data, TRUE);
		// $this->email->attach($data['img2']);
		$this->email->message($body);
		$this->email->send();

		echo $this->email->print_debugger();
	}

	// DMS Michael Sanchez Codes Start here ---->connection_status

	private function get_ir_deadline_settings($field)
	{
		$fields = "deadlineToExplain, deadlineToExplainOption, deadlineToRecommend, deadlineToRecommendOption, deadlineLastRecommendation, deadlineFinalDecision, deadlineUploadDocs";
		$where = "deadlineToExplain IS NOT NULL";
		return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_incident_report_deadline_settings');
	}

	public function add_default_ir_deadline_settings()
	{
		$data['deadlineToExplain'] = 1;
		$data['deadlineToExplainOption'] = 1;
		$data['deadlineToRecommend'] = 1;
		$data['deadlineToRecommendOption'] = 1;
		$data['deadlineLastRecommendation'] = 1;
		$data['deadlineFinalDecision'] = 1;
		$data['deadlineUploadDocs'] = 1;
		$data['changedBy'] = $this->session->userdata('emp_id');
		return $this->general_model->insert_vals($data, 'tbl_dms_incident_report_deadline_settings');
	}

	protected function get_ir_deadline_settings_details_recorded()
	{
		// $field = $this->input->post('field');
		$field = "deadlineToExplain";
		$ir_deadline_settings = $this->get_ir_deadline_settings($field);
		if (count($ir_deadline_settings) > 0) {
			$deadline_settings['record'] = $ir_deadline_settings;
		} else {
			$add_default_stat = $this->add_default_ir_deadline_settings();
			if ($add_default_stat) {
				$deadline_settings['record'] = $this->get_ir_deadline_settings($field);
			} else {
				$deadline_settings['record'] = 0;
			}
		}
		return $deadline_settings;
	}

	private function qry_dms_filing_deadline_old($deadline_type, $dateTime)
	{
		$fields = "dead.dmsDeadline_ID, dead.record_ID, dead.emp_id, dead.deadline, supQualified.level, supQualified.supervisorQualifiedUserDtrViolationNotification_ID, supQualified.qualifiedUserDtrViolation_ID, qualified.userDtrViolation_ID, dtrVio.dtrViolationType_ID, dtrVio.sched_date";
		$where = "dtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.qualifiedUserDtrViolation_ID = supQualified.qualifiedUserDtrViolation_ID AND supQualified.qualifiedFilingSupervisorType_ID = 1 AND supQualified.emp_id = dead.emp_id AND supQualified.qualifiedUserDtrViolation_ID = dead.record_ID AND dead.dmsDeadlineType_ID = $deadline_type AND qualified.status_ID = 2 AND dead.status_ID = 2 AND dead.deadline <= '$dateTime'";
		$table = "tbl_dms_deadline dead, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified, tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation dtrVio";
		return  $this->general_model->fetch_specific_vals($fields,  $where, $table);
	}

	private function qry_dms_filing_deadline($deadline_type, $dateTime)
	{
		$fields = "dead.dmsDeadline_ID, dead.record_ID, dead.emp_id, dead.deadline, supQualified.level, supQualified.supervisorQualifiedUserDtrViolationNotification_ID, supQualified.qualifiedUserDtrViolation_ID, qualified.userDtrViolation_ID, dtrVio.dtrViolationType_ID, dtrVio.sched_date";
		$where = "dtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.qualifiedUserDtrViolation_ID = supQualified.qualifiedUserDtrViolation_ID AND supQualified.qualifiedFilingSupervisorType_ID = 1 AND supQualified.emp_id = dead.emp_id AND supQualified.qualifiedUserDtrViolation_ID = dead.record_ID AND dead.dmsDeadlineType_ID = $deadline_type AND qualified.status_ID = 2 AND dead.status_ID = 2 AND supQualified.status_ID = 4 AND dead.deadline <= '$dateTime'";
		$table = "tbl_dms_deadline dead, tbl_dms_supervisor_qualified_user_dtr_violation_notification supQualified, tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation dtrVio";
		return  $this->general_model->fetch_specific_vals($fields,  $where, $table);
	}

	private function get_qualified_dtr_violation($qualified_violation_id)
	{
		$fields = "qualifiedUserDtrViolation_ID, occurrenceRuleNum, occurrenceValue, userDtrViolation_ID, emp_id, status_ID";
		$where = "qualifiedUserDtrViolation_ID = $qualified_violation_id";
		return $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_qualified_user_dtr_violation');
	}

	protected function get_emp_details_via_emp_id($emp_id)
	{
		$fields = "emp.emp_id, users.uid, app.pic, app.fname, app.lname, app.mname, app.nameExt, app.gender, app.email, emp.Supervisor, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, positions.site_ID, empStat.status as empStatus,users.password as pw";
		$where = "empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.emp_id = $emp_id";
		$table = "tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app, tbl_account accounts, tbl_user users";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	/* protected function get_dtr_violation_settings($dtr_violation_type_id)
	{
		$fields = "dtrVioSettings.dtrViolationTypeSettings_ID, dtrVioSettings.dtrViolationType_ID, dtrVioSettings.oneTimeStat, dtrVioSettings.oneTimeVal, dtrVioSettings.consecutiveStat, dtrVioSettings.consecutiveVal, dtrVioSettings.nonConsecutiveStat, dtrVioSettings.nonConsecutiveVal, dtrVioSettings.durationType, dtrVioSettings.durationVal, dtrVioSettings.durationUnit, dtrVioSettings.offense_ID, dtrVioSettings.category_ID, dtrVioSettings.daysDeadline, dtrVioSettings.deadlineOption";
		$where = "dtrVioSettings.dtrViolationType_ID = $dtr_violation_type_id";
		return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_dtr_violation_type_settings dtrVioSettings");
	} */


	protected function get_supervisors($emp_id, $num)
	{
		$data['error'] = 0;
		$data['error_details'] = "";
		$supervisors = [];
		$direct_sup = $this->get_direct_supervisor_via_emp_id($emp_id);
		if (count($direct_sup) > 0) {
			$sup_obj['level'] = 1;
			$sup_obj['emp_id'] = $direct_sup->Supervisor;
			$supervisors[0] = [
				'level' => 1,
				'emp_id' => $direct_sup->Supervisor
			];
			$dynamic_sup = $direct_sup->Supervisor;
			for ($loop = 1; $loop < $num; $loop++) {
				$next_level_sup = $this->get_direct_supervisor_via_emp_id($dynamic_sup);
				if (count($next_level_sup) > 0) {
					$supervisors[$loop] = [
						'level' => $loop + 1,
						'emp_id' => $next_level_sup->Supervisor
					];
					$dynamic_sup = $next_level_sup->Supervisor;
				} else {
					break;
				}
			}
			$data['supervisors'] = $supervisors;
		} else {
			$data['error'] = 1;
			$data['error_details'] = "direct supervisor does not exist";
		}
		return $data;
	}

	public function default_ero()
	{
		return 207;
	}

	private function get_assigned_ero($acc_id,  $site_id)
	{
		$fields = "emp_id";
		$where = "acc_id =  $acc_id AND site_ID =  $site_id";
		return  $this->general_model->fetch_specific_val($fields,  $where, "tbl_dms_employee_relations_officer");
	}

	private function identify_assigned_ero($acc_id, $site_id)
	{
		$ero = $this->default_ero();
		$assigned_ero = $this->get_assigned_ero($acc_id, $site_id);
		if (count($assigned_ero) > 0) {
			$ero = $assigned_ero->emp_id;
		}
		return $ero;
	}

	public function deadline_to_file_qualified()
	{
		$set_to_missed = [];
		$set_supervisor_qualified_to_missed = [];
		$add_supervisor_qualified = [];
		$transfer_filing = [];
		$transfer_filing_count = 0;
		$dateTime = $this->get_current_date_time();
		$qry_deadline = $this->qry_dms_filing_deadline(1, $dateTime['dateTime']);
		$index_count = 0;
		if (count($qry_deadline) > 0) {
			foreach ($qry_deadline as $index => $row) {
				$violation_settings = $this->get_dtr_violation_settings($row->dtrViolationType_ID);
				$days_to_file = (int) $violation_settings->daysDeadline;
				$next_level_emp_id = 0;
				$deadline_level = 0;
				$file_deadline_date_time = "";
				$notif_transfer_mssg = "";
				$qualified_record = $this->get_qualified_dtr_violation($row->record_ID);
				$sub_emp_details = $this->get_emp_details_via_emp_id($qualified_record->emp_id);
				$due_emp_details = $this->get_emp_details_via_emp_id($row->emp_id);
				if ((int) $violation_settings->deadlineOption == 1) {
					$file_deadline_date = Date("Y-m-d", strtotime($dateTime['date'] . "  $days_to_file days"));
					$file_deadline_date_time =  $file_deadline_date . " 23:59:59";
					// var_dump("Proceed to next supervisor");
					// set deadline to miss
					$set_to_missed[$index_count] = [
						'dmsDeadline_ID' => $row->dmsDeadline_ID,
						'status_ID' => 12
					];
					// set supervisor qualifed to missed
					$set_supervisor_qualified_to_missed[$index_count] = [
						'supervisorQualifiedUserDtrViolationNotification_ID' => $row->supervisorQualifiedUserDtrViolationNotification_ID,
						'status_ID' => 12
					];
					// Notify missed
					$mssg = "You have missed to file an IR to the qualified DTR violation of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname) . " prior " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline));
					$link = 'discipline/dms_supervision/dtrViolations/fileDtrViolationIrRecordTab';
					$this->simple_system_notification($mssg, $link, $due_emp_details->uid, 'dms');
					$deadline_level = $row->level + 1;
					if (($row->level == 1) && ($row->emp_id != 522)) {
						// transfer to next level sup
						// notify to next level sup
						$next_level = $this->get_supervisors($row->emp_id, 1);
						if ($next_level['error'] != 1) {
							$supFilingType = 1;
							$next_level_emp_id = $next_level['supervisors'][0]['emp_id'];
							if($next_level_emp_id == 980){
								$next_level_emp_id = 522;
							}
							$notif_transfer_mssg = "The filing of IR to the qualified DTR violation of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname) . " was forwarded to you because " . ucwords($due_emp_details->fname . " " . $due_emp_details->lname) . " missed to file it before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". Your deadline to file is on " . date("M j, Y", strtotime($file_deadline_date_time)) . ".";
						} else {
							// proceed to ERO
							$next_level_emp_id = $this->identify_assigned_ero($sub_emp_details->acc_id, $sub_emp_details->site_ID);
							$notif_transfer_mssg = "The filing of IR to the qualified DTR violation of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname) . " was forwarded to you because " . ucwords($due_emp_details->fname . " " . $due_emp_details->lname) . " missed to file it before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ".";
							$supFilingType = 2;
							// direct sup does not exist
						}
					} else {
						// proceed to ERO
						$next_level_emp_id = $this->identify_assigned_ero($sub_emp_details->acc_id, $sub_emp_details->site_ID);
						$notif_transfer_mssg = "The filing of IR to the qualified DTR violation of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname) . " was forwarded to you because " . ucwords($due_emp_details->fname . " " . $due_emp_details->lname) . " missed to file it before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ".";
						$supFilingType = 2;
					}
					// add deadline
					$transfer_filing[$transfer_filing_count] = [
						'record_ID' =>  $row->record_ID,
						'emp_id' =>  $next_level_emp_id,
						'deadline' =>  $file_deadline_date_time,
						'dmsDeadlineType_ID' =>  1,
						'status_ID' => 2,
						'level' =>  $deadline_level,
					];
					$transfer_filing_count++;
					// Notify  Transfer
					$transfer_emp_details = $this->get_emp_details_via_emp_id($next_level_emp_id);
					$next_mssg = $notif_transfer_mssg;
					$next_link = "discipline/dms_supervision/dtrViolations/fileDtrViolationIrTab";
					$this->simple_system_notification($next_mssg, $next_link, $transfer_emp_details->uid, 'dms');
					// add supervisor filing
					$add_supervisor_qualified[$index_count] = [
						'qualifiedUserDtrViolation_ID' => $row->qualifiedUserDtrViolation_ID,
						'emp_id' => $next_level_emp_id,
						'level' => $deadline_level,
						'status_ID' => 4,
						'qualifiedFilingSupervisorType_ID' => $supFilingType
					];
					$index_count++;
				}else{
					// var_dump("Don't Proceed to next supervisor");
					// Notify missed
					$days_count_file = date_diff(date_create($dateTime['dateTime']), date_create($row->sched_date), true);
					$days_label = $days_count_file->days." day";
					if((int) $days_count_file->days > 1){
						$days_label .='s';
					}
					$mssg = $days_label." have already passed and you haven't yet filed the dtr violation (QID-". str_pad($row->qualifiedUserDtrViolation_ID, 8, '0', STR_PAD_LEFT).") of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname);
					$link = 'discipline/dms_supervision/dtrViolations/fileDtrViolationIrRecordTab';
					$this->simple_system_notification($mssg, $link, $due_emp_details->uid, 'dms');
				}
			}
			// add supervisor qualified notification to transfer filing privilege to next level supervisor
			if (count($add_supervisor_qualified) > 0) {
				$this->general_model->batch_insert($add_supervisor_qualified, "tbl_dms_supervisor_qualified_user_dtr_violation_notification");
			}
			// set deadline of missed supervisor to missed
			if (count($set_to_missed) > 0) {
				$this->general_model->batch_update($set_to_missed, 'dmsDeadline_ID', 'tbl_dms_deadline');
			}
			// set Supervisor Notification to missed
			if (count($set_to_missed) > 0) {
				$this->general_model->batch_update($set_supervisor_qualified_to_missed, 'supervisorQualifiedUserDtrViolationNotification_ID', 'tbl_dms_supervisor_qualified_user_dtr_violation_notification');
			}
			// set new deadline for next level except ero
			if (count($transfer_filing) > 0) {
				$this->general_model->batch_insert($transfer_filing, 'tbl_dms_deadline');
			}
		}
	}
	public function deadline_to_file_qualified_old()
	{
		$set_to_missed = [];
		$set_supervisor_qualified_to_missed = [];
		$add_supervisor_qualified = [];
		$transfer_filing = [];
		$transfer_filing_count = 0;
		$dateTime = $this->get_current_date_time();
		$qry_deadline = $this->qry_dms_filing_deadline(1, $dateTime['dateTime']);
		$index_count = 0;		
		if (count($qry_deadline) > 0) {
			foreach ($qry_deadline as $index => $row) {				
				$violation_settings = $this->get_dtr_violation_settings($row->dtrViolationType_ID);
				$days_to_file = (int) $violation_settings->daysDeadline;
				$next_level_emp_id = 0;
				$deadline_level = 0;
				$file_deadline_date_time = "";
				$notif_transfer_mssg = "";
				$qualified_record = $this->get_qualified_dtr_violation($row->record_ID);
				$sub_emp_details = $this->get_emp_details_via_emp_id($qualified_record->emp_id);
				$due_emp_details = $this->get_emp_details_via_emp_id($row->emp_id);
				if ((int) $violation_settings->deadlineOption == 1) {
					// set deadline to miss
					$set_to_missed[$index_count] = [
						'dmsDeadline_ID' => $row->dmsDeadline_ID,
						'status_ID' => 12
					];
					// set supervisor qualifed to missed
					$set_supervisor_qualified_to_missed[$index_count] = [
						'supervisorQualifiedUserDtrViolationNotification_ID' => $row->supervisorQualifiedUserDtrViolationNotification_ID,
						'status_ID' => 12
					];
					// Notify missed
					$mssg = "You have missed to file an IR to the qualified DTR violation of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname) . " prior " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline));
					$link = 'discipline/dms_supervision/dtrViolations/fileDtrViolationIrRecordTab';
					$this->simple_system_notification($mssg, $link, $due_emp_details->uid, 'dms');
					$deadline_level = $row->level + 1;
					if ($row->level == 1) {
						// transfer to next level sup
						// notify to next level sup
						$next_level = $this->get_supervisors($row->emp_id, 1);
						if ($next_level['error'] != 1) {
							$supFilingType = 1;
							$next_level_emp_id = $next_level['supervisors'][0]['emp_id'];
							$file_deadline_date = Date("Y-m-d", strtotime($dateTime['date'] . "  $days_to_file days"));
							$file_deadline_date_time =  $file_deadline_date . " 23:59:59";

							$notif_transfer_mssg = "The filing of IR to the qualified DTR violation of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname) . " was forwarded to you because " . ucwords($due_emp_details->fname . " " . $due_emp_details->lname) . " missed to file it before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". Your deadline to file is on " . date("M j, Y", strtotime($file_deadline_date_time)) . ".";

							// add deadline
							$transfer_filing[$transfer_filing_count] = [
								'record_ID' =>  $row->record_ID,
								'emp_id' =>  $next_level_emp_id,
								'deadline' =>  $file_deadline_date_time,
								'dmsDeadlineType_ID' =>  1,
								'status_ID' => 2,
								'level' =>  $deadline_level + 1,
							];
							$transfer_filing_count++;
						} else {
							// proceed to ERO
							$next_level_emp_id = $this->identify_assigned_ero($sub_emp_details->acc_id, $sub_emp_details->site_ID);
							$notif_transfer_mssg = "The filing of IR to the qualified DTR violation of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname) . " was forwarded to you because " . ucwords($due_emp_details->fname . " " . $due_emp_details->lname) . " missed to file it before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ".";
							$supFilingType = 2;
							// direct sup does not exist
						}
					} else if ($row->level == 2) {
						// proceed to ERO
						$next_level_emp_id = $this->identify_assigned_ero($sub_emp_details->acc_id, $sub_emp_details->site_ID);
						$notif_transfer_mssg = "The filing of IR to the qualified DTR violation of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname) . " was forwarded to you because " . ucwords($due_emp_details->fname . " " . $due_emp_details->lname) . " missed to file it before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ".";
						$supFilingType = 2;
					}

					// Notify  Transfer
					$transfer_emp_details = $this->get_emp_details_via_emp_id($next_level_emp_id);
					$next_mssg = $notif_transfer_mssg;
					$next_link = "discipline/dms_supervision/dtrViolations/fileDtrViolationIrTab";
					$this->simple_system_notification($next_mssg, $next_link, $transfer_emp_details->uid, 'dms');

					// add supervisor filing
					$add_supervisor_qualified[$index_count] = [
						'qualifiedUserDtrViolation_ID' => $row->qualifiedUserDtrViolation_ID,
						'emp_id' => $next_level_emp_id,
						'level' => $deadline_level,
						'status_ID' => 4,
						'qualifiedFilingSupervisorType_ID' => $supFilingType
					];
					$index_count++;
				}else{
					// Notify missed
					$days_count_file = date_diff(date_create($dateTime['dateTime']), date_create($row->sched_date), true);
					$days_label = $days_count_file->days." day";
					if((int) $days_count_file->days > 1){
						$days_label .='s';
					}
					$mssg = $days_label." have already passed and you haven't yet filed the dtr violation (QID-". str_pad($row->qualifiedUserDtrViolation_ID, 8, '0', STR_PAD_LEFT).") of " . ucwords($sub_emp_details->fname . " " . $sub_emp_details->lname);
					$link = 'discipline/dms_supervision/dtrViolations/fileDtrViolationIrRecordTab';
					// var_dump($mssg);
					$this->simple_system_notification($mssg, $link, $due_emp_details->uid, 'dms');
				}
			}
			// add supervisor qualified notification to transfer filing privilege to next level supervisor
			if (count($add_supervisor_qualified) > 0) {
				$this->general_model->batch_insert($add_supervisor_qualified, "tbl_dms_supervisor_qualified_user_dtr_violation_notification");
			}
			// set deadline of missed supervisor to missed
			if (count($set_to_missed) > 0) {
				$this->general_model->batch_update($set_to_missed, 'dmsDeadline_ID', 'tbl_dms_deadline');
			}
			// set Supervisor Notification to missed
			if (count($set_to_missed) > 0) {
				$this->general_model->batch_update($set_supervisor_qualified_to_missed, 'supervisorQualifiedUserDtrViolationNotification_ID', 'tbl_dms_supervisor_qualified_user_dtr_violation_notification');
			}

			// set new deadline for next level except ero
			if (count($transfer_filing) > 0) {
				$this->general_model->batch_insert($transfer_filing, 'tbl_dms_deadline');
			}
			// var_dump($set_to_missed);
			// var_dump($notify_missed);
			// var_dump( $set_supervisor_qualified_to_missed);
			// var_dump( $notify_transfer);
			// var_dump( $transfer_filing);
			// var_dump($add_supervisor_qualified);
			// var_dump($missed_recipient);
			// var_dump($next_level_recipient);
		}
	}


	private function qry_deadline_explain($dateTime){
		$fields = "dead.dmsDeadline_ID, dead.record_ID, dead.emp_id, dead.deadline";
		$where = "ir.incidentReport_ID = dead.record_ID AND ir.subjectEmp_ID = dead.emp_id AND ir.subjectExplanationStat = 2 AND ir.incidentReportStages_ID = 1 AND dead.dmsDeadlineType_ID = 2 AND dead.status_ID = 2 AND dead.deadline <= '$dateTime'";
		$table = "tbl_dms_deadline dead, tbl_dms_incident_report ir";
		return  $this->general_model->fetch_specific_vals($fields,  $where, $table);
	}

	private function qry_deadline_recommend_sequential($dateTime)
	{
		$fields = "dead.dmsDeadline_ID, dead.record_ID, dead.emp_id, dead.deadline, dead.dmsDeadlineType_ID, irRec.incidentReport_ID, irRec.level, ir.deadlineToRecommendOption";
		$where = "ir.incidentReport_ID = irRec.incidentReport_ID AND ir.deadlineToRecommendOption = 1 AND irRec.incidentReportRecommendation_ID = dead.record_ID AND irRec.emp_ID = dead.emp_id AND dead.dmsDeadlineType_ID = 3 AND dead.status_ID = 2 AND dead.deadline <= '$dateTime'";
		$table = "tbl_dms_deadline dead, tbl_dms_incident_report_recommendation irRec, tbl_dms_incident_report ir";
		return  $this->general_model->fetch_specific_vals($fields,  $where, $table);
	}

	private function qry_deadline_recommend_parallel($ir_id, $dateTime)
	{
		
		$fields = "dead.dmsDeadline_ID, dead.record_ID, dead.emp_id, dead.deadline, dead.dmsDeadlineType_ID, irRec.incidentReport_ID, irRec.level";
		$where = "dead.record_ID = irRec.incidentReportRecommendation_ID AND irRec.incidentReport_ID =  $ir_id AND irRec.incidentReportRecommendationType_ID = 1 AND dead.status_ID = 2";
		$table = "tbl_dms_deadline dead, tbl_dms_incident_report_recommendation irRec";
		return  $this->general_model->fetch_specific_vals($fields,  $where, $table);
	}

	private function qry_parallel_ir($dateTime){
		$qry = "SELECT ir.incidentReport_ID, dead.dmsDeadline_ID, dead.deadline FROM tbl_dms_incident_report ir, tbl_dms_incident_report_recommendation irRec, tbl_dms_deadline dead WHERE dead.record_ID = irRec.incidentReportRecommendation_ID AND irRec.incidentReport_ID = ir.incidentReport_ID AND ir.deadlineToRecommendOption = 2 AND dead.status_ID = 2 AND dead.deadline <= '$dateTime' GROUP BY ir.incidentReport_ID";
		return $this->general_model->custom_query($qry);
	}

	private function qry_last_recommender($ir_id){
		$fields = "dead.dmsDeadline_ID, dead.record_ID, dead.emp_id, dead.deadline, dead.dmsDeadlineType_ID, irRec.incidentReport_ID, irRec.level";
		$where = "dead.record_ID = irRec.incidentReportRecommendation_ID AND irRec.incidentReport_ID =  $ir_id AND irRec.incidentReportRecommendationType_ID = 2";
		$table = "tbl_dms_deadline dead, tbl_dms_incident_report_recommendation irRec";
		return  $this->general_model->fetch_specific_val($fields,  $where, $table);
	}



	public function parallel_test(){
		$dateTime = $this->get_current_date_time();
		$parallel_ir = $this->qry_parallel_ir($dateTime['dateTime']);
		//var_dump($parallel_ir);
	}

	public function deadline_to_explain()
	{
		$set_to_missed = [];
		$set_explain_ir_to_missed = [];
		$dateTime = $this->get_current_date_time();
		$rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
		if ((int) $rec_deadline['record']->deadlineToExplainOption === 1) {
			// var_dump("check deadline to explain");
			$dead_explain = $this->qry_deadline_explain($dateTime['dateTime']);
			if (count($dead_explain) > 0) {
				foreach ($dead_explain as $index => $row) {
					$missed_emp_details = $this->get_emp_details_via_emp_id($row->emp_id);
					$mssg = "You have missed to give an explanation to your incident report (IR-ID " .  str_pad($row->record_ID, 8, '0', STR_PAD_LEFT)  . ") before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". With this, your IR will now proceed to recommendation process. </b><br><small><b>IR-ID</b>: " . str_pad($row->record_ID, 8, '0', STR_PAD_LEFT) . " </small>";
					$link = "discipline/dms_personal";
					// var_dump($mssg);
					// var_dump($link);
					// SET NOTIFICATION
					$missed_notif_id = $this->simple_system_notification($mssg, $link, $missed_emp_details->uid, 'dms');

					$set_to_missed[$index] = [
						'dmsDeadline_ID' => $row->dmsDeadline_ID,
						'status_ID' => 12
					];

					$set_explain_ir_to_missed[$index] = [
						'incidentReport_ID' => $row->record_ID,
						'subjectExplanationStat' => 12,
						'incidentReportStages_ID' => 2
					];

					// SET RECOMMENDERS
					$this->set_recommenders($row->record_ID);
				}

				// SET EXPLAIN DEADLINE TO MISSED
				if (count($set_to_missed) > 0) {
					$this->general_model->batch_update($set_to_missed, 'dmsDeadline_ID', 'tbl_dms_deadline');
				}
				// UPDATE EXPLAIN STATUS OF IR
				if (count($set_explain_ir_to_missed) > 0) {
					$this->general_model->batch_update($set_explain_ir_to_missed, 'incidentReport_ID', 'tbl_dms_incident_report');
				}
			}
		}
	}

	public function get_next_recommendation($ir_id, $next_level)
	{
		$fields = "recommend.incidentReportRecommendation_ID, recommend.incidentReport_ID, recommend.emp_ID, recommend.liabilityStat_ID, recommend.level, recommend.datedLiabilityStat, recommend.disciplinaryActionCategory_ID, recommend.incidentReportRecommendationType_ID, recommendType.type, recommend.notes, recommend.susTermDetails, dead.dmsDeadline_ID, dead.deadline";
		$where = "recommendType.incidentReportRecommendationType_ID = recommend.incidentReportRecommendationType_ID AND dead.record_ID = recommend.incidentReportRecommendation_ID AND dead.emp_id = recommend.emp_ID AND recommend.incidentReport_ID = $ir_id AND recommend.level = $next_level";
		$table = "tbl_dms_deadline dead, tbl_dms_incident_report_recommendation recommend, tbl_dms_incident_report_recommendation_type recommendType";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	protected function ordinal($number)
	{
		$ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
		if ((($number % 100) >= 11) && (($number % 100) <= 13)) {
			return $number . 'th';
		} else {
			return $number . $ends[$number % 10];
		}
	}

	public function test_next_recommend(){
		$next_recommendation = $this->get_next_recommendation(3, 1);
		//var_dump($next_recommendation);
	}

	public function deadline_to_recommend(){
		$dateTime = $this->get_current_date_time();
		$dead_recommend_sequential = $this->qry_deadline_recommend_sequential($dateTime['dateTime']);
		$dead_recommend_parallel = $this->qry_parallel_ir($dateTime['dateTime']);
		$set_to_missed = [];
		$set_to_missed_parallel = [];
		$set_recommend_ir_to_missed = [];
		$set_recommend_ir_to_missed_parallel = [];
		$set_next_recommendation_current = [];
		$set_next_recommendation_current_parallel = [];
		$paraIndexCount = 0;
		// // var_dump($dead_recommend);
		// var_dump($dead_recommend_parallel);
		if(count($dead_recommend_sequential) > 0){

			foreach($dead_recommend_sequential as $index => $row){
				$ir = $this->get_ir($row->incidentReport_ID);
				$missed_emp_details = $this->get_emp_details_via_emp_id($row->emp_id);
			
				$set_to_missed[$index] = [ //set deadline to missed
					'dmsDeadline_ID' => $row->dmsDeadline_ID,
					'status_ID' => 12
				];

				$set_recommend_ir_to_missed[$index] = [ //set recommendation to missed
					'incidentReportRecommendation_ID' => $row->record_ID,
					'liabilityStat_ID' => 12,
					'datedLiabilityStat' => $dateTime['dateTime']
				];
				// NOTIFY MISSED RECOMMENDER
				$mssg =  "You have missed to give recommendation to the incident report of " .  ucwords($ir->subjectFname . " " . $ir->subjectLname)  . " before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = 'discipline/dms_supervision/irSupervision/irSup_irInvolveTab';
				$this->simple_system_notification($mssg, $link, $missed_emp_details->uid, 'dms');
				$next_recommendation = $this->get_next_recommendation($row->incidentReport_ID, $row->level + 1);
				if(count($next_recommendation) > 0){
					// notify next recommender
					$next_emp_details = $this->get_emp_details_via_emp_id($next_recommendation->emp_ID);
					if ($next_recommendation->incidentReportRecommendationType_ID == 1) {
						$next_mssg = "The IR of <b>". ucwords($ir->subjectFname . " " . $ir->subjectLname) ."</b> is automatically forwarded to you for <b>"  . $this->ordinal($next_recommendation->level) . " Level Recommendation</b> because <b>" . ucwords($missed_emp_details->fname  . " " . $missed_emp_details->lname) . "</b> (". $this->ordinal($row->level). " Level) missed to give recommendation before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
						$next_link = "discipline/dms_supervision/irSupervision/irSup_recommendation";
					}else{
						$next_mssg = "The IR of <b>" . ucwords($ir->subjectFname . " " . $ir->subjectLname) . "</b> is automatically forwarded to you for <b>Last Recommendation</b> because <b>" . ucwords($missed_emp_details->fname . " " . $missed_emp_details->lname ) . "</b> (" . $this->ordinal($row->level) . " Level) missed to give recommendation before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
						$next_link = "discipline/dms_ero/eroRecommendationTab";
					}
					$this->simple_system_notification($next_mssg, $next_link, $next_emp_details->uid, 'dms');
					$set_next_recommendation_current[$index] = [// set next recommendation to current
						'incidentReportRecommendation_ID' => $next_recommendation->incidentReportRecommendation_ID,
						'liabilityStat_ID' => 4
					];
				}
			}
		}
		
		if(count($dead_recommend_parallel) > 0){
			$recommenders_count = 0;
			foreach ($dead_recommend_parallel as $ParaMainIndex => $ParaMainRow) {
				$ir_parallel = $this->get_ir($ParaMainRow->incidentReport_ID); //get ir details of parallel irs that have missed recommenders
				// IR that are Parallel
				$parallel_deadlines = $this->qry_deadline_recommend_parallel($ParaMainRow->incidentReport_ID, $dateTime['dateTime']);
				if(count($parallel_deadlines) > 0){
					foreach($parallel_deadlines as $indexParallel => $rowParallel){ // recommenders involved in the Parallel IR
						$parallel_missed_emp_details = $this->get_emp_details_via_emp_id($rowParallel->emp_id);
						$parallel_missed_mssg = "You have missed to give recommendation to the incident report of " .  ucwords($ir_parallel->subjectFname . " " . $ir_parallel->subjectLname)  . " before " . date("g:i A", strtotime($rowParallel->deadline)) . " last " . date("M j, Y", strtotime($rowParallel->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($ParaMainRow->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
						$parallel_missed_link = 'discipline/discipline/dms_supervision/irSupervision/irSup_recommendation';
						$this->simple_system_notification($parallel_missed_mssg, $parallel_missed_link, $parallel_missed_emp_details->uid, 'dms');
						$set_to_missed_parallel[$recommenders_count] = [
							'dmsDeadline_ID' => $rowParallel->dmsDeadline_ID,
							'status_ID' => 12
						];
						$set_recommend_ir_to_missed_parallel[$recommenders_count] = [
							'incidentReportRecommendation_ID' => $rowParallel->record_ID,
							'liabilityStat_ID' => 12
						];
						$recommenders_count++;
					}
				}
				$last_rec = $this->qry_last_recommender($ParaMainRow->incidentReport_ID);
				if(count($last_rec) > 0){
					$set_next_recommendation_current_parallel[$paraIndexCount] = [
						'incidentReportRecommendation_ID' => $last_rec->record_ID,
						'liabilityStat_ID' => 4
					];
					$parallel_next_emp_details = $this->get_emp_details_via_emp_id($last_rec->emp_id);
					$parallel_next_mssg = "The IR of <b>" . ucwords($ir_parallel->subjectFname . " " . $ir_parallel->subjectLname) . "</b> is automatically forwarded to you for <b>Last Recommendation</b> because some or all of the previous recommenders have failed to give their individual recommendation before " . date("g:i A", strtotime($ParaMainRow->deadline)) . " last " . date("M j, Y", strtotime($ParaMainRow->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($ParaMainRow->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
					$parallel_next_link = "discipline/dms_ero/eroRecommendationTab";
					$this->simple_system_notification($parallel_next_mssg, $parallel_next_link, $parallel_next_emp_details->uid, 'dms');
					$paraIndexCount;
				}
			}
		}

		// SEQUENTIAL ------------------------>
		// SET NEXT RECOMMENDATION TO CURRENT
		if (count( $set_next_recommendation_current) > 0) {
			$this->general_model->batch_update($set_next_recommendation_current, 'incidentReportRecommendation_ID', 'tbl_dms_incident_report_recommendation');
		}
		// SET DEADLINE TO MISSED
		if (count($set_to_missed) > 0) {
			$this->general_model->batch_update($set_to_missed, 'dmsDeadline_ID', 'tbl_dms_deadline');
		}
		// SET IR RECOMMENDATION TO MISSED
		if (count($set_recommend_ir_to_missed) > 0) {
			$this->general_model->batch_update($set_recommend_ir_to_missed, 'incidentReportRecommendation_ID', 'tbl_dms_incident_report_recommendation');
		}

		// PARALLEL ------------------------>

		// SET DEADLINE TO MISSED PARALLEL
		if (count($set_to_missed_parallel) > 0) {
			$this->general_model->batch_update($set_to_missed_parallel, 'dmsDeadline_ID', 'tbl_dms_deadline');
		}
		// SET IR RECOMMENDATION TO MISSED PARALLEL
		if (count( $set_recommend_ir_to_missed_parallel) > 0) {
			$this->general_model->batch_update($set_recommend_ir_to_missed_parallel, 'incidentReportRecommendation_ID', 'tbl_dms_incident_report_recommendation');
		}
		// SET ERO LAST RECOMMENDATION TO CURRENT
		if (count($set_next_recommendation_current_parallel) > 0) {
			$this->general_model->batch_update($set_next_recommendation_current_parallel, 'incidentReportRecommendation_ID', 'tbl_dms_incident_report_recommendation');
		}
		// var_dump( $set_to_missed);
		// var_dump( $missed_recipient_parallel);
		// var_dump($set_to_missed_parallel);
		// var_dump($transfer_notif_parallel);
			// var_dump( $set_missed_recipient);
			// var_dump( $set_recommend_ir_to_missed);
			// var_dump( $notify_transfer);
			// var_dump( $notify_transfer_recipient);
			// var_dump( $set_next_recommendation_current);
			// var_dump( $set_next_recommendation_current_parallel);
			// var_dump( $notify_transfer_parallel);
			// var_dump( $notify_transfer_recipient_parallel);
	}

	public function deadline_to_recommend_test(){
		$dateTime = $this->get_current_date_time();
		$dead_recommend_sequential = $this->qry_deadline_recommend_sequential($dateTime['dateTime']);
		$dead_recommend_parallel = $this->qry_parallel_ir($dateTime['dateTime']);
		// var_dump($dead_recommend_sequential);
		$set_to_missed = [];
		$set_to_missed_parallel = [];
		$set_recommend_ir_to_missed = [];
		$set_recommend_ir_to_missed_parallel = [];
		$set_next_recommendation_current = [];
		$set_next_recommendation_current_parallel = [];
		$paraIndexCount = 0;
		// // // var_dump($dead_recommend);
		// // var_dump($dead_recommend_parallel);
		if(count($dead_recommend_sequential) > 0){

			foreach($dead_recommend_sequential as $index => $row){
				var_dump('row here --------------');
				var_dump($row);
				var_dump('IR here --------------');
				$ir = $this->get_ir($row->incidentReport_ID);
				var_dump($ir);
				var_dump($ir->subjectFname);
				$missed_emp_details = $this->get_emp_details_via_emp_id($row->emp_id);
				var_dump($missed_emp_details);
				$set_to_missed[$index] = [ //set deadline to missed
					'dmsDeadline_ID' => $row->dmsDeadline_ID,
					'status_ID' => 12
				];
				$set_recommend_ir_to_missed[$index] = [ //set recommendation to missed
					'incidentReportRecommendation_ID' => $row->record_ID,
					'liabilityStat_ID' => 12,
					'datedLiabilityStat' => $dateTime['dateTime']
				];
				// NOTIFY MISSED RECOMMENDER
				$mssg =  "You have missed to give recommendation to the incident report of " .  ucwords($ir->subjectFname . " " . $ir->subjectLname)  . " before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = 'discipline/dms_supervision/irSupervision/irSup_irInvolveTab';
				// $this->simple_system_notification($mssg, $link, $missed_emp_details->uid, 'dms');
				$next_recommendation = $this->get_next_recommendation($row->incidentReport_ID, $row->level + 1);
				var_dump($next_recommendation);
				if(count($next_recommendation) > 0){
					// notify next recommender
					$next_emp_details = $this->get_emp_details_via_emp_id($next_recommendation->emp_ID);
					if ($next_recommendation->incidentReportRecommendationType_ID == 1) {
						var_dump('fname: '.$ir->subjectFname);
						var_dump('lname: '.$ir->subjectLname);
						var_dump('rec_level: '.$next_recommendation->level);
						var_dump('missed emp fname: '.$missed_emp_details->fname);
						var_dump('missed emp lname: '.$missed_emp_details->lname);
						var_dump('missed emp level: '.$row->level);
						var_dump('missed emp deadline: '.$row->deadline);
						var_dump('missed ir: '.$row->incidentReport_ID);
						$next_mssg = "The IR of <b>". ucwords($ir->subjectFname . " " . $ir->subjectLname) ."</b> is automatically forwarded to you for <b>"  . $this->ordinal($next_recommendation->level) . " Level Recommendation</b> because <b>" . ucwords($missed_emp_details->fname  . " " . $missed_emp_details->lname) . "</b> (". $this->ordinal($row->level). " Level) missed to give recommendation before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
						$next_link = "discipline/dms_supervision/irSupervision/irSup_recommendation";
					}else{
						$next_mssg = "The IR of <b>" . ucwords($ir->subjectFname . " " . $ir->subjectLname) . "</b> is automatically forwarded to you for <b>Last Recommendation</b> because <b>" . ucwords($missed_emp_details->fname . " " . $missed_emp_details->lname ) . "</b> (" . $this->ordinal($row->level) . " Level) missed to give recommendation before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
						$next_link = "discipline/dms_ero/eroRecommendationTab";
					}
					// $this->simple_system_notification($next_mssg, $next_link, $next_emp_details->uid, 'dms');
					$set_next_recommendation_current[$index] = [// set next recommendation to current
						'incidentReportRecommendation_ID' => $next_recommendation->incidentReportRecommendation_ID,
						'liabilityStat_ID' => 4
					];
				}
			}
		}
		
		// if(count($dead_recommend_parallel) > 0){
		// 	$recommenders_count = 0;
		// 	foreach ($dead_recommend_parallel as $ParaMainIndex => $ParaMainRow) {
		// 		$ir_parallel = $this->get_ir($ParaMainRow->incidentReport_ID); //get ir details of parallel irs that have missed recommenders
		// 		// IR that are Parallel
		// 		$parallel_deadlines = $this->qry_deadline_recommend_parallel($ParaMainRow->incidentReport_ID, $dateTime['dateTime']);
		// 		if(count($parallel_deadlines) > 0){
		// 			foreach($parallel_deadlines as $indexParallel => $rowParallel){ // recommenders involved in the Parallel IR
		// 				$parallel_missed_emp_details = $this->get_emp_details_via_emp_id($rowParallel->emp_id);
		// 				$parallel_missed_mssg = "You have missed to give recommendation to the incident report of " .  ucwords($ir_parallel->subjectFname . " " . $ir_parallel->subjectLname)  . " before " . date("g:i A", strtotime($rowParallel->deadline)) . " last " . date("M j, Y", strtotime($rowParallel->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($ParaMainRow->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
		// 				$parallel_missed_link = 'discipline/discipline/dms_supervision/irSupervision/irSup_recommendation';
		// 				$this->simple_system_notification($parallel_missed_mssg, $parallel_missed_link, $parallel_missed_emp_details->uid, 'dms');
		// 				$set_to_missed_parallel[$recommenders_count] = [
		// 					'dmsDeadline_ID' => $rowParallel->dmsDeadline_ID,
		// 					'status_ID' => 12
		// 				];
		// 				$set_recommend_ir_to_missed_parallel[$recommenders_count] = [
		// 					'incidentReportRecommendation_ID' => $rowParallel->record_ID,
		// 					'liabilityStat_ID' => 12
		// 				];
		// 				$recommenders_count++;
		// 			}
		// 		}
		// 		$last_rec = $this->qry_last_recommender($ParaMainRow->incidentReport_ID);
		// 		if(count($last_rec) > 0){
		// 			$set_next_recommendation_current_parallel[$paraIndexCount] = [
		// 				'incidentReportRecommendation_ID' => $last_rec->record_ID,
		// 				'liabilityStat_ID' => 4
		// 			];
		// 			$parallel_next_emp_details = $this->get_emp_details_via_emp_id($last_rec->emp_id);
		// 			$parallel_next_mssg = "The IR of <b>" . ucwords($ir_parallel->subjectFname . " " . $ir_parallel->subjectLname) . "</b> is automatically forwarded to you for <b>Last Recommendation</b> because some or all of the previous recommenders have failed to give their individual recommendation before " . date("g:i A", strtotime($ParaMainRow->deadline)) . " last " . date("M j, Y", strtotime($ParaMainRow->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($ParaMainRow->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
		// 			$parallel_next_link = "discipline/dms_ero/eroRecommendationTab";
		// 			$this->simple_system_notification($parallel_next_mssg, $parallel_next_link, $parallel_next_emp_details->uid, 'dms');
		// 			$paraIndexCount;
		// 		}
		// 	}
		// }

		// SEQUENTIAL ------------------------>
		// SET NEXT RECOMMENDATION TO CURRENT
		if (count( $set_next_recommendation_current) > 0) {
			var_dump($set_next_recommendation_current);
		}
		// SET DEADLINE TO MISSED
		if (count($set_to_missed) > 0) {
			var_dump($set_to_missed);
		}
		// SET IR RECOMMENDATION TO MISSED
		if (count($set_recommend_ir_to_missed) > 0) {
			var_dump($set_recommend_ir_to_missed);
		}

		// // PARALLEL ------------------------>

		// // SET DEADLINE TO MISSED PARALLEL
		// if (count($set_to_missed_parallel) > 0) {
		// 	$this->general_model->batch_update($set_to_missed_parallel, 'dmsDeadline_ID', 'tbl_dms_deadline');
		// }
		// // SET IR RECOMMENDATION TO MISSED PARALLEL
		// if (count( $set_recommend_ir_to_missed_parallel) > 0) {
		// 	$this->general_model->batch_update($set_recommend_ir_to_missed_parallel, 'incidentReportRecommendation_ID', 'tbl_dms_incident_report_recommendation');
		// }
		// // SET ERO LAST RECOMMENDATION TO CURRENT
		// if (count($set_next_recommendation_current_parallel) > 0) {
		// 	$this->general_model->batch_update($set_next_recommendation_current_parallel, 'incidentReportRecommendation_ID', 'tbl_dms_incident_report_recommendation');
		// }
		// // var_dump( $set_to_missed);
		// // var_dump( $missed_recipient_parallel);
		// // var_dump($set_to_missed_parallel);
		// // var_dump($transfer_notif_parallel);
		// 	// var_dump( $set_missed_recipient);
		// 	// var_dump( $set_recommend_ir_to_missed);
		// 	// var_dump( $notify_transfer);
		// 	// var_dump( $notify_transfer_recipient);
		// 	// var_dump( $set_next_recommendation_current);
		// 	// var_dump( $set_next_recommendation_current_parallel);
		// 	// var_dump( $notify_transfer_parallel);
		// 	// var_dump( $notify_transfer_recipient_parallel);
	}

	private function qry_deadline_ero_recommendation($dateTime, $deadType){
		$fields = "dead.dmsDeadline_ID, dead.record_ID, dead.emp_id, dead.deadline, dead.dmsDeadlineType_ID, irRec.incidentReport_ID, irRec.level";
		$where = "irRec.incidentReportRecommendation_ID = dead.record_ID AND dead.dmsDeadlineType_ID = $deadType AND dead.status_ID = 2 AND dead.deadline <= ' $dateTime'";
		$table = "tbl_dms_deadline dead, tbl_dms_incident_report_recommendation irRec";
		return  $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	private function qry_deadline_necessary_doc($dateTime){
		$fields = "dead.dmsDeadline_ID, dead.record_ID, dead.emp_id, dead.deadline, dead.dmsDeadlineType_ID, doc.incidentReport_ID";
		$where = "doc.irNecessaryDocument_ID = dead.record_ID AND doc.eroEmp_ID = dead.emp_id AND dead.dmsDeadlineType_ID = 7 AND dead.status_ID = 2 AND dead.deadline <= ' $dateTime'";
		$table = "tbl_dms_deadline dead, tbl_dms_necessary_document doc";
		return  $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	public function check_ero_recommendation_deadline($dateTime, $deadType)
	{
		$dead_last_rec = $this->qry_deadline_ero_recommendation($dateTime, $deadType);
		$set_to_missed = [];
		$record_missed_ero = [];
		if (count($dead_last_rec) > 0) {
			foreach ($dead_last_rec as $index => $row) {
				$ir = $this->get_ir($row->incidentReport_ID);
				$missed_emp_details = $this->get_emp_details_via_emp_id($row->emp_id);
				$rec_type = "Last";
				if ($deadType == 5) {
					$rec_type = "Final";
				}
				$mssg = "You have missed to give $rec_type Recommendation to the incident report of " .  ucwords($ir->subjectFname . " " . $ir->subjectLname)  . " before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = "discipline/dms_ero/eroRecommendationTab";
				$set_to_missed[$index] = [
					'dmsDeadline_ID' => $row->dmsDeadline_ID,
					'status_ID' => 12
				];
				$record_missed_ero[$index] = [
					'eroEmp_ID' => $row->emp_id,
					'dmsDeadline_ID' => $row->dmsDeadline_ID
				];
				// var_dump($mssg);
				// var_dump($link);
				$this->simple_system_notification($mssg, $link, $missed_emp_details->uid, 'dms');
			}
			// SET TO MISSED
			if (count($set_to_missed) > 0) {
				$this->general_model->batch_update($set_to_missed, 'dmsDeadline_ID', 'tbl_dms_deadline');
			}
			// RECORD MISSED ERO
			if (count($record_missed_ero) > 0) {
				$this->general_model->batch_insert($record_missed_ero, 'tbl_dms_ero_missed_deadlines');
			}
		}
		// var_dump($set_to_missed);
		// var_dump($record_missed_ero);
		// var_dump($notify_missed_ero);
		// var_dump($set_missed_recipient);
		// var_dump($set_to_missed);
		// var_dump($record_missed_ero);
		// var_dump($notif_missed_ero);
		// notify missed ero
		// set deadline to missed
		// record ero missed deadline
		// var_dump($dead_last_rec);
	}

	public function check_necessary_doc_deadline($dateTime)
	{
		$dead_necessary_doc = $this->qry_deadline_necessary_doc($dateTime);
		$set_to_missed = [];
		$record_missed_ero = [];
		// var_dump($dead_necessary_doc);
		if (count($dead_necessary_doc) > 0) {
			foreach ($dead_necessary_doc as $index => $row) {
				$ir = $this->get_ir($row->incidentReport_ID);
				$missed_emp_details = $this->get_emp_details_via_emp_id($row->emp_id);
				$mssg = "You have missed to <b>Upload the Necessary Document</b> to the incident report of <b>" .  ucwords($ir->subjectFname . " " . $ir->subjectLname)  . "</b> before " . date("g:i A", strtotime($row->deadline)) . " last " . date("M j, Y", strtotime($row->deadline)) . ". </b><br><small><b>IR-ID</b>: " . str_pad($row->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = "discipline/dms_ero/eroRecommendationTab";
				$this->simple_system_notification($mssg, $link, $missed_emp_details->uid);
				$set_to_missed[$index] = [
					'dmsDeadline_ID' => $row->dmsDeadline_ID,
					'status_ID' => 12
				];
				$record_missed_ero[$index] = [
					'eroEmp_ID' => $row->emp_id,
					'dmsDeadline_ID' => $row->dmsDeadline_ID
				];
			}
			// SET TO MISSED
			if (count($set_to_missed) > 0) {
				$this->general_model->batch_update($set_to_missed, 'dmsDeadline_ID', 'tbl_dms_deadline');
			}
			// RECORD MISSED ERO
			if (count($record_missed_ero) > 0) {
				$this->general_model->batch_insert($record_missed_ero, 'tbl_dms_ero_missed_deadlines');
			}
		}
	}

	public function deadline_check_ero(){
		$dateTime = $this->get_current_date_time();
		// deadline last recommendation
		$this->check_ero_recommendation_deadline($dateTime['dateTime'], 4);
		// deadline final recommmendation
		$this->check_ero_recommendation_deadline($dateTime['dateTime'], 5);
		// deadline to upload necessary documents
		$this->check_necessary_doc_deadline($dateTime);
	}

	public function check_dms_deadline(){
		$this->deadline_to_explain();
		$this->deadline_to_recommend();
		$this->deadline_check_ero();
		$this->deadline_to_file_qualified();
		$this->set_past_cure_dates_to_completed();
	}

	// SET RECOMMENDERS
	protected function get_ir($ir_id)
	{
		$fields = "ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID soureEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label";
		$where = "offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.incidentReport_ID = $ir_id";
		$table = "tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin";
		return  $this->general_model->fetch_specific_val($fields,  $where,  $table);
	}

	private function get_account_recommendation_num_via_account($account_id)
	{
		$fields = "accountRecommendationNumber_ID, account_ID, number, dateCreated, dateChanged, changedBy";
		$where = "account_ID = $account_id";
		return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_account_recommendation_number");
	}

	private function default_recommendation_number()
	{
		return 2;
	}

	private function get_specific_exempt_via_account($acc_id)
	{
		$fields = "level, emp_id";
		$where = "account_ID =  $acc_id AND changeType = 'exempt'";
		return  $this->general_model->fetch_specific_vals($fields,  $where, 'tbl_dms_custom_recommendation');
	}

	private function get_specific_reassignment_via_account($acc_id)
	{
		$fields = "cusRec.level, cusRec.emp_id, change.emp_id as assignTo";
		$where = "change.dmsCustomRecommendation_ID = cusRec.dmsCustomRecommendation_ID AND cusRec.account_ID =  $acc_id AND cusRec.changeType = 'change'";
		$tables = "tbl_dms_custom_recommendation cusRec, tbl_dms_custom_recommendation_change change";
		return  $this->general_model->fetch_specific_vals($fields,  $where,  $tables);
	}

	private function filter_exemption_or_reassignment($supervisors,  $acc_id,  $subjectEmp_ID,  $site_id)
	{
		$recommenders =  $supervisors['supervisors'];
		$exemption =  $this->get_specific_exempt_via_account($acc_id);
		$reassign =  $this->get_specific_reassignment_via_account($acc_id, 'change');
		$ero =  $this->identify_assigned_ero($acc_id,  $site_id);
		for ($sup_loop = 0; $sup_loop < count($recommenders); $sup_loop++) {
			if ($recommenders[$sup_loop]['emp_id'] ==  $ero) { //check if ero is one of the normal recommendation
				unset($recommenders[$sup_loop]);
			} else if (count($exemption) > 0) { // check exemption
				$level =  $sup_loop + 1;
				$emp_id =  $recommenders[$sup_loop]['emp_id'];
				$remove_sup = array_filter(
					$exemption,
					function ($e) use ($level,  $emp_id) {
						return (($e->level ==  $level) && ($e->emp_id ==  $emp_id));
					}
				);
				if (count($remove_sup) > 0) {
					// echo "remove sup  $sup_loop <br>";
					unset($recommenders[$sup_loop]);
				} else {
					// echo "reassign sup  $sup_loop <br>";
					if (count($reassign) > 0) {
						$level =  $sup_loop + 1;
						$emp_id =  $recommenders[$sup_loop]['emp_id'];
						$reassign_sup = array_filter(
							$reassign,
							function ($e) use ($level,  $emp_id) {
								return (($e->level ==  $level) && ($e->emp_id ==  $emp_id));
							}
						);
						$reassign_sup = array_merge($reassign_sup);
						if (count($reassign_sup) > 0) { // check if reassign sup
							if ($reassign_sup[0]->assignTo !=  $subjectEmp_ID) {
								$recommenders[$sup_loop]['emp_id'] =  $reassign_sup[0]->assignTo;
							}
						}
					}
				}
			}
		}
		return  $recommenders;
	}

	private function default_ero_sup()
	{
		return 522;
	}

	private function get_ero_supervisor($emp_id)
	{
		$fields = "employeeRelationsSupervisor";
		$where = "emp_id =  $emp_id";
		return  $this->general_model->fetch_specific_val($fields,  $where, "tbl_dms_employee_relations_supervisor");
	}

	private function check_if_ero_is_subject($acc_id, $site_id, $subjectEmp_ID)
	{
		$ero = $this->identify_assigned_ero($acc_id, $site_id);
		if ($ero == $subjectEmp_ID) {
			$ero_sup = $this->get_ero_supervisor($ero);
			if (count($ero_sup) > 0) {
				$ero = $ero_sup->employeeRelationsSupervisor;
			} else {
				$ero = $this->default_ero_sup();
			}
		}
		return $ero;
	}

	private function set_ir_recommendation($recommenders,  $ir,  $acc_id,  $site_id)
	{
		$ir_recommendation = [];
		$filtered_recommendation = [];
		$rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
		$recommendationFlow = (int) $rec_deadline['record']->deadlineToRecommendOption;
		$assigned_ero =  $this->check_if_ero_is_subject($acc_id,  $site_id,  $ir->subjectEmp_ID);
		$status = 4;
		for ($loopRec = 0; $loopRec < count($recommenders); $loopRec++) {
			$ir_recommendation[$loopRec] = (object) [
				'incidentReport_ID' =>  $ir->incidentReport_ID,
				'emp_id' =>  $recommenders[$loopRec]['emp_id'],
				// 'level' =>  $loopRec + 1,
				'disciplinaryActionCategory_ID' =>  $ir->disciplinaryActionCategory_ID,
				'incidentReportRecommendationType_ID' => 1,
			];
		}
		$filtered_recommender_by_ero = array_filter(
			$ir_recommendation,
			function ($e) use ($assigned_ero) {
				return ($e->emp_id !==  $assigned_ero);
			}
		);
		$final_normal_recommenders = array_merge($filtered_recommender_by_ero);
		$final_normal_recommenders[count($final_normal_recommenders)] = (object) [
			'incidentReport_ID' =>  $ir->incidentReport_ID,
			'emp_id' =>  $assigned_ero,
			'liabilityStat_ID' => 2,
			'disciplinaryActionCategory_ID' =>  $ir->disciplinaryActionCategory_ID,
			'incidentReportRecommendationType_ID' => 2,
		];
		foreach ($final_normal_recommenders as $index => $row) {
			if ($index + 1 == count($final_normal_recommenders)) {
				$status = 2;
			} else if ($index > 0 &&  $recommendationFlow == 1) {
				$status = 2;
			}
			$row->{'liabilityStat_ID'} = $status;
			$row->{'level'} = $index + 1;
			$filtered_recommendation[$index] = $row;
		}
		$data['recommenders'] =  $filtered_recommendation;
		$data['first_id'] =  $this->general_model->batch_insert_first_inserted_id($filtered_recommendation, 'tbl_dms_incident_report_recommendation');
		return  $data;
	}

	private function set_recommmendation_deadline($recommenders,  $ir,  $first_id)
	{
		$rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
		$recommendationFlow = (int) $rec_deadline['record']->deadlineToRecommendOption;
		$days_recommend = (int) $rec_deadline['record']->deadlineToRecommend;
		$days_last_recommend = (int) $rec_deadline['record']->deadlineLastRecommendation;
		$start_date =  $ir->dateTimeFiled;
		$deadline_type = 3;
		for ($loopRec = 0; $loopRec < count($recommenders); $loopRec++) {
			if ($recommenders[$loopRec]->incidentReportRecommendationType_ID == 1) {
				$deadline_type = 3;
				$recommend_date = Date("Y-m-d", strtotime($start_date . "  $days_recommend days"));
				$deadline_to_recommend =  $recommend_date . " 23:59:59";
				if ($recommendationFlow == 1) {
					$start_date =  $recommend_date;
				}
			} else {
				$deadline_type = 4;
				if ($recommendationFlow == 1) {
					$recommend_date = Date("Y-m-d", strtotime($start_date . " $days_last_recommend days"));
					$deadline_to_recommend =  $recommend_date . " 23:59:59";
				} else {
					$rec_days =  $days_recommend +  $days_last_recommend;
					$recommend_date = Date("Y-m-d", strtotime($start_date . " $rec_days days"));
					$deadline_to_recommend =  $recommend_date . " 23:59:59";
				}
			}
			$ir_recommendation_deadline[$loopRec] = [
				'record_ID' =>  $first_id +  $loopRec,
				'emp_id' =>  $recommenders[$loopRec]->emp_id,
				'deadline' =>  $deadline_to_recommend,
				'dmsDeadlineType_ID' =>  $deadline_type,
				'status_ID' => 2,
				'level' =>  $loopRec + 1,
			];
		}
		return  $this->general_model->batch_insert($ir_recommendation_deadline, 'tbl_dms_deadline');
	}

	public function simple_system_notification($notif_mssg, $link, $receiver_uid, $reference = 'general')
	{
		$systemNotification_ID = $this->general_model->insert_vals_last_inserted_id(['message' => $notif_mssg, 'link' => $link, 'reference' => $reference], "tbl_system_notification");
		return $this->general_model->insert_vals_last_inserted_id(['systemNotification_ID' => $systemNotification_ID, 'recipient_ID' => $receiver_uid, 'status_ID' => 8], "tbl_system_notification_recipient");
	}

	private function notify_by_flow($recommenders, $incident_report)
	{
		$normal_recommenders = [];
		$normal_recommenders_count = 0;
		$rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
		$recommendationFlow = (int) $rec_deadline['record']->deadlineToRecommendOption;
		if ((int) $recommendationFlow == 1) {
			$emp_details = $this->get_emp_details_via_emp_id($recommenders[0]->emp_id);
			if ($recommenders[0]->incidentReportRecommendationType_ID == 2) {
				$notif_mssg = "You are notified to give <b>Last Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = "discipline/dms_ero";
			} else {
				$notif_mssg = "You are notified to give <b>1st Level Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = "discipline/dms_supervision/irSupervision/irSup_recommendation";
			}
			$data['notif_id'] = $this->simple_system_notification($notif_mssg, $link, $emp_details->uid);
		} else {
			foreach ($recommenders as $recommenders_rows) {
				if ($recommenders_rows->incidentReportRecommendationType_ID == 1) {
					$emp_details = $this->get_emp_details_via_emp_id($recommenders_rows->emp_id);
					$normal_recommenders[$normal_recommenders_count] = [
						"userId" => $emp_details->uid
					];
					$normal_recommenders_count++;
				}
			}
			if (count($normal_recommenders) > 0) {
				$notif_mssg = "You are notified to give <b>Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = "discipline/dms_supervision/irSupervision/irSup_recommendation";
				$notif_id[0] = $this->create_system_notif($notif_mssg, $link);
				$data['notif_id'] = $notif_id;
				$data['send_stat'] = $this->send_system_notif($notif_id[0], $normal_recommenders);
			} else {
				$ero_details = $this->get_emp_details_via_emp_id($recommenders[0]->emp_id);
				$notif_mssg = "You are notified to give <b>Last Recommendation</b> to the Incident Report of <b>" . ucwords($incident_report->subjectFname . " " . $incident_report->subjectLname) . ". </b><br><small><b>IR-ID</b>: " . str_pad($incident_report->incidentReport_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = "discipline/dms_ero";
				$data['notif_id'] = $this->simple_system_notification($notif_mssg, $link, $ero_details->uid);
			}
		}
		return $data;
	}

	public function get_breaks_without_logout($login_id, $dated_shift_out, $emp_id)
	{
		$fields = "dtr.dtr_id, dtr.emp_id, dtr.acc_time_id, dtr.entry, dtr.log, dtr.note, dtr.type, dtr.sched_id, break.break_type, bt.hour, bt.min";
		$where = "break.break_id = bta.break_id AND bt.btime_id = bta.btime_id AND bta.bta_id = shiftBreak.bta_id AND shiftBreak.sbrk_id = dtr.acc_time_id AND dtr.dtr_id > $login_id AND type = 'Break' AND dtr.log <= '$dated_shift_out' AND dtr.emp_id = $emp_id";
		$table = "tbl_dtr_logs dtr, tbl_shift_break shiftBreak, tbl_break_time_account bta, tbl_break_time bt, tbl_break break";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'dtr_id ASC');
	}

	public function get_allowable_break($account_id, $sched_id)
	{
		$fields = "accTime.acc_time_id, break.break_type, breakTime.hour, breakTime.min";
		$where = "breakTime.btime_id = accountBreak.btime_id AND break.break_id = accountBreak.break_id AND accountBreak.bta_id = shiftBreak.bta_id AND times.time_id = accTime.time_id AND shiftBreak.isActive = 1 AND shiftBreak.acc_time_id = accTime.acc_time_id AND  accTime.acc_time_id = sched.acc_time_id  AND acc_id =$account_id AND sched.sched_id = $sched_id";
		$tables = "tbl_acc_time accTime, tbl_shift_break shiftBreak, tbl_break_time_account accountBreak, tbl_break_time breakTime, tbl_break break, tbl_time times, tbl_schedule sched";
		$breaks = $this->general_model->fetch_specific_vals($fields, $where, $tables);
		$hours = 0;
		$minutes = 0;
		if (count($breaks) > 0) {
			foreach ($breaks as $break_val) {
				$hours += $break_val->hour;
				$minutes += $break_val->min;
			}
			$hours_to_min = $hours * 60;
			$minutes += $hours_to_min;
			$break['breakData'] = $breaks;
			$break['minutes'] = $minutes;
		} else {
			$break['breakData'] = 0;
			$break['minutes'] = 0;
		}
		return $break;
	}

	public function get_next_dtr_log($login_id, $emp_id){
		// $fields = "dtr_id, emp_id, acc_time_id, entry, log, note, type, sched_id";
		// $where = "dtr_id > $login_id AND type = 'DTR' AND emp_id= $emp_id";
		// return $this->general_model->fetch_specific_val($fields, $where, 'tbl_dtr_logs', 'log ASC LIMIT 1');
		$query = "SELECT dtr_id, emp_id, acc_time_id, entry, log, note, type, sched_id FROM tbl_dtr_logs WHERE dtr_id > $login_id AND type = 'DTR' AND emp_id= $emp_id ORDER BY dtr_id ASC LIMIT 1";
		return $this->general_model->custom_query($query);
	}

	public function check_start_end_shift($date_time_start, $date_time_end)
	{
		if (strtotime($date_time_end) < strtotime($date_time_start)) {
			$date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' 1 days'));
			$date_time_start = $date_time_start;
		}
		$shift_start_date_time = new DateTime($date_time_start);
		$formatted_shift_start = $shift_start_date_time->format("H:i");
		if ($formatted_shift_start == "00:00") {
			$date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' 1 days'));
			$date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
		}
		$new_date_shift = [
			'start' => $date_time_start,
			'end' => $date_time_end,
		];
		return $new_date_shift;
	}

	public function set_current_recommendation_option($ir_id)
	{
		$rec_deadline =  $this->get_ir_deadline_settings_details_recorded();
		$recommendationFlow = (int) $rec_deadline['record']->deadlineToRecommendOption;
		$datasched['deadlineToRecommendOption'] = $recommendationFlow;
		$where = "incidentReport_ID = $ir_id";
		$this->general_model->update_vals($datasched, $where, 'tbl_dms_incident_report');
	}

	public function set_recommenders($ir_id)
	{
		$data['error'] = 0;
		$data['error_details'] = "";
		$ir = $this->get_ir($ir_id);
		// UPDATE ir reccommendation option
		$this->set_current_recommendation_option($ir_id);
		$exempt = [];
		$rec_num = 0;
		if (count($ir) > 0) {
			$emp_details =  $this->get_emp_details_via_emp_id($ir->subjectEmp_ID);
			// get recommendation number
			$acc_rec_num =  $this->get_account_recommendation_num_via_account($emp_details->acc_id);
			// get exemptions             
			if (count($acc_rec_num) > 0) {
				$rec_num =  $acc_rec_num->number;
			} else {
				$rec_num =  $this->default_recommendation_number();
			}
			$supervisors = $this->get_supervisors($ir->subjectEmp_ID, $rec_num);
			if ($supervisors['error'] != 1) {
				$data['recommenders'] = $this->filter_exemption_or_reassignment($supervisors, $emp_details->acc_id, $ir->subjectEmp_ID, $emp_details->site_ID);
				$data['set_recommendation'] = $this->set_ir_recommendation($data['recommenders'], $ir, $emp_details->acc_id, $emp_details->site_ID);
				$data['set_deadline'] = $this->set_recommmendation_deadline($data['set_recommendation']['recommenders'], $ir, $data['set_recommendation']['first_id']);
				$data['notification'] = $this->notify_by_flow($data['set_recommendation']['recommenders'], $ir);
			} else {
				$data['error'] = 1;
				$data['error_details'] =  $supervisors['error_details'];
			}
		} else {
			$data['error'] = 1;
			$data['error_details'] = "IR Does not exist";
		}
		return $data;
	}
	// DMS Michael Sanchez Codes End here ---->connection_status
	// DMS MICHAEL UPDATED SERVER AUTOMATIC CHECKING OF DTR VIOLATIONS
	private function get_all_sched_for_checking($date_from, $date_to){
		$fields = "sched.*, times.*, users.uid, emp.Supervisor, emp.isFlexiSched";
		$where = "users.emp_id = emp.emp_id AND emp.isActive = 'yes' AND emp.emp_id = sched.emp_id AND sched.schedtype_id IN (1, 4, 5) and sched.isComplete = 0 AND times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND DATE(sched.sched_date) >= '$date_from' AND DATE (sched.sched_date) <= '$date_to'";
		$table = "tbl_schedule sched, tbl_acc_time accTime, tbl_time times, tbl_employee emp, tbl_user users";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'sched.sched_date ASC');
		//  return $this->general_model->custom_query("SELECT sched.*, times.*, users.uid, emp.Supervisor FROM tbl_schedule sched, tbl_acc_time accTime, tbl_time times, tbl_employee emp, tbl_user users where users.emp_id = emp.emp_id AND emp.isActive = 'yes' AND emp.emp_id = sched.emp_id AND sched.schedtype_id IN (1, 4, 5) and sched.isComplete = 0 AND times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND DATE(sched.sched_date) >= '$date_from' AND DATE (sched.sched_date) <= '$date_to' LIMIT 10");
	}
	private function get_all_sched_for_checking_test($date_from, $date_to, $emp_id){
		$fields = "sched.*, times.*, users.uid, emp.Supervisor, emp.isFlexiSched";
		$where = "users.emp_id = emp.emp_id AND emp.isActive = 'yes' AND emp.emp_id = sched.emp_id AND sched.schedtype_id IN (1, 4, 5) and sched.isComplete = 0 AND times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND DATE(sched.sched_date) >= '$date_from' AND DATE (sched.sched_date) <= '$date_to' AND sched.emp_id = $emp_id";
		$table = "tbl_schedule sched, tbl_acc_time accTime, tbl_time times, tbl_employee emp, tbl_user users";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'sched.sched_date ASC');
		//  return $this->general_model->custom_query("SELECT sched.*, times.*, users.uid, emp.Supervisor FROM tbl_schedule sched, tbl_acc_time accTime, tbl_time times, tbl_employee emp, tbl_user users where users.emp_id = emp.emp_id AND emp.isActive = 'yes' AND emp.emp_id = sched.emp_id AND sched.schedtype_id IN (1, 4, 5) and sched.isComplete = 0 AND times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND DATE(sched.sched_date) >= '$date_from' AND DATE (sched.sched_date) <= '$date_to' LIMIT 10");
	}

	private function get_all_dtr_logs($sched_ids){
		$fields = "logs.*, emp.isFlexiSched";
		$where = "emp.emp_id = logs.emp_id AND logs.sched_id IN ($sched_ids)";
		$table = "tbl_dtr_logs logs, tbl_employee emp";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'logs.dtr_id ASC');
		//  return $this->general_model->custom_query("SELECT * FROM tbl_dtr_logs where sched_id IN ($sched_ids) ORDER BY emp_id ASC LIMIT 10");
	}

	private function get_all_break_logs($date_from, $date_to){
		$fields = "dtr.dtr_id, dtr.emp_id, dtr.acc_time_id, dtr.entry, dtr.log, dtr.note, dtr.type, dtr.sched_id, break.break_type, bt.hour, bt.min";
		$where = "break.break_id = bta.break_id AND bt.btime_id = bta.btime_id AND bta.bta_id = shiftBreak.bta_id AND shiftBreak.sbrk_id = dtr.acc_time_id AND DATE(dtr.log) >= '$date_from' AND DATE (dtr.log) <= '$date_to' AND dtr.type = 'Break'";
		$table = "tbl_dtr_logs dtr, tbl_shift_break shiftBreak, tbl_break_time_account bta, tbl_break_time bt, tbl_break break";
		return $this->general_model->fetch_specific_vals($fields, $where, $table , 'dtr.dtr_id ASC');
	}

	private function get_all_allowable_breaks_per_sched($sched_ids){
		$fields = "sched.sched_id, sched.emp_id, accTime.acc_time_id, break.break_type, breakTime.hour, breakTime.min";
		$where = "breakTime.btime_id = accountBreak.btime_id AND break.break_id = accountBreak.break_id AND accountBreak.bta_id = shiftBreak.bta_id AND times.time_id = accTime.time_id AND shiftBreak.isActive = 1 AND shiftBreak.acc_time_id = accTime.acc_time_id AND  accTime.acc_time_id = sched.acc_time_id AND sched.sched_id IN ($sched_ids)";
		$tables = "tbl_acc_time accTime, tbl_shift_break shiftBreak, tbl_break_time_account accountBreak, tbl_break_time breakTime, tbl_break break, tbl_time times, tbl_schedule sched";
		return $this->general_model->fetch_specific_vals($fields, $where, $tables);
	}

	protected function set_sched_date_to_complete($sched_is_complete){
		$this->general_model->batch_update($sched_is_complete, 'sched_id', 'tbl_schedule');
	}
	public function checkAllLogsIssue_disabled(){
		date_default_timezone_set("Asia/Manila");
		$date_from = "2021-03-15";
		$date_to = "2021-03-16";
		// $date_from = date('Y-m-d', strtotime("-1 day", strtotime(date("Y-m-d"))));
		// // $date_from = date('Y-m-d', strtotime("-1 day", strtotime(date("2020-03-11"))));
		// $date_to = date('Y-m-d', strtotime(date("Y-m-d")));
		// $date_to = date('Y-m-d', strtotime(date("2020-03-11")));
		$checker = strtotime($date_to." 23:59");
		// var_dump($date_from);
		// var_dump($date_to);
		$emp_id = 1226;
		$all_sched = $this->get_all_sched_for_checking_test($date_from, $date_to, $emp_id);
		var_dump($all_sched);
		$to_be_checked = [];
		$completed_shift = 0;
		$sched_to_complete = [];
		$sched_check_details = [];
		if(count($all_sched) > 0){
			foreach($all_sched as $all_sched_row){
				$shift_start_date_time = date_format(date_create($all_sched_row->sched_date . " " . $all_sched_row->time_start), 'Y-m-d H:i');
				$shift_end_date_time = date_format(date_create($all_sched_row->sched_date . " " . $all_sched_row->time_end), 'Y-m-d H:i');
				$new_shifts = $this->check_start_end_shift($shift_start_date_time, $shift_end_date_time);
				echo $new_shifts['end'];
				$shift_end_str = strtotime($new_shifts['end']);
				if($shift_end_str <= $checker){
					// CHECK IF THE SHIFT IS ALREADY OVER
					$to_be_checked[$completed_shift] = [
						'sched_row'=>$all_sched_row,
						'dated_shift'=>$new_shifts,
						'sched_id'=>$all_sched_row->sched_id,
						'isFlexi' =>$all_sched_row->isFlexiSched,
						'isComplete' => 1
					];
					$sched_to_complete[$completed_shift] = [
						'sched_id'=>$all_sched_row->sched_id,
						'isComplete' => 1
					];
					$completed_shift++;
				}
			}
			var_dump($to_be_checked);
			
			// $sched_update =  array_column($to_be_checked, 'isComplete');
			// var_dump($sched_to_complete);
			if(count($to_be_checked) > 0){
				$sched_ids = array_column($to_be_checked, 'sched_id');
				var_dump($sched_ids);
				$flexi_sched= array_column($to_be_checked, 'isFlexi');
				$dtr_logs = $this->get_all_dtr_logs(implode(", ", $sched_ids)); // GET ALL DTR LOGS OF ALL SCHED THAT WILL BE CHECKED FOR DTR VIOLATIONS
				$allowable_breaks = $this->get_all_allowable_breaks_per_sched(implode(", ", $sched_ids)); // GET ALL ALLOWABLE BREAKS OF ALL SCHED THAT WILL BE CHECKED FOR DTR VIOLATIONS
				$break_logs = $this->get_all_break_logs($date_from, $date_to); // GET ALL BREAK LOGS OF ALL SCHED THAT WILL BE CHECKED FOR DTR VIOLATIONS
				$inc_break = 0;
				$inc_dtr_logs = 0;
				for($loop = 0; $loop < count($sched_ids); $loop++){
					$sched_violations['absent'] = false;
					$sched_violations['incomplete_dtr_logs'] = false;
					$sched_violations['incomplete_break_logs'] = false;
					$sched_violations['over_break'] = false;
					$sched_check_details[$loop]['assigned_with_breaks'] = false;
					$sched_id = $sched_ids[$loop];
					//GET DTR LOGS RELATED TO THE CURRENT LOOP SCHED
					$sched_dtr_logs = array_merge(array_filter(
						$dtr_logs,
						function ($e) use ($sched_id) {
							return $e->sched_id == $sched_id;
						}
					));
					$schedule_details = array_merge(array_filter(
						$all_sched,
						function ($e) use ($sched_id) {
							return $e->sched_id == $sched_id;
						}
					));
					$sched_check_details[$loop]['sched_details'] = $schedule_details[0];
					if(count($sched_dtr_logs) > 0){ // check if logs exist eLse absent
						$entry_type = "I";
						// get all clock logs of the looped sched
						$sched_dtr_log_in = array_merge(array_filter(
							$sched_dtr_logs,
							function ($e) use ($entry_type) {
								return $e->entry == $entry_type;
							}
						));
						$clock_in_log = "N/A";
						$clock_out_log = "N/A";
						$clock_in_dtr_id = "";
						$clock_out_dtr_id = "";
						$log_complete = 0;
						foreach($sched_dtr_log_in as $sched_dtr_log_in_row){
							$emp_id = $sched_dtr_log_in_row->emp_id;
							$entry_type = "O";
							$sched_dtr_log_out = array_merge(array_filter(
								$sched_dtr_logs,
								function ($e) use ($entry_type, $sched_id) {
									return (($e->entry == $entry_type) && ($e->sched_id == $sched_id));
								}
							));
							// If clock out exist, set DTR Log as complete
							if(count($sched_dtr_log_out) > 0 && $log_complete == 0){
								$clock_in_log = $sched_dtr_log_in_row->log;
								$clock_out_log = $sched_dtr_log_out[0]->log;
								$clock_in_dtr_id = $sched_dtr_log_in_row->dtr_id;
								$clock_out_dtr_id = $sched_dtr_log_out[0]->dtr_id;
								$log_complete = 1;
							}
						}
						if($log_complete == 0){ //INCOMPLETE DTR LOGS
							$sched_violations['incomplete_dtr_logs'] = true;
							$inc_dtr_logs = 1;
							$clock_in_dtr_id = $sched_dtr_log_in[0]->dtr_id;
							$clock_in_log = $sched_dtr_log_in[0]->log;
							$emp_id = $sched_dtr_log_in[0]->emp_id;
							// GET NEXT LOG THAT WILL BE USED AS SUBSTITUTE FOR CLOCKOUT IN IDENTIFYING THE BREAK LOGS
							$next_logs = array_merge(array_filter(
								$sched_dtr_logs,
								function ($e) use ($emp_id, $clock_in_dtr_id) {
									return (($e->dtr_id > $clock_in_dtr_id) && ($e->emp_id == $emp_id));
								}
							));
							if(count($next_logs) > 0){
								$clock_out_dtr_id = $next_logs[0]->dtr_id; // GET BREAK LOGS which dtr id is less than next dtr log id
							}else {
								$clock_out_dtr_id = $clock_in_dtr_id*1000; // GET BREAK LOGS which dtr id is less than clock ing dtr id times 1000
							}
						}
						$sched_check_details[$loop]['dtr_logs'] = [
							"clock_in" => $clock_in_log,
							"clock_out" => $clock_out_log,
						];
						if((int)$flexi_sched[$loop] == 0){// CHECK BREAKS FOR NOT FLEXI SCHED ONLY
							// GET THE ALLOWABLE BREAKS OF THE CURRENT LOOP SCHED
							$sched_allowable_breaks = array_merge(array_filter(
								$allowable_breaks,
								function ($e) use ($sched_id) {
									return ($e->sched_id == $sched_id);
								}
							));
							// CHECK IF SCHED IS ASSIGNED WITH BREAKS
							if((count($sched_allowable_breaks) > 0) && ($schedule_details[0]->remarks != "TITO")){
								$sched_check_details[$loop]['assigned_with_breaks'] = true;
								$sched_check_details[$loop]['allowable_break'] = $sched_allowable_breaks;
								$inc_break_bool = 0;
								$break_hours = 0;
								$allowable_break_minutes = 0;
								$break_out_str = "";
								$break_in_str = "";
								$break_log_duration = "";
								$break_hrs_to_min = 0;
								$total_covered_break = 0;
								$ob = 0;
								$break_spec_details = [];
								foreach($sched_allowable_breaks as $sched_allowable_breaks_index=>$sched_allowable_breaks_row){
									$break_dtr_logs = [];
									$break_type = $sched_allowable_breaks_row->break_type;
									$break_hours += $sched_allowable_breaks_row->hour;
									$allowable_break_minutes += $sched_allowable_breaks_row->min;
									// GET BREAK LOGS ACCORDING TO TYPE
									$sched_break_logs = array_merge(array_filter(
										$break_logs,
										function ($e) use ($clock_in_dtr_id, $clock_out_dtr_id, $emp_id, $break_type) {
											return (($e->dtr_id > $clock_in_dtr_id) && ($e->dtr_id < $clock_out_dtr_id) && ($e->emp_id == $emp_id) && ($e->break_type == $break_type));
										}
									));
									if(count($sched_break_logs) > 1){
										$break_entry = "O";
										$sched_break_out = array_merge(array_filter(
											$sched_break_logs,
											function ($e) use ($break_entry) {
												return ($e->entry == $break_entry);
											}
										));
										if(count($sched_break_out) > 0){
											$break_dtr_logs['out'] = $sched_break_out[0]->log;
											$break_entry = "I";
											$break_out_id = $sched_break_out[0]->dtr_id;
											$sched_break_in = array_merge(array_filter(
												$sched_break_logs,
												function ($e) use ($break_entry, $break_out_id) {
													return (($e->entry == $break_entry) && ($e->note == $break_out_id));
												}
											));
											if(count($sched_break_in) > 0){
												$break_dtr_logs['in'] = $sched_break_in[0]->log;
												$break_out_str = strtotime(date_format(date_create($sched_break_out[0]->log), 'Y-m-d H:i:s'));
												$break_in_str = strtotime(date_format(date_create($sched_break_in[0]->log), 'Y-m-d H:i:s'));
												$break_log_duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
												$break_hrs_to_min = $break_log_duration['hoursTotal'] * 60;
												$total_covered_break += $break_hrs_to_min;
											}else{
												$break_dtr_logs['in'] = "N/A";
												$inc_break_bool = 1;
											}
										}else{
											$inc_break_bool = 1;
										}
									}else{
										$inc_break_bool = 1;
									}
									$break_data[$sched_allowable_breaks_index] = [
										"break_type" => $break_type,
										"break_logs" => $break_dtr_logs,
										"allowable_break_mins" => $allowable_break_minutes,
										"covered_break_mins" => $total_covered_break
									];
								}
								if($inc_break_bool){
									// IF INCOMPLETE DTR LOGS IS TRUE, DO NOT INCLUDE INCOMPLETE BREAK LOGS
									if($inc_dtr_logs == 0){
										$inc_break = 1;
										$sched_violations['incomplete_break_logs'] = true;
									}
								}
								$break_hours_to_min = $break_hours * 60;
								$allowable_break_minutes += $break_hours_to_min;
								if ((int)$total_covered_break > (int)$allowable_break_minutes) {
									$ob = (int)$total_covered_break - (int)$allowable_break_minutes;
									$sched_violations['over_break'] = true;
									$sched_violations['ob_duration'] = $ob;
								}
								$sched_check_details[$loop]['break_info'] = [
									"total_allowable_mins" =>$allowable_break_minutes,
									"total_covered_mins" =>$total_covered_break,
									"break_details" =>$break_data
								];
							}
						}
					}else{// ABSENT
						$sched_violations['absent'] = true;
					}
					$sched_check_details[$loop]['violations'] = $sched_violations;
					// if($sched_violations['absent']){
					// 	$sched_check_details[$loop]['absent'] = true;
					// 	$violation_absent = $this->adddataViolation(5, $schedule_details[0]->Supervisor, $schedule_details[0]->sched_id, $schedule_details[0]->sched_date, $schedule_details[0]->uid, $schedule_details[0]->schedtype_id);
					// 	if($violation_absent['exist']){
					// 		$this->check_qualified_dtr_violation($violation_absent['record']);
					// 	}
					// }
					// if($sched_violations['incomplete_dtr_logs']){
					// 	$sched_check_details[$loop]['incomplete_dtr_logs'] = true;
					// 	$violation_incomplete_logs = $this->adddataViolation(7, $schedule_details[0]->Supervisor, $schedule_details[0]->sched_id, $schedule_details[0]->sched_date, $schedule_details[0]->uid, $schedule_details[0]->schedtype_id);
					// 	if($violation_incomplete_logs['exist']){
					// 		$this->check_qualified_dtr_violation($violation_incomplete_logs['record']);
					// 	}
					// }
					// if($sched_violations['incomplete_break_logs']){
					// 	$sched_check_details[$loop]['incomplete_break_logs'] = true;
					// 	$violation_incomplete_break_logs = $this->adddataViolation(6, $schedule_details[0]->Supervisor, $schedule_details[0]->sched_id, $schedule_details[0]->sched_date, $schedule_details[0]->uid, $schedule_details[0]->schedtype_id);
					// 	if($violation_incomplete_break_logs['exist']){
					// 		$this->check_qualified_dtr_violation($violation_incomplete_break_logs['record']);
					// 	}
					// }
					// if($sched_violations['over_break']){
					// 	$sched_check_details[$loop]['over_break'] = true;
					// 	$violation_overbreak = $this->adddataViolation(4, $schedule_details[0]->Supervisor, $schedule_details[0]->sched_id, $schedule_details[0]->sched_date, $schedule_details[0]->uid, $schedule_details[0]->schedtype_id);
					// 	if($violation_overbreak['exist']){
					// 		$this->check_qualified_dtr_violation($violation_overbreak['record']);
					// 	}
					// }
				}
			}
			// $this->set_sched_date_to_complete($sched_to_complete);
			var_dump($sched_check_details);
		}
	}

	public function checkAllLogsIssue(){
		date_default_timezone_set("Asia/Manila");
		$date_from = date('Y-m-d', strtotime("-1 day", strtotime(date("Y-m-d"))));
		// $date_from = date('Y-m-d', strtotime("-1 day", strtotime(date("2020-03-11"))));
		$date_to = date('Y-m-d', strtotime(date("Y-m-d")));
		// $date_to = date('Y-m-d', strtotime(date("2020-03-11")));
		$checker = strtotime($date_to." 23:59");
		// var_dump($date_from);
		// var_dump($date_to);
		$all_sched = $this->get_all_sched_for_checking($date_from, $date_to);
		// var_dump($all_sched);
		$to_be_checked = [];
		$completed_shift = 0;
		$sched_to_complete = [];
		if(count($all_sched) > 0){
			foreach($all_sched as $all_sched_row){
				$shift_start_date_time = date_format(date_create($all_sched_row->sched_date . " " . $all_sched_row->time_start), 'Y-m-d H:i');
				$shift_end_date_time = date_format(date_create($all_sched_row->sched_date . " " . $all_sched_row->time_end), 'Y-m-d H:i');
				$new_shifts = $this->check_start_end_shift($shift_start_date_time, $shift_end_date_time);
				$shift_end_str = strtotime($new_shifts['end']);
				if($shift_end_str <= $checker){
					// CHECK IF THE SHIFT IS ALREADY OVER
					$to_be_checked[$completed_shift] = [
						'sched_row'=>$all_sched_row,
						'dated_shift'=>$new_shifts,
						'sched_id'=>$all_sched_row->sched_id,
						'isFlexi' =>$all_sched_row->isFlexiSched,
						'isComplete' => 1
					];
					$sched_to_complete[$completed_shift] = [
						'sched_id'=>$all_sched_row->sched_id,
						'isComplete' => 1
					];
					$completed_shift++;
				}
			}
			// var_dump($to_be_checked);
			// $sched_update =  array_column($to_be_checked, 'isComplete');
			// var_dump($sched_to_complete);
			if(count($to_be_checked) > 0){
				$sched_ids = array_column($to_be_checked, 'sched_id');
				$flexi_sched= array_column($to_be_checked, 'isFlexi');
				$dtr_logs = $this->get_all_dtr_logs(implode(", ", $sched_ids)); // GET ALL DTR LOGS OF ALL SCHED THAT WILL BE CHECKED FOR DTR VIOLATIONS
				$allowable_breaks = $this->get_all_allowable_breaks_per_sched(implode(", ", $sched_ids)); // GET ALL ALLOWABLE BREAKS OF ALL SCHED THAT WILL BE CHECKED FOR DTR VIOLATIONS
				$break_logs = $this->get_all_break_logs($date_from, $date_to); // GET ALL BREAK LOGS OF ALL SCHED THAT WILL BE CHECKED FOR DTR VIOLATIONS
				$sched_check_details = [];
				$inc_break = 0;
				$inc_dtr_logs = 0;
				for($loop = 0; $loop < count($sched_ids); $loop++){
					$sched_violations['absent'] = false;
					$sched_violations['incomplete_dtr_logs'] = false;
					$sched_violations['incomplete_break_logs'] = false;
					$sched_violations['over_break'] = false;
					$sched_check_details[$loop]['assigned_with_breaks'] = false;
					$sched_id = $sched_ids[$loop];
					//GET DTR LOGS RELATED TO THE CURRENT LOOP SCHED
					$sched_dtr_logs = array_merge(array_filter(
						$dtr_logs,
						function ($e) use ($sched_id) {
							return $e->sched_id == $sched_id;
						}
					));
					$schedule_details = array_merge(array_filter(
						$all_sched,
						function ($e) use ($sched_id) {
							return $e->sched_id == $sched_id;
						}
					));
					$sched_check_details[$loop]['sched_details'] = $schedule_details[0];
					if(count($sched_dtr_logs) > 0){ // check if logs exist eLse absent
						$entry_type = "I";
						// get all clock logs of the looped sched
						$sched_dtr_log_in = array_merge(array_filter(
							$sched_dtr_logs,
							function ($e) use ($entry_type) {
								return $e->entry == $entry_type;
							}
						));
						$clock_in_log = "N/A";
						$clock_out_log = "N/A";
						$clock_in_dtr_id = "";
						$clock_out_dtr_id = "";
						$log_complete = 0;
						foreach($sched_dtr_log_in as $sched_dtr_log_in_row){
							$emp_id = $sched_dtr_log_in_row->emp_id;
							$entry_type = "O";
							$sched_dtr_log_out = array_merge(array_filter(
								$sched_dtr_logs,
								function ($e) use ($entry_type, $sched_id) {
									return (($e->entry == $entry_type) && ($e->sched_id == $sched_id));
								}
							));
							// If clock out exist, set DTR Log as complete
							if(count($sched_dtr_log_out) > 0 && $log_complete == 0){
								$clock_in_log = $sched_dtr_log_in_row->log;
								$clock_out_log = $sched_dtr_log_out[0]->log;
								$clock_in_dtr_id = $sched_dtr_log_in_row->dtr_id;
								$clock_out_dtr_id = $sched_dtr_log_out[0]->dtr_id;
								$log_complete = 1;
							}
						}
						if($log_complete == 0){ //INCOMPLETE DTR LOGS
							$sched_violations['incomplete_dtr_logs'] = true;
							$inc_dtr_logs = 1;
							$clock_in_dtr_id = $sched_dtr_log_in[0]->dtr_id;
							$clock_in_log = $sched_dtr_log_in[0]->log;
							$emp_id = $sched_dtr_log_in[0]->emp_id;
							// GET NEXT LOG THAT WILL BE USED AS SUBSTITUTE FOR CLOCKOUT IN IDENTIFYING THE BREAK LOGS
							$next_logs = array_merge(array_filter(
								$sched_dtr_logs,
								function ($e) use ($emp_id, $clock_in_dtr_id) {
									return (($e->dtr_id > $clock_in_dtr_id) && ($e->emp_id == $emp_id));
								}
							));
							if(count($next_logs) > 0){
								$clock_out_dtr_id = $next_logs[0]->dtr_id; // GET BREAK LOGS which dtr id is less than next dtr log id
							}else {
								$clock_out_dtr_id = $clock_in_dtr_id*1000; // GET BREAK LOGS which dtr id is less than clock ing dtr id times 1000
							}
						}
						$sched_check_details[$loop]['dtr_logs'] = [
							"clock_in" => $clock_in_log,
							"clock_out" => $clock_out_log,
						];
						if((int)$flexi_sched[$loop] == 0){// CHECK BREAKS FOR NOT FLEXI SCHED ONLY
							// GET THE ALLOWABLE BREAKS OF THE CURRENT LOOP SCHED
							$sched_allowable_breaks = array_merge(array_filter(
								$allowable_breaks,
								function ($e) use ($sched_id) {
									return ($e->sched_id == $sched_id);
								}
							));
							// CHECK IF SCHED IS ASSIGNED WITH BREAKS
							if((count($sched_allowable_breaks) > 0) && ($schedule_details[0]->remarks != "TITO")){
								$sched_check_details[$loop]['assigned_with_breaks'] = true;
								$sched_check_details[$loop]['allowable_break'] = $sched_allowable_breaks;
								$inc_break_bool = 0;
								$break_hours = 0;
								$allowable_break_minutes = 0;
								$break_out_str = "";
								$break_in_str = "";
								$break_log_duration = "";
								$break_hrs_to_min = 0;
								$total_covered_break = 0;
								$ob = 0;
								$break_spec_details = [];
								foreach($sched_allowable_breaks as $sched_allowable_breaks_index=>$sched_allowable_breaks_row){
									$break_dtr_logs = [];
									$break_type = $sched_allowable_breaks_row->break_type;
									$break_hours += $sched_allowable_breaks_row->hour;
									$allowable_break_minutes += $sched_allowable_breaks_row->min;
									// GET BREAK LOGS ACCORDING TO TYPE
									$sched_break_logs = array_merge(array_filter(
										$break_logs,
										function ($e) use ($clock_in_dtr_id, $clock_out_dtr_id, $emp_id, $break_type) {
											return (($e->dtr_id > $clock_in_dtr_id) && ($e->dtr_id < $clock_out_dtr_id) && ($e->emp_id == $emp_id) && ($e->break_type == $break_type));
										}
									));
									if(count($sched_break_logs) > 1){
										$break_entry = "O";
										$sched_break_out = array_merge(array_filter(
											$sched_break_logs,
											function ($e) use ($break_entry) {
												return ($e->entry == $break_entry);
											}
										));
										if(count($sched_break_out) > 0){
											$break_dtr_logs['out'] = $sched_break_out[0]->log;
											$break_entry = "I";
											$break_out_id = $sched_break_out[0]->dtr_id;
											$sched_break_in = array_merge(array_filter(
												$sched_break_logs,
												function ($e) use ($break_entry, $break_out_id) {
													return (($e->entry == $break_entry) && ($e->note == $break_out_id));
												}
											));
											if(count($sched_break_in) > 0){
												$break_dtr_logs['in'] = $sched_break_in[0]->log;
												$break_out_str = strtotime(date_format(date_create($sched_break_out[0]->log), 'Y-m-d H:i:s'));
												$break_in_str = strtotime(date_format(date_create($sched_break_in[0]->log), 'Y-m-d H:i:s'));
												$break_log_duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
												$break_hrs_to_min = $break_log_duration['hoursTotal'] * 60;
												$total_covered_break += $break_hrs_to_min;
											}else{
												$break_dtr_logs['in'] = "N/A";
												$inc_break_bool = 1;
											}
										}else{
											$inc_break_bool = 1;
										}
									}else{
										$inc_break_bool = 1;
									}
									$break_data[$sched_allowable_breaks_index] = [
										"break_type" => $break_type,
										"break_logs" => $break_dtr_logs,
										"allowable_break_mins" => $allowable_break_minutes,
										"covered_break_mins" => $total_covered_break
									];
								}
								if($inc_break_bool){
									// IF INCOMPLETE DTR LOGS IS TRUE, DO NOT INCLUDE INCOMPLETE BREAK LOGS
									if($inc_dtr_logs == 0){
										$inc_break = 1;
										$sched_violations['incomplete_break_logs'] = true;
									}
								}
								$break_hours_to_min = $break_hours * 60;
								$allowable_break_minutes += $break_hours_to_min;
								if ((int)$total_covered_break > (int)$allowable_break_minutes) {
									$ob = (int)$total_covered_break - (int)$allowable_break_minutes;
									$sched_violations['over_break'] = true;
									$sched_violations['ob_duration'] = $ob;
								}
								$sched_check_details[$loop]['break_info'] = [
									"total_allowable_mins" =>$allowable_break_minutes,
									"total_covered_mins" =>$total_covered_break,
									"break_details" =>$break_data
								];
							}
						}
					}else{// ABSENT
						$sched_violations['absent'] = true;
					}
					$sched_check_details[$loop]['violations'] = $sched_violations;
					if($sched_violations['absent']){
						$sched_check_details[$loop]['absent'] = true;
						$violation_absent = $this->adddataViolation(5, $schedule_details[0]->Supervisor, $schedule_details[0]->sched_id, $schedule_details[0]->sched_date, $schedule_details[0]->uid, $schedule_details[0]->schedtype_id);
						if($violation_absent['exist']){
							$this->check_qualified_dtr_violation($violation_absent['record']);
						}
					}
					if($sched_violations['incomplete_dtr_logs']){
						$sched_check_details[$loop]['incomplete_dtr_logs'] = true;
						$violation_incomplete_logs = $this->adddataViolation(7, $schedule_details[0]->Supervisor, $schedule_details[0]->sched_id, $schedule_details[0]->sched_date, $schedule_details[0]->uid, $schedule_details[0]->schedtype_id);
						if($violation_incomplete_logs['exist']){
							$this->check_qualified_dtr_violation($violation_incomplete_logs['record']);
						}
					}
					if($sched_violations['incomplete_break_logs']){
						$sched_check_details[$loop]['incomplete_break_logs'] = true;
						$violation_incomplete_break_logs = $this->adddataViolation(6, $schedule_details[0]->Supervisor, $schedule_details[0]->sched_id, $schedule_details[0]->sched_date, $schedule_details[0]->uid, $schedule_details[0]->schedtype_id);
						if($violation_incomplete_break_logs['exist']){
							$this->check_qualified_dtr_violation($violation_incomplete_break_logs['record']);
						}
					}
					if($sched_violations['over_break']){
						$sched_check_details[$loop]['over_break'] = true;
						$violation_overbreak = $this->adddataViolation(4, $schedule_details[0]->Supervisor, $schedule_details[0]->sched_id, $schedule_details[0]->sched_date, $schedule_details[0]->uid, $schedule_details[0]->schedtype_id);
						if($violation_overbreak['exist']){
							$this->check_qualified_dtr_violation($violation_overbreak['record']);
						}
					}
				}
			}
			$this->set_sched_date_to_complete($sched_to_complete);
			// var_dump($sched_check_details);
		}
	}
	// DMS MICHAEL UPDATE SERVER CHECKING OF DTR VIOLATIONS END
	
	// DMS MJM Codes Start here ---->Auto Check DTR Issue
	public function checkAllLogsIssue_old(){
        // $rs = $this->general_model->fetch_specific_vals('a.sched_id,fname,lname', "a.emp_id=b.emp_id and b.apid=c.apid and sched_date='2019-07-18' and schedtype_id in (1,4,5) and isComplete=0", "tbl_schedule a,tbl_employee b,tbl_applicant c");
		date_default_timezone_set("Asia/Manila");
		$date_to_check = date('Y-m-d', strtotime("-2 day", strtotime(date("Y-m-d"))));
		// $date_to_check = "2020-02-24";
		$rs = $this->general_model->custom_query("SELECT * FROM tbl_schedule a, tbl_employee b where a.emp_id = b.emp_id AND b.isActive = 'yes' AND sched_date='".$date_to_check."' and schedtype_id in (1,4,5) and isComplete=0");
		$arr = array();
		foreach($rs as $row){
			  $uidd = $this->get_emp_details_via_emp_id($row->emp_id);
			  $supervisor = $this->get_direct_supervisor_via_emp_id($row->emp_id);
			  $resultIN = $this->checkLogsViolationIN($row->sched_id,$row->emp_id);
			  $resultOUT = $this->checkLogsViolationOUT($row->sched_id,$row->emp_id);
			  //Check Absent, Overbreak, and Incomplete Logs(No Break/No Clockout)
			  $isOverbreakRemark="Not-Overbreak";
			  $isAbsentRemark="Not-Absent";
			  $breakDetails="break - ";
			  $isIncompleteLogsRemark="Incomplete";
			  $isIncompleteBreakLogsRemark= "Incomplete";
			  $isCompleteBreak=0;
			  $ishasclockOut=0;
			  $isCompleteBreakDATA="";
			//   $violation_overbreak = null;
			// $violation_incomplete_logs = null;
			// $violation_incomplete_break_logs = null;
			// $violation_absent = null;
			  $totalShiftBreak =  $this->general_model->custom_query("SELECT (hour*60) as hour,sum(min) as min,count(min) as totalShiftBreakLogs FROM tbl_schedule a,tbl_shift_break b,tbl_break_time_account c,tbl_break_time d where a.acc_time_id=b.acc_time_id and b.bta_id=c.bta_id and c.btime_id=d.btime_id and sched_id=".$row->sched_id." and b.isActive=1");
				if(count($totalShiftBreak)>0){
					$totalShiftBreakTime = $totalShiftBreak[0]->hour + $totalShiftBreak[0]->min ;
					$in = (count($resultIN)) ?  $resultIN->dtr_id : 0;
					$out = (count($resultOUT)>0) ? $resultOUT->dtr_id : $in*1000;
					$out_check = (count($resultOUT)>0) ? $resultOUT->dtr_id : 0;
						#1 - Check if Present	
						if($in>0){ // Present
							#2 - Check if It has Clock-out
							if($out_check==0){ // If none
								$ishasclockOut = 1;
							}else{
								$ishasclockOut = 0;
							}

							#Checking BREAK Details		
							$totalWorkBreak = $this->getTotalBreak($row->sched_id, $row->emp_id, $resultIN->dtr_id, $out);
							$totalWorkBreakExplode = explode("|", $totalWorkBreak);
							$breakDetails = "break - " . $totalWorkBreakExplode[1] . " = " . $totalShiftBreak[0]->totalShiftBreakLogs;

							#3 - check if Complete Break Logs
							$isCompleteBreakDATA = intval($totalWorkBreakExplode[1]) . " < " . intval($totalShiftBreak[0]->totalShiftBreakLogs);
							if (intval($totalWorkBreakExplode[1]) < intval($totalShiftBreak[0]->totalShiftBreakLogs)) {
								$isCompleteBreak = 1;
							}
							#4 - check if Overbreak
							if (intval($totalWorkBreakExplode[0]) > intval($totalShiftBreakTime)) { # OverBreak
								$isOverbreakRemark = $totalWorkBreak . " - Overbreak - " . $totalShiftBreakTime;
								#File IR for  OverBreak
								$violation_overbreak = $this->adddataViolation(4, $supervisor->Supervisor, $row->sched_id, $row->sched_date, $uidd->uid, $row->schedtype_id);
								// var_dump($violation_overbreak);
								if($violation_overbreak['exist']){
									// var_dump("check qualified");
									$this->check_qualified_dtr_violation($violation_overbreak['record']);
								}
							}	
							#File IR for Incomplete Logs
							if($ishasclockOut==1){
								$violation_incomplete_logs = $this->adddataViolation(7,$supervisor->Supervisor,$row->sched_id,$row->sched_date,$uidd->uid, $row->schedtype_id);
								// echo "inc dtr logs <br>";
								// var_dump($violation_incomplete_logs);
								if($violation_incomplete_logs['exist']){
									// var_dump("check qualified");
									$this->check_qualified_dtr_violation($violation_incomplete_logs['record']);
								}
								$isIncompleteLogsRemark= "Incomplete";
							}else{
								$isIncompleteLogsRemark="Complete";
							}
							#File IR for Incomplete Break Logs
							if($isCompleteBreak==1){
								$violation_incomplete_break_logs = $this->adddataViolation(6,$supervisor->Supervisor,$row->sched_id,$row->sched_date,$uidd->uid, $row->schedtype_id);
								// echo "inc break logs <br>";
								// var_dump($violation_incomplete_break_logs);
								// var_dump($violation_incomplete_break_logs['exist']);
								if($violation_incomplete_break_logs['exist']){
									// var_dump("check qualified");
									$this->check_qualified_dtr_violation($violation_incomplete_break_logs['record']);
								}
								$isIncompleteBreakLogsRemark= "Incomplete";
							}else{
								$isIncompleteBreakLogsRemark="Complete";
							}
						}else{ #Absent
							// $isAbsentRemark="Absent";
							// 	#File IR for Absent
								$violation_absent = $this->adddataViolation(5,$supervisor->Supervisor,$row->sched_id,$row->sched_date,$uidd->uid, $row->schedtype_id);
								// echo "absent <br>";
								// var_dump($violation_absent);
								if($violation_absent['exist']){
									// var_dump("check qualified");
									$this->check_qualified_dtr_violation($violation_absent['record']);
								}
						}									
				}
				$datasched['isComplete'] = 1;
				$wheresched = "sched_id = ".$row->sched_id;
				$this->general_model->update_vals($datasched, $wheresched, 'tbl_schedule');

			  
			$arr[$row->emp_id][$row->sched_id] = array(
				"in" => $resultIN,
				"out" => $resultOUT,
				"isOverBreak" => $isOverbreakRemark,
				"isAbsent" => $isAbsentRemark,
				"isCompleteBreak" => $isCompleteBreak,
				"ishasclockOut" => $ishasclockOut,
				"isIncompleteLogs" => $isIncompleteLogsRemark,
				"isIncompleteBreakLogs" => $isIncompleteBreakLogsRemark,
				"breakDetails" => $breakDetails,
				"isCompleteBreakDATA" => $isCompleteBreakDATA,
			);
		}

		echo json_encode($arr);
	}
 
	protected function checkLogsViolationIN($sched_ID,$emp_id){
        $rs = $this->general_model->fetch_specific_val('dtr_id,log,entry,type,sched_id', "entry='I' and type='DTR'  and emp_id=".$emp_id." and sched_id=".$sched_ID, "tbl_dtr_logs","dtr_id ASC");
		return $rs;
	}

	protected function checkLogsViolationOUT($sched_ID,$emp_id){
        $rs = $this->general_model->fetch_specific_val('dtr_id,log,entry,type,sched_id', "entry='O' and type='DTR' and emp_id=".$emp_id." and sched_id=".$sched_ID, "tbl_dtr_logs","dtr_id ASC");
		return $rs;
	}
	public function getTotalBreak($sched_id,$emp_id,$login2,$logout3){
				$llbreak_sbrk = $this->getLunchBreak_sbrk($emp_id,$login2,$logout3);

					$llbreak1 = ($llbreak_sbrk[0]->break!=null) ? explode(",",$llbreak_sbrk[0]->break) : array(0,0);
			  
					$ffbreak_sbrk = $this->getFirstBreak_sbrk($emp_id,$login2,$logout3);
		
					$ffbreak1 = ($ffbreak_sbrk[0]->break!=null) ? explode(",",$ffbreak_sbrk[0]->break) : array(0,0);
			
					$llastbreak_sbrk = $this->getLastBreak_sbrk($emp_id,$login2,$logout3);
		
					$llastbreak1 = ($llastbreak_sbrk[0]->break!=null) ? explode(",",$llastbreak_sbrk[0]->break) : array(0,0);
			
			$lastbreak_total =  ($llastbreak1[0]!=0) ? round(abs(strtotime($llastbreak1[0])- strtotime($llastbreak1[1])) / 60,2) :  0;
			$lunchbreak_total = ($llbreak1[0]!=0) ? round(abs(strtotime($llbreak1[0])- strtotime($llbreak1[1])) / 60,2) :  0;
			$firstbreak_total = ($ffbreak1[0]!=0) ?  round(abs(strtotime($ffbreak1[0])- strtotime($ffbreak1[1])) / 60,2) :  0;
			
			$ifhasFirstBrk = ($firstbreak_total!=0) ? 1 : 0 ;
			$ifhasLunchBrk = ($lunchbreak_total!=0) ? 1 : 0 ;
			$ifhasLastBrk = ($lastbreak_total!=0) ? 1 : 0 ;
			$countBreakTake = $ifhasFirstBrk + $ifhasLunchBrk + $ifhasLastBrk;
			
			return $lastbreak_total+$lunchbreak_total+$firstbreak_total."|".$countBreakTake;

	}
	public function getLunchBreak_sbrk($emp_id,$login,$logout){
  		$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from  tbl_dtr_logs a,tbl_shift_break b,tbl_break_time_account c,tbl_break d where  a.acc_time_id=b.sbrk_id and b.bta_id=c.bta_id and c.break_id=d.break_id and dtr_id> ".$login." and dtr_id<".$logout." and a.emp_id =".$emp_id." and a.type='Break' and break_type='LUNCH' limit 1");
		return $qry->result();
	}
	public function getFirstBreak_sbrk($emp_id,$login,$logout){
		
		$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from  tbl_dtr_logs a,tbl_shift_break b,tbl_break_time_account c,tbl_break d where  a.acc_time_id=b.sbrk_id and b.bta_id=c.bta_id and c.break_id=d.break_id and dtr_id> ".$login." and dtr_id<".$logout." and a.emp_id =".$emp_id." and a.type='Break' and break_type='FIRST BREAK' limit 1");
		return $qry->result();
	}
	public function getLastBreak_sbrk($emp_id,$login,$logout){
  		$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from  tbl_dtr_logs a,tbl_shift_break b,tbl_break_time_account c,tbl_break d where  a.acc_time_id=b.sbrk_id and b.bta_id=c.bta_id and c.break_id=d.break_id and dtr_id> ".$login." and dtr_id<".$logout." and a.emp_id =".$emp_id." and a.type='Break' and break_type='LAST BREAK' limit 1");
			return $qry->result();
	}

	protected function check_if_dtr_vio_recorded($sched_id, $dtr_vio_type_id, $user_id){
		$fields = "*";
		$where = "user_ID = $user_id AND sched_ID = $sched_id AND dtrViolationType_ID = $dtr_vio_type_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_user_dtr_violation");
	}
	
	public function adddataViolation($dtrViolationType_ID,$supervisor_ID,$sched_id,$schedDate,$uid, $schedType = 0){
		$dtr_vio_record = $this->check_if_dtr_vio_recorded($sched_id, $dtrViolationType_ID, $uid);
		$data['exist'] = 0;
		if(count($dtr_vio_record) < 1){
			$data['exist'] = 1;
			$dataViolation['dtrViolationType_ID'] = $dtrViolationType_ID;
			$dataViolation['supervisor_ID'] = $supervisor_ID;
			$dataViolation['user_ID'] = $uid;
			$dataViolation['sched_id'] =  $sched_id;
			$dataViolation['sched_date'] =  $schedDate;
			$dataViolation['sched_type'] =  $schedType;
			$dataViolation['month'] =  date("F",strtotime($schedDate));
			$dataViolation['year'] =  date("y",strtotime($schedDate));
			$data['record'] =  $this->general_model->insert_vals_last_inserted_id($dataViolation, 'tbl_user_dtr_violation');
		}
		return $data;
	}	
	protected function get_emp_details_via_user_id($user_id)
    {
        $fields = "emp.emp_id, users.uid, app.pic, app.fname, app.lname, app.mname, app.nameExt, app.email, emp.Supervisor, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, empStat.status as empStatus";
        $where = "empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND emp.isActive = 'yes' AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = $user_id";
        $table = "tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app, tbl_account accounts, tbl_user users";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }
	// DMS MJM Codes End here ---->Auto Check DTR Issue
	
	  // DMS FUNCTIONS (MICHAEL) START
    protected function get_user_dtr_violation($user_dtr_violation_id)
    {
        $fields = "userDtrVio.userDtrViolation_ID, userDtrVio.user_ID, userDtrVio.supervisor_ID, userDtrVio.dtrViolationType_ID, vioType.description, userDtrVio.sched_ID, userDtrVio.month, userDtrVio.year, userDtrVio.sched_date, userDtrVio.shift, userDtrVio.occurence, userDtrVio.duration, userDtrVio.dateRecord";
        $where = "vioType.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = $user_dtr_violation_id";
        $table = "tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type vioType";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    protected function get_dtr_violation_settings($dtr_violation_type_id)
    {
        $fields = "dtrVioSettings.dtrViolationTypeSettings_ID, dtrVioSettings.dtrViolationType_ID, dtrVioSettings.oneTimeStat, dtrVioSettings.oneTimeVal, dtrVioSettings.consecutiveStat, dtrVioSettings.consecutiveVal, dtrVioSettings.nonConsecutiveStat, dtrVioSettings.nonConsecutiveVal, dtrVioSettings.durationType, dtrVioSettings.durationVal, dtrVioSettings.durationUnit, dtrVioSettings.offense_ID, dtrVioSettings.category_ID, dtrVioSettings.daysDeadline, dtrVioSettings.deadlineOption";
        $where = "dtrVioSettings.dtrViolationType_ID = $dtr_violation_type_id";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_dtr_violation_type_settings dtrVioSettings");
    }

    protected function get_existing_ir($subject_emp_id, $prescriptive_stat, $liability_stat, $offense_ID)
    {
        $dateTime = $this->get_current_date_time();
        $where_cure_date = "";
        if ($prescriptive_stat == 4) {
            $where_cure_date = "AND date(ir.prescriptionEnd) >= '" . $dateTime['date'] . "'";
        }
        $fields = "ir.incidentReport_ID, ir.subjectEmp_ID, ir.sourceEmp_ID, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.liabilityStat, ir.datedLiabilityStat, ir.subjectExplanation, ir.occurence, discCateg.disciplineCategory_ID, discCateg.category, discAction.disciplinaryAction_ID, discAction.action, discActionCateg.disciplinaryActionCategory_ID, discActionCateg.level, ir.offense_ID, ir.offense, ir.prescriptiveId, ir.disciplinaryActionCategorySettings_ID";
        $where = "discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discActionCateg.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND ir.prescriptiveStat = $prescriptive_stat AND ir.subjectEmp_ID = $subject_emp_id AND ir.liabilityStat = $liability_stat AND ir.offense_ID = $offense_ID " . $where_cure_date;
        $table = "tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_discipline_category discCateg";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

   protected function check_if_pending_qualified_exist($offense_id, $emp_id)
    {
        $fields = "qualified.qualifiedUserDtrViolation_ID, userDtrVio.userDtrViolation_ID, userDtrVio.dateRecord, userDtrVio.sched_date";
        $where = "settings.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.status_ID = 2 AND settings.offense_ID = $offense_id AND qualified.emp_id = $emp_id";
        $table = "tbl_user_dtr_violation userDtrVio, tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_dtr_violation_type_settings settings";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
        
    }

    protected function add_qualified_user_dtr_violation($subject_emp_id, $status_id, $occurence_rule_num, $user_dtr_violation_id, $occurence_value)
    {
        $data['emp_id'] = $subject_emp_id;
        $data['status_ID'] = $status_id;
        $data['occurrenceRuleNum'] = $occurence_rule_num;
        $data['userDtrViolation_ID'] = $user_dtr_violation_id;
        $data['occurrenceValue'] = $occurence_value;
        return $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dms_qualified_user_dtr_violation');
    }

    protected function check_if_previous_ir_exist($user_dtr_violation, $subject_user_emp_details, $violation_rules)
    {
        $data['exist'] = 0;
        $data['ir_details'] = "";
        // get_if previous_ir_exist
        // get_ir_with_current_stat
        // get_ir_with_same_offense
        $existing_ir = $this->get_existing_ir($subject_user_emp_details->emp_id, 4, 17, $violation_rules->offense_ID);
        if (count($existing_ir) > 0) {
            // identify if curren IR prescription end date still covers the date of currently commited violation
            $current_presc_str = strtotime(date("Y-m-d", strtotime($existing_ir->prescriptionEnd)));
            $date_detected_str = strtotime(date("Y-m-d", strtotime($user_dtr_violation->dateRecord)));
            if ($date_detected_str <= $current_presc_str) {
                $data['exist'] = 1;
                $data['ir_details'] = $existing_ir;
            } else {
                //update IR prescriptive status to completed
                $data['prescriptiveStat'] = 3;
                $this->update_incident_report($data, $existing_ir->incidentReport_ID);
            }
        }
        return $data;
    }

    protected function set_dms_deadline($emp_id, $record_id, $days_deadline, $dms_deadline_type, $level)
    {
        $dateTime = $this->get_current_date_time();
        $deadline_date = Date("Y-m-d", strtotime($dateTime['date'] . $days_deadline . ' days'));
        $deadline_date_time = $deadline_date . " 23:59:59";
        $data['record_ID'] = $record_id;
        $data['emp_id'] = $emp_id;
        $data['deadline'] = $deadline_date_time;
        $data['dmsDeadlineType_ID'] = $dms_deadline_type;
        $data['status_ID'] = 2;
        $data['level'] = $level;
        return $this->general_model->insert_vals($data, "tbl_dms_deadline");
    }

    protected function set_qualified_user_dtr_violation_notification($emp_id, $qualified_user_dtr_violation_id, $status_id, $level)
    {
        $data['qualifiedUserDtrViolation_ID'] = $qualified_user_dtr_violation_id;
        $data['emp_id'] = $emp_id;
        $data['level'] = $level;
        $data['status_ID'] = $status_id;
        return $this->general_model->insert_vals($data, 'tbl_dms_supervisor_qualified_user_dtr_violation_notification');
    }

    protected function set_notify_ir_direct_sup($direct_sup_user_details, $subject_user_emp_details, $user_dtr_violation_obj)
    {
        $recipient[0] = ['userId' => $direct_sup_user_details->uid];
        $link = "discipline/dms_supervision/dtrViolations/fileDtrViolationIrTab";
        $notif_mssg = "<b>FILE IR</b>. You are notified to file an IR for " . ucfirst(strtolower($subject_user_emp_details->fname)) . " " . ucfirst(strtolower($subject_user_emp_details->lname));
        if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //LATE
        {
            $notif_mssg .= " for being <b>late</b> last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) // UT
        {
            $notif_mssg .= " due to a recorded <b>undertime</b> last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) //FORGOT TO LOGOUT
        {
            $notif_mssg .= " for <b>not logging out</b> on time last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
        {
            $notif_mssg .= " due to a recorded <b>overbreak</b> last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 6) // INC BREAK
        {
            $notif_mssg .= "'s <b>incomplete break</b> logs issue last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 7) //INC DTR Logs
        {
            $notif_mssg .= "'s <b>incomplete DTR</b> logs issue last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 5) // ABSENT
        {
            $notif_mssg .= " for being <b>absent</b> last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
        }
        return  $this->set_system_notif($notif_mssg,  $link,  $recipient, 'dms');
    }

    protected function auto_notify_ir_direct_sup($user_dtr_violation,  $subject_user_emp_details,  $qualified_user_dtr_violation_id,  $violation_rules,  $violation_details)
    {
        $data['error'] = 0;
        $sup_emp_id = $subject_user_emp_details->Supervisor;
		if($sup_emp_id == 980){
			$sup_emp_id = 522;
			$direct_sup_details = $this->get_emp_details_via_emp_id(522);
		}else{
			$direct_sup_details = $this->get_emp_details_via_emp_id($sup_emp_id);
		}
		if (count($direct_sup_details) > 0) {
			$data['set_deadline_to_file_ir'] =  $this->set_dms_deadline($sup_emp_id,  $qualified_user_dtr_violation_id,  $violation_rules->daysDeadline, 1, 1); // set deadline
			$data['set_qualified_user_dtr_violation_notification'] =  $this->set_qualified_user_dtr_violation_notification($sup_emp_id,  $qualified_user_dtr_violation_id, 4, 1); // set deadline
			$data['notify_ir_direct_sup'] =  $this->set_notify_ir_direct_sup($direct_sup_details,  $subject_user_emp_details,  $user_dtr_violation); // set notify ir direct sup
		} else {
			$data['error'] = 1;
			$data['error_details'] = "Direct Supervisor not found";
		}
		return  $data;
	}

	protected function get_current_disciplinary_action_category_settings()
    {
        $fields = "disciplinaryActionCategorySettings_ID";
        $where = "status_ID = 10";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_disciplinary_action_category_settings");
    }

    protected function get_disciplinary_action_details_prev_occurence($discipline_categ_id, $level, $disciplinaryActionCategorySettings_ID)
    {
        $fields = "discAct.disciplinaryAction_ID, discAct.action, discAct.periodMonth, discAct.abbreviation, discAct.dateCreated, discAct.dateChanged, discAct.changedBy";
        $where = "discAct.disciplinaryAction_ID = discActCateg.disciplinaryAction_ID AND discActCateg.disciplineCategory_ID = $discipline_categ_id AND discActCateg.level = $level AND discActCateg.disciplinaryActionCategorySettings_ID = " . $disciplinaryActionCategorySettings_ID;
        $table = "tbl_dms_disciplinary_action discAct, tbl_dms_disciplinary_action_category discActCateg";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

    protected function get_start_and_end_date_coverage($sched_date,  $rule,  $unit)
    {
        //  $sched_date = '2019-02-10';
        //  $fixed_rule = 4;
        //  $unit = 'week';
        if ($unit == 'month') {
            $coverage_from_month = Date("Y-m-d", strtotime($sched_date . '-' .  $rule . ' months'));
            $start = new DateTime($coverage_from_month);
            $date['start'] =  $start->format('Y-m') . "-01";
            $end = new DateTime($sched_date);
            $date['end'] =  $end->format('Y-m-t');
        } else {
            $start_date = new DateTime(Date("Y-m-d", strtotime($sched_date . '-' .  $rule . ' weeks')));
            $start_year = (int) Date("Y", strtotime($sched_date . '-' .  $rule . ' weeks'));
            $start_week = (int) Date("W", strtotime($sched_date . '-' .  $rule . ' weeks'));
            $date['start'] =  $start_date->setISODate($start_year,  $start_week)->format('Y-m-d');
            $end_year = (int) Date("Y", strtotime($sched_date));
            $end_week = (int) Date("W", strtotime($sched_date));
            $end_date = new DateTime($sched_date);
            $end_date->setISODate($end_year,  $end_week)->format('Y-m-d');
            $date['end'] =  $end_date->modify('+6 days')->format('Y-m-d');
        }
        return  $date;
    }

    protected function check_one_time_rules($user_dtr_violation_obj,  $violation_rules_obj,  $violation_details_obj)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $data['valid'] = 0;
        if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //late
        {
            if ($violation_details_obj['violation_details']['duration'] >=  $violation_rules_obj->oneTimeVal) {
                $data['valid'] = 1;
            }
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) //UT
        {
            if ($violation_details_obj['violation_details']['duration'] >=  $violation_rules_obj->oneTimeVal) {
                $data['valid'] = 1;
            }
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) // forgot to logout
        {
            if ($violation_details_obj['violation_details']['duration'] >=  $violation_rules_obj->oneTimeVal) {
                $data['valid'] = 1;
            }
        } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
        {
            if ($violation_details_obj['violation_details']['over_break'] >=  $violation_rules_obj->oneTimeVal) {
                $data['valid'] = 1;
            }
        }
        return  $data;
    }
    
    public function check_if_consec_rules_covered($user_dtr_violation_obj,  $violation_rules_obj,  $first_date)
    {
        $unit =  $violation_rules_obj->durationUnit;
        $presc_period =  $violation_rules_obj->durationVal;
        $first_date_coverage = strtotime($first_date);
        $covered = 0;
        if ($violation_rules_obj->durationType == 1) {
            $fixed_rule =  $presc_period - 1;
            $start_end_coverage =  $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date,  $fixed_rule,  $unit);
            $consec_coverage_from = strtotime($start_end_coverage['start']);
            $consec_coverage_to = strtotime($start_end_coverage['end']);
            if (($consec_coverage_from <=  $first_date_coverage) && ($consec_coverage_to >=  $first_date_coverage)) {
                $covered = 1;
            } else {
                $covered = 0;
            }
        } else if ($violation_rules_obj->durationType == 2) {
            $custom_rule =  $presc_period;
            $consec_coverage_from = strtotime($user_dtr_violation_obj->sched_date . ' -' .  $custom_rule . ' days');
            $consec_coverage_to = strtotime($user_dtr_violation_obj->sched_date);
            if (($consec_coverage_from <=  $first_date_coverage) && ($consec_coverage_to >=  $first_date_coverage)) {
                $covered = 1;
            } else {
                $covered = 0;
            }
        } else if ($violation_rules_obj->durationType == 3) {
            $category_settings = $this->get_current_disciplinary_action_category_settings();
            $prev_categ = $violation_rules_obj->category_ID; //get categ from prev valid IR
            $prev_level = 1; //get level from prev valid IR
            $disc_action_details = $this->get_disciplinary_action_details_prev_occurence($prev_categ, $prev_level, $category_settings->disciplinaryActionCategorySettings_ID);
            $cod_presc_period = $disc_action_details->periodMonth - 1;
            $start_end_coverage = $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date, $cod_presc_period, 'month');
            $consec_coverage_to = strtotime($start_end_coverage['end']);
            $consec_coverage_from = strtotime($start_end_coverage['start']);
            if (($consec_coverage_from <=  $first_date_coverage) && ($consec_coverage_to >=  $first_date_coverage)) {
                $covered = 1;
            } else {
                $covered = 0;
            }
        }
        return  $covered;
    }

    protected function check_if_consecutive_dates($dtr_consecutive_dates)
    {
        $consec = 1;
        $previous = new DateTime($dtr_consecutive_dates[0]->sched_date); // Set the "previous" value
        unset($dtr_consecutive_dates[0]); // Unset the value we just set to  $previous, so we don't loop it twice
        foreach ($dtr_consecutive_dates as  $row) { // Loop the object
            $current = new DateTime($row->sched_date);
            $diff =  $current->diff($previous);
            // If the difference is exactly 1 day, it's continuous 
            if ($diff->days == 1) {
                $previous = new DateTime($row->sched_date);
            } else {
                $consec = 0;
            }
        }
        return  $consec;
    }




    public function check_consecutive_rules($user_dtr_violation_obj, $violation_rules_obj)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $data['valid'] = 0;
        $days =  $violation_rules_obj->consecutiveVal - 1;
        $to =  $user_dtr_violation_obj->sched_date;
        $from = Date("Y-m-d ", strtotime($user_dtr_violation_obj->sched_date . '-' .  $days . ' days'));

        $day =  $violation_rules_obj->consecutiveVal;

        $dtr_consecutive_dates =  $this->get_user_dtr_violation_by_range($from,  $to,  $user_dtr_violation_obj->user_ID,  $user_dtr_violation_obj->dtrViolationType_ID,  $user_dtr_violation_obj->userDtrViolation_ID);
        if (count($dtr_consecutive_dates) + 1 >= (int) $day) {
            $consec =  $this->check_if_consecutive_dates($dtr_consecutive_dates);
            if ($consec) {
                $covered =  $this->check_if_consec_rules_covered($user_dtr_violation_obj,  $violation_rules_obj,  $from);
                if ($covered) {
                    $consec_count = 0;
                    foreach ($dtr_consecutive_dates as  $rows) {
                        $consec_dates[$consec_count] = [
                            'userDtrViolation_ID' =>  $user_dtr_violation_obj->userDtrViolation_ID,
                            'relatedUserDtrViolation_ID' =>  $rows->userDtrViolation_ID,
                        ];
                        $user_data['status_ID'] = 3;
                        $qualify['user_data_violation_stat'] =  $this->update_user_dtr_violation($rows->userDtrViolation_ID,  $user_data);
                        $consec_count++;
                    }
                    $data['add_user_dtr_violation_related_details'] =  $this->add_user_dtr_violation_related_details($consec_dates);
                    $data['valid'] = 1;
                    $data['violation_details'] =  $dtr_consecutive_dates;
                } else {
                    $data['violation_details'] = "beyond coverage";
                }
            } else {
                $data['violation_details'] = "dates not consecutive";
            }
        } else {
            $data['valid'] = 0;
            $data['violation_details'] = "count does not qualify";
        }
        return  $data;
    }

    public function check_non_consecutive_rules($user_dtr_violation_obj,  $violation_rules_obj)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $data['valid'] = 0;
        $unit =  $violation_rules_obj->durationUnit;
        $presc_period =  $violation_rules_obj->durationVal;
        $covered = 0;
        if ($violation_rules_obj->durationType == 1) {
            // FIXED
            $fixed_rule =  $presc_period - 1;
            $start_end_coverage =  $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date,  $fixed_rule, 'month');
            $coverage_to =  $start_end_coverage['end'];
            $coverage_from =  $start_end_coverage['start'];
        } else if ($violation_rules_obj->durationType == 2) {
            // CUSTOM
            $custom_rule =  $presc_period;
            $coverage_from = date("Y-m-d", strtotime($user_dtr_violation_obj->sched_date . '-' .  $custom_rule . ' days'));
            $coverage_to =  $user_dtr_violation_obj->sched_date;
        } else if ($violation_rules_obj->durationType == 3) {
            // COD
            $prev_categ = 1; //get categ from prev valid IR
            $prev_level = 1; //get level from prev valid IR
            $disc_action_details =  $this->get_disciplinary_action_details_prev_occurence($prev_categ,  $prev_level);
            $cod_presc_period =  $disc_action_details->periodMonth - 1;
            $start_end_coverage =  $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date,  $cod_presc_period, 'month');
            $coverage_to =  $start_end_coverage['end'];
            $coverage_from =  $start_end_coverage['start'];
        }

        if (($user_dtr_violation_obj->dtrViolationType_ID == 3) || ($user_dtr_violation_obj->dtrViolationType_ID == 6) || ($user_dtr_violation_obj->dtrViolationType_ID == 7)) { //remove this if inc break and inc dtr logs policy will be separated
            $accumulated =  $this->get_user_dtr_violation_by_range_inc_logs($coverage_from,  $coverage_to,  $user_dtr_violation_obj->user_ID,  $user_dtr_violation_obj->userDtrViolation_ID);
            $accumulated_count = count($accumulated) + 1;
            if ($accumulated_count >=  $violation_rules_obj->nonConsecutiveVal) {
                if ($accumulated_count > 1) {
                    $non_consec_count = 0;
                    $compute_accumulated =  $this->compute_accumulated($user_dtr_violation_obj->dtrViolationType_ID,  $accumulated,  $violation_rules_obj);
                    if ($compute_accumulated['accumulated'] + 1 >=  $violation_rules_obj->nonConsecutiveVal) {
                        foreach ($compute_accumulated['details'] as  $rows) {
                            $non_consec_dates[$non_consec_count] = [
                                'userDtrViolation_ID' =>  $user_dtr_violation_obj->userDtrViolation_ID,
                                'relatedUserDtrViolation_ID' =>  $rows['user_dtr_vio_id'],
                            ];
                            $non_consec_count++;
                        }
                        $data['add_user_dtr_violation_related_details'] =  $this->add_user_dtr_violation_related_details($non_consec_dates);
                    }
                }
                $data['valid'] = 1;
                $data['violation_details'] =  $accumulated_count;
            }
        } else {
            $accumulated =  $this->get_user_dtr_violation_by_range($coverage_from,  $coverage_to,  $user_dtr_violation_obj->user_ID,  $user_dtr_violation_obj->dtrViolationType_ID,  $user_dtr_violation_obj->userDtrViolation_ID);
            if (count($accumulated) > 0) {
                $compute_accumulated =  $this->compute_accumulated($user_dtr_violation_obj->dtrViolationType_ID,  $accumulated,  $violation_rules_obj);
                if ($compute_accumulated['accumulated'] >=  $violation_rules_obj->nonConsecutiveVal) {
                    $non_consec_count = 0;

                    foreach ($compute_accumulated['details'] as  $rows) {
                        $non_consec_dates[$non_consec_count] = [
                            'userDtrViolation_ID' =>  $user_dtr_violation_obj->userDtrViolation_ID,
                            'relatedUserDtrViolation_ID' =>  $rows['user_dtr_vio_id'],
                        ];
                        $non_consec_count++;
                    }
                    $data['add_user_dtr_violation_related_details'] =  $this->add_user_dtr_violation_related_details($non_consec_dates);
                    $data['valid'] = 1;
                    $data['violation_details'] =  $compute_accumulated;
                }
            } else {
                $data['violation_details'] = "no record found";
            }
        }
        return  $data;
    }
	
    private function get_user_dtr_violation_by_range($from, $to, $user_id, $dtr_vio_type, $user_dtr_id)
    {
        $fields = "userDtrViolation_ID, sched_date, occurence, duration";
        $where = "DATE(sched_date) >= '$from' AND DATE(sched_date) <= '$to' AND user_ID = $user_id AND dtrViolationType_ID = $dtr_vio_type AND status_ID = 0 AND userDtrViolation_ID != $user_dtr_id";
        $table = "tbl_user_dtr_violation";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, 'sched_date ASC');
    }
    protected function check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules)
    {
        $qualified  = 0;
        $qualify['valid'] = 0;
		$qualify['error'] = 0;
		$qualify['occurence_rule_num'] = 0;
		$qualify['occurence_value'] = 0;
        if (count($violation_rules) > 0) {
            if ((int) $violation_rules->oneTimeStat) {
                // one time
                if ($qualified == 0) {
                    $rule_checking_details = $this->check_one_time_rules($user_dtr_violation, $violation_rules, $violation_details);
                    if ($rule_checking_details['error'] == 0) {
                        $qualify['error'] = 0;
                        if ($rule_checking_details['valid'] == 1) {
                            $qualified = 1;
                            $qualify['valid'] = 1;
                            $qualify['occurence_rule_num'] = $qualified;
                            $qualify['occurence_rule'] = "one time";
                            $qualify['related_details'] = $violation_details['violation_details'];
                            $qualify['occurence_value'] = $violation_rules->oneTimeVal;
                        }
                    } else {
                        $qualify['error'] = 1;
                        $qualify['error_details'] = $rule_checking_details['error_details'];
                    }
                }
            }
            if ((int) $violation_rules->consecutiveStat) {
                if ($qualified == 0) {
                    $rule_checking_details = $this->check_consecutive_rules($user_dtr_violation, $violation_rules);
                    if ($rule_checking_details['valid'] == 1) {
                        $qualified = 2;
                        $qualify['valid'] = 1;
                        $qualify['occurence_rule_num'] = $qualified;
                        $qualify['occurence_rule'] = "consecutive";
                        $qualify['related_details'] = $violation_details['violation_details'];
                        $qualify['supporting_details'] = $rule_checking_details['violation_details'];
                        $qualify['occurence_value'] = $violation_rules->consecutiveVal;
                    }
                }
            }
            if ((int) $violation_rules->nonConsecutiveStat) {
                if ($qualified == 0) {
                    $rule_checking_details = $this->check_non_consecutive_rules($user_dtr_violation, $violation_rules);
                    if ($rule_checking_details['valid'] == 1) {
                        $qualified = 3;
                        $qualify['valid'] = 1;
                        $qualify['occurence_rule_num'] = $qualified;
                        $qualify['occurence_rule'] = "non consecutive";
                        $qualify['related_details'] = $violation_details['violation_details'];
                        $qualify['supporting_details'] = $rule_checking_details['violation_details'];
                        $qualify['occurence_value'] = $violation_rules->nonConsecutiveVal;
                    }
                }
            }
        } else {
            $qualify['error'] = 1;
            $qualify['error_details'] = "no violation rules set";
        }
        return $qualify;
    }

    protected function update_user_dtr_violation($user_dtr_vio_id,  $data)
    {
        $where = "userDtrViolation_ID =  $user_dtr_vio_id";
        return  $this->general_model->update_vals($data,  $where, 'tbl_user_dtr_violation');
    }

    protected function get_shedule_shift($sched_id, $user_id)
    {
        $fields = "schedType.schedtype_id as schedTypeId, type, sched.sched_id as schedId, sched.sched_date schedDate, users.emp_id empId, users.uid userId, time_start, time_end, bta_id, accTime.acc_id as accountId";
        $where = "schedType.schedtype_id = sched.schedtype_id AND accTime.acc_time_id = sched.acc_time_id AND times.time_id = accTime.time_id AND sched.sched_id = $sched_id AND sched.emp_id = users.emp_id AND users.uid = $user_id";
        $tables = "tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users";
        return $this->general_model->fetch_specific_val($fields, $where, $tables);
    }

    protected function get_user_dtr_log($sched_id, $entry_type)
    {
        $fields = "dtr_id, emp_id, acc_time_id, entry, log, note, type, sched_id";
        $where = "sched_id = $sched_id AND type='DTR' AND entry = '$entry_type'";
        return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dtr_logs");
    }

    protected function add_user_dtr_violation_details($user_dtr_violation_details)
    {
        return $this->general_model->batch_insert($user_dtr_violation_details, 'tbl_dms_user_dtr_violation_details');
    }

    protected function new_format_midnight_shift_out($formatted_date)
    {
        $new_format_date = $formatted_date;
        if ($formatted_date == Date("Y-m-d", strtotime($formatted_date)) . " 00:00") {
            $new_format_date = Date("Y-m-d", strtotime($formatted_date . ' -1 days')) . " 24:00";
        }
        return $new_format_date;
    }

    protected function add_user_dtr_violation_allowable_break($allowable_break_details)
    {
        return $this->general_model->batch_insert($allowable_break_details, 'tbl_dms_user_dtr_violation_allowable_break');
	}
	
	protected function get_emp_account_details($emp_id)
	{
		$fields = "accounts.acc_id, accounts.acc_name, beforeCallingTime";
		$where = "accounts.acc_id = emp.acc_id AND emp.emp_id = $emp_id";
		$tables = "tbl_account accounts, tbl_employee emp";
		return $record = $this->general_model->fetch_specific_val($fields, $where, $tables);
	}

	public function get_breaks($login_id, $logout_id, $emp_id, $entry)
	{
		$fields = "dtr.dtr_id, dtr.emp_id, dtr.acc_time_id, dtr.entry, dtr.log, dtr.note, dtr.type, dtr.sched_id, break.break_type, bt.hour, bt.min";
		$where = "break.break_id = bta.break_id AND bt.btime_id = bta.btime_id AND bta.bta_id = shiftBreak.bta_id AND shiftBreak.sbrk_id = dtr.acc_time_id AND dtr.dtr_id > $login_id AND dtr.dtr_id < $logout_id AND dtr.emp_id = $emp_id AND dtr.type = 'Break' AND dtr.entry = '$entry'";
		$table = "tbl_dtr_logs dtr, tbl_shift_break shiftBreak, tbl_break_time_account bta, tbl_break_time bt, tbl_break break";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'dtr_id ASC');
	}

	protected function get_human_time_format($date_time_from, $date_time_to, $bct)
	{
		$diff = $date_time_to - $date_time_from;
		$hour = $diff / 3600;
		$hour_bct_diff = $hour - $bct;
		$time['hoursTotal'] = $hour;
		$time['hours'] = (int) $hour_bct_diff;
		$decimal = $hour_bct_diff - (int) $hour_bct_diff;
		$min = floor($diff /60);
		$seconds = $diff - $min * 60;
		$time['minutes'] = $min;
		$time['seconds'] = $seconds;
		return $time;
	}
	private function add_dms_sched($sched_details){
		$data['sched_id'] = $sched_details->sched_id;
		$data['emp_id'] = $sched_details->emp_id;
		$data['sched_date'] = $sched_details->sched_date;
		$data['schedtype_id'] = $sched_details->schedtype_id;
		$data['acc_time_id'] = $sched_details->acc_time_id;
		$data['updatedBy'] = $sched_details->updated_by;
		return $this->general_model->insert_vals($data, "tbl_dms_schedule");
	}

	protected function qry_sched_details($sched_id){
		$fields = "*";
		$where = "sched_id = $sched_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_schedule");
	}
	private function check_if_sched_recorded($sched_id, $emp_id){
		$fields = "*";
		$where = "sched_id = $sched_id AND emp_id = $emp_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_schedule");
	}

	private function record_orig_sched($sched_details){
		$check_recorded = $this->check_if_sched_recorded($sched_details->sched_id, $sched_details->emp_id);
		if(count($check_recorded) < 1){
			$this->add_dms_sched($sched_details);
		}
	}
    protected function get_violation_related_details($user_dtr_violation_obj)
    {
        $data['error'] = 0;
        $data['error_details'] = "";
        $data['valid'] = 0;
		$violation_details = "";
		$sched_details = $this->qry_sched_details($user_dtr_violation_obj->sched_ID);
		// var_dump($sched_details);
		if(count($sched_details) > 0){
			$this->record_orig_sched($sched_details);
		}
        $sched = $this->get_shedule_shift((int) $user_dtr_violation_obj->sched_ID, (int) $user_dtr_violation_obj->user_ID);
        if (count($sched) > 0) {
            if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //LATE
            {
                // DTR logs
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $login = new DateTime($login_raw->log);
                $login_formatted = $login->format("Y-m-d H:i");
                if (count($login_raw) > 0) {
                    $login_str = strtotime($login_formatted);
                    // DATED SCHED
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                    $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
                    $shift_start_str = strtotime($new_formatted_shift_start);
                    // Compute late duration
                    $duration = $this->get_human_time_format($shift_start_str, $login_str, 0);
                    $hrs = $duration['hoursTotal'];
                    $hrsToMin = $duration['hoursTotal'] * 60;
                    $shift_start_end = $sched->time_start . " - " . $sched->time_end;
                    if ($login_str > $shift_start_str) {
                        $data['valid'] = 1;
                        $violation_details = [
                            "sched_date" => $sched->schedDate,
                            "shift" => $shift_start_end,
                            "in_id" => $login_raw->dtr_id,
                            "in_log" => $login_formatted,
                            "shift_in" => $new_formatted_shift_start,
                            "duration" => $hrsToMin,
                            "human_time_format" => $duration
                        ];
                    } else {
                        $data['valid'] = 0;
                    }
                    $user_dtr_violation_details[0] = [
                        'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                        'shift' => $shift_start_end,
                        'dtr_id' => $login_raw->dtr_id,
                        'log' => $login_formatted,
                        'type' => "DTR",
                        'entry' => "I",
                        'minDuration' => 0
                    ];
                    $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                    $data['violation_details'] = $violation_details;
                    $user_data_vio['duration'] = $hrsToMin;
                    $user_data_vio['shift'] = $shift_start_end;
                    $user_data_vio['sched_date'] = $sched->schedDate;
                    $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
                } else {
                    $data['error'] = 1;
                    $data['error_details'] = [
                        'error' => "No login",
                        'origin' => 'Related Violation details function - (get_violation_related_details)',
                        'process' => 'getting of login and logout for late details'
                    ];
                }
            } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) // UT
            {
                // DTR logs
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');

                if (count($login_raw) > 0 && count($logout_raw) > 0) {
                    $login = new DateTime($login_raw->log);
                    $login_formatted = $login->format("Y-m-d H:i");
                    $logout = new DateTime($logout_raw->log);
                    $logout_formatted = $logout->format("Y-m-d H:i");

                    $login_str = strtotime($login_formatted);
                    $logout_str = strtotime($logout_formatted);
                    // DATED SCHED
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                    $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);

                    $shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
                    $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
                    $new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
                    $new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);
                    $shift_out_str = strtotime($new_formatted_shift_end);
                    $duration = $this->get_human_time_format($logout_str, $shift_out_str, 0);
                    $hrs = $duration['hoursTotal'];
                    $hrsToMin = $duration['hoursTotal'] * 60;

                    if ($shift_out_str > $logout_str) {
                        $data['valid'] = 1;
                        $shift_start_end = $sched->time_start . " - " . $sched->time_end;
                        $violation_details = [
                            "sched_date" => $sched->schedDate,
                            "shift" => $shift_start_end,
                            "out_id" => $logout_raw->dtr_id,
                            "out_log" => $logout_formatted,
                            "shif_out" => $formatted_shift_end,
                            "duration" => $hrsToMin,
                            "human_time_format" => $duration
                        ];
                    } else {
                        $data['valid'] = 0;
                    }
                    ////var_dump($data['valid']);
                    $user_dtr_violation_details[0] = [
                        'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                        'shift' => $shift_start_end,
                        'dtr_id' => $logout_raw->dtr_id,
                        'log' => $logout_formatted,
                        'type' => "DTR",
                        'entry' => "O",
                        'minDuration' => $hrsToMin
                    ];
                    $data['violation_details'] = $violation_details;
                    $user_data_vio['duration'] = $hrsToMin;
                    $user_data_vio['shift'] = $shift_start_end;
                    $user_data_vio['sched_date'] = $sched->schedDate;
                    $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                    $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
                } else {
                    $data['error'] = 1;
                    $data['error_details'] = [
                        'error' => "Incomplete Log",
                        'origin' => 'Related Violation details function - (get_violation_related_details)',
                        'process' => 'getting of login and logout for forgot to undertime details'
                    ];
                }
            } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) //FORGOT TO LOGOUT
            {
                // DTR logs
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');

                if (count($login_raw) > 0 && count($logout_raw) > 0) {
                    $login = new DateTime($login_raw->log);
                    $login_formatted = $login->format("Y-m-d H:i");
                    $logout = new DateTime($logout_raw->log);
                    $logout_formatted = $logout->format("Y-m-d H:i");
                    $login_str = strtotime($login_formatted);
                    $logout_str = strtotime($logout_formatted);

                    // DATED SCHED
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");

                    $shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
                    $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
                    $new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
                    $new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);
                    $shift_out_str = strtotime($new_formatted_shift_end);

                    $duration = $this->get_human_time_format($shift_out_str, $logout_str, 0);
                    $hrsToMin = $duration['hoursTotal'] * 60;
                    $shift_start_end = $sched->time_start . " - " . $sched->time_end;
                    if ($logout_str > $shift_out_str) {
                        $data['valid'] = 1;
                        $violation_details = [
                            "sched_date" => $sched->schedDate,
                            "shift" => $shift_start_end,
                            "out_id" => $logout_raw->dtr_id,
                            "out_log" => $logout_formatted,
                            "shift_out" => $formatted_shift_end,
                            "duration" => $hrsToMin,
                            "human_time_format" => $duration
                        ];
                    }
                    $user_dtr_violation_details[0] = [
                        'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                        'shift' => $shift_start_end,
                        'dtr_id' => $logout_raw->dtr_id,
                        'log' => $logout_formatted,
                        'type' => "DTR",
                        'entry' => "O",
                        'minDuration' => $hrsToMin
                    ];
                    $data['violation_details'] = $violation_details;
                    $user_data_vio['duration'] = $hrsToMin;
                    $user_data_vio['shift'] = $shift_start_end;
                    $user_data_vio['sched_date'] = $sched->schedDate;
                    $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                    $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
                } else {
                    $data['error'] = 1;
                    $data['error_details'] = [
                        'error' => "Incomplete Log",
                        'origin' => 'Related Violation details function - (get_violation_related_details)',
                        'process' => 'getting of login and logout for forgot to logout details'
                    ];
                }
            } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
            {
                // DTR logs
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
				$login = new DateTime($login_raw->log);
				$login_formatted = $login->format("Y-m-d H:i");
				$break_out_logs =[];
				$break_in_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
				$login_str = strtotime($login_formatted);
				$shift_start_end = $sched->time_start . " - " . $sched->time_end;
				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					$logout = new DateTime($logout_raw->log);
					$logout_formatted = $logout->format("Y-m-d H:i");
					$logout_str = strtotime($logout_formatted);
					$break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
					$break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');
					$user_dtr_violation_details[1] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $logout_raw->dtr_id,
						'log' => $logout_formatted,
						'type' => "DTR",
						'entry' => "O",
						'minDuration' => 0
					];
				}else if(count($logout_raw) < 1){
					$next_dtr_log = $this->get_next_dtr_log($login_raw->dtr_id, $sched->empId);
					if($next_dtr_log == NULL){
						$break_out_logs = $this->get_breaks($login_raw->dtr_id, $login_raw->dtr_id*1000, $sched->empId, 'O');
						$break_in_logs = $this->get_breaks($login_raw->dtr_id, $login_raw->dtr_id*1000, $sched->empId, 'I');
					}else{
						$break_out_logs = $this->get_breaks($login_raw->dtr_id, $next_dtr_log[0]->dtr_id, $sched->empId, 'O');
						$break_in_logs = $this->get_breaks($login_raw->dtr_id, $next_dtr_log[0]->dtr_id, $sched->empId, 'I');
					}
				}
				// var_dump($break_out_logs);
				// var_dump($break_in_logs);
				if (count($break_out_logs) > 0 && count($break_in_logs) > 0) {
					// DATED SCHED
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
					$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
					$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
					$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
					$new_formatted_shift_end = $this->new_format_midnight_shift_out($new_shifts['end']);
					$allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
					$emp_account_details = $this->get_emp_account_details($sched->empId);
					if ($allowable_breaks['breakData'] > 0) {
						// GET BREAK LOGS
						$hours = 0;
						$minutes = 0;
						$allow_count = 0;
						foreach ($allowable_breaks['breakData'] as $rows) {
							$hours = $rows->hour;
							$minutes = $rows->min;
							$hours_to_min = $hours * 60;
							$minutes += $hours_to_min;
							$dtr_vio_allowable_break[$allow_count] = [
								"userDtrViolation_ID" => $user_dtr_violation_obj->userDtrViolation_ID,
								"breakType" => $rows->break_type,
								"duration" => $minutes
							];
							$allow_count++;
						}
						$data['user_dtr_violation_allowable_break'] = $this->add_user_dtr_violation_allowable_break($dtr_vio_allowable_break);
						// $break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
						// $break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');
						
						$shift_start_end = $sched->time_start . " - " . $sched->time_end;
						$user_dtr_violation_details[0] = [
							'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
							'shift' => $shift_start_end,
							'dtr_id' => $login_raw->dtr_id,
							'log' => $login_formatted,
							'type' => "DTR",
							'entry' => "I",
							'minDuration' => 0
						];
						
						// BREAK COVERED
						$covered_breaks = [];
						$covered_breaks_count = 0;
						$total_covered_break = 0;
						$ob = 0;
						$user_dtr_vio_details_count = 2;
						if (count($break_out_logs) > 0) {
							foreach ($break_out_logs as $break_out_row) {
								$searchedValue = $break_out_row->dtr_id;
								$neededObject = array_merge(array_filter(
									$break_in_logs,
									function ($e) use ($searchedValue) {
										return $e->note == $searchedValue;
									}
								));
								$break_out_log = new DateTime($break_out_row->log);
								$formatted_break_out_log = $break_out_log->format("Y-m-d H:i:s");

								$user_dtr_violation_details[$user_dtr_vio_details_count] = [
									'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
									'shift' => $shift_start_end,
									'dtr_id' => $break_out_row->dtr_id,
									'log' => $formatted_break_out_log,
									'type' => $break_out_row->break_type,
									'entry' => "O",
									'minDuration' => 0
								];
								$user_dtr_vio_details_count++;
								if (count($neededObject[0]) > 0) {
									$break_in_log = new DateTime($neededObject[0]->log);
									$formatted_break_in_log = $break_in_log->format("Y-m-d H:i:s");

									$break_out_str = strtotime($formatted_break_out_log);
									$break_in_str = strtotime($formatted_break_in_log);

									$duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
									$break_hrs_to_min = $duration['hoursTotal'] * 60;
									$total_covered_break += $break_hrs_to_min;

									$covered_breaks[$covered_breaks_count] = [
										"sched_date" => $sched->schedDate,
										"shift" => $shift_start_end,
										"break_type" => $break_out_row->break_type,
										"out_id" => $break_out_row->dtr_id,
										"out_log" => $formatted_break_out_log,
										"in_id" => $neededObject[0]->dtr_id,
										"in_log" => $formatted_break_in_log,
										"minutes" => $break_hrs_to_min,
										"human_time_format" => $duration
									];
									$user_dtr_violation_details[$user_dtr_vio_details_count] = [
										'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
										'shift' => $shift_start_end,
										'dtr_id' => $neededObject[0]->dtr_id,
										'log' => $formatted_break_in_log,
										'type' => $neededObject[0]->break_type,
										'entry' => "I",
										'minDuration' => $break_hrs_to_min
									];
									$user_dtr_vio_details_count++;
									$covered_breaks_count++;
								}
							}
							$data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
							if ((int)$total_covered_break > (int)$allowable_breaks['minutes']) {
								$ob = (int)$total_covered_break - (int)$allowable_breaks['minutes'];
								$data['valid'] = 1;
								$violation_details = [
									"total_covered" => (int)$total_covered_break,
									"break_coverage_info" => $covered_breaks,
									"allowable_break" => $allowable_breaks,
									"over_break" => $ob
								];
								$data['violation_details'] = $violation_details;
							}
							$user_data_vio['duration'] = $ob;
							$user_data_vio['shift'] = $shift_start_end;
							$user_data_vio['sched_date'] = $sched->schedDate;
							$data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
						} else {
							$data['error'] = 1;
							$data['error_details'] = [
								'error' => "Incomplete Break logs",
								'origin' => 'Related Violation details function - (get_violation_related_details)',
								'process' => 'getting out for break logs'
							];
						}
					} else {
						$data['error'] = 1;
						$data['error_details'] = [
							'error' => "Assigned Breaks not found",
							'origin' => 'Related Violation details function - (get_violation_related_details)',
							'process' => 'getting of allowable break details'
						];
					}
				}else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "Incomplete Log",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for over break details'
					];
				}
            } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 6) // INC BREAK
            {
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
                if (count($login_raw) > 0 && count($logout_raw) > 0) {
                    // DATED SCHED
                    $login = new DateTime($login_raw->log);
                    $login_formatted = $login->format("Y-m-d H:i");
                    $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                    $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                    $logout = new DateTime($logout_raw->log);
                    $logout_formatted = $logout->format("Y-m-d H:i");
                    $shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
                    $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
                    $new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
                    $new_formatted_shift_end = $this->new_format_midnight_shift_out($new_shifts['end']);

                    $allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
                    $required_break_logs = count($allowable_breaks['breakData']) * 2;
                    $emp_account_details = $this->get_emp_account_details($sched->empId);
                    $shift_start_end = $sched->time_start . " - " . $sched->time_end;

                    $user_dtr_violation_details[0] = [
                        'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                        'shift' => $shift_start_end,
                        'dtr_id' => $login_raw->dtr_id,
                        'log' => $login_formatted,
                        'type' => "DTR",
                        'entry' => "I",
                        'minDuration' => 0
                    ];
                    $user_dtr_violation_details[1] = [
                        'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                        'shift' => $shift_start_end,
                        'dtr_id' => $logout_raw->dtr_id,
                        'log' => $logout_formatted,
                        'type' => "DTR",
                        'entry' => "O",
                        'minDuration' => 0
                    ];
                    $user_dtr_vio_details_count = 2;

                    if (count($allowable_breaks['breakData']) > 0) {
                        // GET BREAK LOGS
                        $hours = 0;
                        $minutes = 0;
                        $allow_count = 0;
                        foreach ($allowable_breaks['breakData'] as $rows) {
                            $hours = $rows->hour;
                            $minutes = $rows->min;
                            $hours_to_min = $hours * 60;
                            $minutes += $hours_to_min;
                            $dtr_vio_allowable_break[$allow_count] = [
                                "userDtrViolation_ID" => $user_dtr_violation_obj->userDtrViolation_ID,
                                "breakType" => $rows->break_type,
                                "duration" => $minutes
                            ];
                            $allow_count++;
                        }
                        $data['user_dtr_violation_allowable_break'] = $this->add_user_dtr_violation_allowable_break($dtr_vio_allowable_break);
                        $break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
                        $break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');

                        if (count($break_out_logs) > 0) {
                            foreach ($break_out_logs as $break_out_row) {
                                $searchedValue = $break_out_row->dtr_id;
                                $neededObject = array_merge(array_filter(
                                    $break_in_logs,
                                    function ($e) use ($searchedValue) {
                                        return $e->note == $searchedValue;
                                    }
                                ));
                                $break_out_log = new DateTime($break_out_row->log);
                                $formatted_break_out_log = $break_out_log->format("Y-m-d H:i");

                                $user_dtr_violation_details[$user_dtr_vio_details_count] = [
                                    'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                                    'shift' => $shift_start_end,
                                    'dtr_id' => $break_out_row->dtr_id,
                                    'log' => $formatted_break_out_log,
                                    'type' => $break_out_row->break_type,
                                    'entry' => $break_out_row->entry,
                                    'minDuration' => 0
                                ];
								$user_dtr_vio_details_count++;
								// var_dump($neededObject);
                                if(count($neededObject) > 0) {
                                    $break_in_log = new DateTime($neededObject[0]->log);
                                    $formatted_break_in_log = $break_in_log->format("Y-m-d H:i");

                                    $break_out_str = strtotime($formatted_break_out_log);
                                    $break_in_str = strtotime($formatted_break_in_log);

                                    $duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
                                    $break_hrs_to_min = $duration['hoursTotal'] * 60;

                                    $user_dtr_violation_details[$user_dtr_vio_details_count] = [
                                        'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                                        'shift' => $shift_start_end,
                                        'dtr_id' => $neededObject[0]->dtr_id,
                                        'log' => $formatted_break_in_log,
                                        'type' => $neededObject[0]->break_type,
                                        'entry' => "I",
                                        'minDuration' => $break_hrs_to_min
                                    ];
                                    $user_dtr_vio_details_count++;
                                }
                            }
                        } else {
                            $data['error'] = 1;
                            $data['error_details'] = [
                                'error' => "Incomplete Break logs",
                                'origin' => 'Related Violation details function - (get_violation_related_details)',
                                'process' => 'getting out for break logs'
                            ];
                        }
                        $user_data_vio['shift'] = $shift_start_end;
                        $user_data_vio['sched_date'] = $sched->schedDate;
                        $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
                        $recorded_break_logs = count($break_out_logs) + count($break_in_logs);
                        if ($recorded_break_logs != $required_break_logs) {
                            $data['valid'] = 1;
                            $data['violation_details'] = [
                                'allowable_break' => $allowable_breaks,
                                'break_out_logs' => $break_out_logs,
                                'break_in_logs' => $break_in_logs
                            ];
                        }
                    } else {
                        $data['error'] = 1;
                        $data['error_details'] = [
                            'error' => "Assigned Breaks not found",
                            'origin' => 'Related Violation details function - (get_violation_related_details)',
                            'process' => 'getting of allowable break details'
                        ];
                    }
                    $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                } else {
                    $data['error'] = 1;
                    $data['error_details'] = [
                        'error' => "Incomplete Log",
                        'origin' => 'Related Violation details function - (get_violation_related_details)',
                        'process' => 'getting of login and logout for over break details'
                    ];
                }
            } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 7) //INC DTR Logs
            {
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
                $shift_start_end = $sched->time_start . " - " . $sched->time_end;
                if (count($login_raw) > 0 && count($logout_raw) > 0) {
                    // "COMPLETE DTR LOGS";
                } else {
                    if (count($login_raw) > 0) {
                        $login = new DateTime($login_raw->log);
                        $login_formatted = $login->format("Y-m-d H:i");
                        $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                        $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                        $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
                        // $shift_start_end = $sched->time_start." - ".$sched->time_end;

                        $shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
                        $formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
                        $new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
                        $new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);

                        $allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
                        if ($allowable_breaks['breakData'] > 0) {
                            $hours = 0;
                            $minutes = 0;
                            $allow_count = 0;
                            foreach ($allowable_breaks['breakData'] as $rows) {
                                $hours = $rows->hour;
                                $minutes = $rows->min;
                                $hours_to_min = $hours * 60;
                                $minutes += $hours_to_min;
                                $dtr_vio_allowable_break[$allow_count] = [
                                    "userDtrViolation_ID" => $user_dtr_violation_obj->userDtrViolation_ID,
                                    "breakType" => $rows->break_type,
                                    "duration" => $minutes
                                ];
                                $allow_count++;
                            }
                            $data['user_dtr_violation_allowable_break'] = $this->add_user_dtr_violation_allowable_break($dtr_vio_allowable_break);
                            // $shift_start_end = $sched->time_start . " - " . $sched->time_end;
                            $user_dtr_violation_details[0] = [
                                'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                                'shift' => $shift_start_end,
                                'dtr_id' => $login_raw->dtr_id,
                                'log' => $login_formatted,
                                'type' => "DTR",
                                'entry' => "I",
                                'minDuration' => 0
                            ];
                            $user_dtr_vio_count = 1;
                            // BREAK lOGS WITHOUT Logout;
                            $break_logs_without_logout = $this->get_breaks_without_logout($login_raw->dtr_id, $new_formatted_shift_end, $sched->empId);
                            foreach ($break_logs_without_logout  as $break_logs_without_logout) {
                                $user_dtr_violation_details[$user_dtr_vio_count] = [
                                    'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
                                    'shift' => $shift_start_end,
                                    'dtr_id' => $break_logs_without_logout->dtr_id,
                                    'log' => $break_logs_without_logout->log,
                                    'type' => $break_logs_without_logout->break_type,
                                    'entry' => $break_logs_without_logout->entry,
                                    'minDuration' => 0
                                ];
                                $user_dtr_vio_count++;
                            }
                            $user_data_vio['shift'] = $shift_start_end;
                            $user_data_vio['sched_date'] = $sched->schedDate;
                            $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
                            $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
                        } else {
                            $data['error'] = 1;
                            $data['error_details'] = [
                                'error' => "Assigned Breaks not found",
                                'origin' => 'Related Violation details function - (get_violation_related_details)',
                                'process' => 'getting of allowable break details'
                            ];
                        }
                        $data['valid'] = 1;
                        $violation_details = [
                            "sched_date" => $sched->schedDate,
                            "shift" => $shift_start_end,
                            "in_id" => $login_raw->dtr_id,
                            "in_log" => $login_formatted,
                            "shift_in" => $new_formatted_shift_start,
                        ];
                        $data['violation_details'] = $violation_details;
                    } else {
                        $data['error'] = 1;
                        $data['error_details'] = [
                            'error' => "Login log not found",
                            'origin' => 'Related Violation details function - (get_violation_related_details)',
                            'process' => 'getting of log details'
                        ];
                    }
                }
            } else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 5) // ABSENT
            {
                $login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
                $logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
                $shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
                $formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
                $new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
                $shift_start_end = $sched->time_start . " - " . $sched->time_end;
                if (count($login_raw) == 0 && count($logout_raw) == 0) {
                    // "COMPLETE DTR LOGS";
                    $data['valid'] = 1;
                    $violation_details = [
                        "sched_date" => $sched->schedDate,
                        "shift" => $shift_start_end,
                    ];
                    $data['violation_details'] = $violation_details;
                }
                $user_data_vio['shift'] = $shift_start_end;
                $user_data_vio['sched_date'] = $sched->schedDate;
                $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
            }
        } else {
            $data['error'] = 1;
            $data['error_details'] = [
                'error' => "No Sched found",
                'origin' => 'Related Violation details function - (get_violation_related_details)',
                'process' => 'getting of schedule'
            ];
        }
        return $data;
    }

    public function check_qualified_dtr_violation($user_dtr_violation_id)
    {
        // 1687 - late 1693 -2, 1696
        // 1688 - ob 1694 -2 1698, 1699
        // 1689 - ut
        // 1690 - forgot to logout
        // 1691 - inc break 1695 -2
        // 1692 - inc dtr logs 1700
        // 1697 -absent
        $qualified = 0;
        $qualify['valid'] = 0;
        $qualify['error'] = 0;
        $qualify['details'] = "";
        $valid = 0;
        // 1687
        // $user_dtr_violation_id = 1713;
		// GET USER DTR VIOLATION
		// echo $user_dtr_violation_id;
		$user_dtr_violation = $this->get_user_dtr_violation($user_dtr_violation_id);
		// var_dump($user_dtr_violation);
		$subject_user_emp_details = $this->get_emp_details_via_user_id($user_dtr_violation->user_ID);
		// var_dump($subject_user_emp_details);
        $notif_supvervisor_bool = 0;
        if (count($user_dtr_violation) > 0) {
			$violation_details = $this->get_violation_related_details($user_dtr_violation); // check if violation is valid
			// var_dump($violation_details);
            if ($violation_details['valid']) //check if incident is valid
            {
                $qualify['valid'] = 1;
                $violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID); //Get violation rules and prescription (violation settings)
                if (($violation_rules->offense_ID == NULL) || ($violation_rules->category_ID == NULL)) {
                    $qualify['error'] = 1;
                    $qualify['details'] = "Either Default Offense or Default Category was not set for this violation type";
                } else {
                    $ongoing_prescriptive_stat_ir = $this->get_existing_ir($subject_user_emp_details->emp_id, 2, 2, $violation_rules->offense_ID);
                    $pending_qualified_exist = $this->check_if_pending_qualified_exist($violation_rules->offense_ID, $subject_user_emp_details->emp_id); // Check if pending qualified user dtr violation exist
                    if (count($ongoing_prescriptive_stat_ir) > 0) {
                        $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0);
                        $qualify['qualify_status'] = "A pending IR of the same offense exist";
                    } else if (count($pending_qualified_exist) > 0) {
                        $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0);
                        $qualify['qualify_status'] = "A pending qualified user dtr violation of same offense exist";
                    } else {
                        $ir_exist = $this->check_if_previous_ir_exist($user_dtr_violation, $subject_user_emp_details, $violation_rules); // Check if IR of the same violation exist
                        if ($ir_exist['exist']) // if a liable IR of current status and the same offense exist
                        {
                            $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2, 0, $user_dtr_violation_id, 0);
                            $qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
                            //auto_notify_ir_direct_sup 
                            $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules, $violation_details);
                            $qualify['qualify_status'] = "An IR of the same offense that has a currently ongoing prescriptive period exist";
                        } else {
                            $auto_ir_notify_rules = $this->check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules);
						
							////var_dump($auto_ir_notify_rules);
                            if ($auto_ir_notify_rules['valid']) {
								$ruleNum =  $auto_ir_notify_rules['occurence_rule_num'];
								$ruleNumVal =   $auto_ir_notify_rules['occurence_value'];
                                $qualifiedStat = 1;
                               
                                // $user_data['status_ID'] = 3;
                                // $qualify['user_dtr_violation_stat'] = $this->update_user_dtr_violation($user_dtr_violation_id, $user_data);
                                $qualify['qualify_status'] = "The User DTR Violation fits on a specific violation rule";
                            } else {
								$ruleNum =  0;
								$ruleNumVal =   0;
                                if ($user_dtr_violation->dtrViolationType_ID == 5) {
                                    $qualifiedStat = 1;
                                    $qualify['qualify_status'] = "The User DTR violation detected is Absent";
                                } else {
									$qualifiedStat = 0;
                                    $qualify['error_details'] = "Does not qualify Rules";
                                }
                            }
                            if($qualifiedStat){
                                $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2,$ruleNum , $user_dtr_violation_id,$ruleNumVal);
                                $qualify['valid'] = 1;
                                $qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
                                //auto_notify_ir_direct_sup
                                $qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules, $violation_details);
                            }
                        }
                    }
                }
            } else {
                $qualify['error_details'] = "Violation is not true";
                $qualify['related_details'] = $violation_details['error_details'];
                // set USER DTR VIOLATION to INVALID
                $user_data_vio['status_ID'] = 23;
                $qualify['user_dtr_update_to_invalid'] = $this->update_user_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data_vio);
            }
        } else {
            $qualify['error'] = 1;
            $qualify['error_details'] = "user dtr violation not found";
        }
         return $qualify;
    }
	 protected function new_format_midnight($formatted_date, $date)
    {
        
        $new_format_date = $formatted_date;
        if ($formatted_date == $date . " 00:00")
        {
            $new_format_date = $date . " 24:00";
        }
        return $new_format_date;
	}

	protected function get_all_past_current_cure_dates($dateTime)
	{
		$fields = "ir.incidentReport_ID, ir.subjectEmp_ID, ir.sourceEmp_ID, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.liabilityStat, ir.datedLiabilityStat, ir.subjectExplanation, ir.occurence, ir.offense_ID, ir.offense, ir.prescriptiveId, ir.disciplinaryActionCategorySettings_ID";
		$where = "ir.prescriptiveStat = 4 AND date(ir.prescriptionEnd) < '" . $dateTime . "'";
		$table = "tbl_dms_incident_report ir";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	public function set_past_cure_dates_to_completed()
	{
		$dateTime = $this->get_current_date_time();
		$curr_date = $dateTime['date'];
		// $curr_date = "2019-08-31";
		$past_irs = [];
		$current_cure_dates = $this->get_all_past_current_cure_dates($curr_date);
		if (count($current_cure_dates) > 0) {
			foreach ($current_cure_dates as $current_index => $current_row) {
				$past_irs[$current_index] = [
					"incidentReport_ID" => $current_row->incidentReport_ID,
					"prescriptiveStat" => 3
				];
			}
			$this->general_model->batch_update($past_irs, 'incidentReport_ID', 'tbl_dms_incident_report');
		}
	}
	// DMS FUNCTIONS (MICHAEL) END

	//PROCESS STREET FUNCTIONS (MICHAEL) START

	private function qry_due_coaching_log_confirmation()
	{
		$dateTime = $this->get_current_date_time();
		$fields = "dead.deadline_ID, dead.request_ID, dead.userId, dead.deadline";
		$where = "dead.request_ID = chklist.checklist_ID AND chklist.checklist_ID = ass.folProCheTask_ID AND ass.type = 'checklist' AND ass.assignmentRule_ID = 9 AND dead.module_ID = 5 AND dead.status_ID = 2 AND dead.userId = ass.assignedTo AND dead.deadline <='" . $dateTime['dateTime'] . "'";
		$table = "dead.deadline_ID, dead.request_ID, dead.userId, dead.deadline";
		return $this->fetch_specific_vals($fields, $where, $table);
	}

	public function check_confirmation_deadline(){
		$missed_due_confirmation = [];
		$dateTime = $this->get_current_date_time();
		$due_confirmation = $this->qry_due_coaching_log_confirmation();
		if(count($due_confirmation) > 0){
			foreach($due_confirmation as $due_confirmation_index=>$due_confirmation_row){
				$missed_due_confirmation[$due_confirmation_index] = [
					'confirmedBy' => $due_confirmation_row->userId,
					'checklist_ID' => $due_confirmation_row->request_ID,
					'status_ID' => 12,
					'dateConfirmed' => $dateTime['dateTime']
				];
			}
			if(count($missed_due_confirmation) > 0){
				$this->general_model->batch_insert($missed_due_confirmation, 'tbl_processST_checklist_confirmation');
			}
		}
	}
	//PROCESS STREET FUNCTIONS (MICHAEL) END

	// IREQUEST FUNCTIONS (MICHAEL) START
	public function get_pending_irequest_deadline(){
		$fields = "dead.deadline_ID, dead.module_ID, dead.request_ID, dead.requestor, dead.userId, app.fname, app.mname, app.lname, app.nameExt, app.email, dead.approvalLevel, dead.deadline, dead.status_ID, dead.datedStatus, appr.requestApproval_ID, appr.request_ID, appr.emp_ID, appr.status, appr.level, appr.remarks, appr.dated_status";
		$where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = dead.userId AND appr.requestApproval_ID = dead.request_ID AND dead.module_ID = 6 AND appr.status = 4 AND app.email IS NOT NULL";
		$table = "tbl_deadline dead, tbl_irequest_request_approval appr, tbl_applicant app, tbl_employee emp, tbl_user users";
		return  $this->general_model->fetch_specific_vals($fields,  $where, $table, "appr.level ASC");
	}

	protected function send_an_email($data, $sub, $recipient_email, $link, $system)
	{
		$this->email->initialize(array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_user' => 'notification@supportzebra.com',
			'smtp_pass' => 'spt2019!',
			'smtp_port' => 587,
			'smtp_crypto' => 'tls',
			'mailtype	' => 'html',
			'validate' => TRUE,
			'crlf' => "\r\n",
			'charset' => "iso-8859-1",
			'newline' => "\r\n"
		));
		$this->email->set_mailtype("html");
		$this->email->from('supportzebra@no-reply.com', $system);
		$this->email->to($recipient_email);
		// $this->email->cc('jeffrey@supportzebra.com');
		$this->email->subject($sub);
		$body = $this->load->view($link, $data, TRUE);
		// $this->email->attach($data['img2']);
		$this->email->message($body);
		$this->email->send();
		echo $this->email->print_debugger();
	}

	public function check_ten_hours_deadline(){
		$deadlines = $this->get_pending_irequest_deadline();
		$dateTime = $this->get_current_date_time();
		$current_time =  strtotime($dateTime['dateTime']);
		$email_subject = "Deadline on iRequest | ".date_format(date_create($dateTime['dateTime']), 'm/d/Y'). " 11:59 PM";
		$view_page_link = "templates/email/email_content";
		$recipient_email = [];
		$data = [];
		$data_count = 0;
		$for_email = [];
		$for_email_count = 0;
		foreach($deadlines as $deadlines_row){
			$deadline_time = strtotime($deadlines_row->deadline);
			$duration = $this->get_human_time_format($current_time, $deadline_time, 0);
			if($duration['hours'] <= 10){
				$for_email[$for_email_count] = $deadlines_row;
				$for_email_count++;
			}
		}
		if(count($for_email) > 0){
			$unique_users = array_merge(array_unique(array_column($for_email, 'userId')));
			if(count($unique_users) > 0){
				for($loop = 0; $loop < count($unique_users); $loop++){
					$user_id = $unique_users[$loop];
					$user_details = $this->get_emp_details_via_user_id($user_id);
					$user_deadlines = array_merge(array_filter($for_email, function ($e) use ($user_id) {
							return ($e->userId == $user_id);
						}
					));
					$data = [
						'deadline_count' => count($user_deadlines),
						'due_date' => date_format(date_create($dateTime['dateTime']), 'F d, Y'),
						'link' =>  base_url("irequest/my_requestapproval"),
					];
					// $this->send_an_email($data, $email_subject, $user_details->email, $view_page_link, 'iRequest');
					$this->send_an_email($data, $email_subject, "michael.sanchez@supportzebra.com", $view_page_link,  'iRequest');
				}
			}
		}
	}

	
	public function get_irequest_deadline(){
		$dateTime = $this->get_current_date_time();
		$fields = "dead.deadline_ID, dead.module_ID, dead.request_ID deadRequestId, dead.requestor, dead.userId, app.fname, app.mname, app.lname, app.nameExt, app.email, dead.approvalLevel, dead.deadline, dead.status_ID, dead.datedStatus, appr.requestApproval_ID, appr.request_ID, appr.emp_ID, appr.status, appr.level, appr.remarks, appr.dated_status";
		$where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = dead.userId AND appr.requestApproval_ID = dead.request_ID AND dead.module_ID = 6 AND dead.status_ID = 2 AND dead.deadline <= '".$dateTime['dateTime']."'";
		$table = "tbl_deadline dead, tbl_irequest_request_approval appr, tbl_applicant app, tbl_employee emp, tbl_user users";
		return  $this->general_model->fetch_specific_vals($fields,  $where, $table, "appr.level ASC");
	}

	public function get_irequest_details($irequest_ids){
		$fields = "irequest_ID, requesting_dept, position_ID, manpower_requested, manpower_supplied, hiringPurpose_ID, hiringPurpose_desc, educationalAttainment, gender, description, descriptionAttachment, qualifications, aftapproved, status, requested_by, dateRequested, dateStartRequired, dateEndRequired";
		$where = "irequest_ID IN ($irequest_ids)";
		return  $this->general_model->fetch_specific_vals($fields,  $where, 'tbl_irequest_manpower_request');
	}

	public function get_irequest_approval_details($irequest_ids){
		$fields = "appr.requestApproval_ID, appr.request_ID, appr.emp_ID, appr.status, appr.level, appr.remarks, appr.dated_status, users.uid ";
		$where = "users.emp_id = appr.emp_ID AND appr.request_ID IN ($irequest_ids)";
		$table = "tbl_irequest_request_approval appr, tbl_user users";
		return  $this->general_model->fetch_specific_vals($fields,  $where, $table);
	}


	public function check_irequest_deadline(){
		$deadline = $this->get_irequest_deadline();
		$missed_deadline = [];
		$missed_approver = [];
		$next_approver = [];
		$missed_irequest = [];
		$missed_count = 0;
		$next_count = 0;
		$missed_irequest_count = 0;
		if(count($deadline) > 0){
			$request_ids = implode(",", array_column($deadline, 'request_ID'));
			// var_dump($request_ids);
			$irequest_details = $this->get_irequest_details($request_ids);
			$irequest_approval_details = $this->get_irequest_approval_details($request_ids);
			// var_dump($irequest_details);
			foreach($deadline as $deadline_index=>$deadline_row){
				$irequest_id = $deadline_row->request_ID;
				$current_approval_level = $deadline_row->level;
				$next_approval_level = $current_approval_level + 1;
				// var_dump($deadline_row);
				// get request details
				$specific_irequest_details = array_merge(array_filter($irequest_details, function ($e) use ($irequest_id) {
						return (($e->irequest_ID == $irequest_id));
					}
				));
				if(count($specific_irequest_details) > 0){
					// notify missed
					$mssg = "You have missed to approve the Manpower Request No. ". str_pad($deadline_row->request_ID, 8, '0', STR_PAD_LEFT)." prior " . date("g:i A", strtotime($deadline_row->deadline)) . " last " . date("M j, Y", strtotime($deadline_row->deadline));
					$link = 'irequest/my_requestapproval';
					$this->simple_system_notification($mssg, $link, $deadline_row->userId);
					// set missed approver to missed status
					$missed_approver[$missed_count] = [
						'requestApproval_ID' => $deadline_row->requestApproval_ID,
						'status' => 12
					];
					// set deadline to missed
					$missed_deadline[$missed_count] = [
						'deadline_ID' => $deadline_row->deadline_ID,
						'status_ID' => 12
					];
					$missed_count++;
					// get next approver
					$specific_next_irequest_approver_details = array_merge(array_filter($irequest_approval_details, function ($e) use ($irequest_id, $next_approval_level) {
							return (($e->request_ID == $irequest_id) && ($e->level == $next_approval_level));
						}
					));
					// var_dump($specific_next_irequest_approver_details);
					if(count($specific_next_irequest_approver_details) > 0){
						// var_dump($specific_next_irequest_approver_details);
						// notify next approver
						$mssg = "Manpower Request No. ". str_pad($deadline_row->request_ID, 8, '0', STR_PAD_LEFT)." is automatically forwarded to you for approval because ".ucwords($deadline_row->fname  . " " . $deadline_row->lname)." was not able to approve it before " . date("g:i A", strtotime($deadline_row->deadline)) . " last " . date("M j, Y", strtotime($deadline_row->deadline));
						$link = 'irequest/my_requestapproval';
						$this->simple_system_notification($mssg, $link, $specific_next_irequest_approver_details[0]->uid);
						// set next approver to current
						$next_approver[$next_count] = [
							'requestApproval_ID' => $specific_next_irequest_approver_details[count($specific_next_irequest_approver_details) - 1]->requestApproval_ID,
							'status' => 4
						];
						$next_count++;
					}else{
						//set the main request to missed
						// var_dump('set whole request to missed');
						$missed_irequest[$missed_irequest_count] = [
							'irequest_ID' => $irequest_id,
							'status' => 12
						];
						$missed_irequest_count++;
					}
				}
			}
		}
		if(count($missed_deadline) > 0){
			$this->general_model->batch_update($missed_deadline, 'deadline_ID', 'tbl_deadline');
		}
		if(count($missed_approver) > 0){
			$this->general_model->batch_update($missed_approver, 'requestApproval_ID', 'tbl_irequest_request_approval');
		}
		if(count($next_approver) > 0){
			$this->general_model->batch_update($next_approver, 'requestApproval_ID', 'tbl_irequest_request_approval');
		}
		if(count($missed_irequest) > 0){
			$this->general_model->batch_update($missed_irequest, 'irequest_ID', 'tbl_irequest_manpower_request');
		}
	}
	// IREQUEST FUNCTIONS (MICHAEL) END
	// TITO DEADLINE CHECKER START
	public function get_due_tito_deadline(){
		$dateTime = $this->get_current_date_time();
		$date = $dateTime['date'];
		// $date = '2020-11-20';
		$fields = "app.fname, app.mname, app.lname, app.nameExt ,appr.dtrRequestApproval_ID, appr.dtrRequest_ID, appr.approvalLevel, appr.userId, appr.approvalStatus_ID, appr.dateTimeStatus, appr.remarks, dead.deadline_ID, dead.deadline, dead.status_ID, dead.datedStatus";
		$where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = appr.userId AND appr.dtrRequest_ID = dead.request_ID AND dead.module_ID = 3 AND dead.status_ID = 2 AND DATE(dead.deadline) <= '$date' AND appr.userId = dead.userId";
		$table = "tbl_applicant app, tbl_employee emp, tbl_user users, tbl_dtr_request_approval appr, tbl_deadline dead";
		return $this->fetch_specific_vals($fields, $where, $table, 'appr.approvalLevel ASC');
	}
	private function qry_tito_request_details($request_id){
		$fields = "req.dtrRequest_ID, req.userID, emp.emp_id, app.fname, app.mname, app.lname, app.nameExt, req.dateRequest, req.acc_time_id, req.message, req.status_ID, req.dateCreated, req.dateUpdated, req.updatedBy, req.actualDate, req.startDateWorked, req.startTimeWorked, req.endDateWorked, req.endTimeWorked, req.timetotal, req.withAhr";
		$where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = req.userID AND req.dtrRequest_ID IN ($request_id)";
		$table = "tbl_dtr_request req, tbl_applicant app, tbl_employee emp, tbl_user users";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}
	private function qry_tito_approval_details($request_id){
		$fields = "appr.dtrRequestApproval_ID, appr.dtrRequest_ID, appr.approvalLevel, appr.userId, emp.emp_id, app.fname, app.lname, app.mname, app.nameExt, appr.approvalStatus_ID, appr.dateTimeStatus, appr.remarks, dead.deadline_ID, dead.deadline, dead.status_ID, dead.datedStatus";
		$where = "dead.module_ID = 3 AND dead.request_ID = appr.dtrRequest_ID AND dead.userId = appr.userId AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = appr.userId AND appr.dtrRequest_ID IN ($request_id)";
		$table = "tbl_dtr_request_approval appr, tbl_employee emp, tbl_applicant app, tbl_user users, tbl_deadline dead";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}
	public function deadline_tito_checker(){
		$deadline = $this->get_due_tito_deadline();
		$dateTime = $this->get_current_date_time();
		$deadline_ids = implode(',', array_column($deadline, 'dtrRequest_ID'));
		$appr_update = [];
		$missed_deadine_approvers = [];
		$request_to_missed = [];
		$appr_update_count = 0;
		$appr_proceed = [];
		$appr_proceed_count = 0;
		$missed_deadine_approvers_count = 0;
		$request_to_missed_count = 0;
		if(count($deadline) > 0){
			$request_obj = $this->qry_tito_request_details($deadline_ids); // get request details
			$approval_obj = $this->qry_tito_approval_details($deadline_ids);// get approval details
			// get approval details
			foreach($deadline as $deadline_row){
				$current_appr_id = $deadline_row->dtrRequestApproval_ID;
				$appr_level = $deadline_row->approvalLevel;
				$request_id = $deadline_row->dtrRequest_ID;
				$specific_dtr_request = array_merge(array_filter($request_obj, function ($e) use ($request_id) {
					return $e->dtrRequest_ID == $request_id;
				}));
				$specific_dtr_request_appr = array_merge(array_filter($approval_obj, function ($e) use ($request_id) {
					return $e->dtrRequest_ID == $request_id;
				}));
				//SET CURRENT ON DUE TO MISSED
				$appr_update[$appr_update_count] = [
					'dtrRequestApproval_ID' => $deadline_row->dtrRequestApproval_ID,
					'approvalStatus_ID' => 12,
				];
				$missed_deadine_approvers[$missed_deadine_approvers_count] = [
					'deadline_ID' => $deadline_row->deadline_ID,
					'status_ID' => 12
				];
				$missed_deadine_approvers_count += 1;
				$appr_update_count += 1;
				//NOTIFY CURRENT ON DUE APPROVER
				$approval_level_ordinal = $this->ordinal($deadline_row->approvalLevel);
				$notif_mssg_requestor = "<i class='fa fa-info-circle'></i> " . $deadline_row->fname . " $deadline_row->lname, your $approval_level_ordinal level approver, was not able to give approval decision to your TITO request.<br><small><b>TITO-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
				$notif_mssg_due_appr = "<i class='fa fa-warning'></i> You have missed to give an approval decision as the $approval_level_ordinal level approver to the TITO request of ".$specific_dtr_request[0]->fname." ". $specific_dtr_request[0]->lname . " prior " . date("g:i A", strtotime($deadline_row->deadline)) . " last " . date("M j, Y", strtotime($deadline_row->deadline)) . ".<br><small><b>TITO-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
				$miss_appr_link = "tito/approval";
				$requestor_link = "tito/request";
				// echo $notif_mssg_requestor."<br>";
				// echo $notif_mssg_due_appr."<br>";
				$this->simple_system_notification($notif_mssg_requestor, $requestor_link, $specific_dtr_request[0]->userID);
				$this->simple_system_notification($notif_mssg_due_appr, $miss_appr_link, $deadline_row->userId);
				$max_appr = count($specific_dtr_request_appr);
				if($deadline_row->approvalLevel >= $max_appr){
					// var_dump("last");
					//SET REQUEST TO MISSED
					//notify requestor that the request escalated to general approver (HR or any assigned employee)
					$request_to_missed[$request_to_missed_count] = [
						'dtrRequest_ID' => $deadline_row->dtrRequest_ID,
						'status_ID' => 12,
						'dateUpdated' => $dateTime['dateTime']
					];
					$request_to_missed_count += 1;
				}else{
					//notify requestor that the request escalated to the next approver
					//NOTIFY NEXT APPROVER
					// var_dump('proceed');
					$next_appr_level = $deadline_row->approvalLevel + 1;
					$next_dtr_request_appr = array_merge(array_filter($approval_obj, function ($e) use ($request_id, $next_appr_level) {
						return (($e->dtrRequest_ID == $request_id) && ($e->approvalLevel == $next_appr_level));
					}));
					if(count($next_dtr_request_appr) > 0){
						$appr_proceed[$appr_proceed_count] = [
							'dtrRequestApproval_ID' => $next_dtr_request_appr[0]->dtrRequestApproval_ID,
							'approvalStatus_ID' => 4,
						];
						$appr_proceed_count += 1;
						$next_approval_level_ordinal = $this->ordinal($next_appr_level);
						$missed_approval_level_ordinal = $this->ordinal($deadline_row->approvalLevel);
						$next_appr_mssg = "<i class='fa fa-info-circle'></i> The TITO request of ".$specific_dtr_request[0]->fname." ". $specific_dtr_request[0]->lname . " was automatically forwarded to you for $next_approval_level_ordinal level approval because $deadline_row->fname " . $deadline_row->lname . " ($missed_approval_level_ordinal level) was not able to give a decision before the deadline: " . date("g:i A", strtotime($deadline_row->deadline)) . ", " . date("M j, Y", strtotime($deadline_row->deadline)) . ".<br><small>" . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
						$this->simple_system_notification($next_appr_mssg, $miss_appr_link, $next_dtr_request_appr[0]->userId);
						// echo $next_appr_mssg."<br>";
					}
				}
			}
			if(count($appr_proceed) > 0){
				$this->batch_update($appr_proceed, 'dtrRequestApproval_ID', 'tbl_dtr_request_approval');
			}
			if(count($appr_update) > 0){
				$this->batch_update($appr_update, 'dtrRequestApproval_ID', 'tbl_dtr_request_approval');
			}
			if(count($missed_deadine_approvers) > 0){
				$this->batch_update($missed_deadine_approvers, 'deadline_ID', 'tbl_deadline');
			}
			if(count($request_to_missed) > 0){
				$this->batch_update($request_to_missed, 'dtrRequest_ID', 'tbl_dtr_request');
			}
		}
	}
	public function deadline_tito_checker_test(){
		$deadline = $this->get_due_tito_deadline();
		$dateTime = $this->get_current_date_time();
		$deadline_ids = implode(',', array_column($deadline, 'dtrRequest_ID'));
		$appr_update = [];
		$missed_deadine_approvers = [];
		$request_to_missed = [];
		$appr_update_count = 0;
		$appr_proceed = [];
		$appr_proceed_count = 0;
		$missed_deadine_approvers_count = 0;
		$request_to_missed_count = 0;
		// if(count($deadline) > 0){
		// 	$request_obj = $this->qry_tito_request_details($deadline_ids); // get request details
		// 	$approval_obj = $this->qry_tito_approval_details($deadline_ids);// get approval details
		// 	// get approval details
		// 	foreach($deadline as $deadline_row){
		// 		$current_appr_id = $deadline_row->dtrRequestApproval_ID;
		// 		$appr_level = $deadline_row->approvalLevel;
		// 		$request_id = $deadline_row->dtrRequest_ID;
		// 		$specific_dtr_request = array_merge(array_filter($request_obj, function ($e) use ($request_id) {
		// 			return $e->dtrRequest_ID == $request_id;
		// 		}));
		// 		$specific_dtr_request_appr = array_merge(array_filter($approval_obj, function ($e) use ($request_id) {
		// 			return $e->dtrRequest_ID == $request_id;
		// 		}));
		// 		//SET CURRENT ON DUE TO MISSED
		// 		$appr_update[$appr_update_count] = [
		// 			'dtrRequestApproval_ID' => $deadline_row->dtrRequestApproval_ID,
		// 			'approvalStatus_ID' => 12,
		// 		];
		// 		$missed_deadine_approvers[$missed_deadine_approvers_count] = [
		// 			'deadline_ID' => $deadline_row->deadline_ID,
		// 			'status_ID' => 12
		// 		];
		// 		$missed_deadine_approvers_count += 1;
		// 		$appr_update_count += 1;
		// 		//NOTIFY CURRENT ON DUE APPROVER
		// 		$approval_level_ordinal = $this->ordinal($deadline_row->approvalLevel);
		// 		$notif_mssg_requestor = "<i class='fa fa-info-circle'></i> " . $deadline_row->fname . " $deadline_row->lname, your $approval_level_ordinal level approver, was not able to give approval decision to your TITO request.<br><small><b>TITO-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
		// 		$notif_mssg_due_appr = "<i class='fa fa-warning'></i> You have missed to give an approval decision as the $approval_level_ordinal level approver to the TITO request of ".$specific_dtr_request[0]->fname." ". $specific_dtr_request[0]->lname . " prior " . date("g:i A", strtotime($deadline_row->deadline)) . " last " . date("M j, Y", strtotime($deadline_row->deadline)) . ".<br><small><b>TITO-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
		// 		$miss_appr_link = "tito/approval";
		// 		$requestor_link = "tito/request";
		// 		// echo $notif_mssg_requestor."<br>";
		// 		// echo $notif_mssg_due_appr."<br>";
		// 		$this->simple_system_notification($notif_mssg_requestor, $requestor_link, $specific_dtr_request[0]->userID);
		// 		$this->simple_system_notification($notif_mssg_due_appr, $miss_appr_link, $deadline_row->userId);
		// 		$max_appr = count($specific_dtr_request_appr);
		// 		if($deadline_row->approvalLevel >= $max_appr){
		// 			// var_dump("last");
		// 			//SET REQUEST TO MISSED
		// 			//notify requestor that the request escalated to general approver (HR or any assigned employee)
		// 			$request_to_missed[$request_to_missed_count] = [
		// 				'dtrRequest_ID' => $deadline_row->dtrRequest_ID,
		// 				'status_ID' => 12,
		// 				'dateUpdated' => $dateTime['dateTime']
		// 			];
		// 			$request_to_missed_count += 1;
		// 		}else{
		// 			//notify requestor that the request escalated to the next approver
		// 			//NOTIFY NEXT APPROVER
		// 			// var_dump('proceed');
		// 			$next_appr_level = $deadline_row->approvalLevel + 1;
		// 			$next_dtr_request_appr = array_merge(array_filter($approval_obj, function ($e) use ($request_id, $next_appr_level) {
		// 				return (($e->dtrRequest_ID == $request_id) && ($e->approvalLevel == $next_appr_level));
		// 			}));
		// 			if(count($next_dtr_request_appr) > 0){
		// 				$appr_proceed[$appr_proceed_count] = [
		// 					'dtrRequestApproval_ID' => $next_dtr_request_appr[0]->dtrRequestApproval_ID,
		// 					'approvalStatus_ID' => 4,
		// 				];
		// 				$appr_proceed_count += 1;
		// 				$next_approval_level_ordinal = $this->ordinal($next_appr_level);
		// 				$missed_approval_level_ordinal = $this->ordinal($deadline_row->approvalLevel);
		// 				$next_appr_mssg = "<i class='fa fa-info-circle'></i> The TITO request of ".$specific_dtr_request[0]->fname." ". $specific_dtr_request[0]->lname . " was automatically forwarded to you for $next_approval_level_ordinal level approval because $deadline_row->fname " . $deadline_row->lname . " ($missed_approval_level_ordinal level) was not able to give a decision before the deadline: " . date("g:i A", strtotime($deadline_row->deadline)) . ", " . date("M j, Y", strtotime($deadline_row->deadline)) . ".<br><small>" . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
		// 				$this->simple_system_notification($next_appr_mssg, $miss_appr_link, $next_dtr_request_appr[0]->userId);
		// 				// echo $next_appr_mssg."<br>";
		// 			}
		// 		}
		// 	}
		// 	if(count($appr_proceed) > 0){
		// 		$this->batch_update($appr_proceed, 'dtrRequestApproval_ID', 'tbl_dtr_request_approval');
		// 	}
		// 	if(count($appr_update) > 0){
		// 		$this->batch_update($appr_update, 'dtrRequestApproval_ID', 'tbl_dtr_request_approval');
		// 	}
		// 	if(count($missed_deadine_approvers) > 0){
		// 		$this->batch_update($missed_deadine_approvers, 'deadline_ID', 'tbl_deadline');
		// 	}
		// 	if(count($request_to_missed) > 0){
		// 		$this->batch_update($request_to_missed, 'dtrRequest_ID', 'tbl_dtr_request');
		// 	}
		// }
	}
	// TITO DEADLINE CHECkER END
	// RECRUITMENT CHECKER
	private function get_recruitment_status(){
        $query = "SELECT req.irequest_ID, req.requesting_dept, req.position_ID, req.manpower_requested, req.manpower_supplied, req.datedStatus, rec.irequestRecruitment_ID, rec.status_ID, COUNT(app.irecruitApplicant_ID) hired FROM tbl_irequest_recruitment rec, tbl_irequest_manpower_request req, tbl_irecruit_applicant app WHERE req.irequest_ID = rec.irequest_ID AND app.irequestRecruitment_ID = rec.irequestRecruitment_ID AND rec.status_ID = 2 GROUP BY rec.irequestRecruitment_ID";
		return $this->general_model->custom_query($query);
	}
	public function check_recruitment_status(){
		$dateTime = $this->get_current_date_time();
		$rec_stat = $this->get_recruitment_status();
		$due_recruitment = [];
		$due_recruitment_count = 0;
		if(count($rec_stat) > 0){
			foreach($rec_stat as $rec_stat_row){
				$from = strtotime($rec_stat_row->datedStatus);
                $to = strtotime($dateTime['dateTime']);
				$duration = $this->get_human_time_format($from, $to, 0);
				if($duration['hoursTotal']/24 > 30){
					if($rec_stat_row->manpower_requested !== $rec_stat_row->hired){
						$due_recruitment[$due_recruitment_count] = [
							"irequestRecruitment_ID" => $rec_stat_row->irequestRecruitment_ID,
							"status_ID" => 16
						];
						$due_recruitment_count++;
					}
				}
			}
		}
		if(count($due_recruitment) > 0){
			$this->batch_update($due_recruitment, 'irequestRecruitment_ID', 'tbl_irequest_recruitment');
		}
	}
	// RECRUITMENT CHECKER END
}
