<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Announcement extends General
{

    protected $title = 'Announcement';

    public function __construct()
    {
        parent::__construct();
    }
    public function my_announcements()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'My Announcements',
        ];
        if($this->check_access()){
        $this->load_template_view('templates/announcement/my_announcements', $data);
        }        
    }
    public function announcement_list()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Announcement List',
        ];
    
        $this->load_template_view('templates/announcement/announcement_list', $data);
               
    }
    public function get_my_announcements()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $searchSubject = $this->input->post('searchSubject');
        $searchHighlight = $this->input->post('searchHighlight');
        $emp_id = $this->session->emp_id;
        $search = array();
        $searchString = "";
        if ($searchSubject !== '') {
            $search[] = "subject LIKE '%$searchSubject%'";
        }
        if ($searchHighlight !== '') {
            $search[] = "highlightType = '$searchHighlight'";
        }

        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }

        $query = "FROM tbl_announcement WHERE createdBy = $emp_id $searchString ORDER BY announcement_ID DESC";
        $announcements = $this->general_model->custom_query("SELECT * " . $query . " LIMIT $limiter,$perPage");
        $announcement_IDS = array();
        $individuals = array();
        $departments = array();
        $accounts = array();
        foreach ($announcements as $ann) {
            $announcement_IDS[] = $ann->announcement_ID;
            if ($ann->announcerType == 'account') {
                $accounts[] = $ann->announcer_ID;
            } else if ($ann->announcerType == 'department') {
                $departments[] = $ann->announcer_ID;
            } else {
                $individuals[] = $ann->announcer_ID;
            }
        }
        $accounts = (empty($accounts)) ? [0] : array_unique($accounts);
        $departments = (empty($departments)) ? [0] : array_unique($departments);
        $individuals = (empty($individuals)) ? [0] : array_unique($individuals);
        $fetchAccounts = $this->general_model->fetch_specific_vals("acc_name as announcer, acc_id as announcer_ID, 'account' as type", "acc_id IN (" . implode(',', $accounts) . ")", "tbl_account");
        $fetchDepartments = $this->general_model->fetch_specific_vals("dep_details as announcer, dep_id as announcer_ID, 'department' as type", "dep_id IN (" . implode(',', $departments) . ")", "tbl_department");
        $fetchIndividuals = $this->general_model->fetch_specific_vals("CONCAT(a.fname,' ',a.lname) as announcer, b.emp_id as announcer_ID, 'individual' as type", "a.apid = b.apid AND b.emp_id IN (" . implode(',', $individuals) . ")", "tbl_applicant a,tbl_employee b");
        $announcers = array_merge($fetchAccounts, $fetchDepartments, $fetchIndividuals);
        if (!empty($announcement_IDS)) {
            $attachments = $this->general_model->custom_query("SELECT * FROM tbl_announcement_attachments WHERE announcement_ID IN (" . implode(',', $announcement_IDS) . ")");
            foreach ($announcements as $ann) {
                $ann->attachments = array();
                $ann->announcer = '';
                foreach ($attachments as $key => $attach) {
                    if ($ann->announcement_ID == $attach->announcement_ID) {
                        $ann->attachments[] = $attach;
                        unset($attachments[$key]);
                    }
                }
                foreach ($announcers as $announcer) {
                    if ($ann->announcerType == $announcer->type && $ann->announcer_ID == $announcer->announcer_ID) {
                        $ann->announcer = $announcer->announcer;
                    }
                }
            }
        }
        $allannouncements = $this->general_model->custom_query("SELECT COUNT(*) as count " . $query);
        $count = $allannouncements[0]->count;
        echo json_encode(array('announcements' => $announcements, 'total' => $count));
    }
    public function create_new_announcement()
    {
        $message        =   $this->input->post('message');
        $title          =   $this->input->post('title');
        $durationstart  =   $this->input->post('durationstart');
        $durationend    =   $this->input->post('durationend');
        $highlight      =   $this->input->post('highlight');
        $announcer      =   $this->input->post('announcer');
        $list_of_emp_id =   $this->input->post('list_of_emp_id');
        $list_of_acc_id =   $this->input->post('list_of_acc_id');
        if ($announcer === 'account') {
            $announcer_ID =  $this->session->acc_id;
        } else if ($announcer === 'department') {
            $announcer_ID =  $this->general_model->fetch_specific_val("dep_id", "acc_id=" . $this->session->acc_id, "tbl_account")->dep_id;
        } else {
            $announcer_ID = $this->session->emp_id;
        }
        $data = ['subject' => $title, 'message' => $message, 'announcer_ID' => $announcer_ID, 'announcerType' => $announcer, 'highlightType' => $highlight, 'startDate' => $durationstart, 'endDate' => $durationend, 'createdBy' => $this->session->emp_id, 'list_of_emp_id' => $list_of_emp_id, 'list_of_acc_id' => $list_of_acc_id];
        $res = $this->general_model->insert_vals($data, "tbl_announcement");
        $status = ($res) ? "Success" : "Failed";
        echo json_encode(['status' => $status]);
    }
    public function update_announcement()
    {
        $message =   $this->input->post('message');
        $title =   $this->input->post('title');
        $durationstart =   $this->input->post('durationstart');
        $durationend =   $this->input->post('durationend');
        $highlight =   $this->input->post('highlight');
        $announcer =   $this->input->post('announcer');
        $announcement_ID = $this->input->post('announcement_ID');
        $list_of_emp_id = $this->input->post('list_of_emp_id');
        $list_of_acc_id = $this->input->post('list_of_acc_id');
        if ($announcer === 'account') {
            $announcer_ID =  $this->session->acc_id;
        } else if ($announcer === 'department') {
            $announcer_ID =  $this->general_model->fetch_specific_val("dep_id", "acc_id=" . $this->session->acc_id, "tbl_account")->dep_id;
        } else {
            $announcer_ID = $this->session->emp_id;
        }
        $data = ['subject' => $title, 'message' => $message, 'announcer_ID' => $announcer_ID, 'announcerType' => $announcer, 'highlightType' => $highlight, 'startDate' => $durationstart, 'endDate' => $durationend, 'list_of_emp_id' => $list_of_emp_id, 'list_of_acc_id' => $list_of_acc_id];

        $res = $this->general_model->update_vals($data, "announcement_ID=$announcement_ID", "tbl_announcement");
        $status = ($res) ? "Success" : "Failed";
        echo json_encode(['status' => $status]);
    }
    public function delete_announcement()
    {
        $announcement_ID =  $this->input->post('announcement_ID');
        $res = $this->general_model->delete_vals("announcement_ID=$announcement_ID", "tbl_announcement");
        $status = ($res) ? "Success" : "Failed";
        echo json_encode(['status' => $status]);
    }
    public function change_announcement_status()
    {
        $isEnabled = $this->input->post('isEnabled');
        $announcement_ID =  $this->input->post('announcement_ID');
        $isEnabled = ($isEnabled==='true') ? 1 : 0;
        $data = ['isEnabled' => $isEnabled];
        $res = $this->general_model->update_vals($data, "announcement_ID=$announcement_ID", "tbl_announcement");
        $status = ($res) ? "Success" : "Failed";
        echo json_encode(['status' => $status]);
    }
    public function dashboard_announcements()
    {
        $emp_id = $this->session->emp_id;
        $acc_id = $this->session->acc_id;
        $announcements = $this->general_model->custom_query("SELECT * FROM tbl_announcement WHERE (find_in_set($emp_id,list_of_emp_id) OR find_in_set($acc_id,list_of_acc_id) OR (list_of_acc_id='' AND list_of_emp_id='' )) AND DATE(NOW()) between startDate and endDate AND isEnabled=1 ORDER BY announcement_ID DESC");
        $individuals = array();
        $departments = array();
        $accounts = array();
        foreach ($announcements as $ann) {
            // $ann->message = addslashes( $ann->message);
            if ($ann->announcerType == 'account') {
                $accounts[] = $ann->announcer_ID;
            } else if ($ann->announcerType == 'department') {
                $departments[] = $ann->announcer_ID;
            } else {
                $individuals[] = $ann->announcer_ID;
            }
        }
        $accounts = (empty($accounts)) ? [0] : array_unique($accounts);
        $departments = (empty($departments)) ? [0] : array_unique($departments);
        $individuals = (empty($individuals)) ? [0] : array_unique($individuals);
        $fetchAccounts = $this->general_model->fetch_specific_vals("acc_name as announcer, acc_id as announcer_ID, 'account' as type", "acc_id IN (" . implode(',', $accounts) . ")", "tbl_account");
        $fetchDepartments = $this->general_model->fetch_specific_vals("dep_details as announcer, dep_id as announcer_ID, 'department' as type", "dep_id IN (" . implode(',', $departments) . ")", "tbl_department");
        $fetchIndividuals = $this->general_model->fetch_specific_vals("CONCAT(a.fname,' ',a.lname) as announcer, b.emp_id as announcer_ID, 'individual' as type", "a.apid = b.apid AND b.emp_id IN (" . implode(',', $individuals) . ")", "tbl_applicant a,tbl_employee b");
        $announcers = array_merge($fetchAccounts, $fetchDepartments, $fetchIndividuals);
        if (!empty($announcers)) {
            foreach ($announcements as $ann) {
                $ann->announcer = '';
                foreach ($announcers as $announcer) {
                    if ($ann->announcerType == $announcer->type && $ann->announcer_ID == $announcer->announcer_ID) {
                        $ann->announcer = $announcer->announcer;
                    }
                }
            }
        }
        echo json_encode(['announcements' => $announcements]);
    }

    public function get_all_announcements()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $searchSubject = $this->input->post('searchSubject');
        $searchHighlight = $this->input->post('searchHighlight');
        $emp_id = $this->session->emp_id;
        $search = array();
        $searchString = "";
        if ($searchSubject !== '') {
            $search[] = "subject LIKE '%$searchSubject%'";
        }
        if ($searchHighlight !== '') {
            $search[] = "highlightType = '$searchHighlight'";
        }

        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }

        $query = "FROM tbl_announcement WHERE isEnabled=1 $searchString ORDER BY announcement_ID DESC";
        $announcements = $this->general_model->custom_query("SELECT * " . $query . " LIMIT $limiter,$perPage");
        $announcement_IDS = array();
        $individuals = array();
        $departments = array();
        $accounts = array();
        foreach ($announcements as $ann) {
            $announcement_IDS[] = $ann->announcement_ID;
            if ($ann->announcerType == 'account') {
                $accounts[] = $ann->announcer_ID;
            } else if ($ann->announcerType == 'department') {
                $departments[] = $ann->announcer_ID;
            } else {
                $individuals[] = $ann->announcer_ID;
            }
        }
        $accounts = (empty($accounts)) ? [0] : array_unique($accounts);
        $departments = (empty($departments)) ? [0] : array_unique($departments);
        $individuals = (empty($individuals)) ? [0] : array_unique($individuals);
        $fetchAccounts = $this->general_model->fetch_specific_vals("acc_name as announcer, acc_id as announcer_ID, 'account' as type", "acc_id IN (" . implode(',', $accounts) . ")", "tbl_account");
        $fetchDepartments = $this->general_model->fetch_specific_vals("dep_details as announcer, dep_id as announcer_ID, 'department' as type", "dep_id IN (" . implode(',', $departments) . ")", "tbl_department");
        $fetchIndividuals = $this->general_model->fetch_specific_vals("CONCAT(a.fname,' ',a.lname) as announcer, b.emp_id as announcer_ID, 'individual' as type", "a.apid = b.apid AND b.emp_id IN (" . implode(',', $individuals) . ")", "tbl_applicant a,tbl_employee b");
        $announcers = array_merge($fetchAccounts, $fetchDepartments, $fetchIndividuals);
        if (!empty($announcement_IDS)) {
            $attachments = $this->general_model->custom_query("SELECT * FROM tbl_announcement_attachments WHERE announcement_ID IN (" . implode(',', $announcement_IDS) . ")");
            foreach ($announcements as $ann) {
                $ann->attachments = array();
                $ann->announcer = '';
                foreach ($attachments as $key => $attach) {
                    if ($ann->announcement_ID == $attach->announcement_ID) {
                        $ann->attachments[] = $attach;
                        unset($attachments[$key]);
                    }
                }
                foreach ($announcers as $announcer) {
                    if ($ann->announcerType == $announcer->type && $ann->announcer_ID == $announcer->announcer_ID) {
                        $ann->announcer = $announcer->announcer;
                    }
                }
            }
        }
        $allannouncements = $this->general_model->custom_query("SELECT COUNT(*) as count " . $query);
        $count = $allannouncements[0]->count;
        echo json_encode(array('announcements' => $announcements, 'total' => $count));
    }

    public function group_display()
    {
        $check = ($this->input->post('check') != '') ? $this->input->post('check') : 0;
        $query = "
        SELECT acc_id, acc_name, if(find_in_set(acc_id, '$check'), 1, 0) checked
        FROM tbl_account
        ORDER BY checked DESC, acc_name ASC";
        $data = $this->general_model->custom_query($query);
        echo json_encode($data);
    }

    public function individual_display()
    {
        $check = ($this->input->post('check') != '') ? $this->input->post('check') : 0;
        $query = "
        SELECT emp.emp_id, app.lname, app.fname, app.mname, if(find_in_set(emp.emp_id,'$check'),1,0) checked
        FROM tbl_employee emp, tbl_applicant app
        WHERE app.apid=emp.apid AND emp.isActive='yes'
        ORDER BY checked DESC, app.lname ASC";
        $data = $this->general_model->custom_query($query);
        echo json_encode($data);
    }

    function general_upload_photo()
    {
        if (isset($_FILES["image"]["name"])) {
            $config['upload_path'] = './uploads/announcement/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('image')) {
                $this->upload->display_errors();
                return FALSE;
            } else {
                $data = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = './uploads/announcement/' . $data['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '100%';
                // $config['width'] = 800;
                // $config['height'] = 800;
                $config['new_image'] = './uploads/announcement/' . $data['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                echo base_url() . 'uploads/announcement/' . $data['file_name'];
            }
        }
    }

    function delete_image()
    {
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if (unlink($file_name)) {
            echo 'File Delete Successfully';
        }
    }
}
