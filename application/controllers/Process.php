<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Process extends General
{
	protected $title = 'Process';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('process_model');
	}
	public function index()
	{
		$data = $this->user_infos();
		$userid = $this->session->userdata('uid');
		$data['processes'] = $this->process_model->get_allprocess();
		$data['folders'] = $this->general_model->fetch_specific_vals("folder.*, app.*", "folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1' AND folder.status_ID!='15' AND folder.isRemoved!=1", "tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee", "folder.folderName ASC");
		$data['checklists'] = $this->general_model->fetch_all("*", "tbl_processST_checklist", "checklistTitle ASC");
		$data['account'] = $this->general_model->fetch_all("*", "tbl_account", "acc_name ASC");
		$data['members'] = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description", "emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes' AND user.emp_id=emp.emp_id AND user.uid!=$userid", "tbl_applicant ap, tbl_employee emp,tbl_account acc, tbl_user user", "ap.lname ASC");
		$data['user'] = $userid;
		$data['user_info'] = $this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname", "user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid", "tbl_applicant applicant, tbl_employee employee, tbl_user user");
		$this->load_template_view('templates/process/process', $data);
	}
	public function control_panel()
	{
		$data = $this->user_infos();
		if ($this->check_access_privilege()) {
			$this->load_template_view('templates/process/control_panel', $data);
		}
	}
	public function archived()
	{
		$data = $this->user_infos();
		$this->load_template_view('templates/process/archived', $data);
		// if($this->check_access_privilege()){
		// 	$this->load_template_view('templates/process/archived', $data);
		// }
	}
	//ASSIGNED TO USER
	public function get_assignedchecklist_duedate_notif()
	{
		$dateTime = $this->get_current_date_time();
		$duedate = [];
		$userid = $this->session->userdata('uid');
		return $this->general_model->fetch_specific_vals("ass.assign_ID,ass.assignedTo,ass.assignedBy,checklist.dueDate,checklist.checklist_ID,checklist.checklistTitle,checklist.status_ID", "checklist.checklist_ID=ass.folProCheTask_ID AND ass.type='checklist' AND ass.assignedTo=$userid AND checklist.dueDate IS NOT NULL AND DATE(checklist.dueDate)<='" . $dateTime['date'] . "' AND checklist.status_ID=2", "tbl_processST_assignment ass, tbl_processST_checklist checklist");
	}
	//ASSIGNED BY USER
	public function get_assignedBychecklist_duedate_notif()
	{
		$dateTime = $this->get_current_date_time();
		$duedate = [];
		$userid = $this->session->userdata('uid');
		return $this->general_model->fetch_specific_vals("ass.assign_ID,ass.assignedTo,ass.assignedBy,checklist.dueDate,checklist.checklist_ID,checklist.checklistTitle,checklist.status_ID", "checklist.checklist_ID=ass.folProCheTask_ID AND ass.type='checklist' AND ass.assignedBy=$userid AND checklist.dueDate IS NOT NULL AND DATE(checklist.dueDate)<='" . $dateTime['date'] . "' AND checklist.status_ID=2", "tbl_processST_assignment ass, tbl_processST_checklist checklist");
	}

	//ADMIN PERSPECTIVE
	// public function get_
	public function get_assignedtask_duedate_notif()
	{
		$dateTime = $this->get_current_date_time();
		$userid = $this->session->userdata('uid');
		return $this->general_model->fetch_specific_vals("ass.assign_ID,ass.assignedTo,ass.assignedBy,checktask.taskdueDate,checklist.checklistTitle,checklist.checklist_ID,checktask.task_ID,checktask.isCompleted,task.taskTitle,checktask.checklistStatus_ID", "checklist.checklist_ID=checktask.checklist_ID AND checktask.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedTo=$userid AND checktask.taskdueDate IS NOT NULL AND DATE(checktask.taskdueDate)<='" . $dateTime['date'] . "' AND checktask.isCompleted=2 AND task.task_ID=checktask.task_ID AND checktask.task_status IS NULL", "tbl_processST_assignment ass, tbl_processST_checklist checklist,tbl_processST_checklistStatus checktask,tbl_processST_task task");
	}
	public function get_assignedBytask_duedate_notif()
	{
		$dateTime = $this->get_current_date_time();
		$userid = $this->session->userdata('uid');
		// return $this->general_model->fetch_specific_vals("ass.assign_ID,ass.assignedTo,ass.assignedBy,checktask.taskdueDate,checklist.checklistTitle,checklist.checklist_ID,checktask.task_ID,checktask.checklistStatus_ID,checktask.isCompleted,task.taskTitle,checktask.checklistStatus_ID","checklist.checklist_ID=checktask.checklist_ID AND checktask.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedBy=$userid AND checktask.taskdueDate IS NOT NULL AND DATE(checktask.taskdueDate)<='".$dateTime['date']."' AND checktask.isCompleted=2 AND task.task_ID=checktask.task_ID","tbl_processST_assignment ass, tbl_processST_checklist checklist,tbl_processST_checklistStatus checktask,tbl_processST_task task");
		return $this->general_model->fetch_specific_vals("ass.assign_ID,ass.assignedTo,ass.assignedBy,checktask.taskdueDate,checklist.checklistTitle,checklist.checklist_ID,checktask.task_ID,checktask.isCompleted,task.taskTitle,checktask.checklistStatus_ID", "checklist.checklist_ID=checktask.checklist_ID AND checktask.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedBy=$userid AND checktask.taskdueDate IS NOT NULL AND DATE(checktask.taskdueDate)<='" . $dateTime['date'] . "' AND checktask.isCompleted=2 AND task.task_ID=checktask.task_ID AND checktask.task_status IS NULL", "tbl_processST_assignment ass, tbl_processST_checklist checklist,tbl_processST_checklistStatus checktask,tbl_processST_task task");
	}
	public function get_admin_checklist_notification()
	{
		$dateTime = $this->get_current_date_time();
		$duedate = [];
		$userid = $this->session->userdata('uid');
		return $this->general_model->fetch_specific_vals("checklist.dueDate,checklist.checklist_ID,checklist.checklistTitle,checklist.status_ID", "checklist.dueDate IS NOT NULL AND DATE(checklist.dueDate)<='" . $dateTime['date'] . "' AND checklist.status_ID=2", "tbl_processST_checklist checklist");
	}
	public function get_admin_task_notification()
	{
		$dateTime = $this->get_current_date_time();
		$duedate = [];
		$userid = $this->session->userdata('uid');
		return $this->general_model->fetch_specific_vals("checktask.taskdueDate,checklist.checklistTitle,checklist.checklist_ID,checktask.task_ID,checktask.isCompleted,task.taskTitle,checktask.checklistStatus_ID", "checklist.checklist_ID=checktask.checklist_ID AND checktask.taskdueDate IS NOT NULL AND DATE(checktask.taskdueDate)<='" . $dateTime['date'] . "' AND checktask.isCompleted=2 AND checktask.task_status IS NULL AND task.task_ID=checktask.task_ID", "tbl_processST_checklist checklist,tbl_processST_checklistStatus checktask,tbl_processST_task task");
	}
	function get_allemployeesname($type = null)
	{
		$userid = $this->session->userdata('uid');
		$id = $this->input->post("id");
		$val = $this->input->post("val");
		$dataExcempt = $this->general_model->fetch_specific_vals("group_concat(assignedTo) as uid", "type='$val' and folProCheTask_ID=$id", "tbl_processST_assignment");
		$append = ($dataExcempt[0]->uid != null) ? "and user.uid not in (" . $dataExcempt[0]->uid . ")" : "";

		$data = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description", "emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes' AND user.emp_id=emp.emp_id AND user.uid!=$userid $append", "tbl_applicant ap, tbl_employee emp,tbl_account acc, tbl_user user", "ap.lname ASC");
		echo json_encode($data);
	}
	public function task_duedate_notification($task_duedate, $notif)
	{
		$task = [];
		$recipient = [];
		$notification = [];
		$count = 0;
		$userid = $this->session->userdata('uid');

		if (count($task_duedate) > 0) {
			foreach ($task_duedate as $taskdd) {
				if ($notif == 0) {
					$recipient[0] = [
						'userId' => $userid,
					];
				} else {
					$recipient[0] = [
						'userId' => $taskdd->assignedTo,
					];
					$recipient[1] = [
						'userId' => $taskdd->assignedBy,
					];
				}
				$task[$count] = [
					'task_status' => 16,
					'checklistStatus_ID' => $taskdd->checklistStatus_ID,
				];
				//GET ASSIGNED TO NAME
				$ass_emp_id = $this->process_model->get_empid($taskdd->assignedTo)->row();
				$ass_empid = $ass_emp_id->emp_id;
				$ass_ap_id = $this->process_model->get_appid($ass_empid)->row();
				$ass_apid = $ass_ap_id->apid;
				$applicantinfo = $this->general_model->fetch_specific_val("*", "apid=$ass_apid", "tbl_applicant");

				$name = $applicantinfo->lname . "," . $applicantinfo->fname;

				// if($notif==1){
				$notif_message = $taskdd->taskTitle . " " . "task is already overdue" . "<br><small><b>ID</b>:" . str_pad($taskdd->checklistStatus_ID, 8, '0', STR_PAD_LEFT) . " </small>";

				// }else{
				// 	$notif_message =$taskdd->taskTitle." "."assigned to"." ".$name." "."is already overdue" ."<br><small><b>ID</b>:".str_pad($taskdd->checklistStatus_ID, 8, '0', STR_PAD_LEFT)." </small>";
				// }
				$link = "process";

				$system_notif = $this->set_system_notif_preceeding($notif_message, $link, $recipient);
				// var_dump($system_notif);
				$notification[$count] = $system_notif['notif_id'][0];
				$count++;
			}
			$data['notification_ids'] = $notification;
			$data['set_to_due_status'] = $this->general_model->batch_update($task, 'checklistStatus_ID', 'tbl_processST_checklistStatus');
			echo "update chck STAT" . $data['set_to_due_status'];
		} else {
			$data['notification_ids'] = $notification;
			$data['set_to_due_status'] = 0;
		}
		return $data;
	}
	public function checklist_duedate_notification($checklist_duedate, $notif)
	{
		$checklist = [];
		$recipient = [];
		$notification = [];
		$count = 0;
		$userid = $this->session->userdata('uid');

		if (count($checklist_duedate) > 0) {
			foreach ($checklist_duedate as $checkdd) {

				// '0' IF ADMIN
				if ($notif == 0) {
					$recipient[0] = [
						'userId' => $userid,
					];
				} else {
					$recipient[0] = [
						'userId' => $checkdd->assignedTo,
					];
					$recipient[1] = [
						'userId' => $checkdd->assignedBy,
					];
				}
				$checklist[$count] = [
					'status_ID' => 16,
					'checklist_ID' => $checkdd->checklist_ID,
				];
				//GET ASSIGNED TO NAME
				$ass_emp_id = $this->process_model->get_empid($checkdd->assignedTo)->row();
				$ass_empid = $ass_emp_id->emp_id;
				$ass_ap_id = $this->process_model->get_appid($ass_empid)->row();
				$ass_apid = $ass_ap_id->apid;
				$applicantinfo = $this->general_model->fetch_specific_val("*", "apid=$ass_apid", "tbl_applicant");

				$name = $applicantinfo->lname . "," . $applicantinfo->fname;

				// if($notif==1){

				$notif_message = $checkdd->checklistTitle . " " . "is already overdue" . "<br><small><b>ID</b>:" . str_pad($checkdd->checklist_ID, 8, '0', STR_PAD_LEFT) . " </small>";

				// }else{
				// 	$notif_message = $checkdd->checklistTitle." "."assigned to"." ".$name." "."is already overdue" ."<br><small><b>ID</b>:".str_pad($checkdd->checklist_ID, 8, '0', STR_PAD_LEFT)." </small>";
				// }
				$link = "process";
				$system_notif = $this->set_system_notif_preceeding($notif_message, $link, $recipient);
				// var_dump($system_notif);
				$notification[$count] = $system_notif['notif_id'][0];
				$count++;
			}
			$data['notification_ids'] = $notification;
			$data['set_to_due_status'] = $this->general_model->batch_update($checklist, 'checklist_ID', 'tbl_processST_checklist');
		} else {
			$data['notification_ids'] = $notification;
			$data['set_to_due_status'] = 0;
		}
		return $data;
	}
	public function access_duedate_notif()
	{
		// get checklist
		$data_notif_ids = [];
		$notiftype_to = 1;
		$notiftype_by = 2;
		$adminaccess_check = $this->fetch_access_admin();
		//ASSIGNED CHECKLIST TO USER
		$duedate_assignedTo = $this->get_assignedchecklist_duedate_notif();
		$assignedtouser = $this->checklist_duedate_notification($duedate_assignedTo, $notiftype_to);

		//ASSIGNED CHECKLIST BY USER
		$duedate_assignedBy = $this->get_assignedBychecklist_duedate_notif();
		$assignedbyuser = $this->checklist_duedate_notification($duedate_assignedBy, $notiftype_by);

		// ASSIGNED TASK TO USER	
		$duedate_task_assignedTo = $this->get_assignedtask_duedate_notif();
		$assignedtotask = $this->task_duedate_notification($duedate_task_assignedTo, $notiftype_to);

		//ASSIGNED TASK BY USER
		$duedate_task_assignedBy = $this->get_assignedBytask_duedate_notif();
		$assignedBytask = $this->task_duedate_notification($duedate_task_assignedBy, $notiftype_by);

		if (count($adminaccess_check) > 0) {

			//ADMIN CHECKLIST NOTIF
			$duedate_admin_checklist = $this->get_admin_checklist_notification();
			$admin_checknotif = $this->checklist_duedate_notification($duedate_admin_checklist, 0);

			//ADMIN TASK NOTIF
			$duedate_admin_task = $this->get_admin_task_notification();
			$admin_tasknotif = $this->task_duedate_notification($duedate_admin_task, 0);

			$data['notification_ids'] = array_merge($admin_checknotif['notification_ids'], $admin_tasknotif['notification_ids']);
		} else {
			$data['notification_ids'] = array_merge($assignedtouser['notification_ids'], $assignedbyuser['notification_ids'], $assignedtotask['notification_ids'], $assignedBytask['notification_ids']);
		}
		echo json_encode($data);
	}

	//JQUERY DISPLAY	
	//DISPLAY ARCHIVED
	public function display_archivedfolders()
	{
		$word = $this->input->post("word");
		$userid = $this->session->userdata('uid');
		$folder_display = $this->input->post("display_folder");
		$word  = ($word != null or !empty($word) or $word != "") ? $word : null;
		$data = $this->process_model->get_allarchivedfolders($word, $folder_display, $userid);
		echo (count($data) > 0) ? json_encode($data) : 0;
	}

	public function display_archivedprocesstemp()
	{
		$limiter = $this->input->post('limiter');
		$perPage = $this->input->post('perPage');
		$inputSearch = $this->input->post('inputSearch');
		$userid = $_SESSION["uid"];

		$search = array();
		$searchString = "";
		if ($inputSearch !== '') {
			$search[] = "processTitle LIKE '%$inputSearch%'";
		}
		$admin_access = $this->check_if_userisadmin();

		if ($admin_access != null) {
			if (!empty($search)) {
				$searchString = "AND " . implode(" AND ", $search);
			}
			$select = "SELECT DISTINCT process_ID,processTitle,processstatus_ID,dateTimeCreated,'admin' as assignmentRule_ID";
			$from = "FROM tbl_processST_process WHERE isRemoved=0 AND processstatus_ID=15 $searchString order by dateTimeCreated";
		} else {
			if (!empty($search)) {
				$searchString = "WHERE " . implode(" AND ", $search);
			}
			$select = "SELECT *";
			$from = "from (SELECT DISTINCT(p.process_ID),p.processTitle,p.dateTimeCreated,p.processstatus_ID,'owned' as assignmentRule_ID FROM tbl_processST_process p where p.createdBy=$userid AND p.isRemoved=0 AND p.processstatus_ID=15 UNION SELECT DISTINCT(p.process_ID),p.processTitle,p.dateTimeCreated,p.processstatus_ID, a.assignmentRule_ID FROM tbl_processST_assignment a,tbl_processST_process p where a.assignedTo=$userid and a.type='process' and p.isRemoved=0 and p.processstatus_ID=15 and a.folProCheTask_ID=p.process_ID ) a $searchString order by dateTimeCreated DESC";
		}
		$fetched_processtemp = $this->general_model->custom_query($select . " " . $from . " LIMIT $limiter,$perPage");

		$loop = 0;
		$process_details = array();
		if (count($fetched_processtemp) > 0) {
			foreach ($fetched_processtemp as $processtemp) {
				$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
				$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
				$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
				$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
				$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
				$process_details[$loop] = [
					"process" => $processtemp,
					"statuses" => $stat
				];
				$loop++;
			}
		}

		$all = $this->general_model->custom_query("SELECT COUNT(*) as count " . $from);
		$count = $all[0]->count;
		echo json_encode(['process_details' => $process_details, 'total' => $count]);
	}
	// public function display_archivedprocesstemp2()
	// {
	// 	$userid = $_SESSION["uid"];
	// 	$admin_access = $this->check_if_userisadmin();

	// 	if ($admin_access != null) {
	// 		$select = "SELECT DISTINCT process_ID,processTitle,processstatus_ID,dateTimeCreated,'admin' as assignmentRule_ID";
	// 		$from = "FROM tbl_processST_process WHERE isRemoved=0 AND processstatus_ID=15 order by dateTimeCreated";
	// 	}else{
	// 		$select = "SELECT *";
	// 		$from = "from (SELECT DISTINCT(p.process_ID),p.processTitle,p.dateTimeCreated,p.processstatus_ID,'owned' as assignmentRule_ID FROM tbl_processST_process p where p.createdBy=$userid AND p.isRemoved=0 AND p.processstatus_ID=15 UNION SELECT DISTINCT(p.process_ID),p.processTitle,p.dateTimeCreated,p.processstatus_ID, a.assignmentRule_ID FROM tbl_processST_assignment a,tbl_processST_process p where a.assignedTo=$userid and a.type='process' and p.isRemoved=0 and p.processstatus_ID=15 and a.folProCheTask_ID=p.process_ID ) a order by dateTimeCreated DESC";
	// 	}
	// 		$fetched_processtemp = $this->general_model->custom_query($select . " " . $from);

	// 		$loop = 0;
	// 		$process_details = array();
	// 		if (count($fetched_processtemp) > 0) {
	// 			foreach ($fetched_processtemp as $processtemp) {
	// 				$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
	// 				$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
	// 				$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
	// 				$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
	// 				$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
	// 				$process_details[$loop] = [
	// 					"process" => $processtemp,
	// 					"statuses" => $stat
	// 				];
	// 				$loop++;
	// 			}
	// 		}

	// 	 echo json_encode(['process_details' => $process_details]);
	// }

	// $word = $this->input->post("word");
	// $userid = $this->session->userdata('uid');
	// $process_display = $this->input->post("process_display");
	// $word = ($word != null or !empty($word) or $word != "") ? $word : null;
	// $data = $this->process_model->get_allarchivedprocesstemp($word, $process_display, $userid);
	// $dataprocess = $this->process_model->get_allarchivedprocess_underfolder($word, $process_display, $userid);
	// $loop = 0;
	// $my_process = [];
	// $my_process = array_merge($data, $dataprocess);
	// if (count($my_process) > 0) {
	// 	foreach ($my_process as $processtemp) {
	// 		$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
	// 		$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
	// 		$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
	// 		$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
	// 		$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
	// 		$my_process[$loop] = [
	// 			"process" => $processtemp,
	// 			"statuses" => $stat
	// 		];
	// 		$loop++;
	// 	}
	// }
	// echo (count($my_process) > 0) ? json_encode($my_process) : 0;

	public function display_archivedchecklists()
	{
		$limiter = $this->input->post('limiter');
		$perPage = $this->input->post('perPage');
		$inputSearch = $this->input->post('inputSearch');
		$userid = $this->session->userdata('uid');

		$search = array();
		$searchString = "";
		if ($inputSearch !== '') {
			$search[] = "checklistTitle LIKE '%$inputSearch%'";
		}
		$admin_access = $this->check_if_userisadmin();
		if ($admin_access != null) {
			if (!empty($search)) {
				$searchString = "AND " . implode(" AND ", $search);
			}
			$select = "SELECT DISTINCT (c.checklist_ID),c.isArchived,c.dueDate,c.checklistTitle,c.dateTimeCreated,c.status_ID,p.processTitle,p.processstatus_ID,p.process_ID, 'admin' as assignmentRule_ID";
			$from = "FROM tbl_processST_checklist c,tbl_processST_process p WHERE c.process_ID=p.process_ID AND c.isRemoved!=1 AND c.isArchived=1 $searchString order by c.dateTimeCreated";
			$all = $this->general_model->custom_query("SELECT COUNT(*) as count " . $from);
		} else {
			if (!empty($search)) {
				$searchString = "WHERE " . implode(" AND ", $search);
			}
			$select = "SELECT *";
			$from = "FROM (SELECT DISTINCT(c.checklist_ID),c.isArchived,c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'yes' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklistStatus b,tbl_processST_checklist c, tbl_processST_process d where assignedTo=$userid and type='task' and a.folProCheTask_ID=b.checklistStatus_ID and b.checklist_ID=c.checklist_ID and c.isRemoved=0 and c.isArchived=1 and d.process_ID=c.process_ID UNION SELECT DISTINCT(c.checklist_ID),c.isArchived,c.dueDate,checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where c.createdBy=$userid AND c.isRemoved=0 AND c.isArchived=1 and d.process_ID=c.process_ID UNION SELECT DISTINCT(b.checklist_ID),b.isArchived,b.dueDate,checklistTitle,b.dateTimeCreated,b.status_ID, d.processTitle, d.process_ID, a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklist b, tbl_processST_process d where assignedTo=$userid and type='checklist' and a.folProCheTask_ID=b.checklist_ID and b.isRemoved=0 and b.isArchived=1 and d.process_ID=b.process_ID UNION SELECT DISTINCT(c.checklist_ID),c.isArchived,c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and d.processstatus_ID=15 and (a.assignmentRule_ID=2 or a.assignmentRule_ID=4 or a.assignmentRule_ID=7) UNION SELECT DISTINCT(c.checklist_ID),c.isArchived,c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,'owned process' as assignmentRule_ID,'no' as task_permission FROM tbl_processST_process d,tbl_processST_checklist c where d.process_ID=c.process_ID and c.isRemoved=0 and d.process_ID=c.process_ID and d.createdBy=$userid and d.processstatus_ID=15 UNION SELECT DISTINCT(c.checklist_ID),c.isArchived,c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and d.processstatus_ID=15 and (a.assignmentRule_ID=1 or a.assignmentRule_ID=3 or a.assignmentRule_ID=8))a $searchString group by checklist_ID order by dateTimeCreated DESC";
			$all = $this->general_model->custom_query("SELECT COUNT(*) as count FROM ($select $from) zz");
		}
		$c = $this->general_model->custom_query($select . " " . $from . " LIMIT $limiter,$perPage");
		$loop = 0;
		$checklist_details = array();
		if (count($c) > 0) {
			foreach ($c as $checklist) {
				$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
				$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
				$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);
				$checklist_details[$loop] = [
					"checklist" => $checklist,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		$count = $all[0]->count;
		echo json_encode(['checklist_details' => $checklist_details, 'total' => $count]);
	}

	// public function display_archivedchecklists()
	// {
	// 	$userid = $this->session->userdata('uid');
	// 	$admin_access = $this->check_if_userisadmin();
	// 	if ($admin_access != null) {
	// 		$select = "SELECT DISTINCT (c.checklist_ID),c.dueDate,c.checklistTitle,c.dateTimeCreated,c.status_ID,p.processTitle,p.processstatus_ID,p.process_ID, 'admin' as assignmentRule_ID";
	// 		$from = "FROM tbl_processST_checklist c,tbl_processST_process p WHERE c.process_ID=p.process_ID AND c.isRemoved!=1 AND c.isArchived=1 order by c.dateTimeCreated";
	// 	}else{
	// 		$select = "SELECT *";
	// 		$from = "FROM (SELECT DISTINCT(c.checklist_ID),c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'yes' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklistStatus b,tbl_processST_checklist c, tbl_processST_process d where assignedTo=$userid and type='task' and a.folProCheTask_ID=b.checklistStatus_ID and b.checklist_ID=c.checklist_ID and c.isRemoved=0 and c.isArchived=1 and d.process_ID=c.process_ID UNION SELECT DISTINCT(c.checklist_ID),c.dueDate,checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where c.createdBy=$userid AND c.isRemoved=0 AND c.isArchived=1 and d.process_ID=c.process_ID UNION SELECT DISTINCT(b.checklist_ID),b.dueDate,checklistTitle,b.dateTimeCreated,b.status_ID, d.processTitle, d.process_ID, a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklist b, tbl_processST_process d where assignedTo=$userid and type='checklist' and a.folProCheTask_ID=b.checklist_ID and b.isRemoved=0 and b.isArchived=1 and d.process_ID=b.process_ID UNION SELECT DISTINCT(c.checklist_ID),c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and d.processstatus_ID=15 and (a.assignmentRule_ID=2 or a.assignmentRule_ID=4 or a.assignmentRule_ID=7) UNION SELECT DISTINCT(c.checklist_ID),c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,'owned process' as assignmentRule_ID,'no' as task_permission FROM tbl_processST_process d,tbl_processST_checklist c where d.process_ID=c.process_ID and c.isRemoved=0 and d.process_ID=c.process_ID and d.createdBy=$userid and d.processstatus_ID=15 UNION SELECT DISTINCT(c.checklist_ID),c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and d.processstatus_ID=15 and (a.assignmentRule_ID=1 or a.assignmentRule_ID=3 or a.assignmentRule_ID=8))a group by checklist_ID order by dateTimeCreated DESC";
	// 	}
	// 	$c = $this->general_model->custom_query($select . " " . $from);
	// 	$loop = 0;
	// 	$checklist_details = array();
	// 	if (count($c) > 0) {
	// 		foreach ($c as $checklist) {
	// 			$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
	// 			$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
	// 			$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);
	// 			$checklist_details[$loop] = [
	// 				"checklist" => $checklist,
	// 				"statuses" => $stat
	// 			];
	// 			$loop++;
	// 		}
	// 	}
	// 	echo json_encode(['checklist_details' => $checklist_details]);
	// }
	// $my_checklist = [];
	// $word = $this->input->post("word");
	// $userid = $this->session->userdata('uid');
	// $checklist_display = $this->input->post("checklist_display");
	// $word  = ($word != null or !empty($word) or $word != "") ? $word : null;
	// $loop = 0;
	// $data = $this->process_model->get_allarchivedchecklist($word, $checklist_display, $userid);
	// $datacheck = $this->process_model->get_allarchivedchecklist_process($word, $checklist_display, $userid);
	// $datafolcheck = $this->process_model->get_allarchivedchecklist_folder($word, $checklist_display, $userid);
	// $my_checklist = array_merge($data, $datacheck, $datafolcheck);

	// if (count($my_checklist) > 0) {
	// 	foreach ($my_checklist as $checklist) {
	// 		$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
	// 		$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
	// 		$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);
	// 		$my_checklist[$loop] = [
	// 			"checklist" => $checklist,
	// 			"statuses" => $stat
	// 		];
	// 		$loop++;
	// 	}
	// }
	// echo (count($my_checklist) > 0) ? json_encode($my_checklist) : 0;
	public function display_folders()
	{
		$word = $this->input->post("word");
		$folder_ids = $this->input->post("folder_ids");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$data = $this->process_model->get_allfolders($folder_ids, $word);
		echo json_encode(['data' => $data, 'whichfunction' => 0]);
	}
	public function display_ownfolders()
	{
		$userid = $this->session->userdata('uid');
		$word = $this->input->post("word");
		$folder_ids = $this->input->post("folder_ids");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$data = $this->process_model->get_ownfolders($folder_ids, $word, $userid);
		echo json_encode(['data' => $data, 'whichfunction' => 0]);
	}
	public function display_assignedfolders()
	{
		$userid = $this->session->userdata('uid');
		$word = $this->input->post("word");
		$folder_ids = $this->input->post("folder_ids");
		$whichfunction = $this->input->post("whichfunction");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		//get assigned folders
		$assigned = [];
		$owned = [];
		if ($whichfunction == 0) {
			$assigned = $this->process_model->get_assignedfolders($folder_ids, $word, $userid);
			$folder_ids = $this->addToArray_folder($assigned, $folder_ids);
			$my_folders = $assigned;
			$cnt = COUNT($assigned);
			if ($cnt < 6) {
				$whichfunction = 1;
				$perpage = 6 - $cnt;
				$owned = $this->process_model->get_ownfolders($folder_ids, $word, $userid, $perpage);
				$my_folders = array_merge($my_folders, $owned);
			}
		} else {
			$owned = $this->process_model->get_ownfolders($folder_ids, $word, $userid);
			$my_folders = $owned;
		}


		echo json_encode(['data' => $my_folders, 'whichfunction' => $whichfunction]);
	}
	public function display_filefetch($task_ID, $sequence)
	{
		$seq_max = $this->fetch_allsubtask_count2($task_ID);
		if($seq_max==null){
			$maxseq=1;
		}else{
			$maxseq=$seq_max;
			$maxseq++;
		}
		$valid_extensions = array('xlsx', 'xls', 'zip', 'doc', 'mov', 'docx', 'ppt', 'pdf', 'pptx', 'rar'); // valid extensions
		$path = 'uploads/process/form_file/'; // upload directory

		$img = $_FILES['upload_files']['name'];
		$tmp = $_FILES['upload_files']['tmp_name'];
		// get uploaded file's extension
		$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));

		// can upload same image using rand function
		$final_image = time() . $img;
		// check's valid format
		if (in_array($ext, $valid_extensions)) {

			$path = $path . strtolower($final_image);
			// echo $_FILES["videoFile"]["error"];
			if (move_uploaded_file($tmp, $path)) {
				$info['task_ID'] = $task_ID;
				$info['component_ID'] = "18";
				$info['complabel'] = $path;
				$info['sequence'] = $maxseq;

				$info2["subtask"] = $this->general_model->insert_vals_last_inserted_id($info, "tbl_processST_subtask");
				$info2['complabel'] = $path;

				echo json_encode($info2);
			}
		} else {
			echo 'invalid';
		}
	}
	public function display_vidfetch($type, $task_ID, $sequence_num)
	{
		$seq_max = $this->fetch_allsubtask_count2($task_ID);
		if($seq_max==null){
			$maxseq=1;
		}else{
			$maxseq=$seq_max;
			$maxseq++;
		}
		if ($type == 2) { // If Video Upload
			$valid_extensions = array('m4v', 'avi', 'mpg', 'mp4', 'mov', 'mpg', 'mpeg'); // valid extensions
			$path = 'uploads/process/form_videos/'; // upload directory

			$img = $_FILES['videoFile']['name'];
			$tmp = $_FILES['videoFile']['tmp_name'];
			// get uploaded file's extension
			$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			// can upload same image using rand function
			$final_image = time() . $img;
			// check's valid format
			if (in_array($ext, $valid_extensions)) {

				$path = $path . strtolower($final_image);
				// echo $_FILES["videoFile"]["error"];
				if (move_uploaded_file($tmp, $path)) {
					$info['task_ID'] = $task_ID;
					$info['component_ID'] = "17";
					$info['complabel'] = $path;
					$info['sequence'] = $maxseq;

					$info2["subtask"] = $this->general_model->insert_vals_last_inserted_id($info, "tbl_processST_subtask");
					$info2['complabel'] = $path;

					echo json_encode($info2);
				}
			} else {
				echo 'invalid';
			}
		} else { //If Video URL
			$info['task_ID'] = $task_ID;
			$info['component_ID'] = "17";
			$info['sequence'] = $maxseq;
			$path = trim($this->input->post("url_str"));
			$url = explode("/", $path);
			if ($url[2] == "youtube.com" or $url[2] == "www.youtube.com" or $url[2] == "m.youtube.com") {
				$path = str_replace("watch?v=", "embed/", $path);
				$link = '<iframe src="' . $path . '" class="embed-responsive-item" frameborder="0" allowfullscreen ></iframe>';
			} else {
				$link = "Invalid";
			}
			$info['complabel'] = $link;
			if ($link != "Invalid") {
				$subtask = $this->general_model->insert_vals_last_inserted_id($info, "tbl_processST_subtask");
				echo $subtask;
			} else {
				echo $link;
			}
		}
	}
	public function display_imagefetch()
	{
		$data = $_POST['image'];
		$image_array_1 = explode(";", $data);
		$image_array_2 = explode(",", $image_array_1[1]);

		$data = base64_decode($image_array_2[1]);
		$imageName = 'uploads/process/form_images/' . time() . '.jpg';
		file_put_contents($imageName, $data);
		// echo json_encode($imageName);
		$info['task_ID'] = $_POST['task_ID'];
		$tid=$_POST['task_ID'];

		$seq_max = $this->fetch_allsubtask_count2($tid);
		if($seq_max==null){
			$maxseq=1;
		}else{
			$maxseq=$seq_max;
			$maxseq++;
		}

		// $info['sequence'] = $_POST['sequence'];
		$info['sequence'] = $maxseq;
		$info['component_ID'] = "16";
		$info['complabel'] = $imageName;


		$subtask['subTask_ID'] = $this->general_model->insert_vals_last_inserted_id($info, "tbl_processST_subtask");
		$this->general_model->insert_vals($subtask, "tbl_processST_component_subtask");
		echo json_encode($subtask);
	}
	public function displaysubfolders()
	{
		$folder_ID = $this->input->post("folder_ID");
		$word = $this->input->post("word");
		$folder_ids = $this->input->post("folder_ids");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;

		$newAppend = ($folder_ids != NULL && !empty($folder_ids)) ? "AND folder.folder_ID NOT IN (" . implode(',', $folder_ids) . ")" : "";
		$append = ($word != null) ? " AND (folder.folderName like '%$word%' OR CONCAT(app.fname,' ',app.lname) like '%$word%')" : "";
		$data = $this->general_model->fetch_specific_vals("folder.*, app.*", "folder.referenceFolder=$folder_ID AND folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.status_ID!=15 AND folder.isRemoved!=1 $append $newAppend", "tbl_processST_folder folder,tbl_user user,tbl_applicant app, tbl_employee employee", "folder.folderName ASC", 6);

		echo json_encode(['data' => $data, 'whichfunction' => 0]);
	}
	public function display_assigned_subfolders()
	{
		$all_priv = [];
		$folder_ID = $this->input->post("folder_ID");
		$userid = $this->session->userdata('uid');
		$word = $this->input->post("word");
		$folder_ids = $this->input->post("folder_ids");
		$whichfunction = $this->input->post("whichfunction");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;


		$append = ($word != null) ? " AND (folder.folderName like '%$word%' OR CONCAT(app.fname,' ',app.lname) like '%$word%')" : "";
		$newAppend = ($folder_ids != NULL && !empty($folder_ids)) ? "AND folder.folder_ID NOT IN (" . implode(',', $folder_ids) . ")" : "";
		if ($whichfunction == 0) {
			$data = $this->general_model->fetch_specific_vals("folder.*, app.*,ass.assignmentRule_ID as folassign", "folder.referenceFolder=$folder_ID AND folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder=ass.folProCheTask_ID AND ass.type='folder' AND folder.status_ID!=15 AND ass.assignedTo=$userid AND folder.isRemoved!=1 AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) $append $newAppend", "tbl_processST_folder folder,tbl_user user,tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass", "folder.folderName ASC", 6);
			$folder_ids = $this->addToArray_folder($data, $folder_ids);
			$all_priv = $data;
			$cnt = COUNT($data);
			if ($cnt < 6) {
				$whichfunction = 1;
				$perpage = 6 - $cnt;
				$data_owned = $this->general_model->fetch_specific_vals("folder.*, app.*,ass.assignmentRule_ID as folassign", "folder.referenceFolder=$folder_ID AND folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder=ass.folProCheTask_ID AND ass.type='folder' AND folder.status_ID!=15 AND ass.assignedTo=$userid AND folder.createdBy=$userid AND folder.isRemoved!=1 AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7) $append $newAppend", "tbl_processST_folder folder,tbl_user user,tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass", "folder.folderName ASC", $perpage);
				$all_priv = array_merge($all_priv, $data_owned);
			}
		} else {
			$data_owned = $this->general_model->fetch_specific_vals("folder.*, app.*,ass.assignmentRule_ID as folassign", "folder.referenceFolder=$folder_ID AND folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder=ass.folProCheTask_ID AND ass.type='folder' AND folder.status_ID!=15 AND ass.assignedTo=$userid AND folder.createdBy=$userid AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7) $append $newAppend", "tbl_processST_folder folder,tbl_user user,tbl_applicant app, tbl_employee employee, tbl_processST_assignment ass", "folder.folderName ASC", 6);
			$all_priv = $data_owned;
		}
		echo json_encode(['data' => $all_priv, 'whichfunction' => $whichfunction]);
	}
	public function taskid_forupload()
	{
		$taskid = $this->input->post("task_ID");
		return $taskid;
	}
	public function do_upload()
	{
		$filename = $_FILES['file']['name'];
		$taskid = taskid_forupload();

		$location = "assets/documents/" . $filename;
		$uploadOk = 1;
		$imageFileType = pathinfo($location, PATHINFO_EXTENSION);

		$valid_extensions = array("docx", "txt", "xlsx", "xls");
		if (!in_array(strtolower($imageFileType), $valid_extensions)) {
			$uploadOk = 0;
		}
		if ($uploadOk == 0) {
			echo 0;
		} else {
			/* Upload file */
			if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
				$data['task_ID'] = $taskid;
				$data['complabel'] = $location;
				$data['component_ID'] = 16;

				$subtask['subTask_ID'] = $this->general_model->insert_vals_last_inserted_id($data, "tbl_processST_subtask");
				$this->general_model->insert_vals($subtask, "tbl_processST_component_subtask");
				echo json_encode($subtask);
			} else {
				echo 0;
			}
		}
	}
	// NEW checker
	public function check_access_privilege_active_pending($p_ID)
	{
		$userid = $this->session->userdata('uid');
		$a_admin =  $this->check_if_userisadmin();
		$p_o = "SELECT process_ID FROM `tbl_processST_process` WHERE createdBy=$userid AND isRemoved=0 AND processstatus_ID!=15 AND process_ID=$p_ID";
		$p_owned =  $this->general_model->custom_query($p_o);
		$p_p = "SELECT process.process_ID FROM tbl_processST_process as process, tbl_processST_assignment ass WHERE ass.type='process' AND ass.assignedTo=$userid AND ass.folProCheTask_ID=$p_ID AND ass.folProCheTask_ID=process.process_ID AND process.process_ID=$p_ID AND process.isRemoved=0 AND process.processstatus_ID!=15";
		$p_permission =  $this->general_model->custom_query($p_p);

		if ($a_admin != null) {
			$access = "1";
		} else if ($p_owned != null) {
			$access = "1";
		} else if ($p_permission != null) {
			$access = "1";
		} else {
			$access = "0";
		}
		if ($access == "1") {
			return true;
		} else {
			$this->error_403();
			return false;
		}
	}
	public function check_access_privilege_pending($process_ID)
	{
		$all_priv = [];
		$all_priv2 = [];
		$userid = $this->session->userdata('uid');
		$data = $this->general_model->fetch_specific_vals("*", "user_ID=$userid", "tbl_processST_admin_access");
		$processremoved = $this->general_model->fetch_specific_vals("*", "process_ID=$process_ID AND (isRemoved=1 OR processstatus_ID=15)", "tbl_processST_process process");


		$checklist_pending = $this->general_model->fetch_specific_vals("process.*", "ass.assignedTo=$userid AND ass.type='process' AND ass.folProCheTask_ID=$process_ID AND process.process_ID=$process_ID AND ass.folProCheTask_ID=process.process_ID", "tbl_processST_assignment ass, tbl_processST_process process");

		$folder_assign = $this->general_model->fetch_specific_vals("process.processTitle, folder.folder_ID, ass.assign_ID", "process.process_ID=$process_ID AND folder.folder_ID=process.folder_ID AND ass.assignedTo=$userid AND type='folder' AND ass.folProCheTask_ID=folder.folder_ID AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8)", "tbl_processST_assignment ass, tbl_processST_folder folder, tbl_processST_process process");

		$ownedprocess = $this->general_model->fetch_specific_vals("*", "process_ID=$process_ID AND createdBy=$userid", "tbl_processST_process");


		$checklist_pending2 = $this->general_model->fetch_specific_vals("process.*", "ass.assignedTo=$userid AND ass.type='process' AND ass.folProCheTask_ID=$process_ID AND process.process_ID=$process_ID AND ass.folProCheTask_ID=process.process_ID AND (process.isRemoved=1 OR process.processstatus_ID=15)", "tbl_processST_assignment ass, tbl_processST_process process");

		$folder_assign2 = $this->general_model->fetch_specific_vals("process.processTitle, folder.folder_ID, ass.assign_ID", "process.process_ID=$process_ID AND folder.folder_ID=process.folder_ID AND ass.assignedTo=$userid AND type='folder' AND ass.folProCheTask_ID=folder.folder_ID AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) AND (folder.isRemoved=1 OR folder.status_ID=15 OR process.isRemoved=1 OR process.processstatus_ID=15)", "tbl_processST_assignment ass, tbl_processST_folder folder, tbl_processST_process process");

		$ownedprocess2 = $this->general_model->fetch_specific_vals("*", "process_ID=$process_ID AND createdBy=$userid AND (isRemoved=1 OR processstatus_ID=15)", "tbl_processST_process");
		$all_priv = array_merge($checklist_pending, $folder_assign, $ownedprocess);
		$all_priv2 = array_merge($checklist_pending2, $folder_assign2, $ownedprocess2);

		if ($data != null && $processremoved == null) {
			return true;
		} else if ($all_priv != null && $all_priv2 == null) {
			return true;
		} else {
			$this->error_403();
			return false;
		}
	}

	public function check_access_privilege_report($p_ID)
	{

		$userid = $this->session->userdata('uid');
		$a_admin =  $this->check_if_userisadmin();
		$p_o = "SELECT process_ID FROM `tbl_processST_process` WHERE createdBy=$userid AND isRemoved=0 AND processstatus_ID!=15 AND process_ID=$p_ID";
		$p_owned =  $this->general_model->custom_query($p_o);
		$p_p = "SELECT process.process_ID FROM tbl_processST_process as process, tbl_processST_assignment ass WHERE ass.type='process' AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=2) AND ass.assignedTo=$userid AND ass.folProCheTask_ID=$p_ID AND ass.folProCheTask_ID=process.process_ID AND process.process_ID=$p_ID AND process.isRemoved=0 AND process.processstatus_ID!=15";
		$p_permission =  $this->general_model->custom_query($p_p);

		if ($a_admin != null) {
			$access = "1";
		} else if ($p_owned != null) {
			$access = "1";
		} else if ($p_permission != null) {
			$access = "1";
		} else {
			$access = "0";
		}
		if ($access == "1") {
			return true;
		} else {
			$this->error_403();
			return false;
		}
	}

	// public function check_access_privilege_report($process_ID)
	// {
	// 	$all_priv = [];
	// 	$all_priv2 = [];
	// 	$userid = $this->session->userdata('uid');
	// 	$data = $this->general_model->fetch_specific_vals("*", "user_ID=$userid", "tbl_processST_admin_access");
	// 	$processremoved = $this->general_model->fetch_specific_vals("*", "process_ID=$process_ID AND (isRemoved=1 OR processstatus_ID=15)", "tbl_processST_process process");

	// 	$checklist_pending = $this->general_model->fetch_specific_vals("process.*", "ass.assignedTo=$userid AND ass.type='process' AND ass.folProCheTask_ID=$process_ID AND process.process_ID=$process_ID AND ass.folProCheTask_ID=process.process_ID AND ass.assignmentRule_ID=1", "tbl_processST_assignment ass, tbl_processST_process process");

	// 	$folder_assign = $this->general_model->fetch_specific_vals("process.processTitle, folder.folder_ID, ass.assign_ID", "process.process_ID=$process_ID AND folder.folder_ID=process.folder_ID AND ass.assignedTo=$userid AND type='folder' AND ass.folProCheTask_ID=folder.folder_ID AND ass.assignmentRule_ID=1", "tbl_processST_assignment ass, tbl_processST_folder folder, tbl_processST_process process");

	// 	$ownedprocess = $this->general_model->fetch_specific_vals("*", "process_ID=$process_ID AND createdBy=$userid", "tbl_processST_process");
	// 	//

	// 	$checklist_pending2 = $this->general_model->fetch_specific_vals("process.*", "ass.assignedTo=$userid AND ass.type='process' AND ass.folProCheTask_ID=$process_ID AND process.process_ID=$process_ID AND ass.folProCheTask_ID=process.process_ID AND ass.assignmentRule_ID=1 AND (process.isRemoved=1 OR process.processstatus_ID=15)", "tbl_processST_assignment ass, tbl_processST_process process");

	// 	$folder_assign2 = $this->general_model->fetch_specific_vals("process.processTitle, folder.folder_ID, ass.assign_ID", "process.process_ID=$process_ID AND folder.folder_ID=process.folder_ID AND ass.assignedTo=$userid AND type='folder' AND ass.folProCheTask_ID=folder.folder_ID AND (folder.isRemoved=1 OR folder.status_ID=15 OR process.isRemoved=1 OR process.processstatus_ID=15) AND ass.assignmentRule_ID=1", "tbl_processST_assignment ass, tbl_processST_folder folder, tbl_processST_process process");

	// 	$ownedprocess2 = $this->general_model->fetch_specific_vals("*", "process_ID=$process_ID AND createdBy=$userid AND (isRemoved=1 OR processstatus_ID=15)", "tbl_processST_process");

	// 	$all_priv = array_merge($checklist_pending, $folder_assign, $ownedprocess);
	// 	$all_priv2 = array_merge($checklist_pending2, $folder_assign2, $ownedprocess2);

	// 	if ($data != null && $processremoved == null) {
	// 		return true;
	// 	} else if ($all_priv != null && $all_priv2 == null) {
	// 		return true;
	// 	} else {
	// 		$this->error_403();
	// 		return false;
	// 	}
	// }
	public function check_access_privilege_subfolder($folder_ID)
	{
		$all_subfolder = [];
		$all_subfolder2 = [];
		$userid = $this->session->userdata('uid');
		$data = $this->general_model->fetch_specific_vals("*", "user_ID=$userid", "tbl_processST_admin_access access");
		$folderremoved = $this->general_model->fetch_specific_vals("*", "folder_ID=$folder_ID AND (isRemoved=1 OR status_ID=15)", "tbl_processST_folder folder");

		$folderassignment = $this->general_model->fetch_specific_vals("folder.*", "ass.assignedTo=$userid AND ass.type='folder' AND ass.folProCheTask_ID=$folder_ID AND folder.folder_ID=$folder_ID AND ass.folProCheTask_ID=folder.folder_ID", "tbl_processST_assignment ass, tbl_processST_folder folder");

		$folderowned = $this->general_model->fetch_specific_vals("*", "createdBy=$userid AND folder_ID=$folder_ID", "tbl_processST_folder");

		$subfolderassignment = $this->general_model->fetch_specific_vals("folder.folderName,ass.assign_ID", "folder.folder_ID=$folder_ID AND folder.referenceFolder!=1 AND ass.folProCheTask_ID=folder.referenceFolder AND ass.type='folder' AND ass.assignedTo=$userid AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8)", "tbl_processST_assignment ass, tbl_processST_folder folder");

		$subfolderassignment_owned = $this->general_model->fetch_specific_vals("folder.folderName,ass.assign_ID", "folder.folder_ID=$folder_ID AND folder.referenceFolder!=1 AND ass.folProCheTask_ID=folder.referenceFolder AND folder.createdBy=$userid AND ass.type='folder' AND ass.assignedTo=$userid AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7)", "tbl_processST_assignment ass, tbl_processST_folder folder");

		//
		$folderassignment2 = $this->general_model->fetch_specific_vals("folder.*", "ass.assignedTo=$userid AND ass.type='folder' AND ass.folProCheTask_ID=$folder_ID AND folder.folder_ID=$folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND (folder.isRemoved=1 OR folder.status_ID=15)", "tbl_processST_assignment ass, tbl_processST_folder folder");

		$folderowned2 = $this->general_model->fetch_specific_vals("*", "createdBy=$userid AND folder_ID=$folder_ID AND (isRemoved=1 OR status_ID=15)", "tbl_processST_folder");

		$subfolderassignment2 = $this->general_model->fetch_specific_vals("folder.folderName,ass.assign_ID", "folder.folder_ID=$folder_ID AND folder.referenceFolder!=1 AND ass.folProCheTask_ID=folder.referenceFolder AND ass.type='folder' AND ass.assignedTo=$userid AND (folder.isRemoved=1 OR folder.status_ID=15) AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8)", "tbl_processST_assignment ass, tbl_processST_folder folder");

		$subfolderassignment_owned2 = $this->general_model->fetch_specific_vals("folder.folderName,ass.assign_ID", "folder.folder_ID=$folder_ID AND folder.referenceFolder!=1 AND ass.folProCheTask_ID=folder.referenceFolder AND folder.createdBy=$userid AND ass.type='folder' AND (folder.isRemoved=1 OR folder.status_ID=15) AND ass.assignedTo=$userid AND (ass.assignmentRule_ID=2 OR ass.assignmentRule_ID=4 OR ass.assignmentRule_ID=7)", "tbl_processST_assignment ass, tbl_processST_folder folder");

		$all_subfolder = array_merge($folderassignment, $folderowned, $subfolderassignment, $subfolderassignment_owned);
		$all_subfolder2 = array_merge($folderassignment2, $folderowned2, $subfolderassignment2, $subfolderassignment_owned2);

		if ($data != null && $folderremoved == null) {
			return true;
		} else if ($all_subfolder != null && $all_subfolder2 == null) {
			return true;
		} else {
			$this->error_403();
			return false;
		}
	}
	public function check_access_privilege()
	{
		$userid = $this->session->userdata('uid');
		$data = $this->general_model->fetch_specific_vals("*", "user_ID=$userid", "tbl_processST_admin_access");

		if ($data != null) {
			return true;
		} else {
			$this->error_403();
			return false;
		}
	}

	// New checker

	public function check_if_userisadmin()
	{
		$userid = $_SESSION["uid"];
		$admin = "SELECT * FROM tbl_processST_admin_access WHERE user_ID=$userid";
		return $this->general_model->custom_query($admin);
	}

	public function check_access_privilege_answer($p_ID, $c_ID, $page)
	{
		$access = "0";
		$userid = $_SESSION["uid"];
		$a_admin =  $this->check_if_userisadmin();
		// if user ID is admin
		if ($a_admin != null) {
			$access = "1";
		} else {
			// if userid is not admin	
			// CHECK if owned
			// check if checklist owned
			$c_o = "SELECT checklist_ID FROM `tbl_processST_checklist` WHERE createdBy=$userid AND isRemoved=0 AND status_ID!=15 AND checklist_ID=$c_ID";
			$c_owned =  $this->general_model->custom_query($c_o);

			// check if process template owned
			$p_o = "SELECT process_ID FROM `tbl_processST_process` WHERE createdBy=$userid AND isRemoved=0 AND processstatus_ID!=15 AND process_ID=$p_ID";
			$p_owned =  $this->general_model->custom_query($p_o);

			$t_o = "SELECT task_ID FROM tbl_processST_task WHERE createdBy=$userid AND process_ID=$p_ID";
			$t_owned = $this->general_model->custom_query($t_o);

			if ($p_owned != null) {
				$access = "1";
			} else if ($c_owned != null) {
				$access = "1";
			} else if ($t_owned != null) {
				$access = "1";
			} else {
				if ($page == 'checklist_answerable') {
					// condition if checklist owned				
					// CHECK With Permission

					// Task Permission
					$t_p = "SELECT checkStat.checklistStatus_ID FROM tbl_processST_task task, tbl_processST_assignment ass,tbl_processST_checklistStatus checkStat WHERE ass.type='task' AND  ass.assignedTo=$userid AND checkStat.checklist_ID=$c_ID AND checkStat.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND task.task_ID=checkStat.task_ID";
					$t_permission =  $this->general_model->custom_query($t_p);

					// Checklist permission
					$c_p = "SELECT checklist.checklist_ID FROM tbl_processST_checklist as checklist, tbl_processST_assignment ass WHERE ass.type='checklist' AND ass.assignedTo=$userid AND (ass.assignmentRule_ID=6 OR ass.assignmentRule_ID=5) AND ass.folProCheTask_ID=$c_ID AND ass.folProCheTask_ID=checklist.checklist_ID AND checklist.checklist_ID=$c_ID AND checklist.isRemoved=0 AND checklist.status_ID!=15";
					$c_permission =  $this->general_model->custom_query($c_p);

					// Process template permission
					$p_p = "SELECT process.process_ID FROM tbl_processST_process as process, tbl_processST_assignment ass WHERE ass.type='process' AND ass.assignedTo=$userid AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) AND ass.folProCheTask_ID=$p_ID AND ass.folProCheTask_ID=process.process_ID AND process.process_ID=$p_ID AND process.isRemoved=0 AND process.processstatus_ID!=15";
					$p_permission =  $this->general_model->custom_query($p_p);

					//folder permission
					$f_p = "SELECT folder.folder_ID FROM tbl_processST_process process, tbl_processST_folder folder ,tbl_processST_assignment ass WHERE ass.type='folder' AND ass.assignedTo=$userid AND ass.assignmentRule_ID=1 AND ass.folProCheTask_ID=folder.folder_ID AND process.process_ID=$p_ID AND folder.folder_ID=process.folder_ID AND folder.isRemoved=0 AND folder.status_ID!=15";
					$f_permission =  $this->general_model->custom_query($f_p);
					// CONDITION if checklist has permission to edit
					if ($c_permission != null) {
						$access = "1";
					} else if ($p_permission != null) {
						//CONDITION if process temp has permission to edit
						$access = "1";
					} else if ($f_permission != null) {
						$access = "1";
					} else if ($t_permission != null) {
						$access = "1";
					} else {
						$access = "0";
					}
				} else if ($page == 'checklist_update') {
					$t_p = "SELECT checkStat.checklistStatus_ID FROM tbl_processST_task task, tbl_processST_assignment ass,tbl_processST_checklistStatus checkStat WHERE ass.assignmentRule_ID=6 and ass.type='task' AND  ass.assignedTo=$userid AND checkStat.checklist_ID=$c_ID AND checkStat.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND task.task_ID=checkStat.task_ID";
					$t_permission =  $this->general_model->custom_query($t_p);

					$p_p = "SELECT process.process_ID FROM tbl_processST_process as process, tbl_processST_assignment ass WHERE ass.type='process' AND ass.assignedTo=$userid AND ass.assignmentRule_ID=1 AND ass.folProCheTask_ID=$p_ID AND ass.folProCheTask_ID=process.process_ID AND process.process_ID=$p_ID AND process.isRemoved=0 AND process.processstatus_ID!=15";
					$p_permission =  $this->general_model->custom_query($p_p);

					$c_p = "SELECT checklist.checklist_ID FROM tbl_processST_checklist as checklist, tbl_processST_assignment ass WHERE ass.type='checklist' AND ass.assignedTo=$userid AND ass.assignmentRule_ID=6 AND ass.folProCheTask_ID=$c_ID AND ass.folProCheTask_ID=checklist.checklist_ID AND checklist.checklist_ID=$c_ID AND checklist.isRemoved=0 AND checklist.status_ID!=15";
					$c_permission =  $this->general_model->custom_query($c_p);

					if ($t_permission != null) {
						$access = "1";
					} else if ($c_permission != null) {
						//CONDITION if process temp has permission to edit
						$access = "1";
					} else if ($p_permission != null) {
						$access = "1";
					} else {
						$access = "0";
					}
				}
			}
			// ==
		}
		if ($access == "1") {
			return true;
		} else {
			$this->error_403();
			return false;
		}
	}

	// OLD Checker
	// public function check_access_privilege_answer($process_ID,$checklist_ID){
	// 	$all_priv=[];
	// 	$all_priv2=[];
	// 	$userid=$this->session->userdata('uid');
	// 	$data=$this->general_model->fetch_specific_vals("*","user_ID=$userid","tbl_processST_admin_access");
	// 	$processremoved=$this->general_model->fetch_specific_vals("*","process_ID=$process_ID AND (isRemoved=1 OR processstatus_ID=15)","tbl_processST_process process");
	// 	$checklistremoved=$this->general_model->fetch_specific_vals("*","checklist_ID=$checklist_ID AND (isRemoved=1 OR isArchived=1)","tbl_processST_checklist checklist");

	// 	$checklist_assigned2=$this->general_model->fetch_specific_vals("checklist.*","ass.assignedTo=$userid AND ass.type='checklist' AND ass.folProCheTask_ID=$checklist_ID AND (checklist.isRemoved=1 OR checklist.isArchived=1) AND checklist.checklist_ID=$checklist_ID AND checklist.checklist_ID=ass.folProCheTask_ID AND ass.assignmentRule_ID=6","tbl_processST_assignment ass, tbl_processST_checklist checklist");
	// 	$checklist_owned2=$this->general_model->fetch_specific_vals("*","createdBy=$userid AND checklist_ID=$checklist_ID AND (isRemoved=1 OR isArchived=1)","tbl_processST_checklist");

	// 	$checklist_assign_process2=$this->general_model->fetch_specific_vals("process.processTitle,checklist.checklist_ID,checklist.checklistTitle,ass.assign_ID","process.process_ID=$process_ID AND process.process_ID=ass.folProCheTask_ID AND ass.folProCheTask_ID=$process_ID AND ass.type='process' AND ass.assignedTo=$userid AND checklist.process_ID=$process_ID AND checklist.process_ID=process.process_ID AND (process.isRemoved=1 OR process.processstatus_ID=15 OR folder.isRemoved=1 OR folder.status_ID=15) AND(ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8) AND process.folder_ID=folder.folder_ID","tbl_processST_process process,tbl_processST_assignment ass,tbl_processST_checklist checklist,tbl_processST_folder folder");

	// 	$checklist_assign_folder2=$this->general_model->fetch_specific_vals("process.processTitle, folder.folder_ID, ass.assign_ID, checklist.*","process.process_ID=$process_ID AND process.folder_ID=folder.folder_ID AND process.process_ID=checklist.process_ID AND checklist.checklist_ID=$checklist_ID AND $process_ID=checklist.process_ID AND ass.assignedTo=$userid AND type='folder' AND ass.folProCheTask_ID=folder.folder_ID AND (folder.isRemoved=1 OR process.isRemoved=1 OR process.processstatus_ID=15 OR folder.status_ID=15) AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8)","tbl_processST_assignment ass, tbl_processST_folder folder, tbl_processST_process process, tbl_processST_checklist checklist");

	// 	$checklist_assign_task2=$this->general_model->fetch_specific_vals("*","checkstat.checklist_ID=$checklist_ID AND checkstat.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedTo=$userid AND checkstat.checklist_ID=checklist.checklist_ID AND (checklist.isRemoved=1 OR checklist.isArchived=1) AND ass.assignmentRule_ID=6","tbl_processST_checklistStatus checkstat,tbl_processST_checklist checklist,tbl_processST_assignment ass");


	// 	///

	// 	$checklist_assigned=$this->general_model->fetch_specific_vals("checklist.*","ass.assignedTo=$userid AND ass.type='checklist' AND ass.folProCheTask_ID=$checklist_ID AND checklist.checklist_ID=$checklist_ID AND checklist.checklist_ID=ass.folProCheTask_ID AND ass.assignmentRule_ID=6","tbl_processST_assignment ass, tbl_processST_checklist checklist");

	// 	$checklist_owned=$this->general_model->fetch_specific_vals("*","createdBy=$userid AND checklist_ID=$checklist_ID","tbl_processST_checklist");

	// 	$checklist_assign_process=$this->general_model->fetch_specific_vals("process.processTitle,checklist.checklist_ID,checklist.checklistTitle,ass.assign_ID","process.process_ID=$process_ID AND process.process_ID=ass.folProCheTask_ID AND ass.folProCheTask_ID=$process_ID AND ass.type='process' AND ass.assignedTo=$userid AND checklist.process_ID=$process_ID AND checklist.process_ID=process.process_ID AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8)","tbl_processST_process process,tbl_processST_assignment ass,tbl_processST_checklist checklist");

	// 	$checklist_assign_folder=$this->general_model->fetch_specific_vals("process.processTitle, folder.folder_ID, ass.assign_ID, checklist.*","process.process_ID=$process_ID AND process.folder_ID=folder.folder_ID AND process.process_ID=checklist.process_ID AND checklist.checklist_ID=$checklist_ID AND $process_ID=checklist.process_ID AND ass.assignedTo=$userid AND type='folder' AND ass.folProCheTask_ID=folder.folder_ID AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3 OR ass.assignmentRule_ID=8)","tbl_processST_assignment ass, tbl_processST_folder folder, tbl_processST_process process, tbl_processST_checklist checklist");

	// 	$checklist_assign_task=$this->general_model->fetch_specific_vals("","checkstat.checklist_ID=$checklist_ID AND checkstat.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedTo=$userid AND checkstat.checklist_ID=checklist.checklist_ID","tbl_processST_checklistStatus checkstat,tbl_processST_checklist checklist,tbl_processST_assignment ass");

	// 	$all_priv=array_merge($checklist_assigned,$checklist_owned,$checklist_assign_process,$checklist_assign_folder,$checklist_assign_task);
	// 	$all_priv2=array_merge($checklist_assigned2,$checklist_owned2,$checklist_assign_process2,$checklist_assign_folder2,$checklist_assign_task2);

	// 	if ( ($data!=null && $processremoved==null) || ($data!=null && $checklistremoved==null))
	// 	{
	// 		return true;
	// 	}
	// 	else if ($all_priv!=null && $all_priv2==null){
	// 		return true;
	// 	}
	// 	else
	// 	{
	// 		$this->error_403();
	// 		return false;
	// 	}
	// }
	public function check_access_privilege_update($checklist_ID, $process_ID)
	{
		$all_priv = [];
		$all_priv2 = [];
		$userid = $this->session->userdata('uid');
		$data = $this->general_model->fetch_specific_vals("*", "user_ID=$userid", "tbl_processST_admin_access");
		$processremoved = $this->general_model->fetch_specific_vals("*", "process_ID=$process_ID AND (isRemoved=1 OR processstatus_ID=15)", "tbl_processST_process process");
		$checklistremoved = $this->general_model->fetch_specific_vals("*", "checklist_ID=$checklist_ID AND (isRemoved=1 OR isArchived=1)", "tbl_processST_checklist checklist");


		$checklist_assigned = $this->general_model->fetch_specific_vals("checklist.*", "ass.assignedTo=$userid AND ass.type='checklist' AND ass.folProCheTask_ID=$checklist_ID AND checklist.checklist_ID=$checklist_ID AND checklist.checklist_ID=ass.folProCheTask_ID AND ass.assignmentRule_ID=6", "tbl_processST_assignment ass, tbl_processST_checklist checklist");

		$checklist_owned = $this->general_model->fetch_specific_vals("*", "createdBy=$userid AND checklist_ID=$checklist_ID", "tbl_processST_checklist");

		$checklist_assign_process = $this->general_model->fetch_specific_vals("process.processTitle,checklist.checklist_ID,checklist.checklistTitle,ass.assign_ID", "process.process_ID=$process_ID AND process.process_ID=ass.folProCheTask_ID AND ass.folProCheTask_ID=$process_ID AND ass.type='process' AND ass.assignedTo=$userid AND checklist.process_ID=$process_ID AND checklist.process_ID=process.process_ID AND ass.assignmentRule_ID=1", "tbl_processST_process process,tbl_processST_assignment ass,tbl_processST_checklist checklist");

		$checklist_assign_folder = $this->general_model->fetch_specific_vals("process.processTitle, folder.folder_ID, ass.assign_ID, checklist.*", "process.process_ID=$process_ID AND process.folder_ID=folder.folder_ID AND process.process_ID=checklist.process_ID AND checklist.checklist_ID=$checklist_ID AND $process_ID=checklist.process_ID AND ass.assignedTo=$userid AND type='folder' AND ass.folProCheTask_ID=folder.folder_ID AND ass.assignmentRule_ID=1", "tbl_processST_assignment ass, tbl_processST_folder folder, tbl_processST_process process, tbl_processST_checklist checklist");

		$checklist_assign_task = $this->general_model->fetch_specific_vals("", "checkstat.checklist_ID=$checklist_ID AND checkstat.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedTo=$userid AND checkstat.checklist_ID=checklist.checklist_ID AND ass.assignmentRule_ID=6", "tbl_processST_checklistStatus checkstat,tbl_processST_checklist checklist,tbl_processST_assignment ass");

		//
		//
		$checklist_assigned2 = $this->general_model->fetch_specific_vals("checklist.*", "ass.assignedTo=$userid AND ass.type='checklist' AND ass.folProCheTask_ID=$checklist_ID AND (checklist.isRemoved=1 OR checklist.isArchived=1) AND checklist.checklist_ID=$checklist_ID AND checklist.checklist_ID=ass.folProCheTask_ID AND ass.assignmentRule_ID=6", "tbl_processST_assignment ass, tbl_processST_checklist checklist");

		$checklist_owned2 = $this->general_model->fetch_specific_vals("*", "createdBy=$userid AND checklist_ID=$checklist_ID AND (isRemoved=1 OR isArchived=1)", "tbl_processST_checklist");

		$checklist_assign_process2 = $this->general_model->fetch_specific_vals("process.processTitle,checklist.checklist_ID,checklist.checklistTitle,ass.assign_ID", "process.process_ID=$process_ID AND process.process_ID=ass.folProCheTask_ID AND ass.folProCheTask_ID=$process_ID AND ass.type='process' AND ass.assignedTo=$userid AND checklist.process_ID=$process_ID AND checklist.process_ID=process.process_ID AND (process.isRemoved=1 OR process.processstatus_ID=15 OR folder.isRemoved=1 OR folder.status_ID=15) AND ass.assignmentRule_ID=1 AND process.folder_ID=folder.folder_ID", "tbl_processST_process process,tbl_processST_assignment ass,tbl_processST_checklist checklist,tbl_processST_folder folder");

		$checklist_assign_folder2 = $this->general_model->fetch_specific_vals("process.processTitle, folder.folder_ID, ass.assign_ID, checklist.*", "process.process_ID=$process_ID AND process.folder_ID=folder.folder_ID AND process.process_ID=checklist.process_ID AND checklist.checklist_ID=$checklist_ID AND $process_ID=checklist.process_ID AND ass.assignedTo=$userid AND type='folder' AND ass.folProCheTask_ID=folder.folder_ID AND (folder.isRemoved=1 OR process.isRemoved=1 OR process.processstatus_ID=15 OR folder.status_ID=15) AND ass.assignmentRule_ID=1", "tbl_processST_assignment ass, tbl_processST_folder folder, tbl_processST_process process, tbl_processST_checklist checklist");

		$checklist_assign_task2 = $this->general_model->fetch_specific_vals("*", "checkstat.checklist_ID=$checklist_ID AND checkstat.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedTo=$userid AND checkstat.checklist_ID=checklist.checklist_ID AND (checklist.isRemoved=1 OR checklist.isArchived=1) AND ass.assignmentRule_ID=6", "tbl_processST_checklistStatus checkstat,tbl_processST_checklist checklist,tbl_processST_assignment ass");


		$all_priv = array_merge($checklist_assigned, $checklist_owned, $checklist_assign_process, $checklist_assign_folder, $checklist_assign_task);
		$all_priv2 = array_merge($checklist_assigned2, $checklist_owned2, $checklist_assign_process2, $checklist_assign_folder2, $checklist_assign_task2);

		if (($data != null && $processremoved == null) || ($data != null && $checklistremoved == null)) {
			return true;
		} else if ($all_priv != null && $all_priv2 == null) {
			return true;
		}
		// else if($all_priv!=null){
		// 	return true;
		// }
		else {
			$this->error_403();
			return false;
		}
	}
	public function check_ifowned()
	{
		$checkid = $this->input->post("checklist_ID");
		$data = $this->general_model->fetch_specific_val("*", "checklist_ID=$checkid", "tbl_processST_checklist");
		echo (count($data) > 0) ? json_encode($data) : 0;
	}
	public function check_task_required()
	{
		$taskid = 73;
		$checkStat = 50;
		// $taskid=$this->input->post("task_ID");
		$data = $this->general_model->fetch_specific_vals("subtask.complabel", "subtask.task_ID=$taskid AND subtask.required='yes' AND checkstat.task_ID=$taskid AND checkstat.checklistStatus_ID=$checkStat AND checkstat.isCompleted=2 AND answer.subtask_ID=subtask.subTask_ID AND answer.subtask_ID=subtask.subTask_ID AND (answer.answer=' ' OR answer.answer IS NULL)", "tbl_processST_subtask subtask,tbl_processST_checklistStatus checkstat,tbl_processST_answer answer");
		var_dump($data);
		// echo (count($data)>0) ? json_encode($data) : 0;
	}
	public function error_403()
	{
		$this->load->view('errors/custom/error_403');
	}
	public function getchecklist_count($stat, $process_ID)
	{
		if ($stat == 0) {
			$where = "process_ID = $process_ID AND isRemoved=0 AND isArchived=0";
		} else {
			$where = "process_ID = $process_ID AND status_ID=$stat AND isRemoved=0 AND isArchived=0";
		}
		$fields = "count(checklist_ID) as checkCount";
		$record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_processST_checklist');
		return $record[0]->checkCount;
	}

	public function gettaskstatus_count($stat, $checklist_ID)
	{
		if ($stat == 3) {
			$where = "checklist_ID = $checklist_ID AND isCompleted='3' AND isRemoved=0";
		} else if ($stat == 2) {
			$where = "checklist_ID = $checklist_ID AND isCompleted='2' AND isRemoved=0";
		} else {
			$where = "checklist_ID = $checklist_ID AND isRemoved=0";
		}
		$fields = "count(checklistStatus_ID) as checkCount";
		$record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_processST_checklistStatus');
		return $record[0]->checkCount;
	}
	public function display_allchecklists()
	{
		$word = $this->input->post("word");
		$select_val = $this->input->post("statval");
		$checklist_ids = $this->input->post("checklist_ids");
		$whichfunction = $this->input->post("whichfunction");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$select_val = ($select_val != null or !empty($select_val) or $select_val != "") ? $select_val : null;
		$loop = 0;
		$data = $this->process_model->get_alldisplaychecklist($checklist_ids, $word, $select_val);

		if (count($data) > 0) {
			foreach ($data as $checklist) {
				$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
				$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
				$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);

				$data[$loop] = [
					"checklist" => $checklist,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		echo json_encode(['data' => $data, 'whichfunction' => $whichfunction]);
	}
	//DISPLAY PROCESS TEMPLATE
	public function display_processtemplates()
	{
		$word = $this->input->post("word");
		$select_val = $this->input->post("statval");
		$process_ids = $this->input->post("process_ids");
		$whichfunction = $this->input->post("whichfunction");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$select_val = ($select_val != null or !empty($select_val) or $select_val != "") ? $select_val : null;
		$data = $this->process_model->get_allprocess($process_ids, $word, $select_val);
		$loop = 0;

		if (count($data) > 0) {
			foreach ($data as $processtemp) {
				$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
				$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
				$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
				$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
				$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
				$data[$loop] = [
					"process" => $processtemp,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		echo json_encode(['data' => $data, 'whichfunction' => $whichfunction]);
	}

	public function display_ownprocess()
	{
		$userid = $this->session->userdata('uid');
		$select_val = $this->input->post("statval");
		$process_ids = $this->input->post("process_ids");
		$select_val = ($select_val != null or !empty($select_val) or $select_val != "") ? $select_val : null;
		$word = $this->input->post("word");
		$whichfunction = $this->input->post("whichfunction");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$data = $this->process_model->get_ownprocess($process_ids, $word, $select_val, $userid);
		$loop = 0;
		if (count($data) > 0) {
			foreach ($data as $processtemp) {
				$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
				$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
				$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
				$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
				$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
				$data[$loop] = [
					"process" => $processtemp,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		echo json_encode(['data' => $data, 'whichfunction' => $whichfunction]);
	}
	public function display_processtemplates_subfolder()
	{
		$word = $this->input->post("word");
		$folderid = $this->input->post("folder_ID");
		$select_val = $this->input->post("statval");
		$process_ids = $this->input->post("process_ids");
		$whichfunction = $this->input->post("whichfunction");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$select_val = ($select_val != null or !empty($select_val) or $select_val != "") ? $select_val : null;
		$data = $this->process_model->get_allprocess_subfolder($process_ids, $folderid, $word, $select_val);
		$loop = 0;
		if (count($data) > 0) {
			foreach ($data as $processtemp) {
				$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
				$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
				$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
				$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
				$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
				$data[$loop] = [
					"process" => $processtemp,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		echo json_encode(['data' => $data, 'whichfunction' => $whichfunction]);
	}

	public function display_assignedprocess_insidefolder()
	{
		$word = $this->input->post("word");
		$select_val = $this->input->post("statval");
		$folderid = $this->input->post("folder_ID");
		$userid = $this->session->userdata('uid');

		$whichfunction = (int) $this->input->post("whichfunction");
		$process_ids = $this->input->post("process_ids");
		$my_process = [];
		$loop = 0;

		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$select_val = ($select_val != null or !empty($select_val) or $select_val != "") ? $select_val : null;

		if ($whichfunction == 0) {
			$assigned = $this->process_model->get_assignedprocess_insidefolder($process_ids, $userid, $folderid, $word, $select_val);
			$process_ids = $this->addToArray_process($assigned, $process_ids);
			$my_process = $assigned;
			$cnt = COUNT($my_process);
			if ($cnt < 6) {
				$whichfunction = 1;
				$perpage = 6 - $cnt;
				$assigned_processunder_folder = $this->process_model->get_processunder_folder_insidefolder($process_ids, $userid, $folderid, $word, $select_val, $perpage);
				$process_ids = $this->addToArray_process($assigned_processunder_folder, $process_ids);
				$my_process = array_merge($my_process, $assigned_processunder_folder);
				$cnt = COUNT($my_process);
				if ($cnt < 6) {
					$whichfunction = 2;
					$perpage = 6 - $cnt;
					$assigned_ownedprocessunder_folder = $this->process_model->get_ownedprocessunder_folder_insidefolder($process_ids, $userid, $folderid, $word, $select_val, $perpage);

					$my_process = array_merge($my_process, $assigned_ownedprocessunder_folder);
				}
			}
		} else if ($whichfunction == 1) {
			$assigned_processunder_folder = $this->process_model->get_processunder_folder_insidefolder($process_ids, $userid, $folderid, $word, $select_val);
			$process_ids = $this->addToArray_process($assigned_processunder_folder, $process_ids);
			$my_process = $assigned_processunder_folder;
			$cnt = COUNT($my_process);
			if ($cnt < 6) {
				$whichfunction = 2;
				$perpage = 6 - $cnt;
				$assigned_ownedprocessunder_folder = $this->process_model->get_ownedprocessunder_folder_insidefolder($process_ids, $userid, $folderid, $word, $select_val, $perpage);
				$my_process = array_merge($my_process, $assigned_ownedprocessunder_folder);
			}
		} else {
			$assigned_ownedprocessunder_folder = $this->process_model->get_ownedprocessunder_folder_insidefolder($process_ids, $userid, $folderid, $word, $select_val);
			$my_process = $assigned_ownedprocessunder_folder;
		}

		if (count($my_process) > 0) {
			foreach ($my_process as $processtemp) {
				$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
				$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
				$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
				$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
				$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
				$my_process[$loop] = [
					"process" => $processtemp,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		echo json_encode(['data' => $my_process, 'whichfunction' => $whichfunction]);
	}

	//CHECKLIST DISPLAY
	public function testing()
	{
		$userid = 802;
		$assigned = $this->process_model->get_foldertesting($userid);
		foreach ($assigned as $a) {
			$y = $a->folder_ID;
			$j = $this->process_model->get_processfolder_testing($y);
			// var_dump($j);
		}
	}

	public function display_assignedprocess()
	{
		$userid = $this->session->userdata('uid');
		$word = $this->input->post("word");
		$select_val = $this->input->post("statval");
		$process_ids = $this->input->post("process_ids");
		$select_val = ($select_val != null or !empty($select_val) or $select_val != "") ? $select_val : null;

		$whichfunction = (int) $this->input->post("whichfunction");

		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$loop = 0;

		$assigned = [];
		$ownedprocess_folder = [];
		$owned = [];
		$allprocess = [];
		//GET ASSIGNED!

		if ($whichfunction == 0) {
			$assigned = $this->process_model->get_assignedprocess($process_ids, $word, $select_val, $userid);
			$process_ids = $this->addToArray_process($assigned, $process_ids);
			$my_process = $assigned;
			$cnt = COUNT($assigned);
			if ($cnt < 6) {
				$whichfunction = 1;
				$perpage = 6 - $cnt;
				$owned = $this->process_model->get_ownprocess($process_ids, $word, $select_val, $userid, $perpage);
				//                var_dump($owned);
				$process_ids = $this->addToArray_process($owned, $process_ids);
				$my_process = array_merge($my_process, $owned);
				$cnt = COUNT($my_process);
				if ($cnt < 6) {
					$whichfunction = 2;
					$perpage = 6 - $cnt;
					$ownedprocess_folder = $this->process_model->get_ownedprocessunder_folder($process_ids, $word, $select_val, $userid, $perpage);
					//                    var_dump($ownedprocess_folder);
					$process_ids = $this->addToArray_process($ownedprocess_folder, $process_ids);
					$my_process = array_merge($my_process, $ownedprocess_folder);
					$cnt = COUNT($my_process);

					if ($cnt < 6) {
						$whichfunction = 3;
						$perpage = 6 - $cnt;
						$allprocess = $this->process_model->get_processunder_folder($process_ids, $word, $select_val, $userid, $perpage);
						$my_process = array_merge($my_process, $allprocess);
					}
				}
			}
		} else if ($whichfunction == 1) {
			$owned = $this->process_model->get_ownprocess($process_ids, $word, $select_val, $userid);
			$process_ids = $this->addToArray_process($owned, $process_ids);
			$my_process = $owned;
			$cnt = COUNT($owned);
			if ($cnt < 6) {
				$whichfunction = 2;
				$perpage = 6 - $cnt;
				$ownedprocess_folder = $this->process_model->get_ownedprocessunder_folder($process_ids, $word, $select_val, $userid, $perpage);
				$process_ids = $this->addToArray_process($ownedprocess_folder, $process_ids);
				$my_process = array_merge($my_process, $ownedprocess_folder);
				$cnt = COUNT($my_process);
				if ($cnt < 6) {
					$whichfunction = 3;
					$perpage = 6 - $cnt;
					$allprocess = $this->process_model->get_processunder_folder($process_ids, $word, $select_val, $userid, $perpage);

					$my_process = array_merge($my_process, $allprocess);
				}
			}
		} else if ($whichfunction == 2) {
			$ownedprocess_folder = $this->process_model->get_ownedprocessunder_folder($process_ids, $word, $select_val, $userid);
			$process_ids = $this->addToArray_process($ownedprocess_folder, $process_ids);
			$my_process = $ownedprocess_folder;
			$cnt = COUNT($ownedprocess_folder);
			if ($cnt < 6) {
				$whichfunction = 3;
				$perpage = 6 - $cnt;
				$allprocess = $this->process_model->get_processunder_folder($process_ids, $word, $select_val, $userid, $perpage);
				$my_process = array_merge($my_process, $allprocess);
			}
		} else {
			$allprocess = $this->process_model->get_processunder_folder($process_ids, $word, $select_val, $userid);
			$my_process = $allprocess;
		}


		if (count($my_process) > 0) {
			foreach ($my_process as $processtemp) {
				$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
				$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
				$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
				$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
				$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
				$my_process[$loop] = [
					"process" => $processtemp,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		echo json_encode(['data' => $my_process, 'whichfunction' => $whichfunction]);
	}

	public function display_morefolders()
	{
		$userid = $this->session->userdata('uid');
		$lastid = $_POST['folder_ID'];
		$assign = $_POST['assign'];
		$str = ($_POST['str'] == "") ? "" : $_POST['str'];
		if ($assign == "1") {
			$data = $this->process_model->get_nextallfolders($lastid, $str);
		} else if ($assign == "2") {
			$data = $this->process_model->get_nextownedfolders($lastid, $str, $userid);
		} else if ($assign == "3") {
			$data = $this->process_model->get_nextassignedfolders($lastid, $str, $userid);
		}
		echo json_encode($data);
	}
	public function display_moreprocess()
	{
		$userid = $this->session->userdata('uid');
		$lastid = $_POST['process_ID'];
		$assign = $_POST['assign'];
		$str = ($_POST['str'] == "") ? "" : $_POST['str'];
		$loop = 0;

		if ($assign == "1") {
			$data = $this->process_model->get_nextallprocesses($lastid, $str);
			if (count($data) > 0) {
				foreach ($data as $processtemp) {
					$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
					$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
					$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
					$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
					$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
					$data[$loop] = [
						"process" => $processtemp,
						"statuses" => $stat
					];
					$loop++;
				}
			}
			echo (count($data) > 0) ? json_encode($data) : 0;
		} else if ($assign == "2") {
			$data = $this->process_model->get_nextownedprocesses($lastid, $str, $userid);
			if (count($data) > 0) {
				foreach ($data as $processtemp) {
					$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
					$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
					$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
					$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
					$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
					$data[$loop] = [
						"process" => $processtemp,
						"statuses" => $stat
					];
					$loop++;
				}
			}
			echo (count($data) > 0) ? json_encode($data) : 0;
		} else if ($assign == "3") {
			$my_process = [];
			$type = "all";
			$assigned = $this->process_model->get_nextassignedprocesses($lastid, $str, $userid);
			$owned = $this->process_model->get_nextownedprocesses($lastid, $str, $userid);
			if ($type == "all") {
				$my_process = array_merge($assigned, $owned);
			} else if ($type == "owned") {
				$my_process = $owned;
			} else {
				$my_process = $assigned;
			}
			echo (count($my_process) > 0) ? json_encode($my_process) : 0;
		}
	}
	public function display_tasks()
	{
		$process_ID = $_POST['process_ID'];
		$t = $this->general_model->fetch_specific_vals("task.*,process.*", "task.process_ID=$process_ID AND task.process_ID=process.process_ID AND task.isRemoved=0", "tbl_processST_task task, tbl_processST_process process", "task.sequence");
		$tt = "SELECT processstatus_ID,processType_ID FROM tbl_processST_process WHERE process_ID=$process_ID";
		$tm = "SELECT max(sequence) as tsequence FROM `tbl_processST_task` WHERE process_ID=$process_ID AND isRemoved=0";
		$dt = $this->general_model->custom_query($tm);
		$ttype = $this->general_model->custom_query($tt);
		$loop = 0;
		if (count($t) > 0) {
			foreach ($t as $ts) {
				$subtask['max_num'] = $this->fetch_allsubtask_count2($ts->task_ID);
				$task_details[$loop] = [
					"task" => $ts,
					"max" => $subtask
				];
				$loop++;
			}
		}
		echo json_encode(['task_details' => $task_details, 'task_max' => $dt, 'process_type' => $ttype]);
	}
	public function display_tasks_update()
	{
		$process_ID = $_POST['process_ID'];
		$t = $this->general_model->fetch_specific_vals("task.*,process.*", "task.process_ID=$process_ID AND task.process_ID=process.process_ID AND task.isRemoved=0", "tbl_processST_task task, tbl_processST_process process", "task.sequence ASC");
		$tm = "SELECT max(sequence) as tsequence FROM `tbl_processST_task` WHERE process_ID=$process_ID AND isRemoved=0";
		$dt = $this->general_model->custom_query($tm);
		$loop = 0;
		if (count($t) > 0) {
			foreach ($t as $ts) {
				$subtask['max_num'] = $this->fetch_allsubtask_count2($ts->task_ID);
				$task_details[$loop] = [
					"task" => $ts,
					"max" => $subtask
				];
				$loop++;
			}
		}
		echo json_encode(['task_details' => $task_details, 'task_max' => $dt]);
	}
	public function display_tasks_answerable()
	{
		$checklist_ID = $_POST['checklist_ID'];
		$userid = $_SESSION["uid"];
		$assignment_check = $this->Check_task_assignment_before_display($checklist_ID);
		$data['all']=$this->gettaskstatus_count(0,$checklist_ID);
		$data['pending']=$this->gettaskstatus_count(2,$checklist_ID);
		$data['completed']=$this->gettaskstatus_count(3,$checklist_ID);

		if ($assignment_check != null) {
			$q = "SELECT task.*,checkStatus.*,ass.assignmentRule_ID FROM tbl_processST_task task, tbl_processST_checklistStatus checkStatus,tbl_processST_assignment ass WHERE task.task_ID=checkStatus.task_ID AND checkStatus.checklist_ID=$checklist_ID AND checkStatus.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedTo=$userid AND task.isRemoved=0";
			$data['chtask'] = $this->general_model->custom_query($q);
		} else {
			$data['chtask'] = $this->general_model->fetch_specific_vals("task.*,checkStatus.*", "task.task_ID=checkStatus.task_ID AND checkStatus.checklist_ID=$checklist_ID AND task.isRemoved=0", "tbl_processST_task task, tbl_processST_checklistStatus checkStatus");
		}
		echo json_encode($data);
	}
	public function display_addedmembers()
	{
		$fpct_id = $_POST['folProCheTask_ID'];
		$type = $_POST['type'];
		$data = $this->general_model->fetch_specific_vals("assignment.*,assr.*", "assignment.folProCheTask_ID=$fpct_id AND assignment.type='$type' AND assr.assignmentRule_ID=assignment.assignmentRule_ID", "tbl_processST_assignment assignment, tbl_processST_assignmentRule assr");
		echo json_encode($data);
	}

	//CHECKLIST INSIDE PROCESS TEMPLATES
	public function display_checklist()
	{
		$userid = $this->session->userdata('uid');
		$process_ID = $this->input->post("process_ID");
		$select_val = $this->input->post("statval");

		$check_ids = $this->input->post("check_ids");
		$whichfunction = $this->input->post("whichfunction");
		$word = $this->input->post("word");
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$select_val = ($select_val != null or !empty($select_val) or $select_val != "") ? $select_val : null;
		$data = $this->process_model->get_checklist($process_ID, $word, $select_val, $check_ids);
		$loop = 0;

		if (count($data) > 0) {
			foreach ($data as $checklist) {
				$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
				$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
				$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);

				$data[$loop] = [
					"checklist" => $checklist,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		echo json_encode(['data' => $data, 'whichfunction' => $whichfunction]);
	}
	public function display_ownchecklist_underprocess()
	{
		$userid = $this->session->userdata('uid');
		$process_ID = $this->input->post("process_ID");
		$word = $this->input->post("word");
		$word  = ($word != null or !empty($word) or $word != "") ? $word : null;
		$data = $this->process_model->get_ownedchecklists_underprocess($userid, $process_ID, $word);
		echo (count($data) > 0) ? json_encode($data) : 0;
	}

	public function display_ownchecklists()
	{
		$userid = $this->session->userdata('uid');
		$word = $this->input->post("word");
		$select_val = $this->input->post("statval");
		$checklist_ids = $this->input->post("checklist_ids");
		$whichfunction = $this->input->post("whichfunction");

		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$select_val = ($select_val != null or !empty($select_val) or $select_val != "") ? $select_val : null;

		$owned = $this->process_model->get_ownedchecklists($checklist_ids, $word, $select_val, $userid);
		$loop = 0;

		if (count($owned) > 0) {
			foreach ($owned as $checklist) {
				$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
				$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
				$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);

				$owned[$loop] = [
					"checklist" => $checklist,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		echo json_encode(['data' => $owned, 'whichfunction' => $whichfunction]);
	}


	public function display_assignedchecklists()
	{
		$userid = $this->session->userdata('uid');
		$word = $this->input->post("word");
		$select_val = $this->input->post("statval");
		$checklist_ids = $this->input->post("checklist_ids");
		$whichfunction = (int) $this->input->post('whichfunction');
		$count = 0;
		$my_process = [];
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$select_val = ($select_val != null && !empty($select_val) && $select_val != "") ? $select_val : null;
		$loop = 0;

		//------------------------------------------------------------------------------------------------------------------------------------------------
		if ($whichfunction == 0) {
			$assigned = $this->process_model->get_assignedchecklists($checklist_ids, $word, $select_val, $userid);
			$checklist_ids = $this->addToArray_checklist($assigned, $checklist_ids);
			$my_process = $assigned;
			$cnt = COUNT($assigned);
			if ($cnt < 6) {
				$whichfunction = 1;
				$perpage = 6 - $cnt;
				$owned = $this->process_model->get_ownedchecklists($checklist_ids, $word, $select_val, $userid, $perpage);
				$checklist_ids = $this->addToArray_checklist($owned, $checklist_ids);
				$my_process = array_merge($my_process, $owned);


				$cnt = COUNT($my_process);
				if ($cnt < 6) {
					$whichfunction = 2;
					$perpage = 6 - $cnt;
					$assigned_underprocess = $this->process_model->get_checklistunderprocess($checklist_ids, $word, $select_val, $userid, $perpage);
					$checklist_ids = $this->addToArray_checklist($assigned_underprocess, $checklist_ids);
					$my_process = array_merge($my_process, $assigned_underprocess);
					$cnt = COUNT($my_process);

					if ($cnt < 6) {
						$whichfunction = 3;
						$perpage = 6 - $cnt;

						$assigned_ownedunderprocess = $this->process_model->get_ownedchecklistunderprocess($checklist_ids, $word, $select_val, $userid, $perpage);
						$checklist_ids = $this->addToArray_checklist($assigned_ownedunderprocess, $checklist_ids);
						$my_process = array_merge($my_process, $assigned_ownedunderprocess);
						$cnt = COUNT($my_process);
						if ($cnt < 6) {
							$whichfunction = 4;
							$perpage = 6 - $cnt;

							$assigned_underfolder = $this->process_model->get_checklistunder_processfolder($checklist_ids, $word, $select_val, $userid, $perpage); //--
							$checklist_ids = $this->addToArray_checklist($assigned_underfolder, $checklist_ids);
							$my_process = array_merge($my_process, $assigned_underfolder);
							$cnt = COUNT($my_process);
							if ($cnt < 6) {
								$whichfunction = 5;
								$perpage = 6 - $cnt;

								$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_processfolder($checklist_ids, $word, $select_val, $userid, $perpage);
								$checklist_ids = $this->addToArray_checklist($assigned_ownedunderfolder, $checklist_ids);
								$my_process = array_merge($my_process, $assigned_ownedunderfolder);
								$cnt = COUNT($my_process);
								if ($cnt < 6) {
									$whichfunction = 6;
									$perpage = 6 - $cnt;

									$assigned_taskunderchecklist = $this->process_model->get_assignedtasks_underchecklists($checklist_ids, $word, $select_val, $userid, $perpage);

									$my_process = array_merge($my_process, $assigned_taskunderchecklist);
								}
							}
						}
					}
				}
			}
		} else if ($whichfunction == 1) {
			$owned = $this->process_model->get_ownedchecklists($checklist_ids, $word, $select_val, $userid);
			$checklist_ids = $this->addToArray_checklist($owned, $checklist_ids);
			$my_process = $owned;
			$cnt = COUNT($owned);
			if ($cnt < 6) {
				$whichfunction = 2;
				$perpage = 6 - $cnt;
				$assigned_underprocess = $this->process_model->get_checklistunderprocess($checklist_ids, $word, $select_val, $userid, $perpage);
				$checklist_ids = $this->addToArray_checklist($assigned_underprocess, $checklist_ids);
				$my_process = array_merge($my_process, $assigned_underprocess);
				$cnt = COUNT($my_process);

				if ($cnt < 6) {
					$whichfunction = 3;
					$perpage = 6 - $cnt;

					$assigned_ownedunderprocess = $this->process_model->get_ownedchecklistunderprocess($checklist_ids, $word, $select_val, $userid, $perpage);
					$checklist_ids = $this->addToArray_checklist($assigned_ownedunderprocess, $checklist_ids);
					$my_process = array_merge($my_process, $assigned_ownedunderprocess);
					$cnt = COUNT($my_process);
					if ($cnt < 6) {
						$whichfunction = 4;
						$perpage = 6 - $cnt;

						$assigned_underfolder = $this->process_model->get_checklistunder_processfolder($checklist_ids, $word, $select_val, $userid, $perpage);
						$checklist_ids = $this->addToArray_checklist($assigned_underfolder, $checklist_ids);
						$my_process = array_merge($my_process, $assigned_underfolder);
						$cnt = COUNT($my_process);
						if ($cnt < 6) {
							$whichfunction = 5;
							$perpage = 6 - $cnt;

							$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_processfolder($checklist_ids, $word, $select_val, $userid, $perpage);
							$checklist_ids = $this->addToArray_checklist($assigned_ownedunderfolder, $checklist_ids);
							$my_process = array_merge($my_process, $assigned_ownedunderfolder);
							$cnt = COUNT($my_process);
							if ($cnt < 6) {
								$whichfunction = 6;
								$perpage = 6 - $cnt;

								$assigned_taskunderchecklist = $this->process_model->get_assignedtasks_underchecklists($checklist_ids, $word, $select_val, $userid, $perpage);
								$my_process = array_merge($my_process, $assigned_taskunderchecklist);
							}
						}
					}
				}
			}
		} else if ($whichfunction == 2) {
			$assigned_underprocess = $this->process_model->get_checklistunderprocess($checklist_ids, $word, $select_val, $userid);
			$checklist_ids = $this->addToArray_checklist($assigned_underprocess, $checklist_ids);
			$my_process = $assigned_underprocess;
			$cnt = COUNT($assigned_underprocess);

			if ($cnt < 6) {
				$whichfunction = 3;
				$perpage = 6 - $cnt;

				$assigned_ownedunderprocess = $this->process_model->get_ownedchecklistunderprocess($checklist_ids, $word, $select_val, $userid, $perpage);
				$checklist_ids = $this->addToArray_checklist($assigned_ownedunderprocess, $checklist_ids);
				$my_process = array_merge($my_process, $assigned_ownedunderprocess);
				$cnt = COUNT($my_process);
				if ($cnt < 6) {
					$whichfunction = 4;
					$perpage = 6 - $cnt;

					$assigned_underfolder = $this->process_model->get_checklistunder_processfolder($checklist_ids, $word, $select_val, $userid, $perpage);
					$checklist_ids = $this->addToArray_checklist($assigned_underfolder, $checklist_ids);
					$my_process = array_merge($my_process, $assigned_underfolder);
					$cnt = COUNT($my_process);
					if ($cnt < 6) {
						$whichfunction = 5;
						$perpage = 6 - $cnt;

						$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_processfolder($checklist_ids, $word, $select_val, $userid, $perpage);
						$checklist_ids = $this->addToArray_checklist($assigned_ownedunderfolder, $checklist_ids);
						$my_process = array_merge($my_process, $assigned_ownedunderfolder);
						$cnt = COUNT($my_process);
						if ($cnt < 6) {
							$whichfunction = 6;
							$perpage = 6 - $cnt;

							$assigned_taskunderchecklist = $this->process_model->get_assignedtasks_underchecklists($checklist_ids, $word, $select_val, $userid, $perpage);

							$my_process = array_merge($my_process, $assigned_taskunderchecklist);
						}
					}
				}
			}
		} else if ($whichfunction == 3) {

			$assigned_ownedunderprocess = $this->process_model->get_ownedchecklistunderprocess($checklist_ids, $word, $select_val, $userid);
			$checklist_ids = $this->addToArray_checklist($assigned_ownedunderprocess, $checklist_ids);
			$my_process = $assigned_ownedunderprocess;
			$my_process = array_merge($my_process, $assigned_ownedunderprocess);
			$cnt = COUNT($my_process);
			if ($cnt < 6) {
				$whichfunction = 4;
				$perpage = 6 - $cnt;

				$assigned_underfolder = $this->process_model->get_checklistunder_processfolder($checklist_ids, $word, $select_val, $userid, $perpage);
				$checklist_ids = $this->addToArray_checklist($assigned_underfolder, $checklist_ids);
				$my_process = array_merge($my_process, $assigned_underfolder);
				$cnt = COUNT($my_process);
				if ($cnt < 6) {
					$whichfunction = 5;
					$perpage = 6 - $cnt;

					$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_processfolder($checklist_ids, $word, $select_val, $userid, $perpage);
					$checklist_ids = $this->addToArray_checklist($assigned_ownedunderfolder, $checklist_ids);
					$my_process = array_merge($my_process, $assigned_ownedunderfolder);
					$cnt = COUNT($my_process);
					if ($cnt < 6) {
						$whichfunction = 6;
						$perpage = 6 - $cnt;

						$assigned_taskunderchecklist = $this->process_model->get_assignedtasks_underchecklists($checklist_ids, $word, $select_val, $userid, $perpage);

						$my_process = array_merge($my_process, $assigned_taskunderchecklist);
					}
				}
			}
		} else if ($whichfunction == 4) {
			$assigned_underfolder = $this->process_model->get_checklistunder_processfolder($checklist_ids, $word, $select_val, $userid);
			$checklist_ids = $this->addToArray_checklist($assigned_underfolder, $checklist_ids);
			$my_process = $assigned_underfolder;
			$cnt = COUNT($assigned_underfolder);
			if ($cnt < 6) {
				$whichfunction = 5;
				$perpage = 6 - $cnt;

				$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_processfolder($checklist_ids, $word, $select_val, $userid, $perpage);
				$checklist_ids = $this->addToArray_checklist($assigned_ownedunderfolder, $checklist_ids);
				$my_process = array_merge($my_process, $assigned_ownedunderfolder);
				$cnt = COUNT($my_process);
				if ($cnt < 6) {
					$whichfunction = 6;
					$perpage = 6 - $cnt;

					$assigned_taskunderchecklist = $this->process_model->get_assignedtasks_underchecklists($checklist_ids, $word, $select_val, $userid, $perpage);
					$my_process = array_merge($my_process, $assigned_taskunderchecklist);
				}
			}
		} else if ($whichfunction == 4) {
			$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_processfolder($checklist_ids, $word, $select_val, $userid);
			$checklist_ids = $this->addToArray_checklist($assigned_ownedunderfolder, $checklist_ids);
			$my_process = $assigned_ownedunderfolder;
			$cnt = COUNT($assigned_ownedunderfolder);
			if ($cnt < 6) {
				$whichfunction = 6;
				$perpage = 6 - $cnt;

				$assigned_taskunderchecklist = $this->process_model->get_assignedtasks_underchecklists($checklist_ids, $word, $select_val, $userid, $perpage);
				$my_process = array_merge($my_process, $assigned_taskunderchecklist);
			}
		} else {
			$assigned_taskunderchecklist = $this->process_model->get_assignedtasks_underchecklists($checklist_ids, $word, $select_val, $userid);
			$my_process = $assigned_taskunderchecklist;
		}


		foreach ($my_process as $checklist) {
			$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
			$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
			$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);

			$my_process[$loop] = [
				"checklist" => $checklist,
				"statuses" => $stat
			];
			$loop++;
		}
		echo json_encode(['data' => $my_process, 'whichfunction' => $whichfunction]);
	}

	public function display_assignedchecklists_insideprocess()
	{

		$check_ids = $this->input->post("check_ids");
		$whichfunction = (int) $this->input->post("whichfunction");
		$userid = $this->session->userdata('uid');
		$word = $this->input->post("word");
		$process_ID = $this->input->post("process_ID");
		$select_val = $this->input->post("statval");
		$my_checklist = [];
		$word = ($word != null or !empty($word) or $word != "") ? $word : null;
		$select_val = ($select_val != null or !empty($select_val) or $select_val != "") ? $select_val : null;


		if ($whichfunction == 0) {
			$assigned = $this->process_model->get_assignedchecklists_underprocess($check_ids, $userid, $process_ID, $word, $select_val);
			$check_ids = $this->addToArray_process($assigned, $check_ids);
			$my_checklist = $assigned;
			$cnt = COUNT($assigned);
			if ($cnt < 6) {
				$whichfunction = 1;
				$perpage = 6 - $cnt;
				$assigned_underprocess = $this->process_model->get_checklistunder_insideprocess($check_ids, $userid, $process_ID, $word, $select_val, $perpage);
				$check_ids = $this->addToArray_checklist($assigned_underprocess, $check_ids);
				$my_checklist = array_merge($my_checklist, $assigned_underprocess);
				$cnt = COUNT($my_checklist);
				if ($cnt < 6) {
					$whichfunction = 2;
					$perpage = 6 - $cnt;
					$assigned_ownedunderprocess = $this->process_model->get_ownedchecklistunder_insideprocess($check_ids, $userid, $process_ID, $word, $select_val, $perpage);
					$check_ids = $this->addToArray_checklist($assigned_ownedunderprocess, $check_ids);
					$my_checklist = array_merge($my_checklist, $assigned_ownedunderprocess);
					$cnt = COUNT($my_checklist);

					if ($cnt < 6) {
						$whichfunction = 2;
						$perpage = 6 - $cnt;
						$assigned_underfolder = $this->process_model->get_checklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word, $select_val, $perpage);
						$check_ids = $this->addToArray_checklist($assigned_underfolder, $check_ids);
						$my_checklist = array_merge($my_checklist, $assigned_underfolder);
						$cnt = COUNT($my_checklist);

						if ($cnt < 6) {
							$whichfunction = 3;
							$perpage = 6 - $cnt;
							$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word, $select_val, $perpage);
							$my_checklist = array_merge($my_checklist, $assigned_ownedunderfolder);
						}
					}
				}
			}
		} else if ($whichfunction == 1) {
			$assigned_underprocess = $this->process_model->get_checklistunder_insideprocess($check_ids, $userid, $process_ID, $word, $select_val);
			$check_ids = $this->addToArray_checklist($assigned_underprocess, $check_ids);
			$my_checklist = $assigned;
			$cnt = COUNT($assigned);
			if ($cnt < 6) {
				$whichfunction = 1;
				$perpage = 6 - $cnt;
				$assigned_ownedunderprocess = $this->process_model->get_ownedchecklistunder_insideprocess($check_ids, $userid, $process_ID, $word, $select_val, $perpage);
				$check_ids = $this->addToArray_checklist($assigned_ownedunderprocess, $check_ids);
				$my_checklist = array_merge($my_checklist, $assigned_ownedunderprocess);
				$cnt = COUNT($my_checklist);
				if ($cnt < 6) {
					$whichfunction = 2;
					$perpage = 6 - $cnt;
					$assigned_underfolder = $this->process_model->get_checklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word, $select_val, $perpage);

					$check_ids = $this->addToArray_checklist($assigned_underfolder, $check_ids);
					$my_checklist = array_merge($my_checklist, $assigned_underfolder);
					$cnt = COUNT($my_checklist);

					if ($cnt < 6) {
						$whichfunction = 3;
						$perpage = 6 - $cnt;
						$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word, $select_val, $perpage);
						$my_checklist = array_merge($my_checklist, $assigned_ownedunderfolder);
					}
				}
			}
		} else if ($whichfunction == 2) {
			$assigned_ownedunderprocess = $this->process_model->get_ownedchecklistunder_insideprocess($check_ids, $userid, $process_ID, $word, $select_val);
			$check_ids = $this->addToArray_checklist($assigned_ownedunderprocess, $check_ids);
			$my_checklist = $assigned_ownedunderprocess;
			$cnt = COUNT($my_checklist);
			if ($cnt < 6) {
				$whichfunction = 2;
				$perpage = 6 - $cnt;
				$assigned_underfolder = $this->process_model->get_checklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word, $select_val, $perpage);
				$check_ids = $this->addToArray_checklist($assigned_underfolder, $check_ids);
				$my_checklist = array_merge($my_checklist, $assigned_underfolder);
				$cnt = COUNT($my_checklist);
				if ($cnt < 6) {
					$whichfunction = 3;
					$perpage = 6 - $cnt;
					$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word, $select_val, $perpage);
					$my_checklist = array_merge($my_checklist, $assigned_ownedunderfolder);
				}
			}
		} else if ($whichfunction == 3) {
			$assigned_underfolder = $this->process_model->get_checklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word, $select_val);
			$check_ids = $this->addToArray_checklist($assigned_underfolder, $check_ids);
			$my_checklist = $assigned_underfolder;
			$cnt = COUNT($my_checklist);
			if ($cnt < 6) {
				$whichfunction = 3;
				$perpage = 6 - $cnt;
				$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word, $select_val, $perpage);
				$my_checklist = array_merge($my_checklist, $assigned_ownedunderfolder);
			}
		} else {
			$assigned_ownedunderfolder = $this->process_model->get_ownedchecklistunder_insideprocessfolder($check_ids, $userid, $process_ID, $word, $select_val);
			$my_checklist = $assigned_ownedunderfolder;
		}
		//----------------------------------------------------------------------------------
		$loop = 0;


		if (count($my_checklist) > 0) {
			foreach ($my_checklist as $checklist) {
				$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
				$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
				$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);

				$my_checklist[$loop] = [
					"checklist" => $checklist,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		echo json_encode(['data' => $my_checklist, 'whichfunction' => $whichfunction]);
	}


	public function checklist_try()
	{
		$process_ID = $this->input->post("process_ID");
		$data = $this->general_model->fetch_specific_vals("checklist.checklist_ID as cid,checkStat.*", "checklist.process_ID=$process_ID AND checklist.checklist_ID=checkStat.checklist_ID", "tbl_processST_checklistStatus checkStat, tbl_processST_checklist checklist");
		echo json_encode($data);
	}
	//END OF JQUERY DISPLAY
	public function folderload_more()
	{
		$lastid = $_POST['folder_ID'];
		$data['foldernum'] = $this->process_model->count_folders($lastid);
		$data['folders'] = $this->process_model->get_nextallfolders($lastid);
		$data['processes'] = $this->general_model->fetch_specific_vals("process.*, app.*,folder.*", "process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID", "tbl_processST_process process, tbl_user user, tbl_processST_folder folder, tbl_applicant app, tbl_employee employee", "process.processTitle ASC");
		$data['checklists'] = $this->general_model->fetch_all("*", "tbl_processST_checklist", "checklistTitle ASC");
		$data['folderlimit'] = 3;
		$this->load->view('templates/process/folder_loadmore', $data);
	}
	public function processload_more()
	{
		$lastid = $_POST['process_ID'];
		$data['folders'] = $this->process_model->get_allfolders();
		$data['processnum'] = $this->process_model->count_processtemp($lastid);
		$data['processes'] = $this->process_model->get_nextallprocesses($lastid);
		$data['checklists'] = $this->general_model->fetch_all("*", "tbl_processST_checklist", "checklistTitle ASC");
		$this->load->view('templates/process/process_loadmore', $data);
	}
	public function getID($folder_ID)
	{
		$data = $folder_ID;
	}
	public function user_infos()
	{
		$data = [
			'uri_segment' => $this->uri->segment_array(),
			'title' => $this->title,
		];
		$fields = "a.apid,fname,lname,pic,emp_id,gender,acc_name,birthday";
		$where = "a.apid=b.apid and c.acc_id=b.acc_id and month(birthday)=" . date("m") . " and day(birthday)=" . date("d") . " and b.isActive='yes'";
		$result = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_applicant a,tbl_employee b,tbl_account c');
		foreach ($result as $row) {
			$job = $this->getCurrentPosition($row->emp_id);
			$data["celebrants"][$row->emp_id] = array(
				"emp_id" => $row->emp_id,
				"apid" => $row->apid,
				"fname" => $row->fname,
				"lname" => $row->lname,
				"pic" => $row->pic,
				"gender" => $row->gender,
				"acc_name" => $row->acc_name,
				"birthday" => $row->birthday,
				"job" => $job,
			);
		}
		return $data;
	}
	public function subfolders()
	{
		$id = $_POST['folder_ID'];
		$data['subfolders'] = $this->general_model->fetch_specific_vals("folder.*, app.*", "folder.referenceFolder=$id AND folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid", "tbl_processST_folder folder,tbl_user user,tbl_applicant app, tbl_employee employee", "folder.folderName ASC");
		// $this->process_model->get_subfolders($id);
		$data['eachprocess'] = $this->general_model->fetch_specific_vals("process.*,folder.*", "process.folder_ID=$id AND folder.folder_ID=$id", "tbl_processST_process process, tbl_processST_folder folder", "process.processTitle ASC");
		// $this->process_model->get_process($id);
		echo json_encode($data);
	}
	public function subfolder($folder_ID)
	{
		$data = $this->user_infos();
		$data['folderID'] = $folder_ID;
		if ($this->check_access_privilege_subfolder($folder_ID)) {
			$data['processes'] = $this->general_model->fetch_specific_vals("process.*,folder.*", "process.folder_ID=$folder_ID AND folder.folder_ID=$folder_ID", "tbl_processST_process process, tbl_processST_folder folder", "process.processTitle ASC");
			$data['subfolders'] = $this->general_model->fetch_specific_vals("folder.*, app.*", "folder.referenceFolder=$folder_ID AND folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid", "tbl_processST_folder folder,tbl_user user,tbl_applicant app, tbl_employee employee", "folder.folderName ASC");
			$data['account'] = $this->general_model->fetch_all("*", "tbl_account", "acc_name ASC");
			$data['members'] = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description", "emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes'", "tbl_applicant ap, tbl_employee emp,tbl_account acc", "ap.lname ASC");
			$data['folderinfo'] = $this->general_model->fetch_specific_val("*", "folder_ID=$folder_ID", "tbl_processST_folder", "folderName ASC");
			$data['checklists'] = $this->general_model->fetch_all("*", "tbl_processST_checklist", "checklistTitle ASC");
			$data['user'] = $this->session->userdata('uid');
			$this->load_template_view('templates/process/all_processes', $data);
		}
	}
	public function checklist($process_ID)
	{
		$data = $this->user_infos();
		$userid = $this->session->userdata('uid');
		if ($this->check_access_privilege_active_pending($process_ID)) {
			$data['user_info'] = $this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname", "user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid", "tbl_applicant applicant, tbl_employee employee, tbl_user user"); //GET FNAME AND LNAME
			// $this->process_model->get_userinfo($userid);
			$data['processID'] = $process_ID;
			$data['checklist'] = $this->general_model->fetch_specific_vals("checklist.*", "checklist.process_ID=$process_ID", "tbl_processST_checklist checklist");
			// $this->process_model->get_checklist($process_ID);
			$data['task'] = $this->general_model->fetch_specific_vals("task.*,process.*", "task.process_ID=$process_ID AND task.process_ID=process.process_ID", "tbl_processST_task task, tbl_processST_process process");
			// $this->process_model->get_task($process_ID);
			$data['processinfo'] = $this->general_model->fetch_specific_val("*", "process_ID=$process_ID", "tbl_processST_process", "processTitle ASC");
			// $this->process_model->get_singleprocessinfo($process_ID);
			$this->load_template_view('templates/process/checklist', $data);
		}
	}
	public function checklist_update($checklist_ID, $process_ID)
	{
		$data = $this->user_infos();
		$userid = $this->session->userdata('uid');

		if ($this->check_access_privilege_answer($process_ID, $checklist_ID, 'checklist_update')) {
			$data['user_info'] = $this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname", "user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid", "tbl_applicant applicant, tbl_employee employee, tbl_user user");
			$data['processID'] = $process_ID;
			$data['checklist'] = $this->general_model->fetch_specific_val("*", "checklist_ID=$checklist_ID", "tbl_processST_checklist", "checklistTitle ASC");
			// $this->process_model->get_singlechecklistinfo($checklist_ID);
			$data['task'] = $this->general_model->fetch_specific_vals("task.*,process.*", "task.process_ID=$process_ID AND task.process_ID=process.process_ID", "tbl_processST_task task, tbl_processST_process process");
			$data['processinfo'] = $this->general_model->fetch_specific_val("*", "process_ID=$process_ID", "tbl_processST_process", "processTitle ASC");
			$data['account'] = $this->general_model->fetch_all("*", "tbl_account", "acc_name ASC");
			$data['members'] = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description", "emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes'", "tbl_applicant ap, tbl_employee emp,tbl_account acc", "ap.lname ASC");
			$this->load_template_view('templates/process/checklist_update', $data);
		}
	}
	public function checklist_pending($process_ID)
	{
		$data = $this->user_infos();
		$userid = $this->session->userdata('uid');
		if ($this->check_access_privilege_active_pending($process_ID)) {
			$data['user_info'] = $this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname", "user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid", "tbl_applicant applicant, tbl_employee employee, tbl_user user");
			$data['processID'] = $process_ID;
			$data['checklist'] = $this->process_model->get_checklist($process_ID);
			// $this->general_model->fetch_specific_vals("checklist.*","checklist.process_ID=$process_ID","tbl_processST_checklist checklist");
			$data['checklist_status'] = $this->general_model->fetch_all("*", "tbl_processST_checklistStatus");
			// $this->process_model->get_checklistStat();
			$data['account'] = $this->general_model->fetch_all("*", "tbl_account", "acc_name ASC");
			$data['processinfo'] = $this->general_model->fetch_specific_val("*", "process_ID=$process_ID", "tbl_processST_process", "processTitle ASC");
			$data['members'] = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description", "emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes'", "tbl_applicant ap, tbl_employee emp,tbl_account acc", "ap.lname ASC");
			$data['employees'] = $this->general_model->fetch_specific_vals("a.fname,a.lname,uid", "a.apid=b.apid and b.isActive='yes' and b.emp_id=c.emp_id", "tbl_applicant a,tbl_employee b,tbl_user c", "lname ASC");
			$data['user'] = $userid;
			$this->load_template_view('templates/process/checklist_pending', $data);
		}
	}
	public function checklistload_more()
	{
		$data = $this->user_infos();
		$userid = $this->session->userdata('uid');
		$lastid = $_POST['checklist_ID'];
		$process_ID = $_POST['process_ID'];
		$data['processID'] = $process_ID;
		$data['user_info'] = $this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname", "user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid", "tbl_applicant applicant, tbl_employee employee, tbl_user user");
		$data['checklistnum'] = $this->process_model->count_checklist($lastid);
		$data['checklist'] = $this->process_model->get_nextallchecklist($process_ID, $lastid);
		$data['checklist_status'] = $this->general_model->fetch_all("*", "tbl_processST_checklistStatus");
		$data['processinfo'] = $this->general_model->fetch_specific_val("*", "process_ID=$process_ID", "tbl_processST_process", "processTitle ASC");
		$this->load->view('templates/process/checklist_loadmore', $data);
	}
	public function checklist_answerable($process_ID, $checklist_ID)
	{
		$data = $this->user_infos();
		$userid = $this->session->userdata('uid');

		if ($this->check_access_privilege_answer($process_ID, $checklist_ID, 'checklist_answerable')) {
			$data['user_info'] = $this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname", "user.uid=$userid AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid", "tbl_applicant applicant, tbl_employee employee, tbl_user user");
			$data['processID'] = $process_ID;
			$data['checklist'] = $this->general_model->fetch_specific_val("*", "checklist_ID=$checklist_ID", "tbl_processST_checklist", "checklistTitle ASC");
			$data['task'] = $this->general_model->fetch_specific_vals("task.*,checkStatus.*", "task.task_ID=checkStatus.task_ID AND checkStatus.checklist_ID=$checklist_ID", "tbl_processST_task task, tbl_processST_checklistStatus checkStatus");
			// $this->process_model->get_answerabletask($checklist_ID);
			$data['processinfo'] = $this->general_model->fetch_specific_val("*", "process_ID=$process_ID", "tbl_processST_process", "processTitle ASC");
			// $this->process_model->get_singleprocessinfo($process_ID);
			$data['user'] = $this->session->userdata('uid');
			$this->load_template_view('templates/process/checklist_answerable', $data);
		}
	}
	public function add_admin()
	{
		$eid = $this->input->post('emp_id');
		$this->db->query("DELETE FROM tbl_processST_admin_access");
		foreach ($eid as $value) {
			$employeeid = $this->process_model->get_userid_assignment($value)->row();
			$data['user_ID'] = $employeeid->uid;
			$this->general_model->insert_vals($data, "tbl_processST_admin_access");
		}
		echo json_encode($data);
	}
	public function add_assignment()
	{
		$assignedby = $this->session->userdata('uid');
		$ass_uid = $this->input->post('userid');
		$title = $this->input->post('title');
		$type = $this->input->post('type');
		$toassign_id = $this->input->post('folprotaskcheck_id');
		$process_ID = $this->input->post('process_ID');
		$checklist_ID = $this->input->post('checklist_ID');
		$pro_ID = $this->input->post('pro');
		$pro_type = $this->input->post('ptype');
		$c = [];
		$d = 0;
		$ts = [];
		$dd = 0;
		$as_c = [];
		$ddd = 0;
		$as_t = [];
		$dddd = 0;
		// Employee Info 
		$emp_assignedbyinfo = $this->general_model->fetch_specific_val("emp.emp_id,user.uid,ap.fname,ap.lname,ap.apid", "user.uid=$assignedby AND user.emp_id=emp.emp_id AND emp.apid=ap.apid", "tbl_employee emp, tbl_user user, tbl_applicant ap");

		$emp_assignedtoinfo = $this->general_model->fetch_specific_val("emp.emp_id,user.uid,ap.fname,ap.lname,ap.apid", "user.uid=$ass_uid AND user.emp_id=emp.emp_id AND emp.apid=ap.apid", "tbl_employee emp, tbl_user user, tbl_applicant ap");
		$name = $emp_assignedbyinfo->lname . "," . $emp_assignedbyinfo->fname;
		// Get sender name
		$assignedto = $ass_uid;

		if ($type == "process") {
			// Get checklists from process 
			$a = $this->general_model->fetch_specific_vals("checklist_ID", "process_ID=$toassign_id", "tbl_processST_checklist");
			foreach ($a as $v) {
				$c[$d] = $v->checklist_ID;
				$d++;
			}
			$str_checklist_id = implode(",", $c);

			//Get tasks
			$ct_status = $this->general_model->fetch_specific_vals("checklistStatus_ID", "checklist_ID IN($str_checklist_id)", "tbl_processST_checklistStatus");
			foreach ($ct_status as $vv) {
				$ts[$dd] = $vv->checklistStatus_ID;
				$dd++;
			}
			$str_checktasks_id = implode(",", $ts);

			//Get assigned checklist of selected employee
			$ass_checklist = $this->general_model->fetch_specific_vals("assign_ID", "type='checklist' AND folProCheTask_ID IN($str_checklist_id) AND assignedTo=$ass_uid", "tbl_processST_assignment");
			foreach ($ass_checklist as $ac) {
				$as_c[$ddd] = $ac->assign_ID;
				$ddd++;
			}
			$str_checkassign_id = implode(",", $as_c);

			//Get checklistStatus(assigned tasks) from checklists
			$ass_cstatus = $this->general_model->fetch_specific_vals("assign_ID", "type='task' AND folProCheTask_ID IN($str_checktasks_id) AND assignedTo=$ass_uid", "tbl_processST_assignment");
			foreach ($ass_cstatus as $at) {
				$as_t[$dddd] = $at->assign_ID;
				$dddd++;
			}
			$str_taskassign_id = implode(",", $as_t);
			// if($ass_cstatus!=null){
			// 	$insert_stat['checker'] = 2;
			// }else{

			$insert_stat['checker'] = 0;

			if ($ass_cstatus != null) {
				$del_ct = "DELETE FROM tbl_processST_assignment WHERE assign_ID IN($str_taskassign_id)";
				$this->general_model->custom_query_no_return($del_ct);
			} else if ($ass_checklist != null) {
				if ($pro_type == 1) {
					$del_c = "DELETE FROM tbl_processST_assignment WHERE assign_ID IN($str_checkassign_id)";
					$this->general_model->custom_query_no_return($del_c);
				}
			}

			$data['type'] = $type;
			$data['assignmentRule_ID'] = $this->input->post('assignmentRule_ID');
			$data['assignedBy'] = $assignedby;
			$data['assignedTo'] = $assignedto;
			$data['folProCheTask_ID'] = $this->input->post('folprotaskcheck_id');
			$this->general_model->insert_vals($data, "tbl_processST_assignment");

			// //NOTIFICATIONS
			$recipient[0] = [
				'userId' => $assignedto,
			];
			$notif_message = $name . " " . "has assigned you " . $title . " " . $type . "<br><small><b>ID</b>:" . str_pad($toassign_id, 8, '0', STR_PAD_LEFT) . " </small>";

			if ($type == "task") {
				$link = "process/checklist_answerable/$process_ID/$checklist_ID";
			} else if ($type == "checklist") {
				$link = "process/checklist_answerable/$pro_ID/$toassign_id";
			} else if ($type == "process") {
				$link = "process/checklist_pending/$toassign_id";
			} else {
				$link = "process";
			}
			$insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);
			// }
		} else if ($type == "checklist") {
			//Get tasks
			$p_check = $this->general_model->fetch_specific_val("process_ID", "checklist_ID=$toassign_id", "tbl_processST_checklist");
			$pp_id = $p_check->process_ID;

			$p_checklist = $this->general_model->fetch_specific_vals("assign_ID", "type='process' AND folProCheTask_ID=$pp_id AND assignedTo=$ass_uid", "tbl_processST_assignment");
			$insert_stat['sulod'] = $p_checklist;
			$ct_status = $this->general_model->fetch_specific_vals("checklistStatus_ID", "checklist_ID=$toassign_id", "tbl_processST_checklistStatus");
			foreach ($ct_status as $vv) {
				$ts[$dd] = $vv->checklistStatus_ID;
				$dd++;
			}
			$str_checktasks_id = implode(",", $ts);
			//Get checklistStatus(assigned tasks) from checklists
			$ass_cstatus = $this->general_model->fetch_specific_vals("assign_ID", "type='task' AND folProCheTask_ID IN($str_checktasks_id) AND assignedTo=$ass_uid", "tbl_processST_assignment");
			foreach ($ass_cstatus as $at) {
				$as_t[$dddd] = $at->assign_ID;
				$dddd++;
			}
			$str_taskassign_id = implode(",", $as_t);

			if ($p_checklist != null) {
				$insert_stat['checker'] = 3;
			} else {
				$insert_stat['checker'] = 0;
				if ($ass_cstatus != null) {
					$del_ct = "DELETE FROM tbl_processST_assignment WHERE assign_ID IN($str_taskassign_id)";
					$this->general_model->custom_query_no_return($del_ct);
				}
				$data['type'] = $type;
				$data['assignmentRule_ID'] = $this->input->post('assignmentRule_ID');
				$data['assignedBy'] = $assignedby;
				$data['assignedTo'] = $assignedto;
				$data['folProCheTask_ID'] = $this->input->post('folprotaskcheck_id');
				$this->general_model->insert_vals($data, "tbl_processST_assignment");

				// //NOTIFICATIONS
				$recipient[0] = [
					'userId' => $assignedto,
				];
				$notif_message = $name . " " . "has assigned you " . $title . " " . $type . "<br><small><b>ID</b>:" . str_pad($toassign_id, 8, '0', STR_PAD_LEFT) . " </small>";

				if ($type == "task") {
					$link = "process/checklist_answerable/$process_ID/$checklist_ID";
				} else if ($type == "checklist") {
					$link = "process/checklist_answerable/$pro_ID/$toassign_id";
				} else if ($type == "process") {
					$link = "process/checklist_pending/$toassign_id";
				} else {
					$link = "process";
				}
				$insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);
			}
		} else if ($type == "task") {
			$a = $this->general_model->fetch_specific_vals("checklist_ID", "checklistStatus_ID=$toassign_id", "tbl_processST_checklistStatus");
			$pb = $this->general_model->fetch_specific_val("t.process_ID", "cs.checklistStatus_ID=$toassign_id AND cs.task_ID=t.task_ID", "tbl_processST_task t, tbl_processST_checklistStatus cs");
			foreach ($a as $v) {
				$c[$d] = $v->checklist_ID;
				$d++;
			}
			$str_checklist_id = implode(",", $c);
			//Get assigned checklist of selected employee
			$ass_checklist = $this->general_model->fetch_specific_vals("assign_ID", "type='checklist' AND folProCheTask_ID IN($str_checklist_id) AND assignedTo=$ass_uid", "tbl_processST_assignment");
			$pp_id = $pb->process_ID;
			$insert_stat['rrtx'] = $ass_checklist;
			$p_task = $this->general_model->fetch_specific_vals("assign_ID", "type='process' AND folProCheTask_ID=$pp_id AND assignedTo=$ass_uid", "tbl_processST_assignment");

			if ($p_task != null) {
				$insert_stat['checker'] = 3;
			} else if ($ass_checklist != null) {
				$insert_stat['checker'] = 1;
			} else {
				$insert_stat['checker'] = 0;
				$insert_stat['name'] = $name;
				$insert_stat['checker'] = 0;
				$data['type'] = $type;
				$data['assignmentRule_ID'] = $this->input->post('assignmentRule_ID');
				$data['assignedBy'] = $assignedby;
				$data['assignedTo'] = $assignedto;
				$data['folProCheTask_ID'] = $this->input->post('folprotaskcheck_id');
				$this->general_model->insert_vals($data, "tbl_processST_assignment");

				// //NOTIFICATIONS
				$recipient[0] = [
					'userId' => $assignedto,
				];
				$notif_message = $name . " " . "has assigned you " . $title . " " . $type . "<br><small><b>ID</b>:" . str_pad($toassign_id, 8, '0', STR_PAD_LEFT) . " </small>";

				if ($type == "task") {
					$link = "process/checklist_answerable/$process_ID/$checklist_ID";
				} else if ($type == "checklist") {
					$link = "process/checklist_answerable/$pro_ID/$toassign_id";
				} else if ($type == "process") {
					$link = "process/checklist_pending/$toassign_id";
				} else {
					$link = "process";
				}
				$insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);
			}
		}
		echo json_encode($insert_stat);


		// if($ass_checklist!=null){
		// 	$insert_stat['checker'] = 1;
		// }else if($ass_cstatus!=null){
		// 	$insert_stat['checker'] = 2;
		// }else{
		// 	$insert_stat['checker'] = 0;

		// 	// Get sender name
		// $name=$emp_assignedtoinfo->lname . "," . $emp_assignedtoinfo->fname;
		// $assignedto = $ass_uid;

		// 	$data['type'] = $type;
		// 	$data['assignmentRule_ID'] = $this->input->post('assignmentRule_ID');
		// 	$data['assignedBy'] = $assignedby;
		// 	$data['assignedTo'] = $assignedto;
		// 	$data['folProCheTask_ID'] = $this->input->post('folprotaskcheck_id');
		// 	$this->general_model->insert_vals($data, "tbl_processST_assignment");

		// 		// //NOTIFICATIONS
		// 	$recipient[0] = [
		// 		'userId' => $assignedto,
		// 	];
		// 	$notif_message = $name . " " . "has assigned you " . $title . " " . $type . "<br><small><b>ID</b>:" . str_pad($toassign_id, 8, '0', STR_PAD_LEFT) . " </small>";

		// 	if ($type == "task") {
		// 		$link = "process/checklist_answerable/$process_ID/$checklist_ID";
		// 	} else if ($type == "checklist") {
		// 		$link = "process/checklist_answerable/$pro_ID/$toassign_id";
		// 	} else if ($type == "process") {
		// 		$link = "process/checklist_pending/$toassign_id";
		// 	} else {
		// 		$link = "process";
		// 	}
		// 	// $insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);
		// 	$insert_stat['notifid'] = $notif_message . " " . $link . " ". $recipient;

		// }


		// //GET SENDER NAME
		// $sender_emp_id = $this->process_model->get_empid($assignedby)->row();
		// $sender_empid = $sender_emp_id->emp_id;
		// $sender_ap_id = $this->process_model->get_appid($sender_empid)->row();
		// $sender_apid = $sender_ap_id->apid;
		// $applicantinfo = $this->general_model->fetch_specific_val("*", "apid=$sender_apid", "tbl_applicant");

		// $name = $applicantinfo->lname . "," . $applicantinfo->fname;

		// $applicant = $this->process_model->get_empid_assignment($apid)->row();
		// $emp_id = $applicant->emp_id;
		// $employee = $this->process_model->get_userid_assignment($emp_id)->row();
		// $assignedto = $employee->uid;

		// $data['type'] = $type;
		// $data['assignmentRule_ID'] = $this->input->post('assignmentRule_ID');
		// $data['assignedBy'] = $assignedby;
		// $data['assignedTo'] = $assignedto;
		// $data['folProCheTask_ID'] = $this->input->post('folprotaskcheck_id');
		// $assignment = $this->general_model->insert_vals($data, "tbl_processST_assignment");

		// //NOTIFICATIONS
		// $recipient[0] = [
		// 	'userId' => $assignedto,
		// ];
		// $notif_message = $name . " " . "has assigned you " . $title . " " . $type . "<br><small><b>ID</b>:" . str_pad($toassign_id, 8, '0', STR_PAD_LEFT) . " </small>";

		// if ($type == "task") {
		// 	$link = "process/checklist_answerable/$process_ID/$checklist_ID";
		// } else if ($type == "checklist") {
		// 	$link = "process/checklist_answerable/$pro_ID/$toassign_id";
		// } else if ($type == "process") {
		// 	$link = "process/checklist_pending/$toassign_id";
		// } else {
		// 	$link = "process";
		// }
		// $insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);


		// echo json_encode($insert_stat);
	}
	public function add_folder()
	{
		$userid = $this->session->userdata('uid');
		$data['folderName'] = $this->input->post('folderName');
		$data['referenceFolder'] = '1';
		$data['createdBy'] = $userid;
		$data['status_ID'] = '10';
		$this->general_model->insert_vals($data, "tbl_processST_folder");
	}
	public function add_subfolder()
	{
		$userid = $this->session->userdata('uid');
		$data['folderName'] = $this->input->post('folderName');
		$data['referenceFolder'] = $this->input->post('folder_ID');;
		$data['createdBy'] = $userid;
		$data['status_ID'] = '10';
		$this->general_model->insert_vals($data, "tbl_processST_folder");
	}
	public function add_processfolder()
	{
		$userid = $this->session->userdata('uid');
		$ptype = $this->input->post('processtype_ID');
		$data['processTitle'] = $this->input->post('processTitle');
		$data['processType_ID'] = $ptype;
		$data['processstatus_ID'] = '10';
		// $data['folder_ID'] = $this->input->post('folder_ID');
		$data['folder_ID'] = 0;
		$data['createdBy'] = $userid;
		$processid = $this->process_model->add_processfolderM($data);
		if ($ptype == 1) {
			$this->add_checklist($processid);
		}
		$this->add_task($processid);
		echo json_encode($processid);
	}

	public function add_duplicateprocesstemplate(){
		$userid = $this->session->userdata('uid');
		$ptype = $this->input->post('processtype_ID');
		$pp_id = $this->input->post('process_ID');
		$data['processTitle'] = $this->input->post('processDupTitle');
		$data['processType_ID'] = $ptype;
		$data['processstatus_ID'] = '10';
		$data['folder_ID'] = 0;
		$data['createdBy'] = $userid;
		$processid = $this->process_model->add_processfolderM($data);
		if ($ptype != 2) {
			$this->add_checklist($processid);
		}
		// Add task 
		$task = $this->general_model->fetch_specific_vals("task_ID, taskTitle, status_ID, sequence,isRemoved", "process_ID=$pp_id AND isRemoved=0", "tbl_processST_task");

		foreach ($task as $td) {
			$task_det['process_ID'] = $processid;
			$task_det['taskTitle'] = $td->taskTitle;
			$task_det['status_ID'] = 2;
			$task_det['sequence'] = $td->sequence;
			$task_det['isRemoved'] = 0;
			$task_det['createdBy'] = $userid;
			$task_det['updatedBy'] = $userid;
			$last_taskid= $this->general_model->insert_vals_last_inserted_id($task_det, 'tbl_processST_task');
			
			// GEt Subtask 
			$subtasks = $this->general_model->fetch_specific_vals("subTask_ID, component_ID, complabel,sublabel,required,placeholder,text_alignment,text_size", "task_ID=$td->task_ID AND isRemoved=0", "tbl_processST_subtask");	
			
			// if task has subtasks 
			if($subtasks!=null){
				foreach ($subtasks as $st) {
					$subt_det['task_ID'] = $last_taskid;
					$subt_det['component_ID'] = $st->component_ID;
					$subt_det['complabel'] = $st->complabel;
					$subt_det['sublabel'] = $st->sublabel;
					$subt_det['required'] = $st->required;
					$subt_det['placeholder'] = $st->placeholder;
					$subt_det['text_alignment'] = $st->text_alignment;
					$subt_det['text_size'] = $st->text_size;
					$subt_det['min'] = $st->min;
					$subt_det['max'] = $st->max;					
					$subt_det['sequence'] = $st->sequence;
					$subt_det['isRemoved'] = 0;					
					$last_subid= $this->general_model->insert_vals_last_inserted_id($subt_det, 'tbl_processST_subtask');

					// if process template is exam type 
					if($ptype == 5){		
					// GEt answer key
					$answerkey = $this->general_model->fetch_specific_vals("answerkey_ID, subtask_ID,process_ID,correctAnswer,answeredBy", "process_ID=$pp_id AND subtask_ID=$st->subTask_ID", "tbl_processST_irecruit_answerKey");				
					$totalscore_dup = $this->general_model->fetch_specific_vals("totalscore_ID,subtask_ID,totalScore,task_ID,setBy", "subtask_ID=$st->subTask_ID AND task_ID=$td->task_ID", "tbl_processST_irecruit_totalscore");					
					$passingscore_dup = $this->general_model->fetch_specific_vals("passingScore_ID,process_ID,score,updated_by", "process_ID=$pp_id", "tbl_processST_irecruit_passing_score");
					// if answer key is set 
						if($answerkey!=null){
							foreach ($answerkey as $ak) {
								$answer_k['subtask_ID'] = $last_subid;
								$answer_k['process_ID'] = $processid;
								$answer_k['correctAnswer'] = $ak->correctAnswer;
								$answer_k['answeredBy'] = $ak->answeredBy;
								$last_akid= $this->general_model->insert_vals_last_inserted_id($answer_k, 'tbl_processST_irecruit_answerKey');
							}
						}
					// if total score is set 
						if($totalscore_dup!=null){
							foreach ($totalscore_dup as $ts_dup) {
								$totalscore_det['subtask_ID'] = $last_subid;
								$totalscore_det['totalScore'] = $ts_dup->totalScore;
								$totalscore_det['task_ID'] = $last_taskid;
								$totalscore_det['setBy'] = $ak->answeredBy;
								$totalscore_det['process_ID'] = $processid;
								$last_ts_id= $this->general_model->insert_vals_last_inserted_id($totalscore_det, 'tbl_processST_irecruit_totalscore');
							}
						}
					// if passing score is set
						if($passingscore_dup !=null){
							foreach ($passingscore_dup as $ps_dup) {
								$pscore_det['process_ID'] = $processid;
								$pscore_det['score'] = $ps_dup->score;
								$pscore_det['updated_by'] = $ps_dup->updated_by;
								$last_ts_id= $this->general_model->insert_vals_last_inserted_id($pscore_det, 'tbl_processST_irecruit_passing_score');
							}
						}
					}
					// if subtasks has component subtasks	
					$component_subtasks = $this->general_model->fetch_specific_vals("componentSubtask_ID, subTask_ID,compcontent,sequence", "subTask_ID=$st->subTask_ID AND isRemoved=0", "tbl_processST_component_subtask");	

					if($component_subtasks!=null){
						foreach ($component_subtasks as $com_st) {
							$comp_det['subTask_ID']=$last_subid;
							$comp_det['compcontent']= $com_st->compcontent;
							$comp_det['sequence']= $com_st->sequence;
							$this->general_model->insert_vals($comp_det, 'tbl_processST_component_subtask');
						}
					}
				}
			}
		}
	}

	public function add_checklist($process_ID)
	{
		$userid = $this->session->userdata('uid');
		$data['process_ID'] = $process_ID;
		$data['status_ID'] = '10';
		$data['createdBy'] = $userid;
		$this->general_model->insert_vals($data, "tbl_processST_checklist");
	}
	public function add_anotherchecklist()
	{
		$userid = $this->session->userdata('uid');
		$processid = $_POST['process_ID'];
		$checkTitle = $_POST['checklistTitle'];
		$data['process_ID'] = $processid;
		$data['checklistTitle'] = $checkTitle;
		$data['status_ID'] = '2';
		$data['isArchived'] = '0';
		$data['createdBy'] = $userid;
		$data['updatedBy'] = $userid;
		$cid = $this->general_model->insert_vals_last_inserted_id($data, "tbl_processST_checklist");
		// $this->process_model->add_anotherchecklist($data);
		$alltask = $this->process_model->get_task($processid);
		$arTask = [];
		$taskNum = 0;
		foreach ($alltask as $alltaskrow) {
			$arTask[$taskNum] = [
				"checklist_ID" => $cid,
				"task_ID" => $alltaskrow->task_ID,
				"isRemoved" => $alltaskrow->isRemoved,
				"isCompleted" => 2,
			];
			$taskNum++;
		}
		$this->process_model->addbatch_checkstatus($arTask, 'tbl_processST_checklistStatus');
		echo json_encode($data);
	}
	public function add_subtask()
	{
		$data['component_ID'] = $_POST['component_ID'];
		$data['task_ID'] = $_POST['task_ID'];
		// $data['sequence'] = $_POST['sequence'];
		$t_id= $_POST['task_ID'];
		$seq_max = $this->fetch_allsubtask_count2($t_id);

		if($seq_max==null){
			$maxseq=1;
		}else{
			$maxseq=$seq_max;
			$maxseq++;
		}
		$data['sequence'] = $maxseq;
		$subtask['subTask_ID'] = $this->general_model->insert_vals_last_inserted_id($data, "tbl_processST_subtask");
		$this->general_model->insert_vals($subtask, "tbl_processST_component_subtask");
		echo json_encode($subtask);
	}
	public function add_subtaskcomponent()
	{
		$data['subTask_ID'] = $_POST['subTask_ID'];
		$subtaskid = $_POST['subTask_ID'];
		$this->general_model->insert_vals($data, "tbl_processST_component_subtask");
		$subtaskcomp = $this->general_model->fetch_specific_val("*", "subTask_ID=$subtaskid", "tbl_processST_subtask");
		echo json_encode($subtaskcomp);
	}
	public function add_task($process_ID)
	{
		$userid = $this->session->userdata('uid');
		$data['process_ID'] = $process_ID;
		$data['status_ID'] = '2';
		$data['createdBy'] = $userid;
		$data['sequence'] = '1';
		$this->process_model->add_task($data);
	}
	public function add_onetask()
	{
		$userid = $this->session->userdata('uid');
		$processid = $_POST['process_ID'];
		$taskseq = $_POST['task_seq'];
		$data['process_ID'] = $processid;
		$data['status_ID'] = '2';
		$data['createdBy'] = $userid;
		$data['sequence'] = $taskseq;
		$this->general_model->insert_vals($data, "tbl_processST_task");
		// $this->process_model->add_task($data);
		echo json_encode($data);
	}
	public function add_onetaskStatus()
	{
		$userid = $this->session->userdata('uid');
		$processid = $_POST['process_ID'];
		// $checkid = $_POST['checklist_ID'];
		$taskseq = $_POST['task_seq'];

		$data['process_ID'] = $processid;
		$data['status_ID'] = '2';
		$data['createdBy'] = $userid;
		$data['sequence'] = $taskseq;
		// $taskid = $this->process_model->add_anothertask($data);
		$t_id = $this->general_model->insert_vals_last_inserted_id($data, "tbl_processST_task");
		// $allchecklist = $this->process_model->get_checklist($processid);
		$allchecklist = $this->general_model->fetch_specific_vals("checklist_ID", "process_ID=$processid", "tbl_processST_checklist");
		// $arCheck = [];
		// $checkNum = 0;
		foreach ($allchecklist as $allcheckrow) {
			$cs['checklist_ID'] = $allcheckrow->checklist_ID;
			$cs['task_ID'] = $t_id;
			$cs['isCompleted'] = 2;
			$this->general_model->insert_vals($cs, 'tbl_processST_checklistStatus');
		}
		// update checklist status if new task is added 

		// $pp['status_ID'] = 2;
		// $where = "process_ID = $processid";
		// $this->general_model->update_vals($pp, $where, 'tbl_processST_checklist');

		// foreach ($allchecklist as $allcheckrow) {
		// 	$arCheck[$checkNum] = [
		// 		"checklist_ID" => $allcheckrow->checklist_ID,
		// 		"task_ID" => $taskid,
		// 		"isCompleted" => 2,
		// 	];
		// 	$checkNum++;
		// }
		// $this->general_model->batch_insert($arCheck, 'tbl_processST_checklistStatus');


		// $this->process_model->addbatch_checkstatus($arCheck, 'tbl_processST_checklistStatus');
		echo json_encode($data);
	}
	public function add_taskdetails()
	{
		$userid = $this->session->userdata('uid');
		$id = $_POST['task_ID'];
		$taskTitle = $_POST['taskTitle'];
		$data['taskTitle'] = $taskTitle;
		$data['updatedBy'] = $userid;
		$this->process_model->update_taskdetails($id, $data);
		echo json_encode($data);
	}
	public function add_subtaskdetails()
	{
		$id = $_POST['subTask_ID'];
		$data['complabel'] = $_POST['complabel'];
		$this->process_model->update_subtaskdetails($id, $data);
		echo json_encode($data);
	}
	public function add_answer()
	{
		// $empid = $this->user_details();
		// $answeredby = $empid->emp_id;
		$answeredby = $this->session->userdata('uid');
		$subtask = $_POST['subtask_ID'];
		$checklist = $_POST['checklistid'];

		$data['answer'] = $_POST['answer'];
		$data['answeredBy'] = $answeredby;

		$data['subtask_ID'] = $subtask;
		$data['checklist_ID'] = $checklist;
		$result = $this->process_model->check_answerexist($subtask, $checklist);
		if ($result == 0) {
			$this->general_model->insert_vals($data, "tbl_processST_answer");
		} else {
			$id = $this->process_model->get_answerid($subtask, $checklist)->row();
			$ansid = $id->answer_ID;
			$this->process_model->update_answerdetails($ansid, $data);
		}
		echo json_encode($data);
	}
	public function add_multiple_answer()
	{
		// $empid = $this->user_details();
		// $answeredby = $empid->emp_id;		
		$answeredby = $this->session->userdata('uid');
		$subtask = $_POST['subtask_ID'];
		$checklist = $_POST['checklistid'];
		$data['answer'] = $_POST['answer'];
		$data['answeredBy'] = $answeredby;
		$data['subtask_ID'] = $subtask;
		$data['checklist_ID'] = $checklist;
		$this->general_model->insert_vals($data, "tbl_processST_answer");
		echo json_encode($data);
	}
	public function add_optiondetails()
	{
		$id = $_POST['componentSubtask_ID'];
		$data['compcontent'] = $_POST['compcontent'];
		$this->process_model->update_compsubtaskdetails($id, $data);
		echo json_encode($data);
	}
	public function add_sublabeldetails()
	{
		$id = $_POST['subTask_ID'];
		$data['sublabel'] = $_POST['sublabel'];
		$this->process_model->update_subtaskdetails($id, $data);
		echo json_encode($data);
	}
	public function add_checklistdetails()
	{
		$userid = $this->session->userdata('uid');
		$id = $_POST['checklist_ID'];
		$pid = $_POST['process_ID'];
		$checkTitle = $_POST['checklistTitle'];
		$data['checklistTitle'] = $checkTitle;
		$data['status_ID'] = '2';
		$data['isArchived'] = '0';
		$data['updatedBy'] = $userid;
		$pdata['processstatus_ID'] = '2';
		$this->process_model->update_checklistdetails($id, $data);
		$this->process_model->update_process($pid, $pdata);
		$alltask = $this->process_model->get_task($pid);
		$arTask = [];
		$taskNum = 0;
		foreach ($alltask as $alltaskrow) {
			$arTask[$taskNum] = [
				"checklist_ID" => $id,
				"task_ID" => $alltaskrow->task_ID,
				"isCompleted" => 2,
			];
			$taskNum++;
		}
		$this->process_model->addbatch_checkstatus($arTask, 'tbl_processST_checklistStatus');
		echo json_encode($data);
	}

	public function add_checklistdetails_cl()
	{
		$userid = $this->session->userdata('uid');
		$pid = $_POST['process_ID'];
		$ass =  explode(",", $this->input->post('assignees'));
		$remarks = $_POST['checklist_remarks'];
		$pdata['processstatus_ID'] = '2';
		$this->process_model->update_process($pid, $pdata);
		$alltask = $this->process_model->get_task_cl($pid);
		$ac = [];
		$acnum = 0;

		for ($loop = 0; $loop < count($ass); $loop++) {
			$checkname = $this->assignees_names($ass[$loop]);
			$cn = array_shift($checkname);
			$checktitle = $cn->{'fname'} . " " . $cn->{'lname'} . " " . "CL";
			$d['process_ID'] = $pid;
			$d['status_ID'] = '2';
			$d['isArchived'] = '0';
			$d['createdBy'] = $userid;
			$d['remarks'] = $remarks;
			$d['isRemoved'] = '0';
			$d['checklistTitle'] = $checktitle;
			$checklist_id = $this->general_model->insert_vals_last_inserted_id($d, "tbl_processST_checklist");
			$a['type'] = 'checklist';
			$a['assignmentRule_ID'] = '9';
			$a['assignedBy'] = $userid;
			$a['assignedTo'] = $ass[$loop];
			$a['folProCheTask_ID'] = $checklist_id;
			$this->general_model->insert_vals($a, "tbl_processST_assignment");
			foreach ($alltask as $alltaskrow) {
				$t['checklist_ID'] = $checklist_id;
				$t['task_ID'] = $alltaskrow->task_ID;
				$t['isCompleted'] = 2;
				$this->general_model->insert_vals($t, 'tbl_processST_checklistStatus');
			}
			$ac[$acnum] = [
				"uid" => $ass[$loop],
				"checklist_ID" => $checklist_id
			];
			$acnum++;
		}
	}
	private function assignees_names($uid)
	{
		$q = "SELECT ap.fname,ap.lname FROM tbl_applicant ap, tbl_employee emp, tbl_user user WHERE user.uid=$uid AND user.emp_id=emp.emp_id AND emp.apid=ap.apid";
		return $this->general_model->custom_query($q);
	}
	public function delete_one_checklist()
	{
		$checkid = $_POST['checklist_ID'];
		$this->process_model->delete_folprochetask_assignment($checkid);
		$this->process_model->delete_checklist_answer($checkid);
		$this->process_model->delete_check_checkliststat($checkid);
		$this->process_model->delete_onechecklist($checkid);
	}
	public function delete_assignment()
	{
		$id = $_POST['assign_ID'];
		$this->process_model->delete_assignment($id);
	}
	public function delete_folder_process_checklist()
	{
		$id = $_POST['all_ID'];
		$type = $_POST['type'];
		$data['isRemoved'] = 1;
		$this->process_model->update_removefolprocheck($id, $type, $data);
	}
	public function delete_task_update()
	{
		$id = $_POST['task_ID'];
		$this->process_model->delete_folprochetask_assignment($id);
		$this->process_model->delete_checklistStat($id);
		$data = $this->general_model->fetch_specific_vals("subtask.subTask_ID", "subtask.task_ID=$id", "tbl_processST_subtask subtask");
		if ($data != null) {
			foreach ($data as $subtask) {
				$subtaskid = $subtask->subTask_ID;
				$this->process_model->delete_all_answer($subtaskid);
				$this->process_model->delete_compsubtask($subtaskid);
			}
			foreach ($data as $st_ID) {
				$stid = $st_ID->subTask_ID;
				$this->process_model->delete_subtask($stid);
			}
		}
		$this->process_model->delete_task($id);
	}

	//new delete_task 

	public function delete_task_new()
	{
		$task_id = $_POST['task_ID'];
		$task_details = $this->qry_task_details($task_id);
		if(count($task_details) > 0){
			$this->change_order_after_remove_task($task_details->process_ID, $task_details->sequence);
		}
		$type_taskdelete = $_POST['type_del'];
		$subtask_answers = $this->general_model->fetch_specific_vals("s.subTask_ID,a.answer_ID", "a.subtask_ID=s.subTask_ID AND s.task_ID=$task_id", "tbl_processST_subtask s, tbl_processST_answer a");
		$existing_subtask = $this->general_model->fetch_specific_vals("subtask.subTask_ID", "subtask.task_ID=$task_id", "tbl_processST_subtask subtask");
		$d = 0;
		$s = [];
		$update_val['isRemoved'] = 1;

		// if subtask of this task has answers 
		if ($subtask_answers != null) {
			$dis_subtask = $this->general_model->fetch_specific_vals("DISTINCT(s.subTask_ID)", "a.subtask_ID=s.subTask_ID AND s.task_ID=$task_id", "tbl_processST_subtask s, tbl_processST_answer a");
			foreach ($dis_subtask as $cs) {
				$s[$d] = $cs->subTask_ID;
				$d++;
			}
			$str_subtasks_id = implode(",", $s);
			$com_subtask = $this->general_model->fetch_specific_vals("s.subTask_ID,cs.componentSubtask_ID", "s.subTask_ID IN($str_subtasks_id) AND cs.subTask_ID IN($str_subtasks_id) AND s.subTask_ID=cs.subTask_ID", "tbl_processST_subtask s, tbl_processST_component_subtask cs");

			// if subtask has componentsubtask 

			// UPDATE component subtask 
			if ($com_subtask != null) {
				foreach ($com_subtask as $stk) {
					$where = "componentSubtask_ID = $stk->componentSubtask_ID";
					$this->general_model->update_vals($update_val, $where, 'tbl_processST_component_subtask');
				}
			}

			//UPDATE subtask
			foreach ($dis_subtask as $dst) {
				$where_st = "subTask_ID = $dst->subTask_ID";
				$this->general_model->update_vals($update_val, $where_st, 'tbl_processST_subtask');
			}

			// UPDATE TASK 
			$update_val_task['isRemoved'] = 1;
			$update_val_task['sequence'] = 0;
			$where_t = "task_ID = $task_id";
			$this->general_model->update_vals($update_val_task, $where_t, 'tbl_processST_task');


			if ($type_taskdelete == 2) {
				$h['hasanswers'] = "has answers and checklist status";
				$checkstatus_ids = $this->general_model->fetch_specific_vals("cs_t.checklistStatus_ID", "cs_t.task_ID=$task_id", "tbl_processST_checklistStatus cs_t");
				foreach ($checkstatus_ids as $check_stat) {
					$where_cst = "checklistStatus_ID = $check_stat->checklistStatus_ID";
					$this->general_model->update_vals($update_val, $where_cst, 'tbl_processST_checklistStatus');
				}
			} else {
				$h['hasanswers'] = "has answers";
			}
		} else {
			// if task is still raw- no answers 
			if ($existing_subtask != null) {
				foreach ($existing_subtask as $subtask) {
					$subtaskid = $subtask->subTask_ID;
					$this->process_model->delete_compsubtask($subtaskid);
				}
				foreach ($existing_subtask as $st_ID) {
					$stid = $st_ID->subTask_ID;
					$this->process_model->delete_subtask($stid);
				}
			}
			if ($type_taskdelete == 2) {
				$this->process_model->delete_checklistStat($task_id);
			}
			$this->process_model->delete_task($task_id);
			$h['hasanswers'] = "No answers";
		}
		echo json_encode($h);
	}
	//end new delete task

	//old delete_task
	public function delete_task()
	{
		$id = $_POST['task_ID'];
		$data = $this->general_model->fetch_specific_vals("subtask.subTask_ID", "subtask.task_ID=$id", "tbl_processST_subtask subtask");
		if ($data != null) {
			foreach ($data as $subtask) {
				$subtaskid = $subtask->subTask_ID;
				$this->process_model->delete_compsubtask($subtaskid);
			}
			foreach ($data as $st_ID) {
				$stid = $st_ID->subTask_ID;
				$this->process_model->delete_subtask($stid);
			}
		}
		$this->process_model->delete_task($id);
	}
	//end old delete task

	public function delete_process()
	{
		$id = $_POST['folder_ID'];
		$this->process_model->delete_checklist($id);
		$this->process_model->delete_process($id);
	}

	protected function qry_subtask_details($sub_task_id){
		$fields = "*";
		$where = "subTask_ID = $sub_task_id";
		return $this->general_model->fetch_specific_val($fields, $where, 'tbl_processST_subtask');
	}

	private function qry_task_details($task_id){
		$fields = "*";
		$where = "task_ID = $task_id";
		return $this->general_model->fetch_specific_val($fields, $where, 'tbl_processST_task');
	}

	public function delete_component()
	{
		$id = $_POST['subTask_ID'];
		$task_id = $_POST['task_id'];
		$update_val['isRemoved'] = 1;
		$subtask_details = $this->qry_subtask_details($id);
		// change the order after removing of subtask
		if(count($subtask_details) > 0){
			$this->change_order_after_remove($task_id, $subtask_details->sequence); 
		}
		$subtask_answers = $this->general_model->fetch_specific_vals("s.subTask_ID,a.answer_ID", "a.subtask_ID=s.subTask_ID AND s.subTask_ID=$id", "tbl_processST_subtask s, tbl_processST_answer a");

		if ($subtask_answers != null) {
			$comp_subtask_ids = $this->general_model->fetch_specific_vals("cs.componentSubtask_ID", "s.subTask_ID=$id AND cs.subTask_ID=$id AND s.subTask_ID=cs.subTask_ID", "tbl_processST_subtask s, tbl_processST_component_subtask cs");
			// UPDATE component subtask 
			if ($comp_subtask_ids != null) {
				
				foreach ($comp_subtask_ids as $stk) {
					$where = "componentSubtask_ID = $stk->componentSubtask_ID";
					$this->general_model->update_vals($update_val, $where, 'tbl_processST_component_subtask');
				}
			}
			//UPDATE subtask
			$update_val['sequence'] = 0;
			$where_st = "subTask_ID = $id ";
			$this->general_model->update_vals($update_val, $where_st, 'tbl_processST_subtask');
		} else {

			// COMPLETELY DELETE 
			$this->process_model->delete_compsubtask($id);
			$this->process_model->delete_subtask($id);
			$data = $this->general_model->fetch_specific_vals("subtask_ID,sequence", "subtask.task_ID=$task_id", "tbl_processST_subtask subtask", "sequence ASC");

			//$arr = array();
			$i = 1;
			foreach ($data as $row) {
				// $arr[] = array("subtask_ID" => $row->subtask_ID, "sequence" => $i);
				$data = $this->db->query("update tbl_processST_subtask set sequence=" . $i . " where subtask_ID=" . $row->subtask_ID);
				$i++;
			}
		}
		
	}
	public function delete_option()
	{
		$id = $_POST['componentSubtask_ID'];
		$componentsubtask_ids = $this->general_model->fetch_specific_vals("s.subTask_ID,a.answer_ID", "a.subtask_ID=s.subTask_ID AND s.subTask_ID=cs.subTask_ID AND cs.componentSubtask_ID=$id", "tbl_processST_subtask s, tbl_processST_answer a, tbl_processST_component_subtask cs");
		$update_val['isRemoved'] = 1;

		if ($componentsubtask_ids != null) {
			$where = "componentSubtask_ID = $id";
			$this->general_model->update_vals($update_val, $where, 'tbl_processST_component_subtask');
		} else {
			$this->process_model->delete_option($id);
		}
	}
	public function delete_file_answer()
	{
		$id = $_POST['answer_ID'];
		$this->process_model->delete_answer($id);
	}
	public function update_folder()
	{
		$id = $_POST['folder_ID'];
		$data['folderName'] = $this->input->post('folderName');
		$this->process_model->updatefolder_process($id, $data);
	}
	public function updatefolder_archive()
	{
		//set main folder to archive
		$id = $_POST['folder_ID'];
		$data['status_ID'] = $_POST['status_ID'];
		$this->process_model->updatefolder_process($id, $data);

		//set under process templates and checklists to archive
		// $templates=$this->general_model->fetch_specific_vals("process_ID","folder_ID=$id","tbl_processST_process");
		// var_dump($templates);

		// foreach ($templates as $alltemprow) {
		// 	$stat['processstatus_ID']='15';
		// 	$checkStat['status_ID']='15';
		// 	$processid=$alltemprow->process_ID;
		// 	$this->process_model->update_process($processid,$stat);
		// 	$this->process_model->update_checklistStat($processid,$checkStat);
		// }
		echo json_encode($data);
	}
	public function updateprocess_archive()
	{
		$id = $_POST['process_ID'];
		$data['processstatus_ID'] = $_POST['status_ID'];
		$this->process_model->update_process($id, $data);
		echo json_encode($data);
	}
	public function updatechecklist_archive()
	{
		$id = $_POST['checklist_ID'];
		$data['isArchived'] = $_POST['isArchived'];
		$this->process_model->update_checklist($id, $data);
		echo json_encode($data);
	}
	public function update_process()
	{
		$userid = $this->session->userdata('uid');
		$id = $_POST['process_ID'];
		$data['processTitle'] = $this->input->post('processTitle');
		$data['updatedBy'] = $userid;
		$this->process_model->update_process($id, $data);
	}
	public function update_checklist()
	{
		$userid = $this->session->userdata('uid');
		$id = $_POST['checklist_ID'];
		$data['checklistTitle'] = $this->input->post('checklistTitle');
		$data['updatedBy'] = $userid;
		$this->process_model->update_checklist($id, $data);
	}
	public function update_checklistStatus()
	{
		$id = $_POST['checklist_ID'];
		$userid = $this->session->userdata('uid');
		$data['status_ID'] = $_POST['status_ID'];
		$data['completedBy'] = $userid;
		$this->process_model->update_checklist($id, $data);
		echo json_encode($data);
	}
	public function update_checklistDuedate()
	{
		$id = $_POST['checklist_ID'];
		$data['dueDate'] = (!isset($_POST['dueDate']) || $_POST['dueDate'] === '') ? NULL : $_POST['dueDate'];
		$this->process_model->update_checklist($id, $data);
		echo json_encode($data);
	}
	public function update_taskDuedate()
	{
		$checklist_ID = $_POST['checklist_ID'];
		$task_ID = $_POST['task_ID'];;
		$data['taskdueDate'] = (!isset($_POST['taskdueDate']) || $_POST['taskdueDate'] === '') ? NULL : $_POST['taskdueDate'];
		$id = $this->process_model->get_checklistStatusID($checklist_ID, $task_ID)->row();
		$checkstatid = $id->checklistStatus_ID;
		$this->process_model->update_taskduedate($checkstatid, $data);
		echo json_encode($data);
	}
	public function update_taskStatus()
	{
		$id = $_POST['checklistStatus_ID'];
		$data['isCompleted'] = $_POST['isCompleted'];
		$this->process_model->update_taskStatus($id, $data);
		echo json_encode($data);
	}
	public function update_taskAllStatus()
	{
		$id = $_POST['checklist_ID'];
		$data['isCompleted'] = $_POST['stat'];
		$data2['status_ID'] = $_POST['stat'];
		// $_POST['isCompleted'];
		$this->process_model->update_taskAllStatus($id, $data);
		$this->process_model->update_checklist($id, $data2);
		// echo json_encode($data);
	}
	public function update_inputformdetails()
	{
		$id = $_POST['subTask_ID'];
		$data['complabel'] = $_POST['complabel'];
		$data['sublabel'] = $_POST['sublabel'];
		$data['placeholder'] = $_POST['placeholder'];
		$data['required'] = $_POST['required'];
		$data['min'] = $_POST['min'];
		$data['max'] = $_POST['max'];
		$this->process_model->update_inputformdetails($id, $data);
		echo json_encode($data);
	}
	public function update_headingtextdetails()
	{
		$id = $_POST['subTask_ID'];
		$data['complabel'] = $_POST['complabel'];
		$data['sublabel'] = $_POST['sublabel'];
		$data['text_alignment'] = $_POST['text_alignment'];
		$data['text_size'] = $_POST['text_size'];
		$this->process_model->update_inputformdetails($id, $data);
		echo json_encode($data);
	}
	public function update_optiondetails()
	{
		$id = $_POST['subTask_ID'];
		$data['complabel'] = $_POST['complabel'];
		$data['sublabel'] = $_POST['sublabel'];
		$data['required'] = $_POST['required'];
		$this->process_model->update_inputformdetails($id, $data);
		echo json_encode($data);
	}
	public function update_datetimedetails()
	{
		$id = $_POST['subTask_ID'];
		$data['complabel'] = $_POST['complabel'];
		$data['sublabel'] = $_POST['sublabel'];
		$data['placeholder'] = $_POST['placeholder'];
		$data['required'] = $_POST['required'];
		$this->process_model->update_inputformdetails($id, $data);
		echo json_encode($data);
	}
	public function update_assignmentpermission()
	{
		$id = $_POST['assid'];
		$data['assignmentRule_ID'] = $_POST['changepermission'];
		$this->process_model->update_assignmentpermission($id, $data);
		echo json_encode($data);
	}
	public function get_assigningDuedate()
	{
		$taskid = $_POST['task_ID'];
		$checklist_ID = $_POST['checklist_ID'];
		$data = $this->process_model->get_checklistStatusID($checklist_ID, $taskid)->row();
		echo json_encode($data);
	}
	public function get_dropdownfolinfo()
	{
		$userid = $this->session->userdata('uid');
		$assign = $this->input->post("assign");
		$stat = $this->input->post("status_ID");

		if ($assign == "1") {
			$res = $this->process_model->get_dropdowninfo($stat);
		} else if ($assign == "2") {
			$res = $this->process_model->get_owneddropdowninfo($stat, $userid);
		} else if ($assign == "3") {
			$res = $this->process_model->get_assigneddropdowninfo($stat);
		}
		echo json_encode($res);
	}
	public function get_dropdownproinfo()
	{
		$stat = $this->input->post("status_ID");
		$res = $this->process_model->get_dropdownproinfo($stat);
		echo json_encode($res);
	}
	public function get_checkliststat_record()
	{
		$checkid = $_POST['checklist_ID'];
		$data = $this->general_model->fetch_specific_vals("checkstat.*", "checkstat.checklist_ID=$checkid", "tbl_processST_checklistStatus checkstat");
		echo json_encode($data);
	}
	//FETCH
	public function fetch_membersadded()
	{
		$userid = $_POST['uid'];
		$fptc_id = $_POST['fptc_id'];
		$type = $_POST['type'];
		$userdata = $this->general_model->fetch_specific_vals("user.*", "user.uid=$userid", "tbl_user user");

		foreach ($userdata as $user_details) {
			$empid = $user_details->emp_id;
			$empresult = $this->general_model->fetch_specific_vals("emp.*", "emp.emp_id=$empid", "tbl_employee emp");
		}
		foreach ($empresult as $appdetails) {
			$apid = $appdetails->apid;
			$accountid = $appdetails->acc_id;
			$apresult = $this->general_model->fetch_specific_vals("app.*,acc.*,ass.assignmentRule_ID", "app.apid=$apid AND acc.acc_id=$accountid AND ass.assignedTo=$userid AND ass.type='$type' AND ass.folProCheTask_ID=$fptc_id", "tbl_applicant app, tbl_account acc, tbl_processST_assignment ass");
		}
		echo json_encode($apresult);
	}
	public function fetch_usersession()
	{
		$userid = $this->session->userdata('uid');
		echo json_encode($userid);
	}
	public function fetch_allemployees()
	{
		// $data=$this->general_model->fetch_specific_vals("app.apid,app.fname,app.mname,app.lname,emp.isActive,emp.emp_id","emp.apid=app.apid AND emp.isActive='yes'","tbl_applicant app,tbl_employee emp","app.lname ASC");
		$data = $this->general_model->custom_query("SELECT adminAccess_ID,uid,emp.emp_id,app.fname,app.lname FROM tbl_processST_admin_access access RIGHT JOIN tbl_user us ON us.uid=access.user_ID AND us.uid!=6 INNER JOIN tbl_employee emp ON us.emp_id=emp.emp_id INNER JOIN tbl_applicant app ON app.apid=emp.apid AND emp.isActive='yes' ORDER BY app.lname");
		echo json_encode($data);
	}
	public function fetch_allemployeeact()
	{
		$userid = $this->session->userdata('uid');
		$accid = $_POST['acc_id'];
		$data = $this->general_model->fetch_specific_vals("emp.*,user.uid", "emp.acc_id=$accid AND user.emp_id=emp.emp_id AND user.uid!=$userid", "tbl_employee emp, tbl_user user");
		echo json_encode($data);
	}
	public function fetch_oneemployee()
	{
		$empid = $_POST['emp_id'];
		$fptc_id = $_POST['fptcID'];
		$asstype = $_POST['assign_type'];
		$data = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description,user.uid", "emp.emp_id=$empid AND emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes' AND user.emp_id=emp.emp_id", "tbl_applicant ap, tbl_employee emp,tbl_account acc, tbl_user user", "ap.lname ASC");
		echo json_encode($data);
	}
	public function fetch_empdetails()
	{
		$apid = $_POST['apid'];
		$fptc_id = $_POST['fptcID'];
		$asstype = $_POST['assign_type'];
		$uid = $_POST['user_ID'];
		$result = $this->general_model->fetch_specific_vals("app.apid,app.fname,app.mname,app.lname,emp.isActive,user.uid", "app.apid=$apid AND app.apid=emp.apid AND emp.isActive='yes' AND user.emp_id=emp.emp_id", "tbl_applicant app, tbl_employee emp, tbl_user user");

		// $assignmentresult=$this->general_model->fetch_specific_vals("assignmentRule_ID,assignedTo,folProCheTask_ID","folProCheTask_ID=$fptc_id AND type='$asstype'","tbl_processST_assignment");
		// $allresult = [];
		// $allresult = array_merge($result, $assignmentresult);
		echo json_encode($result);
	}
	public function fetch_update()
	{
		$id = $_POST['folder_ID'];
		$res = $this->process_model->fetch_update($id);
		echo json_encode($res);
	}
	public function fetchprocess_update()
	{
		$id = $_POST['process_ID'];
		$res = $this->process_model->fetchprocess_update($id);
		echo json_encode($res);
	}
	public function fetchtask_update()
	{
		$id = $_POST['task_ID'];
		$res = $this->process_model->fetchtask_update($id);
		echo json_encode($res);
	}
	public function fetchchecklist_update()
	{
		$id = $_POST['checklist_ID'];
		$res = $this->process_model->fetchchecklist_update($id);
		echo json_encode($res);
	}
	public function fetch_duedate()
	{
		$task_ID = $_POST['task_ID'];
		$checklist_ID = $_POST['check_ID'];
		$id = $this->process_model->get_checklistStatusID($checklist_ID, $task_ID)->row();
		$checkstatid = $id->checklistStatus_ID;
		$info = $this->process_model->get_singleCheckstatinfo($checkstatid);
		echo json_encode($info);
	}
	public function fetch_subtask()
	{
		$subtask_ID = $_POST['subTask_ID'];
		$data = $this->general_model->fetch_specific_val("*", "subTask_ID=$subtask_ID", "tbl_processST_subtask");
		echo json_encode($data);
	}
	public function fetch_componentsubtask()
	{
		$subtask_ID = $_POST['subTask_ID'];
		$data = $this->general_model->fetch_specific_vals("*", "subTask_ID=$subtask_ID AND isRemoved=0", "tbl_processST_component_subtask");
		echo json_encode($data);
	}
	public function fetch_allsubtask1()
	{
		$task_ID =  $_POST['task_ID'];
		$checkid = $_POST['checklist_ID'];
		$data = $this->general_model->fetch_specific_vals("*", "task_ID=$task_ID AND isRemoved=0", "tbl_processST_subtask", "sequence ASC");
		$arr = array();
		foreach ($data as $row) {
			$answers = $this->getAnswer($row->subTask_ID, $checkid);
			$hasSubcomponent = array(6, 7, 8);

			if (in_array($row->component_ID, $hasSubcomponent)) {
				$subComponent = $this->getComponent($row->subTask_ID);
			} else {
				$subComponent = array();
			}
			$arr[$row->task_ID][] = array(
				"subTask_ID" => $row->subTask_ID,
				"task_ID" => $row->task_ID,
				"component_ID" => trim($row->component_ID),
				"compLabel" => $row->complabel,
				"sublabel" => $row->sublabel,
				"required" => $row->required,
				"text_alignment" => $row->text_alignment,
				"placeholder" => $row->placeholder,
				"text_size" => $row->text_size,
				"min" => $row->min,
				"max" => $row->max,
				"sequence" => $row->sequence,
				"answer_details" => $answers,
				"subComponent" => $subComponent,
			);
		}
		echo json_encode($arr);
		// echo json_encode(['checklist_details' => $checklist_details, 'total' => $count, 'process_type' => $ttype]);
	}
	public function getComponent($subtask_ID)
	{
		$q = "SELECT cs.subtask_ID,a.answer_ID,cs.componentSubtask_ID,cs.compcontent,a.checklist_ID FROM tbl_processST_component_subtask cs LEFT JOIN tbl_processST_answer a ON a.subtask_ID=cs.subTask_ID AND a.answer=cs.compcontent WHERE cs.subtask_ID=$subtask_ID AND cs.isRemoved=0";
		return $this->general_model->custom_query($q);
		// return $this->general_model->fetch_specific_vals("componentSubtask_ID,compcontent", "subtask_ID=$subtask_ID AND isRemoved=0", "tbl_processST_component_subtask");
	}
	public function getAnswer($subtask_ID, $checklist_ID)
	{
		return $this->general_model->fetch_specific_vals("answer_ID,answer,checklist_ID,subtask_ID", "subtask_ID=$subtask_ID AND checklist_ID=$checklist_ID", "tbl_processST_answer");
	}
	public function fetch_allsubtask()
	{
		$task_ID = $_POST['task_ID'];
		$data = $this->general_model->fetch_specific_vals("*", "task_ID=$task_ID AND isRemoved=0", "tbl_processST_subtask", "sequence ASC");
		echo json_encode($data);
	}
	public function fetch_allsubtask_count()
	{
		$task_ID = $_POST['task_ID'];
		// $data = $this->general_model->fetch_specific_vals("max(sequence) as max", "task_ID=$task_ID", "tbl_processST_subtask", "sequence ASC");
		$q = "SELECT max(sequence) as max FROM tbl_processST_subtask WHERE task_ID=$task_ID AND isRemoved=0";
		$data = $this->general_model->custom_query($q);
		echo json_encode($data);
	}
	public function fetch_allsubtask_count2($task_ID)
	{
		$q = "SELECT max(sequence) as max FROM tbl_processST_subtask WHERE task_ID=$task_ID AND isRemoved=0";
		$data = $this->general_model->custom_query($q);
		return $data[0]->max;
	}

	public function fetch_allanswers()
	{
		$task_ID = $_POST['task_ID'];
		$checklist_ID = $_POST['checklist_ID'];
		$data = $this->process_model->get_answers($task_ID, $checklist_ID);
		echo json_encode($data);
	}

	public function fetch_formdetails()
	{
		$subTask_ID = $_POST['subTask_ID'];
		$data = $this->general_model->fetch_specific_val("*", "subTask_ID=$subTask_ID", "tbl_processST_subtask");
		echo json_encode($data);
	}
	public function fetch_ansid()
	{
		$subtask_ID = $_POST['subtask_ID'];
		$checklist_ID = $_POST['checklistid'];
		$answer = $_POST['answer'];
		$ans = $this->general_model->fetch_specific_val("answer_ID", "subtask_ID=$subtask_ID AND checklist_ID=$checklist_ID AND answer='$answer'", "tbl_processST_answer");
		// $ansid = $this->process_model->get_ansid($subtask_ID, $checklist_ID, $answer)->row();
		// $answerid = $ansid->answer_ID;
		$del_ct = "DELETE FROM tbl_processST_answer WHERE answer_ID=$ans->answer_ID";
		$data = $this->general_model->custom_query_no_return($del_ct);
		// $data = $this->process_model->delete_answer($answerid);
		echo json_encode($data);
	}
	public function fetchone_assignment()
	{
		$userid = $this->session->userdata('uid');
		$folderid = $_POST['folderid'];
		$type = $_POST['typename'];
		$data = $this->general_model->fetch_specific_val("*", "folProCheTask_ID=$folderid AND assignedTo=$userid AND type='$type'", "tbl_processST_assignment");
		echo json_encode($data);
	}
	public function fetch_assignment()
	{
		$user_id = $this->session->userdata('uid');
		$type = $_POST['type'];
		$data = $this->general_model->fetch_specific_vals("assign_ID,type,assignmentRule_ID,folProCheTask_ID,assignedBy", "assignedTo=$user_id AND type='$type'", "tbl_processST_assignment");
		echo json_encode($data);
	}
	public function fetch_assignment_subfolder()
	{
		$alldata = [];
		$user_id = $this->session->userdata('uid');
		$process = "process";
		$folder = "folder";
		$data_process = $this->general_model->fetch_specific_vals("assign_ID,type,assignmentRule_ID,folProCheTask_ID,assignedBy", "assignedTo=$user_id AND type='$process'", "tbl_processST_assignment");
		$data_folder = $this->general_model->fetch_specific_vals("assign_ID,type,assignmentRule_ID,folProCheTask_ID,assignedBy", "assignedTo=$user_id AND type='$folder'", "tbl_processST_assignment");
		$alldata = array_merge($data_process, $data_folder);
		echo json_encode($alldata);
	}
	public function fetch_assignment_folder_subfolders()
	{
		$user_id = $this->session->userdata('uid');
		$folderid = $_POST['folder_ID'];
		$data_folder = $this->general_model->fetch_specific_vals("assign_ID,type,assignmentRule_ID,folProCheTask_ID,assignedBy", "assignedTo=$user_id AND type='folder' AND folProCheTask_ID=$folderid", "tbl_processST_assignment");
		echo (count($data_folder) > 0) ? json_encode($data_folder) : 0;
	}
	public function fetch_assignment_checklist()
	{
		$user_id = $this->session->userdata('uid');
		$type = $_POST['type'];
		$task = "task";
		$alldata = [];
		$data_task = $this->general_model->fetch_specific_vals("assign_ID,type,assignmentRule_ID,folProCheTask_ID,assignedBy", "assignedTo=$user_id AND type='$task'", "tbl_processST_assignment");
		$data = $this->general_model->fetch_specific_vals("assign_ID,type,assignmentRule_ID,folProCheTask_ID,assignedBy", "assignedTo=$user_id AND type='$type'", "tbl_processST_assignment");
		$alldata = array_merge($data, $data_task);
		echo json_encode($alldata);
	}
	public function fetch_assignment_task()
	{
		$userid = $this->session->userdata('uid');
		$checklist_ID = $_POST['checklist_ID'];
		$process_ID = $_POST['process_ID'];
		$alldata = [];
		$processdata = $this->general_model->fetch_specific_vals("ass.assign_ID,ass.type,ass.assignmentRule_ID as processrule,ass.folProCheTask_ID,ass.assignedBy,process.processTitle", "ass.assignedTo=$userid AND ass.type='process' AND ass.folProCheTask_ID=$process_ID AND process.process_ID=$process_ID", "tbl_processST_assignment ass, tbl_processST_process process");
		$folderdata = $this->general_model->fetch_specific_vals("ass.assign_ID,ass.type,ass.assignmentRule_ID as folderrule,ass.folProCheTask_ID,ass.assignedBy,folder.folderName", "ass.assignedTo=$userid AND ass.type='folder' AND process.folder_ID=folder.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND process.process_ID=$process_ID", "tbl_processST_assignment ass, tbl_processST_process process, tbl_processST_folder folder");
		$checklistdata = $this->general_model->fetch_specific_vals("ass.assign_ID,ass.type,ass.assignmentRule_ID as checkrule,ass.folProCheTask_ID,ass.assignedBy,checklist.checklistTitle", "ass.assignedTo=$userid AND ass.type='checklist' AND ass.folProCheTask_ID=$checklist_ID", "tbl_processST_assignment ass, tbl_processST_checklist checklist");
		$taskdata = $this->general_model->fetch_specific_vals("ass.assign_ID,ass.type,ass.assignmentRule_ID as taskrule,ass.folProCheTask_ID,ass.assignedBy,checklist.checklistTitle,checkstat.checklistStatus_ID", "ass.assignedTo=$userid AND ass.type='task' AND ass.folProCheTask_ID=checkstat.checklistStatus_ID AND checkstat.checklist_ID=$checklist_ID AND checkstat.checklist_ID=checklist.checklist_ID", "tbl_processST_assignment ass, tbl_processST_checklist checklist, tbl_processST_checklistStatus checkstat");
		$alldata = array_merge($processdata, $checklistdata, $folderdata, $taskdata);
		echo json_encode($alldata);
	}
	public function fetch_asstask()
	{
		$userid = $this->session->userdata('uid');
		$checklist_ID = $_POST['checklist_ID'];
		$taskdata = $this->general_model->fetch_specific_vals("ass.assign_ID,ass.type,ass.assignmentRule_ID as taskrule,ass.folProCheTask_ID,ass.assignedBy,checklist.checklistTitle,checkstat.checklistStatus_ID,checkstat.task_ID as taskid", "ass.assignedTo=$userid AND ass.type='task' AND ass.folProCheTask_ID=checkstat.checklistStatus_ID AND checkstat.checklist_ID=$checklist_ID AND checkstat.checklist_ID=checklist.checklist_ID", "tbl_processST_assignment ass, tbl_processST_checklist checklist, tbl_processST_checklistStatus checkstat");
		echo json_encode($taskdata);
	}
	public function fetch_assignment_checklist_pending()
	{
		$user_id = $this->session->userdata('uid');
		$processid = $_POST['process_ID'];
		$checkinprocess = $_POST['checkinprocess'];
		$checklist = "checklist";
		$process = "process";
		$folder = "folder";
		$allresult = [];

		if ($checkinprocess == "1") {
			$getchecklist_assignment = $this->general_model->fetch_specific_vals("ass.assign_ID,ass.type,ass.assignmentRule_ID,ass.folProCheTask_ID,ass.assignedBy", "ass.assignedTo=$user_id AND ass.type='$checklist' AND checklist.process_ID=$processid AND checklist.checklist_ID=ass.folProCheTask_ID", "tbl_processST_assignment ass, tbl_processST_checklist checklist");
		} else {
			$getchecklist_assignment = $this->general_model->fetch_specific_vals("assign_ID,type,assignmentRule_ID,folProCheTask_ID,assignedBy", "assignedTo=$user_id AND type='$checklist'", "tbl_processST_assignment");
		}

		$getprocess_checklistassignment = $this->general_model->fetch_specific_vals("ass.assign_ID,ass.type,ass.assignmentRule_ID,ass.folProCheTask_ID,ass.assignedBy,process.process_ID, checklist.checklist_ID, checklist.checklistTitle", "ass.assignedTo=$user_id AND type='$process' AND ass.folProCheTask_ID=$processid AND process.process_ID=$processid AND checklist.process_ID=$processid AND checklist.process_ID=process.process_ID", "tbl_processST_assignment ass, tbl_processST_process process, tbl_processST_checklist checklist");
		$getfolder_checklistassignment = $this->general_model->fetch_specific_vals("ass.assign_ID,ass.type,ass.assignmentRule_ID,ass.folProCheTask_ID,ass.assignedBy,process.process_ID, checklist.checklist_ID, checklist.checklistTitle, folder.folderName", "ass.assignedTo=$user_id AND type='$folder' AND process.process_ID=$processid AND checklist.process_ID=$processid AND process.folder_ID=folder.folder_ID AND ass.folProCheTask_ID=folder.folder_ID AND checklist.process_ID=process.process_ID", "tbl_processST_assignment ass, tbl_processST_process process, tbl_processST_checklist checklist, tbl_processST_folder folder");
		$allresult = array_merge($getchecklist_assignment, $getprocess_checklistassignment, $getfolder_checklistassignment);
		echo json_encode($allresult);
	}
	public function fetch_filterassignment()
	{
		$fptc_id = $_POST['fptcID'];
		$type = $_POST['assign_type'];
		$data = $this->general_model->fetch_specific_vals("assign_ID,type,assignmentRule_ID,folProCheTask_ID,assignedTo", "type='$type' AND folProCheTask_ID=$fptc_id", "tbl_processST_assignment");
		echo json_encode($data);
	}
	public function fetch_access()
	{
		$user_id = $this->session->userdata('uid');
		$data = $this->general_model->fetch_specific_vals("*", "user_ID=$user_id", "tbl_processST_admin_access");
		echo json_encode($data);
	}
	// NEW ACCESS CHECKER

	public function fetch_access_new2()
	{
		$userid = $_SESSION["uid"];
		$c_ID = $_POST['checklist_ID'];
		$p_ID = $_POST['process_ID'];
		$admin = "SELECT * FROM tbl_processST_admin_access WHERE user_ID=$userid";
		$a_admin =  $this->general_model->custom_query($admin);
		$c_comp = "SELECT checklist_id FROM tbl_processST_checklist WHERE process_ID=$p_ID AND status_ID=3";
		$ccomp = $this->general_model->custom_query($c_comp);

		$assign_p = $this->general_model->fetch_specific_val("assign_ID", "folProCheTask_ID=$p_ID AND type='process' AND assignedTo=$userid", "tbl_processST_assignment");
		$owned_pt = $this->general_model->fetch_specific_val("process_ID", "process_ID=$p_ID AND createdBy=$userid", "tbl_processST_process");
		$process_info = $this->general_model->fetch_specific_val("processType_ID", "process_ID=$p_ID", "tbl_processST_process");
		$access['process_type']=$process_info->processType_ID;
		$access['coached_person_details'] = $this->general_model->fetch_specific_val("ap.fname,ap.lname,ap.mname,estat.status,acc.acc_name", "user.uid=ass.assignedTo AND user.emp_id=emp.emp_id AND emp.apid=ap.apid AND emppro.emp_id=emp.emp_id AND emppro.isActive=1 AND emppro.posempstat_id=posempS.posempstat_id AND posempS.empstat_id=estat.empstat_id AND emp.acc_id=acc.acc_id AND ass.folProCheTask_ID=$c_ID AND type='checklist'", "tbl_user user,tbl_employee emp,tbl_applicant ap, tbl_emp_promote emppro, tbl_pos_emp_stat posempS, tbl_emp_stat estat, tbl_account acc, tbl_processST_assignment ass");


		if($assign_p!=null || $owned_pt!=null){
			$access['checklist_pending_access'] = "1";
		}else{
			$access['checklist_pending_access'] = "0";
		}

		if ($ccomp != null) {
			$completed = "1";
		} else {
			$completed = "0";
		}

		if ($a_admin != null) {
			$access['admin'] = "1";
			$access['checklist_owned'] = "0";
			$access['process_owned'] = "0";
			$access['task_owned'] = "0";
			$access['checklist_edit_owned'] = "1";
			$access['process_edit_owned'] = "1";
			$access['task_edit_owned'] = "1";
			$access['task_access'] = "1";
			$access['checklist_comp'] = $completed;
		} else {
			$access['admin'] = "0";
			$access['checklist_comp'] = $completed;
			// OWNED
			$t_o = "SELECT task_ID FROM tbl_processST_task WHERE createdBy=$userid AND process_ID=$p_ID";
			$t_owned = $this->general_model->custom_query($t_o);

			$p_o = "SELECT process_ID FROM tbl_processST_process WHERE createdBy = $userid AND isRemoved=0 AND processstatus_ID!=15 AND process_ID=$p_ID";
			$p_owned = $this->general_model->custom_query($p_o);

			$c_o = "SELECT checklist_ID FROM tbl_processST_checklist WHERE createdBy=$userid AND isRemoved=0 AND status_ID!=15 AND checklist_ID=$c_ID";
			$c_owned = $this->general_model->custom_query($c_o);

			// PERMISSION QUERY

			$t_p = "SELECT ch.checklistStatus_ID, ass.assignmentRule_ID FROM tbl_processST_checklistStatus ch, tbl_processST_assignment ass WHERE ch.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedTo=$userid AND ch.checklist_ID=$c_ID";
			$t_permission = $this->general_model->custom_query($t_p);

			$p_p = "SELECT process.process_ID, ass.assignmentRule_ID FROM tbl_processST_process as process, tbl_processST_assignment ass WHERE ass.type='process' AND ass.assignedTo=$userid AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=2) AND ass.folProCheTask_ID=$p_ID AND ass.folProCheTask_ID=process.process_ID AND process.process_ID=$p_ID AND process.isRemoved=0 AND process.processstatus_ID!=15";
			$p_permission = $this->general_model->custom_query($p_p);


			$c_p = "SELECT checklist.checklist_ID, ass.assignmentRule_ID FROM tbl_processST_checklist as checklist, tbl_processST_assignment ass WHERE ass.type='checklist' AND ass.assignedTo=$userid AND ass.assignmentRule_ID=6 AND ass.folProCheTask_ID=$c_ID AND ass.folProCheTask_ID=checklist.checklist_ID AND checklist.checklist_ID=$c_ID AND checklist.isRemoved=0 AND checklist.status_ID!=15";
			$c_permission = $this->general_model->custom_query($c_p);

			if ($p_owned != null) {
				$access['process_owned'] = "1";
				$access['process_edit_access'] = "1";
				if ($c_owned != null) {
					$access['checklist_owned'] = "1";
					$access['checklist_edit_access'] = "1";
					$access['task_access'] = "1";
				} else {
					$access['checklist_owned'] = "0";
					$access['checklist_edit_access'] = "0";
					$access['task_access'] = "0";
				}
			} else if ($c_owned != null) {
				$access['process_owned'] = "0";
				$access['checklist_owned'] = "1";
				$access['checklist_edit_access'] = "1";
				$access['task_edit_access'] = "1";
				if ($p_permission != null) {
					$access['process_edit_access'] = "1";
				} else {
					$access['process_edit_access'] = "0";
				}
			} else if ($t_owned != null) {
				$access['process_owned'] = "0";
				$access['checklist_owned'] = "0";
				$access['task_owned'] = "1";
				if ($p_permission != null) {
					$access['process_edit_access'] = "1";
				} else {
					$access['process_edit_access'] = "0";
					if ($c_permission != null) {
						$access['checklist_edit_access'] = "1";
					} else {
						$access['checklist_edit_access'] = "0";
					}
				}
			} else if ($p_permission != null) {
				$access['process_edit_access'] = "1";
				$access['process_permission'] = "1";
				$access['process_details'] = $p_permission;
			} else if ($c_permission != null) {
				$access['checklist_edit_access'] = "1";
				$access['checklist_details'] = $c_permission;
				$access['checklist_permission'] = "1";
			} else if ($t_permission != null) {
				$access['task_edit_access'] = "1";
				$access['task_details'] = $t_permission;
				$access['task_permission'] = "1";
			}
			echo json_encode($access);
		}
	}

	public function fetch_access_new()
	{
		$userid = $_SESSION["uid"];
		$c_ID = $_POST['checklist_ID'];
		$p_ID = $_POST['process_ID'];
		$admin = "SELECT * FROM tbl_processST_admin_access WHERE user_ID=$userid";
		$a_admin =  $this->general_model->custom_query($admin);

		// if user ID is admin
		if ($a_admin != null) {
			$access['admin'] = "1";
			$access['checklist_owned'] = "0";
			$access['process_owned'] = "0";
			$access['checklist_editable'] = "0";
			$access['process_editable'] = "0";
		} else {
			// if userid is not admin
			$access['admin'] = "0";

			// CHECK if owned

			// check if checklist owned
			$c_o = "SELECT checklist_ID FROM `tbl_processST_checklist` WHERE createdBy=$userid AND isRemoved=0 AND status_ID!=15 AND checklist_ID=$c_ID";
			$c_owned =  $this->general_model->custom_query($c_o);

			// check if process template owned
			$p_o = "SELECT process_ID FROM `tbl_processST_process` WHERE createdBy=$userid AND isRemoved=0 AND processstatus_ID!=15 AND process_ID=$p_ID";
			$p_owned =  $this->general_model->custom_query($p_o);

			// condition if checklist owned
			if ($c_owned != null) {
				$access['checklist_owned'] = "1";

				// if process template also owned
				if ($p_owned != null) {
					$access['process_owned'] = "1";
				} else {
					$access['process_owned'] = "0";
				}

				$access['checklist_editable'] = "0";
				$access['process_editable'] = "0";
			} else if ($p_owned != null) {
				$access['checklist_owned'] = "0";
				$access['process_owned'] = "1";
				$access['checklist_editable'] = "0";
				$access['process_editable'] = "0";
			} else {
				// CHECK With Permission
				$access['checklist_owned'] = "0";
				$access['process_owned'] = "0";

				// Checklist permission
				$c_p = "SELECT checklist.checklist_ID FROM tbl_processST_checklist as checklist, tbl_processST_assignment ass WHERE ass.assignedTo=$userid AND ass.assignmentRule_ID=6 AND ass.folProCheTask_ID=$c_ID AND ass.folProCheTask_ID=checklist.checklist_ID AND checklist.checklist_ID=$c_ID AND checklist.isRemoved=0 AND checklist.status_ID!=15";
				$c_permission =  $this->general_model->custom_query($c_p);

				// Process template permission
				$p_p = "SELECT process.process_ID FROM tbl_processST_process as process, tbl_processST_assignment ass WHERE ass.assignedTo=$userid AND ass.assignmentRule_ID=1 AND ass.folProCheTask_ID=$p_ID AND ass.folProCheTask_ID=process.process_ID AND process.process_ID=$p_ID AND process.isRemoved=0 AND process.processstatus_ID!=15";
				$p_permission =  $this->general_model->custom_query($p_p);

				// CONDITION if checklist has permission to edit
				if ($c_permission != null) {
					$access['checklist_editable'] = "1";
					$access['process_editable'] = "0";
				} else if ($p_permission != null) {
					//CONDITION if process temp has permission to edit
					$access['checklist_editable'] = "0";
					$access['process_editable'] = "1";
				} else {
					$access['checklist_editable'] = "0";
					$access['process_editable'] = "0";
				}
			}
		}
		echo json_encode($access);
	}
	// TEST ACCESS

	public function fetch_access_admin()
	{
		$userid = $this->session->userdata('uid');
		return $this->general_model->fetch_specific_vals("*", "user_ID=$userid", "tbl_processST_admin_access");
	}
	public function find_folder()
	{
		$data = $_POST['input'];
		$res = $this->process_model->search_folder($data);
		echo json_encode($res);
	}
	public function countallforms()
	{
		$id = $_POST['task_ID'];
		$data = $this->process_model->countallforms($id);
		echo json_encode($data);
	}
	public function user_details()
	{
		$userid = $this->session->userdata('uid');
		$answeredby = $this->process_model->get_empid($userid)->row();
		return $answeredby;
	}


	//CUSTOM VALIDATION

	private function get_subtask_of_task($task_id)
	{
		$fields = "subTask_ID, task_ID, complabel, required, min, max,component_ID";
		$where = "task_ID = $task_id";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_subtask");
	}

	public function generate_validation()
	{
		$task_id = $this->input->post('taskId');
		// $task_id = 77;
		$sub_task = $this->get_subtask_of_task($task_id);
		$validation_count = 0;
		$validator_per_subtask = [];
		$no_validation_fields = [];
		$no_validation_fields_count = 0;
		$validation['stat'] = 0;
		// var_dump($sub_task);
		if (count($sub_task) > 0) {
			foreach ($sub_task as $sub_task_row) {
				$validator['validators'] = [];

				$notEmpty['message'] = $sub_task_row->complabel . " is required";
				if($sub_task_row->component_ID == 12){
					$validator['validators'] += ['emailAddress' => 'This is not a valid email'];
				}
				if ($sub_task_row->required === "yes") {
					$validator['validators'] += ['notEmpty' => $notEmpty];
				}
				$charWord = "character";
				if ((int) $sub_task_row->min != 0 && (int) $sub_task_row->max != 0) {
					$minChar = $charWord;
					$maxChar = $charWord;
					if ($sub_task_row->min > 1) {
						$minChar .= "s";
					}
					if ($sub_task_row->max > 1) {
						$maxChar .= "s";
					}
					$length['min'] = $sub_task_row->min;
					$length['max'] = $sub_task_row->max;
					$length['message'] = "input should be minimum of " . $sub_task_row->min . " " . $minChar . " and maximum of " . $sub_task_row->max . " " . $maxChar;
					$validator['validators'] += ['stringLength' => $length];
				} else if ((int) $sub_task_row->min != 0 && (int) $sub_task_row->max == 0) {
					if ($sub_task_row->min > 1) {
						$charWord .= "s";
					}
					$length['min'] = $sub_task_row->min;
					$length['message'] = "input at least " . $sub_task_row->min . " " . $charWord;
					$validator['validators'] += ['stringLength' => $length];
				} else if ((int) $sub_task_row->min == 0 && (int) $sub_task_row->max != 0) {
					if ($sub_task_row->max > 1) {
						$charWord .= "s";
					}
					$length['max'] = $sub_task_row->max;
					$length['message'] = "input should not exceed to " . $sub_task_row->min . " " . $charWord;
					$validator['validators'] += ['stringLength' => $length];
				}
				// var_dump($validator['validators']);
				if (count($validator['validators']) > 0) {
					$validation_count = 1;
					$validator_per_subtask["form_component" . $sub_task_row->subTask_ID] = $validator;
				} else {
					$no_validation_fields[$no_validation_fields_count] = "form_component" . $sub_task_row->subTask_ID;
					$no_validation_fields_count++;
				}
			}

			$validation_option['message'] = 'This value is not valid';
			$validation_option['excluded'] = ':disabled';
			$validation_option['feedbackIcons'] = [
				'valid' => 'glyphicon glyphicon-ok',
				'invalid' => 'glyphicon glyphicon-remove',
				'validating' => 'glyphicon glyphicon-refresh'
			];
			$validation['stat'] = $validation_count;
			$validation_option['fields'] = $validator_per_subtask;
			$validation['option'] = $validation_option;
			$validation['no_validation_fields'] = $no_validation_fields;
		} else {
			$validation['stat'] = 0;
			$validation['no_validation_fields'] = 0;
		}
		echo json_encode($validation);
	}

	#Mark's Code
	public function schedule_checklist()
	{
		$data = $this->user_infos();

		$this->load_template_view('templates/process/schedule_checklist', $data);
	}
	public function add_auto_sched_cron()
	{
		$processID = $this->input->post("process_ID");
		$assignees = implode(",", $this->input->post("assignees"));
		$cronEx = $this->input->post("cronEx");
		$deadline = $this->input->post("deadline");
		$command = $cronEx . " curl '" . base_url("cronjobs/addchecklist/$processID/");

		$info['process_ID'] = $processID;
		$info['assignees'] = $assignees;
		$info['cron_sched'] = $cronEx;
		$info['command'] = $command;
		$info['deadline'] = $deadline;
		$info['added_by'] = $_SESSION["uid"];
		$info['isActive'] = 1;
		// echo json_encode($info);
		if (!empty($cronEx)) {
			$id = $this->general_model->insert_vals_last_inserted_id($info, "tbl_processST_cronjobs");
			$addCron = $this->addCron($processID, $id);
		} else {
			$id = 0;
		}

		echo $id;
	}
	public function addCron($processID, $cron_ID)
	{
		$output = shell_exec('crontab -l');
		$rs = $this->db->query("Select * from tbl_processST_cronjobs where pstCron_ID=" . $cron_ID);
		// $command = $rs->result()[0]->command;
		$command = $rs->result()[0]->cron_sched . ' curl "' . base_url("cronjobs/addchecklist/") . $processID . "/" . $cron_ID . '"';

		file_put_contents('/tmp/crontab.txt', $output . $command . PHP_EOL);
		exec('crontab /tmp/crontab.txt');
	}



	public function deleteCron()
	{

		$process_ID = $this->input->post("process_ID");
		$pstCron_ID = $this->input->post("pstCron_ID");
		$rs = $this->general_model->fetch_specific_vals("a.*,b.processTitle", "a.process_ID=b.process_ID and a.pstCron_ID=" . $pstCron_ID, "tbl_processST_cronjobs a,tbl_processST_process b");
		$link = str_replace("'", "\"", $rs[0]->command . $pstCron_ID) . "\"";
		// echo substr($t_string, 0, -1);
		$output = shell_exec('crontab -l');
		if (strstr($output, $link)) {
			echo 'found';
			$this->db->query("DELETE FROM tbl_processST_cronjobs where pstCron_ID=" . $pstCron_ID);
			$newcron = str_replace($link, "", $output);
			file_put_contents('/tmp/crontab.txt', $newcron . PHP_EOL);
			exec('crontab /tmp/crontab.txt');
		} else {
			echo 'not found';
		}
	}
	public function getRecords()
	{
		$rs = $this->general_model->fetch_specific_vals("a.*,b.processTitle", "a.process_ID=b.process_ID and a.added_by=" . $_SESSION["uid"], "tbl_processST_cronjobs a,tbl_processST_process b");
		$data["cron"]  = array();
		foreach ($rs as $row) {
			$assign_pepz = $this->getNames($row->assignees);
			$data["cron"][] = array(
				"pstCron_ID" => $row->pstCron_ID,
				"process_ID" => $row->process_ID,
				"assignees" => $assign_pepz,
				"cron_sched" => $row->cron_sched,
				"cron_sched" => $row->cron_sched,
				"deadline" => $row->deadline,
				"added_on" => date("Y-m-d h:i A", strtotime($row->added_on)),
				"processTitle" => $row->processTitle,
			);
		}
		echo (!empty($data["cron"])) ? json_encode($data) : 0;
	}
	public function getNames($ids)
	{
		$data = $this->general_model->fetch_specific_vals("applicant.fname,applicant.mname,applicant.lname", "user.uid in ($ids) AND user.emp_id=employee.emp_id AND employee.apid=applicant.apid", "tbl_applicant applicant, tbl_employee employee, tbl_user user");

		return $data;
	}

	public function display_dynamicfilefetch($subtask_ID, $checklist_ID)
	{

		// $img = $_FILES['upload_files'.$subtask_ID]['name'];
		// $tmp = $_FILES['upload_files'.$subtask_ID]['tmp_name'];
		$userid = $this->session->userdata('uid');
		$valid_extensions = array('xlsx', 'xls', 'zip', 'doc', 'mov', 'docx', 'ppt', 'pdf', 'pptx', 'rar', 'png', 'jpg'); // valid extensions
		$path = 'uploads/process/form_dynamic/'; // upload directory

		$img = $_FILES['upload_files']['name'];
		$tmp = $_FILES['upload_files']['tmp_name'];
		// get uploaded file's extension
		$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
		// can upload same image using rand function
		$final_image = time() . $img;
		// check's valid format
		if (in_array($ext, $valid_extensions)) {

			$path = $path . strtolower($final_image);
			// echo $_FILES["videoFile"]["error"];
			if (move_uploaded_file($tmp, $path)) {
				// echo $path;
				$info['subtask_ID'] = $subtask_ID;
				$info['checklist_ID'] = $checklist_ID;
				$info['answer'] = $path;
				$info['answeredBy'] = $userid;

				$info2["answer_ID"] = $this->general_model->insert_vals_last_inserted_id($info, "tbl_processST_answer");
				$info2['complabel'] = $path;
				$info2['subtask_ID'] = $subtask_ID;

				echo json_encode($info2);
			}
		} else {
			echo 'invalid';
		}
	}
	#Mark's Code

	// MIC2X CODES STARTS HERE

	// READ start
	private function qry_select_assignees($process_id)
	{
		$user_id = $this->session->userdata('uid');
		// $fields = "DISTINCT(ass.assignedTo) assignedTo, app.fname, app.mname, app.lname, emp.emp_id ";
		// $where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = ass.assignedTo AND ass.folProCheTask_ID = chkList.checklist_ID AND ass.type = 'checklist' AND chkList.process_ID = $process_id AND assignedTo != $user_id";
		// $table = "tbl_processST_checklist chkList, tbl_processST_assignment ass, tbl_applicant app, tbl_employee emp, tbl_user users";
		// return $this->general_model->fetch_specific_vals($fields, $where, $table);
		$qry = "SELECT DISTINCT(ass.assignedTo) assignedTo, app.fname, app.mname, app.lname, emp.emp_id FROM tbl_processST_checklist chkList, tbl_processST_assignment ass, tbl_applicant app, tbl_employee emp, tbl_user users where app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = ass.assignedTo AND ass.folProCheTask_ID = chkList.checklist_ID AND ass.type = 'checklist' AND chkList.process_ID = $process_id AND chkList.isRemoved = 0 AND ass.assignedTo != $user_id UNION SELECT DISTINCT(ass2.assignedTo) assignedTo, app2.fname, app2.mname, app2.lname, emp2.emp_id FROM tbl_processST_assignment ass2, tbl_user users2, tbl_employee emp2, tbl_applicant app2, tbl_processST_checklist chkList2, tbl_processST_checklistStatus chkListStat, tbl_processST_task task WHERE app2.apid = emp2.apid AND emp2.emp_id = users2.emp_id AND users2.uid = ass2.assignedTo AND task.task_ID = chkListStat.task_ID AND ass2.folProCheTask_ID = chkListStat.checklistStatus_ID AND chkListStat.checklist_ID = chkList2.checklist_ID AND chkList2.process_ID = $process_id AND chkList2.isRemoved = 0 AND ass2.assignedTo != $user_id";
		return $record = $this->general_model->custom_query($qry);
	}
	private function qry_select_statuses($process_id)
	{
		$fields = "DISTINCT(stat.status_ID) status_id, stat.description";
		$where = "stat.status_ID = chkList.status_ID AND chkList.process_ID = proc.process_ID AND proc.process_ID = $process_id";
		$table = "tbl_processST_process proc, tbl_processST_checklist chkList, tbl_status stat";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}
	private function qry_select_columns($process_id)
	{
		$fields = "DISTINCT(ass.assignedTo) assignedTo, app.fname, app.mname, app.lname, emp.emp_id ";
		$where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = ass.assignedTo AND ass.folProCheTask_ID = chkList.checklist_ID AND ass.type = 'checklist' AND chkList.process_ID = $process_id";
		$table = "tbl_processST_checklist chkList, tbl_processST_assignment ass, tbl_applicant app, tbl_employee emp, tbl_user users";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}
	protected function qry_process_details($process_id)
	{
		$fields = "proc.process_ID, proc.processTitle, proc.processType_ID , procType.processType, stat.description, proc.createdBy, app.fname, app.mname, app.lname, proc.dateTimeCreated, proc.dateTimeUpdated, proc.updatedBy";
		$where = "procType.processType_ID = proc.processType_ID AND stat.status_ID = proc.processstatus_ID AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = proc.createdBy AND proc.process_ID = $process_id";
		$table = "tbl_processST_process proc, tbl_processST_processType procType, tbl_status stat, tbl_applicant app, tbl_employee emp, tbl_user users";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}


	private function qry_checklist_details($process_id, $checkListName, $status_id, $assignee, $limit = 0, $confirmation_stat)
	{
		$where_confirm_status =""; 
		$where_name = "";
		$where_status = "";
		$where_assignee = "";
		$permission_checklist = $this->checklist_id_forreport($process_id);
		foreach($permission_checklist as $permission_checklist_index => $permission_checklist_row){
			$permission_checklist_ids[$permission_checklist_index] = $permission_checklist_row->checklist_ID;
		}
		$permission_checklist_str = implode(", ", array_values($permission_checklist_ids));
		// var_dump($permission_checklist_str);
		$confirm_stat_val = trim($confirmation_stat);
		if($confirmation_stat !== '0' && $confirm_stat_val != ''){
			$confirm_stat_arr = explode(",", $confirmation_stat);
			$confirm_statuses = [];
			for($loop = 0; $loop < count($confirm_stat_arr); $loop++){
				// var_dump($confirm_stat_arr[$loop]);
				if((int)$confirm_stat_arr[$loop] !== 0){
					$confirm_statuses[$loop] = "confirm.status_ID = 26";
				}else{
					$confirm_statuses[$loop] = "confirm.status_ID IS NULL";
				}
			}
			if(count($confirm_statuses) > 0){
				$where_confirm_status =  "AND (".implode(" OR ", $confirm_statuses).")";
			}
		}
		
		if ($checkListName != "") {
			$where_name = " AND (chkList.checklistTitle LIKE \"%$checkListName%\" OR chkList.checklistTitle LIKE \"% $checkListName%\") ";
		}
		if ($status_id != "") {
			$where_status = " AND chkList.status_ID IN (" . $status_id . ")";
		}
		if ($assignee != "") {
			$where_assignee = " AND chkList.checklist_ID IN (SELECT DISTINCT(chkList.checklist_ID) as checklist_ID FROM tbl_processST_assignment ass, tbl_processST_checklist chkList WHERE chkList.checklist_ID = ass.folProCheTask_ID AND ass.type = 'checklist' AND ass.assignedTo IN ($assignee) AND chkList.process_ID  = $process_id UNION SELECT DISTINCT(chkList2.checklist_ID) as checklist_ID FROM tbl_processST_assignment ass2, tbl_processST_checklist chkList2, tbl_processST_checklistStatus chkListStat, tbl_processST_task task  WHERE task.task_ID = chkListStat.task_ID AND ass2.folProCheTask_ID = chkListStat.checklistStatus_ID AND chkListStat.checklist_ID = chkList2.checklist_ID AND ass2.assignedTo IN ($assignee) AND chkList2.process_ID  = $process_id)";
		}
		$query = "SELECT chkList.checklist_ID ,chkList.checklistTitle, chkList.status_ID , stat.description, chkList.dateTimeCreated, chkList.dueDate, chkList.dateTimeCompleted, chkList.completedBy, chkList.createdBy FROM tbl_status stat INNER JOIN tbl_processST_checklist chkList ON stat.status_ID = chkList.status_ID LEFT JOIN tbl_processST_checklist_confirmation confirm ON chkList.checklist_ID = confirm.checklist_ID WHERE chkList.checklist_ID IN ($permission_checklist_str) $where_confirm_status AND chkList.isRemoved = 0 AND chkList.process_ID = $process_id" . $where_name . "" . $where_status . "" . $where_assignee . " ORDER BY chkList.dateTimeCreated ASC LIMIT $limit, 10";
		$record = $this->general_model->custom_query($query);
		return $this->general_model->custom_query($query);
	}

	private function qry_confirmation_checklist_ids($checklist_ids){
		$qry = "SELECT chcklist.checklist_ID, confirm.status_ID FROM tbl_processST_checklist chcklist LEFT JOIN tbl_processST_checklist_confirmation confirm ON chcklist.checklist_ID = confirm.checklist_ID WHERE chcklist.checklist_ID IN ($checklist_ids)";
		return $this->general_model->custom_query($qry);
	}

	private function qry_confirmation_select($checklist_ids){
		$qry = "SELECT DISTINCT(confirm.status_ID) FROM tbl_processST_checklist chcklist LEFT JOIN tbl_processST_checklist_confirmation confirm ON chcklist.checklist_ID = confirm.checklist_ID WHERE chcklist.checklist_ID IN ($checklist_ids)";
		return $this->general_model->custom_query($qry);
	}

	// public function filter_checklist_report($checklist_list, $process_id, $confirmation_stat){
	// 	$permission_checklist = $this->checklist_id_forreport($process_id);
	// 	$chklist_count = 0;
	// 	$filtered_checklist_list = [];
	// 	if(count($permission_checklist) > 0){
	// 		$confirm_stat_val = trim($confirmation_stat);
	// 		$filtered_checklist = [];
	// 		$filtered_checklist_count = 0;
	// 		if($confirmation_stat !== '0' && $confirm_stat_val != ''){
	// 			$confirm_stat_arr = explode(",", $confirmation_stat);
	// 			foreach($permission_checklist as $permission_checklist_index => $permission_checklist_row){
	// 				$permission_checklist_ids[$permission_checklist_index] = $permission_checklist_row->checklist_ID;
	// 			}
	// 			$permission_checklist_str = implode(", ", array_values($permission_checklist_ids));
	// 			$confirmation_checklists = $this->qry_confirmation_checklist_ids($permission_checklist_str);

	// 			for($loop = 0; $loop < count($confirm_stat_arr); $loop++){
	// 				$checklist_confirmation_status = $confirm_stat_arr[$loop];
	// 				$filtered_checklist_by_confirm_stat = array_filter(
	// 					$confirmation_checklists,
	// 					function ($e) use ($checklist_confirmation_status) {
	// 						return (int)$e->status_ID === (int)$checklist_confirmation_status;
	// 					}
	// 				);
	// 				if(count($filtered_checklist_by_confirm_stat) > 0){
	// 					foreach($filtered_checklist_by_confirm_stat as $filtered_checklist_by_confirm_stat_row){
	// 						$filtered_checklist[$filtered_checklist_count]['checklist_ID'] = $filtered_checklist_by_confirm_stat_row->checklist_ID;
	// 						$filtered_checklist_count++;
	// 					}
	// 				}
	// 			}

	// 			// var_dump($checklist_list);
	// 			foreach($filtered_checklist as $filtered_checklist_row){
	// 				$checklist_row_id = $filtered_checklist_row['checklist_ID'];
	// 				$filtered_checklist_by_permission_confirm_status = array_filter(
	// 					$checklist_list,
	// 					function ($e) use ($checklist_row_id) {
	// 						return $e->checklist_ID == $checklist_row_id;
	// 					}
	// 				);
	// 				if(count($filtered_checklist_by_permission_confirm_status) > 0){
	// 					foreach($filtered_checklist_by_permission_confirm_status as $filtered_checklist_by_permission_confirm_status_row){
	// 						$filtered_checklist_list[$chklist_count] = $filtered_checklist_by_permission_confirm_status_row;
	// 						$chklist_count++;
	// 					}
	// 				}
	// 			}
	// 		}else{
	// 			foreach($permission_checklist as $permission_checklist_row){
	// 				$checklist_row_id = $permission_checklist_row->checklist_ID;
	// 				$filtered_checklist = array_filter(
	// 					$checklist_list,
	// 					function ($e) use ($checklist_row_id) {
	// 						return $e->checklist_ID == $checklist_row_id;
	// 					}
	// 				);
	// 				if(count($filtered_checklist) > 0){
	// 					foreach($filtered_checklist as $filtered_checklist_row){
	// 						$filtered_checklist_list[$chklist_count] = $filtered_checklist_row;
	// 						$chklist_count++;
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// 	return $filtered_checklist_list;
	// 	// var_dump($filtered_checklist_list);
	// }

	private function qry_no_limit_checklist_details($process_id, $checkListName, $status_id, $assignee, $confirmation_stat)
	{
		// echo $checkListName;
		$where_confirm_status =""; 
		$where_name = "";
		$where_status = "";
		$where_assignee = "";
				$permission_checklist = $this->checklist_id_forreport($process_id);
		foreach($permission_checklist as $permission_checklist_index => $permission_checklist_row){
			$permission_checklist_ids[$permission_checklist_index] = $permission_checklist_row->checklist_ID;
		}
		$permission_checklist_str = implode(", ", array_values($permission_checklist_ids));
		// var_dump($permission_checklist_str);
		$confirm_stat_val = trim($confirmation_stat);
		if($confirmation_stat !== '0' && $confirm_stat_val != ''){
			$confirm_stat_arr = explode(",", $confirmation_stat);
			$confirm_statuses = [];
			for($loop = 0; $loop < count($confirm_stat_arr); $loop++){
				// var_dump($confirm_stat_arr[$loop]);
				if((int)$confirm_stat_arr[$loop] !== 0){
					$confirm_statuses[$loop] = "confirm.status_ID = 26";
				}else{
					$confirm_statuses[$loop] = "confirm.status_ID IS NULL";
				}
			}
			if(count($confirm_statuses) > 0){
				$where_confirm_status =  "AND (".implode(" OR ", $confirm_statuses).")";
			}
		}
		if ($checkListName != "") {
			$where_name = " AND (chkList.checklistTitle LIKE \"%$checkListName%\" OR chkList.checklistTitle LIKE \"% $checkListName%\") ";
		}
		if ($status_id != "") {
			$where_status = " AND chkList.status_ID IN (" . $status_id . ")";
		}
		if ($assignee != "") {
			$where_assignee = " AND chkList.checklist_ID IN (SELECT DISTINCT(chkList.checklist_ID) as checklist_ID FROM tbl_processST_assignment ass, tbl_processST_checklist chkList WHERE chkList.checklist_ID = ass.folProCheTask_ID AND ass.type = 'checklist' AND ass.assignedTo IN ($assignee) AND chkList.process_ID  = $process_id UNION SELECT DISTINCT(chkList2.checklist_ID) as checklist_ID FROM tbl_processST_assignment ass2, tbl_processST_checklist chkList2, tbl_processST_checklistStatus chkListStat, tbl_processST_task task  WHERE task.task_ID = chkListStat.task_ID AND ass2.folProCheTask_ID = chkListStat.checklistStatus_ID AND chkListStat.checklist_ID = chkList2.checklist_ID AND ass2.assignedTo IN ($assignee) AND chkList2.process_ID  = $process_id)";
		}
		$query = "SELECT chkList.checklist_ID ,chkList.checklistTitle, chkList.status_ID , stat.description, chkList.dateTimeCreated, chkList.dueDate, chkList.dateTimeCompleted, chkList.completedBy, chkList.createdBy FROM tbl_status stat INNER JOIN tbl_processST_checklist chkList ON stat.status_ID = chkList.status_ID LEFT JOIN tbl_processST_checklist_confirmation confirm ON chkList.checklist_ID = confirm.checklist_ID WHERE chkList.checklist_ID IN ($permission_checklist_str) $where_confirm_status AND chkList.isRemoved = 0 AND chkList.process_ID = $process_id" . $where_name . "" . $where_status . "" . $where_assignee." ORDER BY chkList.dateTimeCreated ASC";
		return $this->general_model->custom_query($query);
	}

	private function qry_process_task($process_id)
	{
		$fields = "task_ID, taskTitle, status_ID";
		$where = "process_ID = $process_id AND isRemoved != 1";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_task");
	}

	private function qry_checklist_task($process_id)
	{
		$fields = "task.task_ID, task.taskTitle, chkListStat.isCompleted, chkListStat.checklist_ID";
		$where = "chkListStat.task_ID = task.task_ID AND task.process_ID = $process_id AND task.isRemoved != 1";
		$table = "tbl_processST_checklistStatus chkListStat, tbl_processST_task task";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	private function qry_process_subtask($task_id)
	{
		$fields = "sub.subTask_ID, sub.complabel, sub.task_ID, comp.fieldType, comp.component_ID";
		$where = "comp.component_ID = sub.component_ID AND sub.task_ID IN ($task_id) AND sub.isRemoved != 1 AND sub.component_ID != 9";
		$table = "tbl_processST_subtask sub, tbl_processST_component comp";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, "sub.subTask_ID ASC");
	}

	private function qry_checklist_answers($checklist_id)
	{
		$fields = "answer_ID, subtask_ID, answer, answeredBy, dateTimeAnswered";
		$where = "checklist_ID = $checklist_id";
		$table = "tbl_processST_subtask sub, tbl_processST_component comp";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_answer", "subtask_ID ASC");
	}

	private function qry_checklist_assignees($process_id)
	{
		$fields = "ass.assignedTo, app.fname, app.mname, app.lname, app.pic, positions.pos_details, accounts.acc_name, empStat.status, chkList.checklist_ID";
		$where = "empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = ass.assignedTo AND ass.folProCheTask_ID = chkList.checklist_ID AND chkList.process_ID = $process_id AND ass.type = 'checklist'";
		$table = "tbl_processST_assignment ass, tbl_user users, tbl_employee emp, tbl_applicant app, tbl_processST_checklist chkList, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_account accounts";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	private function qry_checklist_task_assignees($process_id)
	{
		$fields = "ass.assignedTo, app.fname, app.mname, app.lname, app.pic, positions.pos_details, accounts.acc_name, chkList.checklist_ID, empStat.status";
		$where = "empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = ass.assignedTo AND task.task_ID = chkListStat.task_ID AND ass.folProCheTask_ID = chkListStat.checklistStatus_ID AND chkListStat.checklist_ID = chkList.checklist_ID AND chkList.process_ID = $process_id";
		$table = "tbl_processST_assignment ass, tbl_user users, tbl_employee emp, tbl_applicant app, tbl_processST_checklist chkList, tbl_processST_checklistStatus chkListStat, tbl_processST_task task, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_account accounts";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	public function get_assignees($check_list_assignees, $task_assignees, $check_list_id)
	{
		$assignees = [];
		$check_list_loop_assignees = array_filter(
			$check_list_assignees,
			function ($e) use ($check_list_id) {
				return $e->checklist_ID == $check_list_id;
			}
		);
		$task_list_loop_assignees = array_filter(
			$task_assignees,
			function ($e) use ($check_list_id) {
				return $e->checklist_ID == $check_list_id;
			}
		);
		return count(array_unique($check_list_loop_assignees, SORT_REGULAR)) + count(array_unique($task_list_loop_assignees, SORT_REGULAR));
	}

	public function get_assignees_view()
	{
		// $process_id = $this->input->post('processId');
		$process_id = 19;
		$check_list_id = 76;
		$check_list_assignees = $this->qry_checklist_assignees($process_id);
		$task_assignees = $this->qry_checklist_task_assignees($process_id);
		$check_list_loop_assignees = array_filter(
			$check_list_assignees,
			function ($e) use ($check_list_id) {
				return $e->checklist_ID == $check_list_id;
			}
		);
		$check_list_task_loop_assignees = array_filter(
			$task_assignees,
			function ($e) use ($check_list_id) {
				return $e->checklist_ID == $check_list_id;
			}
		);
		var_dump($check_list_loop_assignees);
		var_dump($check_list_task_loop_assignees);
	}

	public function get_detailed_assignees_names($check_list_assignees, $task_assignees, $check_list_id)
	{
		$assignees = [];
		$unique_assignees = [];
		$final_assignees = [];
		$check_list_loop_assignees = array_filter(
			$check_list_assignees,
			function ($e) use ($check_list_id) {
				return $e->checklist_ID == $check_list_id;
			}
		);
		$task_list_loop_assignees = array_filter(
			$task_assignees,
			function ($e) use ($check_list_id) {
				return $e->checklist_ID == $check_list_id;
			}
		);
		$chklist_assignee_names = array_unique($check_list_loop_assignees, SORT_REGULAR);
		$task_assignee_names = array_unique($task_list_loop_assignees, SORT_REGULAR);
		// var_dump($task_list_loop_assignees);
		// var_dump($chklist_assignee_names);
		$assignees_count = 0;
		if (count($chklist_assignee_names) > 0) {
			foreach ($chklist_assignee_names as $chklist_assignee_names_row) {
				$chklist_assignee_names_row->{'assignment'} = "checklist";
				$assignees[$assignees_count] = $chklist_assignee_names_row;
				$assignees_count++;
			}
		}
		if (count($task_assignee_names) > 0) {
			foreach ($task_assignee_names as $task_assignee_names_row) {
				$task_assignee_names_row->{'assignment'} = "task";
				$assignees[$assignees_count] = $task_assignee_names_row;
				$assignees_count++;
			}
		}
		// var_dump($task_assignee_names);
		// $assignee_names = array_merge($chklist_assignee_names, $task_assignee_names);
		$unique_assignees = array_unique($assignees, SORT_REGULAR);
		$final_assignees = array_merge($unique_assignees);
		return $final_assignees;
		// $assignee_list = "";
		// if (count($assignee_names) > 0) {
		// 	$assignees = [];
		// 	$assignees_count = 0;

		// 	foreach ($assignee_names as $assignee_names_row) {
		// 		$assignees[$assignees_count] =  ucwords($assignee_names_row->fname) . " " . ucwords($assignee_names_row->lname);
		// 		$assignees_count++;
		// 	}
		// 	$assignee_list = implode(", ", array_values($assignees));
		// }
		// return $assignee_list;
	}

	public function get_assignees_names($check_list_assignees, $task_assignees, $check_list_id)
	{
		$assignees = [];
		$unique_assignees = [];
		$final_assignees = [];
		$check_list_loop_assignees = array_filter(
			$check_list_assignees,
			function ($e) use ($check_list_id) {
				return $e->checklist_ID == $check_list_id;
			}
		);
		$task_list_loop_assignees = array_filter(
			$task_assignees,
			function ($e) use ($check_list_id) {
				return $e->checklist_ID == $check_list_id;
			}
		);
		$chklist_assignee_names = array_unique($check_list_loop_assignees, SORT_REGULAR);
		$task_assignee_names = array_unique($task_list_loop_assignees, SORT_REGULAR);
		$assignees_count = 0;
		if (count($chklist_assignee_names) > 0) {
			foreach ($chklist_assignee_names as $chklist_assignee_names_row) {
				$assignees[$assignees_count] =  ucwords($chklist_assignee_names_row->fname) . " " . ucwords($chklist_assignee_names_row->lname);
				$assignees_count++;
			}
		}
		if (count($task_assignee_names) > 0) {
			foreach ($task_assignee_names as $task_assignee_names_row) {
				$assignees[$assignees_count] =  ucwords($task_assignee_names_row->fname) . " " . ucwords($task_assignee_names_row->lname);
				$assignees_count++;
			}
		}
		$unique_assignees = array_unique($assignees, SORT_REGULAR);
		$final_assignees = array_merge($unique_assignees);
		$assignee_list = implode(", ", array_values($final_assignees));
		return $assignee_list;
	}


	public function increment_text()
	{
		$a = 0;
		$x = 'A';
		while ($a < 20) {
			$x++;
			echo $x;
			$a++;
		}
	}

	private function columnLetter($c)
	{
		$c = intval($c);
		if ($c <= 0) return '';
		$letter = '';

		while ($c != 0) {
			$p = ($c - 1) % 26;
			$c = intval(($c - $p) / 26);
			$letter = chr(65 + $p) . $letter;
		}
		return $letter;
	}

	public function test_checklist_assignee()
	{
		$process_id = 15;
		$check_list_assignees = $this->qry_checklist_assignees($process_id);
		$task_assignees = $this->qry_checklist_task_assignees($process_id);
		$assignees = $this->get_detailed_assignees_names($check_list_assignees, $task_assignees, 82);
		var_dump($assignees);
	}

	public function get_checklist_assignee_details()
	{
		$data['exist'] = 0;
		$process_id = $this->input->post('processId');
		$check_list_assignees = $this->qry_checklist_assignees($process_id);
		$task_assignees = $this->qry_checklist_task_assignees($process_id);
		$assignees = $this->get_detailed_assignees_names($check_list_assignees, $task_assignees, $this->input->post('checklistId'));
		if (count($assignees) > 0) {
			$data['exist'] = 1;
			$data['record'] = $assignees;
		}
		echo json_encode($data);
	}

	private function construct_excel_data($process_id, $checkListName, $status_id, $assignee_select, $hidden_cols_arr, $confirmation_stat)
	{
		$proc_details = $this->qry_process_details($process_id);
		$data['headers'] = "";
		$data['row_records'] = "";
		$headers[0] = ['col' => 'A', 'id' => 'A6', 'title' => 'Checklist Name',];
		if((int)$proc_details->processType_ID == 2){
			$main_col = [
				['class' => '.chkList_info2', 'title' => 'Date Created'],
				['class' => '.chkList_info3', 'title' => 'Due Date'],
				['class' => '.chkList_info4', 'title' => 'Checklist Status'],
				['class' => '.chkList_info8', 'title' => 'Confirmation Status'],
				['class' => '.chkList_info5', 'title' => 'Date Completed'],
				['class' => '.chkList_info6', 'title' => 'Assignees'],
				['class' => '.chkList_info7', 'title' => 'Tasks Completed'],
			];
		}else{
			$main_col = [
				['class' => '.chkList_info2', 'title' => 'Date Created'],
				['class' => '.chkList_info3', 'title' => 'Due Date'],
				['class' => '.chkList_info4', 'title' => 'Checklist Status'],
				['class' => '.chkList_info5', 'title' => 'Date Completed'],
				['class' => '.chkList_info6', 'title' => 'Assignees'],
				['class' => '.chkList_info7', 'title' => 'Tasks Completed'],
			];
		}
		$orig_cell_num_start = 1;
		$letter_num = 2;
		for ($main_col_loop = 0; $main_col_loop < count($main_col); $main_col_loop++) {
			if (array_search($main_col[$main_col_loop]['class'], $hidden_cols_arr) === false) {
				$headers[$orig_cell_num_start] = [
					'col' => $this->columnLetter($letter_num),
					'id' => $this->columnLetter($letter_num) . '6',
					'title' => $main_col[$main_col_loop]['title'],
				];
				$orig_cell_num_start++;
				$letter_num++;
			}
		}
		$checklist = $this->qry_no_limit_checklist_details($process_id, $checkListName, $status_id, $assignee_select, $confirmation_stat);
		// var_dump($checklist);
		// $checklist = $this->filter_checklist_report($all_checklist, $process_id, $confirmation_stat);
		$task = $this->qry_process_task($process_id);
		$checklist_task = $this->qry_checklist_task($process_id);
		$check_list_assignees = $this->qry_checklist_assignees($process_id);
		$task_assignees = $this->qry_checklist_task_assignees($process_id);
		$task_arr = [];
		$col_arr = [];
		$col_arr_count = 0;
		$checklist_answers = [];
		$task_count = 0;
		$checklist_answers_count = 0;
		if (count($task) > 0) {
			foreach ($task as $task_row) {
				$task_arr[$task_count] = $task_row->task_ID;
				$task_count++;
			}
			$task_ids = implode(",", $task_arr);
			$sub_task = $this->qry_process_subtask($task_ids);
			$check_list_count = 0;
			$check_list_row_start = 7;
			if (count($checklist) > 0) {
				$body_row = [];
				$body_row_count = 0;
				foreach ($checklist as $checklist_row) {
					$body_col = [];
					$body_col_count = 0;
					$body_letter_num = 1;
					$assignees_count = $this->get_assignees_names($check_list_assignees, $task_assignees, $checklist_row->checklist_ID);
					$answers = $this->qry_checklist_answers($checklist_row->checklist_ID);
					$searchedCheckListTaskValue = $checklist_row->checklist_ID;
					$checklist_checklist_task = array_filter(
						$checklist_task,
						function ($e) use ($searchedCheckListTaskValue) {
							return $e->checklist_ID == $searchedCheckListTaskValue;
						}
					);
					// PREPARE DEFAULT DATA VALUE
					$checklist_row_content = "";
					$date_created = new DateTime($checklist_row->dateTimeCreated);
					$due_date = "";
					$completed_date = "";
					$completed_by = "";
					$confirmation = "";
					if ($checklist_row->dueDate != null) {
						$date_due = new DateTime($checklist_row->dueDate);
						$due_date = $date_due->format('m/d/Y');
					}
					if ($checklist_row->dateTimeCompleted != null) {
						$date_completed = new DateTime($checklist_row->dateTimeCompleted);
						$completed_date = $date_completed->format('m/d/Y g:i:s A');
					}
					if ($checklist_row->completedBy != null) {
						$completed_by = $checklist_row->completedBy;
					}
					$task_completed_count = 0;
					foreach ($checklist_checklist_task as $checklist_checklist_task_row) {
						if ($checklist_checklist_task_row->isCompleted == 3) {
							$task_completed_count++;
						}
					}
					if (count($checklist_checklist_task) == 0) {
						$task_progress = 0;
						$progress_label = "0/0";
					} else {
						$task_progress = ($task_completed_count / count($checklist_checklist_task)) * 100;
						$progress_label = $task_completed_count . "/" . count($checklist_checklist_task);
					}
					if((int)$proc_details->processType_ID == 2 && (int)$checklist_row->status_ID == 3){
						$assignment = $this->check_coaching_log_assignment($checklist_row->checklist_ID, $checklist_row->createdBy);
						$confirmation = "Pending";
						if (count($assignment) > 0) {
							$confirmation_details = $this->qry_confirmation_details($checklist_row->checklist_ID, $assignment->assignedTo);
							if (count($confirmation_details) > 0) {
								$confirmation = "Confirmed";
								if ($confirmation_details->status_ID == 12) {
									$confirmation = "Missed";
								}
							}
						}
					}
					$check_list_name = "Untitled Checklist";
					if ($checklist_row->checklistTitle != null) {
						$check_list_name = $checklist_row->checklistTitle;
					}
					$body_col[$body_col_count] =  [
						'col' => $this->columnLetter($body_letter_num),
						'id' => $this->columnLetter($body_letter_num) . '' . $check_list_row_start,
						'value' => $check_list_name,
					];
					$body_col_count++;
					$body_letter_num++;
					if((int)$proc_details->processType_ID == 2){
						$body_main_data = [
							['class' => '.chkList_info2', 'value' => $date_created->format('m/d/Y g:i:s A')],
							['class' => '.chkList_info3', 'value' => $due_date],
							['class' => '.chkList_info4', 'value' => $checklist_row->description],
							['class' => '.chkList_info8', 'value' => $confirmation],
							['class' => '.chkList_info5', 'value' => $completed_date],
							['class' => '.chkList_info6', 'value' => $assignees_count],
							['class' => '.chkList_info7', 'value' => $progress_label],
						];
					}else{
						$body_main_data = [
							['class' => '.chkList_info2', 'value' => $date_created->format('m/d/Y g:i:s A')],
							['class' => '.chkList_info3', 'value' => $due_date],
							['class' => '.chkList_info4', 'value' => $checklist_row->description],
							['class' => '.chkList_info5', 'value' => $completed_date],
							['class' => '.chkList_info6', 'value' => $assignees_count],
							['class' => '.chkList_info7', 'value' => $progress_label],
						];
					}
					for ($body_main_data_loop = 0; $body_main_data_loop < count($body_main_data); $body_main_data_loop++) {
						if (array_search($body_main_data[$body_main_data_loop]['class'], $hidden_cols_arr) === false) {
							$body_col[$body_col_count] =  [
								'col' => $this->columnLetter($body_letter_num),
								'id' => $this->columnLetter($body_letter_num) . '' . $check_list_row_start,
								'value' => $body_main_data[$body_main_data_loop]['value'],
							];
							$body_col_count++;
							$body_letter_num++;
						}
					}
					foreach ($checklist_checklist_task as $checklist_checklist_task_row) {
						$task_stat_val = "Not Completed";
						if ($checklist_checklist_task_row->isCompleted == 3) {
							$task_stat_val = "Completed";
						}
						if (array_search('.task' . $checklist_checklist_task_row->task_ID, $hidden_cols_arr) === false) {
							$body_col[$body_col_count] =  [
								'col' => $this->columnLetter($body_letter_num),
								'id' => $this->columnLetter($body_letter_num) . '' . $check_list_row_start,
								'class' => 'task' . $checklist_checklist_task_row->task_ID,
								'value' => $task_stat_val,
							];
							$body_col_count++;
							$body_letter_num++;
						}
						$searchedTaskValue = $checklist_checklist_task_row->task_ID;
						$task_sub_task = array_filter(
							$sub_task,
							function ($e) use ($searchedTaskValue) {
								return $e->task_ID == $searchedTaskValue;
							}
						);
						if (count($task_sub_task) > 0) {
							foreach ($task_sub_task as $task_sub_task_row) {
								$searchedTaskValue = $task_sub_task_row->subTask_ID;
								$sub_task_answer = array_filter(
									$answers,
									function ($e) use ($searchedTaskValue) {
										return $e->subtask_ID == $searchedTaskValue;
									}
								);
								$subtask_answer_val = "";
								$mult_answer = [];
								$mult_answer_count = 0;
								if (count($sub_task_answer) > 0) {
									if (count($sub_task_answer) > 1) {
										foreach ($sub_task_answer as $sub_task_answer_row) {
											$mult_answer[$mult_answer_count] = $sub_task_answer_row->answer;
											$mult_answer_count++;
										}
										$subtask_answer_val = implode(",", $mult_answer);
									} else {
										foreach ($sub_task_answer as $sub_task_answer_row) {
											if ($sub_task_answer_row->answer != null) {
												$subtask_answer_val = $sub_task_answer_row->answer;
											}
										}
									}
								}
								if (array_search('.subtask' . $task_sub_task_row->subTask_ID, $hidden_cols_arr) === false) {
									$body_col[$body_col_count] =  [
										'col' => $this->columnLetter($body_letter_num),
										'id' => $this->columnLetter($body_letter_num) . '' . $check_list_row_start,
										'class' => 'subtask' . $task_sub_task_row->subTask_ID,
										'value' => $subtask_answer_val,
									];
									$body_col_count++;
									$body_letter_num++;
								}
							}
						}
					}
					if (count($body_col) > 0) {
						$body_row[$body_row_count] = $body_col;
						$body_row_count++;
						$check_list_row_start++;
					}
				}
				foreach ($task as $task_row) {
					$searchedTaskValue = $task_row->task_ID;
					$task_title = "Untitled Task";
					if ($task_row->taskTitle != null) {
						$task_title = ucwords(strtolower($task_row->taskTitle));
					}
					if (array_search('.task' . $task_row->task_ID, $hidden_cols_arr) === false) {
						$headers[$orig_cell_num_start] = [
							'col' => $this->columnLetter($letter_num),
							'id' => $this->columnLetter($letter_num) . '6',
							'title' => $task_title,
						];
						$orig_cell_num_start++;
						$letter_num++;
					}
					$task_sub_task = array_filter(
						$sub_task,
						function ($e) use ($searchedTaskValue) {
							return $e->task_ID == $searchedTaskValue;
						}
					);
					if (count($task_sub_task) > 0) {
						foreach ($task_sub_task as $task_sub_task_row) {
							if (($task_sub_task_row->complabel == null) || ($task_sub_task_row->component_ID == 15) || ($task_sub_task_row->component_ID == 16) || ($task_sub_task_row->component_ID == 17) || ($task_sub_task_row->component_ID == 18)) {
								$col_label = $task_sub_task_row->fieldType;
							} else {
								$col_label = $task_sub_task_row->complabel;
							}
							if (array_search('.subtask' . $task_sub_task_row->subTask_ID, $hidden_cols_arr) === false) {
								$headers[$orig_cell_num_start] = [
									'col' => $this->columnLetter($letter_num),
									'id' => $this->columnLetter($letter_num) . '6',
									'title' => $col_label,
								];
								$orig_cell_num_start++;
								$letter_num++;
							}
						}
					}
				}
				$data['headers'] = $headers;
				$data['row_records'] = $body_row;
			}
		}
		return $data;
	}


	public function process_checklist_excel_report()
	{
		$process_id = $this->input->post('processId');
		$checklist_name = $this->input->post('checkListName');
		$status_id = $this->input->post('status');
		$assignee_select = $this->input->post('assignees');
		$export_type = $this->input->post('export_type');
		$not_selected = $this->input->post('notSelectedDataVal');
		$confirmation_stat = $this->input->post('confirmationStatus');
		if ((int) $export_type == '1') {
			$hidden_cols = $not_selected;
		} else {
			$hidden_cols = "";
		}
		$hidden_cols_arr = explode(",", $hidden_cols);
		$excel_data = $this->construct_excel_data($process_id, $checklist_name, $status_id, $assignee_select, $hidden_cols_arr, $confirmation_stat);

		// $logo = 'C:\wamp64\www\supportz\assets\images\img\logo2.png';
		$logo = $this->dir . '/assets/images/img/logo2.png';

		$this->load->library('PHPExcel', null, 'excel');

		$this->excel->createSheet(0);

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Admin');
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($logo);
		$objDrawing->setOffsetX(5); // setOffsetX works properly
		$objDrawing->setOffsetY(5); //setOffsetY has no effect
		$objDrawing->setCoordinates('A1');
		$objDrawing->setHeight(60); // logo height
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
		$this->excel->getActiveSheet()->setShowGridlines(false);

		for ($excel_data_header_loop = 0; $excel_data_header_loop < count($excel_data['headers']); $excel_data_header_loop++) {
			// echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
			$this->excel->getActiveSheet()->setCellValue($excel_data['headers'][$excel_data_header_loop]['id'], $excel_data['headers'][$excel_data_header_loop]['title']);
			$this->excel->getActiveSheet()->getStyle($excel_data['headers'][$excel_data_header_loop]['id'])->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle($excel_data['headers'][$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
			$this->excel->getActiveSheet()->getStyle($excel_data['headers'][$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getColumnDimension($excel_data['headers'][$excel_data_header_loop]['col'])->setWidth(20);
		}

		for ($excel_row_loop = 0; $excel_row_loop < count($excel_data['row_records']); $excel_row_loop++) {
			for ($excel_col_loop = 0; $excel_col_loop < count($excel_data['row_records'][0]); $excel_col_loop++) {
				$this->excel->getActiveSheet()->setCellValue($excel_data['row_records'][$excel_row_loop][$excel_col_loop]['id'], $excel_data['row_records'][$excel_row_loop][$excel_col_loop]['value']);
			}
		}

		$this->excel->getActiveSheet()->getStyle($excel_data['headers'][0]['id'] . ':' . $excel_data['headers'][count($excel_data['headers']) - 1]['id'])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
		// echo $excel_data['headers'][0]['id'].':'.$excel_data['headers'][count($excel_data['headers'])-1]['id'];

		$this->excel->getActiveSheet()->getStyle('A6:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => 'DDDDDD'),
					),
				),
			)
		);

		$this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
		$this->excel->getActiveSheet()->getProtection()->setSheet(true);

		$filename = 'Process_Manage_report.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		// header('Set-Cookie: fileDownload=true; path=/');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	private function qry_confirmation_details($checklist_id, $confirmed_by)
    {
        $fields = "confirmation_ID, checklist_ID, status_ID, confirmedBy, dateConfirmed";
        $where = "checklist_ID = $checklist_id AND confirmedBy = $confirmed_by";
        $table = "tbl_processST_checklist_confirmation";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }

	public function process_checklist_table_details()
	{
		$process_id = $this->input->post('processId');
		$checkListName = $this->input->post('checkListName');
		$status_id = $this->input->post('statusId');
		$assignee_select = $this->input->post('assigneeIds');
		$confirmation_stat = $this->input->post('confirmationStatus');
		$limiter = $this->input->post('limiter');
		$proc_details = $this->qry_process_details($process_id);
		// $checklist = $this->qry_checklist_details($process_id, $checkListName, $status_id, $assignee_select, $limiter);
		$filtered_checklist = $this->qry_checklist_details($process_id, $checkListName, $status_id, $assignee_select, $limiter, $confirmation_stat);
		$filtered_all_checklist = $this->qry_no_limit_checklist_details($process_id, $checkListName, $status_id, $assignee_select, $confirmation_stat);
		$task = $this->qry_process_task($process_id);
		$checklist_task = $this->qry_checklist_task($process_id);
		$check_list_assignees = $this->qry_checklist_assignees($process_id);
		$task_assignees = $this->qry_checklist_task_assignees($process_id);
		// $filtered_checklist = $this->filter_checklist_report($checklist, $process_id, $confirmation_stat);
		// $filtered_all_checklist = $this->filter_checklist_report($all_checklist, $process_id, $confirmation_stat);
		// ---
		// var_dump($filtered_checklist);
		// var_dump($filtered_all_checklist);
		// $this->filter_checklist_report($checklist, $process_id);
		// $checklist = $this->qry_checklist_details(84);
		// $task = $this->qry_process_task(84);
		// -------------------
		$task_arr = [];
		$col_arr = [];
		$col_arr_count = 0;
		$checklist_answers = [];
		$task_count = 0;
		$checklist_answers_count = 0;
		$coaching_log_link = '/process/checklist_answerable/' . $process_id . '/';
		//IDENTIFY PROCESS TYPE
		if((int)$proc_details->processType_ID == 2){
			$coaching_log_link = '/process/coaching_log/';
		}
		if (count($task) > 0) {
			foreach ($task as $task_row) {
				$task_arr[$task_count] = $task_row->task_ID;
				$task_count++;
			}
			$task_ids = implode(",", $task_arr);
			$sub_task = $this->qry_process_subtask($task_ids);
			if (count($filtered_checklist) > 0) {
				foreach ($filtered_checklist as $checklist_row) {
					$assignees_count = $this->get_assignees($check_list_assignees, $task_assignees, $checklist_row->checklist_ID);
					$answers = $this->qry_checklist_answers($checklist_row->checklist_ID);
					$searchedCheckListTaskValue = $checklist_row->checklist_ID;
					$checklist_checklist_task = array_filter(
						$checklist_task,
						function ($e) use ($searchedCheckListTaskValue) {
							return $e->checklist_ID == $searchedCheckListTaskValue;
						}
					);
					$checklist_row_content = "";
					$date_created = new DateTime($checklist_row->dateTimeCreated);
					$due_date = "-";
					$completed_date = "-";
					$completed_by = "-";
					$bar_color = "bg-warning";
					$confirmation = "-";
					$confirmation_stat = "";
					if ($checklist_row->dueDate != null) {
						$date_due = new DateTime($checklist_row->dueDate);
						$due_date = $date_due->format('M j, Y');
					}
					if((int)$checklist_row->status_ID == 3){
						$bar_color = "bg-success";
						if ($checklist_row->dateTimeCompleted != null) {
							$date_completed = new DateTime($checklist_row->dateTimeCompleted);
							$completed_date = $date_completed->format('M j, Y g:i:s A');
						}
						if ($checklist_row->completedBy != null) {
							$completed_by = $checklist_row->completedBy;
						}
					}
					$task_completed_count = 0;
					foreach ($checklist_checklist_task as $checklist_checklist_task_row) {
						if ($checklist_checklist_task_row->isCompleted == 3) {
							$task_completed_count++;
						}
					}
					if (count($checklist_checklist_task) == 0) {
						$task_progress = 0;
						$progress_label = "0 / 0";
					} else {
						$task_progress = ($task_completed_count / count($checklist_checklist_task)) * 100;
						$progress_label = $task_completed_count . " / " . count($checklist_checklist_task);
					}
					$check_list_name = "<i>Untitled Checklist</i>";
					if ($checklist_row->checklistTitle != null) {
						$check_list_name = $checklist_row->checklistTitle;
					}
					$assignees_word = "Assignee";
					if ($assignees_count > 1) {
						$assignees_word .= 's';
					}
					if((int)$proc_details->processType_ID == 2){
						if((int)$checklist_row->status_ID == 3){
							$assignment = $this->check_coaching_log_assignment($checklist_row->checklist_ID, $checklist_row->createdBy);
							$confirmation = "Pending";
							if (count($assignment) > 0) {
								$confirmation_details = $this->qry_confirmation_details($checklist_row->checklist_ID, $assignment->assignedTo);
								if (count($confirmation_details) > 0) {
									$confirmation = "Confirmed";
									if ($confirmation_details->status_ID == 12) {
										$confirmation = "Missed";
									}
								}
							}
						}
						$confirmation_stat = '<td class="text-center chkList_info8 text-truncate" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px">' . $confirmation . '</td>';
					}
					$checklist_row_content .= '<th scope="row" class="text-capitalize text-left first-col col text-truncate"><a class="checklistTitle" data-toggle="tooltip" data-placement="top" data-skin="dark" data-original-title="' . $check_list_name . '" title="' . $check_list_name . '" data-checklistname="' . $check_list_name . '" href="' . base_url($coaching_log_link . $checklist_row->checklist_ID) . '">' . $check_list_name . '</a></th>';
					$checklist_row_content .= '<td class="text-center chkList_info2 text-truncate" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px">' . $date_created->format('M j, Y g:i:s A') . '</td>';
					$checklist_row_content .= '<td class="text-center chkList_info3 text-truncate" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px">' . $due_date . '</td>';
					$checklist_row_content .= '<td class="text-center chkList_info4 text-truncate" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px">' . ucwords($checklist_row->description). '</td>';
					$checklist_row_content .= $confirmation_stat;
					$checklist_row_content .= '<td class="text-center chkList_info5 text-truncate" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px">' . $completed_date . '</td>';
					$checklist_row_content .= '<td class="text-center chkList_info6 text-truncate assigneeInfo" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px" data-processid="' . $process_id . '" data-checklistid="' . $checklist_row->checklist_ID . '">' . $assignees_count . " " . $assignees_word . '</td>';
					$checklist_row_content .= '<td class="text-center text-truncate chkList_info7" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:13px"><div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated '.$bar_color.'" role="progressbar" aria-valuenow="' . $task_progress . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $task_progress . '%"><span class="show font-weight-bold">' . $progress_label . '</span></div></div></td>';
					foreach ($checklist_checklist_task as $checklist_checklist_task_row) {
						$task_stat_val = "-";
						if ($checklist_checklist_task_row->isCompleted == 3) {
							$task_stat_val = '<i class="fa fa-check text-success"></i>';
						}
						$checklist_row_content .= '<td class="text-center task' . $checklist_checklist_task_row->task_ID . '" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:12px">' . $task_stat_val . '</td>';
						// $col_arr[$col_arr_count] = '<th id="column-header-task-'.$task_row->task_ID.'" class="resizable-header text-center col-3 task'.$task_row->task_ID.'" style="word-break: break-word;">Task Completed<div id="column-header-task-'.$task_row->task_ID.'-sizer"></div></th>';
						// $col_arr_count++;
						$searchedTaskValue = $checklist_checklist_task_row->task_ID;
						$task_sub_task = array_filter(
							$sub_task,
							function ($e) use ($searchedTaskValue) {
								return $e->task_ID == $searchedTaskValue;
							}
						);
						if (count($task_sub_task) > 0) {
							foreach ($task_sub_task as $task_sub_task_row) {
								$searchedTaskValue = $task_sub_task_row->subTask_ID;
								$sub_task_answer = array_filter(
									$answers,
									function ($e) use ($searchedTaskValue) {
										return $e->subtask_ID == $searchedTaskValue;
									}
								);
								// var_dump($sub_task_answer);
								$subtask_answer_val = "-";
								$mult_answer = [];
								$mult_answer_count = 0;
								// $col_arr[$col_arr_count] = '<th id="column-header-subtask'.$task_sub_task_row->subTask_ID.'" class="resizable-header text-center col-3 subtask'.$task_sub_task_row->subTask_ID.'" style="word-break: break-word;">Task Completed<div id="column-header-subtask'.$task_sub_task_row->subTask_ID.'-sizer"></div></th>';
								// $col_arr_count++;
								if (count($sub_task_answer) > 0) {
									if (count($sub_task_answer) > 1) {
										foreach ($sub_task_answer as $sub_task_answer_row) {
											$mult_answer[$mult_answer_count] = $sub_task_answer_row->answer;
											$mult_answer_count++;
										}
										$subtask_answer_val = implode(",", $mult_answer);
									} else {
										foreach ($sub_task_answer as $sub_task_answer_row) {
											if ($sub_task_answer_row->answer != null) {
												$subtask_answer_val = $sub_task_answer_row->answer;
											}
										}
									}
								}
								$checklist_row_content .= '<td class="text-center text-truncate subtask' . $task_sub_task_row->subTask_ID . '" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1px;font-size:12px">' . $subtask_answer_val . '</td>';
							}
						}
					}
					$checklist_answers[$checklist_answers_count] = $checklist_row_content;
					$checklist_answers_count++;
				}
				// header
				foreach ($task as $task_row) {
					$searchedTaskValue = $task_row->task_ID;
					$task_title = "<i>Untitled Task</i>";
					if ($task_row->taskTitle != null) {
						if (strlen($task_row->taskTitle) > 50) {
							$task_title = ucwords(substr($task_row->taskTitle, 0, 50)) . "...";
						} else {
							$task_title = ucwords(strtolower($task_row->taskTitle));
						}
					}
					$col_arr[$col_arr_count] = '<th id="column-header-task-' . $task_row->task_ID . '" class="resizable-header text-truncate text-center col-3 task' . $task_row->task_ID . '">' . $task_title . '<div id="column-header-task-' . $task_row->task_ID . '-sizer"></div></th>';
					$col_arr_count++;
					$task_sub_task = array_filter(
						$sub_task,
						function ($e) use ($searchedTaskValue) {
							return $e->task_ID == $searchedTaskValue;
						}
					);
					if (count($task_sub_task) > 0) {
						foreach ($task_sub_task as $task_sub_task_row) {
							if (($task_sub_task_row->complabel == null) || ($task_sub_task_row->component_ID == 15) || ($task_sub_task_row->component_ID == 16) || ($task_sub_task_row->component_ID == 17) || ($task_sub_task_row->component_ID == 18)) {
								$col_label = $task_sub_task_row->fieldType;
							} else {
								if (strlen($task_sub_task_row->complabel) > 50) {
									$col_label = substr($task_sub_task_row->complabel, 0, 50) . "...";
								} else {
									$col_label = $task_sub_task_row->complabel;
								}
							}
							$col_arr[$col_arr_count] = '<th id="column-header-subtask' . $task_sub_task_row->subTask_ID . '" class="text-truncate resizable-header text-center col-3 subtask' . $task_sub_task_row->subTask_ID . '">' . ucwords(strtolower($col_label)) . '<div id="column-header-subtask' . $task_sub_task_row->subTask_ID . '-sizer"></div></th>';
							$col_arr_count++;
						}
					}
				}
			}
		}
		// var_dump($checklist);
		$data['task_columns'] = $col_arr;
		$data['answers'] = $checklist_answers;
		$data['process_details'] = $proc_details;
		$data['total_checklist'] = count($filtered_all_checklist);
		// var_dump($data);
		echo json_encode($data);
	}

	public function select_columns($process_id)
	{
		// $process_id = 84;
		$task = $this->qry_process_task($process_id);
		$task_arr = [];
		$col_arr = [];
		$task_count = 0;
		$col_count = 0;
		if (count($task) > 0) {
			foreach ($task as $task_row) {
				$task_arr[$task_count] = $task_row->task_ID;
				$task_count++;
			}
			$task_ids = implode(",", $task_arr);
			// echo $task_ids;
			$sub_task = $this->qry_process_subtask($task_ids);
			foreach ($task as $task_row) {
				$searchedValue = $task_row->task_ID;
				$task_title = "<i>Untitled Task</i>";
				if ($task_row->taskTitle != null) {
					if (strlen($task_row->taskTitle) > 20) {
						$task_title = ucwords(substr($task_row->taskTitle, 0, 20)) . "...";
					} else {
						$task_title = ucwords(strtolower($task_row->taskTitle));
					}
				}
				$col_arr[$col_count] = "<option class='font-weight-bold' type='task' value='task$task_row->task_ID' taskid='$task_row->task_ID' selected>" . $task_title . "</option>";
				$col_count++;
				$task_sub_task = array_filter(
					$sub_task,
					function ($e) use ($searchedValue) {
						return $e->task_ID == $searchedValue;
					}
				);
				if (count($task_sub_task) > 0) {
					foreach ($task_sub_task as $task_sub_task_row) {
						if (($task_sub_task_row->complabel == null) || ($task_sub_task_row->component_ID == 15) || ($task_sub_task_row->component_ID == 16) || ($task_sub_task_row->component_ID == 17) || ($task_sub_task_row->component_ID == 18)) {
							$col_label = $task_sub_task_row->fieldType;
						} else {
							if (strlen($task_sub_task_row->complabel) > 20) {
								$col_label = ucwords(substr($task_sub_task_row->complabel, 0, 20)) . "...";
							} else {
								$col_label = ucwords(strtolower($task_sub_task_row->complabel));
							}
						}
						$col_arr[$col_count] = "<option type='subtask' taskid='$task_row->task_ID' value='subtask$task_sub_task_row->subTask_ID'  data-content='&nbsp;&nbsp;&nbsp;&nbsp;" . ucwords($col_label) . "' selected></option>";
						$col_count++;
					}
				}
			}
		}
		return $col_arr;
	}

	public function post_select_init()
	{
		$data['assignees_stat'] = 0;
		$data['statuses_stat'] = 0;
		$data['columns_stat'] = 0;
		$data['confirmation_stat'] = 0;
		$data['uid'] = $this->session->userdata('uid');;
		$assignees = $this->qry_select_assignees($this->input->post('processId'));
		$statuses = $this->qry_select_statuses($this->input->post('processId'));
		$columns = $this->select_columns($this->input->post('processId'));
		$data['proc_details'] = $this->qry_process_details($this->input->post('processId'));
		$permission_checklist = $this->checklist_id_forreport($this->input->post('processId'));
		$permission_checklist_ids = [];
		$permission_checklist_str = "";
		if(count($permission_checklist) > 0){
			foreach($permission_checklist as $permission_checklist_index => $permission_checklist_row){
				$permission_checklist_ids[$permission_checklist_index] = $permission_checklist_row->checklist_ID;
			}
			$permission_checklist_str = implode(", ", array_values($permission_checklist_ids));
			// var_dump($permission_checklist_str);
			$confirmation_status = $this->qry_confirmation_select($permission_checklist_str);
			// $confirmed_checklists = $this->qry_confirmation_checklist_ids($permission_checklist_str);
		}
		// var_dump($confirmed_checklists);
		// $this->qry_confirmation_records($checklist_ids);
		if (count($assignees) > 0) {
			$data['assignees_stat'] = 1;
			$data['assignees'] = $assignees;
		}
		if (count($statuses) > 0) {
			$data['statuses_stat'] = 1;
			$data['statuses'] = $statuses;
		}
		if (count($columns) > 0) {
			$data['columns_stat'] = 1;
			$data['columns'] = $columns;
		}
		if (count($confirmation_status) > 0) {
			$data['confirmation_status_stat'] = 1;
			$data['confirmation_status'] = $confirmation_status;
		}
		echo json_encode($data);
	}

	public function qry_updated_by_details($user_id)
	{
		$fields = "users.uid, users.emp_id, app.fname, app.mname, app.lname,";
		$where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = $user_id";
		$table = "tbl_applicant app, tbl_employee emp, tbl_user users";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}
	// READ end
	// VIEW start
	public function report($process_id = 0)
	{
		$process_details = $this->qry_process_details($process_id);
		if (count($process_details) > 0) {
			$update['update_stat'] = 0;
			$update['update_details'] = "";
			if ($process_details->updatedBy != null) {
				if ($process_details->updatedBy != $process_details->createdBy) {
					$update['update_stat'] = 1;
					$update['update_details'] = $this->qry_updated_by_details($process_details->updatedBy);
				}
			}
			$data = [
				'uri_segment' => $this->uri->segment_array(),
				'title' => 'Process Reports',
				'process_details' => $process_details,
				'update_details' => $update,
			];
			if ($this->check_access_privilege_report($process_id)) {
				$this->load_template_view('templates/process/report', $data);
			}
		} else {
			$this->error_404();
		}
	}
	// VIEW end
	// MIC2X CODES ENDS HERE

	//SORTABLE BY MIC

	public function move_specific_subtask($subtask_id, $task_id, $move_to)
	{
		$query = "UPDATE tbl_processST_subtask SET sequence = $move_to WHERE task_ID = $task_id AND subTask_ID = $subtask_id AND isRemoved !=1";
		return $record = $this->general_model->custom_query_no_return($query);
	}
	public function move_order_as_first($current, $task_id)
	{
		$query = "UPDATE tbl_processST_subtask SET sequence = sequence + 1 WHERE task_ID = $task_id AND sequence >= 1 AND sequence < $current AND isRemoved !=1";
		return $record = $this->general_model->custom_query_no_return($query);
	}
	public function move_order_as_last($current, $prev, $task_id)
	{
		$query = "UPDATE tbl_processST_subtask SET sequence = sequence - 1 WHERE task_ID = $task_id AND sequence > $current AND sequence <= $prev AND isRemoved !=1";
		return $record = $this->general_model->custom_query_no_return($query);
	}
	public function increment_sequence_order($next, $current, $task_id)
	{
		$query = "UPDATE tbl_processST_subtask SET sequence = sequence + 1 WHERE task_ID = $task_id AND sequence >= $next AND sequence < $current AND isRemoved !=1";
		return $record = $this->general_model->custom_query_no_return($query);
	}
	public function decrement_sequence_order($prev, $current, $task_id)
	{
		$query = "UPDATE tbl_processST_subtask SET sequence = sequence - 1 WHERE task_ID = $task_id AND sequence > $current AND sequence <= $prev AND isRemoved !=1";
		return $record = $this->general_model->custom_query_no_return($query);
	}
	public function change_order_after_remove($task_id, $deleted_sequence){
		$query = "UPDATE tbl_processST_subtask SET sequence = sequence - 1 WHERE task_ID = $task_id AND sequence > $deleted_sequence AND isRemoved !=1";
		return $record = $this->general_model->custom_query_no_return($query);
	}

	public function change_order_after_remove_task($process_id, $deleted_sequence){
		$query = "UPDATE tbl_processST_task SET sequence = sequence - 1 WHERE process_ID = $process_id AND sequence > $deleted_sequence AND isRemoved !=1";
		return $record = $this->general_model->custom_query_no_return($query);
	}
	public function update_subtask_sequence()
	{
		$task_id = $this->input->post('taskId');
		$subtask_id = $this->input->post('subtask_id');
		$prev = $this->input->post('prevOrder');// the subtask ahead of the moving subtask
		$current = $this->input->post('currentOrder');// the moving subtask
		$next = $this->input->post('nextOrder'); // the subtask after the moving subtask
		$data['other_subtask_order'] = 0;
		$data['specific_subtask_order'] = 0;
		if ($prev == 0 && $next != 0) {
			// move as first in the order
			// increment all orders less than to current
			$data['other_subtask_order'] = $this->move_order_as_first($current, $task_id);
			$data['specific_subtask_order'] = $this->move_specific_subtask($subtask_id, $task_id, 1);
		} else if ($prev != 0 && $next == 0) {
			$data['other_subtask_order'] = $this->move_order_as_last($current, $prev, $task_id);
			$data['specific_subtask_order'] = $this->move_specific_subtask($subtask_id, $task_id, $prev);
			// decrement order greater than current and less than or equal to $prev
			// change order to $prev plus 
		} else if ($prev == 0 && $next == 0) {
			$data['other_subtask_order'] = 0;
			$data['specific_subtask_order'] = 0;
		} else {
			if (($prev < $current) && ($next < $current)) {
				// change to $next
				// increment $next orders and less than to $current
				$data['other_subtask_order'] = $this->increment_sequence_order($next, $current, $task_id);
				$data['specific_subtask_order'] = $this->move_specific_subtask($subtask_id, $task_id, $next);
			} else if (($prev > $current) && ($next > $current)) {
				// change to prev
				// decrement prev orders and greater than to $current
				$data['other_subtask_order'] = $this->decrement_sequence_order($prev, $current, $task_id);
				$data['specific_subtask_order'] = $this->move_specific_subtask($subtask_id, $task_id, $prev);
			}
		}
		echo json_encode($data);
	}

	//END SORTABLE
	private function addToArray_folder($object, $folder_ids)
	{
		if ($folder_ids == NULL) {
			$folder_ids = [];
		}
		foreach ($object as $raw) {
			array_push($folder_ids, $raw->folder_ID);
		}
		return $folder_ids;
	}

	private function addToArray_process($object, $process_ids)
	{
		if ($process_ids == NULL) {
			$process_ids = [];
		}
		foreach ($object as $raw) {
			array_push($process_ids, $raw->process_ID);
		}
		return $process_ids;
	}

	private function addToArray_checklist($object, $checklist_ids)
	{
		if ($checklist_ids == NULL) {
			$checklist_ids = [];
		}
		foreach ($object as $raw) {
			array_push($checklist_ids, $raw->checklist_ID);
		}
		return $checklist_ids;
	}

	// DATATABLE
	// 
	public function list_assignee_datatable()
	{
		$datatable = $this->input->post('datatable');
		$fpct_id = $datatable['query']['folProcId'];
		$type = $datatable['query']['type'];

		if ($datatable['query']['assigneeSearch'] != '') {
			$query['query'] = "SELECT assignment.*,assr.*, app.fname, app.lname, acc.acc_name FROM tbl_processST_assignment assignment, tbl_processST_assignmentRule assr, tbl_employee emp, tbl_applicant app, tbl_user users, tbl_account acc WHERE acc.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = assignment.assignedTo AND assignment.folProCheTask_ID=$fpct_id AND assignment.type='$type' AND assr.assignmentRule_ID=assignment.assignmentRule_ID";
			$keyword = $datatable['query']['assigneeSearch'];
			$query['search']['append'] = " AND (app.fname LIKE '%" . $keyword . "%' OR app.mname LIKE '%" . $keyword . "%' OR app.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname ,app.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname, app.mname ,app.lname) LIKE '%" . $keyword . "%')";
			$query['search']['total'] = " AND (app.fname LIKE '%" . $keyword . "%' OR app.mname LIKE '%" . $keyword . "%' OR app.lname LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname ,app.lname) LIKE '%" . $keyword . "%' OR concat_ws(' ',app.fname, app.mname ,app.lname) LIKE '%" . $keyword . "%')";
		} else {
			$query['query'] = "SELECT assignment.*,assr.*, app.fname, app.lname, acc.acc_name FROM tbl_processST_assignment assignment, tbl_processST_assignmentRule assr, tbl_employee emp, tbl_applicant app, tbl_user users, tbl_account acc WHERE acc.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = assignment.assignedTo AND assignment.folProCheTask_ID=$fpct_id AND assignment.type='$type' AND assr.assignmentRule_ID=assignment.assignmentRule_ID";
		}
		$data = $this->set_datatable_query($datatable, $query);
		echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
	}

	// REVISED CODES BY CHACHA
	public function get_allowedfolders()
	{
		$userid = $_SESSION["uid"];
		// if user ID is admin
		$admin = "SELECT * FROM tbl_processST_admin_access WHERE user_ID=$userid";
		$a_admin =  $this->general_model->custom_query($admin);
		if ($a_admin != null) {
			$fol = "SELECT folder_ID, folderName FROM `tbl_processST_folder` WHERE status_ID!=15 AND isRemoved=0 AND referencefolder=1";
			$a_folders =  $this->general_model->custom_query($fol);
		} else {
			// if userid is not admin				
			$fol = "SELECT folder_ID, folderName FROM `tbl_processST_folder` WHERE referencefolder=1 AND status_ID!=15 AND isRemoved=0 AND createdBy=$userid UNION SELECT fol.folder_ID, fol.folderName FROM tbl_processST_folder as fol, tbl_processST_assignment as ass WHERE fol.referencefolder=1 AND fol.folder_ID=ass.folProCheTask_ID AND fol.status_ID!=15 AND fol.isRemoved=0 AND ass.assignedTo=$userid AND (ass.assignmentRule_ID=1 OR ass.assignmentRule_ID=3)";
			$a_folders =  $this->general_model->custom_query($fol);
		}
		echo json_encode($a_folders);
	}

	public function task_change_checkbox()
	{
		$checklistStat_ID = $_POST['checklistStat_ID'];
		$q_stat2 = $this->Checktask_status_query($checklistStat_ID);
		$check_stat = array_shift($q_stat2);
		$completed =  $check_stat->{'isCompleted'};


		if ($completed == 2) {
			$result['check_status'] = "not checked";
			//  Update this to checked
			$update_val['isCompleted'] = 3;
		} else {
			$result['check_status'] = "checked";
			//  Update this to uncheck
			$update_val['isCompleted'] = 2;
		}
		$where = "checklistStatus_ID = $checklistStat_ID";
		$this->general_model->update_vals($update_val, $where, 'tbl_processST_checklistStatus');
		echo json_encode($result);
	}
	public function fetch_status_progressbar(){
		$checklist_ID = $_POST['checklist_id'];
		$result['all']=$this->gettaskstatus_count(0,$checklist_ID);
		$result['pending']=$this->gettaskstatus_count(2,$checklist_ID);
		$result['completed']=$this->gettaskstatus_count(3,$checklist_ID);
		echo json_encode($result);
	}
	public function Checktask_status()
	{
		$checklistStat_ID = $_POST['checklist_ID'];
		$q_stat2 = $this->Checktask_status_query($checklistStat_ID);
		$check_stat = array_shift($q_stat2);
		$completed =  $check_stat->{'isCompleted'};

		if ($completed == 2) {
			$result['check_status'] = "not checked";
		} else {
			$result['check_status'] = "checked";
		}
		echo json_encode($result);
	}
	public function Checktask_status_query($checklistStat_ID)
	{
		$q_stat = "SELECT isCompleted,task_ID,checklist_ID FROM `tbl_processST_checklistStatus` WHERE checklistStatus_ID=$checklistStat_ID";
		return $this->general_model->custom_query($q_stat);
	}
	public function Check_task_assignment_before_display($checklistID)
	{
		$userid = $_SESSION["uid"];
		$q_ass = "SELECT task.*,checkStatus.*,ass.assignmentRule_ID FROM tbl_processST_task task, tbl_processST_checklistStatus checkStatus,tbl_processST_assignment ass WHERE task.task_ID=checkStatus.task_ID AND checkStatus.checklist_ID=$checklistID AND checkStatus.checklistStatus_ID=ass.folProCheTask_ID AND ass.type='task' AND ass.assignedTo=$userid";
		return $this->general_model->custom_query($q_ass);
	}
	public function Checkprocess_templatepermission()
	{
		$userid = $_SESSION["uid"];
		$process_ID = $_POST['process_id'];
		$adminaccess = $this->check_if_userisadmin();
		$ot = "SELECT process_ID FROM `tbl_processST_process` WHERE createdBy=$userid AND process_ID=$process_ID";
		$ownedtemp = $this->general_model->custom_query($ot);
		$as = "SELECT p.process_ID FROM tbl_processST_process p, tbl_processST_assignment a WHERE p.process_ID=$process_ID AND a.folProCheTask_ID=$process_ID AND p.process_ID=a.folProCheTask_ID AND a.type='process' AND a.assignedTo=$userid AND (a.assignmentRule_ID=1 or a.assignmentRule_ID=2 or a.assignmentRule_ID=3 or a.assignmentRule_ID=4)";
		$assigned = $this->general_model->custom_query($as);
		if ($adminaccess != null) {
			$rt = 1;
		} else if ($ownedtemp != null) {
			$rt = 1;
		} else if ($assigned != null) {
			$rt = 1;
		} else {
			$rt = 0;
		}
		echo json_encode($rt);
	}
	public function checklist_completedchecker()
	{
		$checklist_ID = $_POST['check_ID'];
		$q_com = "SELECT COUNT(checklistStatus_ID) FROM `tbl_processST_checklistStatus` WHERE checklist_ID=$checklist_ID AND isCompleted=3 AND isRemoved=0";
		$completed = $this->general_model->custom_query($q_com);
		$check_comp = array_shift($completed);
		$numcom =  $check_comp->{'COUNT(checklistStatus_ID)'};

		$q_com2 = "SELECT COUNT(checklistStatus_ID) FROM `tbl_processST_checklistStatus` WHERE checklist_ID=$checklist_ID AND isRemoved=0";
		$all = $this->general_model->custom_query($q_com2);
		$check_all = array_shift($all);
		$numall =  $check_all->{'COUNT(checklistStatus_ID)'};
		$p = $this->general_model->fetch_specific_val("p.processType_ID,p.process_ID,c.checklist_ID", "c.checklist_ID=$checklist_ID and c.process_ID=p.process_ID", "tbl_processST_process p, tbl_processST_checklist c");
		$processtype = $p->processType_ID;

		if ($numall == $numcom) {
			$update_val['status_ID'] = 3;
			$update_val['completedBy'] = $_SESSION["uid"];
			if ($processtype == 2) {
				$this->coaching_log_notif_and_deadline($checklist_ID);
				$checker['completed'] = "3";
			} else {
				$checker['completed'] = "1";
			}
		} else {
			$checker['completed'] = "0";
			$update_val['status_ID'] = 2;
		}
		$where = "checklist_ID = $checklist_ID";
		$this->general_model->update_vals($update_val, $where, 'tbl_processST_checklist');
		$checker['processtype'] = $processtype;
		echo json_encode($checker);
	}
	public function checklist_completeall()
	{
		$checklist_ID = $_POST['checklist_ID'];
	}

	// NEW CODES FOR DISPLAYING PROCESS TEMPLATES

	public function processtemplate_display()
	{
		$limiter = $this->input->post('limiter');
		$perPage = $this->input->post('perPage');
		$activeSearch = $this->input->post('activeSearch');
		$inputSearch = $this->input->post('inputSearch');
		$templateType = $this->input->post('templateType');
		$userid = $_SESSION["uid"];
		$search = array();
		$searchString = "";
		if ($inputSearch !== '') {
			$search[] = "processTitle LIKE '%$inputSearch%'";
		}
		if ($activeSearch !== '') {
			$search[] = "processstatus_ID = '$activeSearch'";
		}
		if ($templateType !== '') {
			$search[] = "processType_ID = '$templateType'";
		}
		$admin_access = $this->check_if_userisadmin();
		if ($admin_access != null) {
			if (!empty($search)) {
				$searchString = "AND " . implode(" AND ", $search);
			}
			// $q_process = "SELECT DISTINCT process_ID,processTitle,processstatus_ID,dateTimeCreated,'admin' as assignmentRule_ID FROM tbl_processST_process WHERE isRemoved=0 AND processstatus_ID!=15 order by dateTimeCreated";
			$select = "SELECT DISTINCT process_ID,processTitle,processType_ID,processstatus_ID,dateTimeCreated,'admin' as assignmentRule_ID";
			$from = "FROM tbl_processST_process WHERE isRemoved=0 AND processstatus_ID!=15  $searchString order by dateTimeCreated DESC";
		} else {
			if (!empty($search)) {
				$searchString = "WHERE " . implode(" AND ", $search);
			}
			// $q_process = "SELECT * from (SELECT DISTINCT(p.process_ID),p.processTitle,p.dateTimeCreated,p.processstatus_ID,'owned' as assignmentRule_ID FROM tbl_processST_process p where p.createdBy=$userid AND p.isRemoved=0 AND p.processstatus_ID!=15 UNION SELECT DISTINCT(p.process_ID),p.processTitle,p.dateTimeCreated,p.processstatus_ID, a.assignmentRule_ID FROM tbl_processST_assignment a,tbl_processST_process p where a.assignedTo=$userid and a.type='process' and p.isRemoved=0 and p.processstatus_ID!=15 and a.folProCheTask_ID=p.process_ID) a order by dateTimeCreated DESC";
			$select = "SELECT *";
			$from = "from (SELECT DISTINCT(p.process_ID),p.processTitle,p.processType_ID,p.dateTimeCreated,p.processstatus_ID,'owned' as assignmentRule_ID FROM tbl_processST_process p where p.createdBy=$userid AND p.isRemoved=0 AND p.processstatus_ID!=15 UNION SELECT DISTINCT(p.process_ID),p.processTitle,p.processType_ID,p.dateTimeCreated,p.processstatus_ID, a.assignmentRule_ID FROM tbl_processST_assignment a,tbl_processST_process p where a.assignedTo=$userid and a.type='process' and p.isRemoved=0 and p.processstatus_ID!=15 and a.folProCheTask_ID=p.process_ID ) a  $searchString order by dateTimeCreated DESC";
		}
		$fetched_processtemp = $this->general_model->custom_query($select . " " . $from . " LIMIT $limiter,$perPage");
		// $process_details['processtemp'] = $fetched_processtemp;
		$loop = 0;
		$process_details = array();
		if (count($fetched_processtemp) > 0) {
			foreach ($fetched_processtemp as $processtemp) {
				$stat['all'] = $this->getchecklist_count(0, $processtemp->process_ID);
				$stat['completed'] = $this->getchecklist_count(3, $processtemp->process_ID);
				$stat['pending'] = $this->getchecklist_count(2, $processtemp->process_ID);
				$stat['active'] = $this->getchecklist_count(10, $processtemp->process_ID);
				$stat['overdue'] = $this->getchecklist_count(16, $processtemp->process_ID);
				$process_details[$loop] = [
					"processtemp" => $processtemp,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		// echo json_encode($process_details);

		$all = $this->general_model->custom_query("SELECT COUNT(*) as count " . $from);
		$count = $all[0]->count;
		echo json_encode(['process_details' => $process_details, 'total' => $count]);
	}
	public function checklists_display()
	{
		$limiter = $this->input->post('limiter');
		$perPage = $this->input->post('perPage');
		$activeSearch = $this->input->post('activeSearch');
		$inputSearch = $this->input->post('inputSearch');
		$access = $this->input->post('pageaccess');
		$templateType = $this->input->post('templateType');
		$userid = $_SESSION["uid"];
		$ttype = "";
		$search = array();
		$searchString = "";
		if ($inputSearch !== '') {
			$search[] = "checklistTitle LIKE '%$inputSearch%'";
		}
		if ($activeSearch !== '') {
			$search[] = "status_ID = '$activeSearch'";
		}
		if ($templateType !== null && $templateType !== '') {
			$search[] = "processType_ID = '$templateType'";
		}
		$admin_access = $this->check_if_userisadmin();
		if ($access == '1') {
			// MAIN PAGE - process view
			if ($admin_access != null) {
				if (!empty($search)) {
					$searchString = "AND " . implode(" AND ", $search);
				}
				// $q_checklist = "SELECT DISTINCT (c.checklist_ID),c.checklistTitle,c.dateTimeCreated,c.status_ID,p.processTitle,p.processstatus_ID,p.process_ID, 'admin' as assignmentRule_ID FROM tbl_processST_checklist c,tbl_processST_process p WHERE c.process_ID=p.process_ID AND c.isRemoved!=1 AND c.isArchived!=1 order by c.dateTimeCreated";
				$select = "SELECT DISTINCT (c.checklist_ID),c.remarks,c.dueDate,c.createdBy,c.checklistTitle,c.dateTimeCreated,c.status_ID,p.processTitle,p.processstatus_ID,p.process_ID,p.processType_ID,c.dueDate,'admin' as assignmentRule_ID";
				$from = "FROM tbl_processST_checklist c,tbl_processST_process p WHERE c.process_ID=p.process_ID AND c.isRemoved=0 AND p.processstatus_ID!=15 AND p.isRemoved=0 AND c.isArchived!=1 $searchString order by c.dateTimeCreated";

				$all = $this->general_model->custom_query("SELECT COUNT(*) as count " . $from);
			} else {
				if (!empty($search)) {
					$searchString = "WHERE " . implode(" AND ", $search);
				}
				// $q_checklist = "SELECT * from (SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'yes' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklistStatus b,tbl_processST_checklist c, tbl_processST_process d where assignedTo=$userid and type='task' and a.folProCheTask_ID=b.checklistStatus_ID and b.checklist_ID=c.checklist_ID and c.isRemoved=0 and c.isArchived!=1 and d.process_ID=c.process_ID UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where c.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID UNION SELECT DISTINCT(b.checklist_ID),checklistTitle,b.dateTimeCreated,b.status_ID, d.processTitle, d.process_ID, a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklist b, tbl_processST_process d where assignedTo=$userid and type='checklist' and a.folProCheTask_ID=b.checklist_ID and b.isRemoved=0 and b.isArchived!=1 and d.process_ID=b.process_ID UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and (a.assignmentRule_ID=2 or a.assignmentRule_ID=4 or a.assignmentRule_ID=7) UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and (a.assignmentRule_ID=1 or a.assignmentRule_ID=3 or a.assignmentRule_ID=8) ) a group by checklist_ID order by dateTimeCreated DESC";
				$select = "SELECT *";
				$from = "from (SELECT DISTINCT(c.checklist_ID),c.remarks,c.dueDate,c.createdBy,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,d.processType_ID,a.assignmentRule_ID, 'yes' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklistStatus b,tbl_processST_checklist c, tbl_processST_process d where assignedTo=$userid and type='task' and a.folProCheTask_ID=b.checklistStatus_ID and b.checklist_ID=c.checklist_ID and c.isRemoved=0 and c.isArchived!=1 and d.process_ID=c.process_ID UNION SELECT DISTINCT(c.checklist_ID),c.remarks,c.dueDate,c.createdBy,checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,d.processType_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where d.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID and d.processstatus_ID!=15 and d.isRemoved=0 UNION SELECT DISTINCT(c.checklist_ID),c.remarks,c.dueDate,c.createdBy,checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,d.processType_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where c.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID and d.processstatus_ID!=15 and d.isRemoved=0 UNION SELECT DISTINCT(b.checklist_ID),b.remarks,b.dueDate,b.createdBy,checklistTitle,b.dateTimeCreated,b.status_ID, d.processTitle, d.process_ID,d.processType_ID, a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklist b, tbl_processST_process d where assignedTo=$userid and type='checklist' and a.folProCheTask_ID=b.checklist_ID and b.isRemoved=0 and b.isArchived!=1 and d.process_ID=b.process_ID UNION SELECT DISTINCT(c.checklist_ID),c.remarks,c.dueDate,c.createdBy,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,d.processType_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and (a.assignmentRule_ID=2 or a.assignmentRule_ID=4 or a.assignmentRule_ID=7) UNION SELECT DISTINCT(c.checklist_ID),c.remarks,c.dueDate,c.createdBy,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,d.processType_ID,a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and (a.assignmentRule_ID=1 or a.assignmentRule_ID=3 or a.assignmentRule_ID=8) ) a $searchString group by checklist_ID order by dateTimeCreated DESC";
				$all = $this->general_model->custom_query("SELECT COUNT(*) as count FROM ($select $from) zz");
			}
		} else {
			//Checklist Pending
			$processid = $this->input->post('process_id');
			$tt = "SELECT processType_ID FROM tbl_processST_process WHERE process_ID=$processid";
			$ttype = $this->general_model->custom_query($tt);

			if ($admin_access != null) {
				if (!empty($search)) {
					$searchString = "AND " . implode(" AND ", $search);
				}
				// $q_checklist = "SELECT DISTINCT (c.checklist_ID),c.checklistTitle,c.dateTimeCreated,c.status_ID,p.processTitle,p.processstatus_ID,p.process_ID, 'admin' as assignmentRule_ID FROM tbl_processST_checklist c,tbl_processST_process p WHERE c.process_ID=$processid and p.process_ID=$processid and c.process_ID=p.process_ID AND c.isRemoved!=1 AND c.isArchived!=1 order by c.dateTimeCreated";
				$select = "SELECT DISTINCT (c.checklist_ID),c.createdBy,c.remarks,c.dueDate,c.checklistTitle,c.dateTimeCreated,c.status_ID,p.processTitle,p.processstatus_ID,p.process_ID,p.processType_ID, 'admin' as assignmentRule_ID ";
				$from = "FROM tbl_processST_checklist c,tbl_processST_process p WHERE c.process_ID=$processid and p.process_ID=$processid and c.process_ID=p.process_ID AND c.isRemoved!=1 AND p.processstatus_ID!=15 AND p.isRemoved=0 AND c.isArchived!=1 $searchString order by c.dateTimeCreated";

				$all = $this->general_model->custom_query("SELECT COUNT(*) as count " . $from);
			} else {
				if (!empty($search)) {
					$searchString = "WHERE " . implode(" AND ", $search);
				}
				// $q_checklist = "SELECT * from (SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'yes' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklistStatus b,tbl_processST_checklist c, tbl_processST_process d where assignedTo=$userid and type='task' and a.folProCheTask_ID=b.checklistStatus_ID and b.checklist_ID=c.checklist_ID and c.isRemoved=0 and c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where c.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(b.checklist_ID),checklistTitle,b.dateTimeCreated,b.status_ID, d.processTitle, d.process_ID, a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklist b, tbl_processST_process d where assignedTo=$userid and type='checklist' and a.folProCheTask_ID=b.checklist_ID and b.isRemoved=0 and b.isArchived!=1 and d.process_ID=b.process_ID and d.process_ID=$processid and b.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and (a.assignmentRule_ID=2 or a.assignmentRule_ID=4 or a.assignmentRule_ID=7) and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid and (a.assignmentRule_ID=1 or a.assignmentRule_ID=3 or a.assignmentRule_ID=8) ) a group by checklist_ID order by dateTimeCreated DESC";
				$select = "SELECT *";
				$from = " from (SELECT DISTINCT(c.checklist_ID),c.createdBy,c.remarks,c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,d.processType_ID,a.assignmentRule_ID, 'yes' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklistStatus b,tbl_processST_checklist c, tbl_processST_process d where assignedTo=$userid and type='task' and a.folProCheTask_ID=b.checklistStatus_ID and b.checklist_ID=c.checklist_ID and c.isRemoved=0 and c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID),c.createdBy,c.remarks,c.dueDate,checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,d.processType_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where d.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid and d.processstatus_ID!=15 and d.isRemoved=0 UNION SELECT DISTINCT(c.checklist_ID),c.createdBy,c.remarks,c.dueDate,checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,d.processType_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where c.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid and d.processstatus_ID!=15 and d.isRemoved=0 UNION SELECT DISTINCT(b.checklist_ID),b.createdBy,b.remarks,b.dueDate,checklistTitle,b.dateTimeCreated,b.status_ID, d.processTitle, d.process_ID,d.processType_ID, a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklist b, tbl_processST_process d where assignedTo=$userid and type='checklist' and a.folProCheTask_ID=b.checklist_ID and b.isRemoved=0 and b.isArchived!=1 and d.process_ID=b.process_ID and d.process_ID=$processid and b.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID),c.createdBy,c.remarks,c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,d.processType_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and (a.assignmentRule_ID=2 or a.assignmentRule_ID=4 or a.assignmentRule_ID=7) and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID),c.createdBy,c.remarks,c.dueDate,checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,d.processType_ID,a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid and (a.assignmentRule_ID=1 or a.assignmentRule_ID=3 or a.assignmentRule_ID=8) ) a $searchString group by checklist_ID order by dateTimeCreated DESC";
				$all = $this->general_model->custom_query("SELECT COUNT(*) as count FROM ($select $from) zz");
			}
		}
		$c = $this->general_model->custom_query($select . " " . $from . " LIMIT $limiter,$perPage");
		$loop = 0;
		$checklist_details = array();
		if (count($c) > 0) {
			foreach ($c as $checklist) {
				$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
				$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
				$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);
				$stat['names'] = $this->getdetails_createdby($checklist->createdBy);
				$checklist_details[$loop] = [
					"checklist" => $checklist,
					"statuses" => $stat
				];
				$loop++;
			}
		}
		$count = $all[0]->count;
		echo json_encode(['checklist_details' => $checklist_details, 'total' => $count, 'process_type' => $ttype]);
	}
	// public function checklists_display_2()
	// {
	// 	$userid = $_SESSION["uid"];
	// 	$access = $this->input->post('pageaccess');
	// 	$admin_access = $this->check_if_userisadmin();

	// 	if ($access == 1) {
	// 		// MAIN PAGE - process view
	// 		if ($admin_access != null) {
	// 			$q_checklist = "SELECT DISTINCT (c.checklist_ID),c.checklistTitle,c.dateTimeCreated,c.status_ID,p.processTitle,p.processstatus_ID,p.process_ID, 'admin' as assignmentRule_ID FROM tbl_processST_checklist c,tbl_processST_process p WHERE c.process_ID=p.process_ID AND c.isRemoved!=1 AND c.isArchived!=1 order by c.dateTimeCreated";
	// 		} else {
	// 			$q_checklist = "SELECT * from (SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'yes' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklistStatus b,tbl_processST_checklist c, tbl_processST_process d where assignedTo=$userid and type='task' and a.folProCheTask_ID=b.checklistStatus_ID and b.checklist_ID=c.checklist_ID and c.isRemoved=0 and c.isArchived!=1 and d.process_ID=c.process_ID UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where c.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID UNION SELECT DISTINCT(b.checklist_ID),checklistTitle,b.dateTimeCreated,b.status_ID, d.processTitle, d.process_ID, a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklist b, tbl_processST_process d where assignedTo=$userid and type='checklist' and a.folProCheTask_ID=b.checklist_ID and b.isRemoved=0 and b.isArchived!=1 and d.process_ID=b.process_ID UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and (a.assignmentRule_ID=2 or a.assignmentRule_ID=4 or a.assignmentRule_ID=7) UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and (a.assignmentRule_ID=1 or a.assignmentRule_ID=3 or a.assignmentRule_ID=8) ) a group by checklist_ID order by dateTimeCreated DESC";
	// 		}
	// 	} else {
	// 		//Checklist Pending
	// 		$processid = $this->input->post('process_id');
	// 		if ($admin_access != null) {
	// 			$q_checklist = "SELECT DISTINCT (c.checklist_ID),c.checklistTitle,c.dateTimeCreated,c.status_ID,p.processTitle,p.processstatus_ID,p.process_ID, 'admin' as assignmentRule_ID FROM tbl_processST_checklist c,tbl_processST_process p WHERE c.process_ID=$processid and p.process_ID=$processid and c.process_ID=p.process_ID AND c.isRemoved!=1 AND c.isArchived!=1 order by c.dateTimeCreated";
	// 		} else {
	// 			$q_checklist = "SELECT * from (SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'yes' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklistStatus b,tbl_processST_checklist c, tbl_processST_process d where assignedTo=$userid and type='task' and a.folProCheTask_ID=b.checklistStatus_ID and b.checklist_ID=c.checklist_ID and c.isRemoved=0 and c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated, c.status_ID, d.processTitle, d.process_ID,'owned' as assignmentRule_ID, 'no' as task_permission FROM tbl_processST_checklist c,tbl_processST_process d where c.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(b.checklist_ID),checklistTitle,b.dateTimeCreated,b.status_ID, d.processTitle, d.process_ID, a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_checklist b, tbl_processST_process d where assignedTo=$userid and type='checklist' and a.folProCheTask_ID=b.checklist_ID and b.isRemoved=0 and b.isArchived!=1 and d.process_ID=b.process_ID and d.process_ID=$processid and b.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID,'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and (a.assignmentRule_ID=2 or a.assignmentRule_ID=4 or a.assignmentRule_ID=7) and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID),checklistTitle,c.dateTimeCreated,c.status_ID, d.processTitle, d.process_ID,a.assignmentRule_ID, 'no' as task_permission FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid and (a.assignmentRule_ID=1 or a.assignmentRule_ID=3 or a.assignmentRule_ID=8) ) a group by checklist_ID order by dateTimeCreated DESC";
	// 		}
	// 	}
	// 	$c = $this->general_model->custom_query($q_checklist);
	// 	$loop = 0;
	// 	if (count($c) > 0) {
	// 		foreach ($c as $checklist) {
	// 			$stat['all'] = $this->gettaskstatus_count(0, $checklist->checklist_ID);
	// 			$stat['pending'] = $this->gettaskstatus_count(2, $checklist->checklist_ID);
	// 			$stat['completed'] = $this->gettaskstatus_count(3, $checklist->checklist_ID);

	// 			$checklist_details[$loop] = [
	// 				"checklist" => $checklist,
	// 				"statuses" => $stat
	// 			];
	// 			$loop++;
	// 		}
	// 	}
	// 	echo json_encode(['checklist_details' => $checklist_details]);
	// }

	public function fetch_processtype()
	{
		$q = "SELECT * FROM tbl_processST_processType WHERE isActive=1";
		$data = $this->general_model->custom_query($q);
		echo json_encode($data);
	}

	public function checklist_id_forreport($process_id){
		$userid = $_SESSION["uid"];
		$processid = $process_id;
		$admin_access = $this->check_if_userisadmin();
		if ($admin_access != null) {
			$select = "SELECT DISTINCT (c.checklist_ID)";
			$from = "FROM tbl_processST_checklist c,tbl_processST_process p WHERE c.process_ID=$processid and p.process_ID=$processid and c.process_ID=p.process_ID AND c.isRemoved!=1 AND p.processstatus_ID!=15 AND p.isRemoved=0 AND c.isArchived!=1 order by c.dateTimeCreated";
		}else{
			$select = "SELECT *";
			$from = " from (SELECT DISTINCT(c.checklist_ID) FROM tbl_processST_assignment a,tbl_processST_checklistStatus b,tbl_processST_checklist c, tbl_processST_process d where assignedTo=$userid and type='task' and a.folProCheTask_ID=b.checklistStatus_ID and b.checklist_ID=c.checklist_ID and c.isRemoved=0 and c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID) FROM tbl_processST_checklist c,tbl_processST_process d where d.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid and d.processstatus_ID!=15 and d.isRemoved=0 UNION SELECT DISTINCT(c.checklist_ID) FROM tbl_processST_checklist c,tbl_processST_process d where c.createdBy=$userid AND c.isRemoved=0 AND c.isArchived!=1 and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid and d.processstatus_ID!=15 and d.isRemoved=0 UNION SELECT DISTINCT(b.checklist_ID) FROM tbl_processST_assignment a,tbl_processST_checklist b, tbl_processST_process d where assignedTo=$userid and type='checklist' and a.folProCheTask_ID=b.checklist_ID and b.isRemoved=0 and b.isArchived!=1 and d.process_ID=b.process_ID and d.process_ID=$processid and b.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID) FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and c.createdBy=$userid and (a.assignmentRule_ID=2 or a.assignmentRule_ID=4 or a.assignmentRule_ID=7) and d.process_ID=$processid and c.process_ID=$processid UNION SELECT DISTINCT(c.checklist_ID) FROM tbl_processST_assignment a,tbl_processST_process d,tbl_processST_checklist c where assignedTo=$userid and type='process' and d.process_ID=c.process_ID and c.isRemoved=0 and c.isArchived!=1 and a.folProCheTask_ID=d.process_ID and d.process_ID=c.process_ID and d.process_ID=$processid and c.process_ID=$processid and (a.assignmentRule_ID=1 or a.assignmentRule_ID=3 or a.assignmentRule_ID=8) ) a group by checklist_ID DESC";
		}
		return $c = $this->general_model->custom_query($select . " " . $from);
		// echo json_encode($c);
	}
	//END REVISED CODES BY CHACHA

	//NICCA CHANGES -----------------------------------------
	public function fetch_allemployeedetails()
	{
		$membersIds = $this->input->post('membersIds');

		if ($membersIds == "" || empty($membersIds)) {
			$employees = array();
		} else {
			$employees = $this->general_model->fetch_specific_vals("app.apid,app.fname,app.mname,app.lname,user.uid,emp.emp_id,acc.acc_name", "acc.acc_id=emp.acc_id AND user.uid IN (" . implode(',', $membersIds) . ") AND app.apid=emp.apid AND emp.isActive='yes' AND user.emp_id=emp.emp_id", "tbl_account acc,tbl_applicant app, tbl_employee emp, tbl_user user");
		}
		echo json_encode(['employees' => $employees]);
	}
	public function fetch_oneemployeedetails()
	{
		$membersIds = $this->input->post('membersIds');
		$employees = $this->general_model->fetch_specific_vals("app.apid,app.fname,app.mname,app.lname,user.uid,emp.emp_id,acc.acc_name", "acc.acc_id=emp.acc_id AND user.uid=$membersIds AND app.apid=emp.apid AND emp.isActive='yes' AND user.emp_id=emp.emp_id", "tbl_account acc,tbl_applicant app, tbl_employee emp, tbl_user user");
		echo json_encode(['employees' => $employees]);
	}
	public function fetch_account()
	{
		$q = "SELECT acc_id,acc_name,acc_description FROM tbl_account";
		$rs = $this->general_model->custom_query($q);
		echo json_encode($rs);
	}

	public function fetch_membersPerAccount()
	{
		$emp_id = $this->session->userdata('emp_id');
		$accid = $this->input->post('acc_id');
		if($accid=="0"){
			$members = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,user.uid", "emp.emp_id!=$emp_id AND emp.apid=ap.apid AND emp.isActive='yes' AND user.emp_id=emp.emp_id", "tbl_applicant ap, tbl_employee emp,tbl_user user", "ap.lname ASC");
		}else{
			$members = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description,user.uid", "emp.acc_id=$accid  AND emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes' AND user.emp_id=emp.emp_id AND emp.emp_id!=$emp_id", "tbl_applicant ap, tbl_employee emp,tbl_account acc, tbl_user user", "ap.lname ASC");	
		}
		echo json_encode(['members' => $members]);
	}
	public function fetch_membersPerAccount_2nd()
	{
		$emp_id = $this->session->userdata('emp_id');
		$accid = $this->input->post('acc_id');
		$ass_id = $this->input->post('assigned_pct');
		$ass_type = $this->input->post('ass_type');
		// $members = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description,user.uid", "emp.acc_id=$accid  AND emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes' AND user.emp_id=emp.emp_id AND emp.emp_id!=$emp_id", "tbl_applicant ap, tbl_employee emp,tbl_account acc, tbl_user user", "ap.lname ASC");
		// $members = $this->general_model->fetch_specific_vals("a.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description,user.uid","ass.type='$ass_type' AND user.uid NOT IN (ass.assignedTo) AND ass.folProCheTask_ID=$ass_id AND emp.acc_id=$accid  AND emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes' AND user.emp_id=emp.emp_id AND emp.emp_id!=$emp_id","tbl_applicant ap, tbl_employee emp,tbl_account acc, tbl_user user, tbl_processST_assignment ass","ap.lname ASC");		
		$members = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description,user.uid", "user.uid NOT IN (SELECT assignedTo FROM tbl_processST_assignment WHERE folProCheTask_ID=$ass_id AND type='$ass_type') AND emp.acc_id=$accid  AND emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes' AND user.emp_id=emp.emp_id AND emp.emp_id!=$emp_id", "tbl_applicant ap, tbl_employee emp,tbl_account acc, tbl_user user", "ap.lname ASC");
		echo json_encode(['members' => $members]);
	}
	public function fetch_empsnotassigned()
	{
		$emp_id = $this->session->userdata('emp_id');
		$ass_id = $this->input->post('assigned_pct');
		$ass_type = $this->input->post('ass_type');
		$members = $this->general_model->fetch_specific_vals("ap.fname,ap.mname,ap.lname,ap.apid,emp.emp_id,acc.acc_id,acc.acc_name,acc.acc_description,user.uid", "user.uid NOT IN (SELECT assignedTo FROM tbl_processST_assignment WHERE folProCheTask_ID=$ass_id AND type='$ass_type') AND emp.apid=ap.apid AND emp.acc_id=acc.acc_id AND emp.isActive='yes' AND user.emp_id=emp.emp_id AND emp.emp_id!=$emp_id", "tbl_applicant ap, tbl_employee emp,tbl_account acc, tbl_user user", "ap.lname ASC");
		echo json_encode(['members' => $members]);
	}
	// TRANSFER TO PROCESS -------------------------------- 2019-10-28 
	public function add_anotherchecklist_withassignees()
	{
		$userid = $this->session->userdata('uid');
		$processid = $this->input->post('process_ID');
		$checkListId = $this->input->post('checkListId');
		$checkTitle = $this->input->post('checklistTitle');
		$data['process_ID'] = $processid;
		$data['checklistTitle'] = $checkTitle;
		$data['status_ID'] = '2';
		$data['isArchived'] = '0';
		$data['createdBy'] = $userid;
		$data['updatedBy'] = $userid;
		$cid = $this->general_model->insert_vals_last_inserted_id($data, "tbl_processST_checklist");
		// $this->process_model->add_anotherchecklist($data);
		$alltask = $this->process_model->get_task($processid);
		$arTask = [];
		$taskNum = 0;
		foreach ($alltask as $alltaskrow) {
			$arTask[$taskNum] = [
				"checklist_ID" => $cid,
				"task_ID" => $alltaskrow->task_ID,
				"isRemoved" => $alltaskrow->isRemoved,
				"isCompleted" => 2,
			];
			$taskNum++;
		}
		$this->process_model->addbatch_checkstatus($arTask, 'tbl_processST_checklistStatus');

		$assignments = $this->general_model->custom_query("SELECT assignmentRule_ID,assignedTo FROM tbl_processST_assignment WHERE type='checklist' AND folProCheTask_ID = $checkListId");
		$ass_data = [];
		foreach ($assignments as $ass) {
			$ass_data[] = ['type' => 'checklist', 'assignmentRule_ID' => $ass->assignmentRule_ID, 'assignedBy' => $userid, 'assignedTo' => $ass->assignedTo, 'folProCheTask_ID' => $cid];
		}
		if (!empty($ass_data)) {
			$this->process_model->batch_insert($ass_data, "tbl_processST_assignment");
		}

		echo json_encode($data);
	}

	// Michael's code

	public function coaching_log($checklist_id = null)
	{
		$data = [
			'uri_segment' => $this->uri->segment_array(),
			'title' => 'Coaching Log',
		];
		$session_uid = $this->session->userdata('uid');
		$checklist = $this->get_checklist_details($checklist_id);
		// $p_o = "SELECT process_ID FROM `tbl_processST_process` WHERE createdBy=$session_uid AND isRemoved=0 AND processstatus_ID!=15 AND process_ID=$checklist->process_ID";
		// $p_owned =  $this->general_model->custom_query($p_o);
		
		if (count($checklist) > 0) {
			$admin_access = $this->check_if_userisadmin();
			if ($checklist->createdBy == $session_uid || $admin_access != null) {
				if ($checklist->status_ID == 2) {
					$this->checklist_answerable($checklist->process_ID, $checklist_id);
				} else {
					// $this->coaching_log_access();
					$this->load_template_view('templates/process/coaching_log', $data);
				}
			} else {
				$subordinate_access = $this->check_coaching_log_acces($checklist_id, $checklist->process_ID,  $session_uid, $checklist->createdBy);
				// if (count($subordinate_access) > 0 || $p_owned !=null) {
				if (count($subordinate_access) > 0) {
					// $this->coaching_log_access();
					$this->load_template_view('templates/process/coaching_log', $data);
				} else {
					$this->error_403();
					// var_dump($subordinate_access);
				}
			}
		} else {
			$this->error_404();
		}
	}
	protected function check_coaching_log_acces($checklist_id, $process_id, $subordinate_uid, $sup_uid)
	{
		$fields = "*";
		$where = "type IN ('checklist', 'process') AND folProCheTask_ID IN ($checklist_id, $process_id) AND assignedTo = $subordinate_uid";
		// $where = "assignmentRule_ID = 9 AND type IN ('checklist', 'process') AND folProCheTask_ID = $checklist_id AND assignedTo = $subordinate_uid AND assignedBy = $sup_uid";
		// echo $where;
		return $this->general_model->fetch_specific_val($fields, $where, 'tbl_processST_assignment');
	}
	private function check_if_coaching_log_deadline_exist($checklist_id,  $user_id)
	{
		$fields = "*";
		$where = "module_ID = 5 AND request_ID = $checklist_id AND requestor = $user_id";
		return $this->general_model->fetch_specific_val($fields, $where, 'tbl_deadline');
	}
	public function coaching_log_notif_and_deadline($checklist_id)
	{
		$checklist = $this->get_checklist_details($checklist_id);
		if (count($checklist) > 0) {
			$deadline_exist = $this->check_if_coaching_log_deadline_exist($checklist_id,  $checklist->createdBy);
			if (count($deadline_exist) < 1) {
				$dateTime = $this->get_current_date_time();
				$deadline_date = Date("Y-m-d", strtotime($dateTime['date'] . ' 2 days'));
				$deadline_date_time = $deadline_date . " 23:59:59";
				$follow = $deadline_date . " 00:00:00";
				$pro_noun = "";
				$emp_details = $this->get_emp_details_via_user_id($checklist->createdBy);
				$assignment = $this->check_coaching_log_assignment($checklist_id, $checklist->createdBy);
				if (count($assignment) > 0) {
					if (trim($emp_details->gender) === "Male") {
						$pro_noun = "his";
					} else if (trim($emp_details->gender) === "Female") {
						$pro_noun = "her";
					}
					$notif_mssg = "needs your confirmation on " . $pro_noun . " coaching logs. <br><small><b>CL-ID</b>:" . $checklist_id . "</b></small>";
					$link = "discipline_mic/coaching_log/" . $checklist_id;
					$data['notif_id'] = $this->simple_notification($notif_mssg, $link, $assignment->assignedTo, $checklist->createdBy);

					$confirmation_deadline['module_ID'] = 5;
					$confirmation_deadline['userId'] = $assignment->assignedTo;
					$confirmation_deadline['request_ID'] = $checklist->checklist_ID;
					$confirmation_deadline['requestor'] = $checklist->createdBy;
					$confirmation_deadline['approvalLevel'] = 1;
					$confirmation_deadline['deadline'] = $deadline_date_time;
					$confirmation_deadline['status_ID'] = 2;
					$confirmation_deadline['follow'] = $follow;
					$this->set_deadline_coaching_log($confirmation_deadline);
				}
			}
		}
	}
	private function set_deadline_coaching_log($deadline)
	{
		return $this->general_model->insert_vals($deadline, "tbl_deadline");
	}
	private function log_confirmation($confirm)
	{
		return $this->general_model->insert_vals($confirm, "tbl_processST_checklist_confirmation");
	}
	protected function check_coaching_log_assignment($checklist_id, $sup_uid)
	{
		$fields = "*";
		$where = "assignmentRule_ID = 9 AND type='checklist' AND folProCheTask_ID = $checklist_id AND assignedBy = $sup_uid";
		return $this->general_model->fetch_specific_val($fields, $where, 'tbl_processST_assignment');
	}
	public function getdetails_createdby($userid)
	{
		return $this->general_model->fetch_specific_val("emp.emp_id,user.uid,ap.fname,ap.lname,ap.apid", "user.uid=$userid AND user.emp_id=emp.emp_id AND emp.apid=ap.apid", "tbl_employee emp, tbl_user user, tbl_applicant ap");
	}
	
	//End michael's code

	//NEW CODE FOR Irecruit by Charise
	public function copy_thislink(){
		$pid = $this->input->post("process_id");
		$templink = site_url('process/checklist_pending/').$pid;
		echo json_encode($templink);
	}

	//michael's code for update subtask sequencing
	public function get_max_subtask_sequence($task_id){
		$qry = "SELECT MAX(sequence) max_sequence FROM tbl_processST_subtask WHERE task_ID = $task_id AND isRemoved != 1";
		return $this->general_model->custom_query($qry);
	}

	public function get_subtask_details(){
		$data['exist'] = 0;
		$subtask_details = $this->qry_subtask_details($this->input->post('sub_task_id'));
		$max_seq = $this->get_max_subtask_sequence($this->input->post('task_id'));
		// var_dump($max_seq);
		if(count($subtask_details) > 0){
			$data['exist'] = 1;
			$data['record'] = $subtask_details;
			$data['max_sequence'] = $max_seq[0]->max_sequence;
		}
		echo json_encode($data);
		// var_dump($subtask_details);
	}
	protected function get_former_subtask($task_id, $sequence_num)
    {
        $fields = "subTask_ID, task_ID, component_ID, complabel, sequence, isRemoved";
        $where = "task_ID = $task_id AND sequence = $sequence_num AND isRemoved != 1";
        $table = "tbl_processST_subtask";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
	}
	private function get_subtask_qry($subtask_id)
    {
        $fields = "subTask_ID, task_ID, component_ID, complabel, sequence, isRemoved";
        $where = "subTask_ID = $subtask_id";
        $table = "tbl_processST_subtask";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }
    public function reorder_subtask_sequence(){
		$subtask_update['update'] = 0;
		$task_order = [];
        $subtask_id = $this->input->post('sub_task_id');
        $task_id = $this->input->post('task_id');
        $order_to = $this->input->post('orderTo');
        // $subtask_id = 23;
        // $task_id = 1;
        // $order_to = 1;
		$subtask_details = $this->get_subtask_qry($subtask_id);
		$former_subtask_order = $this->get_former_subtask($task_id, $order_to);// get the details of the subtask to be replaced
		//move subtask to the new order
		$subtask_order[0] = [
            'subTask_ID' => $subtask_id,
            'sequence' => $order_to
        ];
        //the subtask to be replaced will be swapped to the orig sequence of the "moving to" subtask
        $subtask_order[1] = [
            'subTask_ID' => $former_subtask_order->subTask_ID,
            'sequence' => $subtask_details->sequence
		];
		if(count($subtask_order) > 0){
			$subtask_update['update'] = $this->general_model->batch_update($subtask_order, 'subTask_ID', 'tbl_processST_subtask');
		}
		echo json_encode($subtask_update['update']);
    }
	//michael's code for update subtask sequencing

}
