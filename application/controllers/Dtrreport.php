<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Dtrreport extends General{

	public $logo=  "/var/www/html/sz/assets/images/img/logo2.png"; // Linux


    public function index($code =1)
    {
				$data = array('title' => 'TITO Report');
				 if ($this->check_access()) {
 				$this->load_template_view('templates/dtr/report/titoreport',$data);
				}
    }
	public function test()
    {
				$data = array('title' => 'TITO Report','data' => 1);
			 
 				$this->load_template_view('templates/dtr/report/titoreport',$data);
			 
    }
 	public function listTitoRequestRecordChart($hr=null){
			$date = $this->input->post("date");
			$type = $this->input->post("empType");
			
 				$emptyp = ( $type== "All") ? "" : "and i.acc_description='".$type."'"; 
				$date = explode(" - ",$date); 
				$d1 = date_format(date_create($date[0]),"Y-m-d H:i:s");
				$d2 = date_format(date_create($date[1]),"Y-m-d H:i:s");
			$qry="SELECT g.fname,g.lname,i.acc_name,i.acc_description,b.dtrRequest_ID,b.status_ID,time_start,time_end,b.dateCreated,g.pic,b.userId,c.emp_id,h.description as approverstatus,dateRequest,i.acc_id FROM tbl_dtr_request b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h,tbl_account i where b.userID=c.uid and b.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.status_ID=h.status_ID and f.acc_id=i.acc_id and b.status_ID in (5,6) and b.dateCreated between '$d1' and '$d2' $emptyp"; 
			$data = $this->general_model->custom_query($qry);
			$arr = array();
			
			foreach($data as $row){
				$total = $this->getTotalPeoplePerAccount($row->acc_id);
				$arr[$row->acc_description][$row->acc_name][] = array(
					"emp_id" => $row->emp_id,
					"fname" => $row->fname,
					"lname" => $row->lname,
					"total" => $total[0]->total,
				);	
			}
			
			echo json_encode($arr); 
			
	}
 	
 	public function getTotalPeoplePerAccount($acc_id){
  		$fields = "count(*) as total";
        $where = "a.acc_id=b.acc_id and b.isActive='yes' and a.acc_id=$acc_id";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_account a,tbl_employee b");

		
	}
 	public function listTitoRequestRecord($hr=null){
			
			$datatable = $this->input->post('datatable');
			
			$user_id = $datatable['query']['user_id'];
			if($hr==null){
			$query['query'] ="SELECT g.fname,g.lname,a.dtrRequest_ID,a.status_ID,b.approvalStatus_ID,message,a.acc_time_id,dateRequest,time_start,time_end,a.dateCreated,g.pic,b.userId,c.emp_id,h.description as  approverstatus FROM tbl_dtr_request a, tbl_dtr_request_approval b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h where b.approvalStatus_ID =h.status_ID and a.dtrRequest_ID = b.dtrRequest_ID and a.userID=c.uid and a.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.approvalStatus_ID in (5,6) and b.userId=".$user_id;
			}else{
				$date = $datatable['query']['date']; 
				$emptyp = ($datatable['query']['type'] == "All") ? "" : "and i.acc_description='".$datatable['query']['type']."'"; 
				$date = explode(" - ",$date); 
				$d1 = date_format(date_create($date[0]),"Y-m-d H:i:s");
				$d2 = date_format(date_create($date[1]),"Y-m-d H:i:s");
			$query['query'] ="SELECT g.fname,g.lname,i.acc_name,i.acc_description,b.dtrRequest_ID,b.status_ID,time_start,time_end,b.dateCreated,g.pic,b.userId,c.emp_id,h.description as approverstatus,dateRequest FROM tbl_dtr_request b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h,tbl_account i where b.userID=c.uid and b.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.status_ID=h.status_ID and f.acc_id=i.acc_id and b.status_ID in (5,6) and b.dateCreated between '$d1' and '$d2' $emptyp"; 
 
			}	
				if (isset($datatable['query']['searchRecord'])) { 
					$keyword = $datatable['query']['searchRecord'];
  
					$query['search']['append'] = " and  (g.fname LIKE '%" . $keyword . "%' OR  g.lname LIKE '%" . $keyword . "%' OR  b.dtrRequest_ID LIKE '%" .$keyword. "%') ";

					$query['search']['total'] = " and (g.fname LIKE '%" . $keyword . "%' OR g.lname LIKE '%" . $keyword . "%' OR  b.dtrRequest_ID LIKE '%" . $keyword . "%')";
				}
			$data = $this->set_datatable_query($datatable, $query);

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
		}
		public function titoexport($emptype,$date1,$time1,$date2,$time2){
		echo $emptype." ".$date1." ".$time1." ".$date2." ".$time2;
		
		}
		public function download_tito_report($emptype,$dateStart,$timeStart,$dateEnd,$timeEnd){
			$this->load->library('PHPExcel', NULL, 'excel');
			$objPHPExcel = $this->excel;
			$flname =  "SZ_TITO_REPORT_".$dateStart."_".$timeStart."_to_".$dateEnd."_".$timeEnd; 
			$title =  "SZ_TITO_REPORT"; 
			$objPHPExcel->setActiveSheetIndex(0)->setTitle($title);
			$sheet = $objPHPExcel->getActiveSheet(0); 
			//-------------------------------------------------------------------
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath($this->logo);
			$objDrawing->setOffsetX(0);    // setOffsetX works properly
			$objDrawing->setOffsetY(0);  //setOffsetY has no effect
			$objDrawing->setCoordinates('A1');
			$objDrawing->setResizeProportional(false);
			// set width later
			$objDrawing->setWidth(150);
			$objDrawing->setHeight(50);
			$objDrawing->setWorksheet($this->excel->getActiveSheet());
			//----------------------------------------------------------------------
			
 				$emptyp = ( $emptype== "All") ? "" : "and i.acc_description='".$type."'"; 
				$time_start = str_replace("-"," ",$timeStart);
				$time_end = str_replace("-"," ",$timeEnd);
				$d1 = date_format(date_create($dateStart." ".$time_start),"Y-m-d H:i:s");
				$d2 = date_format(date_create($dateEnd." ".$time_end),"Y-m-d H:i:s");

				
				$query="SELECT f.id_num,g.fname,g.lname,i.acc_name,i.acc_description,b.dtrRequest_ID as titoid,b.status_ID,time_start,time_end,b.dateCreated,g.pic,b.userId,c.emp_id,h.description as approverstatus,dateRequest,startDateWorked,startTimeWorked,endDateWorked,endTimeWorked,message FROM tbl_dtr_request b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h,tbl_account i where b.userID=c.uid and b.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.status_ID=h.status_ID and f.acc_id=i.acc_id and b.status_ID in (5,6) and b.dateCreated between '$d1' and '$d2' $emptyp"; 

				
				$result = $this->general_model->custom_query($query);
				
				$j=0;
				$jj=3;
					$sheet->getStyle("E1:E1")->applyFromArray(array('font' => array('color' => array('rgb' => '1167b1'))));
					$sheet->setCellValueByColumnAndRow(3,1, "Date Range:");
					$sheet->setCellValueByColumnAndRow(3,2, ''.(date("F d, Y h:i A",strtotime($d1))).' to '.date("F d, Y h:i A",strtotime($d2)));
					

					$s=5;
					
					$sheet->setCellValueByColumnAndRow($j++,$s, "ID NUMBER");
					$sheet->setCellValueByColumnAndRow($j++,$s, "LASTNAME");
					$sheet->setCellValueByColumnAndRow($j++,$s, "FIRSTNAME");
					$sheet->setCellValueByColumnAndRow($j++,$s, "ACCOUNT");
					$sheet->setCellValueByColumnAndRow($j++,$s, "EMPLOYEE TYPE");
					$sheet->setCellValueByColumnAndRow($j++,$s, "TITO ID");
					$sheet->setCellValueByColumnAndRow($j++,$s, "DATE REQUESTED");
					$sheet->setCellValueByColumnAndRow($j++,$s, "DATE");
					$sheet->setCellValueByColumnAndRow($j++,$s, "START TIME");
					$sheet->setCellValueByColumnAndRow($j++,$s, "END TIME");
					$sheet->setCellValueByColumnAndRow($j++,$s, "CLOCK-IN");
					$sheet->setCellValueByColumnAndRow($j++,$s, "CLOCK-OUT");
					$sheet->setCellValueByColumnAndRow($j++,$s, "REASON");
					

				$s=6;
				$color = 1;
				foreach($result as $row){
				 
						$j=0;
						$sheet->getStyle('E'.$s)->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');
						$sheet->getStyle('F'.$s)->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->emp_id));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->lname));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->fname));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->acc_name));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->acc_description));
						$sheet->setCellValueByColumnAndRow($j++,$s, str_pad($row->titoid, 8, '0', STR_PAD_LEFT));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->dateCreated));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->dateRequest));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->time_start));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->time_end));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->startDateWorked." ".$row->startTimeWorked));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->endDateWorked." ".$row->endTimeWorked));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->message));
 						// $sheet->setCellValue('G'.$s,'=INT(F'.$s.'-E'.$s.')*8+(((F'.$s.'-E'.$s.')-INT(F'.$s.'-E'.$s.'))/0.04166666)');
						$sheet->getStyle('A'.$s.':M'.$s)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
						if($color%2==0){
							$sheet->getStyle('B'.$s.':M'.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CECECE');
						}
						$sheet->getStyle('A'.$s.':M'.$s)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER );

					$s++;
					 
					$color++;
				}
				
				
				foreach(range('A','N') as $columnID) {
					$sheet->getColumnDimension($columnID)->setAutoSize(true);
				}
				foreach(range('E','L') as $columnID) {
					$sheet->getColumnDimension($columnID)->setAutoSize(false);
					$sheet->getColumnDimension($columnID)->setWidth(25);
					
				}
				$sheet->getStyle('M1:M5000')->getAlignment()->setWrapText(true);
				// $sheet->getProtection()->setPassword('SzFlexiC0d3'); 
				// $sheet->getProtection()->setSheet(true);

				$sheet->setShowGridlines(false);
				$sheet->getStyle("A5:M5")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('2d2d2d');
				$sheet->getStyle("A5:M5")->applyFromArray(array('font' => array('color' => array('rgb' => 'FFFFFF'))));
				// $sheet->getColumnDimension('A')->setVisible(FALSE);
				// $sheet->getColumnDimension('G')->setVisible(FALSE);
				$sheet->getRowDimension(4)->setVisible(FALSE);
			$filename = $flname.'.xls'; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save('php://output');
		}
   
}
 