<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Payrollreport extends General
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *     - or -
     *         http://example.com/index.php/welcome/index
     *     - or -
     */
	protected $title = array('title' => 'Payroll Report');
	public $logo=  "/var/www/html/sz/assets/images/img/logo2.png"; // Linux
	// public $logo = "C:\\www\\sz\\assets\\images\\img\\logo2.png"; //windows
	 public $bootstrapcss = '';
     public function coverage(){
		$data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Payroll Coverage',
        ];
		if ($this->check_access()) {
			$this->load_template_view('templates/payroll/index', $data);
		}
    }
	public function detail(){
        $record["title"] = "DTR Upload";
		  if ($this->check_access()) {
			$this->load_template_view('templates/payroll/report/detailreport', $record);
		  }
	}
	public function birreport2(){
        $record["title"] = "Report";
		// if ($this->check_access()) {
 			$this->load_template_view('templates/payroll/report/bir2', $record);
		// }
	}
	public function showAdjustAdd(){
	  $emp = $this->input->post("emp_id");
	  $coverage_id = $this->input->post("coverage_id");
	  $payroll_id = $this->input->post("payroll_id");
 
	  $fields = "a.*,additionname as adjname,description";
	  $where = "a.paddition_id=b.paddition_id and emp_id=$emp and coverage_id=$coverage_id";
	  $rs = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_emp_addition a,tbl_payroll_addition b");
		echo json_encode($rs);
		
	}
	public function showAdjustDeduct(){
	  $emp = $this->input->post("emp_id");
	  $coverage_id = $this->input->post("coverage_id");
	  $payroll_id = $this->input->post("payroll_id");
 
	  $fields = "a.*,deductionname as adjname,description";
	  $where = "a.pdeduct_id=b.pdeduct_id and emp_id=$emp and coverage_id=$coverage_id";
	  $rs = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_emp_deduction a,tbl_payroll_deductions b");
		echo json_encode($rs);
		
	}
	public function payrollhistory(){
		  $emp = $this->input->post("emp");
		  $coverage_id = $this->input->post("coverage_id");

 		$fields = "a.*";
        $where = "coverage_id=$coverage_id and a.emp_promoteId=b.emp_promoteID and b.emp_id in (".implode(",",$emp).") and b.isActive=1";
        $rs =  $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_emp_promote b");
		echo json_encode($rs);
	}
	public function getAdjMinus($emp_id,$coverage_id){
 		$fields = "deductionname,value";
        $where = "a.pdeduct_id=b.pdeduct_id and emp_id=$emp_id and coverage_id=$coverage_id and b.isActive=1 and value>0";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_emp_deduction a,tbl_payroll_deductions b");

	}
	public function getAdjAdd($emp_id,$coverage_id){
 		$fields = "additionname,value,description";
        $where = "a.paddition_id=b.paddition_id and emp_id=$emp_id and coverage_id=$coverage_id and b.isActive=1 and value>0";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_emp_addition a,tbl_payroll_addition b");

	}
	
	public function get_final_pay(){
		$emp = $this->input->post("emp");
		$coverage=$this->input->post("coverage");

 		$fields = "d.fname,d.lname,b.emp_id, a.*";
		$where = "a.emp_promoteID=b.emp_promoteId and b.emp_id=c.emp_id and c.apid=d.apid and coverage_id=$coverage and b.emp_id in (".implode(",",$emp).")";
		$rs = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d");
		// echo json_encode($rs);
		$arr = Array();
		foreach($rs as $row){
			$adjAdd = $this->getAdjAdd($row->emp_id,$coverage);
			$adjMinus = $this->getAdjMinus($row->emp_id,$coverage);

			$arr[$row->payroll_id] = array(
			"absentLate"=>$row->absentLate,
			"absentLate_hours"=>$row->absentLate_hours,
			"account"=>$row->account,
			"adjust_add"=>$row->adjust_add,
			"adjust_details"=>$row->adjust_details,
			"adjust_minus"=>$row->adjust_minus,
			"ahr_amount"=>$row->ahr_amount,
			"ahr_hour"=>$row->ahr_hour,
			"ahr_id"=>$row->ahr_id,
			"bir_details"=>$row->bir_details,
			"bonus"=>$row->bonus,
			"bonus_details"=>$row->bonus_details,
			"clothing"=>$row->clothing,
			"coverage_id"=>$row->coverage_id,
			"daily"=>$row->daily,
			"deduct_details"=>$row->deduct_details,
			"emp_id"=>$row->emp_id,
			"emp_promoteId"=>$row->emp_promoteId,
			"emp_type"=>$row->emp_type,
			"employer_hdmf"=>$row->employer_hdmf,
			"employer_phic"=>$row->employer_phic,
			"employer_sss"=>$row->employer_sss,
			"final"=>$row->final,
			"fname"=>$row->fname,
			"hdmf_details"=>$row->hdmf_details,
			"ho"=>$row->ho,
			"hour_ho"=>$row->hour_ho,
			"hour_hp"=>$row->hour_hp,
			"hour_nd"=>$row->hour_nd,
			"hourly"=>$row->hourly,
			"hp"=>$row->hp,
			"isActive"=>$row->isActive,
			"isDaily"=>$row->isDaily,
			"laundry"=>$row->laundry,
			"leave_id"=>$row->leave_id,
			"lname"=>$row->lname,
			"loan_details"=>$row->loan_details,
			"monthly"=>$row->monthly,
			"nd"=>$row->nd,
			"ot_bct"=>$row->ot_bct,
			"pagibig_id"=>$row->pagibig_id,
			"payroll_id"=>$row->payroll_id,
			"pemp_adjust_id"=>$row->pemp_adjust_id,
			"phic_details"=>$row->phic_details,
			"philhealth_id"=>$row->philhealth_id,
			"quinsina"=>$row->quinsina,
			"rice"=>$row->rice,
			"sss_details"=>$row->sss_details,
			"sss_id"=>$row->sss_id,
			"tax_id"=>$row->tax_id,
			"total_deduction"=>$row->total_deduction,
			"total_earning"=>$row->total_earning,
			"total_hours"=>$row->total_hours,
			"adjAdd"=>$adjAdd, 
			"adjMinus"=>$adjMinus 
			);
		}
		echo json_encode($arr);
		
	}
	public function birTrainReport(){
		ini_set('max_execution_time',3000);

		$site = $this->input->post("site");
		$year = $this->input->post("year");
		$months = $this->input->post("monthz");
 
		$mo = explode("-",$months);
		$objPHPExcel = $this->phpexcel;
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$type2= "report";
			$sheet = $objPHPExcel->getActiveSheet(0);

			$styleArray = array(
				'font' => array(
					'bold' => true
				)
			);
			$fontStyle = [
				'font' => [
					'size' => 8
				]
			];
			$style = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
			);
			$style_text_left = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
			);
			$text_left = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				) 
			);
			$text_left_BOLD = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'font' => array(
					'bold' => true
				)
			);
				$sheet->setShowGridlines(false);
				$objPHPExcel->createSheet();
				$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payroll");
				
				$sheet->setCellValue('A1',"FBC BUSINESS SOLUTIONS");
				$sheet->setCellValue('A2',"Tax computation");
				$sheet->setCellValue('J2',"Taxable income");
				$sheet->setCellValue('L2',"Tax");
				$sheet->setCellValue('A3',"ID NUMBER");
				$sheet->setCellValue('B3',"tinNumber");
				$sheet->setCellValue('C3',"lastName");
				$sheet->setCellValue('D3',"firstName");
				$sheet->setCellValue('E3',"middleName");
				$sheet->setCellValue('F3',"address");
				$sheet->setCellValue('G3',"zipcode");
				$sheet->setCellValue('H3',"birthday");
				$sheet->setCellValue('I3',"nationality");
				$sheet->setCellValue('J3',"employmentStatus");
				$sheet->setCellValue('K3',"DATE HIRED");
				$sheet->setCellValue('L3',"endDate");
				$sheet->setCellValue('M3',"reasonOfSeparation");
				$sheet->setCellValue('N3',"BASIC SALARY");
				
				$sheet->setCellValue('O3',"NIGHT DIFFERENTIAL");
				$sheet->setCellValue('P3',"HAZARD PAY");
				$sheet->setCellValue('Q3',"AHR");
				$sheet->setCellValue('R3',"HOLIDAY PAY");
				
				$sheet->setCellValue('S3',"TOTAL ALLOWANCES");
				$sheet->setCellValue('T3',"TOTAL BONUS");
				$sheet->setCellValue('U3',"LATE & ABSENCES");
				$sheet->setCellValue('V3',"GROSS INCOME");
				$sheet->setCellValue('W3',"SSS PREMIUM");
				$sheet->setCellValue('X3',"HDMF PREMIUM");
				$sheet->setCellValue('Y3',"PHIC PREMIUM");
				
				$sheet->setCellValue('Z3',"Fixed monthly allowable deduction");
				
				$sheet->setCellValue('AA3',"NET TAXABLE INCOME");
				$sheet->setCellValue('AB3',"CURRENT MONTH TAX DUE");
				
				$sheet->setCellValue('AC3',"Remarks");
				
				$sheet->setCellValue('AD3',"PREVIOUS MONTHS' TAX");
				$sheet->setCellValue('AE3',"TOTAL ACCUMULATED TAX WITHHELD");
				
 				$data["emp"] = $this->tax_contri($mo,0,$year);
 				// $data["emp"] = $this->tax_contri($mo,2889,$year);
				
					$adj_add_query = "SELECT paddition_id as id,additionname as adj FROM tbl_payroll_addition where isActive=1 order by additionname";
					$adj_bonus_query = "SELECT bonus_name FROM tbl_bonus WHERE isTaxable=1 and isActive=1";
 
						$adj_add_result = $this->general_model->custom_query($adj_add_query);
						$adj_bonus_result = $this->general_model->custom_query($adj_bonus_query);
										$roww=4;
					foreach($data["emp"] as $row){
							$arrAddAdj = array();
							$arrBonusAdj = array();	
							$arrAllowances = array();	
							$arrBonuses = array();	
							$totalAdj = 0;
							$totalFinalAllowance = 0;
							$totalFinalBonus = 0;
							$totalBonus = 0;
							if(trim($row->adjust_details)!="0" || trim($row->adjust_details)!=0){
								$adjusted = explode(",",$row->adjust_details);
								
								for($i = 0; $i<count($adjusted); $i++){ 
										$orig_add_break = explode("|",$adjusted[$i]);
										$orig_add_break_final = explode("=",$orig_add_break[1]);
										$arrAddAdj[strtolower(trim($orig_add_break_final[0]))] = $orig_add_break_final[1];
										 
								}
							}
							
							foreach($adj_add_result as $row_add){
									if(array_key_exists(strtolower(trim($row_add->adj)),$arrAddAdj)){
										$totalAdj+=(float)$arrAddAdj[strtolower(trim($row_add->adj))];
 										if(strtolower(trim($row_add->adj))!=="client bonus"){
										if(strpos(strtolower(trim($row_add->adj)), "bonus") == TRUE){
											
												$arrBonuses[strtolower(trim($row_add->adj))] = $arrAddAdj[strtolower(trim($row_add->adj))];
												$totalFinalBonus+=$arrAddAdj[strtolower(trim($row_add->adj))];
												//echo "Word Not Found!";
											
										} else{
											$arrAllowances[strtolower(trim($row_add->adj))] = $arrAddAdj[strtolower(trim($row_add->adj))];
											$totalFinalAllowance+=$arrAddAdj[strtolower(trim($row_add->adj))];
											
										}
										}
									}else{
										$totalAdj+=0;
 									}
							}
							
							if(trim($row->bonus_details)!="0" || trim($row->bonus_details)!=0){
								$bonus = explode(",",$row->bonus_details);
								
								for($i = 0; $i<count($bonus); $i++){
										$orig_bonus_final = explode("=",$bonus[$i]);
										$arrBonusAdj[strtolower(trim($orig_bonus_final[0]))] = $orig_bonus_final[1];
								} 
							}
							foreach($adj_bonus_result as $row_bon){
									if(array_key_exists(strtolower(trim($row_bon->bonus_name)),$arrBonusAdj)){
										
										if(strtolower(trim($row_bon->bonus_name))!=="client bonus"){
											$totalBonus+=(float)$arrBonusAdj[strtolower(trim($row_bon->bonus_name))];
											if(strpos(strtolower(trim($row_bon->bonus_name)), "bonus") == TRUE){ 
											
												$arrBonuses[strtolower(trim($row_bon->bonus_name))] = $arrBonusAdj[strtolower(trim($row_bon->bonus_name))];
												$totalFinalBonus+=$arrBonusAdj[strtolower(trim($row_bon->bonus_name))];
												
											}else{ 
												
												$arrAllowances[strtolower(trim($row_bon->bonus_name))] = $arrBonusAdj[strtolower(trim($row_bon->bonus_name))];
												$totalFinalAllowance+=$arrBonusAdj[strtolower(trim($row_bon->bonus_name))];
												
											}
										}
										
									}else{
										$totalBonus+=0;
 									}
  							}
							$ideal13MoPay = floatval(floatval($row->quinsina -$row->absentLate)/12);
							
							
							$totalGrossTrain  =   floatval($row->quinsina)  + floatval($row->nd)+ floatval($row->hp)+ floatval($row->ho) + floatval($row->ahr_amount) + $ideal13MoPay  +  $totalFinalAllowance + $totalFinalBonus;
							 
							
							// $totalGrossTrain  = floatval($pos_details[0]->rate/2) + $ndRate + $hpRate + ($rate_hourly*$holidaywork)+ ($rate_hourly*$ahr_calc) + $totalFinalBasicAllowance+ $ideal13MoPay  + $totalFinalAllowance + $totalFinalBonus ;


							// $totalAmountTrain  =   floatval($totalGrossTrain) - (floatval($row->sss_details) + floatval($row->phic_details) + floatval($row->hdmf_details));
							
							$totalNetTrain_1  = $totalGrossTrain - (floatval($row->absentLate) + floatval($row->sss_details) + floatval($row->phic_details) + floatval($row->hdmf_details));

							
							$assumption90k = ($totalNetTrain_1>10417) ? (90000/24) : $ideal13MoPay ;
							
							$totalAmountTrain  =   floatval($totalGrossTrain) - (floatval($row->sss_details) + floatval($row->phic_details) + floatval($row->hdmf_details) + floatval($row->absentLate) + floatval($assumption90k) );
						 
							
							
 							// $totalNetTrain_2  = $totalGrossTrain - (floatval($totalUTAbsent*$rate_hourly) + floatval($sss_final) + floatval($phic_final) + floatval($hdmf_final) + floatval($assumption90k) );

							
							
							$trainTax = $this->getTaxCol($totalAmountTrain);
							$getEmpDateStarted = $this->getEmpDateStarted($row->emp_id);
							$arr[$row->emp_id][$row->month][$row->payroll_id] = array(
								"lname" => $row->lname,
								"fname" => $row->fname,
								"mname" => $row->mname,
								"birthday" => $row->birthday,
								"address" => $row->address,
								"tin" => $row->bir,
								"civil" => $row->civil,
								"quinsina" => $row->quinsina,
								"sss" => $row->sss_details,
								"phic" => $row->phic_details,
								"hdmf" => $row->hdmf_details,
								"bir" => $row->bir_details,
								"period" => $row->period,
								"emp_promoteId" => $row->emp_promoteId,
								"salarymode" => $row->salarymode,
								"isDaily" => $row->isDaily,
								"id_num" => $row->id_num,
								"absentLate" => $row->absentLate,
								"separationDate" => ($row->separationDate!=null) ? date("m/d/Y",strtotime($row->separationDate)) : date("12/31/Y"),
								"reasonForLeaving" =>  $row->reasonForLeaving,
								"nd" => $row->nd,
								"hp" => $row->hp,
								"ho" => $row->ho,
								"ahr" => $row->ahr_amount,
								"1_arrAddAdj" => $arrAddAdj,
								"1_totalAdj" => $totalAdj,
								"2_arrBonusAdj" => $arrBonusAdj,
								"2_totalBonus" => $totalBonus,
								"3_arrAllowances" => $arrAllowances,
								"4_arrBonuses" => $arrBonuses,
								"5_trainTax" => $trainTax,
								"5_ideal13MoPay" =>$ideal13MoPay,
								"trainTax" => $trainTax,
								"5_totalAmountTrain" => $totalAmountTrain,
								"totalFinalAllowance" => $totalFinalAllowance,
								"totalFinalBonus" => $totalFinalBonus,
								"getEmpDateStarted" => $getEmpDateStarted->dateFrom,
							); 
					}

 					  // echo json_encode($arr);
					  // exit();
					$salary=0;
					$total_salary=0;
					$taxable=0;
					$ntaxable=0;
					$total_sss=0;
					$total_hdmf=0;
					$total_phic=0;
					$total_taxable=0;
					$total_ntaxable=0;
					$total_J=0;
					$total_K=0;
					$total_L=0;
					$total_M=0;
					$total_ntaxable_daily=0;
					foreach($arr as $row => $val){
							$salary=0;
							$sss_details=0;
							$hdmf_details=0;
							$phic_details=0;
							$bir=0;
							$totalFinalAllowance=0;
							$totalFinalBonus=0;
							$trainTax=0;
							$nd=0;
							$hp=0;
							$ho=0;
							$ahr=0;
							
 								foreach($val as $row2 => $val2){
									
									foreach($val2 as $row3 => $val3){
										$lname= $val3["lname"];
										$fname= $val3["fname"];
										$mname= $val3["mname"];
										$birthday= $val3["birthday"];
										$tin= $val3["tin"];
										$civil= $val3["civil"];
										$address= $val3["address"];
										$quinsina= $val3["quinsina"];
										$absentLate= $val3["absentLate"];
										$separationDate= $val3["separationDate"];
										$reasonForLeaving= $val3["reasonForLeaving"];
										$getEmpDateStarted= $val3["getEmpDateStarted"];
										$sss_details+= $val3["sss"];
										$hdmf_details+= $val3["hdmf"];
										$phic_details+= $val3["phic"];
										$bir2=explode("(",$val3["bir"]);
										
										$nd+= (int)$val3["nd"];
										$hp+= (int)$val3["hp"];
										$ho+= (int)$val3["ho"];
										$ahr+= (int)$val3["ahr"];
										
										$bir+= (int)$bir2[0];
										$isDaily=$val3["isDaily"];
										$salary+=$val3["quinsina"];
										$ar_count = count($val);
										$period = $val3["period"];
										$id_num = $val3["id_num"];
										$totalFinalAllowance += $val3["totalFinalAllowance"];
										$totalFinalBonus += $val3["totalFinalBonus"];
										$trainTax += $val3["trainTax"];
										$isDaily = ($val3["salarymode"]=="Daily") ? "1 month period" : "-";

							
									}
								}
 
							if($bir!=0){
								$ntaxable=$sss_details + $hdmf_details + $phic_details;
							}else{
								$ntaxable=$salary + $sss_details + $hdmf_details + $phic_details;
							}
							$taxable= ($bir!=0) ? ($salary-$ntaxable) : 0.00;
							$p1 = ($period==1) ? $taxable : "0.00";
							$p2 = ($period==2) ? $taxable : "0.00";
							$t1 = ($period==1) ? $bir : "0.00";
							$t2 = ($period==2) ? $bir : "0.00";
							

							$grossIncome = $salary+$nd+$hp+$ho +$ahr + $totalFinalAllowance + $totalFinalBonus - $absentLate;
							$sheet->setCellValue('A'.$roww,$id_num);
							$sheet->setCellValue('B'.$roww,str_replace("-","", $tin));
							$sheet->setCellValue('C'.$roww,ucwords($lname));
							$sheet->setCellValue('D'.$roww,ucwords($fname));
							$sheet->setCellValue('E'.$roww,ucwords($mname));
								$_filter = array(",", ":", "'");
								$init_addr = str_replace($_filter, "", $address); 
 								$finaladdr = str_replace("Ñ","N",str_replace("ñ","n", $init_addr));
 							$sheet->setCellValue('F'.$roww,$finaladdr);
							$sheet->setCellValue('G'.$roww,"9000");
							$sheet->setCellValue('H'.$roww,date("m/d/Y",strtotime($birthday)));
							$sheet->setCellValue('I'.$roww,"Filipino");
							$sheet->setCellValue('J'.$roww,"R");
							$sheet->setCellValue('K'.$roww,date("m/d/Y",strtotime($getEmpDateStarted)));
							
							$sheet->setCellValue('L'.$roww,$separationDate);
							$sheet->setCellValue('M'.$roww,$reasonForLeaving);
							$sheet->setCellValue('N'.$roww,$salary);
									 
							$sheet->setCellValue('O'.$roww,$nd);
							$sheet->setCellValue('P'.$roww,$hp);
							$sheet->setCellValue('Q'.$roww,$ahr);
							$sheet->setCellValue('R'.$roww,$ho);
							
							$sheet->setCellValue('S'.$roww,$totalFinalAllowance);
							$sheet->setCellValue('T'.$roww,$totalFinalBonus);
							$sheet->setCellValue('U'.$roww,$absentLate);
							$sheet->setCellValue('V'.$roww,$grossIncome);
							$sheet->setCellValue('W'.$roww,$sss_details);
							$sheet->setCellValue('X'.$roww,$hdmf_details);
							$sheet->setCellValue('Y'.$roww,$phic_details);
							$fixedMonth = (90000 - $salary)/12;
							$sheet->setCellValue('Z'.$roww,$fixedMonth);
							
							$netIncome = $grossIncome - ($sss_details+$hdmf_details+$phic_details) - $fixedMonth;					
							$netIncome = ($netIncome>0) ?  $netIncome : 0;					
							$finalTrainTax = ($netIncome>=20833) ? $trainTax : 0 ;
							$sheet->setCellValue('AA'.$roww,$netIncome);
							$sheet->setCellValue('AB'.$roww,$finalTrainTax);
							$sheet->setCellValue('AC'.$roww,"");
							$sheet->setCellValue('AD'.$roww,"");
							$sheet->setCellValue('AE'.$roww,"");
									$total_J+=$taxable;
									$total_K+=$taxable;
									$total_L+=$bir;
									$total_M+=$bir;
						
 							$sheet->getStyle("A".$roww.":AE".$roww)->applyFromArray($style_text_left);
							$sheet->getStyle("A3:AE3")->applyFromArray($style);
							$sheet->getStyle("A3:AE3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('7b7b7b');
							$sheet->getStyle("K".$roww.":T".$roww)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

							foreach(range('A','AE') as $columnID){
								$sheet->getColumnDimension($columnID)->setAutoSize(true);
							}
							$roww++; 
							$salary=0;
							$taxable=0;
							$ntaxable=0;
					}
 
 						
		$file = 'PAYROLL_BIR_REPORT_'.$year.'_bir'.$this->session->userdata('uid').'.xlsx';
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		/* $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output') */;	
		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		ob_start();
		$objWriter->save("php://output");
		$xlsData = ob_get_contents();
		ob_end_clean();		
			

		$response =  array(
			'op' => 'ok', 
			'rs' => $arr,
 			'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
		);

	die(json_encode($response));
	}
	public function getEmpDateStarted($emp_id){
 		$fields = "dateFrom";
        $where = "emp_id=".$emp_id;
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_emp_promote","emp_promoteID ASC" );
 
	}
	public function getTaxCol($value){
		$amount= floatval($value);
		if($amount<=10417){
			$tax = 0;
		}else if($amount>=10418 && $amount<=16666){
 			$tax = ($amount-10417)*0.20;
		}else if($amount>=16667 && $amount<=33332){
 			$tax = 1250+(($amount-16667)*0.25);
		}else if($amount>=33333 && $amount<=83332){
 			$tax = 5416.67+(($amount-33333)*0.30);
		}else if($amount>=83333 && $amount<=333332){
 			$tax = 20416.67+(($amount-83333)*0.32);
		}else if($amount>=333333){
 			$tax = 100416.67+(($amount-333333)*0.35);
		}else{
			$tax = 0;
		}
		return $tax;
	}
	public function testMJM(){
		ini_set('max_execution_time',3000);

		$objPHPExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		
			
			
			  $coverage = $this->input->post("coverage");
			  $type = $this->input->post("type");
			  $cat = $this->input->post("cat");
			  $site = $this->input->post("site");

			$typeSite = array();
			$siteArr = explode(",",$site);
			$coverageArr = explode(",",$coverage);
			$catArr = explode(",",$cat);
			if($type=="Admin"){
				foreach($siteArr as $row1 => $val1){ 
					foreach($catArr as $row2 => $val2){
					 $val1 = ($val1=="CDO") ? "" : $val1;
						if(trim($val2)=="Probationary_and_Regular"){
							$typers= "admin-both-nonconfi".strtolower($val1); 

						}else if(trim($val2)=="Probationary_and_Regular-TSN"){
							$typers= "admin-both-nonconfi-tsn".strtolower($val1);

						}else if(trim($val2)=="Confidential"){
							$typers= "admin-both-confi".strtolower($val1);

						}else if(trim($val2)=="Trainee-TSN"){
							$typers= "admin-trainee-tsn".strtolower($val1);

						}else{
							$typers= "admin-trainee".strtolower($val1);
						}
						array_push($typeSite,$typers); 
					}	
				}
				
			}else{
				
				foreach($siteArr as $row1 => $val1){
					foreach($catArr as $row2 => $val2){
					 $val1 = ($val1=="CDO") ? "" : $val1;
					array_push($typeSite,"agent-".strtolower($val2).strtolower($val1)); 
					}	
				}
				
			} 
			$ctr =1;
			$superFinalAmount = 0;
			$superFinalNdAmount = 0;
			$superFinalHpAmount = 0;
			$superFinalAhrAmount= 0;
			$superFinalHoAmount = 0;
			$superFinalAdjAmount = 0;
			$superFinalAdjAllowAmount = 0;
			$superFinalBonusAmount = 0;
			$superFinalDeductsAmount= 0;
			$superFinalLoansAmount = 0;
			$superFinalPaskoAmount = 0;
			$superFinalIssuanceAmount = 0;

			//Start
			foreach($coverageArr as $row => $val){
				$objPHPExcel->createSheet();
				$sheet = $objPHPExcel->setActiveSheetIndex($ctr);
					//-------------------------------------------------------------------
						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName('Logo');
						$objDrawing->setDescription('Logo');
						$objDrawing->setPath($this->logo);
						$objDrawing->setOffsetX(0);    // setOffsetX works properly
						$objDrawing->setOffsetY(0);  //setOffsetY has no effect
						$objDrawing->setCoordinates('A1');
						$objDrawing->setResizeProportional(false);
						// set width later
						$objDrawing->setWidth(120);
						$objDrawing->setHeight(50);
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					//----------------------------------------------------------------------

				// $sheet->setShowGridlines(false);
				$sql = "SELECT e.fname,e.lname,e.mname,a.*,b.*,c.*,d.*,f.*,g.daterange,h.acc_name,i.dep_name FROM  tbl_payroll  a,tbl_emp_promote b,tbl_pos_emp_stat c,tbl_employee d,tbl_applicant e ,tbl_position f,tbl_payroll_coverage g,tbl_account h,tbl_department i where f.pos_id=c.pos_id and a.emp_promoteID=b.emp_promoteID and b.posempstat_id =c.posempstat_id and d.emp_id = b.emp_id and e.apid =d.apid and a.coverage_id=g.coverage_id and d.acc_id=h.acc_id and i.dep_id=h.dep_id and a.coverage_id in (".$val.") and emp_type in ('".implode("', '",$typeSite)."')  order by lname";
				$query = $this->db->query($sql); 
		
					$sheet->setCellValue('A6', "TEAM"); 
					$sheet->setCellValue('B6', "CLASS/SUB CLASS");
					$sheet->setCellValue('C6', "ID NUMBER");
					$sheet->setCellValue('D6', "EMPLOYEE NAME");
					$sheet->setCellValue('E6', "MIDDLE NAME");
					$sheet->setCellValue('F6', "POSITION");
					$sheet->setCellValue('G6', "STATUS");
					$sheet->setCellValue('H6', "BASIC PAY");
					$sheet->setCellValue('I6', "NIGHT DIFFERENTIAL");
					$sheet->setCellValue('J6', "HAZARD PAY");
					$sheet->setCellValue('K6', "AHR");
					$sheet->setCellValue('L6', "HOLIDAY PAY"); 
					$sheet->setCellValue('M6', "Clothing Allowance"); 
					$sheet->setCellValue('N6', "Laundry  Allowance"); 
					$sheet->setCellValue('O6', "Rice  Allowance"); 
					$sheet->getStyle('A6:O6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');
					$adj_add_query = "SELECT paddition_id as id,additionname as adj FROM tbl_payroll_addition where isActive=1 order by additionname";
					$adj_bonus_query = "SELECT bonus_name FROM tbl_bonus WHERE isActive=1";
					$adj_minus_query = "SELECT pdeduct_id as id,deductionname as adj FROM tbl_payroll_deductions where isActive=1 order by deductionname";
 
						$adj_add_result = $this->general_model->custom_query($adj_add_query);
						$adj_bonus_result = $this->general_model->custom_query($adj_bonus_query);
						$adj_minus_result = $this->general_model->custom_query($adj_minus_query);
						$jj = 16;
						$finalArr = array();
							$arrAllowances = array();	
							$arrBonuses = array();	
							$arrAdjustment = array();	
						foreach($adj_add_result as $row_add){
							// $sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($row_add->adj));
							// $cell = $this->columnLetter($jj);
							// $sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fcf0a4');
							// $objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
							// $finalArr["adjust_details-".$ctr] += 0; 
							if(strpos(strtolower(trim($row_add->adj)), "allowance") == TRUE){
								array_push($arrAllowances,strtolower(trim($row_add->adj)));
  							} else if(strpos(strtolower(trim($row_add->adj)), "bonus") == TRUE){
								array_push($arrBonuses,strtolower(trim($row_add->adj)));
   							}else{
								array_push($arrAdjustment,strtolower(trim($row_add->adj)));
   							}
						}

					
						foreach($adj_bonus_result as $row_bon){
							// $sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($row_bon->bonus_name));
							// $cell = $this->columnLetter($jj);
							// $sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('89f5eb');
							// $objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
								if(strpos(strtolower(trim($row_bon->bonus_name)), "allowance") == TRUE){
									array_push($arrAllowances,strtolower(trim($row_bon->bonus_name)));
								} else{
									array_push($arrBonuses,strtolower(trim($row_bon->bonus_name)));
								}
						}
							sort($arrAllowances);
							sort($arrBonuses);		  
							sort($arrAdjustment);	  
						foreach($arrAdjustment as $a){
							$sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($a));
							$cell = $this->columnLetter($jj);
							$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fcf0a4');
							$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
						 
						}
						$sheet->setCellValueByColumnAndRow($jj++,6,"TOTAL ADD ADJUSTMENT");
							$totalAdjustCol = $this->columnLetter($jj);
							$sheet->getStyle($totalAdjustCol.'6:'.$totalAdjustCol.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');

						foreach($arrAllowances as $a){
							$sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($a));
							$cell = $this->columnLetter($jj);
							$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fcf0a4');
							$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
						 
						}
						$sheet->setCellValueByColumnAndRow($jj++,6,"TOTAL ADD ALLOWANCES");
							$totalAdjustAllowCol = $this->columnLetter($jj);
							$sheet->getStyle($totalAdjustAllowCol.'6:'.$totalAdjustAllowCol.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');

						foreach($arrBonuses as $a){
							$sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($a));
							$cell = $this->columnLetter($jj);
							$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fcf0a4');
							$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
						 
						}

							$sheet->setCellValueByColumnAndRow($jj++,6,"TOTAL BONUS");
							$totalBonusCol = $this->columnLetter($jj);
							$totalBonusCol2 = $this->columnLetter($jj+8);
							$sheet->getStyle($totalBonusCol.'6:'.$totalBonusCol2.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');

							
							
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYER SHARE SSS CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"SSS- EMPLOYER SHARE EC CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYER SHARE HDMF CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYER SHARE PHIC CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYEE TAX WITHHELD");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYEE SHARE SSS CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYEE SHARE HDMF CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYEE SHARE PHIC CONTRIBUTION");
							
							foreach($adj_minus_result as $row3){
								$sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($row3->adj)); 
								$cell = $this->columnLetter($jj);
								$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('bd78ff');
								$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
	 
							}
							
							$sheet->setCellValueByColumnAndRow($jj++,6,"ABSENT/UNDERTIME");
							$cell = $this->columnLetter($jj);
							$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('bd78ff');
							$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
							$sheet->setCellValueByColumnAndRow($jj++,6,"TOTAL ADJ MINUS");
							$totalDeductCol = $this->columnLetter($jj);
							$sheet->getStyle($totalDeductCol.'6:'.$totalDeductCol.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');

							
							$sheet->getStyle($jj++.'6:'.$jj++.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('bd78ff');
							$jj-=2;
						foreach($adj_minus_result as $row3){
							$sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($row3->adj)); 
							$cell = $this->columnLetter($jj);
 							$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('bd78ff');
							$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
						}
							$sheet->setCellValueByColumnAndRow($jj++,6, "TOTAL LOAN MINUS");
							$totalLoanCol = $this->columnLetter($jj);
							$totalLoanCol2 = $this->columnLetter($jj+2);
							$sheet->getStyle($totalLoanCol.'6:'.$totalLoanCol2.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');

							

						// foreach(range('A','ZZ') as $columnID) {
							// $sheet->getColumnDimension($columnID)->setAutoSize(true);
							// // $sheet->getColumnDimension($columnID)->setWidth(25);
 						// }
						
							$sheet->setCellValueByColumnAndRow($jj++,6,"13th MONTH PAY");
							// $sheet->setCellValueByColumnAndRow($jj++,6,"basic");	  
							// $sheet->setCellValueByColumnAndRow($jj++,6,"totalAdj");	  
							// $sheet->setCellValueByColumnAndRow($jj++,6,"totalBonus");	  
							// $sheet->setCellValueByColumnAndRow($jj++,6,"govDeduct");	  
							// $sheet->setCellValueByColumnAndRow($jj++,6,"totalDeduct");	
							// $sheet->setCellValueByColumnAndRow($jj++,6,"ISSUANCE");
							$sheet->setCellValueByColumnAndRow($jj++,6,"ISSUANCE");
						foreach($this->excelColumnRange('A','ZZ') as $column) {
							$sheet->getColumnDimension($column)->setAutoSize(true);
						}
					$rows =7;
					$s =7;
					$lastRow = 1;
					$finalAmount = 0;
					$finalNdAmount = 0;
					$finalHpAmount = 0;
					$finalAhrAmount= 0;
					$finalHoAmount = 0;
					$finalAdjAmount = 0;
					$finalAdjAllowAmount = 0;
					$finalBonusAmount = 0;
					$finalDeductsAmount= 0;
					$finalLoansAmount = 0;
					$finalPaskoAmount = 0;
					$finalIssuanceAmount = 0;


					 foreach($query->result() as $row_rs){ 
					//	$ctr =0;
						$date = explode("-",$row_rs->daterange); 
						
						$sheet->setTitle(date("M d, Y",strtotime($date[0]))." - ".date("M d, Y",strtotime($date[1]))); 
						$stat = Array("","Trainee","Probationary","Regular","Part-time","On-The-Job Trainee");
						$sheet->setCellValue('A'.$rows, $row_rs->dep_name); 
						$sheet->setCellValue('B'.$rows, $row_rs->acc_name); 
						$sheet->setCellValue('C'.$rows, $row_rs->id_num);
						$sheet->setCellValue('D'.$rows, $row_rs->lname.", ".$row_rs->fname);
						$sheet->setCellValue('E'.$rows, $row_rs->mname);
						$sheet->setCellValue('F'.$rows, $row_rs->pos_details);
						$sheet->setCellValue('G'.$rows, $stat[(int)$row_rs->empstat_id]);
						$sheet->setCellValue('H'.$rows, $row_rs->quinsina);
						$sheet->setCellValue('I'.$rows, $row_rs->nd);
						$sheet->setCellValue('J'.$rows, $row_rs->hp);
						$sheet->setCellValue('K'.$rows, $row_rs->ahr_amount);
						$sheet->setCellValue('L'.$rows, $row_rs->ho); 
						$sheet->setCellValue('M'.$rows, $row_rs->clothing); 
						$sheet->setCellValue('N'.$rows, $row_rs->laundry); 
						$sheet->setCellValue('O'.$rows, $row_rs->rice); 
						$sheet->getStyle('A'.$s.':O'.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');
						
						$jj =16;
						
							$totalAdjAllowance = 0;
							$totalAdj = 0;
							$arrAddAdj = array();
							
 							$arrAdjust_new = array();	
							$arrAllowances_new = array();	
							$arrBonuses_new = array();	
 
								if(trim($row_rs->adjust_details)!="0" || trim($row_rs->adjust_details)!=0){
									$adjusted = explode(",",$row_rs->adjust_details);
									
									for($i = 0; $i<count($adjusted); $i++){
											$orig_add_break = explode("|",$adjusted[$i]);
											$orig_add_break_final = explode("=",$orig_add_break[1]);
											// $arrAddAdj[strtolower(trim($orig_add_break_final[0]))] = $orig_add_break_final[1];
											if(strpos(strtolower(trim($orig_add_break_final[0])), "allowance") == TRUE){
 												$arrAllowances_new[strtolower(trim($orig_add_break_final[0]))] = $orig_add_break_final[1];
											} else if(strpos(strtolower(trim($orig_add_break_final[0])), "bonus") == TRUE){
 													$arrBonuses_new[strtolower(trim($orig_add_break_final[0]))] = $orig_add_break_final[1];
											}else{
 													$arrAdjust_new[strtolower(trim($orig_add_break_final[0]))] = $orig_add_break_final[1];
											} 
									}
								}
 									  
								if(trim($row_rs->bonus_details)!="0" || trim($row_rs->bonus_details)!=0){
									$bonus = explode(",",$row_rs->bonus_details);
									
									for($i = 0; $i<count($bonus); $i++){
											$orig_bonus_final = explode("=",$bonus[$i]);
 											if(strpos(strtolower(trim($orig_bonus_final[0])), "allowance") == TRUE){
 												$arrAllowances_new[strtolower(trim($orig_bonus_final[0]))] = $orig_bonus_final[1];
											} else if(strpos(strtolower(trim($orig_bonus_final[0])), "bonus") == TRUE){
 												$arrBonuses_new[strtolower(trim($orig_bonus_final[0]))] = $orig_bonus_final[1];
											}
									} 
								}
								foreach($arrAdjustment as $row_add){
										if(array_key_exists(strtolower(trim($row_add)),$arrAdjust_new)){
											$sheet->setCellValueByColumnAndRow($jj++,$s, $arrAdjust_new[strtolower(trim($row_add))]);
											$totalAdj+=(float)$arrAdjust_new[strtolower(trim($row_add))];
											$add = $arrAdjust_new[strtolower(trim($row_add))];
										}else{
											$sheet->setCellValueByColumnAndRow($jj++,$s, 0);
											$add = 0;
										}
 									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fff8cf');
								}	
								$sheet->setCellValueByColumnAndRow($jj++,$s, $totalAdj);
 						$col1 = $this->columnLetter($jj);
						$sheet->getStyle($col1.$s.':'.$col1.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');

								foreach($arrAllowances as $row_add){
										if(array_key_exists(strtolower(trim($row_add)),$arrAllowances_new)){
											$sheet->setCellValueByColumnAndRow($jj++,$s, $arrAllowances_new[strtolower(trim($row_add))]);
											$totalAdjAllowance+=(float)$arrAllowances_new[strtolower(trim($row_add))];
											$add = $arrAllowances_new[strtolower(trim($row_add))];
										}else{
											$sheet->setCellValueByColumnAndRow($jj++,$s, 0);
											$add = 0;
										}
 									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fff8cf');
								}	
								$sheet->setCellValueByColumnAndRow($jj++,$s, $totalAdjAllowance);
							
							
							
 						$col1 = $this->columnLetter($jj);
						$sheet->getStyle($col1.$s.':'.$col1.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');

							$totalBonus = 0;
					 
							foreach($arrBonuses as $row_bon){
									if(array_key_exists(strtolower(trim($row_bon)),$arrBonuses_new)){
										$sheet->setCellValueByColumnAndRow($jj++,$s, $arrBonuses_new[strtolower(trim($row_bon))]);
										$totalBonus+=(float)$arrBonuses_new[strtolower(trim($row_bon))];
									}else{
										$sheet->setCellValueByColumnAndRow($jj++,$s, 0);
									}
									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('defffc');
							}
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$totalBonus); 
							$col1 = $this->columnLetter($jj);
							$col2 = $this->columnLetter($jj+8);
							$sheet->getStyle($col1.$s.':'.$col2.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');

							$sheet->setCellValueByColumnAndRow($jj++,$s, (float)$row_rs->employer_sss); 
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->employer_sss -  (float)$row_rs->sss_details);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->employer_hdmf);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->employer_phic);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->bir_details);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->sss_details);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->hdmf_details);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->phic_details);
							
							$arrDeductAdj = array();	
							$arrDeductLoanAdj = array();	
							if(trim($row_rs->deduct_details)!="0" || trim($row_rs->deduct_details)!=0){
								$deduct = explode(",",$row_rs->deduct_details);
								
								for($i = 0; $i<count($deduct); $i++){
										$orig_deduct_final = explode("=",$deduct[$i]);
										$arrDeductAdj[strtolower(trim($orig_deduct_final[0]))] = $orig_deduct_final[1];
								} 
							}
							if(trim($row_rs->loan_details)!="0" || trim($row_rs->loan_details)!=0){
								$deductLoan = explode(",",$row_rs->loan_details);
								
								for($i = 0; $i<count($deductLoan); $i++){
										$orig_deductLoan_final = explode("=",$deductLoan[$i]);
										$arrDeductLoanAdj[strtolower(trim($orig_deductLoan_final[0]))] = $orig_deductLoan_final[1];
								} 
							}
							$total_dtr_deduction = 0;
							$totalDeduct = 0;
							
							
								foreach($adj_minus_result as $row_deduct){
									if(array_key_exists(strtolower(trim($row_deduct->adj)),$arrDeductAdj)){
										$sheet->setCellValueByColumnAndRow($jj++,$s, $arrDeductAdj[strtolower(trim($row_deduct->adj))]);
										if(strtolower(trim($row_deduct->adj))=="undertime"){
											$total_dtr_deduction += (float)$arrDeductAdj[strtolower(trim($row_deduct->adj))]; 
										}
										$totalDeduct+=(float)$arrDeductAdj[strtolower(trim($row_deduct->adj))];
									}else{
										$sheet->setCellValueByColumnAndRow($jj++,$s, 0);
									}
									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6ccff');
								}
								$sheet->setCellValueByColumnAndRow($jj++,$s, $row_rs->absentLate);
									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6ccff');

								$totalDeduct  += (float)$row_rs->absentLate;
								$sheet->setCellValueByColumnAndRow($jj++,$s, $totalDeduct);
								$col1 = $this->columnLetter($jj);
								$sheet->getStyle($col1.$s.':'.$col1.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');

								$totalLoanDeduct = 0;
								foreach($adj_minus_result as $row_deduct_loan){
									if(array_key_exists(strtolower(trim($row_deduct_loan->adj)),$arrDeductLoanAdj)){
										$sheet->setCellValueByColumnAndRow($jj++,$s, $arrDeductLoanAdj[strtolower(trim($row_deduct_loan->adj))]);
										$totalLoanDeduct+=(float)$arrDeductLoanAdj[strtolower(trim($row_deduct_loan->adj))]; 
									}else{
										$sheet->setCellValueByColumnAndRow($jj++,$s, 0);
									}
									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fabbf2');
								}
								$sheet->setCellValueByColumnAndRow($jj++,$s,$totalLoanDeduct);
								$col1 = $this->columnLetter($jj);
								$col2 = $this->columnLetter($jj+2);
								$sheet->getStyle($col1.$s.':'.$col2.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');

								$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fabbf2');
								$sheet->getStyle('A'.$s.':M'.$s)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								
								$pasko13MonthPay = 0;
								$issuance = 0;
								$basic = 0;
								$govDeduct = 0; 
								$pasko13MonthPay = (float)$row_rs->quinsina-(float)$total_dtr_deduction-(float)$row_rs->absentLate;
								
								$basic = (float)$row_rs->quinsina + (float)$row_rs->clothing + (float)$row_rs->laundry + (float)$row_rs->rice + (float)$row_rs->ho + (float)$row_rs->nd + (float)$row_rs->ahr_amount + (float)$row_rs->hp;
								$govDeduct = (float)$row_rs->sss_details +(float)$row_rs->phic_details +(float)$row_rs->hdmf_details +(float)$row_rs->bir_details;
								$issuance = ($basic + $totalAdjAllowance + $totalAdj + $totalBonus) - ($totalDeduct +$totalLoanDeduct + $govDeduct); 
							$sheet->setCellValueByColumnAndRow($jj++,$s,$pasko13MonthPay);	 
							$totalPaskoCol = $this->columnLetter($jj);
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$basic);	  
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$totalAdj);	  
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$totalBonus);	  
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$govDeduct);	  
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$totalDeduct);	   
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$issuance);	  
							$sheet->setCellValueByColumnAndRow($jj++,$s,$row_rs->final);	  
							$totalIssuanceCol = $this->columnLetter($jj);
							
						$s++;
						$rows++;
						$lastRow = $jj;

						$finalAmount += (float)$row_rs->quinsina;
						$finalNdAmount += (float)$row_rs->nd;
						$finalHpAmount += (float)$row_rs->hp;
						$finalAhrAmount += (float)$row_rs->ahr_amount;
						$finalHoAmount += (float)$row_rs->ho;
						$finalAdjAmount += (float)$totalAdj;
						$finalAdjAllowAmount += (float)$totalAdjAllowance;
						$finalBonusAmount += (float)$totalBonus;
						$finalDeductsAmount += (float)$totalDeduct;
						$finalLoansAmount += (float)$totalLoanDeduct;
						$finalPaskoAmount += (float)$pasko13MonthPay;
						$finalIssuanceAmount += (float)$row_rs->final;
									

					}  
					//Starts of total below...
						 

					$sheet->setCellValueByColumnAndRow(7,$rows,$finalAmount);
					$sheet->setCellValueByColumnAndRow(8,$rows,$finalNdAmount);
					$sheet->setCellValueByColumnAndRow(9,$rows,$finalHpAmount);
					$sheet->setCellValueByColumnAndRow(10,$rows,$finalAhrAmount);
					$sheet->setCellValueByColumnAndRow(11,$rows,$finalHoAmount);
					$sheet->setCellValue($totalAdjustCol.$rows,$finalAdjAmount);
					$sheet->setCellValue($totalAdjustAllowCol.$rows,$finalAdjAllowAmount);
					$sheet->setCellValue($totalBonusCol.$rows,$finalBonusAmount);
					$sheet->setCellValue($totalDeductCol.$rows,$finalDeductsAmount);
					$sheet->setCellValue($totalLoanCol.$rows,$finalLoansAmount);
					$sheet->setCellValue($totalPaskoCol.$rows,$finalPaskoAmount);
					$sheet->setCellValue($totalIssuanceCol.$rows,$finalIssuanceAmount);
						$superFinalAmount 		 	+= (float)$finalAmount;
						$superFinalNdAmount 	 	+= (float)$finalNdAmount;
						$superFinalHpAmount 	 	+= (float)$finalHpAmount;
						$superFinalAhrAmount	 	+= (float)$finalAhrAmount;
						$superFinalHoAmount 	 	+= (float)$finalHoAmount;
						$superFinalAdjAmount 	 	+= (float)$finalAdjAmount;
						$superFinalAdjAllowAmount   += (float)$finalAdjAllowAmount;
						$superFinalBonusAmount	 	+= (float)$finalBonusAmount;
						$superFinalDeductsAmount 	+= (float)$finalDeductsAmount;
						$superFinalLoansAmount 	 	+= (float)$finalLoansAmount;
						$superFinalPaskoAmount 	 	+= (float)$finalPaskoAmount;
						$superFinalIssuanceAmount 	+= (float)$finalIssuanceAmount;

					$ctr++;
				 
						 

			} 
		//this start sa additional woorksheet == Total count
				 $objPHPExcel->createSheet();
				$sheet_final = $objPHPExcel->setActiveSheetIndex($ctr++);
				$sheet_final->setTitle("Summary"); 
		
				//-------------------------------------------------------------------
						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName('Logo');
						$objDrawing->setDescription('Logo');
						$objDrawing->setPath($this->logo);
						$objDrawing->setOffsetX(0);    // setOffsetX works properly
						$objDrawing->setOffsetY(0);  //setOffsetY has no effect
						$objDrawing->setCoordinates('A1');
						$objDrawing->setResizeProportional(false);
						// set width later
						$objDrawing->setWidth(100);
						$objDrawing->setHeight(50);
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					//----------------------------------------------------------------------
				$sheet_final->getStyle('A6:B6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');
				$sheet_final->setCellValue('A6',"Items");
				$sheet_final->setCellValue('A7',"Total Basic");
				$sheet_final->setCellValue('A8',"Total Night Differential");
				$sheet_final->setCellValue('A9',"Total Hazard Pay");
				$sheet_final->setCellValue('A10',"Total Holiday");
				$sheet_final->setCellValue('A11',"Total AHR");
				$sheet_final->setCellValue('A12',"Total Additional Adjustment");
				$sheet_final->setCellValue('A13',"Total Allowance");
				$sheet_final->setCellValue('A14',"Total Bonus");
				$sheet_final->setCellValue('A15',"Total Deduction Adjustment");
				$sheet_final->setCellValue('A16',"Total Loan Deduction");
				$sheet_final->setCellValue('A17',"Total 13th Month Pay");
				$sheet_final->setCellValue('A18',"Total Issuance");
				
				$sheet_final->setCellValue('B6',"Amount");
				$sheet_final->setCellValue('B7',$superFinalAmount);
				$sheet_final->setCellValue('B8',$superFinalNdAmount);
				$sheet_final->setCellValue('B9',$superFinalHpAmount);
				$sheet_final->setCellValue('B10',$superFinalHoAmount);
				$sheet_final->setCellValue('B11',$superFinalAhrAmount);
				$sheet_final->setCellValue('B12',$superFinalAdjAmount);
				$sheet_final->setCellValue('B13',$superFinalAdjAllowAmount);
				$sheet_final->setCellValue('B14',$superFinalBonusAmount);
				$sheet_final->setCellValue('B15',$superFinalDeductsAmount);
				$sheet_final->setCellValue('B16',$superFinalLoansAmount);
				$sheet_final->setCellValue('B17',$superFinalPaskoAmount);
				$sheet_final->setCellValue('B18',$superFinalIssuanceAmount);
				foreach($this->excelColumnRange('A','B') as $column) {
					$sheet_final->getColumnDimension($column)->setAutoSize(true);
				}  
				
		 $file = 'PAYROLL_ATM_REPORT.xlsx'; 
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		/* $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output') */;	
		
		  $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		ob_start();
		$objWriter->save("php://output");
		$xlsData = ob_get_contents();
		ob_end_clean();		 
			

		$response =  array(
			'op' => 'ok', 
			'site' => explode(",",$site),  
			'cat' => explode(",",$cat), 
			'type' => $type, 
			'typeSite' => $typeSite,
			'sql' => $sql,
			'rs' => $query->result(),
			'coverage' => explode(",",$coverage), 
			  'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
		);

	die(json_encode($response));

	}
	public function test(){
		ini_set('max_execution_time',3000);

		$objPHPExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		
			
			
			  $coverage = $this->input->post("coverage");
			  $type = $this->input->post("type");
			  $cat = $this->input->post("cat");
			  $site = $this->input->post("site");

			$typeSite = array();
			$siteArr = explode(",",$site);
			$coverageArr = explode(",",$coverage);
			$catArr = explode(",",$cat);
			if($type=="Admin"){
				foreach($siteArr as $row1 => $val1){ 
					foreach($catArr as $row2 => $val2){
					 $val1 = ($val1=="CDO") ? "" : $val1;
						if(trim($val2)=="Probationary_and_Regular"){
							$typers= "admin-both-nonconfi".strtolower($val1); 

						}else if(trim($val2)=="Probationary_and_Regular-TSN"){
							$typers= "admin-both-nonconfi-tsn".strtolower($val1);

						}else if(trim($val2)=="Confidential"){
							$typers= "admin-both-confi".strtolower($val1);

						}else if(trim($val2)=="Trainee-TSN"){
							$typers= "admin-trainee-tsn".strtolower($val1);

						}else{
							$typers= "admin-trainee".strtolower($val1);
						}
						array_push($typeSite,$typers); 
					}	
				}
				
			}else{
				
				foreach($siteArr as $row1 => $val1){
					foreach($catArr as $row2 => $val2){
					 $val1 = ($val1=="CDO") ? "" : $val1;
					array_push($typeSite,"agent-".strtolower($val2).strtolower($val1)); 
					}	
				}
				
			} 
			$ctr =1;
			$superFinalAmount = 0;
			$superFinalNdAmount = 0;
			$superFinalHpAmount = 0;
			$superFinalAhrAmount= 0;
			$superFinalHoAmount = 0;
			$superFinalAdjAmount = 0;
			$superFinalAdjAllowAmount = 0;
			$superFinalBonusAmount = 0;
			$superFinalDeductsAmount= 0;
			$superFinalLoansAmount = 0;
			$superFinalPaskoAmount = 0;
			$superFinalIssuanceAmount = 0;

			//Start
			foreach($coverageArr as $row => $val){
				$objPHPExcel->createSheet();
				$sheet = $objPHPExcel->setActiveSheetIndex($ctr);
					//-------------------------------------------------------------------
						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName('Logo');
						$objDrawing->setDescription('Logo');
						$objDrawing->setPath($this->logo);
						$objDrawing->setOffsetX(0);    // setOffsetX works properly
						$objDrawing->setOffsetY(0);  //setOffsetY has no effect
						$objDrawing->setCoordinates('A1');
						$objDrawing->setResizeProportional(false);
						// set width later
						$objDrawing->setWidth(120);
						$objDrawing->setHeight(50);
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					//----------------------------------------------------------------------

				// $sheet->setShowGridlines(false);
				$sql = "SELECT e.fname,e.lname,e.mname,a.*,b.*,c.*,d.*,f.*,g.daterange,h.acc_name,i.dep_name FROM  tbl_payroll  a,tbl_emp_promote b,tbl_pos_emp_stat c,tbl_employee d,tbl_applicant e ,tbl_position f,tbl_payroll_coverage g,tbl_account h,tbl_department i where f.pos_id=c.pos_id and a.emp_promoteID=b.emp_promoteID and b.posempstat_id =c.posempstat_id and d.emp_id = b.emp_id and e.apid =d.apid and a.coverage_id=g.coverage_id and d.acc_id=h.acc_id and i.dep_id=h.dep_id and a.coverage_id in (".$val.") and emp_type in ('".implode("', '",$typeSite)."')  order by lname";
				$query = $this->db->query($sql); 
		
					$sheet->setCellValue('A6', "TEAM"); 
					$sheet->setCellValue('B6', "CLASS/SUB CLASS");
					$sheet->setCellValue('C6', "ID NUMBER");
					$sheet->setCellValue('D6', "EMPLOYEE NAME");
					$sheet->setCellValue('E6', "MIDDLE NAME");
					$sheet->setCellValue('F6', "POSITION");
					$sheet->setCellValue('G6', "STATUS");
					$sheet->setCellValue('H6', "BASIC PAY");
					$sheet->setCellValue('I6', "NIGHT DIFFERENTIAL");
					$sheet->setCellValue('J6', "HAZARD PAY");
					$sheet->setCellValue('K6', "AHR");
					$sheet->setCellValue('L6', "HOLIDAY PAY"); 
					$sheet->setCellValue('M6', "Clothing Allowance"); 
					$sheet->setCellValue('N6', "Laundry  Allowance"); 
					$sheet->setCellValue('O6', "Rice  Allowance"); 
					$sheet->getStyle('A6:O6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');
					$adj_add_query = "SELECT paddition_id as id,additionname as adj FROM tbl_payroll_addition where isActive=1 order by additionname";
					$adj_bonus_query = "SELECT bonus_name FROM tbl_bonus WHERE isActive=1";
					$adj_minus_query = "SELECT pdeduct_id as id,deductionname as adj FROM tbl_payroll_deductions where isActive=1 order by deductionname";
 
						$adj_add_result = $this->general_model->custom_query($adj_add_query);
						$adj_bonus_result = $this->general_model->custom_query($adj_bonus_query);
						$adj_minus_result = $this->general_model->custom_query($adj_minus_query);
						$jj = 16;
						$finalArr = array();
						
						foreach($adj_add_result as $row1){
							$sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($row1->adj));
							$cell = $this->columnLetter($jj);
							$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fcf0a4');
							$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
							// $finalArr["adjust_details-".$ctr] += 0; 
						}
						$sheet->setCellValueByColumnAndRow($jj++,6,"TOTAL ADD ADJUSTMENT");
							$totalAdjustCol = $this->columnLetter($jj);
							$sheet->getStyle($totalAdjustCol.'6:'.$totalAdjustCol.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');

					
						foreach($adj_bonus_result as $row2){
							$sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($row2->bonus_name));
							$cell = $this->columnLetter($jj);
							$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('89f5eb');
							$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);

						}
						
						
							$sheet->setCellValueByColumnAndRow($jj++,6,"TOTAL BONUS");
							$totalBonusCol = $this->columnLetter($jj);
							$totalBonusCol2 = $this->columnLetter($jj+8);
							$sheet->getStyle($totalBonusCol.'6:'.$totalBonusCol2.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');

							
							
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYER SHARE SSS CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"SSS- EMPLOYER SHARE EC CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYER SHARE HDMF CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYER SHARE PHIC CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYEE TAX WITHHELD");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYEE SHARE SSS CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYEE SHARE HDMF CONTRIBUTION");
							$sheet->setCellValueByColumnAndRow($jj++,6,"EMPLOYEE SHARE PHIC CONTRIBUTION");
							
							foreach($adj_minus_result as $row3){
								$sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($row3->adj)); 
								$cell = $this->columnLetter($jj);
								$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('bd78ff');
								$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
	 
							}
							
							$sheet->setCellValueByColumnAndRow($jj++,6,"ABSENT/UNDERTIME");
							$cell = $this->columnLetter($jj);
							$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('bd78ff');
							$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
							$sheet->setCellValueByColumnAndRow($jj++,6,"TOTAL ADJ MINUS");
							$totalDeductCol = $this->columnLetter($jj);
							$sheet->getStyle($totalDeductCol.'6:'.$totalDeductCol.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');

							
							$sheet->getStyle($jj++.'6:'.$jj++.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('bd78ff');
							$jj-=2;
						foreach($adj_minus_result as $row3){
							$sheet->setCellValueByColumnAndRow($jj++,6, strtoupper($row3->adj)); 
							$cell = $this->columnLetter($jj);
 							$sheet->getStyle($cell.'6:'.$cell.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('bd78ff');
							$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setVisible(false);
						}
							$sheet->setCellValueByColumnAndRow($jj++,6, "TOTAL LOAN MINUS");
							$totalLoanCol = $this->columnLetter($jj);
							$totalLoanCol2 = $this->columnLetter($jj+2);
							$sheet->getStyle($totalLoanCol.'6:'.$totalLoanCol2.'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');

							

						// foreach(range('A','ZZ') as $columnID) {
							// $sheet->getColumnDimension($columnID)->setAutoSize(true);
							// // $sheet->getColumnDimension($columnID)->setWidth(25);
 						// }
						
							$sheet->setCellValueByColumnAndRow($jj++,6,"13th MONTH PAY");
							// $sheet->setCellValueByColumnAndRow($jj++,6,"basic");	  
							// $sheet->setCellValueByColumnAndRow($jj++,6,"totalAdj");	  
							// $sheet->setCellValueByColumnAndRow($jj++,6,"totalBonus");	  
							// $sheet->setCellValueByColumnAndRow($jj++,6,"govDeduct");	  
							// $sheet->setCellValueByColumnAndRow($jj++,6,"totalDeduct");	
							// $sheet->setCellValueByColumnAndRow($jj++,6,"ISSUANCE");
							$sheet->setCellValueByColumnAndRow($jj++,6,"ISSUANCE");
						foreach($this->excelColumnRange('A','ZZ') as $column) {
							$sheet->getColumnDimension($column)->setAutoSize(true);
						}
					$rows =7;
					$s =7;
					$lastRow = 1;
					$finalAmount = 0;
					$finalNdAmount = 0;
					$finalHpAmount = 0;
					$finalAhrAmount= 0;
					$finalHoAmount = 0;
					$finalAdjAmount = 0;
					$finalBonusAmount = 0;
					$finalDeductsAmount= 0;
					$finalLoansAmount = 0;
					$finalPaskoAmount = 0;
					$finalIssuanceAmount = 0;


					foreach($query->result() as $row_rs){ 
					//	$ctr =0;
						$date = explode("-",$row_rs->daterange); 
						
						$sheet->setTitle(date("M d, Y",strtotime($date[0]))." - ".date("M d, Y",strtotime($date[1]))); 
						$stat = Array("","Trainee","Probationary","Regular","Part-time","On-The-Job Trainee");
						$sheet->setCellValue('A'.$rows, $row_rs->dep_name); 
						$sheet->setCellValue('B'.$rows, $row_rs->acc_name); 
						$sheet->setCellValue('C'.$rows, $row_rs->id_num);
						$sheet->setCellValue('D'.$rows, $row_rs->lname.", ".$row_rs->fname);
						$sheet->setCellValue('E'.$rows, $row_rs->mname);
						$sheet->setCellValue('F'.$rows, $row_rs->pos_details);
						$sheet->setCellValue('G'.$rows, $stat[(int)$row_rs->empstat_id]);
						$sheet->setCellValue('H'.$rows, $row_rs->quinsina);
						$sheet->setCellValue('I'.$rows, $row_rs->nd);
						$sheet->setCellValue('J'.$rows, $row_rs->hp);
						$sheet->setCellValue('K'.$rows, $row_rs->ahr_amount);
						$sheet->setCellValue('L'.$rows, $row_rs->ho); 
						$sheet->setCellValue('M'.$rows, $row_rs->clothing); 
						$sheet->setCellValue('N'.$rows, $row_rs->laundry); 
						$sheet->setCellValue('O'.$rows, $row_rs->rice); 
						$sheet->getStyle('A'.$s.':O'.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');
						
						$jj =16;
						
							$totalAdj = 0;
							$arrAddAdj = array();
								if(trim($row_rs->adjust_details)!="0" || trim($row_rs->adjust_details)!=0){
									$adjusted = explode(",",$row_rs->adjust_details);
									
									for($i = 0; $i<count($adjusted); $i++){
											$orig_add_break = explode("|",$adjusted[$i]);
											$orig_add_break_final = explode("=",$orig_add_break[1]);
											$arrAddAdj[strtolower(trim($orig_add_break_final[0]))] = $orig_add_break_final[1];
											 
									}
								}
								foreach($adj_add_result as $row_add){
										if(array_key_exists(strtolower(trim($row_add->adj)),$arrAddAdj)){
											$sheet->setCellValueByColumnAndRow($jj++,$s, $arrAddAdj[strtolower(trim($row_add->adj))]);
											$totalAdj+=(float)$arrAddAdj[strtolower(trim($row_add->adj))];
											$add = $arrAddAdj[strtolower(trim($row_add->adj))];
										}else{
											$sheet->setCellValueByColumnAndRow($jj++,$s, 0);
											$add = 0;
										}
										// $finalArr["adjust_details-".$ctr] += $add; 
									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fff8cf');
								}	
								
								
						$arrBonusAdj = array();	
						$sheet->setCellValueByColumnAndRow($jj++,$s, $totalAdj);
						$col1 = $this->columnLetter($jj);
						$sheet->getStyle($col1.$s.':'.$col1.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');

							$totalBonus = 0;
							if(trim($row_rs->bonus_details)!="0" || trim($row_rs->bonus_details)!=0){
								$bonus = explode(",",$row_rs->bonus_details);
								
								for($i = 0; $i<count($bonus); $i++){
										$orig_bonus_final = explode("=",$bonus[$i]);
										$arrBonusAdj[strtolower(trim($orig_bonus_final[0]))] = $orig_bonus_final[1];
								} 
							}
							foreach($adj_bonus_result as $row_bon){
									if(array_key_exists(strtolower(trim($row_bon->bonus_name)),$arrBonusAdj)){
										$sheet->setCellValueByColumnAndRow($jj++,$s, $arrBonusAdj[strtolower(trim($row_bon->bonus_name))]);
										$totalBonus+=(float)$arrBonusAdj[strtolower(trim($row_bon->bonus_name))];
									}else{
										$sheet->setCellValueByColumnAndRow($jj++,$s, 0);
									}
									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('defffc');
							}
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$totalBonus); 
							$col1 = $this->columnLetter($jj);
							$col2 = $this->columnLetter($jj+8);
							$sheet->getStyle($col1.$s.':'.$col2.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');

							$sheet->setCellValueByColumnAndRow($jj++,$s, (float)$row_rs->employer_sss); 
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->employer_sss -  (float)$row_rs->sss_details);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->employer_hdmf);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->employer_phic);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->bir_details);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->sss_details);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->hdmf_details);
							$sheet->setCellValueByColumnAndRow($jj++,$s,  (float)$row_rs->phic_details);
							
							$arrDeductAdj = array();	
							$arrDeductLoanAdj = array();	
							if(trim($row_rs->deduct_details)!="0" || trim($row_rs->deduct_details)!=0){
								$deduct = explode(",",$row_rs->deduct_details);
								
								for($i = 0; $i<count($deduct); $i++){
										$orig_deduct_final = explode("=",$deduct[$i]);
										$arrDeductAdj[strtolower(trim($orig_deduct_final[0]))] = $orig_deduct_final[1];
								} 
							}
							if(trim($row_rs->loan_details)!="0" || trim($row_rs->loan_details)!=0){
								$deductLoan = explode(",",$row_rs->loan_details);
								
								for($i = 0; $i<count($deductLoan); $i++){
										$orig_deductLoan_final = explode("=",$deductLoan[$i]);
										$arrDeductLoanAdj[strtolower(trim($orig_deductLoan_final[0]))] = $orig_deductLoan_final[1];
								} 
							}
							$total_dtr_deduction = 0;
							$totalDeduct = 0;
							
							
								foreach($adj_minus_result as $row_deduct){
									if(array_key_exists(strtolower(trim($row_deduct->adj)),$arrDeductAdj)){
										$sheet->setCellValueByColumnAndRow($jj++,$s, $arrDeductAdj[strtolower(trim($row_deduct->adj))]);
										if(strtolower(trim($row_deduct->adj))=="undertime"){
											$total_dtr_deduction += (float)$arrDeductAdj[strtolower(trim($row_deduct->adj))]; 
										}
										$totalDeduct+=(float)$arrDeductAdj[strtolower(trim($row_deduct->adj))];
									}else{
										$sheet->setCellValueByColumnAndRow($jj++,$s, 0);
									}
									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6ccff');
								}
								$sheet->setCellValueByColumnAndRow($jj++,$s, $row_rs->absentLate);
									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6ccff');

								$totalDeduct  += (float)$row_rs->absentLate;
								$sheet->setCellValueByColumnAndRow($jj++,$s, $totalDeduct);
								$col1 = $this->columnLetter($jj);
								$sheet->getStyle($col1.$s.':'.$col1.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');

								$totalLoanDeduct = 0;
								foreach($adj_minus_result as $row_deduct_loan){
									if(array_key_exists(strtolower(trim($row_deduct_loan->adj)),$arrDeductLoanAdj)){
										$sheet->setCellValueByColumnAndRow($jj++,$s, $arrDeductLoanAdj[strtolower(trim($row_deduct_loan->adj))]);
										$totalLoanDeduct+=(float)$arrDeductLoanAdj[strtolower(trim($row_deduct_loan->adj))]; 
									}else{
										$sheet->setCellValueByColumnAndRow($jj++,$s, 0);
									}
									$cell = $this->columnLetter($jj);
									$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fabbf2');
								}
								$sheet->setCellValueByColumnAndRow($jj++,$s,$totalLoanDeduct);
								$col1 = $this->columnLetter($jj);
								$col2 = $this->columnLetter($jj+2);
								$sheet->getStyle($col1.$s.':'.$col2.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('e6e6e6');

								$sheet->getStyle($cell.$s.':'.$cell.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fabbf2');
								$sheet->getStyle('A'.$s.':M'.$s)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								
								$pasko13MonthPay = 0;
								$issuance = 0;
								$basic = 0;
								$govDeduct = 0; 
								$pasko13MonthPay = (float)$row_rs->quinsina-(float)$total_dtr_deduction-(float)$row_rs->absentLate;
								
								$basic = (float)$row_rs->quinsina + (float)$row_rs->clothing + (float)$row_rs->laundry + (float)$row_rs->rice + (float)$row_rs->ho + (float)$row_rs->nd + (float)$row_rs->ahr_amount + (float)$row_rs->hp;
								$govDeduct = (float)$row_rs->sss_details +(float)$row_rs->phic_details +(float)$row_rs->hdmf_details +(float)$row_rs->bir_details;
								$issuance = ($basic + $totalAdj + $totalBonus) - ($totalDeduct +$totalLoanDeduct + $govDeduct); 
							$sheet->setCellValueByColumnAndRow($jj++,$s,$pasko13MonthPay);	 
							$totalPaskoCol = $this->columnLetter($jj);
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$basic);	  
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$totalAdj);	  
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$totalBonus);	  
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$govDeduct);	  
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$totalDeduct);	   
							// $sheet->setCellValueByColumnAndRow($jj++,$s,$issuance);	  
							$sheet->setCellValueByColumnAndRow($jj++,$s,$row_rs->final);	  
							$totalIssuanceCol = $this->columnLetter($jj);
							
						$s++;
						$rows++;
						$lastRow = $jj;

						$finalAmount += (float)$row_rs->quinsina;
						$finalNdAmount += (float)$row_rs->nd;
						$finalHpAmount += (float)$row_rs->hp;
						$finalAhrAmount += (float)$row_rs->ahr_amount;
						$finalHoAmount += (float)$row_rs->ho;
						$finalAdjAmount += (float)$totalAdj;
						$finalBonusAmount += (float)$totalBonus;
						$finalDeductsAmount += (float)$totalDeduct;
						$finalLoansAmount += (float)$totalLoanDeduct;
						$finalPaskoAmount += (float)$pasko13MonthPay;
						$finalIssuanceAmount += (float)$row_rs->final;
									

					} 
					//Starts of total below...
						 

					$sheet->setCellValueByColumnAndRow(7,$rows,$finalAmount);
					$sheet->setCellValueByColumnAndRow(8,$rows,$finalNdAmount);
					$sheet->setCellValueByColumnAndRow(9,$rows,$finalHpAmount);
					$sheet->setCellValueByColumnAndRow(10,$rows,$finalAhrAmount);
					$sheet->setCellValueByColumnAndRow(11,$rows,$finalHoAmount);
					$sheet->setCellValue($totalAdjustCol.$rows,$finalAdjAmount);
					$sheet->setCellValue($totalBonusCol.$rows,$finalBonusAmount);
					$sheet->setCellValue($totalDeductCol.$rows,$finalDeductsAmount);
					$sheet->setCellValue($totalLoanCol.$rows,$finalLoansAmount);
					$sheet->setCellValue($totalPaskoCol.$rows,$finalPaskoAmount);
					$sheet->setCellValue($totalIssuanceCol.$rows,$finalIssuanceAmount);
						$superFinalAmount 		 	+= (float)$finalAmount;
						$superFinalNdAmount 	 	+= (float)$finalNdAmount;
						$superFinalHpAmount 	 	+= (float)$finalHpAmount;
						$superFinalAhrAmount	 	+= (float)$finalAhrAmount;
						$superFinalHoAmount 	 	+= (float)$finalHoAmount;
						$superFinalAdjAmount 	 	+= (float)$finalAdjAmount;
						$superFinalBonusAmount	 	+= (float)$finalBonusAmount;
						$superFinalDeductsAmount 	+= (float)$finalDeductsAmount;
						$superFinalLoansAmount 	 	+= (float)$finalLoansAmount;
						$superFinalPaskoAmount 	 	+= (float)$finalPaskoAmount;
						$superFinalIssuanceAmount 	+= (float)$finalIssuanceAmount;

					$ctr++;
				 
						

			} 
		//this start sa additional woorksheet == Total count
				$objPHPExcel->createSheet();
				$sheet_final = $objPHPExcel->setActiveSheetIndex($ctr++);
				$sheet_final->setTitle("Summary"); 
		
				//-------------------------------------------------------------------
						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName('Logo');
						$objDrawing->setDescription('Logo');
						$objDrawing->setPath($this->logo);
						$objDrawing->setOffsetX(0);    // setOffsetX works properly
						$objDrawing->setOffsetY(0);  //setOffsetY has no effect
						$objDrawing->setCoordinates('A1');
						$objDrawing->setResizeProportional(false);
						// set width later
						$objDrawing->setWidth(100);
						$objDrawing->setHeight(50);
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					//----------------------------------------------------------------------
				$sheet_final->getStyle('A6:B6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a1a1a1');
				$sheet_final->setCellValue('A6',"Items");
				$sheet_final->setCellValue('A7',"Total Basic");
				$sheet_final->setCellValue('A8',"Total Night Differential");
				$sheet_final->setCellValue('A9',"Total Hazard Pay");
				$sheet_final->setCellValue('A10',"Total Holiday");
				$sheet_final->setCellValue('A11',"Total AHR");
				$sheet_final->setCellValue('A12',"Total Additional Adjustment");
				$sheet_final->setCellValue('A13',"Total Allowance/Bonus");
				$sheet_final->setCellValue('A14',"Total Deduction Adjustment");
				$sheet_final->setCellValue('A15',"Total Loan Deduction");
				$sheet_final->setCellValue('A16',"Total 13th Month Pay");
				$sheet_final->setCellValue('A17',"Total Issuance");
				
				$sheet_final->setCellValue('B6',"Amount");
				$sheet_final->setCellValue('B7',$superFinalAmount);
				$sheet_final->setCellValue('B8',$superFinalNdAmount);
				$sheet_final->setCellValue('B9',$superFinalHpAmount);
				$sheet_final->setCellValue('B10',$superFinalHoAmount);
				$sheet_final->setCellValue('B11',$superFinalAhrAmount);
				$sheet_final->setCellValue('B12',$superFinalAdjAmount);
				$sheet_final->setCellValue('B13',$superFinalBonusAmount);
				$sheet_final->setCellValue('B14',$superFinalDeductsAmount);
				$sheet_final->setCellValue('B15',$superFinalLoansAmount);
				$sheet_final->setCellValue('B16',$superFinalPaskoAmount);
				$sheet_final->setCellValue('B17',$superFinalIssuanceAmount);
				foreach($this->excelColumnRange('A','B') as $column) {
					$sheet_final->getColumnDimension($column)->setAutoSize(true);
				}
				
		$file = 'PAYROLL_ATM_REPORT.xlsx'; 
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		/* $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output') */;	
		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		ob_start();
		$objWriter->save("php://output");
		$xlsData = ob_get_contents();
		ob_end_clean();		
			

		$response =  array(
			'op' => 'ok', 
			'site' => explode(",",$site),  
			'cat' => explode(",",$cat), 
			'type' => $type, 
			'typeSite' => $typeSite,
			'sql' => $sql,
			'rs' => $query->result(),
			'coverage' => explode(",",$coverage), 
			'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
		);

	die(json_encode($response));

	}
	function excelColumnRange($lower, $upper) {
		++$upper;
		for ($i = $lower; $i !== $upper; ++$i) {
			yield $i;
		}
	}
	function columnLetter($c){

			$c = intval($c);
			if ($c <= 0) return '';

			$letter = '';

			while($c != 0){
			$p = ($c - 1) % 26;
			$c = intval(($c - $p) / 26);
			$letter = chr(65 + $p) . $letter;
			}

			return $letter;

		}
		public function saveFinalPayroll(){
		
		$data=json_decode($this->input->post("dataArray"));
		$coverage_id=$this->input->post("coverage_id");
		$site=$this->input->post("site");
		$arr= array();
		$ahr_id= array();
		$leave_id= array();
  		  // echo json_encode($data);
		  // exit();
		 // $cols =array_column($data, 'payroll_id');
		 // var_dump($cols);
 		 // exit();
		 // if(count($data)>0){
		 $rs = 0;
			 foreach($data as $key1 => $val){
				 if($val!=null){
					  $arr= array();
				 $delete_payroll =0;
				foreach($val as $key2 => $item){
					$record = explode("---",$item);
					if(trim($record[0])=="payroll_id"){
						$where['payroll_id'] =$record[1];
						$table =  "tbl_payroll" ;
						$delete_payroll = $this->general_model->delete_vals($where, $table); 
					}
					$arr[$key1][$record[0]] = $record[1];
				}
				$arr[$key1]["coverage_id"] = $coverage_id;
				 if($delete_payroll>0){
							 $rs = $this->general_model->batch_insert($arr, 'tbl_payroll');
							
						 }
				 }
				
			 }
			  // $rs = $this->general_model->batch_insert($arr, 'tbl_payroll');
			 // echo json_encode($arr);
		  
		  echo $rs;
		
		
	}
}
 
