<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Kudos extends General
{

    protected $title = 'Kudos';
   public $logo = "/var/www/html/sz/assets/images/img/logo2.png";
     // public $logo = "C:\\wamp64\\www\\sz\\assets\\images\\img\\logo2.png";
    public function __construct()
    {
        parent::__construct();
    }



    //              --- Start General Function ---            //

    public function uploadscreenshot(){
        date_default_timezone_set('Asia/Manila'); 
        $kudosid= $this->input->post('kudosid');
        $type_entry = $this->input->post('type');
        
        $path = $_FILES['imagefile']["name"];
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $file_sc = 'uploads/kudos/'.date("Ymdhms.").$extension;
        $new_name = date("Ymdhms").".".$extension;
        $target_dir = "./uploads/kudos/";
        $target_file = $target_dir.$new_name;

        $type = $_FILES['imagefile']["type"];
        $allowed = array('image/jpeg', 'image/png', 'image/gif');
        if($path){
            if(!in_array($type,$allowed)){
                echo 'Failed';
            } else {
                if(move_uploaded_file($_FILES['imagefile']["tmp_name"],$target_file)){
                    $data = array();
                    if($type_entry == 'kudos_card'){
                        $data = array(
                            "kudos_card_pic" => $file_sc
                        );
                    }else if($type_entry == 'screenshot'){
                        $data = array(
                            "file" => $file_sc
                        );
                    }else{
                        $data = array();
                    }
                    
                    $row = $this->general_model->update_vals($data, "kudos_id=$kudosid", "tbl_kudos");
                    if($row>0){
                        echo "Success";
                    }else{
                        echo 'Failed';
                    }
                }else{
                    echo 'Failed';
                }
            }
        }
    }

    private function get_accounts()
    {
        $query = $this->general_model->custom_query("SELECT acc_id, UPPER(acc_name) as account FROM tbl_account WHERE acc_description LIKE '%Agent%' ORDER BY acc_name ASC");
        return $query;
    }

    private function get_kudos_count()
    {
        date_default_timezone_set('Asia/Manila');
        $year=date("Y");
        $query = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count FROM tbl_kudos WHERE YEAR(added_on)='".$year."' GROUP BY month ORDER BY month_number ASC");
        return $query;
    }

    private function get_kudos_top()
    {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->general_model->custom_query("SELECT DATE_FORMAT(added_on, '%m') AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid WHERE MONTH(added_on)='".$month."' AND YEAR(added_on)='".$year."' GROUP BY ambassador ORDER BY kudos_count DESC LIMIT 5");
        return $query;
    }

    private function get_kudos_all_agents()
    {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid WHERE MONTH(added_on)='".$month."' AND YEAR(added_on)='".$year."' GROUP BY ambassador ORDER BY kudos_count DESC");
        return $query;
    }

    private function get_kudos_top_campaigns()
    {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->general_model->custom_query("SELECT COUNT(*) AS kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY campaign ORDER BY kudos_count DESC LIMIT 5");
        return $query;
    }

    private function get_kudos_drilldown()
    {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->general_model->custom_query("SELECT COUNT(*) AS kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY ambassador ORDER BY kudos_count DESC");
        return $query;
    }

    private function get_kudos_drilldown2()
    {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->general_model->custom_query("SELECT COUNT(*) as kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid GROUP BY month, ambassador ORDER BY kudos_count ASC");
        return $query;
    }

    private function get_kudos_by_campaigns()
    {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->general_model->custom_query("SELECT COUNT(*) AS kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, UPPER(tbl_account.acc_name) AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY campaign ORDER BY kudos_count DESC");
        return $query;
    }

    private function check_recipient($current_uri)
    {
        $record = $this->general_model->fetch_specific_vals(
            "_access.user_id",
            "_user.uid=_access.user_id AND _user.emp_id=emp.emp_id AND emp.apid=app.apid AND _access.menu_item_id=_menu.menu_item_id AND _menu.item_link LIKE '%$current_uri%' AND emp.isActive='yes' AND _access.is_assign=1 AND _menu.isNewTemplate=1",
            "tbl_user _user, tbl_applicant app, tbl_employee emp, tbl_menu_items _menu, tbl_user_access _access"
        );
        return $record;
    }

    //              --- End General Function ---            //


    //              --- Start Display Page ---            //

    public function kudos_add()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Kudos Add',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/kudos_add', $data);
        }
    }

    public function kudos_all(){
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'List of Kudos',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/kudos_all', $data);
        }
        
    }

    public function kudos_proofread(){
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Proofreading Kudos',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/kudos_proofread', $data);
        }
        
    }

    public function kudos_release(){
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Cash Release',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/kudos_release', $data);
        }
        
    }

    public function kudos_approve(){
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Cash Approve',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/kudos_approve', $data);
        }
        
    }

    public function kudos_request(){
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Request',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/kudos_request', $data);
        }
        
    }

    public function kudos_card(){
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Kudos Card',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/kudos_card', $data);
        }
        
    }

    public function kudos_notify()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Notification',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/kudos_notify', $data);
        }
        
    }

    public function report3View()
    {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $drilldown3 = $this->general_model->custom_query("SELECT COUNT(*) as kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, UPPER(tbl_account.acc_name) AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY month, ambassador ORDER BY kudos_count ASC");
        $data = [
            'acc'               => $this->get_accounts(),
            'drilldown3'        => $drilldown3,
            'campaign_kcount'   => $this->get_kudos_by_campaigns(),
            'uri_segment'       => $this->uri->segment_array(),
            'title'             => 'Report - Campaign',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/report3View', $data);
        }
        
    }

    public function report2View()
    {
        $data = [
            'all'               => $this->get_kudos_all_agents(),
            'uri_segment'       => $this->uri->segment_array(),
            'title'             => 'Report - Ambassador',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/report2View', $data);
        }
        
    }

    public function report1View()
    {
        $data = [
            'acc'               => $this->get_accounts(),
            'k_count'           => $this->get_kudos_count(),
            'top'               => $this->get_kudos_top(),
            'all'               => $this->get_kudos_all_agents(),
            'top_campaigns'     => $this->get_kudos_top_campaigns(),
            'drilldown'         => $this->get_kudos_drilldown(),
            'drilldown2'        => $this->get_kudos_drilldown2(),
            'campaign_kcount'   => $this->get_kudos_by_campaigns(),
            'uri_segment'       => $this->uri->segment_array(),
            'title'             => 'Report - Monthly',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/kudos/report1View', $data);
        }
        
    }

    //              --- End Display Page ---            //


    //              --- Start Unique Function ---            //

    public function kudos_add_account()
    {
        $accounts = '';
        $accounts_query = $this->general_model->fetch_specific_vals("acc_id,acc_name", "acc_description='Agent'","tbl_account","acc_name ASC");
        foreach($accounts_query as $key){
            $accounts .= "<option value=".$key->acc_id.">".$key->acc_name."</option>";
        }
        
        echo json_encode($accounts);
    }

    public function kudos_add_ambassador()
    {
        $acc_id = $this->input->post('acc_id');
        $employee = '';
        $employee_query = $this->general_model->fetch_specific_vals("e.emp_id,a.lname,a.fname", "e.apid=a.apid AND e.isActive = 'yes' AND e.acc_id=$acc_id","tbl_employee e, tbl_applicant a","a.lname ASC");
        foreach($employee_query as $key){
            $employee .= "<option value=".$key->emp_id.">".$key->lname.", ".$key->fname."</option>";
        }
        echo json_encode($employee);
    }

    public function kudos_add_ambassadorv2($acc_id)
    {
        $employee = '';
        $employee_query = $this->general_model->fetch_specific_vals("e.emp_id,a.lname,a.fname", "e.apid=a.apid AND e.isActive = 'yes' AND e.acc_id=$acc_id","tbl_employee e, tbl_applicant a","a.lname ASC");
        foreach($employee_query as $key){
            $employee .= "<option value=".$key->emp_id.">".$key->lname.", ".$key->fname."</option>";
        }
        return $employee;
    }

    public function kudos_add_data()
    {
        $uid = $this->session->uid;
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by , a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND a.reward_status!='Released' AND a.reward_status!='Reject' AND a.kudos_card!='Done' AND a.proofreading!='Done' AND $uid=a.added_by";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
            }
            
        }
        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);

    }

    public function kudos_add_modal()
    {
        $kudos_id = $this->input->post('kudos_id');
        $query = $this->general_model->custom_query(
            "SELECT b.pic,b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, DATE_FORMAT(a.requested_on, '%M %d %Y %h:%i %p') AS requested_on, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file, a.is_new, h.pos_details, i.status FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d, tbl_employee e, tbl_emp_promote f, tbl_pos_emp_stat g, tbl_position h, tbl_emp_stat i WHERE i.empstat_id=g.empstat_id AND h.pos_id=g.pos_id AND f.posempstat_id=g.posempstat_id AND f.emp_id=e.emp_id AND a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND f.isActive=1 AND f.emp_id=c.emp_id AND a.kudos_id=$kudos_id");
        $ambassador = $this->kudos_add_ambassadorv2($query[0]->acc_id);
        $old_image_link = 'http://3.18.221.50/supportzebra/';
        echo json_encode(['modal' => $query[0], 'ambassador' => $ambassador, 'old_image_link' => $old_image_link]);

    }

    public function kudos_all_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT b.pic,b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file, h.pos_details, i.status FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d, tbl_employee e, tbl_emp_promote f, tbl_pos_emp_stat g, tbl_position h, tbl_emp_stat i WHERE i.empstat_id=g.empstat_id AND h.pos_id=g.pos_id AND f.posempstat_id=g.posempstat_id AND f.emp_id=e.emp_id AND a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND f.isActive=1 AND f.emp_id=c.emp_id";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
            }
            
        }

        if ($datatable['query']['start'] != '' && $datatable['query']['end'] != '') {
            $start = $datatable['query']['start'];
            $end = $datatable['query']['end'];
            if($start != $end){
                $query['search']['append'] .= " AND a.added_on BETWEEN '$start' and '$end'";
                $query['search']['total'] .= " AND a.added_on BETWEEN '$start' and '$end'";
            }else{
                $query['search']['append'] .= " AND a.added_on LIKE '%$start%'";
                $query['search']['total'] .= " AND a.added_on LIKE '%$start%'";
            }
        }

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);

    }

    public function kudos_add_update_data()
    {
        $kudos_id = $this->input->post('kudos_id');
        $query = $this->general_model->custom_query(
            "SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by , a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND a.reward_status!='Released' AND a.kudos_card!='Done' AND a.proofreading!='Done' AND a.kudos_id=$kudos_id"
        );
        $account = '';
        $ambassador = '';
        $reward = '';
        $type = '';
        $client_name = '';
        foreach($query as $value){

        }
    }

    public function kudos_proofread_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT b.pic,b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file, h.pos_details, i.status FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d, tbl_employee e, tbl_emp_promote f, tbl_pos_emp_stat g, tbl_position h, tbl_emp_stat i WHERE i.empstat_id=g.empstat_id AND h.pos_id=g.pos_id AND f.posempstat_id=g.posempstat_id AND f.emp_id=e.emp_id AND a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND f.isActive=1 AND f.emp_id=c.emp_id AND a.proofreading='Pending'";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
            }
        }

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);

    }

    public function create_single()
    {
        $user_name = $this->session->fname." ".$this->session->lname;
        date_default_timezone_set('Asia/Manila'); 
        $array = array(
            'emp_id'        => $this->input->post('create_employee'),
            'acc_id'        => $this->input->post('create_accounts'),
            'kudos_type'    => $this->input->post('create_type'),
            'client_name'   => $this->input->post('create_name'),
            'phone_number'  => $this->input->post('client_phone'),
            'client_email'  => $this->input->post('client_email'),
            'comment'       => $this->input->post('comment'),
            'added_by'      => $this->session->uid,
            'reward_type'   => $this->input->post('create_reward'),
            'proofreading'  => 'Pending',
            'kudos_card'    => 'Pending',
            'reward_status' => 'Pending',
            'added_on'      => date("Y-m-d H:i:s"),
            'added_via'     => 'Manual',
            'is_new'        => 1
        );

        $qry = $this->general_model->insert_vals_last_inserted_id($array, 'tbl_kudos');
        $proofread_notif1 = array(
            'message'       => $user_name.' created a new kudos that needs to be proofread!',
            'link'          => 'kudos/kudos_proofread',
            'reference'     => 'general',
        );
        $request_notif1 = array(
            'message'       => $user_name. 'created a new kudos that needs to be request!',
            'link'          => 'kudos/kudos_request',
            'reference'     => 'general',
        );
        $notif_id = array();
        $systemNotification_ID1 = $this->general_model->insert_vals_last_inserted_id($proofread_notif1, 'tbl_system_notification');
        $systemNotification_ID2 = $this->general_model->insert_vals_last_inserted_id($request_notif1, 'tbl_system_notification');
        $recipient1 = $this->check_recipient('kudos_proofread');
        $recipient2 = $this->check_recipient('kudos_request');
        array_push($notif_id, $systemNotification_ID1, $systemNotification_ID2);
        foreach($recipient1 as $key => $value){
            $proofread_notif2 = array(
                'systemNotification_ID'     => $systemNotification_ID1,
                'recipient_ID'              => $value->user_id,
                'status_ID'                 => '8'
            );
            $this->general_model->insert_vals($proofread_notif2, "tbl_system_notification_recipient");
        }
        foreach($recipient2 as $key => $value){
            $request_notif2 = array(
                'systemNotification_ID'     => $systemNotification_ID2,
                'recipient_ID'              => $value->user_id,
                'status_ID'                 => '8'
            );
            $this->general_model->insert_vals($request_notif2, "tbl_system_notification_recipient");
        }
        echo json_encode(['qry'=>$qry, 'notif_id'=>$notif_id]);
    }

    public function update_kudos_data()
    {
        date_default_timezone_set('Asia/Manila'); 
        $kudos_id = $this->input->post('upkudos_id');
        $array = array(
            'emp_id'         => $this->input->post('upemp_id'),
            'acc_id'         => $this->input->post('upacc_id'),
            'kudos_type'     => $this->input->post('upkudos_type'),
            'client_name'    => $this->input->post('upclient_name'),
            'phone_number'   => $this->input->post('upclient_phone'),
            'client_email'   => $this->input->post('upclient_email'),
            'comment'        => $this->input->post('upcomment'),
            'updated_by'     => $this->session->emp_id,
            'updated_on'     => date("Y-m-d H:i:s"),
            'reward_type'    => $this->input->post('upreward_type'),
            'is_new'         => 1
        );
        $qry = $this->general_model->update_vals($array, "kudos_id=$kudos_id", "tbl_kudos");
        echo json_encode($qry);
    }

    public function update_revision()
    {
        date_default_timezone_set('Asia/Manila'); 
        $kudos_id = $this->input->post('kudosid');
        $emp = $this->session->emp_id;
        $emp_id = $this->general_model->fetch_specific_val("a.lname, a.fname","a.apid=e.apid AND e.emp_id=$emp", "tbl_employee e, tbl_applicant a");
        $array = array(
            'revision'         => $this->input->post('revision'),
            'revised_on'       => date("Y-m-d H:i:s"),
            'revised_by'       => $emp_id->fname.' '.$emp_id->lname,
            'proofreading'     => 'Done',
        );
        $qry = $this->general_model->update_vals($array, "kudos_id=$kudos_id", "tbl_kudos");
        $card_notif1 = array(
            'message'       => 'There is a kudos that has already been proofread and needs a kudos card!',
            'link'          => 'kudos/kudos_card',
            'reference'     => 'general',
        );
        $request_notif1 = array(
            'message'       => 'There are new kudos that need to request!',
            'link'          => 'kudos/kudos_request',
            'reference'     => 'general',
        );
        $notif_id = array();
        $systemNotification_ID1 = $this->general_model->insert_vals_last_inserted_id($card_notif1, 'tbl_system_notification');
        $systemNotification_ID2 = $this->general_model->insert_vals_last_inserted_id($request_notif1, 'tbl_system_notification');
        $recipient1 = $this->check_recipient('kudos_card');
        $recipient2 = $this->check_recipient('kudos_request');
        array_push($notif_id, $systemNotification_ID1, $systemNotification_ID2);
        foreach($recipient1 as $key => $value){
            $proofread_notif2 = array(
                'systemNotification_ID'     => $systemNotification_ID1,
                'recipient_ID'              => $value->user_id,
                'status_ID'                 => '8'
            );
            $this->general_model->insert_vals($proofread_notif2, "tbl_system_notification_recipient");
        }
        foreach($recipient2 as $key => $value){
            $request_notif2 = array(
                'systemNotification_ID'     => $systemNotification_ID2,
                'recipient_ID'              => $value->user_id,
                'status_ID'                 => '8'
            );
            $this->general_model->insert_vals($request_notif2, "tbl_system_notification_recipient");
        }
        echo json_encode(['qry'=>$qry, 'notif_id'=>$notif_id]);
    }

    private function kudos_all_excel_data($searchField,$start,$end)
    {

        $search_filter = '';
        if ($searchField != '') {
            $keyword = explode(' ',$searchField);
            for($x=0; $x<count($keyword); $x++){
                $search_filter .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
            }
            
        }

        $date_filter = '';
        if ($start != '' && $end != '') {
            $date_filter = " AND DATE(a.added_on) >= DATE('$start') AND a.added_on < DATE_ADD('$end', INTERVAL 1 DAY)";
        }

        $query = "SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id
        $search_filter $date_filter";
        
        $data = $this->general_model->custom_query($query);
        return array(
            'data' => $data,
            'currentDate'=>date('M d, Y'),
            'currentTime'=>date('h:i a'),
        );
    }

    public function downloadExcel_kudos()
    {
        $searchField    = $this->input->post('searchField');
        $start          = $this->input->post('start');
        $end            = $this->input->post('end');
        $final_data     = $this->kudos_all_excel_data($searchField,$start,$end);
        //EXCEL PROPER
        $this->load->library('PHPExcel', NULL, 'excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->createSheet(1);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Kudos');
        $this->excel->getActiveSheet()->setShowGridlines(false);
        //------------------------INSERT LOGO-------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(0);  //setOffsetY has no effect
        $objDrawing->setCoordinates('B1');
        $objDrawing->setHeight(100); // logo height

        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(38);
        //set cell A1 content with some text


        $this->excel->getActiveSheet()->setCellValue('F1', "Date Downloaded:");
        $this->excel->getActiveSheet()->setCellValue('F2', DATE('F d, Y h:i a'));
        $this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setWrapText(true); 

        $this->excel->getActiveSheet()->setCellValue('A3', 'Ambassador Name');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Campaign');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Kudos Type');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Kudos Card');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Reward Status');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Date Added');

        $this->excel->getActiveSheet()->mergeCells('A1:E2');

        $this->excel->getActiveSheet()->getStyle('A1:E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('F1:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('F2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
        $this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'ffffff'))));
        $this->excel->getActiveSheet()->getStyle('F2')->applyFromArray(array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => '000000'))));


        $this->excel->getActiveSheet()->setAutoFilter('A3:F3');
        $this->excel->getActiveSheet()->getProtection()->setSort(true);

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(28);

        $x = 4;$bool = TRUE;
        foreach ($final_data['data'] as $k) {

            if($bool==TRUE){
                $this->excel->getActiveSheet()->getStyle('A'.$x.':F'.$x)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'dddddd'))));
                $bool=FALSE;
            }else{
                $this->excel->getActiveSheet()->getStyle('A'.$x.':F'.$x)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'eeeeee'))));
                $bool=TRUE;
            }
            $this->excel->getActiveSheet()->setCellValue('A'.$x, $k->ambassador);
            $this->excel->getActiveSheet()->setCellValue('B'.$x, $k->campaign);
            $this->excel->getActiveSheet()->setCellValue('C'.$x, $k->kudos_type);
            $this->excel->getActiveSheet()->setCellValue('D'.$x, $k->kudos_card);
            $this->excel->getActiveSheet()->setCellValue('E'.$x, $k->reward_status);
            $this->excel->getActiveSheet()->setCellValue('F'.$x, $k->date_given);
            if($k->kudos_card=='Done'){
                $this->excel->getActiveSheet()->getStyle('D'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => '00CC00'))));
            }else{
                $this->excel->getActiveSheet()->getStyle('D'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => 'FF0000'))));
            }
            if($k->reward_status=='Released'){
                $this->excel->getActiveSheet()->getStyle('E'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => '00CC00'))));
            }else{
                $this->excel->getActiveSheet()->getStyle('E'.$x)->applyFromArray(array('font' => array('color' => array('rgb' => 'FF0000'))));
            }

            $x++;

        }
        $x--;

        $this->excel->getActiveSheet()->getStyle('A1:F'.$x)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));

        $filename = 'KudosList.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function kudos_release_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT b.pic,b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file, h.pos_details, i.status FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d, tbl_employee e, tbl_emp_promote f, tbl_pos_emp_stat g, tbl_position h, tbl_emp_stat i WHERE i.empstat_id=g.empstat_id AND h.pos_id=g.pos_id AND f.posempstat_id=g.posempstat_id AND f.emp_id=e.emp_id AND a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND f.isActive=1 AND f.emp_id=c.emp_id AND (a.reward_status='Approved' OR a.reward_status='On Hold')";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
            }
            
        }

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);

    }

    public function update_reward_status()
    {
        date_default_timezone_set('Asia/Manila'); 
        $kudos_id = $this->input->post('kudosid');
        $emp = $this->session->emp_id;
        $emp_id = $this->general_model->fetch_specific_val("a.lname, a.fname","a.apid=e.apid AND e.emp_id=$emp", "tbl_employee e, tbl_applicant a");
        $array = array(
            'released_by'           => $emp_id->fname.' '.$emp_id->lname,
            'released_on'           => date("Y-m-d H:i:s"),
            'reward_status'         => $this->input->post('reward'),
        );
        $qry = $this->general_model->update_vals($array, "kudos_id=$kudos_id", "tbl_kudos");
        $request_notif1 = array(
            'message'       => 'The kudos you requested has been released!',
            'link'          => 'kudos/kudos_all',
            'reference'     => 'general',
        );
        $notif_id = array();
        $systemNotification_ID1 = $this->general_model->insert_vals_last_inserted_id($request_notif1, 'tbl_system_notification');
        $recipient1 = $this->check_recipient('kudos_request');
        array_push($notif_id, $systemNotification_ID1);
        foreach($recipient1 as $key => $value){
            $proofread_notif2 = array(
                'systemNotification_ID'     => $systemNotification_ID1,
                'recipient_ID'              => $value->user_id,
                'status_ID'                 => '8'
            );
            $this->general_model->insert_vals($proofread_notif2, "tbl_system_notification_recipient");
        }
        echo json_encode(['qry'=>$qry, 'notif_id'=>$notif_id]);
    }

    public function kudos_approve_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT b.pic,b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file, h.pos_details, i.status FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d, tbl_employee e, tbl_emp_promote f, tbl_pos_emp_stat g, tbl_position h, tbl_emp_stat i WHERE i.empstat_id=g.empstat_id AND h.pos_id=g.pos_id AND f.posempstat_id=g.posempstat_id AND f.emp_id=e.emp_id AND a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND f.isActive=1 AND f.emp_id=c.emp_id AND a.reward_status='Requested'";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
            }
            
        }

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);

    }

    public function update_approve_status()
    {
        date_default_timezone_set('Asia/Manila'); 
        $kudos_id = $this->input->post('kudosid');
        $emp = $this->session->emp_id;
        $emp_id = $this->general_model->fetch_specific_val("a.lname, a.fname","a.apid=e.apid AND e.emp_id=$emp", "tbl_employee e, tbl_applicant a");
        $array = array(
            'approved_by'           => $emp_id->fname.' '.$emp_id->lname,
            'approved_on'           => date("Y-m-d H:i:s"),
            'reward_status'         => $this->input->post('reward'),
            'reject_note'           => $this->input->post('reject'),
        );
        $qry = $this->general_model->update_vals($array, "kudos_id=$kudos_id", "tbl_kudos");
        $request_notif1 = array(
            'message'       => 'There is a kudos that has been approved!',
            'link'          => 'kudos/kudos_release',
            'reference'     => 'general',
        );
        $notif_id = array();
        $systemNotification_ID1 = $this->general_model->insert_vals_last_inserted_id($request_notif1, 'tbl_system_notification');
        $recipient1 = $this->check_recipient('kudos_release');
        array_push($notif_id, $systemNotification_ID1);
        foreach($recipient1 as $key => $value){
            $proofread_notif2 = array(
                'systemNotification_ID'     => $systemNotification_ID1,
                'recipient_ID'              => $value->user_id,
                'status_ID'                 => '8'
            );
            $this->general_model->insert_vals($proofread_notif2, "tbl_system_notification_recipient");
        }
        echo json_encode(['qry'=>$qry, 'notif_id'=>$notif_id]);
    }

    public function kudos_request_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT b.pic,b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file, h.pos_details, i.status FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d, tbl_employee e, tbl_emp_promote f, tbl_pos_emp_stat g, tbl_position h, tbl_emp_stat i WHERE i.empstat_id=g.empstat_id AND h.pos_id=g.pos_id AND f.posempstat_id=g.posempstat_id AND f.emp_id=e.emp_id AND a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND f.isActive=1 AND f.emp_id=c.emp_id AND a.reward_status='Pending'";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
            }
            
        }

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);

    }

    public function update_request()
    {
        date_default_timezone_set('Asia/Manila'); 
        $kudos_id = $this->input->post('kudosid');
        $request_note = $this->input->post('request_note');
        $emp = $this->session->emp_id;
        $emp_id = $this->general_model->fetch_specific_val("a.lname, a.fname","a.apid=e.apid AND e.emp_id=$emp", "tbl_employee e, tbl_applicant a");
        $array = array(
            'request_note'          => $request_note,
            'requested_by'           => $emp_id->fname.' '.$emp_id->lname,
            'requested_on'           => date("Y-m-d H:i:s"),
            'reward_status'         => 'Requested',
        );
        $qry = $this->general_model->update_vals($array, "kudos_id=$kudos_id", "tbl_kudos");
        $approve_notif1 = array(
            'message'       => 'There is a kudos that has been requested!',
            'link'          => 'kudos/kudos_approve',
            'reference'     => 'general',
        );
        $notif_id = array();
        $systemNotification_ID1 = $this->general_model->insert_vals_last_inserted_id($approve_notif1, 'tbl_system_notification');
        $recipient1 = $this->check_recipient('kudos_approve');
        array_push($notif_id, $systemNotification_ID1);
        foreach($recipient1 as $key => $value){
            $proofread_notif2 = array(
                'systemNotification_ID'     => $systemNotification_ID1,
                'recipient_ID'              => $value->user_id,
                'status_ID'                 => '8'
            );
            $this->general_model->insert_vals($proofread_notif2, "tbl_system_notification_recipient");
        }
        echo json_encode(['qry'=>$qry, 'notif_id'=>$notif_id]);
    }

    public function kudos_card_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT b.pic,b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file, h.pos_details, i.status FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d, tbl_employee e, tbl_emp_promote f, tbl_pos_emp_stat g, tbl_position h, tbl_emp_stat i WHERE i.empstat_id=g.empstat_id AND h.pos_id=g.pos_id AND f.posempstat_id=g.posempstat_id AND f.emp_id=e.emp_id AND a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND f.isActive=1 AND f.emp_id=c.emp_id AND a.proofreading='Done' AND a.kudos_card!='Done'";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((b.lname LIKE '%$keyword[$x]%') OR (b.fname LIKE '%$keyword[$x]%') OR (d.acc_name LIKE '%$keyword[$x]%') OR (a.kudos_type LIKE '%$keyword[$x]%') OR (a.proofreading LIKE '%$keyword[$x]%') OR (a.kudos_card LIKE '%$keyword[$x]%') OR (a.reward_status LIKE '%$keyword[$x]%') OR (DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') LIKE '%$keyword[$x]%'))";
            }
            
        }

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);

    }

    public function update_kudos_card()
    {
        date_default_timezone_set('Asia/Manila'); 
        $kudos_id = $this->input->post('kudosid');
        $array = array(
            'kudos_card'         => 'Done',
        );

        $qry = $this->general_model->update_vals($array, "kudos_id=$kudos_id", 'tbl_kudos');
        echo json_encode($qry);
    }

    public function kudos_notify_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT a.ksetting_id,a.role,d.fname,d.lname,c.emp_id,e.pos_details,i.dep_name,i.dep_details,h.status, a.isActive FROM tbl_kudos_notify a,tbl_user b,tbl_employee c, tbl_applicant d, tbl_position e,tbl_emp_promote f,tbl_pos_emp_stat g,tbl_emp_stat h,tbl_department i,tbl_pos_dep j WHERE b.emp_id=c.emp_id AND c.apid=d.apid AND a.uid=b.uid AND a.isActive=1 AND c.isActive='yes' AND c.emp_id=f.emp_id AND f.posempstat_id=g.posempstat_id AND g.pos_id=e.pos_id AND g.empstat_id=h.empstat_id AND f.isActive=1 AND e.pos_id=j.pos_id AND j.dep_id=i.dep_id";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = explode(' ',$datatable['query']['searchField']);
            for($x=0; $x<count($keyword); $x++){
                $query['search']['append'] .= " AND ((d.fname LIKE '%$keyword[$x]%') OR (a.role LIKE '%$keyword[$x]%') OR (d.lname LIKE '%$keyword[$x]%') OR (e.pos_details LIKE '%$keyword[$x]%') OR (i.dep_name LIKE '%$keyword[$x]%') OR (i.dep_details LIKE '%$keyword[$x]%') OR (h.status LIKE '%$keyword[$x]%'))";
                $query['search']['total'] .= " AND ((d.fname LIKE '%$keyword[$x]%') OR (a.role LIKE '%$keyword[$x]%') OR (d.lname LIKE '%$keyword[$x]%') OR (e.pos_details LIKE '%$keyword[$x]%') OR (i.dep_name LIKE '%$keyword[$x]%') OR (i.dep_details LIKE '%$keyword[$x]%') OR (h.status LIKE '%$keyword[$x]%'))";
            }
            
        }
        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);

    }

    public function update_isActive()
    {
        date_default_timezone_set('Asia/Manila'); 
        $isActive = $this->input->post('isActive');
        $ksetting_id = $this->input->post('ksetting_id');
        $array = array(
            'isActive'         => $this->input->post('isActive'),
        );
        $qry = $this->general_model->update_vals($array, "ksetting_id=$ksetting_id", "tbl_kudos_notify");
        echo json_encode($qry);
    }

    public function notification_department()
    {
        $department = '';
        $department_query = $this->general_model->fetch_all("*","tbl_department","dep_details ASC");
        foreach($department_query as $key){
            $department .= "<option value=".$key->dep_id.">".$key->dep_details."(".$key->dep_name.")</option>";
        }
        
        echo json_encode($department);
    }

    public function notification_user()
    {
        $dep_id = ($this->input->post('dep_id') != '') ? $this->input->post('dep_id') : 0;
        $role = ($this->input->post('role') != '') ? $this->input->post('role') : '';
        $employee = '';
        $employee_query = $this->general_model->custom_query(
            "SELECT a.fname,a.lname,h.uid FROM tbl_applicant a,tbl_employee b, tbl_emp_promote c, tbl_pos_emp_stat d, tbl_position e, tbl_pos_dep f, tbl_department g, tbl_user h WHERE a.apid=b.apid AND b.emp_id=c.emp_id AND c.posempstat_id=d.posempstat_id AND d.pos_id=e.pos_id AND e.pos_id=f.pos_id AND f.dep_id=g.dep_id AND h.emp_id=b.emp_id AND b.isActive='yes' AND c.isActive=1 AND d.isActive=1 AND g.dep_id=$dep_id AND e.class='Admin' ORDER BY a.lname ASC"
        );
        $assign_user = $this->general_model->fetch_specific_vals("uid", "role='$role' AND isActive=1", "tbl_kudos_notify");
        if(count($assign_user) == ''){
            foreach($employee_query as $key){
                $employee .= "<option value=".$key->uid.">".$key->lname.", ".$key->fname."</option>";
            }
        }else{
            foreach($assign_user as $user){
                foreach($employee_query as $key){
                    if($key->uid != $user->uid){
                        $employee .= "<option value=".$key->uid.">".$key->lname.", ".$key->fname."</option>";
                    }
                }
            }
        }
        echo json_encode($employee);
    }

    public function add_new_user()
    {
        date_default_timezone_set('Asia/Manila'); 
        $array = array(
            'role'          => $this->input->post('role'),
            'isActive'      => 1,
            'uid'           => $this->input->post('user'),
        );

        $qry = $this->general_model->insert_vals($array, 'tbl_kudos_notify');
        echo json_encode($qry);
    }

    public function getDate()
    {
        $startDate = $this->input->post('fromDate');
        $endDate = $this->input->post('toDate');
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $data = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count FROM tbl_kudos WHERE added_on >= '".$startDate1." ".$startTime."' AND added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");
        header('Content-type: application/json'); 
        echo json_encode($data);
    }

    public function getDate2()
    {
        $startDate = $this->input->post('fromDate');
        $endDate = $this->input->post('toDate');
        $campaign = $this->input->post('campaign');

        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $campaign1 =mysqli_real_escape_string($this->db->conn_id,trim($campaign));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $data = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(tbl_kudos.added_on, '%m'), ' ',DATE_FORMAT(tbl_kudos.added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_account.acc_id=tbl_kudos.acc_id WHERE tbl_kudos.acc_id='".$campaign1."' AND tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");
        header('Content-type: application/json'); 
        echo json_encode($data);
    }

    public function getDate3()
    {
        $startDate = $this->input->post('fromDate');
        $endDate = $this->input->post('toDate');
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $data = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY ambassador ORDER BY kudos_count DESC");
        header('Content-type: application/json'); 
        echo json_encode($data);
    }

    public function getDate4()
    {
        $startDate = $this->input->post('fromDate');
        $endDate = $this->input->post('toDate');
        $campaign = $this->input->post('campaign');
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $campaign1 =mysqli_real_escape_string($this->db->conn_id,trim($campaign));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $data = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(tbl_kudos.added_on, '%m'), ' ',DATE_FORMAT(tbl_kudos.added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_account.acc_id=tbl_kudos.acc_id WHERE tbl_kudos.acc_id='".$campaign1."' AND tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");


        foreach($data as $d)
        {
            echo "<tr>";
            echo "<td>".$d->month."</td>";
            echo "<td>".$d->campaign."</td>";
            echo "<td>".$d->kudos_count."</td>";
            echo "</tr>";
        }
    }

    public function getDate5()
    {
        $startDate = $this->input->post('fromDate');
        $endDate = $this->input->post('toDate');
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $data = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count FROM tbl_kudos WHERE added_on >= '".$startDate1." ".$startTime."' AND added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");
        foreach($data as $d)
        {
            echo "<tr>";
            echo "<td>".$d->month."</td>";
            echo "<td>".$d->kudos_count."</td>";
            echo "</tr>";
        }
        echo "</tbody>";

    }

    public function getDate6()
    {
        $startDate = $this->input->post('fromDate');
        $endDate = $this->input->post('toDate');
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $data = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY ambassador ORDER BY kudos_count DESC");

        foreach($data as $d)
        {
            echo "<tr>";
            echo "<td>".$d->ambassador."</td>";
            echo "<td>".$d->campaign."</td>";
            echo "<td>".$d->kudos_count."</td>";
            echo "</tr>";
        }
        echo "</tbody>";

    }

    public function excel_report1()
    {
        //$this->load->library('PHPExcel');
        $this->load->library('PHPExcel', NULL, 'excel');

        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Users list');

        $startDate = $this->input->post('fromDate');
        $endDate = $this->input->post('toDate');
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $data = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count FROM tbl_kudos WHERE added_on >= '".$startDate1." ".$startTime."' AND added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");

        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 15,
            ));

        $styleArray2 = array(
            'font'  => array(
                'size'  => 15,
            ));

        $styleArray3 = array(
            'font'  => array(
                'size'  => 15,
                'color' => array('rgb' => 'FFFFFF'),
            ));

        $styleArray6 = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $this->excel->getActiveSheet()->getStyle('A1:B2')->applyFromArray($styleArray6);

        $this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($styleArray6);


        $this->excel->getActiveSheet()->mergeCells('A3:B3');
        $this->excel->getActiveSheet()->setCellValue('A3', 'TOTAL KUDOS COUNT ('.$startDate.' - '.$endDate.')');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
        $this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($styleArray3);
        $this->excel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($styleArray);

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
        $this->excel->getActiveSheet()->mergeCells('A1:B2');

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(40);    // setOffsetX works properly
        $objDrawing->setOffsetY(10);  //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(85); // logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $this->excel->getActiveSheet()->getStyle('A4:B4')->applyFromArray($styleArray6);
        

        $this->excel->getActiveSheet()->getStyle('A4:D4')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);

        $this->excel->getActiveSheet()->setCellValue('A4', 'Month and Year');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Kudos Count');



        $row = 5;
        foreach($data as $r){
            $this->excel->getActiveSheet()->fromArray(array($r->month, $r->kudos_count), null, 'A'.$row);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray2);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray6);
            $row++;

            
        }

        unset($styleArray6);


        

        ob_end_clean();
        date_default_timezone_set("Asia/Manila");
        $timestamp=date("Y-m-d-His");
        $filename='KudosCount.xls'; 

        header('Content-Type: application/vnd.ms-excel'); 

        header('Content-Disposition: attachment;filename="'.$filename.'"'); 

        header('Cache-Control: max-age=0'); 

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        $objWriter->save('php://output');
    }

    public function excel_report2()
    {
        //$this->load->library('PHPExcel');
        $this->load->library('PHPExcel', NULL, 'excel');

        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Users list');

        $startDate = $this->input->post('fromDate');
        $endDate = $this->input->post('toDate');
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $data = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY ambassador ORDER BY kudos_count DESC");

        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 15,
            ));

        $styleArray2 = array(
            'font'  => array(
                'size'  => 15,
            ));

        $styleArray3 = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 15,
                'color' => array('rgb' => 'FFFFFF'),
            ));

        $styleArray6 = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(220);    // setOffsetX works properly
        $objDrawing->setOffsetY(10);  //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(85); // logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
        $this->excel->getActiveSheet()->mergeCells('A1:C2');

        $this->excel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray6);
        $this->excel->getActiveSheet()->getStyle('A2:C2')->applyFromArray($styleArray6);

        $this->excel->getActiveSheet()->mergeCells('A3:C3');
        $this->excel->getActiveSheet()->setCellValue('A3', 'TOTAL KUDOS COUNT BY AMBASSADOR ('.$startDate.' - '.$endDate.')');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
        $this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray3);

        $this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray6);

        $this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

        $this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray6);

        $this->excel->getActiveSheet()->setCellValue('A4', 'Ambassador');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Campaign');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Kudos Count');



        $row = 5;
        foreach($data as $r){
            $this->excel->getActiveSheet()->fromArray(array($r->ambassador, $r->campaign, $r->kudos_count), null, 'A'.$row);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray2);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray6);
            $row++;
        }

        unset($styleArray6);

        ob_end_clean();
        date_default_timezone_set("Asia/Manila");
        $timestamp=date("Y-m-d-His");
        $filename='KudosCountAmbassadors.xls'; 

        header('Content-Type: application/vnd.ms-excel'); 

        header('Content-Disposition: attachment;filename="'.$filename.'"'); 

        header('Cache-Control: max-age=0'); 

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        $objWriter->save('php://output');
    }

    public function excel_report3()
    {
        $this->load->library('PHPExcel', NULL, 'excel');

        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Users list');

        $startDate = $this->input->post('fromDate');
        $endDate = $this->input->post('toDate');
        $campaign = $this->input->post('campaign');
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $campaign1 =mysqli_real_escape_string($this->db->conn_id,trim($campaign));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $data = $this->general_model->custom_query("SELECT CONCAT(DATE_FORMAT(tbl_kudos.added_on, '%m'), ' ',DATE_FORMAT(tbl_kudos.added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_account.acc_id=tbl_kudos.acc_id WHERE tbl_kudos.acc_id='".$campaign1."' AND tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");

        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 15,
            ));

        $styleArray2 = array(
            'font'  => array(
                'size'  => 15,
            ));

        $styleArray3 = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 15,
                'color' => array('rgb' => 'FFFFFF'),
            ));

        $styleArray6 = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(220);    // setOffsetX works properly
        $objDrawing->setOffsetY(10);  //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(85); // logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
        $this->excel->getActiveSheet()->mergeCells('A1:C2');

        $this->excel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray6);
        $this->excel->getActiveSheet()->getStyle('A2:C2')->applyFromArray($styleArray6);

        $this->excel->getActiveSheet()->mergeCells('A3:C3');
        $this->excel->getActiveSheet()->setCellValue('A3', 'TOTAL KUDOS COUNT BY CAMPAIGN ('.$startDate.' - '.$endDate.')');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
        $this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray3);

        $this->excel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray6);
        $this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray6);

        $this->excel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);

        $this->excel->getActiveSheet()->setCellValue('A4', 'Month and Year');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Campaign');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Kudos Count');



        $row = 5;
        foreach($data as $r){
            $this->excel->getActiveSheet()->fromArray(array($r->month, $r->campaign, $r->kudos_count), null, 'A'.$row);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray2);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray6);
            $row++;
        }

        unset($styleArray6);

        ob_end_clean();
        date_default_timezone_set("Asia/Manila");
        $timestamp=date("Y-m-d-His");
        $filename='KudosCountCampaign.xls'; 

        header('Content-Type: application/vnd.ms-excel'); 

        header('Content-Disposition: attachment;filename="'.$filename.'"'); 

        header('Cache-Control: max-age=0'); 

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        $objWriter->save('php://output');
    }





    //              --- Start Unique Function ---            //

    
    
}
