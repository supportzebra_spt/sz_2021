<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Dailypulse extends General {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('general_model');
	}
	public function control_panel(){
		$data = [
			'uri_segment' => $this->uri->segment_array(),
			'title' => 'Control Panel',
		];
		if ($this->check_access()) {
			$this->load_template_view('templates/szfive/daily_pulse/escalation_control_panel', $data);
		}
	}
	public function monitoring(){
		$data = [
			'uri_segment' => $this->uri->segment_array(),
			'title' => 'Daily Pulse Monitoring',
		];
		if ($this->check_access()) {
			$this->load_template_view('templates/szfive/daily_pulse/monitoring', $data);
		}
	}

	private function get_monitoring_acess($emp_id){
		$fields = "*";
		$where = "emp_id = $emp_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_daily_pulse_monitoring_access");
	}

	public function check_daily_pulse_monitoring_access(){
		$emp_id = $this->session->userdata('emp_id');
		// $emp_id = 557;
		$record = $this->get_monitoring_acess($emp_id);
		if(count($record) > 0){
			return true;
		}else{
			$this->error_403();
			return false;
		}

	}
	public function escalation($choice,$occurence,$answerId){
		$record=$this->general_model->custom_query("SELECT symbol,occurence FROM tbl_daily_pulse_escalation where choice_ID=$choice");
		foreach ($record as $row){
			eval( '$result = (' .$occurence.$row->symbol.$row->occurence. ');' );
			if($result){
				// $record =$this->general_model->custom_query("SELECT intervention,isConfetti,message,symbol,isIcon,occurence FROM tbl_daily_pulse_escalation where choice_ID=$choice and occurence=$row->occurence");
				$record =$this->general_model->custom_query("SELECT escalation_ID,intervention,isConfetti,message,symbol,isIcon,occurence,gif,followUpQuestion FROM tbl_daily_pulse_escalation left join tbl_szfive_survey_choices on choice_ID = choiceId where choice_ID=".$choice." and occurence=".$row->occurence);
			}
		}
		$data["intervention"] = $record[0]->intervention;
		$data["isConfetti"] = $record[0]->isConfetti;
		$data["message"] = (!empty($record[0]->message)) ? str_replace("{n}",$this->ordinal($occurence), $record[0]->message) : "0";
		$data["occurence"] =   $record[0]->occurence;
		$data["symbol"] =   $record[0]->symbol;
		$data["icon"] =   $record[0]->isIcon;
		$data["img"] =   $record[0]->gif;
		$data["escalation_ID"] =   $record[0]->escalation_ID;
		if(!empty($record[0]->followUpQuestion)){
			$data["followUpQuestion"] = $record[0]->followUpQuestion;
			$this->db->query("update tbl_szfive_survey_answers set isAnsweredFollowQues=1 where answerId=".$answerId);
		}else{
			$data["followUpQuestion"] = "0";
		}

		return json_encode($data);
	}
	public function identify_occurence($selectedNum,$new_insert_answerid){
		$query_getChoiceID =$this->general_model->custom_query("SELECT choiceId FROM tbl_szfive_survey_choices WHERE levels=$selectedNum limit 1");
		$choice_ID = $query_getChoiceID[0]->choiceId; 
		
		$query_getlastoccurence = "SELECT answerId,answer,occurence FROM tbl_szfive_survey_answers a,tbl_schedule b WHERE a.sched_id=b.sched_id and employeeId=".$_SESSION["uid"]." and answerId!=$new_insert_answerid order by answerId DESC LIMIT 1";
		$record = $this->general_model->custom_query($query_getlastoccurence);
		$occurence=1;
		foreach ($record as $sz_answer){
			$lastanswer=$sz_answer->answer;
			$occurence=$sz_answer->occurence;
			if($lastanswer.""===$selectedNum.""){
				$occurence++;
			}else{
				$occurence=1;
			}
		}
		echo $this->escalation($choice_ID,$occurence,$new_insert_answerid);

	}
	public function interventionEsacation(){
		date_default_timezone_set('Asia/Manila');

		$escalation_ID = $this->input->post("escalation_ID");
		$answerId = $this->input->post("answerId");
		$record =$this->general_model->custom_query("SELECT intervention FROM tbl_daily_pulse_escalation left join tbl_szfive_survey_choices on choice_ID = choiceId where escalation_ID=".$escalation_ID);
		$sup = $this->get_current_intervention($record[0]->intervention,$_SESSION["emp_id"]);
		$checkIfExist =$this->general_model->custom_query("SELECT intervention_ID FROM tbl_daily_survey_intervention where intervener_empid=".$sup." and answer_id=$answerId");
		
		if(count($checkIfExist)==0) {
			$now = date("Y-m-d H:i:00");
			$intervene['intervener_empid'] = $sup;
			$intervene['emp_id'] = $_SESSION["emp_id"];
			$intervene['answer_id'] = $answerId;
			$intervene['interveneLevel'] = $record[0]->intervention;
			$intervene['status_ID'] = 2;
			$intervene['interventionDate'] = $now;
			$intervene['deadline'] = date("Y-m-d H:i:00",strtotime("+1 day", strtotime($now)));
			$insertIntervene = $this->general_model->insert_vals($intervene, 'tbl_daily_survey_intervention');

			if($insertIntervene>0){
				$employee = $_SESSION["fname"]." ".$_SESSION["lname"];
				$message = "You are notified to conduct an engagement session to gather details on how we can make ".$employee." cheerful.";
				$link = 'szfive/survey/view/' . $_SESSION["emp_id"] . '/' . $answerId;

				$query ="select uid from tbl_user  where emp_id=".$sup;

				$supervisor = $this->general_model->custom_query($query);

				echo $this->setSystemNotif($message, $link, $supervisor[0]->uid);

			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
	public function savefollowanswer(){
		date_default_timezone_set('Asia/Manila');
		$ans = $this->input->post("txt");
		$answerId = $this->input->post("answerId");
		$followUpQuestion = $this->input->post("followUpQuestion");
		$data['isAnsweredFollowQues'] =  date("l jS \of F Y  h:i:s A")."|".$ans."|".$followUpQuestion;
		$where = "answerId=$answerId";
		echo $this->general_model->update_vals($data, $where, 'tbl_szfive_survey_answers');
	}
	public function ordinal($number){
		$ends = array('th','st','nd','rd','th','th','th','th','th','th');
		if ((($number % 100) >= 11) && (($number%100) <= 13)){
			return $number. 'th';
		}else{
			return  $number. $ends[$number % 10];
		}
	}
	// charise code
	public function fetch_answersinfo($answerid){
		$userid = $this->session->userdata('uid');
		$query_answers="SELECT ans.answerId,ans.employeeId,ans.answer,ans.detailedAnswer,ans.occurence,ans.isAnsweredFollowQues,ans.isReviewed,ans.dateIntervened,choice.choiceId,choice.label as choice_label,choice.levels,choice.gif,interven.feedback, interven.emp_id, interven.intervener_empid as intervenee, interven.employeeConfirmed, interven.intervention_ID, interven.status_ID, comment.comment_ID,comment.emp_id as comemp_id,comment.comment from tbl_szfive_survey_choices choice,tbl_szfive_survey_answers ans left join tbl_daily_survey_intervention interven on interven.answer_id=ans.answerID AND (interven.status_ID=2 OR interven.status_ID=3) left join tbl_daily_survey_comment comment on comment.intervention_ID=interven.intervention_ID where ans.answer=choice.levels and answerID=$answerid";
		$query= $this->general_model->custom_query($query_answers);
		$getempid=array_shift($query);
		$uID=$getempid->{'employeeId'};
		$comment_empID=$getempid->{'comemp_id'};
		$comment_ID=$getempid->{'comment_ID'};
		$intervention_ID=$getempid->{'intervention_ID'};
		$intervener_emp_ID=$getempid->{'intervenee'};
		$intervention_date=$getempid->{'dateIntervened'};
		if($intervener_emp_ID!=null){
			$record_answers['feedback_empdetails']= $this->fetch_employeedetails($intervener_emp_ID);
			$record_answers['interventionDate']=  $this->time_elapsed_string($intervention_date);
		}
		$record_answers['answers']= $this->general_model->custom_query($query_answers);
		$record_answers['employee']= $this->fetch_employee_ID($userid);

	//Answer- Employee details
		$record_answers['answer_employeeid']= $this->fetch_employee_ID($uID);
		$record_comments = [];
		$record_comments_count = 0;

		if($comment_ID!=null){
			$query_comment="SELECT comment.comment_ID, comment.intervention_ID, comment.comment,comment.emp_id,comment.dateCreated,emp.emp_id,ap.fname, ap.lname, ap.pic FROM tbl_daily_survey_comment comment,tbl_employee emp, tbl_applicant ap WHERE comment.intervention_ID=$intervention_ID AND comment.emp_id=emp.emp_id AND emp.apid=ap.apid";
			$query_c= $this->general_model->custom_query($query_comment);
			foreach($query_c as $q_c){
				$q_c->elapseTime= $this->time_elapsed_string($q_c->dateCreated);
				$record_comments[$record_comments_count] =$q_c;
				$record_comments_count++;
			}
			$record_answers['comment_details']=$record_comments;
		}
		echo json_encode($record_answers);
	}
	public function fetch_employee_ID($userid){
		$employee="SELECT emp.emp_id, app.fname, app.lname FROM tbl_employee emp,tbl_user user,tbl_applicant app WHERE user.uid=$userid AND emp.emp_id=user.emp_id AND app.apid=emp.apid";
		return $this->general_model->custom_query($employee);
	}
	public function fetch_employeedetails($empid){
		$employee="SELECT user.uid, ap.fname, ap.lname,ap.pic,emp.emp_id FROM tbl_employee emp,tbl_user user,tbl_applicant ap WHERE emp.emp_id=user.emp_id AND emp.emp_id=$empid AND user.emp_id=$empid AND emp.apid=ap.apid";
		return $this->general_model->custom_query($employee);
	}
	public function fetch_recipient($answerid){
		$get_recipientID="SELECT user.uid FROM tbl_employee emp, tbl_szfive_survey_answers ans, tbl_user user WHERE user.emp_id=emp.emp_id AND user.uid=ans.employeeId AND ans.answerId=$answerid";
		return $this->general_model->custom_query($get_recipientID);
	}
	public function fetch_intervention_details($intervenid){
		$get_interven="SELECT intervener_empid, emp_id FROM tbl_daily_survey_intervention WHERE intervention_ID=$intervenid";
		return $this->general_model->custom_query($get_interven);
	}
	public function submit_feedback(){
		date_default_timezone_set('Asia/Manila');
		$userid = $this->session->userdata('uid');
		$data['feedback'] = $this->input->post('feedback');
		$data['status_ID'] = 3;
		$data2['dateIntervened']=date("Y-m-d H:i:s");
		$data2['status_ID'] = 3;
		$where = "answer_id = " . $this->input->post('answer_id') ." AND intervener_empid = " . $this->input->post('intervener_empid');
		$where2 = "answerId = " . $this->input->post('answer_id');
		$ansid= $this->input->post('answer_id');
		$intervener_eid=$this->input->post('intervener_empid');

		// Check if intervention status ID is 12
		$interven_stat_query="SELECT status_ID FROM tbl_daily_survey_intervention WHERE answer_id=$ansid AND intervener_empid=$intervener_eid";
		$instat=$this->general_model->custom_query($interven_stat_query);
		$i_stat=array_shift($instat);
		$interven_status=$i_stat->{'status_ID'};

		if($interven_status=="2"){
			$insertedVals = $this->general_model->update_vals($data, $where, 'tbl_daily_survey_intervention');
			$insertedVals2 = $this->general_model->update_vals($data2, $where2, 'tbl_szfive_survey_answers');
		// Notifications
		//Fetching recipient details
			$r_id= $this->fetch_recipient($ansid);
			$rid=array_shift($r_id);
			$recipient_id=$rid->{'uid'};

			$recipient[0]= [
				'userId' => $recipient_id,
			];
			$notif_message = "gave you a feedback on your SnapSZ but it needs your confirmation. <br><small><b>ID</b>:".str_pad($ansid, 8, '0', STR_PAD_LEFT)." </small>";
			$link = "szfive/survey/view/".$recipient_id."/".$ansid;	
			$insert_stat['notifid'] = $this->set_notif($userid,$notif_message, $link, $recipient);
		}else if($interven_status==12){
			return false;
		}
		echo json_encode($insert_stat);
	}
	public function submit_comment(){
		date_default_timezone_set('Asia/Manila');
		$data['comment'] = $this->input->post('comment_content');
		$data['intervention_ID'] = $this->input->post('interven_id');
		$data['emp_id'] = $this->input->post('empid');
		$data['dateCreated'] =date("Y-m-d H:i:s");
		$emp_id_session= $this->input->post('empid');
		$answerid= $this->input->post('answer_id');

		//Get intervention details
		$intervenid= $this->input->post('interven_id');
		$get_interven= $this->fetch_intervention_details($intervenid);
		$in_det=array_shift($get_interven);
		$intervener_empid=$in_det->{'intervener_empid'};
		$intervenee_empid=$in_det->{'emp_id'};

		//Getting User_IDs
		$intervenerEid= $this->fetch_employeedetails($intervener_empid);
		$i_empid=array_shift($intervenerEid);
		$intervener_userid=$i_empid->{'uid'};

		$interveneeEid= $this->fetch_employeedetails($intervenee_empid);
		$int_empid=array_shift($interveneeEid);
		$intervenee_userid=$int_empid->{'uid'};

		if($intervener_empid==$emp_id_session){
			$recipient_id=$int_empid->{'uid'};
			$sender=$i_empid->{'uid'};
			$title="replied to your comment";
		}else if($intervenee_empid==$emp_id_session){
			$recipient_id=$i_empid->{'uid'};
			$sender=$int_empid->{'uid'};
			$query_com="SELECT comment_ID FROM tbl_daily_survey_comment WHERE intervention_ID=$intervenid";
			$query_res_comment=$this->general_model->custom_query($query_com);
			if(count($query_res_comment)==0){
				$title="disagreed to your feedback and gave a comment.";
			}else{
				$title="replied to your comment.";
			}
		}
		$insertedVals = $this->general_model->insert_vals($data, 'tbl_daily_survey_comment');
		// Notifications
		
		//Sender details
		$sender= $this->fetch_employeedetails($emp_id_session);
		$sender_det=array_shift($sender);
		$sender_fname=$sender_det->{'fname'};
		$sender_lname=$sender_det->{'lname'};

		$recipient[0]= [
			'userId' => $recipient_id,
		];
		$notif_message = $sender_lname.",".$sender_fname." ".$title."<br><small><b>ID</b>:".str_pad($answerid, 8, '0', STR_PAD_LEFT)." </small>";
		$link = "szfive/survey/view/".$intervenee_userid."/".$answerid;	
		$insert_stat['notifid'] = $this->set_system_notif_preceeding($notif_message, $link, $recipient);
		// $this->set_notif($sender,$notif_message, $link, $recipient);
		echo json_encode($insert_stat);
	}
	public function feedback_agreed($answerid,$intervener,$intervenee){
		$userid = $this->session->userdata('uid');
		$data['employeeConfirmed'] = "1";
		$where = "answer_id = " . $answerid; 		
		$insertedVals = $this->general_model->update_vals($data, $where, 'tbl_daily_survey_intervention');

		//Get user id for intervener
		$get_uid= $this->fetch_employeedetails($intervener);
		$i_uid=array_shift($get_uid);
		$intervener_uid=$i_uid->{'uid'};

		// Notifications
		$recipient[0]= [
			'userId' => $intervener_uid,
		];
		$notif_message = "has confirmed your SnapSZ feedback"."<br><small><b>ID</b>:".str_pad($answerid, 8, '0', STR_PAD_LEFT)." </small>";
		$link = "szfive/survey/view/".$intervenee."/".$answerid;	
		$insert_stat['notifid'] = $this->set_notif($userid,$notif_message, $link, $recipient);
		echo json_encode($insert_stat);
	}

	//REPORT PAGE
	public function report_intervention(){
		$data = [
			'uri_segment' => $this->uri->segment_array(),
			'title' => 'Report Intervention',
		];
		if ($this->check_access()) {
			$this->load_template_view('templates/szfive/daily_pulse/report_intervention', $data);
		}
	}
	//END of #charise code

	//MIC2X CODES START ------------------------

	// CREATE FUNCTIONS
	public function add_escalation($choice_id, $occurence){
		$data['choice_ID'] = $choice_id;
		$data['symbol'] = ">=";
		$data['occurence'] = $occurence;
		$data['changedBy'] = $this->session->userdata('emp_id');
		return $this->general_model->insert_vals_last_inserted_id($data, 'tbl_daily_pulse_escalation');
	}
	
	public function new_escalation(){
		$choice_id = $this->input->post('choiceId');
		$max = $this->get_max_choice_escalation($choice_id);
		$occurence = 1;
		if (count($max) > 0){
			$occurence  = $max->maximum;
			$data['update_prev_max_symbol'] = $this->update_escalation_symbol($choice_id, $occurence, "==");
			$occurence++;
		}
		$data['add_stat'] = $this->add_escalation($choice_id, $occurence);
		$data['occurence_num'] = $occurence;
		echo json_encode($data);
	}
	// READ FUNCTIONS
	private function get_max_choice_escalation($choice_id){
		$fields = "MAX(occurence) as maximum";
		$where = "choice_ID = $choice_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_daily_pulse_escalation");
	}
	
	private function get_escalation_record($choice_id){
		$fields = "escalation_ID, choice_ID, occurence, symbol, followUpQuestion, message, intervention, isConfetti, isIcon, dateCreated, dateChanged, changedBy";
		$where = "choice_ID = $choice_id";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_daily_pulse_escalation", "occurence ASC");
	}

	public function get_snapsz_choices(){
		$fields = "choiceId, label, img, gif, levels";
		$where = "choiceId IS NOT NULL";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_szfive_survey_choices", "levels ASC");
	}

	private function get_specific_escalation($escalation_id){
		$fields = "escalation_ID, choice_ID, occurence, symbol, followUpQuestion, message, intervention, isConfetti, isIcon, dateCreated, dateChanged, changedBy";
		$where = "escalation_ID = $escalation_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_daily_pulse_escalation");
	}
	
	public function check_if_occurence_exist(){
		$escalation_id = $this->input->post('escalationId');
		$record = $this->get_specific_escalation($escalation_id);
		if(count($record) > 0){
			$data['exist'] = 1;
		}else{
			$data['exist'] = 0;
		}
		echo json_encode($data);
	}

	public function fetch_snapsz_choices(){
		$record = $this->get_snapsz_choices();
		if(count($record) > 0){
			$data['exist'] = 1;
			$data['record'] = $record;
		}else{
			$data['exist'] = 0;
		}
		echo json_encode($data);
	}

	public function get_choice_escalation_table(){
		$record = $this->get_escalation_record($this->input->post('choiceId'));
		// $record = $this->get_escalation_record(6);
		if(count($record) > 0){
			$data['exist'] = 1;
			$data['record'] = $record;
		}else{
			$data['exist'] = 0;
		}
		echo json_encode($data);
	}
	// UPDATE FUNCTIONS
	public function update_escalation(){
		$escalation_id = $this->input->post('escalationId');
		if (ctype_space($this->input->post('value')) || $this->input->post('value') == '') {
			$value = null;
		}else{
			$value = $this->input->post('value');
		}
		$data[$this->input->post('columnAttribute')] = $value;
		$data['changedBy'] = $this->session->userdata('emp_id');
		$where = "escalation_ID = $escalation_id";
		$update_stat = $this->general_model->update_vals($data, $where, 'tbl_daily_pulse_escalation');
		echo json_encode($update_stat);
	}

	public function update_escalation_symbol($choice, $occurence, $symbol){
		$data['symbol'] = $symbol;
		$data['changedBy'] = $this->session->userdata('emp_id');
		$where = "choice_ID = $choice AND occurence = $occurence";
		return $this->general_model->update_vals($data, $where, 'tbl_daily_pulse_escalation');
	}

	public function update_occurence_sequence($choice_id, $occurence){
		$query = "UPDATE tbl_daily_pulse_escalation SET occurence = occurence-1 WHERE choice_ID = $choice_id AND occurence > $occurence";
		return $this->general_model->custom_query_no_return($query);
	}

	public function update_intervention($intervention){
		$data['status_ID'] = 12;
		$where = "intervention_ID = $choice AND occurence = $occurence";
		return $this->general_model->update_vals($data, $where, 'tbl_daily_pulse_escalation');
	}
	// DELETE FUNCTIONS
	public function delete_specific_escalation($escalation_id){
		$where['escalation_ID'] = $escalation_id;
		return $this->general_model->delete_vals($where, 'tbl_daily_pulse_escalation');
	}

	public function remove_specific_escalation(){
		$choice_id = $this->input->post('choiceId');
		$occurence = $this->input->post('occurenceNum');
		$escalation_id = $this->input->post('escalationId');
		$record = $this->get_specific_escalation($escalation_id);
		if(count($record) > 0){
			$data['delete_record_exist'] = 1;
			$max = $this->get_max_choice_escalation($choice_id);
			$data['delete_escalation'] = $this->delete_specific_escalation($escalation_id);
			if($max->maximum == $occurence){
				$next_max_occurence = $this->get_max_choice_escalation($choice_id);
				$data['update_prev_max_symbol'] = $this->update_escalation_symbol($choice_id, $next_max_occurence->maximum, ">=");
			}else{
				$data['update_occurence_sequence'] = $this->update_occurence_sequence($choice_id, $occurence);
			}
		}else{
			$data['delete_record_exist'] = 0;
		}
		echo json_encode($data);
	}

	// Due Intervention and Transfer of intervention 
	private function get_due_intervention(){
		$dateTime = $this->get_current_date_time();
		$fields = "intervention.intervention_ID, intervention.intervener_empid, appIntervene.lname lnameIntervene, appIntervene.fname fnameIntervene, appIntervene.mname mnameIntervene, intervention.emp_id, app.lname, app.fname, app.mname, intervention.interveneLevel, intervention.status_ID, intervention.interventionDate, intervention.deadline, answers.answerId, answers.occurence, choices.choiceId";
		$where = "app.apid = emp.apid AND emp.emp_id = intervention.emp_id AND appIntervene.apid = empIntervene.apid AND empIntervene.emp_id = intervention.intervener_empid AND choices.levels = answers.answer AND answers.answerId = intervention.answer_id AND intervention.status_ID = 2 AND intervention.deadline <='" . $dateTime['dateTime'] . "'";
		$table = "tbl_daily_survey_intervention intervention, tbl_szfive_survey_answers answers, tbl_szfive_survey_choices choices, tbl_applicant app, tbl_employee emp, tbl_applicant appIntervene, tbl_employee empIntervene";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	private function qry_next_intervention($choice_id, $occurrence){
		$fields = "intervention, occurence";
		$where = "choice_ID = $choice_id AND occurence = $occurrence";
		$table = "tbl_daily_pulse_escalation";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	private function qry_all_next_intervention($choice_id, $occurrence){
		$fields = "intervention, occurence";
		$where = "choice_ID = $choice_id AND occurence > $occurrence AND intervention != 0";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_daily_pulse_escalation", "occurence ASC");
	}


	public function qry_all_intervention_records($start_date = '2019-01-01', $end_date = '2019-06-14', $emp_list = 0, $search_val = "", $status_id = 0){
		$session_emp_id = $this->session->userdata('emp_id');
    	$monitoring_acess = $this->get_monitoring_acess($session_emp_id);
		$search_where = "";
		$emp_where = "";
		$status_where = "";
		$where_acess = "";
		if(trim($search_val) != ''){
			if(is_numeric($search_val)){
				$search_where = "AND answers.answerId LIKE '%" . (int) $search_val. "%'";
			}else{
				$search_where = "AND answers.answerId = '" .$search_val. "'";
			}
		}
		if((int) $emp_list != 0){
			$emp_where = "AND intervention.emp_id IN ($emp_list)";
		}
		if((int) $status_id != 0){
			$status_where = "AND intervention.status_ID = $status_id";
		}
		if(count($monitoring_acess) > 0){
    		if($monitoring_acess->accessType != "both"){
    			$where_acess = "acc.acc_description = access.accessType";
    		}
    		if($monitoring_acess->accountExclude!=null){
    			$accountExclude="where acc_id not in (".$monitoring_acess->accountExclude.")";
    		}else{
    			$accountExclude="";
    		}
    		if($monitoring_acess->accountInclude!=null){
    			$accountInclude="acc.acc_id in (".$monitoring_acess->accountInclude.")";
    		}else{
    			$accountInclude = "";
    		}
		}
		$access_filter = "";
    	if($where_acess !="" &&  $accountInclude != ""){
    		$access_filter = "AND (".$where_acess." or ".$accountInclude.")";
    	}
		// $fields = "answers.answerId, sched.sched_date, choices.choiceId, choices.label, choices.levels, answers.detailedAnswer, answers.occurence, intervention.intervention_ID, intervention.status_ID, stat.description statDescription, empEmp.emp_id empEmp_id, appEmp.lname empLname, appEmp.fname empFname, appEmp.mname empMname, empIntervene.emp_id interveneEmp_id, appIntervene.lname interveneLname, appIntervene.fname interveneFname, appIntervene.mname interveneMname, intervention.interveneLevel, intervention.interventionDate, intervention.deadline, answers.dateIntervened, intervention.feedback, intervention.employeeConfirmed, answers.isAnsweredFollowQues";
		// $where = "sched.sched_id = answers.sched_id AND choices.levels = answers.answer AND answers.answerId = intervention.answer_id AND appEmp.apid = empEmp.apid AND empEmp.emp_id = intervention.emp_id AND appIntervene.apid = empIntervene.apid AND empIntervene.emp_id = intervention.intervener_empid AND DATE(sched.sched_date) >= '".$start_date."' AND DATE(sched.sched_date) <= '".$end_date."' AND stat.status_ID = intervention.status_ID ".$emp_where." ".$search_where." ".$status_where;
		// $table = "tbl_daily_survey_intervention intervention, tbl_szfive_survey_answers answers, tbl_szfive_survey_choices choices, tbl_schedule sched, tbl_applicant appEmp, tbl_employee empEmp, tbl_applicant appIntervene, tbl_employee empIntervene, tbl_status stat";
		// return $this->general_model->fetch_specific_vals($fields, $where, $table, "answers.answerId ASC");

		$query_record_emp_name = "SELECT * FROM (SELECT acc.acc_id, answers.answerId, sched.sched_date, choices.choiceId, choices.label, choices.levels, answers.detailedAnswer, answers.occurence, intervention.intervention_ID, intervention.status_ID, stat.description statDescription, empEmp.emp_id empEmp_id, appEmp.lname empLname, appEmp.fname empFname, appEmp.mname empMname, empIntervene.emp_id interveneEmp_id, appIntervene.lname interveneLname, appIntervene.fname interveneFname, appIntervene.mname interveneMname, intervention.interveneLevel, intervention.interventionDate, intervention.deadline, answers.dateIntervened, intervention.feedback, intervention.employeeConfirmed, answers.isAnsweredFollowQues, answers.status_ID answerStat FROM tbl_daily_survey_intervention intervention, tbl_szfive_survey_answers answers, tbl_szfive_survey_choices choices, tbl_schedule sched, tbl_applicant appEmp, tbl_employee empEmp, tbl_applicant appIntervene, tbl_employee empIntervene, tbl_status stat, tbl_account acc, tbl_daily_pulse_monitoring_access access WHERE sched.sched_id = answers.sched_id AND choices.levels = answers.answer AND answers.answerId = intervention.answer_id AND appEmp.apid = empEmp.apid AND empEmp.emp_id = intervention.emp_id AND appIntervene.apid = empIntervene.apid AND empIntervene.emp_id = intervention.intervener_empid AND DATE(sched.sched_date) >= '$start_date' AND DATE(sched.sched_date) <= '$end_date' AND stat.status_ID = intervention.status_ID ".$emp_where." ".$status_where." ".$search_where." AND acc.acc_id = empEmp.acc_id $access_filter AND access.emp_id = $session_emp_id ORDER BY answers.answerId) AS x $accountExclude";
		return $this->general_model->custom_query($query_record_emp_name); 
		// var_dump($this->general_model->custom_query($query_record_emp_name)); 
	}

	private function qry_all_comments($start_date, $end_date, $emp_list, $search_val, $status_id){
		$session_emp_id = $this->session->userdata('emp_id');
		$monitoring_acess = $this->get_monitoring_acess($session_emp_id);
		$where_acess = "";
		$search_where = "";
		$emp_where = "";
		$status_where = "";
		if(trim($search_val) != ''){
			if(is_numeric($search_val)){
				$search_where = "AND answers.answerId LIKE '%" . (int) $search_val. "%'";
			}else{
				$search_where = "AND answers.answerId = '" .$search_val. "'";
			}
		}
		if((int) $emp_list != 0){
			$emp_where = "AND intervention.emp_id IN ($emp_list)";
		}
		if((int) $status_id != 0){
			$status_where = "AND intervention.status_ID = $status_id";
		}if(count($monitoring_acess) > 0){
    		if($monitoring_acess->accessType != "both"){
    			$where_acess = "acc.acc_description = access.accessType";
    		}
    		if($monitoring_acess->accountExclude!=null){
    			$accountExclude="where acc_id not in (".$monitoring_acess->accountExclude.")";
    		}else{
    			$accountExclude="";
    		}
    		if($monitoring_acess->accountInclude!=null){
    			$accountInclude="acc.acc_id in (".$monitoring_acess->accountInclude.")";
    		}else{
    			$accountInclude = "";
    		}
		}
		$access_filter = "";
    	if($where_acess !="" &&  $accountInclude != ""){
    		$access_filter = "AND (".$where_acess." or ".$accountInclude.")";
    	}
		// $fields = "comments.comment_ID, comments.intervention_ID, comments.comment, comments.emp_id, comments.dateCreated";
		// $where = "comments.intervention_ID = intervention.intervention_ID AND  sched.sched_id = answers.sched_id AND answers.answerId = intervention.answer_id AND DATE(sched.sched_date) >= '".$start_date."' AND DATE(sched.sched_date) <= '".$end_date."' ".$emp_where." ".$search_where." ".$status_where;
		// $table = "tbl_daily_survey_comment comments, tbl_daily_survey_intervention intervention, tbl_szfive_survey_answers answers, tbl_schedule sched";
		// return $this->general_model->fetch_specific_vals($fields, $where, $table);

		$query_record_emp_name = "SELECT * FROM (SELECT acc.acc_id, comments.comment_ID, comments.intervention_ID, comments.comment, comments.emp_id, comments.dateCreated FROM tbl_daily_survey_comment comments, tbl_daily_survey_intervention intervention, tbl_szfive_survey_answers answers, tbl_schedule sched, tbl_account acc, tbl_employee emp, tbl_daily_pulse_monitoring_access access WHERE comments.intervention_ID = intervention.intervention_ID AND sched.sched_id = answers.sched_id AND answers.answerId = intervention.answer_id AND DATE(sched.sched_date) >= '$start_date' AND DATE(sched.sched_date) <= '$end_date' ".$emp_where." ".$status_where." ".$search_where." AND acc.acc_id = emp.acc_id AND emp.emp_id = intervention.emp_id $access_filter AND access.emp_id = $session_emp_id) AS x $accountExclude";
		return $this->general_model->custom_query($query_record_emp_name); 
	}


	private function qry_all_escalation(){
		$fields = "escalation_ID, choice_ID, occurence, followUpQuestion, message";
		$where = "escalation_ID IS NOT NULL";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_daily_pulse_escalation");
	}

	public function cmp($a, $b) 
	{
		return strcmp($a->comment_ID, $b->comment_ID);
	}


	// public function daily_pulse_report_data($start_date, $end_date, $emp_list, $search_val){
	// 	$row_data = [];
	// 	$row_count = 0;
	// 	$completed_intervention = $this->qry_all_completed_intervention($start_date, $end_date, $emp_list, $search_val);
	// 	$intervention_comments = $this->qry_all_comments($start_date, $end_date, $emp_list, $search_val);
	// 	$escalation_settings = $this->qry_all_escalation();
	// 	// var_dump($completed_intervention);
	// 	if(count($completed_intervention) > 0){
	// 		foreach($completed_intervention as $intervention_row){
	// 			$searchedValue = $intervention_row->intervention_ID;
	// 			$specific_comments = array_filter($intervention_comments,
	// 				function ($e) use ($searchedValue) {
	// 					return $e->intervention_ID == $searchedValue;
	// 				}
	// 			);


	// 			usort($specific_comments, array($this, "cmp"));

	// 			if(($intervention_row->interventionDate != '' && $intervention_row->interventionDate != null) && ($intervention_row->dateIntervened != '' && $intervention_row->dateIntervened != null)){
	// 				$intervention_date_str = strtotime($intervention_row->interventionDate);
	// 				$intervenened_date_str = strtotime($intervention_row->dateIntervened);
	// 				$duration = $this->get_human_time_format($intervention_date_str, $intervenened_date_str, 0);
	// 				$intervention_row->{"interventionTime"} = $this->get_formatted_time($duration['hoursTotal']);
	// 			}else{
	// 				$intervention_row->{"interventionTime"} = "cannot be identified";
	// 			}
	// 			// 1st Comment;
	// 			if(array_key_exists(0, $specific_comments)){
	// 				$intervention_row->{"comment1"} = $specific_comments[0]->comment;
	// 			}else{
	// 				$intervention_row->{"comment1"} = NULL;
	// 			}
	// 			// 2nd Comment;
	// 			if(array_key_exists(1, $specific_comments)){
	// 				$intervention_row->{"comment2"} = $specific_comments[1]->comment;
	// 			}else{
	// 				$intervention_row->{"comment2"} = NULL;
	// 			}
	// 			// 3rd Comment;
	// 			if(array_key_exists(2, $specific_comments)){
	// 				$intervention_row->{"comment3"} = $specific_comments[2]->comment;
	// 			}else{
	// 				$intervention_row->{"comment3"} = NULL;
	// 			}
	// 			$row_data[$row_count] = $intervention_row;
	// 			$row_count++;
	// 		}
	// 	}
	// 	return $row_data;
	// 	// var_dump($intervention_comments);
	// }

	private function calc_array($array, $prop, $func) {
		$result = array_map(function($o) use($prop) {
			return $o;
		},$array);

		if(function_exists($func)) {
			return $func($result);
		}
		return false;
	}


	public function daily_pulse_report_data($start_date, $end_date, $emp_list, $search_val, $status_id){
		$row_data = [];
		$row_count = 0;
		$all_intervention = $this->qry_all_intervention_records($start_date, $end_date, $emp_list, $search_val, $status_id);
		$intervention_comments = $this->qry_all_comments($start_date, $end_date, $emp_list, $search_val, $status_id);
		$escalation_settings = $this->qry_all_escalation();
		if(count($all_intervention) > 0){
			foreach($all_intervention as $intervention_row){
				$searchedValue = $intervention_row->intervention_ID;
				$specific_comments = array_filter($intervention_comments,
					function ($e) use ($searchedValue) {
						return $e->intervention_ID == $searchedValue;
					}
				);

				$choice_id = $intervention_row->choiceId;
				$occurrenceNum = $intervention_row->occurence;
				$escalation = array_filter($escalation_settings ,
					function ($e) use ($choice_id, $occurrenceNum) {
						return (($e->choice_ID == $choice_id) && ($e->occurence == $occurrenceNum));
					}
				);


				if(count($escalation) > 0){
					foreach($escalation as $escalation_row){
						$intervention_row->{"followUpQuestion"} = $escalation_row->followUpQuestion;
					}
				}else{
					$escalation_choice = array_filter($escalation_settings ,
						function ($e) use ($choice_id) {
							return $e->choice_ID == $choice_id;
						}
					);
					$max_occurence = $this->calc_array($escalation_choice, 'occurence', 'max');
					if($max_occurence !== false){
						$intervention_row->{"followUpQuestion"} = $max_occurence->followUpQuestion;
					}else{
						$intervention_row->{"followUpQuestion"} = "Not Found";
					}
				}

				usort($specific_comments, array($this, "cmp"));

				if($intervention_row->status_ID == 3){
					if(($intervention_row->interventionDate != '' && $intervention_row->interventionDate != null) && ($intervention_row->dateIntervened != '' && $intervention_row->dateIntervened != null)){
						$intervention_date_str = strtotime($intervention_row->interventionDate);
						$intervenened_date_str = strtotime($intervention_row->dateIntervened);
						$duration = $this->get_human_time_format($intervention_date_str, $intervenened_date_str, 0);
						$intervention_row->{"interventionTime"} = $this->get_formatted_time_with_seconds($duration['hoursTotal']);
					}else{
						$intervention_row->{"interventionTime"} = "cannot be identified";
					}
					// 1st Comment;
					if(array_key_exists(0, $specific_comments)){
						$intervention_row->{"comment1"} = $specific_comments[0]->comment;
					}else{
						$intervention_row->{"comment1"} = '-';
					}
					// 2nd Comment;
					if(array_key_exists(1, $specific_comments)){
						$intervention_row->{"comment2"} = $specific_comments[1]->comment;
					}else{
						$intervention_row->{"comment2"} = '-';
					}
					// 3rd Comment;
					if(array_key_exists(2, $specific_comments)){
						$intervention_row->{"comment3"} = $specific_comments[2]->comment;
					}else{
						$intervention_row->{"comment3"} = '-';
					}
				}else{
					$intervention_row->{"interventionTime"} = "-";
					$intervention_row->{"comment1"} = "-";
					$intervention_row->{"comment2"} = "-";
					$intervention_row->{"comment3"} = "-";
				}
				
				$row_data[$row_count] = $intervention_row;
				$row_count++;
			}
		}
		// var_dump($row_data);
		return $row_data;
		// var_dump($all_intervention);
		// var_dump($intervention_comments);
	}
	
	public function export_daily_pulse_report(){
		$start_date = $this->input->post('startDate');
		$end_date = $this->input->post('endDate');
		$emp_list = $this->input->post('empId');
		$search_val = $this->input->post('searchVal');
		$status_id = $this->input->post('statusId');
		$row_data = $this->daily_pulse_report_data($start_date, $end_date, $emp_list, $search_val, $status_id);
		// $logo = 'C:\wamp64\www\sz\assets\images\img\logo2.png';
		$logo = $this->dir . '/assets/images/img/logo2.png';

		$this->load->library('PHPExcel', null, 'excel');
		$this->excel->createSheet(0);
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Daily Pulse');
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath($logo);
        $objDrawing->setOffsetX(5); // setOffsetX works properly
        $objDrawing->setOffsetY(20); //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(45);// logo height
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->setShowGridlines(false);

        $header = [
        	['col' => '', 'id' => 'C2', 'title' => 'Daily Pulse Report'],
        	['col' => '', 'id' => 'C3', 'title' => 'Date: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
        	['col' => 'A', 'id' => 'A5', 'title' => 'PULSE-ID'],
        	['col' => 'B', 'id' => 'B5', 'title' => 'DATE'],
        	['col' => 'C', 'id' => 'C5', 'title' => 'LASTNAME'],
        	['col' => 'D', 'id' => 'D5', 'title' => 'FIRSTNAME'],
        	['col' => 'E', 'id' => 'E5', 'title' => 'RATE'],
        	['col' => 'F', 'id' => 'F5', 'title' => 'RATE DESCRIPTION'],
        	['col' => 'G', 'id' => 'G5', 'title' => 'OCCURENCE'],
        	['col' => 'H', 'id' => 'h5', 'title' => 'FOLLOW-UP QUESTION'],
        	['col' => 'I', 'id' => 'I5', 'title' => 'ANSWER TO FOLLOW-UP QUESTION'],
        	['col' => 'J', 'id' => 'J5', 'title' => 'INTERVENED BY'],
        	['col' => 'K', 'id' => 'K5', 'title' => 'INTERVENTION LEVEL'],
        	['col' => 'L', 'id' => 'L5', 'title' => 'INTERVENTION Status'],
        	['col' => 'M', 'id' => 'M5', 'title' => 'TIME INTERVENED'],
        	['col' => 'N', 'id' => 'N5', 'title' => 'INTERVENED AFTER'],
        	['col' => 'O', 'id' => 'O5', 'title' => 'FEEDBACK'],
        	['col' => 'P', 'id' => 'P5', 'title' => 'RESPONDENT CONFIRMATION'],
        	['col' => 'Q', 'id' => 'Q5', 'title' => '1ST COMMENT(RESPONDENT)'],
        	['col' => 'R', 'id' => 'R5', 'title' => '2ND COMMENT(INTERVENOR)'],
        	['col' => 'S', 'id' => 'S5', 'title' => 'LAST COMMENT(RESPONDENT)'],
        ];

        for($excel_data_header_loop = 0; $excel_data_header_loop < count($header); $excel_data_header_loop++){
			// echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
        	$this->excel->getActiveSheet()->setCellValue($header[$excel_data_header_loop]['id'], $header[$excel_data_header_loop]['title']);
        	$this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getFont()->setBold(true);
        	$this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
        	$this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	// $this->excel->getActiveSheet()->getColumnDimension($header[$excel_data_header_loop]['col'])->setWidth(20);
        }
        $this->excel->getActiveSheet()->getStyle('C2:C2')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('C2:C3')->getFont()->getColor()->setRGB('000000');
        $this->excel->getActiveSheet()->getStyle('C2:C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->getStyle('A5:S5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(17);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(28);
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(30);
        $this->excel->getActiveSheet()->freezePane('F6');
        $rowNum = 6;
		
        $last_row = (count($row_data) + $rowNum)-1;
        if(count($row_data) > 0){
        	foreach ($row_data as $rows) {
        		$intervene_level = "";
        		$repondent_confirmation = "-";
				
        		$time_intervened = new DateTime($rows->interventionDate);
				$timeIntervened = $time_intervened->format('d/m/Y  g:i A');
        		if($rows->interveneLevel < 4 && $rows->interveneLevel > 0){
        			$intervene_level = $this->ordinal($rows->interveneLevel)." Level";
        		}else if($rows->interveneLevel > 4){
        			if($rows->interveneLevel == 4){
        				$intervene_level = "HR Manager";
        			}else{
        				$intervene_level = "Managing Director";
        			}
        		}else{
					$intervene_level = "Default Intervenor";
				}
        		if((int)$rows->interveneEmp_id == 522){
        			$intervene_level = "HR Manager";
        		}else if((int)$rows->interveneEmp_id == 980){
        			$intervene_level = "Managing Director";
				}
				if((int)$rows->employeeConfirmed != 0){
					$repondent_confirmation = "Agree";
				// }else if( (int) $rows->employeeConfirmed != 1 && $rows->answerStat == 3){
				}else if( (int) $rows->employeeConfirmed != 1 && $rows->answerStat == 3 && $rows->comment1 !== '-' &&  $rows->comment3 !== '-'){
					$repondent_confirmation = "Disagree";
				}else{
					$repondent_confirmation = "-";
				}
        		if($rows->interventionTime == "-" || $rows->interventionTime == "cannot be identified"){
					$timeIntervened = "-";
				}
				$answer_follow = explode("|", $rows->isAnsweredFollowQues);
				$follow_ques = $rows->followUpQuestion;
				$follow_ques_answer = "";
				$follow_ques = "";
				if($rows->isAnsweredFollowQues !== NULL){
					if(count($answer_follow) > 0){
						$follow_ques_answer = $answer_follow[1];
						if(count($answer_follow) == 3){
							$follow_ques = $answer_follow[2];
						}
					}
				}
        		$this->excel->getActiveSheet()->setCellValue('A' . $rowNum, str_pad($rows->answerId, 8, '0', STR_PAD_LEFT));
        		$this->excel->getActiveSheet()->setCellValue('B' . $rowNum, date_format(date_create($rows->sched_date), "m/d/Y"));
        		$this->excel->getActiveSheet()->setCellValue('C' . $rowNum, ucfirst($rows->empLname));
        		$this->excel->getActiveSheet()->setCellValue('D' . $rowNum, ucfirst($rows->empFname));
        		$this->excel->getActiveSheet()->setCellValue('E' . $rowNum, $rows->levels."/5");
        		$this->excel->getActiveSheet()->setCellValue('F' . $rowNum, ucfirst($rows->label));
        		$this->excel->getActiveSheet()->setCellValue('G' . $rowNum, $this->ordinal($rows->occurence));
        		// $this->excel->getActiveSheet()->setCellValue('H' . $rowNum, $rows->followUpQuestion);
        		$this->excel->getActiveSheet()->setCellValue('H' . $rowNum, $follow_ques);
        		$this->excel->getActiveSheet()->setCellValue('I' . $rowNum, $follow_ques_answer);
        		$this->excel->getActiveSheet()->setCellValue('J' . $rowNum, $rows->interveneLname.", ".$rows->interveneFname);
        		$this->excel->getActiveSheet()->setCellValue('K' . $rowNum, $intervene_level);
        		$this->excel->getActiveSheet()->setCellValue('L' . $rowNum, ucfirst($rows->statDescription));
        		$this->excel->getActiveSheet()->setCellValue('M' . $rowNum, $timeIntervened);
        		$this->excel->getActiveSheet()->setCellValue('N' . $rowNum, $rows->interventionTime);
        		$this->excel->getActiveSheet()->setCellValue('O' . $rowNum, $rows->feedback);
        		$this->excel->getActiveSheet()->setCellValue('P' . $rowNum, $repondent_confirmation);
        		$this->excel->getActiveSheet()->setCellValue('Q' . $rowNum, $rows->comment1);
        		$this->excel->getActiveSheet()->setCellValue('R' . $rowNum, $rows->comment2);
        		$this->excel->getActiveSheet()->setCellValue('S' . $rowNum, $rows->comment3);
        		$rowNum++;
        	}
        }
        $this->excel->getActiveSheet()->getStyle('A6:A' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B6:B' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B6:B' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E6:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('F6:F' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('G6:G' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('J6:J' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('K6:K' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('L6:K' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('M6:M' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('N6:N' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $this->excel->getActiveSheet()->getStyle('A6:A' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C6:C' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D6:D' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('J6:J' . $last_row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('O6:O' . $last_row)->getFont()->setBold(true);

        $this->excel->getActiveSheet()->getStyle('A5:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
        	array(
        		'borders' => array(
        			'allborders' => array(
        				'style' => PHPExcel_Style_Border::BORDER_THIN,
        				'color' => array('rgb' => 'DDDDDD'),
        			),
        		),
        	)
        );
        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);
        $filename = 'Daily Pulse Report '.$start_date.' to '.$end_date.'.xlsx'; //save our workbook as this file name
        // header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        header('Set-Cookie: fileDownload=true; path=/');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
	}

    public function qry_interverners($answer_id = 42073, $emp_id = 1016){
    	$fields = "intervention_ID, intervener_empid, interveneLevel";
    	$where = "answer_id = $answer_id AND emp_id = $emp_id";
		// return $this->general_model->fetch_specific_vals($fields, $where, "tbl_daily_survey_intervention", "interveneLevel ASC");
    	$record =  $this->general_model->fetch_specific_vals($fields, $where, "tbl_daily_survey_intervention", "interveneLevel ASC");
    	var_dump($record);
    }

    private function get_next_intervention($choice_id, $occurrence, $emp_id){
    	$next_intervention['level'] = 0;
    	$next_intervention['occurrence'] = 0;
    	$next_intervention['emp_id'] = 0;
    	$max = $this->get_max_choice_escalation($choice_id);
    	if($occurrence <= $max->maximum){
    		$all_next_intervention = $this->qry_all_next_intervention($choice_id, $occurrence);
    		if(count($all_next_intervention) > 0){
    			$next_intervention['level'] = (int) $all_next_intervention[0]->intervention;
    			$next_intervention['occurrence'] = (int) $all_next_intervention[0]->occurence;
    			$next_intervention['emp_id'] =(int) $this->get_current_intervention($all_next_intervention[0]->intervention, $emp_id);
    		}
    	}
    	return $next_intervention;
    }



    private function qry_record_emp_name($start_date, $end_date, $status_id){
    	$session_emp_id = $this->session->userdata('emp_id');
    	$monitoring_acess = $this->get_monitoring_acess($session_emp_id);
    	$where_status = "";
    	$where_acess = "";
    	if($status_id != 0){
    		$where_status = "AND intervention.status_ID = $status_id";
    	}else{
			$where_status = "AND intervention.status_ID IN (3, 12)";
		}
    	if(count($monitoring_acess) > 0){
    		if($monitoring_acess->accessType != "both"){
    			$where_acess = "acc.acc_description = access.accessType";
    		}
    		if($monitoring_acess->accountExclude!=null){
    			$accountExclude="where acc_id not in (".$monitoring_acess->accountExclude.")";
    		}else{
    			$accountExclude="";
    		}
    		if($monitoring_acess->accountInclude!=null){
    			$accountInclude="acc.acc_id in (".$monitoring_acess->accountInclude.")";
    		}else{
    			$accountInclude = "";
    		}
		}
		$access_filter = "";
    	if($where_acess !="" &&  $accountInclude != ""){
    		$access_filter = "AND (".$where_acess." or ".$accountInclude.")";
    	}
    	$query_record_emp_name = "Select * from (SELECT DISTINCT(intervention.emp_id) emp_id, acc.acc_id, app.lname, app.fname, app.mname FROM tbl_daily_survey_intervention intervention, tbl_applicant app, tbl_employee emp, tbl_szfive_survey_answers answers, tbl_schedule sched, tbl_daily_pulse_monitoring_access access, tbl_account acc WHERE app.apid = emp.apid AND emp.emp_id = intervention.emp_id AND sched.sched_id = answers.sched_id AND answers.answerId = intervention.answer_id AND DATE(sched.sched_date) >= '$start_date' AND DATE(sched.sched_date) <= '$end_date' ".$where_status." AND acc.acc_id = emp.acc_id $access_filter AND access.emp_id = $session_emp_id) as x $accountExclude";
    	// $where = "app.apid = emp.apid AND emp.emp_id = intervention.emp_id AND sched.sched_id = answers.sched_id AND answers.answerId = intervention.answer_id AND DATE(sched.sched_date) >= '$start_date' AND DATE(sched.sched_date) <= '$end_date'".$where_status." ".$where_acess;
		// $table = "tbl_daily_survey_intervention intervention, tbl_applicant app, tbl_employee emp, tbl_szfive_survey_answers answers, tbl_schedule sched, tbl_daily_pulse_monitoring_access access, tbl_account acc";
		return $this->general_model->custom_query($query_record_emp_name); 

    }

    public function get_record_emp_name(){
    	$record = $this->qry_record_emp_name($this->input->post('startDate'), $this->input->post('endDate'), $this->input->post('statusId'));
    	if(count($record) > 0){
    		$data['exist'] = 1;
    		$data['record'] = $record;
    	}else{
    		$data['exist'] = 0;
    	}
    	echo json_encode($data);
    }

    public function get_daily_pulse_record_datatable(){
    	$datatable = $this->input->post('datatable');
    	$emp_id = $datatable['query']['empId'];
    	$status_val = $datatable['query']['statusId'];
    	$start_date = $datatable['query']['startDate'];
    	$end_date = $datatable['query']['endDate'];
    	$emp_where = "";
    	$status_val_where = "";
    	$session_emp_id = $this->session->userdata('emp_id');
    	$where_acess = "";
    	$monitoring_acess = $this->get_monitoring_acess($session_emp_id);
    	if(count($monitoring_acess) > 0){
    		if($monitoring_acess->accessType != "both"){
    			$where_acess = "acc.acc_description = access.accessType";
    		}
    		if($monitoring_acess->accountExclude!=null){
    			$accountExclude="where acc_id not in (".$monitoring_acess->accountExclude.")";
    		}else{
    			$accountExclude="";
    		}
    		if($monitoring_acess->accountInclude!=null){
    			$accountInclude="acc.acc_id in (".$monitoring_acess->accountInclude.")";
    		}else{
    			$accountInclude = "";
    		}
    	}
    	if((int) $emp_id !== 0){
    		$emp_where = "AND empEmp.emp_id = $emp_id";
    	}
    	if((int) $status_val !== 0){
    		$status_val_where = "AND intervention.status_ID = $status_val";
    	}else{
    		$status_val_where = "AND intervention.status_ID IN (3, 12)";
    	}
    	$access_filter = "";
    	if($where_acess !="" &&  $accountInclude != ""){
    		$access_filter = "AND (".$where_acess." or ".$accountInclude.")";
    	}
    	$query['query'] = "Select * from (SELECT acc.acc_id, answers.answerId, sched.sched_date, empEmp.emp_id empEmp_id, appEmp.lname empLname, appEmp.fname empFname, appEmp.mname empMname, appEmp.pic appEmpPic, intervention.intervention_ID, intervention.interveneLevel, intervention.status_ID, statuses.description, intervention.interventionDate, answers.dateIntervened, intervention.deadline, empIntervene.emp_id interveneEmp_id, appIntervene.lname interveneLname, appIntervene.fname interveneFname, appIntervene.mname interveneMname, appIntervene.pic picIntervene FROM tbl_daily_survey_intervention intervention, tbl_szfive_survey_answers answers, tbl_schedule sched, tbl_applicant appEmp, tbl_employee empEmp, tbl_applicant appIntervene, tbl_employee empIntervene, tbl_status statuses, tbl_daily_pulse_monitoring_access access, tbl_account acc WHERE appEmp.apid = empEmp.apid AND statuses.status_ID = intervention.status_ID AND empEmp.emp_id = intervention.emp_id AND appIntervene.apid = empIntervene.apid AND empIntervene.emp_id = intervention.intervener_empid AND sched.sched_id = answers.sched_id AND answers.answerId = intervention.answer_id AND DATE(sched.sched_date) >= '$start_date' AND DATE(sched.sched_date) <= '$end_date' ".$emp_where." ".$status_val_where." AND acc.acc_id = empEmp.acc_id $access_filter AND access.emp_id = $session_emp_id) as x $accountExclude";
    	if ($datatable['query']['pulseRecordSearch'] != '') {
    		$keyword = $datatable['query']['pulseRecordSearch'];
    		$where = "answerId LIKE '%" . (int) $keyword . "%'";
    		$query['search']['append'] = " AND ($where)";
    		$query['search']['total'] = " AND ($where)";
    	}
    	$data = $this->set_datatable_query($datatable, $query);
    	echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function check_intervention_deadline(){
    	$dateTime = $this->get_current_date_time();
    	$due_intervention = $this->get_due_intervention();
    	$missed_intervention = [];
    	$missed_intervention_recipient = [];
    	$missed_intervention_notif = [];
    	$missed_recipient = [];
    	$next_intervention = [];
    	$next_intervention_recipient = [];
    	$next_intervention_notif = [];
    	$next_recipient = [];
    	$final_missed_intervention = [];
    	$missed_intervention_count = 0;
    	$next_intervention_count = 0;
    	$final_missed_intervention_count = 0;
    	if(count($due_intervention) > 0){
			// transfer to next level supervisor
    		foreach($due_intervention as $due_intervention_row){
				// collect due intervention
    			$missed_intervention[$missed_intervention_count] = [
    				"intervention_ID" => $due_intervention_row->intervention_ID,
    				"status_ID" => 12
    			];
    			$missed_intervention_recipient[$missed_intervention_count] = [
    				"recipient_id" => $due_intervention_row->intervener_empid
    			];
				// collect missed notification
    			$emp_name = $due_intervention_row->fname." ".$due_intervention_row->lname;
    			$intervener_name = $due_intervention_row->fnameIntervene." ".$due_intervention_row->lnameIntervene;
    			$missed_intervention_notif[$missed_intervention_count] = [
    				"message" => "You have missed to intervene to the Daily Pulse of ".ucwords($emp_name)." prior ". date("g:i A", strtotime($due_intervention_row->deadline)) . " last " . date("M j, Y", strtotime($due_intervention_row->deadline)),
    				"link" => 'szfive/survey/view/' . $due_intervention_row->emp_id . '/' . $due_intervention_row->answerId,
    			];
    			$missed_intervention_count++;
    			$next_intervention_level = $this->get_next_intervention($due_intervention_row->choiceId, $due_intervention_row->occurence, $due_intervention_row->emp_id);
    			if($next_intervention_level['level'] !== 0){
					// collect next intervention of next level interveners
    				$next_intervention[$next_intervention_count] = [
    					"intervener_empid" => $next_intervention_level['emp_id'],
    					"emp_id" => $due_intervention_row->emp_id, "answer_id" => $due_intervention_row->answerId,
    					"interveneLevel" => $next_intervention_level['level'],
    					"interventionDate" => $dateTime['dateTime'],
    					"deadline" => date("Y-m-d H:i:00",strtotime("+1 day", strtotime($dateTime['dateTime']))),
    				];
    				$next_intervention_recipient[$next_intervention_count] = [
    					"recipient_id" => $next_intervention_level['emp_id']
    				];

					// collect notificatrion next intervention for next level interveners
    				$next_intervention_notif[$next_intervention_count] = [
    					"message" => "The Daily Pulse Intervention of ".ucwords($emp_name)." was forwarded to you because ".ucwords($intervener_name)." missed to intervene before the deadline: ". date("g:i A", strtotime($due_intervention_row->deadline)) . ", " . date("M j, Y", strtotime($due_intervention_row->deadline)),
    					"link" => 'szfive/survey/view/' . $due_intervention_row->emp_id . '/' . $due_intervention_row->answerId,
    				];
    				$next_intervention_count++;
    			}else{
    				$final_missed_intervention[$final_missed_intervention_count] = [
    					"answerId" => $due_intervention_row->answerId,
    					"status_ID" => 12
    				];
    				$final_missed_intervention_count++;
    			}
    		}
    		if(count($missed_intervention) > 0){
    			$this->general_model->batch_update($missed_intervention, 'intervention_ID', 'tbl_daily_survey_intervention');
    		}
    		if(count($missed_intervention_notif) > 0){
    			$missed_recipient_count = 0;
    			$first_inserted_id = $this->general_model->batch_insert_first_inserted_id($missed_intervention_notif, "tbl_system_notification");
    			for($system_notif_id = $first_inserted_id; $system_notif_id < $first_inserted_id + count($missed_intervention_notif);$system_notif_id++){
    				$missed_emp_details = $this->get_emp_details_via_emp_id($missed_intervention_recipient[$missed_recipient_count]['recipient_id']);
    				$missed_recipient[$missed_recipient_count] = [
    					"systemNotification_ID" => $system_notif_id,
    					"recipient_ID" => $missed_emp_details->uid,
    					"status_ID" => 8
    				];
    				$missed_recipient_count++;
    			}
    			$this->general_model->batch_insert($missed_recipient, "tbl_system_notification_recipient");
    		}
    		if(count($next_intervention) > 0){
    			$this->general_model->batch_insert($next_intervention, 'tbl_daily_survey_intervention');
    		}
    		if(count($next_intervention_notif) > 0){
    			$next_intervention_recipient_count = 0;
    			$first_intervention_inserted_id = $this->general_model->batch_insert_first_inserted_id($next_intervention_notif, "tbl_system_notification");
    			for($system_intervetion_notif_id = $first_intervention_inserted_id; $system_intervetion_notif_id < $first_intervention_inserted_id + count($next_intervention_notif); $system_intervetion_notif_id++){
    				$next_emp_details = $this->get_emp_details_via_emp_id($next_intervention_recipient[$next_intervention_recipient_count]['recipient_id']);
    				$next_recipient[$next_intervention_recipient_count] = [
    					"systemNotification_ID" => $system_intervetion_notif_id,
    					"recipient_ID" => $next_emp_details->uid,
    					"status_ID" => 8
    				];
    				$next_intervention_recipient_count++;
    			}
    			$this->general_model->batch_insert($next_recipient, "tbl_system_notification_recipient");
    		}
    		if(count($final_missed_intervention) > 0){
    			$this->general_model->batch_update($final_missed_intervention, 'answerId', 'tbl_szfive_survey_answers');
    		}
    	}
    }

//# Report Page- Charise codes start

// DATATABLE// 
    public function list_report_datatable(){
    	$datatable = $this->input->post('datatable');
    	$emp_id = $datatable['query']['empId'];
    	$status_id = $datatable['query']['statId'];
    	$start_date = $datatable['query']['startDate'];
		$end_date = $datatable['query']['endDate'];
		$session_emp_id = $this->session->userdata('emp_id');
    	$emp_where = "";
		$stat_where = "";
		$where_acess = "";
		$monitoring_acess = $this->get_monitoring_acess($session_emp_id);
		if(count($monitoring_acess) > 0){
			if($monitoring_acess->accessType != "both"){
				$where_acess = "acc.acc_description = access.accessType";
			}
			if($monitoring_acess->accountExclude!=null){
				$accountExclude="where acc_id not in (".$monitoring_acess->accountExclude.")";
			}else{
				$accountExclude="";
			}
			if($monitoring_acess->accountInclude!=null){
					$accountInclude="acc.acc_id in (".$monitoring_acess->accountInclude.")";
			}else{
				$accountInclude = "";
			}
		}
		$access_filter = "";
		if($where_acess !="" &&  $accountInclude != ""){
			$access_filter = "AND (".$where_acess." or ".$accountInclude.")";
		}
    	if((int) $emp_id !== 0){
    		$emp_where = "AND interven.emp_id = $emp_id ";
    	}
    	if((int) $status_id !== 0){
    		$stat_where = "AND interven.status_ID = $status_id";
    	}
		// $query['query'] = "SELECT interven.feedback,interven.status_ID,interven.intervention_ID,interven.emp_id as intervenee,interven.intervener_empid,interven.answer_id, interven.interveneLevel, sched.sched_date,ans.detailedAnswer,ans.answerId,ans.detailedAnswer, ap.fname,stat.description as stat_desc, ap.lname,ap2.fname as intervenor_fname,ap2.lname as intervenor_lname FROM tbl_szfive_survey_answers ans,tbl_schedule sched,tbl_applicant ap,tbl_employee emp,tbl_applicant ap2,tbl_employee emp2, tbl_daily_survey_intervention interven,tbl_status stat WHERE stat.status_ID=interven.status_ID AND ans.sched_id=sched.sched_id AND ans.answerId=interven.answer_id AND ap.apid=emp.apid AND interven.emp_id=emp.emp_id AND ap2.apid=emp2.apid AND interven.intervener_empid=emp2.emp_id AND DATE(sched.sched_date) >= '$start_date' AND DATE(sched.sched_date) <= '$end_date' ".$emp_where." ".$stat_where;
		$query['query'] = "Select * from (SELECT acc.acc_id, interven.feedback,interven.status_ID,interven.intervention_ID,interven.emp_id as intervenee,interven.intervener_empid,interven.answer_id, interven.interveneLevel, sched.sched_date,ans.detailedAnswer, ans.answerId, ap.fname, ap.lname,ap2.fname as intervenor_fname,ap2.lname as intervenor_lname, stat.description as stat_desc FROM tbl_szfive_survey_answers ans,tbl_schedule sched,tbl_applicant ap,tbl_employee emp,tbl_applicant ap2,tbl_employee emp2, tbl_daily_survey_intervention interven, tbl_account acc, tbl_daily_pulse_monitoring_access access, tbl_status stat WHERE stat.status_ID=interven.status_ID AND ans.sched_id=sched.sched_id AND ans.answerId=interven.answer_id AND ap.apid=emp.apid AND interven.emp_id=emp.emp_id AND ap2.apid=emp2.apid AND interven.intervener_empid=emp2.emp_id AND DATE(sched.sched_date) >= '$start_date' AND DATE(sched.sched_date) <= '$end_date' ".$emp_where." ".$stat_where." AND acc.acc_id = emp.acc_id $access_filter AND access.emp_id = $session_emp_id) as x $accountExclude";
    	if ($datatable['query']['reportVal'] != '') {
    		$keyword = $datatable['query']['reportVal'];
			// var_dump(is_numeric($keyword));
    		if(is_numeric($keyword)){
    			$where = "answerId LIKE '%".(int) $keyword."%'";
    		}else{
    			$where = "answerId = '". $keyword. "'";
    		}
    		// $where = "ans.answerId LIKE '%" . (int) $keyword . "%'";
    		$query['search']['append'] = " AND ($where)";
    		$query['search']['total'] = " AND ($where)";
    	}
    	$data = $this->set_datatable_query($datatable, $query);
    	echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
 //Fetch answer, feedback and comments data
    public function fetch_answerFeedbackComments(){
    	$int_id=$this->input->post("intervention_ID");
    	$ans_id=$this->input->post("answerId");
    	$query_answerFeedback = $this->general_model->custom_query("SELECT ans.detailedanswer,ans.answerId,ans.answer,ans.dateIntervened,inter.intervention_ID,inter.feedback, ap.fname, ap.lname, choice.label as choice_label FROM tbl_szfive_survey_answers ans, tbl_daily_survey_intervention inter, tbl_employee emp, tbl_applicant ap, tbl_szfive_survey_choices choice WHERE inter.intervention_ID=$int_id AND ans.answerId=$ans_id AND ans.answerId=inter.answer_id AND emp.emp_id=inter.intervener_empid AND emp.apid=ap.apid AND ans.answer=choice.levels");
    	$query_comments=$this->general_model->custom_query("SELECT com.comment,com.comment_ID,com.dateCreated,ap.fname,ap.lname FROM tbl_daily_survey_intervention inter LEFT JOIN tbl_daily_survey_comment com ON inter.intervention_ID=com.intervention_ID LEFT JOIN tbl_employee emp ON emp.emp_id=com.emp_id LEFT JOIN tbl_applicant ap ON emp.apid=ap.apid WHERE inter.intervention_ID=$int_id");
    	$display_moredetails['answers_feedbacks']= $query_answerFeedback;
    	$display_moredetails['comments']= $query_comments;
    	echo json_encode($display_moredetails);
    }

//Query get select distinct respondent
    private function query_record_respondent($start_date, $end_date, $statusid){
		$session_emp_id = $this->session->userdata('emp_id');
    	$monitoring_acess = $this->get_monitoring_acess($session_emp_id);
		$stat_where="";
    	$where_acess = "";		
    	if((int) $statusid !== 0){
    		$stat_where = "AND intervention.status_ID = $statusid";
		}
		if(count($monitoring_acess) > 0){
    		if($monitoring_acess->accessType != "both"){
    			$where_acess = "acc.acc_description = access.accessType";
    		}
    		if($monitoring_acess->accountExclude!=null){
    			$accountExclude="where acc_id not in (".$monitoring_acess->accountExclude.")";
    		}else{
    			$accountExclude="";
    		}
    		if($monitoring_acess->accountInclude!=null){
    			$accountInclude="acc.acc_id in (".$monitoring_acess->accountInclude.")";
    		}else{
    			$accountInclude = "";
    		}
		}
		$access_filter = "";
    	if($where_acess !="" &&  $accountInclude != ""){
    		$access_filter = "AND (".$where_acess." or ".$accountInclude.")";
    	}
    	// $fields = "DISTINCT(intervention.emp_id) emp_id, app.lname, app.fname, app.mname";
    	// $where = "app.apid = emp.apid AND emp.emp_id = intervention.emp_id AND sched.sched_id = answers.sched_id AND answers.answerId = intervention.answer_id AND DATE(sched.sched_date) >= '$start_date' AND DATE(sched.sched_date) <= '$end_date' ".$stat_where;

    	// $table = "tbl_daily_survey_intervention intervention, tbl_applicant app, tbl_employee emp, tbl_szfive_survey_answers answers, tbl_schedule sched";
		// return $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
		$query_record_emp_name = "Select * from (SELECT DISTINCT(intervention.emp_id) emp_id, acc.acc_id, app.lname, app.fname, app.mname FROM tbl_daily_survey_intervention intervention, tbl_applicant app, tbl_employee emp, tbl_szfive_survey_answers answers, tbl_schedule sched, tbl_daily_pulse_monitoring_access access, tbl_account acc WHERE app.apid = emp.apid AND emp.emp_id = intervention.emp_id AND sched.sched_id = answers.sched_id AND answers.answerId = intervention.answer_id AND DATE(sched.sched_date) >= '$start_date' AND DATE(sched.sched_date) <= '$end_date' ".$stat_where." AND acc.acc_id = emp.acc_id $access_filter AND access.emp_id = $session_emp_id) as x $accountExclude";
		return $this->general_model->custom_query($query_record_emp_name); 

    }

    public function get_record_respondent_emp_name(){
    	$record = $this->query_record_respondent($this->input->post('startDate'), $this->input->post('endDate'),$this->input->post('statusID'));
    	if(count($record) > 0){
    		$data['exist'] = 1;
    		$data['record'] = $record;
    	}else{
    		$data['exist'] = 0;
    	}
    	echo json_encode($data);
    }


//# Report Page- Charise codes end

	//MIC2X CODES END ------------------------
	//MJM CODES Start ------------------------
    public function showIntervenor(){
    	$answerId = $this->input->post("answerId");
    	$record=$this->general_model->custom_query("SELECT feedback,interveneLevel,answers.answerId, sched.sched_date, empEmp.emp_id empEmp_id, appEmp.lname empLname, appEmp.fname empFname, appEmp.mname empMname, appEmp.pic appEmpPic, intervention.intervention_ID, intervention.interveneLevel, intervention.status_ID, statuses.description, intervention.interventionDate, answers.dateIntervened, intervention.deadline, empIntervene.emp_id interveneEmp_id, appIntervene.lname interveneLname, appIntervene.fname interveneFname, appIntervene.mname interveneMname, appIntervene.pic picIntervene FROM tbl_daily_survey_intervention intervention, tbl_szfive_survey_answers answers, tbl_schedule sched, tbl_applicant appEmp, tbl_employee empEmp, tbl_applicant appIntervene, tbl_employee empIntervene, tbl_status statuses WHERE appEmp.apid = empEmp.apid AND statuses.status_ID = intervention.status_ID AND empEmp.emp_id = intervention.emp_id AND appIntervene.apid = empIntervene.apid AND empIntervene.emp_id = intervention.intervener_empid AND sched.sched_id = answers.sched_id AND answers.answerId = intervention.answer_id AND intervention.answer_id = $answerId");

    	echo json_encode($record);
    }
    public function insertFeedback(){

    	$message = $this->input->post('message');
    	$answerId = $this->input->post('answerId');
		$uid = $this->input->post('uid'); // intervened
		$data["employee"] = $this->fetch_employee_ID($uid);
		$data["interveneLevel"] = $this->general_model->custom_query("SELECT max(interveneLevel) as max FROM tbl_daily_survey_intervention where answer_id= $answerId");
		
		$now = date("Y-m-d H:i:00");
		$intervene['intervener_empid'] = $_SESSION["emp_id"];
		$intervene['emp_id'] = $data["employee"][0]->emp_id;
		$intervene['answer_id'] = $answerId;
		$intervene['interveneLevel'] = $data["interveneLevel"][0]->max+1;
		$intervene['status_ID'] = 3;
		$intervene['feedback'] = $message;
		$intervene['interventionDate'] = $now;
		$intervene['deadline'] = $now;
		$insertIntervene = $this->general_model->insert_vals($intervene, 'tbl_daily_survey_intervention');
		
		if($insertIntervene>0){
			$answer['dateIntervened']=date("Y-m-d H:i:s");
			$answer['status_ID'] = 3;
			$whereanswer = "answerId = $answerId ";
			$updateAnswers = $this->general_model->update_vals($answer, $whereanswer, 'tbl_szfive_survey_answers');
			if($updateAnswers>0){
				$recipient[0]= [
					'userId' => $uid,
				];
				$notif_message = "gave you a feedback on your SnapSZ but it needs your confirmation. <br><small><b>ID</b>:".str_pad($answerId, 8, '0', STR_PAD_LEFT)." </small>";
				$link = "szfive/survey/view/".$data["employee"][0]->emp_id."/".$answerId;	
				$insert_stat= $this->set_notif($_SESSION["uid"],$notif_message, $link, $recipient);

			}
		}
		
		echo json_encode($insert_stat);
		
		
	}

	public function empSelectIntervene(){
		$choice = $this->input->post('choice');
		if($choice==2){
			$append = "and a.status_ID in (2)";
		}elseif($choice==12){
			$append = "and a.status_ID in (12)";
		}else{
			$append = "and a.status_ID in (2,12)";
		}
		
		$query = $this->general_model->custom_query("SELECT distinct(e.emp_id),fname,lname FROM tbl_szfive_survey_answers a,tbl_daily_survey_intervention b,tbl_schedule c,tbl_user d,tbl_employee e,tbl_applicant f,tbl_status g where g.status_ID=a.status_ID and d.uid = a.employeeId and e.emp_id=d.emp_id and f.apid=e.apid and a.answerId=b.answer_id and a.sched_id=c.sched_id $append group by answerId order by lname");
		
		echo json_encode($query);

	}
	public function showongoingintervene($choice,$empid){

		$datatable = $this->input->post('datatable');
		
		$fields = "accessType,accountExclude,accountInclude";
		$where = "emp_id = ".$_SESSION['emp_id'];
		$accessType =  $this->general_model->fetch_specific_vals($fields, $where, "tbl_daily_pulse_monitoring_access");
		$accountInclude ="";
		$appAccess="";
		
		

		// Select * from (SELECT answerId,surveyId,employeeId, a.occurence, a.sched_id, i.label,answer,sched_date,detailedAnswer,fname,lname,g.description,a.status_ID FROM tbl_szfive_survey_answers a,tbl_daily_survey_intervention b,tbl_schedule c,tbl_user d,tbl_employee e,tbl_applicant f,tbl_status g,tbl_account h, tbl_szfive_survey_choices i where i.levels = a.answer AND g.status_ID=a.status_ID and h.acc_id=e.acc_id and h.acc_description='Admin' and d.uid = a.employeeId and e.emp_id=d.emp_id and f.apid=e.apid and a.answerId=b.answer_id and a.sched_id=c.sched_id and a.status_ID in (2,12)  group by answerId) x
		if($empid==0){
			$empid_append = "";
		}else{
			$empid_append = " and e.emp_id=$empid";
		}
		if($choice==2){
			$append = "and a.status_ID in (2)";
		}elseif($choice==12){
			$append = "and a.status_ID in (12)";
		}else{
			$append = "and a.status_ID in (2,12)";
		}
		if($accessType[0]->accessType=="both"){
			$appAccess="";
		}else{
			$appAccess= "h.acc_description='".ucfirst($accessType[0]->accessType)."'";
		}
		
		if($accessType[0]->accountExclude!=null){
			$accountExclude="where acc_id not in (".$accessType[0]->accountExclude.")";
		}else{
			$accountExclude="";
		}
		if($accessType[0]->accountInclude!=null){
			$accountInclude="h.acc_id in (".$accessType[0]->accountInclude.")";
		}else{
			$accountInclude = "";
		}
		if($appAccess=="" && $accountInclude==""){
			$appended ="";
		}else if($appAccess=="" && $accountInclude!=""){
			$appended ="and ".$accountInclude;
		}else if($appAccess!="" && $accountInclude==""){
			$appended ="and ".$appAccess;
		}else{
			$appended ="and (".$appAccess." or ".$accountInclude.")";
		}
		$query['query']="SELECT * from (SELECT answerId,surveyId,employeeId, a.occurence, a.sched_id, i.label,answer,sched_date,detailedAnswer,fname,lname,g.description,a.status_ID,h.acc_id FROM tbl_szfive_survey_answers a,tbl_daily_survey_intervention b,tbl_schedule c,tbl_user d,tbl_employee e,tbl_applicant f,tbl_status g,tbl_account h, tbl_szfive_survey_choices i where i.levels = a.answer AND g.status_ID=a.status_ID and h.acc_id=e.acc_id $appended and d.uid = a.employeeId and e.emp_id=d.emp_id and f.apid=e.apid and a.answerId=b.answer_id and a.sched_id=c.sched_id $append $empid_append group by answerId) as x $accountExclude ";
		
	
		if (isset($datatable['query']['ongoingSearchs']) && $datatable['query']['ongoingSearchs']!="") {
			$keyword = $datatable['query']['ongoingSearchs'];

			$query['search']['append'] = " and  (answerId like '%" . (int)$keyword. "%') ";

			$query['search']['total'] = " and (answerId like '%" . (int)$keyword . "%')";
		}

		$data = $this->set_datatable_query($datatable, $query);
		$data["acc"] = $appAccess;

		echo json_encode($data);
		// echo json_encode($record);
	}
	//MJM CODES End ------------------------
	

}

