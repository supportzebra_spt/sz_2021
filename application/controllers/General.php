<?php

defined('BASEPATH') or exit('No direct script access allowed');

class General extends CI_Controller
{
	
	

	private $perPage = 10; //how many notifications queried per scroll.
	// public  $dir = "C:\Users\SDT-Programmer\Sites\sz"; //if windows Cariliman's local
	// public $dir = "http://localhost/sz/"; //linux
	public $dir = "/var/www/html/sz"; //linux

	public function __construct()
	{
		
		parent::__construct();
		/* if($_SERVER["REMOTE_ADDR"] != "49.145.229.182"){
		echo "Server Maintenance Ongoing...";
		exit();
		} */
		if (!$this->session->has_userdata('uid')) {
			redirect('login');
			date_default_timezone_set('Asia/Manila');
		}
	}

	protected function error_403()
	{
		// $this->load->view('_partials/header', $data);
		$this->load->view('errors/custom/error_403');
	}

	protected function error_500()
	{
		$this->load->view('errors/custom/error_500');
	}

	public function error_404()
	{
		$this->output->set_status_header('404');
		$this->load->view('errors/custom/error_404');
	}

	// protected function check_access()
	// {
	// 	$user_id = $this->session->userdata('uid');
	// 	$current_uri = $this->uri->uri_string();
	// 	$uri_array = explode("/", $current_uri);
	// 	if($uri_array[0] == 'discipline'){
	// 		$current_uri = $uri_array[0].'/'.$uri_array[1];
	// 	}
	// 	$fields = "userAccess.useraccess_id";
	// 	$where = "userAccess.menu_item_id = menuItems.menu_item_id AND menuItems.item_link LIKE '%" . $current_uri . "%' AND userAccess.user_id = $user_id AND is_assign =1";
	// 	$tables = "tbl_user_access userAccess, tbl_menu_items menuItems";
	// 	$record = $this->general_model->fetch_specific_val($fields, $where, $tables);
	// 	if (count($record) > 0) {
	// 		return true;
	// 	} else {
	// 		$this->error_403();
	// 		return false;
	// 	}
	// }

	protected function check_access($uri = null)
	{
		$user_id = $this->session->userdata('uid');
		$current_uri = $uri==null? $this->uri->uri_string(): $uri;
		$uri_array = explode("/", $current_uri);
		if($uri_array[0] == 'discipline'){
			$current_uri = $uri_array[0].'/'.$uri_array[1];
		}
		$fields = "userAccess.useraccess_id";
		$where = "userAccess.menu_item_id = menuItems.menu_item_id AND menuItems.item_link LIKE '%" . $current_uri . "%' AND userAccess.user_id = $user_id AND is_assign =1";
		$tables = "tbl_user_access userAccess, tbl_menu_items menuItems";
		$record = $this->general_model->fetch_specific_val($fields, $where, $tables);
		if (count($record) > 0) {
			return true;
		} else {
			$this->error_403();
			return false;
		}
	}

	// protected function check_access2()
	// {
	// 	$user_id = $this->session->userdata('uid');
	// 	$current_uri = $this->uri->uri_string();
	// 	$uri_array = explode("/", $current_uri);
	// 	if($uri_array[0] == 'discipline'){
	// 		$current_uri = $uri_array[0].'/'.$uri_array[1];
	// 	}
	// 	$fields = "userAccess.useraccess_id";
	// 	$where = "userAccess.menu_item_id = menuItems.menu_item_id AND menuItems.item_link LIKE '%" . $current_uri . "%' AND userAccess.user_id = $user_id AND is_assign =1";
	// 	$tables = "tbl_user_access userAccess, tbl_menu_items menuItems";
	// 	$record = $this->general_model->fetch_specific_val($fields, $where, $tables);
	// 	if (count($record) > 0) {
	// 		return true;
	// 	} else {
	// 		$this->error_403();
	// 		return false;
	// 	}
	// }

	public function checkIfSiteOfficer($site)
	{
		$fields = "pofficer_ID";
		$where = "a.site_ID=b.site_ID and code='" . $site . "' and a.emp_id=" . $_SESSION["emp_id"];
		$rs = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_officer_site a,tbl_site b");
		if (count($rs) > 0) {
			return true;
		} else {
			$this->error_403();
			return false;
		}
	}

	protected function check_access_payroll()
	{

		$user_id = $this->session->userdata('uid');
		$current_uri = $this->uri->uri_string();
		$site_cnt = (strpos($current_uri, "cdo")) ? strpos($current_uri, "/cdo") : strpos($current_uri, "/cebu");
		$removestr = substr($current_uri, $site_cnt);
		$final_url = str_replace($removestr, "", $current_uri);
		$fields = "userAccess.useraccess_id";
		$where = "userAccess.menu_item_id = menuItems.menu_item_id AND menuItems.item_link LIKE '%" . $final_url . "%' AND userAccess.user_id = $user_id AND is_assign =1";
		$tables = "tbl_user_access userAccess, tbl_menu_items menuItems";
		$record = $this->general_model->fetch_specific_val($fields, $where, $tables);
		if (count($record) > 0) {
			return true;
		} else {
			$this->error_403();
			return false;
		}
	}

	protected function load_template_view($path, $data)
	{
		$query = $this->db->query("select  distinct(a.tab_id),tab_name,icon from tbl_menu_tab_item a,tbl_menu_items b,tbl_user_access c,tbl_user d where a.tab_id =b.tab_id and b.menu_item_id=c.menu_item_id and d.uid=c.user_id and d.uid='" . $this->session->userdata('uid') . "'  and c.is_assign=1 and b.isNewTemplate=1 and b.isSidebar=1 ORDER BY tab_name");
		$rez = $query->result();
		$data["res"] = array();
		foreach ($rez as $key => $v) {
			$data["res"][$v->tab_name . "|" . $v->icon] = $this->sideBarMenu($v->tab_id);
		}
		$data['uri_segment'] = $this->uri->segment_array();

		$data['session'] = $this->session->userdata();
		$this->load->view('_partials/header', $data);
		$this->load->view('_partials/sidebar', $data);
		$this->load->view('_partials/header_topbar', $data);
		$this->load->view($path);
		$this->load->view('_partials/footer', $data);
	}

	private function sideBarMenu($rez)
	{
		$query = $this->db->query("select item_link,item_title from tbl_menu_tab_item a,tbl_menu_items b,tbl_user_access c,tbl_user d where a.tab_id =b.tab_id and b.menu_item_id=c.menu_item_id and d.uid=c.user_id and b.tab_id =" . $rez . "  and uid='" . $this->session->userdata('uid') . "'  and c.is_assign=1  and b.isSidebar=1 and b.isNewTemplate=1 ORDER BY item_title ASC");
		return $query->result();
	}

	protected function time_elapsed_string($datetime, $full = false)
	{
		date_default_timezone_set('Asia/Manila');

		$now = new DateTime();
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) {
			$string = array_slice($string, 0, 1);
		}
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

	public function get_latest_notifications()
	{
		$limiter = $this->input->post('limiter'); //start of query limit
		$uid = $this->session->userdata('uid');
		// $uid = 5;
		$notifications = $this->general_model->custom_query("SELECT a.notification_ID,a.sender_ID,a.message,a.link,a.createdOn,b.notificationRecipient_ID,b.recipient_ID,c.status_ID,c.description as status FROM tbl_notification a,tbl_notification_recipient b,tbl_status c WHERE a.notification_ID=b.notification_ID AND b.recipient_ID=$uid AND b.status_ID=c.status_ID ORDER BY a.createdOn DESC LIMIT $limiter,$this->perPage");

		if (empty($notifications)) {
			$status = "Empty";
		} else {
			foreach ($notifications as $notif) {
				$notif->timeElapsed = $this->time_elapsed_string($notif->createdOn);
				$user = $this->general_model->fetch_specific_val("c.fname,c.lname,c.pic,a.uid", "a.emp_id=b.emp_id AND b.apid=c.apid AND a.uid=$notif->sender_ID", "tbl_user a,tbl_employee b, tbl_applicant c");
				$notif->sender = $user->lname . ", " . $user->fname;
				$notif->pic = $user->pic;
				$notif->uid = $user->uid;
			}
			$status = "Success";
		}
		echo json_encode(array('status' => $status, 'notifications' => $notifications));
	}

	public function get_latest_system_notifications($type = 'general')
	{
		$limiter = $this->input->post('limiter'); //start of query limit
		$uid = $this->session->userdata('uid');
		// $uid = 5;
		$notifications = $this->general_model->custom_query("SELECT a.reference,a.systemNotification_ID,a.message,a.link,a.createdOn,b.systemNotificationRecipient_ID,b.recipient_ID,c.status_ID,c.description as status FROM tbl_system_notification a,tbl_system_notification_recipient b,tbl_status c WHERE a.systemNotification_ID=b.systemNotification_ID AND b.recipient_ID=$uid AND b.status_ID=c.status_ID AND reference = '$type' ORDER BY a.createdOn DESC LIMIT $limiter,$this->perPage");

		if (empty($notifications)) {
			$status = "Empty";
		} else {
			foreach ($notifications as $notif) {
				$notif->timeElapsed = $this->time_elapsed_string($notif->createdOn);
			}
			$status = "Success";
		}
		echo json_encode(array('status' => $status, 'notifications' => $notifications));
	}

	public function get_notification_details()
	{
		$notification_ID = $this->input->post('notification_ID');
		$notifications = $this->general_model->fetch_specific_val("sender_ID,message,link,createdOn", "notification_ID=$notification_ID", "tbl_notification");
		if (empty($notifications) or $notifications === '') {
			$status = "Empty";
		} else {
			$status = "Success";
			$recipients = $this->general_model->fetch_specific_vals("a.notificationRecipient_ID,a.recipient_ID,a.status_ID,a.createdOn,a.updatedOn,b.description as status", "a.notification_ID=$notification_ID AND a.status_ID=b.status_ID", "tbl_notification_recipient a,tbl_status b");
			$user = $this->general_model->fetch_specific_val("c.fname,c.lname,c.pic", "a.emp_id=b.emp_id AND b.apid=c.apid AND a.uid=$notifications->sender_ID", "tbl_user a,tbl_employee b, tbl_applicant c");
			$notifications->sender = $user->lname . ", " . $user->fname;
			$notifications->pic = $user->pic;
			foreach ($recipients as $reci) {
				$reci->timeElapsed = $this->time_elapsed_string($reci->createdOn);
				$reci->unreadCount = $this->general_model->fetch_specific_val("COUNT(*) as unreadCount", "a.notification_ID=b.notification_ID AND b.recipient_ID=$reci->recipient_ID AND b.status_ID=c.status_ID AND c.description='unread'", "tbl_notification a,tbl_notification_recipient b,tbl_status c ")->unreadCount;
			}
			$notifications->recipients = $recipients;
		}

		echo json_encode(array('status' => $status, 'notifications' => $notifications));
	}

	public function get_system_notification_details()
	{

		$systemNotification_ID = $this->input->post('systemNotification_ID');
		$reference = $this->input->post('reference');
		$notifications = $this->general_model->fetch_specific_val("message,link,createdOn,reference", "systemNotification_ID=$systemNotification_ID AND reference='$reference'", "tbl_system_notification");

		if (empty($notifications) or $notifications === '') {
			$status = "Empty";
		} else {
			$status = "Success";
			$recipients = $this->general_model->fetch_specific_vals("a.systemNotificationRecipient_ID,a.recipient_ID,a.status_ID,a.createdOn,a.updatedOn,b.description as status", "a.systemNotification_ID=$systemNotification_ID AND a.status_ID=b.status_ID", "tbl_system_notification_recipient a,tbl_status b");
			foreach ($recipients as $reci) {
				$reci->timeElapsed = $this->time_elapsed_string($reci->createdOn);
				$reci->unreadCount = $this->general_model->fetch_specific_val("COUNT(*) as unreadCount", "a.systemNotification_ID=b.systemNotification_ID AND b.recipient_ID=$reci->recipient_ID AND b.status_ID=c.status_ID AND c.description='unread'  AND a.reference='$reference'", "tbl_system_notification a,tbl_system_notification_recipient b,tbl_status c ")->unreadCount;
			}
			$notifications->recipients = $recipients;
		}

		echo json_encode(array('status' => $status, 'notifications' => $notifications));
	}


	public function set_datatable_query($datatable, $query)
	{
		$sql = $query['query'];

		$page = $datatable['pagination']['page'];
		$pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
		$perpage = $datatable['pagination']['perpage'];
		$sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
		$field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';

		if (isset($query['search']['append'])) {
			$sql .= $query['search']['append'];

			$search = $query['query'] . $query['search']['total'];

			$total = count($this->general_model->custom_query($search));
			$pages = ceil($total / $perpage);

			$page = ($page > $pages) ? 1 : $page;
		} else {
			$total = count($this->general_model->custom_query($sql));
		}

		if (isset($datatable['pagination'])) {

			$offset = $page * $perpage - $perpage;
			$limit = ' LIMIT ' . $offset . ' ,' . $perpage;
			$order = $field ? " ORDER BY  " . $field : '';

			if ($perpage < 0) {
				$limit = ' LIMIT 0';
			}

			$sql .= $order . ' ' . $sort . $limit;
		}

		$data = $this->general_model->custom_query($sql);
		$meta = [
			"page" => intval($page),
			"pages" => intval($pages),
			"perpage" => intval($perpage),
			"total" => $total,
			"sort" => $sort,
			"field" => $field,
		];

		$result = [
			'meta' => $meta,
			'data' => $data,
		];

		return $result;
	}

	// MIC
	// NOTIFICATION
	protected function ordinal($number)
	{
		$ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
		if ((($number % 100) >= 11) && (($number % 100) <= 13)) {
			return $number . 'th';
		} else {
			return $number . $ends[$number % 10];
		}
	}

	// REQUEST NOTIFICATION
	private function create_notif($sender_id, $notif_mssg, $link)
	{
		$data['sender_ID'] = $sender_id;
		$data['message'] = $notif_mssg;
		$data['link'] = $link;
		return $notification_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_notification');
	}

	private function send_notif($notif_Id, $recipients)
	{
		$notif_recip = [];
		for ($loop = 0; $loop < count($recipients); $loop++) {
			$notif_recip[$loop] = [
				"notification_ID" => $notif_Id,
				"recipient_ID" => $recipients[$loop]['userId'],
				"status_ID" => 8,
			];
		}
		return $insert_status = $this->general_model->batch_insert($notif_recip, 'tbl_notification_recipient');
	}

	protected function set_notif($sender_id, $notif_mssg, $link, $recipientArray)
	{
		$notif_id = [];
		$approval_type = 1; // 1- Sequential, 2 - Parallel
		if ($approval_type === 1) {
			$recipients[] = $recipientArray[0];
		} else {
			$recipients = $recipientArray;
		}
		$notif_id[0] = $this->create_notif($sender_id, $notif_mssg, $link);
		$data['notif_id'] = $notif_id;
		$data['send_stat'] = $this->send_notif($notif_id[0], $recipients);
		return $data;
	}

	protected function set_notif_preceeding($sender_id, $notif_mssg, $link, $recipientArray)
	{
		$notif_id[0] = $this->create_notif($sender_id, $notif_mssg, $link);
		$data['notif_id'] = $notif_id;
		$data['send_stat'] = $this->send_notif($notif_id[0], $recipientArray);
		return $data;
	}

	//SYSTEM NOTIFICATION
	protected function send_system_notif($notif_Id, $recipients)
	{
		$notif_recip = [];
		for ($loop = 0; $loop < count($recipients); $loop++) {
			$notif_recip[$loop] = [
				"systemNotification_ID" => $notif_Id,
				"recipient_ID" => $recipients[$loop]['userId'],
				"status_ID" => 8,
			];
		}
		return $insert_status = $this->general_model->batch_insert($notif_recip, 'tbl_system_notification_recipient');
	}

	protected function create_system_notif($notif_mssg, $link, $reference)
	{
		$data['message'] = $notif_mssg;
		$data['link'] = $link;
		$data['reference'] = $reference;
		return $notification_id = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_system_notification');
	}

	protected function set_system_notif($notif_mssg, $link, $recipientArray, $reference='general')
	{
		$notif_id = [];
		$approval_type = 1; // 1- Sequential, 2 - Parallel
		if ($approval_type === 1) {
			$recipients[] = $recipientArray[0];
		} else {
			$recipients = $recipientArray;
		}
		$notif_id[0] = $this->create_system_notif($notif_mssg, $link, $reference);
		$data['notif_id'] = $notif_id;
		$data['send_stat'] = $this->send_system_notif($notif_id[0], $recipients);
		return $data;
	}

	protected function set_system_notif_preceeding($notif_mssg, $link, $recipientArray, $reference='general')
	{
		$notif_id[0] = $this->create_system_notif($notif_mssg, $link, $reference);
		$data['notif_id'] = $notif_id;
		$data['send_stat'] = $this->send_system_notif($notif_id[0], $recipientArray);
		return $data;
	}

	private function get_approval_number($module_id)
	{
		$fields = "module, numberOfApproval";
		$where = "module_ID = $module_id";
		$approval_num = $this->general_model->fetch_specific_val($fields, $where, "tbl_module");
		return $approval_num->numberOfApproval;
	}

	private function get_module_links($module_id)
	{
		$fields = "approvalPageLink, pendingRequestPage, monitoringPageLink, abbreviation";
		$where = "module_ID = $module_id";
		return $links = $this->general_model->fetch_specific_val($fields, $where, "tbl_module");
	}

	protected function get_direct_supervisor($user_id)
	{
		$fields = "emp.Supervisor";
		$where = "emp.emp_id = users.emp_id AND users.uid = $user_id";
		$tables = "tbl_employee emp, tbl_user users";
		$sup_emp_id = $this->general_model->fetch_specific_val($fields, $where, $tables);
		if ($sup_emp_id->Supervisor === 0) {
			return 0;
		} else {
			$fields = "uid";
			$where = "emp_id = $sup_emp_id->Supervisor";
			$tables = "tbl_user";
			return $sup_user_details = $this->general_model->fetch_specific_val($fields, $where, $tables);
		}
	}

	protected function get_direct_supervisor_emp($user_id)
	{
		$fields = "emp.Supervisor";
		$where = "emp.emp_id = users.emp_id AND users.uid = $user_id";
		$tables = "tbl_employee emp, tbl_user users";
		$sup_emp_id = $this->general_model->fetch_specific_val($fields, $where, $tables);
		if ($sup_emp_id->Supervisor === 0) {
			return 0;
		} else {
			return $sup_emp_id;
		}
	}

	protected function get_direct_supervisor_emp_id($emp_id)
	{
		$fields = "Supervisor";
		$where = "emp_id = $emp_id";
		return $this->general_model->fetch_specific_val($fields, $where, 'tbl_employee');
	}
	// public function get_current_intervention($intervention_level, $emp_id){
	//     $hr_manager = 522;
	//     $managing_director = 980;
	//     $intervenee = [];
	//     $intervenee_count = 0;
	//     $direct_sup = $this->get_direct_supervisor_via_emp_id($emp_id);
	//     if(count($direct_sup) > 0){
	//         $current_intervenee = (int) $direct_sup->Supervisor;
	//         if($current_intervenee == $managing_director){
	//             if(array_search($hr_manager, $intervenee) === FALSE){
	//                 $intervenee[0] = $hr_manager;
	//                 $current_intervenee = $hr_manager;
	//             }else{
	//                 $intervenee[0] = $current_intervenee;
	//             }
	//         }else{
	//             $intervenee[0] = $current_intervenee;
	//         }
	//         for ($loop = 1; $loop < $intervention_level; $loop++)
	//         {
	//             $direct_sup_supervisor = $this->get_direct_supervisor_via_emp_id($current_intervenee);
	//             if((count($direct_sup_supervisor) > 0) && (strlen($direct_sup_supervisor->Supervisor) !== 0)){
	//                 $intervenee_count++;
	//                 $current_intervenee = (int) $direct_sup_supervisor->Supervisor;
	//                 if($current_intervenee == $managing_director){
	//                     if(array_search($hr_manager, $intervenee) === FALSE){
	//                         $intervenee[$intervenee_count] = $hr_manager;
	//                         $current_intervenee = $hr_manager;
	//                     }else{
	//                         $intervenee[$intervenee_count] = $current_intervenee;
	//                     }
	//                 }else{
	//                     $intervenee[$intervenee_count] = $current_intervenee;
	//                 }
	//                 if($intervention_level == 4 && $loop == 3){
	//                     if(array_search($hr_manager, $intervenee) === FALSE){
	//                         $intervenee[$intervenee_count] = $hr_manager;
	//                     }
	//                 }else if($intervention_level >= 5  && $loop >= 4){
	//                     $intervenee[$intervenee_count] = 980;
	//                 }      
	//             }
	//         }
	//         return $intervenee[count($intervenee)-1];
	//     }
	//     else
	//     {
	//         return 0;
	//     }
	// }
	private function set_deadline($approvers, $module_id, $request_id, $column, $requestor)
	{
		for ($loop = 0; $loop < count($approvers); $loop++) {
			unset($approvers[$loop]['approvalStatus_ID']);
			unset($approvers[$loop][$column]);
			$approvers[$loop]["module_ID"] = $module_id;
			$approvers[$loop]["request_ID"] = $request_id;
			$approvers[$loop]["status_ID"] = 2;
			$approvers[$loop]["requestor"] = $requestor;
		}
		// //var_dump($approvers);
		return $insert_stat = $this->general_model->batch_insert($approvers, 'tbl_deadline');
	}

	public function get_custom_approval_assignment($acc_id){
        $qry = "SELECT customs.requestCustomApproval_ID customApprovalId, acc.acc_name, customs.level, customs.changeType, customs.dateCreated, customs.emp_id approverEmpId, appApprover.fname approverFname, appApprover.lname approverLname, appApprover.mname, changes.requestApprovalChange_ID, changes.requestCustomApproval_ID customApprovalChangeId, changes.emp_id assignEmpId, appAssign.fname assignFname, appAssign.lname assignLname, appAssign.mname assignMname FROM tbl_request_custom_approval customs LEFT JOIN tbl_request_approval_change changes ON changes.requestCustomApproval_ID = customs.requestCustomApproval_ID INNER JOIN tbl_employee empApprover ON empApprover.emp_id = customs.emp_id INNER JOIN tbl_applicant appApprover ON appApprover.apid = empApprover.apid INNER JOIN tbl_account acc ON acc.acc_id = customs.account_ID LEFT JOIN tbl_employee empAssign ON empAssign.emp_id = changes.emp_id LEFT JOIN tbl_applicant appAssign ON appAssign.apid = empAssign.apid WHERE customs.account_ID = $acc_id";
        return $this->general_model->custom_query($qry);
	}

	public function filter_approval_exemption_reassignment($supervisors, $acc_id, $requestor_emp_id){
		$custom_assignment = $this->get_custom_approval_assignment($acc_id);
		// var_dump($custom_assignment);
		if(count($custom_assignment) > 0){
			for ($sup_loop = 0; $sup_loop < count($supervisors); $sup_loop++) {
				$level =  $sup_loop + 1;
				$emp_id =  $supervisors[$sup_loop]['emp_id'];
				$type = "exempt";
				$remove_sup = array_merge(array_filter(
					$custom_assignment,
					function ($e) use ($level,  $emp_id, $type) {
						return (($e->level ==  $level) && ($e->approverEmpId ==  $emp_id) && ($e->changeType ==  $type));
					}
				));
				if (count($remove_sup) > 0) {
					// echo "remove sup  $sup_loop <br>";
					if(count($supervisors) > 1){
						unset($supervisors[$sup_loop]);
					}
				} else {
					// echo "reassign sup  $sup_loop <br>";
					$level =  $sup_loop + 1;
					$emp_id =  $supervisors[$sup_loop]['emp_id'];
					$type = "change";
					$reassign_sup = array_filter(
						$custom_assignment,
						function ($e) use ($level,  $emp_id, $type) {
							return (($e->level ==  $level) && ($e->approverEmpId ==  $emp_id) && ($e->changeType ==  $type));
						}
					);
					$reassign_sup = array_merge($reassign_sup);
					if (count($reassign_sup) > 0) { // check if reassign sup
						if ($reassign_sup[0]->assignEmpId !=  $requestor_emp_id) {
							$supervisors[$sup_loop]['emp_id'] =  $reassign_sup[0]->assignEmpId;
						}
					}
				}
			}
		}
		return $supervisors;
	}

	public function set_approvers_deadline_and_details($filtered_sup, $date_filed, $column, $request_id, $module_id, $requestor_user_id){
		$filtered_supervisors = array_merge($filtered_sup);
		$approvers_deadline = [];
		$approvers = [];
		if(count($filtered_supervisors) > 0){
			$approval_stat_init = 4;
			$approval_level = 1;
			for($loop = 0; $loop < count($filtered_supervisors); $loop++){
				$deadline_date = Date("Y-m-d", strtotime($date_filed . ' 2 days'));
				$deadline_date_time = $deadline_date . " 23:59:59";
				$follow = $deadline_date . " 00:00:00";
				$emp_details =  $this->get_emp_details_via_emp_id($filtered_supervisors[$loop]['emp_id']);
				// var_dump($emp_details);
				$approval_stat = $approval_stat_init;
				$approvers_deadline[$loop] = [
					'userId' => $emp_details->uid,
					$column => $request_id,
					'approvalStatus_ID' => $approval_stat,
					'approvalLevel' => $approval_level,
					'deadline' => $deadline_date_time,
					'follow' => $follow,
				];
				$approvers[$loop] = [
					'userId' => $emp_details->uid,
					$column => $request_id,
					'approvalStatus_ID' => $approval_stat,
					'approvalLevel' => $approval_level,
					// 'employeeName' => $sup_user_details->fname." ".$sup_user_details->fname,
				];
				$date_filed = $deadline_date;
				$approval_stat_init = 2;
				$approval_level++;
			}
			$this->set_deadline($approvers_deadline, $module_id, $request_id, $column, $requestor_user_id);
			// var_dump($approvers_deadline);
			return $approvers;
		}

	}

	public function set_approvers_deadline_and_details_final_test($filtered_sup, $date_filed, $column, $request_id, $module_id, $requestor_user_id){
		$filtered_supervisors = array_merge($filtered_sup);
		$approvers_deadline = [];
		$approvers = [];
		if(count($filtered_supervisors) > 0){
			$approval_stat_init = 4;
			$approval_level = 1;
			for($loop = 0; $loop < count($filtered_supervisors); $loop++){
				$deadline_date = Date("Y-m-d", strtotime($date_filed . ' 2 days'));
				$deadline_date_time = $deadline_date . " 23:59:59";
				$follow = $deadline_date . " 00:00:00";
				$emp_details =  $this->get_emp_details_via_emp_id($filtered_supervisors[$loop]['emp_id']);
				// var_dump($emp_details);
				$approval_stat = $approval_stat_init;
				$approvers_deadline[$loop] = [
					'userId' => $emp_details->uid,
					$column => $request_id,
					'approvalStatus_ID' => $approval_stat,
					'approvalLevel' => $approval_level,
					'deadline' => $deadline_date_time,
					'follow' => $follow,
				];
				$approvers[$loop] = [
					'userId' => $emp_details->uid,
					$column => $request_id,
					'approvalStatus_ID' => $approval_stat,
					'approvalLevel' => $approval_level,
					// 'employeeName' => $sup_user_details->fname." ".$sup_user_details->fname,
				];
				$date_filed = $deadline_date;
				$approval_stat_init = 2;
				$approval_level++;
			}
			// $this->set_deadline($approvers_deadline, $module_id, $request_id, $column, $requestor_user_id);
			// var_dump($approvers_deadline);
			return $approvers;
		}

	}

	public function set_approvers($date_filed, $user_id, $module_id, $column, $request_id){
		$approvers = [];
		$approvers_num = 2;
		$emp_details =  $this->get_emp_details_via_user_id($user_id);
		$supervisors = $this->get_supervisors($emp_details->emp_id, $approvers_num);
		if(count($supervisors) > 0){
			$filtered_supervisors = $this->filter_approval_exemption_reassignment($supervisors['supervisors'], $emp_details->acc_id, $emp_details->emp_id);
			$approvers = $this->set_approvers_deadline_and_details($filtered_supervisors, $date_filed, $column, $request_id, $module_id, $user_id);
		}
		return $approvers;
		// var_dump($approvers);
	}
	public function set_approvers_final_test($date_filed = "2021-03-03", $user_id = 1923, $module_id = 3, $column = "additionalHourRequest_ID", $request_id = 1){
		$approvers = [];
		$approvers_num = 2;
		$emp_details =  $this->get_emp_details_via_user_id($user_id);
		// var_dump($emp_details);
		$supervisors = $this->get_supervisors($emp_details->emp_id, $approvers_num);
		if(count($supervisors) > 0){
			$filtered_supervisors = $this->filter_approval_exemption_reassignment($supervisors['supervisors'], $emp_details->acc_id, $emp_details->emp_id);
			// var_dump($filtered_supervisors);
			$approvers = $this->set_approvers_deadline_and_details_final_test($filtered_supervisors, $date_filed, $column, $request_id, $module_id, $user_id);
		}
		return $approvers;
		// var_dump($approvers);
	}
	// SET APPROVERS

	public function set_approvers_test()
	{
		$date_filed = "2018-10-04";
		$module_id = 1;
		$request_id = 1;
		$column = "additionalHourRequest_ID";
		$approvers = [];
		$approvers_deadline_follow = [];
		$orig_user_id = 213;
		$user_id = 213;
		$approval_num = $this->get_approval_number(1);
		$approval_type = 1; // 1- Sequential, 2 - Parallel
		$approval_level = 1;
		$approval_stat_init = 4;
		// echo "approval_num ->".$approval_num."<br>";
		for ($loop = 0; $loop < $approval_num; $loop++) {
			// echo "loop ->" . $loop . "<br>";
			$sup_user_details = $this->get_direct_supervisor($user_id);
			if (count($sup_user_details) > 0) {
				// echo $sup_user_details->uid . " - level = " . $approval_level . "<br>";
				$deadline_date = Date("Y-m-d", strtotime($date_filed . ' 2 days'));
				$deadline_date_time = $deadline_date . " 23:59:59";
				var_dump($sup_user_details->uid);
				if (($sup_user_details->uid == '766' && $approval_level > 1) || ($sup_user_details->uid == '20' && $approval_level > 1)) {
					   echo "cut on sir mae and sir Sy";
					    // $approver_id = 316;
				} else {
					if ($sup_user_details->uid == '766' && $approval_level == 1) {
						$approver_id = 316;
					} else {
						$approver_id = (int) $sup_user_details->uid;
						"save to approver ----------- <br>";
					}
					if ($loop == 0) {
						$follow = 0;
					} else {
						$follow = $deadline_date . " 00:00:00";
					}
					$approval_stat = $approval_stat_init;
					$approvers_deadline_follow[$loop] = [
						'userId' => $approver_id,
						// $column => $request_id,
						'approvalStatus_ID' => $approval_stat,
						'approvalLevel' => $approval_level,
						'deadline' => $deadline_date_time,
						'follow' => $follow,
					];
					$approvers[$loop] = [
						'userId' => $approver_id,
						// $column => $request_id,
						'approvalStatus_ID' => $approval_stat,
						'approvalLevel' => $approval_level,
						// 'employeeName' => $sup_user_details->fname." ".$sup_user_details->fname,
					];
					$user_id = $sup_user_details->uid;
					if ($approval_type === 1) {
						$approval_stat_init = 2;
					} else {
						$approval_stat_init = 4;
					}
					$date_filed = $deadline_date;
					$approval_level++;
				}
			}
		}
		// $this->set_deadline($approvers_deadline_follow, $module_id, $request_id, $column, $orig_user_id);
		var_dump($approvers);
	}

	protected function set_approvers_old($date_filed, $user_id, $module_id, $column, $request_id)
	{
		$approvers = [];
		$approvers_deadline_follow = [];
		$orig_user_id = $user_id;
		$approval_num = $this->get_approval_number($module_id);
	
		$approval_type = 1; // 1- Sequential, 2 - Parallel
		$approval_level = 1;
		$approval_stat_init = 4;
		for ($loop = 0; $loop < $approval_num; $loop++) {
			$sup_user_details = $this->get_direct_supervisor($user_id);
			if (count($sup_user_details) > 0) {
				$deadline_date = Date("Y-m-d", strtotime($date_filed . ' 2 days'));
				$deadline_date_time = $deadline_date . " 23:59:59";
				if (($sup_user_details->uid == '766' && $approval_level > 1) || ($sup_user_details->uid == '1248' && $approval_level > 1)) {
					//    echo "cut on sir mae";
				}else{
					if ($sup_user_details->uid == '766' && $approval_level == 1) {
						$approver_id = 316;
					} else {
						$approver_id = (int) $sup_user_details->uid;
					}
					if ($loop == 0) {
						$follow = 0;
					} else {
						$follow = $deadline_date . " 00:00:00";
					}
					$approval_stat = $approval_stat_init;
					$approvers_deadline_follow[$loop] = [
						'userId' => $approver_id,
						$column => $request_id,
						'approvalStatus_ID' => $approval_stat,
						'approvalLevel' => $approval_level,
						'deadline' => $deadline_date_time,
						'follow' => $follow,
					];
					$approvers[$loop] = [
						'userId' => $approver_id,
						$column => $request_id,
						'approvalStatus_ID' => $approval_stat,
						'approvalLevel' => $approval_level,
						// 'employeeName' => $sup_user_details->fname." ".$sup_user_details->fname,
					];
					$user_id = $sup_user_details->uid;
					if ($approval_type === 1) {
						$approval_stat_init = 2;
					} else {
						$approval_stat_init = 4;
					}
					$date_filed = $deadline_date;
					$approval_level++;
				}
			}
		}
		$this->set_deadline($approvers_deadline_follow, $module_id, $request_id, $column, $orig_user_id);
		return $approvers;
	}

	protected function get_current_date_time()
	{
		$date = new DateTime("now", new DateTimeZone('Asia/Manila'));
		$dates = [
			'dateTime' => $date->format('Y-m-d H:i:s'),
			'date' => $date->format('Y-m-d'),
			'time' => $date->format('H:i:s'),
		];
		return $dates;
	}

	// GET DEADLINE DATA FOR APPROVERS

	public function get_pending_deadline()
	{
		$user_id = $this->session->userdata('uid');
		$dateTime = $this->get_current_date_time();
		$fields = "module_ID, request_ID, requestor, approvalLevel, deadline";
		$where = "userId = $user_id AND DATE(deadline) ='" . $dateTime['date'] . "' AND status_ID = 2";
		$record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_deadline");
		echo json_encode($record);
	}

	protected function get_preceding_approvers_due($approvalLevel, $request_id, $module_id)
	{
		$fields = "userId, approvalLevel";
		$where = "request_ID = $request_id AND module_ID = $module_id AND approvalLevel < $approvalLevel";
		return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_deadline");
	}

	protected function get_user_details_due($user_id)
	{
		$fields = "a.uid, c.fname, c.lname";
		$where = "a.uid = $user_id AND a.emp_id = b.emp_id AND b.apid=c.apid";
		return $requestor = $this->general_model->fetch_specific_val($fields, $where, "tbl_user a,tbl_employee b,tbl_applicant c");
	}

	private function notify_preceding_approvers($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link)
	{
		$links = $this->get_module_links($module_id);
		$preceeding_approvers = $this->get_preceding_approvers_due($approval_level, $request_id, $module_id);
		for ($loop = 0; $loop < count($preceeding_approvers); $loop++) {
			$recipients[$loop] = [
				"userId" => $preceeding_approvers[$loop]->userId,
			];
		}
		$notif_mssg = "<i class='fa fa-info-circle'></i> " . $approver->fname . " $approver->lname was not able to give approval decision to the $module of " . $requestor->fname . " " . $requestor->lname . " .<br><small><b>" . $links->abbreviation . "-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
		$link = $links->approvalPageLink;
		return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $recipients);
	}

	private function notify_requestor($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link)
	{
		$links = $this->get_module_links($module_id);
		$requestors[0] = ['userId' => $requestor->uid];
		$approval_level_ordinal = $this->ordinal($approval_level);
		$notif_mssg = "<i class='fa fa-info-circle'></i> " . $approver->fname . " $approver->lname, your $approval_level_ordinal level approver, was not able to give approval decision to your $module.<br><small><b>" . $links->abbreviation . "-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
		$link = $links->pendingRequestPage;
		return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $requestors);
	}

	private function notify_dued_approver($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link, $deadline)
	{
		$links = $this->get_module_links($module_id);
		$approvers[0] = ['userId' => $approver->uid];
		$approval_level_ordinal = $this->ordinal($approval_level);
		$notif_mssg = "<i class='fa fa-warning'></i> You have missed to give an approval decision as the $approval_level_ordinal level approver to the $module of $requestor->fname " . $requestor->lname . " prior " . date("g:i A", strtotime($deadline)) . " last " . date("M j, Y", strtotime($deadline)) . ".<br><small><b>" . $links->abbreviation . "-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
		$link = $links->approvalPageLink;
		return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $approvers);
	}

	private function get_max_approval_level_deadline($request_id, $module_id)
	{
		$fields = "MAX(approvalLevel) as approvalLevel";
		$where = "request_ID = $request_id AND module_ID = $module_id";
		return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_deadline');
	}

	// public function set_columns(){
	//     $column = [
	//         'request_id_col' => 'additionalHourRequest_ID',
	//         'approvalLevel' => 'approvalLevel',
	//         'status_col' => 'approvalStatus_ID',
	//         'table' => 'tbl_additional_hour_request_approval',
	//     ];
	//     $col = json_encode($column);
	//     $data['approvalAttrib'] = $col;
	//     $where = "module_ID = 1";
	//     $update_stat = $this->general_model->update_vals($data, $where, 'tbl_module');
	//     echo json_encode($update_stat);
	// }

	private function set_specific_approver_stat($request_id, $request_col, $approval_level, $approvalLevel_col, $status_id, $status_col, $table)
	{
		$data["$status_col"] = $status_id;
		$where = "$request_col = $request_id AND $approvalLevel_col = $approval_level";
		$update_stat = $this->general_model->update_vals($data, $where, $table);
		return ($update_stat);
	}

	private function get_approval_attrib($module_id)
	{
		$fields = "approvalAttrib";
		$where = "module_ID = $module_id";
		return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_module');
	}

	private function get_follow_user_id($module_id, $approval_level, $request_id)
	{
		$fields = "userId";
		$where = "request_ID = $request_id AND module_ID = $module_id AND approvalLevel = $approval_level";
		return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_deadline');
	}

	private function notify_approver_follow($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link, $deadline)
	{
		$follow_approver = $this->get_follow_user_id($module_id, $approval_level + 1, $request_id);
		$approvers[0] = ['userId' => $follow_approver->userId];
		$next_approval_level_ordinal = $this->ordinal($approval_level + 1);
		$missed_approval_level_ordinal = $this->ordinal($approval_level);
		$notif_mssg = "<i class='fa fa-info-circle'></i> The $module of $requestor->fname " . $requestor->lname . " was automatically forwarded to you for $next_approval_level_ordinal level approval because $approver->fname " . $approver->lname . " ($missed_approval_level_ordinal level) was not able to give a decision before the deadline: " . date("g:i A", strtotime($deadline)) . ", " . date("M j, Y", strtotime($deadline)) . ".<br><small>" . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
		return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $approvers);
	}

	private function get_default_approver($module_id)
	{
		$fields = "supervisingUserId";
		$where = "module_ID = $module_id";
		return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_module');
	}

	private function notify_default_approver($requestor, $approver, $approval_level, $request_id, $module, $module_id, $link, $deadline)
	{
		$links = $this->get_module_links($module_id);
		$default_approver = $this->get_default_approver($module_id);
		$approvers[0] = ['userId' => $default_approver->supervisingUserId];
		$missed_approval_level_ordinal = $this->ordinal($approval_level);
		$notif_mssg = "<i class='fa fa-info-circle'></i> The $module of $requestor->fname " . $requestor->lname . " was automatically forwarded to you for default approval because $approver->fname " . $approver->lname . " ($missed_approval_level_ordinal level) was not able to give a decision before the deadline: " . date("g:i A", strtotime($deadline)) . ", " . date("M j, Y", strtotime($deadline)) . ".<br><small><b>" . $links->abbreviation . "-ID</b>: " . str_pad($request_id, 8, '0', STR_PAD_LEFT) . " </small>";
		$link = $links->monitoringPageLink;
		return $notif_stat = $this->set_system_notif_preceeding($notif_mssg, $link, $approvers);
	}

	public function set_current_approver_as_missed($module_id, $request_id, $approval_level, $approver_id)
	{
		$approval_attrib = json_decode($this->get_approval_attrib($module_id)->approvalAttrib);
		$request_col = $approval_attrib->request_id_col;
		$request_details_col = $approval_attrib->requestCol;
		$approvalLevel_col = $approval_attrib->approvalLevel;
		$status_col = $approval_attrib->status_col;
		$approval_table = $approval_attrib->approvalTable;
		$request_tbl = $approval_attrib->requestTable;
		//set missed
		$status_id = 12;
		//approval table
		$curr_approver_stat['set_stat'] = $this->set_specific_approver_stat($request_id, $request_col, $approval_level, $approvalLevel_col, $status_id, $status_col, $approval_table);
		//details
		if (((int) $module_id !== 1) and ((int) $module_id !== 3)) {
			$leave_approval_id = $this->get_request_leave_approval($request_details_col, $request_col, $request_id, $approver_id, $approval_table);
			$data['status'] = 12;
			$where = "$request_details_col = " . $leave_approval_id->$request_details_col;
			$curr_approver_stat['set_details_stat'] = $this->general_model->update_vals($data, $where, $request_tbl);
		}
		return $curr_approver_stat;
	}

	private function set_next_approver_as_current($module_id, $request_id, $approval_level)
	{
		$approval_attrib = json_decode($this->get_approval_attrib($module_id)->approvalAttrib);
		$request_col = $approval_attrib->request_id_col;
		$approvalLevel_col = $approval_attrib->approvalLevel;
		$status_col = $approval_attrib->status_col;
		$approval_table = $approval_attrib->approvalTable;
		$approval_level++;
		$status_id = 4;
		return $set_stat = $this->set_specific_approver_stat($request_id, $request_col, $approval_level, $approvalLevel_col, $status_id, $status_col, $approval_table);
	}

	private function set_deadline_stat($deadline_id)
	{
		$data['status_ID'] = 12;
		$where = "deadline_ID = $deadline_id";
		return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_deadline');
	}

	private function set_main_request($request_id, $request_id_col, $request_tbl, $status_col)
	{
		$data[$status_col] = 12;
		$where = "$request_id_col = $request_id";
		return $update_stat = $this->general_model->update_vals($data, $where, $request_tbl);
	}

	private function get_request_leave_approval($request_details_col, $request_id_col, $request_id, $approver_id, $approval_tbl)
	{
		$fields = "$request_details_col";
		$where = "$request_id_col = $request_id AND uid = $approver_id";
		return $record = $this->general_model->fetch_specific_val($fields, $where, $approval_tbl);
	}

	private function set_leave_status_missed($request_id_col, $request_id, $final_status_col, $main_table)
	{
		$data[$final_status_col] = 12;
		$where = "$request_id_col = $request_id";
		return $update_stat = $this->general_model->update_vals($data, $where, $main_table);
	}

	public function set_request_as_missed($module_id, $request_id)
	{
		$approval_attrib = json_decode($this->get_approval_attrib($module_id)->approvalAttrib);
		if (((int) $module_id !== 1) and ((int) $module_id !== 3)) {
			$update_stat = $this->set_leave_status_missed($approval_attrib->request_id_col, $request_id, $approval_attrib->final_status_col, $approval_attrib->final_table);
		} else {
			if ((int) $module_id == 3) {
				$status_col = $approval_attrib->request_stat_col;
			} else {
				$status_col = $approval_attrib->status_col;
			}
			$update_stat = $this->set_main_request($request_id, $approval_attrib->requestCol, $approval_attrib->requestTable, $status_col);
		}
		return $update_stat;
	}

	private function get_due_deadlines()
	{
		$user_id = $this->session->userdata('uid');
		$dateTime = $this->get_current_date_time();
		$fields = "dead.deadline_ID, dead.module_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage, dead.request_ID, dead.requestor, dead.userId, dead.approvalLevel, dead.deadline";
		$where = "mod.module_ID = dead.module_ID AND dead.userId = $user_id AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND dead.status_ID = 2";
		$table = "tbl_deadline dead, tbl_module mod";
		return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	public function get_follow_requests()
	{
		$user_id = $this->session->userdata('uid');
		$dateTime = $this->get_current_date_time();
		$fields = "module_ID, request_ID, requestor, approvalLevel, follow";
		$where = "userId = $user_id AND DATE(follow) <= '" . $dateTime['date'] . "' AND status_ID = 2";
		$record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_deadline");
		// //var_dump($record);
		return $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_deadline");
	}

	private function get_due_deadlines_approvers()
	{
		$user_id = $this->session->userdata('uid');
		$dateTime = $this->get_current_date_time();
		$fields = "dead.deadline_ID, dead.module_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage, dead.request_ID, dead.requestor, dead.userId, dead.approvalLevel, dead.deadline";
		$where = "mod.module_ID = dead.module_ID AND dead.userId = $user_id AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND dead.status_ID = 2";
		$table = "tbl_deadline dead, tbl_module mod";
		return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	private function get_request_deadlines($module_id, $request_id, $approvalLevel)
	{
		$dateTime = $this->get_current_date_time();
		$fields = "dead.deadline_ID, dead.module_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage, dead.request_ID, dead.requestor, dead.userId, dead.approvalLevel, dead.deadline";
		$where = "mod.module_ID = dead.module_ID AND dead.module_ID = $module_id AND dead.request_ID = $request_id AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND dead.approvalLevel = $approvalLevel AND dead.status_ID = 2";
		$table = "tbl_deadline dead, tbl_module mod";
		return $record = $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	private function check_if_request_exist($request_deadline)
	{
		$approval_attrib = json_decode($this->get_approval_attrib($request_deadline->module_ID)->approvalAttrib);
		if(count($approval_attrib) > 0){
			if (((int) $request_deadline->module_ID !== 1) and ((int) $request_deadline->module_ID !== 3)) {
				$table = $approval_attrib->final_table;
				$attribute = $approval_attrib->request_id_col;
			} else {
				$table = $approval_attrib->requestTable;
				$attribute = $approval_attrib->requestCol;
			}
			$fields = "$attribute";
			$where = "$attribute = $request_deadline->request_ID";
			$record = $this->general_model->fetch_specific_val($fields, $where, $table);
			if (count($record) > 0) {
				return true;
			} else {
				return false;
			}
		}else{
			return false;
		}
	}

	private function proccess_missed_deadlines($request_deadline)
	{
		$requestor = $this->get_user_details_due($request_deadline->requestor);
		$approver = $this->get_user_details_due($request_deadline->userId);
		$approval_level = $request_deadline->approvalLevel;
		$module = $request_deadline->module;
		$module_id = $request_deadline->module_ID;
		$request_id = $request_deadline->request_ID;
		$approvalLink = $request_deadline->approvalPageLink;
		$requestLink = $request_deadline->pendingRequestPage;
		$deadline = $request_deadline->deadline;
		if ($this->check_if_request_exist($request_deadline)) {
			$max_approval_level = $this->get_max_approval_level_deadline($request_id, $module_id);
			if ($approval_level > 1) {
				$due['notify_preceding_approvers'] = $this->notify_preceding_approvers($requestor, $approver, $approval_level, $request_id, $module, $module_id, $approvalLink);
			}
			$due['missed_approver_stat'] = $this->set_current_approver_as_missed($module_id, $request_id, $approval_level, $approver->uid);
			if ($approval_level < $max_approval_level->approvalLevel) {
				//set status of the missed approver and next approver
				$due['next_approver_stat'] = $this->set_next_approver_as_current($module_id, $request_id, $approval_level);
				//notify next approver
				$due['notify_follow_approver'] = $this->notify_approver_follow($requestor, $approver, $approval_level, $request_id, $module, $module_id, $approvalLink, $deadline);
			}
			if ($approval_level == $max_approval_level->approvalLevel) {
				$due['notify_default_approver'] = $this->notify_default_approver($requestor, $approver, $approval_level, $request_id, $module, $module_id, $approvalLink, $deadline);
				$due['set_request_stat'] = $this->set_request_as_missed($module_id, $request_id);
			}
			//system notify requestor
			$due['notify_requestor'] = $this->notify_requestor($requestor, $approver, $approval_level, $request_id, $module, $module_id, $requestLink);
			//notify the deadlined approver
			$due['notify_missed_approver'] = $this->notify_dued_approver($requestor, $approver, $approval_level, $request_id, $module, $module_id, $approvalLink, $deadline);
			$due['set_deadline_status'] = $this->set_deadline_stat($request_deadline->deadline_ID);
			$due['exist'] = true;
		} else {
			$due['exist'] = false;
		}
		return $due;
	}

	// public function get_missed_preceding_approvers(){
	// }
	// approvers;

	public function get_missed_leave_appr()
	{
		$user_id = $this->session->userdata('uid');
		$dateTime = $this->get_current_date_time();
		$fields = "dead.request_ID, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
		$where = "mod.module_ID = dead.module_ID AND dead.request_ID = leaveAppr.requestLeave_ID AND dead.module_ID = 2 AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND leaveMain.requestLeave_ID = leaveAppr.requestLeave_ID and leaveAppr.uid = $user_id AND leaveMain.finalStatus =2 AND dead.status_ID = 2";
		$table = "tbl_module mod, tbl_request_leave_approval leaveAppr, tbl_request_leave leaveMain, tbl_deadline dead";
		return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
		// $record = $this->general_model->fetch_specific_val($fields, $where, $table);
		// //var_dump($record);
	}

	public function get_missed_retract_leave_appr()
	{
		$user_id = $this->session->userdata('uid');
		$dateTime = $this->get_current_date_time();
		$fields = "dead.request_ID, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
		$where = "mod.module_ID = dead.module_ID AND dead.request_ID = retractLeaveAppr.retractLeave_ID AND dead.module_ID = 4 AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND retractLeave.retractLeave_ID = retractLeaveAppr.retractLeave_ID AND retractLeaveAppr.uid = $user_id AND retractLeave.finalStatus = 2 AND dead.status_ID = 2";
		$table = "tbl_module mod, tbl_retract_leave_approval retractLeaveAppr, tbl_retract_leave retractLeave, tbl_deadline dead";
		return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
		// $record = $this->general_model->fetch_specific_val($fields, $where, $table);
		// //var_dump($record);
	}

	public function get_missed_dtr_appr()
	{
		$user_id = $this->session->userdata('uid');
		$dateTime = $this->get_current_date_time();
		$fields = "dead.request_ID, dead.requestor, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
		$where = "mod.module_ID = dead.module_ID AND dead.request_ID = dtrReqAppr.dtrRequest_ID AND dead.module_ID = 3 AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND dtrReq.dtrRequest_ID = dtrReqAppr.dtrRequest_ID AND dtrReqAppr.userId = $user_id AND dtrReq.status_ID = 2 AND dead.status_ID = 2";
		$table = "tbl_module mod, tbl_dtr_request dtrReq, tbl_dtr_request_approval dtrReqAppr, tbl_deadline dead";
		return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
		// $record = $this->general_model->fetch_specific_val($fields, $where, $table);
		// //var_dump($record);
	}

	public function get_missed_ahr_appr()
	{
		$user_id = $this->session->userdata('uid');
		$dateTime = $this->get_current_date_time();
		$fields = "dead.request_ID, dead.requestor, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
		$where = "mod.module_ID = dead.module_ID AND dead.request_ID =  ahrAppr.additionalHourRequest_ID AND dead.module_ID = 1 AND DATE(dead.deadline) <'" . $dateTime['date'] . "' AND ahr.additionalHourRequestId = ahrAppr.additionalHourRequest_ID AND ahrAppr.userId = $user_id AND ahr.approvalStatus_ID = 2 AND dead.status_ID = 2";
		$table = "tbl_module mod, tbl_additional_hour_request ahr, tbl_additional_hour_request_approval ahrAppr, tbl_deadline dead";
		return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
		// $record = $this->general_model->fetch_specific_val($fields, $where, $table);
		// //var_dump($record);
	}

	// public function get_missed_deadline_test()
	// {
	//     //identify if first approver
	//     $approvers = [];
	//     $due_deadlines = $this->get_missed_leave_appr();
	//     // //var_dump($due_deadlines);
	//     // $leave = $this->get_missed_leave_appr();
	//     $retract = $this->get_missed_retract_leave_appr();
	//     $dtr = $this->get_missed_dtr_appr();
	//     $ahr = $this->get_missed_ahr_appr();
	//     if (count($retract) > 0)
	//     {
	//         $due_deadlines = (object) array_merge((array) $due_deadlines, (array) $retract);
	//     }
	//     if (count($dtr) > 0)
	//     {
	//         $due_deadlines = (object) array_merge((array) $due_deadlines, (array) $dtr);
	//     }
	//     if (count($ahr) > 0)
	//     {
	//         $due_deadlines = (object) array_merge((array) $due_deadlines, (array) $ahr);
	//     }
	//     // $due_deadlines = $this->get_due_deadlines_approvers();
	//     $count = 0;
	//     // $due['deadline'] = $due_deadlines;
	//     // -----------------------  
	//     if (count($due_deadlines))
	//     {
	//         $due['has_deadline'] = 1;
	//         foreach ($due_deadlines as $missed_requests)
	//         {
	//             $request_count = 0;
	//             $max_approval_level = $this->get_max_approval_level_deadline($missed_requests->request_ID, $missed_requests->module_ID);
	//             for ($approvalLevel = 0; $approvalLevel < $max_approval_level->approvalLevel; $approvalLevel++)
	//             {
	//                 $request_deadline = $this->get_request_deadlines($missed_requests->module_ID, $missed_requests->request_ID, $approvalLevel + 1);
	//                 if (count($request_deadline))
	//                 {
	//                     $proc_missed_deadline = $this->proccess_missed_deadlines($request_deadline);
	//                     if ($proc_missed_deadline['exist'])
	//                     {
	//                         $approvers['missed_approvers'][$request_count] = $proc_missed_deadline;
	//                         $request_count++;
	//                     }
	//                 }
	//             }
	//             if (count($approvers) > 0)
	//             {
	//                 $due['missed_requests'][$count] = $approvers;
	//             }
	//             $count++;
	//         }
	//     }
	//     else
	//     {
	//         $due['has_deadline'] = 0;
	//     }
	//     if (!array_key_exists('missed_requests', $due))
	//     {
	//         $due['has_deadline'] = 0;
	//     };
	//     echo json_encode($due);
	// }

	public function get_approval_deadlines_count_leave_retract($today_bool)
    {
        $deadline_today = "";
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        if ($today_bool == 1)
        {
            $deadline_today = "AND DATE(dead.deadline) = '" . $dateTime['date'] . "'";
        }
        $fields = "distinct(request_ID), dead.request_ID, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mods.module, mods.approvalPageLink, mods.pendingRequestPage";
		// $where = "mods.module_ID = dead.module_ID AND dead.request_ID = retractLeaveAppr.retractLeave_ID $deadline_today AND dead.module_ID = 4 AND retractLeave.retractLeave_ID = retractLeaveAppr.retractLeave_ID AND dead.userId = retractLeaveAppr.uid AND retractLeaveAppr.uid = $user_id AND retractLeaveAppr.semiStatus = 4 AND dead.status_ID = 2";
        $where = "mods.module_ID = dead.module_ID AND dead.request_ID = request.retractLeave_ID $deadline_today AND dead.module_ID = 4 AND dead.userId = appr.uid AND dead.status_ID = 2 AND request.retractLeave_ID = appr.retractLeave_ID AND appr.uid = $user_id AND appr.semiStatus = 4";
        $table = "tbl_retract_leave request, tbl_retract_leave_approval appr, tbl_deadline dead, tbl_module mods";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return count($record);
    }

    public function get_approval_deadlines_count_leave($today_bool)
    {
        $deadline_today = "";
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        if ($today_bool == 1)
        {
            $deadline_today = "AND DATE(dead.deadline) = '" . $dateTime['date'] . "'";
        }
        $fields = "distinct(request_ID), dead.request_ID, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mods.module, mods.approvalPageLink, mods.pendingRequestPage";
		// $where = " mods.module_ID = dead.module_ID AND leaveAppr.requestLeave_ID = leaveAppr.requestLeave_ID $deadline_today  AND dead.module_ID = 2 AND leaveMain.requestLeave_ID = leaveAppr.requestLeave_ID AND dead.userId = leaveAppr.uid AND leaveAppr.uid = $user_id AND leaveAppr.semiStatus = 4 AND dead.status_ID = 2";
        $where = "mods.module_ID = dead.module_ID AND dead.request_ID = request.requestLeave_ID  $deadline_today AND dead.module_ID = 2 AND dead.userId = appr.uid AND dead.status_ID = 2 AND request.requestLeave_ID = appr.requestLeave_ID AND appr.uid = $user_id AND appr.semiStatus = 4";
        $table = "tbl_request_leave request, tbl_request_leave_approval appr, tbl_deadline dead, tbl_module mods";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return count($record);
	}

	public function get_approval_deadlines_count_dtr($today_bool)
    {
        // $user_id = 2;
        $deadline_today = "";
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        if ($today_bool == 1)
        {
            $deadline_today = "AND DATE(dead.deadline) = '" . $dateTime['date'] . "'";
        }
        $fields = "distinct(request_ID),dead.request_ID, dead.requestor, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mods.module, mods.approvalPageLink, mods.pendingRequestPage";
        $where = "mods.module_ID = dead.module_ID AND dead.request_ID = dtrReqAppr.dtrRequest_ID $deadline_today AND dead.module_ID = 3 AND dtrReq.dtrRequest_ID = dtrReqAppr.dtrRequest_ID AND dead.userId = dtrReqAppr.userId AND dtrReqAppr.userId = $user_id AND dtrReqAppr.approvalStatus_ID = 4 AND dead.status_ID = 2";
        $table = "tbl_module mods, tbl_dtr_request dtrReq, tbl_dtr_request_approval dtrReqAppr, tbl_deadline dead";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return count($record);
    }

	// AHR

	public function get_approval_deadlines_count_ahr($today_bool)
    {
        // $user_id = 81;
        $deadline_today = "";
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        if ($today_bool)
        {
            $deadline_today = "AND DATE(dead.deadline) = '" . $dateTime['date'] . "'";
        }
        /* $fields = "dead.request_ID, dead.requestor, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage";
        $where = "mod.module_ID = dead.module_ID AND dead.request_ID =  ahrAppr.additionalHourRequest_ID $deadline_today AND dead.module_ID = 1 AND ahr.additionalHourRequestId = ahrAppr.additionalHourRequest_ID AND dead.userId = ahrAppr.userId AND ahrAppr.userId = $user_id AND ahrAppr.approvalStatus_ID = 4";
        $table = "tbl_module mod, tbl_additional_hour_request ahr, tbl_additional_hour_request_approval ahrAppr, tbl_deadline dead"; */
		$fields = "distinct(request_ID),dead.request_ID, dead.requestor, dead.module_ID, dead.deadline_ID, dead.userId, dead.approvalLevel, dead.deadline, dead.status_ID, mods.module, mods.approvalPageLink, mods.pendingRequestPage";
        $where = "mods.module_ID = dead.module_ID AND dead.request_ID = ahrAppr.additionalHourRequest_ID $deadline_today AND dead.module_ID = 1 AND ahr.additionalHourRequestId = ahrAppr.additionalHourRequest_ID AND dead.userId = ahrAppr.userId AND ahrAppr.userId = $user_id AND sched.sched_id = ahr.schedId AND ahrAppr.approvalStatus_ID = 4";
        $table = "tbl_module mods, tbl_additional_hour_request ahr, tbl_additional_hour_request_approval ahrAppr, tbl_deadline dead, tbl_schedule sched";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return count($record);
	}
	
	public function get_my_pending_count_ahr(){
		$user_id = $this->session->userdata('uid');
		$fields = "DISTINCT(ahr.additionalHourRequestId)";
        $where = "sched.sched_id = ahr.schedId and ahr.approvalStatus_ID IN (2,12) AND ahr.userId = $user_id";
        $table = "tbl_additional_hour_request ahr, tbl_schedule sched";
        $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
        return count($record);
	}

	public function get_my_pending_count_tito(){
		$user_id = $this->session->userdata('uid');
		$fields = "DISTINCT(dtrReq.dtrRequest_ID)";
        $where = "status_ID  IN (2,12) AND dtrReq.userID = $user_id";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_dtr_request dtrReq");
        return count($record);
	}

	public function get_my_pending_count_leave(){
		$user_id = $this->session->userdata('uid');
		$fields = "DISTINCT(requestLeave_ID)";
        $where = "reqLeave.finalStatus  IN (2,12) AND reqLeave.uid = $user_id";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_request_leave reqLeave");
        return count($record);
	}

	public function get_my_pending_count_retract_leave(){
		$user_id = $this->session->userdata('uid');
		$fields = "DISTINCT(retractLeave_ID)";
        $where = "retractLeave.finalStatus  IN (2,12) AND retractLeave.uid = $user_id";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_retract_leave retractLeave");
        return count($record);
	}

	public function get_approval_links()
	{
		$appr_link['ahr_appr_link'] = $this->get_module_links(1)->approvalPageLink;
		$appr_link['dtr_appr_link'] = $this->get_module_links(3)->approvalPageLink;
		$appr_link['leave_appr_link'] = $this->get_module_links(2)->approvalPageLink;
		$appr_link['leave_retract_appr_link'] = $this->get_module_links(4)->approvalPageLink;
		echo json_encode($appr_link);
	}

	public function get_pending_appr_counts()
	{
		$appr_count['ahr_appr_pending'] = $this->get_approval_deadlines_count_ahr(0);
		$appr_count['dtr_appr_pending'] = $this->get_approval_deadlines_count_dtr(0);
		// $appr_count['dtr_appr_pending'] = 0;
		$appr_count['leave_appr_pending'] = $this->get_approval_deadlines_count_leave(0);
		$appr_count['leave_retract_appr_pending'] = $this->get_approval_deadlines_count_leave_retract(0);
		echo json_encode($appr_count);
		// //var_dump($appr_count);
	}

	public function get_deadline_today_appr_counts()
	{
		$appr_count['ahr_appr_today'] = $this->get_approval_deadlines_count_ahr(1);
		$appr_count['dtr_appr_today'] = $this->get_approval_deadlines_count_dtr(1);
		// $appr_count['dtr_appr_today'] = 0;
		$appr_count['leave_appr_today'] = $this->get_approval_deadlines_count_leave(1);
		$appr_count['leave_retract_appr_today'] = $this->get_approval_deadlines_count_leave_retract(1);
		echo json_encode($appr_count);
	}
	
	public function get_my_pending_request_count()
	{
		$appr_count['ahr'] = $this->get_my_pending_count_ahr();
		$appr_count['tito'] = $this->get_my_pending_count_tito();
		// $appr_count['tito'] = 0;
		$appr_count['leave'] = $this->get_my_pending_count_leave();
		$appr_count['leave_retract'] = $this->get_my_pending_count_retract_leave();
		// $appr_count['dtr_appr_today'] = $this->get_approval_deadlines_count_dtr(1);
		// $appr_count['leave_appr_today'] = $this->get_approval_deadlines_count_leave(1);
		// $appr_count['leave_retract_appr_today'] = $this->get_approval_deadlines_count_leave_retract(1);
		echo json_encode($appr_count);
	}

	public function get_missed_deadline()
	{
		//identify if first approver
		$approvers = [];
		$due_deadlines = $this->get_missed_leave_appr();

		$leave = $this->get_missed_leave_appr();
		$retract = $this->get_missed_retract_leave_appr();
		// $dtr = $this->get_missed_dtr_appr();
		$dtr = [];
		$ahr = $this->get_missed_ahr_appr();
		if (count($retract) > 0) {
			$due_deadlines = (object) array_merge((array) $due_deadlines, (array) $retract);
		}
		if (count($dtr) > 0) {
			$due_deadlines = (object) array_merge((array) $due_deadlines, (array) $dtr);
		}
		if (count($ahr) > 0) {
			$due_deadlines = (object) array_merge((array) $due_deadlines, (array) $ahr);
		}

		// $due_deadlines = $this->get_due_deadlines_approvers();
		$count = 0;
		// $due['deadline'] = $due_deadlines;
		if (count($due_deadlines)) {
			$due['has_deadline'] = 1;
			foreach ($due_deadlines as $missed_requests) {
				$request_count = 0;
				$max_approval_level = $this->get_max_approval_level_deadline($missed_requests->request_ID, $missed_requests->module_ID);
				for ($approvalLevel = 0; $approvalLevel < $max_approval_level->approvalLevel; $approvalLevel++) {
					$request_deadline = $this->get_request_deadlines($missed_requests->module_ID, $missed_requests->request_ID, $approvalLevel + 1);
					if (count($request_deadline)) {
						$proc_missed_deadline = $this->proccess_missed_deadlines($request_deadline);
						if ($proc_missed_deadline['exist']) {
							$approvers['missed_approvers'][$request_count] = $proc_missed_deadline;
							$request_count++;
						}
					}
				}
				if (count($approvers) > 0) {
					$due['missed_requests'][$count] = $approvers;
				}
				$count++;
			}
		} else {
			$due['has_deadline'] = 0;
		}
		if (!array_key_exists('missed_requests', $due)) {
			$due['has_deadline'] = 0;
		};
		echo json_encode($due);
	}

	private function get_modules($requestor)
	{
		$dateTime = $this->get_current_date_time();
		$fields = "DISTINCT(module_ID)";
		if ($requestor != 0) {
			$where = "requestor = $requestor AND DATE(deadline) <'" . $dateTime['date'] . "' AND status_ID = 2 AND module_ID NOT IN (5, 3)";
		} else {
			$where = "DATE(deadline) <'" . $dateTime['date'] . "' AND status_ID = 2 AND module_ID NOT IN (5, 3)";
		}
		return $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_deadline');
	}

	//for Requestor
	private function get_pending_request($requestor, $module_id)
	{
		$dateTime = $this->get_current_date_time();
		$fields = "DISTINCT(request_ID)";
		if ($requestor != 0) {
			$where = "requestor = $requestor AND DATE(deadline) <'" . $dateTime['date'] . "' AND status_ID = 2 AND module_ID = $module_id";
		} else {
			$where = "DATE(deadline) <'" . $dateTime['date'] . "' AND status_ID = 2 AND module_ID = $module_id";
		}
		return $record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_deadline');
	}

	public function check_if_monitoring_assigned()
	{
		$user_id = $this->session->userdata('uid');
		$fields = "useraccess_id";
		$where = "menu_item_id IN (97, 90) AND user_id = $user_id";
		$record = $this->general_model->fetch_specific_vals($fields, $where, 'tbl_user_access');
		if (count($record) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function get_missed_deadline_requestor()
	{
		$user_id = $this->session->userdata('uid');
		$modules = $this->get_modules($user_id);
		$count = 0;
		$approvers = [];
		if (count($modules)) {
			$due['has_deadline'] = 1;
			foreach ($modules as $modules_requests) {
				$due_deadlines = $this->get_pending_request($user_id, $modules_requests->module_ID);
				foreach ($due_deadlines as $missed_requests) {
					$max_approval_level = $this->get_max_approval_level_deadline($missed_requests->request_ID, $modules_requests->module_ID);
					$request_count = 0;
					for ($approvalLevel = 0; $approvalLevel < $max_approval_level->approvalLevel; $approvalLevel++) {
						$request_deadline = $this->get_request_deadlines($modules_requests->module_ID, $missed_requests->request_ID, $approvalLevel + 1);
						if (count($request_deadline)) {
							$proc_missed_deadline = $this->proccess_missed_deadlines($request_deadline);
							if ($proc_missed_deadline['exist']) {
								$approvers['missed_approvers'][$request_count] = $proc_missed_deadline;
								$request_count++;
							}
						}
					}
					if (count($approvers) > 0) {
						$due['missed_requests'][$count] = $approvers;
					}
					$count++;
				}
			}
		} else {
			$due['has_deadline'] = 0;
		}
		if (!array_key_exists('missed_requests', $due)) {
			$due['has_deadline'] = 0;
		};
		echo json_encode($due);
	}

	// public function check_if_assigned_with_monitoring(){
	// }
	//for Monitoring
	public function get_missed_deadline_monitoring()
	{
		$user_id = 0;
		if ($this->check_if_monitoring_assigned()) {
			$modules = $this->get_modules($user_id);
			$count = 0;
			$approvers = [];
			if (count($modules)) {
				$due['has_deadline'] = 1;
				foreach ($modules as $modules_requests) {
					$due_deadlines = $this->get_pending_request($user_id, $modules_requests->module_ID);
					// //var_dump($due_deadlines);
					foreach ($due_deadlines as $missed_requests) {
						$max_approval_level = $this->get_max_approval_level_deadline($missed_requests->request_ID, $modules_requests->module_ID);
						// //var_dump($max_approval_level);
						$request_count = 0;
						for ($approvalLevel = 0; $approvalLevel < $max_approval_level->approvalLevel; $approvalLevel++) {
							$request_deadline = $this->get_request_deadlines($modules_requests->module_ID, $missed_requests->request_ID, $approvalLevel + 1);
							if (count($request_deadline)) {
								$proc_missed_deadline = $this->proccess_missed_deadlines($request_deadline);
								if ($proc_missed_deadline['exist']) {
									$approvers['missed_approvers'][$request_count] = $proc_missed_deadline;
									$request_count++;
								}
							}
						}
						if (count($approvers) > 0) {
							$due['missed_requests'][$count] = $approvers;
						}
						$count++;
					}
				}
			} else {
				$due['has_deadline'] = 0;
			}
			if (!array_key_exists('missed_requests', $due)) {
				$due['has_deadline'] = 0;
			};
		} else {
			$due['has_deadline'] = 0;
		}
		echo json_encode($due);
	}

	// private function get_requestor_due_approvals()
	// {
	//     $dateTime = $this->get_current_date_time();
	//     $fields = "dead.deadline_ID, dead.module_ID, mod.module, mod.approvalPageLink, mod.pendingRequestPage, dead.request_ID, dead.requestor, dead.userId, dead.approvalLevel, dead.deadline";
	//     $where = "mod.module_ID = dead.module_ID AND dead.userId = $user_id AND DATE(dead.deadline) <'".$dateTime['date']."' AND dead.status_ID = 2";
	//     $table = "tbl_deadline dead, tbl_module mod";
	//     return $record = $this->general_model->fetch_specific_vals($fields,$where, $table);
	// }
	// public function get_requestor_due_approvers(){
	//     $fields = "userId, ";
	//     $where = "mod.module_ID = dead.module_ID AND dead.userId = $user_id AND DATE(dead.deadline) <'".$dateTime['date']."' AND dead.status_ID = 2";
	//     $table = "tbl_deadline dead, tbl_module mod";
	//     return $record = $this->general_model->fetch_specific_vals($fields,$where, $table);
	// }  
	// REMOVE DEADLINE

	protected function remove_deadlines($module_id, $request_id)
	{
		$where['module_ID'] = $module_id;
		$where['request_ID'] = $request_id;
		return $delete_stat = $this->general_model->delete_vals($where, 'tbl_deadline');
	}

	// private function get_follow_data(){
	//     $user_id = $this->session->userdata('uid');
	//     $fields = "approvalLevel";
	//     $where = "userId = $user_id";
	//     return $record = $this->general_model->fetch_specific_vals($fields,$where,"tbl_deadline");
	// }
	// private function get_follow_requests(){
	// }
	// public function get_approvers_deadline_follow()
	// {
	//     $deadline['pending_deadline'] = $this->get_pending_deadline();
	//     $deadline['due_deadline'] = $this->get_due_deadlines();
	//     $deadline['follow'] = $this->get_follow_requests();
	//     echo json_encode($deadline);
	// }
	function getCurrentPosition($emp_id)
	{

		$fields = "pos_details,d.status";
		$where = "a.posempstat_id =b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and a.isActive=1 and emp_id=" . $emp_id;
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d");
	}

	//Mark Test
	public function totalHours($id, $custom = null)
	{
		$time = $this->empshifttime_fetch($id);
		$schedDate = $time[0]->sched_date;
		$inlog = $this->empshiftin_fetch($id);
		$outlog = $this->empshiftout_fetch($id);
		$shiftIN = date_format(date_create($time[0]->time_start), 'H:i');
		$shiftOUT = date_format(date_create($time[0]->time_end), 'H:i');
		$ampm = explode("|", $this->getTimeGreeting($shiftIN, $shiftOUT));

		$dateOut = ($ampm[1] == "Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($schedDate))) : $schedDate;

		$logoutNew = $dateOut . " " . $shiftOUT;

		$break = $this->empshiftgetrbreak_fetch($time[0]->acc_time_id);
		$totalBreak = $break[0]->hr + ($break[0]->min / 60);


		if (count($inlog) > 0 && count($outlog) > 0) {
			$in = $schedDate . " " . date_format(date_create($inlog[0]->login), 'H:i');
			$out = $outlog[0]->logout;
			$logout = (strtotime($out) >= strtotime($logoutNew)) ? $logoutNew : $out;

			$loginNew = (strtotime($in) <= strtotime($schedDate . " " . $shiftIN)) ? $schedDate . " " . $shiftIN : $in;

			$diff = strtotime($logout) - strtotime($loginNew);
			$totalWorked = (($diff / 60) / 60) - $totalBreak;
		} else {
			$totalWorked = 0;
			$totalShift = 0;
		}
		$diff_shift = strtotime($logoutNew) - strtotime($schedDate . " " . $shiftIN);
		$totalShift = (($diff_shift / 60) / 60) - $totalBreak;
		/* 			$arr["in"] = $in;
          $arr["loginNew"] = $loginNew;
          $arr["logoutNew"] = $logoutNew;
         */
		$arr["total_actual_worked"] = $totalWorked;
		$arr["total_shift_hours"] = $totalShift;
		$arr["remaining_hour"] = abs($totalShift - $totalWorked);
		if ($custom == null) {
			echo json_encode($arr);
		} else {
			return $arr;
		}
	}

	public function empshifttime_fetch($sched_id)
	{
		$query = $this->db->query("SELECT time_start,time_end,sched_date,a.acc_time_id FROM tbl_schedule a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and sched_id=$sched_id");
		return $query->result();
	}

	public function empshiftgetrbreak_fetch($acc_time_id)
	{
		$query = $this->db->query("SELECT sum(hour) hr,sum(min) min FROM tbl_shift_break a,tbl_break_time_account b,tbl_break_time c where a.bta_id=b.bta_id and b.btime_id=c.btime_id and acc_time_id=$acc_time_id");
		return $query->result();
	}

	public function empshiftout_fetch($shift)
	{
		$query = $this->db->query("select dtr_id,emp_id,log as logout,acc_time_id from tbl_dtr_logs where entry='O' and type='DTR' and sched_id=" . $shift . " limit 1");
		return $query->result();
	}

	public function empshiftin_fetch($shift)
	{
		$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and sched_id=" . $shift . " limit 1");
		return $query->result();
	}

	function getTimeGreeting($shiftIN, $shiftOUT)
	{
		date_default_timezone_set('Asia/Manila');
		$getTimeIn = explode(":", $shiftIN);
		$getTimeOut = explode(":", $shiftOUT);
		if (($getTimeIn[0] >= 0 && $getTimeIn[0] < 12) && ($getTimeOut[0] >= 0 && $getTimeOut[0] < 12)) {

			$ampm = "AM|AM";
		} else {
			$in = ($getTimeIn[0] >= 0 && $getTimeIn[0] < 12) ? "AMs" : "PM";
			$out = (($getTimeOut[0] >= 12 && $getTimeOut[0] <= 23)) ? "PM" : "AM";
			$ampm = $in . "|" . $out;
		}

		if ($ampm == "PM|AM") {
			$newVal = "Today|Tomorrow";
		} else {
			$newVal = "Today|Today";
		}
		return $newVal;
	}

	protected function get_yearly_leave_credits($leaveType_ID, $uid=NULL,$emp_id=NULL)
	{
		$minusplus = 1;
		if($uid==NULL || $emp_id==NULL){
			$uid = $this->session->userdata('uid');
			$emp_id = $this->session->userdata('emp_id');
		}
        $currentPosition = $this->general_model->fetch_specific_val("a.emp_promoteID,a.dateFrom,b.posempstat_id,c.pos_details,d.status,e.credit", "a.emp_id=$emp_id AND a.isHistory=1 AND a.isActive=1 AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND e.isActive=1 AND e.posempstat_id=b.posempstat_id AND e.leaveType_ID=$leaveType_ID", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_leave_credit e", "DATE(dateFrom) DESC");
        if ($currentPosition == null) {
            return array('remaining' => null, 'credits' => null, 'taken' => null);
        } else {
            //add isPaid = 1 nga filter
            $request_leave_details = $this->general_model->fetch_specific_vals("minutes", "b.isPaid = 1 AND b.isActive=1 AND YEAR(b.date) = '" . date('Y') . "' AND b.overAllStatus IN (2,5) AND a.requestLeave_ID=b.requestLeave_ID AND b.leaveType_ID = $leaveType_ID AND a.uid = $uid", "tbl_request_leave a,tbl_request_leave_details b");

            $total_minutes = 0;
            foreach ($request_leave_details as $rld) {
                $total_minutes += $rld->minutes;
            }
            $total_request_leave = $total_minutes / 480;
            //INSERT HERE QUERY FOR NEWLY REGULARIZED CHURVABELS
            $credit = $currentPosition->credit;
            $regularizedDate = $this->newly_regularized_checker($currentPosition->posempstat_id, $leaveType_ID);
           //$regularizedDate is the currentPositionDate if emp is regularized this year (even if promoted multiple times this year)
            if ($regularizedDate !== NULL) {
                $regDate = strtotime($regularizedDate);
                if (date('Y') === date('Y', $regDate)) {
                    $monthnum = date('n', $regDate);
                    $monthsleft = 12 - $monthnum + $minusplus; // plus 1 originally maybe to include current month || minus 1 if not include current month
                    $credit = $credit / 12 * $monthsleft;
                }
            }
            //INSERT HERE  QUERY FOR NEWLY REGULARIZED CHURVABELS
            //FOR DECIMAL CHURVA OF REGULARIZED
            if (is_numeric($credit) === true && floor($credit) !== $credit) {
                $number = (float) $credit;
                $whole = floor($number);      // 1
                $fraction = $number - $whole; // .25
                if ($fraction != 0) {
                    if ($fraction <= 0.25) {
                        $credit = $whole;
                    } else if ($fraction >= 0.75) {
                        $credit = $whole + 1;
                    } else {
                        $credit = (float) $whole . '.5';
                    }
                }
            }

            //FOR DECIMAL CHURVA OF REGULARIZED
            $remaining = $credit - $total_request_leave;
            $final_remaining = ($remaining < 0) ? 0 : round($remaining, 2);
            return array('remaining' => $final_remaining, 'credits' => $credit, 'taken' => $total_request_leave);
        }
	}

	public function newly_regularized_checker($posempstat_id, $leaveType_ID, $emp_id = NULL)
	{
		//checks if employee is regularized this year !!
        //if employee latest emp promote is his 1st regularization
        //then if current history is the 1st regularization
        if ($leaveType_ID == 2 || $leaveType_ID == 6) {
            //2 = vacation
            //6 = sick
            if ($emp_id === NULL) {
                $emp_id = $this->session->userdata('emp_id');
            }
            
            $history = $this->general_model->fetch_specific_val("posempstat_id ", "posempstat_id =$posempstat_id AND isActive = 1 AND empstat_id = 3", "tbl_pos_emp_stat ");

            if ($history === NULL) { //NOT REGULAR
                return NULL;
            } else {
                // $currentPosition = $this->general_model->fetch_specific_val("b.emp_promoteID, a.posempstat_id, b.dateFrom ", "emp_id =$emp_id AND a.isActive = 1 AND b.ishistory = 1 AND a.posempstat_id=b.posempstat_id AND b.isActive = 1", "tbl_pos_emp_stat a,tbl_emp_promote b", "b.dateFrom ASC");

                $regularization = $this->general_model->fetch_specific_val("b.emp_promoteID, a.posempstat_id, b.dateFrom ", "emp_id =$emp_id AND a.isActive = 1 AND b.ishistory = 1 AND a.posempstat_id=b.posempstat_id AND empstat_id = 3", "tbl_pos_emp_stat a,tbl_emp_promote b", "b.dateFrom ASC");
     
                if ($regularization !== NULL) {
                    if (date('Y',strtotime($regularization->dateFrom)) === date('Y')) {
                        return $regularization->dateFrom; //$currentPosition->dateFrom;
                    } 
                } else {
                    return NULL;
                }
            }
        } else {
            return NULL;
        }
	}

	//------------------------------NOTIFICATIONS---------------------------------------------------
	public function readNotificationOnPage()
	{
		$mainNotifTable = $this->input->post('mainNotifTable');
		$subNotifTable = $this->input->post('subNotifTable');
		$mainNotifID = $this->input->post('mainNotifID');
		$subNotifID = $this->input->post('subNotifID');
		$link = $this->input->post('link');
		$uid = $this->session->userdata('uid');
		$final_path = str_replace(base_url(), "", $link);
		$pos = strpos($final_path, "#");
		if ($pos !== false) {
			$final_path = substr($final_path, 0, $pos);
		}
		$stat_read = 9;
		$stat_unread = 8;
		$notifications = $this->general_model->fetch_specific_vals("$subNotifID", "a.$mainNotifID=b.$mainNotifID AND b.recipient_ID=$uid AND status_ID=$stat_unread AND link='$final_path'", "$mainNotifTable a, $subNotifTable b");
		$data = array();
		foreach ($notifications as $notif) {
			$data[] = array('data' => array('status_ID' => $stat_read), 'where' => $subNotifID . '=' . $notif->$subNotifID);
		}
		$res = $this->general_model->update_array_vals($data, $subNotifTable);
		if ($mainNotifTable === 'tbl_system_notification') {
			if ($res) {
				$dms_unread = $this->general_model->fetch_specific_val("COUNT(*) as unreadcount", "a.$mainNotifID=b.$mainNotifID AND b.recipient_ID=$uid AND status_ID=$stat_unread AND a.reference='dms'", " $mainNotifTable a, $subNotifTable b")->unreadcount;
				$snapsz_unread = $this->general_model->fetch_specific_val("COUNT(*) as unreadcount", "a.$mainNotifID=b.$mainNotifID AND b.recipient_ID=$uid AND status_ID=$stat_unread AND a.reference='snapsz'", " $mainNotifTable a, $subNotifTable b")->unreadcount;
				$general_unread = $this->general_model->fetch_specific_val("COUNT(*) as unreadcount", "a.$mainNotifID=b.$mainNotifID AND b.recipient_ID=$uid AND status_ID=$stat_unread AND a.reference='general'", " $mainNotifTable a, $subNotifTable b")->unreadcount;
			} else {
				$dms_unread = 0;
				$snapsz_unread = 0;
				$general_unread = 0;
				//echo failed;
			}
			echo json_encode(array('dms_unread' => $dms_unread, 'snapsz_unread' => $snapsz_unread, 'general_unread' => $general_unread));
		} else {
			if ($res) {
				$unreadcount = $this->general_model->fetch_specific_val("COUNT(*) as unreadcount", "a.$mainNotifID=b.$mainNotifID AND b.recipient_ID=$uid AND status_ID=$stat_unread", " $mainNotifTable a, $subNotifTable b")->unreadcount;
			} else {
				$unreadcount = 0;
				//echo failed;
			}
			echo json_encode(array('unreadCount' => $unreadcount));
		}
	}

	public function readNotification()
	{
		$recipient_ID = $this->input->post('recipient_ID');
		$subNotifTable = $this->input->post('subNotifTable');
		$status_ID = 9;
		$res = $this->general_model->update_vals(array('status_ID' => $status_ID), "recipient_ID=$recipient_ID", $subNotifTable);

		if ($res) {
			echo json_encode(array('status' => 'Success'));
		} else {
			echo json_encode(array('status' => 'Failed'));
		}
	}

	public function getMyAuditTrail()
	{
		$uid = $this->session->userdata('uid');
		$limiter = $this->input->post('limiter');
		$data = $this->general_model->custom_query("SELECT io_id,log,ipaddress,remark FROM tbl_login_logout_session WHERE uid=$uid ORDER BY log DESC LIMIT $limiter,$this->perPage");
		foreach ($data as $d) {
			if ($d->remark === 'Login' || $d->remark === 'Logout') {
				$d->colorclass = 'metal';
			} else if ($d->remark === 'I - DTR' || $d->remark === 'O - DTR' || $d->remark === 'I - DTR (w/o sched)' || $d->remark === 'O - DTR (w/o sched)') {
				$d->colorclass = 'info';
			} else {
				$d->colorclass = 'warning';
			}
		}
		echo json_encode(array('audittrail' => $data));
	}

	public function getChatContacts()
	{
    	$list_contact = [];
    	$group_contact = [];
    	$emp_id = $this->session->emp_id;
    	$status = $this->general_model->fetch_specific_val("status", "emp_id=$emp_id", "tbl_meszenger_status");


		//		========== 		Start Hide Contact 			========== 		//
    	$hide_contact = [];
    	array_push($hide_contact, $emp_id,184);
		//		========== 		End Hide Contact 			========== 		//


		//		========== 		Start Default Contact 		========== 		//
    	$default_query = $this->general_model->fetch_specific_vals("emp_id", "status=1", "tbl_meszenger_status");
    	foreach($default_query as $value){
    		array_push($list_contact, $value->emp_id);
    	}
		//		========== 		End Default Contact 		========== 		//


		//		========== 		Start Supervisor Contact 	========== 		//
    	$supervisor = $this->general_model->custom_query(
    		"SELECT e1.emp_id FROM tbl_employee e1 LEFT JOIN tbl_employee e2 ON e2.Supervisor = e1.emp_id WHERE e2.emp_id = $emp_id"
    	);
    	if(!empty($supervisor)){
    		array_push($list_contact, $supervisor[0]->emp_id);
    	}
		//		========== 		End Supervisor Contact 		========== 		//


		//		========== 		Start Subordinate Contact 	========== 		//
    	$subordinates = $this->general_model->fetch_specific_vals(
    		"Supervisor, emp_id",
    		"Supervisor = $emp_id AND isActive = 'yes'",
    		"tbl_employee"
    	);

    	$results = $subordinates;
    	$stop = false;
    	if (!empty($subordinates)) {
    		while ($stop === false) {
    			foreach ($subordinates as $d) {
    				$emps[] = $d->emp_id;
    			}
    			$query2 = "SELECT emp_id FROM tbl_employee WHERE isActive='yes' AND Supervisor IN (" . implode(',', $emps) . ")";

    			$subordinates = $this->general_model->custom_query($query2);
    			if (!empty($subordinates)) {
    				$results = array_merge($results, $subordinates);
    				unset($emps);
    			} else {
    				foreach($results as $value){
    					array_push($list_contact, $value->emp_id);
    				}
    				$stop = true;
    			}
    		}
    	}else{
    		foreach($subordinates as $value){
    			array_push($list_contact, $value->emp_id);
    		}
    	}
		//		========== 		End Supervisor Contact 		========== 		//


		//		========== 		Start Group Contact 		========== 		//
    	$acc_id = $this->session->acc_id;
    	$group = $this->general_model->fetch_specific_vals(
    		"group_acc_id, group_emp_id", 
    		"flag=1 AND find_in_set($acc_id, group_acc_id)", 
    		"tbl_meszenger_group"
    	);
    	foreach($group as $value){
    		$list_group = explode(',',$value->group_acc_id);
    		for($x=0; $x<count($list_group); $x++){
    			$get_group = $this->general_model->fetch_specific_vals(
    				"emp.emp_id", 
    				"$list_group[$x] = acc.acc_id AND emp.isActive = 'yes' AND acc.acc_id = emp.acc_id",
    				"tbl_employee emp, tbl_account acc"
    			);
    			foreach($get_group as $emp){
    				array_push($group_contact, $emp->emp_id);
    			}
    		}
    		$list_user = explode(',', $value->group_emp_id);
    		for($x=0; $x<count($list_user); $x++){
    			array_push($group_contact, $list_user[$x]);
    		}
    	}
		//		========== 		End Group Contact 			========== 		//

    	
    	switch ($status->status) {
    		case '0':
    		$results = "";
    		break;
    		case '1':
    		if(!empty($list_contact)){
    			$results = $this->general_model->fetch_specific_vals(
    				"a.fname,a.lname,a.pic,b.emp_id", 
    				"a.apid = b.apid AND b.isActive = 'yes' AND b.emp_id NOT IN (" . implode(',', $hide_contact) . ")", 
    				"tbl_applicant a,tbl_employee b", 
    				"a.lname ASC"
    			);
    		}
    		
    		break;
    		case '2':
    		if(!empty($list_contact)){
    			$results = $this->general_model->fetch_specific_vals(
    				"a.fname,a.lname,a.pic,b.emp_id", 
    				"a.apid = b.apid AND b.isActive = 'yes' AND b.emp_id NOT IN (" . implode(',', $hide_contact) . ") AND b.emp_id IN (" . implode(',', $list_contact) . ")", 
    				"tbl_applicant a,tbl_employee b", 
    				"a.lname ASC");
    		}
    		
    		break;
    		case '3':
    		if(!empty($list_contact)){
    			$group_contact = array_merge($group_contact, $list_contact);
    			$results = $this->general_model->fetch_specific_vals(
    				"a.fname,a.lname,a.pic,b.emp_id", 
    				"a.apid = b.apid AND b.isActive = 'yes' AND b.emp_id NOT IN (" . implode(',', $hide_contact) . ") AND b.emp_id IN (" . implode(',', $group_contact) . ")", 
    				"tbl_applicant a,tbl_employee b", 
    				"a.lname ASC");
    		}
    		
    		break;
    		default:
    		$results = "";
    		break;
    	}
    	echo json_encode(['status' => 'Success', 'results' => $results]);
	}

	public function getEmployeeSubordinate($uid)
	{
		$emp_id = $this->general_model->fetch_specific_val('emp_id', "uid=$uid", 'tbl_user')->emp_id;
		$query = "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,d.acc_name,e.uid FROM tbl_applicant a,tbl_employee b,tbl_account d ,tbl_user e WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.isActive='yes' AND e.emp_id=b.emp_id AND b.Supervisor IN ($emp_id)";
		$data = $this->general_model->custom_query($query);
		$results = $data;
		$stop = false;
		if (!empty($data)) {
			while ($stop === false) {
				foreach ($data as $d) {
					$emps[] = $d->emp_id;
				}
				$query2 = "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,d.acc_name,e.uid FROM tbl_applicant a,tbl_employee b,tbl_account d ,tbl_user e WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.isActive='yes' AND e.emp_id=b.emp_id AND b.Supervisor IN (" . implode(',', $emps) . ")";

				$data = $this->general_model->custom_query($query2);
				if (!empty($data)) {
					$results = array_merge($results, $data);
					unset($emps);
				} else {
					$stop = true;
				}
			}
		}
		usort($results, array($this, "cmp"));
		return $results;
	}
	public function getAllEmployeeSubordinate($uid)
	{
		$emp_id = $this->general_model->fetch_specific_val('emp_id', "uid=$uid", 'tbl_user')->emp_id;
		$query = "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,d.acc_name,e.uid FROM tbl_applicant a,tbl_employee b,tbl_account d ,tbl_user e WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND e.emp_id=b.emp_id AND b.Supervisor IN ($emp_id)";
		$data = $this->general_model->custom_query($query);
		$results = $data;
		$stop = false;
		if (!empty($data)) {
			while ($stop === false) {
				foreach ($data as $d) {
					$emps[] = $d->emp_id;
				}
				$query2 = "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,d.acc_name,e.uid FROM tbl_applicant a,tbl_employee b,tbl_account d ,tbl_user e WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND e.emp_id=b.emp_id AND b.Supervisor IN (" . implode(',', $emps) . ")";

				$data = $this->general_model->custom_query($query2);
				if (!empty($data)) {
					$results = array_merge($results, $data);
					unset($emps);
				} else {
					$stop = true;
				}
			}
		}
		usort($results, array($this, "cmp"));
		return $results;
	}
	function cmp($a, $b)
	{
		return strcasecmp($a->lname, $b->lname);
	}

	function cmp2($a, $b)
	{
		return strcasecmp($a->fname, $b->fname);
	}

	public function check_if_date($value)
    {
        // $date_bool = 0;
        // // echo ucfirst($value);
		// $formats = ['M d, Y', 'Y-m-d'];
		
        // foreach ($formats as $f) {
        //     $d = DateTime::createFromFormat($f, ucfirst($value));
		// 	$is_date = $d && $d->format($f) === ucfirst($value);
		// 	var_dump($d);
        //     if (true == $is_date) {
        //         $date_bool = 1;
        //     }
		// }
		// // if(strtotime($value)){
		// // 	$date_bool = 1;
		// // 	// it's in date format
		// // }
		// return $date_bool;
		if (!$value) {
			return false;
		}
		try {
			new \DateTime($value);
			return true;
		} catch (\Exception $e) {
			return false;
		}
	}

	public function convert_date($dateVal)
    {
        $date = date_create($dateVal);
        return date_format($date, "Y-m-d");
    }

	protected function update_sched_lock($sched_id, $remarks, $module_id, $locked_stat)
	{
		$data['remarks'] = $remarks;
		$data['module_ID'] = $module_id;
		$data['isLocked'] = $locked_stat;
		$where = "sched_id = $sched_id";
		return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_schedule');
	}

	protected function get_emp_account_details($emp_id)
	{
		$fields = "accounts.acc_id, accounts.acc_name, beforeCallingTime";
		$where = "accounts.acc_id = emp.acc_id AND emp.emp_id = $emp_id";
		$tables = "tbl_account accounts, tbl_employee emp";
		return $record = $this->general_model->fetch_specific_val($fields, $where, $tables);
	}

	protected function get_allowable_break($account_id, $sched_id)
	{
		$fields = "accTime.acc_time_id, break.break_type, breakTime.hour, breakTime.min";
		$where = "breakTime.btime_id = accountBreak.btime_id AND break.break_id = accountBreak.break_id AND accountBreak.bta_id = shiftBreak.bta_id AND times.time_id = accTime.time_id AND shiftBreak.isActive = 1 AND shiftBreak.acc_time_id = accTime.acc_time_id AND  accTime.acc_time_id = sched.acc_time_id  AND acc_id =$account_id AND sched.sched_id = $sched_id";
		$tables = "tbl_acc_time accTime, tbl_shift_break shiftBreak, tbl_break_time_account accountBreak, tbl_break_time breakTime, tbl_break break, tbl_time times, tbl_schedule sched";
		$breaks = $this->general_model->fetch_specific_vals($fields, $where, $tables);
		$hours = 0;
		$minutes = 0;
		if (count($breaks) > 0) {
			foreach ($breaks as $break_val) {
				$hours += $break_val->hour;
				$minutes += $break_val->min;
			}
			$hours_to_min = $hours * 60;
			$minutes += $hours_to_min;
			$break['breakData'] = $breaks;
			$break['minutes'] = $minutes;
		} else {
			$break['breakData'] = 0;
			$break['minutes'] = 0;
		}
		return $break;
	}
	public function get_breaks($login_id, $logout_id, $emp_id, $entry)
	{
		$fields = "dtr.dtr_id, dtr.emp_id, dtr.acc_time_id, dtr.entry, dtr.log, dtr.note, dtr.type, dtr.sched_id, break.break_type, bt.hour, bt.min";
		$where = "break.break_id = bta.break_id AND bt.btime_id = bta.btime_id AND bta.bta_id = shiftBreak.bta_id AND shiftBreak.sbrk_id = dtr.acc_time_id AND dtr.dtr_id > $login_id AND dtr.dtr_id < $logout_id AND dtr.emp_id = $emp_id AND dtr.type = 'Break' AND dtr.entry = '$entry'";
		$table = "tbl_dtr_logs dtr, tbl_shift_break shiftBreak, tbl_break_time_account bta, tbl_break_time bt, tbl_break break";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'dtr_id ASC');
	}

	public function get_next_dtr_log($login_id, $emp_id){
		// $fields = "dtr_id, emp_id, acc_time_id, entry, log, note, type, sched_id";
		// $where = "dtr_id > $login_id AND type = 'DTR' AND emp_id= $emp_id";
		// return $this->general_model->fetch_specific_val($fields, $where, 'tbl_dtr_logs', 'log ASC LIMIT 1');
		$query = "SELECT dtr_id, emp_id, acc_time_id, entry, log, note, type, sched_id FROM tbl_dtr_logs WHERE dtr_id > $login_id AND type = 'DTR' AND emp_id= $emp_id ORDER BY dtr_id ASC LIMIT 1";
		return $this->general_model->custom_query($query);
	}

	public function test_next_dtr(){
		$test_dtr = $this->get_next_dtr_log(1270549, 1223);
		if($test_dtr == NULL){
			var_dump('empty siya');
		}else{
			var_dump($test_dtr);
		}
	}
	protected function get_standard_shift($date_time_start, $date_time_end){
		if (strtotime($date_time_end) < strtotime($date_time_start)) {
			$date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' 1 days'));
			$date_time_start = $date_time_start;
		}
		$shift_start_date_time = new DateTime($date_time_start);
		$shift_end_date_time = new DateTime($date_time_end);
		$formatted_shift_start = $shift_start_date_time->format("H:i");
		$formatted_shift_end = $shift_end_date_time->format("H:i");
		if ($formatted_shift_start == "00:00") {
			$date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' 1 days'));
			// $date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
			$date_time_start = Date("Y-m-d H:i", strtotime($date_time_start. ' 1 days'));
		}
		if ($formatted_shift_end == "00:00") {
			$date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' -1 days'));
			// $date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
			$date_time_start = Date("Y-m-d H:i", strtotime($date_time_start));
		}
		$new_date_shift = [
			'start' => $date_time_start,
			'end' => $date_time_end,
		];
		return $new_date_shift;
	}
	protected function check_start_end_shift($date_time_start, $date_time_end)
	{
		if (strtotime($date_time_end) < strtotime($date_time_start)) {
			$date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' 1 days'));
			$date_time_start = $date_time_start;
		}
		$shift_start_date_time = new DateTime($date_time_start);
		$shift_end_date_time = new DateTime($date_time_end);
		$formatted_shift_start = $shift_start_date_time->format("H:i");
		$formatted_shift_end = $shift_end_date_time->format("H:i");
		if ($formatted_shift_start == "00:00") {
			$date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' 1 days'));
			// $date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
			$date_time_start = Date("Y-m-d H:i", strtotime($date_time_start));
		}
		if ($formatted_shift_end == "00:00") {
			$date_time_end = Date("Y-m-d H:i", strtotime($date_time_end . ' -1 days'));
			// $date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
			$date_time_start = Date("Y-m-d H:i", strtotime($date_time_start));
		}
		$new_date_shift = [
			'start' => $date_time_start,
			'end' => $date_time_end,
		];
		return $new_date_shift;
	}

	protected function start_end_shift_24($date_time_start, $date_time_end)
	{
		$date_start = Date("Y-m-d", strtotime($date_time_start));
		$time_start = Date("H:i", strtotime($date_time_start));
		if (strtotime($date_time_end) < strtotime($date_time_start)) {
			$date_end = Date("Y-m-d", strtotime($date_time_end . ' 1 days'));
			$time_end = Date("H:i", strtotime($date_time_end . ' 1 days'));
		}else{
			$date_end = Date("Y-m-d", strtotime($date_time_end));
			$time_end = Date("H:i", strtotime($date_time_end));
		}
		$shift_start_date_time = new DateTime($date_time_start);
		$shift_end_date_time = new DateTime($date_time_end);
		$formatted_shift_start = $shift_start_date_time->format("H:i");
		$formatted_shift_end = $shift_end_date_time->format("H:i");
		if ($formatted_shift_start == "00:00") {
			$date_end = Date("Y-m-d", strtotime($date_time_end));
			$time_end = Date("H:i", strtotime($date_time_end));
			// $date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
			$date_start = Date("Y-m-d", strtotime($date_time_start. '-1 days'));
			$time_start = "24:00";
		}
		if ($formatted_shift_end == "00:00") {
			$date_end = Date("Y-m-d", strtotime($date_time_end . ' -1 days'));
			$time_end = "24:00";
			// $date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
			$date_start = Date("Y-m-d", strtotime($date_time_start));
			$time_start = Date("H:i", strtotime($date_time_start));
		}
		$new_date_shift = [
			'start_date_time' =>$date_start." ".$time_start,
			'start_date' => $date_start,
			'start_time' => $time_start,
			'end_date_time' =>$date_end." ".$time_end,
			'end_date' => $date_end,
			'end_time' => $time_end
		];
		return $new_date_shift;
	}

	protected function check_start_end_shift_24($date_time_start, $date_time_end)
	{
		$date_start = Date("Y-m-d", strtotime($date_time_start));
		$time_start = Date("H:i", strtotime($date_time_start));
		if (strtotime($date_time_end) < strtotime($date_time_start)) {
			$date_end = Date("Y-m-d", strtotime($date_time_end . ' 1 days'));
			$time_end = Date("H:i", strtotime($date_time_end . ' 1 days'));
		}else{
			$date_end = Date("Y-m-d", strtotime($date_time_end));
			$time_end = Date("H:i", strtotime($date_time_end));
		}
		$shift_start_date_time = new DateTime($date_time_start);
		$shift_end_date_time = new DateTime($date_time_end);
		$formatted_shift_start = $shift_start_date_time->format("H:i");
		$formatted_shift_end = $shift_end_date_time->format("H:i");
		if ($formatted_shift_start == "00:00") {
			$date_end = Date("Y-m-d", strtotime($date_time_end . ' 1 days'));
			$time_end = Date("H:i", strtotime($date_time_end . ' 1 days'));
			// $date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
			$date_start = Date("Y-m-d", strtotime($date_time_start));
			$time_start .= " 24:00";
		}
		if ($formatted_shift_end == "00:00") {
			$date_end = Date("Y-m-d", strtotime($date_time_end . ' -1 days'));
			$time_end .= " 24:00";
			// $date_time_start = Date("Y-m-d H:i", strtotime($date_time_start . ' 1 days'));
			$date_start = Date("Y-m-d", strtotime($date_time_start));
			$time_start = Date("H:i", strtotime($date_time_start));
		}
		$new_date_shift = [
			'start_date_time' =>$date_start." ".$time_start,
			'start_date' => $date_start,
			'start_time' => $time_start,
			'end_date_time' =>$date_end." ".$time_end,
			'end_date' => $date_end,
			'end_time' => $time_end
		];
		return $new_date_shift;
	}

	protected function new_format_midnight($formatted_date, $date)
	{

		$new_format_date = $formatted_date;
		if ($formatted_date == $date . " 00:00") {
			$new_format_date = $date . " 24:00";
		}
		return $new_format_date;
	}

	// protected function get_human_time_format($date_time_from, $date_time_to, $bct)
	// {
	// 	$diff = $date_time_to - $date_time_from;
	// 	$hour = $diff / 3600;
	// 	$hour_bct_diff = $hour - $bct;
	// 	$time['hoursTotal'] = $hour;
	// 	$time['hours'] = (int) $hour_bct_diff;
	// 	$decimal = $hour_bct_diff - (int) $hour_bct_diff;
	// 	$time['minutes'] = round($decimal * 60);
	// 	return $time;
	// }
	protected function get_human_time_format($date_time_from, $date_time_to, $bct)
	{
		$diff = $date_time_to - $date_time_from;
		$hour = $diff / 3600;
		$hour_bct_diff = $hour - $bct;
		$time['hoursTotal'] = $hour;
		$time['hours'] = (int) $hour_bct_diff;
		$decimal = $hour_bct_diff - (int) $hour_bct_diff;
		$min = floor($diff /60);
		$seconds = $diff - $min * 60;
		$time['minutes'] = $min;
		$time['seconds'] = $seconds;
		return $time;
	}

	public function create_log_history($module_ID, $emp_id, $text)
	{
		return $this->general_model->insert_vals(array('module_ID' => $module_ID, 'emp_id' => $emp_id, 'text' => $text), "tbl_log_history");
	}

	public function fill_leave_credits() //use this to set all empty leave credits to 0
	{
		$uid = $this->session->userdata('uid');
		$noCredits = array();
		$query = "SELECT c.posempstat_id,pos_name,pos_details,b.status,class "
			. "FROM tbl_position a "
			. "INNER JOIN tbl_pos_emp_stat c ON a.pos_id=c.pos_id "
			. "INNER JOIN tbl_emp_stat b ON b.empstat_id=c.empstat_id  ORDER BY pos_name ASC";

		$data = $this->general_model->custom_query($query);
		$leave_list = $this->general_model->fetch_all("leaveType_ID,leaveType", "tbl_leave_type", "leaveType ASC");
		foreach ($data as $pos_stat) {
			foreach ($leave_list as $leave) {
				$cred = $this->general_model->fetch_specific_val("leaveCredit_ID,credit", "leaveType_ID=$leave->leaveType_ID AND posempstat_id=$pos_stat->posempstat_id", "tbl_leave_credit");

				if (empty($cred)) {
					$noCredits[] = array('posempstat_id' => $pos_stat->posempstat_id, 'leaveType_ID' => $leave->leaveType_ID, 'credit' => 0, 'isActive' => 1, 'changedBy' => $uid);
				}
			}
		}
		echo "<pre>";
		print_r($noCredits);
		echo "</pre>";
		$res = $this->general_model->batch_insert($noCredits, "tbl_leave_credit");
		//var_dump($res);
	}

	// ADDED 5-6-2019 MIC MIC
	protected function get_direct_supervisor_via_emp_id($emp_id)
	{
		$fields = "Supervisor";
		$where = "emp_id = $emp_id AND Supervisor IS NOT NULL";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_employee");
	}

	protected function get_supervisors($emp_id, $num)
	{
		$data['error'] = 0;
		$data['error_details'] = "";
		$supervisors = [];
		$direct_sup = $this->get_direct_supervisor_via_emp_id($emp_id);
		if (count($direct_sup) > 0) {
			$sup_obj['level'] = 1;
			$sup_obj['emp_id'] = $direct_sup->Supervisor;
			$supervisors[0] = [
				'level' => 1,
				'emp_id' => $direct_sup->Supervisor
			];
			$dynamic_sup = $direct_sup->Supervisor;
			for ($loop = 1; $loop < $num; $loop++) {
				$next_level_sup = $this->get_direct_supervisor_via_emp_id($dynamic_sup);
				if (count($next_level_sup) > 0) {
					$supervisors[$loop] = [
						'level' => $loop + 1,
						'emp_id' => $next_level_sup->Supervisor
					];
					$dynamic_sup = $next_level_sup->Supervisor;
				} else {
					break;
				}
			}
			$data['supervisors'] = $supervisors;
		} else {
			$data['error'] = 1;
			$data['error_details'] = "direct supervisor does not exist";
		}
		return $data;
	}

	public function get_current_intervention($intervention_level, $emp_id)
	{
		$hr_manager = 522;
		$managing_director = 980;
		$intervenee = [];
		$intervenee_count = 0;
		$direct_sup = $this->get_direct_supervisor_via_emp_id($emp_id);
		if (count($direct_sup) > 0) {
			$current_intervenee = (int) $direct_sup->Supervisor;
			if ($current_intervenee == $managing_director && $emp_id !== 522) {
				$intervenee[0] = $hr_manager;
				$current_intervenee = $hr_manager;
			} else {
				$intervenee[0] = $current_intervenee;
			}
			for ($loop = 2; $loop < $intervention_level + 1; $loop++) {
				$direct_sup_supervisor = $this->get_direct_supervisor_via_emp_id($current_intervenee);
				if ((count($direct_sup_supervisor) > 0) && (strlen($direct_sup_supervisor->Supervisor) !== 0)) {
					$intervenee_count++;
					$orig_intervenee = $current_intervenee;
					$current_intervenee = (int) $direct_sup_supervisor->Supervisor;
					if ($direct_sup_supervisor->Supervisor == $managing_director && $loop == 1) {
						$intervenee[$intervenee_count] = $hr_manager;
						$current_intervenee = $hr_manager;
					} else if ((($direct_sup_supervisor->Supervisor == $managing_director) || ($direct_sup_supervisor->Supervisor == $hr_manager)) && ($loop < 4)) {
						$intervenee[$intervenee_count] = $orig_intervenee;
						$current_intervenee = $orig_intervenee;
					} else {
						if ($direct_sup_supervisor->Supervisor == $managing_director && $loop == 4) {
							$intervenee[$intervenee_count] = $hr_manager;
							$current_intervenee = $hr_manager;
						} else {
							$intervenee[$intervenee_count] = $current_intervenee;
						}
					}
				}
			}
			return $intervenee[count($intervenee) - 1];
		} else {
			return 0;
		}
	}

	protected function get_emp_details_via_emp_id($emp_id)
	{
		$fields = "emp.emp_id, users.uid, app.apid, app.pic, app.fname, app.lname, app.mname, app.nameExt, app.gender, emp.Supervisor, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, positions.site_ID, empStat.status as empStatus,users.password as pw, accounts.acc_description empType";
		$where = "empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.emp_id = $emp_id";
		$table = "tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app, tbl_account accounts, tbl_user users";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	protected function get_emp_details_via_user_id($user_id)
	{
		$fields = "emp.emp_id, users.uid, app.apid, app.pic, app.fname, app.lname, app.mname, app.nameExt, app.gender, emp.Supervisor, accounts.acc_id, accounts.acc_name accountDescription, accounts.acc_description as empType, positions.pos_details positionDescription, empStat.status as empStatus";
		$where = "empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id and app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = $user_id";
		$table = "tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app, tbl_account accounts, tbl_user users";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	protected function get_breaks_without_logout($login_id, $dated_shift_out, $emp_id)
	{
		$fields = "dtr.dtr_id, dtr.emp_id, dtr.acc_time_id, dtr.entry, dtr.log, dtr.note, dtr.type, dtr.sched_id, break.break_type, bt.hour, bt.min";
		$where = "break.break_id = bta.break_id AND bt.btime_id = bta.btime_id AND bta.bta_id = shiftBreak.bta_id AND shiftBreak.sbrk_id = dtr.acc_time_id AND dtr.dtr_id > $login_id AND type = 'Break' AND dtr.log <= '$dated_shift_out' AND dtr.emp_id = $emp_id";
		$table = "tbl_dtr_logs dtr, tbl_shift_break shiftBreak, tbl_break_time_account bta, tbl_break_time bt, tbl_break break";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'dtr_id ASC');
	}

	protected function get_formatted_time($hours)
	{
		$totalHours = (int) $hours;
		$totalMinutes = round(($hours - $totalHours) * 60);

		$totalTime = '';
		if ($totalMinutes == 60) {
			$totalMinutes = 0;
			$totalHours++;
		}
		$totalTime = '';
		if (($totalHours == 0) && ($totalMinutes == 1)) {
			$totalTime = $totalMinutes . " min";
		} else if (($totalHours == 0) && ($totalMinutes != 1)) {
			$totalTime = $totalMinutes . " mins";
		} else if (($totalMinutes == 0) && ($totalHours == 1)) {
			$totalTime = $totalHours . " hr";
		} else if (($totalMinutes == 0) && ($totalHours != 1)) {
			$totalTime = $totalHours . " hrs";
		} else if (($totalHours > 1) && ($totalMinutes == 1)) {
			$totalTime = $totalHours . " hrs & " . $totalMinutes . " min";
		} else if (($totalHours == 1) && ($totalMinutes > 1)) {
			$totalTime = $totalHours . " hr & " . $totalMinutes . " mins";
		} else if (($totalHours == 1) && ($totalMinutes == 1)) {
			$totalTime = $totalHours . " hr & " . $totalMinutes . " min";
		} else if (($totalHours > 1) && ($totalMinutes > 1)) {
			$totalTime = $totalHours . " hrs & " . $totalMinutes . " mins";
		}
		return $totalTime;
	}

	public function get_formatted_time_with_seconds($hours)
	{
		$totalHours = (int) $hours;
		$totalMinutes = round(($hours - $totalHours) * 60);
		$min_int = (int) (($hours - $totalHours) * 60);
		$min_raw = ($hours - $totalHours) * 60;
		$total_seconds = round(($min_raw - $min_int) * 60);
		$totalMinutes = $min_int;
		$totalTime = '';
		$totalTimeArr = [];
		$totalTimeCount = 0;
		if ($totalMinutes == 60) {
			$totalMinutes = 0;
			$totalHours++;
		}
		if ($total_seconds == 60) {
			$total_seconds = 0;
			$totalMinutes++;
		}
		if ($totalHours > 0) {
			if ($totalHours == 1) {
				$totalTimeArr[$totalTimeCount] = $totalHours . " hr";
			} else {
				$totalTimeArr[$totalTimeCount] = $totalHours . " hrs";
			}
			$totalTimeCount++;
		}
		if ($totalMinutes > 0) {
			if ($totalMinutes == 1) {
				$totalTimeArr[$totalTimeCount] = $totalMinutes . " min";
			} else {
				$totalTimeArr[$totalTimeCount] = $totalMinutes . " mins";
			}
			$totalTimeCount++;
		}
		if ($total_seconds > 0) {
			if ($total_seconds == 1) {
				$totalTimeArr[$totalTimeCount] = $total_seconds . " sec";
			} else {
				$totalTimeArr[$totalTimeCount] = $total_seconds . " secs";
			}
			$totalTimeCount++;
		}
		$totalTime = implode(", ", $totalTimeArr);
		return $totalTime;
	}

	
	public function setSystemNotif($message, $link, $sup, $reference = 'general')
	{
		// $message= $_SESSION["fname"]." ".$_SESSION["lname"]." was late on ".date("Y-m-d h");
		// $link="test/test";
		// $sup=114;
		$systemData["message"] = $message;
		$systemData["link"] = $link;
		$systemData["reference"] = $reference;
		$systemNotif = $this->general_model->insert_vals_last_inserted_id($systemData, 'tbl_system_notification');
		$systemDataRecipient["systemNotification_ID"] = $systemNotif;
		$systemDataRecipient["recipient_ID"] = $sup;
		$systemDataRecipient["status_ID"] = 8;
		$systemNotifRec = $this->general_model->insert_vals_last_inserted_id($systemDataRecipient, 'tbl_system_notification_recipient');
		return $systemNotif;
	}

	//plain new notification
	public function simple_notification($notif_mssg, $link, $receiver_uid, $sender_uid)
	{
		$notification_ID = $this->general_model->insert_vals_last_inserted_id(['message' => $notif_mssg, 'link' => $link, 'sender_ID' => $sender_uid], "tbl_notification");
		return $this->general_model->insert_vals_last_inserted_id(['notification_ID' => $notification_ID, 'recipient_ID' => $receiver_uid, 'status_ID' => 8], "tbl_notification_recipient");
	}
	//plain new system notification
	public function simple_system_notification($notif_mssg, $link, $receiver_uid, $reference = 'general')
	{
		$systemNotification_ID = $this->general_model->insert_vals_last_inserted_id(['message' => $notif_mssg, 'link' => $link, 'reference'=>$reference], "tbl_system_notification");
		return $this->general_model->insert_vals_last_inserted_id(['systemNotification_ID' => $systemNotification_ID, 'recipient_ID' => $receiver_uid, 'status_ID' => 8], "tbl_system_notification_recipient");
	}

	// DMS FUNCTIONS (MICHAEL) START
	protected function get_user_dtr_violation($user_dtr_violation_id)
	{
		$fields = "userDtrVio.userDtrViolation_ID, userDtrVio.user_ID, userDtrVio.supervisor_ID, userDtrVio.dtrViolationType_ID, vioType.description, userDtrVio.sched_ID, userDtrVio.month, userDtrVio.year, userDtrVio.sched_date, userDtrVio.shift, userDtrVio.occurence, userDtrVio.duration, userDtrVio.dateRecord";
		$where = "vioType.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = $user_dtr_violation_id";
		$table = "tbl_user_dtr_violation userDtrVio, tbl_dtr_violation_type vioType";
		return $record = $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	protected function get_same_offense_of_same_sched($offense_id, $sched_id){
		$fields = "ir.incidentReport_ID, ir.offense_ID, ir.liabilityStat, vio.sched_ID";
		$where = "vio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.qualifiedUserDtrViolation_ID = auto.qualifiedUserDtrViolation_ID AND ir.incidentReport_ID = auto.incidentReport_ID AND ir.offense_ID = $offense_id AND ir.liabilityStat = 17 AND vio.sched_ID = $sched_id";
		$table = "tbl_dms_incident_report ir, tbl_dms_auto_ir_related_details auto, tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation vio";
		return $record = $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	protected function get_dtr_violation_settings($dtr_violation_type_id)
	{
		$fields = "dtrVioSettings.dtrViolationTypeSettings_ID, dtrVioSettings.dtrViolationType_ID, dtrVioSettings.oneTimeStat, dtrVioSettings.oneTimeVal, dtrVioSettings.consecutiveStat, dtrVioSettings.consecutiveVal, dtrVioSettings.nonConsecutiveStat, dtrVioSettings.nonConsecutiveVal, dtrVioSettings.durationType, dtrVioSettings.durationVal, dtrVioSettings.durationUnit, dtrVioSettings.offense_ID, dtrVioSettings.category_ID, dtrVioSettings.daysDeadline, dtrVioSettings.deadlineOption";
		$where = "dtrVioSettings.dtrViolationType_ID = $dtr_violation_type_id";
		return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_dtr_violation_type_settings dtrVioSettings");
	}

	protected function get_specific_dms_deadline($record_id, $emp_id, $deadline_type_id)
	{
		$fields = "dmsDeadline_ID, deadline, status_ID, level, datedStatus";
		$where = "record_ID = $record_id AND emp_id = $emp_id AND dmsDeadlineType_ID = $deadline_type_id";
		return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_deadline");
	}

	protected function get_existing_ir($subject_emp_id, $prescriptive_stat, $liability_stat, $offense_ID)
	{
		$dateTime = $this->get_current_date_time();
		$where_cure_date = "";
		if ($prescriptive_stat == 4) {
			$where_cure_date = "AND date(ir.prescriptionEnd) >= '" . $dateTime['date'] . "'";
		}
		$fields = "ir.incidentReport_ID, ir.subjectEmp_ID, ir.sourceEmp_ID, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.liabilityStat, ir.datedLiabilityStat, ir.subjectExplanation, ir.occurence, discCateg.disciplineCategory_ID, discCateg.category, discAction.disciplinaryAction_ID, discAction.action, discActionCateg.disciplinaryActionCategory_ID, discActionCateg.level, ir.offense_ID, ir.offense, ir.prescriptiveId, ir.disciplinaryActionCategorySettings_ID";
		$where = "discAction.disciplinaryAction_ID = discActionCateg.disciplinaryAction_ID AND discCateg.disciplineCategory_ID = discActionCateg.disciplineCategory_ID AND discActionCateg.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND ir.prescriptiveStat = $prescriptive_stat AND ir.subjectEmp_ID = $subject_emp_id AND ir.liabilityStat = $liability_stat AND ir.offense_ID = $offense_ID " . $where_cure_date;
		$table = "tbl_dms_incident_report ir, tbl_dms_disciplinary_action_category discActionCateg, tbl_dms_disciplinary_action discAction, tbl_dms_discipline_category discCateg";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	protected function check_if_pending_qualified_exist($offense_id, $emp_id)
	{
		$fields = "qualified.qualifiedUserDtrViolation_ID, userDtrVio.userDtrViolation_ID, userDtrVio.dateRecord, userDtrVio.sched_date";
		$where = "settings.dtrViolationType_ID = userDtrVio.dtrViolationType_ID AND userDtrVio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.status_ID = 2 AND settings.offense_ID = $offense_id AND qualified.emp_id = $emp_id";
		$table = "tbl_user_dtr_violation userDtrVio, tbl_dms_qualified_user_dtr_violation qualified, tbl_dms_dtr_violation_type_settings settings";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	protected function get_suspension_dates($ir_id)
	{
		$fields = "susDates.date";
		$where = "susDates.suspensionTerminationDays_ID = days.suspensionTerminationDays_ID AND days.incidentReport_ID = $ir_id";
		$table = "tbl_dms_suspension_dates susDates, tbl_dms_suspension_termination_days days";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	protected function add_qualified_user_dtr_violation($subject_emp_id, $status_id, $occurence_rule_num, $user_dtr_violation_id, $occurence_value, $refreshment_date)
	{
		$data['emp_id'] = $subject_emp_id;
		$data['status_ID'] = $status_id;
		$data['occurrenceRuleNum'] = $occurence_rule_num;
		$data['userDtrViolation_ID'] = $user_dtr_violation_id;
		$data['occurrenceValue'] = $occurence_value;
		$data['refreshment_date'] = $refreshment_date;
		return $this->general_model->insert_vals_last_inserted_id($data, 'tbl_dms_qualified_user_dtr_violation');
	}

	protected function check_if_previous_ir_exist($user_dtr_violation, $subject_user_emp_details, $violation_rules)
	{
		$data['exist'] = 0;
		$data['ir_details'] = "";
		// get_if previous_ir_exist
		// get_ir_with_current_stat
		// get_ir_with_same_offense
		$existing_ir = $this->get_existing_ir($subject_user_emp_details->emp_id, 4, 17, $violation_rules->offense_ID);
		if (count($existing_ir) > 0) {
			// identify if curren IR prescription end date still covers the date of currently commited violation
			$current_presc_str = strtotime(date("Y-m-d", strtotime($existing_ir->prescriptionEnd)));
			$date_detected_str = strtotime(date("Y-m-d", strtotime($user_dtr_violation->dateRecord)));
			if ($date_detected_str <= $current_presc_str) {
				$data['exist'] = 1;
				$data['ir_details'] = $existing_ir;
			} else {
				//update IR prescriptive status to completed
				$data['prescriptiveStat'] = 3;
				$this->update_incident_report($data, $existing_ir->incidentReport_ID);
			}
		}
		return $data;
	}

	protected function set_dms_deadline($emp_id, $record_id, $days_deadline, $dms_deadline_type, $level)
	{
		$dateTime = $this->get_current_date_time();
		$deadline_date = Date("Y-m-d", strtotime($dateTime['date'] . $days_deadline . ' days'));
		$deadline_date_time = $deadline_date . " 23:59:59";
		$data['record_ID'] = $record_id;
		$data['emp_id'] = $emp_id;
		$data['deadline'] = $deadline_date_time;
		$data['dmsDeadlineType_ID'] = $dms_deadline_type;
		$data['status_ID'] = 2;
		$data['level'] = $level;
		return $this->general_model->insert_vals($data, "tbl_dms_deadline");
	}

	protected function set_qualified_user_dtr_violation_notification($emp_id, $qualified_user_dtr_violation_id, $status_id, $level)
	{
		$data['qualifiedUserDtrViolation_ID'] = $qualified_user_dtr_violation_id;
		$data['emp_id'] = $emp_id;
		$data['level'] = $level;
		$data['status_ID'] = $status_id;
		return $this->general_model->insert_vals($data, 'tbl_dms_supervisor_qualified_user_dtr_violation_notification');
	}

	protected function set_notify_ir_direct_sup($direct_sup_user_details, $subject_user_emp_details, $user_dtr_violation_obj)
	{
		$recipient[0] = ['userId' => $direct_sup_user_details->uid];
		$link = "discipline/dms_supervision/dtrViolations/fileDtrViolationIrTab";
		$notif_mssg = "<b>FILE IR</b>. You are notified to file an IR for " . ucfirst(strtolower($subject_user_emp_details->fname)) . " " . ucfirst(strtolower($subject_user_emp_details->lname));
		if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //LATE
		{
			$notif_mssg .= " for being <b>late</b> last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
		} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) // UT
		{
			$notif_mssg .= " due to a recorded <b>undertime</b> last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
		} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) //FORGOT TO LOGOUT
		{
			$notif_mssg .= " for <b>not logging out</b> on time last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
		} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
		{
			$notif_mssg .= " due to a recorded <b>overbreak</b> last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
		} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 6) // INC BREAK
		{
			$notif_mssg .= "'s <b>incomplete break</b> logs issue last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
		} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 7) //INC DTR Logs
		{
			$notif_mssg .= "'s <b>incomplete DTR</b> logs issue last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
		} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 5) // ABSENT
		{
			$notif_mssg .= " for being <b>absent</b> last " . Date("M j, Y", strtotime($user_dtr_violation_obj->sched_date));
		}
		return  $this->set_system_notif($notif_mssg,  $link,  $recipient, 'dms');
	}

	public function auto_notify_ir_direct_sup_test()
	{
		$data['error'] = 0;
		$sup_emp_id = 980;
		// $direct_sup_details =  $this->get_emp_details_via_emp_id($sup_emp_id);
		// $sup_emp_id = $direct_sup_details->emp_id;
		if($sup_emp_id == 980){
			$sup_emp_id = 522;
			$direct_sup_details = $this->get_emp_details_via_emp_id(522);
		}else{
			$direct_sup_details = $this->get_emp_details_via_emp_id($sup_emp_id);
		}
		var_dump($direct_sup_details);
		// if (count($direct_sup_details) > 0) {
		// 	$data['set_deadline_to_file_ir'] =  $this->set_dms_deadline($sup_emp_id,  $qualified_user_dtr_violation_id,  $violation_rules->daysDeadline, 1, 1); // set deadline
		// 	$data['set_qualified_user_dtr_violation_notification'] =  $this->set_qualified_user_dtr_violation_notification($sup_emp_id,  $qualified_user_dtr_violation_id, 4, 1); // set deadline
		// 	$data['notify_ir_direct_sup'] =  $this->set_notify_ir_direct_sup($direct_sup_details,  $subject_user_emp_details,  $user_dtr_violation); // set notify ir direct sup
		// } else {
		// 	$data['error'] = 1;
		// 	$data['error_details'] = "Direct Supervisor not found";
		// }
		// return  $data;
	}

	protected function auto_notify_ir_direct_sup($user_dtr_violation,  $subject_user_emp_details,  $qualified_user_dtr_violation_id,  $violation_rules)
	{
		$data['error'] = 0;
		$sup_emp_id = $subject_user_emp_details->Supervisor;
		if($sup_emp_id == 980){
			$sup_emp_id = 522;
			$direct_sup_details = $this->get_emp_details_via_emp_id(522);
		}else{
			$direct_sup_details = $this->get_emp_details_via_emp_id($sup_emp_id);
		}
		if (count($direct_sup_details) > 0) {
			$data['set_deadline_to_file_ir'] =  $this->set_dms_deadline($sup_emp_id,  $qualified_user_dtr_violation_id,  $violation_rules->daysDeadline, 1, 1); // set deadline
			$data['set_qualified_user_dtr_violation_notification'] =  $this->set_qualified_user_dtr_violation_notification($sup_emp_id,  $qualified_user_dtr_violation_id, 4, 1); // set deadline
			$data['notify_ir_direct_sup'] =  $this->set_notify_ir_direct_sup($direct_sup_details,  $subject_user_emp_details,  $user_dtr_violation); // set notify ir direct sup
		} else {
			$data['error'] = 1;
			$data['error_details'] = "Direct Supervisor not found";
		}
		return  $data;
	}
	protected function get_current_disciplinary_action_category_settings()
	{
		$fields = "disciplinaryActionCategorySettings_ID";
		$where = "status_ID = 10";
		return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_disciplinary_action_category_settings");
	}

	protected function get_disciplinary_action_details_prev_occurence($discipline_categ_id, $level, $disciplinaryActionCategorySettings_ID)
	{
		$fields = "discAct.disciplinaryAction_ID, discAct.action, discAct.periodMonth, discAct.abbreviation, discAct.dateCreated, discAct.dateChanged, discAct.changedBy";
		$where = "discAct.disciplinaryAction_ID = discActCateg.disciplinaryAction_ID AND discActCateg.disciplineCategory_ID = $discipline_categ_id AND discActCateg.level = $level AND discActCateg.disciplinaryActionCategorySettings_ID = " . $disciplinaryActionCategorySettings_ID;
		$table = "tbl_dms_disciplinary_action discAct, tbl_dms_disciplinary_action_category discActCateg";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	protected function get_start_and_end_date_coverage($sched_date,  $rule,  $unit)
	{
		//  $sched_date = '2019-02-10';
		//  $fixed_rule = 4;
		//  $unit = 'week';
		if ($unit == 'month') {
			$coverage_from_month = Date("Y-m-d", strtotime($sched_date . '-' .  $rule . ' months'));
			$start = new DateTime($coverage_from_month);
			$date['start'] =  $start->format('Y-m') . "-01";
			$end = new DateTime($sched_date);
			$date['end'] =  $end->format('Y-m-t');
		} else {
			$start_date = new DateTime(Date("Y-m-d", strtotime($sched_date . '-' .  $rule . ' weeks')));
			$start_year = (int) Date("Y", strtotime($sched_date . '-' .  $rule . ' weeks'));
			$start_week = (int) Date("W", strtotime($sched_date . '-' .  $rule . ' weeks'));
			$date['start'] =  $start_date->setISODate($start_year,  $start_week)->format('Y-m-d');
			$end_year = (int) Date("Y", strtotime($sched_date));
			$end_week = (int) Date("W", strtotime($sched_date));
			$end_date = new DateTime($sched_date);
			$end_date->setISODate($end_year,  $end_week)->format('Y-m-d');
			$date['end'] =  $end_date->modify('+6 days')->format('Y-m-d');
		}
		return  $date;
	}

	protected function get_user_dtr_violation_by_range_inc_logs($from, $to, $user_id, $user_dtr_id)
	{
		$fields = "userDtrViolation_ID, sched_date, occurence, duration";
		$where = "DATE(sched_date) >= '$from' AND DATE(sched_date) <= '$to' AND user_ID = $user_id AND dtrViolationType_ID IN (6, 7) AND status_ID = 0 AND userDtrViolation_ID !=$user_dtr_id";
		$table = "tbl_user_dtr_violation";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'sched_date ASC');
	}

	protected function compute_accumulated($violation_type,  $fetch_accumulated_obj,  $violation_rules_obj)
	{
		$accumulated_time = 0;
		$occurence = 0;
		$accumulated_violations = [];
		$accumulated_count = 0;
		foreach ($fetch_accumulated_obj as  $row) {
			$accumulated_time +=  $row->duration;
			if (($violation_type == 1) || ($violation_type == 4) || ($violation_type == 2)) {
				if ($row->duration != 0) {
					$accumulated_violations[$accumulated_count] = [
						'user_dtr_vio_id' =>  $row->userDtrViolation_ID,
						'sched_date' =>  $row->sched_date,
						'occurence' =>  $row->occurence,
						'duration' =>  $row->duration,
					];
					$accumulated_count++;
				}
			} else {
				$accumulated_violations[$accumulated_count] = [
					'user_dtr_vio_id' =>  $row->userDtrViolation_ID,
					'sched_date' =>  $row->sched_date,
					'occurence' =>  $row->occurence,
					'duration' =>  $row->duration,
				];
				$accumulated_count++;
			}
		}
		$occurence = count($fetch_accumulated_obj);
		$data['details'] =  $accumulated_violations;

		if (($violation_type == 1) || ($violation_type == 4) || ($violation_type == 2)) {
			$data['accumulated'] =  $accumulated_time;
		} else {
			$data['accumulated'] =  $occurence+1; // pLus one to include the current dtr violation
		}
		return  $data;
	}

	protected function add_user_dtr_violation_related_details($user_dtr_violation_related_details)
	{
		return $this->general_model->batch_insert($user_dtr_violation_related_details, 'tbl_dms_user_dtr_violation_related_details');
	}

	protected function get_user_dtr_violation_by_range($from, $to, $user_id, $dtr_vio_type, $user_dtr_id, $sched_date)
	{
		$fields = "userDtrViolation_ID, sched_date, occurence, duration";
		$where = "DATE(sched_date) >= '$from' AND DATE(sched_date) <= '$to' AND DATE(sched_date) <= '.$sched_date.' AND user_ID = $user_id AND dtrViolationType_ID = $dtr_vio_type AND status_ID = 0 AND userDtrViolation_ID != $user_dtr_id";
		$table = "tbl_user_dtr_violation";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'sched_date ASC');
	}

	protected function check_one_time_rules($user_dtr_violation_obj,  $violation_rules_obj,  $violation_details_obj)
	{
		$data['error'] = 0;
		$data['error_details'] = "";
		$data['valid'] = 0;
		if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //late
		{
			if ($violation_details_obj['violation_details']['duration'] >=  $violation_rules_obj->oneTimeVal) {
				$data['valid'] = 1;
			}
		} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) //UT
		{
			if ($violation_details_obj['violation_details']['duration'] >=  $violation_rules_obj->oneTimeVal) {
				$data['valid'] = 1;
			}
		} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) // forgot to logout
		{
			if ($violation_details_obj['violation_details']['duration'] >=  $violation_rules_obj->oneTimeVal) {
				$data['valid'] = 1;
			}
		} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
		{
			if ($violation_details_obj['violation_details']['over_break'] >=  $violation_rules_obj->oneTimeVal) {
				$data['valid'] = 1;
			}
		}
		return  $data;
	}

	public function check_if_consec_rules_covered($user_dtr_violation_obj,  $violation_rules_obj,  $first_date)
	{
		$unit =  $violation_rules_obj->durationUnit;
		$presc_period =  $violation_rules_obj->durationVal;
		$first_date_coverage = strtotime($first_date);
		$covered = 0;
		if ($violation_rules_obj->durationType == 1) {
			$fixed_rule =  $presc_period - 1;
			$start_end_coverage =  $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date,  $fixed_rule,  $unit);
			$consec_coverage_from = strtotime($start_end_coverage['start']);
			$consec_coverage_to = strtotime($start_end_coverage['end']);
			if (($consec_coverage_from <=  $first_date_coverage) && ($consec_coverage_to >=  $first_date_coverage)) {
				$covered = 1;
			} else {
				$covered = 0;
			}
		} else if ($violation_rules_obj->durationType == 2) {
			$custom_rule =  $presc_period;
			$consec_coverage_from = strtotime($user_dtr_violation_obj->sched_date . ' -' .  $custom_rule . ' days');
			$consec_coverage_to = strtotime($user_dtr_violation_obj->sched_date);
			if (($consec_coverage_from <=  $first_date_coverage) && ($consec_coverage_to >=  $first_date_coverage)) {
				$covered = 1;
			} else {
				$covered = 0;
			}
		} 
		// else if ($violation_rules_obj->durationType == 3) {
		// 	$category_settings = $this->get_current_disciplinary_action_category_settings();
		// 	$prev_categ = $violation_rules_obj->category_ID; //get categ from prev valid IR
		// 	$prev_level = 1; //get level from prev valid IR
		// 	$disc_action_details = $this->get_disciplinary_action_details_prev_occurence($prev_categ, $prev_level, $category_settings->disciplinaryActionCategorySettings_ID);
		// 	$cod_presc_period = $disc_action_details->periodMonth - 1;
		// 	$start_end_coverage = $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date, $cod_presc_period, 'month');
		// 	$consec_coverage_to = strtotime($start_end_coverage['end']);
		// 	$consec_coverage_from = strtotime($start_end_coverage['start']);
		// 	if (($consec_coverage_from <=  $first_date_coverage) && ($consec_coverage_to >=  $first_date_coverage)) {
		// 		$covered = 1;
		// 	} else {
		// 		$covered = 0;
		// 	}
		// }
		return  $covered;
	}

	protected function check_if_consecutive_dates($dtr_consecutive_dates)
	{
		$consec = 1;
		$previous = new DateTime($dtr_consecutive_dates[0]->sched_date); // Set the "previous" value
		unset($dtr_consecutive_dates[0]); // Unset the value we just set to  $previous, so we don't loop it twice
		foreach ($dtr_consecutive_dates as  $row) { // Loop the object
			$current = new DateTime($row->sched_date);
			$diff =  $current->diff($previous);
			// If the difference is exactly 1 day, it's continuous 
			if ($diff->days == 1) {
				$previous = new DateTime($row->sched_date);
			} else {
				$consec = 0;
			}
		}
		return  $consec;
	}

	public function consecutive_test(){
		$sched_date = "2019-08-01";
		$days =  3 - 1;
		$to =  $sched_date;
		$from = Date("Y-m-d ", strtotime($sched_date . '-' .  $days . ' days'));
		$first_date_coverage = strtotime($from);
		// var_dump($from);
		// var_dump($to);
		$fixed_rule =  1 - 1;
		$start_end_coverage =  $this->get_start_and_end_date_coverage($sched_date,  $fixed_rule,  "month");
		$consec_coverage_from = strtotime($start_end_coverage['start']);
		$consec_coverage_to = strtotime($start_end_coverage['end']);
		// var_dump($start_end_coverage['start']);
		// var_dump($start_end_coverage['end']);
		if (($consec_coverage_from <=  $first_date_coverage) && ($consec_coverage_to >=  $first_date_coverage)) {
			$covered = 1;
		} else {
			$covered = 0;
		}
		echo $covered;
		// $day =  $violation_rules_obj->consecutiveVal;
	}

	public function check_consecutive_rules($user_dtr_violation_obj, $violation_rules_obj)
	{
		$data['error'] = 0;
		$data['error_details'] = "";
		$data['valid'] = 0;
		$days =  $violation_rules_obj->consecutiveVal - 1;
		$to =  $user_dtr_violation_obj->sched_date;
		$from = Date("Y-m-d ", strtotime($user_dtr_violation_obj->sched_date . '-' .  $days . ' days'));

		$day =  $violation_rules_obj->consecutiveVal;

		$dtr_consecutive_dates =  $this->get_user_dtr_violation_by_range($from,  $to,  $user_dtr_violation_obj->user_ID,  $user_dtr_violation_obj->dtrViolationType_ID,  $user_dtr_violation_obj->userDtrViolation_ID, $user_dtr_violation_obj->sched_date);
		if (count($dtr_consecutive_dates) + 1 >= (int) $day) {
			$consec =  $this->check_if_consecutive_dates($dtr_consecutive_dates);
			if ($consec) {
				$covered =  $this->check_if_consec_rules_covered($user_dtr_violation_obj,  $violation_rules_obj,  $from);
				if ($covered) {
					$consec_count = 0;
					foreach ($dtr_consecutive_dates as  $rows) {
						$consec_dates[$consec_count] = [
							'userDtrViolation_ID' =>  $user_dtr_violation_obj->userDtrViolation_ID,
							'relatedUserDtrViolation_ID' =>  $rows->userDtrViolation_ID,
						];
						$user_data['status_ID'] = 3;
						$qualify['user_data_violation_stat'] =  $this->update_user_dtr_violation($rows->userDtrViolation_ID,  $user_data);
						$consec_count++;
					}
					$data['add_user_dtr_violation_related_details'] =  $this->add_user_dtr_violation_related_details($consec_dates);
					$data['valid'] = 1;
					$data['violation_details'] =  $dtr_consecutive_dates;
				} else {
					$data['violation_details'] = "beyond coverage";
				}
			} else {
				$data['violation_details'] = "dates not consecutive";
			}
		} else {
			$data['valid'] = 0;
			$data['violation_details'] = "count does not qualify";
		}
		return  $data;
	}

	public function check_non_consecutive_rules($user_dtr_violation_obj,  $violation_rules_obj)
	{
		$data['error'] = 0;
		$data['error_details'] = "";
		$data['valid'] = 0;
		$unit =  $violation_rules_obj->durationUnit;
		$presc_period =  $violation_rules_obj->durationVal;
		$covered = 0;
		if ($violation_rules_obj->durationType == 1) {
			// FIXED
			$fixed_rule =  $presc_period - 1;
			$start_end_coverage =  $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date,  $fixed_rule, 'month');
			$coverage_to =  $start_end_coverage['end'];
			$coverage_from =  $start_end_coverage['start'];
		} else if ($violation_rules_obj->durationType == 2) {
			// CUSTOM
			$custom_rule =  $presc_period;
			$coverage_from = date("Y-m-d", strtotime($user_dtr_violation_obj->sched_date . '-' .  $custom_rule . ' days'));
			$coverage_to =  $user_dtr_violation_obj->sched_date;
		}
		// else if ($violation_rules_obj->durationType == 3) {
		// 	// COD
		// 	$category_settings = $this->get_current_disciplinary_action_category_settings();
		// 	$prev_categ = $violation_rules_obj->category_ID; //get categ from prev valid IR
		// 	$prev_level = 1; //get level from prev valid IR
		// 	$disc_action_details = $this->get_disciplinary_action_details_prev_occurence($prev_categ, $prev_level, $category_settings->disciplinaryActionCategorySettings_ID);
		// 	$cod_presc_period =  $disc_action_details->periodMonth - 1;
		// 	$start_end_coverage =  $this->get_start_and_end_date_coverage($user_dtr_violation_obj->sched_date,  $cod_presc_period, 'month');
		// 	$coverage_to =  $start_end_coverage['end'];
		// 	$coverage_from =  $start_end_coverage['start'];
		// }

		// if (($user_dtr_violation_obj->dtrViolationType_ID == 3) || ($user_dtr_violation_obj->dtrViolationType_ID == 6) || ($user_dtr_violation_obj->dtrViolationType_ID == 7)) { //remove this if inc break and inc dtr logs policy will be separated
		// 	$accumulated =  $this->get_user_dtr_violation_by_range_inc_logs($coverage_from,  $coverage_to,  $user_dtr_violation_obj->user_ID,  $user_dtr_violation_obj->userDtrViolation_ID);
		// 	$accumulated_count = count($accumulated) + 1;
		// 	if ($accumulated_count >=  $violation_rules_obj->nonConsecutiveVal) {
		// 		if ($accumulated_count > 1) {
		// 			$non_consec_count = 0;
		// 			$compute_accumulated =  $this->compute_accumulated($user_dtr_violation_obj->dtrViolationType_ID,  $accumulated,  $violation_rules_obj);
		// 			if ($compute_accumulated['accumulated'] + 1 >=  $violation_rules_obj->nonConsecutiveVal) {
		// 				foreach ($compute_accumulated['details'] as  $rows) {
		// 					$non_consec_dates[$non_consec_count] = [
		// 						'userDtrViolation_ID' =>  $user_dtr_violation_obj->userDtrViolation_ID,
		// 						'relatedUserDtrViolation_ID' =>  $rows['user_dtr_vio_id'],
		// 					];
		// 					$non_consec_count++;
		// 				}
		// 				$data['add_user_dtr_violation_related_details'] =  $this->add_user_dtr_violation_related_details($non_consec_dates);
		// 			}
		// 		}
		// 		$data['valid'] = 1;
		// 		$data['violation_details'] =  $accumulated_count;
		// 	}
		// } else {
			$accumulated =  $this->get_user_dtr_violation_by_range($coverage_from,  $coverage_to,  $user_dtr_violation_obj->user_ID,  $user_dtr_violation_obj->dtrViolationType_ID,  $user_dtr_violation_obj->userDtrViolation_ID, $user_dtr_violation_obj->sched_date);
			// var_dump($accumulated);
			if (count($accumulated) > 0) {
				$compute_accumulated =  $this->compute_accumulated($user_dtr_violation_obj->dtrViolationType_ID,  $accumulated,  $violation_rules_obj);
				if ($compute_accumulated['accumulated'] >=  $violation_rules_obj->nonConsecutiveVal) {
					$non_consec_count = 0;

					foreach ($compute_accumulated['details'] as  $rows) {
						$non_consec_dates[$non_consec_count] = [
							'userDtrViolation_ID' =>  $user_dtr_violation_obj->userDtrViolation_ID,
							'relatedUserDtrViolation_ID' =>  $rows['user_dtr_vio_id'],
						];
						$non_consec_count++;
					}
					$data['add_user_dtr_violation_related_details'] =  $this->add_user_dtr_violation_related_details($non_consec_dates);
					$data['valid'] = 1;
					$data['violation_details'] =  $compute_accumulated;
				}
			} else {
				$data['violation_details'] = "no record found";
			}
		// }
		return  $data;
	}


	protected function check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules)
	{
		$qualified  = 0;
		$qualify['valid'] = 0;
		$qualify['error'] = 0;
		$qualify['occurence_rule_num'] = 0;
		$qualify['occurence_value'] = 0;
		if (count($violation_rules) > 0) {
			if ((int) $violation_rules->oneTimeStat) {
				// one time
				if ($qualified == 0) {
					$rule_checking_details = $this->check_one_time_rules($user_dtr_violation, $violation_rules, $violation_details);
					if ($rule_checking_details['error'] == 0) {
						$qualify['error'] = 0;
						if ($rule_checking_details['valid'] == 1) {
							$qualified = 1;
							$qualify['valid'] = 1;
							$qualify['occurence_rule_num'] = $qualified;
							$qualify['occurence_rule'] = "one time";
							$qualify['related_details'] = $violation_details['violation_details'];
							$qualify['occurence_value'] = $violation_rules->oneTimeVal;
						}
					} else {
						$qualify['error'] = 1;
						$qualify['error_details'] = $rule_checking_details['error_details'];
					}
				}
			}
			if ((int) $violation_rules->consecutiveStat) {
				if ($qualified == 0) {
					$rule_checking_details = $this->check_consecutive_rules($user_dtr_violation, $violation_rules);
					if ($rule_checking_details['valid'] == 1) {
						$qualified = 2;
						$qualify['valid'] = 1;
						$qualify['occurence_rule_num'] = $qualified;
						$qualify['occurence_rule'] = "consecutive";
						$qualify['related_details'] = $violation_details['violation_details'];
						$qualify['supporting_details'] = $rule_checking_details['violation_details'];
						$qualify['occurence_value'] = $violation_rules->consecutiveVal;
					}
				}
			}
			if ((int) $violation_rules->nonConsecutiveStat) {
				if ($qualified == 0) {
					$rule_checking_details = $this->check_non_consecutive_rules($user_dtr_violation, $violation_rules);
					if ($rule_checking_details['valid'] == 1) {
						$qualified = 3;
						$qualify['valid'] = 1;
						$qualify['occurence_rule_num'] = $qualified;
						$qualify['occurence_rule'] = "non consecutive";
						$qualify['related_details'] = $violation_details['violation_details'];
						$qualify['supporting_details'] = $rule_checking_details['violation_details'];
						$qualify['occurence_value'] = $violation_rules->nonConsecutiveVal;
					}
				}
			}
		} else {
			$qualify['error'] = 1;
			$qualify['error_details'] = "no violation rules set";
		}
		return $qualify;
	}

	protected function update_user_dtr_violation($user_dtr_vio_id,  $data)
	{
		$where = "userDtrViolation_ID =  $user_dtr_vio_id";
		return  $this->general_model->update_vals($data,  $where, 'tbl_user_dtr_violation');
	}

	protected function update_qualified_dtr_violation($user_dtr_vio_id,  $data)
	{
		$where = "userDtrViolation_ID =  $user_dtr_vio_id";
		return  $this->general_model->update_vals($data,  $where, 'tbl_dms_qualified_user_dtr_violation');
	}
	protected function update_qualified_dtr_violation_sup_notif($user_dtr_vio_id,  $data)
	{
		$qualified_id = $this->get_qualified_dtr_violation_via_violation($user_violation_id);
		if(count($qualified_id) > 0){
			$where = "qualifiedUserDtrViolation_ID = $qualified_id ";
			return  $this->general_model->update_vals($data,  $where, 'tbl_dms_supervisor_qualified_user_dtr_violation_notification');
		}
	}

	protected function update_dms_deadline_with_type($record_id, $emp_id, $dead_type, $data)
	{
		$where = "record_ID = $record_id AND emp_id = $emp_id AND dmsDeadlineType_ID = $dead_type";
		return  $this->general_model->update_vals($data,  $where, 'tbl_dms_deadline');
	}

	protected function get_shedule_shift1($sched_id, $user_id)
	{
		$fields = "schedType.schedtype_id as schedTypeId, type, sched.sched_id as schedId, sched.sched_date schedDate, users.emp_id empId, users.uid userId, time_start, time_end, bta_id, accTime.acc_id as accountId";
		$where = "schedType.schedtype_id = sched.schedtype_id AND accTime.acc_time_id = sched.acc_time_id AND times.time_id = accTime.time_id AND sched.sched_id = $sched_id AND sched.emp_id = users.emp_id AND users.uid = $user_id";
		$tables = "tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users";
		return $this->general_model->fetch_specific_val($fields, $where, $tables);
	}
	protected function get_shedule_shift($sched_id, $user_id)
	{
		$fields = "schedType.schedtype_id as schedTypeId, type, sched.sched_id as schedId, sched.sched_date schedDate, users.emp_id empId, users.uid userId, time_start, time_end, bta_id, accTime.acc_id as accountId";
		$where = "schedType.schedtype_id = sched.schedtype_id AND accTime.acc_time_id = sched.acc_time_id AND times.time_id = accTime.time_id AND sched.sched_id = $sched_id AND sched.emp_id = users.emp_id AND users.uid = $user_id";
		$tables = "tbl_dms_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users";
		return $this->general_model->fetch_specific_val($fields, $where, $tables);
	}
	protected function get_shedule_shift2($sched_id, $user_id)
	{
		$fields = "schedType.schedtype_id as schedTypeId, type, sched.sched_id as schedId, sched.sched_date schedDate, users.emp_id empId, users.uid userId, time_start, time_end, bta_id, accTime.acc_id as accountId";
		$where = "schedType.schedtype_id = sched.schedtype_id AND accTime.acc_time_id = sched.acc_time_id AND times.time_id = accTime.time_id AND sched.sched_id = $sched_id AND sched.emp_id = users.emp_id AND users.uid = $user_id";
		$tables = "tbl_dms_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users";
		return $this->general_model->fetch_specific_val($fields, $where, $tables);
	}

	protected function get_user_dtr_log($sched_id, $entry_type)
	{
		$fields = "dtr_id, emp_id, acc_time_id, entry, log, note, type, sched_id";
		$where = "sched_id = $sched_id AND type='DTR' AND entry = '$entry_type'";
		return $record = $this->general_model->fetch_specific_val($fields, $where, "tbl_dtr_logs");
	}

	protected function add_user_dtr_violation_details($user_dtr_violation_details)
	{
		return $this->general_model->batch_insert($user_dtr_violation_details, 'tbl_dms_user_dtr_violation_details');
	}

	protected function new_format_midnight_shift_out($formatted_date)
	{
		$new_format_date = $formatted_date;
		if ($formatted_date == Date("Y-m-d", strtotime($formatted_date)) . " 00:00") {
			$new_format_date = Date("Y-m-d", strtotime($formatted_date . ' -1 days')) . " 24:00";
		}
		return $new_format_date;
	}

	protected function add_user_dtr_violation_allowable_break($allowable_break_details)
	{
		return $this->general_model->batch_insert($allowable_break_details, 'tbl_dms_user_dtr_violation_allowable_break');
	}

	protected function get_violation_related_details($user_dtr_violation_obj)
	{
		$data['error'] = 0;
		$data['error_details'] = "";
		$data['valid'] = 0;
		$violation_details = "";
		$sched_details = $this->qry_sched_details($user_dtr_violation_obj->sched_ID);
		// var_dump($sched_details);
		if(count($sched_details) > 0){
			$this->record_orig_sched($sched_details);
		}
		$sched = $this->get_shedule_shift((int) $user_dtr_violation_obj->sched_ID, (int) $user_dtr_violation_obj->user_ID);
		if (count($sched) > 0) {
			if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //LATE
			{
				// DTR logs
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$login = new DateTime($login_raw->log);
				$login_formatted = $login->format("Y-m-d H:i");
				if (count($login_raw) > 0) {
					$login_str = strtotime($login_formatted);
					// DATED SCHED
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
					$new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
					$shift_start_str = strtotime($new_formatted_shift_start);
					// Compute late duration
					$duration = $this->get_human_time_format($shift_start_str, $login_str, 0);
					$hrs = $duration['hoursTotal'];
					$hrsToMin = $duration['hoursTotal'] * 60;
					$shift_start_end = $sched->time_start . " - " . $sched->time_end;
					if ($login_str > $shift_start_str) {
						$data['valid'] = 1;
						$violation_details = [
							"sched_date" => $sched->schedDate,
							"shift" => $shift_start_end,
							"in_id" => $login_raw->dtr_id,
							"in_log" => $login_formatted,
							"shift_in" => $new_formatted_shift_start,
							"duration" => $hrsToMin,
							"human_time_format" => $duration
						];
					} else {
						$data['valid'] = 0;
					}
					$user_dtr_violation_details[0] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $login_raw->dtr_id,
						'log' => $login_formatted,
						'type' => "DTR",
						'entry' => "I",
						'minDuration' => 0
					];
					$data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
					$data['violation_details'] = $violation_details;
					$user_data_vio['duration'] = $hrsToMin;
					$user_data_vio['shift'] = $shift_start_end;
					$user_data_vio['sched_date'] = $sched->schedDate;
					$data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
				} else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "No login",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for late details'
					];
				}
			} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) // UT
			{
				// DTR logs
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');

				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					$login = new DateTime($login_raw->log);
					$login_formatted = $login->format("Y-m-d H:i");
					$logout = new DateTime($logout_raw->log);
					$logout_formatted = $logout->format("Y-m-d H:i");

					$login_str = strtotime($login_formatted);
					$logout_str = strtotime($logout_formatted);
					// DATED SCHED
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
					$new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);

					$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
					$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
					$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
					$new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);
					$shift_out_str = strtotime($new_formatted_shift_end);
					$duration = $this->get_human_time_format($logout_str, $shift_out_str, 0);
					$hrs = $duration['hoursTotal'];
					$hrsToMin = $duration['hoursTotal'] * 60;

					if ($shift_out_str > $logout_str) {
						$data['valid'] = 1;
						$shift_start_end = $sched->time_start . " - " . $sched->time_end;
						$violation_details = [
							"sched_date" => $sched->schedDate,
							"shift" => $shift_start_end,
							"out_id" => $logout_raw->dtr_id,
							"out_log" => $logout_formatted,
							"shif_out" => $formatted_shift_end,
							"duration" => $hrsToMin,
							"human_time_format" => $duration
						];
					} else {
						$data['valid'] = 0;
					}
					$user_dtr_violation_details[0] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $logout_raw->dtr_id,
						'log' => $logout_formatted,
						'type' => "DTR",
						'entry' => "O",
						'minDuration' => $hrsToMin
					];
					$data['violation_details'] = $violation_details;
					$user_data_vio['duration'] = $hrsToMin;
					$user_data_vio['shift'] = $shift_start_end;
					$user_data_vio['sched_date'] = $sched->schedDate;
					$data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
					$data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
				} else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "Incomplete Log",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for forgot to undertime details'
					];
				}
			} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) //FORGOT TO LOGOUT
			{
				// DTR logs
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');

				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					$login = new DateTime($login_raw->log);
					$login_formatted = $login->format("Y-m-d H:i");
					$logout = new DateTime($logout_raw->log);
					$logout_formatted = $logout->format("Y-m-d H:i");
					$login_str = strtotime($login_formatted);
					$logout_str = strtotime($logout_formatted);

					// DATED SCHED
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");

					$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
					$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
					$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
					$new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);
					$shift_out_str = strtotime($new_formatted_shift_end);

					$duration = $this->get_human_time_format($shift_out_str, $logout_str, 0);
					$hrsToMin = $duration['hoursTotal'] * 60;
					$shift_start_end = $sched->time_start . " - " . $sched->time_end;
					if ($logout_str > $shift_out_str) {
						$data['valid'] = 1;
						$violation_details = [
							"sched_date" => $sched->schedDate,
							"shift" => $shift_start_end,
							"out_id" => $logout_raw->dtr_id,
							"out_log" => $logout_formatted,
							"shift_out" => $formatted_shift_end,
							"duration" => $hrsToMin,
							"human_time_format" => $duration
						];
					}
					$user_dtr_violation_details[0] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $logout_raw->dtr_id,
						'log' => $logout_formatted,
						'type' => "DTR",
						'entry' => "O",
						'minDuration' => $hrsToMin
					];
					$data['violation_details'] = $violation_details;
					$user_data_vio['duration'] = $hrsToMin;
					$user_data_vio['shift'] = $shift_start_end;
					$user_data_vio['sched_date'] = $sched->schedDate;
					$data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
					$data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
				} else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "Incomplete Log",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for forgot to logout details'
					];
				}
			} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
			{
				// DTR logs
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
				$login = new DateTime($login_raw->log);
				$login_formatted = $login->format("Y-m-d H:i");
				$break_out_logs =[];
				$break_in_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
				$login_str = strtotime($login_formatted);
				$shift_start_end = $sched->time_start . " - " . $sched->time_end;
				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					$logout = new DateTime($logout_raw->log);
					$logout_formatted = $logout->format("Y-m-d H:i");
					$logout_str = strtotime($logout_formatted);
					$break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
					$break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');
					$user_dtr_violation_details[1] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $logout_raw->dtr_id,
						'log' => $logout_formatted,
						'type' => "DTR",
						'entry' => "O",
						'minDuration' => 0
					];
				}else if(count($logout_raw) < 1){
					$next_dtr_log = $this->get_next_dtr_log($login_raw->dtr_id, $sched->empId);
					if($next_dtr_log == NULL){
						$break_out_logs = $this->get_breaks($login_raw->dtr_id, $login_raw->dtr_id*1000, $sched->empId, 'O');
						$break_in_logs = $this->get_breaks($login_raw->dtr_id, $login_raw->dtr_id*1000, $sched->empId, 'I');
					}else{
						$break_out_logs = $this->get_breaks($login_raw->dtr_id, $next_dtr_log[0]->dtr_id, $sched->empId, 'O');
						$break_in_logs = $this->get_breaks($login_raw->dtr_id, $next_dtr_log[0]->dtr_id, $sched->empId, 'I');
					}
				}
				// var_dump($break_out_logs);
				// var_dump($break_in_logs);
				if (count($break_out_logs) > 0 && count($break_in_logs) > 0) {
					// DATED SCHED
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
					$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
					$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
					$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
					$new_formatted_shift_end = $this->new_format_midnight_shift_out($new_shifts['end']);
					$allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
					$emp_account_details = $this->get_emp_account_details($sched->empId);
					if ($allowable_breaks['breakData'] > 0) {
						// GET BREAK LOGS
						$hours = 0;
						$minutes = 0;
						$allow_count = 0;
						foreach ($allowable_breaks['breakData'] as $rows) {
							$hours = $rows->hour;
							$minutes = $rows->min;
							$hours_to_min = $hours * 60;
							$minutes += $hours_to_min;
							$dtr_vio_allowable_break[$allow_count] = [
								"userDtrViolation_ID" => $user_dtr_violation_obj->userDtrViolation_ID,
								"breakType" => $rows->break_type,
								"duration" => $minutes
							];
							$allow_count++;
						}
						$data['user_dtr_violation_allowable_break'] = $this->add_user_dtr_violation_allowable_break($dtr_vio_allowable_break);
						// $break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
						// $break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');
						
						$shift_start_end = $sched->time_start . " - " . $sched->time_end;
						$user_dtr_violation_details[0] = [
							'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
							'shift' => $shift_start_end,
							'dtr_id' => $login_raw->dtr_id,
							'log' => $login_formatted,
							'type' => "DTR",
							'entry' => "I",
							'minDuration' => 0
						];
						
						// BREAK COVERED
						$covered_breaks = [];
						$covered_breaks_count = 0;
						$total_covered_break = 0;
						$ob = 0;
						$user_dtr_vio_details_count = 2;
						if (count($break_out_logs) > 0) {
							foreach ($break_out_logs as $break_out_row) {
								$searchedValue = $break_out_row->dtr_id;
								$neededObject = array_merge(array_filter(
									$break_in_logs,
									function ($e) use ($searchedValue) {
										return $e->note == $searchedValue;
									}
								));
								$break_out_log = new DateTime($break_out_row->log);
								$formatted_break_out_log = $break_out_log->format("Y-m-d H:i:s");

								$user_dtr_violation_details[$user_dtr_vio_details_count] = [
									'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
									'shift' => $shift_start_end,
									'dtr_id' => $break_out_row->dtr_id,
									'log' => $formatted_break_out_log,
									'type' => $break_out_row->break_type,
									'entry' => "O",
									'minDuration' => 0
								];
								$user_dtr_vio_details_count++;
								if (count($neededObject[0]) > 0) {
									$break_in_log = new DateTime($neededObject[0]->log);
									$formatted_break_in_log = $break_in_log->format("Y-m-d H:i:s");

									$break_out_str = strtotime($formatted_break_out_log);
									$break_in_str = strtotime($formatted_break_in_log);

									$duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
									$break_hrs_to_min = $duration['hoursTotal'] * 60;
									$total_covered_break += $break_hrs_to_min;

									$covered_breaks[$covered_breaks_count] = [
										"sched_date" => $sched->schedDate,
										"shift" => $shift_start_end,
										"break_type" => $break_out_row->break_type,
										"out_id" => $break_out_row->dtr_id,
										"out_log" => $formatted_break_out_log,
										"in_id" => $neededObject[0]->dtr_id,
										"in_log" => $formatted_break_in_log,
										"minutes" => $break_hrs_to_min,
										"human_time_format" => $duration
									];
									$user_dtr_violation_details[$user_dtr_vio_details_count] = [
										'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
										'shift' => $shift_start_end,
										'dtr_id' => $neededObject[0]->dtr_id,
										'log' => $formatted_break_in_log,
										'type' => $neededObject[0]->break_type,
										'entry' => "I",
										'minDuration' => $break_hrs_to_min
									];
									$user_dtr_vio_details_count++;
									$covered_breaks_count++;
								}
							}
							$data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
							if ((int)$total_covered_break > (int)$allowable_breaks['minutes']) {
								$ob = (int)$total_covered_break - (int)$allowable_breaks['minutes'];
								$data['valid'] = 1;
								$violation_details = [
									"total_covered" => (int)$total_covered_break,
									"break_coverage_info" => $covered_breaks,
									"allowable_break" => $allowable_breaks,
									"over_break" => $ob
								];
								$data['violation_details'] = $violation_details;
							}
							$user_data_vio['duration'] = $ob;
							$user_data_vio['shift'] = $shift_start_end;
							$user_data_vio['sched_date'] = $sched->schedDate;
							$data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
						} else {
							$data['error'] = 1;
							$data['error_details'] = [
								'error' => "Incomplete Break logs",
								'origin' => 'Related Violation details function - (get_violation_related_details)',
								'process' => 'getting out for break logs'
							];
						}
					} else {
						$data['error'] = 1;
						$data['error_details'] = [
							'error' => "Assigned Breaks not found",
							'origin' => 'Related Violation details function - (get_violation_related_details)',
							'process' => 'getting of allowable break details'
						];
					}
				}else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "Incomplete Log",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for over break details'
					];
				}
			} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 6) // INC BREAK
			{
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					// DATED SCHED
					$login = new DateTime($login_raw->log);
					$login_formatted = $login->format("Y-m-d H:i");
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
					$logout = new DateTime($logout_raw->log);
					$logout_formatted = $logout->format("Y-m-d H:i");
					$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
					$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
					$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
					$new_formatted_shift_end = $this->new_format_midnight_shift_out($new_shifts['end']);

					$allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
					$required_break_logs = count($allowable_breaks['breakData']) * 2;
					$emp_account_details = $this->get_emp_account_details($sched->empId);
					$shift_start_end = $sched->time_start . " - " . $sched->time_end;

					$user_dtr_violation_details[0] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $login_raw->dtr_id,
						'log' => $login_formatted,
						'type' => "DTR",
						'entry' => "I",
						'minDuration' => 0
					];
					$user_dtr_violation_details[1] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $logout_raw->dtr_id,
						'log' => $logout_formatted,
						'type' => "DTR",
						'entry' => "O",
						'minDuration' => 0
					];
					$user_dtr_vio_details_count = 2;

					if (count($allowable_breaks['breakData']) > 0) {
						// GET BREAK LOGS
						$hours = 0;
						$minutes = 0;
						$allow_count = 0;
						foreach ($allowable_breaks['breakData'] as $rows) {
							$hours = $rows->hour;
							$minutes = $rows->min;
							$hours_to_min = $hours * 60;
							$minutes += $hours_to_min;
							$dtr_vio_allowable_break[$allow_count] = [
								"userDtrViolation_ID" => $user_dtr_violation_obj->userDtrViolation_ID,
								"breakType" => $rows->break_type,
								"duration" => $minutes
							];
							$allow_count++;
						}
						$data['user_dtr_violation_allowable_break'] = $this->add_user_dtr_violation_allowable_break($dtr_vio_allowable_break);
						$break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
						$break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');

						if (count($break_out_logs) > 0) {
							foreach ($break_out_logs as $break_out_row) {
								$searchedValue = $break_out_row->dtr_id;
								$neededObject = array_merge(array_filter(
									$break_in_logs,
									function ($e) use ($searchedValue) {
										return $e->note == $searchedValue;
									}
								));
								$break_out_log = new DateTime($break_out_row->log);
								$formatted_break_out_log = $break_out_log->format("Y-m-d H:i");

								$user_dtr_violation_details[$user_dtr_vio_details_count] = [
									'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
									'shift' => $shift_start_end,
									'dtr_id' => $break_out_row->dtr_id,
									'log' => $formatted_break_out_log,
									'type' => $break_out_row->break_type,
									'entry' => $break_out_row->entry,
									'minDuration' => 0
								];
								$user_dtr_vio_details_count++;
								if (count($neededObject) > 0) {
									$break_in_log = new DateTime($neededObject[0]->log);
									$formatted_break_in_log = $break_in_log->format("Y-m-d H:i");

									$break_out_str = strtotime($formatted_break_out_log);
									$break_in_str = strtotime($formatted_break_in_log);

									$duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
									$break_hrs_to_min = $duration['hoursTotal'] * 60;

									$user_dtr_violation_details[$user_dtr_vio_details_count] = [
										'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
										'shift' => $shift_start_end,
										'dtr_id' => $neededObject[0]->dtr_id,
										'log' => $formatted_break_in_log,
										'type' => $neededObject[0]->break_type,
										'entry' => "I",
										'minDuration' => $break_hrs_to_min
									];
									$user_dtr_vio_details_count++;
								}
							}
						} else {
							$data['error'] = 1;
							$data['error_details'] = [
								'error' => "Incomplete Break logs",
								'origin' => 'Related Violation details function - (get_violation_related_details)',
								'process' => 'getting out for break logs'
							];
						}
						$user_data_vio['shift'] = $shift_start_end;
						$user_data_vio['sched_date'] = $sched->schedDate;
						$data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
						$recorded_break_logs = count($break_out_logs) + count($break_in_logs);
						if ($recorded_break_logs != $required_break_logs) {
							$data['valid'] = 1;
							$data['violation_details'] = [
								'allowable_break' => $allowable_breaks,
								'break_out_logs' => $break_out_logs,
								'break_in_logs' => $break_in_logs
							];
						}
					} else {
						$data['error'] = 1;
						$data['error_details'] = [
							'error' => "Assigned Breaks not found",
							'origin' => 'Related Violation details function - (get_violation_related_details)',
							'process' => 'getting of allowable break details'
						];
					}
					$data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
				} else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "Incomplete Log",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for over break details'
					];
				}
			} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 7) //INC DTR Logs
			{
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
				$shift_start_end = $sched->time_start . " - " . $sched->time_end;
				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					// "COMPLETE DTR LOGS";
				} else {
					if (count($login_raw) > 0) {
						$login = new DateTime($login_raw->log);
						$login_formatted = $login->format("Y-m-d H:i");
						$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
						$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
						$new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
						// $shift_start_end = $sched->time_start." - ".$sched->time_end;

						$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
						$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
						$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
						$new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);

						$allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
						if ($allowable_breaks['breakData'] > 0) {
							$hours = 0;
							$minutes = 0;
							$allow_count = 0;
							foreach ($allowable_breaks['breakData'] as $rows) {
								$hours = $rows->hour;
								$minutes = $rows->min;
								$hours_to_min = $hours * 60;
								$minutes += $hours_to_min;
								$dtr_vio_allowable_break[$allow_count] = [
									"userDtrViolation_ID" => $user_dtr_violation_obj->userDtrViolation_ID,
									"breakType" => $rows->break_type,
									"duration" => $minutes
								];
								$allow_count++;
							}
							$data['user_dtr_violation_allowable_break'] = $this->add_user_dtr_violation_allowable_break($dtr_vio_allowable_break);
							$shift_start_end = $sched->time_start . " - " . $sched->time_end;
							$user_dtr_violation_details[0] = [
								'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
								'shift' => $shift_start_end,
								'dtr_id' => $login_raw->dtr_id,
								'log' => $login_formatted,
								'type' => "DTR",
								'entry' => "I",
								'minDuration' => 0
							];
							$user_dtr_vio_count = 1;
							// BREAK lOGS WITHOUT Logout;
							$break_logs_without_logout = $this->get_breaks_without_logout($login_raw->dtr_id, $new_formatted_shift_end, $sched->empId);
							foreach ($break_logs_without_logout  as $break_logs_without_logout) {
								$user_dtr_violation_details[$user_dtr_vio_count] = [
									'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
									'shift' => $shift_start_end,
									'dtr_id' => $break_logs_without_logout->dtr_id,
									'log' => $break_logs_without_logout->log,
									'type' => $break_logs_without_logout->break_type,
									'entry' => $break_logs_without_logout->entry,
									'minDuration' => 0
								];
								$user_dtr_vio_count++;
							}
							$user_data_vio['shift'] = $shift_start_end;
							$user_data_vio['sched_date'] = $sched->schedDate;
							$data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
							$data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
						} else {
							$data['error'] = 1;
							$data['error_details'] = [
								'error' => "Assigned Breaks not found",
								'origin' => 'Related Violation details function - (get_violation_related_details)',
								'process' => 'getting of allowable break details'
							];
						}
						$data['valid'] = 1;
						$violation_details = [
							"sched_date" => $sched->schedDate,
							"shift" => $shift_start_end,
							"in_id" => $login_raw->dtr_id,
							"in_log" => $login_formatted,
							"shift_in" => $new_formatted_shift_start,
						];
						$data['violation_details'] = $violation_details;
					} else {
						$data['error'] = 1;
						$data['error_details'] = [
							'error' => "Login log not found",
							'origin' => 'Related Violation details function - (get_violation_related_details)',
							'process' => 'getting of log details'
						];
					}
				}
			} else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 5) // ABSENT
			{
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
				$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
				$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
				$new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
				$shift_start_end = $sched->time_start . " - " . $sched->time_end;
				if (count($login_raw) == 0 && count($logout_raw) == 0) {
					// "COMPLETE DTR LOGS";
					$data['valid'] = 1;
					$violation_details = [
						"sched_date" => $sched->schedDate,
						"shift" => $shift_start_end,
					];
					$data['violation_details'] = $violation_details;
				}
				$user_data_vio['shift'] = $shift_start_end;
				$user_data_vio['sched_date'] = $sched->schedDate;
				$data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
			}
		} else {
			$data['error'] = 1;
			$data['error_details'] = [
				'error' => "No Sched found",
				'origin' => 'Related Violation details function - (get_violation_related_details)',
				'process' => 'getting of schedule'
			];
		}
		return $data;
	}

	private function check_if_sched_recorded($sched_id, $emp_id){
		$fields = "*";
		$where = "sched_id = $sched_id AND emp_id = $emp_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_schedule");
	}

	private function add_dms_sched($sched_details){
		$data['sched_id'] = $sched_details->sched_id;
		$data['emp_id'] = $sched_details->emp_id;
		$data['sched_date'] = $sched_details->sched_date;
		$data['schedtype_id'] = $sched_details->schedtype_id;
		$data['acc_time_id'] = $sched_details->acc_time_id;
		$data['updatedBy'] = $sched_details->updated_by;
		return $this->general_model->insert_vals($data, "tbl_dms_schedule");
	}

	private function record_orig_sched($sched_details){
		$check_recorded = $this->check_if_sched_recorded($sched_details->sched_id, $sched_details->emp_id);
		if(count($check_recorded) < 1){
			$this->add_dms_sched($sched_details);
		}
	}

	protected function get_violation_related_details2($user_dtr_violation_obj)
	{
		$data['error'] = 0;
		$data['error_details'] = "";
		$data['valid'] = 0;
		$violation_details = "";
		$sched_details = $this->qry_sched_details($user_dtr_violation_obj->sched_ID);
		// var_dump($sched_details);
		if(count($sched_details) > 0){
			$this->record_orig_sched($sched_details);
		}
		$sched = $this->get_shedule_shift2((int) $user_dtr_violation_obj->sched_ID, (int) $user_dtr_violation_obj->user_ID);
		// var_dump($sched);
		if (count($sched) > 0) {
			if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 1) //LATE
			{
				// DTR logs
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$login = new DateTime($login_raw->log);
				$login_formatted = $login->format("Y-m-d H:i");
				if (count($login_raw) > 0) {
					$login_str = strtotime($login_formatted);
					// DATED SCHED
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
					$new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
					$shift_start_str = strtotime($new_formatted_shift_start);
					// Compute late duration
					$duration = $this->get_human_time_format($shift_start_str, $login_str, 0);
					$hrs = $duration['hoursTotal'];
					$hrsToMin = $duration['hoursTotal'] * 60;
					$shift_start_end = $sched->time_start . " - " . $sched->time_end;
					if ($login_str > $shift_start_str) {
						$data['valid'] = 1;
						$violation_details = [
							"sched_date" => $sched->schedDate,
							"shift" => $shift_start_end,
							"in_id" => $login_raw->dtr_id,
							"in_log" => $login_formatted,
							"shift_in" => $new_formatted_shift_start,
							"duration" => $hrsToMin,
							"human_time_format" => $duration
						];
					} else {
						$data['valid'] = 0;
					}
					$user_dtr_violation_details[0] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $login_raw->dtr_id,
						'log' => $login_formatted,
						'type' => "DTR",
						'entry' => "I",
						'minDuration' => 0
					];
					// $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
					$data['violation_details'] = $violation_details;
					$user_data_vio['duration'] = $hrsToMin;
					$user_data_vio['shift'] = $shift_start_end;
					$user_data_vio['sched_date'] = $sched->schedDate;
					// $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
				} else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "No login",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for late details'
					];
				}
			} 
			else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 2) // UT
			{
				// DTR logs
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');

				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					$login = new DateTime($login_raw->log);
					$login_formatted = $login->format("Y-m-d H:i");
					$logout = new DateTime($logout_raw->log);
					$logout_formatted = $logout->format("Y-m-d H:i");

					$login_str = strtotime($login_formatted);
					$logout_str = strtotime($logout_formatted);
					// DATED SCHED
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
					$new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);

					$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
					$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
					$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
					$new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);
					$shift_out_str = strtotime($new_formatted_shift_end);
					$duration = $this->get_human_time_format($logout_str, $shift_out_str, 0);
					$hrs = $duration['hoursTotal'];
					$hrsToMin = $duration['hoursTotal'] * 60;

					if ($shift_out_str > $logout_str) {
						$data['valid'] = 1;
						$shift_start_end = $sched->time_start . " - " . $sched->time_end;
						$violation_details = [
							"sched_date" => $sched->schedDate,
							"shift" => $shift_start_end,
							"out_id" => $logout_raw->dtr_id,
							"out_log" => $logout_formatted,
							"shif_out" => $formatted_shift_end,
							"duration" => $hrsToMin,
							"human_time_format" => $duration
						];
					} else {
						$data['valid'] = 0;
					}
					$user_dtr_violation_details[0] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $logout_raw->dtr_id,
						'log' => $logout_formatted,
						'type' => "DTR",
						'entry' => "O",
						'minDuration' => $hrsToMin
					];
					$data['violation_details'] = $violation_details;
					$user_data_vio['duration'] = $hrsToMin;
					$user_data_vio['shift'] = $shift_start_end;
					$user_data_vio['sched_date'] = $sched->schedDate;
					// $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
					// $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
				} else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "Incomplete Log",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for forgot to undertime details'
					];
				}
			} 
			else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 3) //FORGOT TO LOGOUT
			{
				// DTR logs
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');

				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					$login = new DateTime($login_raw->log);
					$login_formatted = $login->format("Y-m-d H:i");
					$logout = new DateTime($logout_raw->log);
					$logout_formatted = $logout->format("Y-m-d H:i");
					$login_str = strtotime($login_formatted);
					$logout_str = strtotime($logout_formatted);

					// DATED SCHED
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");

					$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
					$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
					$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
					$new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);
					$shift_out_str = strtotime($new_formatted_shift_end);

					$duration = $this->get_human_time_format($shift_out_str, $logout_str, 0);
					$hrsToMin = $duration['hoursTotal'] * 60;
					$shift_start_end = $sched->time_start . " - " . $sched->time_end;
					if ($logout_str > $shift_out_str) {
						$data['valid'] = 1;
						$violation_details = [
							"sched_date" => $sched->schedDate,
							"shift" => $shift_start_end,
							"out_id" => $logout_raw->dtr_id,
							"out_log" => $logout_formatted,
							"shift_out" => $formatted_shift_end,
							"duration" => $hrsToMin,
							"human_time_format" => $duration
						];
					}
					$user_dtr_violation_details[0] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $logout_raw->dtr_id,
						'log' => $logout_formatted,
						'type' => "DTR",
						'entry' => "O",
						'minDuration' => $hrsToMin
					];
					$data['violation_details'] = $violation_details;
					$user_data_vio['duration'] = $hrsToMin;
					$user_data_vio['shift'] = $shift_start_end;
					$user_data_vio['sched_date'] = $sched->schedDate;
					// $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
					// $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
				} else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "Incomplete Log",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for forgot to logout details'
					];
				}
			} 
			else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 4) // OB
			{
				// DTR logs
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
				$login = new DateTime($login_raw->log);
				$login_formatted = $login->format("Y-m-d H:i");
				$break_out_logs =[];
				$break_in_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
				$login_str = strtotime($login_formatted);
				$shift_start_end = $sched->time_start . " - " . $sched->time_end;
				var_dump($login_raw);
				var_dump($logout_raw);
				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					$logout = new DateTime($logout_raw->log);
					$logout_formatted = $logout->format("Y-m-d H:i");
					$logout_str = strtotime($logout_formatted);
					$break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
					$break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');
					$user_dtr_violation_details[1] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $logout_raw->dtr_id,
						'log' => $logout_formatted,
						'type' => "DTR",
						'entry' => "O",
						'minDuration' => 0
					];
				}else if(count($logout_raw) < 1){
					$next_dtr_log = $this->get_next_dtr_log($login_raw->dtr_id, $sched->empId);
					var_dump($next_dtr_log);
					// if()
					if($next_dtr_log == NULL){
						$break_out_logs = $this->get_breaks($login_raw->dtr_id, $login_raw->dtr_id*1000, $sched->empId, 'O');
						$break_in_logs = $this->get_breaks($login_raw->dtr_id, $login_raw->dtr_id*1000, $sched->empId, 'I');
					}else{
						$break_out_logs = $this->get_breaks($login_raw->dtr_id, $next_dtr_log[0]->dtr_id, $sched->empId, 'O');
						$break_in_logs = $this->get_breaks($login_raw->dtr_id, $next_dtr_log[0]->dtr_id, $sched->empId, 'I');
					}
				}
				// var_dump($break_out_logs);
				// var_dump($break_in_logs);
				if (count($break_out_logs) > 0 && count($break_in_logs) > 0) {
					// DATED SCHED
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
					$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
					$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
					$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
					$new_formatted_shift_end = $this->new_format_midnight_shift_out($new_shifts['end']);
					$allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
					// var_dump($allowable_breaks);
					$emp_account_details = $this->get_emp_account_details($sched->empId);
					if ($allowable_breaks['breakData'] > 0) {
						// GET BREAK LOGS
						$hours = 0;
						$minutes = 0;
						$allow_count = 0;
						foreach ($allowable_breaks['breakData'] as $rows) {
							$hours = $rows->hour;
							$minutes = $rows->min;
							$hours_to_min = $hours * 60;
							$minutes += $hours_to_min;
							$dtr_vio_allowable_break[$allow_count] = [
								"userDtrViolation_ID" => $user_dtr_violation_obj->userDtrViolation_ID,
								"breakType" => $rows->break_type,
								"duration" => $minutes
							];
							$allow_count++;
						}
						// var_dump($dtr_vio_allowable_break);
						// $data['user_dtr_violation_allowable_break'] = $this->add_user_dtr_violation_allowable_break($dtr_vio_allowable_break);
						
						
						// $break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
						// $break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');
						
						$shift_start_end = $sched->time_start . " - " . $sched->time_end;
						$user_dtr_violation_details[0] = [
							'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
							'shift' => $shift_start_end,
							'dtr_id' => $login_raw->dtr_id,
							'log' => $login_formatted,
							'type' => "DTR",
							'entry' => "I",
							'minDuration' => 0
						];
						
						// BREAK COVERED
						$covered_breaks = [];
						$covered_breaks_count = 0;
						$total_covered_break = 0;
						$user_dtr_vio_details_count = 2;
						if (count($break_out_logs) > 0) {
							foreach ($break_out_logs as $break_out_row) {
								var_dump($break_out_row);
								$searchedValue = $break_out_row->dtr_id;
								$neededObject = array_merge(array_filter(
									$break_in_logs,
									function ($e) use ($searchedValue) {
										return $e->note == $searchedValue;
									}
								));
								// array_merge($neededObject);
								$break_out_log = new DateTime($break_out_row->log);
								$formatted_break_out_log = $break_out_log->format("Y-m-d H:i:s");

								$user_dtr_violation_details[$user_dtr_vio_details_count] = [
									'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
									'shift' => $shift_start_end,
									'dtr_id' => $break_out_row->dtr_id,
									'log' => $formatted_break_out_log,
									'type' => $break_out_row->break_type,
									'entry' => "O",
									'minDuration' => 0
								];
								$user_dtr_vio_details_count++;
								if (count($neededObject[0]) > 0) {
									$break_in_log = new DateTime($neededObject[0]->log);
									$formatted_break_in_log = $break_in_log->format("Y-m-d H:i:s");
									$break_out_str = strtotime($formatted_break_out_log);
									$break_in_str = strtotime($formatted_break_in_log);
									$duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
									var_dump($duration);
									$break_hrs_to_min = $duration['hoursTotal'] * 60;
									$total_covered_break += $break_hrs_to_min;
									// var_dump($break_hrs_to_min);
									// var_dump($total_covered_break);
									// $break_hrs_to_min = 1;
									// $total_covered_break = 1;
									$covered_breaks[$covered_breaks_count] = [
										"sched_date" => $sched->schedDate,
										"shift" => $shift_start_end,
										"break_type" => $break_out_row->break_type,
										"out_id" => $break_out_row->dtr_id,
										"out_log" => $formatted_break_out_log,
										"in_id" => $neededObject[0]->dtr_id,
										"in_log" => $formatted_break_in_log,
										"minutes" => $break_hrs_to_min,
										"human_time_format" => $duration
									];
									$user_dtr_violation_details[$user_dtr_vio_details_count] = [
										'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
										'shift' => $shift_start_end,
										'dtr_id' => $neededObject[0]->dtr_id,
										'log' => $formatted_break_in_log,
										'type' => $neededObject[0]->break_type,
										'entry' => "I",
										'minDuration' => $break_hrs_to_min
									];
									$user_dtr_vio_details_count++;
									$covered_breaks_count++;
								}
							}
							// $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
							if ((int)$total_covered_break > floatval($allowable_breaks['minutes'])) {
								$ob = $total_covered_break - $allowable_breaks['minutes'];
								// var_dump($ob);
								// $min = floor($ob /60);
								// $seconds = round(($ob - (int)$ob) * 60);
								// var_dump($min);
								// var_dump($seconds);
								$data['valid'] = 1;
								$violation_details = [
									"total_covered" => (int)$total_covered_break,
									"break_coverage_info" => $covered_breaks,
									"allowable_break" => $allowable_breaks,
									"over_break" => $ob
								];
								$data['violation_details'] = $violation_details;
							}
							$user_data_vio['duration'] = $total_covered_break;
							$user_data_vio['shift'] = $shift_start_end;
							$user_data_vio['sched_date'] = $sched->schedDate;
							// $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
						} else {
							$data['error'] = 1;
							$data['error_details'] = [
								'error' => "Incomplete Break logs",
								'origin' => 'Related Violation details function - (get_violation_related_details)',
								'process' => 'getting out for break logs'
							];
						}
					} else {
						$data['error'] = 1;
						$data['error_details'] = [
							'error' => "Assigned Breaks not found",
							'origin' => 'Related Violation details function - (get_violation_related_details)',
							'process' => 'getting of allowable break details'
						];
					}
				}else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "Incomplete Log",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for over break details'
					];
				}
			} 
			else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 6) // INC BREAK
			{
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					// DATED SCHED
					$login = new DateTime($login_raw->log);
					$login_formatted = $login->format("Y-m-d H:i");
					$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
					$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
					$logout = new DateTime($logout_raw->log);
					$logout_formatted = $logout->format("Y-m-d H:i");
					$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
					$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
					$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
					$new_formatted_shift_end = $this->new_format_midnight_shift_out($new_shifts['end']);

					$allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
					$required_break_logs = count($allowable_breaks['breakData']) * 2;
					$emp_account_details = $this->get_emp_account_details($sched->empId);
					$shift_start_end = $sched->time_start . " - " . $sched->time_end;

					$user_dtr_violation_details[0] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $login_raw->dtr_id,
						'log' => $login_formatted,
						'type' => "DTR",
						'entry' => "I",
						'minDuration' => 0
					];
					$user_dtr_violation_details[1] = [
						'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
						'shift' => $shift_start_end,
						'dtr_id' => $logout_raw->dtr_id,
						'log' => $logout_formatted,
						'type' => "DTR",
						'entry' => "O",
						'minDuration' => 0
					];
					$user_dtr_vio_details_count = 2;

					if (count($allowable_breaks['breakData']) > 0) {
						// GET BREAK LOGS
						$hours = 0;
						$minutes = 0;
						$allow_count = 0;
						foreach ($allowable_breaks['breakData'] as $rows) {
							$hours = $rows->hour;
							$minutes = $rows->min;
							$hours_to_min = $hours * 60;
							$minutes += $hours_to_min;
							$dtr_vio_allowable_break[$allow_count] = [
								"userDtrViolation_ID" => $user_dtr_violation_obj->userDtrViolation_ID,
								"breakType" => $rows->break_type,
								"duration" => $minutes
							];
							$allow_count++;
						}
						// $data['user_dtr_violation_allowable_break'] = $this->add_user_dtr_violation_allowable_break($dtr_vio_allowable_break);
						$break_out_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'O');
						$break_in_logs = $this->get_breaks($login_raw->dtr_id, $logout_raw->dtr_id, $sched->empId, 'I');

						if (count($break_out_logs) > 0) {
							foreach ($break_out_logs as $break_out_row) {
								$searchedValue = $break_out_row->dtr_id;
								$neededObject = array_merge(array_filter(
									$break_in_logs,
									function ($e) use ($searchedValue) {
										return $e->note == $searchedValue;
									}
								));
								$break_out_log = new DateTime($break_out_row->log);
								$formatted_break_out_log = $break_out_log->format("Y-m-d H:i");

								$user_dtr_violation_details[$user_dtr_vio_details_count] = [
									'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
									'shift' => $shift_start_end,
									'dtr_id' => $break_out_row->dtr_id,
									'log' => $formatted_break_out_log,
									'type' => $break_out_row->break_type,
									'entry' => $break_out_row->entry,
									'minDuration' => 0
								];
								$user_dtr_vio_details_count++;
								if (count($neededObject) > 0) {
									$break_in_log = new DateTime($neededObject[0]->log);
									$formatted_break_in_log = $break_in_log->format("Y-m-d H:i");

									$break_out_str = strtotime($formatted_break_out_log);
									$break_in_str = strtotime($formatted_break_in_log);

									$duration = $this->get_human_time_format($break_out_str, $break_in_str, 0);
									$break_hrs_to_min = $duration['hoursTotal'] * 60;

									$user_dtr_violation_details[$user_dtr_vio_details_count] = [
										'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
										'shift' => $shift_start_end,
										'dtr_id' => $neededObject[0]->dtr_id,
										'log' => $formatted_break_in_log,
										'type' => $neededObject[0]->break_type,
										'entry' => "I",
										'minDuration' => $break_hrs_to_min
									];
									$user_dtr_vio_details_count++;
								}
							}
						} else {
							$data['error'] = 1;
							$data['error_details'] = [
								'error' => "Incomplete Break logs",
								'origin' => 'Related Violation details function - (get_violation_related_details)',
								'process' => 'getting out for break logs'
							];
						}
						$user_data_vio['shift'] = $shift_start_end;
						$user_data_vio['sched_date'] = $sched->schedDate;
						// $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
						$recorded_break_logs = count($break_out_logs) + count($break_in_logs);
						if ($recorded_break_logs != $required_break_logs) {
							$data['valid'] = 1;
							$data['violation_details'] = [
								'allowable_break' => $allowable_breaks,
								'break_out_logs' => $break_out_logs,
								'break_in_logs' => $break_in_logs
							];
						}
					} else {
						$data['error'] = 1;
						$data['error_details'] = [
							'error' => "Assigned Breaks not found",
							'origin' => 'Related Violation details function - (get_violation_related_details)',
							'process' => 'getting of allowable break details'
						];
					}
					// $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
				} else {
					$data['error'] = 1;
					$data['error_details'] = [
						'error' => "Incomplete Log",
						'origin' => 'Related Violation details function - (get_violation_related_details)',
						'process' => 'getting of login and logout for over break details'
					];
				}
			} 
			else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 7) //INC DTR Logs
			{
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
				$shift_start_end = $sched->time_start . " - " . $sched->time_end;
				if (count($login_raw) > 0 && count($logout_raw) > 0) {
					// "COMPLETE DTR LOGS";
				} else {
					if (count($login_raw) > 0) {
						$login = new DateTime($login_raw->log);
						$login_formatted = $login->format("Y-m-d H:i");
						$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
						$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
						$new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
						// $shift_start_end = $sched->time_start." - ".$sched->time_end;

						$shift_end_date_time = new DateTime($sched->schedDate . " " . $sched->time_end);
						$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
						$new_shifts = $this->check_start_end_shift($formatted_shift_start, $formatted_shift_end);
						$new_formatted_shift_end = $this->new_format_midnight($new_shifts['end'], $sched->schedDate);

						$allowable_breaks = $this->get_allowable_break($sched->accountId, $user_dtr_violation_obj->sched_ID); //get breaks
						if ($allowable_breaks['breakData'] > 0) {
							$hours = 0;
							$minutes = 0;
							$allow_count = 0;
							foreach ($allowable_breaks['breakData'] as $rows) {
								$hours = $rows->hour;
								$minutes = $rows->min;
								$hours_to_min = $hours * 60;
								$minutes += $hours_to_min;
								$dtr_vio_allowable_break[$allow_count] = [
									"userDtrViolation_ID" => $user_dtr_violation_obj->userDtrViolation_ID,
									"breakType" => $rows->break_type,
									"duration" => $minutes
								];
								$allow_count++;
							}
							// $data['user_dtr_violation_allowable_break'] = $this->add_user_dtr_violation_allowable_break($dtr_vio_allowable_break);
							$shift_start_end = $sched->time_start . " - " . $sched->time_end;
							$user_dtr_violation_details[0] = [
								'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
								'shift' => $shift_start_end,
								'dtr_id' => $login_raw->dtr_id,
								'log' => $login_formatted,
								'type' => "DTR",
								'entry' => "I",
								'minDuration' => 0
							];
							$user_dtr_vio_count = 1;
							// BREAK lOGS WITHOUT Logout;
							$break_logs_without_logout = $this->get_breaks_without_logout($login_raw->dtr_id, $new_formatted_shift_end, $sched->empId);
							foreach ($break_logs_without_logout  as $break_logs_without_logout) {
								$user_dtr_violation_details[$user_dtr_vio_count] = [
									'userDtrViolation_ID' => $user_dtr_violation_obj->userDtrViolation_ID,
									'shift' => $shift_start_end,
									'dtr_id' => $break_logs_without_logout->dtr_id,
									'log' => $break_logs_without_logout->log,
									'type' => $break_logs_without_logout->break_type,
									'entry' => $break_logs_without_logout->entry,
									'minDuration' => 0
								];
								$user_dtr_vio_count++;
							}
							$user_data_vio['shift'] = $shift_start_end;
							$user_data_vio['sched_date'] = $sched->schedDate;
							// $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
							// $data['user_dtr_violation_details'] = $this->add_user_dtr_violation_details($user_dtr_violation_details);
						} else {
							$data['error'] = 1;
							$data['error_details'] = [
								'error' => "Assigned Breaks not found",
								'origin' => 'Related Violation details function - (get_violation_related_details)',
								'process' => 'getting of allowable break details'
							];
						}
						$data['valid'] = 1;
						$violation_details = [
							"sched_date" => $sched->schedDate,
							"shift" => $shift_start_end,
							"in_id" => $login_raw->dtr_id,
							"in_log" => $login_formatted,
							"shift_in" => $new_formatted_shift_start,
						];
						$data['violation_details'] = $violation_details;
					} else {
						$data['error'] = 1;
						$data['error_details'] = [
							'error' => "Login log not found",
							'origin' => 'Related Violation details function - (get_violation_related_details)',
							'process' => 'getting of log details'
						];
					}
				}
			} 
			else if ((int) $user_dtr_violation_obj->dtrViolationType_ID == 5) // ABSENT
			{
				$login_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'I');
				$logout_raw = $this->get_user_dtr_log($user_dtr_violation_obj->sched_ID, 'O');
				$shift_start_date_time = new DateTime($sched->schedDate . " " . $sched->time_start);
				$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
				$new_formatted_shift_start = $this->new_format_midnight($formatted_shift_start, $sched->schedDate);
				$shift_start_end = $sched->time_start . " - " . $sched->time_end;
				if (count($login_raw) == 0 && count($logout_raw) == 0) {
					// "COMPLETE DTR LOGS";
					$data['valid'] = 1;
					$violation_details = [
						"sched_date" => $sched->schedDate,
						"shift" => $shift_start_end,
					];
					$data['violation_details'] = $violation_details;
				}
				$user_data_vio['shift'] = $shift_start_end;
				$user_data_vio['sched_date'] = $sched->schedDate;
				// $data['user_dtr_duration_update'] = $this->update_user_dtr_violation($user_dtr_violation_obj->userDtrViolation_ID, $user_data_vio);
			}
		} else {
			$data['error'] = 1;
			$data['error_details'] = [
				'error' => "No Sched found",
				'origin' => 'Related Violation details function - (get_violation_related_details)',
				'process' => 'getting of schedule'
			];
		}
		return $data;
	}

	protected function update_qualified_dtr_vio($qualified_user_dtr_vio_id, $data)
	{
		$where = "qualifiedUserDtrViolation_ID =  $qualified_user_dtr_vio_id";
		return  $this->general_model->update_vals($data,  $where, 'tbl_dms_qualified_user_dtr_violation');
	}

	protected function update_supervisor_qualified_notification($qualified_user_dtr_vio_id, $emp_id, $data)
	{
		$where = "qualifiedUserDtrViolation_ID =  $qualified_user_dtr_vio_id AND emp_id = $emp_id";
		return  $this->general_model->update_vals($data,  $where, 'tbl_dms_supervisor_qualified_user_dtr_violation_notification');
	}

	protected function check_refreshment_date($violation_type, $emp_id, $sched_date, $user_dtr_violation_id)
	{
		$fields = "qualified.qualifiedUserDtrViolation_ID, qualified.refreshment_date";
		$where = "violation.userDtrViolation_ID = qualified.userDtrViolation_ID AND violation.dtrViolationType_ID = $violation_type AND qualified.status_ID = 3 AND qualified.emp_id = $emp_id AND qualified.refreshment_date > '$sched_date' AND qualified.userDtrViolation_ID  != $user_dtr_violation_id";
		$table = "tbl_dms_qualified_user_dtr_violation qualified, tbl_user_dtr_violation violation";
		return  $this->general_model->fetch_specific_val($fields,  $where,  $table, "qualified.refreshment_date DESC");
	}

	protected function add_months($months, DateTime $dateObject) 
    {
        $next = new DateTime($dateObject->format('Y-m-d'));
        $next->modify('last day of +'.$months.' month');

        if($dateObject->format('d') > $next->format('d')) {
            return $dateObject->diff($next);
        } else {
            return new DateInterval('P'.$months.'M');
        }
    }

	protected function endCycle($d1, $months)
		{
			$date = new DateTime($d1);

			// call second function to add the months
			$newDate = $date->add($this->add_months($months, $date));

			// goes back 1 day from date, remove if you want same day of month
			$newDate->sub(new DateInterval('P1D')); 

			//formats final date to Y-m-d form
			$dateReturned = $newDate->format('Y-m-d'); 

			return $dateReturned;
		}

	protected function get_start_and_end_date_coverage_increment($sched_date,  $rule,  $unit)
	{
		//  $sched_date = '2019-02-10';
		//  $fixed_rule = 4;
		//  $unit = 'week';
		if ($unit == 'month') {
			$coverage_to_month = $this->endCycle($sched_date, $rule);
			$start = new DateTime($sched_date);
			$date['start'] =  $start->format('Y-m') . "-01";
			$end = new DateTime($coverage_to_month);
			$date['end'] =  $end->format('Y-m-t');
		} else {
			$start_date = new DateTime($sched_date);
			$start_year = (int) Date("Y", strtotime($sched_date));
			$start_week = (int) Date("W", strtotime($sched_date));
			$date['start'] =  $start_date->setISODate($start_year,  $start_week)->format('Y-m-d');
			$end_date = new DateTime(Date("Y-m-d", strtotime($sched_date . '+' .  $rule . ' weeks')));
			$end_year = (int) Date("Y", strtotime($sched_date . '+' .  $rule . ' weeks'));
			$end_week = (int) Date("W", strtotime($sched_date . '+' .  $rule . ' weeks'));
			$end_date->setISODate($end_year,  $end_week)->format('Y-m-d');
			$date['end'] =  $end_date->modify('+6 days')->format('Y-m-d');
		}
		return  $date;
	}

	protected function get_start_and_end_date_coverage_increment_test($sched_date,  $rule,  $unit)
	{
		//  $sched_date = '2019-02-10';
		//  $fixed_rule = 4;
		//  $unit = 'week';
		if ($unit == 'month') {
			$coverage_to_month = $this->endCycle($sched_date, $rule);
			$start = new DateTime($sched_date);
			$date['start'] =  $start->format('Y-m') . "-01";
			$end = new DateTime($coverage_to_month);
			$date['end'] =  $end->format('Y-m-t');
		} else {
			$start_date = new DateTime($sched_date);
			$start_year = (int) Date("Y", strtotime($sched_date));
			$start_week = (int) Date("W", strtotime($sched_date));
			$date['start'] =  $start_date->setISODate($start_year,  $start_week)->format('Y-m-d');
			$end_date = new DateTime(Date("Y-m-d", strtotime($sched_date . '+' .  $rule . ' weeks')));
			$end_year = (int) Date("Y", strtotime($sched_date . '+' .  $rule . ' weeks'));
			$end_week = (int) Date("W", strtotime($sched_date . '+' .  $rule . ' weeks'));
			$end_date->setISODate($end_year,  $end_week)->format('Y-m-d');
			$date['end'] =  $end_date->modify('+6 days')->format('Y-m-d');
		}
		return  $date;
	}

	protected function identify_refreshment($violation_rules, $user_dtr_violation)
	{
		// $violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID);
		// ////var_dump($violation_rules);
		$incident_date = $user_dtr_violation->sched_date;
		$unit = $violation_rules->durationUnit;
		$presc_period = $violation_rules->durationVal;
		// ////var_dump($incident_date);
		$covered = 0;
		if ($violation_rules->durationType == 1) {
			$fixed_rule = $presc_period - 1;
			$start_end_coverage = $this->get_start_and_end_date_coverage_increment($incident_date, $fixed_rule, $unit);
			$data['period'] = $presc_period;
			$data['unit'] = $unit;
			$data['cure_date'] = $start_end_coverage['end'];
			if ($start_end_coverage['end'] == $incident_date) {
				$start_end_coverage = $this->get_start_and_end_date_coverage_increment($incident_date, $fixed_rule + 1, $unit);
				$data['cure_date'] = $start_end_coverage['end'];
			}
		} else if ($violation_rules->durationType == 2) {
			$data['period'] = $presc_period;
			$data['unit'] = 'day';
			$data['cure_date'] = Date("Y-m-d", strtotime($incident_date . '+' . $presc_period . ' days'));
		}
		// ////var_dump($data);
		return $data;
	}

	public function identify_refreshment_test()
	{
		$violation_rules = $this->get_dtr_violation_settings(1);
		// var_dump($violation_rules);
		$incident_date = "2021-05-31";
		$unit = $violation_rules->durationUnit;
		$presc_period = $violation_rules->durationVal;
		// ////var_dump($incident_date);
		$covered = 0;
		if ($violation_rules->durationType == 1) {
			$fixed_rule = $presc_period - 1;
			$start_end_coverage = $this->get_start_and_end_date_coverage_increment_test($incident_date, $fixed_rule, $unit);
			$data['period'] = $presc_period;
			$data['unit'] = $unit;
			$data['cure_date'] = $start_end_coverage['end'];
			// var_dump($start_end_coverage['end']);
			// var_dump($data);
			if ($start_end_coverage['end'] == $incident_date) {
				// var_dump($fixed_rule);
				$start_end_coverage = $this->get_start_and_end_date_coverage_increment_test($incident_date, $fixed_rule + 1, $unit);
				// var_dump($start_end_coverage);
				$data['cure_date'] = $start_end_coverage['end'];
			}
		} else if ($violation_rules->durationType == 2) {
			$data['period'] = $presc_period;
			$data['unit'] = 'day';
			$data['cure_date'] = Date("Y-m-d", strtotime($incident_date . '+' . $presc_period . ' days'));
		}
		var_dump($data);
		// return $data;
	}

	protected function qry_invalid_overbreak($start_date)
	{
		$fields = "userDtrViolation_ID";
		$where = "dtrViolationType_ID = 4 AND DATE(sched_date) >='$start_date' AND status_ID = 23";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_user_dtr_violation", "sched_date ASC");
	}

	protected function qry_sched_details($sched_id){
		$fields = "*";
		$where = "sched_id = $sched_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_schedule");
	}

	public function recheck_overbreak(){
		$user_dtr_violations = $this->qry_invalid_overbreak('2020-01-15');
		// var_dump($user_dtr_violations);
		foreach($user_dtr_violations as $user_dtr_violations_row){
			// echo $user_dtr_violations_row->userDtrViolation_ID."<br>";
			$this->check_qualified_dtr_violation($user_dtr_violations_row->userDtrViolation_ID);
		}
		// $dtr_vio = [9881, 9885, 9888, 9889, 9899, 9911, 9916, 9918, 9920, 9932, 9933, 9941, 9946, 9963, 9967, 9973, 9986, 9988, 9991, 10004, 10010, 10011, 10014, 10015, 10018, 10026, 10027, 10028, 10034, 10039];
		// // echo count($dtr_vio);
		// for($loop = 0; $loop < count($dtr_vio); $loop++){
		// 	// var_dump($dtr_vio[$loop]);
		// 	// $this->check_qualified_dtr_violation($dtr_vio[$loop]);
		// 	// $this->check_qualified_dtr_violation_test($dtr_vio[$loop]);
		// }
	}

	public function qualified_zero_status(){
		$fields = "vio.*, qualified.status_ID";
		$where = "vio.userDtrViolation_ID = qualified.userDtrViolation_ID AND qualified.status_ID = 0";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_user_dtr_violation vio, tbl_dms_qualified_user_dtr_violation qualified");
	}

	public function test_qualified(){
		$zero_status = $this->qualified_zero_status();
		var_dump(count($zero_status));
		if(count($zero_status) > 0){
			foreach($zero_status as $zero_status_row){
				$this->check_qualified_dtr_violation_test($zero_status_row->userDtrViolation_ID);
			}
		}
	}

	public function check_qualified_dtr_violation_test($user_dtr_violation_id = 68118)
	{
		echo "<br>$user_dtr_violation_id";
		// 1687 - late 1693 -2, 1696
		// 1688 - ob 1694 -2 1698, 1699
		// 1689 - ut
		// 1690 - forgot to logout
		// 1691 - inc break 1695 -2
		// 1692 - inc dtr logs 1700
		// 1697 -absent
		$qualified = 0;
		$qualify['valid'] = 0;
		$qualify['error'] = 0;
		$qualify['details'] = "";
		$qualifiedStat = 0;
		$valid = 0;
		// 1687
		// $user_dtr_violation_id = 1713;
		// GET USER DTR VIOLATION
		$user_dtr_violation = $this->get_user_dtr_violation($user_dtr_violation_id);
		$violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID); //Get violation rules and prescription (violation settings)
		if(($violation_rules->offense_ID != NULL) && ($violation_rules->category_ID != NULL)){
			$same_sched_offense = $this->get_same_offense_of_same_sched($violation_rules->offense_ID, $user_dtr_violation->sched_ID);
			// var_dump($same_sched_offense);
			if (count($same_sched_offense) < 1) {
				$notif_supvervisor_bool = 0;
				if (count($user_dtr_violation) > 0) {
					$subject_user_emp_details = $this->get_emp_details_via_user_id($user_dtr_violation->user_ID);
					$violation_details = $this->get_violation_related_details2($user_dtr_violation); // check if violation is valid
					if ($violation_details['valid']) //check if incident is valid
					{
						$qualify['valid'] = 1;
						if (($violation_rules->offense_ID == NULL) || ($violation_rules->category_ID == NULL)) {
							$qualify['error'] = 1;
							$qualify['details'] = "Either Default Offense or Default Category was not set for this violation type";
						} else {
							 
								echo "- qualified";
								$refreshment = $this->check_refreshment_date($user_dtr_violation->dtrViolationType_ID, $subject_user_emp_details->emp_id, $user_dtr_violation->sched_date, $user_dtr_violation_id);
								var_dump($refreshment);
								// $refreshment = $this->check_refreshment_date();
								if (count($refreshment) > 0) {
									// echo "dont check violation rules";
									var_dump('dont check violation rules');
								// 	$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2, 0, $user_dtr_violation_id, 0, $refreshment->refreshment_date);
								// 	$qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
								// 	//auto_notify_ir_direct_sup 
								// 	$qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules);
									$qualify['qualify_status'] = "An IR of the same offense that has a currently ongoing prescriptive period exist";
								} else {
									$refreshment_value = $this->identify_refreshment_test($violation_rules, $user_dtr_violation);
								// 	// var_dump($refreshment_value);
								// 	// check rule
									$auto_ir_notify_rules = $this->check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules);
									var_dump($auto_ir_notify_rules);
									if ($auto_ir_notify_rules['valid']) {
										$qualifiedStat = 1;
								// 		// $user_data['status_ID'] = 3;
										// $qualify['user_dtr_violation_stat'] = $this->update_user_dtr_violation($user_dtr_violation_id, $user_data);
										$qualify['qualify_status'] = "The User DTR Violation fits on a specific violation rule";
									} else {
										if ($user_dtr_violation->dtrViolationType_ID == 5) {
											$qualifiedStat = 1;
											$qualify['qualify_status'] = "The User DTR violation detected is Absent";
										} else {
											echo "- not qualified";
											$qualifiedStat = 0;
											$qualify['error_details'] = "Does not qualify Rules";
										}
									}

									
									if ($qualifiedStat) {
										$ongoing_prescriptive_stat_ir = $this->get_existing_ir($subject_user_emp_details->emp_id, 2, 2, $violation_rules->offense_ID);
										$pending_qualified_exist = $this->check_if_pending_qualified_exist($violation_rules->offense_ID, $subject_user_emp_details->emp_id); // Check if pending qualified user dtr violation exist
										if (count($ongoing_prescriptive_stat_ir) > 0) { // CHECK IF THERE IS AN ONGOING IR
											// var_dump($ongoing_prescriptive_stat_ir);
											echo "- ongoing ir";
											// $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0);
											$qualify['qualify_status'] = "A pending IR of the same offense exist";
										} else if (count($pending_qualified_exist) > 0) { // CHECK IF THERE IS AN ONGOING QUALIFIED DTR violation of the same offense
											// $qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0);
											echo "- pending qualified";
											$qualify['qualify_status'] = "A pending qualified user dtr violation of same offense exist";
										}else{
											echo "- set to pending";
											// 		$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2, $auto_ir_notify_rules['occurence_rule_num'], $user_dtr_violation_id, $auto_ir_notify_rules['occurence_value'], $refreshment_value["cure_date"]);
													$qualify['valid'] = 1;
													// $qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
											// 		//auto_notify_ir_direct_sup
											// 		$qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules);
										}
									}


								}

						}
					} else {
						echo "- not qualified";
						$qualify['error_details'] = "Violation is not true";
						$qualify['related_details'] = $violation_details['error_details'];
						// set USER DTR VIOLATION to INVALID
						// $user_data_vio['status_ID'] = 23;
						// $qualify['user_dtr_update_to_invalid'] = $this->update_user_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data_vio);
					}
				} else {
					$qualify['error'] = 1;
					$qualify['error_details'] = "user dtr violation not found";
				}
				
				
			} else {
				$qualify['error'] = 1;
				$qualify['error_details'] = "a liable IR of the same offense and of the same sched id exist";
			}
		}else{
			$qualify['error'] = 1;
			$qualify['error_details'] = "Violation type is not assigned with a specific offense";
		}
		// return $qualify;
		var_dump($qualify);
	}

	protected function get_qualified_dtr_violation($qualified_violation_id)
    {
        $fields = "qualifiedUserDtrViolation_ID, occurrenceRuleNum, occurrenceValue, userDtrViolation_ID, emp_id, status_ID";
        $where = "qualifiedUserDtrViolation_ID = $qualified_violation_id";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_qualified_user_dtr_violation');
	}

	protected function get_qualified_dtr_violation_via_violation($user_violation_id)
    {
        $fields = "qualifiedUserDtrViolation_ID, occurrenceRuleNum, occurrenceValue, userDtrViolation_ID, emp_id, status_ID";
        $where = "userDtrViolation_ID = $user_violation_id";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_dms_qualified_user_dtr_violation');
	}
	

	// protected function check_if_dtr_vio_recorded_qualified($user_dtr_violation_id){
	// 	$fields = "*";
	// 	$where = "userDtrViolation_ID = $user_dtr_violation_id";
	// 	return $this->general_model->fetch_specific_val($fields, $where, "tbl_dms_qualified_user_dtr_violation");
	// }

	protected function check_if_dtr_vio_recorded($sched_id, $dtr_vio_type_id, $user_id){
		$fields = "*";
		$where = "user_ID = $user_id AND sched_ID = $sched_id AND dtrViolationType_ID = $dtr_vio_type_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_user_dtr_violation");
	}

	public function adddataViolation($dtrViolationType_ID, $supervisor_ID, $sched_id, $schedDate, $uid, $schedType = 0)
	{
		$dtr_vio_record = $this->check_if_dtr_vio_recorded($sched_id, $dtrViolationType_ID, $uid);
		$data['exist'] = 0;
		if(count($dtr_vio_record) < 1){
			$data['exist'] = 1;
			$dataViolation['dtrViolationType_ID'] = $dtrViolationType_ID;
			$dataViolation['supervisor_ID'] = $supervisor_ID;
			$dataViolation['user_ID'] = $uid;
			$dataViolation['sched_id'] =  $sched_id;
			$dataViolation['sched_date'] =  $schedDate;
			$dataViolation['sched_type'] =  $schedType;
			$dataViolation['month'] =  date("F", strtotime($schedDate));
			$dataViolation['year'] =  date("y", strtotime($schedDate));
			$data['record'] = $this->general_model->insert_vals_last_inserted_id($dataViolation, 'tbl_user_dtr_violation');
		}
		return $data;
	}
	
	public function check_qualified_dtr_violation($user_dtr_violation_id)
	{
		// 1687 - late 1693 -2, 1696
		// 1688 - ob 1694 -2 1698, 1699
		// 1689 - ut
		// 1690 - forgot to logout
		// 1691 - inc break 1695 -2
		// 1692 - inc dtr logs 1700
		// 1697 -absent
		$qualified = 0;
		$qualify['valid'] = 0;
		$qualify['error'] = 0;
		$qualify['details'] = "";
		$qualifiedStat = 0;
		$valid = 0;
		// 1687
		// $user_dtr_violation_id = 1713;
		// GET USER DTR VIOLATION
		$user_dtr_violation = $this->get_user_dtr_violation($user_dtr_violation_id);
		$violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID); //Get violation rules and prescription (violation settings)
		if(($violation_rules->offense_ID != NULL) && ($violation_rules->category_ID != NULL)){
			$same_sched_offense = $this->get_same_offense_of_same_sched($violation_rules->offense_ID, $user_dtr_violation->sched_ID);
			if(count($same_sched_offense) < 1){
				$notif_supvervisor_bool = 0;
				if (count($user_dtr_violation) > 0) {
					$subject_user_emp_details = $this->get_emp_details_via_user_id($user_dtr_violation->user_ID);
					$violation_details = $this->get_violation_related_details($user_dtr_violation); // check if violation is valid
					// var_dump($violation_details);
					if ($violation_details['valid']) //check if incident is valid
					{
						$qualify['valid'] = 1;
						if (($violation_rules->offense_ID == NULL) || ($violation_rules->category_ID == NULL)) {
							$qualify['error'] = 1;
							$qualify['details'] = "Either Default Offense or Default Category was not set for this violation type";
						} else {
							$refreshment = $this->check_refreshment_date($user_dtr_violation->dtrViolationType_ID, $subject_user_emp_details->emp_id, $user_dtr_violation->sched_date, $user_dtr_violation_id);
							// $refreshment = $this->check_refreshment_date();
							if (count($refreshment) > 0) {
								// dont check violation rules
								// var_dump('dont check violation rules');
								$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2, 0, $user_dtr_violation_id, 0, $refreshment->refreshment_date);
								$qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
								//auto_notify_ir_direct_sup 
								$qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules);
								$qualify['qualify_status'] = "A violation of the same offense that has a currently ongoing refreshment period exist";
							} else {
								$refreshment_value = $this->identify_refreshment($violation_rules, $user_dtr_violation);
								// var_dump($refreshment_value);
								// check rule
								$auto_ir_notify_rules = $this->check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules);
								// var_dump($auto_ir_notify_rules);
								if ($auto_ir_notify_rules['valid']) {
									$qualifiedStat = 1;
									// $user_data['status_ID'] = 3;
									// $qualify['user_dtr_violation_stat'] = $this->update_user_dtr_violation($user_dtr_violation_id, $user_data);
									$qualify['qualify_status'] = "The User DTR Violation fits on a specific violation rule";
								} else {
									if ($user_dtr_violation->dtrViolationType_ID == 5) {
										$qualifiedStat = 1;
										$qualify['qualify_status'] = "The User DTR violation detected is Absent";
									} else {
										$qualifiedStat = 0;
										$qualify['error_details'] = "Does not qualify Rules";
									}
								}
								if ($qualifiedStat) {
									$ongoing_prescriptive_stat_ir = $this->get_existing_ir($subject_user_emp_details->emp_id, 2, 2, $violation_rules->offense_ID);
									$pending_qualified_exist = $this->check_if_pending_qualified_exist($violation_rules->offense_ID, $subject_user_emp_details->emp_id); // Check if pending qualified user dtr violation exist
									if (count($ongoing_prescriptive_stat_ir) > 0) { // CHECK IF THERE IS AN ONGOING IR
										$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0, null);
										$qualify['qualify_status'] = "A pending IR of the same offense exist";
									} else if (count($pending_qualified_exist) > 0) { // CHECK IF THERE IS AN ONGOING QUALIFIED DTR violation of the same offense
										$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0, null);
										$qualify['qualify_status'] = "A pending qualified user dtr violation of same offense exist";
									} else {
										$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2, $auto_ir_notify_rules['occurence_rule_num'], $user_dtr_violation_id, $auto_ir_notify_rules['occurence_value'], $refreshment_value["cure_date"]);
										$qualify['valid'] = 1;
										$qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
										//auto_notify_ir_direct_sup
										$qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules);
									}
								}
							}
						}
					} else {
						$qualify['error_details'] = "Violation is not true";
						$qualify['related_details'] = $violation_details['error_details'];
						// set USER DTR VIOLATION to INVALID
						$user_data_vio['status_ID'] = 23;
						// $user_data_vio['status_ID'] = 23;
						$qualify['user_dtr_update_to_invalid'] = $this->update_user_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data_vio);
						// $qualify['qualified_update_to_invalid'] = $this->update_qualified_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data_vio);
						// $qualify['qualified_sup_update_to_invalid'] = $this->update_qualified_dtr_violation_sup_notif($user_dtr_violation->userDtrViolation_ID, $user_data_vio);
					}
				} else {
					$qualify['error'] = 1;
					$qualify['error_details'] = "user dtr violation not found";
				}
				
			}else{
				$qualify['error'] = 1;
				$qualify['error_details'] = "a liable IR of the same offense and of the same sched id exist";
			}
		}else{
			$qualify['error'] = 1;
			$qualify['error_details'] = "Either Default Offense or Default Category was not set for this violation type";
		}
		// var_dump($qualify);
		return $qualify;
	}

	// public function check_qualified_dtr_violation($user_dtr_violation_id)
	// {
	// 	// 1687 - late 1693 -2, 1696
	// 	// 1688 - ob 1694 -2 1698, 1699
	// 	// 1689 - ut
	// 	// 1690 - forgot to logout
	// 	// 1691 - inc break 1695 -2
	// 	// 1692 - inc dtr logs 1700
	// 	// 1697 -absent
	// 	$qualified = 0;
	// 	$qualify['valid'] = 0;
	// 	$qualify['error'] = 0;
	// 	$qualify['details'] = "";
	// 	$qualifiedStat = 0;
	// 	$valid = 0;
	// 	// 1687
	// 	// $user_dtr_violation_id = 1713;
	// 	// GET USER DTR VIOLATION
	// 	$user_dtr_violation = $this->get_user_dtr_violation($user_dtr_violation_id);
	// 	$notif_supvervisor_bool = 0;
	// 	if (count($user_dtr_violation) > 0) {
	// 		$subject_user_emp_details = $this->get_emp_details_via_user_id($user_dtr_violation->user_ID);
	// 		$violation_details = $this->get_violation_related_details($user_dtr_violation); // check if violation is valid
	// 		if ($violation_details['valid']) //check if incident is valid
	// 		{
	// 			$qualify['valid'] = 1;
	// 			$violation_rules = $this->get_dtr_violation_settings($user_dtr_violation->dtrViolationType_ID); //Get violation rules and prescription (violation settings)
	// 			if (($violation_rules->offense_ID == NULL) || ($violation_rules->category_ID == NULL)) {
	// 				$qualify['error'] = 1;
	// 				$qualify['details'] = "Either Default Offense or Default Category was not set for this violation type";
	// 			} else {
	// 				$ongoing_prescriptive_stat_ir = $this->get_existing_ir($subject_user_emp_details->emp_id, 2, 2, $violation_rules->offense_ID);
	// 				$pending_qualified_exist = $this->check_if_pending_qualified_exist($violation_rules->offense_ID, $subject_user_emp_details->emp_id); // Check if pending qualified user dtr violation exist
	// 				if (count($ongoing_prescriptive_stat_ir) > 0) { // CHECK IF THERE IS AN ONGOING IR
	// 					$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0);
	// 					$qualify['qualify_status'] = "A pending IR of the same offense exist";
	// 				} else if (count($pending_qualified_exist) > 0) { // CHECK IF THERE IS AN ONGOING QUALIFIED DTR violation of the same offense
	// 					$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 21, 0, $user_dtr_violation_id, 0);
	// 					$qualify['qualify_status'] = "A pending qualified user dtr violation of same offense exist";
	// 				} else {
	// 					//  I ADD DIRI KUNG MU CHECK BAYA SIYA SA RULE OF DILI

	// 					$ir_exist = $this->check_if_previous_ir_exist($user_dtr_violation, $subject_user_emp_details, $violation_rules); // Check if IR of the same violation exist
	// 					if ($ir_exist['exist']) // if a liable IR of current status and the same offense exist
	// 					{
	// 						$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2, 0, $user_dtr_violation_id, 0);
	// 						$qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
	// 						//auto_notify_ir_direct_sup 
	// 						$qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules);
	// 						$qualify['qualify_status'] = "An IR of the same offense that has a currently ongoing prescriptive period exist";
	// 					} else {
	// 						$auto_ir_notify_rules = $this->check_auto_ir_notif_rules($user_dtr_violation, $violation_details, $violation_rules);
	// 						// var_dump($auto_ir_notify_rules);
	// 						if ($auto_ir_notify_rules['valid']) {
	// 							$qualifiedStat = 1;

	// 							// $user_data['status_ID'] = 3;
	// 							// $qualify['user_dtr_violation_stat'] = $this->update_user_dtr_violation($user_dtr_violation_id, $user_data);
	// 							$qualify['qualify_status'] = "The User DTR Violation fits on a specific violation rule";
	// 						} else {
	// 							if ($user_dtr_violation->dtrViolationType_ID == 5) {
	// 								$qualifiedStat = 1;
	// 								$qualify['qualify_status'] = "The User DTR violation detected is Absent";
	// 							} else {
	// 								$qualifiedStat = 0;
	// 								$qualify['error_details'] = "Does not qualify Rules";
	// 							}
	// 						}
	// 						if ($qualifiedStat) {
	// 							$qualified_user_dtr_violation_id = $this->add_qualified_user_dtr_violation($subject_user_emp_details->emp_id, 2, $auto_ir_notify_rules['occurence_rule_num'], $user_dtr_violation_id, $auto_ir_notify_rules['occurence_value']);
	// 							$qualify['valid'] = 1;
	// 							$qualify['add_qualified_user_dtr_violation'] = $qualified_user_dtr_violation_id;
	// 							//auto_notify_ir_direct_sup
	// 							$qualify['notify_set_deadline_direct_sup'] = $this->auto_notify_ir_direct_sup($user_dtr_violation, $subject_user_emp_details, $qualified_user_dtr_violation_id, $violation_rules);
	// 						}
	// 					}
	// 				}
	// 			}
	// 		} else {
	// 			$qualify['error_details'] = "Violation is not true";
	// 			$qualify['related_details'] = $violation_details['error_details'];
	// 			// set USER DTR VIOLATION to INVALID
	// 			$user_data_vio['status_ID'] = 23;
	// 			$qualify['user_dtr_update_to_invalid'] = $this->update_user_dtr_violation($user_dtr_violation->userDtrViolation_ID, $user_data_vio);
	// 		}
	// 	} else {
	// 		$qualify['error'] = 1;
	// 		$qualify['error_details'] = "user dtr violation not found";
	// 	}
	// 	return $qualify;
	// }

	protected function get_ir($ir_id)
	{
		$fields = "ir.incidentReport_ID, ir.incidentDate, ir.incidentTime, ir.place, ir.expectedAction, ir.dateTimeFiled, ir.details, ir.prescriptionEnd, ir.prescriptiveStat, ir.subjectPosition, ir.subjectEmpStat, ir.subjectAccount, ir.empType, prescStat.description prescriptiveStatDesc, ir.liabilityStat, liableStat.description liabilityStatDesc, ir.datedLiabilityStat, ir.explanationDate, ir.prescriptiveId, ir.subjectExplanation, ir.disciplinaryActionCategory_ID, ir.disciplinaryActionCategorySettings_ID, ir.offense_ID, ir.offense, ir.incidentReportFilingType_ID, irFilingType.description, ir.incidentReportStages_ID, irStages.description, ir.subjectEmp_ID, ir.occurence, subApp.fname subjectFname, subApp.mname subjectMname, subApp.lname subjectLname, subApp.nameExt subjectNameExt, subApp.pic, ir.sourceEmp_ID sourceEmp_ID, sourceApp.fname sourceFname, sourceApp.mname sourceMname, sourceApp.lname sourceLname, sourceApp.nameExt sourceNameExt, offType.letter, offType.offenseType, ir.deadlineToRecommendOption, dmsActionSug.action suggestedAction, dmsActionFin.action finalAction, dmsActionFin.label";
		$where = "offType.offenseType_ID = off.offenseType_ID AND off.offense_ID = ir.offense_ID AND prescStat.status_ID = ir.prescriptiveStat AND liableStat.status_ID = ir.liabilityStat AND irFilingType.incidentReportFilingType_ID = ir.incidentReportFilingType_ID AND irStages.incidentReportStages_ID = ir.incidentReportStages_ID AND subApp.apid = subEmp.apid AND dmsActionSug.disciplinaryAction_ID = dmsActionCategSug.disciplinaryAction_ID AND dmsActionCategSug.disciplinaryActionCategory_ID = ir.disciplinaryActionCategory_ID AND dmsActionFin.disciplinaryAction_ID = dmsActionCategFin.disciplinaryAction_ID AND dmsActionCategFin.disciplinaryActionCategory_ID = ir.finaldisciplinaryActionCategory_ID AND subEmp.emp_id = ir.subjectEmp_ID AND sourceApp.apid = sourceEmp.apid AND sourceEmp.emp_id = ir.sourceEmp_ID AND ir.incidentReport_ID = $ir_id";
		$table = "tbl_dms_incident_report ir, tbl_status prescStat, tbl_status liableStat, tbl_dms_incident_report_filing_type irFilingType, tbl_dms_incident_report_stages irStages, tbl_applicant subApp, tbl_employee subEmp, tbl_applicant sourceApp, tbl_employee sourceEmp, tbl_dms_offense_type offType, tbl_dms_offense off, tbl_dms_disciplinary_action dmsActionSug, tbl_dms_disciplinary_action_category dmsActionCategSug, tbl_dms_disciplinary_action dmsActionFin, tbl_dms_disciplinary_action_category dmsActionCategFin";
		return  $this->general_model->fetch_specific_val($fields,  $where,  $table);
	}

	// public function get_ir_test($ir_id = 25){
	// 	var_dump($this->get_ir($ir_id));
	// }
	protected function update_incident_report($data, $ir_id)
	{
		$where = "incidentReport_ID = $ir_id";
		$update_stat = $this->general_model->update_vals($data, $where, 'tbl_dms_incident_report');
		return $update_stat;
	}

	public function check_if_cure_date_is_completed($ir_id)
	{
		$ir = $this->get_ir($ir_id);
		$dateTime = $this->get_current_date_time();
		$current_str = strtotime($dateTime['date']);
		$cure_date_str = strtotime($ir->prescriptionEnd);
		if ($cure_date_str < $current_str) {
			$data['prescriptiveStat'] = 3;
			$this->update_incident_report($data, $ir_id);
		}
	}

	// DMS FUNCTIONS (MICHAEL) END

	public function mark_as_read_nofication()
	{
		$table = $this->input->post('table');
		$uid = $this->session->uid;
		if ($table === 'tbl_system_notification_recipient') {
			$where = $this->input->post('where');
			$res = $this->general_model->custom_query_no_return("UPDATE tbl_system_notification a, tbl_system_notification_recipient b SET b.status_ID = 9 WHERE a.systemNotification_ID = b.systemNotification_ID AND b.recipient_ID = $uid $where");
			$res = ($res > 0) ? 1 : 0;
		} else {
			$res = $this->general_model->update_vals(['status_ID' => 9], "recipient_ID=$uid", 'tbl_notification_recipient');
		}
		if ($res) {
			$status = "Success";
		} else {
			$status = "Failed";
		}
		echo json_encode(['status' => $status]);
	}

	// PROCESS MANAGEMENT MICHAEL START


	protected function get_checklist_details($checklist_id)
	{
		$fields = "*";
		$where = "checklist_ID = $checklist_id";
		$table = "tbl_processST_checklist";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	protected function qry_spec_process_subtask($task_id)
	{
		$fields = "sub.subTask_ID, sub.complabel, sub.sublabel, sub.task_ID, comp.fieldType, comp.component_ID";
		$where = "comp.component_ID = sub.component_ID AND sub.task_ID IN ($task_id)";
		$table = "tbl_processST_subtask sub, tbl_processST_component comp";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, "sub.sequence ASC");
	}

	protected function qry_spec_checklist_task($process_id, $checklist_id)
	{
		$fields = "task.task_ID, task.taskTitle, chkListStat.isCompleted, chkListStat.checklist_ID";
		$where = "chkListStat.task_ID = task.task_ID AND task.process_ID = $process_id AND chkListStat.checklist_ID = $checklist_id";
		$table = "tbl_processST_checklistStatus chkListStat, tbl_processST_task task";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'task.sequence ASC');
	}

	protected function qry_spec_checklist_answers($checklist_id)
	{
		$fields = "answer_ID, subtask_ID, answer, answeredBy, dateTimeAnswered";
		$where = "checklist_ID = $checklist_id";
		$table = "tbl_processST_subtask sub, tbl_processST_component comp";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_answer", "subtask_ID ASC");
	}
	
	protected function file_format_layout($file_format)
	{
		$file_formatting['file_type'] = '';
		$file_formatting['border'] = '';
		$file_formatting['background'] = '';
		if ('xlsx' == $file_format) {
			$file_formatting['file_type'] = "XLSX";
			$file_formatting['border'] = '#4caf5082';
			$file_formatting['background'] = '#4caf500d';
		} else if ('xls' == $file_format) {
			$file_formatting['file_type'] = "XLS";
			$file_formatting['border'] = '#4caf5082';
			$file_formatting['background'] = '#4caf500d';
		} else if ('zip' == $file_format) {
			$file_formatting['file_type'] = "ZIP";
			$file_formatting['border'] = '#4caf5082';
			$file_formatting['background'] = '#4caf500d';
		} else if ('doc' == $file_format) {
			$file_formatting['file_type'] = "DOC";
			$file_formatting['border'] = '#03a9f46b';
			$file_formatting['background'] = '#03a9f41c';
		} else if ('mov' == $file_format) {
			$file_formatting['file_type'] = "MOV";
			$file_formatting['border'] = '#03a9f46b';
			$file_formatting['background'] = '#03a9f41c';
		} else if ('docx' == $file_format) {
			$file_formatting['file_type'] = "DOCX";
			$file_formatting['border'] = '#03a9f46b';
			$file_formatting['background'] = '#03a9f41c';
		} else if ('ppt' == $file_format) {
			$file_formatting['file_type'] = "PPT";
			$file_formatting['border'] = '#f443367d';
			$file_formatting['background'] = '#f443360d';
		} else if ('pdf' == $file_format) {
			$file_formatting['file_type'] = "PDF";
			$file_formatting['border'] = '#f443367d';
			$file_formatting['background'] = '#f443360d';
		} else if ('pptx' == $file_format) {
			$file_formatting['file_type'] = "PPTX";
			$file_formatting['border'] = '#f443367d';
			$file_formatting['background'] = '#f443360d';
		} else if ('rar' == $file_format) {
			$file_formatting['file_type'] = "RAR";
			$file_formatting['border'] = '#4caf5082';
			$file_formatting['background'] = '#4caf500d';
		} else if ('png' == $file_format) {
			$file_formatting['file_type'] = "PNG";
			$file_formatting['border'] = '#4caf5082';
			$file_formatting['background'] = '#4caf500d';
		} else if ('jpg' == $file_format) {
			$file_formatting['file_type'] = "JPG";
			$file_formatting['border'] = '#4caf5082';
			$file_formatting['background'] = '#4caf500d';
		}
		return $file_formatting;
	}
	// PROCESS MANAGEMENT MICHAEL END
	// PROCESS MANAGEMENT ADDITIONS MICHAEL START
	protected function qry_process_details($process_id){
		$fields = "process_ID, processTitle, processType_ID, processstatus_ID, isRemoved, createdBy, updatedBy, dateTimeCreated";
		$where = "process_ID = $process_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_processST_process");
	}

	protected function qry_process_exam_checklist_details($checklist_ID){
		$fields = "chkList.checklist_ID, chkList.process_ID, chkList.checklistTitle, chkList.status_ID, chkList.remarks, chkList.isRemoved, chkList.isArchived, chkList.dueDate, chkList.createdBy, chkList.updatedBy, chkList.completedBy, chkList.dateTimeCreated, chkList.dateTimeUpdated, chkList.dateTimeCompleted, chkListExam.checklistExam_ID, chkListExam.examtype_ID, chkListExam.start, chkListExam.end";
		$where = "chkListExam.checklist_ID = $checklist_ID AND chkList.checklist_ID = $checklist_ID";
		$table = "tbl_processST_checklist chkList, tbl_processST_checklist_exam chkListExam";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}
	
	protected function qry_process_tasks($process_id)
	{
		$fields = "task.task_ID, task.taskTitle, task.status_ID, task.sequence, isRemoved";
		$where = "task.process_ID = $process_id AND isRemoved = 0";
		$table = "tbl_processST_task task";
		return $this->general_model->fetch_specific_vals($fields, $where, $table, 'task.sequence ASC');
	}

	protected function qry_specific_process_tasks($task_id)
	{
		$fields = "task.task_ID, task.taskTitle, task.status_ID, task.sequence, isRemoved";
		$where = "task.task_ID = $task_id";
		$table = "tbl_processST_task task";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	protected function qry_process_subtasks($task_ids)
	{
		$task_id_str = 0;
        if(count($task_ids) > 0){
            $task_id_str = implode(",",$task_ids);
        }
		$fields = "subTask_ID, task_ID, component_ID, complabel, sublabel, required, placeholder, text_alignment, text_size, min, max, sequence, isRemoved";
		$where = "task_ID IN ($task_id_str) AND isRemoved = 0";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_subtask", 'sequence ASC');
	}

	protected function qry_specific_process_subtasks($task_id){
		$fields = "subTask_ID, task_ID, component_ID, complabel, sublabel, required, placeholder, text_alignment, text_size, min, max, sequence, isRemoved";
		$where = "task_ID = $task_id AND isRemoved = 0";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_subtask", 'sequence ASC');
	}

	protected function qry_subtask_details($sub_task_id){
		$fields = "*";
		$where = "subTask_ID = $sub_task_id";
		return $this->general_model->fetch_specific_val($fields, $where, 'tbl_processST_subtask');
	}

	protected function qry_answerable_subtask($task_ids){
		$component_ids = "1, 2, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15";
		$task_id_str = 0;
        if(count($task_ids) > 0){
            $task_id_str = implode(",",$task_ids);
        }
		$fields = "subTask_ID, task_ID, component_ID, complabel, sublabel, required, placeholder, text_alignment, text_size, min, max, sequence, isRemoved";
		$where = "task_ID IN ($task_id_str) AND isRemoved = 0 AND component_ID IN ($component_ids)";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_subtask", 'sequence ASC');
	}

	protected function qry_process_component_subtask($sub_task_ids){
		$sub_task_id_str = 0;
        if(count($sub_task_ids) > 0){
            $sub_task_id_str = implode(",",$sub_task_ids);
        }
		$fields = "componentSubtask_ID, subTask_ID, compcontent, sequence, correctAnswer, isRemoved";
		$where = "subTask_ID IN ($sub_task_id_str) AND isRemoved = 0";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_component_subtask", 'componentSubtask_ID ASC');
	}

	protected function qry_process_answers($checklist_id, $sub_task_ids)
	{
		$sub_task_id_str = 0;
        if(count($sub_task_ids) > 0){
            $sub_task_id_str = implode(",",$sub_task_ids);
        }
		$fields = "answer_ID, subtask_ID, answer, answeredBy, dateTimeAnswered";
		$where = "checklist_ID = $checklist_id AND subTask_ID IN ($sub_task_id_str)";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_answer", "subtask_ID ASC");
	}

	protected function qry_process_subtask_answers($checklist_id, $sub_task_ids)
	{
		$sub_task_id_str = 0;
        if(count($sub_task_ids) > 0){
            $sub_task_id_str = implode(",",$sub_task_ids);
        }
		$fields = "DISTINCT(subtask_ID), answer_ID, answer, answeredBy, dateTimeAnswered";
		$where = "checklist_ID = $checklist_id AND subTask_ID IN ($sub_task_id_str)";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_answer", "subtask_ID ASC");
		// return $this->general_model->custom_query("SELECT DISTINCT(subtask_ID), answer_ID, answer, answeredBy, dateTimeAnswered FROM tbl_processST_answer WHERE checklist_ID = $checklist_id AND subTask_ID IN ($sub_task_id_str) GROUP BY subtask_ID");
	}

	protected function qry_process_answer_key($process_id){
		$fields = "answerkey_ID, subtask_ID, process_ID, correctAnswer, answeredBy, dateTimeSet";
		$where = "process_ID = $process_id";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_irecruit_answerKey");
	}

	protected function qry_process_scores($process_id){
		$fields = "totalscore_ID, subtask_ID, totalScore, task_ID, process_ID, setBy, dateTimeUpdated";
		$where = "process_ID = $process_id";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_irecruit_totalscore");
	}

	protected function qry_process_task_scores($process_id, $task_id){
		$fields = "totalscore_ID, subtask_ID, totalScore, task_ID, process_ID, setBy, dateTimeUpdated";
		$where = "process_ID = $process_id AND task_ID = $task_id";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_irecruit_totalscore");
	}

	protected function qry_process_answer_score($checklist_id, $sub_task_ids){
		$sub_task_id_str = 0;
        if(count($sub_task_ids) > 0){
            $sub_task_id_str = implode(",",$sub_task_ids);
        }
		$fields = "scores.score_ID, scores.subtask_ID, scores.checklist_ID, scores.score, scores.checked_by, scores.dateTimeChecked, sub.task_ID";
		$where = "sub.subTask_ID = scores.subtask_ID AND scores.checklist_ID = $checklist_id AND scores.subtask_ID IN ($sub_task_id_str)";
		$table = "tbl_processST_irecruit_scores scores, tbl_processST_subtask sub";
		return $this->general_model->fetch_specific_vals($fields, $where, $table);
	}

	protected function qry_process_passing_score($process_id){
		$fields = "passingScore_ID, process_ID, score, updated_by, datetime_added";
		$where = "process_ID = $process_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_processST_irecruit_passing_score");
	}

	public function get_process_details($process_id){
		$task = $this->qry_process_tasks($process_id);
		$task_ids = array_column($task, 'task_ID');
		$subtask = $this->qry_process_subtasks($task_ids);
		$subtask_component = $this->qry_process_component_subtask(array_column($subtask, 'subTask_ID'));
		// var_dump($subtask);
		// var_dump($subtask_component);
	}

	protected function get_employee_exam_details($checklist_ID){
		$fields = "ass.assignedTo";
		$where = "ass.type = 'checklist' AND ass.folProCheTask_ID = $checklist_ID";
		$table = "tbl_processST_assignment ass";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}

	protected function get_applicant_exam_details($checklist_exam_id){
		$fields = "app.apid, app.fname, app.mname, app.lname, pos.pos_details";
		$where = "pos.pos_id = irecruitApp.position_ID AND app.apid = irecruitApp.apid AND irecruitApp.application_ID = exam.application_ID AND exam.checklistExam_ID = $checklist_exam_id";
		$table = "tbl_applicant app, tbl_irecruit_application irecruitApp, tbl_irecruit_exam exam, tbl_position pos";
		return $this->general_model->fetch_specific_val($fields, $where, $table);
	}
	protected function get_prev_next_exam_item($sequence, $process_id){
		$fields = "task_ID, process_ID, taskTitle";
		$where = "process_ID = $process_id AND sequence IN ($sequence) AND isRemoved = 0";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_processST_task", "sequence ASC");
	}

	public function layout_item_footer_btn($order_val = 0, $orig_sequence = 0, $process_id = 0, $exam_page_type){
		$sequence = 0;
		$footer_btn = "";
		$process_details = $this->qry_process_details($process_id);
		// var_dump($process_details);dfgf
		$next_button_label = '<span><span>Next<span>&nbsp;&nbsp<i class="fa fa-chevron-right"></i></span>';
		$prev_button_label = '<span><i class="fa fa-chevron-left"></i><span>Previous</span></span>';
		$submit_btn ="type='button'";
		if(count($process_details) > 0){
			if((int)$process_details->processType_ID == 6){
				$next_button_label = 'Next';
				$prev_button_label = 'Previous';
				if($exam_page_type == 2){
					$submit_btn = 'type="submit"';
				}
			}
		}
		if($order_val == 1){
			$sequence = $orig_sequence + 1;
		}else if($order_val == 2){
			$sequence = $orig_sequence - 1;
			// $prev = $next = $this->get_prev_next_exam_item($sequence, $process_id);
		}else if($order_val == 3){
			$prev = $orig_sequence - 1;
			$next = $orig_sequence + 1;
			$sequence = $prev.",".$next;
		}
		$prev_next_items = $this->get_prev_next_exam_item($sequence, $process_id);
		if(count($prev_next_items) > 0){
			if($order_val == 1){
				$footer_btn = '<button '.$submit_btn.' class="btn btn-primary btn-md m-btn m-btn--icon mx-1 footer_btn submitButton" data-submittype="next" data-taskid="'.$prev_next_items[0]->task_ID.'">'.$next_button_label.'</button>';
			}
			if($order_val == 2){
				$footer_btn = '<button type="button" class="btn btn-primary btn-md m-btn m-btn--icon mx-1 footer_btn noValidationBtn" data-taskid="'.$prev_next_items[0]->task_ID.'" data-submittype="prev">'.$prev_button_label.'</button>';
				if((int)$process_details->processType_ID == 6 && $exam_page_type == 2){
					$footer_btn .= '<button type="submit" class="btn btn-success btn-md m-btn footer_btn m-btn--icon mx-1 submitButton" data-submittype="submit">Submit</button>';
				}
			}
			if($order_val == 3){
				$footer_btn .= '<button type="button" class="btn btn-primary btn-md m-btn m-btn--icon mx-1 footer_btn noValidationBtn" data-taskid="'.$prev_next_items[0]->task_ID.'" data-submittype="prev">'.$prev_button_label.'</button>';
				if(count($prev_next_items) > 1){
					$footer_btn .= '<button '.$submit_btn.' class="btn btn-primary btn-md m-btn m-btn--icon mx-1 footer_btn submitButton" data-submittype="next" data-taskid="'.$prev_next_items[1]->task_ID.'">'.$next_button_label.'</button>';
				}
			}
		}
		return $footer_btn;
	}

	public function no_answer_items(){
		$checklist_id = $this->input->post('checklistId');
		$process_id = $this->input->post('processId');
		$tasks = $this->qry_process_tasks($process_id);
		$answerable_subtasks = $this->qry_answerable_subtask(array_column($tasks, 'task_ID')); // subtasks that are answerable
		$subtask_answers = $this->qry_process_subtask_answers($checklist_id, array_column($answerable_subtasks, 'subTask_ID'));
		$data['no_answer'] = '';
		foreach($tasks as $task_index=>$tasks_row){
			$subtask_no_answer = [];
			$subtask_no_answer_count = 0;
			$task_row_id = $tasks_row->task_ID;
			$task_answerable_subtasks = array_merge(array_filter($answerable_subtasks, function ($e) use ($task_row_id) {
					return ($e->task_ID == $task_row_id);
				}
			));
			if(count($task_answerable_subtasks) > 0){
				foreach($task_answerable_subtasks as $task_answerable_subtasks_index => $task_answerable_subtasks_row){
					$subtask_row_id = $task_answerable_subtasks_row->subTask_ID;
					$subtasks_specific_answer = array_merge(array_filter($subtask_answers, function ($e) use ($subtask_row_id) {
							return ($e->subtask_ID == $subtask_row_id);
						}
					));
					if(count($subtasks_specific_answer) < 1){
						$task_answerable_subtasks_row->{'number'} =  $task_answerable_subtasks_index+1;
						$subtask_no_answer[$subtask_no_answer_count] = $task_answerable_subtasks_row;
						$subtask_no_answer_count++;
					}
				}
			}
			if(count($subtask_no_answer) > 0){
				$tasks_index = $task_index+1;
				$task_item_title = "Untitled Test Set $tasks_index";
				if (trim($tasks_row->taskTitle) != '') {
					$task_item_title = trim($tasks_row->taskTitle);
				}
				$subtask_no_answer_num = '';
				foreach($subtask_no_answer as $subtask_no_answer_index=>$subtask_no_answer_row){
					$subtask_no_answer_num .= '<div class="col-1 p-2 m-1 noAnswerItem" data-taskid="'.$subtask_no_answer_row->task_ID.'" data-subtaskid="'.$subtask_no_answer_row->subTask_ID.'" style="border: 1px solid #9E9E9E;border-radius: 0.2rem;color: #585757;font-weight: 700;">'.$subtask_no_answer_row->number.'</div>';
				}
				$data['no_answer'] .= '<div class="col-12 my-2">';
				$data['no_answer'] .= '<div class="row">';
				$data['no_answer'] .= '<span class="ml-3" style="font-size: 22px; font-weight: 500; color: #5d4747;">'.$task_item_title.'</span>';
				$data['no_answer'] .= '<div class="col">';
				$data['no_answer'] .= '<hr style="margin-top: 1.2rem !important;">';
				$data['no_answer'] .= '</div>';
				$data['no_answer'] .= '</div>';
				$data['no_answer'] .= '</div>';

				$data['no_answer'] .= '<div class="col-12">';
				$data['no_answer'] .= '<div class="row text-center px-3">';
				$data['no_answer'] .= $subtask_no_answer_num;
				$data['no_answer'] .= '</div>';
				$data['no_answer'] .= '</div>';
			}
		}
		echo json_encode($data);
	}

	public function exam_score_summary(){
		$answer_score_total = 0;
		$process_id = $this->input->post('processId');
		$checklist_id =  $this->input->post('checklistId');
		$tasks = $this->qry_process_tasks($process_id);
		$answerable_subtasks = $this->qry_answerable_subtask(array_column($tasks, 'task_ID'));
		$process_scores = $this->qry_process_scores($process_id);
		$answer_score = $this->qry_process_answer_score($checklist_id, array_column($answerable_subtasks, 'subTask_ID'));
		$passing_score = $this->qry_process_passing_score($process_id);
		$data['summary_content'] = '';
		$passing_score_value = 'No Passing Score Set';
		$exam_evaluation = 'Cannot be evaluated';
		$evaluation_color = "#F44336";
		$item_score_total = array_sum(array_map('intval', array_column($process_scores, 'totalScore')));
		$answer_score_total = array_sum(array_map('intval', array_column($answer_score, 'score')));
		if(count($passing_score) > 0){
			$passing_score_value = $passing_score->score;
			if($passing_score->score > $answer_score_total){
				$exam_evaluation = "Failed";
			}else{
				$exam_evaluation = "Passed";
				$evaluation_color = "#4caf50";
			}
		}
		if(count($tasks) > 0){
			$task_list = "";
			$test_set_score = "";
			$table_body = "";
			foreach($tasks as $tasks_index=>$tasks_row){
				$task_row_id = $tasks_row->task_ID;
				$task_item_title = "Untitled Test Set $tasks_index";
				$task_item_score_total = 0;
				$task_answer_score_total = 0;
				$task_scores = [];
				$task_scores = array_merge(array_filter($process_scores, function ($e) use ($task_row_id) {
						return ($e->task_ID == $task_row_id);
					}
				));
				$task_answer_scores = array_merge(array_filter($answer_score, function ($e) use ($task_row_id) {
						return ($e->task_ID == $task_row_id);
					}
				));
				if (trim($tasks_row->taskTitle) != '') {
					$task_item_title = trim($tasks_row->taskTitle);
				}
				$task_item_score_total = array_sum(array_map('intval', array_column($task_scores, 'totalScore')));
				$task_answer_score_total = array_sum(array_map('intval', array_column($task_answer_scores, 'score')));
				$table_body .= '<tr>';
				$table_body .= '<td class="text-left pl-3">'.$task_item_title.'</td>';
				$table_body .= '<td class="text-center">'.$task_item_score_total.'</td>';
				$table_body .= '<td class="text-center">'.$task_answer_score_total.'</td>';
				$table_body .= '</tr>';
			}
			$table_body .=  '<tr style="font-size: 1rem;font-weight: 600;background-color: beige;">';
			$table_body .= '<td class="text-right pl-3">TOTAL</td>';
			$table_body .= '<td class="text-center">'.$item_score_total.'</td>';
			$table_body .= '<td class="text-center">'.$answer_score_total.'</td>';
			$table_body .= '</tr>';
		}
		$data['summary_content'] .= '<div class="col-12">';
		$data['summary_content'] .= '<h5 class="px-0">Passing Score: '.$passing_score_value.'</h5>';
		$data['summary_content'] .= '</div>';
		$data['summary_content'] .= '<div class="col-12 mt-2">';
		$data['summary_content'] .= '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">';
		$data['summary_content'] .= '<thead style="background: #555;color: white;">';
		$data['summary_content'] .= '<tr>';
		$data['summary_content'] .= '<th class="text-center">Test Set</th>';
		$data['summary_content'] .= '<th class="text-center">Items</th>';
		$data['summary_content'] .= '<th class="text-center">Score</th>';
		$data['summary_content'] .= '</tr>';
		$data['summary_content'] .= '</thead>';
		$data['summary_content'] .= '<tbody>';
		$data['summary_content'] .= $table_body;
		$data['summary_content'] .= '</tbody>';
		$data['summary_content'] .= '</table>';
		$data['summary_content'] .= '</div>';
		$data['summary_content'] .= '<div class="col-12">';
		$data['summary_content'] .= '<h5 class="px-0">Exam Evaluation: <span style="color:'.$evaluation_color.' ;">'.$exam_evaluation.'</span></h5>';
		$data['summary_content'] .= '</div>';
		echo json_encode($data);
	}

	public function get_process_exam_details($exam_page_type, $process_id, $checklist_id){
		$exam_details['exam_page_type'] = $exam_page_type;
		$exam_details['process_id'] = $process_id;
		$exam_details['checklist_id'] = $checklist_id;
		$exam_details['exam_type'] = 0;
		$exam_details['exam_title'] = "Untitled Exam";
		$exam_details['exam_examinee_name'] = "No Examinee Name";
		$exam_details['exam_examinee_name_label'] = "Examinee"; 
		$exam_details['exam_examinee_position'] = "No Examinee Position";
		$exam_details['exam_started'] = "No Start Datetime details";
		$exam_details['exam_ended'] = "";
		$exam_details['exam_test_sets'] = "";
		$exam_details['exam_item'] = "";
		$exam_details['exam_answer_ratio'] = "";
		$exam_details['exam_score_ratio'] = "";
		$exam_details['exam_passing_score'] = 0;
		$exam_details['test_set_list'] = "";
		$exam_details['footer_btn'] = "";
		$process_score_total = 0;
		$answer_score_total = 0;
		$total_score_ratio = "";
		$exam_evaluation = "Passed";
		$exam_evaluation_color = "text-success";
		$exam_evaluation_icon = "fa-check";
		$process_details = $this->qry_process_details($process_id);
		if(($checklist_id !== 0) && ($exam_page_type !== 1)){
			$checklist_details = $this->qry_process_exam_checklist_details($checklist_id);
			if(count($checklist_details) > 0){
				$exam_details['exam_type'] = $checklist_details->examtype_ID;
				if($checklist_details->examtype_ID == 2){
					$emp_exam_details = $this->get_employee_exam_details($checklist_id);
					$emp_details = $this->get_emp_details_via_user_id($emp_exam_details->assignedTo);
					$exam_details['exam_examinee_name'] = $emp_details->fname." ".$emp_details->lname;
					$exam_details['exam_examinee_name_label'] = "Employee"; 
					$exam_details['exam_examinee_position'] = $emp_details->positionDescription;
				}else if($checklist_details->examtype_ID == 1){
					$emp_exam_details = $this->get_applicant_exam_details($checklist_details->checklistExam_ID);
					$exam_details['exam_examinee_name_label'] = "Applicant"; 
					$exam_details['exam_examinee_name'] = $emp_exam_details->fname." ".$emp_exam_details->lname;
					$exam_details['exam_examinee_position'] = $emp_exam_details->pos_details;
				}
				$exam_details['exam_started'] = $checklist_details->start;
				$exam_details['exam_ended'] = $checklist_details->end;
			}
		}
		$tasks = $this->qry_process_tasks($process_id);
		if(count($tasks) > 0){
			$task_list = "";
			$task_item_title = "";
			foreach($tasks as $tasks_index=>$tasks_row){
				$task_num = $tasks_index +1;
				$task_item_title = "Untitled Test Set $task_num";
				if (trim($tasks_row->taskTitle) != '') {
					$task_item_title = trim($tasks_row->taskTitle);
				}
				$task_list .= '<li class="m-menu__item taskListMenu" aria-haspopup="true" data-taskid="'.$tasks_row->task_ID.'">';
				$task_list .= '<a class="m-menu__link " style="cursor: unset;"><i class="m-menu__link-icon fa fa-dot-circle-o"></i>';
				$task_list .= '<span class="m-menu__link-title">';
				$task_list .= '<span class="m-menu__link-wrap"><span class="m-menu__link-text">'.$task_item_title.'</span></span>';
				$task_list .= '</span>';
				$task_list .= '</a>';
				$task_list .= '</li>';
			}
			$exam_details['test_set_list'] = $task_list;
		}
		$exam_details['exam_tasks'] = $tasks;
		$exam_details['exam_test_sets'] = count($tasks);
		if(count($process_details) > 0){
			if (trim($process_details->processTitle) != '') {
				$exam_details['exam_title'] = $process_details->processTitle;
			}
		}
		if($exam_page_type == 3){
			$this->check_all_scores($process_id, $checklist_id);
		}
		$answerable_subtasks = $this->qry_answerable_subtask(array_column($tasks, 'task_ID')); // subtasks that are answerable
		$subtask_answers = $this->qry_process_subtask_answers($checklist_id, array_column($answerable_subtasks, 'subTask_ID'));
		// var_dump(count($answerable_subtasks));
		$exam_details['exam_item'] = count($answerable_subtasks);
		$exam_details['exam_answer_ratio'] = count($subtask_answers)." out of ".count($answerable_subtasks);
		$process_scores = $this->qry_process_scores($process_id);
		$answer_score = $this->qry_process_answer_score($checklist_id, array_column($answerable_subtasks, 'subTask_ID'));
		$process_score_total = array_sum(array_map('intval', array_column($process_scores, 'totalScore')));
		$answer_score_total = array_sum(array_map('intval', array_column($answer_score, 'score')));
		$exam_details['exam_score_ratio'] = $answer_score_total."/".$process_score_total;
		$passing_score = $this->qry_process_passing_score($process_id);
		if(count($passing_score) > 0){
			$exam_details['exam_passing_score'] = $passing_score->score;
			if($passing_score->score > $answer_score_total){
				$exam_evaluation = "Failed";
				$exam_evaluation_color = "text-danger";
				$exam_evaluation_icon = 'fa-remove';
			}
		}
		if(($exam_page_type == 3) || ($exam_page_type == 4)){
			$total_score_ratio .= '<div class="m-alert m-alert--icon alert alert-accent mb-3 col-12 score_details" role="alert" style="background-color: #ffffff; border-color: unset; color: #636363;">';
			$total_score_ratio .= '<div class="m-alert__text" style="font-size: 2rem;">';
			$total_score_ratio .= 'Total Score: <strong id="changeable_Score">'.$answer_score_total.'</strong>/<strong>'.$process_score_total.'</strong><span id="eval_color_change" class="ml-3 '.$exam_evaluation_color.'" style="font-size: 1rem; font-weight: 600 !important;"><i id="ins_icon" class="fa '.$exam_evaluation_icon.' mr-1"></i><span id="overall_evaluation_ex">'.$exam_evaluation.'</span></span>';
			$total_score_ratio .= '</div>	';
			$total_score_ratio .= '<div class="m-alert__actions" style="width: 160px;">';
			$total_score_ratio .= '<button type="button" class="btn btn-metal btn-sm m-btn m-btn--pill m-btn--wide score_details_btn" id="score_summary">View Summary';
			$total_score_ratio .= '</button>';
			$total_score_ratio .= '</div>';
			$total_score_ratio .= '</div>';
		}
		$exam_details['total_score_ratio'] = $total_score_ratio;
		return $exam_details;
	}

	public function process_scoring_layout($subtask_component_id, $input_field_val, $exam_page_type, $subtask_final_answer_score, $subtask_final_set_score, $subtask_id, $task_id){
		$score_content = "";
		$score_content .= '<div class="row">';
		$score_content .= '<div class="col-5 my-2 px-0">';
		$score_content .= '<div class="form-group m-form__group row mb-0">';
		$score_content .= '<label for="example-number-input" class="col-2 col-form-label font-italic" style="font-size: 0.9rem;">Score:</label>';
		if(($subtask_component_id == 6) || ($subtask_component_id == 7) || ($subtask_component_id == 10) || ($subtask_component_id == 11) || ($subtask_component_id == 13) || ($subtask_component_id == 14)){
			$score_content .= '<div class="col-1 col-form-label">';
			$score_content .= $subtask_final_answer_score.'/'.$subtask_final_set_score;
			$score_content .= '</div>';
		}else if(($subtask_component_id == 1) || ($subtask_component_id == 2) || ($subtask_component_id == 5) || ($subtask_component_id == 8) || ($subtask_component_id == 12) || ($subtask_component_id == 15)){
			// if($subtask_component_id == 8){
			// 	$score_content .= '<div class="col-3 mr-0 pr-0">';
			// 	$score_content .= '<input class="form-control subtaskScore" type="number" value="'.$subtask_final_answer_score.'" data-subtaskid="'.$subtask_id.'">';
			// 	$score_content .= '</div>';
			// 	$score_content .= '<div class="col-1 col-form-label">';
			// 	$score_content .= '/'.$subtask_final_set_score;
			// 	$score_content .= '</div>';
			// }
			if($subtask_component_id == 8){
				// var_dump(strtolower(trim($input_field_val)));
				if($exam_page_type == 4){
					$score_content .= '<div class="col-1 col-form-label">';
					$score_content .= $subtask_final_answer_score.'/'.$subtask_final_set_score;
					$score_content .= '</div>';
				}else{
					$score_content .= '<div class="col-3 mr-0 pr-0">';
					$score_content .= '<input class="form-control subtaskScore" type="number" value="'.$subtask_final_answer_score.'" data-subtaskid="'.$subtask_id.'" max="'.$subtask_final_set_score.'" min="0" data-taskid="'.$task_id.'">';
					$score_content .= '<span style="color:red;font-weight: 500;font-size: smaller" id="erlabel'.$subtask_id.'"></span>';
					$score_content .= '</div>';
					$score_content .= '<div class="col-1 col-form-label">';
					$score_content .= '/'.$subtask_final_set_score;
					$score_content .= '</div>';
				}
			}else if((strtolower(trim($input_field_val)) == '') || ($exam_page_type == 4)){
				$score_content .= '<div class="col-1 col-form-label">';
				$score_content .= $subtask_final_answer_score.'/'.$subtask_final_set_score;
				$score_content .= '</div>';
			}else{
				$score_content .= '<div class="col-3 mr-0 pr-0">';
				$score_content .= '<input class="form-control subtaskScore" type="number" value="'.$subtask_final_answer_score.'" data-subtaskid="'.$subtask_id.'" max="'.$subtask_final_set_score.'" min="0" data-taskid="'.$task_id.'">';
				$score_content .= '<span style="color:red;font-weight: 500;font-size: smaller" id="erlabel'.$subtask_id.'"></span>';
				$score_content .= '</div>';
				$score_content .= '<div class="col-1 col-form-label">';
				$score_content .= '/'.$subtask_final_set_score;
				$score_content .= '</div>';
			}
			// }
		}
		$score_content .= '</div>';
		$score_content .= '</div>';
		$score_content .= '</div>';
		return $score_content;
	}

	protected function add_item_score($subtask_id, $checklist_id, $subtask_final_answer_score){
		$data['subtask_ID'] = $subtask_id;
		$data['checklist_ID'] = $checklist_id;
		$data['score'] = $subtask_final_answer_score;
		$data['checked_by'] = $this->session->userdata('uid');
		return $this->general_model->insert_vals($data, "tbl_processST_irecruit_scores");
	}

	protected function update_item_score($score_id, $subtask_final_answer_score){
		$data['score'] = $subtask_final_answer_score;
		$where = "score_ID = $score_id";
		return $this->general_model->update_vals($data, $where, 'tbl_processST_irecruit_scores');
	}

	protected function check_item_score_exist($subtask_id, $checklist_id){
		$fields = "*";
		$where = "checklist_ID = $checklist_id AND subtask_ID = $subtask_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_processST_irecruit_scores");
	}

	protected function save_item_score($subtask_id, $checklist_id, $subtask_final_answer_score){
		$check_item_score = $this->check_item_score_exist($subtask_id, $checklist_id);
		$data['save_stat'] = 0;
		if(count($check_item_score) > 0){
			// update
			$data['action'] = 'update';
			$data['save_stat'] = $this->update_item_score($check_item_score->score_ID, $subtask_final_answer_score);
		}else{
			// insert
			$data['save_stat'] = $this->add_item_score($subtask_id, $checklist_id, $subtask_final_answer_score);
			$data['action'] = 'add';
		}
	}

	protected function initial_save_item_score($subtask_id, $checklist_id, $subtask_final_answer_score){
		$check_item_score = $this->check_item_score_exist($subtask_id, $checklist_id);
		$data['save_stat'] = 0;
		if(count($check_item_score) > 0){
			// do not add
		}else{
			// insert
			$this->add_item_score($subtask_id, $checklist_id, $subtask_final_answer_score);
		}
	}

	public function check_all_scores($process_id, $checklist_id){
	// public function check_all_scores(){
		// $process_id = 35;
		// $checklist_id = 67;
		$tasks = $this->qry_process_tasks($process_id);
		$answer_key = $this->qry_process_answer_key($process_id);
		$process_scores = $this->qry_process_scores($process_id);
		$answerable_subtasks = $this->qry_answerable_subtask(array_column($tasks, 'task_ID'));
		// var_dump($answerable_subtasks);
		// var_dump(array_column($answerable_subtasks, 'subTask_ID'));
		$subtask_answers = $this->qry_process_subtask_answers($checklist_id, array_column($answerable_subtasks, 'subTask_ID'));
		$subtask_components = $this->qry_process_component_subtask(array_column($answerable_subtasks, 'subTask_ID'));
		$answer_score = $this->qry_process_answer_score($checklist_id, array_column($answerable_subtasks, 'subTask_ID'));
		// var_dump($subtask_answers);
		// var_dump($answerable_subtasks);
		foreach($answerable_subtasks as $answerable_subtasks_row){
			// var_dump($answerable_subtasks_row);
			$correct_answer = "";
			$subtask_final_answer_score = 0;
			$subtask_orig_final_answer_score = 0;
			$subtask_id = $answerable_subtasks_row->subTask_ID;
			$task_id = $answerable_subtasks_row->task_ID;
			$input_field_val = "";
			// var_dump($subtask_answers);
			$subtask_answer = array_merge(array_filter($subtask_answers, function ($e) use ($subtask_id) {
				return ($e->subtask_ID == $subtask_id);
			}
			));
			// var_dump($subtask_answer);
			$subtask_answer_key = array_merge(array_filter($answer_key, function ($e) use ($subtask_id) {
				return ($e->subtask_ID == $subtask_id);
				}
			));
			if((count($subtask_answer) > 0) && ($answerable_subtasks_row->component_ID  != 8)){ // GET FIELD ANSWER (except field with multiple answers such as checkbox)
				$input_field_val = $subtask_answer[0]->answer;
			}
			if(count($subtask_answer_key) > 0){
				foreach($subtask_answer_key as $subtask_answer_key_row){
					$correct_answer = $subtask_answer_key_row->correctAnswer;
				}
			}
			$subtask_set_score = array_merge(array_filter($process_scores, function ($e) use ($subtask_id, $task_id, $process_id) {
					return (($e->subtask_ID == $subtask_id) && ($e->task_ID == $task_id) && ($e->process_ID == $process_id));
				}
			));
			if(count($subtask_set_score) > 0){
				foreach($subtask_set_score as $subtask_set_score_row){
					$subtask_final_set_score = $subtask_set_score_row->totalScore;
					$subtask_totalscore_id = $subtask_set_score_row->totalscore_ID;
				}
			}
			$subtask_answer_score = array_merge(array_filter($answer_score, function ($e) use ($subtask_id, $checklist_id) {
					return (($e->subtask_ID == $subtask_id) && ($e->checklist_ID == $checklist_id));
				}
			));
			if(($answerable_subtasks_row->component_ID == 6) || ($answerable_subtasks_row->component_ID == 7) || ($answerable_subtasks_row->component_ID == 8)){
				$sub_components = array_merge(array_filter($subtask_components, function ($e) use ($subtask_id) {
						return ($e->subTask_ID == $subtask_id);
					}
				));
			}
			if((count($subtask_set_score) > 0) && (count($subtask_answer_score) > 0)){
				foreach($subtask_answer_score as $subtask_answer_score_row){
					$subtask_orig_final_answer_score = $subtask_answer_score_row->score;
				}
			}
			if(($answerable_subtasks_row->component_ID == 1) || ($answerable_subtasks_row->component_ID == 5) || ($answerable_subtasks_row->component_ID == 7) || ($answerable_subtasks_row->component_ID == 10) || ($answerable_subtasks_row->component_ID == 11) || ($answerable_subtasks_row->component_ID == 13) || ($answerable_subtasks_row->component_ID == 14)){
				if(strtolower(trim($input_field_val)) == ''){
					$subtask_final_answer_score = 0;
				}else if(strtolower(trim($correct_answer)) == strtolower(trim($input_field_val))){
					$subtask_final_answer_score = $subtask_final_set_score;
				}else{
					if($subtask_orig_final_answer_score > 0){
						$subtask_final_answer_score = $subtask_orig_final_answer_score;
					}else{
						$subtask_final_answer_score = 0;
					}
				}
			}else if($answerable_subtasks_row->component_ID == 2){
				if(strtolower(trim($input_field_val)) == ''){
					$subtask_final_answer_score = 0;
				}else{
					if($subtask_orig_final_answer_score > 0){
						$subtask_final_answer_score = $subtask_orig_final_answer_score;
					}else{
						$subtask_final_answer_score = 0;
					}
				}
			}else if($answerable_subtasks_row->component_ID == 6){
				$subtask_final_answer_score = 0;
				if(count($sub_components) > 0){
				// var_dump($input_field_val);
					if(trim($input_field_val) == ''){
						$subtask_final_answer_score = 0;
					}
					$correct_checkbox_bool = 0;
					// var_dump($sub_components);
					foreach($sub_components as $sub_components_row){
						$sub_component_label = $sub_components_row->compcontent;
						if(($sub_component_label == $input_field_val) && ($sub_component_label == $correct_answer)){
							$correct_checkbox_bool += 1;
						}
					}
					// var_dump($correct_checkbox_bool);
					if($correct_checkbox_bool){
						$subtask_final_answer_score = $subtask_final_set_score;
					}
				}
				// var_dump($subtask_final_answer_score);
			}else if($answerable_subtasks_row->component_ID == 8){
				$correct_checkbox_answers = array_column($subtask_answer_key, 'correctAnswer');
				$checkbox_answers = array_column($subtask_answer, 'answer');
				if(count($sub_components) > 0){
					$incorrect_checkbox = 0;
					foreach($sub_components as $sub_components_row){
						$option_score = 0;
						$sub_component_label = $sub_components_row->compcontent;
						if(in_array($sub_component_label, $checkbox_answers)){
							$option_score += 1;
						}
						if(in_array($sub_component_label, $correct_checkbox_answers)){
							$option_score += 2;
						}
						if($option_score == 1 || $option_score == 2){
							$incorrect_checkbox += 1;
						}
					}
					if($incorrect_checkbox > 0){
						$subtask_final_answer_score = 0;
					}else{
						$subtask_final_answer_score = $subtask_final_set_score;
					}
				}
			}else if($answerable_subtasks_row->component_ID == 12){
				$bg_color = "";
				$text_color = "";
				$correction_label = "";
				$correction_icon = "";
				$final_answer = "";
				if(strtolower(trim($input_field_val)) == ''){
					$subtask_final_answer_score = 0;
				}else{
					$sanitized_email = filter_var($input_field_val, FILTER_SANITIZE_EMAIL);
					if (filter_var($sanitized_email, FILTER_VALIDATE_EMAIL) === false || $sanitized_email != $input_field_val) {
						$subtask_final_answer_score = 0;
					}else{
						$subtask_final_answer_score = $subtask_final_set_score;
					}
				}
			}else if($answerable_subtasks_row->component_ID == 15){
				$subtask_final_answer_score = 0;
				// if(trim($input_field_val) == ''){
				// 	$subtask_final_answer_score = 0;
				// }else{
				// 	$subtask_final_answer_score = $subtask_final_set_score;
				// }
			}
			// var_dump($subtask_final_answer_score);
			$this->save_item_score($subtask_id, $checklist_id, $subtask_final_answer_score);
		}
	}

	public function process_answers_layout($subtask_component_id, $input_field_val, $subtask_id, $task_id, $process_id, $checklist_id, $exam_page_type, $sub_components, $subtask_answers){
		// var_dump($input_field_val);
		$correction_content = "";
		$subtask_content_answer = "";
		$correction_data['content']= "";
		$correction_data['specific_correction_layout']= "";
		$bg_color = "";
		$text_color = "";
		$correction_label = "";
		$correction_icon = "";
		$final_answer = "";
		$show_correct_answer = "";
		if(($subtask_component_id == 1) || ($subtask_component_id == 5) || ($subtask_component_id == 7) || ($subtask_component_id == 10) ||  ($subtask_component_id == 11) || ($subtask_component_id == 13) || ($subtask_component_id == 14)){
			if(strtolower(trim($input_field_val)) == ''){
				$subtask_final_answer_score = 0;
				$bg_color = "bg-no-answer";
				$text_color = "text-danger";
				$correction_label = "No Answer";
				$final_answer = '';
			}else{
				$bg_color = "bg-correct";
				$text_color = "text-success";
				$final_answer = $input_field_val;
			}
			$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
			$correction_content .= '<div class="col-6 my-2 py-2 '.$bg_color.'">';
			$correction_content .= $final_answer;
			$correction_content .= '</div>';
			$correction_content .= '<div class="col-6 my-2 py-2 '.$text_color.'">';
			$correction_content .= $correction_label;
			$correction_content .= '</div>';
		}else if($subtask_component_id == 2){
			$bg_length = "col-8";
			if(strtolower(trim($input_field_val)) == ''){
				$subtask_final_answer_score = 0;
				$bg_color = "bg-no-answer";
				$text_color = "text-danger";
				$correction_label = "No Answer";
				$final_answer = '';
				$bg_length = "col-6";
			}else{
				$bg_color = "bg-correct";
				$text_color = "text-success";
				$correction_label = "Correct";
				$final_answer = $input_field_val;
			}
			$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
			$correction_content .= '<div class="'.$bg_length.' my-2 py-2 '.$bg_color.'">';
			$correction_content .= $final_answer;
			$correction_content .= '</div>';
			$correction_content .= '<div class="col-4 my-2 py-2 '.$text_color.'">';
			$correction_content .= $correction_label;
			$correction_content .= '</div>';
		}else if($subtask_component_id == 6){
			if(count($sub_components) > 0){
				$show_no_answer = "";
				$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
				if(trim($input_field_val) == ''){
					$show_no_answer .= '<div class="col-6 my-2 py-2 bg-no-answer">';
					$show_no_answer .= '</div>';
					$show_no_answer .= '<div class="col-6 my-2 py-2 text-danger">';
					$show_no_answer .= 'No Answer';
					$show_no_answer .= '</div>';
				}
				$correction_content .= $show_no_answer;
				foreach($sub_components as $sub_components_row){
					$bg_color = "";
					$text_color = "";
					$correction_label = "";
					$checked = "";
					$sub_component_label = $sub_components_row->compcontent;
					if(trim($sub_components_row->compcontent) == '') {
						$sub_component_label = "No Option Label";
					}
					if($sub_component_label == $input_field_val){
						$bg_color = "bg-correct";
						$checked = 'checked="checked"';
						$text_color = "text-success";
					}
					$correction_content .= '<div class="col-6 my-2 py-2 '.$bg_color.'" style="word-break: break-word;">';
					$correction_content .= '<label class="m-radio mb-0">';
					$correction_content .= '<input type="radio" '.$checked.' class="nochange">'.$sub_component_label;
					$correction_content .= '<span></span>';
					$correction_content .= '</label>';
					$correction_content .= '</div>';
					$correction_content .= '<div class="col-6 my-2 py-2 '.$text_color.'">';
					$correction_content .= $correction_label;
					$correction_content .= '</div>';
				}
			}
		}else if($subtask_component_id == 8){
			$checkbox_answers = array_column($subtask_answers, 'answer');
			$incorrect_checkbox = 0;
			if(count($sub_components) > 0){
				$show_no_answer = "";
				$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
				if(count($checkbox_answers) < 1){
						$show_no_answer .= '<div class="col-6 my-2 py-2 bg-no-answer">';
						$show_no_answer .= '</div>';
						$show_no_answer .= '<div class="col-6 my-2 py-2 text-danger">';
						$show_no_answer .= 'No Answer';
						$show_no_answer .= '</div>';
				}
				$correction_content .= $show_no_answer;
				foreach($sub_components as $sub_components_row){
					$bg_color = "";
					$text_color = "";
					$correction_label = "";
					$checked = "";
					$option_score = 0;
					$sub_component_label = $sub_components_row->compcontent;
					if (trim($sub_components_row->compcontent) == '') {
						$sub_component_label = "No Option Label";
					}
					if(in_array($sub_component_label, $checkbox_answers)){
						$checked = 'checked="checked"';
						$bg_color = "bg-correct";
					}
					// var_dump($option_score);
					$correction_content .= '<div class="col-6 my-2 py-2 '.$bg_color.'" style="word-break: break-word;">';
					$correction_content .= '<label class="m-checkbox mb-0">';
					$correction_content .= '<input type="checkbox" '.$checked.' class="nochange">'.$sub_component_label;
					$correction_content .= '<span></span>';
					$correction_content .= '</label>';
					$correction_content .= '</div>';
					$correction_content .= '<div class="col-6 my-2 py-2 '.$text_color.'">';
					$correction_content .= $correction_label;
					$correction_content .= '</div>';
				}
			}
		}else if($subtask_component_id == 12){
			$bg_color = "";
			$text_color = "";
			$correction_label = "";
			$final_answer = "";
			if(strtolower(trim($input_field_val)) == ''){
				$bg_color = "bg-no-answer";
				$text_color = "text-danger";
				$correction_label = "No Answer";
				$final_answer = '';
			}else{
				$bg_color = "bg-correct";
				$text_color = "text-success";
				$final_answer = $input_field_val;
			}
			$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
			$correction_content .= '<div class="col-6 my-2 py-2 '.$bg_color.'">';
			$correction_content .= $final_answer;
			$correction_content .= '</div>';
			$correction_content .= '<div class="col-6 my-2 py-2 '.$text_color.'">';
			$correction_content .= $correction_label;
			$correction_content .= '</div>';
		}else if($subtask_component_id == 15){
			if(trim($input_field_val) == ''){
				$correction_content .= '<div class="col-6 my-2 py-2 bg-no-answer">';
				$correction_content .= '</div>';
				$correction_content .= '<div class="col-6 my-2 py-2 text-danger">';
				$correction_content .= 'No File Uploaded';
				$correction_content .= '</div>';
			}else{
				$format_start = strpos($input_field_val, '.');
				$format_name_start = strrpos($input_field_val, '/');
				$file_title_substr = substr($input_field_val, $format_start + 1);
				$format_name_end = $format_start - $format_name_start;
				$file_title = substr($input_field_val, $format_name_start + 1, $format_name_end - 1);
				$file_format = $this->file_format_layout($file_title_substr);
				$file_format_type = $file_format['file_type'];
				$file_link = base_url($input_field_val);
				$correction_content .= '<div class="col-9 p-0">';
				$correction_content .= '<div class="col-12 text-center p-3" style=" border: 1px solid black dotted; border: 2px dashed #b9b9b9;">';
				$correction_content .= '<div class="col-12 text-center fileDiv">';
				$correction_content .= '<button class="btn btn-outline-focus btn-lg  m-btn m-btn--icon m-btn--outline-2x mr-2 uploadedFileBtn" download="">';
				$correction_content .= '<div><a href="'.$file_link.'" class="uploadedFile"><i class="fa fa-file-text-o mr-2"></i>'.$file_format_type.'</a></div>';
				$correction_content .= '</button>';
				$correction_content .= '';
				$correction_content .= '<br><div class="my-2" style="font-size: 0.8rem;">(<span class="font-italic">Click to download and View Uploaded File</span>)</div>';
				$correction_content .= '<div class="col-12 text-center" style="font-size:1.1em;color:#7e61b1;word-break: break-word;">';
				$correction_content .= $file_title;
				$correction_content .= '</div>';
				$correction_content .= '</div>';
				$correction_content .= '</div></div>';
			}
		}
		// $score_content = $this->process_scoring_layout($subtask_component_id, $input_field_val, $exam_page_type, $subtask_final_answer_score, $subtask_final_set_score, $subtask_id, $task_id);
		$subtask_content_answer .= '<div id="corrected_answers_content'.$subtask_id.'" class="col-12 mx-3 checkField" data-subtaskid="'.$subtask_id.'">';
		$subtask_content_answer .= '<div id="subtaskCorrection'.$subtask_id.'" class="row subtaskCorrection" data-subtaskid="'.$subtask_id.'">';
		$subtask_content_answer .= $correction_content;
		$subtask_content_answer .= '</div>';
		$subtask_content_answer .= '</div>';
		$correction_data['content']= $subtask_content_answer;
		$correction_data['specific_correction_layout']= $correction_content;
		return $correction_data;
	}

	public function process_correction_layout($subtask_component_id, $input_field_val, $process_scores, $answer_key, $answer_score , $subtask_id, $task_id, $process_id, $checklist_id, $exam_page_type, $sub_components, $subtask_answers){
		$correct_answer = "";
		$subtask_final_set_score = 0;
		$subtask_final_answer_score = 0;
		$subtask_orig_final_answer_score = 0;
		$subtask_totalscore_id = 0;
		$correction_content = "";
		$score_content = "";
		$subtask_content_answer = "";
		$correction_data['content']= "";
		$correction_data['specific_correction_layout']= "";
		// FILTER ANSWER KEY
		$subtask_answer_key = array_merge(array_filter($answer_key, function ($e) use ($subtask_id) {
				return ($e->subtask_ID == $subtask_id);
			}
		));
		if(count($subtask_answer_key) > 0){
			foreach($subtask_answer_key as $subtask_answer_key_row){
				$correct_answer = $subtask_answer_key_row->correctAnswer;
			}
		}
		// FILTER SET SCORES
		$subtask_set_score = array_merge(array_filter($process_scores, function ($e) use ($subtask_id, $task_id, $process_id) {
				return (($e->subtask_ID == $subtask_id) && ($e->task_ID == $task_id) && ($e->process_ID == $process_id));
			}
		));
		if(count($subtask_set_score) > 0){
			foreach($subtask_set_score as $subtask_set_score_row){
				$subtask_final_set_score = $subtask_set_score_row->totalScore;
				$subtask_totalscore_id = $subtask_set_score_row->totalscore_ID;
			}
		}
		// FILTER ANSWER SCORE
		$subtask_answer_score = array_merge(array_filter($answer_score, function ($e) use ($subtask_id, $checklist_id) {
				return (($e->subtask_ID == $subtask_id) && ($e->checklist_ID == $checklist_id));
			}
		));
		if((count($subtask_set_score) > 0) && (count($subtask_answer_score) > 0)){
			foreach($subtask_answer_score as $subtask_answer_score_row){
				$subtask_orig_final_answer_score = $subtask_answer_score_row->score;
			}
		}
		$bg_color = "";
		$text_color = "";
		$correction_label = "";
		$correction_icon = "";
		$final_answer = "";
		$show_correct_answer = "";
		if(($subtask_component_id == 1) || ($subtask_component_id == 5) || ($subtask_component_id == 7) || ($subtask_component_id == 10) ||  ($subtask_component_id == 11) || ($subtask_component_id == 13) || ($subtask_component_id == 14)){
			if(strtolower(trim($input_field_val)) == ''){
				$subtask_final_answer_score = 0;
				$bg_color = "bg-no-answer";
				$text_color = "text-danger";
				$correction_label = "No Answer";
				$correction_icon = "fa-remove";
				$final_answer = '';
				$show_correct_answer .= '<div class="col-6 my-2 py-2 bg-correct-answer">';
				$show_correct_answer .= $correct_answer;
				$show_correct_answer .= '</div>';
				$show_correct_answer .= '<div class="col-6 my-2 py-2 text-info">';
				$show_correct_answer .= '<i class="fa fa-check mr-2"></i>Correct Answer';
				$show_correct_answer .= '</div>';
			}else if(strtolower(trim($correct_answer)) == strtolower(trim($input_field_val))){
				$subtask_final_answer_score = $subtask_final_set_score;
				$bg_color = "bg-correct";
				$text_color = "text-success";
				$correction_label = "Correct";
				$correction_icon = "fa-check";
				$final_answer = $input_field_val;
			}else{
				if($subtask_orig_final_answer_score > 0){
					$subtask_final_answer_score = $subtask_orig_final_answer_score;
					$bg_color = "bg-correct";
					$text_color = "text-success";
					$correction_label = "Correct";
					$correction_icon = "fa-check";
					$final_answer = $input_field_val;
				}else{
					$subtask_final_answer_score = 0;
					$bg_color = "bg-wrong";
					$text_color = "text-danger";
					$correction_label = "Incorrect";
					$correction_icon = "fa-remove";
					$final_answer = $input_field_val;
					$show_correct_answer .= '<div class="col-6 my-2 py-2 bg-correct-answer">';
					$show_correct_answer .= $correct_answer;
					$show_correct_answer .= '</div>';
					$show_correct_answer .= '<div class="col-6 my-2 py-2 text-info">';
					$show_correct_answer .= '<i class="fa fa-check mr-2"></i>Correct Answer';
					$show_correct_answer .= '</div>';
				}
			}
			$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
			$correction_content .= '<div class="col-6 my-2 py-2 '.$bg_color.'">';
			$correction_content .= $final_answer;
			$correction_content .= '</div>';
			$correction_content .= '<div class="col-6 my-2 py-2 '.$text_color.'">';
			$correction_content .= '<i class="fa '.$correction_icon.' mr-2"></i>'.$correction_label;
			$correction_content .= '</div>';
			$correction_content .= $show_correct_answer;
		}else if($subtask_component_id == 2){
			$bg_length = "col-8";
			if(strtolower(trim($input_field_val)) == ''){
				$subtask_final_answer_score = 0;
				$bg_color = "bg-no-answer";
				$text_color = "text-danger";
				$correction_label = "No Answer";
				$correction_icon = "fa-remove";
				$final_answer = '';
				$bg_length = "col-6";
			}else{
				if($subtask_orig_final_answer_score > 0){
					$subtask_final_answer_score = $subtask_orig_final_answer_score;
					$bg_color = "bg-correct";
					$text_color = "text-success";
					$correction_label = "Correct";
					$correction_icon = "fa-check";
					$final_answer = $input_field_val;
				}else{
					$subtask_final_answer_score = 0;
					$bg_color = "bg-wrong";
					$text_color = "text-danger";
					$correction_label = "Incorrect";
					$correction_icon = "fa-remove";
					$final_answer = $input_field_val;
				}
			}
			$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
			$correction_content .= '<div class="'.$bg_length.' my-2 py-2 '.$bg_color.'">';
			$correction_content .= $final_answer;
			$correction_content .= '</div>';
			$correction_content .= '<div class="col-4 my-2 py-2 '.$text_color.'">';
			$correction_content .= '<i class="fa '.$correction_icon.' mr-2"></i>'.$correction_label;
			$correction_content .= '</div>';
		}else if($subtask_component_id == 6){
			if(count($sub_components) > 0){
				$show_no_answer = "";
				$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
				if(trim($input_field_val) == ''){
					$show_no_answer .= '<div class="col-6 my-2 py-2 bg-no-answer">';
					$show_no_answer .= '</div>';
					$show_no_answer .= '<div class="col-6 my-2 py-2 text-danger">';
					$show_no_answer .= '<i class="fa fa-remove mr-2"></i>No Answer';
					$show_no_answer .= '</div>';
				}
				$correction_content .= $show_no_answer;
				$correct_radio_answer = 0;
				foreach($sub_components as $sub_components_row){
					$bg_color = "";
					$text_color = "";
					$correction_label = "";
					$correction_icon = "";
					$checked = "";
					$sub_component_label = $sub_components_row->compcontent;
					if (trim($sub_components_row->compcontent) == '') {
						$sub_component_label = "No Option Label";
					}
					if($sub_component_label == $input_field_val){
						$checked = 'checked="checked"';
					}
					if(($sub_component_label == $input_field_val) && ($sub_component_label == $correct_answer)){
						$correct_radio_answer++;
						$bg_color = "bg-correct";
						$text_color = "text-success";
						$correction_label = "Correct";
						$correction_icon = "fa-check";
					}else if(($sub_component_label == $input_field_val) && ($sub_component_label != $correct_answer)){
						$bg_color = "bg-wrong";
						$text_color = "text-danger";
						$correction_label = "Incorrect";
						$correction_icon = "fa-remove";
					}else if(($sub_component_label != $input_field_val) && ($sub_component_label == $correct_answer)){
						$bg_color = "bg-correct-answer";
						$text_color = "text-info";
						$correction_label = "Correct Answer";
						$correction_icon = "fa-check";
					}
					$correction_content .= '<div class="col-6 my-2 py-2 '.$bg_color.'" style="word-break: break-word;">';
					$correction_content .= '<label class="m-radio mb-0">';
					$correction_content .= '<input type="radio" '.$checked.' class="nochange">'.$sub_component_label;
					$correction_content .= '<span></span>';
					$correction_content .= '</label>';
					$correction_content .= '</div>';
					$correction_content .= '<div class="col-6 my-2 py-2 '.$text_color.'">';
					$correction_content .= '<i class="fa '.$correction_icon.' mr-1"></i>'.$correction_label;
					$correction_content .= '</div>';
				}
				if($correct_radio_answer > 0){
					$subtask_final_answer_score = $subtask_final_set_score;
				}else{
					$subtask_final_answer_score = 0;
				}
			}
		}else if($subtask_component_id == 8){
			$correct_checkbox_answers = array_column($subtask_answer_key, 'correctAnswer');
			$checkbox_answers = array_column($subtask_answers, 'answer');
			$incorrect_checkbox = 0;
			if(count($sub_components) > 0){
				$show_no_answer = "";
				$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
				if(count($checkbox_answers) < 1){
						$show_no_answer .= '<div class="col-6 my-2 py-2 bg-no-answer">';
						$show_no_answer .= '</div>';
						$show_no_answer .= '<div class="col-6 my-2 py-2 text-danger">';
						$show_no_answer .= '<i class="fa fa-remove mr-2"></i>No Answer';
						$show_no_answer .= '</div>';
				}
				$correction_content .= $show_no_answer;
				foreach($sub_components as $sub_components_row){
					$bg_color = "";
					$text_color = "";
					$correction_label = "";
					$correction_icon = "";
					$checked = "";
					$option_score = 0;
					$sub_component_label = $sub_components_row->compcontent;
					if (trim($sub_components_row->compcontent) == '') {
						$sub_component_label = "No Option Label";
					}
					if(in_array($sub_component_label, $checkbox_answers)){
						$checked = 'checked="checked"';
						$option_score += 1;
					}
					if(in_array($sub_component_label, $correct_checkbox_answers)){
						$option_score += 2;
					}
					if($option_score == 1 || $option_score == 2){
						$incorrect_checkbox += 1;
					}
					if($option_score == 1){
						$bg_color = "bg-wrong";
						$text_color = "text-danger";
						$correction_label = "Incorrect";
						$correction_icon = "fa-remove";
					}else if($option_score == 2){
						$bg_color = "bg-correct-answer";
						$text_color = "text-info";
						$correction_label = "Correct Answer";
						$correction_icon = "fa-check";
					}elseif($option_score == 3){
						$bg_color = "bg-correct";
						$text_color = "text-success";
						$correction_label = "Correct";
						$correction_icon = "fa-check";
					}
					// var_dump($option_score);
					$correction_content .= '<div class="col-6 my-2 py-2 '.$bg_color.'" style="word-break: break-word;">';
					$correction_content .= '<label class="m-checkbox mb-0">';
					$correction_content .= '<input type="checkbox" '.$checked.' class="nochange">'.$sub_component_label;
					$correction_content .= '<span></span>';
					$correction_content .= '</label>';
					$correction_content .= '</div>';
					$correction_content .= '<div class="col-6 my-2 py-2 '.$text_color.'">';
					$correction_content .= '<i class="fa '.$correction_icon.' mr-1"></i>'.$correction_label;
					$correction_content .= '</div>';
				}
			}
			if($incorrect_checkbox > 0){
				$subtask_final_answer_score = 0;
			}else{
				$subtask_final_answer_score = $subtask_final_set_score;
			}
		}else if($subtask_component_id == 12){
			$bg_color = "";
			$text_color = "";
			$correction_label = "";
			$correction_icon = "";
			$final_answer = "";
			if(strtolower(trim($input_field_val)) == ''){
				$subtask_final_answer_score = 0;
				$bg_color = "bg-no-answer";
				$text_color = "text-danger";
				$correction_label = "No Answer";
				$correction_icon = "fa-remove";
				$final_answer = '';
			}else{
				$sanitized_email = filter_var($input_field_val, FILTER_SANITIZE_EMAIL);
				if (filter_var($sanitized_email, FILTER_VALIDATE_EMAIL) === false || $sanitized_email != $input_field_val) {
					$subtask_final_answer_score = 0;
					$bg_color = "bg-wrong";
					$text_color = "text-danger";
					$correction_label = "Incorrect";
					$correction_icon = "fa-remove";
					$final_answer = $input_field_val;
				}else{
					$subtask_final_answer_score = $subtask_final_set_score;
					$bg_color = "bg-correct";
					$text_color = "text-success";
					$correction_label = "Correct";
					$correction_icon = "fa-check";
					$final_answer = $input_field_val;
				}
			}
			$correction_content .= '<div class="col-12 px-0 font-italic" style="font-size: 0.9rem;">Answer :</div>';
			$correction_content .= '<div class="col-6 my-2 py-2 '.$bg_color.'">';
			$correction_content .= $final_answer;
			$correction_content .= '</div>';
			$correction_content .= '<div class="col-6 my-2 py-2 '.$text_color.'">';
			$correction_content .= '<i class="fa '.$correction_icon.' mr-2"></i>'.$correction_label;
			$correction_content .= '</div>';
		}else if($subtask_component_id == 15){
			if(trim($input_field_val) == ''){
				$correction_content .= '<div class="col-6 my-2 py-2 bg-no-answer">';
				$correction_content .= '</div>';
				$correction_content .= '<div class="col-6 my-2 py-2 text-danger">';
				$correction_content .= '<i class="fa fa-remove mr-2"></i>No File Uploaded';
				$correction_content .= '</div>';
			}else{
				$format_start = strpos($input_field_val, '.');
				$format_name_start = strrpos($input_field_val, '/');
				$file_title_substr = substr($input_field_val, $format_start + 1);
				$format_name_end = $format_start - $format_name_start;
				$file_title = substr($input_field_val, $format_name_start + 1, $format_name_end - 1);
				$file_format = $this->file_format_layout($file_title_substr);
				$file_format_type = $file_format['file_type'];
				$file_link = base_url($input_field_val);
				$correction_content .= '<div class="col-9 p-0">';
				$correction_content .= '<div class="col-12 text-center p-3" style=" border: 1px solid black dotted; border: 2px dashed #b9b9b9;">';
				$correction_content .= '<div class="col-12 text-center fileDiv">';
				$correction_content .= '<button class="btn btn-outline-focus btn-lg  m-btn m-btn--icon m-btn--outline-2x mr-2 uploadedFileBtn" download="">';
				$correction_content .= '<div><a href="'.$file_link.'" class="uploadedFile"><i class="fa fa-file-text-o mr-2"></i>'.$file_format_type.'</a></div>';
				$correction_content .= '</button>';
				$correction_content .= '';
				$correction_content .= '<br><div class="my-2" style="font-size: 0.8rem;">(<span class="font-italic">Click to download and View Uploaded File</span>)</div>';
				$correction_content .= '<div class="col-12 text-center" style="font-size:1.1em;color:#7e61b1;word-break: break-word;">';
				$correction_content .= $file_title;
				$correction_content .= '</div>';
				$correction_content .= '</div>';
				$correction_content .= '</div></div>';
			}
		}
		if($exam_page_type == 4){
			$subtask_final_answer_score = $subtask_orig_final_answer_score;
		}
		// if($subtask_component_id == 8){
		// 	var_dump($subtask_final_answer_score);
		// }
		$score_content = $this->process_scoring_layout($subtask_component_id, $input_field_val, $exam_page_type, $subtask_final_answer_score, $subtask_final_set_score, $subtask_id, $task_id);
		$subtask_content_answer .= '<div id="corrected_answers_content'.$subtask_id.'" class="col-12 mx-3 checkField" data-subtaskid="'.$subtask_id.'">';
		$subtask_content_answer .= $score_content;
		$subtask_content_answer .= '<div id="subtaskCorrection'.$subtask_id.'" class="row subtaskCorrection" data-subtaskid="'.$subtask_id.'">';
		$subtask_content_answer .= $correction_content;
		$subtask_content_answer .= '</div>';
		$subtask_content_answer .= '</div>';
		$correction_data['content']= $subtask_content_answer;
		$correction_data['specific_correction_layout']= $correction_content;
		return $correction_data;
	}

	public function get_field_layout($subtask_component_id, $subtask_id, $input_field_val, $subtask_answer, $sub_components){
		$subtask_content_answer = "";
		if($subtask_component_id == 1){ // INPUT TEXT (SHORT TEXT)
			$subtask_content_answer .= '<div class="col-9 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
            $subtask_content_answer .= '<input type="text" class="form-control m-input shortText field-border answerField" placeholder="Answer Here" data-subtaskid="'.$subtask_id.'" name="form_component'.$subtask_id.'" value="'.$input_field_val.'">';
            $subtask_content_answer .= '</div>';
		}else if($subtask_component_id == 2){// LONG TEXT
			$subtask_content_answer .= '<div class="col-9 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
			$subtask_content_answer .= '<textarea class="form-control m-input textAreaInput answerField" rows="3" placeholder="Answer Here" data-subtaskid="'.$subtask_id.'" name="form_component'.$subtask_id.'">'.$input_field_val.'</textarea>';
			$subtask_content_answer .= '</div>';
		}else if($subtask_component_id == 5){// NUMBER
			$subtask_content_answer .= '<div class="col-6 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
			$subtask_content_answer .= '<input class="form-control m-input numberInput answerField" type="number" placeholder="Answer Here" data-subtaskid="'.$subtask_id.'" name="form_component'.$subtask_id.'" value="'.$input_field_val.'">';
			$subtask_content_answer .= '</div>';
		}else if($subtask_component_id == 6){// RADIO BUTTON
			$radio_content = "";
			if(count($sub_components) > 0){
				$option_content = "";
				foreach($sub_components as $sub_components_row){
					$checked = "";
					$sub_component_label = $sub_components_row->compcontent;
					if (trim($sub_components_row->compcontent) == '') {
						$sub_component_label = "No Option Label";
					}
					if($sub_component_label == $input_field_val){
						$checked = 'checked="checked"';
					}
					$option_content .= '<label class="m-radio">';
                    $option_content .= '<input type="radio" class="answerField form-group m-form__group" name="form_component'.$sub_components_row->subTask_ID.'" value="'.$sub_component_label.'" data-subtaskid="'.$subtask_id.'" '.$checked.'>'.$sub_component_label;
                    $option_content .= '<span></span>';
					$option_content .= '</label>';
				}
				$radio_content .= '<div class="m-radio-list">'.$option_content.'</div>';
			}
			$subtask_content_answer .= '<div class="col-6 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
			$subtask_content_answer .= $radio_content;
			$subtask_content_answer .= '</div>';

		}else if($subtask_component_id == 7){// Dropdown
			$dropdown_content = "";
			if(count($sub_components) > 0){
				$option_content = "<option hidden disabled selected value> -- Select Your Answer -- </option>";
				foreach($sub_components as $sub_components_row){
					$selected = "";
					$sub_component_label = $sub_components_row->compcontent;
					if (trim($sub_components_row->compcontent) == '') {
						$sub_component_label = "No Option Label";
					}
					if($sub_component_label == $input_field_val){
						$selected = 'selected="selected"';
					}
					$option_content .= '<option value="'.$sub_component_label.'" '.$selected.'>';
                    $option_content .= $sub_component_label;
					$option_content .= '</option>';
				}
				$dropdown_content .= '<select class="form-control m-input answerField" id="exampleSelect1" placeholder="Select Your Answer" name="form_component'.$subtask_id.'" data-subtaskid="'.$subtask_id.'">'.$option_content.'</select>';
			}
			$subtask_content_answer .= '<div class="col-6 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
			$subtask_content_answer .= $dropdown_content;
			$subtask_content_answer .= '</div>';
		}else if($subtask_component_id == 8){// CHECKBOX
			$checkbox_content = "";
			if(count($sub_components) > 0){
				$option_content = "";
				foreach($sub_components as $sub_components_row){
					$checked = "";
					$sub_component_label = $sub_components_row->compcontent;
					if (trim($sub_components_row->compcontent) == '') {
						$sub_component_label = "No Option Label";
					}
					if(count($subtask_answer) > 0){
						$checkbox_answer = array_merge(array_filter($subtask_answer, function ($e) use ($sub_component_label) {
								return ($e->answer == $sub_component_label);
							}
						));
						if(count($checkbox_answer) > 0){
							$checked = 'checked="checked"';
						}
					}
					$option_content .= '<label class="m-checkbox">';
                    $option_content .= '<input type="checkbox" class="answerField_checkbox" name="form_component'.$subtask_id.'" value="'.$sub_component_label.'" data-subtaskid="'.$subtask_id.'" '.$checked.'>'.$sub_component_label;
                    $option_content .= '<span></span>';
					$option_content .= '</label>';
				}
				$checkbox_content .= '<div class="m-checkbox-list">'.$option_content.'</div>';
			}
			$subtask_content_answer .= '<div class="col-6 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
			$subtask_content_answer .= $checkbox_content;
			$subtask_content_answer .= '</div>';
		}else if($subtask_component_id == 10){// DATEPICKER
			// $init_date_picker[$init_date_picker_count] = '#datepicker'.$sub_components_row->subTask_ID;
			// $init_date_picker_count++;
			$subtask_content_answer .= '<div class="col-6 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
			$subtask_content_answer .= '<input id="datepicker'.$subtask_id.'" type="text" class="form-control datePicker answerField" data-subtaskid="'.$subtask_id.'" name="form_component'.$subtask_id.'" value="'.$input_field_val.'" readonly/>';
			$subtask_content_answer .= '</div>';
		}else if($subtask_component_id == 11){// TOUCHSPIN
			// $init_date_picker[$init_date_picker_count] = '#datepicker'.$sub_components_row->subTask_ID;
			// $init_date_picker_count++;
			$subtask_content_answer .= '<div class="col-6 mt-2 answerFieldDiv form-group m-form__group">';
			$subtask_content_answer .= '<input type="text" data-subtaskid="'.$subtask_id.'" class="form-control bootstrap-touchspin-vertical-btn touchspin touchspinInput answerField" type="text" name="form_component'.$subtask_id.'" value="'.$input_field_val.'">';
			$subtask_content_answer .= '</div>';
		}else if($subtask_component_id == 12){ // EMAIL
			$subtask_content_answer .= '<div class="col-9 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
            $subtask_content_answer .= '<input type="text" class="form-control m-input shortText field-border answerField" placeholder="Answer Here" data-subtaskid="'.$subtask_id.'" name="form_component'.$subtask_id.'" value="'.$input_field_val.'">';
			$subtask_content_answer .= '</div>';
		}else if($subtask_component_id == 13){// TIMEPICKER
			$subtask_content_answer .= '<div class="col-6 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
			$subtask_content_answer .= '<input type="text" class="form-control m-input timePicker answerField" data-subtaskid="'.$subtask_id.'" name="form_component'.$subtask_id.'" value="'.$input_field_val.'">';
			$subtask_content_answer .= '</div>';
		}else if($subtask_component_id == 14){// DATETIMEPICKER		
			$subtask_content_answer .= '<div class="col-6 mt-2 answerFieldDiv form-group m-form__group" id="answerFieldDiv'.$subtask_id.'">';
			$subtask_content_answer .= '<input type="text" class="form-control m-input dateTimePicker answerField" data-subtaskid="'.$subtask_id.'" name="form_component'.$subtask_id.'" value="'.$input_field_val.'">';
			$subtask_content_answer .= '</div>';		
		}
		return $subtask_content_answer;
	}


	public function get_survey_task_content(){
		$data['task_content'] = "";
		$data['task_score_ratio'] = "";
		$data['init_datepickers'] = [];
		$data['task_title'] = "Untitled Test Set";
		// $exam_page_type = 4;
		$exam_page_type = $this->input->post('examPageType');
		// $task_id = 91;
		$task_id = $this->input->post('taskId');
		// $process_id = 35;
		$process_id = $this->input->post('processId');
		$checklist_id = $this->input->post('checklistId');
		$process_score_total = 0;
		$answer_score_total = 0;
		$process_details = $this->qry_process_details($process_id);
		$task_details = $this->qry_specific_process_tasks($task_id);
		$tasks = $this->qry_process_tasks($process_id);
		$data['task_footer_btn'] = "";
		if(count($tasks) > 1){
			foreach($tasks as $tasks_index=>$tasks_row){
				$order_val = 0;
				if($tasks_index == 0){
					$order_val = 1;
				}else if($tasks_index == count($tasks)-1){
					$order_val = 2;
				}else{
					$order_val = 3;
				}
				if($tasks_row->task_ID == $task_id){
					$data['task_footer_btn'] = $this->layout_item_footer_btn($order_val, $tasks_row->sequence, $process_id, $exam_page_type);
				}
			}
		}
		if(((int)$process_details->processType_ID == 6) && ($exam_page_type == 2)){
			$data['task_footer_btn'] .= '<button type="submit" class="btn btn-success btn-md m-btn m-btn--icon mx-1 surveySubmit">Submit</button>';
		}
		if(count($task_details) > 0){
			if (trim($task_details->taskTitle) != '') {
                $data['task_title'] = trim($task_details->taskTitle);
            }
		}
		// var_dump($process_details->processType_ID);
		// var_dump($data['task_footer_btn']);
		$subtasks = $this->qry_specific_process_subtasks($task_id);
		// var_dump($subtasks); 
		$answer_key = $this->qry_process_answer_key($process_id);
		$process_scores = $this->qry_process_task_scores($process_id, $task_id);
		$answers = [];
		$answer_score = [];
		$subtask_components = $this->qry_process_component_subtask(array_column($subtasks, 'subTask_ID'));
		if($exam_page_type != 1){ // GET ANSWER DATA
			$answers = $this->qry_process_answers($checklist_id, array_column($subtasks, 'subTask_ID'));
			$answer_score = $this->qry_process_answer_score($checklist_id, array_column($subtasks, 'subTask_ID'));
			if(($exam_page_type == 3) || ($exam_page_type == 4)){
				$process_score_total = array_sum(array_map('intval', array_column($process_scores, 'totalScore')));
				$answer_score_total = array_sum(array_map('intval', array_column($answer_score, 'score')));
				$data['task_score_ratio'] = "Test Score: ".$answer_score_total."/".$process_score_total;
			}
		}
		if(count($subtasks) > 0){
			$task_content = "";
			$init_date_picker = [];
			$init_date_picker_count = 0;
			foreach($subtasks as $subtasks_row){
				$subtask_section = "";
				$subtask_content = "";
				$subtask_label = $subtasks_row->complabel;
				$subtask_sub_label = "";
				$subtask_row_id = $subtasks_row->subTask_ID;
				if (trim($subtasks_row->complabel) == '') {
                    $subtask_label = "No Question Label";
                }
				if (trim($subtasks_row->sublabel) !== '') {
                    $subtask_sub_label = '<div class="col-12 font-italic mt-1" style="font-size: 0.9rem;">('.$subtasks_row->sublabel.')</div>';
                }
				if($exam_page_type == 2){
					// WITH SUBMIT BUTTON
				}else{
					// NO SUBMIT BUTTON
				}
				$subtask_content_label = "";
				$subtask_section_content = "";
				$subtask_content_answer = "";
				$subtask_content_score = "";
				if(($subtasks_row->component_ID == 3) || ($subtasks_row->component_ID == 4) || ($subtasks_row->component_ID == 9) || ($subtasks_row->component_ID == 15) || ($subtasks_row->component_ID == 16) || ($subtasks_row->component_ID == 17) || ($subtasks_row->component_ID == 18)){
					$position = "";
					if($subtasks_row->component_ID == 3){ // HEADING
						$subtask_section_content .= '<h5 class="px-0">'.$subtask_label.'</h5>';
					}else if($subtasks_row->component_ID == 4){ // TEXT
						$subtask_section_content .= '<h7 class="px-0">'.$subtask_label.'<h7/>';
					}else if($subtasks_row->component_ID == 9){ // DIVIDER
						$subtask_section_content .= '<div class="col-12 px-0 py-0"><hr style="margin-top: 0.8rem !important;"></div>';
					}else if($subtasks_row->component_ID == 16){
						$position = 'text-center';
						$subtask_section_content .= '<div class="col-12 mb-4 mt-2 text-center">';
						$subtask_section_content .= '<figure style="margin:0 !important">';
						$subtask_section_content .= '<img src="'.base_url($subtasks_row->complabel).'" class="gambar img-responsiv" id="item-img-output274" style="background:white;width:50%!important;object-fit:cover;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);">';
						$subtask_section_content .= '</figure>';
						$subtask_section_content .= '</div>';
					}else if($subtasks_row->component_ID == 17){
						$position = 'text-center';
						$subtask_section_content .= '<div class="col-12 text-center" style="padding: 1rem 5rem 1rem 5rem;">';
						$subtask_section_content .= '<div class="row iframepadding">';
						$subtask_section_content .= '<div class="embed-responsive embed-responsive-16by9">';
						// var_dump(strpos($subtasks_row->complabel, 'uploads/process/form_videos/'));
						if (strpos($subtasks_row->complabel, 'uploads/process/form_videos/') != false) {
                            $subtask_section_content .= '<video width="320" height="240" controls>';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mp4">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/m4v">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/avi">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mpg">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mov">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mpg">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mpeg">';
                            $subtask_section_content .= '</video>';
                        } else {
                            $subtask_section_content .= $subtasks_row->complabel;
                        }
						$subtask_section_content .= '</div>';
						$subtask_section_content .= '</div>';
						$subtask_section_content .= '</div>';
					}else if($subtasks_row->component_ID == 18){
						$position = 'text-center';
						$format_start = strpos($subtasks_row->complabel, '.');
						$format_name_start = strrpos($subtasks_row->complabel, '/');
						$file_format = substr($subtasks_row->complabel, $format_start + 1);
						$format_name_end = $format_start - $format_name_start;
						$file_title = substr($subtasks_row->complabel, $format_name_start + 1, $format_name_end - 1);
						$file_format = $this->file_format_layout($file_format);
						$subtask_section_content .= '<div class="col-md-3"></div>';
						$subtask_section_content .= '<a href="'.base_url($subtasks_row->complabel).'" class="col-12 col-md-6 text-center p-2 fileDisplay" style="border: 2px dashed '.$file_format['border'].';background: '.$file_format['background'].';" download="">';
						$subtask_section_content .= '<div class="" style="color:dimgrey;">';
						$subtask_section_content .= '<span><i class="fa fa-file-text-o mr-2"></i><span>'.$file_format['file_type'].'</span></span>';
						$subtask_section_content .= '</div>';
						$subtask_section_content .= '<div style="font-size:1.1em;color:#7e61b1">'.$file_title.'</div>';
						$subtask_section_content .= '<div class="" style="font-size: 0.8rem;color:dimgrey">(<span class="font-italic">Click to download and View Uploaded File</span>)</div>';
						$subtask_section_content .= '</a>';
						$subtask_section_content .= '<div class="col-md-3"></div>';
					}else if($subtasks_row->component_ID == 15){// FILE
						// $position = 'text-center';
						$subtask_answer = [];
						$input_field_val = '';
						$subtask_answer = array_merge(array_filter($answers, function ($e) use ($subtask_row_id) {
								return ($e->subtask_ID == $subtask_row_id);
							}
						));
						if(count($subtask_answer) > 0){ // GET FIELD ANSWER (except field with multiple answers such as checkbox)
							$input_field_val = $subtask_answer[0]->answer;
						}
						if(($exam_page_type == 1) || ($exam_page_type == 2)){ // FIELD
							$subtask_section_content .= '<div class="col-9">';
							$subtask_section_content .= '<div class="col-12 text-center p-3" style=" border: 1px solid black dotted; border: 2px dashed #b9b9b9;">';
							if($exam_page_type == 1){
								$subtask_section_content .= '<label class="btn btn-accent btn-sm mb-0 fileUploadBtnDiv">';
								$subtask_section_content .= '<i class="fa fa-paperclip mr-1"></i>Upload your file here.';
								$subtask_section_content .= '</label>';
							}else if($exam_page_type == 2){
								$input_field_val = '';
								$file_format_type = '';
								$file_link = '';
								$file_title = '';
								$hide_file_upload_btn = '';
								$answer_speci_ID='';
								$hide_file = 'style="display:none;"';
								
								if(count($subtask_answer) > 0){
									$hide_file_upload_btn = 'style="display:none;"';
									$hide_file = '';
									foreach($subtask_answer as $subtask_answer_row){
										$input_field_val = $subtask_answer_row->answer;
										$answer_speci_ID = $subtask_answer_row->answer_ID;
									}
									$format_start = strpos($input_field_val, '.');
									$format_name_start = strrpos($input_field_val, '/');
									$file_title_substr = substr($input_field_val, $format_start + 1);
									$format_name_end = $format_start - $format_name_start;
									$file_title = substr($input_field_val, $format_name_start + 1, $format_name_end - 1);
									$file_format = $this->file_format_layout($file_title_substr);
									$file_format_type = $file_format['file_type'];
									$file_link = base_url($input_field_val);
								}
								$subtask_section_content .= '<label class="btn btn-accent btn-sm mb-0 fileUploadBtnDiv" '.$hide_file_upload_btn.'>';
								$subtask_section_content .= '<i class="fa fa-paperclip mr-1"></i>Upload your file here.';
								$subtask_section_content .= '<form enctype="multipart/form-data method="post" action="">';
								$subtask_section_content .= '<input type="file" data-subtaskid="'.$subtask_row_id.'" data-taskid="'.$task_id.'" style="display: none;" name="upload_files" class="answerField_upload" id="upload_files'.$subtask_row_id.'">';								
								$subtask_section_content .= '</form>';
								$subtask_section_content .= '</label>';
								$subtask_section_content .= '<div class="col-12 text-center fileDiv" '.$hide_file.'>';
								$subtask_section_content .= '<button class="btn btn-outline-focus btn-lg  m-btn m-btn--icon m-btn--outline-2x mr-2 uploadedFileBtn" download="">';
								$subtask_section_content .= '<div><a href="'.$file_link.'" class="uploadedFile"><i class="fa fa-file-text-o mr-2"></i>'.$file_format_type.'</a><span data-answerid="'.$answer_speci_ID.'" data-subtaskid="'.$subtask_row_id.'" data-taskid="'.$task_id.'" class="pull-right remove_fileuploaded" style="margin-top: -1rem !important; margin-right: -1.2rem !important;"><i class="fa fa-remove" style="color:#f4516c"></i></span></div>';								
								$subtask_section_content .= '</button>';
								$subtask_section_content .= '';
								$subtask_section_content .= '<br><div class="my-2" style="font-size: 0.8rem;">(<span class="font-italic">Click to download and View Uploaded File</span>)</div>';
								$subtask_section_content .= '<div class="col-12 text-center" style="font-size:1.1em;color:#7e61b1;word-break: break-word;">';
								$subtask_section_content .= $file_title;
								$subtask_section_content .= '</div>';
								$subtask_section_content .= '</div>';
							}
							$subtask_section_content .= '</div></div>';
						}else{ // CORRECTION
							$correction_layout = $this->process_answers_layout($subtasks_row->component_ID, $input_field_val, $subtask_row_id, $task_id, $process_id, $checklist_id, $exam_page_type, $sub_components, $subtask_answer);
							$subtask_section_content .= $correction_layout['content'];
						}
					}
					if (trim($subtasks_row->sublabel) !== '') {
						$subtask_sub_label = '<div class="col-12 font-italic mt-1 px-3 '.$position.'" style="font-size: 0.9rem;">('.$subtasks_row->sublabel.')</div>';
					}
					if($subtasks_row->component_ID == 15){
						$subtask_section .= '<li class="examItems" id="examItem'.$subtask_row_id.'">'.$subtask_sub_label.'</li><div class="row mb-4">'.$subtask_section_content.'</div>';

					}else{
						$subtask_section .= $subtask_content_label.'<div class="row mb-4">'.$subtask_section_content.$subtask_sub_label.'</div>';
					}
				}else{// ANSWERABLE SUBTASK
					$label_required = "";
					if($subtasks_row->required == 'yes'){
						$label_required = '<span class="ml-1 text-danger">*</span>';
					}
					$subtask_content_label = '<li class="examItems" id="examItem'.$subtask_row_id.'">'.$subtask_label.$label_required.'</li>';
					$input_field_val = '';
					$subtask_answer = [];
					//GET SUBTASK ANSWER IF NOT PREVIEW PAGE
					if($exam_page_type != 1){
						$subtask_answer = array_merge(array_filter($answers, function ($e) use ($subtask_row_id) {
								return ($e->subtask_ID == $subtask_row_id);
							}
						));
						if((count($subtask_answer) > 0) && ($subtasks_row->component_ID != 8)){ // GET FIELD ANSWER (except field with multiple answers such as checkbox)
							$input_field_val = $subtask_answer[0]->answer;
						}
						// var_dump($input_field_val);
					}
					$sub_components = [];
					if(($subtasks_row->component_ID == 6) || ($subtasks_row->component_ID == 7) || ($subtasks_row->component_ID == 8)){
						$sub_components = array_merge(array_filter($subtask_components, function ($e) use ($subtask_row_id) {
								return ($e->subTask_ID == $subtask_row_id);
							}
						));
					}
					if(($exam_page_type == 1) || ($exam_page_type == 2)){
						$subtask_content_answer .= $this->get_field_layout($subtasks_row->component_ID, $subtask_row_id, $input_field_val, $subtask_answer, $sub_components);
					}else{
						$correction_layout = $this->process_answers_layout($subtasks_row->component_ID, $input_field_val, $subtask_row_id, $task_id, $process_id, $checklist_id, $exam_page_type, $sub_components, $subtask_answer);
						$subtask_content_answer .= $correction_layout['content'];
					}
					$subtask_content .= '<div class="row mb-4">'.$subtask_sub_label.$subtask_content_answer.'</div>';
					$subtask_section = $subtask_content_label.$subtask_content;
				}
				$task_content .= $subtask_section;
			}
			$data['task_content'] .= '<form id="form-task" class="" method="post">';
				$data['task_content'] .= '<div class="row m-row--no-padding m-row--col-separator-xl">';
					$data['task_content'] .= '<div class="col-12 p-4">';
						$data['task_content'] .= '<h3 class="pt-4" style="padding-left: 1.8rem; padding-right: 1.8rem;" id="task_title">'.$data['task_title'].'</h3>';
						$data['task_content'] .= '<div class="col-12" id="empty-test-set" style="display:none;">';
							$data['task_content'] .= '<div class="col-md-12 p-0" style="display: flex;align-items:center;">';
								$data['task_content'] .= '<div class="col-md-12 p-0 text-center">';
									$data['task_content'] .= '<div class="m-demo-icon__preview"><i style="font-size: 8rem;" class="flaticon-notes"></i></div>';
									$data['task_content'] .= '<span class="m-demo-icon__class" style="font-size: 2rem;">Test Set is empty</span>';
								$data['task_content'] .= '</div>';
							$data['task_content'] .= '</div>';
						$data['task_content'] .= '</div>';
						$data['task_content'] .= '<ol id="exam_task_content" class="pr-5">';
						$data['task_content'] .= $task_content;
						$data['task_content'] .= '</ol>';
					$data['task_content'] .= '</div>';
					$data['task_content'] .= '<div class="col-12 p-4 text-right" id="footer_btn_div" style="border-top: 2px dashed #9e9e9e61;">';
					$data['task_content'] .= $data['task_footer_btn'];
					$data['task_content'] .= '</div>';
				$data['task_content'] .= '</div>';
			$data['task_content'] .= '</form>';
		}
		echo json_encode($data);
	}

	public function get_exam_task_content(){
		$data['task_content'] = "";
		$data['task_score_ratio'] = "";
		$data['init_datepickers'] = [];
		$data['task_title'] = "Untitled Test Set";
		// $exam_page_type = 4;
		$exam_page_type = $this->input->post('examPageType');
		// $task_id = 91;
		$task_id = $this->input->post('taskId');
		// $process_id = 35;
		$process_id = $this->input->post('processId');
		$checklist_id = $this->input->post('checklistId');
		$process_score_total = 0;
		$answer_score_total = 0;
		$process_details = $this->qry_process_details($process_id);
		$task_details = $this->qry_specific_process_tasks($task_id);
		$tasks = $this->qry_process_tasks($process_id);
		$data['task_footer_btn'] = "";
		if(count($tasks) > 1){
			foreach($tasks as $tasks_index=>$tasks_row){
				$order_val = 0;
				if($tasks_index == 0){
					$order_val = 1;
				}else if($tasks_index == count($tasks)-1){
					$order_val = 2;
				}else{
					$order_val = 3;
				}
				if($tasks_row->task_ID == $task_id){
					$data['task_footer_btn'] = $this->layout_item_footer_btn($order_val, $tasks_row->sequence, $process_id, $exam_page_type);
				}
			}
		}
		if(((int)$process_details->processType_ID != 6) && ($exam_page_type == 2)){
			$data['task_footer_btn'] .= '<button type="submit" class="btn btn-success btn-md m-btn m-btn--icon mx-1 submit_examanswers"><span><i class="fa fa-check"></i><span>Submit</span></span></button>';
		}
		if(((int)$process_details->processType_ID != 6) && ($exam_page_type == 3)){
			$data['task_footer_btn'] .= '<button type="submit" class="btn btn-success btn-md m-btn m-btn--icon mx-1 correctionSubmit"><span><i class="fa fa-check"></i><span>Submit Correction</span></span></button>';
		}
		if(count($task_details) > 0){
			if (trim($task_details->taskTitle) != '') {
                $data['task_title'] = trim($task_details->taskTitle);
            }
		}
		$subtasks = $this->qry_specific_process_subtasks($task_id);
		// var_dump($subtasks);
		$answer_key = $this->qry_process_answer_key($process_id);
		$process_scores = $this->qry_process_task_scores($process_id, $task_id);
		$answers = [];
		$answer_score = [];
		$subtask_components = $this->qry_process_component_subtask(array_column($subtasks, 'subTask_ID'));
		if($exam_page_type != 1){ // GET ANSWER DATA
			// var_dump($checklist_id);
			$answers = $this->qry_process_answers($checklist_id, array_column($subtasks, 'subTask_ID'));
			// var_dump($answers);
			$answer_score = $this->qry_process_answer_score($checklist_id, array_column($subtasks, 'subTask_ID'));
			// var_dump($subtasks);
			// var_dump($answer_score);
			if(($exam_page_type == 3) || ($exam_page_type == 4)){
				// var_dump(array_column($process_scores, 'totalScore'));
				// var_dump(array_column($answer_score, 'score'));
				$process_score_total = array_sum(array_map('intval', array_column($process_scores, 'totalScore')));
				$answer_score_total = array_sum(array_map('intval', array_column($answer_score, 'score')));
				// var_dump($answer_score);
				// var_dump($answer_score_total);
				$data['task_score_ratio'] = "Test Score: ".$answer_score_total."/".$process_score_total;
			}
			// var_dump($process_score_total);
			// var_dump($answer_score_total);
		}
		if(count($subtasks) > 0){
			$task_content = "";
			$init_date_picker = [];
			$init_date_picker_count = 0;
			foreach($subtasks as $subtasks_row){
				// var_dump($subtasks_row);
				// var_dump($answers);
				$subtask_section = "";
				$subtask_content = "";
				$subtask_label = $subtasks_row->complabel;
				$subtask_sub_label = "";
				$subtask_row_id = $subtasks_row->subTask_ID;
				if (trim($subtasks_row->complabel) == '') {
                    $subtask_label = "No Question Label";
                }
				if (trim($subtasks_row->sublabel) !== '') {
                    $subtask_sub_label = '<div class="col-12 font-italic mt-1" style="font-size: 0.9rem;">('.$subtasks_row->sublabel.')</div>';
                }
				if($exam_page_type == 2){
					// WITH SUBMIT BUTTON
				}else{
					// NO SUBMIT BUTTON
				}
				$subtask_content_label = "";
				$subtask_section_content = "";
				$subtask_content_answer = "";
				$subtask_content_score = "";
				$input_field_val = '';
				if(($subtasks_row->component_ID == 3) || ($subtasks_row->component_ID == 4) || ($subtasks_row->component_ID == 9) || ($subtasks_row->component_ID == 15) || ($subtasks_row->component_ID == 16) || ($subtasks_row->component_ID == 17) || ($subtasks_row->component_ID == 18)){
					$position = "";
					if($subtasks_row->component_ID == 3){ // HEADING
						$subtask_section_content .= '<h5 class="px-0">'.$subtask_label.'</h5>';
					}else if($subtasks_row->component_ID == 4){ // TEXT
						$subtask_section_content .= '<h7 class="px-0">'.$subtask_label.'<h7/>';
					}else if($subtasks_row->component_ID == 9){ // DIVIDER
						$subtask_section_content .= '<div class="col-12 px-0 py-0"><hr style="margin-top: 0.8rem !important;"></div>';
					}else if($subtasks_row->component_ID == 16){
						$position = 'text-center';
						$subtask_section_content .= '<div class="col-12 mb-4 mt-2 text-center">';
						$subtask_section_content .= '<figure style="margin:0 !important">';
						$subtask_section_content .= '<img src="'.base_url($subtasks_row->complabel).'" class="gambar img-responsiv" id="item-img-output274" style="background:white;width:50%!important;object-fit:cover;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);">';
						$subtask_section_content .= '</figure>';
						$subtask_section_content .= '</div>';
					}else if($subtasks_row->component_ID == 17){
						$position = 'text-center';
						$subtask_section_content .= '<div class="col-12 text-center" style="padding: 1rem 5rem 1rem 5rem;">';
						$subtask_section_content .= '<div class="row iframepadding">';
						$subtask_section_content .= '<div class="embed-responsive embed-responsive-16by9">';
						// var_dump(strpos($subtasks_row->complabel, 'uploads/process/form_videos/'));
						if (strpos($subtasks_row->complabel, 'uploads/process/form_videos/') != false) {
                            $subtask_section_content .= '<video width="320" height="240" controls>';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mp4">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/m4v">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/avi">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mpg">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mov">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mpg">';
                            $subtask_section_content .= '<source src="'. base_url($subtasks_row->complabel).'" type="video/mpeg">';
                            $subtask_section_content .= '</video>';
                        } else {
                            $subtask_section_content .= $subtasks_row->complabel;
                        }
						$subtask_section_content .= '</div>';
						$subtask_section_content .= '</div>';
						$subtask_section_content .= '</div>';
					}else if($subtasks_row->component_ID == 18){
						$position = 'text-center';
						$format_start = strpos($subtasks_row->complabel, '.');
						$format_name_start = strrpos($subtasks_row->complabel, '/');
						$file_format = substr($subtasks_row->complabel, $format_start + 1);
						$format_name_end = $format_start - $format_name_start;
						$file_title = substr($subtasks_row->complabel, $format_name_start + 1, $format_name_end - 1);
						$file_format = $this->file_format_layout($file_format);
						$subtask_section_content .= '<div class="col-md-3"></div>';
						$subtask_section_content .= '<a href="'.base_url($subtasks_row->complabel).'" class="col-12 col-md-6 text-center p-2 fileDisplay" style="border: 2px dashed '.$file_format['border'].';background: '.$file_format['background'].';" download="">';
						$subtask_section_content .= '<div class="" style="color:dimgrey;">';
						$subtask_section_content .= '<span><i class="fa fa-file-text-o mr-2"></i><span>'.$file_format['file_type'].'</span></span>';
						$subtask_section_content .= '</div>';
						$subtask_section_content .= '<div style="font-size:1.1em;color:#7e61b1">'.$file_title.'</div>';
						$subtask_section_content .= '<div class="" style="font-size: 0.8rem;color:dimgrey">(<span class="font-italic">Click to download and View Uploaded File</span>)</div>';
						$subtask_section_content .= '</a>';
						$subtask_section_content .= '<div class="col-md-3"></div>';
					}else if($subtasks_row->component_ID == 15){// FILE
						// $position = 'text-center';
						$subtask_answer = [];
						$input_field_val = '';
						$answer_speci_ID='';
						$subtask_answer = array_merge(array_filter($answers, function ($e) use ($subtask_row_id) {
								return ($e->subtask_ID == $subtask_row_id);
							}
						));
						if(count($subtask_answer) > 0){ // GET FIELD ANSWER (except field with multiple answers such as checkbox)
							$input_field_val = $subtask_answer[0]->answer;
						}
						if(($exam_page_type == 1) || ($exam_page_type == 2)){ // FIELD
							$subtask_section_content .= '<div class="col-9">';
							$subtask_section_content .= '<div class="col-12 text-center p-3" style=" border: 1px solid black dotted; border: 2px dashed #b9b9b9;">';
							if($exam_page_type == 1){
								$subtask_section_content .= '<label class="btn btn-accent btn-sm mb-0 fileUploadBtnDiv">';
								$subtask_section_content .= '<i class="fa fa-paperclip mr-1"></i>Upload your file here.';
								$subtask_section_content .= '</label>';
							}else if($exam_page_type == 2){
								$input_field_val = '';
								$file_format_type = '';
								$file_link = '';
								$file_title = '';
								$hide_file_upload_btn = '';
								$hide_file = 'style="display:none;"';
								
								if(count($subtask_answer) > 0){
									$hide_file_upload_btn = 'style="display:none;"';
									$hide_file = '';
									foreach($subtask_answer as $subtask_answer_row){
										$input_field_val = $subtask_answer_row->answer;
										$answer_speci_ID = $subtask_answer_row->answer_ID;
									}
									$format_start = strpos($input_field_val, '.');
									$format_name_start = strrpos($input_field_val, '/');
									$file_title_substr = substr($input_field_val, $format_start + 1);
									$format_name_end = $format_start - $format_name_start;
									$file_title = substr($input_field_val, $format_name_start + 1, $format_name_end - 1);
									$file_format = $this->file_format_layout($file_title_substr);
									$file_format_type = $file_format['file_type'];
									$file_link = base_url($input_field_val);

									// enctype="multipart/form-data" 
								}
								$subtask_section_content .= '<label id="get_subtaskid_up" data-subtaskid="" class="btn btn-accent btn-sm mb-0 fileUploadBtnDiv" '.$hide_file_upload_btn.'>';
								$subtask_section_content .= '<i class="fa fa-paperclip mr-1"></i>Upload your file here.';
								$subtask_section_content .= '<form enctype="multipart/form-data method="post" action="">';
								$subtask_section_content .= '<input type="file" data-subtaskid="'.$subtask_row_id.'" data-taskid="'.$task_id.'" style="display: none;" name="upload_files" class="answerField_upload" id="upload_files'.$subtask_row_id.'">';								
								$subtask_section_content .= '</form>';
								$subtask_section_content .= '</label>';
								$subtask_section_content .= '<ol id="labelx'.$subtask_row_id.'"></ol>';
								$subtask_section_content .= '<div class="col-12 text-center fileDiv" '.$hide_file.'>';
								$subtask_section_content .= '<button class="btn btn-outline-focus btn-lg  m-btn m-btn--icon m-btn--outline-2x mr-2 uploadedFileBtn" download="">';
								$subtask_section_content .= '<div><a href="'.$file_link.'" class="uploadedFile"><i class="fa fa-file-text-o mr-2"></i>'.$file_format_type.'</a><span data-answerid="'.$answer_speci_ID.'" data-subtaskid="'.$subtask_row_id.'" data-taskid="'.$task_id.'" class="pull-right remove_fileuploaded" style="margin-top: -1rem !important; margin-right: -1.2rem !important;"><i class="fa fa-remove" style="color:#f4516c"></i></span></div>';
								$subtask_section_content .= '</button>';
								$subtask_section_content .= '';
								$subtask_section_content .= '<br><div class="my-2" style="font-size: 0.8rem;">(<span class="font-italic">Click to download and View Uploaded File</span>)</div>';
								$subtask_section_content .= '<div class="col-12 text-center" style="font-size:1.1em;color:#7e61b1;word-break: break-word;">';
								$subtask_section_content .= $file_title;
								$subtask_section_content .= '</div>';
								$subtask_section_content .= '</div>';
							}
							$subtask_section_content .= '</div></div>';
						}else{ // CORRECTION
							$correction_layout = $this->process_correction_layout($subtasks_row->component_ID, $input_field_val, $process_scores, $answer_key, $answer_score , $subtask_row_id, $task_id, $process_id, $checklist_id, $exam_page_type, $sub_components, $subtask_answer);
							$subtask_section_content .= $correction_layout['content'];						}
					}
					if (trim($subtasks_row->sublabel) !== '') {
						$subtask_sub_label = '<div class="col-12 font-italic mt-1 px-3 '.$position.'" style="font-size: 0.9rem;">('.$subtasks_row->sublabel.')</div>';
					}
					if($subtasks_row->component_ID == 15){
						$subtask_section .= '<li class="examItems" id="examItem'.$subtask_row_id.'">'.$subtask_sub_label.'</li><div class="row mb-4">'.$subtask_section_content.'</div>';

					}else{
						$subtask_section .= $subtask_content_label.'<div class="row mb-4">'.$subtask_section_content.$subtask_sub_label.'</div>';
					}
				}else{// ANSWERABLE SUBTASK
					$label_required = "";
					if($subtasks_row->required == 'yes'){
						$label_required = '<span class="ml-1 text-danger">*</span>';
					}
					$subtask_content_label = '<li class="examItems" id="examItem'.$subtask_row_id.'">'.$subtask_label.$label_required.'</li>';;
					$subtask_answer = [];
					//GET SUBTASK ANSWER IF NOT PREVIEW PAGE
					if($exam_page_type != 1){
						$subtask_answer = array_merge(array_filter($answers, function ($e) use ($subtask_row_id) {
								return ($e->subtask_ID == $subtask_row_id);
							}
						));
						if((count($subtask_answer) > 0) && ($subtasks_row->component_ID != 8)){ // GET FIELD ANSWER (except field with multiple answers such as checkbox)
							$input_field_val = $subtask_answer[0]->answer;
						}
					}
					$sub_components = [];
					if(($subtasks_row->component_ID == 6) || ($subtasks_row->component_ID == 7) || ($subtasks_row->component_ID == 8)){
						$sub_components = array_merge(array_filter($subtask_components, function ($e) use ($subtask_row_id) {
								return ($e->subTask_ID == $subtask_row_id);
							}
						));
					}
					if(($exam_page_type == 1) || ($exam_page_type == 2)){
						$subtask_content_answer .= $this->get_field_layout($subtasks_row->component_ID, $subtask_row_id, $input_field_val, $subtask_answer, $sub_components);
					}else{
						$correction_layout = $this->process_correction_layout($subtasks_row->component_ID, $input_field_val, $process_scores, $answer_key, $answer_score , $subtask_row_id, $task_id, $process_id, $checklist_id, $exam_page_type, $sub_components, $subtask_answer);
						$subtask_content_answer .= $correction_layout['content'];
					}
					$subtask_content .= '<div class="row mb-4">'.$subtask_sub_label.$subtask_content_answer.'</div>';
					$subtask_section = $subtask_content_label.$subtask_content;
				}
				$task_content .= $subtask_section;
			}
			$data['task_content'] = $task_content;
			// $data['init_datepickers'] = $init_date_picker;
			// var_dump($task_content);
		}
		echo json_encode($data);
	}

	public function get_specific_correction_layout($subtask_id, $checklist_id, $process_id){
		// $subtask_id = 262;
		// $checklist_id = 67;
		// $process_id = 35;
		$specific_correction = "";
		$exam_page_type = 3;
		$subtask_details = $this->qry_subtask_details($subtask_id);
		$answer_key = $this->qry_process_answer_key($process_id);
		$answers = $this->qry_process_answers($checklist_id, [$subtask_id]);
		$answer_score = $this->qry_process_answer_score($checklist_id, [$subtask_id]);
		$subtask_components = $this->qry_process_component_subtask([$subtask_id]);
		$input_field_val = '';
		//GET SUBTASK ANSWER IF NOT PREVIEW PAGE
		if(count($subtask_details) > 0){
			$task_id = $subtask_details->task_ID;
			$process_scores = $this->qry_process_task_scores($process_id, $subtask_details->task_ID);
			$subtask_answer = array_merge(array_filter($answers, function ($e) use ($subtask_id) {
					return ($e->subtask_ID == $subtask_id);
				}
			));
			if((count($subtask_answer) > 0) && ($subtask_details->component_ID != 8)){ // GET FIELD ANSWER (except field with multiple answers such as checkbox)
				$input_field_val = $subtask_answer[0]->answer;
			}
			$sub_components = [];
			if(($subtask_details->component_ID == 6) || ($subtask_details->component_ID == 7) || ($subtask_details->component_ID == 8)){
				$sub_components = array_merge(array_filter($subtask_components, function ($e) use ($subtask_id) {
						return ($e->subTask_ID == $subtask_id);
					}
				));
			}
			$correction_layout = $this->process_correction_layout($subtask_details->component_ID , $input_field_val, $process_scores, $answer_key, $answer_score , $subtask_id, $task_id, $process_id, $checklist_id, $exam_page_type, $sub_components, $subtask_answer);
			$specific_correction = $correction_layout['specific_correction_layout'];
		}
		return $specific_correction;
	}

	protected function check_employee_exam_start($checklist_id){
		$fields = "checklistExam_ID, checklist_ID, examtype_ID, start, end, status_ID";
		$where = "checklist_ID = $checklist_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_processST_checklist_exam");
	}

	protected function check_applicant_exam_start(){
		$fields = "checklistExam_ID, checklist_ID, examtype_ID, start, end";
		$where = "checklist_ID = $checklist_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_processST_checklist_exam");
	}

	protected function check_exam_irecruit_application($code){
		$fields = "*";
		$where = "code = '$code'";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_irecruit_application");
	}

	protected function check_applicant_irecruit_exam($application_id){
		$fields = "*";
		$where = "application_ID = $application_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_irecruit_exam");
	}

	protected function check_applicant_checklist_exam($checklist_exam_id){
		$fields = "*";
		$where = "checklistExam_ID = $checklist_exam_id";
		return $this->general_model->fetch_specific_val($fields, $where, "tbl_processST_checklist_exam");
	}

	protected function start_employee_exam($checklist_id){
		
		$data['checklist_ID'] = $checklist_id;
		$data['examtype_ID'] = 2;
		$data['start'] = NULL;
		$data['status_ID'] = 2;
		return $this->general_model->insert_vals($data, "tbl_processST_checklist_exam");
	}

	protected function start_exam_timer($checklist_id){
		$dateTime = $this->get_current_date_time();
		$update_val['start'] = $dateTime['dateTime'];
		$where = "checklist_ID = $checklist_id";
		$this->general_model->update_vals($update_val, $where, 'tbl_processST_checklist_exam');
	}

	protected function not_owner_access($checklist_id, $accessor_uid)
    {
        $fields = "*";
        $where = "type='checklist' AND folProCheTask_ID = $checklist_id AND assignedTo = $accessor_uid";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_processST_assignment');
	}

	public function confirm_survey_user_acces(){
		$checklist_id = $this->input->post('checklistId');
		$session_uid = $this->session->userdata('uid'); //802;
		$process_id = $this->input->post('processId');
		$checklist = $this->get_checklist_details($checklist_id);
		if (count($checklist) > 0) {
			if ($checklist->createdBy == $session_uid) {// if owner
				$data['access_user'] = 1;
            } else {
				$not_owner_access = $this->not_owner_access($checklist_id,  $session_uid);
				if (count($not_owner_access) > 0) {
					if($not_owner_access->assignmentRule_ID == 11){ // if examinee
						$data['access_user'] = 2;
					}else if($not_owner_access->assignmentRule_ID == 13){ // if checker
						$data['access_user'] = 3;
					}else{
						$data['access_user'] = 0;
					}
                } else {
					$data['access_user'] = 0;
                }
			}
			if(($data['access_user'] == 1) || ($data['access_user'] == 3)){
				if($checklist->status_ID == 2 || $checklist->status_ID == 3){
					$page_type = 5;
				}
				$data['url'] = base_url("process/survey/$page_type/$process_id/$checklist_id");
			}else if($data['access_user'] == 2){
				if($checklist->status_ID == 2){
					$page_type = 2;
				}else if($checklist->status_ID == 3){
					$page_type = 5;
				}
				$data['url'] = base_url("process/survey/$page_type/$process_id/$checklist_id");
			}else{
				$data['url'] = base_url('general/error_403');
			}
		}else {
			$data['access_user'] = 0;
			$data['url'] = base_url('general/error_403');
		}
		echo json_encode($data);
	}

	public function confirm_user_access(){
		$checklist_id = $this->input->post('checklistId');
		$session_uid = $this->session->userdata('uid'); //802;
		$process_id = $this->input->post('processId');
		$checklist = $this->get_checklist_details($checklist_id);
		
		// var_dump($checklist);
		$data['url'] = base_url();
		if (count($checklist) > 0) {
            if ($checklist->createdBy == $session_uid) {// if owner
				$data['access_user'] = 1;
            } else {
				$not_owner_access = $this->not_owner_access($checklist_id,  $session_uid);
				// var_dump($not_owner_access);
                if (count($not_owner_access) > 0) {
					if($not_owner_access->assignmentRule_ID == 10){ // if examinee
						$data['access_user'] = 2;
					}else if($not_owner_access->assignmentRule_ID == 13){ // if checker
						$data['access_user'] = 3;
					}else{
						$data['access_user'] = 0;
						$data['url'] = base_url('general/error_403');
					}
                } else {
					$data['access_user'] = 0;
					$data['url'] = base_url('general/error_403');
                }
			}
			if(($data['access_user'] != 2) && ($data['access_user'] != 0)){
				$exam_start = $this->check_employee_exam_start($checklist_id);
				if(count($exam_start) < 1){
					$this->start_employee_exam($checklist_id);
					$exam_start = $this->check_employee_exam_start($checklist_id);
				}
				$exam_page_type = 0;
				if($exam_start->status_ID == 2){
					$exam_page_type = 1;
				}else if($exam_start->status_ID == 3){
					$exam_page_type = 3;
				}else if($exam_start->status_ID == 28){
					$exam_page_type = 4;
				}
				$data['url'] = base_url("process/exam/$exam_page_type/$process_id/$checklist_id");
			}
        } else {
			$data['access_user'] = 0;
			$data['url'] = base_url('general/error_403');
		}
		echo json_encode($data);
	}

	public function get_respondent_access(){
		$checklist_id = $this->input->post('checklistId');
		$session_uid = $this->session->userdata('uid'); //802;
		$process_id = $this->input->post('processId');
	}

	public function get_exam_access_page(){
		// $checklist_id = 77;
		// $session_uid = 275; //802;
		// $process_id = 35;
		$data['started'] = 1;
		$checklist_id = $this->input->post('checklistId');
		$session_uid = $this->session->userdata('uid'); //802;
		$process_id = $this->input->post('processId');

		$exam_start = $this->check_employee_exam_start($checklist_id);
		if(count($exam_start) < 1){
			$this->start_employee_exam($checklist_id);
			$exam_start = $this->check_employee_exam_start($checklist_id);
		}
		if($exam_start->start == NULL){
			$data['started'] = 0;
		}
		$data['exam_stat'] = $exam_start->status_ID;
		$exam_page_type = 0;
		if($exam_start->status_ID == 2){
			$exam_page_type = 2;
		}else if($exam_start->status_ID == 28){
			$exam_page_type = 4;
		}
		if($exam_start->status_ID != 3){
			$data['url'] = base_url("process/exam/$exam_page_type/$process_id/$checklist_id");
		}
		// var_dump($data);
		echo json_encode($data);
	}

	public function confirm_exam_access(){
		$uid = $this->session->userdata('uid');
		$accessing_user_details = $this->get_emp_details_via_user_id($uid);
		$data['started'] = 1;
		$data['opened_by_examinee'] = 0;
		$data['exam_page_type'] = 1;
		$checklist_id = $this->input->post('checklistId');
		$exam_start = $this->check_employee_exam_start($checklist_id);
		if(count($exam_start) < 1){
			$this->start_employee_exam($checklist_id);
		}
		$exam_start = $this->check_employee_exam_start($checklist_id);
		if($exam_start->examtype_ID == 1){
			$emp_exam_details = $this->get_applicant_exam_details($exam_start->checklistExam_ID);
			if($emp_exam_details->apid == $accessing_user_details->apid){
				$data['opened_by_examinee'] = 1;
			}
		}else{
			$emp_exam_details = $this->get_employee_exam_details($checklist_id);
			$emp_details = $this->get_emp_details_via_user_id($emp_exam_details->assignedTo);
			if($emp_details->uid == $uid){
				$data['opened_by_examinee'] = 1;
			}
		}

		if($data['opened_by_examinee']){
			if($exam_start->status_ID == 2){
				$data['exam_page_type'] = 2;
			}else if($exam_start->status_ID == 3){
				$data['exam_page_type'] = 0;
			}else if($exam_start->status_ID == 28){
				$data['exam_page_type'] = 4;
			}
		}else{
			if($exam_start->status_ID == 2){
				$data['exam_page_type'] = 1;
			}else if($exam_start->status_ID == 3){
				$data['exam_page_type'] = 3;
			}else if($exam_start->status_ID == 28){
				$data['exam_page_type'] = 4;
			}
		}
		if($exam_start->start == NULL){
			$data['started'] = 0;
		}
		echo json_encode($data);
	}

	// START EXAM FUNCTION 
	public function open_employee_exam($checklist_id){
		$session_uid = $this->session->userdata('uid');
		$not_owner_access = $this->not_owner_access($checklist_id,  $session_uid);
		if (count($not_owner_access) > 0) {
			if($not_owner_access->assignmentRule_ID == 10){ // if examinee
				$this->start_exam_timer($checklist_id);
			}
        } 
	}

	public function open_applicant_exam($code = null){
		$data['error'] = "";
		$code = 'SZAPP0121';
		$application = $this->check_exam_irecruit_application($code);
		if(count($application) > 0){
			$irecruit = $this->check_applicant_irecruit_exam($application->application_ID);
			if(count($irecruit) > 0){
				$checklist = $this->check_applicant_checklist_exam($irecruit->checklistExam_ID);
				var_dump($checklist);
			}
		}else{

		}
	}
	// PROCESS MANAGEMENT ADDITIONS MICHAEL END
	 public function tax_contri($month,$emp_promoteId=0,$year){
		$empP = ($emp_promoteId!=0) ? 'and c.emp_promoteId='.$emp_promoteId : '';
	   $query = $this->db->query("SELECT payroll_id,c.emp_promoteId,quinsina,sss_details,phic_details,hdmf_details,bir_details,lname,fname,mname,period,d.emp_id,salarymode, a.isDaily,month,e.birthday,e.civilStatus as civil,e.presentAddress as address,d.bir,d.id_num,a.coverage_id,a.absentLate,a.adjust_details,a.bonus_details,a.*,d.separationDate,d.reasonForLeaving from tbl_payroll a,tbl_payroll_coverage b,tbl_emp_promote c,tbl_employee d,tbl_applicant e where a.coverage_id=b.coverage_id and c.emp_promoteId=a.emp_promoteId and c.emp_id=d.emp_id and e.apid=d.apid and month in ( '" . implode($month, "', '") . "' ) and year='".$year."' $empP order by lname"); 
	  
	  return $query->result();
	}
		public function getTaxCol($value){
		$amount= floatval($value);
		if($amount<=10417){
			$tax = 0;
		}else if($amount>=10418 && $amount<=16666){
 			$tax = ($amount-10417)*0.20;
		}else if($amount>=16667 && $amount<=33332){
 			$tax = 1250+(($amount-16666)*0.25);
		}else if($amount>=33333 && $amount<=83332){
 			$tax = 5416.67+(($amount-33332)*0.30);
		}else if($amount>=83333 && $amount<=333332){
 			$tax = 20416.67+(($amount-83332)*0.32);
		}else if($amount>=333333){
 			$tax = 100416.67+(($amount-333332)*0.35);
		}else{
			$tax = 0;
		}
		return $tax;
	}
	
	// AHR CHECKER 
	public function process_correct_ahr($shift_start, $shift_end, $ahr_type, $hours, $sched_logs){
        
        $corrected_ot = [];
        $corrected_ot_start_date = "";
        $corrected_ot_start_time = "";
        $corrected_ot_end_date = "";
        $corrected_ot_end_time = "";
        switch ((int) $ahr_type) {
            case 1:
                // var_dump('after');
                $ot_end_date = date_create($shift_end);
                date_add($ot_end_date, date_interval_create_from_date_string(($hours*3600).' seconds'));
                $corrected_ot_start_date = date('Y-m-d', strtotime($shift_end));
                $corrected_ot_start_time = date('H:i', strtotime($shift_end));
                $corrected_ot_end_date = date_format($ot_end_date, 'Y-m-d');
                $corrected_ot_end_time = date_format($ot_end_date, 'H:i');
                // var_dump($corrected_ot_end_date." ".$corrected_ot_end_time);
                break;
            case 2:
                // var_dump('pre');
                // $ot_start_date = date_create($shift_start);
                $corrected_ot_start_date = date('Y-m-d', strtotime($shift_start) - $hours * 3600);
                $corrected_ot_start_time = date('H:i', strtotime($shift_start) - $hours * 3600); 
                $corrected_ot_end_date = date('Y-m-d', strtotime($shift_start));
                $corrected_ot_end_time = date('H:i', strtotime($shift_start));
                break;
            case 3:
                // var_dump('word');
                $clock_in = 'N/A';
                $clock_out = 'N/A';
                if(count($sched_logs) > 0){
                    foreach($sched_logs as $sched_logs_row){
                        if($sched_logs_row->entry == 'I'){
                            $clock_in = $sched_logs_row->log;
                        }else if($sched_logs_row->entry == 'O'){
                            $clock_out = $sched_logs_row->log;
                        }
                    }
                    if(strtotime($shift_start) < strtotime($clock_in)){
                        var_dump('late');
                        $corrected_ot_start_date = date('Y-m-d', strtotime($clock_in));
                        $corrected_ot_start_time = date('H:i', strtotime($clock_in));
                    }else{
                        $corrected_ot_start_date = date('Y-m-d', strtotime($shift_start));
                        $corrected_ot_start_time = date('H:i', strtotime($shift_start));
                    }
                    if(strtotime($clock_out) < strtotime($shift_end)){
                        $corrected_ot_end_date = date('Y-m-d', strtotime($clock_out));
                        $corrected_ot_end_time = date('H:i', strtotime($clock_out));
                    }else{
                        $corrected_ot_end_date = date('Y-m-d', strtotime($shift_end));
                        $corrected_ot_end_time = date('H:i', strtotime($shift_end));
                    }
                }
                break;
        } 
        if($corrected_ot_start_date != "" && $corrected_ot_start_time != "" && $corrected_ot_end_date != "" && $corrected_ot_end_time != ""){
            $corrected_ot = $this->start_end_shift_24($corrected_ot_start_date.' '.$corrected_ot_start_time, $corrected_ot_end_date.' '.$corrected_ot_end_time); // function is in general controller
        }
        return $corrected_ot;
    }
	public function qry_logs($sched_ids){
        $qry = "SELECT dtr.dtr_id, dtr.info, dtr.emp_id, dtr.acc_time_id, dtr.entry, dtr.log, dtr.note, dtr.sched_id FROM tbl_dtr_logs dtr WHERE dtr.sched_id IN ($sched_ids)";
        return $this->general_model->custom_query($qry);
    }
}

