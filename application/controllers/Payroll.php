<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Payroll extends General
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *     - or -
     *         http://example.com/index.php/welcome/index
     *     - or -
     */
	protected $title = array('title' => 'Payroll');
	public $logo=  "/var/www/html/sz/assets/images/img/logo2.png"; // Linux
	// public $logo = "C:\\www\\sz\\assets\\images\\img\\logo2.png"; //windows
	 public $bootstrapcss = '';
     public function coverage(){
		$data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'Payroll Coverage',
        ];
		if ($this->check_access()) {
			$this->load_template_view('templates/payroll/index', $data);
		}
    }
	public function dtrupload(){
        $record["title"] = "DTR Upload";
		if ($this->check_access()) {
			$this->load_template_view('templates/payroll/dtrupload', $record);
		}
	}
	public function report(){
        $record["title"] = "Report";
		if ($this->check_access()) {
			$this->load_template_view('templates/payroll/report/index', $record);
		
		}
	}
	public function summaryreport(){
        $record["title"] = "Report";
		if ($this->check_access()) {
			$this->load_template_view('templates/payroll/report/summaryreport', $record);
		
		}
	}
	public function bankreport(){
        $record["title"] = "Report";
		if ($this->check_access()) {
			$this->load_template_view('templates/payroll/report/bankreport', $record);
		
		}
	}
	public function bankreport2(){
        $record["title"] = "Report";
		if ($this->check_access()) {
			$this->load_template_view('templates/payroll/report/bankreport_new', $record);
		
		}
	}
	public function birreport(){
        $record["title"] = "Report";
		if ($this->check_access()) {
 			$this->load_template_view('templates/payroll/report/bir', $record);
		}
	}
	public function pasko(){
        $record["title"] = "Report";
		if ($this->check_access()) {
 			$this->load_template_view('templates/payroll/report/pasko', $record);
		}
	}
	public function government(){
        $record["title"] = "Report";
		  if ($this->check_access()) {
 			$this->load_template_view('templates/payroll/report/government', $record);
		  }
	}
	public function governmentPremium(){
        $record["title"] = "Report";
		  if ($this->check_access()) {
 			$this->load_template_view('templates/payroll/report/governmentPremium', $record);
		  }
	}
	public function payrollreport(){
        $record["title"] = "Report";
		if ($this->check_access()) {
			$this->load_template_view('templates/payroll/report/payrollreport', $record);
		
		}
	}
	public function payslip(){
        $record["title"] = "Payslip";
		$this->load_template_view('templates/payroll/payslip', $record);
	}
	public function attendance($type,$empstat,$site,$coverage){
		$fields = "*";
        $where = "coverage_id=$coverage";
        $record["pcover"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_coverage");
        $record["site"] = $site;
        $record["type"] = $type;
        $record["empstat"] = $empstat;
        $record["site"] = $site;

        $record["title"] = "Employee's Payroll";
			$current_uri = $this->uri->uri_string();
			$site = (strpos($current_uri,"cdo")) ? "cdo" : "cebu";
			$rs = $this->checkIfSiteOfficer($site);
			if($rs){
				  if($this->check_access_payroll()){
					$this->load_template_view('templates/payroll/attendance', $record);
				  }
			} 
		
		
	}
	public function adjustment($act,$type,$empstat,$site,$coverage){
		$fields = "*";
        $where = "coverage_id=$coverage";
        $record["pcover"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_coverage");
        $record["site"] = $site;
        $record["type"] = $type;
        $record["act"] = $act;
        $record["empstat"] = $empstat;
        $record["site"] = $site;

        $record["title"] = "Employee's Payroll Adjustment";
			$current_uri = $this->uri->uri_string();
			$site = (strpos($current_uri,"cdo")) ? "cdo" : "cebu";
			$rs = $this->checkIfSiteOfficer($site);
			if($rs){
				  if($this->check_access_payroll()){
					$this->load_template_view('templates/payroll/adjustment', $record);
				  }
			}
		
		
	}
	public function calculator($type,$empstat,$site,$coverage){
		$fields = "*";
        $where = "coverage_id=$coverage";
        $record["pcover"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_coverage");
        $record["site"] = $site;
        $record["type"] = $type;
        $record["empstat"] = $empstat;
        $record["site"] = $site;

        $record["title"] = "Employee's Payroll Calculator Page";
			$current_uri = $this->uri->uri_string();
			$site = (strpos($current_uri,"cdo")) ? "cdo" : "cebu";
			$rs = $this->checkIfSiteOfficer($site);
			if($rs){
				  if($this->check_access_payroll()){
					$this->load_template_view('templates/payroll/calculator', $record);
				  }
			} 
	}
	public function finalpay($type,$empstat,$site,$coverage){
		$fields = "*";
        $where = "coverage_id=$coverage";
        $record["pcover"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_coverage");
        $record["site"] = $site;
        $record["type"] = $type;
        $record["empstat"] = $empstat;
        $record["site"] = $site;

        $record["title"] = "Employee's Payroll Calculator Page";
			$current_uri = $this->uri->uri_string();
			$site = (strpos($current_uri,"cdo")) ? "cdo" : "cebu";
			$rs = $this->checkIfSiteOfficer($site);
			if($rs){
				  if($this->check_access_payroll()){
					$this->load_template_view('templates/payroll/final_pay', $record);
				  }
			} 

		
	}
	public function general($type,$empstat,$site,$coverage){
		$fields = "*";
        $where = "coverage_id=$coverage";
        $record["pcover"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_coverage");
        $record["title"] = "Employee's Payroll";
        $record["site"] = $site;
        $record["type"] = $type;
        $record["empstat"] = $empstat;
        $current_uri = $this->uri->uri_string();
		$site = (strpos($current_uri,"cdo")) ? "cdo" : "cebu";
		$rs = $this->checkIfSiteOfficer($site);
		if($rs){
			  if($this->check_access_payroll()){
				$this->load_template_view('templates/payroll/general', $record);
			  }
		} 
	}	
	public function emp($site,$coverage){		
		$fields = "*";
        $where = "coverage_id=$coverage";
        $record["pcover"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_coverage");
        $record["title"] = "Employee's Payroll";
        $record["site"] = $site;
		
		$current_uri = $this->uri->uri_string();
		$site = (strpos($current_uri,"cdo")) ? "cdo" : "cebu";
		$rs = $this->checkIfSiteOfficer($site);
		 if($rs){
			$this->load_template_view('templates/payroll/payroll_index', $record);
		 }
	}
	public function payrollcoverage($year,$month){
		$append = ($month=="All") ? "" : " and month='".$month."'";
		$fields = "*";
        $where = "year=$year ".$append;
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_coverage");
		
		echo json_encode($record);
	}
 
	public function loadEmployeeFlexiSched(){
		$fields = "emp_id,a.apid,fname,lname";
        $where = "a.apid=b.apid and isFlexiSched=1";
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_applicant a,tbl_employee b");
		
		echo json_encode($record);
	}
	public function payslipreport(){
        $record["title"] = "DTR Upload";
		if ($this->check_access()) {
			$this->load_template_view('templates/payroll/report/payslipreport', $record);
		}
	}
	public function loadPayrollEmployees(){
		$emp_id_pofficer = $_SESSION["emp_id"];
		if($emp_id_pofficer==26){
			$append = "";
		}elseif($emp_id_pofficer==386 or $emp_id_pofficer==1428 or $emp_id_pofficer==1639 or $emp_id_pofficer ==1938){ // Agnes && Ana && Raquel && 10/21/2021 Pacuribot
			$append = " and confidential=0";
		}elseif($emp_id_pofficer==48 or $emp_id_pofficer==1967){ // Maam Joy : 02/11/2020 4:46PM || Ms Emma 2021/03/31 1:25pm
			$append = " and confidential=0";
		}elseif($emp_id_pofficer==522){ // Tina
			$append = "  and confidential=1";
		}elseif($emp_id_pofficer==147){ // Shendie
			// $append = "  and confidential=1";
			$append = "  and (confidential = 1 OR acc_id = 32)"; // confi and TSN
		}else{ // if wala jud sa payroll
			$append = " and confidential=3"; #3 dont exist
		}
		$coverage = $this->input->post("coverage");
		$fields = "payroll_id,c.emp_id,fname,lname";
        $where = "a.emp_promoteID=b.emp_promoteID and b.emp_id=c.emp_id and c.apid=d.apid and coverage_id=$coverage ".$append;
        $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d","lname ASC");
		
		echo json_encode($record);
	}
	public function employeePayslip($bulk=null){
		
		
		
		$coverage = $this->input->post("coverage");
		$emp = ($bulk==null) ? $this->input->post("emp_id") : implode(",",$this->input->post("emp_id"));
				$fields = "fname,lname,isAtm,pos_details,pos_name,b.*,e.*";
				$tables= "tbl_emp_promote a,tbl_payroll b,tbl_pos_emp_stat c,tbl_position d,tbl_payroll_coverage e,tbl_employee f,tbl_applicant g";
				$where = "a.emp_promoteID = b.emp_promoteID and b.coverage_id = e.coverage_id and c.posempstat_id=a.posempstat_id and c.pos_id=d.pos_id and a.emp_id=f.emp_id and f.apid=g.apid and a.emp_id in ($emp) and b.coverage_id=$coverage ";
				$data["payslip"] = $this->general_model->fetch_specific_vals($fields, $where, $tables, "lname ASC");	
 
				// $data["employee"] = $this->general_model->fetch_specific_vals("fname,lname,isAtm", "a.apid=b.apid and emp_id=$emp","tbl_employee a,tbl_applicant b");	
				
			echo json_encode($data);				
	}
	public function adjustmentOption(){
		
		$act = $this->input->post("act");
		$coverage = $this->input->post("coverage");
		$emp = $this->input->post("emp");
		
		$fields =($act=="add") ? "paddition_id as id,additionname as options" : "pdeduct_id as id,deductionname as options" ;
		$table = ($act=="add") ? "tbl_payroll_addition" : "tbl_payroll_deductions";
        $where = ($act=="add") ? "paddition_id not in (SELECT paddition_id FROM tbl_payroll_emp_addition where emp_id=".$emp." and coverage_id=".$coverage.")" :  "pdeduct_id not in (SELECT pdeduct_id FROM tbl_payroll_emp_deduction where emp_id=".$emp." and coverage_id=".$coverage.")";
		
		$qry = "select ".$fields." from ".$table." where isActive=1 and ".$where; 
		$record = $this->general_model->custom_query($qry);

		echo json_encode($record);
	}
	public function addAdjustment(){
		$act = $this->input->post("act");
		$coverage = $this->input->post("coverage");
		$emp = $this->input->post("emp");
		$amount = $this->input->post("amount");
		$adj = $this->input->post("adj");
		
		$field_id =($act=="add") ? "paddition_id" : "pdeduct_id" ;
		$table = ($act=="add") ? "tbl_payroll_emp_addition" : "tbl_payroll_emp_deduction";
		
  		$data[$field_id] = $adj;
		$data["coverage_id"] = $coverage;
		$data["value"] = $amount;
		$data["emp_id"] = $emp;
		echo $record = $this->general_model->insert_vals($data, $table);

		
	}
	public function payroll_calculate(){ 
		error_reporting(E_ALL ^ E_WARNING); 
		$emp = $this->input->post("emp");
		$coverage=$this->input->post("coverage");
		$coverage_text=$this->input->post("coverage_text");
		$empstat=$this->input->post("empstat");
		$class_type=$this->input->post("class_type");
		$empzSite=$this->input->post("site");
		$site = (strtoupper($empzSite)=="CDO") ? "" : "-".$empzSite;
		if(isset($emp)){
		// $ifCCaTrainee = trim($class_type)."-".trim($empstat);

		$attendance = $this->attendance_checker($emp,$coverage,$coverage_text,$empstat,$class_type,"return");
			
			if(count($attendance)>0){
				
				foreach($attendance["emp"] as $row => $val){
					$ifSSSdeduct  = 1; // means deductan
					$ifPHICdeduct  = 1; // means deductan
					$ifBIRdeduct  = 1; // means deductan
					$ifHDMFdeduct  = 1; // means deductan

					$total_shift_work = 0;
					$night_diff = 0;
					$hazard_pay_time = 0;
					$rate_quinsina = 0;
					$rate_daily = 0;
					$rate_hourly = 0;
					$rate_nd = 0;
					$hpRateTotalPerc = 0;
					$hpRate = 0;
					$ndRate = 0;
					$hpCnt = 0;
					$ahr_val = 0;
					$ahr_id = array();
					$leave_id = array();
					$holidaywork = 0;
					$ut = 0;
					$totalUTAbsent = 0;
					$leaveDeduct = 0;
					$late = 0;
					$absent = 0;
					$hour_nd = 0;
					$hour_ho = 0;
					$hour_hp = 0;
					$ahr = 0;
					$hdmf_employer = 0;
					$phic_employer = 0;
					$sss_employer = 0;
					$total_shift_work_OB_include = 0;
					$ahr_cost = array();
					$total_shift_work_OB = array();
					$salarymode="Monthly";
					$coverage_start= date("Y-m-d");

					foreach($val as $row1 => $val1){
					if($row1!="absent_system"){
						$total_shift_work += $val1["total_shift_work"];
						$total_shift_work_OB[] = $val1["sched_type"];
						$total_shift_work_OB_include += ($val1["sched_type"]=="Official Business") ? 1: 0;
						$night_diff += $val1["ND"];
						$hazard_pay_time += $val1["HP"];
						$hpRateTotalPerc += $val1["hpRate"];
						$holidaywork += $val1["holidaywork"];
						$late += $val1["issue_late"]/60;
						$ut += $val1["issue_ut"]/60;
						$absent += $val1["issue_absent"]/60;
						$totalUTAbsent += $val1["totalUTAbsent"]/60;
						$leaveDeduct += $val1["leaveDeduct"]/60;
						$hpCnt += ($val1["hpRate"]>0) ? 1  : 0;
						$ifSSSdeduct 	 = ($val1["sss_num"]=="") ? 0 : 1;
						$ifPHICdeduct 	 = ($val1["philhealth_num"]=="") ? 0 : 1;
						$ifBIRdeduct 	 = ($val1["bir_num"]=="") ? 0 : 1;
						$ifHDMFdeduct 	 = ($val1["pagibig_num"]=="") ? 0 : 1;
						$pagibig_num 	 =	$val1["pagibig_num"];
						$salarymode 	 =	$val1["salarymode"];
						$coverage_start	 = 	$val1["coverage_start"];
						

						if(!empty($val1["Ot_arr"])){
							foreach($val1["Ot_arr"] as $rowAHR => $valAHR){
								$ahr_val +=  $valAHR["ahr"];
								$ahr_id[] = $valAHR["id"];
							}
						}
						
						
						// $ahr = $this->check_ahr($val1["uid"],$coverage,$coverage_text);
												
					} 
					}
					
					//Deduct LEave in Absent
					$totalLeaveDeductinDTRIssue = $totalUTAbsent- $leaveDeduct;
					//  $totalUTAbsent =($totalLeaveDeductinDTRIssue <=0) ? 0 :  $totalLeaveDeductinDTRIssue;
						$emp_id = $val["absent_system"]["emp_id"];
						$uid = $val["absent_system"]["uid"]; 
						$account = $val["absent_system"]["account"];
						$lname = $val["absent_system"]["lname"];
						$fullname =  $val["absent_system"]["lname"].", ". $val["absent_system"]["fname"];
						$leave_id= $this->leaveCheck_id($uid,$coverage_text);
					$absent_dates = ($val["absent_system"]["dates"]!=0) ? $val["absent_system"]["dates"] : array();
					$absent += count($val["absent_system"]["dates"])*8;
					$pos_details = $this->getCurrentPositionRates($emp_id);
					// if(count($pos_details) <= 0){
						// echo $lname." ".$emp_id;
						// die();
					// }
						
					$allowances = $this->getAllowances($pos_details[0]->posempstat_id);
					$bonus = $this->getBonuses($pos_details[0]->posempstat_id);
					$bonus_tax = $this->bonus_tax();
					$addition_tax = $this->additionName_tax();
					
					$adjAdd = $this->getAdjAdd($emp_id,$coverage);
					$adjMinus = $this->getAdjMinus($emp_id,$coverage);

					
					
					
					if(trim($empstat)=="trainee" || trim($empstat)=="trainee-tsn"){
 						$rate = ($pos_details[0]->dep_name=='SMT') ? (($pos_details[0]->rate*313)/12) : (($pos_details[0]->rate*261)/12);
						$rate_monthly = "0.00";
						$rate_daily = $pos_details[0]->rate;
						$rate_hourly = $rate_daily/8;
						$rate_quinsina = $rate_hourly*$total_shift_work;
						/* $hdmf = ($class_type=="admin") ? 75 : 0;
						$sss = ($class_type=="admin") ? 150 : 0; */
						$hdmf = ($class_type=="admin") ? 95 : 0;
						// $sss = ($class_type=="admin") ? 190 : 0; // updated since 1-19-2020 
						// $sss_employer = ($class_type=="admin") ? 380 : 0;  updated since 1-19-2020 
						$temp_monthly = $rate_daily*21.75;
						$sss = $this->SSS_Contri($temp_monthly);
						$sss_employer=(float)$sss["ER"]/2;
						$sss=(float)$sss["EE"]/2;
						$emp_type= ($class_type=="admin") ? "admin-".trim($empstat).$site : "agent-".trim($empstat).$site;
						
 					}else{
						$rate_monthly = $pos_details[0]->rate;
						$rate_quinsina = ($pos_details[0]->rate/2);
						$rate_daily = ($rate_quinsina*24)/261;
						$rate_hourly = $rate_daily/8;
							if($emp_id==1462){ // Sir Randy requested to change his deduction to 50 pesos per pay. Changed on Jan. 16, 2020
								$hdmf = 50; 
							}else{
								$hdmf = ($pos_details[0]->rate*0.02)/2;
							}
						$rate_temp = ((float) $pos_details[0]->rate >0) ? $pos_details[0]->rate : 1000  ;
						$sss = $this->SSS_Contri($rate_temp);
						$sss_employer=(float)$sss["ER"]/2;
						$sss=(float)$sss["EE"]/2;
							if($class_type=="admin"){
								$emp_type = ($empstat=="confidential") ? "admin-both-confi".$site : (($empstat=="probationary-regular-tsn") ? "admin-both-nonconfi-tsn".$site: "admin-both-nonconfi".$site);
							}else{
								$emp_type = "agent-".$empstat.$site;
							}
						$totalUTAbsent +=(count($absent_dates)*8);
					}
					$hdmfcheck = $this->CheckHDMFdeduct($emp_id);
					$hdmf_employer = $hdmf;
					$hdmf += (count($hdmfcheck)>0) ? $hdmfcheck->amount/2 : 0;
 					$rate_nd = $rate_hourly*.15;
					$hpRate = ($hpCnt>0) ? ($hpRateTotalPerc/$hpCnt) * $hazard_pay_time * $rate_hourly : 0;
					$ndRate = $night_diff*$rate_nd ;
					$hour_nd = $night_diff;
					$hour_ho = $holidaywork;
					$hour_hp = $night_diff;
					
					
					//$phic = $this->PHIC_Contri($pos_details[0]->rate,2); //Year 2018 - changed codes due to new policy of PHIC. Changed on Jan 16, 2020
					// $phic = ($salarymode=="Daily") ? 75 :$this->PHIC_Contri($pos_details[0]->rate,4);
					if($salarymode=="Daily"){
					 $phic = 75;
					}else{
				 
						$phic_deduct_init = $this->PHIC_Contri($pos_details[0]->rate,4,$coverage_start);
						 $phic = ($phic_deduct_init<=75) ? 75 : $phic_deduct_init;

					 
											}
					
					$phic_employer = $phic;
					$bir_total_rate = $pos_details[0]->rate - ($phic*2) - ($sss*2) - ($hdmf*2);
					$bir = $this->BIR_Contri($bir_total_rate);
					
					// foreach($ahr as $row2 => $val2){
						// $ahr_cost[] =  $this->check_ahr($val2["otStartDate"],$coverage_text);
					// }
		  	 
			 

					// TRAIN LAW
							$totalFinalBasicAllowance = 0;
							$totalFinalAllowance = 0;
							$totalFinalBonus = 0;
							$totalFinalAddTxt = array();
							$totalFinalBasicAddTxt = array();
							$totalFinalBonusTxt = array();
 							/* if(count($adjAdd)>0){
								foreach($adjAdd as $row_add){
										if(strpos(strtolower(trim($row_add->additionname)), "allowance") == TRUE){
												$totalFinalAllowance+=floatval($row_add->value);
												array_push($totalFinalBonusTxt,$row_add->additionname);
										} else if(strpos(strtolower(trim($row_add->additionname)), "bonus") == TRUE){
  												$totalFinalBonus+=floatval($row_add->value);
 										}
									 
								}
							} */
							$arrAdditionTax = array_column($addition_tax, 'addition_name');
							if(count($allowances)>0){
								foreach($allowances as $allowances_add){
 													$totalFinalBasicAllowance+=floatval($allowances_add->value/2);
													array_push($totalFinalBasicAddTxt,$allowances_add->allowance_name);
 
 								}								
							}
							if(count($adjAdd)>0){
								foreach($adjAdd as $row_add){
									// if(count($addition_tax)>0){
										// if(in_array(strtolower(trim($row_add->additionname)),$arrAdditionTax)){
											// if(strpos(strtolower(trim($row_add->additionname)), "allowance") == TRUE){
												// $totalFinalAllowance+=floatval($row_add->value);
												// array_push($totalFinalAddTxt,$row_add->additionname);
											// } else if(strpos(strtolower(trim($row_add->additionname)), "bonus") == TRUE){
													// $totalFinalBonus+=floatval($row_add->value);
													// array_push($totalFinalAddTxt,$row_add->additionname);
											// }
									 
										// }
									// }else{
										if(strtolower(trim($row_add->additionname))!=="client bonus"){
											 if(strpos(strtolower(trim($row_add->additionname)), "bonus") == TRUE){
													$totalFinalBonus+=floatval($row_add->value);
													array_push($totalFinalAddTxt,$row_add->additionname);
											}else{
												// if(strpos(strtolower(trim($row_add->additionname)), "allowance") == TRUE){
													$totalFinalAllowance+=floatval($row_add->value);
													array_push($totalFinalAddTxt,$row_add->additionname); 
											}
 										}
									 
									// }
										
								}
							}
							$arrBonuzTax = array_column($bonus_tax, 'bonus_name');
							
							if(count($bonus)>0){
								foreach($bonus as $row_bon){
										// if(count($bonus_tax)
										// if(count($bonus_tax)>0){
											// if(in_array(strtolower(trim($row_bon->bonus_name)),$arrBonuzTax)){
												// if(strpos(strtolower(trim($row_bon->bonus_name)), "allowance") == TRUE){
													// $totalFinalAllowance+=floatval($row_bon->amount/2);
													// array_push($totalFinalBonusTxt,strtolower(trim($row_bon->bonus_name))." = ".$row_bon->amount/2);
												// } else if(strpos(strtolower(trim($row_bon->bonus_name)), "bonus") == TRUE){
														// $totalFinalBonus+=floatval($row_bon->amount/2);
														// array_push($totalFinalBonusTxt,strtolower(trim($row_bon->bonus_name))." = ".$row_bon->amount/2);
												// }
											// }
										// }else{
											if(strtolower(trim($row_bon->bonus_name))!=="client bonus"){
												if(strpos(strtolower(trim($row_bon->bonus_name)), "allowance") == TRUE){
													$totalFinalAllowance+=floatval($row_bon->amount/2);
													array_push($totalFinalBonusTxt,$row_bon->amount/2);
												} else if(strpos(strtolower(trim($row_bon->bonus_name)), "bonus") == TRUE){
														$totalFinalBonus+=floatval($row_bon->amount/2);
														array_push($totalFinalBonusTxt,$row_bon->amount/2);
												}
											}
										// }
										
										
									 
								}
							}
							$sss_final	 = ($pos_details[0]->status!="Trainee") ? (($ifSSSdeduct  ==1) ? $sss : 0): 0;
							$phic_final  = ($pos_details[0]->status!="Trainee") ? (($ifPHICdeduct ==1) ? $phic : 0 ): 0;
							$bir_final   = ($pos_details[0]->status!="Trainee") ? (($ifBIRdeduct  ==1 ) ? $bir : 0): 0;
							$hdmf_final  = ($pos_details[0]->status!="Trainee") ? (($ifHDMFdeduct ==1) ? $hdmf : 0 ): 0;
							$ahr_calc = floatVal($ahr_val) >0 ? $ahr_val : 0;
							// $ideal13MoPay = floatval(($pos_details[0]->rate/2)/24);
							$ideal13MoPay = floatval((($pos_details[0]->rate/2) - floatval($totalUTAbsent*$rate_hourly))/12);
							
							$totalGrossTrain  = floatval($pos_details[0]->rate/2) + $ndRate + $hpRate + ($rate_hourly*$holidaywork)+ ($rate_hourly*$ahr_calc) + $totalFinalBasicAllowance+ $ideal13MoPay  + $totalFinalAllowance + $totalFinalBonus ;
							 
							$totalGrossTrainTxt  = "";
							$totalNetTrain_1  = $totalGrossTrain - (floatval($totalUTAbsent*$rate_hourly) + floatval($sss_final) + floatval($phic_final) + floatval($hdmf_final));
							
							$assumption90k = ($totalNetTrain_1>10417) ? (90000/24) : $ideal13MoPay ;
							
 							$totalNetTrain_2  = $totalGrossTrain - (floatval($totalUTAbsent*$rate_hourly) + floatval($sss_final) + floatval($phic_final) + floatval($hdmf_final) + floatval($assumption90k) );
							$totalAmountTrain  =   floatval($totalGrossTrain) - (floatval($sss_final) + floatval($phic_final) + floatval($hdmf_final));
							$totalAmountTrainTxt  =   "";
							$trainTax = $this->getTaxCol($totalNetTrain_2);
							$trainTax_text = $this->getTaxCol($totalNetTrain_2);
							// $bir_final   = ($bir_final!=0)  ? $trainTax : 0;
							#6-29-2021 - Temporarily disabled because requested by NVD
							#8-17-2021 - JEss informed that TRAIN LAW will be applied to confi employees
 

 
							// $isTrainDeduct = 0;
							// if($empstat!="probationary-regular" && $empstat!="trainee" ){
								// $bir_final   = ($bir_final!=0)  ? $trainTax : 0;
								$bir_final   = ($pos_details[0]->status!="Trainee") ? (($ifBIRdeduct  ==1 ) ? $trainTax : 0) : 0;
								// $bir_final   = $trainTax;
								 $isTrainDeduct = 1;
							// }
							
					// TRAIN LAW
					
					$loan = $this->checkIfHasLoan($coverage,$emp_id);  
   
 					$arr["payroll"][$lname."-".$emp_id] = array(
						"0011totalGrossTrain"=> $totalGrossTrain,
						"0011isTrainDeduct"=> $isTrainDeduct,
						"000trainTax_text"=> $trainTax_text, 
						"00totalFinalBasicAllowance"=> $totalFinalBasicAllowance,
						"00totalFinalAllowance"=> $totalFinalAllowance,
						"00totalFinalBonus"=> $totalFinalBonus,
						"111totalNetTrain_2"=> $totalNetTrain_2,
						"222ideal13MoPay"=> $ideal13MoPay,
						"333assumption90k"=> $assumption90k,
						"bir_total_rate"=> $bir_total_rate,
						"employer_hdmf"=> $hdmf_employer,
						"coverage_start"=> $coverage_start,
						"employer_phic"=> $phic_employer,
						"employer_sss"=> $sss_employer, 
						"1"=> $absent,
						"3"=> $total_shift_work_OB,
						"total_shift_work_OB"=> $total_shift_work_OB_include,
						"2"=> $absent_dates,
						"pos_details"=> $pos_details,
						"contri_sss"=> $sss_final,
						"contri_phic"=> $phic_final,
						"contri_bir"=> $bir_final,
						"contri_hdmf"=> $hdmf_final,
						"ifSSSdeduct"=>$ifSSSdeduct ,
						"ifPHICdeduct"=>$ifPHICdeduct  , 
						"ifBIRdeduct"=>$ifBIRdeduct  , 
						"_train"=>$trainTax  , 
						"_totalGrossTrainTxt"=>$totalGrossTrainTxt  , 
						"_totalAmountTrainTxt"=>$totalAmountTrainTxt  , 
						"_totalFinalBonusTxt"=> $totalFinalBonusTxt, 
						"_totalFinalAddTxt"=> $totalFinalAddTxt, 
						"_add_tax"=>$addition_tax,
						"_bonus_tax"=>$arrBonuzTax ,
						"ifHDMFdeduct"=>$ifHDMFdeduct,
						"emp_id"=> $emp_id,
						"fullname"=> $fullname,
						"total_shift_work"=> $total_shift_work,
						"night_diff"=> $night_diff,
						"Rate_monthly"=> $rate_monthly,
						"Rate_quinsina"=> $rate_quinsina,
						"Rate_daily"=> $rate_daily,
						"Rate_hourly"=> $rate_hourly,
						"Rate_nd"=> $rate_nd,
						"hazard_pay_cost"=>  $hpRate ,
						"night_diff_cost"=>  $ndRate ,
						"allowances"=> $allowances,
						"bonus"=> $bonus,
						"holidaywork"=> $holidaywork,
						"adjAdd"=> $adjAdd,
						"adjMinus"=> $adjMinus,
						// "logs_issue"=> $late+$ut+$absent,
						"logs_issue"=> $totalUTAbsent ,
						"emp_type"=> $emp_type,
						"coverage"=> $coverage,
						"site"=> $site,
						"emp_promoteid"=> $pos_details[0]->emp_promoteID,
						"hour_nd"=>	$hour_nd,
						"hour_ho"=>	$hour_ho,
						"hour_hp"=>	$hour_hp,
						"account"=>	$account,
						"ahr_val"=>	floatVal($ahr_val) >0 ? $ahr_val : 0,
						"ahr"=>	$ahr_id,
						"leave"=>	$leave_id,
						"loan"=>	$loan,
						"isDaily"=>	(trim($empstat)=="trainee" || trim($empstat)=="trainee-tsn") ? 1 : 0,

 					);
				}
				echo json_encode($arr);
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
			
		
	}
	public function checkIfHasLoan($coverage_id,$emp_id){
 
 		$fields = "deductionname,adj_amount,a.emp_id,coverage_id,a.pemp_adjust_id";
		$where = "a.pemp_adjust_id=b.pemp_adjust_id and b.pdeduct_id=c.pdeduct_id and coverage_id=$coverage_id and a.emp_id =$emp_id";
		return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_deduct_child a, tbl_payroll_emp_adjustment b,tbl_payroll_deductions c");

	}
	private function CheckHDMFdeduct($emp_id){
		$where['emp_id'] = $emp_id;
		return $this->general_model->fetch_specific_val("amount", $where, 'tbl_pagibig_add_contribution');

	}
	public function BIR_Contri($msalary){
			
			$msalary=$msalary*12;
			if($msalary<=250000){
				$BIRTotalCompute2=0;
				}
			else if($msalary<=400000){
				$AnnualSalaryBase = 250000;
				$Add = 0;
				$percentz = 0.20;
				$rsBIR = (($msalary - $AnnualSalaryBase)*$percentz)+$Add;
				$BIRTotalCompute2 = $rsBIR/ 24;
				
				}
			else if($msalary<=800000){
				$AnnualSalaryBase = 400000;
				$Add = 30000;
				$percentz = 0.25;

				$rsBIR = (($msalary - $AnnualSalaryBase)*$percentz)+$Add;
				$BIRTotalCompute2 = $rsBIR/ 24;

				}
			else if($msalary<=2000000){
				$AnnualSalaryBase = 800000;
				$Add = 130000;
				$percentz = 0.30;

				$rsBIR = (($msalary - $AnnualSalaryBase)*$percentz)+$Add;
				$BIRTotalCompute2 = $rsBIR/ 24;

				}
			else if($msalary<=8000000){
				$AnnualSalaryBase = 2000000;
				$Add = 490000;
				$percentz = 0.32;

				$rsBIR = (($msalary - $AnnualSalaryBase)*$percentz)+$Add;
				$BIRTotalCompute2 = $rsBIR/ 24;

				}
			else if($msalary>8000000){
				$AnnualSalaryBase = 2000000;
				$Add = 2410000;
				$percentz = 0.35;

				$rsBIR = (($msalary - $AnnualSalaryBase)*$percentz)+$Add;
				$BIRTotalCompute2 = $rsBIR/ 24;

				}
			else{
				$BIRTotalCompute2=0;

			}
				return $BIRTotalCompute2;					
					
		}
		public function PHIC_Contri($msalary,$divide,$coverage){
			
			
			//Year 2018 - changed codes due to new policy of PHIC. Changed on Jan 16, 2020
			    $philhealth_percentage_2019 = 0.0275;
				$philhealth_percentage_2020 = 0.030;
				// $philhealth_percentage_2021 = 0.035; // For the meantime.. As requested by Ms. Raquel, 1-17-2021
				$philhealth_percentage_2021 = 0.030;
				$philhealth_percentage_2022 = 0.040;
				$philhealth_percentage_2023 = 0.045;
				$philhealth_percentage_2024_2025 = 0.050;
				$PhilHealthCompute =  0;
			/* if($msalary<=10000){
			  $PhilHealthCompute =  (137.50/$divide);
			 }else if($msalary>=10000.01 && $msalary<=39999.99){
			  $PhilHealthCompute =  (($msalary*0.0275)/($divide*2));
			 }else if($msalary>=40000){
			  $PhilHealthCompute =  (550/$divide);
			 }else{
			  $PhilHealthCompute =  0;
			 } */
			if (date("Y",strtotime($coverage)) == 2020) {
                $philhealthToUse = $philhealth_percentage_2020;
				if($msalary>=60000){
					$PhilHealthCompute = 900;
				}else{
					$PhilHealthCompute = ($msalary *  $philhealthToUse)/$divide;
				}
            } else if (date("Y",strtotime($coverage)) == 2021) {
                $philhealthToUse = $philhealth_percentage_2021;
				// if($msalary>=70000){
					// $PhilHealthCompute = 1225;
				if($msalary>=60000){
 					$PhilHealthCompute = 450;
				}else{
					$PhilHealthCompute = ($msalary *  $philhealthToUse)/$divide;
				}
            } else if (date("Y",strtotime($coverage)) == 2022) {
                $philhealthToUse = $philhealth_percentage_2022;
				if($msalary>=80000){
					$PhilHealthCompute = 1600;
				}else{
					$PhilHealthCompute = ($msalary *  $philhealthToUse)/$divide;
				}
            } else if (date("Y",strtotime($coverage)) == 2023) {
                $philhealthToUse = $philhealth_percentage_2023;
				if($msalary>=90000){
					$PhilHealthCompute = 2025;
				}else{
					$PhilHealthCompute = ($msalary *  $philhealthToUse)/$divide;
				}
            } else {
                $philhealthToUse = $philhealth_percentage_2024_2025;
				if($msalary>=100000){
					$PhilHealthCompute = 2500;
				}else{
					$PhilHealthCompute = ($msalary *  $philhealthToUse)/$divide;
				}
            }
			 
			 // $PhilHealthCompute = ($msalary *  $philhealthToUse)/$divide;
			 return $PhilHealthCompute;
									
		}
	/* 
	public function getCurrentPositionRates($empz){
 		$fields = "emp_promoteID,a.posempstat_id,rate,pos_name,pos_details";
        $where = "a.emp_id= $empz and a.isActive=1 and b.pos_id=d.pos_id and a.posempstat_id=b.posempstat_id and c.posempstat_id=b.posempstat_id and c.isActive=1";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_rate c,tbl_position d");

	} */
 
	public function getCurrentPositionRates($empz){
 		$fields = "emp_promoteID,a.posempstat_id,rate,pos_name,pos_details,dep_name,g.status";
        $where = "a.emp_id= $empz and a.isActive=1 and b.pos_id=d.pos_id and a.posempstat_id=b.posempstat_id and c.posempstat_id=b.posempstat_id and d.pos_id = e.pos_id and e.dep_id=f.dep_id and c.isActive=1 and g.empstat_id=b.empstat_id";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_rate c,tbl_position d,tbl_pos_dep e,tbl_department f,tbl_emp_stat g");

	}
	public function getAdjMinus($emp_id,$coverage_id){
 		$fields = "deductionname,value";
        $where = "a.pdeduct_id=b.pdeduct_id and emp_id=$emp_id and coverage_id=$coverage_id and b.isActive=1 and value>0";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_emp_deduction a,tbl_payroll_deductions b");

	}
	public function getAdjAdd($emp_id,$coverage_id){
 		$fields = "additionname,value,description";
        $where = "a.paddition_id=b.paddition_id and emp_id=$emp_id and coverage_id=$coverage_id and b.isActive=1 and value>0";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_emp_addition a,tbl_payroll_addition b");

	}
	
	public function getBonuses($posempstat_id){
 		$fields = "pbonus_id,bonus_name,amount";
        $where = "a.bonus_id=b.bonus_id and posempstat_id=$posempstat_id and a.isActive=1 and amount>0";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_pos_bonus a,tbl_bonus b");

	}
	public function getAllowances($posempstat_id){
 		$fields = "allowance_id,allowance_name,value";
        $where = "posempstat_id=$posempstat_id and isActive=1 and value>0";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_allowance");

	}
	
 
	public function check_ahr($empz=null,$date=null){
 		$fields = "additionalHourRequestId,a.additionalHourTypeId,b.description,otStartDate,otStartTime,otEndDate,otEndTime,totalOtHours,reason,approvalStatus_ID,c.sched_date";
        // $where = "a.additionalHourTypeId=b.additionalHourTypeId and userId=$empz  and a.approvalStatus_ID=5 and otStartDate between '".date_format(date_create($date[0]),"Y-m-d")."' and '".date_format(date_create($date[1]),"Y-m-d")."'";
        $where = "a.additionalHourTypeId=b.additionalHourTypeId and userId=$empz  and a.schedId=c.sched_id and a.approvalStatus_ID=5 and schedId = '".$date."'";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_additional_hour_request a,tbl_additional_hour_type b,tbl_schedule c");

	}
	public function attendance_checker($empz=null,$coverage=null,$coverage_text=null,$empstat_payroll=null,$class_type=null,$return=null){
		$emp = $this->input->post("emp");
		$coverage=$this->input->post("coverage");
		$coverage_text=$this->input->post("coverage_text");
		$site=$this->input->post("site");
		$empstat= ($empstat_payroll==null) ? $this->input->post("empstat") : $empstat_payroll;
		$class= ($class_type==null) ? $this->input->post("class_type") : $class_type;
		$ifCCaTrainee = trim($class)."-".trim($empstat);
		$loc = (strtolower($site)=="cdo") ? 1 : 2;

		$fields = "b.apid,a.fname,a.lname,b.emp_id,c.acc_id,acc_name,uid,acc_description,allowHazard,allowHoliday,allowNightDifferential,allowAdditionalHour,b.sss,b.philhealth,b.bir,b.pagibig,b.salarymode";
        $where = "a.apid=b.apid and b.isActive='yes' and b.acc_id=c.acc_id and b.emp_id=d.emp_id and b.emp_id in (".implode(",",$emp).")";
        $employee = $this->general_model->fetch_specific_vals($fields, $where, "tbl_applicant a,tbl_employee b,tbl_account c,tbl_user d", "lname ASC");
		$date = explode(" - ",$coverage_text);
		$range = $this->DateRangeShift($date);
		$prevApr8 = array();
		$prevApr8["08"] = 0;
		
		$data["isHOt"] = array();
		foreach($employee as $row){
			$ifApr8IsHo = 0;
			$ifApr9HasHo = 0;
			$data[$row->lname."-".$row->emp_id]=array();
			$datas[$row->lname."-".$row->emp_id]["date"] = array("0000-00-00");
			$shift = $this->empshift($row->emp_id,date_format(date_create($date[0]),"Y-m-d"),date_format(date_create($date[1]),"Y-m-d"));
			
				 foreach($shift as $rowshift){
					$date_prevv = explode("-",$rowshift->sched_date);
					$data["isHOt"][$row->lname."-".$row->emp_id][$date_prevv[2]]=array();
				 }
				 foreach($shift as $rowshift){
					 $date_prevv = explode("-",$rowshift->sched_date);
					
					 $timeschedule = $this->timeschedule($rowshift->sched_id);

					$in = null; 
					$actualIn = null; 
					$out = null; 
					$actualOut = null; 
					$shiftIN = null;
					$shiftOUT = null; 
					$ampm = null; 
					$loginNew = null; 
					$logoutNew = null; 
					$late = null; 
					$total_ut = null; 
					$total_late = null; 
					$total_shift = null; 
					$time_start = $rowshift->type; 
					$time_end = ""; 
					
					$NDbegin = "10:00:00 PM";
					$NDend   = "6:00:00 AM";
					
					$HPbegin = "12:00:00 AM";
					$HPend   = "3:00:00 AM";
					
					$HPbegin2 = "2:00:00 PM";
					$HPend2   = "11:59:59 PM";
					
					$HPbegin3 = "4:00:00 AM";
					$HPend3   = "5:00:00 AM";
					$ndRemark = "N";
					$hpRate = '';	
					$NDStart = '';	
					$NDend = '';	
					$newLogin = '';	
					$newLogout = '';	
					$finalND2 = 0;	
					$hpRemark = 'N';	
					$ndstartBetween = 0;	
					$ndendBetween = 0;	
					$breaks = 0;	
					$totalBreak = 0;	
					$breakDeduct = 0;	
					$total_shift_work_cover = 0;	
					$ndHo = 0;
					$ndHOFirstDay = 0;
					$ndHOSecondDay = 0;
					$totalHO = 0;
					$loginnDTRID = 0;
					$logouttDTRID = 0;
					$ifTITO = "";
					$absent = 0;
					$absentTXT = "waz";
					$NDEnd = "00:00:00";
					$break_total = 1;
					$isSameDay = 0;
					$holidaylogin = 0;
					$ahr_break_identify_in= 0;
					$ahr_break_identify_out = 0;
					$deductBreakIfNotWithinToday = 0;
					$ot_arr = array();
					$totalHOTXT="";
					$day1 = "0"; // Flexible - Determine if today of tomorrow
					$day2 = "0";// Flexible - Determine if today of tomorrow
					
						if($rowshift->type=="Normal" or $rowshift->type=="WORD" or $rowshift->type=="Double" or $rowshift->type=="Flexible"){ 
							$logs = $this->empshiftin($row->emp_id,$rowshift->sched_id);
								$shiftBreak=  ($rowshift->acc_time_id!=null ) ? $this->shiftBrk($rowshift->acc_time_id) : 0;
									$totalBreak = ($shiftBreak!=0) ? ($shiftBreak[0]->hr + $shiftBreak[0]->min)/60 : 0;
								if($rowshift->type!="Flexible" && count($timeschedule)){
									$shiftIN =  ($timeschedule[0]->time_start=="12:00:00 AM") ? "24:00": date_format(date_create($timeschedule[0]->time_start), 'H:i');								
									$shiftOUT = date_format(date_create($timeschedule[0]->time_end), 'H:i');
									$ampm = explode("|",$this->getTimeGreeting($shiftIN,$shiftOUT));
									$loginNew = $rowshift->sched_date." ".$shiftIN;
									$dateOut = ($ampm[1] == "Tomorrow")? date('Y-m-d', strtotime("+1 day", strtotime($rowshift->sched_date))) : $rowshift->sched_date;
									$logoutNew = $dateOut . " " . $shiftOUT;
									$day1=$ampm[0];
									$day2=$ampm[1];
								}
								
								
								// $emplogsShift = $this->emplogsShift($rowshift->sched_id,$row->emp_id);
								$NDStart_shift = $rowshift->sched_date." 14:00:00";
								/* if($day1!="0"){
									if($day1==$day2){
										$NDEnd_2 = date('Y-m-d', strtotime("+1 day", strtotime($rowshift->sched_date)))." 14:59:59";
									}else{
										$NDEnd_2 = date('Y-m-d', strtotime("+1 day", strtotime($rowshift->sched_date)))." 14:59:59";
									}									
								}else{
									$NDEnd_2 = date('Y-m-d', strtotime("+1 day", strtotime($rowshift->sched_date)))." 14:59:59";
								} */
								$NDEnd_2 = date('Y-m-d', strtotime("+1 day", strtotime($rowshift->sched_date)))." 14:59:59";
								if(count($logs)){
								$time_start=($rowshift->type=="Flexible") ? "" : $timeschedule[0]->time_start." - ";
								$time_end=	($rowshift->type=="Flexible") ? "Flexible" :	$timeschedule[0]->time_end;
								$NDStart = $rowshift->sched_date." 22:00:00";
								if($day1!="0"){
									if($day1=="Today" && $day2=="Today"){
										$NDEnd = date('Y-m-d',strtotime($rowshift->sched_date))." 24:00:00";
									}else{
										$NDEnd = date('Y-m-d', strtotime("+1 day", strtotime($rowshift->sched_date)))." 6:00:00";
									}									
								}else{
									$NDEnd = date('Y-m-d', strtotime("+1 day", strtotime($rowshift->sched_date)))." 6:00:00";
								}
								
								
									 foreach($logs as $logshift){
										if($logshift->entry=='I') {
											$ifTITO = trim($logshift->info);
											// $in =  ($ifTITO=="TITO") ? $rowshift->sched_date." ".date_format(date_create($logshift->log), 'H:i') : date_format(date_create($logshift->log), 'Y-m-d H:i');
											$in =  ($ifTITO=="TITO") ? date_format(date_create($logshift->log), 'Y-m-d H:i') : date_format(date_create($logshift->log), 'Y-m-d H:i');
											$loginnDTRID = $logshift->dtr_id;
											$actualIn = date_format(date_create($logshift->log), 'Y-m-d H:i');
											if($rowshift->type=="Flexible"){
												$shiftIN = date_format(date_create($in), 'H:i');
												$loginNew = $rowshift->sched_date." ".$shiftIN;
											}
											
										}
										if($logshift->entry=='O'){
											// $out = date_format(date_create($logshift->log), 'Y-m-d H:i');
											$out1 = strtotime($logshift->log);
											$out2 = strtotime($logoutNew);
											$out3 = ($out1>$out2) ? $logshift->log : $logoutNew ;
											
											// $out =  ($ifTITO=="TITO") ? $dateOut." ".date_format(date_create($logshift->log), 'H:i') : date_format(date_create($logshift->log), 'Y-m-d H:i');
											$out =  ($ifTITO=="TITO") ? date_format(date_create($out3), 'Y-m-d H:i') : date_format(date_create($logshift->log), 'Y-m-d H:i');

											$actualOut = date_format(date_create($logshift->log), 'Y-m-d H:i');
											$logouttDTRID = $logshift->dtr_id;
											if($rowshift->type=="Flexible"){
												$shiftOUT = date_format(date_create($out), 'H:i');
												$ampm = explode("|",$this->getTimeGreeting($shiftIN,$shiftOUT));
												$dateOut = ($ampm[1] == "Tomorrow")? date('Y-m-d', strtotime("+1 day", strtotime($rowshift->sched_date))) : $rowshift->sched_date;
												$logoutNew = $dateOut . " " . $shiftOUT;
											}
										}
										
										
												
										if($in!=null and $out!=null){
											
											if($rowshift->type!="Flexible"){
											$late = (strtotime($in) >= strtotime($rowshift->sched_date. " " .$shiftIN)) ? (strtotime($in) - strtotime($rowshift->sched_date. " " .$shiftIN)) : 0;
											$ut = (strtotime($out) <= strtotime($logoutNew)) ? (strtotime($logoutNew) - strtotime($out)) : 0;
											}else{
												$late = 0;
												$ut = 0;
											}
											$total_ut = ($ut<=0) ? 0 : ($ut/60);
											$total_late = ($late<=0) ? 0 : ($late/60);
												$newLogin = ($late<=0) ? $rowshift->sched_date. " " .$shiftIN : $in; // New Login
												$newLogout = ($ut<=0) ? $logoutNew : $out; // New Logout
											$total_shift = (strtotime($logoutNew) - strtotime($loginNew))/3600;
											
											/*****To GET Night Premium****/
												 $ndLogin_determine = (count($timeschedule)>0) ? $timeschedule[0]->time_start : $loginNew;
												
												$NDdate1 = DateTime::createFromFormat('H:i a',  $ndLogin_determine);
												$NDdate2 = DateTime::createFromFormat('H:i a', $NDbegin);
												$NDdate3 = DateTime::createFromFormat('H:i a', $NDend);

												$HPdate1 = DateTime::createFromFormat('H:i a',  $ndLogin_determine);
												$HPdate2 = DateTime::createFromFormat('H:i a', $HPbegin);
												$HPdate3 = DateTime::createFromFormat('H:i a', $HPend);
												
												$HPdate4 = DateTime::createFromFormat('H:i a', $HPbegin2);
												$HPdate5 = DateTime::createFromFormat('H:i a', $HPend2);
												
												$HPdate6 = DateTime::createFromFormat('H:i a', $HPbegin3);
												$HPdate7 = DateTime::createFromFormat('H:i a', $HPend3);
												 $ndRemark = ($NDdate1 >= $NDdate2 or $NDdate1 <= $NDdate3) ? "Y" : "N"; 
											
												if (($HPdate1 >= $HPdate4 and $HPdate1 <= $HPdate5) or ($HPdate1 >= $HPdate6 and $HPdate1 <= $HPdate7) ){
															$hpRemark = "Y";
															$hpRate = 0.08;
												}else if(($HPdate1 >= $HPdate2) and ($HPdate1 <= $HPdate3)){
														   $hpRemark = "Y";
															$hpRate = 0.10;
												}else{
															$hpRemark= "N";
															 $hpRate = 0;	
												} 
											/*****To GET Night Premium****/
										}
										
									 }
										
									 }else{
										$absent = ((strtotime($logoutNew) - strtotime($loginNew))/3600)-$totalBreak;
										$absent *=60; 
										$absent = ($absent<=0) ? 480 : $absent; 
										
									 }
									
											// $login_shift_date = date_format(date_create($newLogin),"Y-m-d");
											$login_shift_date = explode(" ",$newLogin);
											$logout_shift_date = date_format(date_create($newLogout),"Y-m-d");
											$shiftout_date = date_format(date_create($logoutNew),"Y-m-d");

											$breaks = $this->breakIdentify($loginnDTRID,$logouttDTRID,$row->emp_id,"id");
											$break_total = ($breaks[0]->hr+$breaks[0]->min)/60;

											$login_nd = strtotime($newLogin);
											$logout_nd = (strtotime($newLogout)>strtotime($NDEnd)) ? strtotime($NDEnd) : strtotime($newLogout) ;
											$logout_nd2 = (strtotime($newLogout)>strtotime($NDEnd)) ? $NDEnd : $newLogout ;
											$shiftstart_nd = strtotime($NDStart);
											$shiftstart_nd2 = strtotime($NDStart_shift);
											$shiftstart_nd_end = strtotime($rowshift->sched_date." 24:00:00");
											$shiftstart_nd_start = strtotime($shiftout_date." 00:00:00");
											$shiftend_nd = strtotime($NDEnd);
											$shiftend_nd2 = strtotime($NDEnd_2);
											

											$breakDeduct  = ($total_ut>0 or $total_late>0 or $break_total>0.76) ? (($break_total<=0.25) ? $totalBreak : $break_total) : $totalBreak;
											$breakDeduct  = ($breakDeduct>1) ? 1 : $breakDeduct;
											//if(($logout_nd<=$shiftend_nd2)){
											if($login_shift_date[0]===$logout_shift_date){
												$startOfND =strtotime($rowshift->sched_date." 22:00:00");
												$startOfND111 =$out." < ".$rowshift->sched_date." 22:00:00";
												$determineOutLog = ($total_ut>0) ? $newLogout : $logoutNew ;
												$ndendBetween = (strtotime($out)<$startOfND) ? 0 : ((strtotime($determineOutLog)-$startOfND)/3600)-$breakDeduct;
												$deductBreakIfNotWithinToday = 0;
											}else{
												if(($logout_nd>=$shiftstart_nd2) && ($logout_nd<=$shiftend_nd2)){

													$lou = ($shiftend_nd<=$logout_nd) ? $shiftstart_nd_start : $logout_nd;
													// $ndendBetween = $shiftend_nd.'<='.$logout_nd;
													// $ndendBetween = ($total_ut>0 or ($shiftend_nd>strtotime($logoutNew))) ?  ($lou-$shiftstart_nd_end) : ($shiftend_nd-$lou);
													$ndendBetween = ($shiftend_nd>strtotime($logoutNew)) ?  ($lou-$shiftstart_nd_end) : ($shiftend_nd-$lou);
													$ndendBetween = ($ndendBetween<0 or $in==null) ? 0 : $ndendBetween;
													// $ndendBetween = $ndendBetween/3600;
													$ndendBetween = $ndendBetween/3600;
													$deductBreakIfNotWithinToday = 1;
												 }
												 
											}
													
											if($login_shift_date[0]===$logout_shift_date){ //If Morning Shift
												$endOfND =strtotime($rowshift->sched_date." 6:00:00");
												$ndstartBetween = ($login_nd>$endOfND) ? 0 : ($endOfND-$login_nd)/3600;
												$deductBreakIfNotWithinToday = 0;
											}else{
												if(($login_nd>=$shiftstart_nd2) && ($login_nd<=$shiftend_nd)){
													$liu = ($shiftstart_nd>=strtotime($newLogin)) ? $shiftstart_nd : $login_nd;
													$ndstartBetween = ($shiftstart_nd_end-$liu)/3600;
													$deductBreakIfNotWithinToday = 1;
												}
												
											}
											
											
											$hologin =  ($ifTITO=="TITO") ? $in : $newLogin;
											$dayIn = date_format(date_create($hologin),"j");
											$moIn = date_format(date_create($hologin),"m");
											$holidaylogin = $this->isHoliday($moIn,$dayIn,$loc);
										$hologout =  ($ifTITO=="TITO") ? $out : $newLogout;

										$dayOut = date_format(date_create($hologout),"j");
										$moOut = date_format(date_create($hologout),"m");
										$holidaylogout = $this->isHoliday($moOut,$dayOut,$loc);
											$login_end_of_the_day = strtotime($login_shift_date[0]." 24:00:00");
											$logout_start_of_the_day = strtotime($logout_shift_date." 00:00:00");
											
													$breaks_between_log1 = $this->breakIdentify($newLogin,$logout_shift_date." 00:00:00",$row->emp_id,"log");
													$total_break_time1 = ($breaks_between_log1[0]->hr+$breaks_between_log1[0]->min)/60;
													
													$logoutPlus1Hour=date('Y-m-d H:i',strtotime('+1 hour',strtotime($newLogout)));
													$breaks_between_log2 = $this->breakIdentify($logout_shift_date." 00:00:00",$logoutPlus1Hour,$row->emp_id,"log");
													$total_break_time2 = (($breaks_between_log2[0]->hr*60)+$breaks_between_log2[0]->min)/60;
													$total_break_time2txt = "(".($breaks_between_log2[0]->hr*60)."+".$breaks_between_log2[0]->min.")/60";
													
													$ahr_break_identify_in = $total_break_time1;
													$ahr_break_identify_out = $total_break_time2;//xxxx
										
										$hoEndTXT="";
										$hoStartTXT="";
										if(strtolower($ifCCaTrainee)!="cca-trainee"){	
											if(count($logs)){	
												if($holidaylogin[0]->totalHo or $holidaylogout[0]->totalHo){
													
													if($login_shift_date[0]==$logout_shift_date){ //If Morning Shift
													 
													$schedDate6AM = strtotime($rowshift->sched_date." 6:00:00");
													$checkifQualifyND= ($schedDate6AM>strtotime($newLogin)) ? $schedDate6AM-strtotime($newLogin):0;	
														$isSameDay = "Y";
														$totalHO =((strtotime($newLogout)-strtotime($newLogin))/3600)-$breakDeduct;
														$totalHO *=$holidaylogin[0]->totalHo;
														
														$ndHo = ($checkifQualifyND/3600)-$breakDeduct;
														$ndHo *= $holidaylogin[0]->totalHo;
													}else{
														$isSameDay = "N";
														// $breaks_between_log = $this->breakIdentify($loginnDTRID,$logouttDTRID,$row->emp_id,"log");

														if($holidaylogin[0]->totalHo!=null && $newLogin!=""){
															
															$hoStart = (($login_end_of_the_day-$login_nd)/3600)-$total_break_time1;
															$hoStartTXT = "((".$login_end_of_the_day."-".$login_nd.")/3600)-".$total_break_time1;
															$hoStart = ($hoStart<=0) ? 0 : $hoStart;
															$ndHOFirstDay = ($ndstartBetween * $holidaylogin[0]->totalHo)-$total_break_time1 ;
														}else{
															$total_break_time1 = 0;
															$hoStart = 0;
															$ndHOFirstDay = 0;
														}
														if($holidaylogout[0]->totalHo!=null && $newLogout!=""){
															// $hoEnd = (($logout_nd-$logout_start_of_the_day)/3600)-$total_break_time2;
															$hoEnd = ((strtotime($logoutNew)-$login_end_of_the_day)/3600)-$total_break_time2;
															$hoEnd = ($hoEnd<=0) ? 0 : $hoEnd;
															// $hoEndTXT = "((".$login_shift_date[0]." 24:00:00-".$logout_shift_date." 00:00:00)/3600)-".$total_break_time2;
															$hoEndTXT = "((".strtotime($logoutNew)." - ".$login_end_of_the_day." )/3600)-".$total_break_time2;
															$ndHOSecondDay = ($ndendBetween * $holidaylogout[0]->totalHo)-$total_break_time2;
														}else{
															$total_break_time2 = 0;
															$hoEnd = 0;
															$ndHOSecondDay = 0;
														}
														$totalHO = ($hoStart * $holidaylogin[0]->totalHo) + ($hoEnd*$holidaylogout[0]->totalHo);
														// $totalHOTXT = $hoStart." ".$hoEnd." ".$hoStartTXT;
														$totalHOTXT =  $hoStart." * ".$holidaylogin[0]->totalHo .") + (".$hoEnd." * ".$holidaylogout[0]->totalHo." hoEndTXT=".$hoEndTXT." total_break_time2txt=".$total_break_time2txt ;
														$ndHo = $ndHOFirstDay + $ndHOSecondDay;
													}
												
												$ndHo = ($ndHo<0) ? 0 : (($row->allowHoliday==1) ? $ndHo : 0);
												}else{
													$totalHO=0;
													$isSameDay = "N";
												}
											}
										}
										$breek =  ($break_total<=0 && $total_ut>0) ? $totalBreak * 60: 0;

										$total_shift_work_cover = number_format(($total_shift-$breakDeduct)-(($total_late+$total_ut+$absent-$breek)/60),2);
										if($rowshift->type=="Flexible"){
											$finalND2 = ($ndstartBetween-.50 + $ndendBetween-.50 + $ndHo)-$breakDeduct; // Final ND Computation
										}else{
											// $finalND2 = ($ndstartBetween + $ndendBetween + $ndHo)-$breakDeduct; // Final ND Computation
											$finalND2 = ($ndstartBetween + $ndendBetween + $ndHo); // Final ND Computation
											$finalND2 = ($deductBreakIfNotWithinToday==0) ? $finalND2 : $finalND2-$breakDeduct; // Final ND Computation
										}
										
										
										$ahr = $this->check_ahr($row->uid,$rowshift->sched_id);
										$OTin =0;
										$OTout =0;
										$OT_in  =0;
										$OT_out =0;
										$id ="";
										$OTinTXT = "";
										$OToutTXT = "";
										// $data["ahr"][$row->emp_id] = $ahr;
 										if(count($ahr)>0){
											   foreach($ahr as $rowahr){
												   	$OT = 0;
												   	$ahr_time = 0;
													$id =$rowahr->additionalHourRequestId;
												  
													##### AHR Computation
													$ahr_dayIn = date_format(date_create($rowahr->otStartDate),"j");
													$ahr_moIn = date_format(date_create($rowahr->otStartDate),"m");
													$ahr_holidaylogin = $this->isHoliday($ahr_moIn,$ahr_dayIn,$loc);

													$ahr_dayOut = date_format(date_create($rowahr->otEndDate),"j");
													$ahr_mOut = date_format(date_create($rowahr->otEndDate),"m");
													$ahr_holidaylogOut = $this->isHoliday($ahr_mOut,$ahr_dayOut,$loc);
													$in_ho = (!empty($ahr_holidaylogin[0]->totalHo)) ? $ahr_holidaylogin[0]->totalHo : 1;
													$out_ho = (!empty($ahr_holidaylogOut[0]->totalHo)) ? $ahr_holidaylogOut[0]->totalHo : 1;
													
													
													$otStartTime = ($rowahr->otStartTime=="00:00") ? "24:00" : $rowahr->otStartTime;
													$otEndTime = ($rowahr->otEndTime=="00:00") ? "24:00" : $rowahr->otEndTime;
													if($ahr_dayIn==$ahr_dayOut && $otStartTime!="24:00"){ // If Morning
														if($rowahr->description=="WORD"){
															$ot_percent = ($row->acc_description=="Admin") ? .3 :.5;
															$breakk=$breakDeduct;
															$otStartDate = $rowahr->sched_date;
														}else{
															$ot_percent = .25;
															$breakk =0;
															$otStartDate = $rowahr->otStartDate;
														}
															$ahr_in = strtotime($otStartDate." ".$otStartTime);
															$ahr_out = strtotime($rowahr->otEndDate." ".$otEndTime);

															$OT = (($ahr_out-$ahr_in)/3600)-$breakk;
															$ahr_time = number_format($OT,2);

															$OTinTXT = "Nicca".$OT."*".$in_ho ."+ ".$OT."*".$ot_percent;
															// $OT = $OT*$in_ho + $OT*$ot_percent ; // naa HO #note: HO is not included since it is already computed in the payroll
															$OT =  $OT*$ot_percent +  $OT;
															
															$OT = number_format($OT,2);
															
													}else{
															
															$ahr_in = strtotime($rowahr->otStartDate." ".$otStartTime);
															$ahr_in_to_12mn = strtotime($rowahr->otStartDate." 24:00:00");
																$ahr_out = strtotime($rowahr->otEndDate." ".$otEndTime);
																$ahr_12mn_to_out = strtotime( date('Y-m-d',strtotime($rowahr->otEndDate))." 00:00:00");
																
															$totalBreakCheck = $ahr_break_identify_in+$ahr_break_identify_out;
															$totalBreakCheckTxT = "CHCHA($ahr_break_identify_in"."+"."$ahr_break_identify_out)";
															if($totalBreakCheck<=0){
																$OTinCheckifWhichIsGreater =(($ahr_in_to_12mn-$ahr_in)/3600);
																$OToutCheckifWhichIsGreater = (($ahr_out-$ahr_12mn_to_out)/3600);
																if($OTinCheckifWhichIsGreater>=$OToutCheckifWhichIsGreater){
																	$ahr_break_identify_in = $breakDeduct;
																}else{
																	$ahr_break_identify_out = $breakDeduct;
																}
																
															}	
																
															if($rowahr->description=="WORD"){
																$ot_percent = ($row->acc_description=="Admin") ? .3 :.5;
																$ahr_break_identify_in = 0;
																if($ahr_break_identify_out==0){ //wala before: updated on 12/19/2019 4:49Am
																	$ahr_break_identify_out = 1;
																}
															}else{
																$ot_percent = .25;
																$ahr_break_identify_in = 0;
																$ahr_break_identify_out = 0;
															}	
															 $OTin =(($ahr_in_to_12mn-$ahr_in)/3600)-$ahr_break_identify_in;
															$OTin = ($OTin<=0) ? 0 : $OTin;
															$OTinTXT = "MJM((".$ahr_in_to_12mn."-".$ahr_in.")/3600)-".$ahr_break_identify_in;
															$OTout = (($ahr_out-$ahr_12mn_to_out)/3600)-$ahr_break_identify_out; //change to 1 
															$OTout = ($OTout<=0) ? 0 : $OTout;
															$OToutTXT = "((".$ahr_out."-".$ahr_12mn_to_out.")/3600)-".$ahr_break_identify_out;
																// $OT_in = (($OTin*$in_ho)+($OTin*$ot_percent)); // naa HO #note: HO is not included since it is already computed in the payroll
																$OT_in = ($OTin*$ot_percent) +$OTin;
																$OT_out = ($OTout*$ot_percent) +$OTout;
																// $OT_out = (($OTout*$out_ho)+($OTout*$ot_percent)); // naa HO #note: HO is not included since it is already computed in the payroll
																$OT = $OT_in + $OT_out;
																$OT1 = $OTin + $OTout;
															$ahr_time = number_format($OT1,2);
															
															$OT = number_format($OT,2);
													
													}
												 
													##### AHR Computation
													$ot_arr[] = array(2222=> $OTin."+".$OTout,1111 => $OTinTXT." <-----> ".$OToutTXT,"id" => $id,"ahr_time" => $ahr_time, "ahr" => $OT, "OTin" => $OTin, "OTout" => $OTout,"OT_in" => $OT_in,"OT_out" => $OT_out,"HO_1" => $ahr_holidaylogin[0]->totalHo,"HO_2" => $ahr_holidaylogOut[0]->totalHo,"description" => $rowahr->description,"ot_percent" => $ot_percent,"AHR_DETAILS" => $ahr);
												}
											 }
						}
						
						 
							
						 
						 
						
						$totalUTAbsent = ($rowshift->type=="Flexible") ? 0 : ($total_ut+$total_late+$absent);
						
						if ($break_total<=0 && $total_ut>0){
							$totalUTAbsent -=$totalBreak*60; 						
						}
						// if($totalUTAbsent>0){
							$sched_type = $rowshift->type;

							$schedtype_idnum = ($sched_type == 'Leave-WoP') ? 1 : 0;
							$leave_check = $this->leaveCheck($rowshift->sched_id, $schedtype_idnum);
							
							$leaveDeduct  = (count($leave_check) > 0) ? $leave_check[0]->minutes : 0;
							$total_shift_work_cover += $leaveDeduct/60 ;
							
						// }else{
							// $sched_type = $rowshift->type;
							// $leaveDeduct  = 0;
						// }
						if(trim($sched_type)=="Leave-WoP"){
							$leave_check_date = $this->leaveWoPCheckDate($row->uid,$rowshift->sched_date,0);
							$totalUTAbsent += (count($leave_check_date) > 0) ? $leave_check_date[0]->minutes : 0;
							// $totalUTAbsent += 1;
						}
						
						if($rowshift->type=="Holiday Off-WoP"  or $rowshift->type=="Suspension" or $rowshift->type=="VTO-WoP"){
							$totalUTAbsent +=480;
						}
						if(trim($rowshift->type)=="Absent"){
							if(trim($empstat)=="trainee" || trim($empstat)=="trainee-tsn"){
								$totalUTAbsent +=0;
							}
						}
							
							
 							 if(trim($rowshift->sched_date)==="2020-04-08"){
								if($totalHO>0 ){
									
									$ifApr8IsHo = 1;
									$data["isHOt"][$row->lname."-".$row->emp_id]["08"]=$ifApr8IsHo;
									$data["isHO"][$row->lname."-".$row->emp_id][$date_prevv[2]] = array(
										"isHO" => "yes",
										"ifApr8IsHo"=>$ifApr8IsHo,
										
									);
								}else{
									$ifApr8IsHo = 0;
								}
							
							}
							/* $data["isHOs"][$row->lname."-".$row->emp_id][$rowshift->sched_date] = array(
							"ifApr8IsHo"=>$ifApr8IsHo,
 							"aaa"=>$data["isHOt"][$row->lname."-".$row->emp_id]["08"]
							); */
							$schedTest = array("Rest Day", "Holiday Off-WP", "Holiday Off-WoP");
							if($rowshift->sched_date==="2020-04-09"){
								if( in_array($sched_type,$schedTest) && $ifApr8IsHo===0){
									$data["isHO"][$row->lname."-".$row->emp_id][$date_prevv[2]] = array(
										"isHO" => "yes",
										"ifApr8IsHo"=>$ifApr8IsHo,
 									
									);
									if($row->emp_id==44){
										$ifApr9HasHo = 0;
									}else{
										$ifApr9HasHo = ($empstat=="trainee") ? 16 : 8;
									}
									
								}else{
									$ifApr9HasHo = 0;
								}
							}else{
								$ifApr9HasHo = 0;
							}
							
							
							
							//$holidayWorkTotal = ($row->allowHoliday==1) ? (($totalHO>0) ? $totalHO: 0 ) : 0;

							if($row->allowHoliday==1){
								$totalHO+=$ifApr9HasHo;
								$holidayWorkTotal =(($totalHO>0) ? $totalHO: 0 );
							}else{
								if($ifApr9HasHo==0){
									$holidayWorkTotal = 0;
								}else{
									$holidayWorkTotal=$ifApr9HasHo;
								}
								
							}
							$data["emp"][$row->lname."-".$row->emp_id][strtotime($rowshift->sched_date)+$rowshift->sched_id] = array(
								"coverage_start" => $date[0],
								"0_ifApr8IsHo" => $ifApr8IsHo,
								"1_ifApr9HasHo" => $ifApr9HasHo,
								"1111" => $absentTXT,
								"empstat" => $empstat,
								"totalHOTXT" => $totalHOTXT,
								"sched_date" => $rowshift->sched_date,
								"sched_id" =>$rowshift->sched_id,
								"sched_type" =>$rowshift->type,
								"fname" => $row->fname,
								"account" => $row->acc_id."|".$row->acc_name,
								"lname" => $row->lname,
								// "total_break_time2" => $total_break_time2,
								"emp_id" => $row->emp_id,
								"uid" => $row->uid,
								"Ot_arr" => ($row->allowAdditionalHour==1) ? $ot_arr : array(),
								"range" => $range,
								"leaveDeduct" => $leaveDeduct,
								"1" => $ahr_break_identify_in,
								"2" => $ahr_break_identify_out,
								// "login_shift_date" => $login_shift_date[0],
								// "logout_shift_date" => $logout_shift_date,
								"type" => $sched_type,
								"in" => ($in!=null) ? date_format(date_create($in),"Y-m-d h:i a")  : "<span style='color: #F44336;'>No Logs</span>",
								"out" => ($out!=null) ? date_format(date_create($out),"Y-m-d h:i a")  : "<span style='color: #F44336;'>No Logs</span>",
								"shift_time-start" => $shiftIN,
								"shift_time-out" => $shiftOUT,
								"am-pm" => $ampm,
								 "totalUTAbsent" => (($totalUTAbsent-$leaveDeduct)<0) ? 0 : $totalUTAbsent-$leaveDeduct,
								 //"totalUTAbsent" => $totalUTAbsent-$leaveDeduct,
								"shift_out" => $logoutNew,
								"shift_in" => $loginNew,
								"issue_ut" =>	$total_ut,
								"issue_late" => $total_late,
								"issue_absent" => $absent,
								"breakDeduct" => $breakDeduct,
								"total_shift" => $total_shift-$breakDeduct,
								"total_shift_work" => ($total_shift_work_cover >0) ? $total_shift_work_cover: 0,
								"total_shift_work1" => $total_shift_work_cover,
								"totalShiftBreak" => $totalBreak,
								"time_end" => $time_end,
								"time_start" => $time_start,
								// "ND" => ($finalND2>0) ? number_format($finalND2,2) : 0,
								"ND" => ($row->allowNightDifferential==1) ? ( ($finalND2>0) ? number_format($finalND2,2) : 0 ): 0,
								"ND_" => $deductBreakIfNotWithinToday,
								"HP" => ($row->allowHazard==1) ? ( ($finalND2>0) ? number_format($finalND2,2) : 0 ): 0,
								"hpRemark" => $hpRemark,
								"hpRate" => $hpRate,
								"NDStart" => $NDStart,
								"NDEnd" => $NDEnd,				
								"sss_num" => $row->sss,
								"philhealth_num" => $row->philhealth,
								"bir_num" => $row->bir,
								"pagibig_num" => $row->pagibig,
								"salarymode" => $row->salarymode,
								"newLogin" => $newLogin,
								"newLogout" => $newLogout,
								"ndstartBetween" => $ndstartBetween,
								"ndendBetween" => $ndendBetween,
								"consumed_breaks" => $break_total,
								"isSameDay" => $isSameDay,
								"holiday" => ($holidaylogin!=0) ? $holidaylogin[0]->totalHo+$holidaylogout[0]->totalHo : 0,
								"holidaywork" => $holidayWorkTotal,
								"ndHo" => ($ndHo>0) ? $ndHo : 0,
								"ActualOut" =>  ($actualOut!=null) ? date_format(date_create($actualOut),"Y-m-d h:i a")  : "<span style='color: #F44336;'>No Logs</span>",
								"ActualIn" =>  ($actualIn!=null) ? date_format(date_create($actualIn),"Y-m-d h:i a")  : "<span style='color: #F44336;'>No Logs</span>",

							);
							$holidayWorkTotal = 0;
					$datas[$row->lname."-".$row->emp_id]["date"][] = $rowshift->sched_date;
				}
				if(isset($sched_type)){
					$data["emp"][$row->lname."-".$row->emp_id]["absent_system"]["dates"] = ($sched_type!="Flexible") ? array_diff($range,$datas[$row->lname."-".$row->emp_id]["date"]) : 0;
				}else{
					$data["emp"][$row->lname."-".$row->emp_id]["absent_system"]["dates"] = array_diff($range,$datas[$row->lname."-".$row->emp_id]["date"]);
				}
				
				$data["emp"][$row->lname."-".$row->emp_id]["absent_system"]["lname"] = $row->lname;
				$data["emp"][$row->lname."-".$row->emp_id]["absent_system"]["fname"] = $row->fname;
				$data["emp"][$row->lname."-".$row->emp_id]["absent_system"]["emp_id"] = $row->emp_id;
				$data["emp"][$row->lname."-".$row->emp_id]["absent_system"]["uid"] = $row->uid;
				$data["emp"][$row->lname."-".$row->emp_id]["absent_system"]["account"] = $row->acc_id."|".$row->acc_name."|".$row->acc_description;
		}
		
		if($return==null){		
			echo json_encode($data);
		}else{
			return $data;
		}
	}
	public function leaveCheck_id($uid,$coverage_text){
		$date = explode(" - ",$coverage_text);
		$query = $this->db->query("SELECT requestLeaveDetails_ID FROM tbl_request_leave_details where date between '".date_format(date_create($date[0]),"Y-m-d")."' and '".date_format(date_create($date[1]),"Y-m-d")."' and uid=$uid and overAllStatus=5 and isActive=1");
		return $query->result();

		
	}
	public function leaveCheck($schedule_id, $schedtype_idnum){
		$append_id = ($schedtype_idnum===1)? "AND isPaid = 1":"";
		$query = $this->db->query("SELECT requestLeaveDetails_ID,minutes FROM tbl_request_leave_details where retract_sched_id=$schedule_id  and overAllStatus=5 and isActive=1 $append_id"); // isPaid dapat wala: updated on 12/19/2019 2:51 AM
		return $query->result();
	}
	public function leaveWoPCheckDate($uid,$schedule_date,$isPaid){
		$query = $this->db->query("SELECT requestLeaveDetails_ID,minutes FROM tbl_request_leave_details where date='$schedule_date' and uid=$uid and isPaid=$isPaid and overAllStatus=5 and isActive=1");
		return $query->result();
	}
	public function DateRangeShift($date){
		$strDateFrom = date("Y-m-d", strtotime($date[0]));
		$strDateTo = date("Y-m-d", strtotime($date[1]));
		$aryRange=array();
		$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
			if($iDateTo>=$iDateFrom){
				array_push($aryRange,date('Y-m-d',$iDateFrom));
				 while($iDateFrom<$iDateTo){
					$iDateFrom+=86400; // add 24 hours
						array_push($aryRange,date('Y-m-d',$iDateFrom));
					}
			}
			 return $aryRange;
			 		
	}
	public function SSS_Contri($search_key){
			# SSS Contribution 2016
			/* $arr = array(
				"1	  - 1249.99" => array("Mothly Salary Credit"=>1000,"ER"=>73.70,"EE"=>36.30,"Total"=>110),
				"1250 - 1749.99" => array("Mothly Salary Credit"=>1500,"ER"=>110.50,"EE"=>54.50,"Total"=>165),
				"1750 - 2249.99" => array("Mothly Salary Credit"=>2000,"ER"=>147.30,"EE"=>72.70,"Total"=>220),
				"2250 - 2749.99" => array("Mothly Salary Credit"=>2500,"ER"=>184.20,"EE"=>90.80,"Total"=>275),
				"2750 - 3249.99" => array("Mothly Salary Credit"=>3000,"ER"=>221,"EE"=>109,"Total"=>330),
				"3250 - 3749.99" => array("Mothly Salary Credit"=>3500,"ER"=>257.80,"EE"=>127.20,"Total"=>385),
				"3750 - 4249.99" => array("Mothly Salary Credit"=>4000,"ER"=>294.70,"EE"=>145.30,"Total"=>440),
				"4250 - 4749.99" => array("Mothly Salary Credit"=>4500,"ER"=>331.50,"EE"=>163.50,"Total"=>495),
				"4750 - 5249.99" => array("Mothly Salary Credit"=>5000,"ER"=>368.30,"EE"=>181.70,"Total"=>550),
				"5250 - 5749.99" => array("Mothly Salary Credit"=>5500,"ER"=>405.20,"EE"=>199.80,"Total"=>605),
				"5750 - 6249.99" => array("Mothly Salary Credit"=>6000,"ER"=>442,"EE"=>218,"Total"=>660),
				"6250 - 6749.99" => array("Mothly Salary Credit"=>6500,"ER"=>478.80,"EE"=>236.20,"Total"=>715),
				"6750 - 7249.99" => array("Mothly Salary Credit"=>7000,"ER"=>515.70,"EE"=>254.30,"Total"=>770),
				"7250 - 7749.99" => array("Mothly Salary Credit"=>7500,"ER"=>552.50,"EE"=>272.50,"Total"=>825),
				"7750 - 8249.99" => array("Mothly Salary Credit"=>8000,"ER"=>589.30,"EE"=>290.70,"Total"=>880),
				"8250 - 8749.99" => array("Mothly Salary Credit"=>8500,"ER"=>626.20,"EE"=>308.80,"Total"=>935),
				"8750 - 9249.99" => array("Mothly Salary Credit"=>9000,"ER"=>663,"EE"=>327,"Total"=>990),
				"9250 - 9749.99" => array("Mothly Salary Credit"=>9500,"ER"=>699.8,"EE"=>345.2,"Total"=>1045),
				"9750 - 10249.99" => array("Mothly Salary Credit"=>10000,"ER"=>736.7,"EE"=>363.3,"Total"=>1100),
				"10250 - 10749.99" => array("Mothly Salary Credit"=>10500,"ER"=>773.5,"EE"=>381.5,"Total"=>1155),
				"10750 - 11249.99" => array("Mothly Salary Credit"=>11000,"ER"=>810.3,"EE"=>399.7,"Total"=>1210),
				"11250 - 11749.99" => array("Mothly Salary Credit"=>11500,"ER"=>847.2,"EE"=>417.8,"Total"=>1265),
				"11750 - 12249.99" => array("Mothly Salary Credit"=>12000,"ER"=>884,"EE"=>436,"Total"=>1320),
				"12250 - 12749.99" => array("Mothly Salary Credit"=>12500,"ER"=>920.8,"EE"=>454.2,"Total"=>1375),
				"12750 - 13249.99" => array("Mothly Salary Credit"=>13000,"ER"=>957.7,"EE"=>472.3,"Total"=>1430),
				"13250 - 13749.99" => array("Mothly Salary Credit"=>13500,"ER"=>994.5,"EE"=>490.5,"Total"=>1485),
				"13750 - 14249.99" => array("Mothly Salary Credit"=>14000,"ER"=>1031.3,"EE"=>508.7,"Total"=>1540),
				"14250 - 14749.99" => array("Mothly Salary Credit"=>14500,"ER"=>1068.2,"EE"=>526.8,"Total"=>1595),
				"14750 - 15249.99" => array("Mothly Salary Credit"=>15000,"ER"=>1105,"EE"=>545,"Total"=>1650),
				"15250 - 15749.99" => array("Mothly Salary Credit"=>15500,"ER"=>1141.8,"EE"=>563.2,"Total"=>1705),
				"15750 - 1000000" => array("Mothly Salary Credit"=>16000,"ER"=>1178.7,"EE"=>581.3,"Total"=>1760),
			); */
			
			#SSS Contribution 2019 
			/* $arr = array(
				"1	  - 2249.99" => array("Mothly Salary Credit"=>2000,"ER"=>160,"EE"=>80,"Total"=>240),
				"2250 - 2749.99" => array("Mothly Salary Credit"=>2500,"ER"=>200,"EE"=>100,"Total"=>300),
				"2250 - 3249.99" => array("Mothly Salary Credit"=>3000,"ER"=>240,"EE"=>120,"Total"=>360),
				"3250 - 3749.99" => array("Mothly Salary Credit"=>3500,"ER"=>280,"EE"=>140,"Total"=>420),
				"3750 - 4249.99" => array("Mothly Salary Credit"=>4000,"ER"=>320,"EE"=>160,"Total"=>480),
				"4250 - 4749.99" => array("Mothly Salary Credit"=>4500,"ER"=>360,"EE"=>180,"Total"=>540),
				"4750 - 5249.99" => array("Mothly Salary Credit"=>5000,"ER"=>400,"EE"=>200,"Total"=>600),
				"5250 - 5249.99" => array("Mothly Salary Credit"=>5500,"ER"=>440,"EE"=>220,"Total"=>660),
				"5750 - 6249.99" => array("Mothly Salary Credit"=>6000,"ER"=>480,"EE"=>240,"Total"=>720),
				"6250 - 6749.99" => array("Mothly Salary Credit"=>6500,"ER"=>520,"EE"=>260,"Total"=>780),
				"6750 - 7249.99" => array("Mothly Salary Credit"=>7000,"ER"=>560,"EE"=>280,"Total"=>840),
				"7250 - 7749.99" => array("Mothly Salary Credit"=>7500,"ER"=>600,"EE"=>300,"Total"=>900),
				"7750 - 8249.99" => array("Mothly Salary Credit"=>8000,"ER"=>640,"EE"=>320,"Total"=>960),
				"8250 - 8749.99" => array("Mothly Salary Credit"=>8500,"ER"=>680,"EE"=>340,"Total"=>1020),
				"8750 - 9249.99" => array("Mothly Salary Credit"=>9000,"ER"=>720,"EE"=>360,"Total"=>1080),
				"9250 - 9749.99" => array("Mothly Salary Credit"=>9500,"ER"=>760,"EE"=>380,"Total"=>1140),
				"9750 - 10249.99" => array("Mothly Salary Credit"=>10000,"ER"=>800,"EE"=>400,"Total"=>1200),
				"10250 - 10749.99" => array("Mothly Salary Credit"=>10500,"ER"=>840,"EE"=>420,"Total"=>1260),
				"10750 - 11249.99" => array("Mothly Salary Credit"=>11000,"ER"=>880,"EE"=>440,"Total"=>1320),
				"11250 - 11749.99" => array("Mothly Salary Credit"=>11500,"ER"=>920,"EE"=>460,"Total"=>1380),
				"11750 - 12249.99" => array("Mothly Salary Credit"=>12000,"ER"=>960,"EE"=>480,"Total"=>1440),
				"12250 - 12749.99" => array("Mothly Salary Credit"=>12500,"ER"=>1000,"EE"=>500,"Total"=>1500),
				"12750 - 13249.99" => array("Mothly Salary Credit"=>13000,"ER"=>1040,"EE"=>520,"Total"=>1560),
				"13250 - 13749.99" => array("Mothly Salary Credit"=>13500,"ER"=>1080,"EE"=>540,"Total"=>1620),
				"13750 - 14249.99" => array("Mothly Salary Credit"=>14000,"ER"=>1120,"EE"=>560,"Total"=>1680),
				"14250 - 14749.99" => array("Mothly Salary Credit"=>14500,"ER"=>1160,"EE"=>580,"Total"=>1740),
				"14750 - 15249.99" => array("Mothly Salary Credit"=>15000,"ER"=>1200,"EE"=>600,"Total"=>1800),
				"15250 - 15749.99" => array("Mothly Salary Credit"=>15500,"ER"=>1240,"EE"=>620,"Total"=>1860),
				"15750 - 16249.99" => array("Mothly Salary Credit"=>16000,"ER"=>1280,"EE"=>640,"Total"=>1920),
				"16250 - 16749.99" => array("Mothly Salary Credit"=>16500,"ER"=>1320,"EE"=>660,"Total"=>1980),
				"16750 - 17249.99" => array("Mothly Salary Credit"=>17000,"ER"=>1360,"EE"=>680,"Total"=>2040),
				"17250 - 17749.99" => array("Mothly Salary Credit"=>17500,"ER"=>1400,"EE"=>700,"Total"=>2100),
				"17750 - 18249.99" => array("Mothly Salary Credit"=>18000,"ER"=>1440,"EE"=>720,"Total"=>2160),
				"18250 - 18749.99" => array("Mothly Salary Credit"=>18500,"ER"=>1480,"EE"=>740,"Total"=>2220),
				"18750 - 19249.99" => array("Mothly Salary Credit"=>19000,"ER"=>1520,"EE"=>760,"Total"=>2280),
				"19250 - 19749.99" => array("Mothly Salary Credit"=>19500,"ER"=>1560,"EE"=>780,"Total"=>2340),
				"19750 - 1000000" => array("Mothly Salary Credit"=>20000,"ER"=>1600,"EE"=>800,"Total"=>2400),
			); */
			
			#SSS Contribution 2021 - Active
			$arr = array(
				"1000 - 3249.99" => array("Mothly Salary Credit"=>3000,"ER"=>235,"EE"=>135,"Total"=>400),
				"3250 - 3749.99" => array("Mothly Salary Credit"=>3500,"ER"=>307.50,"EE"=>157.50,"Total"=>465),
				"3250 - 4249.99" => array("Mothly Salary Credit"=>4000,"ER"=>350,"EE"=>180,"Total"=>530),
				"4250 - 4749.99" => array("Mothly Salary Credit"=>4500,"ER"=>392.50,"EE"=>202.50,"Total"=>595),
				"4750 - 5249.99" => array("Mothly Salary Credit"=>5000,"ER"=>435,"EE"=>225,"Total"=>660),
				"5250 - 5749.99" => array("Mothly Salary Credit"=>5500,"ER"=>477.50,"EE"=>247.50,"Total"=>725),
				"5750 - 6249.99" => array("Mothly Salary Credit"=>6000,"ER"=>520,"EE"=>270,"Total"=>790),
				"6250 - 6249.99" => array("Mothly Salary Credit"=>6500,"ER"=>562.50,"EE"=>292.50,"Total"=>855),
				"6750 - 7249.99" => array("Mothly Salary Credit"=>7000,"ER"=>605,"EE"=>315,"Total"=>920),
				"7250 - 7749.99" => array("Mothly Salary Credit"=>7500,"ER"=>647.50,"EE"=>337.50,"Total"=>985),
				"7750 - 8249.99" => array("Mothly Salary Credit"=>8000,"ER"=>690,"EE"=>360,"Total"=>1050),
				"8250 - 8749.99" => array("Mothly Salary Credit"=>8500,"ER"=>732.50,"EE"=>382.50,"Total"=>1115),
				"8750 - 9249.99" => array("Mothly Salary Credit"=>9000,"ER"=>775,"EE"=>405,"Total"=>1180),
				"9250 - 9749.99" => array("Mothly Salary Credit"=>9500,"ER"=>817.50,"EE"=>427.50,"Total"=>1245),
				"9750 - 10249.99" => array("Mothly Salary Credit"=>10000,"ER"=>860,"EE"=>450,"Total"=>1310),
				"10250 - 10749.99" => array("Mothly Salary Credit"=>10500,"ER"=>902.50,"EE"=>472.50,"Total"=>1375),
				"10750 - 11249.99" => array("Mothly Salary Credit"=>11000,"ER"=>945,"EE"=>495,"Total"=>1440),
				"11250 - 11749.99" => array("Mothly Salary Credit"=>11500,"ER"=>987.50,"EE"=>517.50,"Total"=>1505),
				"11750 - 12249.99" => array("Mothly Salary Credit"=>12000,"ER"=>1030,"EE"=>540,"Total"=>1570),
				"12250 - 12749.99" => array("Mothly Salary Credit"=>12500,"ER"=>1072.50,"EE"=>562.50,"Total"=>1635),
				"12750 - 13249.99" => array("Mothly Salary Credit"=>13000,"ER"=>1115,"EE"=>585,"Total"=>1700),
				"13250 - 13749.99" => array("Mothly Salary Credit"=>13500,"ER"=>1157.50,"EE"=>607.50,"Total"=>1765),
				"13750 - 14249.99" => array("Mothly Salary Credit"=>14000,"ER"=>1200,"EE"=>630,"Total"=>1830),
				"14250 - 14749.99" => array("Mothly Salary Credit"=>14500,"ER"=>1242.50,"EE"=>652.50,"Total"=>1895),
				"14750 - 15249.99" => array("Mothly Salary Credit"=>15000,"ER"=>1305,"EE"=>675,"Total"=>1980),
				"15250 - 15749.99" => array("Mothly Salary Credit"=>15500,"ER"=>1347.50,"EE"=>697.50,"Total"=>2045),
				"15750 - 16249.99" => array("Mothly Salary Credit"=>16000,"ER"=>1390,"EE"=>720,"Total"=>2110),
				"16250 - 16749.99" => array("Mothly Salary Credit"=>16500,"ER"=>1432.50,"EE"=>742.50,"Total"=>2175),
				"16750 - 17249.99" => array("Mothly Salary Credit"=>17000,"ER"=>1475,"EE"=>765,"Total"=>2240),
				"17250 - 17749.99" => array("Mothly Salary Credit"=>17500,"ER"=>1517.50,"EE"=>787.50,"Total"=>2305),
				"17750 - 18249.99" => array("Mothly Salary Credit"=>18000,"ER"=>1560,"EE"=>810,"Total"=>2370),
				"18250 - 18749.99" => array("Mothly Salary Credit"=>18500,"ER"=>1602.50,"EE"=>832.50,"Total"=>2435),
				"18750 - 19249.99" => array("Mothly Salary Credit"=>19000,"ER"=>1645,"EE"=>855,"Total"=>2500),
				"19250 - 19749.99" => array("Mothly Salary Credit"=>19500,"ER"=>1687.50,"EE"=>877.50,"Total"=>2565),
				"19750 - 20249.99" => array("Mothly Salary Credit"=>20000,"ER"=>1730,"EE"=>900,"Total"=>2630),
				"20250 - 20749.99" => array("Mothly Salary Credit"=>20500,"ER"=>1772.50,"EE"=>922.50,"Total"=>2695),
				"20750 - 21249.99" => array("Mothly Salary Credit"=>21000,"ER"=>1815,"EE"=>945,"Total"=>2760),
				"21250 - 21749.99" => array("Mothly Salary Credit"=>21500,"ER"=>1857.50,"EE"=>967.50,"Total"=>2825),
				"21750 - 22249.99" => array("Mothly Salary Credit"=>22000,"ER"=>1900,"EE"=>990,"Total"=>2890),
				"22250 - 22749.99" => array("Mothly Salary Credit"=>22500,"ER"=>1942.50,"EE"=>1012.50,"Total"=>2955),
				"22750 - 23249.99" => array("Mothly Salary Credit"=>23000,"ER"=>1985,"EE"=>1035,"Total"=>3020),
				"23250 - 23749.99" => array("Mothly Salary Credit"=>23500,"ER"=>2027.50,"EE"=>1057.50,"Total"=>3085),
				"23750 - 24249.99" => array("Mothly Salary Credit"=>24000,"ER"=>2070,"EE"=>1080,"Total"=>3450),
				"24250 - 24749.99" => array("Mothly Salary Credit"=>24500,"ER"=>2112.50,"EE"=>1102.50,"Total"=>3215),
				"24750 - 1000000" => array("Mothly Salary Credit"=>25000,"ER"=>2155,"EE"=>1125,"Total"=>3280),
			);
			$result = '';

			foreach ($arr as $k => $v) {
				$range = array_map('intval', explode('-', $k));
				if ($search_key >= $range[0] && $search_key <= $range[1]) {
					$result = $v;
					break;
				}
			}	
			
			return $result;
		}
	public function isHoliday($mo,$day,$loc){
		 			$query = $this->db->query("SELECT sum(calc) as totalHo FROM tbl_holiday WHERE month(date)=$mo and day(date)=$day and FIND_IN_SET($loc,site)");
			 return $query->result();

	}
	public function loginnNdtimeID($time_id){
		 			$query = $this->db->query("SELECT b.* FROM tbl_time a,tbl_payroll_nd b  where a.time_id=b.time_id and a.time_id=".$time_id);
			 return $query->result();

	}
	 public function breakIdentify($login,$logout,$empid,$typ){
		 
		 	// $query = $this->db->query("SELECT sum(hour) as hr,sum(min) as min FROM `tbl_dtr_logs` a,tbl_acc_time b,tbl_time c, tbl_break_time_account d, tbl_break e,tbl_break_time f where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and c.bta_id=d.bta_id and d.break_id=e.break_id and d.btime_id=f.btime_id and dtr_id between '".$login."' and '".$logout."' and emp_id=".$empid." and a.type='Break' and entry='O'");
			$append= ($typ=="id") ? "dtr_id between '".$login."' and '".$logout."'" : "log between '".$login."' and '".$logout."'";
		 	$query = $this->db->query("SELECT sum(hour) as hr,sum(min) as min FROM tbl_dtr_logs a, tbl_break_time_account d, tbl_break e,tbl_break_time f,tbl_shift_break g where a.acc_time_id=g.sbrk_id and  g.bta_id=d.bta_id and d.break_id=e.break_id and d.btime_id=f.btime_id and ".$append."  and emp_id=".$empid." and a.type='Break' and entry='O'");
			return $query->result();

	}
	public function emplogsShift($sched_id,$empid){
 
			$query = $this->db->query("select distinct(c.time_id),time_start,time_end,(select group_concat(concat(hour,'-',minutes))  from tbl_payroll_nd as s where s.time_id=c.time_id) as nd from tbl_dtr_logs a,tbl_acc_time b,tbl_time c,tbl_schedule d where b.acc_time_id=d.acc_time_id and b.time_id=c.time_id and a.sched_id=d.sched_id and a.sched_id=".$sched_id."  and a.emp_id=".$empid."");
			return $query->result();
 		}
	public function empshift($empid,$dateFrom,$dateTo){
		$query = $this->db->query("SELECT sched_id,sched_date,type,acc_time_id FROM tbl_schedule a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and a.isActive=1 and sched_date >= '".$dateFrom."' and sched_date <= '".$dateTo."' and emp_id=".$empid);
		return $query->result();
	}
	public function timeschedule($timeschedule){
		$query = $this->db->query("SELECT time_start,time_end FROM tbl_schedule a,tbl_acc_time b,tbl_time c  where a.acc_time_id = b.acc_time_id and b.time_id = c.time_id and sched_id=".$timeschedule);
		return $query->result();
	}
	public function empshiftin($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log,entry,acc_time_id,info from tbl_dtr_logs where  type='DTR' and sched_id=".$shift." and emp_id=".$empid." order by dtr_id asc");
			  return $query->result();
	}
	 public function shiftBrk($acc_time_id){
		$query = $this->db->query("SELECT sum(hour*60) as hr, sum(min) as min FROM `tbl_shift_break` a,tbl_break_time_account b,tbl_break_time c where a.bta_id=b.bta_id and b.btime_id = c.btime_id and a.acc_time_id =$acc_time_id and a.isActive=1");
		return $query->result();
	 }
	public function updateAdjustment(){
		$coverage=$this->input->post("coverage");
		$act=$this->input->post("act");
		$amount=$this->input->post("amount");
		$id=$this->input->post("id");
		$emp_id=$this->input->post("emp_id");
		
		$table = ($act=="add") ? "tbl_payroll_emp_addition" : "tbl_payroll_emp_deduction" ;
		$field = ($act=="add") ? "paddition_id" : "pdeduct_id" ;

		$data['value'] = $amount;
		$where = "coverage_id=$coverage and emp_id=$emp_id and ".$field."=$id";
		echo $this->general_model->update_vals($data, $where, $table);

	}
	public function loadAdjustmentEmployee(){
		$type=$this->input->post("class_type");
		$empstat=$this->input->post("empstat");
		$site=$this->input->post("site");
		$coverage=$this->input->post("coverage");
		$act=$this->input->post("act");
				/* if($type=="admin"){
					$typ = "admin";
					$st2 = ($empstat=="trainee") ? "Daily" : "Monthly";
					$conf= ($empstat=="probationary-regular" || $empstat=="trainee") ? 0 : 1;
					$append_empstat = "(g.status!='Part-time') ";
				}else{
					$typ = "agent";
					$st2 = ($empstat=="trainee") ? "Daily" : "Monthly";
					$conf= 0;
					$append_empstat = "(g.status='".ucfirst($empstat)."')";
				}
				  $query = $this->db->query("select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g,tbl_site h where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='".$typ."'  and $append_empstat and confidential=$conf and b.isActive='yes'  and d.isActive=1 and b.salarymode='".$st2."'  and e.pos_details!='SMT Vogmsi' and e.site_ID = h.site_ID and h.code='".strtoupper($site)."'  order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$coverage);
				  
				   */
				  
				  			
				if($type=="admin"){
					$typ = "admin";
					$st2 = ($empstat=="trainee" || $empstat=="trainee-tsn") ? "Daily" : "Monthly";
					$conf= ($empstat=="probationary-regular" || $empstat=="probationary-regular-tsn" || $empstat=="trainee" || $empstat=="trainee-tsn") ? "and confidential=0" : "and confidential=1";
					$append_empstat = ($empstat=="trainee") ? "(g.status!='Part-time')" :  "(g.status!='Part-time' and pos_name!='OJT')";
					
				}else{
					$typ = "agent";
					$st2 = ($empstat=="trainee") ? "Daily" : "Monthly";
					$conf= "and confidential=0";
					$append_empstat = "(g.status='".ucfirst($empstat)."')";
				}
				$append_tsn = ($empstat=="probationary-regular-tsn" || $empstat=="trainee-tsn" || $empstat=="confidential-tsn") ? "(e.pos_name like '%TSN%')" :  "(e.pos_name not like '%TSN%')";
				$conf = ($empstat=="probationary-regular-tsn" || $empstat=="trainee-tsn" || $empstat=="confidential-tsn") ? "" : $conf;
				  $query = $this->db->query("select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g,tbl_site h where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='".$typ."'  and $append_empstat  $conf and b.isActive='yes' and f.isActive=1  and d.isActive=1 and b.salarymode='".$st2."'  and e.pos_details!='SMT Vogmsi'  and e.site_ID = h.site_ID and h.code='".strtoupper($site)."' and $append_tsn order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$coverage);

		$record = array();
		foreach ($query->result() as $row){
				$adjustment = $this->empadjustment($row->emp_id,$act,$coverage);
					if(!empty($adjustment)){
						$record['employee'][$row->lname."-".$row->emp_id] = array(
						"apid" => $row->apid,
						"fname" => $row->fname,
						"lname" => $row->lname,
						"emp_id" => $row->emp_id,
						"adjustment" => $adjustment,
						"cnt" => count($adjustment),
						);
					}
				}		
			echo (!empty($record)) ? json_encode($record) : 0;
		}

	public function export_adjustment_template($type,$empstat,$site,$act,$coverage){
		$this->load->library('PHPExcel', NULL, 'excel');
		$objPHPExcel = $this->excel;
		$flname = ($act == "add") ? "Payroll_Addition_Template" : "Payroll_Deduction_Template" ; 
		$objPHPExcel->setActiveSheetIndex(0)->setTitle($flname);
		$sheet = $objPHPExcel->getActiveSheet(0); 
		//-------------------------------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(0);  //setOffsetY has no effect
        $objDrawing->setCoordinates('B1');
		$objDrawing->setResizeProportional(false);
		// set width later
		$objDrawing->setWidth(100);
		$objDrawing->setHeight(50);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
		//----------------------------------------------------------------------
				/* if($type=="admin"){
					$typ = "admin";
					$st2 = ($empstat=="trainee") ? "Daily" : "Monthly";
					$conf= ($empstat=="probationary-regular" || $empstat=="trainee") ? 0 : 1;
					$append_empstat = "(g.status!='Part-time') ";
				}else{
					$typ = "agent";
					$st2 = ($empstat=="trainee") ? "Daily" : "Monthly";
					$conf= 0;
					$append_empstat = "(g.status='".ucfirst($empstat)."')";
				} */
					// $adjustment = $this->empadjustment($row->emp_id,$act,$coverage);

			// $query ="selects x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g,tbl_site h where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='".$typ."'  and $append_empstat and confidential=$conf and b.isActive='yes'  and d.isActive=1 and b.salarymode='".$st2."'  and e.pos_details!='SMT Vogmsi'  and e.site_ID = h.site_ID and h.code='".strtoupper($site)."' and f.isActive=1 order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$coverage;
			$letter ='Z';
			
				if($type=="admin"){
					$typ = "admin";
					$st2 = ($empstat=="trainee" || $empstat=="trainee-tsn") ? "Daily" : "Monthly";
					$conf= ($empstat=="probationary-regular" || $empstat=="probationary-regular-tsn" || $empstat=="trainee" || $empstat=="trainee-tsn") ? "and confidential=0" : "and confidential=1";
					$append_empstat = ($empstat=="trainee") ? "(g.status!='Part-time')" :  "(g.status!='Part-time' and pos_name!='OJT')";
					
				}else{
					$typ = "agent";
					$st2 = ($empstat=="trainee") ? "Daily" : "Monthly";
					$conf= "and confidential=0";
					$append_empstat = "(g.status='".ucfirst($empstat)."')";
				}
				$append_tsn = ($empstat=="probationary-regular-tsn" || $empstat=="trainee-tsn" || $empstat=="confidential-tsn") ? "(e.pos_name like '%TSN%')" :  "(e.pos_name not like '%TSN%')";
				$conf = ($empstat=="probationary-regular-tsn" || $empstat=="trainee-tsn" || $empstat=="confidential-tsn") ? "" : $conf;
				  $query ="select x.*,y.payroll_id from (select DISTINCT(emp_promoteId),b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g,tbl_site h where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='".$typ."'  and $append_empstat  $conf and b.isActive='yes' and f.isActive=1  and d.isActive=1 and b.salarymode='".$st2."'  and e.pos_details!='SMT Vogmsi'  and e.site_ID = h.site_ID and h.code='".strtoupper($site)."' and $append_tsn order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$coverage;

			$result = $this->general_model->custom_query($query);
			
			if($act == "add"){
				$adj_query = "SELECT paddition_id as id,additionname as adj FROM tbl_payroll_addition where isActive=1 order by additionname";
			}else{
				$adj_query = "SELECT pdeduct_id as id,deductionname as adj FROM tbl_payroll_deductions where isActive=1 order by deductionname";
			}
			$adj_result = $this->general_model->custom_query($adj_query);
			$j=0;
			$jj=3;
				$sheet->setCellValueByColumnAndRow(0,1, "sZPayrollCoveragec0d3");
				$sheet->getStyle("A1:A1")->applyFromArray(array('font' => array('color' => array('rgb' => 'FFFFFF'))));
				$sheet->setCellValueByColumnAndRow(3,1, "Coverage");
				$sheet->setCellValueByColumnAndRow(3,2, "--");
				
				$s=5;
				
 				$sheet->setCellValueByColumnAndRow($j++,$s, "ID");
 				$sheet->setCellValueByColumnAndRow($j++,$s, "LASTNAME");
				$sheet->setCellValueByColumnAndRow($j++,$s, "FIRSTNAME");
 				foreach($adj_result as $row){
					$sheet->setCellValueByColumnAndRow($jj++,4, $row->id);
				}
				foreach($adj_result as $row){
					$sheet->setCellValueByColumnAndRow($j++,$s, $row->adj);
				}

			$s=6;

			foreach($result as $row){
				if($act == "add"){
					$adj_qry = "SELECT additionname,value FROM tbl_payroll_addition a left  join tbl_payroll_emp_addition b on a.paddition_id = b.paddition_id and a.isActive=1 and emp_id=".$row->emp_id." and coverage_id=$coverage order by additionname";
				}else{
					$adj_qry = "SELECT deductionname,value FROM tbl_payroll_deductions a left  join tbl_payroll_emp_deduction b on a.pdeduct_id = b.pdeduct_id and a.isActive=1 and emp_id=".$row->emp_id." and coverage_id=$coverage order by deductionname";
				}
				$adj_rs = $this->general_model->custom_query($adj_qry);
				$j=0;
				
					$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->emp_id));
					$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->lname));
					$sheet->setCellValueByColumnAndRow($j++,$s,strtoupper($row->fname));
					$d=4;
					foreach($adj_rs as $row_adj){
						
						$letter= $this->columnLetter($d);
						$sheet->getStyle($letter)->getNumberFormat()->setFormatCode('#,##0.00');
						$adj =  ($row_adj->value!=null) ? $row_adj->value: "0.00";
						$sheet->setCellValueByColumnAndRow($j++,$s,$adj);
						
						$d++;
					}
				$s++;
			}
 			
				foreach(range('A','Z') as $columnID) {
					$sheet->getColumnDimension($columnID)->setAutoSize(true);
				}
				$sheet->setShowGridlines(false);
				$sheet->getStyle("A5:".$letter."5")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('2d2d2d');
				$sheet->getStyle("A5:".$letter."5")->applyFromArray(array('font' => array('color' => array('rgb' => 'FFFFFF'))));
				$sheet->getColumnDimension('A')->setVisible(FALSE);
				$sheet->getRowDimension(4)->setVisible(FALSE);
        $filename = $flname.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
		
	}
		function uploadAdjustment(){
			$this->load->library('PHPExcel', NULL, 'excel');

 		$filename = explode(".",$_FILES['excel_file']['name']);
			if(!empty($_FILES['excel_file']['name']) && $filename[1]=="xls"){
				$tmpfname = $_FILES['excel_file']['tmp_name'];
				$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
				$excelObj = $excelReader->load($tmpfname);
				$worksheet = $excelObj->getActiveSheet();
				$lastRow = $worksheet->getHighestRow();
				$lastCol = $worksheet->getHighestRowAndColumn();
				
			 if(trim($worksheet->getCell('A1')->getValue())=="sZPayrollCoveragec0d3"){
				 
					$output = $worksheet->rangeToArray('B5:' . $lastCol['column'] . $lastCol['row']);
					$output = array_map('array_filter', $output);
					$output = array_filter($output);

					echo json_encode($output);
			 }else{
					echo  "Invalid";
			 }
			 
			}else{
				echo "Invalid";
			}
		}
		function saveAdjustment($act,$coverage_id){
		$this->load->library('PHPExcel', NULL, 'excel');

 		$filename = explode(".",$_FILES['excel_file']['name']);
			if(!empty($_FILES['excel_file']['name']) && $filename[1]=="xls"){
				$tmpfname = $_FILES['excel_file']['tmp_name'];
				$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
				$excelObj = $excelReader->load($tmpfname);
				$worksheet = $excelObj->getActiveSheet();
				$lastRow = $worksheet->getHighestRow();
				$lastCol = $worksheet->getHighestRowAndColumn();
				
			 if(trim($worksheet->getCell('A1')->getValue())=="sZPayrollCoveragec0d3"){
				 
					$output = $worksheet->rangeToArray('A4:' . $lastCol['column'] . $lastCol['row']);
					$output = array_map('array_filter', $output);
					$output = array_filter($output);

					$data = $worksheet->rangeToArray('A6:' . $lastCol['column'] . $lastCol['row']);
					$data = array_map('array_filter', $data);
					$data = array_filter($data);
					for($i=0;$i<count($data);$i++){
						$val = $data[$i];
						$empid = (int)$data[$i][0];
						$where['coverage_id'] = $coverage_id;
						$where['emp_id'] = $empid;
						$table = ($act=="add") ? "tbl_payroll_emp_addition" : "tbl_payroll_emp_deduction" ;
						$field = ($act=="add") ? "paddition_id" : "pdeduct_id";
						$delete_stat = $this->general_model->delete_vals($where, $table);  
						for($j=3;$j<count($val);$j++){
							$value = $val[$j];
							if($value>0){
								$dtr[$field] = $output[0][$j];
								$dtr["value"] = str_replace(",","",$value);
								$dtr["emp_id"] = $empid;
								$dtr["coverage_id"] = $coverage_id;
 								echo $this->general_model->insert_vals_last_inserted_id($dtr,$table);

								 // echo $output[0][$j]." ".$value."  ".$empid." **** " ;
							}
						}
					}
					 // echo json_encode($data);
 			 }else{
					echo  "Invalid";
			 }
			 
			}else{
				echo "Invalid";
			}
		}
		
	##### Start of Flexi Schedule Code
		
		public function download_template_flexi($dateStart,$dateEnd,$empid){
			$this->load->library('PHPExcel', NULL, 'excel');
			$objPHPExcel = $this->excel;
			$flname =  "SZ_FLEXIBLE_TEMPLATE" ; 
			$objPHPExcel->setActiveSheetIndex(0)->setTitle($flname);
			$sheet = $objPHPExcel->getActiveSheet(0); 
			//-------------------------------------------------------------------
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath($this->logo);
			$objDrawing->setOffsetX(0);    // setOffsetX works properly
			$objDrawing->setOffsetY(0);  //setOffsetY has no effect
			$objDrawing->setCoordinates('B1');
			$objDrawing->setResizeProportional(false);
			// set width later
			$objDrawing->setWidth(150);
			$objDrawing->setHeight(50);
			$objDrawing->setWorksheet($this->excel->getActiveSheet());
			//----------------------------------------------------------------------
				$query = "SELECT emp_id,a.apid,fname,lname FROM tbl_applicant a,tbl_employee b where a.apid=b.apid and emp_id in (".str_replace("_",",",$empid).")";
				
				$result = $this->general_model->custom_query($query);
				
				$j=0;
				$jj=3;
					$sheet->setCellValueByColumnAndRow(0,1, "sZPayrollFlexic0d3");
					$sheet->getStyle("A1:A1")->applyFromArray(array('font' => array('color' => array('rgb' => 'FFFFFF'))));
					$sheet->getStyle("E1:E1")->applyFromArray(array('font' => array('color' => array('rgb' => 'FF0000'))));
					$sheet->setCellValueByColumnAndRow(4,1, "NOTE:");
					$sheet->setCellValueByColumnAndRow(4,2, 'Please follow this format "dd/mm/yyyy h:mm"');
					

					$s=5;
					
					$sheet->setCellValueByColumnAndRow($j++,$s, "ID");
					$sheet->setCellValueByColumnAndRow($j++,$s, "LASTNAME");
					$sheet->setCellValueByColumnAndRow($j++,$s, "FIRSTNAME");
					$sheet->setCellValueByColumnAndRow($j++,$s, "DATE");
					$sheet->setCellValueByColumnAndRow($j++,$s, "CLOCK-IN");
					$sheet->setCellValueByColumnAndRow($j++,$s, "CLOCK-OUT");
					

				$s=6;
				$dateRange = $this->DateRange($dateStart,$dateEnd);
				$color = 1;
				foreach($result as $row){
					
					
					foreach($dateRange as $row1 => $data){
						$j=0;
						$sheet->getStyle('E'.$s)->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');
						$sheet->getStyle('F'.$s)->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->emp_id));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->lname));
						$sheet->setCellValueByColumnAndRow($j++,$s,strtoupper($row->fname));
						$sheet->setCellValueByColumnAndRow($j++,$s, $data);
						// $sheet->setCellValue('G'.$s,'=INT(F'.$s.'-E'.$s.')*8+(((F'.$s.'-E'.$s.')-INT(F'.$s.'-E'.$s.'))/0.04166666)');
						$sheet->getStyle('E'.$s.':F'.$s)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
						$sheet->getStyle('E'.$s.':F'.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F7FCB6');
						$sheet->getStyle('A'.$s.':F'.$s)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
						if($color%2==0){
							$sheet->getStyle('B'.$s.':D'.$s)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CECECE');
						}
					$s++;
					}
					$color++;
				}
				

				foreach(range('A','D') as $columnID) {
					$sheet->getColumnDimension($columnID)->setAutoSize(true);
				}
				foreach(range('E','F') as $columnID) {
					$sheet->getColumnDimension($columnID)->setAutoSize(false);
					$sheet->getColumnDimension($columnID)->setWidth(25);

				}
				$sheet->getProtection()->setPassword('SzFlexiC0d3');
				$sheet->getProtection()->setSheet(true);

				$sheet->setShowGridlines(false);
				$sheet->getStyle("A5:F5")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('2d2d2d');
				$sheet->getStyle("A5:F5")->applyFromArray(array('font' => array('color' => array('rgb' => 'FFFFFF'))));
				$sheet->getColumnDimension('A')->setVisible(FALSE);
				$sheet->getColumnDimension('G')->setVisible(FALSE);
				$sheet->getRowDimension(4)->setVisible(FALSE);
			$filename = $flname.'.xls'; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save('php://output');
		}
		
		function uploadDTRFlex(){
		$this->load->library('PHPExcel', NULL, 'excel');
 		$filename = explode(".",$_FILES['excel_file']['name']);
			if(!empty($_FILES['excel_file']['name']) && $filename[1]=="xls"){
				$tmpfname = $_FILES['excel_file']['tmp_name'];
				$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
				$excelObj = $excelReader->load($tmpfname);
				$worksheet = $excelObj->getActiveSheet();
				$lastRow = $worksheet->getHighestRow();
				$lastCol = $worksheet->getHighestRowAndColumn();
				
			 if(trim($worksheet->getCell('A1')->getValue())=="sZPayrollFlexic0d3"){
				 
					$output = $worksheet->rangeToArray('B6:' . $lastCol['column'] . $lastCol['row']);
					// $output = array_map('array_filter', $output);
					// $output = array_filter($output);
					$arr = array();
					$arr["header"] = array("LASTNAME","FIRSTNAME","DATE","CLOCK-IN","CLOCK-OUT");
					for($i=0;$i<count($output);$i++){
						
						if($output[$i][3]!=null && $output[$i][4]!=null){
							// $arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]]= $output[$i];
							$dateIn = date("Y-m-d H:i",strtotime(substr_replace($output[$i][3], "", -2)));
							$dateOut = date("Y-m-d H:i",strtotime(substr_replace($output[$i][4], "", -2)));
							
 							$arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][0]= $output[$i][0];
							$arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][1]= $output[$i][1];
							$arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][2]= $output[$i][2];
							$arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][3]= $dateIn;
							$arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][4]= $dateOut;

							$rs = (strtotime($dateOut)-strtotime($dateIn))/3600;
							$arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][5]= $rs;
						}
					}

					echo json_encode($arr);
			 }else{
					echo  "Invalid";
			 }
			}else{
				echo "Invalid";
			}
		}
		function saveDTRFlex(){
			$this->load->library('PHPExcel', NULL, 'excel');

 		$filename = explode(".",$_FILES['excel_file']['name']);
			if(!empty($_FILES['excel_file']['name']) && $filename[1]=="xls"){
				$tmpfname = $_FILES['excel_file']['tmp_name'];
				$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
				$excelObj = $excelReader->load($tmpfname);
				$worksheet = $excelObj->getActiveSheet();
				$lastRow = $worksheet->getHighestRow();
				$lastCol = $worksheet->getHighestRowAndColumn();
				
			 if(trim($worksheet->getCell('A1')->getValue())=="sZPayrollFlexic0d3"){
				 
					$output = $worksheet->rangeToArray('A6:' . $lastCol['column'] . $lastCol['row']);
					// $output = array_map('array_filter', $output);
					// $output = array_filter($output);
					$arr = array();
					$clockout = 0;
							// $dateIn = date("Y-m-d H:i",strtotime(substr_replace($output[$i][3], "", -2)));
							// $dateOut = date("Y-m-d H:i",strtotime(substr_replace($output[$i][4], "", -2)));
							
 							// $arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][0]= $output[$i][0];
							// $arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][1]= $output[$i][1];
							// $arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][2]= $output[$i][2];
							// $arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][3]= $dateIn;
							// $arr["data"][$output[$i][0]." ".$output[$i][1]][$output[$i][2]][4]= $dateOut;

							// $rs = (strtotime($dateOut)-strtotime($dateIn))/3600;

					for($i=0;$i<count($output);$i++){
						if($output[$i][4]!=null or $output[$i][5]!=null){
							
								// $clockInDateTime = $this->dateTimeFormat(substr_replace($output[$i][4], -2));
								// $clockOutDateTime = $this->dateTimeFormat(substr_replace($output[$i][5], -2));
							$clockInDateTime = date("Y-m-d H:i",strtotime(substr_replace($output[$i][4], "", -2)));
							$clockOutDateTime = date("Y-m-d H:i",strtotime(substr_replace($output[$i][5], "", -2)));

							$rs = (strtotime($clockOutDateTime)-strtotime($clockInDateTime))/3600;
							
							  if($rs>0){
							  	$dtr["emp_id"] = $output[$i][0]; //emp_id
								$dtr["sched_date"] = $output[$i][3];
								$dtr["schedType_id"] = 14;
								$dtr["remarks"] = "Excel";
								$dtr["updated_by"] = $_SESSION["uid"];
 								$sched = $this->general_model->insert_vals_last_inserted_id($dtr,"tbl_schedule");
							
							
								if($sched>0){
									$clockin = $this->addLogsFlexi($output[$i][0],$clockInDateTime,$sched,"I",null);
										if($clockin>0){
											$clockout = $this->addLogsFlexi($output[$i][0],$clockOutDateTime,$sched,"O",$clockin);
										}
								}   
							}     
							 							 
							
							// $arr[$output[$i][0]][$output[$i][3]] = array(
								// // "sched_date" => $date[1]."/".$date[0]."/".$date[2]." ".$time,
								// "sched_date" => $output[$i][3],
								// "clock-in" => $clockInDateTime,
								// "clock-out" => $clockOutDateTime
							// );
						}
						
					}
					 echo $clockout;
					 // echo json_encode($arr);
			 }else{
					echo  "Invalid";
			 }
			 
			}else{
				echo "Invalid";
			}
		}
		function dateTimeFormat($datetime){
			$separate_date = explode(" ",$datetime);
			$date =  explode("/",$separate_date[0]);
			$time =  $separate_date[1];
			  return date("Y-m-d H:i",strtotime($date[2]."/".$date[0]."/".$date[1]." ".$time));
 		}
		function addLogsFlexi($emp_id,$log,$sched,$entry,$inID){
				$dtr["emp_id"] = $emp_id;
				$dtr["info"] = "Flexible";
				$dtr["entry"] = $entry;
				$dtr["log"] = $log;
				$dtr["type"] = "DTR";
				$dtr["sched_id"] = $sched;
				$dtr["note"] = ($entry=="I") ? null : $inID;
				  return $this->general_model->insert_vals_last_inserted_id($dtr,"tbl_dtr_logs");
				// return json_encode($dtr);

		}
		function showFlexiSched(){
			$empid = $this->input->post("emp_id");
			$date1 = $this->input->post("date1");
			$date2 = $this->input->post("date2");
			$fields = "fname,lname,dtr_id,a.sched_id,entry,a.emp_id,log,type,sched_date";
			$where = "a.emp_id=c.emp_id and c.apid=d.apid and a.sched_id=b.sched_id and b.schedtype_id=14 and a.emp_id in (".implode(",",$empid).") and sched_date between '".$date1."' and '".$date2."'";
			$record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_dtr_logs a,tbl_schedule b,tbl_employee c,tbl_applicant d");
			$arr = array();
			foreach($record as $row){
				$arr[$row->sched_id][$row->lname.", ".$row->fname][] =  array(
					$row->entry => $row->log,
					"date" => $row->sched_date,
				);
			}
			echo json_encode($arr);
 		}
		function insertManualFlexiLogs(){
			$empid = $this->input->post("emp");
			$sched_date = date("Y-m-d",strtotime($this->input->post("sched_date")));
 			$clockInDateTime =  date("Y-m-d H:i",strtotime($this->input->post("clockIn")));
 			$clockOutDateTime =  date("Y-m-d H:i",strtotime($this->input->post("clockOut")));

			$dtr["emp_id"] = $empid; //emp_id
			$dtr["sched_date"] = $sched_date;
			$dtr["schedType_id"] = 14;
			$dtr["remarks"] = "Excel";
			$dtr["updated_by"] = $_SESSION["uid"];
			$sched = $this->general_model->insert_vals_last_inserted_id($dtr,"tbl_schedule");

		
		if($sched>0){
			$clockin = $this->addLogsFlexi($empid,$clockInDateTime,$sched,"I",null);
				if($clockin>0){
					$clockout = $this->addLogsFlexi($empid,$clockOutDateTime,$sched,"O",$clockin);
				}
		}  
		echo $clockout;
 		}
		##### End of Flexi Schedule Code
		
		public function DateRange($date1,$date2){
			$strDateFrom = date("Y-m-d", strtotime($date1));
			$strDateTo = date("Y-m-d", strtotime($date2));
			$aryRange=array();
			$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
			$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
				if($iDateTo>=$iDateFrom){
					array_push($aryRange,date('Y-m-d',$iDateFrom));
					 while($iDateFrom<$iDateTo){
						$iDateFrom+=86400; // add 24 hours
							array_push($aryRange,date('Y-m-d',$iDateFrom));
						}
				}
			 return $aryRange;
			 		
		}
	function columnLetter($c){

		$c = intval($c);
		if ($c <= 0) return '';

		$letter = '';
		while($c != 0){
		$p = ($c - 1) % 26;
		$c = intval(($c - $p) / 26);
		$letter = chr(65 + $p) . $letter;
		}
		return $letter;
	}
	
	public function empadjustment($emp_id,$act,$coverage,$id=null){
		$field_id =($act=="add") ? "a.paddition_id" : "a.pdeduct_id" ;

		$adj_id = ($id==null) ? "" : " and ".$field_id." = ".$id;
		$fields =($act=="add") ? "a.paddition_id as id,additionname as options,b.* " : "a.pdeduct_id as id,deductionname as options,b.* " ;
		$table = ($act=="add") ? "tbl_payroll_addition a,tbl_payroll_emp_addition b" : "tbl_payroll_deductions a,tbl_payroll_emp_deduction b";
        $where = ($act=="add") ? "a.paddition_id = b.paddition_id and  emp_id=".$emp_id." and coverage_id=".$coverage." ".$adj_id." order by additionname" :  "a.pdeduct_id =b.pdeduct_id  and  emp_id=".$emp_id." and coverage_id=".$coverage." ".$adj_id." order by deductionname";

		$qry = "select ".$fields." from ".$table." where isActive=1 and ".$where; 
		return $record = $this->general_model->custom_query($qry);

		
	}
	
	public function trest(){
		

	}
	public function savePayroll(){
		
		$data=json_decode($this->input->post("dataArray"));
		$coverage_id=$this->input->post("coverage_id");
		$site=$this->input->post("site");
		$arr= array();
		$ahr_id= array();
		$leave_id= array();
  		
		 if(count($data)>0){
			 
				# 1.) Delete the current saved Payroll to renew the data.
				$type = explode("---",$data[0][22]);
				
				if(strtolower($site)=="cdo"){
					// $emp_type= substr($type[1], 0, -4);
					$emp_type= $type[1];
					// $where = "coverage_id=$coverage_id and emp_type='$emp_type' or emp_type='$type[1]'";
					$where = "coverage_id=$coverage_id and emp_type='$emp_type'";
 				}else{
					
					$where['coverage_id'] = $coverage_id;
					$where['emp_type'] =  $type[1];

				}
				// echo json_encode($data);
				// echo $where." >> ".$type[1];
				// die();
				$emp_type=$type[1];
				
				$table =  "tbl_payroll" ;
				$delete_payroll = $this->general_model->delete_vals($where, $table); 
				
				# 2.) Get the ids of AHR & Leave.
				$tbl_fields = "id";
				$tbl_pr_ahr_where['coverage_id'] = $coverage_id;
				$tbl_pr_ahr_where['emp_type'] =  $emp_type;
				$tbl_pr_ahr_where['type'] =  "AHR";
				$tbl_pr_ahr_recordz = $this->general_model->fetch_specific_val($tbl_fields, $tbl_pr_ahr_where, 'tbl_payroll_ahr_leave');

				$tbl_leave_fields = "id";
				$tbl_pr_leave_where['coverage_id'] = $coverage_id;
				$tbl_pr_leave_where['emp_type'] =  $emp_type;
				$tbl_pr_leave_where['type'] =  "LEAVE";
				$tbl_pr_leave_recordz = $this->general_model->fetch_specific_val($tbl_leave_fields, $tbl_pr_leave_where, 'tbl_payroll_ahr_leave');

				# 3.) update ahr back to unreleased for payroll.
				if(!empty($tbl_pr_ahr_recordz->id)){
				$tbl_ahr_data['isReleased'] = 2;
				$tbl_ahr_where =  "additionalHourRequestId in (".$tbl_pr_ahr_recordz->id.")";
 				$update_stat = $this->general_model->update_vals($tbl_ahr_data, $tbl_ahr_where, 'tbl_additional_hour_request');
				}
				if(!empty($tbl_pr_leave_recordz->id)){
				$tbl_leave_data['isReleased'] = 0;
				$tbl_leave_where =  "requestLeaveDetails_ID in (".$tbl_pr_leave_recordz->id.")";
 				$update_stat = $this->general_model->update_vals($tbl_leave_data, $tbl_leave_where, 'tbl_request_leave_details');
				}
				 
				# 4.) Delete ahr records to renew data.
				$ahr_where['coverage_id'] = $coverage_id;
				$ahr_where['emp_type'] =  $emp_type;
				$ahr_table =  "tbl_payroll_ahr_leave" ;
				$delete_payroll_ahr = $this->general_model->delete_vals($ahr_where, $ahr_table);  

			foreach($data as $key1 => $val){
				
				foreach($val as $key2 => $item){
					$record = explode("---",$item);
						 $arr[$key1][$record[0]] = $record[1];
						
						if($record[0]=="ahr_id" and $record[1]!=""){
							$ahr_id[] =  $record[1];
						}
						if($record[0]=="leave_id" and $record[1]!=""){
							$leave_id[] =  $record[1];
						}
						
					
				}
				$arr[$key1]["coverage_id"] = $coverage_id;
			 }
					if(count($ahr_id)>0){
						$ahr_ids = implode(",",$ahr_id);
						$ahr_data['coverage_id'] = $coverage_id;
						$ahr_data['type'] =  "AHR";
						$ahr_data['emp_type'] =  $emp_type;
						$ahr_data['id'] =  $ahr_ids;
						$ahr_rs = $this->general_model->insert_vals_last_inserted_id($ahr_data, 'tbl_payroll_ahr_leave');
						
						$tbl_ahr_data_updt['isReleased'] = 3;
						$tbl_ahr_where_updt =  "additionalHourRequestId in (".$ahr_ids.")";
						$update_stat_updt = $this->general_model->update_vals($tbl_ahr_data_updt, $tbl_ahr_where_updt, 'tbl_additional_hour_request');
					}
					if(count($leave_id)>0){
						$leave_ids = implode(",",$leave_id);
						$leave_data['coverage_id'] = $coverage_id;
						$leave_data['type'] =  "LEAVE";
						$leave_data['emp_type'] =  $emp_type;
						$leave_data['id'] =  $leave_ids;
						$leave_rs = $this->general_model->insert_vals_last_inserted_id($leave_data, 'tbl_payroll_ahr_leave');
						

						$tbl_leave_data_updt['isReleased'] = 1;
						$tbl_leave_where_updt =  "requestLeaveDetails_ID in (".$leave_ids.")";
						$update_stat_updt_leave = $this->general_model->update_vals($tbl_leave_data_updt, $tbl_leave_where_updt, 'tbl_request_leave_details');
					}
			 if($delete_payroll>0){
				 $rs = $this->general_model->batch_insert($arr, 'tbl_payroll');
				
 			 }else{
				$rs=0;
			 }
		 }else{
			 $rs=0;
		 }
		 echo $rs;
		
		
	}
	public function loadEmployee(){
		// $append = ($month=="All") ? "" : " and month='".$month."'";
		// $fields = "*";
        // $where = "year=$year ".$append;
        // $record = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_coverage");
		$type=$this->input->post("class_type");
		$empstat=$this->input->post("empstat");
		$site=$this->input->post("site");
		$coverage=$this->input->post("coverage");
				if($type=="admin"){
					$typ = "admin";
					$st2 = ($empstat=="trainee" || $empstat=="trainee-tsn") ? "Daily" : "Monthly";
					$conf= ($empstat=="probationary-regular" || $empstat=="probationary-regular-tsn" || $empstat=="trainee" || $empstat=="trainee-tsn") ? "and confidential=0" : "and confidential=1";
					$append_empstat = ($empstat=="trainee") ? "(g.status!='Part-time')" :  "(g.status!='Part-time' and pos_name!='OJT')";
					
				}else{
					$typ = "agent";
					$st2 = ($empstat=="trainee") ? "Daily" : "Monthly";
					$conf= "and confidential=0";
					$append_empstat = "(g.status='".ucfirst($empstat)."')";
				}
				$append_tsn = ($empstat=="probationary-regular-tsn" || $empstat=="trainee-tsn" || $empstat=="confidential-tsn") ? "(e.pos_name like '%TSN%')" :  "(e.pos_name not like '%TSN%')";
				$conf = ($empstat=="probationary-regular-tsn" || $empstat=="trainee-tsn" || $empstat=="confidential-tsn") ? "" : $conf;
				  $query = $this->db->query("select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g,tbl_site h where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='".$typ."'  and $append_empstat  $conf and b.isActive='yes' and f.isActive=1  and d.isActive=1 and b.salarymode='".$st2."'  and e.pos_details!='SMT Vogmsi'  and e.site_ID = h.site_ID and h.code='".strtoupper($site)."' and $append_tsn order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$coverage);
				  $exclude = array(980,185,979,184); // SZ, Maam En, Sir Mai and Sir Nathan will be excluded from the list
		foreach ($query->result() as $row){ 
			if(!in_array($row->emp_id,$exclude)){
				$record['employee'][$row->lname."-".$row->apid] = array( 
				"apid" => $row->apid,
 				"fname" => $row->fname,
 				"lname" => $row->lname,
 				"emp_id" => $row->emp_id,
 				"payroll_id" => $row->payroll_id,
 				"sql" => "select x.*,y.payroll_id from (select emp_promoteId,b.apid,b.emp_id,fname,lname,pos_name,g.status,rate,(select group_concat(concat(allowance_name,'=',value)) from tbl_allowance as z where z.posempstat_id=c.posempstat_id) as allowance,(select group_concat(concat(bonus_name,'=',amount)) from tbl_pos_bonus as p,tbl_bonus q where q.bonus_id=p.bonus_id and p.posempstat_id=c.posempstat_id) as bunos from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_rate f,tbl_emp_stat g,tbl_site h where g.empstat_id = c.empstat_id and f.posempstat_id = d.posempstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and e.class='".$typ."'  and $append_empstat  $conf and b.isActive='yes' and f.isActive=1  and d.isActive=1 and b.salarymode='".$st2."'  and e.pos_details!='SMT Vogmsi'  and e.site_ID = h.site_ID and h.code='".strtoupper($site)."' and $append_tsn order by lname) as x left join tbl_payroll y on x.emp_promoteId = y.emp_promoteId and y.coverage_id=".$coverage,
  				);
			}		
		}		
		echo json_encode($record);
		
	}
	
      public function pcCheck(){
	
	$pcyear = $this->input->post('pcyear');
	$pcmonth = $this->input->post('pcmonth');
	$pcperiod = $this->input->post('pcperiod');
	if(!empty($pcyear) and  $pcmonth!="undefined" and $pcperiod!="null"){
		$qry =  $this->db->query("select count(*) as cnt from tbl_payroll_coverage where year=".$pcyear." and month='".$pcmonth."'  and period=".$pcperiod."");
		 
			echo $qry->result()[0]->cnt;
		 
	}else{
		echo "3";
	}
	}
public function pcInsert(){
	$pcyear = $this->input->post('pcyear');
	$pcmonth = $this->input->post('pcmonth');
	$pcperiod = $this->input->post('pcperiod');
	$pcDescription = $this->input->post('pcDescription');
	$transact_date= explode("-",$pcDescription);
	$transact_date2= date('y-m-d',strtotime($transact_date[1]."+1 days"));


	$qry =  $this->db->query("insert into tbl_payroll_coverage(daterange,transact_date,month,year,period,status) values('$pcDescription','".$transact_date2."','$pcmonth','$pcyear','$pcperiod','Pending')");
	$id = $this->db->insert_id();
		if($this->db->affected_rows() >0){
			echo $id;

 		}else{
			echo 0;

		}
	

}
public function downloadPayslipExcel($coverage=null,$emp_id = null){
$this->load->library('PHPExcel', NULL, 'excel');
			$objPHPExcel = $this->excel;
			$flname =  "SZ_PAYSLIP" ; 
			$objPHPExcel->setActiveSheetIndex(0)->setTitle($flname);
			$sheet = $objPHPExcel->getActiveSheet(0); 
			
            $emp_ids = explode("_", $emp_id);

			$emp =  implode(",",$emp_ids);
				$fields = "f.emp_id,fname,lname,isAtm,pos_details,pos_name,b.*,e.*";
				$tables= "tbl_emp_promote a,tbl_payroll b,tbl_pos_emp_stat c,tbl_position d,tbl_payroll_coverage e,tbl_employee f,tbl_applicant g";
				$where = "a.emp_promoteID = b.emp_promoteID and b.coverage_id = e.coverage_id and c.posempstat_id=a.posempstat_id and c.pos_id=d.pos_id and a.emp_id=f.emp_id and f.apid=g.apid and a.emp_id in ($emp) and b.coverage_id=$coverage";
			$result = $this->general_model->fetch_specific_vals($fields, $where, $tables, "lname ASC");	
				
				$j=0;
				$jj=3;
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$styleArray = array(
					'font' => array(
						'bold' => true,
 					),
					 
				);
				$styleArrayCenter = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
					 
				);
				foreach($result as $row){
					
						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName('Logo'.$row->emp_id);
						$objDrawing->setDescription('Logo'.$row->emp_id);
						$objDrawing->setPath($this->logo);
						$objDrawing->setOffsetX(10);
						$objDrawing->setOffsetY(0);
						$objDrawing->setCoordinates($this->columnLetter($j+1)."4");
						$objDrawing->setResizeProportional(false);
						// set width later
						$objDrawing->setWidth(180);
						$objDrawing->setHeight(50);
						$objDrawing->setWorksheet($sheet);
				$clothing = explode("(",$row->clothing);
				$laundry = explode("(",$row->laundry);
				$rice = explode("(",$row->rice);
				$total = 0;
				$deduct = 0;
				$takehome = 0;
				$sheet->getStyle($this->columnLetter($j+1))->applyFromArray($styleArray);
				$sheet->getStyle($this->columnLetter($j+2))->applyFromArray($styleArrayCenter);

				$sheet->setCellValueByColumnAndRow($j,7, "PAY STATEMENT - EMPLOYEES COPY");
				$sheet->setCellValueByColumnAndRow($j,8, "NAME :");
				$sheet->setCellValueByColumnAndRow($j+1,8, $row->lname.", ".$row->fname);
				$sheet->setCellValueByColumnAndRow($j,9, "POSITION :");
				$sheet->setCellValueByColumnAndRow($j+1,9, $row->pos_details);
				$sheet->setCellValueByColumnAndRow($j,10, "PERIOD COVERED :");
				$sheet->setCellValueByColumnAndRow($j+1,10, $row->daterange);
 				$sheet->setCellValueByColumnAndRow($j,11, "ATM :");
				$sheet->setCellValueByColumnAndRow($j+1,11, (($row->isAtm==1) ? "ATM" : "NON-ATM"));

 				$sheet->setCellValueByColumnAndRow($j,12, "EARNINGS");
 				$sheet->setCellValueByColumnAndRow($j,13, "SALARY");
 				$sheet->setCellValueByColumnAndRow($j+1,13, number_format($row->quinsina,2,".",","));
					$total+= $row->quinsina;

 				$sheet->setCellValueByColumnAndRow($j,14, "CLOTHING");
 				$sheet->setCellValueByColumnAndRow($j+1,14, number_format(trim($clothing[0]),2,".",","));
 				$sheet->setCellValueByColumnAndRow($j,15, "LAUNDRY");
 				$sheet->setCellValueByColumnAndRow($j+1,15, number_format(trim($laundry[0]),2,".",","));
 				$sheet->setCellValueByColumnAndRow($j,16, "RICE");
 				$sheet->setCellValueByColumnAndRow($j+1,16,number_format(trim($rice[0]),2,".",","));
					$total+= $clothing[0];
					$total+= $laundry[0];
					$total+= $rice[0];

 				$sheet->setCellValueByColumnAndRow($j,17, "NIGHT DIFFERENTIAL:");
 				$sheet->setCellValueByColumnAndRow($j+1,17, number_format($row->nd,2,".",","));
				$total+= $row->nd;

 				$sheet->setCellValueByColumnAndRow($j,18, "HAZARD PAY:");
 				$sheet->setCellValueByColumnAndRow($j+1,18,  number_format($row->hp,2,".",","));
				$total+= $row->hp;

 				$sheet->setCellValueByColumnAndRow($j,17, "HOLIDAY PAY:");
 				$sheet->setCellValueByColumnAndRow($j+1,17, number_format($row->ho,2,".",","));
				$total+= $row->ho;

				if($row->ahr_amount>0){
 				$sheet->setCellValueByColumnAndRow($j,19, "OVERTIME(".$row->ahr_hour." hr.):");
 				$sheet->setCellValueByColumnAndRow($j+1,19, number_format($row->ahr_amount,2,".",","));
				$total+= $row->ahr_amount;
				}
				$inc = ($row->ahr_amount>0) ? 20 : 19;
				if(trim($row->adjust_details)!="0"){
					 $adjusted = explode(",",$row->adjust_details);
					 

						for($i = 0; $i<count($adjusted); $i++){
								$ii = explode("|",$adjusted[$i]);
								$iii = explode("=",$ii[1]);

								if(trim($ii[0])!="Bonus"){
									$sheet->setCellValueByColumnAndRow($j,$inc,strtoupper($iii[0]));
									$sheet->setCellValueByColumnAndRow($j+1,$inc, number_format(trim($iii[1]),2,".",","));
									$inc++;
									$total +=  $iii[1];

								}
							
							}
					} 
 				$sheet->setCellValueByColumnAndRow($j,$inc, "TOTAL EARNING:	");
 				$sheet->setCellValueByColumnAndRow($j+1,$inc, number_format($total,2,".",","));

 				 $sheet->setCellValueByColumnAndRow($j,$inc+1, " ");
 				$sheet->setCellValueByColumnAndRow($j,$inc+2, "DEDUCTIONS");

				$bir = explode("(",$row->bir_details);
 				$sheet->setCellValueByColumnAndRow($j,$inc+3, "WITHHOLDING TAX: ");
 				$sheet->setCellValueByColumnAndRow($j+1,$inc+3, number_format(trim($bir[0]),2,".",","));
					$deduct+=$bir[0];

 				$sheet->setCellValueByColumnAndRow($j,$inc+4, "SSS PREMIUM:");
 				$sheet->setCellValueByColumnAndRow($j+1,$inc+4, number_format(trim($row->sss_details),2,".",","));
					$deduct+=$row->sss_details;

 				$sheet->setCellValueByColumnAndRow($j,$inc+5, "HDMF PREMIUM:");
 				$sheet->setCellValueByColumnAndRow($j+1,$inc+5, number_format(trim($row->hdmf_details),2,".",","));
					$deduct+=$row->hdmf_details;

 				$sheet->setCellValueByColumnAndRow($j,$inc+6, "PHILHEALTH:");
 				$sheet->setCellValueByColumnAndRow($j+1,$inc+6, number_format(trim($row->phic_details),2,".",","));
					$deduct+=$row->phic_details;

				if(trim($row->absentLate)!="0.00"){
					$sheet->setCellValueByColumnAndRow($j,$inc+7, "ABSENT/UNDERTIME:");
					$sheet->setCellValueByColumnAndRow($j+1,$inc+7, number_format(trim($row->absentLate),2,".",","));
						$deduct+=$row->absentLate;
					$inc = $inc+8;
				}else{
					$inc = $inc+7;
				}
				// $inc = (trim($row->absentLate)!="0.00") ? $inc+8 : $inc+7;
					if(trim($row->deduct_details)!="0"){
					 $deduct_details = explode(",",$row->deduct_details);

						for($i = 0; $i<count($deduct_details); $i++){
								$ii = explode("=",$deduct_details[$i]);
								$iii = explode("|",$ii[0]);

									$sheet->setCellValueByColumnAndRow($j,$inc,strtoupper($iii[0]));
									$sheet->setCellValueByColumnAndRow($j+1,$inc, number_format(trim($ii[1]),2,".",","));
									$inc++;
									$deduct +=  $ii[1];

								 
							
							}
					}
					 
					if(trim($row->loan_details)!="0" && trim($row->loan_details)!="" && trim($row->loan_details)!=null){
						
 						$loan_details = explode(",",$row->loan_details);
						echo count($loan_details);
						for($v = 0; $v<count($loan_details); $v++){
						$vv = explode("=",$loan_details[$v]);
							if(count($vv)>0){
								$sheet->setCellValueByColumnAndRow($j,$inc,strtoupper($vv[0]));
								$sheet->setCellValueByColumnAndRow($j+1,$inc, number_format(trim($vv[1]),2,".",","));
								$inc++;
								$deduct +=  $vv[1];
							}
						}
					} 
				$sheet->setCellValueByColumnAndRow($j,$inc, "TOTAL DEDUCTION: ");
				$sheet->setCellValueByColumnAndRow($j+1,$inc, number_format($deduct,2,".",","));
				$takehome = $total - $deduct;
				$sheet->setCellValueByColumnAndRow($j,$inc+1, " ");
				$sheet->setCellValueByColumnAndRow($j,$inc+2, "TOTAKE HOME PAY: ");
				$sheet->setCellValueByColumnAndRow($j+1,$inc+2, number_format($takehome,2,".",","));
 
				$sheet->setCellValueByColumnAndRow($j,$inc+3, " ");
				$sheet->setCellValueByColumnAndRow($j,($inc+4), "SIGNATURE");
 				$sheet->setCellValueByColumnAndRow($j,($inc+5), "DATE");
				$sheet->setCellValueByColumnAndRow($j,($inc+6), "Note: Keep this statement permanently for Income Tax purposes.");
				
				  $sheet->getStyle($this->columnLetter($j+1).''.($inc+6).':'.$this->columnLetter($j+4).''.($inc+6))->applyFromArray($fontStyle);
 
				$sheet->getColumnDimension($this->columnLetter($j+2))->setAutoSize(true);
				$sheet->getColumnDimension($this->columnLetter($j+1))->setWidth(20);

					$j+=4;
					$inc =0;
 					$total =0;
					$deduct =0;
					$takehome =0;

				}
/*****START BONUS*****/
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(1)->setTitle("Bonus");
			$sheet1 = $objPHPExcel->getActiveSheet();
			$sheet1->setShowGridlines(false);
				$jb=0;
			foreach($result as $row){
				

				$sheet1->getStyle($this->columnLetter($jb+1))->applyFromArray($styleArray);
				$sheet1->getStyle($this->columnLetter($jb+4))->applyFromArray($styleArray);

				$objDrawing1 = new PHPExcel_Worksheet_Drawing();
				$objDrawing1->setName('Logo'.$row->emp_id);
				$objDrawing1->setDescription('Logo'.$row->emp_id);
				$objDrawing1->setPath($this->logo);
				$objDrawing1->setOffsetX(0);
				$objDrawing1->setOffsetY(0);
				$objDrawing1->setCoordinates($this->columnLetter($jb+1)."4");
				$objDrawing1->setResizeProportional(false);
				// set width later
				$objDrawing1->setWidth(150);
				$objDrawing1->setHeight(50);
				$objDrawing1->setWorksheet($sheet1);

				$sheet1->setCellValueByColumnAndRow($jb,7, "BONUS STATEMENT - EMPLOYEES COPY");
				$sheet1->setCellValueByColumnAndRow($jb,8, "NAME :");
				$sheet1->setCellValueByColumnAndRow($jb+1,8, $row->lname.", ".$row->fname);
				$sheet1->setCellValueByColumnAndRow($jb,9, "POSITION :");
				$sheet1->setCellValueByColumnAndRow($jb+1,9, $row->pos_details);
				$sheet1->setCellValueByColumnAndRow($jb,10, "PERIOD COVERED :");
				$sheet1->setCellValueByColumnAndRow($jb+1,10, $row->daterange);
 				$sheet1->setCellValueByColumnAndRow($jb,11, "ATM :");
				$sheet1->setCellValueByColumnAndRow($jb+1,11, (($row->isAtm==1) ? "ATM" : "NON-ATM"));
				$inc=13;

				
					if(trim($row->adjust_details)!="0"){
					$adjusted = explode(",",$row->adjust_details);
						for($i = 0; $i<count($adjusted); $i++){
						$ii = explode("|",$adjusted[$i]);
						$iii = explode("=",$ii[1]);

							if(trim($ii[0])=="Bonus"){
							$sheet1->setCellValueByColumnAndRow($jb,$inc,strtoupper($iii[0]));
							$sheet1->setCellValueByColumnAndRow($jb+1,$inc, number_format(trim($iii[1]),2,".",","));
							// $objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($jb+2).''.$inc.':'.$this->columnLetter($jb+3).''.$inc);
							$inc++;
		 
							}
						}
					}
					

					if(trim($row->bonus_details)!="0"){
						// $sheet1->setCellValueByColumnAndRow($jb+1,$inc,$row->bonus_details);

 						$bonus_details = explode(",",$row->bonus_details);
						for($v = 0; $v<count($bonus_details); $v++){
						$vv = explode("=",$bonus_details[$v]);
 
						$sheet1->setCellValueByColumnAndRow($jb,$inc,strtoupper($vv[0]));
						$sheet1->setCellValueByColumnAndRow($jb+1,$inc, number_format(trim($vv[1]),2,".",","));
						$inc++;
						}
					} 

				$sheet1->setCellValueByColumnAndRow($jb,($inc+2), "SIGNATURE");
 				$sheet1->setCellValueByColumnAndRow($jb+3,($inc+2), "DATE");
				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($jb+1).''.($inc+2).':'.$this->columnLetter($jb+3).''.($inc+2));

				$sheet1->setCellValueByColumnAndRow($jb,($inc+3), "Note: Keep this statement permanently for Income Tax Purposes.");
 				$objPHPExcel->getActiveSheet()->mergeCells($this->columnLetter($jb+1).''.($inc+3).':'.$this->columnLetter($jb+4).''.($inc+3));

				$sheet1->getColumnDimension($this->columnLetter($jb+2))->setAutoSize(true);
				$sheet1->getStyle($this->columnLetter($jb+1).''.($inc+3).':'.$this->columnLetter($jb+4).''.($inc+3))->applyFromArray($fontStyle);

				$jb+=6;
				 }
			 /*****END BONUS*****/
			
				$sheet->setShowGridlines(false);
			$filename = $flname.'.xls'; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save('php://output');
		}



		public function downloadPayslipPDF($coverage=null,$emp_id = null){
        if ($coverage === null && $emp_id === '')
        {
            echo "Failed";
        }
        else
        {
            $emp_ids = explode("_", $emp_id);

			$emp =  implode(",",$emp_ids);
				$fields = "fname,lname,isAtm,pos_details,pos_name,b.*,e.*";
				$tables= "tbl_emp_promote a,tbl_payroll b,tbl_pos_emp_stat c,tbl_position d,tbl_payroll_coverage e,tbl_employee f,tbl_applicant g";
				$where = "a.emp_promoteID = b.emp_promoteID and b.coverage_id = e.coverage_id and c.posempstat_id=a.posempstat_id and c.pos_id=d.pos_id and a.emp_id=f.emp_id and f.apid=g.apid and a.emp_id in ($emp) and b.coverage_id=$coverage";
			$employees = $this->general_model->fetch_specific_vals($fields, $where, $tables, "lname ASC");	

			// var_dump($employees);
			// die();

            //load mPDF library
            $this->load->library('m_pdf');
            //load the pdf.php by passing our data and get all data in $html varriable.
           
            
            $data['employees'] = $employees;
            $html = $this->load->view('templates/payroll/pdf_payslip_template', $data, true);

            $pdfFilePath = "Payslip.pdf";

            try{
             //actually, you can pass mPDF parameter on this load() function
                $pdf = $this->m_pdf->load();
                $pdf->debug = true;
				
				$this->bootstrapcss = ($this->bootstrapcss === '') ? file_get_contents('assets/src/custom/css/payslip.css') : $this->bootstrapcss;
                $stylesheet = '<style>' . $this->bootstrapcss . '</style>';
				
            // apply external css
				$pdf->AddPage('L');
                $pdf->WriteHTML($stylesheet, 1);
                $pdf->WriteHTML($html, 2);
            //offer it to user via browser download! (The PDF won't be saved on your server HDD)
                header("Content-type:application/pdf");

			// It will be called downloaded.pdf
                header("Content-Disposition:inline;filename='".$pdfFilePath."'");
                $pdf->Output($pdfFilePath, "I");
            }catch(\Mpdf\MpdfException $e){
                echo $e->getMessage();
            }
            exit;
        }
    }
	
	
	
	 public function exportpayrolldetail_excel($coverage,$empzCat,$empzType,$empzSite){
	 		$objPHPExcel = $this->phpexcel;
			$objDrawing = new PHPExcel_Worksheet_Drawing();

		 $site = ($empzSite=="CDO") ? "" : $empzSite;
			if($empzType=="Admin"){
				if(trim($empzCat)=="Probationary_and_Regular"){
					$type= "admin-both-nonconfi".strtolower($site);

				}else if(trim($empzCat)=="Probationary_and_Regular-TSN"){
					$type= "admin-both-nonconfi-tsn".strtolower($site);

				}else if(trim($empzCat)=="Confidential"){
					$type= "admin-both-confi".strtolower($site);

				}else if(trim($empzCat)=="Trainee-TSN"){
					$type= "admin-trainee-tsn".strtolower($site);

				}else{
					$type= "admin-trainee".strtolower($site);
				}
			}else{
				

				$type= strtolower($empzType)."-".strtolower($empzCat).strtolower($site);
			}
	
			$query = $this->db->query("SELECT e.fname,e.lname,e.mname,a.*,b.*,c.*,d.*,f.* FROM `tbl_payroll` a,tbl_emp_promote b,tbl_pos_emp_stat c,tbl_employee d,tbl_applicant e ,tbl_position f where f.pos_id=c.pos_id and a.emp_promoteID=b.emp_promoteID and b.posempstat_id =c.posempstat_id and d.emp_id = b.emp_id and e.apid =d.apid and coverage_id= $coverage and emp_type='$type' order by lname"); 
			$coveraged = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$coverage);
			$year= $coveraged->result()[0]->year;
			$month= $coveraged->result()[0]->month; 
			$period= $coveraged->result()[0]->period;
			$daterange= $coveraged->result()[0]->daterange;
			$sheet = $objPHPExcel->getActiveSheet(0);
			//------------------------INSERT LOGO-------------------------------------------
			 $objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Logo');
				$objDrawing->setPath($this->logo);
			$objDrawing->setOffsetX(0);    // setOffsetX works properly
			$objDrawing->setOffsetY(0);  //setOffsetY has no effect
			$objDrawing->setCoordinates('A1');
			$objDrawing->setHeight(92); // logo height
			// $objDrawing->setWidth(320); // logo width
			// $objDrawing->setWidthAndHeight(200,400);
			$objDrawing->setResizeProportional(true);
			$objDrawing->setWorksheet($sheet);
			
			//----------------------------------------------------------------------

			$type2= "report";
				$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
				$style_text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
 
					$sheet->setShowGridlines(false);
					$objPHPExcel->createSheet();
					$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payroll");
				$sheet->setCellValue('G1', "Payroll Period:");
				$sheet->setCellValue('H1', $daterange);
				$sheet->setCellValue('G2', "Year:");
				$sheet->setCellValue('H2', $year);
				$sheet->setCellValue('G3', "Month:");
				$sheet->setCellValue('H3', $month);
				$sheet->setCellValue('G4', "Period:");
				$sheet->setCellValue('H4', $period);
				$sheet->setCellValue('A6', "ID NUMBER");
				$sheet->setCellValue('B6', "LAST NAME");
				$sheet->setCellValue('C6', "FIRST NAME");
				$sheet->setCellValue('D6', "MIDDLE NAME");
				$sheet->setCellValue('E6', "POSITION");
				$sheet->setCellValue('F6', "ATM");
				$sheet->setCellValue('G6', "BASIC SALARY");
				$sheet->setCellValue('H6', "CLOTHING");
				$sheet->setCellValue('I6', "LAUNDRY");
				$sheet->setCellValue('J6', "RICE");
				$sheet->setCellValue('K6', "ND");
				$sheet->setCellValue('L6', "OT-BCT");
				$sheet->setCellValue('M6', "HP");
				$sheet->setCellValue('N6', "HO");
				$sheet->setCellValue('O6', "ALLOWANCE DETAILS");
				$sheet->setCellValue('P6', "ALLOWANCE");
				$sheet->setCellValue('Q6', "ADJUSTMENT DETAILS");
				$sheet->setCellValue('R6', "TOTAL ADJUSTMENT ADDITION");
				$sheet->setCellValue('S6', "GROSS");
				$sheet->setCellValue('T6', "TAX");
				$sheet->setCellValue('U6', "SSS");
				$sheet->setCellValue('V6', "HDMF");
				$sheet->setCellValue('W6', "PHIC");
				$sheet->setCellValue('X6', "ABSENCE/UT/LATE");
				$sheet->setCellValue('Y6', "DEDUCTION DETAIL");
				$sheet->setCellValue('Z6', "TOTAL ADJUSTMENT DEDUCTION");
				$sheet->setCellValue('AA6', "TERM DEDUCTION DETAILS");
				$sheet->setCellValue('AB6', "TOTAL TERM DEDUCTION");
				$sheet->setCellValue('AC6', "TOTAL DEDUCTION");
				$sheet->setCellValue('AD6', "NET");
				$sheet->getStyle("A6:AD6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF808080');
				$sheet->getStyle("A6:AD6")->applyFromArray($style);

					for($h=1;$h<=4;$h++){
						$sheet->getStyle("A".$h.":Y".$h)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					}
							$row =7;
							$roww =7;
							$col=1;
							$bonus_detail="";
							$adjj_detail="";
							$ded_detail="";
							$ded_loan_detail="";
 							$bonus_total=0;
 							$adjj_total=0;
 							$ded_total=0;
 							$ded_loan_total=0;
							$TOTAL_Basic = 0;
							$TOTAL_Clothing = 0;
							$TOTAL_Laundry = 0;
							$TOTAL_Rice = 0;
							$TOTAL_ND = 0;
							$TOTAL_OTBCT = 0;
							$TOTAL_HP = 0;
							$TOTAL_HO = 0;
							$TOTAL_Allowance = 0;
							$TOTAL_Adjustment = 0;
							$TOTAL_Gross = 0;
							$TOTAL_Tax = 0;
							$TOTAL_SSS = 0;
							$TOTAL_HDMF = 0;
							$TOTAL_PHIC = 0;
							$TOTAL_UT = 0;
							$TOTAL_Deduction = 0;
							$TOTAL_Deduction_loan = 0;
							$TOTAL_Deduction_final = 0;
							$TOTAL_NET = 0;
							$TOTAL_ATM = 0;
							$TOTAL_NONATM = 0;
							$TOTAL_HOLDATM = 0;
							$newRow = 0;
						
					foreach($query->result() as $row1){
							
							$clothing = explode("(",$row1->clothing);
							$laundry = explode("(",$row1->laundry);
							$rice = explode("(",$row1->rice);
							$tax = explode("(",$row1->bir_details);
							$adjbonus = explode(",",$row1->adjust_details);
							$bonus = explode(",",$row1->bonus_details);
							$deductionz = (trim($row1->deduct_details)!="0") ? explode(",", $row1->deduct_details) : 0;
							$deductionz_loan = (trim($row1->loan_details)!="0" && trim($row1->loan_details)!="" ) ? explode(",", $row1->loan_details) : 0;

								$isATM = ($row1->isAtm == 1) ? "ATM" : "NON-ATM" ;
	 
							$sheet->setCellValue('A'.$row, $row1->id_num);
							$sheet->setCellValue('B'.$row, $row1->lname);
							$sheet->setCellValue('C'.$row, $row1->fname);
							$sheet->setCellValue('D'.$row, $row1->mname);
							$sheet->setCellValue('E'.$row, $row1->pos_name);
							$sheet->setCellValue('F'.$row, $isATM);
							$sheet->setCellValue('G'.$row, $row1->quinsina);
							$sheet->setCellValue('H'.$row, trim($clothing[0]));
							$sheet->setCellValue('I'.$row, trim($laundry[0]));
							$sheet->setCellValue('J'.$row, trim($rice[0]));
							$sheet->setCellValue('K'.$row, $row1->nd);
							$sheet->setCellValue('L'.$row, $row1->ot_bct);
							$sheet->setCellValue('M'.$row, $row1->hp);
							$sheet->setCellValue('N'.$row, $row1->ho); 
								if(count($adjbonus)>=1 and trim($row1->adjust_details)!="0"){
										for($i=0;$i<count($adjbonus);$i++){
											$adjbon = explode("|",$adjbonus[$i]);
												if(trim($adjbon[0])=="Bonus"){
													$bon = explode("=",$adjbon[1]);
													$bonus_detail .= "[".$bon[0]." = ".$bon[1]."], ";
													$bonus_total+=$bon[1]; // Total Bonus under adjust_detail
												}
										}
									}else{
										$bonus_detail .="";	
									}
									
									// echo $row1->lname."==  ".$bonus_detail."<br>";
								 
 									if(trim($row1->bonus_details)!="0"){
										for($i=0;$i<count($bonus);$i++){
													$bon = explode("=",$bonus[$i]);
													
													$bonus_detail .= "[".$bon[0]." = ".$bon[1]."], ";
													$bonus_total+=$bon[1]; // Total Bonus under bonus_detail
													
										}
									}else{
										$bonus_detail .="";	
									}
							$sheet->setCellValue('O'.$row, $bonus_detail);
							$sheet->setCellValue('P'.$row, $bonus_total);
								if(trim($row1->adjust_details)!="0"){
								if(count($adjbonus)>=1){
									for($i=0;$i<count($adjbonus);$i++){
										$adjbon = explode("|",$adjbonus[$i]);
											if(trim($adjbon[0])!="Bonus"){
												 
												$bon = explode("=",$adjbon[1]);
												$adjj_detail .= "[".$bon[0]." = ".$bon[1]."], ";
												$adjj_total+=$bon[1]; // Total Bonus under adjust_detail
												 
											}else{
											$adjj_detail .= "";
											}
									}
								}
								}
							
							$sheet->setCellValue('Q'.$row, $adjj_detail);
							$sheet->setCellValue('R'.$row, $adjj_total);
							$gross = $row1->quinsina + $clothing[0] + $laundry[0] + $rice[0] + $row1->nd + $row1->ot_bct + $row1->hp+ $row1->ho+$bonus_total+$adjj_total+$row1->ahr_amount;
							$sheet->setCellValue('S'.$row, $gross);
							$sheet->setCellValue('T'.$row, $tax[0]);
							$sheet->setCellValue('U'.$row,  $row1->sss_details);
							$sheet->setCellValue('V'.$row,  $row1->hdmf_details);
							$sheet->setCellValue('W'.$row,  $row1->phic_details);
							$sheet->setCellValue('X'.$row,  trim($row1->absentLate));
							// if($deductionz!=0){
							if($deductionz!=0 or $deductionz!="0"){
								
								 for($i=0;$i<count($deductionz);$i++){
													$ded = explode("=",$deductionz[$i]);
													$ded_detail .= "[".$ded[0]." = ".$ded[1]."], ";
													$ded_total+=$ded[1]; // Total deduction under deduct_detail
										}
							}else{
										$ded_detail ="";	
							}
							
							if($deductionz_loan!=0){
								 for($i=0;$i<count($deductionz_loan);$i++){
											$ded_loan = explode("=",$deductionz_loan[$i]);
											$ded_loan_detail .= "[".$ded_loan[0]." = ".$ded_loan[1]."], ";
											$ded_loan_total+=$ded_loan[1]; // Total deduction under deduct_detail
								 }
							}else{
									$ded_loan_detail ="";	
							}
							
							$sheet->setCellValue('Y'.$row, $ded_detail);
							$sheet->setCellValue('Z'.$row, $ded_total);
							$totalDeduction = $tax[0] +  $row1->sss_details + $row1->hdmf_details + $row1->phic_details + $row1->absentLate + $ded_total;
							
							$sheet->setCellValue('AA'.$row, $ded_loan_detail);
							$total_deduction_final = $ded_loan_total + $totalDeduction;
							$sheet->setCellValue('AB'.$row, $ded_loan_total);
							$sheet->setCellValue('AC'.$row, $total_deduction_final);
							$takehome = ((float) $row1->final > 0) ? $gross - $total_deduction_final : 0;
							$sheet->setCellValue('AD'.$row, $takehome);
							
							
							
								$sheet->getStyle("A".$roww.":N".$roww)->applyFromArray($style);
							  $sheet->getStyle("O".$roww)->applyFromArray($style_text_left);
							  $sheet->getStyle("P".$roww)->applyFromArray($style);
							  $sheet->getStyle("Q".$roww)->applyFromArray($style_text_left);
								$sheet->getStyle("R".$roww.":V".$roww)->applyFromArray($style);
								$sheet->getStyle("Y".$roww)->applyFromArray($style_text_left);
								$sheet->getStyle("Z".$roww.":AD".$roww)->applyFromArray($style);

								$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setVisible(false);
								$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setVisible(false);
								$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setVisible(false);
								$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setVisible(false);

						
						 if($this->columnLetter($col)!='O' or $this->columnLetter($col) !='Q'){
							$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
						 }else{
						 $sheet->getColumnDimension('O')->setWidth(30);
						 $sheet->getColumnDimension('Q')->setWidth(30);

						 }
						 $TOTAL_Basic += $row1->quinsina;
						 $TOTAL_Clothing += $clothing[0];
						 $TOTAL_Laundry += $laundry[0];
						 $TOTAL_Rice += $rice[0];
						 $TOTAL_ND += $row1->nd;
						 $TOTAL_OTBCT += $row1->ot_bct;
						 $TOTAL_HP += $row1->hp;
						 $TOTAL_HO += $row1->ho;
						 $TOTAL_Allowance += $bonus_total;
						 $TOTAL_Adjustment += $adjj_total;
						 $TOTAL_Gross += $gross;
						 $TOTAL_Tax += $tax[0];
						 $TOTAL_SSS += $row1->sss_details;
						 $TOTAL_HDMF += $row1->hdmf_details;
						 $TOTAL_PHIC += $row1->phic_details;
						 $TOTAL_UT += $row1->absentLate;
						 $TOTAL_Deduction += $ded_total;
						 $TOTAL_Deduction_loan += $ded_loan_total;
						 $TOTAL_Deduction_final += $total_deduction_final;
						 $TOTAL_NET += $takehome;
						
						if($row1->isAtm == 1){
							$TOTAL_ATM+=$takehome;
						}elseif($row1->isAtm == 2){
							$TOTAL_HOLDATM+=$takehome;
						}else{
							$TOTAL_NONATM+=$takehome;
						}
						
						$row++;
						$roww++;
						$col++;
						$bonus_detail="";
						$adjj_detail="";
						$ded_detail="";
						$ded_loan_detail="";
						$bonus_total=0;
						$ded_total=0;
						$ded_loan_total=0;
						$adjj_total=0;
						$takehome=0;
						$gross=0;
						$totalDeduction=0;
						$total_deduction_final=0;
					}

					 $sheet->setCellValue('G'.$row, $TOTAL_Basic);
					 $sheet->setCellValue('H'.$row, $TOTAL_Clothing);
					 $sheet->setCellValue('I'.$row, $TOTAL_Laundry);
					 $sheet->setCellValue('J'.$row, $TOTAL_Rice);
					 $sheet->setCellValue('K'.$row, $TOTAL_ND);
					 $sheet->setCellValue('L'.$row, $TOTAL_OTBCT);
					 $sheet->setCellValue('M'.$row, $TOTAL_HP);
					 $sheet->setCellValue('N'.$row, $TOTAL_HO);
					 $sheet->setCellValue('P'.$row, $TOTAL_Allowance);
					 $sheet->setCellValue('R'.$row, $TOTAL_Adjustment);
					 $sheet->setCellValue('S'.$row, $TOTAL_Gross);
					 $sheet->setCellValue('T'.$row, $TOTAL_Tax);
					 $sheet->setCellValue('U'.$row, $TOTAL_SSS);
					 $sheet->setCellValue('V'.$row, $TOTAL_HDMF);
					 $sheet->setCellValue('W'.$row, $TOTAL_PHIC);
					 $sheet->setCellValue('X'.$row, $TOTAL_UT);
					 $sheet->setCellValue('Z'.$row, $TOTAL_Deduction);
					 $sheet->setCellValue('AB'.$row, $TOTAL_Deduction_loan);
					 $sheet->setCellValue('AC'.$row, $TOTAL_Deduction_final);
					 $sheet->setCellValue('AD'.$row, $TOTAL_NET);
	 				$sheet->getStyle("A".$row.":AD".$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFEB3B');
					$sheet->getStyle("A".$row.":AD".$row)->applyFromArray($style);

					$sheet->getStyle('G'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('H'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('I'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('J'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('K'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('L'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('M'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('N'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('P'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('R'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('S'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('T'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('U'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('V'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('W'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('X'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('Z'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('AB'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('AC'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;
					$sheet->getStyle('AD'.$row)->getNumberFormat()->setFormatCode('#,##0.00');;

 				  	$row+=6;
					
					$sheet->setCellValue('B'.$row, "Prepared By:");
					$sheet->setCellValue('E'.$row, "Noted By:");
					$sheet->setCellValue('I'.$row, "Checked By:");
					$sheet->setCellValue('R'.$row, "Approved By:");
					$sheet->getStyle('B'.$row.':R'.$row)->getFont()->setBold( true );
					$sheet->getStyle('B'.$row.':R'.$row)->getFont()->setUnderline( true );
					
					$sheet->setCellValue('B'.($row+5), "ANNA MARIE S. ESTILORE");
					$sheet->setCellValue('B'.($row+7), "HR Analyst II-Compensation");
					$sheet->getStyle('B'.($row+5))->getFont()->setUnderline( true );
					
					$sheet->setCellValue('B'.($row+10), "Checked By:");
					$sheet->getStyle('B'.($row+10).':B'.($row+10))->getFont()->setBold( true );
					$sheet->getStyle('B'.($row+10).':B'.($row+10))->getFont()->setUnderline( true );
					
					$sheet->setCellValue('B'.($row+13), "RAQUEL M.JACOT");
					$sheet->setCellValue('B'.($row+15), "Assistant HR Manager");
					$sheet->getStyle('B'.($row+13))->getFont()->setUnderline( true );


					$sheet->setCellValue('E'.($row+5), "ANA BELLA CHRISTINA CHEE");
					$sheet->setCellValue('E'.($row+7), "HR Manager");
					$sheet->getStyle('E'.($row+5))->getFont()->setUnderline( true );

					$sheet->setCellValue('I'.($row+5), "DOROTHY JOY D. DAGCUTA");
					$sheet->setCellValue('I'.($row+7),  "General Accounting III");
					$sheet->getStyle('I'.($row+5))->getFont()->setUnderline( true );

					$sheet->setCellValue('I'.($row+10), "SHENDIE CHATTO-VALLENTE");
					$sheet->setCellValue('I'.($row+12), "Senior Finance Officer");
					$sheet->getStyle('I'.($row+10))->getFont()->setUnderline( true );

					$sheet->setCellValue('R'.($row+5), "DR. NIÑO MAE V. DURAN");
					$sheet->setCellValue('R'.($row+7), "Managing Director");  
					$sheet->getStyle('R'.($row+5))->getFont()->setUnderline( true );

					/****NEW SHEET***/
					$objPHPExcel->createSheet();
					$objPHPExcel->setActiveSheetIndex(1)->setTitle("SUMMARY");
					$sheet1 = $objPHPExcel->getActiveSheet();
					$sheet1->setShowGridlines(false);
							//------------------------INSERT LOGO-------------------------------------------
							 $objDrawing1 = new PHPExcel_Worksheet_Drawing();
								$objDrawing1->setName('Logo');
								$objDrawing1->setDescription('Logo');
								$objDrawing1->setPath($this->logo);
							$objDrawing1->setOffsetX(0);    // setOffsetX works properly
							$objDrawing1->setOffsetY(0);  //setOffsetY has no effect
							$objDrawing1->setCoordinates('A1');
							$objDrawing1->setHeight(92); // logo height
							$objDrawing1->setResizeProportional(true);
							$objDrawing1->setWorksheet($sheet1);
							
							//----------------------------------------------------------------------
						$sheet1->setCellValue('G1', "Payroll Period:");
						$sheet1->setCellValue('H1', $daterange);
						$sheet1->setCellValue('G2', "Year:");
						$sheet1->setCellValue('H2', $year);
						$sheet1->setCellValue('G3', "Month:");
						$sheet1->setCellValue('H3', $month);
						$sheet1->setCellValue('G4', "Period:");
						$sheet1->setCellValue('H4', $period);
						
						
						$sheet1->setCellValue('A7', "SUMMARY:");
						$sheet1->setCellValue('A8', "ATM:");
						$sheet1->setCellValue('A9', "NON-ATM:");
						$sheet1->setCellValue('A10',  "HOLD:");
						
						$sheet1->setCellValue('B8',  $TOTAL_ATM);
						$sheet1->setCellValue('B9',  $TOTAL_NONATM);
						$sheet1->setCellValue('B10',  $TOTAL_HOLDATM);
						
						$sheet1->setCellValue('A11',  "TOTAL NET PAY:");
						$sheet1->setCellValue('B11',  $TOTAL_NET);
						foreach(range('A','Z') as $columnID)
						{
							$sheet1->getColumnDimension($columnID)->setAutoSize(true);
						}					
											
							for($h=1;$h<=4;$h++){
								$sheet1->getStyle("G".$h.":I".$h)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
							}
	 				$sheet1->getStyle("A7")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFEB3B');
					$sheet1->mergeCells('A7:B7');
					for($c=7;$c<=12;$c++){
						$sheet1->getStyle('A'.$c)->getFont()->setBold( true );
						$sheet1->getStyle('B'.$c)->getNumberFormat()->setFormatCode('#,##0.00');
					}
					/****END OF NEW SHEET***/
					
			

						// die();
					
					
		$sheet->getProtection()->setSheet(true);
		$sheet->getProtection()->setPassword('pazzword');
		$sheet1->getProtection()->setSheet(true);
		$sheet1->getProtection()->setPassword('pazzword');
		
	   $file = 'PAYROLL_REPORT_'.strtoupper(trim($empzSite)).'_'.strtoupper(trim($empzType)).'_'.strtoupper(trim($empzCat)).'_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_pr'.$this->session->userdata('uid').'.xlsx';

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');	 
	 }
	
	private function atm_report($coverage_id,$atmType,$site){
	if($atmType==3){
		$andIsATM="";
	}else{
		$andIsATM=" and b.isAtm=".$atmType;
   
	}
	$append = ($site=="CDO") ? "and emp_type not like '%cebu%'" : "and emp_type like '%cebu%'";
	$query = $this->db->query("SELECT distinct(b.emp_id),lname,fname,mname,pos_name,rate,atm_account_number,isAtm,h.final FROM `tbl_applicant` a,tbl_employee b,tbl_emp_promote c,tbl_pos_emp_stat d,tbl_position e,tbl_emp_stat f,tbl_rate g,tbl_payroll h,tbl_payroll_coverage i where a.apid=b.apid and b.emp_id=c.emp_id and c.posempstat_id = d.posempstat_id and d.pos_id= e.pos_id and f.empstat_id=d.empstat_id and g.posempstat_id = d.posempstat_id and h.emp_promoteId = c.emp_promoteId and i.coverage_id=h.coverage_id and i.coverage_id=".$coverage_id." and f.status in ('Probationary','Regular') and  b.isActive='Yes' and g.isActive=1  and d.isActive=1 and c.isActive=1 ".$andIsATM." $append order by lname"); 
	return $query->result();

}
	public function exportATM_excel($payrollC,$atmType,$site){
		$objPHPExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$sheet = $objPHPExcel->getActiveSheet(0);
			$coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$payrollC);
			$year= $coverage->result()[0]->year;
			$month= $coverage->result()[0]->month; 
			$period= $coverage->result()[0]->period;
			$data["list"] = $this->atm_report($payrollC,$atmType,$site);
				$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
				$style_text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					)
				);


				$month_arr = array(
				"January" => 1,
				"February" => 2,
				"March" => 3,
				"April" => 4,
				"May" => 5,
				"June" => 6,
				"July" => 7,
				"August" => 8,
				"September" => 9,
				"October" => 10,
				"November" => 11,
				"December" => 12			
				);
			$sheet->setShowGridlines(false);
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payslip");
			
				foreach($data as $row1 => $val){
						$row =2;
						$roww =1;
						$col=1;
						$sheet->setCellValue('A1', "ATM Account Number");
						$sheet->setCellValue('B1', "LAST NAME");
						$sheet->setCellValue('C1', "FIRST NAME");
						$sheet->setCellValue('D1', "MIDDLE NAME");
						$sheet->setCellValue('E1', "TYPE");
						$sheet->setCellValue('F1', "AMOUNT");

						if($row1=="list"){
						foreach($val as $row2){
							$atm = (!empty($row2->atm_account_number) ? trim($row2->atm_account_number) : "--") ;
							if($row2->isAtm == 1){
								$isATM =  "ATM";
							}elseif($row2->isAtm == 2){
								$isATM =  "HOLD";
							}else{
								$isATM =  "NON-ATM";
							}
							// $isATM = ($row2->isAtm == 1) ? "ATM" : "NON-ATM" ;
						
						
						
 						$sheet->setCellValue('A'.$row, $atm);
						$sheet->setCellValue('B'.$row, $row2->lname);
						$sheet->setCellValue('C'.$row, $row2->fname);
						$sheet->setCellValue('D'.$row, $row2->mname);
						$sheet->setCellValue('E'.$row, $isATM);
						$sheet->setCellValue('F'.$row, trim($row2->final));
						$sheet->getStyle("A".$roww.":F".$roww)->applyFromArray($style);
						$sheet->getStyle('F'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
						$sheet->getStyle('A'.$row)->getNumberFormat()->setFormatCode('0');
						
 					$row++;
					$sheet->getStyle("A1:F1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF808080');
					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					// $sheet->getColumnDimension('A')->setWidth('17');
					// $sheet->getColumnDimension('B')->setWidth('25');
					$col++;
						}
					}
				}
	  $file = 'PAYROLL_ATM_REPORT_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_u'.$this->session->userdata('uid').'.xlsx';
      $path= "reports/payroll/$year/$month/$period/report/".$file;
 
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');	 


	}
	public function exportATM_new_excel($payrollC,$atmType,$site){
		$objPHPExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$sheet = $objPHPExcel->getActiveSheet(0);
			$coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$payrollC);
			$year= $coverage->result()[0]->year;
			$month= $coverage->result()[0]->month; 
			$period= $coverage->result()[0]->period;
			$data["list"] = $this->atm_report($payrollC,$atmType,$site);
				$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
				$style_text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					)
				);


				$month_arr = array(
				"January" => 1,
				"February" => 2,
				"March" => 3,
				"April" => 4,
				"May" => 5,
				"June" => 6,
				"July" => 7,
				"August" => 8,
				"September" => 9,
				"October" => 10,
				"November" => 11,
				"December" => 12			
				);
			$sheet->setShowGridlines(false);
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payslip");
			
				foreach($data as $row1 => $val){
						$row =2;
						$roww =1;
						$col=1;
						$sheet->setCellValue('A1', "EMPLOYEE NAME");
						$sheet->setCellValue('B1', "ACCOUNT NUMBER");
						$sheet->setCellValue('C1', "AMOUNT*");
						$sheet->setCellValue('D1', "REMARKS");
						$sheet->setCellValue('E1', "SOURCE ACCOUNT:");
						$sheet->setCellValue('F1', "");
						$sheet->setCellValue('G1', "PAYROLL DATE:");
						$sheet->setCellValue('H1', "");
						$sheet->setCellValue('I1', "PAYROLL TIME:");
						$sheet->setCellValue('J1', "");
						$sheet->setCellValue('K1', "TOTAL AMOUNT:");
						$sheet->setCellValue('L1', "");
						$sheet->setCellValue('M1', "TOTAL COUNT:");
						$sheet->setCellValue('N1', count($val));

						if($row1=="list"){
							$total = 0;
							$sheet->setCellValue('L1', $total);
						foreach($val as $row2){
							$atm = (!empty($row2->atm_account_number) ? trim($row2->atm_account_number) : "--") ;
							if($row2->isAtm == 1){
								$isATM =  "ATM";
							}elseif($row2->isAtm == 2){
								$isATM =  "HOLD";
							}else{
								$isATM =  "NON-ATM";
							}
							// $isATM = ($row2->isAtm == 1) ? "ATM" : "NON-ATM" ;
						
						
						
						$sheet->setCellValue('A'.$row, ucwords($row2->fname)." ".ucwords($row2->lname));
 						$sheet->setCellValue('B'.$row, $atm);
						$sheet->setCellValue('C'.$row, trim($row2->final));
						$sheet->getStyle("A".$roww.":D".$roww)->applyFromArray($style);
						$sheet->getStyle('C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
						$sheet->getStyle('B'.$row)->getNumberFormat()->setFormatCode('0');
						
 					$row++;
					$sheet->getStyle("A1:D1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('2F75B5');
					$sheet->getStyle("E1:E1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00B0F0');
					$sheet->getStyle("G1:G1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00B0F0');
					$sheet->getStyle("I1:I1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00B0F0');
					$sheet->getStyle("K1:K1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00B0F0');
					$sheet->getStyle("M1:M1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00B0F0');
					$sheet->getStyle("L1:L1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C9C9C9');
					$sheet->getStyle("N1:N1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C9C9C9');
					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					// $sheet->getColumnDimension('A')->setWidth('17');
					// $sheet->getColumnDimension('B')->setWidth('25');
					$total+= (float)$row2->final;
					$col++;
						}
						$sheet->setCellValue('L1',number_format($total, 2, '.', ''));
					}
				}
	  $file = 'PAYROLL_ATM_REPORT_NEW_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_u'.$this->session->userdata('uid').'.xlsx';
      $path= "reports/payroll/$year/$month/$period/report/".$file;
 
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');	 


	}
	private function payroll_account($payrollC,$empzType,$site){
		// $append = ($site=="CDO") ? "and emp_type not like '%cebu%'" : "and emp_type like '%cebu%'";
		$append = (strtoupper(trim($site))=="CDO") ? "and emp_type not like '%cebu%' and emp_type like '%".$empzType."%'" : "and emp_type like '%cebu%' and emp_type like '%".$empzType."%'";
		
		$query = $this->db->query("SELECT a.*,d.fname,d.lname,c.emp_id, (select count(*) from tbl_payroll z where  coverage_id=$payrollC and SUBSTRING_INDEX(z.account, '|', 2)=SUBSTRING_INDEX(a.account, '|', 2) $append) as headcount FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid $append and coverage_id=$payrollC group by SUBSTRING_INDEX(trim(a.account), '|', 2) order by headcount asc"); 
	 return $query->result();
	}
	private function payroll_account2($payrollC,$empzType,$site){
		$append = (strtoupper(trim($site))=="CDO") ? "and emp_type not like '%cebu%' and emp_type like '%".$empzType."%'" : "and emp_type like '%cebu%' and emp_type like '%".$empzType."%'";
		$query = $this->db->query("SELECT a.*,d.fname,d.lname,c.emp_id, (select count(*) from tbl_payroll z where coverage_id=$payrollC and z.account=a.account  $append) as headcount FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid $append and coverage_id=$payrollC"); 
	 return $query->result();
	}
	private function additionName(){
	  $query = $this->db->query("SELECT * FROM tbl_payroll_addition"); 
	  return $query->result();
	}
	private function bonuzName(){
	  $query = $this->db->query("SELECT * FROM tbl_bonus"); 
	  return $query->result();
	}
		private function bonus_tax(){
	  $query = $this->db->query("SELECT LOWER(bonus_name) as bonus_name FROM tbl_bonus where isActive=1 and isTaxable=1"); 
	  return $query->result_array();
	}
	private function additionName_tax(){
	  $query = $this->db->query("SELECT LOWER(additionname) as addition_name  FROM tbl_payroll_addition where isActive=1 and isTaxable=1"); 
	  return $query->result_array();
	}
	private function getAllDeductions(){
	  $this->db->where('isActive',1);
	  $this->db->from('tbl_payroll_deductions');
	  return $this->db->get()->result();
	}
	private function deduct_details($dname,$account,$emp_type,$cid,$site,$empzType){
		$append = (strtoupper(trim($site))=="CDO") ? "and emp_type not like '%cebu%' and emp_type like '%".$empzType."%'" : "and emp_type like '%cebu%' and emp_type like '%".$empzType."%'";

		$query = $this->db->query("SELECT a.deduct_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   deduct_details like '%$dname%' and account = '$account'  $append");
		$total=0;
		  foreach ($query->result() as $row){
				$deduct = explode(",",$row->deduct_details);	
				  for($i=0;$i<count($deduct);$i++){
					$deduct1 = explode("=",$deduct[$i]);	
						  if(trim($deduct1[0])==$dname){
							$total += $deduct1[1];
						  }
				  }
		  }
			  return $total;
		}
		private function OT_details($dname,$account,$emp_type,$cid,$site,$empzType){
		$append = (strtoupper(trim($site))=="CDO") ? "and emp_type not like '%cebu%' and emp_type like '%".$empzType."%'" : "and emp_type like '%cebu%' and emp_type like '%".$empzType."%'";
	
		$query = $this->db->query("SELECT sum(ahr_amount) as sum FROM  tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and account = '$account'  $append ");
			return $query->result()[0]->sum;
		}
		private function add_details($dname,$account,$emp_type,$cid,$site,$empzType){
		$append = (strtoupper(trim($site))=="CDO") ? "and emp_type not like '%cebu%' and emp_type like '%".$empzType."%'" : "and emp_type like '%cebu%' and emp_type like '%".$empzType."%'";
	
		$query = $this->db->query("SELECT a.adjust_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   adjust_details like '%$dname%' and account = '$account'  $append ");
		$total=0;
		  foreach ($query->result() as $row){
				$added = explode(",",$row->adjust_details);	
				  for($i=0;$i<count($added);$i++){
				  $add = explode("|",$added[$i]);
					$add1 = explode("=",$add[1]);	
						  if(trim($add1[0])==$dname){
							$total += $add1[1];
						  }
				  }
		  }
			  return $total."-SELECT a.adjust_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   adjust_details like '%$dname%'and account = '$account' $append ";
		}
		private function bonus_details($bname,$account,$emp_type,$cid,$site,$empzType){
		$append = (strtoupper(trim($site))=="CDO") ? "and emp_type not like '%cebu%' and emp_type like '%".$empzType."%'" : "and emp_type like '%cebu%' and emp_type like '%".$empzType."%'";

		$query = $this->db->query("SELECT a.bonus_details FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid and   bonus_details like '%$bname%' and account = '$account' $append ");
		$total=0;
		  foreach ($query->result() as $row){
				$deduct = explode(",",$row->bonus_details);	
				  for($i=0;$i<count($deduct);$i++){
					$deduct1 = explode("=",$deduct[$i]);	
						  if(trim($deduct1[0])==$bname){
							$total += $deduct1[1];
						  }
				  }
		  }
			  return $total;
		}
	private function payrolled($bname,$account,$emp_type,$cid,$site,$empzType){
	$append = (strtoupper(trim($site))=="CDO") ? "and emp_type not like '%cebu%' and emp_type like '%".$empzType."%'" : "and emp_type like '%cebu%' and emp_type like '%".$empzType."%'";

		$query = $this->db->query("SELECT a.".$bname."  FROM tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and coverage_id=$cid  and account = '$account' $append");
		$total=0;
		  foreach ($query->result() as $row){
 						   if(trim($bname)=="bir_details"){
							$bir=explode("(",$row->$bname);
								$total += number_format($bir[0]);
							}else{
								$total += $row->$bname;

							}
						  
 		  }
			  return $total;
		}
	function export_payroll_summary($payrollC,$empzType,$site){  
		$objPHPExcel = $this->phpexcel;
		$objDrawing = new PHPExcel_Worksheet_Drawing();
 		$sheet = $objPHPExcel->getActiveSheet(0);
			$coverage = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$payrollC);
			$year= $coverage->result()[0]->year;
			$month= $coverage->result()[0]->month; 
			$period= $coverage->result()[0]->period;
			$daterange= $coverage->result()[0]->daterange;

			$data["list_account"] = $this->payroll_account($payrollC,$empzType,$site);
			
			$data["list_account2"] = $this->payroll_account2($payrollC,$empzType,$site);
			$data["deduction_name"] = $this->getAllDeductions();
			$data["adittion_name"] = $this->additionName();
			$data["bonuz_name"] = $this->bonuzName();
			$data["other_head"] = array("QUINSINA","CLOTHING","LAUNDRY","RICE","OT-BCT","NIGHT PREMIUM","HAZARD PAY","HOLIDAY","SSS","PHIC","HDMF","BIR");
			$other = array("quinsina","clothing","laundry","rice","ot_bct","nd","hp","ho","sss_details","phic_details","hdmf_details","bir_details");
				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Logo');
				$objDrawing->setPath($this->logo);
			$objDrawing->setOffsetX(0);    // setOffsetX works properly
			$objDrawing->setOffsetY(0);  //setOffsetY has no effect
			$objDrawing->setCoordinates('A1');
			$objDrawing->setHeight(92); // logo height
			// $objDrawing->setWidth(320); // logo width
			// $objDrawing->setWidthAndHeight(200,400);
			$objDrawing->setResizeProportional(true);
			$objDrawing->setWorksheet($sheet);

				$styleArray = array(
					'font' => array(
						'bold' => true
					)
				);
				$fontStyle = [
					'font' => [
						'size' => 8
					]
				];
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
				);
				$style_text_left = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					)
				);

			$sheet->setShowGridlines(false);
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(0)->setTitle("Summary_".$year."_".$month."_".$period);
			$headercol=3;
			$row=7;
			$inc=6;
			$headerrow=6;
 				$sheet->setCellValue('C1', "Payroll Period:");
				$sheet->setCellValue('D1', $daterange);
				$sheet->setCellValue('C2', "Year:");
				$sheet->setCellValue('D2', $year);
				$sheet->setCellValue('C3', "Month:");
				$sheet->setCellValue('D3', $month);
				$sheet->setCellValue('C4', "Period:");
				$sheet->setCellValue('D4', $period);

				$sheet->setCellValue('A6',"DEPARTMENT");
				$sheet->setCellValue('B6', "ACCOUNTS");
				$sheet->setCellValue('C6', "HEADCOUNT");
				$sheet->getStyle("A6:C6")->applyFromArray($style);

				for($i=0;$i<count($data["other_head"]);$i++){
					$sheet->setCellValueByColumnAndRow($headercol, $headerrow,$data["other_head"][$i]);
					$headercol++;
					$sheet->getStyle($this->columnLetter($headercol).''.($inc).':'.$this->columnLetter($headercol).''.($inc))->applyFromArray($style);
                                                                                                      
				}
 				foreach($data["adittion_name"] as $row_adittion){
					$sheet->setCellValueByColumnAndRow($headercol, $headerrow,$row_adittion->additionname);
					$headercol++;
					$sheet->getStyle($this->columnLetter($headercol).''.($inc).':'.$this->columnLetter($headercol).''.($inc))->applyFromArray($style);

				}
				foreach($data["bonuz_name"] as $row_bonuz){
					$sheet->setCellValueByColumnAndRow($headercol, $headerrow,$row_bonuz->bonus_name);
					$headercol++;
					$sheet->getStyle($this->columnLetter($headercol).''.($inc).':'.$this->columnLetter($headercol).''.($inc))->applyFromArray($style);

				}
				foreach($data["deduction_name"] as $row_deduct){
					$sheet->setCellValueByColumnAndRow($headercol, $headerrow,$row_deduct->deductionname);
					$headercol++;
					$sheet->getStyle($this->columnLetter($headercol).''.($inc).':'.$this->columnLetter($headercol).''.($inc))->applyFromArray($style);
 				}
						
						$head1=3;
						$head2=15;
						$head3=34;
						$head3=34;
						$head4=42;
						$headrow1=7;
						$headrow2=7;
						$headrow3=7;
						$headrow4=7;
						$inc=7;
						 
					foreach($data as $row1 => $val){
								if($row1=="list_account"){
									foreach($val as $row2){
										
										$account = explode("|",$row2->account);
										$depName = $this->db->query("SELECT dep_details FROM tbl_account a, tbl_department b where a.dep_id=b.dep_id and acc_id=".$account[0]);
										$sheet->setCellValue('A'.$row,$depName->result()[0]->dep_details);
										$sheet->setCellValue('B'.$row,$account[1]);
										$sheet->setCellValue('C'.$row,$row2->headcount);
										$sheet->getStyle("A".$row.":C".$row)->applyFromArray($style);

										$row++;

									}
									foreach($val as $row2){
											for($i=0;$i<count($other);$i++){
												$typ = explode("-",$row2->emp_type);
												$accz = explode("|",$row2->account);
											
												$dd = $this->payrolled($other[$i],trim($row2->account),$typ[0],$row2->coverage_id,$site,$empzType);
												// $dd = $this->payroll($other[$i],$row2->account,$typ[0],$row2->coverage_id);
											
													$sheet->setCellValueByColumnAndRow($head1, $headrow1,number_format($dd,2,".",","));
												$head1++;
												$sheet->getStyle($this->columnLetter($head1).''.($inc).':'.$this->columnLetter($head1).''.($inc))->applyFromArray($style);
											}
										$head1=3;
										$headrow1++;
										$inc++;
									 }								 
								 }
								 $inc=7;
								 if($row1=="list_account"){
									 		foreach ($val as $row2){
												foreach($data["adittion_name"] as $row_adittion){
												$typ = explode("-",$row2->emp_type);
												$accz = explode("|",$row2->account);
												$add = $this->add_details($row_adittion->additionname,trim($row2->account),$typ[0],$row2->coverage_id,$site,$empzType);
												$add_OT_details = $this->OT_details($row_adittion->additionname,trim($row2->account),$typ[0],$row2->coverage_id,$site,$empzType);
												 $rs = explode("-",$add);
												 $amt_add = $rs[0];
												 if(trim($row_adittion->additionname)=="Overtime"){
													 $amt_add +=$add_OT_details;
												 }
													// $sheet->setCellValueByColumnAndRow($head2, $headrow2,number_format($amt_add,2,".",","));
													$sheet->setCellValueByColumnAndRow($head2, $headrow2,$amt_add);
												$head2++;
												$sheet->getStyle($this->columnLetter($head2).''.($inc).':'.$this->columnLetter($head2).''.($inc))->applyFromArray($style);

												}
											$head2=count($other)+3;
											$headrow2++;
											$inc++;
											}
											$inc=7;
											foreach ($val as $row2){
												foreach($data["bonuz_name"] as $row_bonuz){
												$typ = explode("-",$row2->emp_type);
												$accz = explode("|",$row2->account);
												$bon = $this->bonus_details($row_bonuz->bonus_name,trim($row2->account),$typ[0],$row2->coverage_id,$site,$empzType);
												 
													$sheet->setCellValueByColumnAndRow($head3, $headrow3,number_format($bon,2,".",","));
												$head3++;
												$sheet->getStyle($this->columnLetter($head3).''.($inc).':'.$this->columnLetter($head3).''.($inc))->applyFromArray($style);

												}
											$head3=count($data["adittion_name"])+$head2;
											$headrow3++;
											$inc++;

											}  
											$inc=7;
											foreach ($val as $row2){
												foreach($data["deduction_name"] as $row_deduct){
												$typ = explode("-",$row2->emp_type);
												$accz = explode("|",$row2->account);
												$deduc = $this->deduct_details($row_deduct->deductionname,trim($row2->account),$typ[0],$row2->coverage_id,$site,$empzType);
												 
													$sheet->setCellValueByColumnAndRow($head4, $headrow4,number_format($deduc,2,".",","));
												$head4++;
												$sheet->getStyle($this->columnLetter($head4).''.($inc).':'.$this->columnLetter($head4).''.($inc))->applyFromArray($style);

												}
											$head4=count($data["bonuz_name"])+$head3;
											$headrow4++;
											$inc++;

											}  
											$inc=7;
											
								 }
							
						 
					}
						
 						$ending = count($other)+count($data["adittion_name"])+count($data["bonuz_name"])+count($data["deduction_name"])+3;
						for($i=1;$i<=$ending;$i++){
						  $sheet->getStyle($this->columnLetter($i).'6')->getFont()->setBold( true );
						  $sheet->getStyle($this->columnLetter($i).'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFEB3B');
						  $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						  $sheet->getStyle("B".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						  $sheet->getColumnDimension($this->columnLetter($i))->setAutoSize(true);
						}
							for($h=1;$h<=4;$h++){
								$sheet->getStyle("D".$h.":F".$h)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
							    $sheet->getStyle('D'.$h)->getFont()->setBold( true );
	
							}
 				/*  foreach($data["list_account2"] as $row1 => $val){
							foreach ($val as $row2){
								foreach($data["adittion_name"] as $row_adittion){
								$typ = explode("-",$row2->emp_type);
								$add = $this->add_details($row_adittion->additionname,$row2->account,$typ[0],$row2->coverage_id);
								 
									$sheet->setCellValueByColumnAndRow($head2, $headrow2,number_format($add,2,".",","));
								$head2++;
								  
							}
							$head2=2;
							$headrow2++;
						
							}  
					
					} */
		  $sheet->getProtection()->setSheet(true);
		$sheet->getProtection()->setPassword('pazzword');

 	 $file = 'PAYROLL_SUMMARY_REPORT_'.strtoupper($empzType).'_'.strtoupper($year).'_'.strtoupper($month).'_'.$period.'_u'.$this->session->userdata('uid').'_'.$site.'.xlsx';
      $path= "reports/payroll/$year/$month/$period/report/".$file;

	header('Content-Type: application/vnd.ms-excel'); //mime type
	header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
	header('Cache-Control: max-age=0'); //no cache
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');    
		 
	}
	public function taxReport(){
		 $month = $this->input->post("month");
		 $year = $this->input->post("year");
		 $mo = explode("-",$month);

		$data["emp"] = $this->tax_contri($mo,0,$year);
			$arr= array();
			foreach($data["emp"] as $row){
						$arr["test"][$row->emp_id][$row->month][$row->payroll_id] = array(
							"lname" => $row->lname,
							"fname" => $row->fname,
							"mname" => $row->mname,
							"quinsina" => $row->quinsina,
							"sss" => $row->sss_details,
							"phic" => $row->phic_details,
							"hdmf" => $row->hdmf_details,
							"bir" => $row->bir_details,
							"period" => $row->period,
							"emp_promoteId" => $row->emp_promoteId,
							"salarymode" => $row->salarymode,
							"isDaily" => $row->isDaily,
							"id_num" => $row->id_num
						); 
 				}
				
			echo (count($arr)>0) ? json_encode($arr) : 0;
	 }
	 
	 
	
	 public function exportbirReport_excel($month,$year,$site){
			 $cid = $this->input->post("cid");
			 $cidYear = $this->input->post("cidYear");
			$objPHPExcel = $this->phpexcel;
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$type2= "report";
			$sheet = $objPHPExcel->getActiveSheet(0);

			$styleArray = array(
				'font' => array(
					'bold' => true
				)
			);
			$fontStyle = [
				'font' => [
					'size' => 8
				]
			];
			$style = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
			);
			$style_text_left = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
			);
			$text_left = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				) 
			);
			$text_left_BOLD = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'font' => array(
					'bold' => true
				)
			);



				$sheet->setShowGridlines(false);
				$objPHPExcel->createSheet();
				$objPHPExcel->setActiveSheetIndex(0)->setTitle("Payroll");
				
				
				
				$sheet->setCellValue('A1',"FBC BUSINESS SOLUTIONS");
				$sheet->setCellValue('A2',"Tax computation");
				$sheet->setCellValue('J2',"Taxable income");
				$sheet->setCellValue('L2',"Tax");
				$sheet->setCellValue('A3',"ID Number");
				$sheet->setCellValue('B3',"FullName");
				$sheet->setCellValue('C3',"Middle Name");
				$sheet->setCellValue('D3',"Birthday");
				$sheet->setCellValue('E3',"Civil status");
				$sheet->setCellValue('F3',"Address");
				$sheet->setCellValue('G3',"TIN");
				$sheet->setCellValue('H3',"13th month pay");
				
				$sheet->setCellValue('I3',"Status");
				$sheet->setCellValue('J3',"Relationship");
				$sheet->setCellValue('K3',"Basic Salary");
				$sheet->setCellValue('L3',"SSS");
				$sheet->setCellValue('M3',"HDMF");
				$sheet->setCellValue('N3',"PHIC");
				$sheet->setCellValue('O3',"Taxable income");
				$sheet->setCellValue('P3',"Non-taxable income");
				$sheet->setCellValue('Q3',"15th");
				$sheet->setCellValue('R3',"30th");
				$sheet->setCellValue('S3',"15TH");
				$sheet->setCellValue('T3',"30th");
				$sheet->setCellValue('U3'," ");
				$mo = explode("-",$month);
				$data["emp"] = $this->tax_contri($mo,0,$year);
				$roww=4;
					/* foreach($data["emp"] as $row){
						$data["list"]= $this->tax_contri($mo,$row->emp_promoteId,$year);
						foreach($data["list"] as $row2){
							  $arr[$row->emp_id][$row2->period] = array(
								"lname" => $row->lname,
								"fname" => $row->fname,
								"mname" => $row->mname,
								"quinsina" => $row2->quinsina,
								"sss" => $row2->sss_details,
								"phic" => $row2->phic_details,
								"hdmf" => $row2->hdmf_details,
								"bir" => $row2->bir_details,
								"period" => $row2->period,
								"emp_promoteId" => $row2->emp_promoteId,
								"salarymode" => $row2->salarymode,
								"isDaily" => $row2->isDaily
							);   
						}
					} */
					
					foreach($data["emp"] as $row){
							$arr[$row->emp_id][$row->month][$row->payroll_id] = array(
								"lname" => $row->lname,
								"fname" => $row->fname,
								"mname" => $row->mname,
								"birthday" => $row->birthday,
								"address" => $row->address,
								"tin" => $row->bir,
								"civil" => $row->civil,
								"quinsina" => $row->quinsina,
								"sss" => $row->sss_details,
								"phic" => $row->phic_details,
								"hdmf" => $row->hdmf_details,
								"bir" => $row->bir_details,
								"period" => $row->period,
								"emp_promoteId" => $row->emp_promoteId,
								"salarymode" => $row->salarymode,
								"isDaily" => $row->isDaily,
								"id_num" => $row->id_num
							); 
					}
 					  // echo json_encode($arr);
					
					
					  // exit();
					$salary=0;
					$total_salary=0;
					$taxable=0;
					$ntaxable=0;
					$total_sss=0;
					$total_hdmf=0;
					$total_phic=0;
					$total_taxable=0;
					$total_ntaxable=0;
					$total_J=0;
					$total_K=0;
					$total_L=0;
					$total_M=0;
					$total_ntaxable_daily=0;
					foreach($arr as $row => $val){
							$salary=0;
							$sss_details=0;
							$hdmf_details=0;
							$phic_details=0;
							$bir=0;
 								foreach($val as $row2 => $val2){
									
									foreach($val2 as $row3 => $val3){
										$lname= $val3["lname"];
										$fname= $val3["fname"];
										$mname= $val3["mname"];
										$birthday= $val3["birthday"];
										$tin= $val3["tin"];
										$civil= $val3["civil"];
										$address= $val3["address"];
										$quinsina= $val3["quinsina"];
										$sss_details+= $val3["sss"];
										$hdmf_details+= $val3["hdmf"];
										$phic_details+= $val3["phic"];
										$bir2=explode("(",$val3["bir"]);
										$bir+= (int)$bir2[0];
										$isDaily=$val3["isDaily"];
										$salary+=$val3["quinsina"];
										$ar_count = count($val);
										$period = $val3["period"];
										$id_num = $val3["id_num"];
										$isDaily = ($val3["salarymode"]=="Daily") ? "1 month period" : "-";

									}
								}
							
							if($bir!=0){
								$ntaxable=$sss_details + $hdmf_details + $phic_details;
							}else{
								$ntaxable=$salary + $sss_details + $hdmf_details + $phic_details;
							}
							$taxable= ($bir!=0) ? ($salary-$ntaxable) : 0.00;
							$p1 = ($period==1) ? $taxable : "0.00";
							$p2 = ($period==2) ? $taxable : "0.00";
							$t1 = ($period==1) ? $bir : "0.00";
							$t2 = ($period==2) ? $bir : "0.00";
						  $sheet->setCellValue('A'.$roww,$id_num);
						  $sheet->setCellValue('B'.$roww,$lname.", ".$fname);
							$sheet->setCellValue('C'.$roww,$mname);
							$sheet->setCellValue('D'.$roww,$birthday);
							$sheet->setCellValue('E'.$roww,$civil);
							$sheet->setCellValue('F'.$roww,$address);
							$sheet->setCellValue('G'.$roww,$tin);
							$sheet->setCellValue('H'.$roww,"-");
							
							$sheet->setCellValue('I'.$roww,"-");
							$sheet->setCellValue('J'.$roww,"-");
							$sheet->setCellValue('K'.$roww,$salary);
							$sheet->setCellValue('L'.$roww,$sss_details);
							$sheet->setCellValue('M'.$roww,$hdmf_details);
							$sheet->setCellValue('N'.$roww,$phic_details);
							$sheet->setCellValue('O'.$roww,$taxable);
							$sheet->setCellValue('P'.$roww,$ntaxable);
							/* if($ar_count==2){ */
								$sheet->setCellValue('Q'.$roww,$taxable);
								$sheet->setCellValue('R'.$roww,$taxable);
								$sheet->setCellValue('S'.$roww,$bir);
								$sheet->setCellValue('T'.$roww,$bir);
									$total_J+=$taxable;
									$total_K+=$taxable;
									$total_L+=$bir;
									$total_M+=$bir;
							/* }else{
								$sheet->setCellValue('J'.$roww,$p1);
								$sheet->setCellValue('K'.$roww,$p2);
								$sheet->setCellValue('L'.$roww,$t1);
								$sheet->setCellValue('M'.$roww,$t2);
									$total_J+=$p1;
									$total_K+=$p2;
									$total_L+=$t1;
									$total_M+=$t2;
							} */
							$sheet->setCellValue('U'.$roww,$isDaily);
							$sheet->getStyle("A".$roww.":U".$roww)->applyFromArray($style_text_left);
							$sheet->getStyle("A3:U3")->applyFromArray($style);
							$sheet->getStyle("A3:U3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('7b7b7b');
							$sheet->getStyle("K".$roww.":T".$roww)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

							foreach(range('A','Z') as $columnID){
								$sheet->getColumnDimension($columnID)->setAutoSize(true);
							}
							$roww++; 
							$total_salary+=$salary;
							$total_sss+=$sss_details;
							$total_hdmf+=$hdmf_details;
							$total_phic+=$phic_details;
							$total_taxable+=$taxable;
							$total_ntaxable+=$ntaxable;
							$total_ntaxable_daily += ($isDaily==1) ? $ntaxable :0;
							$salary=0;
							$taxable=0;
							$ntaxable=0;
					}
						$sheet->getStyle("A".$roww.":S".$roww)->applyFromArray($text_left_BOLD);

						$sheet->setCellValue('K'.$roww,$total_salary);
						$sheet->setCellValue('L'.$roww,$total_sss);
						$sheet->setCellValue('M'.$roww,$total_hdmf);
						$sheet->setCellValue('N'.$roww,$total_phic);
						$sheet->setCellValue('O'.$roww,$total_taxable);
						$sheet->setCellValue('P'.$roww,$total_ntaxable);
						$sheet->setCellValue('Q'.$roww,$total_J);
						$sheet->setCellValue('R'.$roww,$total_K);
						$sheet->setCellValue('S'.$roww,$total_L);
						$sheet->setCellValue('T'.$roww,$total_M);
						$sheet->getStyle('K'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('L'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('M'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('N'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('O'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('P'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('Q'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('R'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('S'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('T'.$roww)->getNumberFormat()->setFormatCode('#,##0.00');

						$sheet->setCellValue('I'.($roww+4),"Non Taxable Compensation");
						$sheet->setCellValue('J'.($roww+4),"Taxable Compensation");
						$sheet->setCellValue('K'.($roww+4),"Total Compensation");
						$sheet->setCellValue('M'.($roww+4),"Tax Payable");
						
						$sheet->setCellValue('I'.($roww+5),$total_ntaxable);
						$sheet->setCellValue('J'.($roww+5),($total_J+$total_K));
						$sheet->setCellValue('K'.($roww+5),$total_ntaxable +($total_J+$total_K));
						$sheet->setCellValue('M'.($roww+5),($total_L+$total_M));
						$sheet->getStyle('I'.($roww+5))->applyFromArray($text_left);
						$sheet->getStyle('J'.($roww+5))->applyFromArray($text_left);
						$sheet->getStyle('K'.($roww+5))->applyFromArray($text_left);
						$sheet->getStyle('M'.($roww+5))->applyFromArray($text_left);
						$sheet->getStyle('I'.($roww+5))->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('J'.($roww+5))->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('K'.($roww+5))->getNumberFormat()->setFormatCode('#,##0.00');
						$sheet->getStyle('M'.($roww+5))->getNumberFormat()->setFormatCode('#,##0.00');
							
						
						$sheet->setCellValue('H'.($roww+7),"MWE");
						$sheet->setCellValue('H'.($roww+8),"Other NTC");
						$sheet->setCellValue('I'.($roww+7),$total_ntaxable_daily);
						$sheet->setCellValue('I'.($roww+8),($total_ntaxable-$total_ntaxable_daily));
						$sheet->setCellValue('I'.($roww+9),$total_ntaxable);
							$sheet->getStyle('I'.($roww+7))->applyFromArray($text_left);						
							$sheet->getStyle('I'.($roww+8))->applyFromArray($text_left);						
							$sheet->getStyle('I'.($roww+9))->applyFromArray($text_left);						
							$sheet->getStyle('I'.($roww+7))->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('I'.($roww+8))->getNumberFormat()->setFormatCode('#,##0.00');
							$sheet->getStyle('I'.($roww+9))->getNumberFormat()->setFormatCode('#,##0.00');

				$file = 'PAYROLL_BIR_REPORT_'.$cidYear.'_'.$cid.'_bir'.$this->session->userdata('uid').'.xlsx';
				$path= "reports/payroll/".$file;
				header('Content-Type: application/vnd.ms-excel'); //mime type
				header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
				header('Cache-Control: max-age=0'); //no cache
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objWriter->save('php://output');	 
		 }
	/*  public function tax_contri($month,$emp_promoteId=0,$year){
		$empP = ($emp_promoteId!=0) ? 'and c.emp_promoteId='.$emp_promoteId : '';
	   $query = $this->db->query("SELECT payroll_id,c.emp_promoteId,quinsina,sss_details,phic_details,hdmf_details,bir_details,lname,fname,mname,period,d.emp_id,salarymode, a.isDaily,month,e.birthday,e.civilStatus as civil,e.presentAddress as address,d.bir,d.id_num from tbl_payroll a,tbl_payroll_coverage b,tbl_emp_promote c,tbl_employee d,tbl_applicant e where a.coverage_id=b.coverage_id and c.emp_promoteId=a.emp_promoteId and c.emp_id=d.emp_id and e.apid=d.apid and month in ( '" . implode($month, "', '") . "' ) and year='".$year."' $empP order by lname"); 
	   // $query = $this->db->query("SELECT payroll_id,c.emp_promoteId,quinsina,sss_details,phic_details,hdmf_details,bir_details,lname,fname,mname,period,d.emp_id,salarymode, a.isDaily,month,e.birthday,e.civilStatus as civil,e.presentAddress as address,d.bir,d.id_num from tbl_payroll a,tbl_payroll_coverage b,tbl_emp_promote c,tbl_employee d,tbl_applicant e where a.coverage_id=b.coverage_id and c.emp_promoteId=a.emp_promoteId and c.emp_id=d.emp_id and e.apid=d.apid and month in ( '" . implode($month, "', '") . "' ) and d.emp_id in (1637,1676) and  year='".$year."' $empP order by lname");    //--> Robert Sanchez and Lapira
	  
	  return $query->result();
	} */
	public function payroll_cover_details($cid){
 		$fields = "daterange,transact_date,period";
        $where = "coverage_id in (".$cid.")";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_coverage");
		
		echo json_encode($record);
	}
	 public function exportGovBenefits_payroll(){
		 $cid  = $this->input->post("coverage");
		 $coverageArr = implode(",",$cid);
		 $mode = $this->input->post("mode");
		 $site = $this->input->post("site");
		 $objPHPExcel = $this->phpexcel;
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$type2= "report";
			$sheet = $objPHPExcel->getActiveSheet(0);
			//------------------------INSERT LOGO-------------------------------------------
			 $objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Logo');
				$objDrawing->setPath($this->logo);
			$objDrawing->setOffsetX(0);    // setOffsetX works properly
			$objDrawing->setOffsetY(0);  //setOffsetY has no effect
			$objDrawing->setCoordinates('A1');
			$objDrawing->setHeight(92); // logo height
		  $objDrawing->setWidth(220); // logo width
			// $objDrawing->setWidthAndHeight(200,400);
			$objDrawing->setResizeProportional(true);
			$objDrawing->setWorksheet($sheet);
			
			//----------------------------------------------------------------------

			$styleArray = array(
				'font' => array(
					'bold' => true
				)
			);
			$fontStyle = [
				'font' => [
					'size' => 8
				]
			];
			$style = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
			);
			$style_text_left = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
			);
			$text_left = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				) 
			);
			$text_left_BOLD = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'font' => array(
					'bold' => true
				)
			);
			$border_style= array(
				  'borders' => array(
					  'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
					  )
				  )
			  );

			$sheet->setShowGridlines(false);
			$objPHPExcel->createSheet();

			if($mode==1){ //SSS ---------------------------------------------------------------------------------->>>>>>>>>>
				$type = "SSS";
				$row = 6;
				$sheet->setCellValue('A'.$row,"SQ. NO.");
				$sheet->setCellValue('B'.$row,"FAMILY NAME");
				$sheet->setCellValue('C'.$row,"FIRST NAME");
				$sheet->setCellValue('D'.$row,"GROSS PAY");
				$sheet->setCellValue('E'.$row,"SSS PREM. (EE SHARE)");
				$sheet->setCellValue('F'.$row,"SSS PREM. (EE SHARE)");
				$sheet->setCellValue('G'.$row,"TOTAL");
				$sheet->setCellValue('G7',"EE SHARE");
				$sheet->setCellValue('H'.$row,"SSS PREM.");
				$sheet->setCellValue('H7',"EMPLOYER SHARE");
				$sheet->setCellValue('I'.$row,"GRAND TOTAL");
				$sheet->setCellValue('I7'," SSS PREM.");
				$sheet->setCellValue('J'.$row,"ECC");
				$coverage_details = $this->payroll_cover_details($coverageArr);
				$sss_report = $this->sss_report_premium($coverageArr);
				
				$sheet->mergeCells("A6:A7");              
				$sheet->mergeCells("B6:B7");              
				$sheet->mergeCells("C6:C7");              
				$sheet->mergeCells("D6:D7");              
				$sheet->mergeCells("J6:J7");              

				$row += 2;
				if(count($cid)==1){
					$sheet->setCellValue('E7',date('d-F', strtotime($coverage_details[0]->transact_date. ' -1 days')));
				}else{
					$sheet->setCellValue('E7',date('d-F', strtotime($coverage_details[0]->transact_date. ' -1 days')));
					$sheet->setCellValue('F7',date('d-F', strtotime($coverage_details[1]->transact_date. ' -1 days')));
				}
				
				$col=1; 
				$roww=6;				
				$i=1;		
				$sheet->getStyle("A6:J6")->applyFromArray($border_style);
				$sheet->getStyle("A7:J7")->applyFromArray($border_style);
				$arrSSS = Array();		
				foreach($sss_report as $row2){
					$arrSSS[$row2->lname."-".$row2->emp_promoteId][$row2->coverage_id] = array(
						"fname" => ucwords($row2->fname),
						"lname" => ucwords($row2->lname),
						"coverage_id" => $row2->coverage_id,
						"daterange" => $row2->daterange,
						"employer_sss" => $row2->employer_sss,
						"month" => $row2->month,
						"monthly" => $row2->monthly,
						"sss" => $row2->sss, 
						"sss_details" => $row2->sss_details, 
						"year" => $row2->year,
						"isDaily" => $row2->isDaily,
						"quinsina" => $row2->quinsina,
					);
				}
				foreach($arrSSS as $rr => $val){
					$sheet->setCellValue('A'.$row, $i);
					$ee0 = 0;
					$ee1 = 0;
					$monthly =0;
					$employer_sss =0;
					foreach($val as $rr1 => $val1){
						
						$lname= $val1["lname"];
						$fname= $val1["fname"];
						$employer_sss+= $val1["employer_sss"];
						if($val1["isDaily"]){
							// if(count($cid)>1){
								$monthly = $val1["quinsina"]*2;
							// }else{
								// $monthly += $val1["quinsina"];
							// }
							 
						}else{
							$monthly= $val1["monthly"];
						}
						
						if(($val1["coverage_id"]==$cid[0])){
							$ee0 = 	$val1["sss_details"];
						}
						if(isset($cid[1]) && ($val1["coverage_id"]==$cid[1])){
							$ee1 = 	$val1["sss_details"];
						}else{
							$ee1 = 0;
						}
 						
					} 
					$totalEEshare = $ee0 + $ee1 ;
					$ec = ((float)$monthly >=15000) ? 30 : 10;
					$sheet->setCellValue('B'.$row, $lname);
					$sheet->setCellValue('C'.$row, $fname); 
					$sheet->setCellValue('D'.$row, $monthly);
					$sheet->setCellValue('E'.$row, $ee0);
					$sheet->setCellValue('F'.$row, $ee1);
					$sheet->setCellValue('G'.$row, $totalEEshare);
					$sheet->setCellValue('H'.$row, $employer_sss);
					$sheet->setCellValue('I'.$row, $totalEEshare+$employer_sss);
					$sheet->setCellValue('J'.$row, $ec);
 						// $ss = (float)$row2->employer_sss+(float)$row2->sss_details;
						// $ec = ((float)$row2->monthly >=15000) ? 30 : 10;
						// $total = $ss+$ec;
						  
						  
						// $sheet->setCellValue('C'.$row, $row2->fname);
						// $sheet->setCellValue('D'.$row,"gross");
					$sheet->getStyle("A".$row.":J".$row)->applyFromArray($border_style);	
					$row++;
					$i++;

					$sheet->getStyle("A6:J6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDCDCDC');
					$sheet->getStyle("A7:J7")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDCDCDC');
					

					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					$col++;
					
				}
			}else if($mode==2){ //PHIC ---------------------------------------------------------------------------->>>>>>>>>>
				$type = "PHIC";
				$row = 6;
				$sheet->setCellValue('A'.$row,"SQ. NO.");
				$sheet->setCellValue('B'.$row,"FAMILY NAME");
				$sheet->setCellValue('C'.$row,"FIRST NAME");
				$sheet->setCellValue('D'.$row,"GROSS PAY");
				$sheet->setCellValue('E'.$row,"PHIC PREM. (EE SHARE)");
				$sheet->setCellValue('F'.$row,"PHIC PREM. (EE SHARE)");
				$sheet->setCellValue('G'.$row,"GRAND TOTAL");
				$sheet->setCellValue('G7',"EE SHARE");
				$sheet->setCellValue('H'.$row,"PHIC PREM. (ER SHARE)");
 				$sheet->setCellValue('I'.$row,"PHIC PREM (ER SHARE)");
				$sheet->setCellValue('J'.$row,"GRAND TOTAL");
				$sheet->setCellValue('J7',"ER SHARE");
				
				$sheet->mergeCells("A6:A7");              
				$sheet->mergeCells("B6:B7");              
				$sheet->mergeCells("C6:C7");              
				$sheet->mergeCells("D6:D7");    
				$coverage_details = $this->payroll_cover_details($coverageArr);
				$phic_report = $this->phic_report_premium($coverageArr);
				
				if(count($cid)==1){
					$sheet->setCellValue('E7',date('d-F', strtotime($coverage_details[0]->transact_date. ' -1 days')));
					$sheet->setCellValue('H7',date('d-F', strtotime($coverage_details[0]->transact_date. ' -1 days')));
				}else{
					$sheet->setCellValue('E7',date('d-F', strtotime($coverage_details[0]->transact_date. ' -1 days')));
					$sheet->setCellValue('F7',date('d-F', strtotime($coverage_details[1]->transact_date. ' -1 days')));
					
					$sheet->setCellValue('H7',date('d-F', strtotime($coverage_details[0]->transact_date. ' -1 days')));
					$sheet->setCellValue('I7',date('d-F', strtotime($coverage_details[1]->transact_date. ' -1 days')));
				}
				$col=1; 
				$roww=6;				
				$i=1;		
				$sheet->getStyle("A6:J6")->applyFromArray($border_style);
				$sheet->getStyle("A7:J7")->applyFromArray($border_style);

				$arrSSS = Array();		
					$row += 2;
				foreach($phic_report as $row2){
					$arrSSS[$row2->lname."-".$row2->emp_promoteId][$row2->coverage_id] = array(
						"fname" => ucwords($row2->fname),
						"lname" => ucwords($row2->lname),
						"coverage_id" => $row2->coverage_id,
						"daterange" => $row2->daterange,
						"month" => $row2->month,
						"monthly" => $row2->monthly,
						"year" => $row2->year,
						"isDaily" => $row2->isDaily,
						"quinsina" => $row2->quinsina,
						"phic_details" => $row2->phic_details,
						"employer_phic" => $row2->employer_phic,
					);
				}
 				foreach($arrSSS as $rr => $val){
					$sheet->setCellValue('A'.$row, $i);
					$emp_share1 = 0;
					$empr_share1 = 0;
					$emp_share2 = 0;
					$empr_share2 = 0;
					$total_emp_share3 = 0;
					$total_empr_share3 = 0;
					$monthly =0;
					$employer_sss =0;
					foreach($val as $rr1 => $val1){
						
						$lname= $val1["lname"];
						$fname= $val1["fname"];
						//$employer_sss+= $val1["employer_sss"];
						if($val1["isDaily"]){
							// if(count($cid)>1){
								$monthly = $val1["quinsina"]*2;
							// }else{
								// $monthly += $val1["quinsina"];
							// }
							 
						}else{
							$monthly= $val1["monthly"];
						}
						
						if(($val1["coverage_id"]==$cid[0])){
							$emp_share1 = 	$val1["phic_details"];
							$empr_share1 = 	$val1["employer_phic"];
						}
						if(isset($cid[1]) && ($val1["coverage_id"]==$cid[1])){
							$emp_share2 = 	$val1["phic_details"];
							$empr_share2 = 	$val1["employer_phic"];
						}else{
							$emp_share2 = 0;
						} 
 						$total_emp_share3 = $emp_share1  + $emp_share2 ;
 						$total_empr_share3 = $empr_share1  + $empr_share2 ;
					} 
 					 $sheet->setCellValue('B'.$row, $lname);
					 $sheet->setCellValue('C'.$row, $fname); 
					 $sheet->setCellValue('D'.$row, $monthly);
					  $sheet->setCellValue('E'.$row, $emp_share1);
					  $sheet->setCellValue('F'.$row, $emp_share2);
					  $sheet->setCellValue('G'.$row, $total_emp_share3);
					  $sheet->setCellValue('H'.$row, $empr_share1);
					  $sheet->setCellValue('I'.$row, $empr_share2);
					  $sheet->setCellValue('J'.$row, $total_empr_share3);
 
					$sheet->getStyle("A".$row.":J".$row)->applyFromArray($border_style);	
					$row++;
					$i++;

					$sheet->getStyle("A6:J6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDCDCDC');
					$sheet->getStyle("A7:J7")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDCDCDC');
					

					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					$col++;
					
				}

			}else{ // HDMF ---------------------------------------------------------------------------------------->>>>>>>>>>
				$type = "HDMF";
				$row = 6;
				$sheet->setCellValue('A'.$row,"SQ. NO.");
				$sheet->setCellValue('B'.$row,"FAMILY NAME");
				$sheet->setCellValue('C'.$row,"FIRST NAME");
				$sheet->setCellValue('D'.$row,"GROSS PAY");
				$sheet->setCellValue('E'.$row,"HDMF PREM. (EE SHARE)");
				$sheet->setCellValue('F'.$row,"HDMF PREM. (EE SHARE)");
				$sheet->setCellValue('G'.$row,"GRAND TOTAL");
				$sheet->setCellValue('G7',"EE SHARE");
				$sheet->setCellValue('H'.$row,"WITH ADDITIONAL HDMF PREM.");
 				$sheet->setCellValue('I'.$row,"HDMF PREMIUM (ER SHARE)");
 				
				$sheet->mergeCells("A6:A7");              
				$sheet->mergeCells("B6:B7");              
				$sheet->mergeCells("C6:C7");              
				$sheet->mergeCells("D6:D7");    
				$sheet->mergeCells("H6:H7");    
				$sheet->mergeCells("I6:I7");    
				$coverage_details = $this->payroll_cover_details($coverageArr);
				$hdmf_report = $this->hdmf_report_premium($coverageArr);
				
				if(count($cid)==1){
					$sheet->setCellValue('E7',date('d-F', strtotime($coverage_details[0]->transact_date. ' -1 days')));
				}else{
					$sheet->setCellValue('E7',date('d-F', strtotime($coverage_details[0]->transact_date. ' -1 days')));
					$sheet->setCellValue('F7',date('d-F', strtotime($coverage_details[1]->transact_date. ' -1 days')));
				}
				$col=1; 
				$roww=6;				
				$i=1;		
				$sheet->getStyle("A6:I6")->applyFromArray($border_style);
				$sheet->getStyle("A7:I7")->applyFromArray($border_style);

				$arrSSS = Array();		
					$row += 2;
				foreach($hdmf_report as $row2){
					$arrSSS[$row2->lname."-".$row2->emp_promoteId][$row2->coverage_id] = array(
						"fname" => ucwords($row2->fname),
						"lname" => ucwords($row2->lname),
						"coverage_id" => $row2->coverage_id,
						"daterange" => $row2->daterange,
						"month" => $row2->month,
						"monthly" => $row2->monthly,
						"year" => $row2->year,
						"isDaily" => $row2->isDaily,
						"quinsina" => $row2->quinsina,
						"employer_hdmf" => $row2->employer_hdmf,
						"hdmf_details" => $row2->hdmf_details,
					);
				}
 				foreach($arrSSS as $rr => $val){
					$sheet->setCellValue('A'.$row, $i);
					$emp_share1 = 0;
					$empr_share1 = 0;
					$emp_share2 = 0;
					$empr_share2 = 0;
					$total_emp_share3 = 0;
					$total_empr_share3 = 0;
					$monthly =0;
					$employer_sss =0;
					foreach($val as $rr1 => $val1){
						
						$lname= $val1["lname"];
						$fname= $val1["fname"];
						//$employer_sss+= $val1["employer_sss"];
						if($val1["isDaily"]){
							// if(count($cid)>1){
								$monthly = $val1["quinsina"]*2;
							// }else{
								// $monthly += $val1["quinsina"];
							// }
							 
						}else{
							$monthly= $val1["monthly"];
						}
						
  						if(($val1["coverage_id"]==$cid[0])){
							$emp_share1 = 	$val1["hdmf_details"];
							$empr_share1 = 	$val1["employer_hdmf"];
						}
						if(isset($cid[1]) && ($val1["coverage_id"]==$cid[1])){
							$emp_share2 = 	$val1["hdmf_details"];
							$empr_share2 = 	$val1["employer_hdmf"];
						}else{
							$emp_share2 = 0;
						} 
 						$total_emp_share3 = $emp_share1  + $emp_share2 ;
 						$total_empr_share3 = $empr_share1  + $empr_share2 ;
 					 
					} 
 					 $sheet->setCellValue('B'.$row, $lname);
					 $sheet->setCellValue('C'.$row, $fname); 
					 $sheet->setCellValue('D'.$row, $monthly);
					   $sheet->setCellValue('E'.$row, $emp_share1);
					   $sheet->setCellValue('F'.$row, $emp_share2);
					   $sheet->setCellValue('G'.$row, $total_emp_share3);
					   $sheet->setCellValue('H'.$row, "");
					   $sheet->setCellValue('I'.$row, $total_empr_share3);
 
					$sheet->getStyle("A".$row.":I".$row)->applyFromArray($border_style);	
					$row++;
					$i++;

					$sheet->getStyle("A6:I6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDCDCDC');
					$sheet->getStyle("A7:I7")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDCDCDC');
					

					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					$col++;
					
				}

			}
			$objPHPExcel->setActiveSheetIndex(0)->setTitle($type." Report");

			$file = 'PAYROLL_GOVERNMENT_REPORT_'.$type.'__bir'.$this->session->userdata('uid').'.xlsx';
			$path= "reports/payroll/".$file;
			header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		/* $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output') */;	
		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		ob_start();
		$objWriter->save("php://output");
		$xlsData = ob_get_contents();
		ob_end_clean();		
			
			$response =  array(
			// 'data' =>$hdmf_report,
			'arrSSS' =>$arrSSS,
			'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
			);

	die(json_encode($response));
	 }
	 
	 
	 
	 public function exportGovBenefits($cid,$mode,$site){
		 $objPHPExcel = $this->phpexcel;
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$type2= "report";
			$sheet = $objPHPExcel->getActiveSheet(0);
			//------------------------INSERT LOGO-------------------------------------------
			 $objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Logo');
				$objDrawing->setPath($this->logo);
			$objDrawing->setOffsetX(0);    // setOffsetX works properly
			$objDrawing->setOffsetY(0);  //setOffsetY has no effect
			$objDrawing->setCoordinates('A1');
			$objDrawing->setHeight(92); // logo height
			// $objDrawing->setWidth(320); // logo width
			// $objDrawing->setWidthAndHeight(200,400);
			$objDrawing->setResizeProportional(true);
			$objDrawing->setWorksheet($sheet);
			
			//----------------------------------------------------------------------

			$styleArray = array(
				'font' => array(
					'bold' => true
				)
			);
			$fontStyle = [
				'font' => [
					'size' => 8
				]
			];
			$style = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
			);
			$style_text_left = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
			);
			$text_left = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				) 
			);
			$text_left_BOLD = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'font' => array(
					'bold' => true
				)
			);
			$coveraged = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=".$cid);
			$daterange= $coveraged->result()[0]->daterange;

			$sheet->setShowGridlines(false);
			$objPHPExcel->createSheet();
			$sheet->setCellValue('D1',"Coverage");
			$sheet->setCellValue('D2',$daterange);

			if($mode==1){ //SSS ---------------------------------------------------------------------------------->>>>>>>>>>
				$type = "SSS";
				$row = 6;
				$sheet->setCellValue('A'.$row,"Name of Employee");
				$sheet->setCellValue('B'.$row,"SS Number");
				$sheet->setCellValue('C'.$row,"SS");
				$sheet->setCellValue('D'.$row,"EC");
				$sheet->setCellValue('E'.$row,"TOTAL CONTRIBUTION");
				$sss_report = $this->sss_report($cid);
				
				$row += 1;
				$col=1;
				$roww=6;				
				foreach($sss_report as $row2){
						$fullname = strtoupper($row2->lname.", ".$row2->fname.", ".substr($row2->mname,0,1).". ");
						$ss = (float)$row2->employer_sss+(float)$row2->sss_details;
						$ec = ((float)$row2->monthly >=15000) ? 30 : 10;
						$total = $ss+$ec;
						$sheet->setCellValue('A'.$row, $fullname);
						$sheet->setCellValue('B'.$row, $row2->sss);
						$sheet->setCellValue('C'.$row, $ss);
						$sheet->setCellValue('D'.$row, $ec);
						$sheet->setCellValue('E'.$row, $total);
						
					$row++;
					$sheet->getStyle("A6:E6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF808080');
					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					$col++;
					
				}
			}else if($mode==2){ //PHIC ---------------------------------------------------------------------------->>>>>>>>>>
				$type = "PHIC";
				$row = 6;
				$sheet->setCellValue('A'.$row,"Philhealth No.");
				$sheet->setCellValue('B'.$row,"Surname");
				$sheet->setCellValue('C'.$row,"Given Name");
				$sheet->setCellValue('D'.$row,"Middle Name");
				$sheet->setCellValue('E'.$row,"EE Share");
				$sheet->setCellValue('F'.$row,"ER Share");
				$phic_report = $this->phic_report($cid);

				$row += 1;
				$col=1;
				$roww=6;
				foreach($phic_report as $row2){
 						$sheet->setCellValue('A'.$row, $row2->philhealth);
						$sheet->setCellValue('B'.$row, $row2->lname);
						$sheet->setCellValue('C'.$row, $row2->fname);
						$sheet->setCellValue('D'.$row, $row2->mname);
						$sheet->setCellValue('E'.$row, $row2->phic_details);
						$sheet->setCellValue('F'.$row, $row2->employer_phic);
						$sheet->getStyle("A".$roww.":F".$roww)->applyFromArray($style);
						$sheet->getStyle('E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
						$sheet->getStyle('F'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
 						
 					$row++;
					$sheet->getStyle("A6:F6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF808080');
					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					$col++;
					}

			}else{ // HDMF ---------------------------------------------------------------------------------------->>>>>>>>>>
				$type = "HDMF";
				$row = 6;
				$sheet->setCellValue('A'.$row,"ID Number");
				$sheet->setCellValue('B'.$row,"Employee's Company ID");
				$sheet->setCellValue('C'.$row,"Last name");
				$sheet->setCellValue('D'.$row,"First Name");
				$sheet->setCellValue('E'.$row,"Middle Name");
				$sheet->setCellValue('F'.$row,"Employee Contribution");
				$sheet->setCellValue('G'.$row,"Employer Contribution");
				$sheet->setCellValue('H'.$row,"TIN");
				$sheet->setCellValue('I'.$row,"Birth Date");
				
				$hdmf_report = $this->hdmf_report($cid);
				
				$row += 1;
				$col=1;
				$roww=6;
				foreach($hdmf_report as $row2){

 						$sheet->setCellValue('A'.$row, $row2->pagibig);
						$sheet->setCellValue('B'.$row, $row2->id_num);
						$sheet->setCellValue('C'.$row, $row2->lname);
						$sheet->setCellValue('D'.$row, $row2->fname);
						$sheet->setCellValue('E'.$row, $row2->mname);
						$sheet->setCellValue('F'.$row, $row2->hdmf_details);
						$sheet->setCellValue('G'.$row, $row2->employer_hdmf);
						$sheet->setCellValue('H'.$row, $row2->bir);
						$sheet->setCellValue('I'.$row, $row2->birthday);
						$sheet->getStyle("A".$roww.":I".$roww)->applyFromArray($style);
						$sheet->getStyle('F'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
						$sheet->getStyle('G'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
 						
 					$row++;
					$sheet->getStyle("A6:I6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF808080');
					$sheet->getColumnDimension($this->columnLetter($col))->setAutoSize(true);
					$col++;
					}

			}
			$objPHPExcel->setActiveSheetIndex(0)->setTitle($type." Report");

			$file = 'PAYROLL_GOVERNMENT_REPORT_'.$type.'_'.$cid.'_bir'.$this->session->userdata('uid').'.xlsx';
			$path= "reports/payroll/".$file;
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="' . $file . '"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');	
	 }
	 private function hdmf_report($cid){
			$fields = "pagibig,id_num,lname,fname,mname,hdmf_details,employer_hdmf,bir,birthday";
			$where = "a.emp_promoteID=b.emp_promoteID and b.emp_id=c.emp_id and c.apid=d.apid and coverage_id=$cid";
			return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d");

	 }
	 private function phic_report($cid){
			$fields = "philhealth,lname,fname,mname,phic_details,employer_phic";
			$where = "a.emp_promoteID=b.emp_promoteID and b.emp_id=c.emp_id and c.apid=d.apid and coverage_id=$cid";
			return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d");

	 }
	  private function hdmf_report_premium($cid){
			$fields = "b.emp_promoteId,pagibig,id_num,lname,fname,mname,hdmf_details,employer_hdmf,bir,birthday,a.isDaily,a.quinsina,monthly,a.coverage_id,e.*";
			$where = "a.emp_promoteID=b.emp_promoteID and b.emp_id=c.emp_id and c.apid=d.apid and a.coverage_id=e.coverage_id and b.isActive=1  and a.coverage_id in ($cid)";
			return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d,tbl_payroll_coverage e","lname ASC");

	 }
	 private function phic_report_premium($cid){
			$fields = "b.emp_promoteId,philhealth,lname,fname,mname,phic_details,a.isDaily,a.quinsina,monthly,a.coverage_id,employer_phic,e.*";
			$where = "a.emp_promoteID=b.emp_promoteID and b.emp_id=c.emp_id and c.apid=d.apid and a.coverage_id=e.coverage_id and b.isActive=1 and a.coverage_id in ($cid)";
			return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d,tbl_payroll_coverage e","lname ASC");

	 }
	 private function sss_report_premium($cid){
			$fields = "b.emp_promoteId,sss,lname,fname,mname,sss_details,c.emp_id,employer_sss,monthly,a.coverage_id,a.isDaily,a.quinsina,e.*";
			$where = "a.emp_promoteID=b.emp_promoteID and b.emp_id=c.emp_id and c.apid=d.apid and a.coverage_id=e.coverage_id and b.isActive=1 and a.coverage_id in ($cid)";
			// echo "select ".$fields." from tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where ".$where." and emp_id=26";
			// exit();
			return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d,tbl_payroll_coverage e","lname ASC");

	 }
	 private function sss_report($cid){
			$fields = "sss,lname,fname,mname,sss_details,employer_sss,monthly";
			$where = "a.emp_promoteID=b.emp_promoteID and b.emp_id=c.emp_id and c.apid=d.apid and coverage_id=$cid";
			return $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_emp_promote b,tbl_employee c,tbl_applicant d");

	 }
	 public function paskoTEST($flag=null){
			$year = $this->input->post("year");
			$empzType = $this->input->post("empType");
			$site = $this->input->post("site");
			$site = (strtoupper($site)=="CDO") ? "" : "-".$site;

			$empzCat = $this->input->post("category");
 			if($empzType=="Admin"){
				if(trim($empzCat)=="Probationary_and_Regular"){
					$type= "admin-both-nonconfi".strtolower($site);

				}else if(trim($empzCat)=="Probationary_and_Regular-TSN"){
					$type= "admin-both-nonconfi-tsn".strtolower($site);

				}else if(trim($empzCat)=="Confidential"){
					$type= "admin-both-confi".strtolower($site);

				}else if(trim($empzCat)=="Trainee-TSN"){
					$type= "admin-trainee-tsn".strtolower($site);

				}else{
					$type= "admin-trainee".strtolower($site);
				}
			}else{
				$type= strtolower($empzType)."-".strtolower($empzCat).strtolower($site);
			}
/*  			$fields_people = "distinct(emp_id) as emp_id";
			$where_people = "a.coverage_id = b.coverage_id and a.emp_promoteID = c.emp_promoteID and a.emp_type='".$type."' and year=$year";
			$rs_people = $this->general_model->fetch_specific_vals($fields_people, $where_people, "tbl_payroll a, tbl_payroll_coverage b, tbl_emp_promote c" );
 */			
		if($flag==null){
			$rs_people = $this->db->query("SELECT emp_id FROM tbl_payroll a, tbl_payroll_coverage b, tbl_emp_promote c WHERE a.coverage_id = b.coverage_id and a.emp_promoteID = c.emp_promoteID and a.emp_type='".$type."' and year=$year GROUP BY emp_id")->result_array();
			$rs_p = array_column($rs_people, 'emp_id');
 			$emp = implode(",",$rs_p);
		}else{
			$emp = $flag;
			$rs_people = $flag;
		}
 			$fields = "fname,lname,month,year,period,daterange,a.*,c.emp_id,empstat_id";
			$where = "a.coverage_id=b.coverage_id and a.emp_promoteID = c.emp_promoteID and c.emp_id=d.emp_id and d.apid=e.apid and f.posempstat_id = c.posempstat_id and empstat_id in (2,3) and c.emp_id in (".$emp.") and year=$year";
			$rs = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll a,tbl_payroll_coverage b,tbl_emp_promote c,tbl_employee d,tbl_applicant e,tbl_pos_emp_stat f","lname ASC");
 			$arr = array();
 			
			$arr["pepz"] = $rs_people;
			foreach($rs as $row){
 					$arr["emp"][$row->lname."-".$row->fname."-".$row->emp_id][$row->coverage_id] = array(
						"lname" => $row->lname,
						"fname" => $row->fname,
						"emp_id" => $row->emp_id,
						"month" => $row->month,
						"year" => $row->year,
						"daterange" => $row->daterange,
						"coverage_id" => $row->coverage_id,
						"quinsina" => $row->quinsina,
						"absentLate" => $row->absentLate,
						"empstat_id" => $row->empstat_id,
						"absentLate_hours" => $row->absentLate_hours,
					);
				 
			}
			echo json_encode($arr);
	 }
	 
}
