<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once dirname(__FILE__) . "/General.php";

class Tito extends General
{
    // public function request()
    // {
    //     $access = $this->check_temp_access();
    //     $data = [
    //         'uri_segment' => $this->uri->segment_array(),
    //         'title' => 'My TITO Request',
    //     ];
    //     if($access){
    //         $this->load_template_view('templates/tito/request', $data);
    //     }
    // }
    public function request()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'My TITO Request',
        ];
        // if ($this->check_access()) {
            $this->load_template_view('templates/tito/request', $data);
        // }
    }
    public function check_temp_access(){
        $acc_id = $_SESSION["acc_id"];
        // $access_arr = [2, 37];
        $dateTime = $this->get_current_date_time();
        $current_date_time = strtotime($dateTime['dateTime']);
        $up_date_time = strtotime('2020-11-16 08:00:00');
        if($current_date_time >= $up_date_time){
            // var_dump('up');
            $access_arr = [23, 28, 69, 80, 37];
        }else{
            $access_arr = [37,2];
            // var_dump('down');
        }
        if(!in_array($acc_id, $access_arr)){
            $this->error_403();
            return false;
        }else{
            return true;
        }
    }
    public function check_temp_access_sup(){
        // $acc_id = $_SESSION["acc_id"];
        $dateTime = $this->get_current_date_time();
        $emp_id = $this->session->userdata('emp_id');

        $dateTime = $this->get_current_date_time();
        $current_date_time = strtotime($dateTime['dateTime']);
        $up_date_time = strtotime('2020-11-16 08:00:00');
        // $access_arr = [];
        if($current_date_time >= $up_date_time){
            // var_dump('up');
            $access_arr = [1464, 813, 1796, 250, 745, 88, 1396, 152, 286, 43, 26, 557, 114];
        }else{
            $access_arr = [26, 557];
            // var_dump('down');
        }
        if(!in_array($emp_id, $access_arr)){
            $this->error_403();
            return false;
        }else{
            return true;
        }
    }
    // public function approval()
    // {
    //     $access = $this->check_temp_access_sup();
    //     $data = [
    //         'uri_segment' => $this->uri->segment_array(),
    //         'title' => 'TITO Approval',
    //     ];
    //     if($access){
    //         $this->load_template_view('templates/tito/approval', $data);
    //     }
    // }
    public function approval()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'TITO Approval',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/tito/approval', $data);
        }
    }
    public function monitoring()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'TITO Monitoring',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/tito/monitoring', $data);
        }
    }
    function get_allowable_break_acc(){
        $acc_time_id = $this->input->post('acc_time_id');
        $fields = "accTime.acc_time_id, break.break_type, breakTime.hour, breakTime.min";
        $where = "breakTime.btime_id = accountBreak.btime_id AND break.break_id = accountBreak.break_id AND accountBreak.bta_id = shiftBreak.bta_id AND times.time_id = accTime.time_id AND shiftBreak.isActive = 1 AND shiftBreak.acc_time_id = accTime.acc_time_id AND  accTime.acc_time_id = $acc_time_id";
        $tables = "tbl_acc_time accTime, tbl_shift_break shiftBreak, tbl_break_time_account accountBreak, tbl_break_time breakTime, tbl_break break, tbl_time times";
        $breaks = $this->general_model->fetch_specific_vals($fields, $where, $tables);
        $hours = 0;
        $minutes = 0;
        if (count($breaks) > 0) {
            foreach ($breaks as $break_val) {
                $hours += $break_val->hour;
                $minutes += $break_val->min;
            }
            $hours_to_min = $hours * 60;
            $minutes += $hours_to_min;
            $break['breakData'] = $breaks;
            $break['minutes'] = $minutes;
        } else {
            $break['breakData'] = 0;
            $break['minutes'] = 0;
        }
        // return $break;
        echo $break["minutes"];
    }
    private function qry_shifts($acc_id=null){
        date_default_timezone_set('Asia/Manila');
        // $acc_id = $this->input->post("acc_id");
		if($acc_id == null){
            $acc_id = $_SESSION["acc_id"];
        }
        // var_dump($acc_id);
        $qry="SELECT acc_time_id,a.time_id,acc_id,note,time_start,time_end FROM `tbl_acc_time` a,tbl_time b where a.time_id=b.time_id and acc_id=".$acc_id." and note='DTR' order by STR_TO_DATE(CONCAT(CURDATE(),' ',time_start), '%Y-%m-%d %l:%i:%s %p') ASC";
		$rs = $this->general_model->custom_query($qry);
		$arr = array();
		foreach($rs as $row){
			// $break = $this->get_allowable_break_acc($row->acc_time_id);
            $arr[] = array(
                "acc_time_id" => $row->acc_time_id,
                "time_id" => $row->time_id,
                "acc_id" => $row->acc_id,
                "note" => $row->note,
                "time_start" => $row->time_start,
                "time_end" => $row->time_end,
            );
		}
        return $arr;
    }
    private function get_emp_by_acc_class($class, $acc_id, $in_clause = NULL, $emp_list = NULL)
    {
        $where_class = "";
        $where_acc = "";
        $where_qip = "";
        $where_emp = "";
        if ($class != 'All') {
            $where_class = "AND acc.acc_description = '$class'";
        }
        if ((int) $acc_id !== 0) {
            $where_acc = "AND acc.acc_id = $acc_id";
        }
        if($emp_list != NULL){
            $where_emp = "AND emp.emp_id $in_clause ($emp_list)";
        }
        $where = "app.apid = emp.apid AND emp.acc_id = acc.acc_id AND emp.isActive = 'yes' ".$where_emp. " " . $where_class . " " . $where_acc . " ";
        $fields = "emp.emp_id, app.fname, app.mname, app.lname";
        $table = "tbl_applicant app, tbl_employee emp, tbl_account acc";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "app.lname ASC");
    }
    private function get_account_by_class($class, $in_clause = NULL, $acc_ids = NULL)
    {
        $where_acc = "acc_id IS NOT NULL";
        $where_class = "";
        if ($class != 'All') {
            $where_class = "AND acc_description = '$class'";
        }
        if($in_clause != NULL){
            $where_acc = "acc_id $in_clause ($acc_ids)";
        }
        $fields = "acc_id, acc_name";
        $where =  $where_acc." ".$where_class;
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_account", "acc_name", 'ASC');
    }
    public function get_monitoring_access_assigned_emp(){
        $fields = "emp.emp_id, emp.acc_id";
        $where = "emp.emp_id = access.assigned_to";
        $tables = "tbl_tito_monitoring_access access, tbl_employee emp";
        return $this->general_model->fetch_specific_vals($fields, $where, $tables);
    }
    public function get_emp_filter()
    {
        $data['exist'] = 0;
        $class = $this->input->post('class');
        $acc_id = $this->input->post('acc_id');
        $filter_type = $this->input->post('filter_type');
        $assigned_emp = $this->get_monitoring_access_assigned_emp();
        $emp_list = ['0'];
        if(count($assigned_emp) > 0){
            $emp_list = implode(',', array_column($assigned_emp, 'emp_id'));
        }
        if($filter_type == 'filter'){
            $record = $this->get_emp_by_acc_class($class, $acc_id, 'IN', $emp_list);
        }else{
            $record = $this->get_emp_by_acc_class($class, $acc_id, 'NOT IN', $emp_list);
        }
        if (count($record) > 0) {
            $data['record'] = $record;
            $data['exist'] = 1;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }
    public function get_account_filters(){
        $data['exist'] = 0;
        $class = $this->input->post('class');
        $filter_type = $this->input->post('filter_type');
        if($filter_type == 'filter'){
            $assigned_emp = $this->get_monitoring_access_assigned_emp();
            $acc_list = ['0'];
            if(count($assigned_emp) > 0){
                $acc_list = implode(',', array_column($assigned_emp, 'acc_id'));
            }
            $record = $this->get_account_by_class($class, 'IN', $acc_list);
        }else{
            $record = $this->get_account_by_class($class);
        }
        // var_dump($this->db->last_query());
        if (count($record) > 0) {
            $data['record'] = $record;
            $data['exist'] = 1;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }
    public function get_shifts(){
		$acc_id = $this->input->post("acc_id");
        $data['exist'] = 0;
        $shifts = $this->qry_shifts($acc_id);
        if(count($shifts) > 0){
            $data['exist'] = 1;
            $data['record'] = $shifts;
        }
        echo json_encode($data);
    }
    // public function get_sched($date, $user_id)
    // {
    //     $fields = "sched.sched_id, sched.schedtype_id, accTime.acc_time_id, times.time_start, times.time_end";
    //     $where = "sched.sched_date = '$date' AND sched.emp_id = users.emp_id AND users.uid = $user_id";
    //     $tables = "tbl_schedule sched, tbl_user users, tbl_acc_time accTime, tbl_time times";
    //     return $this->general_model->fetch_specific_vals($fields, $where, $tables);
    // }
    public function get_sched($date, $user_id, $acc_time_id){
        $fields = "schedType.schedtype_id as schedTypeId, type, sched.sched_id as schedId, users.emp_id empId, users.uid userId, time_start, time_end, bta_id, accTime.acc_id as accountId, remarks, isLocked";
        $where = "schedType.schedtype_id = sched.schedtype_id AND accTime.acc_time_id = sched.acc_time_id AND times.time_id = accTime.time_id AND sched.sched_date = '$date' AND sched.acc_time_id = $acc_time_id AND sched.emp_id = users.emp_id AND users.uid = $user_id";
        $tables = "tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users";
        return $this->general_model->fetch_specific_vals($fields, $where, $tables);
    }
    private function get_dtr_log($empId, $sched_ids)
    {
        $fields = "dtr_id, entry, log, note, type, sched_id as schedId";
        $where = "emp_id = $empId AND sched_id IN ($sched_ids)";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_dtr_logs");
    }
    private function qry_sched_ahrs($user_id, $sched_ids, $release = 0){
        $where_released = "";
        if($release){
            $where_released = " AND ahr.isReleased = 2";
        }
        $fields = "ahr.additionalHourRequestId, ahr.additionalHourTypeId, ahr_type.description ahrType, ahr.otStartDate, ahr.otStartTime, ahr.otEndDate, ahr.otEndTime, ahr.totalOtHours, ahr.approvalStatus_ID, ahr.isReleased, ahr.schedId, stat.description";
        $where = "stat.status_ID = ahr.approvalStatus_ID AND ahr_type.additionalHourTypeId = ahr.additionalHourTypeId AND ahr.userId = $user_id AND ahr.approvalStatus_ID IN (2, 5) AND ahr.schedId IN ($sched_ids)".$where_released;
        $table = "tbl_additional_hour_request ahr, tbl_status stat, tbl_additional_hour_type ahr_type";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }
    private function remove_multiple_ahr_approvers($ahr_ids)
    {
        $where = "additionalHourRequest_ID IN ($ahr_ids)";
        return $this->general_model->delete_vals($where, 'tbl_additional_hour_request_approval');
    }
    private function remove_multiple_ahr_deadlines($ahr_ids, $user_id)
    {
        $where = "module_ID = 1 AND request_ID IN ($ahr_ids) AND requestor = $user_id";
        return $this->general_model->delete_vals($where, 'tbl_deadline');
    }
    private function remove_multiple_ahr_requests($ahr_ids, $user_id){
        $where = "additionalHourRequestId IN ($ahr_ids) AND userId = $user_id";
        return $this->general_model->delete_vals($where, 'tbl_additional_hour_request');
    }
    protected function set_multiple_sched($sched_ids, $remarks, $module_id, $locked_stat)
	{
		$data['remarks'] = $remarks;
		$data['module_ID'] = $module_id;
		$data['isLocked'] = $locked_stat;
		$where = "sched_id IN ($sched_ids)";
		return $this->general_model->update_vals($data, $where, 'tbl_schedule');
    }
    protected function set_general_system_log($action, $committed_by, $date_committed, $module, $details){
        $data['action'] = $action;
		$data['committed_by'] = $committed_by;
		$data['updated_by'] = $committed_by;
		$data['date_committed'] = $date_committed;
		$data['module'] = $module;
        $data['details'] = $details;
        return $this->general_model->insert_vals($data, "tbl_general_system_log");
    }
    public function get_sched_attachments(){
        $data['with_details'] = 0;
        $data['logs_content'] = '';
        $sched_date = $this->input->post('sched_date');
        $acc_time_id = $this->input->post('acc_time_id');
        $user_id = $this->session->userdata('uid');
        $emp_id = $this->session->userdata('emp_id');
        $sched_details = $this->get_sched($sched_date, $user_id, $acc_time_id);
        // var_dump($sched_details);
        if(count($sched_details) > 0){
            $sched_ids = array_column($sched_details, 'schedId');
            // var_dump($sched_ids);
            $logs = $this->get_dtr_log($emp_id, implode(",",$sched_ids));
            // var_dump($logs);
            if(count($logs) > 0){
                $data['with_details'] = 1;
            }
            foreach($sched_details as $sched_details_row){
                // var_dump($sched_details_row);
                $sched_details_row_sched_id = $sched_details_row->schedId;
                // var_dump($sched_details_row_sched_id);
                $sched_logs = array_merge(array_filter($logs, function ($e) use ($sched_details_row_sched_id) {
                        return $e->schedId == $sched_details_row_sched_id;
                    }
                ));
                $clock_in = "NO LOG FOUND";
                $clock_out = "NO LOG FOUND";
                foreach($sched_logs as $sched_logs_row){
                    if($sched_logs_row->entry == 'I'){
                        $clock_in = $sched_logs_row->log;
                    }else if($sched_logs_row->entry == 'O'){
                        $clock_out = $sched_logs_row->log;
                    }
                }
                $locked_label = "";
                if($sched_details_row->isLocked != 0){
                    $locked_status = 'Locked';
                    if($sched_details_row->isLocked == 2){
                        $locked_status = 'Pending';
                    }
                    $locked_label = '</br><div style="font-size: 0.8rem;" class="text-primary">('.$locked_status.' with '.$sched_details_row->remarks.')</div>';
                }
                // var_dump($sched_logs);
                $data['logs_content'] .= '<div class="col-12">';
                    $data['logs_content'] .= '<table class="table table-bordered table-hover">';
                        $data['logs_content'] .= '<thead>';
                            $data['logs_content'] .= '<tr class="" style="background:#d8d8d8;">';
                                $data['logs_content'] .= '<th class="align-middle p-2" colspan="3" style="font-size: 0.7rem;">SCHED ID: '.str_pad($sched_details_row_sched_id, 8, '0', STR_PAD_LEFT).'</th>';
                            $data['logs_content'] .= '</tr>';
                        $data['logs_content'] .= '</thead>';
                        $data['logs_content'] .= '<tbody style="font-size: 1rem;" class="text-center">';
                            $data['logs_content'] .= '<tr>';
                                $data['logs_content'] .= '<th class="align-middle" scope="row" rowspan="2" style="padding: 0.5rem;">'.$sched_details_row->time_start.' - '.$sched_details_row->time_end.' '.$locked_label.' </th>';
                                $data['logs_content'] .= '<td style="padding: 0.5rem;">IN</td>';
                                $data['logs_content'] .= '<td style="padding: 0.5rem;">'.$clock_in.'</td>';
                            $data['logs_content'] .= '</tr>';
                            $data['logs_content'] .= '<tr>';
                                $data['logs_content'] .= '<td style="padding: 0.5rem;">OUT</td>';
                                $data['logs_content'] .= '<td style="padding: 0.5rem;">'.$clock_out.'</td>';
                            $data['logs_content'] .= '</tr>';
                        $data['logs_content'] .= '</tbody>';
                    $data['logs_content'] .= '</table>';
                $data['logs_content'] .= '</div>';
            }
        }
        echo json_encode($data);
    }
    public function remove_ahr(){
        $remove['remove_approver'] = 0;
        $remove['remove_deadlines'] = 0;
        $remove['remove_ahrs'] = 0;
        $remove['update_scheds'] = 0;
        $remove['set_log'] = 0;
        $sched_date = $this->input->post('sched_date');
        $acc_time_id = $this->input->post('acc_time_id');
        $user_id = $this->session->userdata('uid');
        $dateTime = $this->get_current_date_time();
        $sched_details = $this->get_sched($sched_date, $user_id, $acc_time_id);
        if(count($sched_details) > 0){
            $sched_ids = array_column($sched_details, 'schedId');
            if(count($sched_ids) > 0){
                // $sched_ahrs = $this->qry_sched_ahrs($user_id, implode(",",$sched_ids), 1);
                $sched_ahrs = $this->qry_sched_ahrs($user_id, implode(",",$sched_ids));
                // var_dump($sched_ahrs);
                if(count($sched_ahrs) > 0){
                    $ahr_ids = array_column($sched_ahrs, 'additionalHourRequestId');
                    $ahr_sched_ids = array_column($sched_ahrs, 'schedId');
                    // var_dump($ahr_sched_ids);
                    if(count($ahr_ids) > 0){
                        //remove ahr approvers
                        $remove['remove_approver'] = $this->remove_multiple_ahr_approvers(implode(",",$ahr_ids));
                        //remove ahr deadlines
                        $remove['remove_deadlines'] = $this->remove_multiple_ahr_deadlines(implode(",",$ahr_ids), $user_id);
                        // remove ahr
                        $remove['remove_ahrs'] = $this->remove_multiple_ahr_requests(implode(",",$ahr_ids), $user_id);
                        // update sched
                        $remove['update_scheds'] = $this->set_multiple_sched(implode(",",$ahr_sched_ids), "Excel", NULL, 0);
                        // log deletion
                        $action = "DELETE";
                        $committed_by = $this->session->userdata('emp_id');
                        $date_committed = $dateTime['dateTime'];
                        $module = 1;
                        $ahr_ids_str = implode(",",$ahr_ids);
                        $details = "Deleted AHR IDs: $ahr_ids_str, inorder to file a TITO request for $sched_date";
                        $remove['set_log'] = $this->set_general_system_log($action, $committed_by, $date_committed, $module, $details);
                    }
                    // remove approvers
                }
            }
        }
        echo json_encode($remove);
    }
    public function get_ahr_attachments(){
        $data['with_ahr'] = 0;
        $data['ahr_content'] = '';
        $sched_date = $this->input->post('sched_date');
        $acc_time_id = $this->input->post('acc_time_id');
        $user_id = $this->session->userdata('uid');
        $sched_details = $this->get_sched($sched_date, $user_id, $acc_time_id);
        if(count($sched_details) > 0){
            $sched_ids = array_column($sched_details, 'schedId');
            if(count($sched_ids) > 0){
                $sched_ahrs = $this->qry_sched_ahrs($user_id, implode(",",$sched_ids));
                if(count($sched_ahrs) > 0){
                    foreach($sched_ahrs as $sched_ahrs_row){
                        $locked_label = '</br><div style="font-size: 0.8rem;" class="text-primary">('.$sched_ahrs_row->description.')</div>';
                        $duration = $this->get_formatted_time($sched_ahrs_row->totalOtHours);
                        $data['ahr_content'] .= '<div class="col-12">';
                            $data['ahr_content'] .= '<table class="table table-bordered table-hover">';
                                $data['ahr_content'] .= '<thead>';
                                    $data['ahr_content'] .= '<tr class="" style="background:#d8d8d8;">';
                                        $data['ahr_content'] .= '<th class="align-middle p-2" colspan="4" style="font-size: 0.7rem;">AHR ID: '.str_pad($sched_ahrs_row->additionalHourRequestId, 8, '0', STR_PAD_LEFT).'</th>';
                                    $data['ahr_content'] .= '</tr>';
                                $data['ahr_content'] .= '</thead>';
                                $data['ahr_content'] .= '<tbody style="font-size: 0.8rem;" class="text-center">';
                                    $data['ahr_content'] .= '<tr>';
                                        $data['ahr_content'] .= '<td class="align-middle" style="padding: 0.5rem;" rowspan="2">'.$sched_ahrs_row->ahrType.' '.$locked_label.'</td>';
                                        $data['ahr_content'] .= '<td class="align-middle" style="padding: 0.5rem;" rowspan="2">'.$duration.'</td>';
                                        $data['ahr_content'] .= '<td style="padding: 0.5rem;">Start</td>';
                                        $data['ahr_content'] .= '<td style="padding: 0.5rem;">'.$sched_ahrs_row->otStartDate.' '.$sched_ahrs_row->otStartTime.'</td>';
                                    $data['ahr_content'] .= '</tr>';
                                    $data['ahr_content'] .= '<tr> ';
                                        $data['ahr_content'] .= '<td style="padding: 0.5rem;">End</td>';
                                        $data['ahr_content'] .= '<td style="padding: 0.5rem;">'.$sched_ahrs_row->otEndDate.' '.$sched_ahrs_row->otEndTime.'</td>';
                                    $data['ahr_content'] .= '</tr>';
                                $data['ahr_content'] .= '</tbody>';
                            $data['ahr_content'] .= '</table>';
                        $data['ahr_content'] .= '</div>';
                    }
                }
            }
        }
        echo json_encode($data);
    }
    public function check_if_has_sched_details(){
        $data['with_details'] = 0;
        $sched_date = $this->input->post('sched_date');
        $acc_time_id = $this->input->post('acc_time_id');
        $user_id = $this->session->userdata('uid');
        $emp_id = $this->session->userdata('emp_id');
        $sched_details = $this->get_sched($sched_date, $user_id, $acc_time_id);
        if(count($sched_details) > 0){
            $sched_ids = array_column($sched_details, 'schedId');
            if(count($sched_ids) > 0){
                $logs = $this->get_dtr_log($emp_id, implode(",",$sched_ids));
                if(count($logs) > 0){
                    $data['with_details'] = 1;
                }
            }
        }
        echo json_encode($data);
    }
    private function qry_tito_request_exist($date_request, $acc_time_id){
        $user_id = $this->session->userdata('uid');
        $fields = "dtrRequest_ID, userID, dateRequest, acc_time_id, message, status_ID, dateCreated, dateUpdated, updatedBy, actualDate, startDateWorked, startTimeWorked, endDateWorked, endTimeWorked, timetotal, withAhr";
        $where = "DATE(dateRequest) = '$date_request' AND acc_time_id = $acc_time_id AND status_ID IN (2) AND userID = $user_id";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dtr_request');
    }
    private function qry_all_tito_request_filed($date_request){
        $user_id = $this->session->userdata('uid');
        $fields = "dtrRequest_ID, userID, dateRequest, acc_time_id, message, status_ID, dateCreated, dateUpdated, updatedBy, actualDate, startDateWorked, startTimeWorked, endDateWorked, endTimeWorked, timetotal, withAhr";
        $where = "DATE(dateRequest) = '$date_request' AND status_ID IN (5) AND userID = $user_id";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_dtr_request');
    }
    public function check_if_tito_filed(){
        $data['exist'] = 0;
        $date_request = $this->input->post('sched_date');
        $acc_time_id = $this->input->post('acc_time_id');
        // $date_request = '2018-09-10';
        // $acc_time_id = 1831;
        $record = $this->qry_tito_request_exist($date_request, $acc_time_id);
        if(count($record) > 0){
            $data['exist'] = 1;
        }
        echo json_encode($data);
    }
    public function check_num_of_tito_filed(){
        $data['exist'] = 0;
        $date_request = $this->input->post('sched_date');
        // $date_request = '2018-09-10';
        // $acc_time_id = 1831;
        $record = $this->qry_all_tito_request_filed($date_request);
        if(count($record) > 2){
            $data['exist'] = 1;
        }
        echo json_encode($data);
    }
    public function check_if_sched_has_ahrs(){
        $data['with_ahr'] = 0;
        $sched_date = $this->input->post('sched_date');
        $acc_time_id = $this->input->post('acc_time_id');
        $user_id = $this->session->userdata('uid');
        $sched_details = $this->get_sched($sched_date, $user_id, $acc_time_id);
        if(count($sched_details) > 0){
            $sched_ids = array_column($sched_details, 'schedId');
            if(count($sched_ids) > 0){
                $sched_ahrs = $this->qry_sched_ahrs($user_id, implode(",",$sched_ids));
                if(count($sched_ahrs) > 0){
                    $data['with_ahr'] = 1;
                    $data['removable'] = 1;
                    $release_stat = 3;
                    $released_ahr = array_merge(array_filter($sched_ahrs, function ($e) use ($release_stat) {
                            return $e->isReleased == $release_stat;
                        }
                    ));
                    if(count($released_ahr) > 0){
                        $data['with_ahr'] = 1;
                        $data['removable'] = 0;
                    }
                }
            }
        }
        echo json_encode($data);
    }
    protected function insert_monitoring_access($action, $committed_by, $date_committed, $module, $details){
        $data['action'] = $action;
		$data['committed_by'] = $committed_by;
		$data['date_committed'] = $date_committed;
		$data['module'] = $module;
        $data['details'] = $details;
        return $this->general_model->insert_vals($data, "tbl_general_system_log");
    }
    public function assign_monitoring_access(){
        $data['stat'] = 0;
        $emp_ids = $this->input->post('emp_ids');
        $dateTime = $this->get_current_date_time();
        $user_access = [];
        $emp_id_arr = explode(',', $emp_ids);
        if(count($emp_id_arr) > 0){
            for($loop = 0; $loop < count($emp_id_arr); $loop++){
                $user_access[$loop] = [
                    'assigned_to' => $emp_id_arr[$loop],
                    'added_by' => $this->session->userdata('emp_id'),
                    'date_time_created' => $dateTime['dateTime']
                ];
            }
            if(count($user_access) > 0){
                $data['stat'] = $this->general_model->batch_insert($user_access, 'tbl_tito_monitoring_access');
            }
        }else{
            $data['stat'];
        }
        echo json_encode($data);
        // var_dump($user_access);
        // var_dump()
    }
    public function get_monitoring_access_datatable(){
        $datatable = $this->input->post('datatable');
        $acc_id = $datatable['query']['acc_id'];
        $class = $datatable['query']['class'];
        $emp_id = $datatable['query']['emp_id'];
        $where_class = "";
        $where_acc = "";
        $where_emp = "";
        if ($class !== 'All') {
            $where_class = " AND acc.acc_description = '$class'";
        }
        if ((int) $acc_id !== 0) {
            $where_acc = " AND acc.acc_id = $acc_id";
        }
        if ((int) $emp_id !== 0) {
            $where_emp = " AND access.assigned_to = $emp_id";
        }
        $query['query'] = "SELECT access.titoMonitoringAccess_ID, access.assigned_to, app.fname, app.mname, app.lname, app.pic, access.added_by, access.date_time_created, access.access_type, emp.acc_id, acc.acc_description FROM tbl_tito_monitoring_access access, tbl_employee emp, tbl_applicant app, tbl_account acc WHERE acc.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = access.assigned_to ".$where_emp." ".$where_class." ".$where_acc;
        $data = $this->set_datatable_query($datatable, $query);
        // var_dump($this->db->last_query());
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    private function qry_update_monitoring_access($access_id, $access_type){
        $dateTime = $this->get_current_date_time();
        $emp_id = $this->session->userdata('emp_id');
        $where = "titoMonitoringAccess_ID = $access_id";
        $data['updated_by'] = $emp_id;
		$data['date_time_created'] = $dateTime['dateTime'];
        $data['access_type'] = $access_type;
        return $this->general_model->update_vals($data, $where, "tbl_tito_monitoring_access");
    }
    private function qry_remove_monitoring_access($access_id)
    {
        $where['titoMonitoringAccess_ID'] = $access_id;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_tito_monitoring_access');
    }
    public function update_monitoring_access(){
        $access_id = $this->input->post('access_id');
        $access_type = $this->input->post('access');
        $update_access = $this->qry_update_monitoring_access($access_id, $access_type);
        echo json_encode($update_access);
    }
    public function remove_monitoring_access(){
        $remove_status = $this->qry_remove_monitoring_access($this->input->post('access_id'));
        echo json_encode($remove_status);
    }
    public function get_tito_request_datatable(){
        $user_id = $this->session->userdata('uid');
        $datatable = $this->input->post('datatable');
        $status = $datatable['query']['status'];
        $type = $datatable['query']['type'];
        $search = $datatable['query']['search'];
        $type_arr = [];
        $stat_where = "AND tito.status_ID IN (2, 12)";
        $with_ahr = [];
        $ahr_where = "";
        if((int) $status !== 0){
            $stat_where = "AND tito.status_ID = $status";
        }
        if(trim($type) != ''){
            $type_arr = explode(',', $type);
            for($loop = 0; $loop < count($type_arr); $loop++){
                if($type_arr[$loop] == 'tito_ahr'){
                    $with_ahr[$loop] = 1;
                }
                if($type_arr[$loop] == 'tito'){
                    $with_ahr[$loop] = 0;
                }
            }
            if(count($with_ahr) > 0){
                $ahr_bool = implode(',', $with_ahr);
                $ahr_where = "AND tito.withAhr IN ($ahr_bool)";
            }
        }
        $query['query'] = "SELECT tito.dtrRequest_ID, emp.emp_id, tito.userID, app.fname, app.mname, app.lname, app.pic, tito.dateRequest, times.time_start, times.time_end, tito.status_ID, tito.dateCreated, tito.timetotal, stat.description FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN  tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_applicant app ON app.apid = emp.apid WHERE tito.userID = $user_id ".$stat_where." ".$ahr_where;
        if (trim($search) != '') {
            $keyword = $search;
            $date_bool = $this->check_if_date($keyword);
            // var_dump($date_bool);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "tito.dateRequest = '$date' OR tito.dateCreated LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "tito.dtrRequest_ID LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "tito.dtrRequest_ID = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function get_tito_record_datatable(){
        $user_id = $this->session->userdata('uid');
        $datatable = $this->input->post('datatable');
        $status = $datatable['query']['status'];
        $type = $datatable['query']['type'];
        $search = $datatable['query']['search'];
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];
        $type_arr = [];
        $stat_where = "AND tito.status_ID IN (5, 6)";
        $with_ahr = [];
        $ahr_where = "";
        if(trim($status) != ''){
            $stat_where = "AND tito.status_ID IN ($status)";
        }
        if(trim($type) != ''){
            $type_arr = explode(',', $type);
            for($loop = 0; $loop < count($type_arr); $loop++){
                if($type_arr[$loop] == 'tito_ahr'){
                    $with_ahr[$loop] = 1;
                }
                if($type_arr[$loop] == 'tito'){
                    $with_ahr[$loop] = 0;
                }
            }
            if(count($with_ahr) > 0){
                $ahr_bool = implode(',', $with_ahr);
                $ahr_where = "AND tito.withAhr IN ($ahr_bool)";
            }
        }
        $query['query'] = "SELECT tito.dtrRequest_ID, emp.emp_id, tito.userID, app.fname, app.mname, app.lname, app.pic, tito.dateRequest, times.time_start, times.time_end, tito.status_ID, tito.dateCreated, tito.timetotal, stat.description FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN  tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_applicant app ON app.apid = emp.apid WHERE tito.userID = $user_id AND DATE(tito.dateRequest) >= '$start_date' AND DATE(tito.dateRequest) <= '$end_date' ".$stat_where." ".$ahr_where;
        if (trim($search) != '') {
            $keyword = $search;
            $date_bool = $this->check_if_date($keyword);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "tito.dateRequest = '$date' OR tito.dateCreated LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "tito.dtrRequest_ID LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "tito.dtrRequest_ID = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    private function remove_tito_approvers($dtrRequest_ID){
        $where['dtrRequest_ID'] = $dtrRequest_ID;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_dtr_request_approval');  
    }
    private function remove_tito($dtrRequest_ID){
        $where['dtrRequest_ID'] = $dtrRequest_ID;
        return $delete_stat = $this->general_model->delete_vals($where, 'tbl_dtr_request');
    }
    private function qry_tito_ahrs($tito_id){
        $fields = "titoAhrRequest_ID, titoRequest_ID, ahr_ID, dateTimeCreated";
        $where = "titoRequest_ID = $tito_id";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_tito_ahr_request");
    }
    private function remove_attached_ahr($tito_id){
        $delete_stat = 1;
        $where_ahr = "tito_ID = $tito_id";
        $delete_ahr_stat = $this->general_model->delete_vals($where_ahr, 'tbl_additional_hour_request');
        if($delete_ahr_stat == 1){
            $delete_stat = 1;
        }else{
            $delete_stat = 0;
        }
        return $delete_stat;
    }
    public function remove_tito_request(){
        $tito_id = $this->input->post("tito_id");
		$delete_stat['delete_deadline_stat'] = $this->remove_deadlines(3, $tito_id);
        $delete_stat['delete_approvers_stat'] = $this->remove_tito_approvers($tito_id);
        $delete_stat['delete_ahr_stat'] = $this->remove_attached_ahr($tito_id);
        $delete_stat['delete_tito_stat'] = $this->remove_tito($tito_id);
		echo json_encode($delete_stat);
    }

    // Approval

    //init account filter 

    private function qry_pending_approval_accounts($level)
    {
        $where_level = "";
        if((int) $level > 0){
            $where_level = " AND appr.approvalLevel = $level";
        }
        $user_id = $this->session->userdata('uid');
        $query = "SELECT DISTINCT(account.acc_id) accId, account.acc_name, account.acc_description FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid INNER JOIN tbl_dtr_request_approval appr ON tito.dtrRequest_ID = appr.dtrRequest_ID INNER JOIN tbl_deadline dead ON dead.request_ID = tito.dtrRequest_ID AND dead.module_ID = 3 AND dead.userId = appr.userId AND dead.status_ID = 2 AND appr.userId = $user_id AND appr.approvalStatus_ID = 4 AND tito.status_ID = 2".$where_level;
        return $record = $this->general_model->custom_query($query);
    }
    private function qry_record_approval_accounts($start_date, $end_date)
    {
        $user_id = $this->session->userdata('uid');
        $query = "SELECT DISTINCT(account.acc_id) accId, account.acc_name, account.acc_description FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid INNER JOIN tbl_dtr_request_approval appr ON tito.dtrRequest_ID = appr.dtrRequest_ID INNER JOIN tbl_deadline dead ON dead.request_ID = tito.dtrRequest_ID AND dead.module_ID = 3 AND dead.userId = appr.userId AND dead.status_ID IN (3, 12) AND appr.userId = $user_id AND appr.approvalStatus_ID IN (5, 6, 12) AND tito.status_ID IN (5, 6, 12) AND DATE(tito.dateRequest) >= '$start_date' AND DATE(tito.dateRequest) <= '$end_date'";
        return $record = $this->general_model->custom_query($query);
    }
    private function qry_pending_approval_employee($level, $acc_id)
    {
        $where = "";
        $where_arr = [];
        $where_count = 0;
        if((int) $level > 0){
            $where_arr[$where_count] = "AND appr.approvalLevel = $level";
            $where_count += 1;
        }
        if((int) $acc_id != 0){
            $where_arr[$where_count] = "AND emp.acc_id = $acc_id";
            $where_count += 1;
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $user_id = $this->session->userdata('uid');
        $query = "SELECT DISTINCT(emp.emp_id) empId, users.uid, app.fname, app.mname, app.lname FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid INNER JOIN tbl_dtr_request_approval appr ON tito.dtrRequest_ID = appr.dtrRequest_ID INNER JOIN tbl_deadline dead ON dead.request_ID = tito.dtrRequest_ID AND dead.module_ID = 3 AND dead.userId = appr.userId AND dead.status_ID = 2 AND appr.userId = $user_id AND appr.approvalStatus_ID = 4 AND tito.status_ID = 2 ".$where;
        return $record = $this->general_model->custom_query($query);
    }
    private function qry_record_approval_employee($acc_id, $start_date, $end_date)
    {
        $where = "";
        $where_arr = [];
        $where_count = 0;
        if((int) $acc_id != 0){
            $where_arr[$where_count] = "AND emp.acc_id = $acc_id";
            $where_count += 1;
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $user_id = $this->session->userdata('uid');
        $query = "SELECT DISTINCT(emp.emp_id) empId, users.uid, app.fname, app.mname, app.lname FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid INNER JOIN tbl_dtr_request_approval appr ON tito.dtrRequest_ID = appr.dtrRequest_ID INNER JOIN tbl_deadline dead ON dead.request_ID = tito.dtrRequest_ID AND dead.module_ID = 3 AND dead.userId = appr.userId AND dead.status_ID IN (3, 12) AND appr.userId = $user_id AND appr.approvalStatus_ID IN (5, 6, 12) AND tito.status_ID IN (5, 6, 12) AND DATE(tito.dateRequest) >= '$start_date' AND DATE(tito.dateRequest) <= '$end_date' ".$where;
        return $record = $this->general_model->custom_query($query);
    }
    public function get_pending_approval_accounts(){
        $data['exist'] = 0;
        $level = $this->input->post('level');
        $record = $this->qry_pending_approval_accounts($level);
        // echo $this->db->last_query();
        if(count($record) > 0){
            $data['exist'] = 1;
            $data['record'] = $record;
        }
        echo json_encode($data);
    }
    public function get_pending_approval_emp(){
        $data['exist'] = 0;
        $level = $this->input->post('level');
        $acc_id = $this->input->post('account_id');
        $record = $this->qry_pending_approval_employee($level, $acc_id);
        // echo $this->db->last_query();
        if(count($record) > 0){
            $data['exist'] = 1;
            $data['record'] = $record;
        }
        echo json_encode($data);
    }
    public function get_tito_pending_approval_datatable(){
        $user_id = $this->session->userdata('uid');
        $datatable = $this->input->post('datatable');
        $level = $datatable['query']['level'];
        $acc_id = $datatable['query']['acc_id'];
        $emp_id = $datatable['query']['emp_id'];
        $type = $datatable['query']['type'];
        $search = $datatable['query']['search'];
        $type_arr = [];
        $with_ahr = [];
        $ahr_where = "";
        $where = "";
        $where_arr = [];
        $where_count = 0;
        if((int) $level > 0){
            $where_arr[$where_count] = "AND appr.approvalLevel = $level";
            $where_count += 1;
        }
        if((int) $acc_id != 0){
            $where_arr[$where_count] = "AND emp.acc_id = $acc_id";
            $where_count += 1;
        }
        if((int) $emp_id != 0){
            $where_arr[$where_count] = "AND emp.emp_id = $emp_id";
            $where_count += 1;
        }
        if(trim($type) != ''){
            $type_arr = explode(',', $type);
            for($loop = 0; $loop < count($type_arr); $loop++){
                if($type_arr[$loop] == 'tito_ahr'){
                    $with_ahr[$loop] = 1;
                }
                if($type_arr[$loop] == 'tito'){
                    $with_ahr[$loop] = 0;
                }
            }
            if(count($with_ahr) > 0){
                $ahr_bool = implode(',', $with_ahr);
                $where_arr[$where_count] = "AND tito.withAhr IN ($ahr_bool)";
                $where_count += 1;
            }
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $query['query'] = "SELECT distinct(tito.dtrRequest_ID),appr.dtrRequestApproval_ID, emp.emp_id, tito.userID, app.fname, app.mname, app.lname, app.pic, tito.dateRequest, times.time_start, times.time_end, tito.status_ID, tito.dateCreated, tito.timetotal, stat.description, dead.deadline, account.acc_name, account.acc_description, appr.approvalLevel FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid INNER JOIN tbl_dtr_request_approval appr ON tito.dtrRequest_ID = appr.dtrRequest_ID INNER JOIN tbl_deadline dead ON dead.request_ID = tito.dtrRequest_ID AND dead.module_ID = 3 AND dead.userId = appr.userId AND dead.status_ID = 2 AND appr.userId = $user_id AND appr.approvalStatus_ID = 4 AND tito.status_ID = 2 ".$where;
        if (trim($search) != '') {
            $keyword = $search;
            $date_bool = $this->check_if_date($keyword);
            // var_dump($date_bool);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "tito.dateRequest = '$date' OR tito.dateCreated LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "tito.dtrRequest_ID LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "tito.dtrRequest_ID = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function get_record_approval_accounts(){
        $data['exist'] = 0;
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $record = $this->qry_record_approval_accounts($start_date, $end_date);
        // echo $this->db->last_query();
        if(count($record) > 0){
            $data['exist'] = 1;
            $data['record'] = $record;
        }
        echo json_encode($data);
    }
    public function get_record_approval_emp(){
        $data['exist'] = 0;
        $acc_id = $this->input->post('account_id');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $record = $this->qry_record_approval_employee($acc_id, $start_date, $end_date);
        // echo $this->db->last_query();
        if(count($record) > 0){
            $data['exist'] = 1;
            $data['record'] = $record;
        }
        echo json_encode($data);
    }
    public function get_tito_record_approval_datatable(){
        $user_id = $this->session->userdata('uid');
        $datatable = $this->input->post('datatable');
        $acc_id = $datatable['query']['acc_id'];
        $emp_id = $datatable['query']['emp_id'];
        $type = $datatable['query']['type'];
        $status = $datatable['query']['status'];
        $search = $datatable['query']['search'];
        $start_date = $datatable['query']['start_date'];
        $end_date = $datatable['query']['end_date'];
        $type_arr = [];
        $with_ahr = [];
        $where = "";
        $where_arr = [];
        $where_count = 0;
        if((int) $acc_id != 0){
            $where_arr[$where_count] = "AND emp.acc_id = $acc_id";
            $where_count += 1;
        }
        if((int) $emp_id != 0){
            $where_arr[$where_count] = "AND emp.emp_id = $emp_id";
            $where_count += 1;
        }
        if(trim($type) != ''){
            $type_arr = explode(',', $type);
            for($loop = 0; $loop < count($type_arr); $loop++){
                if($type_arr[$loop] == 'tito_ahr'){
                    $with_ahr[$loop] = 1;
                }
                if($type_arr[$loop] == 'tito'){
                    $with_ahr[$loop] = 0;
                }
            }
            if(count($with_ahr) > 0){
                $ahr_bool = implode(',', $with_ahr);
                $where_arr[$where_count] = "AND tito.withAhr IN ($ahr_bool)";
                $where_count += 1;
            }
        }
        if(trim($status) != ''){
            $where_arr[$where_count] = "AND appr.approvalStatus_ID IN ($status)";
            $where_count += 1;
        }else{
            $where_arr[$where_count] = "AND appr.approvalStatus_ID IN (5, 6, 12)";
            $where_count += 1;
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $query['query'] = "SELECT appr.dtrRequestApproval_ID, tito.dtrRequest_ID, emp.emp_id, tito.userID, app.fname, app.mname, app.lname, app.pic, tito.dateRequest, times.time_start, times.time_end, appr.approvalStatus_ID, tito.dateCreated, tito.timetotal, stat.description, dead.deadline, account.acc_name, account.acc_description, appr.approvalLevel FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid INNER JOIN tbl_dtr_request_approval appr ON tito.dtrRequest_ID = appr.dtrRequest_ID INNER JOIN tbl_status stat ON stat.status_ID = appr.approvalStatus_ID INNER JOIN tbl_deadline dead ON dead.request_ID = tito.dtrRequest_ID AND dead.module_ID = 3 AND dead.userId = appr.userId AND dead.status_ID IN (3, 12) AND appr.userId = $user_id AND DATE(tito.dateRequest) >= '$start_date' AND DATE(tito.dateRequest) <= '$end_date' ".$where;
        if (trim($search) != '') {
            $keyword = $search;
            $date_bool = $this->check_if_date($keyword);
            // var_dump($date_bool);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "tito.dateRequest = '$date' OR tito.dateCreated LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "tito.dtrRequest_ID LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "tito.dtrRequest_ID = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    //Monitoring
    private function qry_pending_tito_request_acc($class, $stat_ids, $emp_ids = ''){
        $where = "";
        $where_arr = [];
        $where_count = 0;
        if($class != 'All'){
            $where_arr[$where_count] ="AND account.acc_description = '$class'";
            $where_count += 1;
        }
        if(trim($emp_ids) != ""){
            $where_arr[$where_count] ="AND emp.emp_id IN ($emp_ids)";
            $where_count += 1;
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $qry="SELECT DISTINCT(account.acc_id) accId, account.acc_name, account.acc_description FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid AND tito.status_ID IN ($stat_ids) ".$where." ORDER BY account.acc_name ASC";
        return $this->general_model->custom_query($qry);
    }
    
    private function qry_record_tito_request_acc($class, $start_date, $end_date, $emp_ids = ''){
        $where = "";
        $where_arr = [];
        $where_count = 0;
        if($class != 'All'){
            $where_arr[$where_count] ="AND account.acc_description = '$class'";
            $where_count += 1;
        }
        if(trim($emp_ids) != ""){
            $where_arr[$where_count] ="AND emp.emp_id IN ($emp_ids)";
            $where_count += 1;
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $qry="SELECT DISTINCT(account.acc_id) accId, account.acc_name, account.acc_description FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid AND tito.status_ID IN (5, 6) AND DATE(tito.dateRequest) >= '$start_date' AND DATE(tito.dateRequest) <= '$end_date' ".$where." ORDER BY account.acc_name ASC";
        return $this->general_model->custom_query($qry);
    }
    private function qry_all_pending_tito_request_emp($class, $acc_id, $type, $status, $emp_ids =''){
        $where = "";
        $where_arr = [];
        $where_count = 0;
        if($class != 'All'){
            $where_arr[$where_count] = " AND account.acc_description = '$class'";
            $where_count++;
        }
        if((int)$acc_id != 0){
            $where_arr[$where_count] = " AND emp.acc_id = '$acc_id'";
            $where_count++;
        }
        if(trim($emp_ids) != ""){
            $where_arr[$where_count] ="AND emp.emp_id IN ($emp_ids)";
            $where_count += 1;
        }
        if(trim($type) != ''){
            $type_arr = explode(',', $type);
            for($loop = 0; $loop < count($type_arr); $loop++){
                if($type_arr[$loop] == 'tito_ahr'){
                    $with_ahr[$loop] = 1;
                }
                if($type_arr[$loop] == 'tito'){
                    $with_ahr[$loop] = 0;
                }
            }
            if(count($with_ahr) > 0){
                $ahr_bool = implode(',', $with_ahr);
                $where_arr[$where_count] = "AND tito.withAhr IN ($ahr_bool)";
                $where_count += 1;
            }
        }
        if(trim($status) != ''){
            $where_arr[$where_count] = "AND tito.status_ID IN ($status)";
            $where_count += 1;
        }else{
            $where_arr[$where_count] = "AND tito.status_ID IN (2, 12)";
            $where_count += 1;
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $qry="SELECT DISTINCT(emp.emp_id) empId, app.fname, app.mname, app.lname, app.nameExt, app.pic FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid ".$where." ORDER BY app.lname";
        return $this->general_model->custom_query($qry);
    } 
    private function qry_all_record_tito_request_emp($class, $acc_id, $type, $status, $start_date, $end_date, $emp_ids){
        $where = "";
        $where_arr = [];
        $where_count = 0;
        if($class != 'All'){
            $where_arr[$where_count] = " AND account.acc_description = '$class'";
            $where_count++;
        }
        if((int)$acc_id != 0){
            $where_arr[$where_count] = " AND emp.acc_id = '$acc_id'";
            $where_count++;
        }
        if(trim($emp_ids) != ""){
            $where_arr[$where_count] ="AND emp.emp_id IN ($emp_ids)";
            $where_count += 1;
        }
        if(trim($type) != ''){
            $type_arr = explode(',', $type);
            for($loop = 0; $loop < count($type_arr); $loop++){
                if($type_arr[$loop] == 'tito_ahr'){
                    $with_ahr[$loop] = 1;
                }
                if($type_arr[$loop] == 'tito'){
                    $with_ahr[$loop] = 0;
                }
            }
            if(count($with_ahr) > 0){
                $ahr_bool = implode(',', $with_ahr);
                $where_arr[$where_count] = "AND tito.withAhr IN ($ahr_bool)";
                $where_count += 1;
            }
        }
        if(trim($status) != ''){
            $where_arr[$where_count] = "AND tito.status_ID IN ($status)";
            $where_count += 1;
        }else{
            $where_arr[$where_count] = "AND tito.status_ID IN (5, 6)";
            $where_count += 1;
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $qry="SELECT DISTINCT(emp.emp_id) empId, app.fname, app.mname, app.lname, app.nameExt, app.pic FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid AND DATE(tito.dateRequest) >= '$start_date' AND DATE(tito.dateRequest) <= '$end_date' ".$where." ORDER BY app.lname";
        return $this->general_model->custom_query($qry);
    }
    private function qry_monitoring_access($emp_id){
        $fields = "titoMonitoringAccess_ID, access_type, assigned_to, added_by, updated_by, date_time_created";
        $where = "assigned_to = $emp_id";
        $table = "tbl_applicant app, tbl_employee emp, tbl_account acc";
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_tito_monitoring_access");
    }
    public function check_monitoring_access(){
        $emp_id = $this->session->userdata('emp_id');
        $data['access_type'] = '';
        $data['subordinates'] = '';
        $data['exist'] = 0;
        $monitoring_access = $this->qry_monitoring_access($emp_id);
        if(count($monitoring_access) > 0){
            $data['exist'] = 1;
            $data['access_type'] = $monitoring_access->access_type;
            if($monitoring_access->access_type == 'limited'){
                $emplist = $this->getAllEmployeeSubordinate($this->session->userdata('uid'));
                if(count($emplist) > 0);
                $data['subordinates'] = implode(',',array_column($emplist, 'emp_id'));
            }
        }
        return $data;
    }
    public function get_access_content(){
        $access = $this->check_monitoring_access();
        $content = "";
        if($access['access_type'] == 'admin'){
            $content =   '<div class="col-12">'.
                '<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">'.
                    '<div class="m-portlet__head">'.
                        '<div class="m-portlet__head-caption">'.
                            '<div class="m-portlet__head-title">'.
                                '<h3 class="m-portlet__head-text">Tito Monitoring Access</h3>'.
                            '</div>'.
                        '</div>'.
                        '<div class="m-portlet__head-tools">'.
                            '<ul class="m-portlet__nav">'.
                                '<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">'.
                                    '<a href="#" class="btn btn-outline-info m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air" id="open_assign_user">'.
                                        '<span>'.
                                            '<i class="fa fa-user-plus"></i>'.
                                            '<span>Assign</span>'.
                                        '</span>'.
                                    '</a>'.
                                '</li>'.
                            '</ul>'.
                        '</div>'.
                    '</div>'.
                    '<div class="m-portlet__body">'.
                        '<div class="row" id="access_filter">'.
                            '<div class="col-3 mb-3">'.
                                '<label for="example-text-input" class="col-form-label">Class</label>'.
                                '<div>'.
                                    '<select class="custom-select form-control" id="tito_access_class" name="tito_access_class">'.
                                        '<option value="All">ALL</option>'.
                                        '<option value="Agent">Ambassador</option>'.
                                        '<option value="Admin">SZ Family</option>'.
                                    '</select>'.
                                '</div>'.
                            '</div>'.
                            '<div class="col-4 mb-3">'.
                                '<label for="example-text-input" class="col-form-label">Account</label>'.
                                '<div>'.
                                    '<select class="form-control m-select2" id="tito_access_account" name="tito_access_account">'.
                                    '</select>'.
                                '</div>'.
                            '</div>'.
                            '<div class="col-5 mb-3">'.
                                '<label for="example-text-input" class="col-form-label">Employee</label>'.
                                '<div>'.
                                    '<select class="form-control m-select2" id="tito_access_employee" name="tito_access_employee">'.
                                    '</select>'.
                                '</div>'.
                            '</div>'.
                        '</div>'.
                        '<div class="row" id="assign_filter" style="display:none;">'.
                            '<div class="col-3">'.
                                '<label for="example-text-input" class="col-form-label">Class</label>'.
                                '<div>'.
                                    '<select class="custom-select form-control" id="tito_assign_class" name="tito_assign_class">'.
                                        '<option value="All">ALL</option>'.
                                        '<option value="Agent">Ambassador</option>'.
                                        '<option value="Admin">SZ Family</option>'.
                                    '</select>'.
                                '</div>'.
                            '</div>'.
                            '<div class="col-4">'.
                                '<label class="col-form-label">Account</label>'.
                                '<div>'.
                                    '<select class="form-control m-select2" id="tito_assign_account" name="tito_assign_account"></select>'.
                                '</div>'.
                            '</div>'.
                            '<div class="col-5">'.
                                '<label class="col-form-label">Select User</label>'.
                                '<div>'.
                                    '<select class="form-control m-bootstrap-select m_selectpicker-columns" id="tito_assign_employee" multiple data-live-search="true">'.
                                    '</select>	'.
                                '</div>'.
                            '</div>'.
                            '<div class="col-12 py-3">'.
                                '<a href="#" class="btn btn-outline-warning m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air assign_btn" id="close_assign_user">'.
                                    '<span>'.
                                        '<i class="fa fa-remove"></i>'.
                                        '<span>Cancel</span>'.
                                    '</span>'.
                                '</a>'.
                                '<a href="#" class="btn btn-outline-info m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air assign_btn" id="assign_user" style="display:none;">'.
                                    '<span>'.
                                        '<i class="fa fa-user-plus"></i>'.
                                        '<span>Assign</span>'.
                                    '</span>'.
                                '</a>'.
                            '</div>'.
                        '</div>'.
                        '<div class="row">'.
                            '<div class="col-12" id="tito_access_record_div">'.
                                '<div id="tito_access_datatable"></div>'.
                            '</div>'.
                        '</div>'.
                    '</div>'.
                '</div>'.
            '</div>';
        }
        echo json_encode($content);
    }
    public function get_user_monitoring_access(){
        $access = $this->check_monitoring_access();
        echo json_encode($access);
    }
    public function get_monitoring_pending_tito_acc(){
        $data['exist'] = 0;
        $class = $this->input->post('class');
        $access = $this->check_monitoring_access();
        $acc = [];
        if($access['exist']){
            $emp_ids = '';
            if($access['access_type'] == 'limited'){
                if(trim($access['subordinates']) != ""){
                    $emp_ids = $access['subordinates'];
                }else{
                    $emp_ids = 0;
                }
            }
            $acc = $this->qry_pending_tito_request_acc($class, "2, 12", $emp_ids);
        }
        if (count($acc) > 0) {
            $data['record'] = $acc;
            $data['exist'] = 1;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }
    public function get_monitoring_record_tito_acc(){
        $data['exist'] = 0;
        $class = $this->input->post('class');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $access = $this->check_monitoring_access();
        $acc = [];
        if($access['exist']){
            $emp_ids = '';
            if($access['access_type'] == 'limited'){
                if(trim($access['subordinates']) != ""){
                    $emp_ids = $access['subordinates'];
                }else{
                    $emp_ids = 0;
                }
            }
            $acc = $this->qry_record_tito_request_acc($class, $start_date, $end_date, $emp_ids);
        }
        if (count($acc) > 0) {
            $data['record'] = $acc;
            $data['exist'] = 1;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }
    public function get_monitoring_pending_tito_emp(){
        $data['exist'] = 0;
        $class = $this->input->post('class');
        $acc_id = $this->input->post('acc_id');
        $type = $this->input->post('type');
        $status = $this->input->post('status');
        $access = $this->check_monitoring_access();
        $emp = [];
        if($access['exist']){
            $emp_ids = '';
            if($access['access_type'] == 'limited'){
                if(trim($access['subordinates']) != ""){
                    $emp_ids = $access['subordinates'];
                }else{
                    $emp_ids = 0;
                }
            }
            $emp = $this->qry_all_pending_tito_request_emp($class, $acc_id, $type, $status, $emp_ids);
        }
        if (count($emp) > 0) {
            $data['record'] = $emp;
            $data['exist'] = 1;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }
    public function get_monitoring_record_tito_emp(){
        $data['exist'] = 0;
        $class = $this->input->post('class');
        $acc_id = $this->input->post('acc_id');
        $type = $this->input->post('type');
        $status = $this->input->post('status');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $access = $this->check_monitoring_access();
        $emp = [];
        if($access['exist']){
            $emp_ids = '';
            if($access['access_type'] == 'limited'){
                if(trim($access['subordinates']) != ""){
                    $emp_ids = $access['subordinates'];
                }else{
                    $emp_ids = 0;
                }
            }
            $emp = $this->qry_all_record_tito_request_emp($class, $acc_id, $type, $status, $start_date, $end_date, $emp_ids);
        }
        if (count($emp) > 0) {
            $data['record'] = $emp;
            $data['exist'] = 1;
        } else {
            $data['record'] = 0;
        }
        echo json_encode($data);
    }
    public function get_tito_pending_monitoring_datatable(){
        $user_id = $this->session->userdata('uid');
        $datatable = $this->input->post('datatable');
        $class = $datatable['query']['class'];
        $acc_id = $datatable['query']['acc_id'];
        $emp_id = $datatable['query']['emp_id'];
        $type = $datatable['query']['type'];
        $status = $datatable['query']['status'];
        $search = $datatable['query']['search'];
        $type_arr = [];
        $with_ahr = [];
        $where = "";
        $where_arr = [];
        $where_count = 0;
        $access = $this->check_monitoring_access();
        $emp = [];
        if($access['exist']){
            $emp_ids = '';
            if($access['access_type'] == 'limited'){
                if(trim($access['subordinates']) != ""){
                    $emp_ids = $access['subordinates'];
                }else{
                    $emp_ids = 0;
                }
            }
        }
        if($class != 'All'){
            $where_arr[$where_count] = " AND account.acc_description = '$class'";
            $where_count++;
        }
        if((int) $acc_id != 0){
            $where_arr[$where_count] = "AND emp.acc_id = $acc_id";
            $where_count += 1;
        }
        if((int) $emp_id != 0){
            $where_arr[$where_count] = "AND emp.emp_id = $emp_id";
            $where_count += 1;
        }else{
            if(trim($emp_ids) != ''){
                $where_arr[$where_count] = "AND emp.emp_id IN ($emp_ids)";
                $where_count += 1;
            }
        }  
        if(trim($type) != ''){
            $type_arr = explode(',', $type);
            for($loop = 0; $loop < count($type_arr); $loop++){
                if($type_arr[$loop] == 'tito_ahr'){
                    $with_ahr[$loop] = 1;
                }
                if($type_arr[$loop] == 'tito'){
                    $with_ahr[$loop] = 0;
                }
            }
            if(count($with_ahr) > 0){
                $ahr_bool = implode(',', $with_ahr);
                $where_arr[$where_count] = "AND tito.withAhr IN ($ahr_bool)";
                $where_count += 1;
            }
        }
        if(trim($status) != ''){
            $where_arr[$where_count] = "AND tito.status_ID IN ($status)";
            $where_count += 1;
        }else{
            $where_arr[$where_count] = "AND tito.status_ID IN (2, 12)";
            $where_count += 1;
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $query['query'] = "SELECT tito.dtrRequest_ID, emp.emp_id, tito.userID, app.fname, app.mname, app.lname, app.pic, tito.dateRequest, times.time_start, times.time_end, tito.status_ID, tito.dateCreated, tito.timetotal, stat.description FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid ".$where;
        if (trim($search) != '') {
            $keyword = $search;
            $date_bool = $this->check_if_date($keyword);
            // var_dump($date_bool);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "tito.dateRequest = '$date' OR tito.dateCreated LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "tito.dtrRequest_ID LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "tito.dtrRequest_ID = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function get_tito_record_monitoring_datatable(){
        $user_id = $this->session->userdata('uid');
        $datatable = $this->input->post('datatable');
        $class = $datatable['query']['class'];
        $acc_id = $datatable['query']['acc_id'];
        $emp_id = $datatable['query']['emp_id'];
        $type = $datatable['query']['type'];
        $status = $datatable['query']['status'];
        $search = $datatable['query']['search'];
        $start_date = $datatable['query']['start_date'];
        $end_date = $datatable['query']['end_date'];
        $type_arr = [];
        $with_ahr = [];
        $where = "";
        $where_arr = [];
        $where_count = 0;
        $access = $this->check_monitoring_access();
        $emp = [];
        if($access['exist']){
            $emp_ids = '';
            if($access['access_type'] == 'limited'){
                if(trim($access['subordinates']) != ""){
                    $emp_ids = $access['subordinates'];
                }else{
                    $emp_ids = 0;
                }
            }
        }
        if($class != 'All'){
            $where_arr[$where_count] = " AND account.acc_description = '$class'";
            $where_count++;
        }
        if((int) $acc_id != 0){
            $where_arr[$where_count] = "AND emp.acc_id = $acc_id";
            $where_count += 1;
        }
        if((int) $emp_id != 0){
            $where_arr[$where_count] = "AND emp.emp_id = $emp_id";
            $where_count += 1;
        }else{
            if(trim($emp_ids) != ''){
                $where_arr[$where_count] = "AND emp.emp_id IN ($emp_ids)";
                $where_count += 1;
            }
        } 
        if(trim($type) != ''){
            $type_arr = explode(',', $type);
            for($loop = 0; $loop < count($type_arr); $loop++){
                if($type_arr[$loop] == 'tito_ahr'){
                    $with_ahr[$loop] = 1;
                }
                if($type_arr[$loop] == 'tito'){
                    $with_ahr[$loop] = 0;
                }
            }
            if(count($with_ahr) > 0){
                $ahr_bool = implode(',', $with_ahr);
                $where_arr[$where_count] = "AND tito.withAhr IN ($ahr_bool)";
                $where_count += 1;
            }
        }
        if(trim($status) != ''){
            $where_arr[$where_count] = "AND tito.status_ID IN ($status)";
            $where_count += 1;
        }else{
            $where_arr[$where_count] = "AND tito.status_ID IN (5, 6)";
            $where_count += 1;
        }
        if(count($where_arr) > 0){
            $where = implode(' ', $where_arr);
        }
        $query['query'] = "SELECT tito.dtrRequest_ID, emp.emp_id, tito.userID, app.fname, app.mname, app.lname, app.pic, tito.dateRequest, times.time_start, times.time_end, tito.status_ID, tito.dateCreated, tito.timetotal, stat.description FROM tbl_time times INNER JOIN tbl_acc_time acc ON times.time_id = acc.time_id INNER JOIN tbl_dtr_request tito ON acc.acc_time_id = tito.acc_time_id INNER JOIN tbl_status stat ON stat.status_ID= tito.status_ID INNER JOIN tbl_user users ON users.uid = tito.userID INNER JOIN tbl_employee emp ON emp.emp_id = users.emp_id INNER JOIN tbl_account account ON account.acc_id = emp.acc_id INNER JOIN tbl_applicant app ON app.apid = emp.apid AND DATE(tito.dateRequest) >= '$start_date' AND DATE(tito.dateRequest) <= '$end_date'".$where;
        if (trim($search) != '') {
            $keyword = $search;
            $date_bool = $this->check_if_date($keyword);
            // var_dump($date_bool);
            if ((int) $date_bool == 1) {
                $date = $this->convert_date($keyword);
                $where = "tito.dateRequest = '$date' OR tito.dateCreated LIKE '%" . $date . "%'";
            } else {
                if (is_numeric($keyword)) {
                    $where = "tito.dtrRequest_ID LIKE '%" . (int) $keyword . "%'";
                } else {
                    $where = "tito.dtrRequest_ID = $keyword";
                }
            }
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    //monitoring record
    // Approval area
    private function update_values($data, $where, $table){
		return $this->general_model->update_vals($data, $where, $table);
    }
    private function set_tito_appr_deadline_status($status_id, $deadline_id){
        $data['status_ID'] = $status_id;
		$where = "deadline_ID = $deadline_id";
		return $this->general_model->update_vals($data, $where, "tbl_deadline");
    }
    private function qry_tito_request_details($request_id){
        $fields = "req.dtrRequest_ID, req.userID, emp.emp_id, app.fname, app.mname, app.lname, app.nameExt, req.dateRequest, req.acc_time_id, req.message, req.status_ID, req.dateCreated, req.dateUpdated, req.updatedBy, req.actualDate, req.startDateWorked, req.startTimeWorked, req.endDateWorked, req.endTimeWorked, req.timetotal, req.withAhr";
        $where = "app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = req.userID AND req.dtrRequest_ID IN ($request_id)";
        $table = "tbl_dtr_request req, tbl_applicant app, tbl_employee emp, tbl_user users";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }
    private function qry_tito_approval_details($request_id){
        $fields = "DISTINCT(appr.dtrRequestApproval_ID) dtrRequestApproval_ID, appr.approvalLevel, appr.userId, emp.emp_id, app.fname, app.lname, app.mname, app.nameExt, app.pic, appr.approvalStatus_ID, appr.dateTimeStatus, stat.description, appr.remarks, dead.deadline_ID, dead.deadline, positions.pos_details positionDescription, dead.status_ID, dead.datedStatus";
        $where = "dead.module_ID = 3 AND empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND dead.request_ID = appr.dtrRequest_ID AND dead.approvalLevel = appr.approvalLevel AND dead.userId = appr.userId AND app.apid = emp.apid AND emp.emp_id = users.emp_id AND users.uid = appr.userId AND stat.status_ID = appr.approvalStatus_ID AND appr.dtrRequest_ID IN ($request_id)";
        $table = "tbl_dtr_request_approval appr, tbl_employee emp, tbl_applicant app, tbl_user users, tbl_deadline dead, tbl_status stat, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }
    private function qry_tito_ahr_requests($tito_id){
        $fields = "additionalHourRequestId, additionalHourTypeId, userId";
        $where = "tito_ID = $tito_id";
        return $this->general_model->fetch_specific_vals($fields, $where, 'tbl_additional_hour_request');
    }
    private function transfer_tito_approvers_to_ahr($request_obj, $approval_obj){
        $approval_arr = [];
        $approval_arr_count = 0;
        $dead_arr = [];
        $ahr_obj = [];
        if($request_obj[0]->withAhr == 1){
            $ahr_obj = $this->qry_tito_ahr_requests($request_obj[0]->dtrRequest_ID);
        }
        if(count($ahr_obj) > 0){
            foreach($ahr_obj as $ahr_obj_row){
                foreach($approval_obj as $approval_obj_index=>$approval_obj_row){
                    $approval_arr[$approval_arr_count] = [
                        'additionalHourRequest_ID' => $ahr_obj_row->additionalHourRequestId,
                        'approvalLevel' => $approval_obj_row->approvalLevel,
                        'userId' => $approval_obj_row->userId,
                        'approvalStatus_ID' => 5,
                        'dateTimeStatus' => $approval_obj_row->dateTimeStatus,
                        'remarks' =>$approval_obj_row->remarks,
                    ];
                    $dead_arr[$approval_arr_count] = [
                        'module_ID' => 1,
                        'request_ID' => $ahr_obj_row->additionalHourRequestId,
                        'requestor' => $ahr_obj_row->userId,
                        'userId' => $approval_obj_row->userId,
                        'approvalLevel' => $approval_obj_row->approvalLevel,
                        'deadline' => $approval_obj_row->deadline,
                        'status_ID' => 3,
                        'datedStatus' => $approval_obj_row->dateTimeStatus,
                    ];
                    $approval_arr_count += 1;
                }
            }
        }
        if(count($approval_arr) > 0){
            $this->general_model->batch_insert($approval_arr, 'tbl_additional_hour_request_approval');
        }
        if(count($approval_arr) > 0){
            $this->general_model->batch_insert($dead_arr, 'tbl_deadline');
        }
    }
    private function setTitoDetailStatus($decision, $id,$schedule=null){
        $appr_dec['status_ID'] = $decision;
        $appr_dec_where = "dtrRequest_ID = ".$id;
        $appr_dec_table = "tbl_dtr_request";
        $this->update_values($appr_dec, $appr_dec_where, $appr_dec_table);// UPDATE STATUS

        // update AHR
        $data_ahr['schedId'] = $schedule;
        $data_ahr['requestType'] = "rendered";
        $data_ahr['approvalStatus_ID'] = $decision;
        $where_ahr = "tito_ID =".$id;
        $this->general_model->update_vals($data_ahr, $where_ahr, 'tbl_additional_hour_request');

    }
    private function set_tito_approval_approved($request_obj, $approval_obj, $current_approver_obj,$isWord){
        $data['error'] = 0;
        $data['description'] = "";
        $data['record'] = "";
        $error_count = 0;
        $with_error_bool = 0;
        // var_dump($approval_obj);
        // var_dump($current_approver_obj);
        // var_dump($approval_obj);
        $max_appr_level = count($approval_obj);
        // var_dump($max_appr_level);
        // var_dump($approval_obj);
        if($max_appr_level <= intval($current_approver_obj[0]->approvalLevel)){
            $rs = $this->set_final_tito_request($request_obj, $approval_obj,$isWord);
			if($rs>0){
				$this->setTitoDetailStatus(5,$request_obj[0]->dtrRequest_ID,$rs);
				$this->transfer_tito_approvers_to_ahr($request_obj, $approval_obj);
				$sys_notif_mssg = "Your TITO request was successfully APPROVED<br><small><b>TITO-ID</b>: " . str_pad($request_obj[0]->dtrRequest_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = "tito/request";
                $this->simple_system_notification($sys_notif_mssg, $link, $request_obj[0]->userID);
                $data['record'] = $rs;
			}else{
				$data['error'] = 1;
			}
        }else{// PROCEED TO NEXT APPROVER
            $next_level = $current_approver_obj[0]->approvalLevel + 1;
            $next_approver_obj = array_merge(array_filter($approval_obj, function ($e) use ($next_level) {//get current approver details
                    return $e->approvalLevel == $next_level;
                }
            ));
            $appr_stat_data['approvalStatus_ID'] = 4;
            $appr_stat_where = "dtrRequestApproval_ID = ".$next_approver_obj[0]->dtrRequestApproval_ID;
            $appr_stat_table = "tbl_dtr_request_approval";
            $next_appr_stat = $this->update_values($appr_stat_data, $appr_stat_where, $appr_stat_table);// UPDATE STATUS
            if(!$next_appr_stat){
                $data['error'] = 1;
                $data['description'] = 'ERROR IN SETTING NEXT APPROVER AS CURRENT APPROVER';
            }
            //SEND NOTIFICATION TO NEXT APPROVER
            $approval_level_ordinal = $this->ordinal($next_approver_obj[0]->approvalLevel);
            $notif_mssg = "forwarded to you the TITO request of <strong>" . $request_obj[0]->fname . " " . $request_obj[0]->lname . "</strong> for $approval_level_ordinal Approval.<br><small><b>TITO-ID</b>: " . str_pad($request_obj[0]->dtrRequest_ID, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "tito/approval";
            $next_approver_uid[0] = ['userId' => $next_approver_obj[0]->userId];
            $this->set_notif($current_approver_obj[0]->userId, $notif_mssg, $link, $next_approver_uid);
        }
        return $data;
    }
    public function set_tito_approval_disapproved($request_obj, $approval_obj, $current_approver_obj){
        $data['error'] = 0;
        $data['description'] = "";
        $max_appr_level = count($approval_obj);
        $this->setTitoDetailStatus(6,$request_obj[0]->dtrRequest_ID);
        // if($max_appr_level <= $current_approver_obj[0]->approvalLevel){
        //     var_dump('last approver');
        // }else{
            $current_appr_level = $current_approver_obj[0]->approvalLevel;
            $succeeding_approver_obj = array_merge(array_filter($approval_obj, function ($e) use ($current_appr_level) {//get current approver details
                    return $e->approvalLevel > $current_appr_level;
                }
            ));
            if(count($succeeding_approver_obj) > 0){
                $succeeding_appr_id = implode(',', array_column($succeeding_approver_obj, 'dtrRequestApproval_ID'));
                $succeeding_dead_id = implode(',', array_column($succeeding_approver_obj, 'deadline_ID'));
                //SET SUCCEEDING APPROVERS' APPROVAL TO CANCELLED
                $appr_stat_data['approvalStatus_ID'] = 7;
                $appr_stat_where = "dtrRequestApproval_ID IN ($succeeding_appr_id)";
                $appr_stat_table = "tbl_dtr_request_approval";
                $cancel_appr_stat = $this->update_values($appr_stat_data, $appr_stat_where, $appr_stat_table);// UPDATE STATUS
                //SET SUCCEEDING APPROVER DEADLINE TO CANCELLED
                $appr_dead_data['status_ID'] = 7;
                $appr_dead_where = "deadline_ID IN ($succeeding_dead_id)";
                $appr_dead_table = "tbl_deadline";
                $cancel_appr_dead_stat = $this->update_values($appr_dead_data, $appr_dead_where, $appr_dead_table);// UPDATE DEADLINE
                if(!$cancel_appr_stat){
                    $data['error'] = 1;
                    $data['description'] .= 'ERROR IN SETTING NEXT APPROVER AS CURRENT APPROVER';
                }
                if(!$cancel_appr_dead_stat){
                    $data['error'] = 1;
                    $data['description'] .= 'ERROR IN SETTING NEXT APPROVER AS CURRENT APPROVER';
                }
            }
        // }
        return $data;
    } 
    public function set_tito_approval($request_id = 1, $approval_stat = 5, $approval_id = 2,$appr_message="",$isWord=0, $isMonitoring = 0){
        $data['error'] = 0;
        $data['description'] = [];
        $error_count = 0;
        $with_error_bool = 0;
        $current_appr_stat = 0;
        $current_appr_dead_stat = 0;
        $status = "Approved";
        $request_obj = $this->qry_tito_request_details($request_id); // get request details
        $approval_obj = $this->qry_tito_approval_details($request_id);// get approval details
        // var_dump($approval_obj);
        $current_approver_obj = array_merge(array_filter($approval_obj, function ($e) use ($approval_id) {//get current approver details
                return $e->dtrRequestApproval_ID == $approval_id;
            }
        ));
        if(count($request_obj) < 1){
            $with_error_bool = 1;
            $data['description'][$error_count] = 'TITO REQUEST DETAILS NOT FOUND';
            $error_count +=1;
        }
        if(count($approval_obj) < 1){
            $with_error_bool = 1; 
            $data['description'][$error_count] = 'TITO REQUEST APPROVAL DETAILS NOT FOUND';
            $error_count +=1;
        }
        if(count($current_approver_obj) < 1){
            $with_error_bool = 1; 
            $data['description'][$error_count] = 'CURRENT APPROVER DETAILS NOT FOUND';
            $error_count +=1;
        }
        if(!$with_error_bool){// If no error in fetching all data
            //SET APPROVAL OF CURRENT APPROVER
            $appr_stat_data['approvalStatus_ID'] = $approval_stat;
            $appr_stat_data['remarks'] = $appr_message;
            $appr_stat_where = "dtrRequestApproval_ID = $approval_id";
            $appr_stat_table = "tbl_dtr_request_approval";
            $current_appr_stat = $this->update_values($appr_stat_data, $appr_stat_where, $appr_stat_table);// UPDATE STATUS
            //SET DEADLINE OF CURRENT APPROVER
            if(!$isMonitoring){
                $appr_dead_data['status_ID'] = 3;
                $appr_dead_where = "deadline_ID = ".$current_approver_obj[0]->deadline_ID;
                $appr_dead_table = "tbl_deadline";
                $current_appr_dead_stat = $this->update_values($appr_dead_data, $appr_dead_where, $appr_dead_table);// UPDATE DEADLINE
            }
            if($approval_stat == 6){
                $status = "Disapproved"; 
            }
            //SEND NOTIFICATION TO REQUESTOR ABOUT THE APPROVAL OF THE CURRENT APPROVER
            $link = "tito/request";
            $requestors[0] = ['userId' => $request_obj[0]->userID];
            $notif_mssg =  $status." your TITO request.<br><small><b>TITO-ID</b>: " . str_pad($request_obj[0]->dtrRequest_ID, 8, '0', STR_PAD_LEFT) . " </small>";
            $this->set_notif($current_approver_obj[0]->userId, $notif_mssg, $link, $requestors);
            if(!$current_appr_stat){
                $with_error_bool = 1; 
                $data['description'][$error_count] = 'ERROR IN UPDATING STATUS OF CURRENT APPROVER';
                $error_count +=1;
            }
            if(!$current_appr_dead_stat){
                $with_error_bool = 1; 
                $data['description'][$error_count] = 'ERROR IN UPDATING DEADLINE STATUS OF CURRENT APPROVER';
                $error_count +=1;
            }
            if($approval_stat == 5){ // APPROVED
                $approved = $this->set_tito_approval_approved($request_obj, $approval_obj, $current_approver_obj,$isWord);
                if(!$approved['error']){
                    $data['record'] = $approved['record'];
                }else{
                    $data['error'] = 1;
                    $data['description'][$error_count] = $approved['description'];
                    $error_count +=1;
                }
             }else{// DISAPPROVED
                $data['record'] = $this->set_tito_approval_disapproved($request_obj, $approval_obj, $current_approver_obj);
             }
        }else{
            $data['error'] = 1;
        }
        return $data;      
    }
    public function get_tito_approver_info(){
        $data['exist'] = 0;
        $tito_id = $this->input->post('titoId');
        $approval = $this->qry_tito_approval_details($tito_id);
        // var_dump($approval);
        if(count($approval) > 0){
            $data['exist'] = 1;
            $data['record'] = $approval;
        }
        echo json_encode($data);
    }
    function checkTotalTimePreShiftOT(){
       $start = (date_format(date_create($this->input->post("timeStart")),"H:i")=="00:00") ? strtotime("+1 minutes", strtotime(date_format(date_create($this->input->post("timeStart")),"Y-m-d")." 11:59 PM")) : strtotime($this->input->post("timeStart"));
        // $end = strtotime($this->input->post("timeEnd"));
        $end = (date_format(date_create($this->input->post("timeEnd")),"H:i")=="00:00") ?  date_format(date_create($this->input->post("timeEnd")),"Y-m-d")." 24:00" : $this->input->post("timeEnd");

        // $end = strtotime($this->input->post("timeEnd"));
		 $end = strtotime( $end);
        $start_text = (date_format(date_create($this->input->post("timeStart")),"H:i")=="00:00") ?  date_format(date_create($this->input->post("timeStart")),"Y-m-d")." 24:00" : $this->input->post("timeStart");
        $end_text = $this->input->post("timeEnd");
        if(strtotime($this->input->post("timeEnd"))==strtotime($this->input->post("timeStart"))){
            $seconds_diff =  0 ;
        }else{
            $seconds_diff =  $end - $start;
        }
        $totalHours = ($seconds_diff<1  ) ? 0 : number_format($seconds_diff/3600, 2);
        
        echo $totalHours."_".$this->decimal_to_time($totalHours)."_".$start_text."_".$end_text."_".$seconds_diff;
        
    }
	#MJM Start Code
	function addSchedule($emp_id,$sched_date,$acc_time_id,$withAhr,$isWord){
		$dtr['emp_id'] = $emp_id;
		$dtr['sched_date'] = $sched_date;
		$dtr['acc_time_id'] =  $acc_time_id;
		$dtr['schedtype_id'] =  $isWord;
		$dtr['remarks'] =  "TITO";
		$dtr['updated_by'] = $_SESSION["uid"];
		$dtr['isActive'] =1;
		$dtr['isDouble'] =0;
		if(intval($withAhr)==1){
			$dtr['isLocked'] =14; //311272
		}
		return $this->general_model->insert_vals_last_inserted_id($dtr, 'tbl_schedule');
	}
	private function set_final_tito_request($request_obj,$approvers=null,$isWord){
		// echo json_encode($request_obj);
		$checkSchedid  = $this->checkSchedID($request_obj[0]->dateRequest, $request_obj[0]->userID);
		$login = date_format(date_create($request_obj[0]->startDateWorked." ".$request_obj[0]->startTimeWorked),"Y-m-d H:i:s");
		$logout = date_format(date_create($request_obj[0]->endDateWorked." ".$request_obj[0]->endTimeWorked),"Y-m-d H:i:s");
		
		$schedule = $this->addSchedule($request_obj[0]->emp_id,$request_obj[0]->dateRequest,$request_obj[0]->acc_time_id,$request_obj[0]->withAhr,$isWord);
		
		$in = $this->dtrInEntry("DTR", $request_obj[0]->acc_time_id, $request_obj[0]->emp_id,$schedule,$login,"I");
        $out = $this->dtrOutEntry("DTR", $request_obj[0]->acc_time_id, $request_obj[0]->emp_id, $schedule,$logout,"O",$in);
		
		
        return intval($schedule);
     }
     function dtrInEntry($type, $acc_time_id,$emp_id,$sched_id,$log,$entry){
        date_default_timezone_set('Asia/Manila');
        $dtr['type'] = $type;
        $dtr['acc_time_id'] = $acc_time_id;
        $dtr['emp_id'] =  $emp_id;
        $dtr['sched_id'] =  $sched_id;
        $dtr['log'] =  $log;
        $dtr['entry'] = $entry;
        $dtr['info'] = "TITO";
        $violation_late = 1;
        $sched = $this->general_model->custom_query("Select sched_id,a.acc_time_id,time_start,time_end,sched_date from tbl_schedule a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and sched_id=" . $sched_id);
        $time2 = ($sched[0]->time_start == "12:00:00 AM") ? "24:00" : date_format(date_create($sched[0]->time_start), 'H:i');
        $date1 = date_format(date_create($log), 'Y-m-d H:i');
        $date2 = $sched[0]->sched_date . " " . $time2;
        $idIn = $this->general_model->insert_vals_last_inserted_id($dtr, 'tbl_dtr_logs');
        if (strtotime($date1) > strtotime($date2)) { //Late
            // $supervisor = $this->get_direct_supervisor_via_emp_id($_SESSION["emp_id"]);
            $uidd = $this->get_emp_details_via_emp_id($emp_id);
            $violation_late = $this->adddataViolation(1, $uidd->Supervisor, $sched_id, $sched[0]->sched_date, $uidd->uid);
            if($violation_late['exist']){
                $this->check_qualified_dtr_violation($violation_late['record']);
            }
        }
        return $idIn;
    }
    function dtrOutEntry($type, $acc_time_id,$emp_id,$sched_id,$log,$entry,$in){
        date_default_timezone_set('Asia/Manila');
        $dtr['type'] = $type;
        $dtr['acc_time_id'] = $acc_time_id;
        $dtr['emp_id'] =  $emp_id;
        $dtr['sched_id'] =  $sched_id;
        $dtr['log'] =  $log;
        $dtr['entry'] = $entry;
        $dtr['note'] = $in;
        $dtr['info'] = "TITO";
        $violation_ut = 1;
        $sched = $this->general_model->custom_query("Select sched_id,a.acc_time_id,time_start,time_end,sched_date from tbl_schedule a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and sched_id=" . $sched_id);
        $time2 = ($sched[0]->time_end == "12:00:00 AM") ? "24:00" : date_format(date_create($sched[0]->time_end), 'H:i');
        $date1 = date_format(date_create($log), 'Y-m-d H:i');
        $shiftIN = date_format(date_create($sched[0]->time_start), 'H:i');
        $shiftOut = date_format(date_create($sched[0]->time_end), 'H:i');
        $ampm = explode("|", $this->getTimeGreeting($shiftIN, $shiftOut));
        // var_dump($sched);
        // $ampm = explode("|", $this->getTimeGreeting($sched[0]->time_start, $sched[0]->time_end));
        // var_dump($ampm);
        $date2 = ($ampm[1] == "Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($sched[0]->sched_date))) : $sched[0]->sched_date;
        $date2 .= " " . $time2;
        $out_id = $this->general_model->insert_vals_last_inserted_id($dtr, 'tbl_dtr_logs');
        // var_dump($date1);
        // var_dump($date2);
        if (strtotime($date1) < strtotime($date2)) { // Undertime
            // $supervisor = $this->get_direct_supervisor_via_emp_id($_SESSION["emp_id"]);
            $uidd = $this->get_emp_details_via_emp_id($emp_id);
            // var_dump($supervisor);
            $violation_ut = $this->adddataViolation(2, $uidd->Supervisor, $sched_id, $sched[0]->sched_date, $uidd->uid);
            if($violation_ut['exist']){
                $this->check_qualified_dtr_violation($violation_ut['record']);
            }
        }
        // echo $date1." ".$date2." ".$violation_ut;
        // die();
        return $out_id;
    }
	private function checkSchedID($dateRequest,$requestor){
		$query = "SELECT distinct(c.sched_id) as sched_id,c.schedtype_id  FROM tbl_dtr_request a,tbl_user b left join tbl_schedule c on b.emp_id=c.emp_id where a.userID=b.uid and b.uid=$requestor and dateRequest='".$dateRequest."' and sched_date='".$dateRequest."' and c.schedtype_id in (1,4,5)";
		$rs = $this->general_model->custom_query($query);
		$delete_stat =0;
		if(count($rs)>0){
			foreach($rs as $row){
				$details = $this->totalHours($row->sched_id,"custom");
				 
				if(count($details)>0){
					if($details["remaining_hour"]>0){
						$where['sched_id'] = $row->sched_id;
						$where['isLocked'] = 0;
						$delete_stat = $this->general_model->delete_vals($where, 'tbl_schedule');  
						if($delete_stat>0){ // Checks if schedule was successfully deleted. Set sched_id in DTR to null.
							$data_dtr_sched['sched_id'] = NULL;
							$where_dtr_sched = "sched_id=".$row->sched_id;
							$this->general_model->update_vals($data_dtr_sched, $where_dtr_sched, 'tbl_dtr_logs');
						}							
					}
				}
			}
		}else{
			$query_user = "SELECT emp_id from tbl_user where uid=".$requestor;
			$rs_user = $this->general_model->custom_query($query_user);
			$where['sched_date'] = $dateRequest;
			$where['emp_id'] = $rs_user[0]->emp_id;
			$where['isLocked'] = 0;
			$delete_stat = $this->general_model->delete_vals($where, 'tbl_schedule');   
		}
		return $delete_stat;
	}
	function checkTimeOfShift(){
		$shiftIN =  date_format(date_create($this->input->post("timeStart")), 'H:i');
		$shiftOUT = date_format(date_create($this->input->post("timeEnd")), 'H:i');
		$shiftOUT = ($shiftOUT=="00:00") ? "23:59" : $shiftOUT ;
		$ampm = explode("|",$this->getTimeGreeting($shiftIN,$shiftOUT));
			echo $ampm[0]."|".$ampm[1];
	}
    function checkTotalTimeAfterShiftOT(){
        $start= (date_format(date_create($this->input->post("timeStart")),"H:i")=="00:00") ? strtotime(date_format(date_create($this->input->post("timeStart")),"Y-m-d")." 24:00") : strtotime($this->input->post("timeStart"));
		// $end= (date_format(date_create($this->input->post("timeEnd")),"H:i")=="00:00") ? strtotime("+1 minutes", strtotime(date_format(date_create($this->input->post("timeEnd")),"Y-m-d")." 11:59 PM")) : strtotime($this->input->post("timeEnd"));
		$end= (date_format(date_create($this->input->post("timeEnd")),"H:i")=="00:00") ? strtotime(date_format(date_create($this->input->post("timeEnd")),"Y-m-d")." 24:00") : strtotime($this->input->post("timeEnd"));
        // $end= strtotime($this->input->post("timeEnd"));
        $acctimeid= floatVal($this->input->post("acctimeid"))*60;
        $start_text= (date_format(date_create($this->input->post("timeStart")),"H:i")=="00:00") ? date_format(date_create($this->input->post("timeStart")),"Y-m-d")." 24:00" : $this->input->post("timeStart");		
        $end_text= (date_format(date_create($this->input->post("timeEnd")),"H:i")=="00:00") ? date_format(date_create($this->input->post("timeEnd")),"Y-m-d")." 24:00" : $this->input->post("timeEnd");
        // $end_text= $this->input->post("timeEnd");        
         // if($end<=$start){
            // $seconds_diff =  0 ;
        // }else{
            $seconds_diff =  $end - $start; 
        // }
        // echo $start;
        $totalHours = number_format($seconds_diff/3600, 2);
        $totalHours_withBreak = number_format(($seconds_diff-$acctimeid)/3600, 2);       
        echo $totalHours."_".$this->decimal_to_time($totalHours)."_".$this->decimal_to_time($totalHours_withBreak)."_".$start_text."_".$end_text; 
    }
function checkTotalTime(){
        $start= (date_format(date_create($this->input->post("timeStart")),"H:i")=="00:00") ? strtotime(date_format(date_create($this->input->post("timeStart")),"Y-m-d")." 24:00:00") : strtotime($this->input->post("timeStart"));
        // $end= strtotime($this->input->post("timeEnd"));
		$end= (date_format(date_create($this->input->post("timeEnd")),"H:i")=="00:00") ? strtotime("+1 minutes", strtotime(date_format(date_create($this->input->post("timeEnd")),"Y-m-d")." 11:59 PM")) : strtotime($this->input->post("timeEnd"));
        $acctimeid= floatVal($this->input->post("acctimeid"))*60;
        $seconds_diff =  $end - $start; 
        // echo $start;
        $totalHours = number_format($seconds_diff/3600, 2);
        $totalHours_withBreak = number_format(($seconds_diff-$acctimeid)/3600, 2);
        echo $totalHours."_".$this->decimal_to_time($totalHours)."_".$this->decimal_to_time($totalHours_withBreak)."_".$totalHours_withBreak;
}
    function checkWordTotal(){
        $origdatestart= (date_format(date_create($this->input->post("origdatestart")),"H:i")=="00:00") ? strtotime(date_format(date_create($this->input->post("origdatestart")),"Y-m-d")." 24:00:00") : strtotime($this->input->post("origdatestart"));
        // $origdatestart= date("Y-m-d h:i A",$origdatestart);
        // $origdateend = $this->input->post("origdateend");
		$origdateend= (date_format(date_create($this->input->post("origdateend")),"H:i")=="00:00") ? strtotime(date_format(date_create($this->input->post("origdateend")),"Y-m-d")." 24:00:00") : strtotime($this->input->post("origdateend"));
		$newdatestart= (date_format(date_create($this->input->post("newdatestart")),"H:i")=="00:00") ? strtotime(date_format(date_create($this->input->post("newdatestart")),"Y-m-d")." 24:00:00") : strtotime($this->input->post("newdatestart"));

        // $newdatestart = $this->input->post("newdatestart");
        // $newdateend = $this->input->post("newdateend");
		$newdateend= (date_format(date_create($this->input->post("newdateend")),"H:i")=="00:00") ? strtotime(date_format(date_create($this->input->post("newdateend")),"Y-m-d")." 24:00:00") : strtotime($this->input->post("newdateend"));
		// $newdateend=  date("Y-m-d h:i A",$newdateend);

        $acctimeid= floatVal($this->input->post("acctimeid"))*60;
        $startWord = ($newdatestart <= $origdatestart) ? $origdatestart : $newdatestart ;
        $endWord = ($origdateend >= $newdateend) ? $newdateend : $origdateend ;
		$startWord_text = ($newdatestart <= strtotime($origdatestart)) ? (date_format(date_create($this->input->post("origdatestart")),"H:i")=="00:00") ?  date_format(date_create($this->input->post("origdatestart")),"Y-m-d")." 24:00" :  $this->input->post("origdatestart") : $newdatestart ;
		$endWord_text = ($origdateend >= $newdateend) ? $newdateend : $origdateend;
        $seconds_diff =  $endWord - $startWord; 
        $totalHours = number_format($seconds_diff/3600, 2);
        $totalHours_withBreak = number_format(($seconds_diff-$acctimeid)/3600, 2);
        echo $totalHours_withBreak."_".$this->decimal_to_time($totalHours)."_".$this->decimal_to_time($totalHours_withBreak)."_".$startWord_text."_".$endWord_text."_".date("Y-m-d H:i",$endWord)."----".date("Y-m-d H:i",$startWord);
        // echo $totalHours."_".$totalHours."_".$totalHours_withBreak;
    }
    function decimal_to_time($decimal) {
        $hours = floor($decimal);
        $minutes = round($decimal * 60 )% 60;  
        return str_pad($hours, 2, "0", STR_PAD_LEFT) . " hr, " . $minutes. " min";
    }
function saveTitoDetails() {
    error_reporting(E_ALL ^ E_WARNING); 
     date_default_timezone_set("Asia/Manila");
     $dateStart = $this->input->post('dateStart');
     $dateTimeStart = $this->input->post("dateTimeStart");
     $dateTimeEnd = $this->input->post("dateTimeEnd");
     $acc_time_id = $this->input->post("acc_time_id");
     $message = $this->input->post("tito_reason");
     $checkPre = $this->input->post("checkPre");
     $checkAfter = $this->input->post("checkAfter");
     $checkWord = $this->input->post("checkWord");
     $newtotal = $this->input->post("newtotal");
     $withAhr = 0;
        if($checkWord=="true" || $checkAfter=="true" || $checkPre=="true"){
            $withAhr = 1;
        }
         $arr = array(
            "dateRequest" => $dateStart,
            "acc_time_id" => $acc_time_id,
            "message" => $message,
            "actualDate" => $dateStart,
            "startDateWorked" => date("Y-m-d",strtotime($dateTimeStart)),
            "startTimeWorked" => date("h:i A",strtotime($dateTimeStart)),
            "endDateWorked" => date("Y-m-d",strtotime($dateTimeEnd)),
            "endTimeWorked" => date("h:i A",strtotime($dateTimeEnd)),
            "timetotal" => $newtotal,
            "withAhr" => $withAhr,
            "userID" => $_SESSION["uid"],
            "updatedBy" => $_SESSION["uid"],
         );
    //echo json_encode($arr);
         $dtrRequest_ID = $this->general_model->insert_vals_last_inserted_id($arr, "tbl_dtr_request"); 
           if ($dtrRequest_ID === 0){
            $status = "Failed";
            echo 0;
        }else{
            $status = "Success";
            $createdOn = date("Y-m-d H:i:s");
            $approvers = $this->set_approvers($createdOn,$_SESSION["uid"],3,'dtrRequest_ID',$dtrRequest_ID);
            $insert_stat['insert_approvers'] = $this->general_model->batch_insert($approvers, 'tbl_dtr_request_approval');
                // SET NOTIFICATION
            $notif_mssg = "has sent you a Time-in & Time-out request for ".date("F d, Y",strtotime($this->input->post("dateStart")))." shift. <br><small><b>TITO-ID</b>: " . str_pad($dtrRequest_ID, 8, '0', STR_PAD_LEFT) . " </small>";
            $link = "tito/approval";
            $notif_details = $this->set_notif($_SESSION["uid"], $notif_mssg, $link, $approvers);
            // echo json_encode($notif_details);
            echo $dtrRequest_ID;
        }  
        if($checkAfter=="true"){			
            $otafter = $this->input->post("otafter");
            $otStartDate = date("Y-m-d",strtotime($this->input->post("otafterStart")));
            $time_start = date("H:i",strtotime($this->input->post("otafterStart")));
            $otEndDate = date("Y-m-d",strtotime($this->input->post("otafterEnd")));
            $time_end = date("H:i",strtotime($this->input->post("otafterEnd")));
            $this->fileAHR(1,$otStartDate,$time_start,$otEndDate,$time_end,$otafter,$message,$dtrRequest_ID);
            // echo json_encode($after);
        }
        if($checkPre=="true"){
            $otpre = $this->input->post("otpre");
            $otStartDate = date("Y-m-d",strtotime($this->input->post("otpreStart")));
            $time_start = date("H:i",strtotime($this->input->post("otpreStart")));
            $otEndDate = date("Y-m-d",strtotime($this->input->post("otpreEnd")));
            $time_end = date("H:i",strtotime($this->input->post("otpreEnd")));
            $this->fileAHR(2,$otStartDate,$time_start,$otEndDate,$time_end,$otpre,$message,$dtrRequest_ID);
          // echo json_encode($before);
        }
        if($checkWord=="true"){
            $otword = $this->input->post("otword");
            #if 12MN
            $timeS = date("H:i",strtotime($this->input->post("otwordStart")));
            if(trim($timeS)=="00:00"){
                $start = explode(" ",$this->input->post("otwordStart"));
                  $otStartDate = $start[0];
                  $time_start = "24:00";
            }else{
                 $otStartDate = date("Y-m-d",strtotime($this->input->post("otwordStart")));
                  $time_start = $timeS;
            }
            $otEndDate = date("Y-m-d",strtotime($this->input->post("otwordEnd")));
            $time_end = date("H:i",strtotime($this->input->post("otwordEnd")));
            $this->fileAHR(3,$otStartDate,$time_start,$otEndDate,$time_end,$otword,$message,$dtrRequest_ID);
             // echo json_encode($word);
        }
    }
    function fileAHR($ahrType,$otStartDate,$time_start,$otEndDate,$time_end,$otHours,$reason,$dtrRequest_ID){
		if(floatVal($otHours)>0){
			$ot['userId'] = $_SESSION["uid"];
			$ot['additionalHourTypeId'] = $ahrType;
			$ot['schedId'] = null;
			$ot['otStartDate'] = $otStartDate;
			$ot['otStartTime'] = $time_start;
			$ot['otEndDate'] = $otEndDate;
			$ot['otEndTime'] = $time_end;
			$ot['totalOtHours'] = $otHours;
			$ot['reason'] = $reason;
			$ot['tito_ID'] = $dtrRequest_ID;
			$ahr_id = $this->general_model->insert_vals_last_inserted_id($ot, "tbl_additional_hour_request");
			return $ahr_id;
		}
        return 0;
        //return $ot;
    }
	function getTitoAhr($dtrRequest_ID){
        $fields = "*";
        $where = "tito_ID=$dtrRequest_ID";
        $tables = "tbl_additional_hour_request";
       return $this->general_model->fetch_specific_vals($fields, $where, $tables);
	}
	function checkTitoRequest(){
		#SELECT * FROM  tbl_dtr_request a,tbl_additional_hour_request b where a.dtrRequest_ID=b.tito_ID and  a.dtrRequest_ID=8795
        $dtrRequest_ID = $this->input->post("dtrRequest_ID");
        $fields = "*";
        $where = "dtrRequest_ID=$dtrRequest_ID";
        $tables = "tbl_dtr_request a";
        $res = $this->general_model->fetch_specific_vals($fields, $where, $tables);
		$arr = array();
		foreach($res as $row){
			$ahr = $this->getTitoAhr($row->dtrRequest_ID);
			$getAccount = $this->getAccountMjm($row->acc_time_id);
			$arr = array(
				"acc_id" => $getAccount[0]->acc_id,
				"acc_time_id" => $row->acc_time_id,
				"actualDate" => $row->actualDate,
				"dtrRequest_ID" => $row->dtrRequest_ID,
				"dateRequest" => $row->dateRequest,
				"endDateWorked" => $row->endDateWorked,
				"endTimeWorked" => $row->endTimeWorked,
				"message" => $row->message,
				"startDateWorked" => $row->startDateWorked,
				"startTimeWorked" => $row->startTimeWorked,
				"status_ID" => $row->status_ID,
				"timetotal" => $row->timetotal,
				"userID" => $row->userID,
				"withAhr" => $row->withAhr,
				"withAhrRecord" => $ahr,
 			);
		}
		echo json_encode($arr);
    }
    function getAccountMjm($id){
         $fields = "acc_id";
        $where = "acc_time_id=$id";
        $tables = "tbl_acc_time";
       return $this->general_model->fetch_specific_vals($fields, $where, $tables);
	}
    function requestdetails($id){
        $request_obj = $this->qry_tito_request_details($id); // get request details
        echo "<pre>";
        var_dump($request_obj);
        echo "</pre>";
    }
	function titoRequestAct(){
        $approval_stat =	$this->input->post("act");
        $appr_message =	$this->input->post("appr_message");
		$checkAfter =	$this->input->post("checkAfter");
		$ahridAfter =	$this->input->post("ahridAfter");
		$checkPre =	$this->input->post("checkPre");
		$ahridPre =	$this->input->post("ahridPre");
		$checkWord =	$this->input->post("checkWord");
		$ahridWord =	$this->input->post("ahridWord");
		$request_id =	$this->input->post("tito_ID");
		$approval_id =	$this->input->post("approverid");
		$isMonitoring =	$this->input->post("isMonitoring");
		//echo $request_id." ".$approval_stat." ".$approval_id;
		
		if((int) $approval_id == 0){
            $approval_obj = $this->qry_tito_approval_details($request_id);// get approval details
            $request_obj = $this->qry_tito_request_details($request_id);
			$max_approver = count($approval_obj)+1;
			$monitoring_approver['dtrRequest_ID'] = $request_id;
			$monitoring_approver['approvalLevel'] = $max_approver;
			$monitoring_approver['userId'] = $_SESSION["uid"];
			$monitoring_approver['approvalStatus_ID'] = 4;
			$monitoring_approver['dateTimeStatus'] = date("Y-m-d H:i:s");
			$monitoring_approver['remarks'] = $appr_message;
            $approval_id = $this->general_model->insert_vals_last_inserted_id($monitoring_approver, "tbl_dtr_request_approval");
            $deadline_detatils['module_ID'] = 3;
            $deadline_detatils['request_ID'] = $request_id;
            $deadline_detatils['requestor'] = $request_obj[0]->userID;
            $deadline_detatils['userId'] = $_SESSION["uid"];
            $deadline_detatils['approvalLevel'] = $max_approver;
            $deadline_detatils['deadline'] = date("Y-m-d H:i:s");
            $deadline_detatils['status_ID'] = 3;
            $deadline_detatils['datedStatus'] = date("Y-m-d H:i:s");
            $this->general_model->insert_vals($deadline_detatils, "tbl_deadline");
            // var_dump($approval_id);
		}
		$ahr_ids = array(0);
		$isWord = 1;
		if($checkAfter=="true"){
			array_push($ahr_ids, $ahridAfter);
		}
		if($checkPre=="true"){
			array_push($ahr_ids, $ahridPre);
		}
		if($checkWord=="true"){
			array_push($ahr_ids, $ahridWord);
			$isWord=4;
		}
        $where = "additionalHourRequestId NOT IN (".implode(",",$ahr_ids).") and tito_ID=$request_id";
		$this->general_model->delete_vals($where, 'tbl_additional_hour_request');
		
        $data = $this->set_tito_approval($request_id, $approval_stat, $approval_id,$appr_message,$isWord, $isMonitoring);
        echo json_encode($data); 
	}
	#MJM End Code
    // fix Monitoring 
    private function get_missed_tito_requests(){
        $fields = "req.dtrRequest_ID, req.dateRequest, req.status_ID";
        $where = "req.status_ID = 12";
        // $tables = "tbl_acc_time accTime, tbl_shift_break shiftBreak, tbl_break_time_account accountBreak, tbl_break_time breakTime, tbl_break break, tbl_time times";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_dtr_request req");
    }
    private function get_missed_tito_request_approvals($tito){
        $fields = "appr.dtrRequestApproval_ID, appr.dtrRequest_ID, appr.approvalLevel, appr.userId, appr.approvalStatus_ID, appr.dateTimeStatus, appr.remarks";
        $where = "appr.dtrRequest_ID IN ($tito)";
        // $tables = "tbl_acc_time accTime, tbl_shift_break shiftBreak, tbl_break_time_account accountBreak, tbl_break_time breakTime, tbl_break break, tbl_time times";
        return $this->general_model->fetch_specific_vals($fields, $where, "tbl_dtr_request_approval appr");
    }
    public function fix_missed_approvals(){
        $missed_req = $this->get_missed_tito_requests();
        $missed_req_tito_ids = implode(',', array_column($missed_req, 'dtrRequest_ID'));
        $approvals = $this->get_missed_tito_request_approvals($missed_req_tito_ids);
        // var_dump($missed_req_tito_ids);
        // var_dump();
        if(count($missed_req) > 0){
            foreach($missed_req as $missed_req_rows){
                $tito_req_id = $missed_req_rows->dtrRequest_ID;
                $tito_req_appr = array_merge(array_filter($approvals, function ($e) use ($tito_req_id) {//get current approver details
                        return $e->dtrRequest_ID == $tito_req_id;
                    }
                ));
                var_dump(count($tito_req_appr));
                $tito_max_appr = count($tito_req_appr) -1;
                // var_dump($tito_req_id);
                // var_dump($tito_req_appr);
                var_dump($tito_req_appr[$tito_max_appr]);
                // if(($tito_req_appr[$tito_max_appr]->approvalStatus_ID == 5) || ($tito_req_appr[$tito_max_appr]->approvalStatus_ID == 6)){
                //     var_dump($tito_req_appr[$tito_max_appr]);
                // }
            }
        }
    }
    private function get_tito_request_to_check(){
        $fields = "req.dtrRequest_ID, req.userID, emp.emp_id, req.dateRequest, req.acc_time_id, req.status_ID";
        $where = "emp.emp_id = users.emp_id AND users.uid = req.userID AND DATE(req.dateCreated) >='2021-01-01' AND DATE(req.dateRequest) >='2021-01-01' AND req.status_ID = 5";
        $tables = "tbl_employee emp, tbl_user users, tbl_dtr_request req";
        return $this->general_model->fetch_specific_vals($fields, $where, $tables);
    }
    private function get_tito_scheds(){
        $fields = "sched.sched_id, sched.emp_id, sched.sched_date, sched.acc_time_id, sched.remarks";
        $where = "DATE(sched.sched_date) >='2021-01-01'";
        $tables = "tbl_schedule sched";
        return $this->general_model->fetch_specific_vals($fields, $where, $tables);
    }
    
    public function check_if_tito_is_implemented(){
        $tito_requests = $this->get_tito_request_to_check();
        $scheds = $this->get_tito_scheds();
        // var_dump($tito_requests);
        // var_dump($scheds);
        // if(count($tito_requests)){
        //     foreach($tito_requests as $tito_requests_row){
        //         var_dump($tito_requests_row->dtrRequest_ID);
        //         $tito_date = $tito_requests_row->dateRequest;
        //         $tito_emp_id = $tito_requests_row->emp_id;
        //         $acc_time_id = $tito_requests_row->acc_time_id;
        //         $tito_req_appr = array_merge(array_filter($scheds, function ($e) use ($tito_date, $tito_emp_id, $acc_time_id) {//get current approver details
        //                 return (($e->sched_date == $tito_date) && ($e->emp_id == $tito_emp_id));
        //             }
        //         ));
        //         var_dump($tito_req_appr);
        //     }
        // }
    }

    // CHECK INCORRECT TITO AHR DATES
    private function get_ahr_incorrect_dates($end_date){
        $fields = "request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as description, request.userId, app.pic, app.fname, app.lname, app.mname, app.nameExt, users.emp_id, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedId, request.requestType as requestType, sched.sched_date as sched_date, schedType.type as schedType, time_start, time_end, reason, dateTimeFiled, statuses.description as statusDescription, empStat.status as empStatus, request.tito_ID";
        $where = 'times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = request.schedId AND empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = users.emp_id  AND users.uid = request.userId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND ((DATE(request.otStartDate) <= "2018-01-01") OR (DATE(request.otStartDate) > "'.$end_date.'") OR (DATE(request.otEndDate) <= "2018-01-01") OR (DATE(request.otEndDate) > "'.$end_date.'"))';
        $tables = "tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users, tbl_account accounts, tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app";
        return $this->general_model->fetch_specific_vals($fields, $where, $tables);
    } 
    private function get_ahr_incorrect_dates_test($end_date){
        $fields = "request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as description, request.userId, app.pic, app.fname, app.lname, app.mname, app.nameExt, users.emp_id, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedId, request.requestType as requestType, sched.sched_date as sched_date, schedType.type as schedType, time_start, time_end, reason, dateTimeFiled, statuses.description as statusDescription, empStat.status as empStatus, request.tito_ID";
        $where = 'times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = request.schedId AND empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = users.emp_id  AND users.uid = request.userId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND ((DATE(request.otStartDate) <= "2018-01-01") OR (DATE(request.otStartDate) > "'.$end_date.'") OR (DATE(request.otEndDate) <= "2018-01-01") OR (DATE(request.otEndDate) > "'.$end_date.'"))';
        $tables = "tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users, tbl_account accounts, tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app";
        // return $this->general_model->fetch_specific_vals($fields, $where, $tables);
        $this->general_model->fetch_specific_vals($fields, $where, $tables);
        echo $this->db->last_query();
    } 
    
    // private function get_ahr_incorrect_dates($end_date){
    //     $fields = "request.additionalHourRequestId additionalHourRequestId, types.additionalHourTypeId, types.description as description, request.userId, app.pic, app.fname, app.lname, app.mname, app.nameExt, users.emp_id, accounts.acc_id, accounts.acc_name accountDescription, positions.pos_details positionDescription, otStartDate, otStartTime, otEndDate, otEndTime, totalOtHours, schedId, request.requestType as requestType, sched.sched_date as sched_date, schedType.type as schedType, time_start, time_end, reason, dateTimeFiled, statuses.description as statusDescription, empStat.status as empStatus, request.tito_ID";
    //     $where = 'times.time_id = accTime.time_id AND accTime.acc_time_id = sched.acc_time_id AND schedType.schedtype_id = sched.schedtype_id AND sched.sched_id = request.schedId AND empStat.empstat_id = posEmpStat.empstat_id AND positions.pos_id = posEmpStat.pos_id AND posEmpStat.posempstat_id = empPromote.posempstat_id AND empPromote.emp_id = emp.emp_id AND empPromote.isActive = 1 AND accounts.acc_id = emp.acc_id AND app.apid = emp.apid AND emp.emp_id = users.emp_id  AND users.uid = request.userId AND types.additionalHourTypeId = request.additionalHourTypeId AND statuses.status_ID = request.approvalStatus_ID AND ((request.otStartDate IS NULL) OR (request.otStartDate IS NULL))';
    //     $tables = "tbl_status statuses, tbl_additional_hour_request request, tbl_additional_hour_type types, tbl_schedule sched, tbl_schedule_type schedType, tbl_acc_time accTime, tbl_time times, tbl_user users, tbl_account accounts, tbl_employee emp, tbl_position positions, tbl_pos_emp_stat posEmpStat, tbl_emp_stat empStat, tbl_emp_promote empPromote, tbl_applicant app";
    //     return $this->general_model->fetch_specific_vals($fields, $where, $tables);
    // }

    public function check_incorrect_tito_ahr_dates(){
        $dateTime = $this->get_current_date_time();
        $end_date = $dateTime['date'];
        $ahr_records = $this->get_ahr_incorrect_dates($end_date);
        // var_dump($ahr_records);
        $update_ahr = [];
        if(count($ahr_records) > 0){
            var_dump(count($ahr_records));
            foreach($ahr_records as $ahr_records_index=>$ahr_records_row){
                var_dump($ahr_records_row);
                $sched_logs = [];
                $sched_id = $ahr_records_row->schedId;
                $logs = $this->qry_logs(implode(',',array_column($ahr_records, 'schedId')));
                $sched_logs = array_merge(array_filter($logs, function($e) use ($sched_id){
                    return $e->sched_id == $sched_id;
                }));
                $shift_start_date_time = new DateTime($ahr_records_row->sched_date . " " . $ahr_records_row->time_start);
				$formatted_shift_start = $shift_start_date_time->format("Y-m-d H:i");
				$shift_end_date_time = new DateTime($ahr_records_row->sched_date . " " . $ahr_records_row->time_end);
				$formatted_shift_end = $shift_end_date_time->format("Y-m-d H:i");
				// $new_shifts = $this->check_start_end_shift_24($formatted_shift_start, $formatted_shift_end);
                $new_shifts = $this->get_standard_shift($formatted_shift_start, $formatted_shift_end); // function is in general controller
                // echo $formatted_shift_start." - ".$formatted_shift_end;
                // var_dump($new_shifts);
                $corrected_ot = $this->process_correct_ahr($new_shifts['start'], $new_shifts['end'], $ahr_records_row->additionalHourTypeId, $ahr_records_row->totalOtHours, $sched_logs);
				echo "<br><br>correct <br><br><br>";
                var_dump($corrected_ot);
                $update_ahr[$ahr_records_index] = [
                    'additionalHourRequestId' => $ahr_records_row->additionalHourRequestId,
                    'otStartDate' => $corrected_ot['start_date'],
                    'otStartTime' => $corrected_ot['start_time'],
                    'otEndDate' => $corrected_ot['end_date'],
                    'otEndTime' => $corrected_ot['end_time']
                ];
                // var_dump($ahr_records_row);
                // var_dump($new_shifts);            
            }
			echo "<br><br>new corrected <br><br><br>";
            var_dump($update_ahr);
             $this->general_model->batch_update($update_ahr, 'additionalHourRequestId', 'tbl_additional_hour_request');
        }
    }
    public function check_incorrect_tito_ahr_dates_test(){
        $dateTime = $this->get_current_date_time();
        $end_date = $dateTime['date'];
        $ahr = $this->get_ahr_incorrect_dates_test($end_date);
    }

    public function manual_set_approver(){
        //NOTE: for no supervisor approver nga tito request
        $data = $this->general_model->custom_query("SELECT req.*, COUNT(appr.dtrRequestApproval_ID) as count FROm tbl_dtr_request req LEFT JOIN tbl_dtr_request_approval appr ON appr.dtrRequest_ID = req.dtrRequest_ID WHERE req.userId = 1923 GROUP BY req.dtrRequest_ID HAVING count = 0");

        if(COUNT($data)>0){
            foreach($data as $row){
             $approvers = $this->set_approvers($row->dateCreated, $row->userID, 3, 'dtrRequest_ID', $row->dtrRequest_ID);
             $this->general_model->batch_insert($approvers, 'tbl_dtr_request_approval');
             var_dump($approvers);
            }
        }
    }

}