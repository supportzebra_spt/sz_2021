<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Irequest extends General
{
    public $logo = "/var/www/html/sz/assets/images/img/logo2.png";  
    public function my_request()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'My Requests',
            'access_page' => 1,
        ];
        if($this->check_access()){
            $this->load_template_view('templates/irequest/my_request', $data);       
        }   
    }
    public function irequest_controlpanel()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'iRequest Control Panel'
        ];
        if($this->check_access()){
            $this->load_template_view('templates/irequest/irequest_controlpanel', $data);   
        }    
    }
    public function my_requestapproval()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'My Requests Approval',
            'access_page' => 2,
        ];
        if($this->check_access()){
            $this->load_template_view('templates/irequest/my_request', $data);  
        }     
    }
    public function all_requests()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'All Requests',
            'access_page' => 3,
        ];
        if($this->check_access()){
            $this->load_template_view('templates/irequest/my_request', $data);
        }       
    }
    public function tat_monitoring()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'TAT Monitoring',
            'access_page' => 4,
        ];
        if($this->check_access()){
            $this->load_template_view('templates/irequest/tat_mrfmonitoring', $data); 
        }      
    }
    public function fetch_account_position_det(){
        $acc = $this->general_model->custom_query("SELECT ac.acc_id,ac.acc_name FROM tbl_account ac, tbl_department dep WHERE ac.dep_id=dep.dep_id order by ac.acc_name ASC");
        $position = $this->general_model->custom_query("SELECT pos_id,pos_details,pos_name FROM `tbl_position` order by pos_details ASC");
        $purposes = $this->general_model->custom_query("SELECT hiringPurpose_ID, purpose FROM tbl_irequest_hiring_purpose order by hiringPurpose_ID ASC");
        $hiring_type = $this->general_model->custom_query("SELECT hiringType_ID, description FROM tbl_irequest_hiring_type WHERE status_ID=10 order by hiringType_ID ASC");
        echo json_encode(['accounts' => $acc, 'positions' => $position, 'purposes' => $purposes, 'hiring_type' => $hiring_type]);
    }
    public function fetch_approvaldeadline(){
        $data = $this->general_model->custom_query("SELECT * from tbl_irequest_approval_duration");
        echo json_encode($data);
    }
    public function fetch_approversdisplay(){
        $ireq = $this->input->post("ireq");
        $data = $this->general_model->custom_query("SELECT appr.remarks,appr.request_ID,appr.emp_ID,appr.status,appr.level,appr.dated_status, apl.fname,apl.lname FROM tbl_irequest_request_approval appr, tbl_employee emp, tbl_applicant apl WHERE appr.request_ID=$ireq AND appr.emp_ID=emp.emp_id AND emp.apid=apl.apid");
        echo json_encode($data);
    }
    public function checker_Level(){
        $ireq = $this->input->post("ireq");
        $data = $this->general_model->custom_query("SELECT * FROM `tbl_irequest_request_approval` WHERE request_ID=$ireq AND status=4 AND level=1");
        if($data!=null){
            $res = 1;
        }else{
            $res = 0;
        }
        echo json_encode($res);
    }
    public function approve_disapprove_manpowerrequest(){
        $dateTime = $this->get_current_date_time();
        $em_id = $this->session->userdata('emp_id');
        $ireq = $this->input->post("ireq");
        $decision = $this->input->post('dec');
        $remarks= $this->input->post("remarks");
        $missed= $this->input->post("missed");
        $approver = $this->session->userdata('emp_id');
        $app['status'] = $decision;
        $where = "emp_ID = $approver AND request_ID = $ireq";
        $app['remarks'] = $remarks;
        $app['dated_status'] = $dateTime['dateTime'];
        $des2="";
        $aft = $this->input->post("aft");

        // AFT approve 
        if($aft==1){
            $whereAft = "irequest_ID = $ireq";
            $aftApp['aftapproved'] = $aft;
            $this->general_model->update_vals($aftApp, $whereAft, 'tbl_irequest_manpower_request');
        }

        $requestor_idd = $this->general_model->custom_query("SELECT u.uid, r.requested_by FROM tbl_irequest_manpower_request r, tbl_employee emp, tbl_user u WHERE r.irequest_ID=$ireq AND r.requested_by=emp.emp_id AND emp.emp_id=u.emp_id");
        $reQ = $this->general_model->custom_query("SELECT requestApproval_ID FROM `tbl_irequest_request_approval` WHERE request_ID = $ireq AND emp_ID = $em_id");

        if($missed == 1){
            //if Missed but change status
            $request['status'] = $decision;
            $wherereq = "irequest_ID = $ireq";
            $this->general_model->update_vals($request, $wherereq, 'tbl_irequest_manpower_request');
            if($decision==5){
                $des2 = "approved";
            }else{
                $des2 = "disapproved";
            }
            //Notif to requestor
            $link = "irequest/my_request";
            $recipient[0] = [
                'userId' => $requestor_idd[0]->uid,
                ];
            $notif_message ="Your Manpower Request was ".$des2.".<br><small><b>ID</b>:" . str_pad($ireq, 8, '0', STR_PAD_LEFT) . " </small>";
            $insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);

            // Update records to approval 
            $highest_level = $this->general_model->custom_query("SELECT MAX(level) as hlevel FROM `tbl_irequest_request_approval` WHERE request_ID = $ireq");
            $apvl_rec['request_ID'] =  $ireq;
            $apvl_rec['emp_ID'] =   $em_id;
            $apvl_rec['status'] = $decision;
            $apvl_rec['remarks'] = "(Approved by General Approver.) ".$remarks;
            $nLevel = (int) $highest_level[0]->hlevel + 1;
            $apvl_rec['level'] = $nLevel;
            $this->general_model->insert_vals($apvl_rec, "tbl_irequest_request_approval");
        }else{
            $this->general_model->update_vals($app, $where, 'tbl_irequest_request_approval');

            if($decision==6){
                //if disapproved
                $request['status'] = $decision;
                $wherereq = "irequest_ID = $ireq";
                $this->general_model->update_vals($request, $wherereq, 'tbl_irequest_manpower_request');
                //Notif to requestor
                $link = "irequest/my_request";
                $recipient[0] = [
                    'userId' => $requestor_idd[0]->uid,
                    ];
                $notif_message ="Your Manpower Request was disapproved.<br><small><b>ID</b>:" . str_pad($ireq, 8, '0', STR_PAD_LEFT) . " </small>";
                $insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);
            }else{
                // if MRF is approved
                $qt=$this->general_model->custom_query("SELECT emp_ID FROM `tbl_irequest_request_approval` WHERE request_ID=$ireq AND status=2 ORDER BY level LIMIT 1");
                if($qt!=null){
                    // Change status for pending to current 
                    $nextlevel = $qt[0]->emp_ID;
                    $reci_id = $this->general_model->custom_query("SELECT uid FROM `tbl_user` WHERE emp_id=$nextlevel"); 
                    $app2['status'] = 4;
                    $whereqt = "emp_ID = $nextlevel AND request_ID = $ireq";
                    $this->general_model->update_vals($app2, $whereqt, 'tbl_irequest_request_approval');
                    // should send notication here
                   
                    $recipient[0] = [
                        'userId' => $reci_id[0]->uid,
                        ];
                    $link = "irequest/my_requestapproval";
                    $notif_message = "forwarded a Manpower Request for approval. <br><small><b>MRF-ID</b>: " . str_pad($ireq, 8, '0', STR_PAD_LEFT) . " </small>";
                    $insert_stat['notifid'] = $this->set_notif($this->session->userdata('uid'), $notif_message, $link, $recipient);
                }else{
                    // Code to change status of MRF
                    $request['status'] = $decision;
                    $request['datedStatus'] = $dateTime['dateTime'];
                    $wherereq = "irequest_ID = $ireq";
                    $this->general_model->update_vals($request, $wherereq, 'tbl_irequest_manpower_request');  
    
                     //Notif to requestor
                    $link = "irequest/my_request";
                    $recipient[0] = [
                        'userId' => $requestor_idd[0]->uid,
                        ];
                    $notif_message ="Your Manpower Request was Approved.<br><small><b>ID</b>:" . str_pad($ireq, 8, '0', STR_PAD_LEFT) . " </small>";
                    $insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);
    
                    // add data to tbl_irequest_recruitment
                    $recruitment['irequest_ID'] = $ireq;
                    $recruitment['status_ID'] = 2;
                    $this->general_model->insert_vals($recruitment, "tbl_irequest_recruitment");
                }
            }
        }
        $this->set_irequest_approval_statuses($ireq, $reQ[0]->requestApproval_ID, $decision);
        echo json_encode($insert_stat);
    }
    public function cancel_manpowerrequest(){
        $ireq = $this->input->post("ireq");
        $st= $this->input->post("status");
        $allowed = "";
        // Check manpower request 
        $q = $this->general_model->custom_query("SELECT requestApproval_ID FROM tbl_irequest_request_approval WHERE request_ID=$ireq AND status=5");
        if($q==null){
            $where = "irequest_ID = $ireq";
            $where2 = "request_ID = $ireq";         
            foreach($q as $qa){
                $where3 = "request_ID = $qa->requestApproval_ID";
                $this->general_model->delete_vals($where3, 'tbl_deadline');
            }
            $allowed = 1;
            $this->general_model->delete_vals($where, 'tbl_irequest_manpower_request');
            $this->general_model->delete_vals($where2, 'tbl_irequest_request_approval');
        }else{
            $allowed = 0;
        }
        echo json_encode($allowed);
    }
    public function cancel_manpowerRecruitment(){
        $rec = $this->input->post("irecruitment");
        $reqid = $this->input->post("ireqid");
        $reqapp['status_ID'] = 7;
        $where = "irequestRecruitment_ID = $rec";
        $this->general_model->update_vals($reqapp, $where, 'tbl_irequest_recruitment');
    }
    public function remove_approver(){
        $ireq_approver = $this->input->post("ireq_approver");
        $apptype = $this->input->post("apptype");
        $level = $this->input->post("level");
        $where = "approver_ID = $ireq_approver";
        $max_level = $this->qry_max_approval_level($apptype);
        if($level < (int)$max_level->level+1){
            $this->update_irequest_approval_levels_remove($level, $apptype);
        }
        $this->general_model->delete_vals($where, 'tbl_irequest_approver');
    }

    public function save_manpowerrequest(){
        $dateTime = $this->get_current_date_time();
        $requestorid = $this->session->userdata('uid');
        $reqidd= $this->session->userdata('emp_id');
        $path = 'uploads/irequest/job_description/';
        $attached_f = $_FILES['upload_attachment_jobdesc']['name'];
        $tmp = $_FILES['upload_attachment_jobdesc']['tmp_name'];
        $ext = strtolower(pathinfo($attached_f, PATHINFO_EXTENSION));
        $final_file = time() . $attached_f;
        $path = $path . strtolower($final_file);
        if (move_uploaded_file($tmp, $path)) {
            $mrf['descriptionAttachment'] = $path;
        }
        $op=$this->input->post("otherpurpose");
        $mrf['days_needed'] = $this->input->post("daysneeded");
        $mrf['dateRequested'] = $dateTime['dateTime'];

        // $date_ns=$this->input->post("dateduration");
        // $varz = explode('-',$date_ns);
        // $mrf['dateStartRequired'] = date('Y-m-d',strtotime($varz[0]));
        // $mrf['dateEndRequired'] = date('Y-m-d',strtotime($varz[1]));

        $mrf['requesting_dept'] = $this->input->post("req_department");
        $posi = $this->input->post("position");
        $mrf['position_ID']  = $posi;
        $mrf['status']  = 2;

        $mrf['hiringPurpose_ID']  = $this->input->post("purpose_hiring");
        $mrf['manpower_requested']  = $this->input->post("manpower");
        $mrf['educationalAttainment']  = $this->input->post("edrequirement");
        $mrf['gender']  = $this->input->post("gender");
        $mrf['description']  = $this->input->post("brief_description");
        $mrf['qualifications']  = $this->input->post("qualifications");
        $mrf['site_ID']  = $this->input->post("position_site");
        $mrf['hiringtype_ID']  = $this->input->post("type_hiring");
        $mrf['hiringtype_desc']  = $this->input->post("type_hiring_desc");
        $mrf['requested_by'] = $reqidd;

        if($op!="" || $op!=null ){
            $mrf['hiringPurpose_desc']  = $op;
        }else{
            $mrf['hiringPurpose_desc'] = null;
        }
        $last_mrf_id = $this->general_model->insert_vals_last_inserted_id($mrf, 'tbl_irequest_manpower_request');
        $pos_emptype = $this->general_model->custom_query("SELECT class,pos_details FROM `tbl_position` WHERE pos_id = $posi"); 
        $insert_stat['notifid'] = $this->set_irequest_approval($last_mrf_id,$pos_emptype[0]->class);
        echo json_encode($insert_stat);
    }

    public function save_manpowerrequest2(){
        $requestorid = $this->session->userdata('uid');
        $reqidd= $this->session->userdata('emp_id');
        // if attached file is not empty
        $path = 'uploads/irequest/job_description/';
        $attached_f = $_FILES['upload_attachment_jobdesc']['name'];
        $tmp = $_FILES['upload_attachment_jobdesc']['tmp_name'];
        // get uploaded file's extension
		$ext = strtolower(pathinfo($attached_f, PATHINFO_EXTENSION));
        $final_file = time() . $attached_f;
        $path = $path . strtolower($final_file); 

        if (move_uploaded_file($tmp, $path)) {
            $mrf['descriptionAttachment'] = $path;
		}
        $op=$this->input->post("otherpurpose");
        // $ap=$this->input->post("aftapproved");
        $date_ns=$this->input->post("dateduration");

        $varz = explode('-',$date_ns);
        $mrf['dateStartRequired'] = date('Y-m-d',strtotime($varz[0]));
        $mrf['dateEndRequired'] = date('Y-m-d',strtotime($varz[1]));

        $mrf['requesting_dept'] = $this->input->post("req_department");
        $posi = $this->input->post("position");
        $mrf['position_ID']  = $posi;
        $mrf['status']  = 2;
    // $mrf['']  = $this->input->post("dateduration");

        $mrf['hiringPurpose_ID']  = $this->input->post("purpose_hiring");
        $mrf['manpower_requested']  = $this->input->post("manpower");
        $mrf['educationalAttainment']  = $this->input->post("edrequirement");
        $mrf['gender']  = $this->input->post("gender");
        $mrf['description']  = $this->input->post("brief_description");
        $mrf['qualifications']  = $this->input->post("qualifications");
        // $mrf['aftapproved']  = $this->input->post("aftapproved");
        $mrf['requested_by'] = $reqidd;

        // if($op!="" || $op!=null || $ap!="" || $ap!=null){
        //     $mrf['hiringPurpose_desc']  = $op;
        //     // $mrf['aftapproved']  = $ap;
        // }

        if($op!="" || $op!=null ){
            $mrf['hiringPurpose_desc']  = $op;
            // $mrf['aftapproved']  = $ap;
        }else{
            $mrf['hiringPurpose_desc'] = null;
            // $mrf['aftapproved'] = 0;
        }
        $last_mrf_id= $this->general_model->insert_vals_last_inserted_id($mrf, 'tbl_irequest_manpower_request');
         
        // SHOULD HAVE notifications  

         //  MRF filer details 
         $sender_mrf = $this->general_model->custom_query("SELECT app.fname, app.lname, app.mname FROM tbl_applicant app, tbl_employee emp WHERE emp.apid=app.apid AND emp.emp_id=$reqidd");
         $name = $sender_mrf[0]->lname .",". $sender_mrf[0]->fname;

         $class = $this->general_model->custom_query("SELECT class FROM `tbl_position` WHERE pos_id=$posi");
         $f_class = $class[0]->class;
         $approvers = $this->general_model->custom_query("SELECT * FROM `tbl_irequest_approver` WHERE (emp_type='both' OR emp_type='$f_class') ORDER BY level ASC");
        //  Saving to Request Approval 
        $ad= $this->general_model->custom_query("SELECT days FROM `tbl_irequest_approval_duration`");
         
        $approvers_set = array();
        $approver_deadline = array();
        $loop=0;
        $first = true;
        $valf= 0;

        $ad2 = $ad[0]->days;
        $ad4 = 0;
        $fixdate = $ad2;
        $changedays = $ad2;

        foreach($approvers as $ap){
            $ad3 = date('Y-m-d', strtotime("+$changedays day")) . ' 23:59:59';     
            $appuid = $this->general_model->custom_query("SELECT uid FROM `tbl_user` WHERE emp_id=$ap->emp_ID");
            if($first){
                $valf=4;
                $first = false;
                // send notifications to first approver
                $recipient[0] = [
                    'userId' => $appuid[0]->uid,
                    ];
                $link = "irequest/my_requestapproval";
                // $notif_message = $name . " " . "has sent you a Manpower Request.<br><small><b>ID</b>:" . str_pad($last_mrf_id, 8, '0', STR_PAD_LEFT) . " </small>";
            
                $notif_message = "has sent you a Manpower Request and it needs your approval. <br><small><b>MRF-ID</b>: " . str_pad($last_mrf_id, 8, '0', STR_PAD_LEFT) . " </small>";
            }else{
                $valf=2;
            }
            $ap_set['request_ID'] = $last_mrf_id;
            $ap_set['emp_ID'] = $ap->emp_ID;
            $ap_set['status'] = $valf;
            $ap_set['level'] = $ap->level;
            
            $last_appval_id= $this->general_model->insert_vals_last_inserted_id($ap_set, 'tbl_irequest_request_approval');

            $approver_deadline[$loop] = [
                'module_ID' => 6,
                'request_ID' => $last_appval_id,
                'requestor' => $requestorid,
                'userId' => $appuid[0]->uid,
                'approvalLevel' => $ap->level,
                'deadline' => $ad3,
                'status_ID' => 2
            ];
            $loop++;
            $changedays = $changedays + $fixdate;
        }
        // $this->general_model->batch_insert($approvers_set, 'tbl_irequest_request_approval');
        $this->general_model->batch_insert($approver_deadline, 'tbl_deadline');
        // $insert_stat['notifid'] = $this->set_system_notif($notif_message, $link, $recipient);
        $insert_stat['notifid'] = $this->set_notif($this->session->userdata('uid'), $notif_message, $link, $recipient);
        echo json_encode($insert_stat);
    }

    public function filters_myreq_pending(){
        $emp_id = $this->session->emp_id;
        $filt = $this->input->post("filter_no");
        if($filt==1){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE requested_by=$emp_id AND (status=2 OR status=12) AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE requested_by=$emp_id AND (status=2 OR status=12) AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }else if($filt==2){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE requested_by=$emp_id AND (status=5 OR status=6) AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE requested_by=$emp_id AND (status=5 OR status=6) AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }else if($filt==3){
            $c_apptype = $this->general_model->custom_query("SELECT emp_type FROM `tbl_irequest_approver` WHERE emp_ID=$emp_id");                       
            if($c_apptype!=null){

                if ($c_apptype[0]->emp_type=='agent' || $c_apptype[0]->emp_type=='admin'){
                    $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                    $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                }else {
                    $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                    $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                }

                // if ($c_apptype[0]->emp_type=='both'){
                //     $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                //     $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                // }else if ($c_apptype[0]->emp_type=='agent' || $c_apptype[0]->emp_type=='admin') {
                //     $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                //     $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                // }


            }else{
                $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                    $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
            }
        }else if($filt==4){
            $c_apptype = $this->general_model->custom_query("SELECT emp_type FROM `tbl_irequest_approver` WHERE emp_ID=$emp_id");                       
            if($c_apptype!=null){

                if ($c_apptype[0]->emp_type=='agent' || $c_apptype[0]->emp_type=='admin'){
                    $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND (appl.status=5 OR appl.status=6 OR req.status=12) AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id");
                    $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND (appl.status=5 OR appl.status=6 OR req.status=12) AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id");
                }else{
                    $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND (appl.status=5 OR appl.status=6 OR req.status=12) AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id");
                    $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr,tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND (appl.status=5 OR appl.status=6 OR req.status=12) AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id");
                }

                // if ($c_apptype[0]->emp_type=='both'){
                //     $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND (appl.status=5 OR appl.status=6 OR req.status=12) AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id");
                //     $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr,tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND (appl.status=5 OR appl.status=6 OR req.status=12) AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id");
                // }else if ($c_apptype[0]->emp_type=='agent' || $c_apptype[0]->emp_type=='admin') {
                //     $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND (appl.status=5 OR appl.status=6 OR req.status=12) AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id");
                //     $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND (appl.status=5 OR appl.status=6 OR req.status=12) AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id");
                // }


            }else{
                $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
                    $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2");
            }
        }else if($filt==5){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE status=2 AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE status=2 AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }else if($filt==6){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE (status=6 OR status=5 OR status=12) AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE (status=6 OR status=5 OR status=12) AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }else if($filt==7){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE status=12 AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp WHERE status=12 AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }else if($filt==8){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq WHERE recreq.status_ID = 2 AND recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq WHERE recreq.status_ID = 2 AND recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }else if($filt==9){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq WHERE recreq.status_ID = 3 AND recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq WHERE recreq.status_ID = 3 AND recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }else if($filt==10){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq WHERE recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.requested_by = $emp_id GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq WHERE recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.requested_by = $emp_id GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }else if($filt==11){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq, tbl_irequest_request_approval appreq WHERE recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND appreq.request_ID = req.irequest_ID AND appreq.emp_ID=$emp_id GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq, tbl_irequest_request_approval appreq WHERE recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND appreq.request_ID = req.irequest_ID AND appreq.emp_ID=$emp_id GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }else if($filt==12){
            $pos = $this->general_model->custom_query("SELECT DISTINCT(req.position_ID),pos.pos_details FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq WHERE recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.position_ID ORDER BY pos.pos_details ASC");
            $acc = $this->general_model->custom_query("SELECT DISTINCT(req.requesting_dept),acc.acc_name FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_irequest_recruitment recreq WHERE recreq.irequest_ID = req.irequest_ID AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid GROUP BY req.requesting_dept ORDER BY acc.acc_name ASC");
        }
        echo json_encode(['position' => $pos, 'account' => $acc]);
    }
    public function getpos_perdepartment(){
        $dep_id = $this->input->post('department_id');
        // $q = $this->general_model->custom_query("SELECT pos.pos_id,pos.pos_name,pos.pos_details,posdep.dep_id FROM tbl_pos_dep as posdep, tbl_position as pos,tbl_account acc WHERE pos.pos_id = posdep.pos_id AND posdep.dep_id=acc.dep_id AND acc.acc_id = $dep_id");
        $q = $this->general_model->custom_query("SELECT pos.pos_id,pos.site_ID,pos.pos_name,pos.pos_details,accpos.accounts_ID,accpos.positions_ID FROM tbl_account_position accpos, tbl_position as pos,tbl_account acc WHERE accpos.accounts_ID = acc.acc_id AND pos.pos_id = accpos.positions_ID AND acc.acc_id = $dep_id");
        echo json_encode($q);
    }
    //Get Manpower request 
    private function get_manpowerrequest($stat_mrf,$ireq_id,$req,$app){    
        $emp_id = $this->session->emp_id;
        if($stat_mrf==2){
            if($app==0){
                return $this->general_model->custom_query("SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, hpurp.purpose as hpurpose FROM tbl_irequest_hiring_purpose hpurp, tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND hpurp.hiringPurpose_ID=req.hiringPurpose_ID AND req.irequest_ID=$ireq_id");
            }else{
                return $this->general_model->custom_query("SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, hpurp.purpose as hpurpose, appl.status as aplstatus FROM tbl_irequest_hiring_purpose hpurp, tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_request_approval appl WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND hpurp.hiringPurpose_ID=req.hiringPurpose_ID AND req.irequest_ID=$ireq_id AND appl.emp_ID=$emp_id AND appl.request_ID=$ireq_id AND appl.request_ID=req.irequest_ID AND appl.status=stat.status_ID");
            }
        }else if($stat_mrf==4){
            return $this->general_model->custom_query("SELECT req.*,pos.pos_details,pos.site_ID,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, hpurp.purpose as hpurpose, recr.irequestRecruitment_ID, recr.irequest_ID, recr.numberOfRecruit, recr.status_ID as recstatus, recr.dateTimeCompleted FROM tbl_irequest_hiring_purpose hpurp, tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_recruitment recr WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND recr.status_ID=stat.status_ID AND recr.irequest_ID = $ireq_id AND recr.irequest_ID=req.irequest_ID AND hpurp.hiringPurpose_ID=req.hiringPurpose_ID AND req.irequest_ID=$ireq_id");
        }else{
            return $this->general_model->custom_query("SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, hpurp.purpose as hpurpose FROM tbl_irequest_hiring_purpose hpurp, tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND hpurp.hiringPurpose_ID=req.hiringPurpose_ID AND req.irequest_ID=$ireq_id");
        }
    }
    //Get Approver Details
    public function get_ir_approversdetails(){
        $approver_id = $this->input->post('approver');
        $emptype = $this->input->post('emptype');
        $data['info'] = $this->general_model->fetch_specific_val("apr.*,ap.fname,ap.lname,ap.mname", "apr.approver_ID=$approver_id AND apr.emp_ID=emp.emp_id AND emp.apid=ap.apid", "tbl_irequest_approver apr, tbl_employee emp, tbl_applicant ap");
        $hlevel = $this->qry_max_approval_level($emptype);
        $data['highest_level'] = $hlevel->level;
        echo json_encode($data);
    }
    // View manpower details
    public function manpowerdetails(){
        // $emp_id = $this->session->emp_id;
        $ireq_id = $this->input->post('ireq');
        $stat_mrf = $this->input->post('stat');
        $req = $this->input->post('requestor');
        $app = $this->input->post('approver');
        $data = $this->get_manpowerrequest($stat_mrf,$ireq_id,$req,$app);
        echo json_encode($data);
    }
    public function manpowerdetails2(){
        $ireq_id = $this->input->post('ireq');
        $stat_mrf = $this->input->post('stat');
        $req = $this->input->post('requestor');
        $app = $this->input->post('approver');
        $pos = $this->input->post('position');

        $details = $this->get_manpowerrequest($stat_mrf,$ireq_id,$req,$app);
        $data['details'] =  $details;
        $data['applicants'] = $this->Get_Applicants($ireq_id);
        $a = $this->get_ApplicantsHired($details[0]->irequestRecruitment_ID);

        if($details[0]->recstatus == 3){
            // APPROVED Manrequest
            $from = strtotime($details[0]->datedStatus);
            $to = strtotime($details[0]->dateTimeCompleted);
            $duration = $this->get_human_time_format($from, $to, 0);
            $dayS = (int) $duration['hoursTotal'] /24;
            $data['days'] = (int) $dayS;
            $data['to'] = $details[0]->dateTimeCompleted;
            $data['from'] = $details[0]->datedStatus;
        }else{
            $data['days'] = 0;
        }
        if($a!=null){
            $data['hired'] =  $a;
            $data['hiredEx'] = 1;
        }else{
            $data['hiredEx'] = 0;
        }
        echo json_encode($data);
    }
    private function Get_Applicants($reqID){
        $hiredApplicants = $this->general_model->custom_query("SELECT app.*, apps.application_ID, appt.irecruitApplicant_ID, reqrec.irequestRecruitment_ID, reqrec.irequest_ID FROM tbl_applicant app, tbl_irecruit_application apps, tbl_irecruit_applicant appt, tbl_irequest_recruitment reqrec WHERE app.apid=apps.apid AND apps.application_ID = appt.application_ID AND appt.irequestRecruitment_ID = reqrec.irequestRecruitment_ID AND reqrec.irequest_ID = $reqID");
        
        if($hiredApplicants !=null){
            $app_id = array();
            $loop=0;  
            foreach($hiredApplicants as $a){
                    $app_id[$loop] = $a->apid;
                    $loop++;
            }      
            // return $this->general_model->custom_query("SELECT DISTINCT(ap.apid),ap.fname,ap.apid,ap.lname,ap.mname FROM tbl_applicant ap WHERE ap.apid NOT IN(".implode (", ", $app_id).") AND ap.isActive=1 ORDER BY ap.lname asc");              
            return $this->general_model->custom_query("SELECT DISTINCT(ap.apid),ap.fname,ap.apid,ap.lname,ap.mname FROM tbl_applicant ap, tbl_employee emp WHERE emp.apid=ap.apid AND emp.isActive='yes' AND ap.apid NOT IN(".implode (", ", $app_id).") ORDER BY ap.lname asc");                
        }else{
            // return $this->general_model->custom_query("SELECT DISTINCT(ap.apid),ap.fname,ap.lname,ap.mname FROM tbl_applicant ap WHERE ap.isActive=1 ORDER BY ap.lname asc");
            return $this->general_model->custom_query("SELECT DISTINCT(ap.apid),ap.fname,ap.lname,ap.mname FROM tbl_applicant ap, tbl_employee emp WHERE emp.apid=ap.apid AND emp.isActive='yes' ORDER BY ap.lname asc");
        }
    }
    // Datatable 
    public function list_irequest_mypending_datatable()
    {
        $emp_id = $this->session->emp_id;
        $datatable = $this->input->post('datatable');
        $contaccess = $datatable['query']['cont_access'];
        $position = $datatable['query']['position'];
        $acc = $datatable['query']['req_department'];
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];
        $pagefetchtype = $datatable['query']['page_type'];
        $position_where = "";
        $acc_where = "";
        $stat_where = "";
        $status_diff= "";
        $status_diff_pending= "";
        $appl_stat= "";
        $date_q= "";

        if ((int) $position !== 0) {
            $position_where = "AND req.position_ID = $position";
        }
        if ((int) $acc !== 0) {
            $acc_where = "AND req.requesting_dept = $acc";
        }       
        if($contaccess==1){
            $statt_q = $datatable['query']['status'];
            $status_diff = "AND (req.status=2 OR req.status=12)";
            if ((int) $statt_q !== 0) {     
                $status_diff_pending = "AND req.status = $statt_q";
            }
        }else if($contaccess==2){
            //Manpower
            // $status_diff = "AND (status=5 OR status=6)";
            $stat = $datatable['query']['mrf_stat'];

            if($pagefetchtype=='myreq'){
                if ((int) $stat !== 0) {
                    $stat_where = "AND req.status = $stat";
                }
            }else{
                if ((int) $stat !== 0) {
                    $stat_where = "AND appl.status = $stat";
                }
            } 
            $date_q= "AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date'";
        }else if($contaccess==4){
            $status_diff = "AND req.status=12";
        }else{
            $stat = $datatable['query']['mrf_stat'];
            // $status_diff = "AND (req.status=6 OR req.status=5 OR req.status=12)";
            $status_diff = "AND (req.status=6 OR req.status=5)";
            if ((int) $stat !== 0) {
                $stat_where = "AND req.status = $stat";
            }
        }
        // if 
        if($pagefetchtype=='myreq'){
            if($contaccess==2){
                 $status_diff = "AND (status=5 OR status=6)";
                //  $start_date = $datatable['query']['startDate'];
                //  $end_date = $datatable['query']['endDate'];
                 $date_q= "AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date'";
            }
            // $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat WHERE requested_by=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' " . $status_diff ." ". $position_where . " " . $acc_where . " " . $stat_where . " " . $status_diff_pending;
            $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat WHERE requested_by=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID " . $date_q ." ". $status_diff ." ". $position_where . " " . $acc_where . " " . $stat_where . " " . $status_diff_pending;

        }else if($pagefetchtype == 'allreq'){
            $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID " . $date_q ." ". $status_diff ." ". $position_where . " " . $acc_where . " " . $stat_where . " " . $status_diff_pending;
        }else if($pagefetchtype == 'missedReq'){
            $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' " . $status_diff ." ". $position_where . " " . $acc_where;
        }else{
            // for approval 
            //if approver
            $c_apptype = $this->general_model->custom_query("SELECT emp_type FROM `tbl_irequest_approver` WHERE emp_ID=$emp_id");         
            $count_rec = $this->general_model->custom_query("SELECT COUNT(emp_type) as num_assigned FROM `tbl_irequest_approver` WHERE emp_ID=$emp_id");
            if($c_apptype!=null){
                // FOR PENDING 
                if($contaccess==1){
                    $appl_stat= "AND appl.status=4 AND req.status=2";
                }else{
                    $appl_stat= "AND (appl.status=5 OR appl.status=6 OR appl.status=12)";
                    $start_date = $datatable['query']['startDate'];
                    $end_date = $datatable['query']['endDate'];
                    $date_q= "AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date'";
                }


                // if ($c_apptype[0]->emp_type=='both'){
                //     $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, appl.status as aplstat FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' " . $position_where . " " . $acc_where . " " . $stat_where . " " . $appl_stat . " " . $status_diff_pending;                 
                // }else if($c_apptype[0]->emp_type=='agent' || $c_apptype[0]->emp_type=='admin'){
                //     $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, appl.status as aplstat FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' " . $position_where . " " . $acc_where . " " . $appl_stat . " " . $stat_where . " " . $status_diff_pending;
                // }

               if($c_apptype[0]->emp_type=='agent' || $c_apptype[0]->emp_type=='admin'){
                    $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, appl.status as aplstat FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND appr.emp_type=pos.class AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id " . $date_q . " " . $position_where . " " . $acc_where . " " . $appl_stat . " " . $stat_where . " " . $status_diff_pending;
                }else{
                    $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, appl.status as aplstat FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr, tbl_irequest_request_approval appl WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND appl.request_ID=req.irequest_ID AND appl.emp_ID=$emp_id AND appl.emp_ID=$emp_id " . $date_q . " " . $position_where . " " . $acc_where . " " . $stat_where . " " . $appl_stat . " " . $status_diff_pending;                 
                }
            }else{
                $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_approver appr WHERE appr.emp_ID=$emp_id AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=2 " . $date_q ." ". $status_diff ." ". $position_where . " " . $acc_where . " " . $stat_where . " " . $status_diff_pending;
            }
        }
        if ($datatable['query']['search_irequestid'] != '') {
            $keyword = $datatable['query']['search_irequestid'];
            $where = "req.irequest_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function list_irequest_recruitment_datatable(){
        $empID = $this->session->emp_id;
        $datatable = $this->input->post('datatable');
        $position = $datatable['query']['position'];
        $acc = $datatable['query']['req_department'];
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];
        $t = $datatable['query']['type'];
        $position_where = "";
        $acc_where = "";
        $stat_where = "";
        
        if ((int) $position !== 0) {
            $position_where = "AND req.position_ID = $position";
        }
        if ((int) $acc !== 0) {
            $acc_where = "AND req.requesting_dept = $acc";
        }

        if($t==1){
            $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, reqrec.irequestRecruitment_ID, reqrec.numberOfRecruit, reqrec.status_ID FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_recruitment reqrec WHERE reqrec.status_ID IN (2, 16) AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND reqrec.status_ID=stat.status_ID AND req.status = 5 AND reqrec.irequest_ID=req.irequest_ID " . $position_where ." ". $acc_where;
        }else if($t==2){
            $stat = $datatable['query']['status'];
            if ((int) $stat !== 0) {
                $stat_where = "AND reqrec.status_ID = $stat";
            }
            $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, reqrec.irequestRecruitment_ID, reqrec.numberOfRecruit, reqrec.status_ID as reqstatus FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_recruitment reqrec WHERE (reqrec.status_ID=3 OR reqrec.status_ID=7) AND pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND reqrec.status_ID=stat.status_ID AND (req.status = 5) AND reqrec.irequest_ID=req.irequest_ID AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' " . $position_where ." ". $acc_where . " " . $stat_where;
        }else if($t==3){
            $stat = $datatable['query']['status'];
            if ((int) $stat !== 0) {
                $stat_where = "AND reqrec.status_ID = $stat";
            }
            $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, reqrec.irequestRecruitment_ID, reqrec.numberOfRecruit, reqrec.status_ID as reqstatus FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_recruitment reqrec WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND reqrec.status_ID=stat.status_ID AND req.status = 5 AND reqrec.irequest_ID=req.irequest_ID AND req.requested_by=$empID AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' " . $position_where ." ". $acc_where ." ". $stat_where;
        }else if($t==4){
            $stat = $datatable['query']['status'];
            if ((int) $stat !== 0) {
                $stat_where = "AND reqrec.status_ID = $stat";
            }
            // Get condition of this 
            // $c_apptype = $this->general_model->custom_query("SELECT emp_type FROM `tbl_irequest_approver` WHERE emp_ID=$empID");
            // if($c_apptype[0]->emp_type=='agent' || $c_apptype[0]->emp_type=='admin'){
                $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, reqrec.irequestRecruitment_ID, reqrec.numberOfRecruit, reqrec.status_ID as reqstatus FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_recruitment reqrec, tbl_irequest_request_approval appreq WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND reqrec.status_ID=stat.status_ID AND req.status = 5 AND reqrec.irequest_ID=req.irequest_ID AND appreq.request_ID = req.irequest_ID AND appreq.emp_ID=$empID AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' " . $position_where ." ". $acc_where ." ". $stat_where;
            // }else{

            // }          
        }else if($t==5){
            $stat = $datatable['query']['status'];
            if ((int) $stat !== 0) {
                $stat_where = "AND reqrec.status_ID = $stat";
            }
            $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc, reqrec.irequestRecruitment_ID, reqrec.numberOfRecruit, reqrec.status_ID as reqstatus FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat, tbl_irequest_recruitment reqrec WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND reqrec.status_ID=stat.status_ID AND req.status = 5 AND reqrec.irequest_ID=req.irequest_ID AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' " . $position_where ." ". $acc_where ." ". $stat_where;
        }
        
        if ($datatable['query']['search_irequestid'] != '') {
            $keyword = $datatable['query']['search_irequestid'];
            $where = "req.irequest_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }      
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }


    public function list_irequest_hired_datatable()
    {
        $datatable = $this->input->post('datatable');
        $pos_id = $datatable['query']['position'];
        $apid = $datatable['query']['hired'];
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];
        $pos_where = "";
        $apid_where = "";

        if ((int) $pos_id !== 0) {
            $pos_where = "AND iappn.position_ID = $pos_id";
        }

        if ((int) $apid  !== 0) {
            $apid_where = "AND app.apid = $apid";
        }    

        // $order = "ORDER BY apr.level asc";
        // if ((string) $apptype !== "") {
        //     $app_where = "AND apr.emp_type = '$apptype'";
        // }

        // $query['query'] = "SELECT apr.*,ap.fname,ap.lname,ap.mname FROM tbl_irequest_approver apr, tbl_employee emp, tbl_applicant ap WHERE apr.emp_ID=emp.emp_id AND emp.apid=ap.apid " . $app_where ." ORDER BY apr.level ASC";
      
        $query['query'] = "SELECT app.*,iapp.irecruitApplicant_ID,iapp.date_hired,iappn.application_ID,iappn.status_ID as applicationStat, iappn.position_ID,iappn.dated_status,irec.irequestRecruitment_ID,irec.status_ID as irecStatus,irec.numberOfRecruit, manpower.irequest_ID,manpower.manpower_requested, manpower.manpower_supplied,manpower.position_ID,manpower.datedStatus, pos.pos_id,pos.pos_name,pos.pos_details,pos.site_ID,pos.isHiring FROM tbl_applicant app, tbl_irecruit_applicant iapp, tbl_irecruit_application iappn, tbl_irequest_recruitment irec,tbl_irequest_manpower_request manpower, tbl_position pos WHERE app.apid = iappn.apid AND iapp.application_ID = iappn.application_ID AND irec.irequest_ID = manpower.irequest_ID AND irec.irequestRecruitment_ID = iapp.irequestRecruitment_ID AND pos.pos_id = manpower.position_ID AND pos.pos_id = iappn.position_ID AND DATE(iapp.date_hired) >= '$start_date' AND DATE(iapp.date_hired) <= '$end_date' " . $pos_where ." ". $apid_where;
        if ($datatable['query']['search_irequestid'] != '') {
            $keyword = $datatable['query']['search_irequestid'];
            $where = "manpower.irequest_ID LIKE '%" . (int) $keyword . "%'";
            $query['search']['append'] = " AND ($where)";
            $query['search']['total'] = " AND ($where)";
        }  
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function list_irequest_approver_datatable()
    {
        $datatable = $this->input->post('datatable');
        $apptype = $datatable['query']['app_type'];
        $app_where = "";
        $order = "ORDER BY apr.level asc";
        if ((string) $apptype !== "") {
            $app_where = "AND apr.emp_type = '$apptype'";
        }
        $query['query'] = "SELECT apr.*,ap.fname,ap.lname,ap.mname FROM tbl_irequest_approver apr, tbl_employee emp, tbl_applicant ap WHERE apr.emp_ID=emp.emp_id AND emp.apid=ap.apid " . $app_where ." ORDER BY apr.level ASC";
        
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function emp_details_ir_approvers(){
        $app_id = array();
        $loop=0;
        $app=$this->general_model->custom_query("SELECT apr.*,ap.fname,ap.lname,ap.mname FROM tbl_irequest_approver apr, tbl_employee emp, tbl_applicant ap WHERE apr.emp_ID=emp.emp_id AND emp.apid=ap.apid");  
        foreach($app as $a){
                $app_id[$loop] = $a->emp_ID;
                $loop++;
        }
       $q=$this->general_model->custom_query("SELECT DISTINCT(emp.emp_ID),ap.fname,ap.apid,ap.lname,ap.mname FROM tbl_employee emp, tbl_applicant ap WHERE emp.emp_ID NOT IN(".implode (", ", $app_id).") AND emp.apid=ap.apid AND emp.isActive='Yes' ORDER BY ap.lname asc");
       echo json_encode($q);
    }

// START MIC CODE

private function qry_update_irequest_approval_level_to($level_to, $level_from, $emp_type){
    $data['level'] = $level_from;
    $where = "emp_type = '$emp_type' AND level = $level_to";
    $this->general_model->update_vals($data, $where, 'tbl_irequest_approver');
}
    
private function qry_update_irequest_approval_level($approver_id, $level_to){
    $data['level'] = $level_to;
    $where = "approver_ID = $approver_id";
    $this->general_model->update_vals($data, $where, 'tbl_irequest_approver');
}
    
public function switch_irequest_approval_level_to($approver_id, $level_to, $level_from, $emp_type){
    $this->qry_update_irequest_approval_level_to($level_to, $level_from, $emp_type);
    $this->qry_update_irequest_approval_level($approver_id, $level_to);
}

private function update_irequest_approval_levels_add($level, $emp_type){
    $query = "UPDATE tbl_irequest_approver SET level = level+1 WHERE emp_type = '$emp_type' AND level >= $level";
    return $this->general_model->custom_query_no_return($query);
}

private function update_irequest_approval_levels_remove($level, $emp_type){
    $query = "UPDATE tbl_irequest_approver SET level = level-1 WHERE emp_type = '$emp_type' AND level > $level";
    return $this->general_model->custom_query_no_return($query);
}

private function qry_max_approval_level($emp_type){
    $fields = "MAX(level) level";
    $where = "emp_type = '$emp_type'";
    return $this->general_model->fetch_specific_val($fields, $where, "tbl_irequest_approver");
}

public function insert_irequest_approver(){
    $emp_type = $this->input->post('approval');
    $level = $this->input->post('level');
    $reqapp['emp_ID'] = $this->input->post('emp');
    $reqapp['level'] = $level;
    $reqapp['emp_type'] = $emp_type;
    $reqapp['added_by'] = $this->session->emp_id;

    $max_level = $this->qry_max_approval_level($emp_type);
    if($level < (int)$max_level->level+1){
        $this->update_irequest_approval_levels_add($level, $emp_type);
    }
    $this->general_model->insert_vals($reqapp, "tbl_irequest_approver");
}

public function remove_irequest_approver($level, $emp_type){
    $max_level = $this->qry_max_approval_level($emp_type);
    if($level < (int)$max_level->level+1){
        $this->update_irequest_approval_levels_remove($level, $emp_type);
    }
}

// SET IREQUEST APPROVAL
private function qry_irequest_approvers($emp_type){
    $fields = "appr.approver_ID, appr.emp_ID, appr.level, appr.emp_type, appr.added_by, users.uid";
    $where = "users.emp_ID = appr.emp_ID AND emp_type = '$emp_type'";
    $table = "tbl_irequest_approver appr, tbl_user users";
    return $this->general_model->fetch_specific_vals($fields, $where, $table, "level ASC");
}

private function qry_irequest_approver_deadline_days(){
    $fields = "days";
    $where = "apduration_id IS NOT NULL";
    return $this->general_model->fetch_specific_val($fields, $where, "tbl_irequest_approval_duration");
}

public function get_irequest_details($irequest_id){
    $fields = "irequest_ID, requesting_dept, position_ID, manpower_requested, manpower_supplied, hiringPurpose_ID, hiringPurpose_desc, educationalAttainment, gender, description, descriptionAttachment, qualifications, aftapproved, status, requested_by, dateRequested, dateStartRequired, dateEndRequired, uid";
    $where = "emp_ID = requested_by AND irequest_ID = $irequest_id";
    $table = "tbl_irequest_manpower_request, tbl_user";
    return  $this->general_model->fetch_specific_val($fields,  $where, $table);
}

public function set_irequest_approval($irequest_id, $emp_type){
    $dateTime = $this->get_current_date_time();
    $irequest_details = $this->get_irequest_details($irequest_id);
    $deadline_duration = $this->qry_irequest_approver_deadline_days();
    $days_deadline_duration = 2;
    if(count($deadline_duration) > 0){
        $days_deadline_duration = $deadline_duration->days;
    }
    $approvers = $this->qry_irequest_approvers($emp_type);
    if(count($irequest_details) > 0){
        $deadline = [];
        $date_requested = date_format(date_create($irequest_details->dateRequested), "Y-m-d");
        foreach($approvers as $approvers_index=>$approvers_row){
            $deadline_date = Date("Y-m-d", strtotime($date_requested . "$days_deadline_duration days"));
            $deadline_date_time = $deadline_date . " 23:59:59";
            $approval_status = 2; 
            $deadline_status = 2; 
            if($approvers_row->level == 1){
                $approval_status = 4;
                // Send notifications 
                $recipient[0] = [
                    'userId' => $approvers_row->uid,
                ];
                $link = "irequest/my_requestapproval";
                $notif_message = "has sent you a Manpower Request and it needs your approval. <br><small><b>MRF-ID</b>: " . str_pad($irequest_id, 8, '0', STR_PAD_LEFT) . " </small>";
            }
            $request_approval['request_ID'] =$irequest_id;
            $request_approval['emp_ID'] = $approvers_row->emp_ID;
            $request_approval['status'] = $approval_status;
            $request_approval['level'] = $approvers_row->level;
            $request_approval['dated_status'] = $dateTime['dateTime'];
            $request_approval_id = $this->general_model->insert_vals_last_inserted_id($request_approval, 'tbl_irequest_request_approval');
            $deadline[$approvers_index] = [
                'module_ID' => 6,
                'request_ID' => $request_approval_id,
                'requestor' => $irequest_details->uid,
                'userId' => $approvers_row->uid,
                'approvalLevel' => $approvers_row->level,
                'deadline' => $deadline_date_time,
                'status_ID' => $deadline_status
            ];
            $date_requested = $deadline_date; 
        }
        if(count($deadline) > 0){
            $this->general_model->batch_insert($deadline, 'tbl_deadline');
        }
    }
    return $this->set_notif($this->session->userdata('uid'), $notif_message, $link, $recipient);
}
// END MIC CODE

    // public function insert_irequest_approver(){
    //     $emp_type = $this->input->post('approval');
    //     $level = $this->input->post('level');
    //     $reqapp['emp_ID'] = $this->input->post('emp');
    //     $reqapp['level'] = $level;
    //     $reqapp['emp_type'] = $emp_type;
    //     $reqapp['added_by'] = $this->session->emp_id;

    //     $max_level = $this->qry_max_approval_level($emp_type);
    //     if($level < (int)$max_level->level+1){
    //         $this->update_irequest_approval_levels($level, $emp_type);
    //     }
    //     $this->general_model->insert_vals($reqapp, "tbl_irequest_approver");
    // }

    public function update_approver_details(){
        $approver_id = $this->input->post("ireq_approver");
        $emp_type = $this->input->post('etype');
        $level_to = $this->input->post('level');
        $level_from = $this->input->post('olevel');
        $reqapp['level'] = $level_to;
        $where = "approver_ID = $approver_id";
        $this->switch_irequest_approval_level_to($approver_id, $level_to, $level_from, $emp_type);
    }
    public function save_deadlineApproval(){
        $ins['days']=$this->input->post('deadline');
        $where = "apduration_id = 1";
        $this->general_model->update_vals($ins, $where, 'tbl_irequest_approval_duration');
    }
    public function manpower_reqsup(){
        $id = $this->input->post('ireq');
        $up_num = $this->input->post('update_num');
        if($up_num=="" || $up_num==null){
            $up_num = 0;
        }
        $change = $this->input->post('type_change');
        $allow = [];
        $check = $this->general_model->custom_query("SELECT manpower_requested,manpower_supplied FROM `tbl_irequest_manpower_request` WHERE irequest_ID = $id");
        
        if($change=="ned"){
            if($check[0]->manpower_supplied > $up_num){
                //don't save
                $allow['notif'] = 0;
                $allow['exit'] = 2;
            }else{
                // save 
                $allow['notif'] = 1;
                $save['manpower_requested'] = $up_num;
                $wherereq = "irequest_ID = $id";        

                // Set Manpower to Completed 
                if($check[0]->manpower_supplied == $up_num){
                    $save2['status_ID'] = 3;
                    $allow['exit'] = 1;
                }else{
                    $save2['status_ID'] = 2;
                    $allow['exit'] = 0;
                }
                $this->general_model->update_vals($save, $wherereq, 'tbl_irequest_manpower_request');
                $this->general_model->update_vals($save2, $wherereq, 'tbl_irequest_recruitment');
            }
        }else{
            if($check[0]->manpower_requested < $up_num){
                //don't save
                $allow['notif'] = 0;
                $allow['exit'] = 2;
            }else{
                // save 
                $allow['notif'] = 1;
                $save['manpower_supplied'] = $up_num;
                $wherereq = "irequest_ID = $id";

                  // Set Manpower to Completed 
                if($check[0]->manpower_requested == $up_num){
                    $save2['status_ID'] = 3;
                    $allow['exit'] = 1;
                }else{
                    $save2['status_ID'] = 2;
                    $allow['exit'] = 0;
                }
                $this->general_model->update_vals($save, $wherereq, 'tbl_irequest_manpower_request');
                $this->general_model->update_vals($save2, $wherereq, 'tbl_irequest_recruitment');
            }
        }
        echo json_encode($allow);
    }
    public function manpowerto_completed(){
        $id = $this->input->post('ireq');
        $stat = $this->input->post('stat');
        $save['status_ID'] = $stat;
        $wherereq = "irequest_ID = $id";  
        $allowed = 0;     
      
        // Checker if manpower supplied equals to manpower required 
        if($stat==2){
            $q = $this->general_model->custom_query("SELECT irequest_ID FROM `tbl_irequest_manpower_request`WHERE manpower_requested = manpower_supplied AND irequest_ID=$id");
            if($q!=null){
                //No to revert
                $allowed = 0; 
            }else{
                $this->general_model->update_vals($save, $wherereq, 'tbl_irequest_recruitment');
                $allowed = 1; 
            }
        }else{
            $this->general_model->update_vals($save, $wherereq, 'tbl_irequest_recruitment');
            $allowed = 1; 
        } 
        echo json_encode($allowed);    
    }
    //temporary here
    public function cronjob_missed(){
        $empID = $this->session->emp_id;
        $dateTime = $this->get_current_date_time();
        $dnow = $dateTime['dateTime'];
        // $query = $this->general_model->custom_query("SELECT DISTINCT(ra.requestApproval_ID),ra.request_ID,ra.level FROM tbl_deadline d, tbl_irequest_request_approval ra, tbl_irequest_manpower_request req WHERE d.module_ID=6 AND d.deadline < '$dnow' AND ra.request_ID = d.request_ID AND req.irequest_ID = d.request_ID AND req.irequest_ID = ra.request_ID");
        // $d_query = $this->general_model->custom_query("SELECT DISTINCT(d.deadline_ID) FROM tbl_deadline d, tbl_irequest_request_approval ra, tbl_irequest_manpower_request req WHERE d.module_ID=6 AND d.deadline < '$dnow' AND ra.request_ID = d.request_ID AND req.irequest_ID = d.request_ID AND req.irequest_ID = ra.request_ID");
        $query = $this->general_model->custom_query("SELECT DISTINCT(ra.requestApproval_ID),ra.request_ID,ra.level,d.deadline_ID FROM tbl_deadline d, tbl_irequest_request_approval ra, tbl_irequest_manpower_request req WHERE d.module_ID=6 AND d.deadline < '$dnow' AND ra.requestApproval_ID = d.request_ID");
        $ar_approversdeadline = array();
        $ar_deadline = array();
        $loop=0;
        $loop2=0;
        foreach($query as $q1){
            $rid = $q1->requestApproval_ID;
            $req_Id = $q1->request_ID;
            if($q1->level == 1 || $q1->level == 2){
                // get next level
                $ql = $q1->level;
                $ql++;
                $query2 = $this->general_model->custom_query("SELECT requestApproval_ID,status FROM tbl_irequest_request_approval WHERE request_ID = $req_Id AND level = $ql AND status=2");
                $r['status'] = 4;
                $rnl = $query2[0]->requestApproval_ID;
                $wherereq = "requestApproval_ID = $rnl";
                if($query2!=null){
                    $this->general_model->update_vals($r, $wherereq, 'tbl_irequest_request_approval');
                }
            }else if($q1->level== 3){
                //set Manpower to Missed
                $re['status'] = 12;
                $wherereq2 = "irequest_ID = $req_Id";
                $this->general_model->update_vals($re, $wherereq2, 'tbl_irequest_manpower_request');
            }

            $query3 = $this->general_model->custom_query("SELECT request_ID,requestApproval_ID FROM tbl_irequest_request_approval WHERE request_ID=$req_Id AND (status=5 OR status=6 OR status=2 OR status=3 OR status=4)");

            // if detected all are missed 
            if($query3==null){
                $re['status'] = 12;
                $wherereq2 = "irequest_ID = $req_Id";
                $this->general_model->update_vals($re, $wherereq2, 'tbl_irequest_manpower_request');
            }

            $ar_approversdeadline[$loop] = [
                'requestApproval_ID' => $q1->requestApproval_ID,
                'status' => 12
            ];
            $loop++;

            $ar_deadline[$loop2] = [
                'deadline_ID' => $q1->deadline_ID,
                'status_ID' => 12
            ];
            $loop2++;
        }
        $this->general_model->batch_update($ar_deadline, 'deadline_ID', 'tbl_deadline');
        $this->general_model->batch_update($ar_approversdeadline, 'requestApproval_ID', 'tbl_irequest_request_approval');
    }
    public function test1(){
        $ad3 = date('Y-m-d', strtotime("+3 day")) . ' 23:59:59'; 
        // var_dump($ad3);
        echo json_encode($ad3);
    }
    public function queryApproval_employees_level(){
        $ap_type = $this->input->post('approval');
        $toreturn = [];
        $approver = $this->general_model->custom_query("SELECT approver_ID,emp_ID,emp_type,level FROM `tbl_irequest_approver` WHERE emp_type = '$ap_type'");     
        if($approver!=null){
            //Get highest level + 1
            $appmax = $this->general_model->custom_query("SELECT MAX(level) as highest FROM `tbl_irequest_approver` WHERE emp_type = '$ap_type'");
            $toreturn['level'] = 1;
            $toreturn['highest_num'] = $appmax[0]->highest + 1;       
            //Get employees not on the list
            $toreturn['employees'] = $this->emp_details_check_approvers($ap_type);
        }else{
            // level is option 1 ONLY 
            $toreturn['level'] = 0;
            $toreturn['employees'] = $this->general_model->custom_query("SELECT DISTINCT(emp.emp_ID),ap.fname,ap.apid,ap.lname,ap.mname FROM tbl_employee emp, tbl_applicant ap WHERE emp.apid=ap.apid AND emp.isActive='Yes' ORDER BY ap.lname asc");
        }
        
        echo json_encode($toreturn);
    }
    protected function emp_details_check_approvers($ap_type){
        $app_id = array();
        $loop=0;
        $app=$this->general_model->custom_query("SELECT apr.*,ap.fname,ap.lname,ap.mname FROM tbl_irequest_approver apr, tbl_employee emp, tbl_applicant ap WHERE apr.emp_ID=emp.emp_id AND emp.apid=ap.apid AND emp_type = '$ap_type'");  
        foreach($app as $a){
                $app_id[$loop] = $a->emp_ID;
                $loop++;
        }
       return $this->general_model->custom_query("SELECT DISTINCT(emp.emp_ID),ap.fname,ap.apid,ap.lname,ap.mname FROM tbl_employee emp, tbl_applicant ap WHERE emp.emp_ID NOT IN(".implode (", ", $app_id).") AND emp.apid=ap.apid AND emp.isActive='Yes' ORDER BY ap.lname asc");
    }
    public function set_irequest_approval_statuses($irequest_id, $approval_id, $status_id){
        $appr_dead_records = $this->qry_irequest_approveral_deadline_records($irequest_id);
        if(count($appr_dead_records) > 0){
        $current_approver = array_merge(array_filter($appr_dead_records, function($e) use ($approval_id){
        return ($e->requestApproval_ID == $approval_id);
        }
        ));
        if(count($current_approver) > 0){
        $current_level = $current_approver[0]->level;
        $this->update_irequest_deadline_statuses(7, $current_approver[0]->deadline_ID);
        if($status_id == 6){ //if disapproved
        $succeeding_approvers_stat = 7;
        if($current_level < count($appr_dead_records)){ // less than == max approver
        $succeeding_approvers = array_merge(array_filter($appr_dead_records, function($e) use ($current_level){
        return ((int)$e->level > $current_level);
        }
        ));
        $this->update_irequest_deadline_statuses($succeeding_approvers_stat, implode(", ",array_column($succeeding_approvers, 'deadline_ID')));
        if(count($succeeding_approvers) > 0){
        // set the rest approver greater than current to cancelled
        $this->update_irequest_approval_statuses($succeeding_approvers_stat, implode(", ",array_column($succeeding_approvers, 'requestApproval_ID')));
        // set the rest of deadlines from current approver to max to cancelled
        $this->update_irequest_deadline_statuses($succeeding_approvers_stat, implode(", ",array_column($succeeding_approvers, 'deadline_ID')));
        }
        }
        }else{
        $succeeding_approvers_stat = 3;
        // update current deadline to complete
        $this->update_irequest_deadline_statuses($succeeding_approvers_stat, implode(", ",array_column($current_approver, 'deadline_ID')));
    }
    }
    }
    }
    private function qry_irequest_approveral_deadline_records($irequest_id){
        $fields = "dead.deadline_ID, dead.userId, appr.requestApproval_ID, appr.request_ID, appr.emp_ID, appr.status, appr.level, appr.remarks, appr.dated_status";
        $where = "dead.request_ID = appr.requestApproval_ID AND dead.module_ID = 6 AND appr.request_ID = $irequest_id";
        $table = "tbl_irequest_request_approval appr, tbl_deadline dead";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "level ASC");
    }
    private function update_irequest_approval_statuses($status, $approval_ids){
        $data['status'] = $status;
        $where = "requestApproval_ID IN ($approval_ids)";
        $this->general_model->update_vals($data, $where, 'tbl_irequest_request_approval');
        }
        
    private function update_irequest_deadline_statuses($status, $deadline_ids){
        $data['status_ID'] = $status;
        $where = "deadline_ID IN ($deadline_ids)";
        $this->general_model->update_vals($data, $where, 'tbl_deadline');
    }

    // START MIC CODE
    // STAT DATA
    private function qry_irequest_account_count($start_date, $end_date){
        $query = "SELECT COUNT(req.irequest_ID) requestCount, acc.acc_id, acc.acc_name, SUM(req.status = 2) as pending, SUM(req.status = 5) as approved, SUM(req.status = 6) as disapproved, SUM(req.status = 12) as missed FROM tbl_irequest_manpower_request req, tbl_account acc, tbl_position pos WHERE pos.pos_id = req.position_ID AND acc.acc_id = req.requesting_dept AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' GROUP BY acc.acc_id";
        return $this->general_model->custom_query($query);
    }

    private function qry_irequest_approval_count($start_date, $end_date){
        $query = "SELECT appr.emp_ID, app.fname, app.mname, app.lname, app.nameExt, COUNT(appr.requestApproval_ID) apprCount, SUM(appr.status = 4) as pending, SUM(appr.status = 5) as approved, SUM(appr.status = 6) as disapproved, SUM(appr.status = 12) as missed FROM tbl_applicant app, tbl_employee emp, tbl_irequest_request_approval appr, tbl_irequest_manpower_request req WHERE app.apid = emp.apid AND emp.emp_id = appr.emp_ID AND appr.request_ID = req.irequest_ID AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' GROUP BY appr.emp_ID";
        return $this->general_model->custom_query($query);
    }

    private function qry_irecruit_count($start_date, $end_date){
        $query = "SELECT req.position_ID, COUNT(rec.irequestRecruitment_ID) recCount, SUM(rec.status_ID = 2) as pending, SUM(rec.status_ID = 3) as completed, SUM(rec.status_ID = 12) as missed FROM tbl_irequest_recruitment rec, tbl_irequest_manpower_request req, tbl_position pos WHERE pos.pos_id = req.position_ID AND req.irequest_ID = rec.irequest_ID AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' GROUP BY req.position_ID";
        return $this->general_model->custom_query($query);
    }

    private function qry_detailed_irequest_req_appr($start_date, $end_date){
        $fields = "req.irequest_ID, req.requesting_dept, acc.acc_name, req.position_ID, pos.pos_details , req.requested_by, appReq.fname requestorFname, appReq.mname requestorMname, appReq.lname requestorLname, req.position_ID, req.manpower_requested, req.manpower_supplied, pur.purpose, req.hiringPurpose_desc, req.hiringtype_desc, req.educationalAttainment, req.gender, req.aftapproved, req.description reqDescription, req.qualifications, req.status reqStat, statReq.description statReqDescription, req.days_needed, req.dateRequested, req.dateStartRequired, req.dateEndRequired,  appr.requestApproval_ID, appr.emp_ID, appAppr.fname, appAppr.mname, appAppr.lname, appr.status apprStat, statAppr.description apprStatDesc, appr.level, appr.remarks, appr.dated_status, dead.deadline_ID, dead.deadline, dead.status_ID, site.code";
        $where = "site.site_ID = req.site_ID AND dead.request_ID = appr.requestApproval_ID AND pur.hiringPurpose_ID = req.hiringPurpose_ID AND dead.module_ID = 6 AND dead.userId = users.uid AND users.emp_id = appr.emp_ID AND statReq.status_ID = req.status AND statAppr.status_ID = appr.status AND pos.pos_id = req.position_ID AND acc.acc_id = req.requesting_dept AND appReq.apid = empReq.apid AND empReq.emp_id = req.requested_by AND appAppr.apid = empAppr.apid AND empAppr.emp_id = appr.emp_ID AND appr.request_ID = req.irequest_ID AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date'";
        $table = "tbl_irequest_manpower_request req, tbl_irequest_hiring_purpose pur, tbl_irequest_request_approval appr, tbl_applicant appReq, tbl_employee empReq, tbl_applicant appAppr, tbl_employee empAppr, tbl_account acc, tbl_position pos, tbl_status statReq, tbl_status statAppr, tbl_deadline dead, tbl_user users, tbl_site site";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "req.irequest_ID, appr.level ASC");
    }
    private function qry_irequest_total_count($start_date, $end_date){
        $query = "SELECT COUNT(req.irequest_ID) requestCount, SUM(req.status = 2) as pending, SUM(req.status = 5) as approved, SUM(req.status = 6) as disapproved, SUM(req.status = 12) as missed FROM tbl_irequest_manpower_request req, tbl_account acc, tbl_position pos WHERE pos.pos_id = req.position_ID AND acc.acc_id = req.requesting_dept AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date'";
        return $this->general_model->custom_query($query);
    }
    private function qry_irequest_position_count($start_date, $end_date){
        $query = "SELECT COUNT(req.irequest_ID) requestCount, pos.pos_id, pos.pos_details , SUM(req.status = 2) as pending, SUM(req.status = 5) as approved, SUM(req.status = 6) as disapproved, SUM(req.status = 12) as missed FROM tbl_irequest_manpower_request req, tbl_account acc, tbl_position pos WHERE pos.pos_id = req.position_ID AND acc.acc_id = req.requesting_dept AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date' GROUP BY pos.pos_id ORDER BY requestCount DESC";
        return $this->general_model->custom_query($query);
    }
    public function sample_date_diff(){
        $from = strtotime("2020-11-23 17:41:14");
        $to = strtotime("2020-11-26 17:41:14");
        $duration = $this->get_human_time_format($from, $to, 0);
        var_dump($duration);
    }
    private function get_irequest_data($start_date, $end_date){
        $data['error'] = 0;
        $data['error_details'] = "";
        $error_list = [];
        $distinct_request_id = []; 
        $error_count = 0;
        $data_account = $this->qry_irequest_account_count($start_date, $end_date);
        $total_count = $this->qry_irequest_total_count($start_date, $end_date);
        $data_appr = $this->qry_irequest_approval_count($start_date, $end_date);
        $data_details_req_appr = $this->qry_detailed_irequest_req_appr($start_date, $end_date);
        $data_position = $this->qry_irequest_position_count($start_date, $end_date);
        if(count($data_account) < 1){
            $error_list[$error_count] = "Account data not found";
            $error_count++;
        }
        if(count($data_appr) < 1){
            $error_list[$error_count] = "Approver data not found";
            $error_count++;
        }
        if(count($data_details_req_appr) < 1){
            $error_list[$error_count] = "Detail request and approval data not found";
            $error_count++;
        }
        if(count($total_count) < 1){
            $error_list[$error_count] = "Total Count data not found";
            $error_count++;
        }
        if(count($error_list) < 1){
            $distinct_request_id = array_column($data_details_req_appr, 'irequest_ID');
            $distinct_request_id = array_merge(array_unique($distinct_request_id));
            $req_ave_approval = [];
            for($req_loop = 0; $req_loop < count($distinct_request_id); $req_loop++){
                $hours_appr_req = [];
                $request_id = $distinct_request_id[$req_loop];
                $req_appr_details = array_merge(array_filter($data_details_req_appr, function ($e) use ($request_id) {
                        return $e->irequest_ID == $request_id;
                    }
                ));
                if(count($req_appr_details) > 0){
                    foreach($req_appr_details as $req_appr_details_index=>$req_appr_details_row){
                        if(($req_appr_details_row->apprStat == 5) || ($req_appr_details_row->apprStat == 6)){
                            $requested_date = strtotime($req_appr_details_row->dateRequested);
                            $approval_date = strtotime($req_appr_details_row->dated_status);
                            $duration = $this->get_human_time_format($requested_date, $approval_date, 0);
                            $hours_appr_req[$req_appr_details_index] = $duration['hoursTotal'];
                        }else{
                            $hours_appr_req[$req_appr_details_index] = 0;
                        }
                    }
                }
                $req_ave_approval[$request_id] = round(array_sum($hours_appr_req)/count($hours_appr_req), 2);
            }
            foreach($data_account as $data_account_row){
                $hours_total_arr = [];
                $hours_total_arr_count = 0;
                $average = 0;
                $ave_str = "0";
                $ave_str_hr = 0;
                $acc_id = $data_account_row->acc_id;
                $req_appr_data = array_merge(array_filter($data_details_req_appr, function ($e) use ($acc_id) {
                        return $e->requesting_dept == $acc_id;
                    }
                ));
                if(count($req_appr_data) > 0){
                    foreach($req_appr_data as $req_appr_data_row){
                        if(($req_appr_data_row->apprStat == 5) || ($req_appr_data_row->apprStat == 6)){
                            $requested_date = strtotime($req_appr_data_row->dateRequested);
                            $approval_date = strtotime($req_appr_data_row->dated_status);
                            $duration = $this->get_human_time_format($requested_date, $approval_date, 0);
                            $hours_total_arr[$hours_total_arr_count] = $duration['hoursTotal'];
                            $hours_total_arr_count +=1;
                        }
                    }
                    $hours_total_arr = array_filter($hours_total_arr);
                    if(count($hours_total_arr) > 0) {
                        $average = array_sum($hours_total_arr)/count($hours_total_arr);
                    }
                    if($average > 0){
                        $ave_str_hr = round($average, 2);
                        $ave_str = $this->get_formatted_time_with_seconds($average);
                    }
                }
                $data_account_row->{"averageTimeApproval"} = $ave_str;
                $data_account_row->{"averageTimeApprovalHour"} = $ave_str_hr;
                
            }
            $data['account_stat'] = $data_account;
            foreach($data_appr as $data_appr_row){
                $hours_total_arr = [];
                $hours_total_arr_count = 0;
                $average = 0;
                $ave_str = "0";
                $ave_str_hr = 0;
                $emp_id = $data_appr_row->emp_ID;
                $req_appr_data = array_merge(array_filter($data_details_req_appr, function ($e) use ($emp_id) {
                        return $e->emp_ID == $emp_id;
                    }
                ));
                if(count($req_appr_data) > 0){
                    foreach($req_appr_data as $req_appr_data_row){
                        if(($req_appr_data_row->apprStat == 5) || ($req_appr_data_row->apprStat == 6)){
                            $requested_date = strtotime($req_appr_data_row->dateRequested);
                            $approval_date = strtotime($req_appr_data_row->dated_status);
                            $duration = $this->get_human_time_format($requested_date, $approval_date, 0);
                            $hours_total_arr[$hours_total_arr_count] = $duration['hoursTotal'];
                            $hours_total_arr_count +=1;
                        }
                    }
                    $hours_total_arr = array_filter($hours_total_arr);
                    if(count($hours_total_arr) > 0) {
                        $average = array_sum($hours_total_arr)/count($hours_total_arr);
                    }
                    if($average > 0){
                        $ave_str_hr = round($average, 2);
                        $ave_str = $this->get_formatted_time_with_seconds($average);
                    }
                }
                $data_appr_row->{"averageTimeApproval"} = $ave_str;
                $data_appr_row->{"averageTimeApprovalHour"} = $ave_str_hr;
            }
            $data['appr_stat'] = $data_appr;
            foreach($data_position as $data_position_row){
                $hours_total_arr = [];
                $hours_total_arr_count = 0;
                $average = 0;
                $ave_str = "0";
                $ave_str_hr = 0;
                $pos_id = $data_position_row->pos_id;
                $req_appr_data = array_merge(array_filter($data_details_req_appr, function ($e) use ($pos_id) {
                        return $e->position_ID == $pos_id;
                    }
                ));
                foreach($req_appr_data as $req_appr_data_row){
                    if(($req_appr_data_row->apprStat == 5) || ($req_appr_data_row->apprStat == 6)){
                        $requested_date = strtotime($req_appr_data_row->dateRequested);
                        $approval_date = strtotime($req_appr_data_row->dated_status);
                        $duration = $this->get_human_time_format($requested_date, $approval_date, 0);
                        $hours_total_arr[$hours_total_arr_count] = $duration['hoursTotal'];
                        $hours_total_arr_count +=1;
                    }
                }
                $hours_total_arr = array_filter($hours_total_arr);
                if(count($hours_total_arr) > 0) {
                    $average = array_sum($hours_total_arr)/count($hours_total_arr);
                }
                if($average > 0){
                    $ave_str_hr = round($average, 2);
                    $ave_str = $this->get_formatted_time_with_seconds($average);
                }
                $data_position_row->{"averageTimeApproval"} = $ave_str;
                $data_position_row->{"averageTimeApprovalHour"} = $ave_str_hr;
            }
            $data['position_stat'] = $data_position;
            foreach($data_details_req_appr as $data_details_req_appr_row){
                $average = 0;
                $duration_str = "0";
                $duration_hr = 0;
                $aft_approved = "-";
                $date_needed = "-";
                if(($data_details_req_appr_row->apprStat == 5) || ($data_details_req_appr_row->apprStat == 6)){
                    $requested_date = strtotime($data_details_req_appr_row->dateRequested);
                    $approval_date = strtotime($data_details_req_appr_row->dated_status);
                    $duration = $this->get_human_time_format($requested_date, $approval_date, 0);
                    if($duration['hoursTotal'] > 0){
                        $duration_str = $this->get_formatted_time_with_seconds($duration['hoursTotal']);
                        $duration_hr = round($duration['hoursTotal'], 2);
                    }
                    if((int)$data_details_req_appr_row->aftapproved){
                        $aft_approved = "Yes";
                    }else{
                        $aft_approved = "No";
                    }
                    if($data_details_req_appr_row->reqStat == 5){
                        $date_needed = Date("m/d/Y", strtotime($data_details_req_appr_row->dated_status .' '.$data_details_req_appr_row->days_needed .' days'));
                    }
                }
                $data_details_req_appr_row->{"averageTimeApproval"} = $duration_str;
                $data_details_req_appr_row->{"averageTimeApprovalHour"} = $duration_hr;
                $data_details_req_appr_row->{"ordinalLevel"} = $this->ordinal($data_details_req_appr_row->level);
                $data_details_req_appr_row->{"averageRequestApproval"} = $req_ave_approval[$data_details_req_appr_row->irequest_ID];
                $data_details_req_appr_row->{"approvedByAft"} = $aft_approved;
                $data_details_req_appr_row->{"dateNeeded"} = $date_needed;
            }
            $data['request_approval'] = $data_details_req_appr;
            $data['total_count'] = $total_count;
        }else{
            $data['error'] = 1;
            $data['error_details'] = $error_list;
        }
        return $data;
    }
    public function irequest_data(){
        $start_date = '2020-09-01';
        $end_date = '2020-12-20';
        $data = $this->get_irequest_data($start_date, $end_date);
        var_dump($data);
    }
    public function irequest_excel_report(){
        $start_date = $this->input->post('start');
        $end_date = $this->input->post('end');
        $data = $this->get_irequest_data($start_date, $end_date);
        // var_dump($data);
        if(!$data['error']){
            // $logo = 'C:\wamp64\www\sz\assets\images\img\logo2.png';
            $logo = $this->dir . '/assets/images/img/logo2.png';
            $this->load->library('PHPExcel', null, 'excel');
            for($sheet_loop = 0; $sheet_loop < 3;  $sheet_loop++){
                $this->excel->createSheet($sheet_loop);
            }
            // ----------------------------------- Approver iRequest report
            $this->excel->setActiveSheetIndex(3);
            $this->excel->getActiveSheet()->setTitle('Approver Stat');
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($logo);
            $objDrawing->setOffsetX(5); // setOffsetX works properly
            $objDrawing->setOffsetY(20); //setOffsetY has no effect
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(45);// logo height
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            $this->excel->getActiveSheet()->setShowGridlines(false);
            $header3 = [
                ['col' => '', 'id' => 'B2', 'title' => 'iRequest Approver Stat'],
                ['col' => '', 'id' => 'B3', 'title' => 'Date Requested: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
                ['col' => 'A', 'id' => 'A5', 'title' => 'LASTNAME'],
                ['col' => 'B', 'id' => 'B5', 'title' => 'FIRSTNAME'],
                ['col' => 'C', 'id' => 'C5', 'title' => 'MIDDLENAME'],
                ['col' => 'D', 'id' => 'D5', 'title' => 'REQUEST'],
                ['col' => 'E', 'id' => 'E5', 'title' => 'PENDING'],
                ['col' => 'F', 'id' => 'F5', 'title' => 'APPROVED'],
                ['col' => 'G', 'id' => 'G5', 'title' => 'DISAPPROVED'],
                ['col' => 'H', 'id' => 'H5', 'title' => 'MISSED'],
                ['col' => 'I', 'id' => 'I5', 'title' => 'Ave. Time Approval (Formatted)'],
                ['col' => 'J', 'id' => 'J5', 'title' => 'Ave. Time Approval (Hours)'],
            ];
            for($excel_data_header_loop = 0; $excel_data_header_loop < count($header3); $excel_data_header_loop++){
                // echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
                $this->excel->getActiveSheet()->setCellValue($header3[$excel_data_header_loop]['id'], $header3[$excel_data_header_loop]['title']);
                $this->excel->getActiveSheet()->getStyle($header3[$excel_data_header_loop]['id'])->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle($header3[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
                $this->excel->getActiveSheet()->getStyle($header3[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $this->excel->getActiveSheet()->getColumnDimension($header[$excel_data_header_loop]['col'])->setWidth(20);
            }
            $this->excel->getActiveSheet()->getStyle('B2:B2')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getFont()->getColor()->setRGB('000000');
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->getStyle('A5:j5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
            $rowNum2 = 6;		
            $last_row = (count($data['appr_stat']) + $rowNum2)-1;
            // var_dump($last_row);
            if(count($data['appr_stat']) > 0){
                foreach ($data['appr_stat'] as $appr_stat_rows) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $rowNum2, $appr_stat_rows->lname);
                    $this->excel->getActiveSheet()->setCellValue('B' . $rowNum2, $appr_stat_rows->fname);
                    $this->excel->getActiveSheet()->setCellValue('C' . $rowNum2, $appr_stat_rows->mname);
                    $this->excel->getActiveSheet()->setCellValue('D' . $rowNum2, $appr_stat_rows->apprCount);
                    $this->excel->getActiveSheet()->setCellValue('E' . $rowNum2, $appr_stat_rows->pending);
                    $this->excel->getActiveSheet()->setCellValue('F' . $rowNum2, $appr_stat_rows->approved);
                    $this->excel->getActiveSheet()->setCellValue('G' . $rowNum2, $appr_stat_rows->disapproved);
                    $this->excel->getActiveSheet()->setCellValue('H' . $rowNum2, $appr_stat_rows->missed);
                    $this->excel->getActiveSheet()->setCellValue('I' . $rowNum2, $appr_stat_rows->averageTimeApproval);
                    $this->excel->getActiveSheet()->setCellValue('J' . $rowNum2, $appr_stat_rows->averageTimeApprovalHour);
                    $rowNum2++;
                }
            }
            $this->excel->getActiveSheet()->getStyle('D6:D' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('E6:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('F6:F' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('G6:G' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('H6:H' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('I6:I' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('J6:J' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A6:A' . $last_row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A5:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => 'DDDDDD'),
                        ),
                    ),
                )
            );
             // ----------------------------------- Position iRequest report
            $this->excel->setActiveSheetIndex(2);
            $this->excel->getActiveSheet()->setTitle('Position Stat');
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($logo);
            $objDrawing->setOffsetX(5); // setOffsetX works properly
            $objDrawing->setOffsetY(20); //setOffsetY has no effect
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(45);// logo height
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            $this->excel->getActiveSheet()->setShowGridlines(false);
            $header3 = [
                ['col' => '', 'id' => 'B2', 'title' => 'iRequest Position Stat'],
                ['col' => '', 'id' => 'B3', 'title' => 'Date Requested: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
                ['col' => 'A', 'id' => 'A5', 'title' => 'Position'],
                ['col' => 'B', 'id' => 'B5', 'title' => 'REQUEST'],
                ['col' => 'C', 'id' => 'C5', 'title' => 'PENDING'],
                ['col' => 'D', 'id' => 'D5', 'title' => 'APPROVED'],
                ['col' => 'E', 'id' => 'E5', 'title' => 'DISAPPROVED'],
                ['col' => 'F', 'id' => 'F5', 'title' => 'MISSED'],
                ['col' => 'G', 'id' => 'G5', 'title' => 'Ave. Time Approval (Formatted)'],
                ['col' => 'H', 'id' => 'H5', 'title' => 'Ave. Time Approval (Hours)'],
            ];
            for($excel_data_header_loop = 0; $excel_data_header_loop < count($header3); $excel_data_header_loop++){
                // echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
                $this->excel->getActiveSheet()->setCellValue($header3[$excel_data_header_loop]['id'], $header3[$excel_data_header_loop]['title']);
                $this->excel->getActiveSheet()->getStyle($header3[$excel_data_header_loop]['id'])->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle($header3[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
                $this->excel->getActiveSheet()->getStyle($header3[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $this->excel->getActiveSheet()->getColumnDimension($header[$excel_data_header_loop]['col'])->setWidth(20);
            }
            $this->excel->getActiveSheet()->getStyle('B2:B2')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getFont()->getColor()->setRGB('000000');
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->getStyle('A5:H5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
            $rowNum2 = 6;		
            $last_row = (count($data['position_stat']) + $rowNum2)-1;
            // var_dump($last_row);
            if(count($data['position_stat']) > 0){
                foreach ($data['position_stat'] as $position_stat_rows) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $rowNum2, $position_stat_rows->pos_details);
                    $this->excel->getActiveSheet()->setCellValue('B' . $rowNum2, $position_stat_rows->requestCount);
                    $this->excel->getActiveSheet()->setCellValue('C' . $rowNum2, $position_stat_rows->pending);
                    $this->excel->getActiveSheet()->setCellValue('D' . $rowNum2, $position_stat_rows->approved);
                    $this->excel->getActiveSheet()->setCellValue('E' . $rowNum2, $position_stat_rows->disapproved);
                    $this->excel->getActiveSheet()->setCellValue('F' . $rowNum2, $position_stat_rows->missed);
                    $this->excel->getActiveSheet()->setCellValue('G' . $rowNum2, $position_stat_rows->averageTimeApproval);
                    $this->excel->getActiveSheet()->setCellValue('H' . $rowNum2, $position_stat_rows->averageTimeApprovalHour);
                    $rowNum2++;
                }
            }
            $this->excel->getActiveSheet()->getStyle('B6:B' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C6:C' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('D6:D' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('E6:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('F6:F' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('G6:G' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('H6:H' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A6:A' . $last_row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A5:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => 'DDDDDD'),
                        ),
                    ),
                )
            );
            // ----------------------------------- Team iRequest report
            $this->excel->setActiveSheetIndex(1);
            $this->excel->getActiveSheet()->setTitle('Team Stat');
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($logo);
            $objDrawing->setOffsetX(5); // setOffsetX works properly
            $objDrawing->setOffsetY(20); //setOffsetY has no effect
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(45);// logo height
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            $this->excel->getActiveSheet()->setShowGridlines(false);
            $header2 = [
                ['col' => '', 'id' => 'B2', 'title' => 'iRequest Team Stat'],
                ['col' => '', 'id' => 'B3', 'title' => 'Date Requested: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
                ['col' => 'A', 'id' => 'A5', 'title' => 'TEAM'],
                ['col' => 'B', 'id' => 'B5', 'title' => 'REQUEST'],
                ['col' => 'C', 'id' => 'C5', 'title' => 'PENDING'],
                ['col' => 'D', 'id' => 'D5', 'title' => 'APPROVED'],
                ['col' => 'E', 'id' => 'E5', 'title' => 'DISAPPROVED'],
                ['col' => 'F', 'id' => 'F5', 'title' => 'MISSED'],
                ['col' => 'G', 'id' => 'G5', 'title' => 'Ave. Time Approval (Formatted)'],
                ['col' => 'H', 'id' => 'H5', 'title' => 'Ave. Time Approval (Hours)'],
            ];
            for($excel_data_header_loop = 0; $excel_data_header_loop < count($header2); $excel_data_header_loop++){
                // echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
                $this->excel->getActiveSheet()->setCellValue($header2[$excel_data_header_loop]['id'], $header2[$excel_data_header_loop]['title']);
                $this->excel->getActiveSheet()->getStyle($header2[$excel_data_header_loop]['id'])->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle($header2[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
                $this->excel->getActiveSheet()->getStyle($header2[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $this->excel->getActiveSheet()->getColumnDimension($header[$excel_data_header_loop]['col'])->setWidth(20);
            }
            $this->excel->getActiveSheet()->getStyle('B2:B2')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getFont()->getColor()->setRGB('000000');
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->getStyle('A5:H5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
            $rowNum2 = 6;		
            $last_row = (count($data['account_stat']) + $rowNum2)-1;
            // var_dump($last_row);
            if(count($data['account_stat']) > 0){
                foreach ($data['account_stat'] as $account_stat_rows) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $rowNum2, $account_stat_rows->acc_name);
                    $this->excel->getActiveSheet()->setCellValue('B' . $rowNum2, $account_stat_rows->requestCount);
                    $this->excel->getActiveSheet()->setCellValue('C' . $rowNum2, $account_stat_rows->pending);
                    $this->excel->getActiveSheet()->setCellValue('D' . $rowNum2, $account_stat_rows->approved);
                    $this->excel->getActiveSheet()->setCellValue('E' . $rowNum2, $account_stat_rows->disapproved);
                    $this->excel->getActiveSheet()->setCellValue('F' . $rowNum2, $account_stat_rows->missed);
                    $this->excel->getActiveSheet()->setCellValue('G' . $rowNum2, $account_stat_rows->averageTimeApproval);
                    $this->excel->getActiveSheet()->setCellValue('H' . $rowNum2, $account_stat_rows->averageTimeApprovalHour);
                    $rowNum2++;
                }
            }
            $this->excel->getActiveSheet()->getStyle('B6:B' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C6:C' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('D6:D' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('E6:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('F6:F' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('G6:G' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('H6:H' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A6:A' . $last_row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A5:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => 'DDDDDD'),
                        ),
                    ),
                )
            );

            // ----------------------------------- DETAILED iRequest report
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('iRequest Detailed Report');
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($logo);
            $objDrawing->setOffsetX(5); // setOffsetX works properly
            $objDrawing->setOffsetY(20); //setOffsetY has no effect
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(45);// logo height
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            $this->excel->getActiveSheet()->setShowGridlines(false);
            $header = [
                ['col' => '', 'id' => 'C2', 'title' => 'iRequest Records'],
                ['col' => '', 'id' => 'C3', 'title' => 'Date Requested: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
                ['col' => '', 'id' => 'A5', 'title' => 'Pending: '.$data['total_count'][0]->pending],
                ['col' => '', 'id' => 'A6', 'title' => 'Approved: '.$data['total_count'][0]->approved],
                ['col' => '', 'id' => 'A7', 'title' => 'Disapproved: '.$data['total_count'][0]->disapproved],
                ['col' => '', 'id' => 'C5', 'title' => 'Missed: '.$data['total_count'][0]->missed],
                ['col' => '', 'id' => 'C6', 'title' => 'Requests: '.$data['total_count'][0]->requestCount],
                ['col' => 'A', 'id' => 'A10', 'title' => 'IREQUEST-ID'],
                ['col' => 'B', 'id' => 'B10', 'title' => 'DATE FILED'],
                ['col' => 'C', 'id' => 'C10', 'title' => 'STATUS'],
                ['col' => 'D', 'id' => 'D10', 'title' => 'AVE. REQUEST APPROVAL'],
                ['col' => 'E', 'id' => 'E10', 'title' => 'POSITION'],
                ['col' => 'F', 'id' => 'F10', 'title' => 'SITE'],
                ['col' => 'G', 'id' => 'G10', 'title' => 'MANPOWER NEEDED'],
                ['col' => 'H', 'id' => 'H10', 'title' => 'FILED BY'],
                ['col' => 'I', 'id' => 'I10', 'title' => 'REQUESTING TEAM'],
                ['col' => 'J', 'id' => 'J10', 'title' => 'DATE NEEDED'],
                ['col' => 'K', 'id' => 'K10', 'title' => 'Justification'],
                ['col' => 'L', 'id' => 'L10', 'title' => 'Hiring Type'],
                ['col' => 'M', 'id' => 'M10', 'title' => 'Education Attainment'],
                ['col' => 'N', 'id' => 'N10', 'title' => 'GENDER PREFERENCE'],
                ['col' => 'O', 'id' => 'O10', 'title' => 'QUALIFICATIONS'],
                ['col' => 'P', 'id' => 'P10', 'title' => 'DESCRIPTION'],
                ['col' => 'Q', 'id' => 'Q10', 'title' => 'AFT APPROVED'],
                ['col' => 'R', 'id' => 'R10', 'title' => 'LAST NAME'],
                ['col' => 'S', 'id' => 'S10', 'title' => 'FIRST NAME'],
                ['col' => 'T', 'id' => 'T10', 'title' => 'MIDDLE NAME'],
                ['col' => 'U', 'id' => 'U10', 'title' => 'LEVEL'],
                ['col' => 'V', 'id' => 'V10', 'title' => 'DEADLINE'],
                ['col' => 'W', 'id' => 'W10', 'title' => 'APPROVAL STATUS'],
                ['col' => 'X', 'id' => 'X10', 'title' => 'REMARKS'],
                ['col' => 'Y', 'id' => 'Y10', 'title' => 'Ave. Time Approval (Formatted)'],
                ['col' => 'Z', 'id' => 'Z10', 'title' => 'Ave. Time Approval (Hours)'],
            ];
            for($excel_data_header_loop = 0; $excel_data_header_loop < count($header); $excel_data_header_loop++){
                // echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
                $this->excel->getActiveSheet()->setCellValue($header[$excel_data_header_loop]['id'], $header[$excel_data_header_loop]['title']);
                $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
                $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $this->excel->getActiveSheet()->getColumnDimension($header[$excel_data_header_loop]['col'])->setWidth(20);
            }
            $this->excel->getActiveSheet()->getStyle('C2:C2')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('C2:C6')->getFont()->getColor()->setRGB('000000');
            $this->excel->getActiveSheet()->getStyle('A4:A8')->getFont()->getColor()->setRGB('000000');
            $this->excel->getActiveSheet()->getStyle('A4:A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->getStyle('C2:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->getStyle('A10:Z10')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(17);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(28);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(17);
            $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(17);
            $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(17);
            $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(22);
            $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(28);
            $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(30);
            // $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(30);
            $this->excel->getActiveSheet()->freezePane('E11');
            $rowNum = 11;		
            $last_row = (count($data['request_approval']) + $rowNum)-1;
            // var_dump($last_row);
            if(count($data['request_approval']) > 0){
                foreach ($data['request_approval'] as $detailed_irequest_rows) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $rowNum, str_pad($detailed_irequest_rows->irequest_ID, 8, '0', STR_PAD_LEFT));
                    $this->excel->getActiveSheet()->setCellValue('B' . $rowNum, date_format(date_create($detailed_irequest_rows->dateRequested), "m/d/Y"));
                    $this->excel->getActiveSheet()->setCellValue('C' . $rowNum, ucfirst($detailed_irequest_rows->statReqDescription));
                    $this->excel->getActiveSheet()->setCellValue('D' . $rowNum, $detailed_irequest_rows->averageRequestApproval);
                    $this->excel->getActiveSheet()->setCellValue('E' . $rowNum, $detailed_irequest_rows->pos_details);
                    $this->excel->getActiveSheet()->setCellValue('F' . $rowNum, $detailed_irequest_rows->code);
                    $this->excel->getActiveSheet()->setCellValue('G' . $rowNum, $detailed_irequest_rows->manpower_requested);
                    $this->excel->getActiveSheet()->setCellValue('H' . $rowNum, ucfirst($detailed_irequest_rows->requestorLname).", ".ucfirst($detailed_irequest_rows->requestorFname)." ".ucfirst($detailed_irequest_rows->requestorMname));
                    $this->excel->getActiveSheet()->setCellValue('I' . $rowNum, ucwords($detailed_irequest_rows->acc_name));
                    $this->excel->getActiveSheet()->setCellValue('J' . $rowNum, $detailed_irequest_rows->dateNeeded);
                    $this->excel->getActiveSheet()->setCellValue('K' . $rowNum, $detailed_irequest_rows->purpose);
                    $this->excel->getActiveSheet()->setCellValue('L' . $rowNum, $detailed_irequest_rows->hiringtype_desc);
                    $this->excel->getActiveSheet()->setCellValue('M' . $rowNum, $detailed_irequest_rows->educationalAttainment);
                    $this->excel->getActiveSheet()->setCellValue('N' . $rowNum, $detailed_irequest_rows->gender);
                    $this->excel->getActiveSheet()->setCellValue('O' . $rowNum, $detailed_irequest_rows->qualifications);
                    $this->excel->getActiveSheet()->setCellValue('P' . $rowNum, $detailed_irequest_rows->reqDescription);
                    $this->excel->getActiveSheet()->setCellValue('Q' . $rowNum, $detailed_irequest_rows->approvedByAft);
                    $this->excel->getActiveSheet()->setCellValue('R' . $rowNum, $detailed_irequest_rows->lname);
                    $this->excel->getActiveSheet()->setCellValue('S' . $rowNum, $detailed_irequest_rows->fname);
                    $this->excel->getActiveSheet()->setCellValue('T' . $rowNum, $detailed_irequest_rows->mname);
                    $this->excel->getActiveSheet()->setCellValue('U' . $rowNum, $detailed_irequest_rows->ordinalLevel);
                    $this->excel->getActiveSheet()->setCellValue('V' . $rowNum, date_format(date_create($detailed_irequest_rows->deadline), "m/d/Y H:i:s A"));
                    $this->excel->getActiveSheet()->setCellValue('W' . $rowNum, ucfirst($detailed_irequest_rows->apprStatDesc));
                    $this->excel->getActiveSheet()->setCellValue('X' . $rowNum, $detailed_irequest_rows->remarks);
                    $this->excel->getActiveSheet()->setCellValue('Y' . $rowNum, $detailed_irequest_rows->averageTimeApproval);
                    $this->excel->getActiveSheet()->setCellValue('Z' . $rowNum, $detailed_irequest_rows->averageTimeApprovalHour);
                    $rowNum++;
                }
            }
            $this->excel->getActiveSheet()->getStyle('A11:A' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('B11:B' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C11:C' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('D11:D' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('F11:F' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('G11:G' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('J11:J' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('K11:K' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('L11:L' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('M11:M' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('N11:N' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('Q11:Q' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('U11:U' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('V11:V' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('W11:W' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('Y11:Y' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('Z11:Z' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A11:A' . $last_row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E11:E' . $last_row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('G11:G' . $last_row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A11:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => 'DDDDDD'),
                        ),
                    ),
                )
            );
            $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
            $this->excel->getActiveSheet()->getProtection()->setSheet(true);
            $filename = 'iRequest Report '.$start_date.' to '.$end_date.'.xlsx'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            // header('Set-Cookie: fileDownload=true; path=/');
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
            ob_end_clean();
            $objWriter->save('php://output');
        }
    }
    private function qry_detailed_irecruit_records($start_date, $end_date){
        $fields = "req.irequest_ID, req.requesting_dept, req.position_ID, pos.pos_details , req.manpower_requested, req.manpower_supplied, pur.purpose, req.hiringPurpose_desc, req.hiringtype_desc, req.educationalAttainment, req.gender, req.aftapproved, req.description reqDescription, req.qualifications, req.gender, req.dateRequested, req.requested_by, req.status reqStatus, req.days_needed, acc.acc_name, req.datedStatus, rec.irequestRecruitment_ID, rec.numberOfRecruit, rec.status_ID recStatus, rec_status.description recruitmentStat, req_status.status_ID request_stat_id, req_status.description, rec.dateTimeCompleted, rec_app.application_ID, rec_app.apid, app.fname, app.mname, app.lname, app.nameExt, app.pic, app.gender, rec_app.status_ID rec_applicant_stat_id , app_status.description applicantStat, rec_app.site, rec_app.type, rec_app.date_created, rec_app.dated_status, site.code";
        $where = "site.site_ID = req.site_ID AND acc.acc_id = req.requesting_dept AND rec_status.status_ID = rec.status_ID AND req_status.status_ID = req.status AND app_status.status_ID = rec_app.status_ID AND pos.pos_id = req.position_ID AND req.irequest_ID = rec.irequest_ID AND app.apid = rec_app.apid AND rec_app.application_ID = app_emp.application_ID AND app_emp.irequestRecruitment_ID = rec.irequestRecruitment_ID AND pur.hiringPurpose_ID = req.hiringPurpose_ID AND DATE(rec_app.date_created) >= '$start_date' AND DATE(rec_app.date_created) <= '$end_date'";
        $table = "tbl_irecruit_application rec_app, tbl_irecruit_applicant app_emp, tbl_irequest_recruitment rec, tbl_applicant app, tbl_irequest_manpower_request req, tbl_position pos, tbl_status req_status, tbl_status app_status, tbl_status rec_status, tbl_account acc, tbl_irequest_hiring_purpose pur, tbl_site site";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "req.dateRequested ASC");
    }
    private function qry_all_detailed_recruitment_records(){
        $fields = "req.irequest_ID, req.requesting_dept, req.position_ID, pos.pos_details , req.manpower_requested, req.manpower_supplied, pur.purpose, req.hiringPurpose_desc, req.educationalAttainment, req.gender, req.aftapproved, req.description reqDescription, req.qualifications, req.gender, req.dateRequested, req.requested_by, req.status reqStatus, acc.acc_name, req.datedStatus, rec.irequestRecruitment_ID, rec.numberOfRecruit, rec.status_ID recStatus, rec_status.description recruitmentStat, req_status.status_ID request_stat_id, req_status.description, rec.dateTimeCompleted, rec_app.application_ID, rec_app.apid, app.fname, app.mname, app.lname, app.nameExt, app.pic, app.gender, rec_app.status_ID rec_applicant_stat_id , app_status.description applicantStat, rec_app.site, rec_app.type, rec_app.date_created, rec_app.dated_status, site.code";
        $where = "site.site_ID = req.site_ID AND acc.acc_id = req.requesting_dept AND rec_status.status_ID = rec.status_ID AND req_status.status_ID = req.status AND app_status.status_ID = rec_app.status_ID AND pos.pos_id = req.position_ID AND req.irequest_ID = rec.irequest_ID AND app.apid = rec_app.apid AND rec_app.application_ID = app_emp.application_ID AND app_emp.irequestRecruitment_ID = rec.irequestRecruitment_ID AND pur.hiringPurpose_ID = req.hiringPurpose_ID";
        $table = "tbl_irecruit_application rec_app, tbl_irecruit_applicant app_emp, tbl_irequest_recruitment rec, tbl_applicant app, tbl_irequest_manpower_request req, tbl_position pos, tbl_status req_status, tbl_status app_status, tbl_status rec_status, tbl_account acc, tbl_irequest_hiring_purpose pur, tbl_site site";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "req.dateRequested ASC");
    }
    private function qry_irecruit_total_records(){
        $fields = "req.manpower_requested, req.irequest_ID, req.position_ID, pos.pos_details, req.requesting_dept, acc.acc_name, req.datedStatus, rec.status_ID";
        $where = "acc.acc_id = req.requesting_dept AND pos.pos_id = req.position_ID AND req.irequest_ID = rec.irequest_ID";
        $table = "tbl_account acc, tbl_irequest_manpower_request req, tbl_irequest_recruitment rec, tbl_position pos";
        return $this->general_model->fetch_specific_vals($fields, $where, $table, "req.dateRequested ASC");
    }
    private function qry_recruitment_position_data($request_ids){
        $query = "SELECT req.position_ID, req.datedStatus, pos.pos_details, SUM(req.manpower_requested) requested FROM tbl_irequest_recruitment rec, tbl_irequest_manpower_request req, tbl_position pos WHERE pos.pos_id = req.position_ID AND req.irequest_ID = rec.irequest_ID AND rec.irequest_ID IN ($request_ids) GROUP BY req.position_ID";
        return $this->general_model->custom_query($query);
    }
    private function qry_recruitment_team_data($request_ids){
        $query = "SELECT req.requesting_dept, acc.acc_name, req.datedStatus, SUM(req.manpower_requested) requested FROM tbl_irequest_recruitment rec, tbl_irequest_manpower_request req, tbl_account acc WHERE acc.acc_id = req.requesting_dept AND req.irequest_ID = rec.irequest_ID AND rec.irequest_ID IN ($request_ids) GROUP BY req.requesting_dept";
        return $this->general_model->custom_query($query);
    }
    public function get_recruitment_data($start_date, $end_date){
        $data['error'] = 0;
        $data['error_details'] = "";
        $distinct_request_id = []; 
        $error_list = [];
        $error_count = 0;
        $req_days_to_fill_ave = [];
        $detailed_applicants = $this->qry_detailed_irecruit_records($start_date, $end_date);
        $total_records = $this->qry_irecruit_total_records();
        $all_detaied_record = $this->qry_all_detailed_recruitment_records();
        $total_count['recruitment_count'] = 0;
        $total_count['vacant'] = 0;
        $total_count['hired'] = 0;
        $total_count['cancelled'] = 0;
        $total_count['exceed_30'] = 0;
        // $total_count['exceed_30_applicants'] = [];
        $exceed_30_applicants = [];
        $exceed_30_vacants = [];
        $cancelled_requests = [];
        $vacant_positions = [];
        $vacant_position_details = [];
        $exceed_30_applicants_count = 0;
        $exceed_30_vacants_count = 0;
        $exceeded_30_total = 0;
        $vacant_positions = 0;
        $vacant_position_details_count = 0;
        if(count($detailed_applicants) < 1){
            $error_list[$error_count] = "Recruitment data not found";
            $error_count++;
        }
        if(count($error_list) < 1){
            $distinct_request_id = array_column($total_records, 'irequest_ID');
            $distinct_request_id = array_merge(array_unique($distinct_request_id));
            $app_arr = array_merge(array_unique(array_column($detailed_applicants, 'apid')));
            foreach($total_records as $total_records_row){
                $days_fill_ave_rec = [];
                $request_id = $total_records_row->irequest_ID;
                $rec_applicant_details = array_merge(array_filter($detailed_applicants, function ($e) use ($request_id) {
                        return $e->irequest_ID == $request_id;
                    }
                ));
                if(count($rec_applicant_details) > 0){
                    foreach($rec_applicant_details as $rec_applicant_details_index=>$rec_applicant_details_row){
                        $date_approved = strtotime($rec_applicant_details_row->datedStatus);
                        $date_hired = strtotime($rec_applicant_details_row->dated_status);
                        $duration = $this->get_human_time_format($date_approved, $date_hired, 0);
                        if($duration['hoursTotal'] > 0){
                            $days_fill_ave_rec[$rec_applicant_details_index] = $duration['hoursTotal']/24;
                        }else{
                            $days_fill_ave_rec[$rec_applicant_details_index] = 0;
                        }
                        if($duration['hoursTotal']/24 > 30){
                            $total_count['exceed_30'] +=1;
                            $exceed_30_applicants[$exceed_30_applicants_count] = $rec_applicant_details_row;
                            $exceed_30_applicants_count++;
                        }
                    }
                }
                if($total_records_row->manpower_requested != count($rec_applicant_details)){
                    $dateTime = $this->get_current_date_time();
                    $from = strtotime($total_records_row->datedStatus);
                    $to = strtotime($dateTime['dateTime']);
                    $duration = $this->get_human_time_format($from, $to, 0);
                    $duration['hoursTotal'];
                    if($duration['hoursTotal']/24 > 30){
                        $total_count['exceed_30'] += $total_records_row->manpower_requested - count($rec_applicant_details);
                        $exceed_30_vacants[$request_id] = [
                            'details' => $total_records_row,
                            'exceed_count' => $total_records_row->manpower_requested - count($rec_applicant_details)
                        ];
                    }
                }
                if(count($days_fill_ave_rec) > 0){
                    $req_days_to_fill_ave[$request_id] = round(array_sum($days_fill_ave_rec)/count($days_fill_ave_rec), 2);
                }else{
                    $req_days_to_fill_ave[$request_id] = 0;
                }
                if($total_records_row->status_ID == 7){
                    $total_count['cancelled'] += $total_records_row->manpower_requested - count($rec_applicant_details);
                    $cancelled_requests[$request_id] = [
                        'details' => $total_records_row,
                        'cancelled_count' => $total_records_row->manpower_requested - count($rec_applicant_details)
                    ];
                }              
            }
            $requested_manpower_arr = array_column($total_records,'manpower_requested');
            if(count($requested_manpower_arr) > 0){
                $total_count['recruitment_count'] = array_sum($requested_manpower_arr);
            }
            $total_count['hired'] = count($detailed_applicants);
            $total_count['vacant'] = array_sum($requested_manpower_arr) - count($all_detaied_record);
            $rec_pos_data = $this->qry_recruitment_position_data(implode(',', $distinct_request_id));
            $rec_account_data = $this->qry_recruitment_team_data(implode(',', $distinct_request_id));
            foreach($rec_pos_data as $rec_pos_data_row){
                $hired = 0;
                $position_id = $rec_pos_data_row->position_ID;
                $days_to_fill = [];
                $exceeded_30 = 0;
                $cancelled = 0;
                $vacant = 0;
                $req_position_details = array_merge(array_filter($total_records, function ($e) use ($position_id) {
                        return $e->position_ID == $position_id;
                    }
                ));
                if(count($req_position_details) > 0){
                    foreach($req_position_details as $req_position_details_row){
                        $requested_manpower = $req_position_details_row->manpower_requested;
                        $request_id = $req_position_details_row->irequest_ID;
                        $position_applicants = array_merge(array_filter($detailed_applicants, function ($e) use ($request_id) {
                                return $e->irequest_ID == $request_id;
                            }
                        ));
                        if(count($position_applicants) > 0){
                            foreach($position_applicants as $position_applicants_index=>$position_applicants_row){
                                $from = strtotime($position_applicants_row->datedStatus);
                                $to = strtotime($position_applicants_row->dated_status);
                                $duration = $this->get_human_time_format($from, $to, 0);
                                $days_duration = $duration['hoursTotal']/24;
                                if($duration['hoursTotal']/24 > 30){
                                    $exceeded_30 +=1;
                                }
                            }
                        }
                        if($requested_manpower != count($position_applicants)){
                            $dateTime = $this->get_current_date_time();
                            $from = strtotime($req_position_details_row->datedStatus);
                            $to = strtotime($dateTime['dateTime']);
                            $duration = $this->get_human_time_format($from, $to, 0);
                            $duration['hoursTotal'];
                            if($duration['hoursTotal']/24 > 30){
                                $exceeded_30 += $requested_manpower - count($position_applicants);
                            }
                        }
                        if($req_position_details_row->status_ID == 7){
                            $cancelled += $requested_manpower - count($position_applicants);
                        }
                        $req_all_applicant_details = array_merge(array_filter($all_detaied_record, function ($e) use ($request_id) {
                                return $e->irequest_ID == $request_id;
                            }
                        ));
                        $vacant +=  $requested_manpower - count($req_all_applicant_details);
                    }
                }
                if($vacant > 0){
                    $vacant_position_details[$vacant_position_details_count] = [
                        "position" => $rec_pos_data_row,
                        "count" => $vacant
                    ];
                    $vacant_position_details_count++;
                }
                $position_applicants = array_merge(array_filter($detailed_applicants, function ($e) use ($position_id) {
                        return $e->position_ID == $position_id;
                    }
                ));
                if(count($position_applicants) > 0){
                    foreach($position_applicants as $position_applicants_index=>$position_applicants_row){
                        $from = strtotime($position_applicants_row->datedStatus);
                        $to = strtotime($position_applicants_row->dated_status);
                        $duration = $this->get_human_time_format($from, $to, 0);
                        if($duration['hoursTotal'] > 0){
                            $days_to_fill[$position_applicants_index] = $duration['hoursTotal']/24;
                        }else{
                            $days_to_fill[$position_applicants_index] = 0;
                        }
                    }
                }
                if(count($days_to_fill) > 0){
                    $days_to_fill_ave = array_sum($days_to_fill)/count($days_to_fill);
                }else{
                    $days_to_fill_ave = 0;
                }
                $hired = count($position_applicants);
                $cancelled = 0;
                $rec_pos_data_row->{"hired"} = $hired;
                $rec_pos_data_row->{"vacant"} = $vacant;
                $rec_pos_data_row->{"cancelled"} = $cancelled;
                $rec_pos_data_row->{"exceed_30"} = $exceeded_30;
                $rec_pos_data_row->{"days_fill_ave"} = round($days_to_fill_ave, 2);
            }
            $data['recruitment_position'] = $rec_pos_data;
            foreach($rec_account_data as $rec_account_data_index=>$rec_account_data_row){
                $hired = 0;
                $acc_id = $rec_account_data_row->requesting_dept;
                $days_to_fill = [];
                $exceeded_30 = 0;
                $cancelled = 0;
                $vacant = 0;
                $req_account_details = array_merge(array_filter($total_records, function ($e) use ($acc_id) {
                        return $e->requesting_dept == $acc_id;
                    }
                ));
                if(count($req_account_details) > 0){
                    foreach($req_account_details as $req_account_details_row){
                        $requested_manpower = $req_account_details_row->manpower_requested;
                        $request_id = $req_account_details_row->irequest_ID;
                        $account_applicants = array_merge(array_filter($detailed_applicants, function ($e) use ($request_id) {
                                return $e->irequest_ID == $request_id;
                            }
                        ));
                        if(count($account_applicants) > 0){
                            foreach($account_applicants as $account_applicants_index=>$account_applicants_row){
                                $from = strtotime($account_applicants_row->datedStatus);
                                $to = strtotime($account_applicants_row->dated_status);
                                $duration = $this->get_human_time_format($from, $to, 0);
                                $days_duration = $duration['hoursTotal']/24;
                                if($duration['hoursTotal']/24 > 30){
                                    $exceeded_30 +=1;
                                }
                            }
                        }
                        if($requested_manpower != count($account_applicants)){
                            $dateTime = $this->get_current_date_time();
                            $from = strtotime($req_account_details_row->datedStatus);
                            $to = strtotime($dateTime['dateTime']);
                            $duration = $this->get_human_time_format($from, $to, 0);
                            $duration_days = $duration['hoursTotal']/24;
                            if($duration['hoursTotal']/24 > 30){
                                $exceeded_30 += $requested_manpower - count($account_applicants);
                            }
                        }
                        if($req_account_details_row->status_ID == 7){
                            $cancelled += $requested_manpower - count($account_applicants);
                        }
                        $req_all_applicant_details = array_merge(array_filter($all_detaied_record, function ($e) use ($request_id) {
                            return $e->irequest_ID == $request_id;
                            }
                        ));
                        $vacant +=  $requested_manpower - count($req_all_applicant_details);
                    }
                }
                $account_applicants = array_merge(array_filter($detailed_applicants, function ($e) use ($acc_id) {
                        return $e->requesting_dept == $acc_id;
                    }
                ));
                if(count($account_applicants) > 0){
                    foreach( $account_applicants as $account_applicants_index=>$account_applicants_row){
                        $from = strtotime($account_applicants_row->datedStatus);
                        $to = strtotime($account_applicants_row->dated_status);
                        $duration = $this->get_human_time_format($from, $to, 0);
                        if($duration['hoursTotal'] > 0){
                            $days_to_fill[$account_applicants_index] = $duration['hoursTotal']/24;
                        }else{
                            $days_to_fill[$account_applicants_index] = 0;
                        }
                    }
                }
                if(count($days_to_fill) > 0){
                    $days_to_fill_ave = array_sum($days_to_fill)/count($days_to_fill);
                }else{
                    $days_to_fill_ave = 0;
                }
                $hired = count($account_applicants);
                $rec_account_data_row->{"hired"} = $hired;
                $rec_account_data_row->{"vacant"} = $vacant;
                $rec_account_data_row->{"cancelled"} = $cancelled;
                $rec_account_data_row->{"exceed_30"} = $exceeded_30;
                $rec_account_data_row->{"days_fill_ave"} = round($days_to_fill_ave, 2);
            }
            $data['recruitment_account'] = $rec_account_data;
            foreach($detailed_applicants as $detailed_applicants_row){
                $days_hired = 0;
                $aft_approved = "-";
                $date_needed = "-";
                $from = strtotime($detailed_applicants_row->datedStatus);
                $to = strtotime($detailed_applicants_row->dated_status);
                $duration = $this->get_human_time_format($from, $to, 0);
                if($duration['hoursTotal'] > 0){
                    $days_hired = round($duration['hoursTotal']/24, 2);
                }
                if((int)$detailed_applicants_row->aftapproved){
                    $aft_approved = "Yes";
                }else{
                    $aft_approved = "No";
                }
                $date_needed = Date("Y-m-d", strtotime($detailed_applicants_row->datedStatus .' '.$detailed_applicants_row->days_needed .' days'));
                $detailed_applicants_row->{"days_fill_ave"} = $days_hired;
                $detailed_applicants_row->{"request_days_fill_ave"} = $req_days_to_fill_ave[$detailed_applicants_row->irequest_ID];
                $detailed_applicants_row->{"approvedByAft"} = $aft_approved;
                $detailed_applicants_row->{"dateNeeded"} = $date_needed;
            }
            $data['recruitment_detailed_applicants'] = $detailed_applicants;
            $data['exceed_30_applicant'] = $exceed_30_applicants;
            $data['exceed_30_vacant'] = $exceed_30_vacants;
            $data['cancelled_details'] = $cancelled_requests;
            $data['vacant_details'] = $vacant_position_details;
            $data['total_count'] = $total_count;
        }else{
            // var_dump($total_records);
            $vacant_manpower = [];
            if(count($total_records) > 0){
                $positions = array_column($total_records, 'position_ID');
                $vacant_total_count = 0;
                // var_dump($positions);
                for($loop = 0; $loop < count($positions); $loop++){
                    $current_position = $positions[$loop];
                    $position_count = array_merge(array_filter($total_records, function ($e) use ($current_position) {
                            return $e->position_ID == $current_position;
                        }
                    ));
                    $position_manpower = array_column($position_count, 'manpower_requested');
                    $position_count[0]->{"vacant"} =  $position_count[0]->manpower_requested;
                    $position_count[0]->{"requested"} =  $position_count[0]->manpower_requested;
                    $position_count[0]->{"hired"} =  0;
                    $position_count[0]->{"days_fill_ave"} =  0;
                    $vacant_manpower[$loop] = [
                        "position" => $position_count[0],
                        "count" => array_sum($position_manpower)
                    ];
                    $vacant_total_count += array_sum($position_manpower);
                }
                $total_count['vacant'] = $vacant_total_count; 
                $data['vacant_details'] = $vacant_manpower;
                $data['total_count'] = $total_count;
            }else{
                $data['error'] = 1;
                $data['error_details'] = $error_list;    
            }
        }
        return $data;
    }
    public function recruitment_data(){
        $start_date = '2020-09-01';
        $end_date = '2020-10-20';
        $data = $this->get_recruitment_data($start_date, $end_date);
        var_dump($data);
    }
    public function irecruit_excel_report(){
        $start_date = $this->input->post('start');
        $end_date = $this->input->post('end');
        $data = $this->get_recruitment_data($start_date, $end_date);
        // var_dump($data);
        if(!$data['error']){
            // $logo = $this->dir . '/assets/images/img/logo2.png';
            $logo = $this->dir . '/assets/images/img/logo2.png';
            $this->load->library('PHPExcel', null, 'excel');
            for($sheet_loop = 0; $sheet_loop < 2;  $sheet_loop++){
                $this->excel->createSheet($sheet_loop);
            }
            // ----------------------------------- Team recruitment report
            $this->excel->setActiveSheetIndex(2);
            $this->excel->getActiveSheet()->setTitle('Position Recruitment Stat');
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($logo);
            $objDrawing->setOffsetX(5); // setOffsetX works properly
            $objDrawing->setOffsetY(20); //setOffsetY has no effect
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(45);// logo height
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            $this->excel->getActiveSheet()->setShowGridlines(false);
            $header2 = [
                ['col' => '', 'id' => 'B2', 'title' => 'Recruitment Position Stat'],
                ['col' => '', 'id' => 'B3', 'title' => 'Date Hired: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
                ['col' => 'A', 'id' => 'A5', 'title' => 'POSITION'],
                ['col' => 'B', 'id' => 'B5', 'title' => 'REQUESTED'],
                ['col' => 'C', 'id' => 'C5', 'title' => 'HIRED'],
                ['col' => 'D', 'id' => 'D5', 'title' => 'VACANT'],
                ['col' => 'E', 'id' => 'E5', 'title' => 'EXCEED 30 DAYS'],
                ['col' => 'F', 'id' => 'F5', 'title' => 'AVERAGE DAYS-TO-FILL'],
            ];
            for($excel_data_header_loop = 0; $excel_data_header_loop < count($header2); $excel_data_header_loop++){
                // echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
                $this->excel->getActiveSheet()->setCellValue($header2[$excel_data_header_loop]['id'], $header2[$excel_data_header_loop]['title']);
                $this->excel->getActiveSheet()->getStyle($header2[$excel_data_header_loop]['id'])->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle($header2[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
                $this->excel->getActiveSheet()->getStyle($header2[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $this->excel->getActiveSheet()->getColumnDimension($header[$excel_data_header_loop]['col'])->setWidth(20);
            }
            $this->excel->getActiveSheet()->getStyle('B2:B2')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getFont()->getColor()->setRGB('000000');
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->getStyle('A5:F5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $rowNum2 = 6;		
            $last_row = (count($data['recruitment_position']) + $rowNum2)-1;
            // var_dump($last_row);
            if(count($data['recruitment_position']) > 0){
                foreach ($data['recruitment_position'] as $position_stat_rows) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $rowNum2, $position_stat_rows->pos_details);
                    $this->excel->getActiveSheet()->setCellValue('B' . $rowNum2, $position_stat_rows->requested);
                    $this->excel->getActiveSheet()->setCellValue('C' . $rowNum2, $position_stat_rows->hired);
                    $this->excel->getActiveSheet()->setCellValue('D' . $rowNum2, $position_stat_rows->vacant);
                    $this->excel->getActiveSheet()->setCellValue('E' . $rowNum2, $position_stat_rows->exceed_30);
                    $this->excel->getActiveSheet()->setCellValue('F' . $rowNum2, $position_stat_rows->days_fill_ave);
                    $rowNum2++;
                }
            }
            $this->excel->getActiveSheet()->getStyle('B6:B' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C6:C' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('D6:D' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('E6:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('F6:F' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A6:A' . $last_row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A5:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => 'DDDDDD'),
                        ),
                    ),
                )
            );
            // ----------------------------------- Team recruitment report
            $this->excel->setActiveSheetIndex(1);
            $this->excel->getActiveSheet()->setTitle('Team Recruitment Stat');
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($logo);
            $objDrawing->setOffsetX(5); // setOffsetX works properly
            $objDrawing->setOffsetY(20); //setOffsetY has no effect
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(45);// logo height
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            $this->excel->getActiveSheet()->setShowGridlines(false);
            $header2 = [
                ['col' => '', 'id' => 'B2', 'title' => 'Recruitment Team Stat'],
                ['col' => '', 'id' => 'B3', 'title' => 'Date Hired: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
                ['col' => 'A', 'id' => 'A5', 'title' => 'TEAM'],
                ['col' => 'B', 'id' => 'B5', 'title' => 'REQUESTED'],
                ['col' => 'C', 'id' => 'C5', 'title' => 'HIRED'],
                ['col' => 'D', 'id' => 'D5', 'title' => 'VACANT'],
                ['col' => 'E', 'id' => 'E5', 'title' => 'EXCEED 30 DAYS'],
                ['col' => 'F', 'id' => 'F5', 'title' => 'AVERAGE DAYS-TO-FILL'],
            ];
            for($excel_data_header_loop = 0; $excel_data_header_loop < count($header2); $excel_data_header_loop++){
                // echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
                $this->excel->getActiveSheet()->setCellValue($header2[$excel_data_header_loop]['id'], $header2[$excel_data_header_loop]['title']);
                $this->excel->getActiveSheet()->getStyle($header2[$excel_data_header_loop]['id'])->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle($header2[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
                $this->excel->getActiveSheet()->getStyle($header2[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $this->excel->getActiveSheet()->getColumnDimension($header[$excel_data_header_loop]['col'])->setWidth(20);
            }
            $this->excel->getActiveSheet()->getStyle('B2:B2')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getFont()->getColor()->setRGB('000000');
            $this->excel->getActiveSheet()->getStyle('B2:B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->getStyle('A5:F5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $rowNum1 = 6;		
            $last_row = (count($data['recruitment_account']) + $rowNum1)-1;
            // var_dump($last_row);
            if(count($data['recruitment_account']) > 0){
                foreach ($data['recruitment_account'] as $account_stat_rows) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $rowNum1, $account_stat_rows->acc_name);
                    $this->excel->getActiveSheet()->setCellValue('B' . $rowNum1, $account_stat_rows->requested);
                    $this->excel->getActiveSheet()->setCellValue('C' . $rowNum1, $account_stat_rows->hired);
                    $this->excel->getActiveSheet()->setCellValue('D' . $rowNum1, $account_stat_rows->vacant);
                    $this->excel->getActiveSheet()->setCellValue('E' . $rowNum1, $account_stat_rows->exceed_30);
                    $this->excel->getActiveSheet()->setCellValue('F' . $rowNum1, $account_stat_rows->days_fill_ave);
                    $rowNum1++;
                }
            }
            $this->excel->getActiveSheet()->getStyle('B6:B' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C6:C' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('D6:D' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('E6:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('F6:F' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A6:A' . $last_row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A5:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => 'DDDDDD'),
                        ),
                    ),
                )
            );
            // ----------------------------------- DETAILED recruitment report
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('Recruitment Detailed Report');
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($logo);
            $objDrawing->setOffsetX(5); // setOffsetX works properly
            $objDrawing->setOffsetY(20); //setOffsetY has no effect
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(45);// logo height
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            $this->excel->getActiveSheet()->setShowGridlines(false);
            $header = [
                ['col' => '', 'id' => 'C2', 'title' => 'Recruitment Records'],
                ['col' => '', 'id' => 'C3', 'title' => 'Date Hired: '.date_format(date_create($start_date), "M j, Y").' - '.date_format(date_create($end_date), "M j, Y")],
                ['col' => '', 'id' => 'A5', 'title' => 'Hired: '.$data['total_count']['hired']],
                ['col' => '', 'id' => 'A6', 'title' => 'Vacant: '.$data['total_count']['vacant']],
                ['col' => '', 'id' => 'A7', 'title' => 'Recruitment Count: '.$data['total_count']['recruitment_count']],
                ['col' => '', 'id' => 'C5', 'title' => 'Cancelled: '.$data['total_count']['cancelled']],
                ['col' => '', 'id' => 'C6', 'title' => 'Exceeded 30 days: '.$data['total_count']['exceed_30']],
                ['col' => 'A', 'id' => 'A9', 'title' => 'IREQUEST-ID'],
                ['col' => 'B', 'id' => 'B9', 'title' => 'SITE'],
                ['col' => 'C', 'id' => 'C9', 'title' => 'DATE REQUESTED'],
                ['col' => 'D', 'id' => 'D9', 'title' => 'REQUESTING TEAM'],
                ['col' => 'E', 'id' => 'E9', 'title' => 'STATUS'],
                ['col' => 'F', 'id' => 'F9', 'title' => 'DATE NEEDED'],
                ['col' => 'G', 'id' => 'G9', 'title' => 'POSITION'],
                ['col' => 'H', 'id' => 'H9', 'title' => 'Justification'],
                ['col' => 'I', 'id' => 'I9', 'title' => 'Hiring Type'],
                ['col' => 'J', 'id' => 'J9', 'title' => 'MANPOWER NEEDED'],
                ['col' => 'K', 'id' => 'K9', 'title' => 'AFT APPROVED'],
                ['col' => 'L', 'id' => 'L9', 'title' => 'DATE APPROVED'],
                ['col' => 'M', 'id' => 'M9', 'title' => 'REQUEST AVE. DAYS-TO-FILL'],
                ['col' => 'N', 'id' => 'N9', 'title' => 'LAST NAME'],
                ['col' => 'O', 'id' => 'O9', 'title' => 'FIRST NAME'],
                ['col' => 'P', 'id' => 'P9', 'title' => 'MIDDLE NAME'],
                ['col' => 'Q', 'id' => 'Q9', 'title' => 'DATE HIRED'],
                ['col' => 'R', 'id' => 'R9', 'title' => 'DAYS-TO-FILL'],

            ];
            for($excel_data_header_loop = 0; $excel_data_header_loop < count($header); $excel_data_header_loop++){
                // echo $excel_data['headers'][$excel_data_header_loop]['id']." ".$excel_data['headers'][$excel_data_header_loop]['title']."<br>";
                $this->excel->getActiveSheet()->setCellValue($header[$excel_data_header_loop]['id'], $header[$excel_data_header_loop]['title']);
                $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getFont()->getColor()->setRGB('FFFFFF');
                $this->excel->getActiveSheet()->getStyle($header[$excel_data_header_loop]['id'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $this->excel->getActiveSheet()->getColumnDimension($header[$excel_data_header_loop]['col'])->setWidth(20);
            }
            $this->excel->getActiveSheet()->getStyle('C2:C2')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('C2:C6')->getFont()->getColor()->setRGB('000000');
            $this->excel->getActiveSheet()->getStyle('A4:A7')->getFont()->getColor()->setRGB('000000');
            $this->excel->getActiveSheet()->getStyle('C2:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->getStyle('A4:A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->getStyle('A9:R9')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('404040');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(17);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(27);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(17);
            $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(17);
            $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(23);
            $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(22);
            $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(28);
            $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(23);
            $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
            $this->excel->getActiveSheet()->freezePane('A10');
            $rowNum = 10;		
            $last_row = (count($data['recruitment_detailed_applicants']) + $rowNum)-1;
            // // var_dump($last_row);
            // var_dump($data['recruitment_detailed_applicants']);
            if(count($data['recruitment_detailed_applicants']) > 0){
                foreach ($data['recruitment_detailed_applicants'] as $detailed_recruitment_rows) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $rowNum, str_pad($detailed_recruitment_rows->irequest_ID, 8, '0', STR_PAD_LEFT));
                    $this->excel->getActiveSheet()->setCellValue('B' . $rowNum, $detailed_recruitment_rows->code);
                    $this->excel->getActiveSheet()->setCellValue('C' . $rowNum, date_format(date_create($detailed_recruitment_rows->dateRequested), "m/d/Y"));
                    $this->excel->getActiveSheet()->setCellValue('D' . $rowNum, ucwords($detailed_recruitment_rows->acc_name));
                    $this->excel->getActiveSheet()->setCellValue('E' . $rowNum, $detailed_recruitment_rows->recruitmentStat);
                    $this->excel->getActiveSheet()->setCellValue('F' . $rowNum, date_format(date_create($detailed_recruitment_rows->dateNeeded), "m/d/Y H:i:s A"));
                    $this->excel->getActiveSheet()->setCellValue('G' . $rowNum, $detailed_recruitment_rows->pos_details);
                    $this->excel->getActiveSheet()->setCellValue('H' . $rowNum, $detailed_recruitment_rows->purpose);
                    $this->excel->getActiveSheet()->setCellValue('I' . $rowNum, $detailed_recruitment_rows->hiringtype_desc);
                    $this->excel->getActiveSheet()->setCellValue('J' . $rowNum, $detailed_recruitment_rows->manpower_requested);
                    $this->excel->getActiveSheet()->setCellValue('K' . $rowNum, $detailed_recruitment_rows->approvedByAft);
                    $this->excel->getActiveSheet()->setCellValue('L' . $rowNum, date_format(date_create($detailed_recruitment_rows->datedStatus), "m/d/Y H:i:s A"));
                    $this->excel->getActiveSheet()->setCellValue('M' . $rowNum, $detailed_recruitment_rows->request_days_fill_ave);
                    $this->excel->getActiveSheet()->setCellValue('N' . $rowNum, $detailed_recruitment_rows->lname);
                    $this->excel->getActiveSheet()->setCellValue('O' . $rowNum, $detailed_recruitment_rows->fname);
                    $this->excel->getActiveSheet()->setCellValue('P' . $rowNum, $detailed_recruitment_rows->mname);
                    $this->excel->getActiveSheet()->setCellValue('Q' . $rowNum, date_format(date_create($detailed_recruitment_rows->date_created), "m/d/Y H:i:s A"));
                    $this->excel->getActiveSheet()->setCellValue('R' . $rowNum, $detailed_recruitment_rows->days_fill_ave);
                    if($detailed_recruitment_rows->days_fill_ave > 30){
                        $this->excel->getActiveSheet()->getStyle('A' .$rowNum.':'.'R' .$rowNum)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF9999');
                    }
                    $rowNum++;
                }
            }
            $this->excel->getActiveSheet()->getStyle('A9:A' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('B9:B' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C9:C' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('E9:E' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('F9:F' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('G9:G' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('H9:H' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('I9:I' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('J9:J' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('K9:K' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('L9:L' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('M9:M' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('Q9:Q' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('R9:R' . $last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            // $this->excel->getActiveSheet()->getStyle('A6:A' . $last_row)->getFont()->setBold(true);
            // $this->excel->getActiveSheet()->getStyle('E6:E' . $last_row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A9:' . $this->excel->getActiveSheet()->getHighestColumn() . $this->excel->getActiveSheet()->getHighestRow())->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => 'DDDDDD'),
                        ),
                    ),
                )
            );
            $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
            $this->excel->getActiveSheet()->getProtection()->setSheet(true);
            $filename = 'Recruitment Report '.$start_date.' to '.$end_date.'.xlsx'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            // header('Set-Cookie: fileDownload=true; path=/');

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
            ob_end_clean();
            $objWriter->save('php://output');
        }
    }
    public function add_Applicants(){
        $dateTime = $this->get_current_date_time();
        $irecReq_ID = $this->input->post('irec_req');
        $app =  explode(",", $this->input->post('applicant'));
        $ireq = $this->input->post('ireq');
        $numApp = $this->input->post('num_Applicant');

        $irecruit_applicant = array();
        //Check if Enough pa ba ang gi add
            $irequest = $this->general_model->custom_query("SELECT manpower_requested,manpower_supplied FROM `tbl_irequest_manpower_request` WHERE irequest_ID = $ireq");
            $needed = (int) $irequest[0]->manpower_requested;
            $supplied = (int) $irequest[0]->manpower_supplied;
            $remaining = $needed - $supplied;
           
            if($numApp<=$remaining){
                $ret['morethan'] = 0;
                for ($loop = 0; $loop < count($app); $loop++) {
                    $application['apid'] = $app[$loop];
                    $application['status_ID'] = 29; 
                    // hired is 29
                    $application['site'] = $this->input->post('site');
                    $application['position_ID'] = $this->input->post('position');
                    $application['type'] = "irequest";
                    $application['added_by'] = $this->session->userdata('emp_id');
                    $application['date_created'] = $dateTime['dateTime'];
                    $last_application_id = $this->general_model->insert_vals_last_inserted_id($application, 'tbl_irecruit_application');
        
                    $irecruit_applicant[$loop] = [
                        'irequestRecruitment_ID' =>  $irecReq_ID,
                        'application_ID' => $last_application_id,
                        'date_hired' => $dateTime['dateTime'],
                    ];               
                }
                $supp = $supplied + $numApp;
                $where = "irequest_ID = $ireq";
                $suppd['manpower_supplied'] = $supp;
                $ret['supply'] = $supp;
                $ret['tosupply'] = $remaining;
                // update supply here 

                if($supp==$needed){
                    //Mark this ireq as completed
                    $ret['completed'] = 1;
                    $save['status_ID'] = 3;
                    $save['numberOfRecruit'] = $supp;
                    $save['dateTimeCompleted'] = $dateTime['dateTime'];
                    $suppd['datedStatus'] = $dateTime['dateTime'];
                }else{
                    $ret['completed'] = 0;
                    $save['numberOfRecruit'] = $supp;
                }
                $this->general_model->update_vals($save, $where, 'tbl_irequest_recruitment');
                $this->general_model->update_vals($suppd, $where, 'tbl_irequest_manpower_request');
                $this->general_model->batch_insert($irecruit_applicant, 'tbl_irecruit_applicant');
                $ret['HiredApplicants'] = $this->get_ApplicantsHired($irecReq_ID);
            }else{
                $ret['morethan'] = 1;
                $ret['remaining'] = $remaining;
            }
            $ret['applicants'] = $this->Get_Applicants($ireq);
            echo json_encode($ret);
    }

    public function get_ApplicantsHired($irecIreq){
        return $this->general_model->custom_query("SELECT ap.*, iapp.application_ID, iapp.position_ID, iappt.irequestRecruitment_ID,iappt.date_hired FROM tbl_applicant ap, tbl_irecruit_application iapp, tbl_irecruit_applicant iappt, tbl_irequest_recruitment recreq WHERE recreq.irequestRecruitment_ID=iappt.irequestRecruitment_ID AND ap.apid = iapp.apid AND iapp.application_ID = iappt.application_ID AND recreq.irequestRecruitment_ID=$irecIreq AND iappt.irequestRecruitment_ID=$irecIreq");
    }
    public function remove_Applicant(){
        $application = $this->input->post('application_id');
        $applicant = $this->input->post('applicant_id');
        $ireq = $this->input->post('ireq');

        $where = "application_ID = $application";
        $where2 = "application_ID = $application";
        $where3 = "irequest_ID = $ireq";
        $this->general_model->delete_vals($where, 'tbl_irecruit_application');
        $this->general_model->delete_vals($where2, 'tbl_irecruit_applicant');

        $supply = $this->general_model->custom_query("SELECT manpower_supplied,manpower_requested FROM `tbl_irequest_manpower_request` WHERE irequest_ID = $ireq");
        $recruitment = $this->general_model->custom_query("SELECT irequestRecruitment_ID FROM `tbl_irequest_recruitment` WHERE irequest_ID = $ireq");
        $newSupply = (int) $supply[0]->manpower_supplied - 1;
        $remaining = (int) $supply[0]->manpower_requested - $newSupply;
        $where3 = "irequest_ID = $ireq";
        $sUpp['manpower_supplied'] = $newSupply;

        $this->general_model->update_vals($sUpp, $where3, 'tbl_irequest_manpower_request');
        $ha['hired'] = $this->get_ApplicantsHired($recruitment[0]->irequestRecruitment_ID);
        $ha['applicants'] = $this->Get_Applicants($ireq);
        $ha['supply'] = $newSupply;
        $ha['tosupply'] = $remaining;
        $save['numberOfRecruit'] = $newSupply;
        $this->general_model->update_vals($save, $where3, 'tbl_irequest_recruitment');
        echo json_encode($ha);
    }
    public function get_ApplicantList(){
        $ireq = $this->input->post('ireq');
        $appcant = $this->Get_Applicants($ireq);
        echo json_encode($appcant);
    }
    public function filters_hired_applicant(){
        $q['applicant'] = $this->general_model->custom_query("SELECT DISTINCT(app.apid),app.*,iapp.irecruitApplicant_ID,iapp.date_hired,iappn.application_ID,iappn.status_ID as applicationStat, iappn.position_ID,iappn.dated_status,irec.irequestRecruitment_ID,irec.status_ID as irecStatus,irec.numberOfRecruit, manpower.irequest_ID,manpower.manpower_requested, manpower.manpower_supplied,manpower.position_ID,manpower.datedStatus, pos.pos_id,pos.pos_name,pos.pos_details,pos.site_ID,pos.isHiring FROM tbl_applicant app, tbl_irecruit_applicant iapp, tbl_irecruit_application iappn, tbl_irequest_recruitment irec,tbl_irequest_manpower_request manpower, tbl_position pos WHERE app.apid = iappn.apid AND iapp.application_ID = iappn.application_ID AND irec.irequest_ID = manpower.irequest_ID AND irec.irequestRecruitment_ID = iapp.irequestRecruitment_ID AND pos.pos_id = manpower.position_ID AND pos.pos_id = iappn.position_ID GROUP BY app.apid ORDER BY app.lname ASC");
        $q['position'] = $this->general_model->custom_query("SELECT DISTINCT(pos.pos_id),app.*,iapp.irecruitApplicant_ID,iapp.date_hired,iappn.application_ID,iappn.status_ID as applicationStat, iappn.position_ID,iappn.dated_status,irec.irequestRecruitment_ID,irec.status_ID as irecStatus,irec.numberOfRecruit, manpower.irequest_ID,manpower.manpower_requested, manpower.manpower_supplied,manpower.position_ID,manpower.datedStatus, pos.pos_id,pos.pos_name,pos.pos_details,pos.site_ID,pos.isHiring FROM tbl_applicant app, tbl_irecruit_applicant iapp, tbl_irecruit_application iappn, tbl_irequest_recruitment irec,tbl_irequest_manpower_request manpower, tbl_position pos WHERE app.apid = iappn.apid AND iapp.application_ID = iappn.application_ID AND irec.irequest_ID = manpower.irequest_ID AND irec.irequestRecruitment_ID = iapp.irequestRecruitment_ID AND pos.pos_id = manpower.position_ID AND pos.pos_id = iappn.position_ID GROUP BY pos.pos_id");
        echo json_encode($q);
    }
    public function getRecReqRecords_Number(){
        // iREquest
        $start_date = $this->input->post('s');
        $end_date = $this->input->post('e');
        $date_request = "";
        $date_recruit = "";
        $date_request= "AND DATE(dateRequested) >= '$start_date' AND DATE(dateRequested) <= '$end_date'";
        $date_recruit= "AND DATE(req.datedStatus) >= '$start_date' AND DATE(req.datedStatus) <= '$end_date'";

        // $q['request'] = $this->get_irequest_data($start_date, $end_date);

        $q['pending_req'] =$this->general_model->custom_query("SELECT COUNT(irequest_ID) as pending FROM tbl_irequest_manpower_request WHERE status = 2 " . $date_request);
        $q['approved_req'] = $this->general_model->custom_query("SELECT COUNT(irequest_ID) as approved FROM tbl_irequest_manpower_request WHERE status = 5 " . $date_request);
        $q['disapproved_req'] = $this->general_model->custom_query("SELECT COUNT(irequest_ID) as disapproved FROM tbl_irequest_manpower_request WHERE status = 6 " . $date_request);
        $q['missed_req'] = $this->general_model->custom_query("SELECT COUNT(irequest_ID) as missed FROM tbl_irequest_manpower_request WHERE status = 12 " . $date_request);
        $q['all_req'] = $this->general_model->custom_query("SELECT COUNT(irequest_ID) as all_req FROM tbl_irequest_manpower_request WHERE DATE(dateRequested) >= '$start_date' AND DATE(dateRequested) <= '$end_date' ");
           
        // iRecruit
        $q['recruitment'] = $this->get_recruitment_data($start_date, $end_date);
        echo json_encode($q);
    }
    public function list_get_manpowerDetails(){
        $date_q = "";
        $datatable = $this->input->post('datatable');
        $status = $datatable['query']['status'];
        $status = (int) $status;
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];
        $date_q= "AND DATE(req.dateRequested) >= '$start_date' AND DATE(req.dateRequested) <= '$end_date'";
       
        $query['query'] = "SELECT req.*,pos.pos_details,acc.acc_name,ap.fname, ap.mname, ap.lname, stat.description as statdesc FROM tbl_irequest_manpower_request req, tbl_position pos, tbl_department dept, tbl_account acc, tbl_applicant ap, tbl_employee emp, tbl_status stat WHERE pos.pos_id=req.position_ID AND acc.dep_id=dept.dep_id AND acc.acc_id=req.requesting_dept AND req.requested_by=emp.emp_id AND emp.apid=ap.apid AND req.status=stat.status_ID AND req.status=$status " . $date_q;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function list_get_reqDetails(){
        $datatable = $this->input->post('datatable');
        // $status = $datatable['query']['status'];
        $start_date = $datatable['query']['startDate'];
        $end_date = $datatable['query']['endDate'];
        $ar[]=0;
     
        $a = $this->get_recruitment_data($start_date, $end_date);
        $query['query'] = $a['recruitment_position'];

        // $ids = array_column($a, 'vacant_details');
        // $ids = array_map(function ($ar) {return $ar['vacant_details'];}, $a);
     
        // var_dump($a);

        // var_dump($a['vacant_details']);
        // var_dump($a);
        // die();
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function getRequestDetails(){
        $datatable = $this->input->post('status');
        $start_date = $this->input->post('start');
        $end_date = $this->input->post('end');

        $a = $this->get_recruitment_data($start_date, $end_date);
        echo json_encode($a);
    }
    // START MIC CODE
    // END MIC CODE
}
?>