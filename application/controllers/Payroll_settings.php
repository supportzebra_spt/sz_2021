<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once dirname(__FILE__) . "/General.php";

class Payroll_settings extends General
{

    protected $title = array('title' => 'Payroll Settings');

    public function __construct()
    {
        parent::__construct();
    }

    public function page()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();

        $this->load_template_view('templates/payroll/settings/settings_page', $data);

    }
    public function loandeduct()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/payroll/settings/loandeduct', $data);
        }
    }
    public function positions()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/payroll_settings/positions', $data);
        }
    }

    public function get_all_departments()
    {
        $data = $this->general_model->fetch_all("dep_id,dep_name,dep_details", "tbl_department", "dep_details ASC");
        echo json_encode(array('departments' => $data));
    }

    public function get_all_sites()
    {
        $data = $this->general_model->fetch_all("site_ID,code,siteName", "tbl_site");
        echo json_encode(array('sites' => $data));
    }

    public function getPositions()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $searchposition = $this->input->post('searchposition');
        $searchsite = $this->input->post('searchsite');
        $searchclass = $this->input->post('searchclass');
        $searchdepartments = $this->input->post('searchdepartments');
        $search = array();
        $searchString = "";
        if ($searchposition !== '') {
            $search[] = "(a.pos_name LIKE \"%$searchposition%\" OR a.pos_details LIKE \"%$searchposition%\")";
        }
        if ($searchsite !== '') {
            $search[] = "d.site_ID = '$searchsite'";
        }
        if ($searchclass !== '') {
            $search[] = "a.class = '$searchclass'";
        }
        if ($searchdepartments !== '') {
            $search[] = "c.dep_id=$searchdepartments";
        }

        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT a.pos_id,a.pos_name,a.pos_details,c.dep_id,c.dep_name,c.dep_details,d.code as site,a.class,a.isHiring FROM tbl_position a,tbl_pos_dep b,tbl_department c,tbl_site d WHERE a.pos_id=b.pos_id AND b.dep_id=c.dep_id AND a.site_ID=d.site_ID   $searchString ORDER BY pos_details";
        $positions = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('positions' => $positions, 'total' => $count));
    }

    public function getStatusRates()
    {
        $pos_id = $this->input->post('pos_id');
        $statuses = $this->general_model->fetch_all("empstat_id,status", "tbl_emp_stat");
        foreach ($statuses as $status) {
            $status->details = $this->general_model->fetch_specific_val("b.rate_id,b.rate,b.date_effect ", "a.posempstat_id=b.posempstat_id AND a.pos_id = $pos_id AND a.empstat_id = " . $status->empstat_id . " AND b.isActive = 1", "tbl_pos_emp_stat a,tbl_rate b");
        }
        echo json_encode(array('statuses' => $statuses));
    }

    public function getAllowances()
    {
        $pos_id = $this->input->post('pos_id');
        $allowances = ['Clothing', 'Laundry', 'Rice'];

        $statuses = $this->general_model->fetch_specific_vals("a.empstat_id,a.status", "b.pos_id=$pos_id AND a.empstat_id=b.empstat_id AND b.posempstat_id=c.posempstat_id AND c.isActive = 1", "tbl_emp_stat a,tbl_pos_emp_stat b,tbl_rate c");
        foreach ($statuses as $status) {
            $status->details = $this->general_model->fetch_specific_vals("b.allowance_id,b.allowance_name,b.value,b.posempstat_id,c.status", "a.posempstat_id=b.posempstat_id AND a.empstat_id=" . $status->empstat_id . " AND a.pos_id = $pos_id AND allowance_name IN ('" . implode("','", $allowances) . "') AND b.isActive = 1 AND a.empstat_id=c.empstat_id", "tbl_pos_emp_stat a,tbl_allowance b,tbl_emp_stat c");
        }

        echo json_encode(array('statuses' => $statuses, 'allowances' => $allowances));
    }

    public function getBonuses()
    {
        $pos_id = $this->input->post('pos_id');
        $bonuses = $this->general_model->fetch_all("bonus_id,bonus_name", "tbl_bonus");
        $bonusids = array();
        foreach ($bonuses as $bonus) {
            $bonusids[] = $bonus->bonus_id;
        }
        $statuses = $this->general_model->fetch_specific_vals("a.empstat_id,a.status", "b.pos_id=$pos_id AND a.empstat_id=b.empstat_id AND b.posempstat_id=c.posempstat_id AND c.isActive = 1", "tbl_emp_stat a,tbl_pos_emp_stat b,tbl_rate c");
        foreach ($statuses as $status) {
            $status->details = $this->general_model->fetch_specific_vals("b.pbonus_id,b.bonus_id,b.amount,b.posempstat_id,c.status", "a.posempstat_id=b.posempstat_id AND a.empstat_id=" . $status->empstat_id . " AND a.pos_id = $pos_id AND b.bonus_id IN (" . implode(",", $bonusids) . ") AND b.isActive = 1 AND a.empstat_id=c.empstat_id", "tbl_pos_emp_stat a,tbl_pos_bonus b,tbl_emp_stat c");
        }

        echo json_encode(array('statuses' => $statuses, 'bonuses' => $bonuses));
    }

    public function saveRatesDetails()
    {
        $uid = $this->session->userdata('uid');
        $empstat_id = $this->input->post('empstat_id');
        $rate_id = $this->input->post('rate_id');
        $pos_id = $this->input->post('pos_id');
        $rate = $this->input->post('rate');
        $date = $this->input->post('date');
        $posempstat = $this->general_model->fetch_specific_val("posempstat_id", "pos_id=$pos_id AND empstat_id=$empstat_id AND isActive = 1", "tbl_pos_emp_stat");
        if ($posempstat === null || !isset($posempstat->posempstat_id)) {

            $posempstat_id = $this->general_model->insert_vals_last_inserted_id(array('pos_id' => $pos_id, 'empstat_id' => $empstat_id, 'date' => date('Y-m-d'), 'isActive' => 1), "tbl_pos_emp_stat");
            if ($posempstat_id === 0) {
                $status = "Failed";
            } else {
                //assign default value for leave credits for new posempstat_id
                $noCredits = array();
                $leave_list = $this->general_model->fetch_all("leaveType_ID,leaveType", "tbl_leave_type", "leaveType ASC");

                foreach ($leave_list as $leave) {
                    $cred = $this->general_model->fetch_specific_val("leaveCredit_ID,credit", "leaveType_ID=$leave->leaveType_ID AND posempstat_id=$posempstat_id", "tbl_leave_credit");

                    if (empty($cred)) {
                        $noCredits[] = array('posempstat_id' => $posempstat_id, 'leaveType_ID' => $leave->leaveType_ID, 'credit' => 0, 'isActive' => 1, 'changedBy' => $uid);
                    }
                }
                $this->general_model->batch_insert($noCredits, "tbl_leave_credit");
            }
        } else {
            $posempstat_id = $posempstat->posempstat_id;
        }
        if ($posempstat_id !== null || $status !== "Failed") {
            $data = array(
                'rate' => $rate,
                'posempstat_id' => $posempstat_id,
                'date_effect' => $date,
                'isActive' => 1,
                'added_by' => $this->session->userdata('uid'),
                'date_added' => date("Y-m-d H:i:s"),
            );
            $res = $this->general_model->insert_vals($data, "tbl_rate");
            $res2 = 1;
            if ($rate_id !== '') {
                $res2 = $this->general_model->update_vals(array('isActive' => 0), "rate_id=$rate_id", "tbl_rate");
            }
            $status = ($res === 1 && $res2 === 1) ? "Success" : "Failed";
        }
        echo json_encode(array('status' => $status));
    }

    public function getSinglePosition()
    {
        $pos_id = $this->input->post('pos_id');
        $position = $this->general_model->fetch_specific_val("a.*,b.dep_id,c.*", "a.pos_id=$pos_id AND a.pos_id=b.pos_id AND a.site_ID = c.site_ID", "tbl_position a,tbl_pos_dep b,tbl_site c");
        echo json_encode(array('position' => $position));
    }

    public function savePosition()
    {
        $pos_name = $this->input->post('code');
        $pos_details = $this->input->post('positionname');
        $isHiring = $this->input->post('ishiring');
        $dep_id = $this->input->post('department');
        $class = $this->input->post('class');
        $site_ID = $this->input->post('site_ID');
        $data = array(
            'pos_name' => $pos_name,
            'pos_details' => $pos_details,
            'class' => $class,
            'site_ID' => $site_ID,
            'isHiring' => $isHiring,
        );
        if (isset($_POST['pos_id'])) {
            $from = "updated";
            $pos_id = $this->input->post('pos_id');
            $data['pos_id'] = $pos_id;
            $res1 = $this->general_model->update_vals($data, "pos_id=$pos_id", "tbl_position");
            $res2 = $this->general_model->update_vals(array('dep_id' => $dep_id), "pos_id=$pos_id", "tbl_pos_dep");
            $status = ($res1 === 1 && $res2 === 1) ? "Success" : "Failed";
        } else {
            $from = "created";
            $last_insert_id = $this->general_model->insert_vals_last_inserted_id($data, "tbl_position");
            if ($last_insert_id === 0) {
                $status = "Failed";
            } else {
                $res = $this->general_model->insert_vals(array('pos_id' => $last_insert_id, 'dep_id' => $dep_id), "tbl_pos_dep");
                $status = ($res === 1) ? "Success" : "Failed";
            }
            
            $pos_id = $last_insert_id;
        }
        echo json_encode(array('status' => $status, 'from' => $from, 'pos_id'=>$pos_id));
    }

    public function saveAllowanceDetails()
    {
        $empstat_id = $this->input->post('empstat_id');
        $pos_id = $this->input->post('pos_id');
        $allowance_id = $this->input->post('allowance_id');
        $allowance = $this->input->post('allowance');
        $allowance_name = $this->input->post('allowance_name');
        $posempstat = $this->general_model->fetch_specific_val("posempstat_id", "pos_id=$pos_id AND empstat_id=$empstat_id AND isActive = 1", "tbl_pos_emp_stat");
        if ($posempstat === null || !isset($posempstat->posempstat_id)) {
            $status = "Failed";
        } else {
            $posempstat_id = $posempstat->posempstat_id;
            $data = array(
                'allowance_name' => $allowance_name,
                'description' => $allowance_name,
                'value' => $allowance,
                'posempstat_id' => $posempstat_id,
                'isActive' => 1,
                'changedBy' => $this->session->userdata('uid'),
            );
            $res = $this->general_model->insert_vals($data, "tbl_allowance");
            $res2 = 1;
            if ($allowance_id !== '') {
                $res2 = $this->general_model->update_vals(array('isActive' => 0), "allowance_id=$allowance_id", "tbl_allowance");
            }
            $status = ($res === 1 && $res2 === 1) ? "Success" : "Failed";
        }

        echo json_encode(array('status' => $status));
    }

    public function saveBonusDetails()
    {
        $empstat_id = $this->input->post('empstat_id');
        $pos_id = $this->input->post('pos_id');
        $pbonus_id = $this->input->post('pbonus_id');
        $bonus = $this->input->post('bonus');
        $bonus_id = $this->input->post('bonus_id');
        $posempstat = $this->general_model->fetch_specific_val("posempstat_id", "pos_id=$pos_id AND empstat_id=$empstat_id AND isActive = 1", "tbl_pos_emp_stat");
        if ($posempstat === null || !isset($posempstat->posempstat_id)) {
            $status = "Failed";
        } else {
            $posempstat_id = $posempstat->posempstat_id;
            $data = array(
                'bonus_id' => $bonus_id,
                'amount' => $bonus,
                'posempstat_id' => $posempstat_id,
                'isActive' => 1,
                'changedBy' => $this->session->userdata('uid'),
            );
            $res = $this->general_model->insert_vals($data, "tbl_pos_bonus");
            $res2 = 1;
            if ($pbonus_id !== '') {
                $res2 = $this->general_model->update_vals(array('isActive' => 0), "pbonus_id=$pbonus_id", "tbl_pos_bonus");
            }
            $status = ($res === 1 && $res2 === 1) ? "Success" : "Failed";
        }

        echo json_encode(array('status' => $status));
    }

    /**MArk Codes**/

    public function deleteAdjust()
    {
        $pemp_adjust_id = $this->input->post('pemp_adjust_id');
        $delete_stat = $this->general_model->delete_vals("pemp_adjust_id=$pemp_adjust_id", "tbl_payroll_emp_adjustment");
        echo $delete_stat;
    }
    public function deleteEmpAdjust()
    {
        $pemp_adjust_id = $this->input->post('pemp_adjust_id');
        $count = $this->input->post('count');
        $fields = "count(*) as cnt";
        $where = "pemp_adjust_id=" . $pemp_adjust_id;
        $data = $this->general_model->fetch_specific_val($fields, $where, "tbl_payroll_deduct_child");
        if ($data->cnt > 0) {
            $rs = 2;
        } else {
            // $where = "a.pdeduct_id=b.pdeduct_id and pemp_adjust_id=".$pemp_adjust_id;
            // $rs =  $this->general_model->fetch_specific_val("*", $where, "tbl_payroll_emp_adjustment a,tbl_payroll_deductions b");
        }
        echo json_encode($rs);
    }
    public function updateEmpAdjust()
    {
        $pemp_adjust_id = $this->input->post('pemp_adjust_id');
        $count = $this->input->post('count');
        $fields = "count(*) as cnt";
        $where = "pemp_adjust_id=" . $pemp_adjust_id;
        $data = $this->general_model->fetch_specific_val($fields, $where, "tbl_payroll_deduct_child");
        if ($data->cnt > 0) {
            $rs = 2;
        } else {
            $where = "a.pdeduct_id=b.pdeduct_id and pemp_adjust_id=" . $pemp_adjust_id;
            $rs = $this->general_model->fetch_specific_val("*", $where, "tbl_payroll_emp_adjustment a,tbl_payroll_deductions b");
        }
        echo json_encode($rs);
    }
    public function updateActiveness()
    {
        $pemp_adjust_id = $this->input->post('pemp_adjust_id');
        $ans = $this->input->post('answer');
        $count = $this->input->post('count');
        $fields = "count(*) as cnt";
        $where = "pemp_adjust_id=" . $pemp_adjust_id;
        $data = $this->general_model->fetch_specific_val($fields, $where, "tbl_payroll_deduct_child");
        if ($data->cnt >= $count) {
            $rs = 2;
        } else {
            $answer['isActive'] = $ans;
            $rs = $this->general_model->update_vals($answer, "pemp_adjust_id=$pemp_adjust_id", "tbl_payroll_emp_adjustment");
        }
        echo $rs;
    }
    public function viewLoans()
    {
        $emp_id = $this->input->post('emp_id');

        $fields = "a.pemp_adjust_id,a.emp_id,deductionname,max_amount,adj_amount,count,a.isActive,c.isActive as empIsActive,(SELECT count(*) totalDeduct FROM tbl_payroll_deduct_child z where z.pemp_adjust_id=a.pemp_adjust_id) totalDeduct";
        $where = "a.pdeduct_id=b.pdeduct_id and  a.emp_id = c.emp_id and a.emp_id=" . $emp_id;
        $data["data"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_emp_adjustment a,tbl_payroll_deductions b,tbl_employee c");
        $data["personal"] = $this->get_emp_details_via_emp_id($emp_id);
        echo json_encode($data);
    }
    public function getListAdjustment()
    {
        // $emp_id = $this->input->post('emp_id');
        // $fields = "deductionname,description,pdeduct_id";
        // $where = "pdeduct_id not in (SELECT pdeduct_id FROM tbl_payroll_emp_adjustment where emp_id=" . $emp_id . ") and isActive=1";
        // $data["adjust"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_deductions");

		$emp_id = $this->input->post('emp_id');
        $fields = "deductionname,description,pdeduct_id";
        $where = "isActive=1";
        $data["adjust"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_deductions");

        $fields = "fname,lname,emp_id";
        $where = "a.apid=b.apid and b.isActive='yes' and emp_id not in (SELECT emp_id from tbl_payroll_emp_adjustment)";
        $data["emp"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_applicant a,tbl_employee b", "lname ASC");

        echo json_encode($data);

    }

    public function addAdjustment($act)
    {
        $emp_id = $this->input->post('emp_id');
        $pdeduct_id = $this->input->post('adjustname');
        $max_amount = $this->input->post('ma');
        $count = $this->input->post('mtp');
        $adj_date = $this->input->post('adjustDate');
        $remarks = $this->input->post('adjustComment');
        $pempadjustid = $this->input->post('pempadjustid');
        $adj_amount = (float) $max_amount / (float) $count;
        $data["emp_id"] = $emp_id;
        $data["pdeduct_id"] = $pdeduct_id;
        $data["max_amount"] = $max_amount;
        $data["adj_amount"] = $adj_amount;
        $data["adj_date"] = $adj_date;
        $data["isActive"] = 1;
        $data["remarks"] = $remarks;
        $data["count"] = $count;
        if ($pempadjustid == 0) {
            $last_insert_id = $this->general_model->insert_vals_last_inserted_id($data, "tbl_payroll_emp_adjustment");
        } else {
            $last_insert_id = $this->general_model->update_vals($data, "pemp_adjust_id=$pempadjustid", "tbl_payroll_emp_adjustment");

        }
        echo json_encode($last_insert_id);

    }
    public function loadEmpLoan($empStat)
    {
        $datatable = $this->input->post('datatable');

        // $query['query']  = "SELECT  fname,lname,a.* FROM tbl_payroll_emp_adjustment a,tbl_employee b,tbl_applicant c where a.emp_id=b.emp_id and b.apid=c.apid";

        $append = ($empStat == "all") ? "" : " and e.isActive='" . $empStat . "'";

        // $query['query']  = "SELECT a.emp_id,fname,lname,pos_name,pos_details,d.status FROM tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_employee e,tbl_applicant f,tbl_payroll_emp_adjustment g where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and a.emp_id=e.emp_id and g.emp_id=e.emp_id and e.apid=f.apid and a.isActive=1 $append";
        $query['query'] = "SELECT a.emp_id,fname,lname,pos_name,pos_details,d.status,(select count(*) cnt1 from tbl_payroll_emp_adjustment x where x.isActive=1 and x.emp_id=g.emp_id) cnt_active,(select count(*) cnt2 from tbl_payroll_emp_adjustment x where x.isActive=0 and x.emp_id=g.emp_id) cnt_inactive FROM tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_employee e,tbl_applicant f,tbl_payroll_emp_adjustment g where a.posempstat_id=b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and a.emp_id=e.emp_id and g.emp_id=e.emp_id and e.apid=f.apid and a.isActive=1 $append";
        if (isset($datatable['query']['ongoingSearchs'])) {
            $keyword = $datatable['query']['ongoingSearchs'];

            $query['search']['append'] = " and (lname LIKE  '%" . $keyword . "%' OR fname LIKE  '%" . $keyword . "%') GROUP BY a.emp_id";

            $query['search']['total'] = " and (lname LIKE '%" . $keyword . "%' OR fname LIKE  '%" . $keyword . "%') GROUP BY a.emp_id";
        } else {
            $query['query'] .= " GROUP BY a.emp_id";
        }

        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }

    public function checkIfreachedLimit($pemp_adjust_id)
    {
        $fields = "count(*) as cnt";
        $where = "pemp_adjust_id=" . $pemp_adjust_id;
        return $this->general_model->fetch_specific_val($fields, $where, "tbl_payroll_deduct_child");

    }
    public function checkCoverage()
    {
		$pemp_adjust_id = $this->input->post("pemp_adjust_id");
		$emp_id = $this->input->post("emp_id");
		
        echo  json_encode($this->general_model->custom_query("select daterange from tbl_payroll_coverage where coverage_id in (SELECT coverage_id FROM `tbl_payroll_deduct_child` where emp_id=$emp_id and pemp_adjust_id=$pemp_adjust_id)"));

    }
    public function syncLoans()
    {
        date_default_timezone_set('Asia/Manila');

        $emp_id = implode(",", $this->input->post('emp'));
        $emp_selected = implode(",", $this->input->post('emp_selected'));
        $coverage = $this->input->post('coverage');
        $fields = "*";
        $where = "coverage_id = $coverage and emp_id in (" . $emp_id . ")";
        $data["deleteAllSavedDeduct"] = $this->general_model->fetch_specific_vals($fields, $where, "tbl_payroll_deduct_child");

        $delete_stat = $this->general_model->delete_vals("coverage_id = $coverage and emp_id in (" . $emp_id . ")", "tbl_payroll_deduct_child");

        $field_check = "a.pemp_adjust_id,emp_id,deductionname,max_amount,adj_amount,count,a.isActive, (SELECT count(*) totalDeduct FROM tbl_payroll_deduct_child z where z.pemp_adjust_id=a.pemp_adjust_id) totalDeduct";
        $where_check = "a.pdeduct_id=b.pdeduct_id and emp_id in (" . $emp_selected . ") and a.isActive=1";
        $data["checkIfHasLoan"] = $this->general_model->fetch_specific_vals($field_check, $where_check, "tbl_payroll_emp_adjustment a,tbl_payroll_deductions b");
        $data["add"] = array();
        foreach ($data["checkIfHasLoan"] as $row) {
            $limit = $this->checkIfreachedLimit($row->pemp_adjust_id);
            if ((int) $limit->cnt < (int) $row->count) {
                $data["add"][$row->pemp_adjust_id] = array(
                    "deductionname" => $row->deductionname,
                    "max_amount" => $row->max_amount,
                    "count" => $row->count,
                    "totalDeduct" => $row->totalDeduct,
                    "limit" => $limit->cnt,
                );
                $data_add["coverage_id"] = $coverage;
                $data_add["emp_id"] = $row->emp_id;
                $data_add["pemp_adjust_id"] = $row->pemp_adjust_id;
                $data_add["date_given"] = date("Y-m-d H:i:s");
                $last_insert_id = $this->general_model->insert_vals_last_inserted_id($data_add, "tbl_payroll_deduct_child");
            }else{
                $data_emp_adjust["isActive"] = 0;
                $last_insert_id = $this->general_model->update_vals($data_emp_adjust, "pemp_adjust_id=" . $row->pemp_adjust_id, "tbl_payroll_emp_adjustment");

            }
        }

        echo json_encode($data);

    }
    /**Mark Codes**/

    //=====================================================================START OF NICCA CODES=====================================================================
    public function payroll_totalhours()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()){
        $this->load_template_view('templates/payroll_settings/totalhours', $data);
         }
    }

    public function get_coverage()
    {
        $search_year = $this->input->post('year');
        $search_month = $this->input->post('month');
        $search = array();
        $searchString = "";
        if ($search_year !== '') {
            $search[] = "year='$search_year'";
        }
        if ($search_month !== '') {
            $search[] = "month='$search_month'";
        }
        if (!empty($search)) {
            $searchString = "WHERE " . implode(" AND ", $search);
        }

        $data = $this->general_model->custom_query("SELECT * FROM tbl_payroll_coverage $searchString ORDER BY coverage_id DESC");
        echo json_encode(['coverage' => $data]);
    }
    public function downloadExcel_payroll_totalhours()
    {

        $coverage_id = $this->input->post('coverage_id');
        $coverage_text = $this->input->post('coverage_text');
        $searchtype = $this->input->post('type');
        $search = array();
        $searchString = "";
        if ($searchtype !== '') {
            foreach ($searchtype as $type) {
            	if($type=='agent'){
					$search[] = "emp_type like '%$type%'";
            	}else{
					$search[] = "(emp_type like '%$type%' OR b.emp_id = 139)";
            	}
            }
        }
        if (!empty($search)) {
            $searchString = "AND (" . implode(" OR ", $search) . ")";
        }
        $data = $this->general_model->custom_query("SELECT lname,fname,total_hours,daterange,a.emp_promoteId,emp_type FROM `tbl_payroll` a,tbl_emp_promote b,tbl_employee c,tbl_applicant d,tbl_payroll_coverage e where a.coverage_id=e.coverage_id and a.emp_promoteId = b.emp_promoteID and b.emp_id = c.emp_id and c.apid = d.apid and e.coverage_id =$coverage_id $searchString order by lname");
        //EXCEL PROPER
        $this->load->library('PHPExcel', null, 'excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Payroll Total Hours');
        $this->excel->getActiveSheet()->setShowGridlines(false);

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);

        $this->excel->getActiveSheet()->mergeCells('A1:D1');
        //set cell A1 content with some text

        $this->excel->getActiveSheet()->setCellValue('A1', $coverage_text);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Status');
        $this->excel->getActiveSheet()->setCellValue('B2', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('C2', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('D2', 'Total Hours');

        $emprow = 3;
        foreach ($data as $d) {
            $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $d->emp_type);
            $this->excel->getActiveSheet()->setCellValue('B' . $emprow, $d->lname);
            $this->excel->getActiveSheet()->setCellValue('C' . $emprow, $d->fname);
            $this->excel->getActiveSheet()->setCellValue('D' . $emprow, $d->total_hours);
            if($emprow%2==0){
                $this->excel->getActiveSheet()->getStyle('A' . $emprow.':D'.$emprow)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'eeeeee'))));
            }
            $emprow++;
        }

        $column = $this->excel->getActiveSheet()->getHighestColumn();
        $row = $this->excel->getActiveSheet()->getHighestRow();
        $this->excel->getActiveSheet()->getStyle('A1:' . $column . '' . $row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
        //HEADER
        $this->excel->getActiveSheet()->getStyle('A1:D1')->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'E66868')),'font' => array('bold' => true, 'size'=> 20)));
        //SUBHEADER
        $this->excel->getActiveSheet()->getStyle('A2:' . $column . '2')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'F7D794')),'font' => array('bold' => true, 'size'=> 14)));

        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);

        $filename = 'SZ_PayrollTotalHours.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    //======================================================================END OF NICCA CODES======================================================================
        //======================================================================START OF LATEST NICCA CODES=============================================================

   public function bonuses()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/payroll_settings/bonuses', $data);
        }
    }

    public function get_master_bonuses(){
        $searchStatus = $this->input->post('searchStatus');
        $searchTaxable = $this->input->post('searchTaxable');
        $search = array();
        $searchString = "";
     
        if ($searchStatus !== '' && $searchStatus != NULL) {
            $search[] = "isActive = $searchStatus";
        }
        if ($searchTaxable !== '' && $searchTaxable != NULL) {
            $search[] = "isTaxable = $searchTaxable";
        }

        if (!empty($search)) {
            $searchString = "WHERE " . implode(" AND ", $search);
        }
       $data = $this->general_model->custom_query("SELECT bonus_id,bonus_name,description,isActive,isTaxable FROM tbl_bonus $searchString ORDER BY bonus_name ASC");
       echo json_encode($data);
    }
    public function createMasterBonus(){
        $bonus_name = $this->input->post('bonus_name');
        $bonus_desc = $this->input->post('bonus_desc');
        $bonus_taxable = $this->input->post('bonus_taxable');
        $bonus_status = $this->input->post('bonus_status');
        $data = array(
                'bonus_name' => $bonus_name,
                'description' => $bonus_desc,
                'isTaxable' => $bonus_taxable,
                'isActive' => $bonus_status,
                'createdBy' => $this->session->userdata('emp_id'),
                'createdOn' => date("Y-m-d H:i:s"),
            );
        $res = $this->general_model->insert_vals($data, "tbl_bonus");
        echo json_encode(array('status' => ($res) ? "Success" : "Failed"));
    }
    public function updateMasterBonus(){
        $bonus_id = $this->input->post('bonus_id');
        $bonus_name = $this->input->post('bonus_name');
        $bonus_desc = $this->input->post('bonus_desc');
        $bonus_taxable = $this->input->post('bonus_taxable');
        $bonus_status = $this->input->post('bonus_status');
        $data = array(
                'bonus_name' => $bonus_name,
                'description' => $bonus_desc,
                'isTaxable' => $bonus_taxable,
                'isActive' => $bonus_status,
                'updatedBy' => $this->session->userdata('emp_id'),
                'updatedOn' => date("Y-m-d H:i:s"),
            );
        $res = $this->general_model->update_vals($data, "bonus_id = $bonus_id", "tbl_bonus");
        echo json_encode(array('status' => ($res) ? "Success" : "Failed"));
    }
     //======================================================================START OF LATEST NICCA CODES=============================================================

   public function holidays()
   {
       $data = $this->title;
       $data['uri_segment'] = $this->uri->segment_array();
       $data['sites'] = $this->general_model->custom_query("SELECT site_ID,code FROM tbl_site ORDER BY code ASC");
       if ($this->check_access()) {
           $this->load_template_view('templates/payroll_settings/holidays', $data);
       }
   }

   public function get_master_holidays(){
        $searchType = $this->input->post('searchType');
        $searchSite = $this->input->post('searchSite');
        $search = array();
        $searchString = "";
    
        if ($searchType !== '' && $searchType != NULL) {
            $searchString = "WHERE type = '$searchType'";
        }
        
        $mastersites = $this->general_model->custom_query("SELECT site_ID,code FROM tbl_site ORDER BY code ASC");
        $data = $this->general_model->custom_query("SELECT holiday_id,holiday,description,type,date,site FROM tbl_holiday $searchString ORDER BY date ASC");
        $final_data = array();
        if ($searchSite !== '' && $searchSite != NULL) {
            foreach($data as $d){
                $allowedSites = explode(",",$d->site);
                if(in_array($searchSite,$allowedSites)){
                    $final_data[] = $d;
                }
            }
        }else{
            $final_data = $data;
        }
        foreach($final_data as $d){
            $currentSites = explode(",",$d->site);
            $formattedSites = "";
            foreach($mastersites as $mastersite){
                foreach( $currentSites as $currentSite){
                    if($currentSite == $mastersite->site_ID){
                        $formattedSites.=$mastersite->code." ";
                    }
                }
            }
            $d->formattedSites = $formattedSites;
        }
        echo json_encode($final_data);
    }
    public function createMasterHoliday(){
        $holiday_name = $this->input->post('holiday_name');
        $holiday_desc = $this->input->post('holiday_desc');
        $holiday_type = $this->input->post('holiday_type');
        $holiday_date = $this->input->post('holiday_date');
        $holiday_site = $this->input->post('holiday_site');

        $data = array(
                'holiday' => $holiday_name,
                'description' => $holiday_desc,
                'type' => $holiday_type,
                'date' => date('2000-m-d',strtotime($holiday_date)),
                'calc' => ($holiday_type=='Regular')?1:0.3,
            'site'=>$holiday_site
            );
        $res = $this->general_model->insert_vals($data, "tbl_holiday");
        echo json_encode(array('status' => ($res) ? "Success" : "Failed"));
    }
    public function removeMasterHoliday($holiday_id){

        $delete_stat = $this->general_model->delete_vals("holiday_id=$holiday_id", "tbl_holiday");
        echo json_encode(array('status'=>$delete_stat?"Success":"Failed"));
    }
}