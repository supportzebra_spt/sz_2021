<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Schedule extends General
{

    private $administrator = array(6, 222, 1241); //uid //set,  levi, galleto
    protected $title = array('title' => 'Schedule');
    public $logo = "/var/www/html/sz/assets/images/img/logo2.png";
    // public $logo = "C:\\xampp\\htdocs\\sz\\assets\\images\\img\\logo2.png";

    public function __construct()
    {
        parent::__construct();
        //        $this->schedulefile_prev = "C:\\wamp64\\www\\Cloned\\uploads\\schedules\\" . $this->session->userdata('emp_id') . "\\schedule_prev.xlsx";
        //        $this->schedulefile_current = "C:\\wamp64\\www\\Cloned\\uploads\\schedules\\" . $this->session->userdata('emp_id') . "\\schedule_current.xlsx";
        $this->schedulefile_prev = "/var/www/html/sz/uploads/schedules/" . $this->session->userdata('emp_id') . "/schedule_prev.xlsx";
        $this->schedulefile_current = "/var/www/html/sz/uploads/schedules/" . $this->session->userdata('emp_id') . "/schedule_current.xlsx";
    }

    //-----------------------------------------------------------------------GENERAL AREA----------------------------------------------------------------------------------------------------------------------
    public function get_all_accounts()
    {
        $data = $this->general_model->fetch_all("acc_id,acc_name,acc_description", "tbl_account", "acc_name ASC");
        echo json_encode(array('accounts' => $data));
    }

    public function get_all_schedule_types()
    {
        $data = $this->general_model->fetch_all("schedtype_id,type", "tbl_schedule_type", "type ASC");
        echo json_encode(array('schedtypes' => $data));
    }

    public function get_current_job($emp_id)
    {
        return $this->general_model->fetch_specific_val("c.pos_details as position,d.status", "a.emp_id = $emp_id AND a.isActive=1 AND a.ishistory=1 AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d");
    }
    private function general_getEmpAcc_ids($includeSelf, $agentOnly=false){
        $uid = $this->session->userdata('uid');
        $acc_ids = array();
        if (!in_array($uid, $this->administrator)&& !in_array($this->session->userdata('role_id'), array('1','5'))) {
            $subordinates = $this->getEmployeeSubordinate($uid);
            $empids = array();
            if ($includeSelf === '1') {
                $empids[] = $this->session->emp_id;
            }
            foreach ($subordinates as $s) {
                $empids[] = $s->emp_id;
            }
            if ($this->session->userdata('role_id') === '2') {
                $empids[] = $this->session->userdata('emp_id');
            }
            if (empty($empids)) {
                $query = "SELECT acc_id FROM tbl_employee  WHERE isActive='yes' AND emp_id IN (0)";
            } else {
                $query = "SELECT acc_id FROM tbl_employee WHERE isActive='yes' AND emp_id IN (" . implode(",", $empids) . ")";
            }
        } else {
            $query = "SELECT acc_id FROM tbl_employee WHERE isActive='yes'";
        }
        $account_ids = $this->general_model->custom_query($query);
        foreach ($account_ids as $acc_id) {
            if ($acc_id->acc_id !== NULL) {
                $acc_ids[] = $acc_id->acc_id;
            }
        }
        if($agentOnly){
            $agentOnlyStr = " AND acc_description='Agent'";
        }else{
            $agentOnlyStr = "";
        }
        if (empty($acc_ids)) {
            $accounts = $this->general_model->fetch_specific_vals("acc_id,acc_name,acc_description", "acc_id IN (0)", "tbl_account", "acc_name ASC");
        } else {
            $unique_acc_ids = array_unique($acc_ids);
            $accounts = $this->general_model->fetch_specific_vals("acc_id,acc_name,acc_description", "acc_id IN (" . implode(",", $unique_acc_ids) . ")  $agentOnlyStr", "tbl_account", "acc_name ASC");
        }
        return $accounts;
    }

    public function getUniqueEmpAcc_ids($includeSelf = '0')
    {
        $accounts = $this->general_getEmpAcc_ids($includeSelf);
        echo json_encode(array('accounts' => $accounts, 'query' => $this->db->last_query()));
    }

    //-----------------------------------------------------------------------SCHEDULE INDEX AREA--------------------------------------------------------------------------------------------------------------
    public function manual()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/schedule/index', $data);
        }
    }

    function compareOrder($a, $b)
    {
        return strcmp($a['details']->lname, $b['details']->lname);
    }

    public function get_employee_schedules()
    {
        $uid = $this->session->userdata('uid');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $searchname = $this->input->post('searchname');
        $searchclass = $this->input->post('searchclass');
        $searchaccounts = $this->input->post('searchaccounts');
        $search = array();
        $searchString = "";
        if ($searchname !== '') {
            $search[] = "(a.fname LIKE \"%$searchname%\" OR a.lname LIKE \"%$searchname%\")";
        }
        if ($searchclass !== '') {
            $search[] = "c.acc_description = '$searchclass'";
        }
        if ($searchaccounts !== '') {
            $search[] = "c.acc_id=$searchaccounts";
        }

        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $subordinateString = "";

        if (!in_array($uid, $this->administrator) && !in_array($this->session->userdata('role_id'), array('1','5'))) {//xx
            $subordinates = $this->getEmployeeSubordinate($uid);
            $empids = array();
            foreach ($subordinates as $s) {
                $empids[] = $s->emp_id;
            }
            if ($this->session->userdata('role_id') === '2') {
                $empids[] = $this->session->userdata('emp_id');
            }
            if (empty($empids)) {
                $subordinateString = " AND e.emp_id IN (0) ";
                $query2 = "SELECT a.fname,a.lname,a.pic,b.emp_id,b.acc_id FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid=b.apid AND b.isActive='yes' AND b.emp_id IN (0) AND c.acc_id=b.acc_id $searchString  ORDER BY a.lname ASC";
            } else {
                $subordinateString = " AND e.emp_id IN (" . implode(",", $empids) . ") ";
                $query2 = "SELECT a.fname,a.lname,a.pic,b.emp_id,b.acc_id FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid=b.apid AND b.isActive='yes' AND b.emp_id IN (" . implode(",", $empids) . ") AND c.acc_id=b.acc_id $searchString  ORDER BY a.lname ASC";
            }
            $details = $this->general_model->custom_query($query2);
        } else {
            $empids = array();
            $query2 = "SELECT a.fname,a.lname,a.pic,b.emp_id,b.acc_id FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid=b.apid AND b.isActive='yes' AND c.acc_id=b.acc_id $searchString ORDER BY a.lname ASC";
            $details = $this->general_model->custom_query($query2);
            foreach ($details as $det) {
                $empids[] = $det->emp_id;
            }
            if (empty($empids)) {
                $subordinateString = " AND e.emp_id IN (0) ";
            } else {
                $subordinateString = " AND e.emp_id IN (" . implode(",", $empids) . ") ";
            }
        }
        $query = "SELECT a.sched_id,a.sched_date,c.time_start,c.time_end,d.type,d.style,e.emp_id,c.time_id,a.leavenote,f.leaveType,a.schedtype_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id INNER JOIN tbl_user e ON e.emp_id=a.emp_id LEFT JOIN tbl_leave_type f ON a.leavenote=f.leaveType_ID WHERE a.sched_date BETWEEN date('$startDate') AND date('$endDate') $subordinateString ORDER BY a.sched_date";
        $schedules = $this->general_model->custom_query($query);
        $employees = array();

        foreach ($details as $det) {
            $employees[$det->emp_id]['details'] = $det;
            $employees[$det->emp_id]['schedule'] = NULL;
            foreach ($schedules as $sched) {
                if ((int) $sched->emp_id === (int) $det->emp_id) {
                    $employees[$det->emp_id]['schedule'][$sched->sched_date][] = $sched;
                }
            }
        }
        usort($employees, array($this, "compareOrder"));
        echo json_encode(array('data' => $employees));
    }

    public function getDateSchedule()
    {
        $sched_date = $this->input->post('sched_date');
        $emp_id = $this->input->post('emp_id');
        $details = $this->general_model->fetch_specific_val("a.fname,a.lname,a.pic,b.emp_id,b.acc_id", "a.apid=b.apid AND b.isActive='yes' AND b.emp_id =$emp_id", "tbl_applicant a,tbl_employee b");
        $details->job = $this->get_current_job($details->emp_id);
        $schedule_query = "SELECT a.sched_id,a.sched_date,c.time_start,c.time_end,d.type,d.style,e.emp_id,a.updated_by,a.updated_on,a.remarks,a.module_ID,a.isLocked,a.acc_time_id,a.schedtype_id,c.time_id,a.leavenote FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id INNER JOIN tbl_user e ON e.emp_id=a.emp_id WHERE a.sched_date = '$sched_date' AND e.emp_id = $emp_id";
        $schedules = $this->general_model->custom_query($schedule_query);
        foreach ($schedules as $sched) {
            if ($sched->updated_by === NULL || $sched->updated_by === '' || $sched->updated_by === '0') {
                $sched->last_updater = "None";
            } else {
                $sched->last_updater = $this->general_model->fetch_specific_val("CONCAT(c.fname,' ',c.lname) as last_updater", "a.uid=" . $sched->updated_by . " AND a.emp_id=b.emp_id AND b.apid=c.apid", "tbl_user a,tbl_employee b,tbl_applicant c")->last_updater;
            }
        }
        $available_shifts = $this->general_model->fetch_specific_vals("time_start,time_end,time_id", "bta_id=1", "tbl_time", "STR_TO_DATE(CONCAT(CURDATE(),' ',time_start), '%Y-%m-%d %l:%i:%s %p') ASC, STR_TO_DATE(CONCAT(CURDATE(),' ',time_end), '%Y-%m-%d %l:%i:%s %p') ASC");
        //        $available_schedtypes = $this->general_model->fetch_specific_vals("schedtype_id,type,sched_description,style", "schedtype_id NOT IN (4)", "tbl_schedule_type", "type ASC");
        $available_schedtypes = $this->general_model->fetch_all("schedtype_id,type,sched_description,style", "tbl_schedule_type", "type ASC");
        $available_leavetypes = $this->general_model->fetch_all("leaveType_ID,leaveType", "tbl_leave_type", "leaveType ASC");
        echo json_encode(array('details' => $details, 'schedules' => $schedules, 'available_shifts' => $available_shifts, 'available_schedtypes' => $available_schedtypes, 'available_leavetypes' => $available_leavetypes));
    }

    public function getSchedTypeAndShift($acc_id)
    {
        $available_shifts = $this->general_model->fetch_specific_vals("time_start,time_end,time_id", "bta_id=1", "tbl_time", "STR_TO_DATE(CONCAT(CURDATE(),' ',time_start), '%Y-%m-%d %l:%i:%s %p') ASC, STR_TO_DATE(CONCAT(CURDATE(),' ',time_end), '%Y-%m-%d %l:%i:%s %p') ASC");
        //        $available_schedtypes = $this->general_model->fetch_specific_vals("schedtype_id,type,sched_description,style", "schedtype_id NOT IN (4)", "tbl_schedule_type", "type ASC");
        $available_schedtypes = $this->general_model->fetch_all("schedtype_id,type,sched_description,style", "tbl_schedule_type", "type ASC");
        $available_leavetypes = $this->general_model->fetch_all("leaveType_ID,leaveType", "tbl_leave_type", "leaveType ASC");
        echo json_encode(array('available_shifts' => $available_shifts, 'available_schedtypes' => $available_schedtypes, 'available_leavetypes' => $available_leavetypes));
    }

    public function insertSingleSchedule()
    {
        $leaveType_ID = $this->input->post('leaveType_ID');
        $schedtype_id = $this->input->post('schedtype_id');
        $time_id = $this->input->post('time_id');
        $sched_date = $this->input->post('sched_date');
        $emp_id = $this->input->post('emp_id');
        $remarks = $this->input->post('remarks');
        $last_updater = $this->session->userdata('uid');
        // $acc_id = $this->session->userdata('acc_id');
        $acc_id = $this->general_model->fetch_specific_val("acc_id", "emp_id = $emp_id", "tbl_employee")->acc_id;

        $acc_time_id = NULL;
        if (trim($time_id) !== '') {
            $acc_time = $this->general_model->fetch_specific_val("acc_time_id", "time_id=$time_id AND acc_id=$acc_id", "tbl_acc_time");
            if (isset($acc_time->acc_time_id) and $acc_time !== NULL) {
                $acc_time_id = $acc_time->acc_time_id;
            } else {
                //create acc_time_id
                $acc_time_id = $this->general_model->insert_vals_last_inserted_id(array('time_id' => $time_id, 'acc_id' => $acc_id, 'note' => 'DTR'), "tbl_acc_time");
            }
        }
        $data = array(
            'schedtype_id' => $schedtype_id,
            'acc_time_id' => $acc_time_id,
            'sched_date' => $sched_date,
            'remarks' => $remarks,
            'emp_id' => $emp_id,
            'updated_by' => $last_updater,
            'isActive' => 1,
            'isDouble' => 0,
            'isLocked' => 0,
            'leavenote' => $leaveType_ID
        );

        $sched_id = $this->general_model->insert_vals_last_inserted_id($data, "tbl_schedule");
        $schedule = NULL;
        if ($sched_id === 0) {
            $status = "Failed";
        } else {
            $query = "SELECT a.sched_id,a.sched_date,c.time_start,c.time_end,d.type,d.style,e.emp_id,c.time_id,a.leavenote,f.leaveType,a.schedtype_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id INNER JOIN tbl_user e ON e.emp_id=a.emp_id LEFT JOIN tbl_leave_type f ON a.leavenote=f.leaveType_ID WHERE a.sched_id=$sched_id ORDER BY a.sched_date";
            $result = $this->general_model->custom_query($query);
            if (empty($result)) {
                $status = "Failed";
            } else {
                $schedule = $result[0];
            }
            $status = "Success";
        }
        echo json_encode(array('status' => $status, 'schedule' => $schedule));
    }

    public function updateSingleSchedule()
    {
        $leaveType_ID = $this->input->post('leaveType_ID');
        $sched_id = $this->input->post('sched_id');
        $schedtype_id = $this->input->post('schedtype_id');
        $time_id = $this->input->post('time_id');
        $remarks = $this->input->post('remarks');
        $last_updater = $this->session->userdata('uid');
        // $acc_id = $this->session->userdata('acc_id');
        $acc_id = $this->general_model->fetch_specific_val("b.acc_id", "a.emp_id = b.emp_id AND a.sched_id=$sched_id", "tbl_schedule a,tbl_employee b")->acc_id;

        $acc_time_id = NULL;
        if (trim($time_id) !== '') {
            $acc_time = $this->general_model->fetch_specific_val("acc_time_id", "time_id=$time_id AND acc_id=$acc_id", "tbl_acc_time");
            if (isset($acc_time->acc_time_id) and $acc_time !== NULL) {
                $acc_time_id = $acc_time->acc_time_id;
            } else {
                //create acc_time_id
                $acc_time_id = $this->general_model->insert_vals_last_inserted_id(array('time_id' => $time_id, 'acc_id' => $acc_id, 'note' => 'DTR'), "tbl_acc_time");
            }
        }
        $data = array(
            'schedtype_id' => $schedtype_id,
            'acc_time_id' => $acc_time_id,
            'remarks' => $remarks,
            'updated_by' => $last_updater,
            'isActive' => 1,
            'isDouble' => 0,
            'isLocked' => 0,
            'leavenote' => $leaveType_ID
        );

        $res = $this->general_model->update_vals($data, "sched_id=$sched_id", "tbl_schedule");
        if ($res) {
            $query = "SELECT a.sched_id,a.sched_date,c.time_start,c.time_end,d.type,d.style,e.emp_id,c.time_id,a.leavenote,f.leaveType,a.schedtype_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id INNER JOIN tbl_user e ON e.emp_id=a.emp_id LEFT JOIN tbl_leave_type f ON a.leavenote=f.leaveType_ID WHERE a.sched_id=$sched_id ORDER BY a.sched_date";
            $result = $this->general_model->custom_query($query);
            if (empty($result)) {
                $status = "Failed";
            } else {
                $schedule = $result[0];
            }
            $status = "Success";
        } else {
            $status = "Failed";
        }
        echo json_encode(array('status' => $status, 'schedule' => $schedule));
    }

    public function deleteSingleSchedule()
    {
        $sched_id = $this->input->post('sched_id');
        $res = $this->general_model->delete_vals("sched_id=$sched_id", "tbl_schedule");
        if ($res) {
            $this->general_model->update_vals(array('sched_id' => NULL), "sched_id=$sched_id", "tbl_dtr_logs");
            $status = "Success";
        } else {
            $status = "Failed";
        }
        echo json_encode(array('status' => $status));
    }

    //-------------------------------------------------------------------SCHEDULE LOGS AREA-------------------------------------------------------------------------------------------------------------------
    public function schedule_logs()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/schedule/schedule_logs', $data);
        }
    }

    public function get_schedule_logs()
    {
        $uid = $this->session->userdata('uid');
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');

        $searchAccounts = $this->input->post('searchaccounts');
        $searchClass = $this->input->post('searchclass');
        $searchName = $this->input->post('searchname');
        $searchSchedTypes = $this->input->post('searchschedtypes');
        $search = array();
        $searchString = "";
        if ($searchAccounts !== '') {
            $search[] = "g.acc_id = $searchAccounts";
        }
        if ($searchSchedTypes !== '') {
            $search[] = "a.schedtype_id = $searchSchedTypes";
        }

        if ($searchClass !== '') {
            $search[] = "g.acc_description = '$searchClass'";
        }
        if ($searchName !== '') {
            $search[] = "(f.fname LIKE \"%$searchName%\" OR f.lname LIKE \"%$searchName%\")";
        }
        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $subordinateString = "";
        $subordinateString2 = "";
        if(!in_array($this->session->userdata('role_id'), array('1','5'))){//xx
             if (!in_array($uid, $this->administrator)) {
                $subordinates = $this->getEmployeeSubordinate($uid);
                $empids = array();
                foreach ($subordinates as $s) {
                    $empids[] = $s->emp_id;
                }
                if ($this->session->userdata('role_id') === '2') {
                    $empids[] = $this->session->userdata('emp_id');
                }
                if (empty($empids)) {
                    $subordinateString = " AND e.emp_id IN (0) ";
                    $subordinateString2 = " AND emp_id IN (0) ";
                } else {
                    $subordinateString = " AND e.emp_id IN (" . implode(",", $empids) . ") ";
                    $subordinateString2 = " AND emp_id IN (" . implode(",", $empids) . ") ";
                }
            }
        }
       
        //add these after where if you want to filter only schedtype with logs
        //d.schedtype_id IN (1,4,5) AND 
        $query = "SELECT f.fname,f.lname,f.mname,a.remarks,a.isLocked,a.sched_id,a.sched_date,c.time_start,c.time_end,d.type,d.style,e.emp_id,a.schedtype_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id INNER JOIN tbl_employee e ON a.emp_id=e.emp_id INNER JOIN tbl_applicant f ON e.apid=f.apid INNER JOIN tbl_account g ON e.acc_id=g.acc_id WHERE  date(a.sched_date) between date('$datestart') and date('$dateend')  AND e.isActive='yes' $subordinateString $searchString ORDER BY f.lname ASC,a.sched_date ASC";
        $schedulelogs = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $schedids = array();
        foreach ($schedulelogs as $schedlog) {
            $schedids[] = $schedlog->sched_id;
            $schedlog->clock_in = NULL;
            $schedlog->clock_out = NULL;
        }
        $logs = array();
        if (!empty($schedids)) {
            $logs = $this->general_model->fetch_specific_vals("dtr_id,entry,log,sched_id", "type='DTR' AND sched_id IN (" . implode(",", $schedids) . ") ", "tbl_dtr_logs");
        }

        $start_alllogs = date('Y-m-d', strtotime("-1 week", strtotime($datestart)));
        $end_alllogs = date('Y-m-d', strtotime("+1 week", strtotime($dateend)));
        $alllogs = $this->general_model->custom_query("SELECT dtr_id,entry,log,sched_id,emp_id FROM tbl_dtr_logs WHERE date(log) between date('$start_alllogs') and date('$end_alllogs') AND type='DTR' $subordinateString2 ORDER BY log ASC");
        foreach ($schedulelogs as $schedlog) {
            $available_clock_in = array();
            $available_clock_out = array();
            foreach ($alllogs as $thelog) {
                $sched_date = strtotime($schedlog->sched_date);
                if ($schedlog->emp_id === $thelog->emp_id) {
                    if ($sched_date > strtotime("-1 week", strtotime($thelog->log)) && $sched_date < strtotime("+1 week", strtotime($thelog->log))) {
                        if ($thelog->entry === 'I') {
                            $available_clock_in[] = $thelog;
                        } else {
                            $available_clock_out[] = $thelog;
                        }
                    }
                }
            }

            $schedlog->available_clock_in = $available_clock_in;
            $schedlog->available_clock_out = $available_clock_out;
        }
        foreach ($schedulelogs as $schedlog) {
            foreach ($logs as $key => $log) {
                if ($schedlog->sched_id === $log->sched_id) {
                    if ($log->entry === 'I') {
                         if($schedlog->clock_in == NULL){
                        $schedlog->clock_in = $log;
                        $schedlog->clock_in_id = $log->dtr_id;
                    }
                    } else {
                         if($schedlog->clock_out == NULL){
                        $schedlog->clock_out = $log;
                        $schedlog->clock_out_id = $log->dtr_id;
                    }
                    }
                    unset($logs[$key]);
                }
            }
        }
        $count = COUNT($this->general_model->custom_query($query));
        echo json_encode(array('data' => $schedulelogs, 'total' => $count));
    }
   

    public function assign_schedule_log()
    {
        $clocktype = $this->input->post('clocktype');
        $dtr_id = $this->input->post('dtr_id');
        $original_dtr_id = $this->input->post('original_dtr_id');
        $sched_id = $this->input->post('sched_id'); //sure
        $entry = ($clocktype === 'clockin') ? 'I' : 'O';
        $new_dtr_id = 0;
        $exists_sched = $this->general_model->fetch_specific_val("dtr_id,sched_id", "sched_id=$sched_id AND entry='$entry'", "tbl_dtr_logs");
        $sched_acc_time = $this->general_model->fetch_specific_val("acc_time_id", "sched_id=$sched_id", "tbl_schedule");

        if ($exists_sched === NULL) //NO LOG IS USING THE SCHED_ID
        {
            if ($original_dtr_id === '') //FROM NO SCHED TO WITH SCHED
            {
                $exists_logs = $this->general_model->fetch_specific_val("dtr_id,sched_id", "dtr_id=$dtr_id AND entry='$entry'", "tbl_dtr_logs");
                if ($exists_logs->sched_id === NULL) {
                    $res = $this->general_model->update_vals(array('sched_id' => $sched_id, 'acc_time_id' => $sched_acc_time->acc_time_id), "dtr_id = $dtr_id AND entry = '$entry'", "tbl_dtr_logs");
                    $new_dtr_id = $dtr_id;
                    $status = ($res === 1) ? "Success" : "Failed";
                } else {
                    $status = "Warning";
                }
            } else if ($dtr_id === '') //FROM WITH SCHED TO NO SCHED
            {
                $this->general_model->update_vals(array('sched_id' => NULL, 'acc_time_id' => NULL), "dtr_id = $original_dtr_id AND entry = '$entry'", "tbl_dtr_logs");
                $status = ($res === 1) ? "Success" : "Failed";
            } else {
                $status = "Unknown";
            }
        } else {
            if ($original_dtr_id === '') //FROM NO SCHED TO WITH SCHED
            {
                $status = "Warning";
            } else if ($dtr_id === '') //FROM WITH SCHED TO NO SCHED
            {
                $res = $this->general_model->update_vals(array('sched_id' => NULL, 'acc_time_id' => NULL), "dtr_id = $original_dtr_id AND entry = '$entry'", "tbl_dtr_logs");
                $status = ($res === 1) ? "Success" : "Failed";
            } else {   //change logs
                if ($exists_sched->dtr_id === $dtr_id) {
                    $status = "No Change";
                } else {
                    $exists_logs = $this->general_model->fetch_specific_val("dtr_id,sched_id", "dtr_id=$dtr_id AND entry='$entry'", "tbl_dtr_logs");
                    if ($exists_logs->sched_id === NULL) {
                        $res = $this->general_model->update_vals(array('sched_id' => NULL, 'acc_time_id' => NULL), "dtr_id = $original_dtr_id AND entry = '$entry'", "tbl_dtr_logs");
                        $res2 = $this->general_model->update_vals(array('sched_id' => $sched_id, 'acc_time_id' => $sched_acc_time->acc_time_id), "dtr_id = $dtr_id AND entry = '$entry'", "tbl_dtr_logs");
                        $new_dtr_id = $dtr_id;
                        $status = ($res === 1 && $res2 === 1) ? "Success" : "Failed";
                    } else {
                        $status = "Warning";
                    }
                }
            }
        }
        echo json_encode(array('status' => $status, 'new_dtr_id' => $new_dtr_id));
    }

    public function assign_schedule_log_v2()
    {
        $clocktype = $this->input->post('clocktype');
        $dtr_id = $this->input->post('dtr_id');
        $original_dtr_id = $this->input->post('original_dtr_id');
        $sched_id = $this->input->post('sched_id'); //sure

        $entry = ($clocktype === 'clockin') ? 'I' : 'O';
        $new_dtr_id = 0;
        $exists_sched_logs = $this->general_model->fetch_specific_val("dtr_id,sched_id", "sched_id=$sched_id AND entry='$entry'", "tbl_dtr_logs");
  
        $sched_acc_time = $this->general_model->fetch_specific_val("acc_time_id", "sched_id=$sched_id", "tbl_schedule");
        if ($exists_sched_logs === NULL) //NO LOG IS USING THE SCHED_ID
        {
           if ($dtr_id === '') //FROM WITH SCHED TO NO SCHED
            {
                $status = $this->schedule_logs_untagging($entry,$original_dtr_id,$sched_id);
            } else {
                $this->schedule_logs_untagging($entry,$original_dtr_id,$sched_id);
                $res_arr = $this->schedule_logs_tagging($dtr_id,$entry,$sched_id,$sched_acc_time,$original_dtr_id);
                $status = $res_arr['status'];
                $new_dtr_id = $res_arr['new_dtr_id'];
            }
        } else {
            if ($exists_sched_logs->dtr_id === $dtr_id) {
                $status = "No Change";
            } else {
                if ($dtr_id === '') //FROM WITH SCHED TO NO SCHED
                {
                    $status = $this->schedule_logs_untagging($entry,$original_dtr_id,$sched_id);
                } else {   //change logs                    
                    $res_arr = $this->schedule_logs_tagging($dtr_id,$entry,$sched_id,$sched_acc_time,$original_dtr_id);
                    $status = $res_arr['status'];
                    $new_dtr_id = $res_arr['new_dtr_id'];
                
                }
            }
            
        }

        echo json_encode(array('status' => $status, 'new_dtr_id' => $new_dtr_id));
    }

    private function schedule_logs_untagging($entry,$original_dtr_id,$sched_id){
        $this->general_model->update_vals(array('sched_id' => NULL, 'acc_time_id' => NULL,'note'=> NULL), "sched_id=$sched_id AND entry='$entry' AND dtr_id!=$original_dtr_id", "tbl_dtr_logs");
        if($entry=='I'){
            $res = $this->general_model->update_vals(array('sched_id' => NULL, 'acc_time_id' => NULL,'note'=> NULL), "dtr_id = $original_dtr_id", "tbl_dtr_logs");
            $clock_out = $this->general_model->fetch_specific_val("dtr_id", "sched_id=$sched_id AND entry='O'", "tbl_dtr_logs");
            if($clock_out!=NULL){
                $res = $this->general_model->update_vals(array('note'=> NULL), "dtr_id = ".$clock_out->dtr_id, "tbl_dtr_logs");
                $status = ($res === 1) ? "Success" : "Failed";
            }else {
                $status = "Success";
            }
        }else{
            $res = $this->general_model->update_vals(array('sched_id' => NULL, 'acc_time_id' => NULL,'note'=> NULL), "dtr_id = $original_dtr_id", "tbl_dtr_logs");
            $status = ($res === 1) ? "Success" : "Failed";
        }
        
        return $status;
    }
    private function schedule_logs_tagging($dtr_id,$entry,$sched_id,$sched_acc_time,$original_dtr_id){
        $exists_logs = $this->general_model->fetch_specific_val("dtr_id,sched_id", "dtr_id=$dtr_id AND entry='$entry'", "tbl_dtr_logs");
        
        $new_dtr_id = 0;
        if ($exists_logs->sched_id === NULL) { // check if the current log has schedule attached or not
            $this->schedule_logs_untagging($entry,$original_dtr_id,$sched_id);
            if($entry=='I'){ //CLOCK IN CHANGES
                $clock_out = $this->general_model->fetch_specific_val("dtr_id", "sched_id=$sched_id AND entry='O'", "tbl_dtr_logs");
                if($clock_out!=NULL){
                    $this->general_model->update_vals(array('note'=>$dtr_id), "dtr_id =".$clock_out->dtr_id, "tbl_dtr_logs");
                }
                $res = $this->general_model->update_vals(array('sched_id' => $sched_id, 'acc_time_id' => $sched_acc_time->acc_time_id, 'note'=>NULL), "dtr_id = $dtr_id", "tbl_dtr_logs");
            }else{ //CLOCK OUT CHANGES
                $clock_in = $this->general_model->fetch_specific_val("dtr_id", "sched_id=$sched_id AND entry='I'", "tbl_dtr_logs");
                if($clock_in!=NULL){ //NAAY CLOCK IN KAUBAN
                    $res = $this->general_model->update_vals(array('sched_id' => $sched_id, 'acc_time_id' => $sched_acc_time->acc_time_id, 'note'=>$clock_in->dtr_id), "dtr_id = $dtr_id", "tbl_dtr_logs");
                }else{ //WALAY CLOCK IN KAUBAN
                    $res = $this->general_model->update_vals(array('sched_id' => $sched_id, 'acc_time_id' => $sched_acc_time->acc_time_id, 'note'=>NULL), "dtr_id = $dtr_id", "tbl_dtr_logs");
                } 
            }
            $new_dtr_id = $dtr_id;
            $status = ($res === 1) ? "Success" : "Failed";
        } else {
            $status = "Warning";
        }
        return ['status'=>$status,'new_dtr_id'=>$new_dtr_id];
    }

    public function excel()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/schedule/excel', $data);
        }
    }

    public function getDownloadEmployees()
    {
        $searchclass = $this->input->post('searchclass');
        $searchaccounts = $this->input->post('searchaccounts');
        $search = array();
        $searchString = "";
        if ($searchclass !== '') {
            $search[] = "c.acc_description = '$searchclass'";
        }
        if ($searchaccounts !== '') {
            $search[] = "c.acc_id=$searchaccounts";
        }

        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $subordinateString = "";
        $uid = $this->session->userdata('uid');
       if(!in_array($this->session->userdata('role_id'), array('1','5'))){
            if (!in_array($uid, $this->administrator)) {
                $subordinates = $this->getEmployeeSubordinate($uid);
                $empids = array();
                foreach ($subordinates as $s) {
                    $empids[] = $s->emp_id;
                }
                if ($this->session->userdata('role_id') === '2') {
                    $empids[] = $this->session->userdata('emp_id');
                }
                if (empty($empids)) {
                    $subordinateString = " AND b.emp_id IN (0) ";
                } else {
                    $subordinateString = " AND b.emp_id IN (" . implode(",", $empids) . ") ";
                }
            }
        }
      
        $employees = $this->general_model->custom_query("SELECT a.fname,a.lname,b.emp_id,b.acc_id FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid=b.apid AND b.isActive='yes' AND c.acc_id=b.acc_id AND b.isActive='yes' $subordinateString $searchString ORDER BY a.lname ASC");

        echo json_encode(array('employees' => $employees));
    }

    public function downloadExcel($start_str, $end_str, $employeelist)
    {
        $schedtypes_notallowed = [3, 4, 5, 8];
        $schedtypes = $this->general_model->fetch_specific_vals("schedtype_id,type", "schedtype_id NOT IN (1," . implode(',', $schedtypes_notallowed) . ")", "tbl_schedule_type", "type ASC");
        $shifts = $this->general_model->fetch_specific_vals("time_start,time_end", "bta_id = 1", "tbl_time", "STR_TO_DATE(CONCAT(CURDATE(),' ',time_start), '%Y-%m-%d %l:%i:%s %p') ASC, STR_TO_DATE(CONCAT(CURDATE(),' ',time_end), '%Y-%m-%d %l:%i:%s %p') ASC");
        $startDate = date('Y-m-d', substr($start_str, 0, 10));
        $endDate = date('Y-m-d', substr($end_str, 0, 10));
        $emp_ids = explode("-", $employeelist);
        $period = new DatePeriod(
            new DateTime("$startDate"),
            new DateInterval('P1D'),
            new DateTime("$endDate +1 day ")
        );
        foreach ($period as $date) {
            $dates[] = $date->format("l - M d, Y"); //"D - M d, Y"
            $dates_ymd[] = $date->format("Y-m-d");
        }
        $employees = $this->general_model->fetch_specific_vals("a.lname,a.fname,b.emp_id", "a.apid=b.apid AND b.emp_id IN (" . implode(",", $emp_ids) . ")", "tbl_applicant a,tbl_employee b", "a.lname ASC");
        foreach ($employees as $emp) {
            $emp->schedules = $this->general_model->custom_query("SELECT a.sched_date,c.time_start,c.time_end,d.type,a.isLocked,a.schedtype_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id INNER JOIN tbl_user e ON e.emp_id=a.emp_id WHERE a.sched_date IN ('" . implode("','", $dates_ymd) . "') AND e.emp_id = " . $emp->emp_id);
        }
        //EXCEL PROPER
        $this->load->library('PHPExcel', NULL, 'excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->createSheet(1);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Schedule');
        $this->excel->getActiveSheet()->setShowGridlines(false);
        //------------------------INSERT LOGO-------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(5);  //setOffsetY has no effect
        $objDrawing->setCoordinates('B1');
        $objDrawing->setHeight(80); // logo height
        // $objDrawing->setWidth(320); // logo width
        // $objDrawing->setWidthAndHeight(200,400);
        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
        //set cell A1 content with some text
        $headerrow = 3;
        $headercol = 1;
        $letters = array();
        $this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(FALSE);
        $this->excel->getActiveSheet()->setCellValue('A1', 'CODE-SZScheduleTemplate-NEW');
        $this->excel->getActiveSheet()->setCellValue('A3', 'Profile Code');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Full Name');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Note: Select only the shift that is on the options provided in each cell. Manual shift input will cause error in uploading the schedule excel file. ');
        foreach ($dates as $d) {
            $headercol++;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headercol, $headerrow, $d);
            $this->excel->getActiveSheet()->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($headercol))->setWidth(24);
            array_push($letters, array('letter' => PHPExcel_Cell::stringFromColumnIndex($headercol), 'date' => $d));
        }

        //----------------REFERENCE----------------------------------------------------------------------------------------------
        $this->excel->setActiveSheetIndex(1);
        $this->excel->getActiveSheet()->setTitle('Reference');
        $cnt = 0;
        foreach ($shifts as $shift) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($cnt, 1, $shift->time_start . " - " . $shift->time_end);
            $cnt++;
        }
        foreach ($schedtypes as $sched) {

            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($cnt, 1, $sched->type);
            $cnt++;
        }

        $this->excel->addNamedRange(
            new PHPExcel_NamedRange(
                'Shifts',
                $this->excel->getActiveSheet(),
                'A1:' . $this->excel->getActiveSheet()->getHighestColumn() . '' . $this->excel->getActiveSheet()->getHighestRow()
            )
        );

        $objValidation = $this->excel->getActiveSheet()->getCellByColumnAndRow(0, 2)->getDataValidation();
        $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
        $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
        $objValidation->setAllowBlank(false);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setShowDropDown(true);
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('Value is not in list.');
        $objValidation->setPromptTitle('Pick from list');
        $objValidation->setPrompt('Please pick a value from the drop-down list.');
        $objValidation->setFormula1('=Shifts');
        $this->excel->setActiveSheetIndex(0);
        //----------------REFERENCE----------------------------------------------------------------------------------------------
        $emprow = 4;
        foreach ($employees as $emp) {
            $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $emp->emp_id);
            $this->excel->getActiveSheet()->setCellValue('B' . $emprow, $emp->lname . ', ' . $emp->fname);
            $emprow++;
        }


        $column = $this->excel->getActiveSheet()->getHighestColumn();
        $row = $this->excel->getActiveSheet()->getHighestRow();
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '4b4c54')), 'font' => array('bold' => true, 'color' => array('rgb' => 'ffffff'))));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
        $this->excel->getActiveSheet()->getProtection()->setSheet(true);
        $this->excel->getActiveSheet()->getStyle('C4:' . $column . '' . $row)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
        $this->excel->getActiveSheet()->freezePane('C4');
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(45);

        $emprow = 4;
        foreach ($employees as $emp) {
            $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $emp->emp_id);
            $this->excel->getActiveSheet()->setCellValue('B' . $emprow, $emp->lname . ', ' . $emp->fname);
            $schedules = $emp->schedules;
            $start = 0;
            foreach ($letters as $l) {
                $formatteddate = date_create_from_format("l - M d, Y", $l['date']);
                $dateSchedules = array();
                foreach ($schedules as $schedx) {
                    if ($schedx->sched_date === $formatteddate->format('Y-m-d')) {
                        $dateSchedules[] = $schedx;
                    }
                }
                if (!empty($dateSchedules)) {

                    //a.sched_date,c.time_start,c.time_end,d.type,a.isLocked,a.schedtype_id
                    $isMultiple = (COUNT($dateSchedules) > 1) ? true : false;
                    if ($isMultiple) {
                        $this->excel->getActiveSheet()->setCellValue($l['letter'] . '' . $emprow, "! Multiple Schedule");
                        $this->excel->getActiveSheet()->getStyle($l['letter'] . '' . $emprow)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                        $this->excel->getActiveSheet()->getStyle($l['letter'] . '' . $emprow)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '3c6478')), 'font' => array('bold' => true, 'size' => 10, 'color' => array('rgb' => 'ffffff'))));
                    } else {
                        if (in_array($dateSchedules[0]->schedtype_id, $schedtypes_notallowed) || $dateSchedules[0]->isLocked === '2' || $dateSchedules[0]->isLocked === '14') {
                            $this->excel->getActiveSheet()->setCellValue($l['letter'] . '' . $emprow, "! " . $dateSchedules[0]->type);
                            $this->excel->getActiveSheet()->getStyle($l['letter'] . '' . $emprow)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                            $this->excel->getActiveSheet()->getStyle($l['letter'] . '' . $emprow)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'c02f1d')), 'font' => array('bold' => true, 'size' => 10, 'color' => array('rgb' => 'ffffff'))));
                        } else if ($dateSchedules[0]->type == 'Normal') {

                            $this->excel->getActiveSheet()->setCellValue($l['letter'] . '' . $emprow, $dateSchedules[0]->time_start . ' - ' . $dateSchedules[0]->time_end);
                        } else {

                            $this->excel->getActiveSheet()->setCellValue($l['letter'] . '' . $emprow, $dateSchedules[0]->type);
                        }
                    }
                }

                $value = substr($this->excel->getActiveSheet()->getCell($l['letter'] . '' . $emprow)->getValue(), 0, 1);
                if ($value !== '!') {
                    $this->excel->getActiveSheet()->getCell($l['letter'] . '' . $emprow)->setDataValidation(clone $objValidation);
                }
                $start++;
            }
            $emprow++;
        }

        $this->excel->getSheetByName('Reference')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
        $filename = 'SZScheduleTemplate.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function excel_upload()
    {
        try {
            if (!isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
                throw new RuntimeException('Invalid parameters.');
            }

            switch ($_FILES['file']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }
            $dir = "uploads/schedules/" . $this->session->userdata('emp_id');
            if (file_exists($dir) && is_dir($dir)) { // print $dir . ' is a directory!'; 
            } else { // print $dir . ' is not directory'; 
                // mkdir($dir);
                mkdir($dir, 0777, true);
                chmod($dir, 0777);
            }
            if (file_exists($this->schedulefile_current)) {
                rename($this->schedulefile_current, $this->schedulefile_prev);
            }

            if (!move_uploaded_file($_FILES['file']['tmp_name'], $this->schedulefile_current)) {
                throw new RuntimeException('Failed to move uploaded file.');
            }


            $this->load->library('PHPExcel', NULL, 'excel');
            $objPHPExcel = PHPExcel_IOFactory::load($this->schedulefile_current);
            $code = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
            if ($code !== 'CODE-SZScheduleTemplate-NEW') {
                throw new RuntimeException('Schedule template mismatch !.');
            }

            // All good, send the response
            echo json_encode([
                'status' => 'ok',
                'path' => $this->schedulefile_current
            ]);
        } catch (RuntimeException $e) {
            // Something went wrong, send the err message as JSON
            http_response_code(400);

            echo json_encode([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function excel_preview()
    {
        $this->load->library('PHPExcel', NULL, 'excel');
        $objPHPExcel = PHPExcel_IOFactory::load($this->schedulefile_current);
        $maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
        $data = $objPHPExcel->getActiveSheet()->rangeToArray('B3:' . $maxCell['column'] . $maxCell['row']);

        if (empty($data)) {
            $status = "Empty";
        } else {
            $status = "Success";
        }
        echo json_encode(array('status' => $status, 'data' => $data));
    }

    private function excel_save_update($emp_id, $sched_date, $schedtype_id, $acc_time_id)
    {
        $uid = $this->session->userdata('uid');
        $values = array(
            'emp_id' => $emp_id,
            'sched_date' => $sched_date,
            'schedtype_id' => $schedtype_id,
            'acc_time_id' => $acc_time_id,
            'remarks' => "Excel",
            'updated_by' => $uid,
            'isActive' => 1,
            'isDouble' => 0,
            'isLocked' => 0
        );
        $isExists = $this->general_model->fetch_specific_val("sched_id,isLocked", "emp_id=$emp_id AND sched_date='$sched_date'", "tbl_schedule");
        if ($isExists === NULL || $isExists === '') {
            $res = $this->general_model->insert_vals($values, "tbl_schedule");
        } else {
            if ((int) $isExists->isLocked === 0) {
                $res = $this->general_model->update_vals($values, "sched_id=" . $isExists->sched_id, "tbl_schedule");
            } else {
                $res = 1;
            }
        }

        return $res;
    }

    public function excel_submit()
    {
        $this->load->library('PHPExcel', NULL, 'excel');
        $objPHPExcel = PHPExcel_IOFactory::load($this->schedulefile_current);
        $maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
        $colnumber = PHPExcel_Cell::columnIndexFromString($maxCell['column']);
        $data = $objPHPExcel->getActiveSheet()->rangeToArray('A3:' . $maxCell['column'] . $maxCell['row']);
        $res = array();
        $scheduleTypes = $this->general_model->fetch_all("schedtype_id,type", "tbl_schedule_type", "type ASC");
        for ($row = 1; $row < count($data); $row++) {
            for ($col = 2; $col < $colnumber; $col++) {
                $date = date_create_from_format('l - M d, Y', $data[0][$col]);
                $sched_date = date_format($date, 'Y-m-d');
                $emp_id = $data[$row][0];
                $schedule = $data[$row][$col];
                $shift = explode(" - ", $schedule);
                $warning = explode("! ", $schedule);
                $values = NULL;
                if (COUNT($shift) > 1) {
                    $start_shift = substr_replace($shift[0], "00", -5, 2);
                    $end_shift = substr_replace($shift[1], "00", -5, 2);
                    $schedtype_id = 1; //Normal by default
                    $acc_time = $this->general_model->fetch_specific_val("c.acc_time_id", "a.acc_id=b.acc_id AND b.acc_id=c.acc_id AND d.time_id=c.time_id AND time_start='$start_shift' AND time_end='$end_shift' AND a.emp_id=$emp_id", "tbl_employee as a,tbl_account as b,tbl_acc_time as c,tbl_time as d");
                    if ($acc_time === NULL) {
                        $acc_time_id = NULL;
                    } else {
                        $acc_time_id = $acc_time->acc_time_id;
                    }

                    $res[] = $this->excel_save_update($emp_id, $sched_date, $schedtype_id, $acc_time_id);
                } else {
                    $acc_time_id = NULL;
                    $schedtype_id = NULL;
                    if ($schedule === null) {
                        //                        echo "CODE DELETE HERE";
                    } else {

                        if (COUNT($warning) > 1) {
                            //                            echo "DO NOT TOUCH THE CODE";
                        } else {
                            foreach ($scheduleTypes as $schedtype) {
                                if ($schedtype->type == $schedule) {
                                    $schedtype_id = $schedtype->schedtype_id;
                                }
                            }

                            $res[] = $this->excel_save_update($emp_id, $sched_date, $schedtype_id, $acc_time_id);
                        }
                    }
                }
            }
        }
        if (array_sum($res) == count($res)) {
            $status = "Success";
        } else {
            $status = "Failed";
        }
        echo json_encode(array('status' => $status));
    }

    public function settings()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        // if ($this->check_access())
        // {
        $this->load_template_view('templates/schedule/settings', $data);
        // }
    }

    public function getScheduleBreaks()
    {
        $acc_id = $this->input->post('acc_id');
        $search_shift = $this->input->post('search_shift');
        $search = array();
        $searchString = "";
        if ($search_shift !== '') {
            $search[] = "(DATE_FORMAT(STR_TO_DATE(CONCAT(CURDATE(),' ',time_start),  '%Y-%m-%d %l:%i:%s %p'),'%h:%i %p') LIKE '%$search_shift%' OR DATE_FORMAT(STR_TO_DATE(CONCAT(CURDATE(),' ',time_end),  '%Y-%m-%d %l:%i:%s %p'),'%h:%i %p') LIKE '%$search_shift%')";
        }
        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT acc_time_id,acc_id,a.time_id, time_start,time_end FROM tbl_acc_time a,tbl_time b WHERE a.time_id=b.time_id AND a.acc_id=$acc_id AND a.note='DTR' $searchString ORDER BY STR_TO_DATE(CONCAT(CURDATE(),' ',time_start),  '%Y-%m-%d %l:%i:%s %p') ASC,STR_TO_DATE(CONCAT(CURDATE(),' ',time_end),  '%Y-%m-%d %l:%i:%s %p') ASC";
        $shifts = $this->general_model->custom_query($query);
        $acc_time_ids = array();
        foreach ($shifts as $shift) {
            $acc_time_ids[] = $shift->acc_time_id;
        }
        if (!empty($acc_time_ids)) {
            $breaks = $this->general_model->custom_query("SELECT a.acc_time_id,b.break_id,c.hour,c.min FROM tbl_shift_break a, tbl_break_time_account b, tbl_break_time c WHERE a.acc_time_id IN (" . implode(',', $acc_time_ids) . ") AND a.bta_id= b.bta_id AND b.btime_id = c.btime_id AND a.isActive = 1");
            foreach ($shifts as $shift) {
                $shift->min_firstb = "";
                $shift->min_lunchb = "";
                $shift->min_lastb = "";
                foreach ($breaks as $break) {
                    $finalBreak = "";
                    if ($break->hour == '0' && $break->min == '0') {
                        // $finalBreak = "--";
                        $finalBreak = '<span class="m--font-danger">0 mins</span>';
                    } else if ($break->hour == '0' && $break->min != '0') {
                        $finalBreak = ($break->min === '1') ? $break->min . " min" : $break->min . " mins";
                        $finalBreak = '<span class="m--font-brand">' . $finalBreak . '</span>';
                    } else if ($break->hour != '0' && $break->min == '0') {
                        $finalBreak = ($break->hour === '1') ? $break->hour . " hour" : $break->hour . " hrs";
                        $finalBreak = '<span class="m--font-success">' . $finalBreak . '</span>';
                    } else {
                        $finalHr = ($break->hour === '1') ? $break->hour . " hour" : $break->hour . " hrs";
                        $finalMin = ($break->min === '1') ? $break->min . " min" : $break->min . " mins";
                        $finalBreak = '<span class="m--font-info">' . $finalHr . ' & ' . $finalMin . '</span>';
                    }
                    if ($shift->acc_time_id === $break->acc_time_id) {
                        if ($break->break_id === '2') {
                            $shift->min_firstb = $finalBreak;
                        } else if ($break->break_id === '3') {
                            $shift->min_lunchb = $finalBreak;
                        } else if ($break->break_id === '4') {
                            $shift->min_lastb = $finalBreak;
                        }
                    }
                }
            }
        }

        echo json_encode(['shifts' => $shifts]);
    }
    public function get_available_break_times()
    {
        $data =  $this->general_model->fetch_specific_vals("*", "a.btime_id=b.btime_id", "tbl_break_time_account a,tbl_break_time b ");
        $firstb = array();
        $lunchb = array();
        $lastb = array();
        foreach ($data as $d) {
            if ($d->break_id == '2') {
                $firstb[] = $d;
            } else if ($d->break_id == '3') {
                $lunchb[] = $d;
            } else if ($d->break_id == '4') {
                $lastb[] = $d;
            }
        }
        echo json_encode(['firstb' => $firstb, 'lunchb' => $lunchb, 'lastb' => $lastb]);
    }
    public function get_used_shift_breaks()
    {
        $acc_time_id = $this->input->post('acc_time_id');
        $data =  $this->general_model->fetch_specific_vals("*", "a.acc_time_id=$acc_time_id AND a.bta_id=b.bta_id AND b.btime_id=c.btime_id AND a.isActive = 1", " tbl_shift_break a,tbl_break_time_account b,tbl_break_time c");
        $firstb = null;
        $lunchb = null;
        $lastb = null;
        foreach ($data as $d) {
            if ($d->break_id == '2') {
                $firstb = $d;
            } else if ($d->break_id == '3') {
                $lunchb = $d;
            } else if ($d->break_id == '4') {
                $lastb = $d;
            }
        }
        echo json_encode(['firstb' => $firstb, 'lunchb' => $lunchb, 'lastb' => $lastb]);
    }
    public function update_shift_break()
    {
        $fb_bta = $this->input->post('firstb');
        $l_bta = $this->input->post('lunchb');
        $lb_bta = $this->input->post('lastb');
        $acc_time_id = $this->input->post('acc_time_id');
        if ($fb_bta == '' && $l_bta == '' && $lb_bta == '') {
            $res = $this->general_model->update_vals(['isActive' => 0], "acc_time_id=$acc_time_id", "tbl_shift_break"); //reset break
            if ($res) {
                $status = "Success";
            } else {
                $status = "Failed";
            }
        } else {
            $res = $this->general_model->update_vals(['isActive' => 0], "acc_time_id=$acc_time_id", "tbl_shift_break"); //reset break
            $isfb = $this->general_model->fetch_specific_val("*", "acc_time_id=$acc_time_id AND bta_id = $fb_bta", "tbl_shift_break");
            $isl = $this->general_model->fetch_specific_val("*", "acc_time_id=$acc_time_id AND bta_id = $l_bta", "tbl_shift_break");
            $islb = $this->general_model->fetch_specific_val("*", "acc_time_id=$acc_time_id AND bta_id = $lb_bta", "tbl_shift_break");

            $rowfb = true;
            $rowl = true;
            $rowlb = true;
            if ($fb_bta !== null && $fb_bta !== '') {
                if ($isfb == NULL || $isfb == '') {
                    $rowfb = $this->general_model->insert_vals(['acc_time_id' => $acc_time_id, 'bta_id' => $fb_bta, 'isActive' => 1], 'tbl_shift_break');
                } else {
                    $rowfb = $this->general_model->update_vals(['isActive' => 1], "sbrk_id = $isfb->sbrk_id", "tbl_shift_break");
                }
            }
            if ($l_bta !== null && $l_bta !== '') {
                if ($isl == NULL || $isl == '') {
                    $rowl = $this->general_model->insert_vals(['acc_time_id' => $acc_time_id, 'bta_id' => $l_bta, 'isActive' => 1], 'tbl_shift_break');
                } else {
                    $rowl = $this->general_model->update_vals(['isActive' => 1], "sbrk_id = $isl->sbrk_id", "tbl_shift_break");
                }
            }
            if ($lb_bta !== null && $lb_bta !== '') {
                if ($islb == NULL || $islb == '') {
                    $rowlb = $this->general_model->insert_vals(['acc_time_id' => $acc_time_id, 'bta_id' => $lb_bta, 'isActive' => 1], 'tbl_shift_break');
                } else {
                    $rowlb = $this->general_model->update_vals(['isActive' => 1], "sbrk_id = $islb->sbrk_id", "tbl_shift_break");
                }
            }
            if ($rowfb && $rowl && $rowlb) {
                $status = "Success";
            } else {
                $status = "Failed";
            }
        }
        echo json_encode(['status' => $status]);
    }
    public function check_time_exists()
    {
        $time_start = $this->input->post('time_start');
        $time_end = $this->input->post('time_end');
        $ts = explode(" ", $time_start);
        $en = explode(" ", $time_end);
        $time_start = "$ts[0]:00 $ts[1]";
        $time_end = "$en[0]:00 $en[1]";
        $row = $this->general_model->fetch_specific_val("COUNT(*) as count", "time_start='$time_start' AND time_end='$time_end'", "tbl_time");
        echo json_encode(['count' => $row->count]);
    }
    public function create_shift()
    {
        $fb_bta = $this->input->post('firstb');
        $l_bta = $this->input->post('lunchb');
        $lb_bta = $this->input->post('lastb');
        $time_start = $this->input->post('time_start');
        $time_end = $this->input->post('time_end');

        $ts = explode(" ", $time_start);
        $en = explode(" ", $time_end);
        $time_start = "$ts[0]:00 $ts[1]";
        $time_end = "$en[0]:00 $en[1]";

        // $time_id = $this->ScheduleModel->createTime($time_start, $time_end);
        $time_id = $this->general_model->insert_vals_last_inserted_id(['time_start' => $time_start, 'time_end' => $time_end, 'time_break' => "", 'bta_id' => 1], "tbl_time");
        if ($time_id !== '' && $time_id !== NULL && $time_id !== 0) {
            // $accounts = $this->ScheduleModel->getAllAccount();
            $accounts = $this->general_model->fetch_all("acc_id", "tbl_account", "acc_name ASC");
            $isokay = true;
            foreach ($accounts as $acc) {
                $rowfb = true;
                $rowl = true;
                $rowlb = true;
                $acc_time_id = $this->general_model->insert_vals_last_inserted_id(['time_id' => $time_id, 'acc_id' => $acc->acc_id, 'note' => 'DTR'], "tbl_acc_time");
                if ($acc_time_id !== '' && $acc_time_id !== NULL && $acc_time_id !== 0) {
                    if ($fb_bta !== null && $fb_bta !== '') {
                        $rowfb = $this->general_model->insert_vals(['acc_time_id' => $acc_time_id, 'bta_id' => $fb_bta, 'isActive' => 1], 'tbl_shift_break');
                    }
                    if ($l_bta !== null && $l_bta !== '') {
                        $rowl = $this->general_model->insert_vals(['acc_time_id' => $acc_time_id, 'bta_id' => $l_bta, 'isActive' => 1], 'tbl_shift_break');
                    }
                    if ($lb_bta !== null && $lb_bta !== '') {
                        $rowlb = $this->general_model->insert_vals(['acc_time_id' => $acc_time_id, 'bta_id' => $lb_bta, 'isActive' => 1], 'tbl_shift_break');
                    }
                    if ($rowfb && $rowl && $rowlb) {
                        $isokay = true;
                    } else {
                        $isokay = false;
                    }
                } else {
                    $isokay = false;
                }
            }
            if ($isokay) {
                $status = "Success";
            } else {
                $status = "Failed";
            }
        } else {
            $status = "Failed";
        }
        echo json_encode(['status' => $status]);
    }

    public function intraday()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        $uid = $this->session->uid;

        // if ($this->check_access()) {
            $this->load_template_view('templates/schedule/intraday', $data);
        // }
    }
    private function filter_unique($array, $column){
        $unique_array = array();
        $key_array = array();
        foreach($array as $arrkey => $arr){
            if(!in_array($arr[$column],$key_array)){
                $key_array[] = $arr[$column];
                $unique_array[] = $arr;
            }
        }
        return $unique_array;
    }
    private function get_intraday_computations($searchStartDate, $searchEndDate,$searchStartTime,$searchEndTime,$searchAccounts,$searchteamleads,$searchmanagers){
        date_default_timezone_set('Asia/Manila');
        $uid = $this->session->uid;

        $searchStartTime = strtotime($searchStartDate.' '.$searchStartTime);
        $searchEndTime = strtotime($searchEndDate.' '.$searchEndTime);
        $searchStartDate = date('Y-m-d',$searchStartTime);
        $searchEndDate = date('Y-m-d',$searchEndTime);
        $scheduleFilter = $searchStartDate==$searchEndDate?"'$searchStartDate'":"'$searchStartDate','$searchEndDate'";

        $search = array();
        $searchString = "";
        if (is_array($searchAccounts) && $searchAccounts != NULL) {
            $searchAccounts = array_filter($searchAccounts);
            if(COUNT($searchAccounts)>0){
                $searchString = " AND b.acc_id IN (".implode($searchAccounts,',').")";
            }
        }

        $teamleadSearchString = "";
        if ($searchteamleads !== '' && $searchteamleads != NULL) {
            $teamleadSearchString = " AND b.Supervisor = $searchteamleads";
        } 
     
        $subordinateString = "";
        if(!in_array($this->session->userdata('role_id'), array('1','5'))){
             if (!in_array($uid, $this->administrator)) {
                $subordinates = $this->getEmployeeSubordinate($uid);
                $empids = array();
                foreach ($subordinates as $s) {
                    $empids[] = $s->emp_id;
                }
                if ($this->session->userdata('role_id') === '2') {
                    $empids[] = $this->session->userdata('emp_id');
                }
                if (empty($empids)) {
                    $subordinateString = " AND emp_id IN (0) ";
                } else {
                    $subordinateString = " AND emp_id IN (" . implode(",", $empids) . ") ";
                }
            }
        }
       
        //remove staffing ninjas and genuent and cca floating
        $employees = $this->general_model->custom_query("SELECT b.emp_id,a.lname,a.fname,b.acc_id,b.Supervisor,c.acc_name FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid = b.apid AND b.isActive = 'yes' AND b.acc_id = c.acc_id AND c.acc_description ='Agent' AND c.acc_id NOT IN (32,81,45) $subordinateString $searchString $teamleadSearchString ORDER BY a.lname, a.fname ASC");
        //query fetching based on position class => SELECT b.emp_id,a.lname,a.fname,b.acc_id,b.Supervisor,c.acc_name,c.acc_description,f.class FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_emp_promote d,tbl_pos_emp_stat e, tbl_position f WHERE a.apid = b.apid AND b.isActive = 'yes' AND b.acc_id = c.acc_id AND c.acc_description ='Agent' AND d.emp_id = b.emp_id AND d.isActive = 1 AND d.isHistory = 1 AND e.posempstat_id = d.posempstat_id AND e.pos_id = f.pos_id ORDER BY a.lname, a.fname ASC
        $schedules = $this->general_model->custom_query("SELECT a.sched_id,a.sched_date,c.time_start,c.time_end,a.emp_id,c.time_id,a.schedtype_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id WHERE a.sched_date IN ($scheduleFilter) $subordinateString $searchString AND a.schedtype_id IN (1,4) ORDER BY a.sched_date");

        $schedIds = array();
        $filter_employees = array();
        foreach($employees as $empkey => $emp){
            $emp->schedules = array();
            $tempScheds = array();
            foreach($schedules as $schedkey => $sched){
                if($sched->time_start=='12:00:00 AM'){
                    $shift_time_start = strtotime('+1 day', strtotime($sched->sched_date.' '.$sched->time_start));
                }else{
                    $shift_time_start = strtotime($sched->sched_date.' '.$sched->time_start);
                }
                if($sched->emp_id == $emp->emp_id){
                	//NOTE: Remove && $shift_time_start<=strtotime(date('H:i')) if not required ang filtering sa current time
                    if($searchStartTime <= $shift_time_start && $shift_time_start <= $searchEndTime && $shift_time_start<=strtotime(date('H:i'))){
                        // echo date("Y-m-d h:i A",$searchStartTime).' <= '.date("Y-m-d h:i A",$shift_time_start).' <= '.date("Y-m-d h:i A",$searchEndTime).'<br>';
                        // echo $emp->lname.'<br>';
                        // echo "between giset nga time<br>";
                        $tempScheds[] = array('sched_id'=>$sched->sched_id,'time_start'=> $sched->time_start,'time_end'=>$sched->time_end,'login'=>NULL);
                        array_push($schedIds,$sched->sched_id);
                    }
                    unset($schedules[$schedkey]);
                }
            }
            if(COUNT($tempScheds)>0){
                $emp->schedules = $tempScheds;
                array_push($filter_employees,$employees[$empkey]);
            }
            
        }
    // echo "<pre>";
    //     var_dump($filter_employees);
    //     echo "</pre>";
        if(COUNT($schedIds)>0){
            $dtrLogs = $this->general_model->custom_query("SELECT log,entry,sched_id FROM tbl_dtr_logs WHERE sched_id IN (".implode($schedIds, ',').") AND entry = 'I' AND type='DTR'");
            foreach($filter_employees as $empkey => $emp){
                foreach($emp->schedules as $schedkey => $sched){
                    foreach($dtrLogs as $logkey => $log){
                        if($log->sched_id ==$sched['sched_id']){
                            $filter_employees[$empkey]->schedules[$schedkey]['login'] = $log->log;
                            unset($dtrLogs[$logkey]);
                       }
                    }
                }
            }
        }
    
        $accounts = array();
        foreach($filter_employees as  $emp){
            array_push($accounts, array(
                'acc_id'=>$emp->acc_id,
                'acc_name'=>$emp->acc_name,
                'supervisors'=>array(),
                'supervisorArray'=>array()
            ));
        }
        
        $tempAcc = array_unique(array_column($accounts, 'acc_id'));
        $accounts = array_intersect_key($accounts, $tempAcc);
        $directSupIds = array();
        foreach($accounts as $key => $acc){
            foreach($filter_employees as $emp){
                if($acc['acc_id']==$emp->acc_id && !in_array($emp->Supervisor, $acc['supervisors'])){
                    array_push($accounts[$key]['supervisors'] ,$emp->Supervisor);
                    array_push($directSupIds,$emp->Supervisor);
                }
            }
            $accounts[$key]['supervisors'] = array_filter(array_unique($accounts[$key]['supervisors']));
        }
        $directSupIds = array_filter(array_unique($directSupIds));
        
        $all_accounts = array();
        $all_teamleads = array();
        $all_managers = array();
        $total_absentlate = 0;
        $total_present = 0;
        $total_scheduled = 0;
        if(COUNT($directSupIds)>0){
            $directSupervisors = $this->general_model->custom_query("SELECT b.emp_id,a.lname,a.fname,b.Supervisor FROM tbl_applicant a, tbl_employee b WHERE b.emp_id IN (".implode($directSupIds,',').") AND a.apid = b.apid");
            $nextSupIds = array_filter(array_unique(array_column($directSupervisors,'Supervisor')));
            if(COUNT($nextSupIds)>0){
                $nextSupervisors = $this->general_model->custom_query("SELECT b.emp_id,a.lname,a.fname,a.mname FROM tbl_applicant a, tbl_employee b WHERE b.emp_id IN (".implode($nextSupIds,',').") AND a.apid = b.apid");
            }

            foreach ($accounts as $key => $acc){
                
                $accounts[$key]['type']='normal';
                foreach ($acc['supervisors'] as $supkey => $supId){
                    foreach($directSupervisors as $dir){
                        if($supId == $dir->emp_id){
                            $accounts[$key]['supervisorArray'][$supkey]['directSupervisorId'] = $supId;
                            $accounts[$key]['supervisorArray'][$supkey]['directSupervisor'] = $dir->lname.", ".$dir->fname;
                            $accounts[$key]['supervisorArray'][$supkey]['nextSupervisorId'] = $dir->Supervisor;

                            foreach($filter_employees as $empkey => $emp){
                                if($emp->Supervisor==$supId && $emp->acc_id==$acc['acc_id']){
                                    $accounts[$key]['supervisorArray'][$supkey]['data'][] = array('lname'=>$emp->lname,'fname'=>$emp->fname,'empid'=>$emp->emp_id,'schedules'=>$emp->schedules);
                                    
                                }
                            }
                        }
                    }
                    foreach($nextSupervisors as $next){
                        if($accounts[$key]['supervisorArray'][$supkey]['nextSupervisorId'] == $next->emp_id){
                            $accounts[$key]['supervisorArray'][$supkey]['nextSupervisor'] = substr($next->fname,0,1).substr($next->mname,0,1).substr($next->lname,0,1);
                            $accounts[$key]['supervisorArray'][$supkey]['nextSupervisorFull'] = $next->lname.', '.$next->fname;
                            
                        }
                    }
                }
            }
            // echo "<pre>";
            // var_dump($accounts);
            // echo "</pre>";
            foreach ($accounts as $key => $acc){
                $all_accounts[] = array('acc_id'=>$acc['acc_id'],'acc_name'=>$acc['acc_name']);
                foreach($acc['supervisorArray'] as $supkey => $supervisor){
                    if ($searchmanagers !== '' && $searchmanagers != NULL && $supervisor['nextSupervisorId']!=$searchmanagers) {
                        unset($accounts[$key]['supervisorArray'][$supkey]);
                    }else{
                        $all_teamleads[] = array('emp_id'=>$supervisor['directSupervisorId'],'name'=>$supervisor['directSupervisor']);
                        $all_managers[] = array('emp_id'=>$supervisor['nextSupervisorId'],'name'=>$supervisor['nextSupervisor']);
                        $absentlate = 0;
                        $present = 0;
                        $total = 0;
                        foreach($supervisor['data'] as $subdata){
                            foreach($subdata['schedules'] as $sched){
                                $total_scheduled++;
                                if($sched['login']!==NULL){
                                    $present++;
                                    $total_present++;
                                }else{
                                    $absentlate++;
                                    $total_absentlate++;
                                }
                                $total++;
                            }
                           
                        }
                        $accounts[$key]['supervisorArray'][$supkey]['absentlate'] = $absentlate;
                        $accounts[$key]['supervisorArray'][$supkey]['present'] = $present;
                        $accounts[$key]['supervisorArray'][$supkey]['totalscheduled'] = $total;
                        $accounts[$key]['supervisorArray'][$supkey]['percentage'] = number_format(round(100*($present/$total),2), 2)."%";
                    }
                    
                }
                usort($accounts[$key]['supervisorArray'], function($a, $b) {
                    return $a['directSupervisor'] <=> $b['directSupervisor'];
                });
                
                unset($accounts[$key]['supervisors']);
            }
        }

        //------------------------------------------------CUSTOMIZE ACCOUNTS-------------------------------------------------------------------------------------
        $customAccounts[] = array('account_name'=>'ONYX TOTAL','accIds'=>array(23,80));
        $customAccounts[] = array('account_name'=>'LONEWOLF TOTAL','accIds'=>array(82,83));
        
      
        foreach($customAccounts as $custom){
            $newAccountData = array();
            $custom_absentlate = 0;
            $custom_present = 0;
            $custom_scheduled = 0;
            foreach($accounts as $acckey => $account){
                if($account['type']=='normal'){
                    if(in_array($account['acc_id'],$custom['accIds'])){
                        $newAccountData[] = $account;
                        foreach($account['supervisorArray'] as $supervisor){
                            $custom_absentlate += $supervisor['absentlate'];
                            $custom_present += $supervisor['present'];
                            $custom_scheduled += $supervisor['totalscheduled'];
                        }
                        unset($accounts[$acckey]);
                    }
                }
            }
            if(COUNT($newAccountData)>0){
                $newAccount = array('acc_name'=>$custom['account_name'],'type'=>'custom','customdata'=>$newAccountData,'custom_absentlate'=>$custom_absentlate,'custom_present'=>$custom_present,'custom_scheduled'=>$custom_scheduled,'custom_percentage'=>number_format(round(100*($custom_present/$custom_scheduled),2), 2)."%");
                array_push($accounts,$newAccount);
            }
        }
        //-------------------------------------------------------------------------------------------------------------------------------------
    

        usort($all_accounts, function($a, $b) {
            return $a['acc_name'] <=> $b['acc_name'];
        });
   
        usort($accounts, function($a, $b) {
            return $a['acc_name'] <=> $b['acc_name'];
        });
   
        // $all_accounts = $this->filter_unique($accounts, 'acc_id');
        $all_teamleads = $this->filter_unique($all_teamleads, 'emp_id');
        $all_managers = $this->filter_unique($all_managers, 'emp_id');
        usort($all_teamleads, function($a, $b) {
            return $a['name'] <=> $b['name'];
        });
        usort($all_managers, function($a, $b) {
            return $a['name'] <=> $b['name'];
        });
        
        $total_percentage_str = $total_scheduled==0?"0.00%": number_format(round(100*($total_present/$total_scheduled),2), 2)."%";

        return array(
            'data' => $accounts,
            'currentDate'=>date('M d, Y'),
            'currentTime'=>date('h:i a'),
            'total_absentlate'=>$total_absentlate,
            'total_present'=>$total_present,
            'total_scheduled'=>$total_scheduled,
            'total_percentage'=>$total_percentage_str,
            'all_accounts'=>$all_accounts,
            'all_teamleads'=>$all_teamleads,
            'all_managers'=>$all_managers,
            'searchStartTime'=>date('M d, Y h:i A',$searchStartTime),
            'searchEndTime'=>date('M d, Y h:i A',$searchEndTime)
        );
        
    }
    
    
    public function get_intraday_record()
    {
        $searchStartDate = $this->input->post('searchstartdate');
        $searchEndDate = $this->input->post('searchenddate');
        // $searchDate = '2020-11-09';
        $searchAccounts = $this->input->post('searchaccounts');
        $searchteamleads = $this->input->post('searchteamleads');
        $searchmanagers = $this->input->post('searchmanagers');
        $searchStartTime = $this->input->post('searchstarttime');
        $searchEndTime = $this->input->post('searchendtime');
        // $searchStartTime = '12:01 AM';
        // $searchEndTime = '12:00 AM';
        $data = $this->get_intraday_computations( $searchStartDate, $searchEndDate ,$searchStartTime,$searchEndTime,$searchAccounts,$searchteamleads,$searchmanagers);
        echo json_encode($data);
    }

    public function downloadExcel_intraday()
    {
        $searchStartDate = $this->input->post('searchstartdate');
        $searchEndDate = $this->input->post('searchenddate');
        $searchAccounts = $this->input->post('searchaccounts');
        $searchteamleads = $this->input->post('searchteamleads');
        $searchmanagers = $this->input->post('searchmanagers');
        $searchStartTime = $this->input->post('searchstarttime');
        $searchEndTime = $this->input->post('searchendtime');
        $final_data = $this->get_intraday_computations( $searchStartDate, $searchEndDate ,$searchStartTime,$searchEndTime,$searchAccounts,$searchteamleads,$searchmanagers);

        //EXCEL PROPER
        $this->load->library('PHPExcel', NULL, 'excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->createSheet(1);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Intraday Attendance Report');
        $this->excel->getActiveSheet()->setShowGridlines(false);
        //------------------------INSERT LOGO-------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(5);  //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(80); // logo height
        // $objDrawing->setWidth(320); // logo width
        // $objDrawing->setWidthAndHeight(200,400);
        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
        //set cell A1 content with some text


        $this->excel->getActiveSheet()->setCellValue('A3', 'START TIME: '. $final_data['searchStartTime']);
        $this->excel->getActiveSheet()->setCellValue('A4', 'END TIME: '. $final_data['searchEndTime']);
        $this->excel->getActiveSheet()->setCellValue('B3', 'GENERATED ON:');
        $this->excel->getActiveSheet()->setCellValue('B4', $final_data['currentDate'].' '.$final_data['currentTime']);
        $this->excel->getActiveSheet()->setCellValue('C3', 'TOTAL:');
        $this->excel->getActiveSheet()->mergeCells('C3:C4');
        $this->excel->getActiveSheet()->mergeCells('D3:D4');
        $this->excel->getActiveSheet()->mergeCells('E3:E4');
        $this->excel->getActiveSheet()->mergeCells('F3:F4');
        $this->excel->getActiveSheet()->mergeCells('G3:G4');
        $this->excel->getActiveSheet()->setCellValue('D3', $final_data['total_absentlate']);
        $this->excel->getActiveSheet()->setCellValue('E3', $final_data['total_present']);
        $this->excel->getActiveSheet()->setCellValue('F3', $final_data['total_scheduled']);
        $this->excel->getActiveSheet()->setCellValue('G3', $final_data['total_percentage']);
        $this->excel->getActiveSheet()->getStyle("C3:G3")->getFont()->setSize(18);


        $this->excel->getActiveSheet()->setCellValue('A5', 'ACCOUNT');

        $this->excel->getActiveSheet()->setCellValue('A5', 'ACCOUNT');
        $this->excel->getActiveSheet()->setCellValue('B5', 'TEAM LEADER');
        $this->excel->getActiveSheet()->setCellValue('C5', 'AM');
        $this->excel->getActiveSheet()->setCellValue('D5', 'Absent / Late');
        $this->excel->getActiveSheet()->setCellValue('E5', 'Present');
        $this->excel->getActiveSheet()->setCellValue('F5', 'Total Scheduled');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Attendance');
   
        $emprow = 6;
        foreach ($final_data['data'] as $data) {
            if ($data['type'] == 'custom') {
                $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $data['acc_name']);
                $this->excel->getActiveSheet()->setCellValue('D' . $emprow, $data['custom_absentlate']);
                $this->excel->getActiveSheet()->setCellValue('E' . $emprow, $data['custom_present']);
                $this->excel->getActiveSheet()->setCellValue('F' . $emprow, $data['custom_scheduled']);
                $this->excel->getActiveSheet()->setCellValue('G' . $emprow, $data['custom_percentage']);
                $this->excel->getActiveSheet()->getStyle('B'. $emprow .':G'. $emprow)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'dddddd')), 'font' => array('bold' => true)));
                $this->excel->getActiveSheet()->getStyle('B'. $emprow .':G'. $emprow)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
                
                $this->excel->getActiveSheet()->getStyle('A'. $emprow)->applyFromArray(array('font' => array('bold' => true)));
                $firstEmprow = $emprow;
                $emprow++;
                
               foreach($data['customdata'] as $custom){
                    foreach($custom['supervisorArray'] as $supervisor){
                        $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $custom['acc_name']);
                        $this->excel->getActiveSheet()->setCellValue('B' . $emprow, $supervisor['directSupervisor']);
                        $this->excel->getActiveSheet()->setCellValue('C' . $emprow, $supervisor['nextSupervisor']);
                        $this->excel->getActiveSheet()->setCellValue('D' . $emprow, $supervisor['absentlate']);
                        $this->excel->getActiveSheet()->setCellValue('E' . $emprow, $supervisor['present']);
                        $this->excel->getActiveSheet()->setCellValue('F' . $emprow, $supervisor['totalscheduled']);
                        $this->excel->getActiveSheet()->setCellValue('G' . $emprow, $supervisor['percentage']);
                        $this->excel->getActiveSheet()->getStyle('A'. $emprow)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)));
                        $this->excel->getActiveSheet()->getStyle('B'. $emprow .':G'. $emprow)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
                        $this->excel->getActiveSheet()->getStyle('A'. $emprow)->getFont()->setItalic(true);
                        $emprow++;
                    }
               }
               $this->excel->getActiveSheet()->getStyle('A'.$firstEmprow.':A'.$emprow)->applyFromArray(array('borders' => array('outsideborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
              
            }else{
                
                $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $data['acc_name']);
                $this->excel->getActiveSheet()->getStyle('A' . $emprow)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
                
                $count = COUNT($data['supervisorArray']);
                if($count>1){
                    $this->excel->getActiveSheet()->mergeCells('A'.$emprow.':A'.($emprow+$count-1));
                }
                foreach($data['supervisorArray'] as $supervisor){
                    $this->excel->getActiveSheet()->getStyle('B'. $emprow .':G'. $emprow)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
                
                    $this->excel->getActiveSheet()->setCellValue('B' . $emprow, $supervisor['directSupervisor']);
                    $this->excel->getActiveSheet()->setCellValue('C' . $emprow, $supervisor['nextSupervisor']);
                    $this->excel->getActiveSheet()->setCellValue('D' . $emprow, $supervisor['absentlate']);
                    $this->excel->getActiveSheet()->setCellValue('E' . $emprow, $supervisor['present']);
                    $this->excel->getActiveSheet()->setCellValue('F' . $emprow, $supervisor['totalscheduled']);
                    $this->excel->getActiveSheet()->setCellValue('G' . $emprow, $supervisor['percentage']);
                    $emprow++;
                }
            }
           
        }


        $column = $this->excel->getActiveSheet()->getHighestColumn();
        $row = $this->excel->getActiveSheet()->getHighestRow();
        $this->excel->getActiveSheet()->getStyle('C3:' . $column . '3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'ffffff')), 'font' => array('bold' => true, 'color' => array('rgb' => '002060'))));
        $this->excel->getActiveSheet()->getStyle('C3:' . $column . '3')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        
        // $this->excel->getActiveSheet()->getStyle('A5:' . $column . '' . $row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
        $this->excel->getActiveSheet()->getStyle('A5:' . $column . '5')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '0074CD')), 'font' => array('bold' => true, 'color' => array('rgb' => 'ffffff'))));
        $this->excel->getActiveSheet()->getStyle('A5:' . $column . '5')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('A5:' . $column . '' . $row)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);

        $filename = 'SZIntradayAttendanceReport.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
  
}